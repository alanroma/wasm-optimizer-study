; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btStridingMeshInterface.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btStridingMeshInterface.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btStridingMeshInterface = type { i32 (...)**, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btInternalTriangleIndexCallback = type { i32 (...)** }
%struct.AabbCalculationCallback = type { %class.btInternalTriangleIndexCallback, %class.btVector3, %class.btVector3 }
%class.btSerializer = type { i32 (...)** }
%struct.btStridingMeshInterfaceData = type { %struct.btMeshPartData*, %struct.btVector3FloatData, i32, [4 x i8] }
%struct.btMeshPartData = type { %struct.btVector3FloatData*, %struct.btVector3DoubleData*, %struct.btIntIndexData*, %struct.btShortIntIndexTripletData*, %struct.btCharIndexTripletData*, %struct.btShortIntIndexData*, i32, i32 }
%struct.btVector3DoubleData = type { [4 x double] }
%struct.btIntIndexData = type { i32 }
%struct.btShortIntIndexTripletData = type { [3 x i16], [2 x i8] }
%struct.btCharIndexTripletData = type { [3 x i8], i8 }
%struct.btShortIntIndexData = type { i16, [2 x i8] }
%struct.btVector3FloatData = type { [4 x float] }
%class.btChunk = type { i32, i32, i8*, i32, i32 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK23btStridingMeshInterface10getScalingEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

$_ZNK23btStridingMeshInterface14hasPremadeAabbEv = comdat any

$_ZNK23btStridingMeshInterface14setPremadeAabbERK9btVector3S2_ = comdat any

$_ZNK23btStridingMeshInterface14getPremadeAabbEP9btVector3S1_ = comdat any

$_ZNK23btStridingMeshInterface28calculateSerializeBufferSizeEv = comdat any

$_ZN31btInternalTriangleIndexCallbackC2Ev = comdat any

$_ZN9btVector36setMinERKS_ = comdat any

$_ZN9btVector36setMaxERKS_ = comdat any

$_Z8btSetMinIfEvRT_RKS0_ = comdat any

$_ZNK9btVector31wEv = comdat any

$_Z8btSetMaxIfEvRT_RKS0_ = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@.str = private unnamed_addr constant [15 x i8] c"btIntIndexData\00", align 1
@.str.1 = private unnamed_addr constant [27 x i8] c"btShortIntIndexTripletData\00", align 1
@.str.2 = private unnamed_addr constant [23 x i8] c"btCharIndexTripletData\00", align 1
@.str.3 = private unnamed_addr constant [19 x i8] c"btVector3FloatData\00", align 1
@.str.4 = private unnamed_addr constant [20 x i8] c"btVector3DoubleData\00", align 1
@.str.5 = private unnamed_addr constant [15 x i8] c"btMeshPartData\00", align 1
@.str.6 = private unnamed_addr constant [28 x i8] c"btStridingMeshInterfaceData\00", align 1
@_ZTV23btStridingMeshInterface = hidden unnamed_addr constant { [17 x i8*] } { [17 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI23btStridingMeshInterface to i8*), i8* bitcast (%class.btStridingMeshInterface* (%class.btStridingMeshInterface*)* @_ZN23btStridingMeshInterfaceD1Ev to i8*), i8* bitcast (void (%class.btStridingMeshInterface*)* @_ZN23btStridingMeshInterfaceD0Ev to i8*), i8* bitcast (void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_ to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (i1 (%class.btStridingMeshInterface*)* @_ZNK23btStridingMeshInterface14hasPremadeAabbEv to i8*), i8* bitcast (void (%class.btStridingMeshInterface*, %class.btVector3*, %class.btVector3*)* @_ZNK23btStridingMeshInterface14setPremadeAabbERK9btVector3S2_ to i8*), i8* bitcast (void (%class.btStridingMeshInterface*, %class.btVector3*, %class.btVector3*)* @_ZNK23btStridingMeshInterface14getPremadeAabbEP9btVector3S1_ to i8*), i8* bitcast (i32 (%class.btStridingMeshInterface*)* @_ZNK23btStridingMeshInterface28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)* @_ZNK23btStridingMeshInterface9serializeEPvP12btSerializer to i8*)] }, align 4
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS23btStridingMeshInterface = hidden constant [26 x i8] c"23btStridingMeshInterface\00", align 1
@_ZTI23btStridingMeshInterface = hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btStridingMeshInterface, i32 0, i32 0) }, align 4
@_ZTVZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback to i8*), i8* bitcast (%struct.AabbCalculationCallback* (%struct.AabbCalculationCallback*)* @_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallbackD2Ev to i8*), i8* bitcast (void (%struct.AabbCalculationCallback*)* @_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallbackD0Ev to i8*), i8* bitcast (void (%struct.AabbCalculationCallback*, %class.btVector3*, i32, i32)* @_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallback28internalProcessTriangleIndexEPS0_ii to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTSZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback = internal constant [94 x i8] c"ZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback\00", align 1
@_ZTI31btInternalTriangleIndexCallback = external constant i8*
@_ZTIZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([94 x i8], [94 x i8]* @_ZTSZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback, i32 0, i32 0), i8* bitcast (i8** @_ZTI31btInternalTriangleIndexCallback to i8*) }, align 4
@_ZTV31btInternalTriangleIndexCallback = external unnamed_addr constant { [5 x i8*] }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btStridingMeshInterface.cpp, i8* null }]

@_ZN23btStridingMeshInterfaceD1Ev = hidden unnamed_addr alias %class.btStridingMeshInterface* (%class.btStridingMeshInterface*), %class.btStridingMeshInterface* (%class.btStridingMeshInterface*)* @_ZN23btStridingMeshInterfaceD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btStridingMeshInterface* @_ZN23btStridingMeshInterfaceD2Ev(%class.btStridingMeshInterface* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  ret %class.btStridingMeshInterface* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN23btStridingMeshInterfaceD0Ev(%class.btStridingMeshInterface* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  call void @llvm.trap() #7
  unreachable
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #2

; Function Attrs: noinline optnone
define hidden void @_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_(%class.btStridingMeshInterface* %this, %class.btInternalTriangleIndexCallback* %callback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  %callback.addr = alloca %class.btInternalTriangleIndexCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %numtotalphysicsverts = alloca i32, align 4
  %part = alloca i32, align 4
  %graphicssubparts = alloca i32, align 4
  %vertexbase = alloca i8*, align 4
  %indexbase = alloca i8*, align 4
  %indexstride = alloca i32, align 4
  %type = alloca i32, align 4
  %gfxindextype = alloca i32, align 4
  %stride = alloca i32, align 4
  %numverts = alloca i32, align 4
  %numtriangles = alloca i32, align 4
  %gfxindex = alloca i32, align 4
  %triangle = alloca [3 x %class.btVector3], align 16
  %meshScaling = alloca %class.btVector3, align 4
  %graphicsbase = alloca float*, align 4
  %tri_indices = alloca i32*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp49 = alloca float, align 4
  %ref.tmp53 = alloca float, align 4
  %tri_indices63 = alloca i16*, align 4
  %ref.tmp70 = alloca float, align 4
  %ref.tmp74 = alloca float, align 4
  %ref.tmp78 = alloca float, align 4
  %ref.tmp87 = alloca float, align 4
  %ref.tmp91 = alloca float, align 4
  %ref.tmp95 = alloca float, align 4
  %ref.tmp104 = alloca float, align 4
  %ref.tmp108 = alloca float, align 4
  %ref.tmp112 = alloca float, align 4
  %tri_indices126 = alloca i8*, align 4
  %ref.tmp134 = alloca float, align 4
  %ref.tmp138 = alloca float, align 4
  %ref.tmp142 = alloca float, align 4
  %ref.tmp151 = alloca float, align 4
  %ref.tmp155 = alloca float, align 4
  %ref.tmp159 = alloca float, align 4
  %ref.tmp168 = alloca float, align 4
  %ref.tmp172 = alloca float, align 4
  %ref.tmp176 = alloca float, align 4
  %graphicsbase187 = alloca double*, align 4
  %tri_indices192 = alloca i32*, align 4
  %ref.tmp199 = alloca float, align 4
  %ref.tmp204 = alloca float, align 4
  %ref.tmp209 = alloca float, align 4
  %ref.tmp218 = alloca float, align 4
  %ref.tmp223 = alloca float, align 4
  %ref.tmp228 = alloca float, align 4
  %ref.tmp237 = alloca float, align 4
  %ref.tmp242 = alloca float, align 4
  %ref.tmp247 = alloca float, align 4
  %tri_indices262 = alloca i16*, align 4
  %ref.tmp270 = alloca float, align 4
  %ref.tmp275 = alloca float, align 4
  %ref.tmp280 = alloca float, align 4
  %ref.tmp290 = alloca float, align 4
  %ref.tmp295 = alloca float, align 4
  %ref.tmp300 = alloca float, align 4
  %ref.tmp310 = alloca float, align 4
  %ref.tmp315 = alloca float, align 4
  %ref.tmp320 = alloca float, align 4
  %tri_indices335 = alloca i8*, align 4
  %ref.tmp343 = alloca float, align 4
  %ref.tmp348 = alloca float, align 4
  %ref.tmp353 = alloca float, align 4
  %ref.tmp363 = alloca float, align 4
  %ref.tmp368 = alloca float, align 4
  %ref.tmp373 = alloca float, align 4
  %ref.tmp383 = alloca float, align 4
  %ref.tmp388 = alloca float, align 4
  %ref.tmp393 = alloca float, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  store %class.btInternalTriangleIndexCallback* %callback, %class.btInternalTriangleIndexCallback** %callback.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  store i32 0, i32* %numtotalphysicsverts, align 4
  %2 = bitcast %class.btStridingMeshInterface* %this1 to i32 (%class.btStridingMeshInterface*)***
  %vtable = load i32 (%class.btStridingMeshInterface*)**, i32 (%class.btStridingMeshInterface*)*** %2, align 4
  %vfn = getelementptr inbounds i32 (%class.btStridingMeshInterface*)*, i32 (%class.btStridingMeshInterface*)** %vtable, i64 7
  %3 = load i32 (%class.btStridingMeshInterface*)*, i32 (%class.btStridingMeshInterface*)** %vfn, align 4
  %call = call i32 %3(%class.btStridingMeshInterface* %this1)
  store i32 %call, i32* %graphicssubparts, align 4
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %this1)
  %4 = bitcast %class.btVector3* %meshScaling to i8*
  %5 = bitcast %class.btVector3* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  store i32 0, i32* %part, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc410, %arrayctor.cont
  %6 = load i32, i32* %part, align 4
  %7 = load i32, i32* %graphicssubparts, align 4
  %cmp = icmp slt i32 %6, %7
  br i1 %cmp, label %for.body, label %for.end412

for.body:                                         ; preds = %for.cond
  %8 = load i32, i32* %part, align 4
  %9 = bitcast %class.btStridingMeshInterface* %this1 to void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)***
  %vtable4 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)**, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*** %9, align 4
  %vfn5 = getelementptr inbounds void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vtable4, i64 4
  %10 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vfn5, align 4
  call void %10(%class.btStridingMeshInterface* %this1, i8** %vertexbase, i32* nonnull align 4 dereferenceable(4) %numverts, i32* nonnull align 4 dereferenceable(4) %type, i32* nonnull align 4 dereferenceable(4) %stride, i8** %indexbase, i32* nonnull align 4 dereferenceable(4) %indexstride, i32* nonnull align 4 dereferenceable(4) %numtriangles, i32* nonnull align 4 dereferenceable(4) %gfxindextype, i32 %8)
  %11 = load i32, i32* %numtriangles, align 4
  %mul = mul nsw i32 %11, 3
  %12 = load i32, i32* %numtotalphysicsverts, align 4
  %add = add nsw i32 %12, %mul
  store i32 %add, i32* %numtotalphysicsverts, align 4
  %13 = load i32, i32* %type, align 4
  switch i32 %13, label %sw.default406 [
    i32 0, label %sw.bb
    i32 1, label %sw.bb186
  ]

sw.bb:                                            ; preds = %for.body
  %14 = load i32, i32* %gfxindextype, align 4
  switch i32 %14, label %sw.default [
    i32 2, label %sw.bb6
    i32 3, label %sw.bb59
    i32 5, label %sw.bb122
  ]

sw.bb6:                                           ; preds = %sw.bb
  store i32 0, i32* %gfxindex, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc, %sw.bb6
  %15 = load i32, i32* %gfxindex, align 4
  %16 = load i32, i32* %numtriangles, align 4
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body9, label %for.end

for.body9:                                        ; preds = %for.cond7
  %17 = load i8*, i8** %indexbase, align 4
  %18 = load i32, i32* %gfxindex, align 4
  %19 = load i32, i32* %indexstride, align 4
  %mul10 = mul nsw i32 %18, %19
  %add.ptr = getelementptr inbounds i8, i8* %17, i32 %mul10
  %20 = bitcast i8* %add.ptr to i32*
  store i32* %20, i32** %tri_indices, align 4
  %21 = load i8*, i8** %vertexbase, align 4
  %22 = load i32*, i32** %tri_indices, align 4
  %arrayidx = getelementptr inbounds i32, i32* %22, i32 0
  %23 = load i32, i32* %arrayidx, align 4
  %24 = load i32, i32* %stride, align 4
  %mul11 = mul i32 %23, %24
  %add.ptr12 = getelementptr inbounds i8, i8* %21, i32 %mul11
  %25 = bitcast i8* %add.ptr12 to float*
  store float* %25, float** %graphicsbase, align 4
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %26 = load float*, float** %graphicsbase, align 4
  %arrayidx14 = getelementptr inbounds float, float* %26, i32 0
  %27 = load float, float* %arrayidx14, align 4
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %28 = load float, float* %call15, align 4
  %mul16 = fmul float %27, %28
  store float %mul16, float* %ref.tmp, align 4
  %29 = load float*, float** %graphicsbase, align 4
  %arrayidx18 = getelementptr inbounds float, float* %29, i32 1
  %30 = load float, float* %arrayidx18, align 4
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %31 = load float, float* %call19, align 4
  %mul20 = fmul float %30, %31
  store float %mul20, float* %ref.tmp17, align 4
  %32 = load float*, float** %graphicsbase, align 4
  %arrayidx22 = getelementptr inbounds float, float* %32, i32 2
  %33 = load float, float* %arrayidx22, align 4
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %34 = load float, float* %call23, align 4
  %mul24 = fmul float %33, %34
  store float %mul24, float* %ref.tmp21, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx13, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %35 = load i8*, i8** %vertexbase, align 4
  %36 = load i32*, i32** %tri_indices, align 4
  %arrayidx25 = getelementptr inbounds i32, i32* %36, i32 1
  %37 = load i32, i32* %arrayidx25, align 4
  %38 = load i32, i32* %stride, align 4
  %mul26 = mul i32 %37, %38
  %add.ptr27 = getelementptr inbounds i8, i8* %35, i32 %mul26
  %39 = bitcast i8* %add.ptr27 to float*
  store float* %39, float** %graphicsbase, align 4
  %arrayidx28 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 1
  %40 = load float*, float** %graphicsbase, align 4
  %arrayidx30 = getelementptr inbounds float, float* %40, i32 0
  %41 = load float, float* %arrayidx30, align 4
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %42 = load float, float* %call31, align 4
  %mul32 = fmul float %41, %42
  store float %mul32, float* %ref.tmp29, align 4
  %43 = load float*, float** %graphicsbase, align 4
  %arrayidx34 = getelementptr inbounds float, float* %43, i32 1
  %44 = load float, float* %arrayidx34, align 4
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %45 = load float, float* %call35, align 4
  %mul36 = fmul float %44, %45
  store float %mul36, float* %ref.tmp33, align 4
  %46 = load float*, float** %graphicsbase, align 4
  %arrayidx38 = getelementptr inbounds float, float* %46, i32 2
  %47 = load float, float* %arrayidx38, align 4
  %call39 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %48 = load float, float* %call39, align 4
  %mul40 = fmul float %47, %48
  store float %mul40, float* %ref.tmp37, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx28, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp37)
  %49 = load i8*, i8** %vertexbase, align 4
  %50 = load i32*, i32** %tri_indices, align 4
  %arrayidx41 = getelementptr inbounds i32, i32* %50, i32 2
  %51 = load i32, i32* %arrayidx41, align 4
  %52 = load i32, i32* %stride, align 4
  %mul42 = mul i32 %51, %52
  %add.ptr43 = getelementptr inbounds i8, i8* %49, i32 %mul42
  %53 = bitcast i8* %add.ptr43 to float*
  store float* %53, float** %graphicsbase, align 4
  %arrayidx44 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 2
  %54 = load float*, float** %graphicsbase, align 4
  %arrayidx46 = getelementptr inbounds float, float* %54, i32 0
  %55 = load float, float* %arrayidx46, align 4
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %56 = load float, float* %call47, align 4
  %mul48 = fmul float %55, %56
  store float %mul48, float* %ref.tmp45, align 4
  %57 = load float*, float** %graphicsbase, align 4
  %arrayidx50 = getelementptr inbounds float, float* %57, i32 1
  %58 = load float, float* %arrayidx50, align 4
  %call51 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %59 = load float, float* %call51, align 4
  %mul52 = fmul float %58, %59
  store float %mul52, float* %ref.tmp49, align 4
  %60 = load float*, float** %graphicsbase, align 4
  %arrayidx54 = getelementptr inbounds float, float* %60, i32 2
  %61 = load float, float* %arrayidx54, align 4
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %62 = load float, float* %call55, align 4
  %mul56 = fmul float %61, %62
  store float %mul56, float* %ref.tmp53, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx44, float* nonnull align 4 dereferenceable(4) %ref.tmp45, float* nonnull align 4 dereferenceable(4) %ref.tmp49, float* nonnull align 4 dereferenceable(4) %ref.tmp53)
  %63 = load %class.btInternalTriangleIndexCallback*, %class.btInternalTriangleIndexCallback** %callback.addr, align 4
  %arraydecay = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %64 = load i32, i32* %part, align 4
  %65 = load i32, i32* %gfxindex, align 4
  %66 = bitcast %class.btInternalTriangleIndexCallback* %63 to void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)***
  %vtable57 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)**, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*** %66, align 4
  %vfn58 = getelementptr inbounds void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vtable57, i64 2
  %67 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vfn58, align 4
  call void %67(%class.btInternalTriangleIndexCallback* %63, %class.btVector3* %arraydecay, i32 %64, i32 %65)
  br label %for.inc

for.inc:                                          ; preds = %for.body9
  %68 = load i32, i32* %gfxindex, align 4
  %inc = add nsw i32 %68, 1
  store i32 %inc, i32* %gfxindex, align 4
  br label %for.cond7

for.end:                                          ; preds = %for.cond7
  br label %sw.epilog

sw.bb59:                                          ; preds = %sw.bb
  store i32 0, i32* %gfxindex, align 4
  br label %for.cond60

for.cond60:                                       ; preds = %for.inc119, %sw.bb59
  %69 = load i32, i32* %gfxindex, align 4
  %70 = load i32, i32* %numtriangles, align 4
  %cmp61 = icmp slt i32 %69, %70
  br i1 %cmp61, label %for.body62, label %for.end121

for.body62:                                       ; preds = %for.cond60
  %71 = load i8*, i8** %indexbase, align 4
  %72 = load i32, i32* %gfxindex, align 4
  %73 = load i32, i32* %indexstride, align 4
  %mul64 = mul nsw i32 %72, %73
  %add.ptr65 = getelementptr inbounds i8, i8* %71, i32 %mul64
  %74 = bitcast i8* %add.ptr65 to i16*
  store i16* %74, i16** %tri_indices63, align 4
  %75 = load i8*, i8** %vertexbase, align 4
  %76 = load i16*, i16** %tri_indices63, align 4
  %arrayidx66 = getelementptr inbounds i16, i16* %76, i32 0
  %77 = load i16, i16* %arrayidx66, align 2
  %conv = zext i16 %77 to i32
  %78 = load i32, i32* %stride, align 4
  %mul67 = mul nsw i32 %conv, %78
  %add.ptr68 = getelementptr inbounds i8, i8* %75, i32 %mul67
  %79 = bitcast i8* %add.ptr68 to float*
  store float* %79, float** %graphicsbase, align 4
  %arrayidx69 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %80 = load float*, float** %graphicsbase, align 4
  %arrayidx71 = getelementptr inbounds float, float* %80, i32 0
  %81 = load float, float* %arrayidx71, align 4
  %call72 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %82 = load float, float* %call72, align 4
  %mul73 = fmul float %81, %82
  store float %mul73, float* %ref.tmp70, align 4
  %83 = load float*, float** %graphicsbase, align 4
  %arrayidx75 = getelementptr inbounds float, float* %83, i32 1
  %84 = load float, float* %arrayidx75, align 4
  %call76 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %85 = load float, float* %call76, align 4
  %mul77 = fmul float %84, %85
  store float %mul77, float* %ref.tmp74, align 4
  %86 = load float*, float** %graphicsbase, align 4
  %arrayidx79 = getelementptr inbounds float, float* %86, i32 2
  %87 = load float, float* %arrayidx79, align 4
  %call80 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %88 = load float, float* %call80, align 4
  %mul81 = fmul float %87, %88
  store float %mul81, float* %ref.tmp78, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx69, float* nonnull align 4 dereferenceable(4) %ref.tmp70, float* nonnull align 4 dereferenceable(4) %ref.tmp74, float* nonnull align 4 dereferenceable(4) %ref.tmp78)
  %89 = load i8*, i8** %vertexbase, align 4
  %90 = load i16*, i16** %tri_indices63, align 4
  %arrayidx82 = getelementptr inbounds i16, i16* %90, i32 1
  %91 = load i16, i16* %arrayidx82, align 2
  %conv83 = zext i16 %91 to i32
  %92 = load i32, i32* %stride, align 4
  %mul84 = mul nsw i32 %conv83, %92
  %add.ptr85 = getelementptr inbounds i8, i8* %89, i32 %mul84
  %93 = bitcast i8* %add.ptr85 to float*
  store float* %93, float** %graphicsbase, align 4
  %arrayidx86 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 1
  %94 = load float*, float** %graphicsbase, align 4
  %arrayidx88 = getelementptr inbounds float, float* %94, i32 0
  %95 = load float, float* %arrayidx88, align 4
  %call89 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %96 = load float, float* %call89, align 4
  %mul90 = fmul float %95, %96
  store float %mul90, float* %ref.tmp87, align 4
  %97 = load float*, float** %graphicsbase, align 4
  %arrayidx92 = getelementptr inbounds float, float* %97, i32 1
  %98 = load float, float* %arrayidx92, align 4
  %call93 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %99 = load float, float* %call93, align 4
  %mul94 = fmul float %98, %99
  store float %mul94, float* %ref.tmp91, align 4
  %100 = load float*, float** %graphicsbase, align 4
  %arrayidx96 = getelementptr inbounds float, float* %100, i32 2
  %101 = load float, float* %arrayidx96, align 4
  %call97 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %102 = load float, float* %call97, align 4
  %mul98 = fmul float %101, %102
  store float %mul98, float* %ref.tmp95, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx86, float* nonnull align 4 dereferenceable(4) %ref.tmp87, float* nonnull align 4 dereferenceable(4) %ref.tmp91, float* nonnull align 4 dereferenceable(4) %ref.tmp95)
  %103 = load i8*, i8** %vertexbase, align 4
  %104 = load i16*, i16** %tri_indices63, align 4
  %arrayidx99 = getelementptr inbounds i16, i16* %104, i32 2
  %105 = load i16, i16* %arrayidx99, align 2
  %conv100 = zext i16 %105 to i32
  %106 = load i32, i32* %stride, align 4
  %mul101 = mul nsw i32 %conv100, %106
  %add.ptr102 = getelementptr inbounds i8, i8* %103, i32 %mul101
  %107 = bitcast i8* %add.ptr102 to float*
  store float* %107, float** %graphicsbase, align 4
  %arrayidx103 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 2
  %108 = load float*, float** %graphicsbase, align 4
  %arrayidx105 = getelementptr inbounds float, float* %108, i32 0
  %109 = load float, float* %arrayidx105, align 4
  %call106 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %110 = load float, float* %call106, align 4
  %mul107 = fmul float %109, %110
  store float %mul107, float* %ref.tmp104, align 4
  %111 = load float*, float** %graphicsbase, align 4
  %arrayidx109 = getelementptr inbounds float, float* %111, i32 1
  %112 = load float, float* %arrayidx109, align 4
  %call110 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %113 = load float, float* %call110, align 4
  %mul111 = fmul float %112, %113
  store float %mul111, float* %ref.tmp108, align 4
  %114 = load float*, float** %graphicsbase, align 4
  %arrayidx113 = getelementptr inbounds float, float* %114, i32 2
  %115 = load float, float* %arrayidx113, align 4
  %call114 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %116 = load float, float* %call114, align 4
  %mul115 = fmul float %115, %116
  store float %mul115, float* %ref.tmp112, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx103, float* nonnull align 4 dereferenceable(4) %ref.tmp104, float* nonnull align 4 dereferenceable(4) %ref.tmp108, float* nonnull align 4 dereferenceable(4) %ref.tmp112)
  %117 = load %class.btInternalTriangleIndexCallback*, %class.btInternalTriangleIndexCallback** %callback.addr, align 4
  %arraydecay116 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %118 = load i32, i32* %part, align 4
  %119 = load i32, i32* %gfxindex, align 4
  %120 = bitcast %class.btInternalTriangleIndexCallback* %117 to void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)***
  %vtable117 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)**, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*** %120, align 4
  %vfn118 = getelementptr inbounds void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vtable117, i64 2
  %121 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vfn118, align 4
  call void %121(%class.btInternalTriangleIndexCallback* %117, %class.btVector3* %arraydecay116, i32 %118, i32 %119)
  br label %for.inc119

for.inc119:                                       ; preds = %for.body62
  %122 = load i32, i32* %gfxindex, align 4
  %inc120 = add nsw i32 %122, 1
  store i32 %inc120, i32* %gfxindex, align 4
  br label %for.cond60

for.end121:                                       ; preds = %for.cond60
  br label %sw.epilog

sw.bb122:                                         ; preds = %sw.bb
  store i32 0, i32* %gfxindex, align 4
  br label %for.cond123

for.cond123:                                      ; preds = %for.inc183, %sw.bb122
  %123 = load i32, i32* %gfxindex, align 4
  %124 = load i32, i32* %numtriangles, align 4
  %cmp124 = icmp slt i32 %123, %124
  br i1 %cmp124, label %for.body125, label %for.end185

for.body125:                                      ; preds = %for.cond123
  %125 = load i8*, i8** %indexbase, align 4
  %126 = load i32, i32* %gfxindex, align 4
  %127 = load i32, i32* %indexstride, align 4
  %mul127 = mul nsw i32 %126, %127
  %add.ptr128 = getelementptr inbounds i8, i8* %125, i32 %mul127
  store i8* %add.ptr128, i8** %tri_indices126, align 4
  %128 = load i8*, i8** %vertexbase, align 4
  %129 = load i8*, i8** %tri_indices126, align 4
  %arrayidx129 = getelementptr inbounds i8, i8* %129, i32 0
  %130 = load i8, i8* %arrayidx129, align 1
  %conv130 = zext i8 %130 to i32
  %131 = load i32, i32* %stride, align 4
  %mul131 = mul nsw i32 %conv130, %131
  %add.ptr132 = getelementptr inbounds i8, i8* %128, i32 %mul131
  %132 = bitcast i8* %add.ptr132 to float*
  store float* %132, float** %graphicsbase, align 4
  %arrayidx133 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %133 = load float*, float** %graphicsbase, align 4
  %arrayidx135 = getelementptr inbounds float, float* %133, i32 0
  %134 = load float, float* %arrayidx135, align 4
  %call136 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %135 = load float, float* %call136, align 4
  %mul137 = fmul float %134, %135
  store float %mul137, float* %ref.tmp134, align 4
  %136 = load float*, float** %graphicsbase, align 4
  %arrayidx139 = getelementptr inbounds float, float* %136, i32 1
  %137 = load float, float* %arrayidx139, align 4
  %call140 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %138 = load float, float* %call140, align 4
  %mul141 = fmul float %137, %138
  store float %mul141, float* %ref.tmp138, align 4
  %139 = load float*, float** %graphicsbase, align 4
  %arrayidx143 = getelementptr inbounds float, float* %139, i32 2
  %140 = load float, float* %arrayidx143, align 4
  %call144 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %141 = load float, float* %call144, align 4
  %mul145 = fmul float %140, %141
  store float %mul145, float* %ref.tmp142, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx133, float* nonnull align 4 dereferenceable(4) %ref.tmp134, float* nonnull align 4 dereferenceable(4) %ref.tmp138, float* nonnull align 4 dereferenceable(4) %ref.tmp142)
  %142 = load i8*, i8** %vertexbase, align 4
  %143 = load i8*, i8** %tri_indices126, align 4
  %arrayidx146 = getelementptr inbounds i8, i8* %143, i32 1
  %144 = load i8, i8* %arrayidx146, align 1
  %conv147 = zext i8 %144 to i32
  %145 = load i32, i32* %stride, align 4
  %mul148 = mul nsw i32 %conv147, %145
  %add.ptr149 = getelementptr inbounds i8, i8* %142, i32 %mul148
  %146 = bitcast i8* %add.ptr149 to float*
  store float* %146, float** %graphicsbase, align 4
  %arrayidx150 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 1
  %147 = load float*, float** %graphicsbase, align 4
  %arrayidx152 = getelementptr inbounds float, float* %147, i32 0
  %148 = load float, float* %arrayidx152, align 4
  %call153 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %149 = load float, float* %call153, align 4
  %mul154 = fmul float %148, %149
  store float %mul154, float* %ref.tmp151, align 4
  %150 = load float*, float** %graphicsbase, align 4
  %arrayidx156 = getelementptr inbounds float, float* %150, i32 1
  %151 = load float, float* %arrayidx156, align 4
  %call157 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %152 = load float, float* %call157, align 4
  %mul158 = fmul float %151, %152
  store float %mul158, float* %ref.tmp155, align 4
  %153 = load float*, float** %graphicsbase, align 4
  %arrayidx160 = getelementptr inbounds float, float* %153, i32 2
  %154 = load float, float* %arrayidx160, align 4
  %call161 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %155 = load float, float* %call161, align 4
  %mul162 = fmul float %154, %155
  store float %mul162, float* %ref.tmp159, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx150, float* nonnull align 4 dereferenceable(4) %ref.tmp151, float* nonnull align 4 dereferenceable(4) %ref.tmp155, float* nonnull align 4 dereferenceable(4) %ref.tmp159)
  %156 = load i8*, i8** %vertexbase, align 4
  %157 = load i8*, i8** %tri_indices126, align 4
  %arrayidx163 = getelementptr inbounds i8, i8* %157, i32 2
  %158 = load i8, i8* %arrayidx163, align 1
  %conv164 = zext i8 %158 to i32
  %159 = load i32, i32* %stride, align 4
  %mul165 = mul nsw i32 %conv164, %159
  %add.ptr166 = getelementptr inbounds i8, i8* %156, i32 %mul165
  %160 = bitcast i8* %add.ptr166 to float*
  store float* %160, float** %graphicsbase, align 4
  %arrayidx167 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 2
  %161 = load float*, float** %graphicsbase, align 4
  %arrayidx169 = getelementptr inbounds float, float* %161, i32 0
  %162 = load float, float* %arrayidx169, align 4
  %call170 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %163 = load float, float* %call170, align 4
  %mul171 = fmul float %162, %163
  store float %mul171, float* %ref.tmp168, align 4
  %164 = load float*, float** %graphicsbase, align 4
  %arrayidx173 = getelementptr inbounds float, float* %164, i32 1
  %165 = load float, float* %arrayidx173, align 4
  %call174 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %166 = load float, float* %call174, align 4
  %mul175 = fmul float %165, %166
  store float %mul175, float* %ref.tmp172, align 4
  %167 = load float*, float** %graphicsbase, align 4
  %arrayidx177 = getelementptr inbounds float, float* %167, i32 2
  %168 = load float, float* %arrayidx177, align 4
  %call178 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %169 = load float, float* %call178, align 4
  %mul179 = fmul float %168, %169
  store float %mul179, float* %ref.tmp176, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx167, float* nonnull align 4 dereferenceable(4) %ref.tmp168, float* nonnull align 4 dereferenceable(4) %ref.tmp172, float* nonnull align 4 dereferenceable(4) %ref.tmp176)
  %170 = load %class.btInternalTriangleIndexCallback*, %class.btInternalTriangleIndexCallback** %callback.addr, align 4
  %arraydecay180 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %171 = load i32, i32* %part, align 4
  %172 = load i32, i32* %gfxindex, align 4
  %173 = bitcast %class.btInternalTriangleIndexCallback* %170 to void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)***
  %vtable181 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)**, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*** %173, align 4
  %vfn182 = getelementptr inbounds void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vtable181, i64 2
  %174 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vfn182, align 4
  call void %174(%class.btInternalTriangleIndexCallback* %170, %class.btVector3* %arraydecay180, i32 %171, i32 %172)
  br label %for.inc183

for.inc183:                                       ; preds = %for.body125
  %175 = load i32, i32* %gfxindex, align 4
  %inc184 = add nsw i32 %175, 1
  store i32 %inc184, i32* %gfxindex, align 4
  br label %for.cond123

for.end185:                                       ; preds = %for.cond123
  br label %sw.epilog

sw.default:                                       ; preds = %sw.bb
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %for.end185, %for.end121, %for.end
  br label %sw.epilog407

sw.bb186:                                         ; preds = %for.body
  %176 = load i32, i32* %gfxindextype, align 4
  switch i32 %176, label %sw.default404 [
    i32 2, label %sw.bb188
    i32 3, label %sw.bb258
    i32 5, label %sw.bb331
  ]

sw.bb188:                                         ; preds = %sw.bb186
  store i32 0, i32* %gfxindex, align 4
  br label %for.cond189

for.cond189:                                      ; preds = %for.inc255, %sw.bb188
  %177 = load i32, i32* %gfxindex, align 4
  %178 = load i32, i32* %numtriangles, align 4
  %cmp190 = icmp slt i32 %177, %178
  br i1 %cmp190, label %for.body191, label %for.end257

for.body191:                                      ; preds = %for.cond189
  %179 = load i8*, i8** %indexbase, align 4
  %180 = load i32, i32* %gfxindex, align 4
  %181 = load i32, i32* %indexstride, align 4
  %mul193 = mul nsw i32 %180, %181
  %add.ptr194 = getelementptr inbounds i8, i8* %179, i32 %mul193
  %182 = bitcast i8* %add.ptr194 to i32*
  store i32* %182, i32** %tri_indices192, align 4
  %183 = load i8*, i8** %vertexbase, align 4
  %184 = load i32*, i32** %tri_indices192, align 4
  %arrayidx195 = getelementptr inbounds i32, i32* %184, i32 0
  %185 = load i32, i32* %arrayidx195, align 4
  %186 = load i32, i32* %stride, align 4
  %mul196 = mul i32 %185, %186
  %add.ptr197 = getelementptr inbounds i8, i8* %183, i32 %mul196
  %187 = bitcast i8* %add.ptr197 to double*
  store double* %187, double** %graphicsbase187, align 4
  %arrayidx198 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %188 = load double*, double** %graphicsbase187, align 4
  %arrayidx200 = getelementptr inbounds double, double* %188, i32 0
  %189 = load double, double* %arrayidx200, align 8
  %conv201 = fptrunc double %189 to float
  %call202 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %190 = load float, float* %call202, align 4
  %mul203 = fmul float %conv201, %190
  store float %mul203, float* %ref.tmp199, align 4
  %191 = load double*, double** %graphicsbase187, align 4
  %arrayidx205 = getelementptr inbounds double, double* %191, i32 1
  %192 = load double, double* %arrayidx205, align 8
  %conv206 = fptrunc double %192 to float
  %call207 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %193 = load float, float* %call207, align 4
  %mul208 = fmul float %conv206, %193
  store float %mul208, float* %ref.tmp204, align 4
  %194 = load double*, double** %graphicsbase187, align 4
  %arrayidx210 = getelementptr inbounds double, double* %194, i32 2
  %195 = load double, double* %arrayidx210, align 8
  %conv211 = fptrunc double %195 to float
  %call212 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %196 = load float, float* %call212, align 4
  %mul213 = fmul float %conv211, %196
  store float %mul213, float* %ref.tmp209, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx198, float* nonnull align 4 dereferenceable(4) %ref.tmp199, float* nonnull align 4 dereferenceable(4) %ref.tmp204, float* nonnull align 4 dereferenceable(4) %ref.tmp209)
  %197 = load i8*, i8** %vertexbase, align 4
  %198 = load i32*, i32** %tri_indices192, align 4
  %arrayidx214 = getelementptr inbounds i32, i32* %198, i32 1
  %199 = load i32, i32* %arrayidx214, align 4
  %200 = load i32, i32* %stride, align 4
  %mul215 = mul i32 %199, %200
  %add.ptr216 = getelementptr inbounds i8, i8* %197, i32 %mul215
  %201 = bitcast i8* %add.ptr216 to double*
  store double* %201, double** %graphicsbase187, align 4
  %arrayidx217 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 1
  %202 = load double*, double** %graphicsbase187, align 4
  %arrayidx219 = getelementptr inbounds double, double* %202, i32 0
  %203 = load double, double* %arrayidx219, align 8
  %conv220 = fptrunc double %203 to float
  %call221 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %204 = load float, float* %call221, align 4
  %mul222 = fmul float %conv220, %204
  store float %mul222, float* %ref.tmp218, align 4
  %205 = load double*, double** %graphicsbase187, align 4
  %arrayidx224 = getelementptr inbounds double, double* %205, i32 1
  %206 = load double, double* %arrayidx224, align 8
  %conv225 = fptrunc double %206 to float
  %call226 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %207 = load float, float* %call226, align 4
  %mul227 = fmul float %conv225, %207
  store float %mul227, float* %ref.tmp223, align 4
  %208 = load double*, double** %graphicsbase187, align 4
  %arrayidx229 = getelementptr inbounds double, double* %208, i32 2
  %209 = load double, double* %arrayidx229, align 8
  %conv230 = fptrunc double %209 to float
  %call231 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %210 = load float, float* %call231, align 4
  %mul232 = fmul float %conv230, %210
  store float %mul232, float* %ref.tmp228, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx217, float* nonnull align 4 dereferenceable(4) %ref.tmp218, float* nonnull align 4 dereferenceable(4) %ref.tmp223, float* nonnull align 4 dereferenceable(4) %ref.tmp228)
  %211 = load i8*, i8** %vertexbase, align 4
  %212 = load i32*, i32** %tri_indices192, align 4
  %arrayidx233 = getelementptr inbounds i32, i32* %212, i32 2
  %213 = load i32, i32* %arrayidx233, align 4
  %214 = load i32, i32* %stride, align 4
  %mul234 = mul i32 %213, %214
  %add.ptr235 = getelementptr inbounds i8, i8* %211, i32 %mul234
  %215 = bitcast i8* %add.ptr235 to double*
  store double* %215, double** %graphicsbase187, align 4
  %arrayidx236 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 2
  %216 = load double*, double** %graphicsbase187, align 4
  %arrayidx238 = getelementptr inbounds double, double* %216, i32 0
  %217 = load double, double* %arrayidx238, align 8
  %conv239 = fptrunc double %217 to float
  %call240 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %218 = load float, float* %call240, align 4
  %mul241 = fmul float %conv239, %218
  store float %mul241, float* %ref.tmp237, align 4
  %219 = load double*, double** %graphicsbase187, align 4
  %arrayidx243 = getelementptr inbounds double, double* %219, i32 1
  %220 = load double, double* %arrayidx243, align 8
  %conv244 = fptrunc double %220 to float
  %call245 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %221 = load float, float* %call245, align 4
  %mul246 = fmul float %conv244, %221
  store float %mul246, float* %ref.tmp242, align 4
  %222 = load double*, double** %graphicsbase187, align 4
  %arrayidx248 = getelementptr inbounds double, double* %222, i32 2
  %223 = load double, double* %arrayidx248, align 8
  %conv249 = fptrunc double %223 to float
  %call250 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %224 = load float, float* %call250, align 4
  %mul251 = fmul float %conv249, %224
  store float %mul251, float* %ref.tmp247, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx236, float* nonnull align 4 dereferenceable(4) %ref.tmp237, float* nonnull align 4 dereferenceable(4) %ref.tmp242, float* nonnull align 4 dereferenceable(4) %ref.tmp247)
  %225 = load %class.btInternalTriangleIndexCallback*, %class.btInternalTriangleIndexCallback** %callback.addr, align 4
  %arraydecay252 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %226 = load i32, i32* %part, align 4
  %227 = load i32, i32* %gfxindex, align 4
  %228 = bitcast %class.btInternalTriangleIndexCallback* %225 to void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)***
  %vtable253 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)**, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*** %228, align 4
  %vfn254 = getelementptr inbounds void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vtable253, i64 2
  %229 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vfn254, align 4
  call void %229(%class.btInternalTriangleIndexCallback* %225, %class.btVector3* %arraydecay252, i32 %226, i32 %227)
  br label %for.inc255

for.inc255:                                       ; preds = %for.body191
  %230 = load i32, i32* %gfxindex, align 4
  %inc256 = add nsw i32 %230, 1
  store i32 %inc256, i32* %gfxindex, align 4
  br label %for.cond189

for.end257:                                       ; preds = %for.cond189
  br label %sw.epilog405

sw.bb258:                                         ; preds = %sw.bb186
  store i32 0, i32* %gfxindex, align 4
  br label %for.cond259

for.cond259:                                      ; preds = %for.inc328, %sw.bb258
  %231 = load i32, i32* %gfxindex, align 4
  %232 = load i32, i32* %numtriangles, align 4
  %cmp260 = icmp slt i32 %231, %232
  br i1 %cmp260, label %for.body261, label %for.end330

for.body261:                                      ; preds = %for.cond259
  %233 = load i8*, i8** %indexbase, align 4
  %234 = load i32, i32* %gfxindex, align 4
  %235 = load i32, i32* %indexstride, align 4
  %mul263 = mul nsw i32 %234, %235
  %add.ptr264 = getelementptr inbounds i8, i8* %233, i32 %mul263
  %236 = bitcast i8* %add.ptr264 to i16*
  store i16* %236, i16** %tri_indices262, align 4
  %237 = load i8*, i8** %vertexbase, align 4
  %238 = load i16*, i16** %tri_indices262, align 4
  %arrayidx265 = getelementptr inbounds i16, i16* %238, i32 0
  %239 = load i16, i16* %arrayidx265, align 2
  %conv266 = zext i16 %239 to i32
  %240 = load i32, i32* %stride, align 4
  %mul267 = mul nsw i32 %conv266, %240
  %add.ptr268 = getelementptr inbounds i8, i8* %237, i32 %mul267
  %241 = bitcast i8* %add.ptr268 to double*
  store double* %241, double** %graphicsbase187, align 4
  %arrayidx269 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %242 = load double*, double** %graphicsbase187, align 4
  %arrayidx271 = getelementptr inbounds double, double* %242, i32 0
  %243 = load double, double* %arrayidx271, align 8
  %conv272 = fptrunc double %243 to float
  %call273 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %244 = load float, float* %call273, align 4
  %mul274 = fmul float %conv272, %244
  store float %mul274, float* %ref.tmp270, align 4
  %245 = load double*, double** %graphicsbase187, align 4
  %arrayidx276 = getelementptr inbounds double, double* %245, i32 1
  %246 = load double, double* %arrayidx276, align 8
  %conv277 = fptrunc double %246 to float
  %call278 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %247 = load float, float* %call278, align 4
  %mul279 = fmul float %conv277, %247
  store float %mul279, float* %ref.tmp275, align 4
  %248 = load double*, double** %graphicsbase187, align 4
  %arrayidx281 = getelementptr inbounds double, double* %248, i32 2
  %249 = load double, double* %arrayidx281, align 8
  %conv282 = fptrunc double %249 to float
  %call283 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %250 = load float, float* %call283, align 4
  %mul284 = fmul float %conv282, %250
  store float %mul284, float* %ref.tmp280, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx269, float* nonnull align 4 dereferenceable(4) %ref.tmp270, float* nonnull align 4 dereferenceable(4) %ref.tmp275, float* nonnull align 4 dereferenceable(4) %ref.tmp280)
  %251 = load i8*, i8** %vertexbase, align 4
  %252 = load i16*, i16** %tri_indices262, align 4
  %arrayidx285 = getelementptr inbounds i16, i16* %252, i32 1
  %253 = load i16, i16* %arrayidx285, align 2
  %conv286 = zext i16 %253 to i32
  %254 = load i32, i32* %stride, align 4
  %mul287 = mul nsw i32 %conv286, %254
  %add.ptr288 = getelementptr inbounds i8, i8* %251, i32 %mul287
  %255 = bitcast i8* %add.ptr288 to double*
  store double* %255, double** %graphicsbase187, align 4
  %arrayidx289 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 1
  %256 = load double*, double** %graphicsbase187, align 4
  %arrayidx291 = getelementptr inbounds double, double* %256, i32 0
  %257 = load double, double* %arrayidx291, align 8
  %conv292 = fptrunc double %257 to float
  %call293 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %258 = load float, float* %call293, align 4
  %mul294 = fmul float %conv292, %258
  store float %mul294, float* %ref.tmp290, align 4
  %259 = load double*, double** %graphicsbase187, align 4
  %arrayidx296 = getelementptr inbounds double, double* %259, i32 1
  %260 = load double, double* %arrayidx296, align 8
  %conv297 = fptrunc double %260 to float
  %call298 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %261 = load float, float* %call298, align 4
  %mul299 = fmul float %conv297, %261
  store float %mul299, float* %ref.tmp295, align 4
  %262 = load double*, double** %graphicsbase187, align 4
  %arrayidx301 = getelementptr inbounds double, double* %262, i32 2
  %263 = load double, double* %arrayidx301, align 8
  %conv302 = fptrunc double %263 to float
  %call303 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %264 = load float, float* %call303, align 4
  %mul304 = fmul float %conv302, %264
  store float %mul304, float* %ref.tmp300, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx289, float* nonnull align 4 dereferenceable(4) %ref.tmp290, float* nonnull align 4 dereferenceable(4) %ref.tmp295, float* nonnull align 4 dereferenceable(4) %ref.tmp300)
  %265 = load i8*, i8** %vertexbase, align 4
  %266 = load i16*, i16** %tri_indices262, align 4
  %arrayidx305 = getelementptr inbounds i16, i16* %266, i32 2
  %267 = load i16, i16* %arrayidx305, align 2
  %conv306 = zext i16 %267 to i32
  %268 = load i32, i32* %stride, align 4
  %mul307 = mul nsw i32 %conv306, %268
  %add.ptr308 = getelementptr inbounds i8, i8* %265, i32 %mul307
  %269 = bitcast i8* %add.ptr308 to double*
  store double* %269, double** %graphicsbase187, align 4
  %arrayidx309 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 2
  %270 = load double*, double** %graphicsbase187, align 4
  %arrayidx311 = getelementptr inbounds double, double* %270, i32 0
  %271 = load double, double* %arrayidx311, align 8
  %conv312 = fptrunc double %271 to float
  %call313 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %272 = load float, float* %call313, align 4
  %mul314 = fmul float %conv312, %272
  store float %mul314, float* %ref.tmp310, align 4
  %273 = load double*, double** %graphicsbase187, align 4
  %arrayidx316 = getelementptr inbounds double, double* %273, i32 1
  %274 = load double, double* %arrayidx316, align 8
  %conv317 = fptrunc double %274 to float
  %call318 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %275 = load float, float* %call318, align 4
  %mul319 = fmul float %conv317, %275
  store float %mul319, float* %ref.tmp315, align 4
  %276 = load double*, double** %graphicsbase187, align 4
  %arrayidx321 = getelementptr inbounds double, double* %276, i32 2
  %277 = load double, double* %arrayidx321, align 8
  %conv322 = fptrunc double %277 to float
  %call323 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %278 = load float, float* %call323, align 4
  %mul324 = fmul float %conv322, %278
  store float %mul324, float* %ref.tmp320, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx309, float* nonnull align 4 dereferenceable(4) %ref.tmp310, float* nonnull align 4 dereferenceable(4) %ref.tmp315, float* nonnull align 4 dereferenceable(4) %ref.tmp320)
  %279 = load %class.btInternalTriangleIndexCallback*, %class.btInternalTriangleIndexCallback** %callback.addr, align 4
  %arraydecay325 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %280 = load i32, i32* %part, align 4
  %281 = load i32, i32* %gfxindex, align 4
  %282 = bitcast %class.btInternalTriangleIndexCallback* %279 to void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)***
  %vtable326 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)**, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*** %282, align 4
  %vfn327 = getelementptr inbounds void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vtable326, i64 2
  %283 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vfn327, align 4
  call void %283(%class.btInternalTriangleIndexCallback* %279, %class.btVector3* %arraydecay325, i32 %280, i32 %281)
  br label %for.inc328

for.inc328:                                       ; preds = %for.body261
  %284 = load i32, i32* %gfxindex, align 4
  %inc329 = add nsw i32 %284, 1
  store i32 %inc329, i32* %gfxindex, align 4
  br label %for.cond259

for.end330:                                       ; preds = %for.cond259
  br label %sw.epilog405

sw.bb331:                                         ; preds = %sw.bb186
  store i32 0, i32* %gfxindex, align 4
  br label %for.cond332

for.cond332:                                      ; preds = %for.inc401, %sw.bb331
  %285 = load i32, i32* %gfxindex, align 4
  %286 = load i32, i32* %numtriangles, align 4
  %cmp333 = icmp slt i32 %285, %286
  br i1 %cmp333, label %for.body334, label %for.end403

for.body334:                                      ; preds = %for.cond332
  %287 = load i8*, i8** %indexbase, align 4
  %288 = load i32, i32* %gfxindex, align 4
  %289 = load i32, i32* %indexstride, align 4
  %mul336 = mul nsw i32 %288, %289
  %add.ptr337 = getelementptr inbounds i8, i8* %287, i32 %mul336
  store i8* %add.ptr337, i8** %tri_indices335, align 4
  %290 = load i8*, i8** %vertexbase, align 4
  %291 = load i8*, i8** %tri_indices335, align 4
  %arrayidx338 = getelementptr inbounds i8, i8* %291, i32 0
  %292 = load i8, i8* %arrayidx338, align 1
  %conv339 = zext i8 %292 to i32
  %293 = load i32, i32* %stride, align 4
  %mul340 = mul nsw i32 %conv339, %293
  %add.ptr341 = getelementptr inbounds i8, i8* %290, i32 %mul340
  %294 = bitcast i8* %add.ptr341 to double*
  store double* %294, double** %graphicsbase187, align 4
  %arrayidx342 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %295 = load double*, double** %graphicsbase187, align 4
  %arrayidx344 = getelementptr inbounds double, double* %295, i32 0
  %296 = load double, double* %arrayidx344, align 8
  %conv345 = fptrunc double %296 to float
  %call346 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %297 = load float, float* %call346, align 4
  %mul347 = fmul float %conv345, %297
  store float %mul347, float* %ref.tmp343, align 4
  %298 = load double*, double** %graphicsbase187, align 4
  %arrayidx349 = getelementptr inbounds double, double* %298, i32 1
  %299 = load double, double* %arrayidx349, align 8
  %conv350 = fptrunc double %299 to float
  %call351 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %300 = load float, float* %call351, align 4
  %mul352 = fmul float %conv350, %300
  store float %mul352, float* %ref.tmp348, align 4
  %301 = load double*, double** %graphicsbase187, align 4
  %arrayidx354 = getelementptr inbounds double, double* %301, i32 2
  %302 = load double, double* %arrayidx354, align 8
  %conv355 = fptrunc double %302 to float
  %call356 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %303 = load float, float* %call356, align 4
  %mul357 = fmul float %conv355, %303
  store float %mul357, float* %ref.tmp353, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx342, float* nonnull align 4 dereferenceable(4) %ref.tmp343, float* nonnull align 4 dereferenceable(4) %ref.tmp348, float* nonnull align 4 dereferenceable(4) %ref.tmp353)
  %304 = load i8*, i8** %vertexbase, align 4
  %305 = load i8*, i8** %tri_indices335, align 4
  %arrayidx358 = getelementptr inbounds i8, i8* %305, i32 1
  %306 = load i8, i8* %arrayidx358, align 1
  %conv359 = zext i8 %306 to i32
  %307 = load i32, i32* %stride, align 4
  %mul360 = mul nsw i32 %conv359, %307
  %add.ptr361 = getelementptr inbounds i8, i8* %304, i32 %mul360
  %308 = bitcast i8* %add.ptr361 to double*
  store double* %308, double** %graphicsbase187, align 4
  %arrayidx362 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 1
  %309 = load double*, double** %graphicsbase187, align 4
  %arrayidx364 = getelementptr inbounds double, double* %309, i32 0
  %310 = load double, double* %arrayidx364, align 8
  %conv365 = fptrunc double %310 to float
  %call366 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %311 = load float, float* %call366, align 4
  %mul367 = fmul float %conv365, %311
  store float %mul367, float* %ref.tmp363, align 4
  %312 = load double*, double** %graphicsbase187, align 4
  %arrayidx369 = getelementptr inbounds double, double* %312, i32 1
  %313 = load double, double* %arrayidx369, align 8
  %conv370 = fptrunc double %313 to float
  %call371 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %314 = load float, float* %call371, align 4
  %mul372 = fmul float %conv370, %314
  store float %mul372, float* %ref.tmp368, align 4
  %315 = load double*, double** %graphicsbase187, align 4
  %arrayidx374 = getelementptr inbounds double, double* %315, i32 2
  %316 = load double, double* %arrayidx374, align 8
  %conv375 = fptrunc double %316 to float
  %call376 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %317 = load float, float* %call376, align 4
  %mul377 = fmul float %conv375, %317
  store float %mul377, float* %ref.tmp373, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx362, float* nonnull align 4 dereferenceable(4) %ref.tmp363, float* nonnull align 4 dereferenceable(4) %ref.tmp368, float* nonnull align 4 dereferenceable(4) %ref.tmp373)
  %318 = load i8*, i8** %vertexbase, align 4
  %319 = load i8*, i8** %tri_indices335, align 4
  %arrayidx378 = getelementptr inbounds i8, i8* %319, i32 2
  %320 = load i8, i8* %arrayidx378, align 1
  %conv379 = zext i8 %320 to i32
  %321 = load i32, i32* %stride, align 4
  %mul380 = mul nsw i32 %conv379, %321
  %add.ptr381 = getelementptr inbounds i8, i8* %318, i32 %mul380
  %322 = bitcast i8* %add.ptr381 to double*
  store double* %322, double** %graphicsbase187, align 4
  %arrayidx382 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 2
  %323 = load double*, double** %graphicsbase187, align 4
  %arrayidx384 = getelementptr inbounds double, double* %323, i32 0
  %324 = load double, double* %arrayidx384, align 8
  %conv385 = fptrunc double %324 to float
  %call386 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %325 = load float, float* %call386, align 4
  %mul387 = fmul float %conv385, %325
  store float %mul387, float* %ref.tmp383, align 4
  %326 = load double*, double** %graphicsbase187, align 4
  %arrayidx389 = getelementptr inbounds double, double* %326, i32 1
  %327 = load double, double* %arrayidx389, align 8
  %conv390 = fptrunc double %327 to float
  %call391 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %328 = load float, float* %call391, align 4
  %mul392 = fmul float %conv390, %328
  store float %mul392, float* %ref.tmp388, align 4
  %329 = load double*, double** %graphicsbase187, align 4
  %arrayidx394 = getelementptr inbounds double, double* %329, i32 2
  %330 = load double, double* %arrayidx394, align 8
  %conv395 = fptrunc double %330 to float
  %call396 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %331 = load float, float* %call396, align 4
  %mul397 = fmul float %conv395, %331
  store float %mul397, float* %ref.tmp393, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx382, float* nonnull align 4 dereferenceable(4) %ref.tmp383, float* nonnull align 4 dereferenceable(4) %ref.tmp388, float* nonnull align 4 dereferenceable(4) %ref.tmp393)
  %332 = load %class.btInternalTriangleIndexCallback*, %class.btInternalTriangleIndexCallback** %callback.addr, align 4
  %arraydecay398 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %333 = load i32, i32* %part, align 4
  %334 = load i32, i32* %gfxindex, align 4
  %335 = bitcast %class.btInternalTriangleIndexCallback* %332 to void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)***
  %vtable399 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)**, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*** %335, align 4
  %vfn400 = getelementptr inbounds void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vtable399, i64 2
  %336 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vfn400, align 4
  call void %336(%class.btInternalTriangleIndexCallback* %332, %class.btVector3* %arraydecay398, i32 %333, i32 %334)
  br label %for.inc401

for.inc401:                                       ; preds = %for.body334
  %337 = load i32, i32* %gfxindex, align 4
  %inc402 = add nsw i32 %337, 1
  store i32 %inc402, i32* %gfxindex, align 4
  br label %for.cond332

for.end403:                                       ; preds = %for.cond332
  br label %sw.epilog405

sw.default404:                                    ; preds = %sw.bb186
  br label %sw.epilog405

sw.epilog405:                                     ; preds = %sw.default404, %for.end403, %for.end330, %for.end257
  br label %sw.epilog407

sw.default406:                                    ; preds = %for.body
  br label %sw.epilog407

sw.epilog407:                                     ; preds = %sw.default406, %sw.epilog405, %sw.epilog
  %338 = load i32, i32* %part, align 4
  %339 = bitcast %class.btStridingMeshInterface* %this1 to void (%class.btStridingMeshInterface*, i32)***
  %vtable408 = load void (%class.btStridingMeshInterface*, i32)**, void (%class.btStridingMeshInterface*, i32)*** %339, align 4
  %vfn409 = getelementptr inbounds void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vtable408, i64 6
  %340 = load void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vfn409, align 4
  call void %340(%class.btStridingMeshInterface* %this1, i32 %338)
  br label %for.inc410

for.inc410:                                       ; preds = %sw.epilog407
  %341 = load i32, i32* %part, align 4
  %inc411 = add nsw i32 %341, 1
  store i32 %inc411, i32* %part, align 4
  br label %for.cond

for.end412:                                       ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %m_scaling = getelementptr inbounds %class.btStridingMeshInterface, %class.btStridingMeshInterface* %this1, i32 0, i32 1
  ret %class.btVector3* %m_scaling
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_(%class.btStridingMeshInterface* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #3 {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %aabbCallback = alloca %struct.AabbCalculationCallback, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %call = call %struct.AabbCalculationCallback* @_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallbackC2Ev(%struct.AabbCalculationCallback* %aabbCallback)
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp2, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp4, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp5, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp6, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %1, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %2 = bitcast %struct.AabbCalculationCallback* %aabbCallback to %class.btInternalTriangleIndexCallback*
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %5 = bitcast %class.btStridingMeshInterface* %this1 to void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*** %5, align 4
  %vfn = getelementptr inbounds void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %6 = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %6(%class.btStridingMeshInterface* %this1, %class.btInternalTriangleIndexCallback* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %m_aabbMin = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %aabbCallback, i32 0, i32 1
  %7 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %8 = bitcast %class.btVector3* %7 to i8*
  %9 = bitcast %class.btVector3* %m_aabbMin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  %m_aabbMax = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %aabbCallback, i32 0, i32 2
  %10 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %11 = bitcast %class.btVector3* %10 to i8*
  %12 = bitcast %class.btVector3* %m_aabbMax to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false)
  %call7 = call %struct.AabbCalculationCallback* @_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallbackD2Ev(%struct.AabbCalculationCallback* %aabbCallback) #8
  ret void
}

; Function Attrs: noinline optnone
define internal %struct.AabbCalculationCallback* @_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallbackC2Ev(%struct.AabbCalculationCallback* returned %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %struct.AabbCalculationCallback*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  store %struct.AabbCalculationCallback* %this, %struct.AabbCalculationCallback** %this.addr, align 4
  %this1 = load %struct.AabbCalculationCallback*, %struct.AabbCalculationCallback** %this.addr, align 4
  %0 = bitcast %struct.AabbCalculationCallback* %this1 to %class.btInternalTriangleIndexCallback*
  %call = call %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackC2Ev(%class.btInternalTriangleIndexCallback* %0) #8
  %1 = bitcast %struct.AabbCalculationCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_aabbMin = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMin)
  %m_aabbMax = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMax)
  %m_aabbMin4 = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 1
  store float 0x43ABC16D60000000, float* %ref.tmp, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp5, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp6, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_aabbMin4, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %m_aabbMax7 = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 2
  store float 0xC3ABC16D60000000, float* %ref.tmp8, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp9, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp10, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_aabbMax7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  ret %struct.AabbCalculationCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define internal %struct.AabbCalculationCallback* @_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallbackD2Ev(%struct.AabbCalculationCallback* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.AabbCalculationCallback*, align 4
  store %struct.AabbCalculationCallback* %this, %struct.AabbCalculationCallback** %this.addr, align 4
  %this1 = load %struct.AabbCalculationCallback*, %struct.AabbCalculationCallback** %this.addr, align 4
  %0 = bitcast %struct.AabbCalculationCallback* %this1 to %class.btInternalTriangleIndexCallback*
  %call = call %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackD2Ev(%class.btInternalTriangleIndexCallback* %0) #8
  ret %struct.AabbCalculationCallback* %this1
}

; Function Attrs: noinline optnone
define hidden i8* @_ZNK23btStridingMeshInterface9serializeEPvP12btSerializer(%class.btStridingMeshInterface* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %trimeshData = alloca %struct.btStridingMeshInterfaceData*, align 4
  %chunk = alloca %class.btChunk*, align 4
  %memPtr = alloca %struct.btMeshPartData*, align 4
  %part = alloca i32, align 4
  %graphicssubparts = alloca i32, align 4
  %vertexbase = alloca i8*, align 4
  %indexbase = alloca i8*, align 4
  %indexstride = alloca i32, align 4
  %type = alloca i32, align 4
  %gfxindextype = alloca i32, align 4
  %stride = alloca i32, align 4
  %numverts = alloca i32, align 4
  %numtriangles = alloca i32, align 4
  %gfxindex = alloca i32, align 4
  %numindices = alloca i32, align 4
  %chunk18 = alloca %class.btChunk*, align 4
  %tmpIndices = alloca %struct.btIntIndexData*, align 4
  %tri_indices = alloca i32*, align 4
  %chunk48 = alloca %class.btChunk*, align 4
  %tmpIndices52 = alloca %struct.btShortIntIndexTripletData*, align 4
  %tri_indices61 = alloca i16*, align 4
  %chunk85 = alloca %class.btChunk*, align 4
  %tmpIndices89 = alloca %struct.btCharIndexTripletData*, align 4
  %tri_indices98 = alloca i8*, align 4
  %graphicsbase = alloca float*, align 4
  %chunk123 = alloca %class.btChunk*, align 4
  %tmpVertices = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  %chunk158 = alloca %class.btChunk*, align 4
  %tmpVertices162 = alloca %struct.btVector3DoubleData*, align 4
  %i168 = alloca i32, align 4
  %graphicsbase172 = alloca double*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btStridingMeshInterfaceData*
  store %struct.btStridingMeshInterfaceData* %1, %struct.btStridingMeshInterfaceData** %trimeshData, align 4
  %2 = bitcast %class.btStridingMeshInterface* %this1 to i32 (%class.btStridingMeshInterface*)***
  %vtable = load i32 (%class.btStridingMeshInterface*)**, i32 (%class.btStridingMeshInterface*)*** %2, align 4
  %vfn = getelementptr inbounds i32 (%class.btStridingMeshInterface*)*, i32 (%class.btStridingMeshInterface*)** %vtable, i64 7
  %3 = load i32 (%class.btStridingMeshInterface*)*, i32 (%class.btStridingMeshInterface*)** %vfn, align 4
  %call = call i32 %3(%class.btStridingMeshInterface* %this1)
  %4 = load %struct.btStridingMeshInterfaceData*, %struct.btStridingMeshInterfaceData** %trimeshData, align 4
  %m_numMeshParts = getelementptr inbounds %struct.btStridingMeshInterfaceData, %struct.btStridingMeshInterfaceData* %4, i32 0, i32 2
  store i32 %call, i32* %m_numMeshParts, align 4
  %5 = load %struct.btStridingMeshInterfaceData*, %struct.btStridingMeshInterfaceData** %trimeshData, align 4
  %m_meshPartsPtr = getelementptr inbounds %struct.btStridingMeshInterfaceData, %struct.btStridingMeshInterfaceData* %5, i32 0, i32 0
  store %struct.btMeshPartData* null, %struct.btMeshPartData** %m_meshPartsPtr, align 4
  %6 = load %struct.btStridingMeshInterfaceData*, %struct.btStridingMeshInterfaceData** %trimeshData, align 4
  %m_numMeshParts2 = getelementptr inbounds %struct.btStridingMeshInterfaceData, %struct.btStridingMeshInterfaceData* %6, i32 0, i32 2
  %7 = load i32, i32* %m_numMeshParts2, align 4
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %if.then, label %if.end204

if.then:                                          ; preds = %entry
  %8 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %9 = load %struct.btStridingMeshInterfaceData*, %struct.btStridingMeshInterfaceData** %trimeshData, align 4
  %m_numMeshParts3 = getelementptr inbounds %struct.btStridingMeshInterfaceData, %struct.btStridingMeshInterfaceData* %9, i32 0, i32 2
  %10 = load i32, i32* %m_numMeshParts3, align 4
  %11 = bitcast %class.btSerializer* %8 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable4 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %11, align 4
  %vfn5 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable4, i64 4
  %12 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn5, align 4
  %call6 = call %class.btChunk* %12(%class.btSerializer* %8, i32 32, i32 %10)
  store %class.btChunk* %call6, %class.btChunk** %chunk, align 4
  %13 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %13, i32 0, i32 2
  %14 = load i8*, i8** %m_oldPtr, align 4
  %15 = bitcast i8* %14 to %struct.btMeshPartData*
  store %struct.btMeshPartData* %15, %struct.btMeshPartData** %memPtr, align 4
  %16 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %17 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4
  %18 = bitcast %struct.btMeshPartData* %17 to i8*
  %19 = bitcast %class.btSerializer* %16 to i8* (%class.btSerializer*, i8*)***
  %vtable7 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %19, align 4
  %vfn8 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable7, i64 7
  %20 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn8, align 4
  %call9 = call i8* %20(%class.btSerializer* %16, i8* %18)
  %21 = bitcast i8* %call9 to %struct.btMeshPartData*
  %22 = load %struct.btStridingMeshInterfaceData*, %struct.btStridingMeshInterfaceData** %trimeshData, align 4
  %m_meshPartsPtr10 = getelementptr inbounds %struct.btStridingMeshInterfaceData, %struct.btStridingMeshInterfaceData* %22, i32 0, i32 0
  store %struct.btMeshPartData* %21, %struct.btMeshPartData** %m_meshPartsPtr10, align 4
  %23 = bitcast %class.btStridingMeshInterface* %this1 to i32 (%class.btStridingMeshInterface*)***
  %vtable11 = load i32 (%class.btStridingMeshInterface*)**, i32 (%class.btStridingMeshInterface*)*** %23, align 4
  %vfn12 = getelementptr inbounds i32 (%class.btStridingMeshInterface*)*, i32 (%class.btStridingMeshInterface*)** %vtable11, i64 7
  %24 = load i32 (%class.btStridingMeshInterface*)*, i32 (%class.btStridingMeshInterface*)** %vfn12, align 4
  %call13 = call i32 %24(%class.btStridingMeshInterface* %this1)
  store i32 %call13, i32* %graphicssubparts, align 4
  store i32 0, i32* %part, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc198, %if.then
  %25 = load i32, i32* %part, align 4
  %26 = load i32, i32* %graphicssubparts, align 4
  %cmp = icmp slt i32 %25, %26
  br i1 %cmp, label %for.body, label %for.end200

for.body:                                         ; preds = %for.cond
  %27 = load i32, i32* %part, align 4
  %28 = bitcast %class.btStridingMeshInterface* %this1 to void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)***
  %vtable14 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)**, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*** %28, align 4
  %vfn15 = getelementptr inbounds void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vtable14, i64 4
  %29 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vfn15, align 4
  call void %29(%class.btStridingMeshInterface* %this1, i8** %vertexbase, i32* nonnull align 4 dereferenceable(4) %numverts, i32* nonnull align 4 dereferenceable(4) %type, i32* nonnull align 4 dereferenceable(4) %stride, i8** %indexbase, i32* nonnull align 4 dereferenceable(4) %indexstride, i32* nonnull align 4 dereferenceable(4) %numtriangles, i32* nonnull align 4 dereferenceable(4) %gfxindextype, i32 %27)
  %30 = load i32, i32* %numtriangles, align 4
  %31 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4
  %m_numTriangles = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %31, i32 0, i32 6
  store i32 %30, i32* %m_numTriangles, align 4
  %32 = load i32, i32* %numverts, align 4
  %33 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4
  %m_numVertices = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %33, i32 0, i32 7
  store i32 %32, i32* %m_numVertices, align 4
  %34 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4
  %m_indices16 = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %34, i32 0, i32 5
  store %struct.btShortIntIndexData* null, %struct.btShortIntIndexData** %m_indices16, align 4
  %35 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4
  %m_indices32 = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %35, i32 0, i32 2
  store %struct.btIntIndexData* null, %struct.btIntIndexData** %m_indices32, align 4
  %36 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4
  %m_3indices16 = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %36, i32 0, i32 3
  store %struct.btShortIntIndexTripletData* null, %struct.btShortIntIndexTripletData** %m_3indices16, align 4
  %37 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4
  %m_3indices8 = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %37, i32 0, i32 4
  store %struct.btCharIndexTripletData* null, %struct.btCharIndexTripletData** %m_3indices8, align 4
  %38 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4
  %m_vertices3f = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %38, i32 0, i32 0
  store %struct.btVector3FloatData* null, %struct.btVector3FloatData** %m_vertices3f, align 4
  %39 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4
  %m_vertices3d = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %39, i32 0, i32 1
  store %struct.btVector3DoubleData* null, %struct.btVector3DoubleData** %m_vertices3d, align 4
  %40 = load i32, i32* %gfxindextype, align 4
  switch i32 %40, label %sw.default [
    i32 2, label %sw.bb
    i32 3, label %sw.bb45
    i32 5, label %sw.bb82
  ]

sw.bb:                                            ; preds = %for.body
  %41 = load i32, i32* %numtriangles, align 4
  %mul = mul nsw i32 %41, 3
  store i32 %mul, i32* %numindices, align 4
  %42 = load i32, i32* %numindices, align 4
  %tobool16 = icmp ne i32 %42, 0
  br i1 %tobool16, label %if.then17, label %if.end

if.then17:                                        ; preds = %sw.bb
  %43 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %44 = load i32, i32* %numindices, align 4
  %45 = bitcast %class.btSerializer* %43 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable19 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %45, align 4
  %vfn20 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable19, i64 4
  %46 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn20, align 4
  %call21 = call %class.btChunk* %46(%class.btSerializer* %43, i32 4, i32 %44)
  store %class.btChunk* %call21, %class.btChunk** %chunk18, align 4
  %47 = load %class.btChunk*, %class.btChunk** %chunk18, align 4
  %m_oldPtr22 = getelementptr inbounds %class.btChunk, %class.btChunk* %47, i32 0, i32 2
  %48 = load i8*, i8** %m_oldPtr22, align 4
  %49 = bitcast i8* %48 to %struct.btIntIndexData*
  store %struct.btIntIndexData* %49, %struct.btIntIndexData** %tmpIndices, align 4
  %50 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %51 = load %struct.btIntIndexData*, %struct.btIntIndexData** %tmpIndices, align 4
  %52 = bitcast %struct.btIntIndexData* %51 to i8*
  %53 = bitcast %class.btSerializer* %50 to i8* (%class.btSerializer*, i8*)***
  %vtable23 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %53, align 4
  %vfn24 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable23, i64 7
  %54 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn24, align 4
  %call25 = call i8* %54(%class.btSerializer* %50, i8* %52)
  %55 = bitcast i8* %call25 to %struct.btIntIndexData*
  %56 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4
  %m_indices3226 = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %56, i32 0, i32 2
  store %struct.btIntIndexData* %55, %struct.btIntIndexData** %m_indices3226, align 4
  store i32 0, i32* %gfxindex, align 4
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc, %if.then17
  %57 = load i32, i32* %gfxindex, align 4
  %58 = load i32, i32* %numtriangles, align 4
  %cmp28 = icmp slt i32 %57, %58
  br i1 %cmp28, label %for.body29, label %for.end

for.body29:                                       ; preds = %for.cond27
  %59 = load i8*, i8** %indexbase, align 4
  %60 = load i32, i32* %gfxindex, align 4
  %61 = load i32, i32* %indexstride, align 4
  %mul30 = mul nsw i32 %60, %61
  %add.ptr = getelementptr inbounds i8, i8* %59, i32 %mul30
  %62 = bitcast i8* %add.ptr to i32*
  store i32* %62, i32** %tri_indices, align 4
  %63 = load i32*, i32** %tri_indices, align 4
  %arrayidx = getelementptr inbounds i32, i32* %63, i32 0
  %64 = load i32, i32* %arrayidx, align 4
  %65 = load %struct.btIntIndexData*, %struct.btIntIndexData** %tmpIndices, align 4
  %66 = load i32, i32* %gfxindex, align 4
  %mul31 = mul nsw i32 %66, 3
  %arrayidx32 = getelementptr inbounds %struct.btIntIndexData, %struct.btIntIndexData* %65, i32 %mul31
  %m_value = getelementptr inbounds %struct.btIntIndexData, %struct.btIntIndexData* %arrayidx32, i32 0, i32 0
  store i32 %64, i32* %m_value, align 4
  %67 = load i32*, i32** %tri_indices, align 4
  %arrayidx33 = getelementptr inbounds i32, i32* %67, i32 1
  %68 = load i32, i32* %arrayidx33, align 4
  %69 = load %struct.btIntIndexData*, %struct.btIntIndexData** %tmpIndices, align 4
  %70 = load i32, i32* %gfxindex, align 4
  %mul34 = mul nsw i32 %70, 3
  %add = add nsw i32 %mul34, 1
  %arrayidx35 = getelementptr inbounds %struct.btIntIndexData, %struct.btIntIndexData* %69, i32 %add
  %m_value36 = getelementptr inbounds %struct.btIntIndexData, %struct.btIntIndexData* %arrayidx35, i32 0, i32 0
  store i32 %68, i32* %m_value36, align 4
  %71 = load i32*, i32** %tri_indices, align 4
  %arrayidx37 = getelementptr inbounds i32, i32* %71, i32 2
  %72 = load i32, i32* %arrayidx37, align 4
  %73 = load %struct.btIntIndexData*, %struct.btIntIndexData** %tmpIndices, align 4
  %74 = load i32, i32* %gfxindex, align 4
  %mul38 = mul nsw i32 %74, 3
  %add39 = add nsw i32 %mul38, 2
  %arrayidx40 = getelementptr inbounds %struct.btIntIndexData, %struct.btIntIndexData* %73, i32 %add39
  %m_value41 = getelementptr inbounds %struct.btIntIndexData, %struct.btIntIndexData* %arrayidx40, i32 0, i32 0
  store i32 %72, i32* %m_value41, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body29
  %75 = load i32, i32* %gfxindex, align 4
  %inc = add nsw i32 %75, 1
  store i32 %inc, i32* %gfxindex, align 4
  br label %for.cond27

for.end:                                          ; preds = %for.cond27
  %76 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %77 = load %class.btChunk*, %class.btChunk** %chunk18, align 4
  %78 = load %class.btChunk*, %class.btChunk** %chunk18, align 4
  %m_oldPtr42 = getelementptr inbounds %class.btChunk, %class.btChunk* %78, i32 0, i32 2
  %79 = load i8*, i8** %m_oldPtr42, align 4
  %80 = bitcast %class.btSerializer* %76 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable43 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %80, align 4
  %vfn44 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable43, i64 5
  %81 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn44, align 4
  call void %81(%class.btSerializer* %76, %class.btChunk* %77, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str, i32 0, i32 0), i32 1497453121, i8* %79)
  br label %if.end

if.end:                                           ; preds = %for.end, %sw.bb
  br label %sw.epilog

sw.bb45:                                          ; preds = %for.body
  %82 = load i32, i32* %numtriangles, align 4
  %tobool46 = icmp ne i32 %82, 0
  br i1 %tobool46, label %if.then47, label %if.end81

if.then47:                                        ; preds = %sw.bb45
  %83 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %84 = load i32, i32* %numtriangles, align 4
  %85 = bitcast %class.btSerializer* %83 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable49 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %85, align 4
  %vfn50 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable49, i64 4
  %86 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn50, align 4
  %call51 = call %class.btChunk* %86(%class.btSerializer* %83, i32 8, i32 %84)
  store %class.btChunk* %call51, %class.btChunk** %chunk48, align 4
  %87 = load %class.btChunk*, %class.btChunk** %chunk48, align 4
  %m_oldPtr53 = getelementptr inbounds %class.btChunk, %class.btChunk* %87, i32 0, i32 2
  %88 = load i8*, i8** %m_oldPtr53, align 4
  %89 = bitcast i8* %88 to %struct.btShortIntIndexTripletData*
  store %struct.btShortIntIndexTripletData* %89, %struct.btShortIntIndexTripletData** %tmpIndices52, align 4
  %90 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %91 = load %struct.btShortIntIndexTripletData*, %struct.btShortIntIndexTripletData** %tmpIndices52, align 4
  %92 = bitcast %struct.btShortIntIndexTripletData* %91 to i8*
  %93 = bitcast %class.btSerializer* %90 to i8* (%class.btSerializer*, i8*)***
  %vtable54 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %93, align 4
  %vfn55 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable54, i64 7
  %94 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn55, align 4
  %call56 = call i8* %94(%class.btSerializer* %90, i8* %92)
  %95 = bitcast i8* %call56 to %struct.btShortIntIndexTripletData*
  %96 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4
  %m_3indices1657 = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %96, i32 0, i32 3
  store %struct.btShortIntIndexTripletData* %95, %struct.btShortIntIndexTripletData** %m_3indices1657, align 4
  store i32 0, i32* %gfxindex, align 4
  br label %for.cond58

for.cond58:                                       ; preds = %for.inc75, %if.then47
  %97 = load i32, i32* %gfxindex, align 4
  %98 = load i32, i32* %numtriangles, align 4
  %cmp59 = icmp slt i32 %97, %98
  br i1 %cmp59, label %for.body60, label %for.end77

for.body60:                                       ; preds = %for.cond58
  %99 = load i8*, i8** %indexbase, align 4
  %100 = load i32, i32* %gfxindex, align 4
  %101 = load i32, i32* %indexstride, align 4
  %mul62 = mul nsw i32 %100, %101
  %add.ptr63 = getelementptr inbounds i8, i8* %99, i32 %mul62
  %102 = bitcast i8* %add.ptr63 to i16*
  store i16* %102, i16** %tri_indices61, align 4
  %103 = load i16*, i16** %tri_indices61, align 4
  %arrayidx64 = getelementptr inbounds i16, i16* %103, i32 0
  %104 = load i16, i16* %arrayidx64, align 2
  %105 = load %struct.btShortIntIndexTripletData*, %struct.btShortIntIndexTripletData** %tmpIndices52, align 4
  %106 = load i32, i32* %gfxindex, align 4
  %arrayidx65 = getelementptr inbounds %struct.btShortIntIndexTripletData, %struct.btShortIntIndexTripletData* %105, i32 %106
  %m_values = getelementptr inbounds %struct.btShortIntIndexTripletData, %struct.btShortIntIndexTripletData* %arrayidx65, i32 0, i32 0
  %arrayidx66 = getelementptr inbounds [3 x i16], [3 x i16]* %m_values, i32 0, i32 0
  store i16 %104, i16* %arrayidx66, align 2
  %107 = load i16*, i16** %tri_indices61, align 4
  %arrayidx67 = getelementptr inbounds i16, i16* %107, i32 1
  %108 = load i16, i16* %arrayidx67, align 2
  %109 = load %struct.btShortIntIndexTripletData*, %struct.btShortIntIndexTripletData** %tmpIndices52, align 4
  %110 = load i32, i32* %gfxindex, align 4
  %arrayidx68 = getelementptr inbounds %struct.btShortIntIndexTripletData, %struct.btShortIntIndexTripletData* %109, i32 %110
  %m_values69 = getelementptr inbounds %struct.btShortIntIndexTripletData, %struct.btShortIntIndexTripletData* %arrayidx68, i32 0, i32 0
  %arrayidx70 = getelementptr inbounds [3 x i16], [3 x i16]* %m_values69, i32 0, i32 1
  store i16 %108, i16* %arrayidx70, align 2
  %111 = load i16*, i16** %tri_indices61, align 4
  %arrayidx71 = getelementptr inbounds i16, i16* %111, i32 2
  %112 = load i16, i16* %arrayidx71, align 2
  %113 = load %struct.btShortIntIndexTripletData*, %struct.btShortIntIndexTripletData** %tmpIndices52, align 4
  %114 = load i32, i32* %gfxindex, align 4
  %arrayidx72 = getelementptr inbounds %struct.btShortIntIndexTripletData, %struct.btShortIntIndexTripletData* %113, i32 %114
  %m_values73 = getelementptr inbounds %struct.btShortIntIndexTripletData, %struct.btShortIntIndexTripletData* %arrayidx72, i32 0, i32 0
  %arrayidx74 = getelementptr inbounds [3 x i16], [3 x i16]* %m_values73, i32 0, i32 2
  store i16 %112, i16* %arrayidx74, align 2
  br label %for.inc75

for.inc75:                                        ; preds = %for.body60
  %115 = load i32, i32* %gfxindex, align 4
  %inc76 = add nsw i32 %115, 1
  store i32 %inc76, i32* %gfxindex, align 4
  br label %for.cond58

for.end77:                                        ; preds = %for.cond58
  %116 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %117 = load %class.btChunk*, %class.btChunk** %chunk48, align 4
  %118 = load %class.btChunk*, %class.btChunk** %chunk48, align 4
  %m_oldPtr78 = getelementptr inbounds %class.btChunk, %class.btChunk* %118, i32 0, i32 2
  %119 = load i8*, i8** %m_oldPtr78, align 4
  %120 = bitcast %class.btSerializer* %116 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable79 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %120, align 4
  %vfn80 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable79, i64 5
  %121 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn80, align 4
  call void %121(%class.btSerializer* %116, %class.btChunk* %117, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.1, i32 0, i32 0), i32 1497453121, i8* %119)
  br label %if.end81

if.end81:                                         ; preds = %for.end77, %sw.bb45
  br label %sw.epilog

sw.bb82:                                          ; preds = %for.body
  %122 = load i32, i32* %numtriangles, align 4
  %tobool83 = icmp ne i32 %122, 0
  br i1 %tobool83, label %if.then84, label %if.end119

if.then84:                                        ; preds = %sw.bb82
  %123 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %124 = load i32, i32* %numtriangles, align 4
  %125 = bitcast %class.btSerializer* %123 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable86 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %125, align 4
  %vfn87 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable86, i64 4
  %126 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn87, align 4
  %call88 = call %class.btChunk* %126(%class.btSerializer* %123, i32 4, i32 %124)
  store %class.btChunk* %call88, %class.btChunk** %chunk85, align 4
  %127 = load %class.btChunk*, %class.btChunk** %chunk85, align 4
  %m_oldPtr90 = getelementptr inbounds %class.btChunk, %class.btChunk* %127, i32 0, i32 2
  %128 = load i8*, i8** %m_oldPtr90, align 4
  %129 = bitcast i8* %128 to %struct.btCharIndexTripletData*
  store %struct.btCharIndexTripletData* %129, %struct.btCharIndexTripletData** %tmpIndices89, align 4
  %130 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %131 = load %struct.btCharIndexTripletData*, %struct.btCharIndexTripletData** %tmpIndices89, align 4
  %132 = bitcast %struct.btCharIndexTripletData* %131 to i8*
  %133 = bitcast %class.btSerializer* %130 to i8* (%class.btSerializer*, i8*)***
  %vtable91 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %133, align 4
  %vfn92 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable91, i64 7
  %134 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn92, align 4
  %call93 = call i8* %134(%class.btSerializer* %130, i8* %132)
  %135 = bitcast i8* %call93 to %struct.btCharIndexTripletData*
  %136 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4
  %m_3indices894 = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %136, i32 0, i32 4
  store %struct.btCharIndexTripletData* %135, %struct.btCharIndexTripletData** %m_3indices894, align 4
  store i32 0, i32* %gfxindex, align 4
  br label %for.cond95

for.cond95:                                       ; preds = %for.inc113, %if.then84
  %137 = load i32, i32* %gfxindex, align 4
  %138 = load i32, i32* %numtriangles, align 4
  %cmp96 = icmp slt i32 %137, %138
  br i1 %cmp96, label %for.body97, label %for.end115

for.body97:                                       ; preds = %for.cond95
  %139 = load i8*, i8** %indexbase, align 4
  %140 = load i32, i32* %gfxindex, align 4
  %141 = load i32, i32* %indexstride, align 4
  %mul99 = mul nsw i32 %140, %141
  %add.ptr100 = getelementptr inbounds i8, i8* %139, i32 %mul99
  store i8* %add.ptr100, i8** %tri_indices98, align 4
  %142 = load i8*, i8** %tri_indices98, align 4
  %arrayidx101 = getelementptr inbounds i8, i8* %142, i32 0
  %143 = load i8, i8* %arrayidx101, align 1
  %144 = load %struct.btCharIndexTripletData*, %struct.btCharIndexTripletData** %tmpIndices89, align 4
  %145 = load i32, i32* %gfxindex, align 4
  %arrayidx102 = getelementptr inbounds %struct.btCharIndexTripletData, %struct.btCharIndexTripletData* %144, i32 %145
  %m_values103 = getelementptr inbounds %struct.btCharIndexTripletData, %struct.btCharIndexTripletData* %arrayidx102, i32 0, i32 0
  %arrayidx104 = getelementptr inbounds [3 x i8], [3 x i8]* %m_values103, i32 0, i32 0
  store i8 %143, i8* %arrayidx104, align 1
  %146 = load i8*, i8** %tri_indices98, align 4
  %arrayidx105 = getelementptr inbounds i8, i8* %146, i32 1
  %147 = load i8, i8* %arrayidx105, align 1
  %148 = load %struct.btCharIndexTripletData*, %struct.btCharIndexTripletData** %tmpIndices89, align 4
  %149 = load i32, i32* %gfxindex, align 4
  %arrayidx106 = getelementptr inbounds %struct.btCharIndexTripletData, %struct.btCharIndexTripletData* %148, i32 %149
  %m_values107 = getelementptr inbounds %struct.btCharIndexTripletData, %struct.btCharIndexTripletData* %arrayidx106, i32 0, i32 0
  %arrayidx108 = getelementptr inbounds [3 x i8], [3 x i8]* %m_values107, i32 0, i32 1
  store i8 %147, i8* %arrayidx108, align 1
  %150 = load i8*, i8** %tri_indices98, align 4
  %arrayidx109 = getelementptr inbounds i8, i8* %150, i32 2
  %151 = load i8, i8* %arrayidx109, align 1
  %152 = load %struct.btCharIndexTripletData*, %struct.btCharIndexTripletData** %tmpIndices89, align 4
  %153 = load i32, i32* %gfxindex, align 4
  %arrayidx110 = getelementptr inbounds %struct.btCharIndexTripletData, %struct.btCharIndexTripletData* %152, i32 %153
  %m_values111 = getelementptr inbounds %struct.btCharIndexTripletData, %struct.btCharIndexTripletData* %arrayidx110, i32 0, i32 0
  %arrayidx112 = getelementptr inbounds [3 x i8], [3 x i8]* %m_values111, i32 0, i32 2
  store i8 %151, i8* %arrayidx112, align 1
  br label %for.inc113

for.inc113:                                       ; preds = %for.body97
  %154 = load i32, i32* %gfxindex, align 4
  %inc114 = add nsw i32 %154, 1
  store i32 %inc114, i32* %gfxindex, align 4
  br label %for.cond95

for.end115:                                       ; preds = %for.cond95
  %155 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %156 = load %class.btChunk*, %class.btChunk** %chunk85, align 4
  %157 = load %class.btChunk*, %class.btChunk** %chunk85, align 4
  %m_oldPtr116 = getelementptr inbounds %class.btChunk, %class.btChunk* %157, i32 0, i32 2
  %158 = load i8*, i8** %m_oldPtr116, align 4
  %159 = bitcast %class.btSerializer* %155 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable117 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %159, align 4
  %vfn118 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable117, i64 5
  %160 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn118, align 4
  call void %160(%class.btSerializer* %155, %class.btChunk* %156, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.2, i32 0, i32 0), i32 1497453121, i8* %158)
  br label %if.end119

if.end119:                                        ; preds = %for.end115, %sw.bb82
  br label %sw.epilog

sw.default:                                       ; preds = %for.body
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %if.end119, %if.end81, %if.end
  %161 = load i32, i32* %type, align 4
  switch i32 %161, label %sw.default194 [
    i32 0, label %sw.bb120
    i32 1, label %sw.bb155
  ]

sw.bb120:                                         ; preds = %sw.epilog
  %162 = load i32, i32* %numverts, align 4
  %tobool121 = icmp ne i32 %162, 0
  br i1 %tobool121, label %if.then122, label %if.end154

if.then122:                                       ; preds = %sw.bb120
  %163 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %164 = load i32, i32* %numverts, align 4
  %165 = bitcast %class.btSerializer* %163 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable124 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %165, align 4
  %vfn125 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable124, i64 4
  %166 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn125, align 4
  %call126 = call %class.btChunk* %166(%class.btSerializer* %163, i32 16, i32 %164)
  store %class.btChunk* %call126, %class.btChunk** %chunk123, align 4
  %167 = load %class.btChunk*, %class.btChunk** %chunk123, align 4
  %m_oldPtr127 = getelementptr inbounds %class.btChunk, %class.btChunk* %167, i32 0, i32 2
  %168 = load i8*, i8** %m_oldPtr127, align 4
  %169 = bitcast i8* %168 to %struct.btVector3FloatData*
  store %struct.btVector3FloatData* %169, %struct.btVector3FloatData** %tmpVertices, align 4
  %170 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %171 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %tmpVertices, align 4
  %172 = bitcast %struct.btVector3FloatData* %171 to i8*
  %173 = bitcast %class.btSerializer* %170 to i8* (%class.btSerializer*, i8*)***
  %vtable128 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %173, align 4
  %vfn129 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable128, i64 7
  %174 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn129, align 4
  %call130 = call i8* %174(%class.btSerializer* %170, i8* %172)
  %175 = bitcast i8* %call130 to %struct.btVector3FloatData*
  %176 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4
  %m_vertices3f131 = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %176, i32 0, i32 0
  store %struct.btVector3FloatData* %175, %struct.btVector3FloatData** %m_vertices3f131, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond132

for.cond132:                                      ; preds = %for.inc148, %if.then122
  %177 = load i32, i32* %i, align 4
  %178 = load i32, i32* %numverts, align 4
  %cmp133 = icmp slt i32 %177, %178
  br i1 %cmp133, label %for.body134, label %for.end150

for.body134:                                      ; preds = %for.cond132
  %179 = load i8*, i8** %vertexbase, align 4
  %180 = load i32, i32* %i, align 4
  %181 = load i32, i32* %stride, align 4
  %mul135 = mul nsw i32 %180, %181
  %add.ptr136 = getelementptr inbounds i8, i8* %179, i32 %mul135
  %182 = bitcast i8* %add.ptr136 to float*
  store float* %182, float** %graphicsbase, align 4
  %183 = load float*, float** %graphicsbase, align 4
  %arrayidx137 = getelementptr inbounds float, float* %183, i32 0
  %184 = load float, float* %arrayidx137, align 4
  %185 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %tmpVertices, align 4
  %186 = load i32, i32* %i, align 4
  %arrayidx138 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %185, i32 %186
  %m_floats = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %arrayidx138, i32 0, i32 0
  %arrayidx139 = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %184, float* %arrayidx139, align 4
  %187 = load float*, float** %graphicsbase, align 4
  %arrayidx140 = getelementptr inbounds float, float* %187, i32 1
  %188 = load float, float* %arrayidx140, align 4
  %189 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %tmpVertices, align 4
  %190 = load i32, i32* %i, align 4
  %arrayidx141 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %189, i32 %190
  %m_floats142 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %arrayidx141, i32 0, i32 0
  %arrayidx143 = getelementptr inbounds [4 x float], [4 x float]* %m_floats142, i32 0, i32 1
  store float %188, float* %arrayidx143, align 4
  %191 = load float*, float** %graphicsbase, align 4
  %arrayidx144 = getelementptr inbounds float, float* %191, i32 2
  %192 = load float, float* %arrayidx144, align 4
  %193 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %tmpVertices, align 4
  %194 = load i32, i32* %i, align 4
  %arrayidx145 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %193, i32 %194
  %m_floats146 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %arrayidx145, i32 0, i32 0
  %arrayidx147 = getelementptr inbounds [4 x float], [4 x float]* %m_floats146, i32 0, i32 2
  store float %192, float* %arrayidx147, align 4
  br label %for.inc148

for.inc148:                                       ; preds = %for.body134
  %195 = load i32, i32* %i, align 4
  %inc149 = add nsw i32 %195, 1
  store i32 %inc149, i32* %i, align 4
  br label %for.cond132

for.end150:                                       ; preds = %for.cond132
  %196 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %197 = load %class.btChunk*, %class.btChunk** %chunk123, align 4
  %198 = load %class.btChunk*, %class.btChunk** %chunk123, align 4
  %m_oldPtr151 = getelementptr inbounds %class.btChunk, %class.btChunk* %198, i32 0, i32 2
  %199 = load i8*, i8** %m_oldPtr151, align 4
  %200 = bitcast %class.btSerializer* %196 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable152 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %200, align 4
  %vfn153 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable152, i64 5
  %201 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn153, align 4
  call void %201(%class.btSerializer* %196, %class.btChunk* %197, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.3, i32 0, i32 0), i32 1497453121, i8* %199)
  br label %if.end154

if.end154:                                        ; preds = %for.end150, %sw.bb120
  br label %sw.epilog195

sw.bb155:                                         ; preds = %sw.epilog
  %202 = load i32, i32* %numverts, align 4
  %tobool156 = icmp ne i32 %202, 0
  br i1 %tobool156, label %if.then157, label %if.end193

if.then157:                                       ; preds = %sw.bb155
  %203 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %204 = load i32, i32* %numverts, align 4
  %205 = bitcast %class.btSerializer* %203 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable159 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %205, align 4
  %vfn160 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable159, i64 4
  %206 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn160, align 4
  %call161 = call %class.btChunk* %206(%class.btSerializer* %203, i32 32, i32 %204)
  store %class.btChunk* %call161, %class.btChunk** %chunk158, align 4
  %207 = load %class.btChunk*, %class.btChunk** %chunk158, align 4
  %m_oldPtr163 = getelementptr inbounds %class.btChunk, %class.btChunk* %207, i32 0, i32 2
  %208 = load i8*, i8** %m_oldPtr163, align 4
  %209 = bitcast i8* %208 to %struct.btVector3DoubleData*
  store %struct.btVector3DoubleData* %209, %struct.btVector3DoubleData** %tmpVertices162, align 4
  %210 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %211 = load %struct.btVector3DoubleData*, %struct.btVector3DoubleData** %tmpVertices162, align 4
  %212 = bitcast %struct.btVector3DoubleData* %211 to i8*
  %213 = bitcast %class.btSerializer* %210 to i8* (%class.btSerializer*, i8*)***
  %vtable164 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %213, align 4
  %vfn165 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable164, i64 7
  %214 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn165, align 4
  %call166 = call i8* %214(%class.btSerializer* %210, i8* %212)
  %215 = bitcast i8* %call166 to %struct.btVector3DoubleData*
  %216 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4
  %m_vertices3d167 = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %216, i32 0, i32 1
  store %struct.btVector3DoubleData* %215, %struct.btVector3DoubleData** %m_vertices3d167, align 4
  store i32 0, i32* %i168, align 4
  br label %for.cond169

for.cond169:                                      ; preds = %for.inc187, %if.then157
  %217 = load i32, i32* %i168, align 4
  %218 = load i32, i32* %numverts, align 4
  %cmp170 = icmp slt i32 %217, %218
  br i1 %cmp170, label %for.body171, label %for.end189

for.body171:                                      ; preds = %for.cond169
  %219 = load i8*, i8** %vertexbase, align 4
  %220 = load i32, i32* %i168, align 4
  %221 = load i32, i32* %stride, align 4
  %mul173 = mul nsw i32 %220, %221
  %add.ptr174 = getelementptr inbounds i8, i8* %219, i32 %mul173
  %222 = bitcast i8* %add.ptr174 to double*
  store double* %222, double** %graphicsbase172, align 4
  %223 = load double*, double** %graphicsbase172, align 4
  %arrayidx175 = getelementptr inbounds double, double* %223, i32 0
  %224 = load double, double* %arrayidx175, align 8
  %225 = load %struct.btVector3DoubleData*, %struct.btVector3DoubleData** %tmpVertices162, align 4
  %226 = load i32, i32* %i168, align 4
  %arrayidx176 = getelementptr inbounds %struct.btVector3DoubleData, %struct.btVector3DoubleData* %225, i32 %226
  %m_floats177 = getelementptr inbounds %struct.btVector3DoubleData, %struct.btVector3DoubleData* %arrayidx176, i32 0, i32 0
  %arrayidx178 = getelementptr inbounds [4 x double], [4 x double]* %m_floats177, i32 0, i32 0
  store double %224, double* %arrayidx178, align 8
  %227 = load double*, double** %graphicsbase172, align 4
  %arrayidx179 = getelementptr inbounds double, double* %227, i32 1
  %228 = load double, double* %arrayidx179, align 8
  %229 = load %struct.btVector3DoubleData*, %struct.btVector3DoubleData** %tmpVertices162, align 4
  %230 = load i32, i32* %i168, align 4
  %arrayidx180 = getelementptr inbounds %struct.btVector3DoubleData, %struct.btVector3DoubleData* %229, i32 %230
  %m_floats181 = getelementptr inbounds %struct.btVector3DoubleData, %struct.btVector3DoubleData* %arrayidx180, i32 0, i32 0
  %arrayidx182 = getelementptr inbounds [4 x double], [4 x double]* %m_floats181, i32 0, i32 1
  store double %228, double* %arrayidx182, align 8
  %231 = load double*, double** %graphicsbase172, align 4
  %arrayidx183 = getelementptr inbounds double, double* %231, i32 2
  %232 = load double, double* %arrayidx183, align 8
  %233 = load %struct.btVector3DoubleData*, %struct.btVector3DoubleData** %tmpVertices162, align 4
  %234 = load i32, i32* %i168, align 4
  %arrayidx184 = getelementptr inbounds %struct.btVector3DoubleData, %struct.btVector3DoubleData* %233, i32 %234
  %m_floats185 = getelementptr inbounds %struct.btVector3DoubleData, %struct.btVector3DoubleData* %arrayidx184, i32 0, i32 0
  %arrayidx186 = getelementptr inbounds [4 x double], [4 x double]* %m_floats185, i32 0, i32 2
  store double %232, double* %arrayidx186, align 8
  br label %for.inc187

for.inc187:                                       ; preds = %for.body171
  %235 = load i32, i32* %i168, align 4
  %inc188 = add nsw i32 %235, 1
  store i32 %inc188, i32* %i168, align 4
  br label %for.cond169

for.end189:                                       ; preds = %for.cond169
  %236 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %237 = load %class.btChunk*, %class.btChunk** %chunk158, align 4
  %238 = load %class.btChunk*, %class.btChunk** %chunk158, align 4
  %m_oldPtr190 = getelementptr inbounds %class.btChunk, %class.btChunk* %238, i32 0, i32 2
  %239 = load i8*, i8** %m_oldPtr190, align 4
  %240 = bitcast %class.btSerializer* %236 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable191 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %240, align 4
  %vfn192 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable191, i64 5
  %241 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn192, align 4
  call void %241(%class.btSerializer* %236, %class.btChunk* %237, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.4, i32 0, i32 0), i32 1497453121, i8* %239)
  br label %if.end193

if.end193:                                        ; preds = %for.end189, %sw.bb155
  br label %sw.epilog195

sw.default194:                                    ; preds = %sw.epilog
  br label %sw.epilog195

sw.epilog195:                                     ; preds = %sw.default194, %if.end193, %if.end154
  %242 = load i32, i32* %part, align 4
  %243 = bitcast %class.btStridingMeshInterface* %this1 to void (%class.btStridingMeshInterface*, i32)***
  %vtable196 = load void (%class.btStridingMeshInterface*, i32)**, void (%class.btStridingMeshInterface*, i32)*** %243, align 4
  %vfn197 = getelementptr inbounds void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vtable196, i64 6
  %244 = load void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vfn197, align 4
  call void %244(%class.btStridingMeshInterface* %this1, i32 %242)
  br label %for.inc198

for.inc198:                                       ; preds = %sw.epilog195
  %245 = load i32, i32* %part, align 4
  %inc199 = add nsw i32 %245, 1
  store i32 %inc199, i32* %part, align 4
  %246 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4
  %incdec.ptr = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %246, i32 1
  store %struct.btMeshPartData* %incdec.ptr, %struct.btMeshPartData** %memPtr, align 4
  br label %for.cond

for.end200:                                       ; preds = %for.cond
  %247 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %248 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %249 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %m_oldPtr201 = getelementptr inbounds %class.btChunk, %class.btChunk* %249, i32 0, i32 2
  %250 = load i8*, i8** %m_oldPtr201, align 4
  %251 = bitcast %class.btSerializer* %247 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable202 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %251, align 4
  %vfn203 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable202, i64 5
  %252 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn203, align 4
  call void %252(%class.btSerializer* %247, %class.btChunk* %248, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.5, i32 0, i32 0), i32 1497453121, i8* %250)
  br label %if.end204

if.end204:                                        ; preds = %for.end200, %entry
  %m_scaling = getelementptr inbounds %class.btStridingMeshInterface, %class.btStridingMeshInterface* %this1, i32 0, i32 1
  %253 = load %struct.btStridingMeshInterfaceData*, %struct.btStridingMeshInterfaceData** %trimeshData, align 4
  %m_scaling205 = getelementptr inbounds %struct.btStridingMeshInterfaceData, %struct.btStridingMeshInterfaceData* %253, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_scaling, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_scaling205)
  ret i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.6, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %2, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK23btStridingMeshInterface14hasPremadeAabbEv(%class.btStridingMeshInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  ret i1 false
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK23btStridingMeshInterface14setPremadeAabbERK9btVector3S2_(%class.btStridingMeshInterface* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK23btStridingMeshInterface14getPremadeAabbEP9btVector3S1_(%class.btStridingMeshInterface* %this, %class.btVector3* %aabbMin, %class.btVector3* %aabbMax) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK23btStridingMeshInterface28calculateSerializeBufferSizeEv(%class.btStridingMeshInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  ret i32 28
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackC2Ev(%class.btInternalTriangleIndexCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btInternalTriangleIndexCallback*, align 4
  store %class.btInternalTriangleIndexCallback* %this, %class.btInternalTriangleIndexCallback** %this.addr, align 4
  %this1 = load %class.btInternalTriangleIndexCallback*, %class.btInternalTriangleIndexCallback** %this.addr, align 4
  %0 = bitcast %class.btInternalTriangleIndexCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV31btInternalTriangleIndexCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btInternalTriangleIndexCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallbackD0Ev(%struct.AabbCalculationCallback* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.AabbCalculationCallback*, align 4
  store %struct.AabbCalculationCallback* %this, %struct.AabbCalculationCallback** %this.addr, align 4
  %this1 = load %struct.AabbCalculationCallback*, %struct.AabbCalculationCallback** %this.addr, align 4
  %call = call %struct.AabbCalculationCallback* @_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallbackD2Ev(%struct.AabbCalculationCallback* %this1) #8
  %0 = bitcast %struct.AabbCalculationCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define internal void @_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallback28internalProcessTriangleIndexEPS0_ii(%struct.AabbCalculationCallback* %this, %class.btVector3* %triangle, i32 %partId, i32 %triangleIndex) unnamed_addr #3 {
entry:
  %this.addr = alloca %struct.AabbCalculationCallback*, align 4
  %triangle.addr = alloca %class.btVector3*, align 4
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  store %struct.AabbCalculationCallback* %this, %struct.AabbCalculationCallback** %this.addr, align 4
  store %class.btVector3* %triangle, %class.btVector3** %triangle.addr, align 4
  store i32 %partId, i32* %partId.addr, align 4
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4
  %this1 = load %struct.AabbCalculationCallback*, %struct.AabbCalculationCallback** %this.addr, align 4
  %m_aabbMin = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 1
  %0 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %m_aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  %m_aabbMax = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 2
  %1 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %m_aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx2)
  %m_aabbMin3 = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 1
  %2 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx4 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 1
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %m_aabbMin3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4)
  %m_aabbMax5 = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 2
  %3 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx6 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 1
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %m_aabbMax5, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6)
  %m_aabbMin7 = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 1
  %4 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx8 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 2
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %m_aabbMin7, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx8)
  %m_aabbMax9 = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 2
  %5 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx10 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 2
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %m_aabbMax9, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx10)
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btVector36setMinERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btVector36setMaxERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %b.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %a.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = load float*, float** %a.addr, align 4
  store float %5, float* %6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %b.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = load float*, float** %a.addr, align 4
  store float %5, float* %6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: nounwind
declare %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackD2Ev(%class.btInternalTriangleIndexCallback* returned) unnamed_addr #6

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btStridingMeshInterface.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { cold noreturn nounwind }
attributes #3 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { noreturn nounwind }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
