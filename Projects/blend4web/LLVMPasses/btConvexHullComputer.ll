; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/LinearMath/btConvexHullComputer.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/LinearMath/btConvexHullComputer.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%"class.btConvexHullInternal::Int128" = type { i64, i64 }
%"class.btConvexHullInternal::Rational64" = type <{ i64, i64, i32, [4 x i8] }>
%"class.btConvexHullInternal::Rational128" = type <{ %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128", i32, i8, [3 x i8] }>
%"class.btConvexHullInternal::Edge" = type { %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Face"*, i32 }
%"class.btConvexHullInternal::Face" = type { %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32" }
%"class.btConvexHullInternal::Point32" = type { i32, i32, i32, i32 }
%class.btConvexHullInternal = type { %class.btVector3, %class.btVector3, %"class.btConvexHullInternal::Pool", %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.2", %class.btAlignedObjectArray, i32, i32, i32, i32, i32, i32, %"class.btConvexHullInternal::Vertex"* }
%class.btVector3 = type { [4 x float] }
%"class.btConvexHullInternal::Pool" = type { %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::Vertex"*, i32 }
%"class.btConvexHullInternal::PoolArray" = type { %"class.btConvexHullInternal::Vertex"*, i32, %"class.btConvexHullInternal::PoolArray"* }
%"class.btConvexHullInternal::Pool.0" = type { %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::Edge"*, i32 }
%"class.btConvexHullInternal::PoolArray.1" = type { %"class.btConvexHullInternal::Edge"*, i32, %"class.btConvexHullInternal::PoolArray.1"* }
%"class.btConvexHullInternal::Pool.2" = type { %"class.btConvexHullInternal::PoolArray.3"*, %"class.btConvexHullInternal::PoolArray.3"*, %"class.btConvexHullInternal::Face"*, i32 }
%"class.btConvexHullInternal::PoolArray.3" = type { %"class.btConvexHullInternal::Face"*, i32, %"class.btConvexHullInternal::PoolArray.3"* }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %"class.btConvexHullInternal::Vertex"**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%"class.btConvexHullInternal::Vertex" = type <{ %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"*, [4 x i8], %"class.btConvexHullInternal::PointR128", %"class.btConvexHullInternal::Point32", i32, [4 x i8] }>
%"class.btConvexHullInternal::PointR128" = type { %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128" }
%"class.btConvexHullInternal::IntermediateHull" = type { %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"* }
%"class.btConvexHullInternal::Point64" = type { i64, i64, i64 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %"class.btConvexHullInternal::Point32"*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.pointCmp = type { i8 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %"class.btConvexHullInternal::Face"**, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btConvexHullComputer = type { %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.20 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %"class.btConvexHullComputer::Edge"*, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%"class.btConvexHullComputer::Edge" = type { i32, i32, i32 }
%class.btAlignedObjectArray.20 = type <{ %class.btAlignedAllocator.21, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.21 = type { i8 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZNK20btConvexHullInternal6Int128ngEv = comdat any

$_ZN20btConvexHullInternal6Int128C2Ev = comdat any

$_ZN20btConvexHullInternal4DMulIyjE3mulEyyRyS2_ = comdat any

$_ZNK20btConvexHullInternal6Int1284ucmpERKS0_ = comdat any

$_ZN20btConvexHullInternal4DMulINS_6Int128EyE3mulES1_S1_RS1_S3_ = comdat any

$_ZN20btConvexHullInternal4PoolINS_4EdgeEE9newObjectEv = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEixEi = comdat any

$_ZNK20btConvexHullInternal7Point32neERKS0_ = comdat any

$_ZN20btConvexHullInternal4Edge4linkEPS0_ = comdat any

$_ZNK20btConvexHullInternal7Point32eqERKS0_ = comdat any

$_ZN20btConvexHullInternal16IntermediateHullC2Ev = comdat any

$_ZNK20btConvexHullInternal7Point325crossERKS0_ = comdat any

$_ZNK20btConvexHullInternal6VertexmiERKS0_ = comdat any

$_ZNK20btConvexHullInternal7Point643dotERKS0_ = comdat any

$_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E = comdat any

$_ZN20btConvexHullInternal10Rational64C2Exx = comdat any

$_ZNK20btConvexHullInternal10Rational645isNaNEv = comdat any

$_ZNK20btConvexHullInternal7Point32miERKS0_ = comdat any

$_ZNK20btConvexHullInternal7Point325crossERKNS_7Point64E = comdat any

$_ZNK20btConvexHullInternal7Point323dotERKS0_ = comdat any

$_ZN20btConvexHullInternal7Point32C2Ev = comdat any

$_ZN20btConvexHullInternal7Point32C2Eiii = comdat any

$_ZNK20btConvexHullInternal10Rational6418isNegativeInfinityEv = comdat any

$_ZN20btConvexHullInternal14removeEdgePairEPNS_4EdgeE = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN9btVector36setMinERKS_ = comdat any

$_ZN9btVector36setMaxERKS_ = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector37maxAxisEv = comdat any

$_ZNK9btVector37minAxisEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE6resizeEiRKS1_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EEixEi = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE9quickSortI8pointCmpEEvRKT_ = comdat any

$_ZN20btConvexHullInternal4PoolINS_6VertexEE5resetEv = comdat any

$_ZN20btConvexHullInternal4PoolINS_6VertexEE12setArraySizeEi = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE6resizeEiRKS2_ = comdat any

$_ZN20btConvexHullInternal4PoolINS_6VertexEE9newObjectEv = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE5clearEv = comdat any

$_ZN20btConvexHullInternal4PoolINS_4EdgeEE5resetEv = comdat any

$_ZN20btConvexHullInternal4PoolINS_4EdgeEE12setArraySizeEi = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EED2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZNK9btVector310normalizedEv = comdat any

$_ZNK20btConvexHullInternal6Vertex6xvalueEv = comdat any

$_ZNK20btConvexHullInternal6Vertex6yvalueEv = comdat any

$_ZNK20btConvexHullInternal6Vertex6zvalueEv = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9push_backERKS2_ = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEEC2Ev = comdat any

$_ZN20btConvexHullInternal6Int128C2Eyy = comdat any

$_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE8pop_backEv = comdat any

$_ZN20btConvexHullInternal4PoolINS_4FaceEE9newObjectEv = comdat any

$_ZN20btConvexHullInternal4Face4initEPNS_6VertexES2_S2_ = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE9push_backERKS2_ = comdat any

$_ZNK20btConvexHullInternal7Point32plERKS0_ = comdat any

$_ZN20btConvexHullInternal6Int128C2Ex = comdat any

$_ZN20btConvexHullInternal6Int128pLERKS0_ = comdat any

$_ZNK20btConvexHullInternal6Int1287getSignEv = comdat any

$_ZNK20btConvexHullInternal6Int1288toScalarEv = comdat any

$_ZN9btVector3mLERKS_ = comdat any

$_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEEixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_Z5btMinIfERKT_S2_S2_ = comdat any

$_Z6btSwapIPN20btConvexHullInternal4FaceEEvRT_S4_ = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEC2ERKS3_ = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEED2Ev = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEED2Ev = comdat any

$_ZN20btConvexHullInternal7Point326isZeroEv = comdat any

$_ZN20btConvexHullInternal4Face9getNormalEv = comdat any

$_ZNK20btConvexHullInternal6Vertex3dotERKNS_7Point64E = comdat any

$_ZNK20btConvexHullInternal6Int128miERKS0_ = comdat any

$_ZNK20btConvexHullInternal6Int128plERKS0_ = comdat any

$_ZN20btConvexHullInternal9PointR128C2ENS_6Int128ES1_S1_S1_ = comdat any

$_ZNK20btConvexHullInternal9PointR1286xvalueEv = comdat any

$_ZNK20btConvexHullInternal9PointR1286yvalueEv = comdat any

$_ZNK20btConvexHullInternal9PointR1286zvalueEv = comdat any

$_ZN20btConvexHullInternal6Vertex18receiveNearbyFacesEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZN20btConvexHullInternalC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_ = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayIiE6resizeEiRKi = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_ = comdat any

$_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE9push_backERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEixEi = comdat any

$_ZN20btAlignedObjectArrayIiE9push_backERKi = comdat any

$_ZN20btConvexHullInternalD2Ev = comdat any

$_ZN20btConvexHullInternal7Point64C2Exxx = comdat any

$_ZN20btConvexHullInternal4PoolINS_4EdgeEE10freeObjectEPS1_ = comdat any

$_ZN20btConvexHullInternal4EdgeD2Ev = comdat any

$_Z8btSetMinIfEvRT_RKS0_ = comdat any

$_ZNK9btVector31wEv = comdat any

$_Z8btSetMaxIfEvRT_RKS0_ = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN20btConvexHullInternal11Rational128C2Ex = comdat any

$_ZN20btConvexHullInternal11Rational128C2ERKNS_6Int128ES3_ = comdat any

$_ZN20btConvexHullInternal6Int128C2Ey = comdat any

$_ZN20btConvexHullInternal4PoolINS_6VertexEEC2Ev = comdat any

$_ZN20btConvexHullInternal4PoolINS_4EdgeEEC2Ev = comdat any

$_ZN20btConvexHullInternal4PoolINS_4FaceEEC2Ev = comdat any

$_ZN20btConvexHullInternal4PoolINS_4FaceEED2Ev = comdat any

$_ZN20btConvexHullInternal4PoolINS_4EdgeEED2Ev = comdat any

$_ZN20btConvexHullInternal4PoolINS_6VertexEED2Ev = comdat any

$_ZN20btConvexHullInternal9PoolArrayINS_4FaceEED2Ev = comdat any

$_ZN20btConvexHullInternal9PoolArrayINS_4EdgeEED2Ev = comdat any

$_ZN20btConvexHullInternal9PoolArrayINS_6VertexEED2Ev = comdat any

$_ZN20btConvexHullInternal4DMulIyjE3mulEjj = comdat any

$_ZN20btConvexHullInternal4DMulIyjE3lowEy = comdat any

$_ZN20btConvexHullInternal4DMulIyjE4highEy = comdat any

$_ZN20btConvexHullInternal4DMulIyjE7shlHalfERy = comdat any

$_ZN20btConvexHullInternal4DMulINS_6Int128EyE3mulEyy = comdat any

$_ZN20btConvexHullInternal4DMulINS_6Int128EyE3lowES1_ = comdat any

$_ZN20btConvexHullInternal4DMulINS_6Int128EyE4highES1_ = comdat any

$_ZN20btConvexHullInternal4DMulINS_6Int128EyE7shlHalfERS1_ = comdat any

$_ZNK20btConvexHullInternal6Int128ltERKS0_ = comdat any

$_ZN20btConvexHullInternal6Int128ppEv = comdat any

$_ZN20btConvexHullInternal9PoolArrayINS_4EdgeEEC2Ei = comdat any

$_ZN20btConvexHullInternal9PoolArrayINS_4EdgeEE4initEv = comdat any

$_ZN18btAlignedAllocatorIN20btConvexHullInternal7Point32ELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE4initEv = comdat any

$_ZNK20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE4copyEiiPS1_ = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIN20btConvexHullInternal7Point32ELj16EE8allocateEiPPKS1_ = comdat any

$_ZN18btAlignedAllocatorIN20btConvexHullInternal7Point32ELj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE17quickSortInternalI8pointCmpEEvRKT_ii = comdat any

$_ZNK8pointCmpclERKN20btConvexHullInternal7Point32ES3_ = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4copyEiiPS2_ = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIPN20btConvexHullInternal6VertexELj16EE8allocateEiPPKS2_ = comdat any

$_ZN18btAlignedAllocatorIPN20btConvexHullInternal6VertexELj16EE10deallocateEPS2_ = comdat any

$_ZN20btConvexHullInternal9PoolArrayINS_6VertexEEC2Ei = comdat any

$_ZN20btConvexHullInternal9PoolArrayINS_6VertexEE4initEv = comdat any

$_ZN20btConvexHullInternal6VertexC2Ev = comdat any

$_ZN20btConvexHullInternal9PointR128C2Ev = comdat any

$_ZN18btAlignedAllocatorIPN20btConvexHullInternal6VertexELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4initEv = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9allocSizeEi = comdat any

$_ZN18btAlignedAllocatorIPN20btConvexHullInternal4FaceELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE4initEv = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIPN20btConvexHullInternal4FaceELj16EE10deallocateEPS2_ = comdat any

$_ZN20btConvexHullInternal9PoolArrayINS_4FaceEEC2Ei = comdat any

$_ZN20btConvexHullInternal9PoolArrayINS_4FaceEE4initEv = comdat any

$_ZN20btConvexHullInternal4FaceC2Ev = comdat any

$_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE4copyEiiPS2_ = comdat any

$_ZN18btAlignedAllocatorIPN20btConvexHullInternal4FaceELj16EE8allocateEiPPKS2_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4initEv = comdat any

$_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE8allocateEiPPKS1_ = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIiE9allocSizeEi = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btConvexHullComputer.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZNK20btConvexHullInternal6Int128mlEx(%"class.btConvexHullInternal::Int128"* noalias sret align 8 %agg.result, %"class.btConvexHullInternal::Int128"* %this, i64 %b) #2 {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  %b.addr = alloca i64, align 8
  %negative = alloca i8, align 1
  %a = alloca %"class.btConvexHullInternal::Int128", align 8
  %result = alloca %"class.btConvexHullInternal::Int128", align 8
  store %"class.btConvexHullInternal::Int128"* %this, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  store i64 %b, i64* %b.addr, align 8
  %this1 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  %high = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 1
  %0 = load i64, i64* %high, align 8
  %cmp = icmp slt i64 %0, 0
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %negative, align 1
  %1 = load i8, i8* %negative, align 1
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  call void @_ZNK20btConvexHullInternal6Int128ngEv(%"class.btConvexHullInternal::Int128"* sret align 8 %a, %"class.btConvexHullInternal::Int128"* %this1)
  br label %cond.end

cond.false:                                       ; preds = %entry
  %2 = bitcast %"class.btConvexHullInternal::Int128"* %a to i8*
  %3 = bitcast %"class.btConvexHullInternal::Int128"* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %2, i8* align 8 %3, i32 16, i1 false)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %4 = load i64, i64* %b.addr, align 8
  %cmp2 = icmp slt i64 %4, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %5 = load i8, i8* %negative, align 1
  %tobool3 = trunc i8 %5 to i1
  %lnot = xor i1 %tobool3, true
  %frombool4 = zext i1 %lnot to i8
  store i8 %frombool4, i8* %negative, align 1
  %6 = load i64, i64* %b.addr, align 8
  %sub = sub nsw i64 0, %6
  store i64 %sub, i64* %b.addr, align 8
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end
  %low = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %a, i32 0, i32 0
  %7 = load i64, i64* %low, align 8
  %8 = load i64, i64* %b.addr, align 8
  call void @_ZN20btConvexHullInternal6Int1283mulEyy(%"class.btConvexHullInternal::Int128"* sret align 8 %result, i64 %7, i64 %8)
  %high5 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %a, i32 0, i32 1
  %9 = load i64, i64* %high5, align 8
  %10 = load i64, i64* %b.addr, align 8
  %mul = mul i64 %9, %10
  %high6 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %result, i32 0, i32 1
  %11 = load i64, i64* %high6, align 8
  %add = add i64 %11, %mul
  store i64 %add, i64* %high6, align 8
  %12 = load i8, i8* %negative, align 1
  %tobool7 = trunc i8 %12 to i1
  br i1 %tobool7, label %cond.true8, label %cond.false9

cond.true8:                                       ; preds = %if.end
  call void @_ZNK20btConvexHullInternal6Int128ngEv(%"class.btConvexHullInternal::Int128"* sret align 8 %agg.result, %"class.btConvexHullInternal::Int128"* %result)
  br label %cond.end10

cond.false9:                                      ; preds = %if.end
  %13 = bitcast %"class.btConvexHullInternal::Int128"* %agg.result to i8*
  %14 = bitcast %"class.btConvexHullInternal::Int128"* %result to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %13, i8* align 8 %14, i32 16, i1 false)
  br label %cond.end10

cond.end10:                                       ; preds = %cond.false9, %cond.true8
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btConvexHullInternal6Int128ngEv(%"class.btConvexHullInternal::Int128"* noalias sret align 8 %agg.result, %"class.btConvexHullInternal::Int128"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  store %"class.btConvexHullInternal::Int128"* %this, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  %low = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 0
  %0 = load i64, i64* %low, align 8
  %sub = sub nsw i64 0, %0
  %high = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 1
  %1 = load i64, i64* %high, align 8
  %neg = xor i64 %1, -1
  %low2 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 0
  %2 = load i64, i64* %low2, align 8
  %cmp = icmp eq i64 %2, 0
  %conv = zext i1 %cmp to i64
  %add = add i64 %neg, %conv
  %call = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Eyy(%"class.btConvexHullInternal::Int128"* %agg.result, i64 %sub, i64 %add)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline optnone
define hidden void @_ZN20btConvexHullInternal6Int1283mulEyy(%"class.btConvexHullInternal::Int128"* noalias sret align 8 %agg.result, i64 %a, i64 %b) #2 {
entry:
  %a.addr = alloca i64, align 8
  %b.addr = alloca i64, align 8
  store i64 %a, i64* %a.addr, align 8
  store i64 %b, i64* %b.addr, align 8
  %call = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ev(%"class.btConvexHullInternal::Int128"* %agg.result)
  %0 = load i64, i64* %a.addr, align 8
  %1 = load i64, i64* %b.addr, align 8
  %low = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %agg.result, i32 0, i32 0
  %high = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %agg.result, i32 0, i32 1
  call void @_ZN20btConvexHullInternal4DMulIyjE3mulEyyRyS2_(i64 %0, i64 %1, i64* nonnull align 8 dereferenceable(8) %low, i64* nonnull align 8 dereferenceable(8) %high)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN20btConvexHullInternal6Int1283mulExx(%"class.btConvexHullInternal::Int128"* noalias sret align 8 %agg.result, i64 %a, i64 %b) #2 {
entry:
  %a.addr = alloca i64, align 8
  %b.addr = alloca i64, align 8
  %result = alloca %"class.btConvexHullInternal::Int128", align 8
  %negative = alloca i8, align 1
  store i64 %a, i64* %a.addr, align 8
  store i64 %b, i64* %b.addr, align 8
  %call = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ev(%"class.btConvexHullInternal::Int128"* %result)
  %0 = load i64, i64* %a.addr, align 8
  %cmp = icmp slt i64 %0, 0
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %negative, align 1
  %1 = load i8, i8* %negative, align 1
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i64, i64* %a.addr, align 8
  %sub = sub nsw i64 0, %2
  store i64 %sub, i64* %a.addr, align 8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %3 = load i64, i64* %b.addr, align 8
  %cmp1 = icmp slt i64 %3, 0
  br i1 %cmp1, label %if.then2, label %if.end6

if.then2:                                         ; preds = %if.end
  %4 = load i8, i8* %negative, align 1
  %tobool3 = trunc i8 %4 to i1
  %lnot = xor i1 %tobool3, true
  %frombool4 = zext i1 %lnot to i8
  store i8 %frombool4, i8* %negative, align 1
  %5 = load i64, i64* %b.addr, align 8
  %sub5 = sub nsw i64 0, %5
  store i64 %sub5, i64* %b.addr, align 8
  br label %if.end6

if.end6:                                          ; preds = %if.then2, %if.end
  %6 = load i64, i64* %a.addr, align 8
  %7 = load i64, i64* %b.addr, align 8
  %low = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %result, i32 0, i32 0
  %high = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %result, i32 0, i32 1
  call void @_ZN20btConvexHullInternal4DMulIyjE3mulEyyRyS2_(i64 %6, i64 %7, i64* nonnull align 8 dereferenceable(8) %low, i64* nonnull align 8 dereferenceable(8) %high)
  %8 = load i8, i8* %negative, align 1
  %tobool7 = trunc i8 %8 to i1
  br i1 %tobool7, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end6
  call void @_ZNK20btConvexHullInternal6Int128ngEv(%"class.btConvexHullInternal::Int128"* sret align 8 %agg.result, %"class.btConvexHullInternal::Int128"* %result)
  br label %cond.end

cond.false:                                       ; preds = %if.end6
  %9 = bitcast %"class.btConvexHullInternal::Int128"* %agg.result to i8*
  %10 = bitcast %"class.btConvexHullInternal::Int128"* %result to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %9, i8* align 8 %10, i32 16, i1 false)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ev(%"class.btConvexHullInternal::Int128"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  store %"class.btConvexHullInternal::Int128"* %this, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  ret %"class.btConvexHullInternal::Int128"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btConvexHullInternal4DMulIyjE3mulEyyRyS2_(i64 %a, i64 %b, i64* nonnull align 8 dereferenceable(8) %resLow, i64* nonnull align 8 dereferenceable(8) %resHigh) #2 comdat {
entry:
  %a.addr = alloca i64, align 8
  %b.addr = alloca i64, align 8
  %resLow.addr = alloca i64*, align 4
  %resHigh.addr = alloca i64*, align 4
  %p00 = alloca i64, align 8
  %p01 = alloca i64, align 8
  %p10 = alloca i64, align 8
  %p11 = alloca i64, align 8
  %p0110 = alloca i64, align 8
  store i64 %a, i64* %a.addr, align 8
  store i64 %b, i64* %b.addr, align 8
  store i64* %resLow, i64** %resLow.addr, align 4
  store i64* %resHigh, i64** %resHigh.addr, align 4
  %0 = load i64, i64* %a.addr, align 8
  %call = call i32 @_ZN20btConvexHullInternal4DMulIyjE3lowEy(i64 %0)
  %1 = load i64, i64* %b.addr, align 8
  %call1 = call i32 @_ZN20btConvexHullInternal4DMulIyjE3lowEy(i64 %1)
  %call2 = call i64 @_ZN20btConvexHullInternal4DMulIyjE3mulEjj(i32 %call, i32 %call1)
  store i64 %call2, i64* %p00, align 8
  %2 = load i64, i64* %a.addr, align 8
  %call3 = call i32 @_ZN20btConvexHullInternal4DMulIyjE3lowEy(i64 %2)
  %3 = load i64, i64* %b.addr, align 8
  %call4 = call i32 @_ZN20btConvexHullInternal4DMulIyjE4highEy(i64 %3)
  %call5 = call i64 @_ZN20btConvexHullInternal4DMulIyjE3mulEjj(i32 %call3, i32 %call4)
  store i64 %call5, i64* %p01, align 8
  %4 = load i64, i64* %a.addr, align 8
  %call6 = call i32 @_ZN20btConvexHullInternal4DMulIyjE4highEy(i64 %4)
  %5 = load i64, i64* %b.addr, align 8
  %call7 = call i32 @_ZN20btConvexHullInternal4DMulIyjE3lowEy(i64 %5)
  %call8 = call i64 @_ZN20btConvexHullInternal4DMulIyjE3mulEjj(i32 %call6, i32 %call7)
  store i64 %call8, i64* %p10, align 8
  %6 = load i64, i64* %a.addr, align 8
  %call9 = call i32 @_ZN20btConvexHullInternal4DMulIyjE4highEy(i64 %6)
  %7 = load i64, i64* %b.addr, align 8
  %call10 = call i32 @_ZN20btConvexHullInternal4DMulIyjE4highEy(i64 %7)
  %call11 = call i64 @_ZN20btConvexHullInternal4DMulIyjE3mulEjj(i32 %call9, i32 %call10)
  store i64 %call11, i64* %p11, align 8
  %8 = load i64, i64* %p01, align 8
  %call12 = call i32 @_ZN20btConvexHullInternal4DMulIyjE3lowEy(i64 %8)
  %conv = zext i32 %call12 to i64
  %9 = load i64, i64* %p10, align 8
  %call13 = call i32 @_ZN20btConvexHullInternal4DMulIyjE3lowEy(i64 %9)
  %conv14 = zext i32 %call13 to i64
  %add = add i64 %conv, %conv14
  store i64 %add, i64* %p0110, align 8
  %10 = load i64, i64* %p01, align 8
  %call15 = call i32 @_ZN20btConvexHullInternal4DMulIyjE4highEy(i64 %10)
  %conv16 = zext i32 %call15 to i64
  %11 = load i64, i64* %p11, align 8
  %add17 = add i64 %11, %conv16
  store i64 %add17, i64* %p11, align 8
  %12 = load i64, i64* %p10, align 8
  %call18 = call i32 @_ZN20btConvexHullInternal4DMulIyjE4highEy(i64 %12)
  %conv19 = zext i32 %call18 to i64
  %13 = load i64, i64* %p11, align 8
  %add20 = add i64 %13, %conv19
  store i64 %add20, i64* %p11, align 8
  %14 = load i64, i64* %p0110, align 8
  %call21 = call i32 @_ZN20btConvexHullInternal4DMulIyjE4highEy(i64 %14)
  %conv22 = zext i32 %call21 to i64
  %15 = load i64, i64* %p11, align 8
  %add23 = add i64 %15, %conv22
  store i64 %add23, i64* %p11, align 8
  call void @_ZN20btConvexHullInternal4DMulIyjE7shlHalfERy(i64* nonnull align 8 dereferenceable(8) %p0110)
  %16 = load i64, i64* %p0110, align 8
  %17 = load i64, i64* %p00, align 8
  %add24 = add i64 %17, %16
  store i64 %add24, i64* %p00, align 8
  %18 = load i64, i64* %p00, align 8
  %19 = load i64, i64* %p0110, align 8
  %cmp = icmp ult i64 %18, %19
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %20 = load i64, i64* %p11, align 8
  %inc = add i64 %20, 1
  store i64 %inc, i64* %p11, align 8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %21 = load i64, i64* %p00, align 8
  %22 = load i64*, i64** %resLow.addr, align 4
  store i64 %21, i64* %22, align 8
  %23 = load i64, i64* %p11, align 8
  %24 = load i64*, i64** %resHigh.addr, align 4
  store i64 %23, i64* %24, align 8
  ret void
}

; Function Attrs: noinline optnone
define hidden i32 @_ZNK20btConvexHullInternal10Rational647compareERKS0_(%"class.btConvexHullInternal::Rational64"* %this, %"class.btConvexHullInternal::Rational64"* nonnull align 8 dereferenceable(20) %b) #2 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.btConvexHullInternal::Rational64"*, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Rational64"*, align 4
  %ref.tmp = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp10 = alloca %"class.btConvexHullInternal::Int128", align 8
  store %"class.btConvexHullInternal::Rational64"* %this, %"class.btConvexHullInternal::Rational64"** %this.addr, align 4
  store %"class.btConvexHullInternal::Rational64"* %b, %"class.btConvexHullInternal::Rational64"** %b.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Rational64"*, %"class.btConvexHullInternal::Rational64"** %this.addr, align 4
  %sign = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 2
  %0 = load i32, i32* %sign, align 8
  %1 = load %"class.btConvexHullInternal::Rational64"*, %"class.btConvexHullInternal::Rational64"** %b.addr, align 4
  %sign2 = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %1, i32 0, i32 2
  %2 = load i32, i32* %sign2, align 8
  %cmp = icmp ne i32 %0, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %sign3 = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 2
  %3 = load i32, i32* %sign3, align 8
  %4 = load %"class.btConvexHullInternal::Rational64"*, %"class.btConvexHullInternal::Rational64"** %b.addr, align 4
  %sign4 = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %4, i32 0, i32 2
  %5 = load i32, i32* %sign4, align 8
  %sub = sub nsw i32 %3, %5
  store i32 %sub, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %sign5 = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 2
  %6 = load i32, i32* %sign5, align 8
  %cmp6 = icmp eq i32 %6, 0
  br i1 %cmp6, label %if.then7, label %if.end

if.then7:                                         ; preds = %if.else
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.else
  br label %if.end8

if.end8:                                          ; preds = %if.end
  %sign9 = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 2
  %7 = load i32, i32* %sign9, align 8
  %m_numerator = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 0
  %8 = load i64, i64* %m_numerator, align 8
  %9 = load %"class.btConvexHullInternal::Rational64"*, %"class.btConvexHullInternal::Rational64"** %b.addr, align 4
  %m_denominator = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %9, i32 0, i32 1
  %10 = load i64, i64* %m_denominator, align 8
  call void @_ZN20btConvexHullInternal6Int1283mulEyy(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp, i64 %8, i64 %10)
  %m_denominator11 = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 1
  %11 = load i64, i64* %m_denominator11, align 8
  %12 = load %"class.btConvexHullInternal::Rational64"*, %"class.btConvexHullInternal::Rational64"** %b.addr, align 4
  %m_numerator12 = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %12, i32 0, i32 0
  %13 = load i64, i64* %m_numerator12, align 8
  call void @_ZN20btConvexHullInternal6Int1283mulEyy(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp10, i64 %11, i64 %13)
  %call = call i32 @_ZNK20btConvexHullInternal6Int1284ucmpERKS0_(%"class.btConvexHullInternal::Int128"* %ref.tmp, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp10)
  %mul = mul nsw i32 %7, %call
  store i32 %mul, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end8, %if.then7, %if.then
  %14 = load i32, i32* %retval, align 4
  ret i32 %14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btConvexHullInternal6Int1284ucmpERKS0_(%"class.btConvexHullInternal::Int128"* %this, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %b) #1 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  store %"class.btConvexHullInternal::Int128"* %this, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  store %"class.btConvexHullInternal::Int128"* %b, %"class.btConvexHullInternal::Int128"** %b.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  %high = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 1
  %0 = load i64, i64* %high, align 8
  %1 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %b.addr, align 4
  %high2 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %1, i32 0, i32 1
  %2 = load i64, i64* %high2, align 8
  %cmp = icmp ult i64 %0, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %high3 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 1
  %3 = load i64, i64* %high3, align 8
  %4 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %b.addr, align 4
  %high4 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %4, i32 0, i32 1
  %5 = load i64, i64* %high4, align 8
  %cmp5 = icmp ugt i64 %3, %5
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end
  store i32 1, i32* %retval, align 4
  br label %return

if.end7:                                          ; preds = %if.end
  %low = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 0
  %6 = load i64, i64* %low, align 8
  %7 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %b.addr, align 4
  %low8 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %7, i32 0, i32 0
  %8 = load i64, i64* %low8, align 8
  %cmp9 = icmp ult i64 %6, %8
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store i32 -1, i32* %retval, align 4
  br label %return

if.end11:                                         ; preds = %if.end7
  %low12 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 0
  %9 = load i64, i64* %low12, align 8
  %10 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %b.addr, align 4
  %low13 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %10, i32 0, i32 0
  %11 = load i64, i64* %low13, align 8
  %cmp14 = icmp ugt i64 %9, %11
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %if.end11
  store i32 1, i32* %retval, align 4
  br label %return

if.end16:                                         ; preds = %if.end11
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end16, %if.then15, %if.then10, %if.then6, %if.then
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

; Function Attrs: noinline optnone
define hidden i32 @_ZNK20btConvexHullInternal11Rational1287compareERKS0_(%"class.btConvexHullInternal::Rational128"* %this, %"class.btConvexHullInternal::Rational128"* nonnull align 8 dereferenceable(37) %b) #2 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.btConvexHullInternal::Rational128"*, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Rational128"*, align 4
  %nbdLow = alloca %"class.btConvexHullInternal::Int128", align 8
  %nbdHigh = alloca %"class.btConvexHullInternal::Int128", align 8
  %dbnLow = alloca %"class.btConvexHullInternal::Int128", align 8
  %dbnHigh = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp18 = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp19 = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp21 = alloca %"class.btConvexHullInternal::Int128", align 8
  %cmp23 = alloca i32, align 4
  store %"class.btConvexHullInternal::Rational128"* %this, %"class.btConvexHullInternal::Rational128"** %this.addr, align 4
  store %"class.btConvexHullInternal::Rational128"* %b, %"class.btConvexHullInternal::Rational128"** %b.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Rational128"*, %"class.btConvexHullInternal::Rational128"** %this.addr, align 4
  %sign = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 2
  %0 = load i32, i32* %sign, align 8
  %1 = load %"class.btConvexHullInternal::Rational128"*, %"class.btConvexHullInternal::Rational128"** %b.addr, align 4
  %sign2 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %1, i32 0, i32 2
  %2 = load i32, i32* %sign2, align 8
  %cmp = icmp ne i32 %0, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %sign3 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 2
  %3 = load i32, i32* %sign3, align 8
  %4 = load %"class.btConvexHullInternal::Rational128"*, %"class.btConvexHullInternal::Rational128"** %b.addr, align 4
  %sign4 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %4, i32 0, i32 2
  %5 = load i32, i32* %sign4, align 8
  %sub = sub nsw i32 %3, %5
  store i32 %sub, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %sign5 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 2
  %6 = load i32, i32* %sign5, align 8
  %cmp6 = icmp eq i32 %6, 0
  br i1 %cmp6, label %if.then7, label %if.end

if.then7:                                         ; preds = %if.else
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.else
  br label %if.end8

if.end8:                                          ; preds = %if.end
  %isInt64 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 3
  %7 = load i8, i8* %isInt64, align 4
  %tobool = trunc i8 %7 to i1
  br i1 %tobool, label %if.then9, label %if.end12

if.then9:                                         ; preds = %if.end8
  %8 = load %"class.btConvexHullInternal::Rational128"*, %"class.btConvexHullInternal::Rational128"** %b.addr, align 4
  %sign10 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 2
  %9 = load i32, i32* %sign10, align 8
  %conv = sext i32 %9 to i64
  %numerator = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 0
  %low = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %numerator, i32 0, i32 0
  %10 = load i64, i64* %low, align 8
  %mul = mul nsw i64 %conv, %10
  %call = call i32 @_ZNK20btConvexHullInternal11Rational1287compareEx(%"class.btConvexHullInternal::Rational128"* %8, i64 %mul)
  %sub11 = sub nsw i32 0, %call
  store i32 %sub11, i32* %retval, align 4
  br label %return

if.end12:                                         ; preds = %if.end8
  %call13 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ev(%"class.btConvexHullInternal::Int128"* %nbdLow)
  %call14 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ev(%"class.btConvexHullInternal::Int128"* %nbdHigh)
  %call15 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ev(%"class.btConvexHullInternal::Int128"* %dbnLow)
  %call16 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ev(%"class.btConvexHullInternal::Int128"* %dbnHigh)
  %numerator17 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 0
  %11 = bitcast %"class.btConvexHullInternal::Int128"* %agg.tmp to i8*
  %12 = bitcast %"class.btConvexHullInternal::Int128"* %numerator17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %11, i8* align 8 %12, i32 16, i1 false)
  %13 = load %"class.btConvexHullInternal::Rational128"*, %"class.btConvexHullInternal::Rational128"** %b.addr, align 4
  %denominator = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %13, i32 0, i32 1
  %14 = bitcast %"class.btConvexHullInternal::Int128"* %agg.tmp18 to i8*
  %15 = bitcast %"class.btConvexHullInternal::Int128"* %denominator to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %14, i8* align 8 %15, i32 16, i1 false)
  call void @_ZN20btConvexHullInternal4DMulINS_6Int128EyE3mulES1_S1_RS1_S3_(%"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp, %"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp18, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %nbdLow, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %nbdHigh)
  %denominator20 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 1
  %16 = bitcast %"class.btConvexHullInternal::Int128"* %agg.tmp19 to i8*
  %17 = bitcast %"class.btConvexHullInternal::Int128"* %denominator20 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %16, i8* align 8 %17, i32 16, i1 false)
  %18 = load %"class.btConvexHullInternal::Rational128"*, %"class.btConvexHullInternal::Rational128"** %b.addr, align 4
  %numerator22 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %18, i32 0, i32 0
  %19 = bitcast %"class.btConvexHullInternal::Int128"* %agg.tmp21 to i8*
  %20 = bitcast %"class.btConvexHullInternal::Int128"* %numerator22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %19, i8* align 8 %20, i32 16, i1 false)
  call void @_ZN20btConvexHullInternal4DMulINS_6Int128EyE3mulES1_S1_RS1_S3_(%"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp19, %"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp21, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %dbnLow, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %dbnHigh)
  %call24 = call i32 @_ZNK20btConvexHullInternal6Int1284ucmpERKS0_(%"class.btConvexHullInternal::Int128"* %nbdHigh, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %dbnHigh)
  store i32 %call24, i32* %cmp23, align 4
  %21 = load i32, i32* %cmp23, align 4
  %tobool25 = icmp ne i32 %21, 0
  br i1 %tobool25, label %if.then26, label %if.end29

if.then26:                                        ; preds = %if.end12
  %22 = load i32, i32* %cmp23, align 4
  %sign27 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 2
  %23 = load i32, i32* %sign27, align 8
  %mul28 = mul nsw i32 %22, %23
  store i32 %mul28, i32* %retval, align 4
  br label %return

if.end29:                                         ; preds = %if.end12
  %call30 = call i32 @_ZNK20btConvexHullInternal6Int1284ucmpERKS0_(%"class.btConvexHullInternal::Int128"* %nbdLow, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %dbnLow)
  %sign31 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 2
  %24 = load i32, i32* %sign31, align 8
  %mul32 = mul nsw i32 %call30, %24
  store i32 %mul32, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end29, %if.then26, %if.then9, %if.then7, %if.then
  %25 = load i32, i32* %retval, align 4
  ret i32 %25
}

; Function Attrs: noinline optnone
define hidden i32 @_ZNK20btConvexHullInternal11Rational1287compareEx(%"class.btConvexHullInternal::Rational128"* %this, i64 %b) #2 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.btConvexHullInternal::Rational128"*, align 4
  %b.addr = alloca i64, align 8
  %a = alloca i64, align 8
  %ref.tmp = alloca %"class.btConvexHullInternal::Int128", align 8
  store %"class.btConvexHullInternal::Rational128"* %this, %"class.btConvexHullInternal::Rational128"** %this.addr, align 4
  store i64 %b, i64* %b.addr, align 8
  %this1 = load %"class.btConvexHullInternal::Rational128"*, %"class.btConvexHullInternal::Rational128"** %this.addr, align 4
  %isInt64 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 3
  %0 = load i8, i8* %isInt64, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %sign = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 2
  %1 = load i32, i32* %sign, align 8
  %conv = sext i32 %1 to i64
  %numerator = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 0
  %low = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %numerator, i32 0, i32 0
  %2 = load i64, i64* %low, align 8
  %mul = mul nsw i64 %conv, %2
  store i64 %mul, i64* %a, align 8
  %3 = load i64, i64* %a, align 8
  %4 = load i64, i64* %b.addr, align 8
  %cmp = icmp sgt i64 %3, %4
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %5 = load i64, i64* %a, align 8
  %6 = load i64, i64* %b.addr, align 8
  %cmp2 = icmp slt i64 %5, %6
  %7 = zext i1 %cmp2 to i64
  %cond = select i1 %cmp2, i32 -1, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond3 = phi i32 [ 1, %cond.true ], [ %cond, %cond.false ]
  store i32 %cond3, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %8 = load i64, i64* %b.addr, align 8
  %cmp4 = icmp sgt i64 %8, 0
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %sign6 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 2
  %9 = load i32, i32* %sign6, align 8
  %cmp7 = icmp sle i32 %9, 0
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.then5
  store i32 -1, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.then5
  br label %if.end19

if.else:                                          ; preds = %if.end
  %10 = load i64, i64* %b.addr, align 8
  %cmp10 = icmp slt i64 %10, 0
  br i1 %cmp10, label %if.then11, label %if.else16

if.then11:                                        ; preds = %if.else
  %sign12 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 2
  %11 = load i32, i32* %sign12, align 8
  %cmp13 = icmp sge i32 %11, 0
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.then11
  store i32 1, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.then11
  %12 = load i64, i64* %b.addr, align 8
  %sub = sub nsw i64 0, %12
  store i64 %sub, i64* %b.addr, align 8
  br label %if.end18

if.else16:                                        ; preds = %if.else
  %sign17 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 2
  %13 = load i32, i32* %sign17, align 8
  store i32 %13, i32* %retval, align 4
  br label %return

if.end18:                                         ; preds = %if.end15
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %if.end9
  %numerator20 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 0
  %denominator = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 1
  %14 = load i64, i64* %b.addr, align 8
  call void @_ZNK20btConvexHullInternal6Int128mlEx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp, %"class.btConvexHullInternal::Int128"* %denominator, i64 %14)
  %call = call i32 @_ZNK20btConvexHullInternal6Int1284ucmpERKS0_(%"class.btConvexHullInternal::Int128"* %numerator20, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp)
  %sign21 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 2
  %15 = load i32, i32* %sign21, align 8
  %mul22 = mul nsw i32 %call, %15
  store i32 %mul22, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end19, %if.else16, %if.then14, %if.then8, %cond.end
  %16 = load i32, i32* %retval, align 4
  ret i32 %16
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btConvexHullInternal4DMulINS_6Int128EyE3mulES1_S1_RS1_S3_(%"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %a, %"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %b, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %resLow, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %resHigh) #2 comdat {
entry:
  %resLow.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  %resHigh.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  %p00 = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp1 = alloca %"class.btConvexHullInternal::Int128", align 8
  %p01 = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp3 = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp5 = alloca %"class.btConvexHullInternal::Int128", align 8
  %p10 = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp7 = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp9 = alloca %"class.btConvexHullInternal::Int128", align 8
  %p11 = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp11 = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp13 = alloca %"class.btConvexHullInternal::Int128", align 8
  %p0110 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp15 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp18 = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp19 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp22 = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp23 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp27 = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp28 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp32 = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp33 = alloca %"class.btConvexHullInternal::Int128", align 8
  store %"class.btConvexHullInternal::Int128"* %resLow, %"class.btConvexHullInternal::Int128"** %resLow.addr, align 4
  store %"class.btConvexHullInternal::Int128"* %resHigh, %"class.btConvexHullInternal::Int128"** %resHigh.addr, align 4
  %0 = bitcast %"class.btConvexHullInternal::Int128"* %agg.tmp to i8*
  %1 = bitcast %"class.btConvexHullInternal::Int128"* %a to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %0, i8* align 8 %1, i32 16, i1 false)
  %call = call i64 @_ZN20btConvexHullInternal4DMulINS_6Int128EyE3lowES1_(%"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp)
  %2 = bitcast %"class.btConvexHullInternal::Int128"* %agg.tmp1 to i8*
  %3 = bitcast %"class.btConvexHullInternal::Int128"* %b to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %2, i8* align 8 %3, i32 16, i1 false)
  %call2 = call i64 @_ZN20btConvexHullInternal4DMulINS_6Int128EyE3lowES1_(%"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp1)
  call void @_ZN20btConvexHullInternal4DMulINS_6Int128EyE3mulEyy(%"class.btConvexHullInternal::Int128"* sret align 8 %p00, i64 %call, i64 %call2)
  %4 = bitcast %"class.btConvexHullInternal::Int128"* %agg.tmp3 to i8*
  %5 = bitcast %"class.btConvexHullInternal::Int128"* %a to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %4, i8* align 8 %5, i32 16, i1 false)
  %call4 = call i64 @_ZN20btConvexHullInternal4DMulINS_6Int128EyE3lowES1_(%"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp3)
  %6 = bitcast %"class.btConvexHullInternal::Int128"* %agg.tmp5 to i8*
  %7 = bitcast %"class.btConvexHullInternal::Int128"* %b to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %6, i8* align 8 %7, i32 16, i1 false)
  %call6 = call i64 @_ZN20btConvexHullInternal4DMulINS_6Int128EyE4highES1_(%"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp5)
  call void @_ZN20btConvexHullInternal4DMulINS_6Int128EyE3mulEyy(%"class.btConvexHullInternal::Int128"* sret align 8 %p01, i64 %call4, i64 %call6)
  %8 = bitcast %"class.btConvexHullInternal::Int128"* %agg.tmp7 to i8*
  %9 = bitcast %"class.btConvexHullInternal::Int128"* %a to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %8, i8* align 8 %9, i32 16, i1 false)
  %call8 = call i64 @_ZN20btConvexHullInternal4DMulINS_6Int128EyE4highES1_(%"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp7)
  %10 = bitcast %"class.btConvexHullInternal::Int128"* %agg.tmp9 to i8*
  %11 = bitcast %"class.btConvexHullInternal::Int128"* %b to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %10, i8* align 8 %11, i32 16, i1 false)
  %call10 = call i64 @_ZN20btConvexHullInternal4DMulINS_6Int128EyE3lowES1_(%"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp9)
  call void @_ZN20btConvexHullInternal4DMulINS_6Int128EyE3mulEyy(%"class.btConvexHullInternal::Int128"* sret align 8 %p10, i64 %call8, i64 %call10)
  %12 = bitcast %"class.btConvexHullInternal::Int128"* %agg.tmp11 to i8*
  %13 = bitcast %"class.btConvexHullInternal::Int128"* %a to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %12, i8* align 8 %13, i32 16, i1 false)
  %call12 = call i64 @_ZN20btConvexHullInternal4DMulINS_6Int128EyE4highES1_(%"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp11)
  %14 = bitcast %"class.btConvexHullInternal::Int128"* %agg.tmp13 to i8*
  %15 = bitcast %"class.btConvexHullInternal::Int128"* %b to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %14, i8* align 8 %15, i32 16, i1 false)
  %call14 = call i64 @_ZN20btConvexHullInternal4DMulINS_6Int128EyE4highES1_(%"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp13)
  call void @_ZN20btConvexHullInternal4DMulINS_6Int128EyE3mulEyy(%"class.btConvexHullInternal::Int128"* sret align 8 %p11, i64 %call12, i64 %call14)
  %16 = bitcast %"class.btConvexHullInternal::Int128"* %agg.tmp15 to i8*
  %17 = bitcast %"class.btConvexHullInternal::Int128"* %p01 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %16, i8* align 8 %17, i32 16, i1 false)
  %call16 = call i64 @_ZN20btConvexHullInternal4DMulINS_6Int128EyE3lowES1_(%"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp15)
  %call17 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ey(%"class.btConvexHullInternal::Int128"* %ref.tmp, i64 %call16)
  %18 = bitcast %"class.btConvexHullInternal::Int128"* %agg.tmp19 to i8*
  %19 = bitcast %"class.btConvexHullInternal::Int128"* %p10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %18, i8* align 8 %19, i32 16, i1 false)
  %call20 = call i64 @_ZN20btConvexHullInternal4DMulINS_6Int128EyE3lowES1_(%"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp19)
  %call21 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ey(%"class.btConvexHullInternal::Int128"* %ref.tmp18, i64 %call20)
  call void @_ZNK20btConvexHullInternal6Int128plERKS0_(%"class.btConvexHullInternal::Int128"* sret align 8 %p0110, %"class.btConvexHullInternal::Int128"* %ref.tmp, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp18)
  %20 = bitcast %"class.btConvexHullInternal::Int128"* %agg.tmp23 to i8*
  %21 = bitcast %"class.btConvexHullInternal::Int128"* %p01 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %20, i8* align 8 %21, i32 16, i1 false)
  %call24 = call i64 @_ZN20btConvexHullInternal4DMulINS_6Int128EyE4highES1_(%"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp23)
  %call25 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ey(%"class.btConvexHullInternal::Int128"* %ref.tmp22, i64 %call24)
  %call26 = call nonnull align 8 dereferenceable(16) %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128pLERKS0_(%"class.btConvexHullInternal::Int128"* %p11, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp22)
  %22 = bitcast %"class.btConvexHullInternal::Int128"* %agg.tmp28 to i8*
  %23 = bitcast %"class.btConvexHullInternal::Int128"* %p10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %22, i8* align 8 %23, i32 16, i1 false)
  %call29 = call i64 @_ZN20btConvexHullInternal4DMulINS_6Int128EyE4highES1_(%"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp28)
  %call30 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ey(%"class.btConvexHullInternal::Int128"* %ref.tmp27, i64 %call29)
  %call31 = call nonnull align 8 dereferenceable(16) %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128pLERKS0_(%"class.btConvexHullInternal::Int128"* %p11, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp27)
  %24 = bitcast %"class.btConvexHullInternal::Int128"* %agg.tmp33 to i8*
  %25 = bitcast %"class.btConvexHullInternal::Int128"* %p0110 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %24, i8* align 8 %25, i32 16, i1 false)
  %call34 = call i64 @_ZN20btConvexHullInternal4DMulINS_6Int128EyE4highES1_(%"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp33)
  %call35 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ey(%"class.btConvexHullInternal::Int128"* %ref.tmp32, i64 %call34)
  %call36 = call nonnull align 8 dereferenceable(16) %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128pLERKS0_(%"class.btConvexHullInternal::Int128"* %p11, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp32)
  call void @_ZN20btConvexHullInternal4DMulINS_6Int128EyE7shlHalfERS1_(%"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %p0110)
  %call37 = call nonnull align 8 dereferenceable(16) %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128pLERKS0_(%"class.btConvexHullInternal::Int128"* %p00, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %p0110)
  %call38 = call zeroext i1 @_ZNK20btConvexHullInternal6Int128ltERKS0_(%"class.btConvexHullInternal::Int128"* %p00, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %p0110)
  br i1 %call38, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call39 = call nonnull align 8 dereferenceable(16) %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128ppEv(%"class.btConvexHullInternal::Int128"* %p11)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %26 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %resLow.addr, align 4
  %27 = bitcast %"class.btConvexHullInternal::Int128"* %26 to i8*
  %28 = bitcast %"class.btConvexHullInternal::Int128"* %p00 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %27, i8* align 8 %28, i32 16, i1 false)
  %29 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %resHigh.addr, align 4
  %30 = bitcast %"class.btConvexHullInternal::Int128"* %29 to i8*
  %31 = bitcast %"class.btConvexHullInternal::Int128"* %p11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %30, i8* align 8 %31, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define hidden %"class.btConvexHullInternal::Edge"* @_ZN20btConvexHullInternal11newEdgePairEPNS_6VertexES1_(%class.btConvexHullInternal* %this, %"class.btConvexHullInternal::Vertex"* %from, %"class.btConvexHullInternal::Vertex"* %to) #2 {
entry:
  %this.addr = alloca %class.btConvexHullInternal*, align 4
  %from.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %to.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %e = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %r = alloca %"class.btConvexHullInternal::Edge"*, align 4
  store %class.btConvexHullInternal* %this, %class.btConvexHullInternal** %this.addr, align 4
  store %"class.btConvexHullInternal::Vertex"* %from, %"class.btConvexHullInternal::Vertex"** %from.addr, align 4
  store %"class.btConvexHullInternal::Vertex"* %to, %"class.btConvexHullInternal::Vertex"** %to.addr, align 4
  %this1 = load %class.btConvexHullInternal*, %class.btConvexHullInternal** %this.addr, align 4
  %edgePool = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 3
  %call = call %"class.btConvexHullInternal::Edge"* @_ZN20btConvexHullInternal4PoolINS_4EdgeEE9newObjectEv(%"class.btConvexHullInternal::Pool.0"* %edgePool)
  store %"class.btConvexHullInternal::Edge"* %call, %"class.btConvexHullInternal::Edge"** %e, align 4
  %edgePool2 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 3
  %call3 = call %"class.btConvexHullInternal::Edge"* @_ZN20btConvexHullInternal4PoolINS_4EdgeEE9newObjectEv(%"class.btConvexHullInternal::Pool.0"* %edgePool2)
  store %"class.btConvexHullInternal::Edge"* %call3, %"class.btConvexHullInternal::Edge"** %r, align 4
  %0 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %r, align 4
  %1 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %reverse = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %1, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* %0, %"class.btConvexHullInternal::Edge"** %reverse, align 4
  %2 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %3 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %r, align 4
  %reverse4 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %3, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* %2, %"class.btConvexHullInternal::Edge"** %reverse4, align 4
  %mergeStamp = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 6
  %4 = load i32, i32* %mergeStamp, align 4
  %5 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %copy = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %5, i32 0, i32 5
  store i32 %4, i32* %copy, align 4
  %mergeStamp5 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 6
  %6 = load i32, i32* %mergeStamp5, align 4
  %7 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %r, align 4
  %copy6 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %7, i32 0, i32 5
  store i32 %6, i32* %copy6, align 4
  %8 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %to.addr, align 4
  %9 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %target = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %9, i32 0, i32 3
  store %"class.btConvexHullInternal::Vertex"* %8, %"class.btConvexHullInternal::Vertex"** %target, align 4
  %10 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %from.addr, align 4
  %11 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %r, align 4
  %target7 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %11, i32 0, i32 3
  store %"class.btConvexHullInternal::Vertex"* %10, %"class.btConvexHullInternal::Vertex"** %target7, align 4
  %12 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %face = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %12, i32 0, i32 4
  store %"class.btConvexHullInternal::Face"* null, %"class.btConvexHullInternal::Face"** %face, align 4
  %13 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %r, align 4
  %face8 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %13, i32 0, i32 4
  store %"class.btConvexHullInternal::Face"* null, %"class.btConvexHullInternal::Face"** %face8, align 4
  %usedEdgePairs = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 10
  %14 = load i32, i32* %usedEdgePairs, align 4
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %usedEdgePairs, align 4
  %usedEdgePairs9 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 10
  %15 = load i32, i32* %usedEdgePairs9, align 4
  %maxUsedEdgePairs = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 11
  %16 = load i32, i32* %maxUsedEdgePairs, align 4
  %cmp = icmp sgt i32 %15, %16
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %usedEdgePairs10 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 10
  %17 = load i32, i32* %usedEdgePairs10, align 4
  %maxUsedEdgePairs11 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 11
  store i32 %17, i32* %maxUsedEdgePairs11, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %18 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  ret %"class.btConvexHullInternal::Edge"* %18
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Edge"* @_ZN20btConvexHullInternal4PoolINS_4EdgeEE9newObjectEv(%"class.btConvexHullInternal::Pool.0"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Pool.0"*, align 4
  %o = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %p = alloca %"class.btConvexHullInternal::PoolArray.1"*, align 4
  store %"class.btConvexHullInternal::Pool.0"* %this, %"class.btConvexHullInternal::Pool.0"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Pool.0"*, %"class.btConvexHullInternal::Pool.0"** %this.addr, align 4
  %freeObjects = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 2
  %0 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %freeObjects, align 4
  store %"class.btConvexHullInternal::Edge"* %0, %"class.btConvexHullInternal::Edge"** %o, align 4
  %1 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %o, align 4
  %tobool = icmp ne %"class.btConvexHullInternal::Edge"* %1, null
  br i1 %tobool, label %if.end9, label %if.then

if.then:                                          ; preds = %entry
  %nextArray = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 1
  %2 = load %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::PoolArray.1"** %nextArray, align 4
  store %"class.btConvexHullInternal::PoolArray.1"* %2, %"class.btConvexHullInternal::PoolArray.1"** %p, align 4
  %3 = load %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::PoolArray.1"** %p, align 4
  %tobool2 = icmp ne %"class.btConvexHullInternal::PoolArray.1"* %3, null
  br i1 %tobool2, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.then
  %4 = load %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::PoolArray.1"** %p, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.1", %"class.btConvexHullInternal::PoolArray.1"* %4, i32 0, i32 2
  %5 = load %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::PoolArray.1"** %next, align 4
  %nextArray4 = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 1
  store %"class.btConvexHullInternal::PoolArray.1"* %5, %"class.btConvexHullInternal::PoolArray.1"** %nextArray4, align 4
  br label %if.end

if.else:                                          ; preds = %if.then
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 12, i32 16)
  %6 = bitcast i8* %call to %"class.btConvexHullInternal::PoolArray.1"*
  %arraySize = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 3
  %7 = load i32, i32* %arraySize, align 4
  %call5 = call %"class.btConvexHullInternal::PoolArray.1"* @_ZN20btConvexHullInternal9PoolArrayINS_4EdgeEEC2Ei(%"class.btConvexHullInternal::PoolArray.1"* %6, i32 %7)
  store %"class.btConvexHullInternal::PoolArray.1"* %6, %"class.btConvexHullInternal::PoolArray.1"** %p, align 4
  %arrays = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 0
  %8 = load %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::PoolArray.1"** %arrays, align 4
  %9 = load %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::PoolArray.1"** %p, align 4
  %next6 = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.1", %"class.btConvexHullInternal::PoolArray.1"* %9, i32 0, i32 2
  store %"class.btConvexHullInternal::PoolArray.1"* %8, %"class.btConvexHullInternal::PoolArray.1"** %next6, align 4
  %10 = load %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::PoolArray.1"** %p, align 4
  %arrays7 = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 0
  store %"class.btConvexHullInternal::PoolArray.1"* %10, %"class.btConvexHullInternal::PoolArray.1"** %arrays7, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then3
  %11 = load %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::PoolArray.1"** %p, align 4
  %call8 = call %"class.btConvexHullInternal::Edge"* @_ZN20btConvexHullInternal9PoolArrayINS_4EdgeEE4initEv(%"class.btConvexHullInternal::PoolArray.1"* %11)
  store %"class.btConvexHullInternal::Edge"* %call8, %"class.btConvexHullInternal::Edge"** %o, align 4
  br label %if.end9

if.end9:                                          ; preds = %if.end, %entry
  %12 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %o, align 4
  %next10 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %12, i32 0, i32 0
  %13 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next10, align 4
  %freeObjects11 = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* %13, %"class.btConvexHullInternal::Edge"** %freeObjects11, align 4
  %14 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %o, align 4
  %15 = bitcast %"class.btConvexHullInternal::Edge"* %14 to i8*
  %16 = bitcast i8* %15 to %"class.btConvexHullInternal::Edge"*
  %17 = bitcast %"class.btConvexHullInternal::Edge"* %16 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %17, i8 0, i32 24, i1 false)
  ret %"class.btConvexHullInternal::Edge"* %16
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN20btConvexHullInternal15mergeProjectionERNS_16IntermediateHullES1_RPNS_6VertexES4_(%class.btConvexHullInternal* %this, %"class.btConvexHullInternal::IntermediateHull"* nonnull align 4 dereferenceable(16) %h0, %"class.btConvexHullInternal::IntermediateHull"* nonnull align 4 dereferenceable(16) %h1, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %c0, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %c1) #1 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btConvexHullInternal*, align 4
  %h0.addr = alloca %"class.btConvexHullInternal::IntermediateHull"*, align 4
  %h1.addr = alloca %"class.btConvexHullInternal::IntermediateHull"*, align 4
  %c0.addr = alloca %"class.btConvexHullInternal::Vertex"**, align 4
  %c1.addr = alloca %"class.btConvexHullInternal::Vertex"**, align 4
  %v0 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %v1 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %v1p = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %v1n = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %v00 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %v10 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %sign = alloca i32, align 4
  %side = alloca i32, align 4
  %dx = alloca i32, align 4
  %dy = alloca i32, align 4
  %w0 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %dx0 = alloca i32, align 4
  %dy0 = alloca i32, align 4
  %w1 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %dx1 = alloca i32, align 4
  %dy1 = alloca i32, align 4
  %dxn = alloca i32, align 4
  %dy156 = alloca i32, align 4
  %w1162 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %dx1172 = alloca i32, align 4
  %dy1179 = alloca i32, align 4
  %w0203 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %dx0213 = alloca i32, align 4
  %dy0220 = alloca i32, align 4
  %dxn226 = alloca i32, align 4
  %x249 = alloca i32, align 4
  %y0 = alloca i32, align 4
  %w0254 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %t = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %y1 = alloca i32, align 4
  %w1276 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  store %class.btConvexHullInternal* %this, %class.btConvexHullInternal** %this.addr, align 4
  store %"class.btConvexHullInternal::IntermediateHull"* %h0, %"class.btConvexHullInternal::IntermediateHull"** %h0.addr, align 4
  store %"class.btConvexHullInternal::IntermediateHull"* %h1, %"class.btConvexHullInternal::IntermediateHull"** %h1.addr, align 4
  store %"class.btConvexHullInternal::Vertex"** %c0, %"class.btConvexHullInternal::Vertex"*** %c0.addr, align 4
  store %"class.btConvexHullInternal::Vertex"** %c1, %"class.btConvexHullInternal::Vertex"*** %c1.addr, align 4
  %this1 = load %class.btConvexHullInternal*, %class.btConvexHullInternal** %this.addr, align 4
  %0 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h0.addr, align 4
  %maxYx = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %0, i32 0, i32 3
  %1 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %maxYx, align 4
  store %"class.btConvexHullInternal::Vertex"* %1, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %2 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h1.addr, align 4
  %minYx = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %2, i32 0, i32 2
  %3 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %minYx, align 4
  store %"class.btConvexHullInternal::Vertex"* %3, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %4 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %point = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %4, i32 0, i32 7
  %x = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point, i32 0, i32 0
  %5 = load i32, i32* %x, align 8
  %6 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %point2 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %6, i32 0, i32 7
  %x3 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point2, i32 0, i32 0
  %7 = load i32, i32* %x3, align 8
  %cmp = icmp eq i32 %5, %7
  br i1 %cmp, label %land.lhs.true, label %if.end63

land.lhs.true:                                    ; preds = %entry
  %8 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %point4 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %8, i32 0, i32 7
  %y = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point4, i32 0, i32 1
  %9 = load i32, i32* %y, align 4
  %10 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %point5 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %10, i32 0, i32 7
  %y6 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point5, i32 0, i32 1
  %11 = load i32, i32* %y6, align 4
  %cmp7 = icmp eq i32 %9, %11
  br i1 %cmp7, label %if.then, label %if.end63

if.then:                                          ; preds = %land.lhs.true
  %12 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %prev = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %12, i32 0, i32 1
  %13 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %prev, align 4
  store %"class.btConvexHullInternal::Vertex"* %13, %"class.btConvexHullInternal::Vertex"** %v1p, align 4
  %14 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1p, align 4
  %15 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %cmp8 = icmp eq %"class.btConvexHullInternal::Vertex"* %14, %15
  br i1 %cmp8, label %if.then9, label %if.end12

if.then9:                                         ; preds = %if.then
  %16 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %17 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %c0.addr, align 4
  store %"class.btConvexHullInternal::Vertex"* %16, %"class.btConvexHullInternal::Vertex"** %17, align 4
  %18 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %edges = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %18, i32 0, i32 2
  %19 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edges, align 8
  %tobool = icmp ne %"class.btConvexHullInternal::Edge"* %19, null
  br i1 %tobool, label %if.then10, label %if.end

if.then10:                                        ; preds = %if.then9
  %20 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %edges11 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %20, i32 0, i32 2
  %21 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edges11, align 8
  %target = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %21, i32 0, i32 3
  %22 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target, align 4
  store %"class.btConvexHullInternal::Vertex"* %22, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  br label %if.end

if.end:                                           ; preds = %if.then10, %if.then9
  %23 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %24 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %c1.addr, align 4
  store %"class.btConvexHullInternal::Vertex"* %23, %"class.btConvexHullInternal::Vertex"** %24, align 4
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.then
  %25 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %25, i32 0, i32 0
  %26 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %next, align 8
  store %"class.btConvexHullInternal::Vertex"* %26, %"class.btConvexHullInternal::Vertex"** %v1n, align 4
  %27 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1n, align 4
  %28 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1p, align 4
  %next13 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %28, i32 0, i32 0
  store %"class.btConvexHullInternal::Vertex"* %27, %"class.btConvexHullInternal::Vertex"** %next13, align 8
  %29 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1p, align 4
  %30 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1n, align 4
  %prev14 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %30, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* %29, %"class.btConvexHullInternal::Vertex"** %prev14, align 4
  %31 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %32 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h1.addr, align 4
  %minXy = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %32, i32 0, i32 0
  %33 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %minXy, align 4
  %cmp15 = icmp eq %"class.btConvexHullInternal::Vertex"* %31, %33
  br i1 %cmp15, label %if.then16, label %if.end37

if.then16:                                        ; preds = %if.end12
  %34 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1n, align 4
  %point17 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %34, i32 0, i32 7
  %x18 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point17, i32 0, i32 0
  %35 = load i32, i32* %x18, align 8
  %36 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1p, align 4
  %point19 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %36, i32 0, i32 7
  %x20 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point19, i32 0, i32 0
  %37 = load i32, i32* %x20, align 8
  %cmp21 = icmp slt i32 %35, %37
  br i1 %cmp21, label %if.then33, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then16
  %38 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1n, align 4
  %point22 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %38, i32 0, i32 7
  %x23 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point22, i32 0, i32 0
  %39 = load i32, i32* %x23, align 8
  %40 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1p, align 4
  %point24 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %40, i32 0, i32 7
  %x25 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point24, i32 0, i32 0
  %41 = load i32, i32* %x25, align 8
  %cmp26 = icmp eq i32 %39, %41
  br i1 %cmp26, label %land.lhs.true27, label %if.else

land.lhs.true27:                                  ; preds = %lor.lhs.false
  %42 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1n, align 4
  %point28 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %42, i32 0, i32 7
  %y29 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point28, i32 0, i32 1
  %43 = load i32, i32* %y29, align 4
  %44 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1p, align 4
  %point30 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %44, i32 0, i32 7
  %y31 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point30, i32 0, i32 1
  %45 = load i32, i32* %y31, align 4
  %cmp32 = icmp slt i32 %43, %45
  br i1 %cmp32, label %if.then33, label %if.else

if.then33:                                        ; preds = %land.lhs.true27, %if.then16
  %46 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1n, align 4
  %47 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h1.addr, align 4
  %minXy34 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %47, i32 0, i32 0
  store %"class.btConvexHullInternal::Vertex"* %46, %"class.btConvexHullInternal::Vertex"** %minXy34, align 4
  br label %if.end36

if.else:                                          ; preds = %land.lhs.true27, %lor.lhs.false
  %48 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1p, align 4
  %49 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h1.addr, align 4
  %minXy35 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %49, i32 0, i32 0
  store %"class.btConvexHullInternal::Vertex"* %48, %"class.btConvexHullInternal::Vertex"** %minXy35, align 4
  br label %if.end36

if.end36:                                         ; preds = %if.else, %if.then33
  br label %if.end37

if.end37:                                         ; preds = %if.end36, %if.end12
  %50 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %51 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h1.addr, align 4
  %maxXy = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %51, i32 0, i32 1
  %52 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %maxXy, align 4
  %cmp38 = icmp eq %"class.btConvexHullInternal::Vertex"* %50, %52
  br i1 %cmp38, label %if.then39, label %if.end62

if.then39:                                        ; preds = %if.end37
  %53 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1n, align 4
  %point40 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %53, i32 0, i32 7
  %x41 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point40, i32 0, i32 0
  %54 = load i32, i32* %x41, align 8
  %55 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1p, align 4
  %point42 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %55, i32 0, i32 7
  %x43 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point42, i32 0, i32 0
  %56 = load i32, i32* %x43, align 8
  %cmp44 = icmp sgt i32 %54, %56
  br i1 %cmp44, label %if.then57, label %lor.lhs.false45

lor.lhs.false45:                                  ; preds = %if.then39
  %57 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1n, align 4
  %point46 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %57, i32 0, i32 7
  %x47 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point46, i32 0, i32 0
  %58 = load i32, i32* %x47, align 8
  %59 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1p, align 4
  %point48 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %59, i32 0, i32 7
  %x49 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point48, i32 0, i32 0
  %60 = load i32, i32* %x49, align 8
  %cmp50 = icmp eq i32 %58, %60
  br i1 %cmp50, label %land.lhs.true51, label %if.else59

land.lhs.true51:                                  ; preds = %lor.lhs.false45
  %61 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1n, align 4
  %point52 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %61, i32 0, i32 7
  %y53 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point52, i32 0, i32 1
  %62 = load i32, i32* %y53, align 4
  %63 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1p, align 4
  %point54 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %63, i32 0, i32 7
  %y55 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point54, i32 0, i32 1
  %64 = load i32, i32* %y55, align 4
  %cmp56 = icmp sgt i32 %62, %64
  br i1 %cmp56, label %if.then57, label %if.else59

if.then57:                                        ; preds = %land.lhs.true51, %if.then39
  %65 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1n, align 4
  %66 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h1.addr, align 4
  %maxXy58 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %66, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* %65, %"class.btConvexHullInternal::Vertex"** %maxXy58, align 4
  br label %if.end61

if.else59:                                        ; preds = %land.lhs.true51, %lor.lhs.false45
  %67 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1p, align 4
  %68 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h1.addr, align 4
  %maxXy60 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %68, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* %67, %"class.btConvexHullInternal::Vertex"** %maxXy60, align 4
  br label %if.end61

if.end61:                                         ; preds = %if.else59, %if.then57
  br label %if.end62

if.end62:                                         ; preds = %if.end61, %if.end37
  br label %if.end63

if.end63:                                         ; preds = %if.end62, %land.lhs.true, %entry
  %69 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h0.addr, align 4
  %maxXy64 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %69, i32 0, i32 1
  %70 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %maxXy64, align 4
  store %"class.btConvexHullInternal::Vertex"* %70, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %71 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h1.addr, align 4
  %maxXy65 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %71, i32 0, i32 1
  %72 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %maxXy65, align 4
  store %"class.btConvexHullInternal::Vertex"* %72, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %v00, align 4
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %v10, align 4
  store i32 1, i32* %sign, align 4
  store i32 0, i32* %side, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end63
  %73 = load i32, i32* %side, align 4
  %cmp66 = icmp sle i32 %73, 1
  br i1 %cmp66, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %74 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %point67 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %74, i32 0, i32 7
  %x68 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point67, i32 0, i32 0
  %75 = load i32, i32* %x68, align 8
  %76 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %point69 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %76, i32 0, i32 7
  %x70 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point69, i32 0, i32 0
  %77 = load i32, i32* %x70, align 8
  %sub = sub nsw i32 %75, %77
  %78 = load i32, i32* %sign, align 4
  %mul = mul nsw i32 %sub, %78
  store i32 %mul, i32* %dx, align 4
  %79 = load i32, i32* %dx, align 4
  %cmp71 = icmp sgt i32 %79, 0
  br i1 %cmp71, label %if.then72, label %if.else152

if.then72:                                        ; preds = %for.body
  br label %while.body

while.body:                                       ; preds = %if.then72, %if.then103, %if.then149
  %80 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %point73 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %80, i32 0, i32 7
  %y74 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point73, i32 0, i32 1
  %81 = load i32, i32* %y74, align 4
  %82 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %point75 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %82, i32 0, i32 7
  %y76 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point75, i32 0, i32 1
  %83 = load i32, i32* %y76, align 4
  %sub77 = sub nsw i32 %81, %83
  store i32 %sub77, i32* %dy, align 4
  %84 = load i32, i32* %side, align 4
  %tobool78 = icmp ne i32 %84, 0
  br i1 %tobool78, label %cond.true, label %cond.false

cond.true:                                        ; preds = %while.body
  %85 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %next79 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %85, i32 0, i32 0
  %86 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %next79, align 8
  br label %cond.end

cond.false:                                       ; preds = %while.body
  %87 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %prev80 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %87, i32 0, i32 1
  %88 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %prev80, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"class.btConvexHullInternal::Vertex"* [ %86, %cond.true ], [ %88, %cond.false ]
  store %"class.btConvexHullInternal::Vertex"* %cond, %"class.btConvexHullInternal::Vertex"** %w0, align 4
  %89 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w0, align 4
  %90 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %cmp81 = icmp ne %"class.btConvexHullInternal::Vertex"* %89, %90
  br i1 %cmp81, label %if.then82, label %if.end111

if.then82:                                        ; preds = %cond.end
  %91 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w0, align 4
  %point83 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %91, i32 0, i32 7
  %x84 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point83, i32 0, i32 0
  %92 = load i32, i32* %x84, align 8
  %93 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %point85 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %93, i32 0, i32 7
  %x86 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point85, i32 0, i32 0
  %94 = load i32, i32* %x86, align 8
  %sub87 = sub nsw i32 %92, %94
  %95 = load i32, i32* %sign, align 4
  %mul88 = mul nsw i32 %sub87, %95
  store i32 %mul88, i32* %dx0, align 4
  %96 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w0, align 4
  %point89 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %96, i32 0, i32 7
  %y90 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point89, i32 0, i32 1
  %97 = load i32, i32* %y90, align 4
  %98 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %point91 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %98, i32 0, i32 7
  %y92 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point91, i32 0, i32 1
  %99 = load i32, i32* %y92, align 4
  %sub93 = sub nsw i32 %97, %99
  store i32 %sub93, i32* %dy0, align 4
  %100 = load i32, i32* %dy0, align 4
  %cmp94 = icmp sle i32 %100, 0
  br i1 %cmp94, label %land.lhs.true95, label %if.end110

land.lhs.true95:                                  ; preds = %if.then82
  %101 = load i32, i32* %dx0, align 4
  %cmp96 = icmp eq i32 %101, 0
  br i1 %cmp96, label %if.then103, label %lor.lhs.false97

lor.lhs.false97:                                  ; preds = %land.lhs.true95
  %102 = load i32, i32* %dx0, align 4
  %cmp98 = icmp slt i32 %102, 0
  br i1 %cmp98, label %land.lhs.true99, label %if.end110

land.lhs.true99:                                  ; preds = %lor.lhs.false97
  %103 = load i32, i32* %dy0, align 4
  %104 = load i32, i32* %dx, align 4
  %mul100 = mul nsw i32 %103, %104
  %105 = load i32, i32* %dy, align 4
  %106 = load i32, i32* %dx0, align 4
  %mul101 = mul nsw i32 %105, %106
  %cmp102 = icmp sle i32 %mul100, %mul101
  br i1 %cmp102, label %if.then103, label %if.end110

if.then103:                                       ; preds = %land.lhs.true99, %land.lhs.true95
  %107 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w0, align 4
  store %"class.btConvexHullInternal::Vertex"* %107, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %108 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %point104 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %108, i32 0, i32 7
  %x105 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point104, i32 0, i32 0
  %109 = load i32, i32* %x105, align 8
  %110 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %point106 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %110, i32 0, i32 7
  %x107 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point106, i32 0, i32 0
  %111 = load i32, i32* %x107, align 8
  %sub108 = sub nsw i32 %109, %111
  %112 = load i32, i32* %sign, align 4
  %mul109 = mul nsw i32 %sub108, %112
  store i32 %mul109, i32* %dx, align 4
  br label %while.body

if.end110:                                        ; preds = %land.lhs.true99, %lor.lhs.false97, %if.then82
  br label %if.end111

if.end111:                                        ; preds = %if.end110, %cond.end
  %113 = load i32, i32* %side, align 4
  %tobool112 = icmp ne i32 %113, 0
  br i1 %tobool112, label %cond.true113, label %cond.false115

cond.true113:                                     ; preds = %if.end111
  %114 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %next114 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %114, i32 0, i32 0
  %115 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %next114, align 8
  br label %cond.end117

cond.false115:                                    ; preds = %if.end111
  %116 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %prev116 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %116, i32 0, i32 1
  %117 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %prev116, align 4
  br label %cond.end117

cond.end117:                                      ; preds = %cond.false115, %cond.true113
  %cond118 = phi %"class.btConvexHullInternal::Vertex"* [ %115, %cond.true113 ], [ %117, %cond.false115 ]
  store %"class.btConvexHullInternal::Vertex"* %cond118, %"class.btConvexHullInternal::Vertex"** %w1, align 4
  %118 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w1, align 4
  %119 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %cmp119 = icmp ne %"class.btConvexHullInternal::Vertex"* %118, %119
  br i1 %cmp119, label %if.then120, label %if.end151

if.then120:                                       ; preds = %cond.end117
  %120 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w1, align 4
  %point121 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %120, i32 0, i32 7
  %x122 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point121, i32 0, i32 0
  %121 = load i32, i32* %x122, align 8
  %122 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %point123 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %122, i32 0, i32 7
  %x124 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point123, i32 0, i32 0
  %123 = load i32, i32* %x124, align 8
  %sub125 = sub nsw i32 %121, %123
  %124 = load i32, i32* %sign, align 4
  %mul126 = mul nsw i32 %sub125, %124
  store i32 %mul126, i32* %dx1, align 4
  %125 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w1, align 4
  %point127 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %125, i32 0, i32 7
  %y128 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point127, i32 0, i32 1
  %126 = load i32, i32* %y128, align 4
  %127 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %point129 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %127, i32 0, i32 7
  %y130 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point129, i32 0, i32 1
  %128 = load i32, i32* %y130, align 4
  %sub131 = sub nsw i32 %126, %128
  store i32 %sub131, i32* %dy1, align 4
  %129 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w1, align 4
  %point132 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %129, i32 0, i32 7
  %x133 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point132, i32 0, i32 0
  %130 = load i32, i32* %x133, align 8
  %131 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %point134 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %131, i32 0, i32 7
  %x135 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point134, i32 0, i32 0
  %132 = load i32, i32* %x135, align 8
  %sub136 = sub nsw i32 %130, %132
  %133 = load i32, i32* %sign, align 4
  %mul137 = mul nsw i32 %sub136, %133
  store i32 %mul137, i32* %dxn, align 4
  %134 = load i32, i32* %dxn, align 4
  %cmp138 = icmp sgt i32 %134, 0
  br i1 %cmp138, label %land.lhs.true139, label %if.end150

land.lhs.true139:                                 ; preds = %if.then120
  %135 = load i32, i32* %dy1, align 4
  %cmp140 = icmp slt i32 %135, 0
  br i1 %cmp140, label %land.lhs.true141, label %if.end150

land.lhs.true141:                                 ; preds = %land.lhs.true139
  %136 = load i32, i32* %dx1, align 4
  %cmp142 = icmp eq i32 %136, 0
  br i1 %cmp142, label %if.then149, label %lor.lhs.false143

lor.lhs.false143:                                 ; preds = %land.lhs.true141
  %137 = load i32, i32* %dx1, align 4
  %cmp144 = icmp slt i32 %137, 0
  br i1 %cmp144, label %land.lhs.true145, label %if.end150

land.lhs.true145:                                 ; preds = %lor.lhs.false143
  %138 = load i32, i32* %dy1, align 4
  %139 = load i32, i32* %dx, align 4
  %mul146 = mul nsw i32 %138, %139
  %140 = load i32, i32* %dy, align 4
  %141 = load i32, i32* %dx1, align 4
  %mul147 = mul nsw i32 %140, %141
  %cmp148 = icmp slt i32 %mul146, %mul147
  br i1 %cmp148, label %if.then149, label %if.end150

if.then149:                                       ; preds = %land.lhs.true145, %land.lhs.true141
  %142 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w1, align 4
  store %"class.btConvexHullInternal::Vertex"* %142, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %143 = load i32, i32* %dxn, align 4
  store i32 %143, i32* %dx, align 4
  br label %while.body

if.end150:                                        ; preds = %land.lhs.true145, %lor.lhs.false143, %land.lhs.true139, %if.then120
  br label %if.end151

if.end151:                                        ; preds = %if.end150, %cond.end117
  br label %while.end

while.end:                                        ; preds = %if.end151
  br label %if.end300

if.else152:                                       ; preds = %for.body
  %144 = load i32, i32* %dx, align 4
  %cmp153 = icmp slt i32 %144, 0
  br i1 %cmp153, label %if.then154, label %if.else248

if.then154:                                       ; preds = %if.else152
  br label %while.body155

while.body155:                                    ; preds = %if.then154, %if.then194, %if.then244
  %145 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %point157 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %145, i32 0, i32 7
  %y158 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point157, i32 0, i32 1
  %146 = load i32, i32* %y158, align 4
  %147 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %point159 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %147, i32 0, i32 7
  %y160 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point159, i32 0, i32 1
  %148 = load i32, i32* %y160, align 4
  %sub161 = sub nsw i32 %146, %148
  store i32 %sub161, i32* %dy156, align 4
  %149 = load i32, i32* %side, align 4
  %tobool163 = icmp ne i32 %149, 0
  br i1 %tobool163, label %cond.true164, label %cond.false166

cond.true164:                                     ; preds = %while.body155
  %150 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %prev165 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %150, i32 0, i32 1
  %151 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %prev165, align 4
  br label %cond.end168

cond.false166:                                    ; preds = %while.body155
  %152 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %next167 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %152, i32 0, i32 0
  %153 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %next167, align 8
  br label %cond.end168

cond.end168:                                      ; preds = %cond.false166, %cond.true164
  %cond169 = phi %"class.btConvexHullInternal::Vertex"* [ %151, %cond.true164 ], [ %153, %cond.false166 ]
  store %"class.btConvexHullInternal::Vertex"* %cond169, %"class.btConvexHullInternal::Vertex"** %w1162, align 4
  %154 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w1162, align 4
  %155 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %cmp170 = icmp ne %"class.btConvexHullInternal::Vertex"* %154, %155
  br i1 %cmp170, label %if.then171, label %if.end202

if.then171:                                       ; preds = %cond.end168
  %156 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w1162, align 4
  %point173 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %156, i32 0, i32 7
  %x174 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point173, i32 0, i32 0
  %157 = load i32, i32* %x174, align 8
  %158 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %point175 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %158, i32 0, i32 7
  %x176 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point175, i32 0, i32 0
  %159 = load i32, i32* %x176, align 8
  %sub177 = sub nsw i32 %157, %159
  %160 = load i32, i32* %sign, align 4
  %mul178 = mul nsw i32 %sub177, %160
  store i32 %mul178, i32* %dx1172, align 4
  %161 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w1162, align 4
  %point180 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %161, i32 0, i32 7
  %y181 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point180, i32 0, i32 1
  %162 = load i32, i32* %y181, align 4
  %163 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %point182 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %163, i32 0, i32 7
  %y183 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point182, i32 0, i32 1
  %164 = load i32, i32* %y183, align 4
  %sub184 = sub nsw i32 %162, %164
  store i32 %sub184, i32* %dy1179, align 4
  %165 = load i32, i32* %dy1179, align 4
  %cmp185 = icmp sge i32 %165, 0
  br i1 %cmp185, label %land.lhs.true186, label %if.end201

land.lhs.true186:                                 ; preds = %if.then171
  %166 = load i32, i32* %dx1172, align 4
  %cmp187 = icmp eq i32 %166, 0
  br i1 %cmp187, label %if.then194, label %lor.lhs.false188

lor.lhs.false188:                                 ; preds = %land.lhs.true186
  %167 = load i32, i32* %dx1172, align 4
  %cmp189 = icmp slt i32 %167, 0
  br i1 %cmp189, label %land.lhs.true190, label %if.end201

land.lhs.true190:                                 ; preds = %lor.lhs.false188
  %168 = load i32, i32* %dy1179, align 4
  %169 = load i32, i32* %dx, align 4
  %mul191 = mul nsw i32 %168, %169
  %170 = load i32, i32* %dy156, align 4
  %171 = load i32, i32* %dx1172, align 4
  %mul192 = mul nsw i32 %170, %171
  %cmp193 = icmp sle i32 %mul191, %mul192
  br i1 %cmp193, label %if.then194, label %if.end201

if.then194:                                       ; preds = %land.lhs.true190, %land.lhs.true186
  %172 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w1162, align 4
  store %"class.btConvexHullInternal::Vertex"* %172, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %173 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %point195 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %173, i32 0, i32 7
  %x196 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point195, i32 0, i32 0
  %174 = load i32, i32* %x196, align 8
  %175 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %point197 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %175, i32 0, i32 7
  %x198 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point197, i32 0, i32 0
  %176 = load i32, i32* %x198, align 8
  %sub199 = sub nsw i32 %174, %176
  %177 = load i32, i32* %sign, align 4
  %mul200 = mul nsw i32 %sub199, %177
  store i32 %mul200, i32* %dx, align 4
  br label %while.body155

if.end201:                                        ; preds = %land.lhs.true190, %lor.lhs.false188, %if.then171
  br label %if.end202

if.end202:                                        ; preds = %if.end201, %cond.end168
  %178 = load i32, i32* %side, align 4
  %tobool204 = icmp ne i32 %178, 0
  br i1 %tobool204, label %cond.true205, label %cond.false207

cond.true205:                                     ; preds = %if.end202
  %179 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %prev206 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %179, i32 0, i32 1
  %180 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %prev206, align 4
  br label %cond.end209

cond.false207:                                    ; preds = %if.end202
  %181 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %next208 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %181, i32 0, i32 0
  %182 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %next208, align 8
  br label %cond.end209

cond.end209:                                      ; preds = %cond.false207, %cond.true205
  %cond210 = phi %"class.btConvexHullInternal::Vertex"* [ %180, %cond.true205 ], [ %182, %cond.false207 ]
  store %"class.btConvexHullInternal::Vertex"* %cond210, %"class.btConvexHullInternal::Vertex"** %w0203, align 4
  %183 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w0203, align 4
  %184 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %cmp211 = icmp ne %"class.btConvexHullInternal::Vertex"* %183, %184
  br i1 %cmp211, label %if.then212, label %if.end246

if.then212:                                       ; preds = %cond.end209
  %185 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w0203, align 4
  %point214 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %185, i32 0, i32 7
  %x215 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point214, i32 0, i32 0
  %186 = load i32, i32* %x215, align 8
  %187 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %point216 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %187, i32 0, i32 7
  %x217 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point216, i32 0, i32 0
  %188 = load i32, i32* %x217, align 8
  %sub218 = sub nsw i32 %186, %188
  %189 = load i32, i32* %sign, align 4
  %mul219 = mul nsw i32 %sub218, %189
  store i32 %mul219, i32* %dx0213, align 4
  %190 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w0203, align 4
  %point221 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %190, i32 0, i32 7
  %y222 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point221, i32 0, i32 1
  %191 = load i32, i32* %y222, align 4
  %192 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %point223 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %192, i32 0, i32 7
  %y224 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point223, i32 0, i32 1
  %193 = load i32, i32* %y224, align 4
  %sub225 = sub nsw i32 %191, %193
  store i32 %sub225, i32* %dy0220, align 4
  %194 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %point227 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %194, i32 0, i32 7
  %x228 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point227, i32 0, i32 0
  %195 = load i32, i32* %x228, align 8
  %196 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w0203, align 4
  %point229 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %196, i32 0, i32 7
  %x230 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point229, i32 0, i32 0
  %197 = load i32, i32* %x230, align 8
  %sub231 = sub nsw i32 %195, %197
  %198 = load i32, i32* %sign, align 4
  %mul232 = mul nsw i32 %sub231, %198
  store i32 %mul232, i32* %dxn226, align 4
  %199 = load i32, i32* %dxn226, align 4
  %cmp233 = icmp slt i32 %199, 0
  br i1 %cmp233, label %land.lhs.true234, label %if.end245

land.lhs.true234:                                 ; preds = %if.then212
  %200 = load i32, i32* %dy0220, align 4
  %cmp235 = icmp sgt i32 %200, 0
  br i1 %cmp235, label %land.lhs.true236, label %if.end245

land.lhs.true236:                                 ; preds = %land.lhs.true234
  %201 = load i32, i32* %dx0213, align 4
  %cmp237 = icmp eq i32 %201, 0
  br i1 %cmp237, label %if.then244, label %lor.lhs.false238

lor.lhs.false238:                                 ; preds = %land.lhs.true236
  %202 = load i32, i32* %dx0213, align 4
  %cmp239 = icmp slt i32 %202, 0
  br i1 %cmp239, label %land.lhs.true240, label %if.end245

land.lhs.true240:                                 ; preds = %lor.lhs.false238
  %203 = load i32, i32* %dy0220, align 4
  %204 = load i32, i32* %dx, align 4
  %mul241 = mul nsw i32 %203, %204
  %205 = load i32, i32* %dy156, align 4
  %206 = load i32, i32* %dx0213, align 4
  %mul242 = mul nsw i32 %205, %206
  %cmp243 = icmp slt i32 %mul241, %mul242
  br i1 %cmp243, label %if.then244, label %if.end245

if.then244:                                       ; preds = %land.lhs.true240, %land.lhs.true236
  %207 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w0203, align 4
  store %"class.btConvexHullInternal::Vertex"* %207, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %208 = load i32, i32* %dxn226, align 4
  store i32 %208, i32* %dx, align 4
  br label %while.body155

if.end245:                                        ; preds = %land.lhs.true240, %lor.lhs.false238, %land.lhs.true234, %if.then212
  br label %if.end246

if.end246:                                        ; preds = %if.end245, %cond.end209
  br label %while.end247

while.end247:                                     ; preds = %if.end246
  br label %if.end299

if.else248:                                       ; preds = %if.else152
  %209 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %point250 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %209, i32 0, i32 7
  %x251 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point250, i32 0, i32 0
  %210 = load i32, i32* %x251, align 8
  store i32 %210, i32* %x249, align 4
  %211 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %point252 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %211, i32 0, i32 7
  %y253 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point252, i32 0, i32 1
  %212 = load i32, i32* %y253, align 4
  store i32 %212, i32* %y0, align 4
  %213 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  store %"class.btConvexHullInternal::Vertex"* %213, %"class.btConvexHullInternal::Vertex"** %w0254, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body270, %if.else248
  %214 = load i32, i32* %side, align 4
  %tobool255 = icmp ne i32 %214, 0
  br i1 %tobool255, label %cond.true256, label %cond.false258

cond.true256:                                     ; preds = %while.cond
  %215 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w0254, align 4
  %next257 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %215, i32 0, i32 0
  %216 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %next257, align 8
  br label %cond.end260

cond.false258:                                    ; preds = %while.cond
  %217 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w0254, align 4
  %prev259 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %217, i32 0, i32 1
  %218 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %prev259, align 4
  br label %cond.end260

cond.end260:                                      ; preds = %cond.false258, %cond.true256
  %cond261 = phi %"class.btConvexHullInternal::Vertex"* [ %216, %cond.true256 ], [ %218, %cond.false258 ]
  store %"class.btConvexHullInternal::Vertex"* %cond261, %"class.btConvexHullInternal::Vertex"** %t, align 4
  %219 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %cmp262 = icmp ne %"class.btConvexHullInternal::Vertex"* %cond261, %219
  br i1 %cmp262, label %land.lhs.true263, label %land.end

land.lhs.true263:                                 ; preds = %cond.end260
  %220 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %t, align 4
  %point264 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %220, i32 0, i32 7
  %x265 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point264, i32 0, i32 0
  %221 = load i32, i32* %x265, align 8
  %222 = load i32, i32* %x249, align 4
  %cmp266 = icmp eq i32 %221, %222
  br i1 %cmp266, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true263
  %223 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %t, align 4
  %point267 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %223, i32 0, i32 7
  %y268 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point267, i32 0, i32 1
  %224 = load i32, i32* %y268, align 4
  %225 = load i32, i32* %y0, align 4
  %cmp269 = icmp sle i32 %224, %225
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true263, %cond.end260
  %226 = phi i1 [ false, %land.lhs.true263 ], [ false, %cond.end260 ], [ %cmp269, %land.rhs ]
  br i1 %226, label %while.body270, label %while.end273

while.body270:                                    ; preds = %land.end
  %227 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %t, align 4
  store %"class.btConvexHullInternal::Vertex"* %227, %"class.btConvexHullInternal::Vertex"** %w0254, align 4
  %228 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %t, align 4
  %point271 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %228, i32 0, i32 7
  %y272 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point271, i32 0, i32 1
  %229 = load i32, i32* %y272, align 4
  store i32 %229, i32* %y0, align 4
  br label %while.cond

while.end273:                                     ; preds = %land.end
  %230 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w0254, align 4
  store %"class.btConvexHullInternal::Vertex"* %230, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %231 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %point274 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %231, i32 0, i32 7
  %y275 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point274, i32 0, i32 1
  %232 = load i32, i32* %y275, align 4
  store i32 %232, i32* %y1, align 4
  %233 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  store %"class.btConvexHullInternal::Vertex"* %233, %"class.btConvexHullInternal::Vertex"** %w1276, align 4
  br label %while.cond277

while.cond277:                                    ; preds = %while.body295, %while.end273
  %234 = load i32, i32* %side, align 4
  %tobool278 = icmp ne i32 %234, 0
  br i1 %tobool278, label %cond.true279, label %cond.false281

cond.true279:                                     ; preds = %while.cond277
  %235 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w1276, align 4
  %prev280 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %235, i32 0, i32 1
  %236 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %prev280, align 4
  br label %cond.end283

cond.false281:                                    ; preds = %while.cond277
  %237 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w1276, align 4
  %next282 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %237, i32 0, i32 0
  %238 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %next282, align 8
  br label %cond.end283

cond.end283:                                      ; preds = %cond.false281, %cond.true279
  %cond284 = phi %"class.btConvexHullInternal::Vertex"* [ %236, %cond.true279 ], [ %238, %cond.false281 ]
  store %"class.btConvexHullInternal::Vertex"* %cond284, %"class.btConvexHullInternal::Vertex"** %t, align 4
  %239 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %cmp285 = icmp ne %"class.btConvexHullInternal::Vertex"* %cond284, %239
  br i1 %cmp285, label %land.lhs.true286, label %land.end294

land.lhs.true286:                                 ; preds = %cond.end283
  %240 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %t, align 4
  %point287 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %240, i32 0, i32 7
  %x288 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point287, i32 0, i32 0
  %241 = load i32, i32* %x288, align 8
  %242 = load i32, i32* %x249, align 4
  %cmp289 = icmp eq i32 %241, %242
  br i1 %cmp289, label %land.rhs290, label %land.end294

land.rhs290:                                      ; preds = %land.lhs.true286
  %243 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %t, align 4
  %point291 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %243, i32 0, i32 7
  %y292 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point291, i32 0, i32 1
  %244 = load i32, i32* %y292, align 4
  %245 = load i32, i32* %y1, align 4
  %cmp293 = icmp sge i32 %244, %245
  br label %land.end294

land.end294:                                      ; preds = %land.rhs290, %land.lhs.true286, %cond.end283
  %246 = phi i1 [ false, %land.lhs.true286 ], [ false, %cond.end283 ], [ %cmp293, %land.rhs290 ]
  br i1 %246, label %while.body295, label %while.end298

while.body295:                                    ; preds = %land.end294
  %247 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %t, align 4
  store %"class.btConvexHullInternal::Vertex"* %247, %"class.btConvexHullInternal::Vertex"** %w1276, align 4
  %248 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %t, align 4
  %point296 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %248, i32 0, i32 7
  %y297 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point296, i32 0, i32 1
  %249 = load i32, i32* %y297, align 4
  store i32 %249, i32* %y1, align 4
  br label %while.cond277

while.end298:                                     ; preds = %land.end294
  %250 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w1276, align 4
  store %"class.btConvexHullInternal::Vertex"* %250, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  br label %if.end299

if.end299:                                        ; preds = %while.end298, %while.end247
  br label %if.end300

if.end300:                                        ; preds = %if.end299, %while.end
  %251 = load i32, i32* %side, align 4
  %cmp301 = icmp eq i32 %251, 0
  br i1 %cmp301, label %if.then302, label %if.end305

if.then302:                                       ; preds = %if.end300
  %252 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  store %"class.btConvexHullInternal::Vertex"* %252, %"class.btConvexHullInternal::Vertex"** %v00, align 4
  %253 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  store %"class.btConvexHullInternal::Vertex"* %253, %"class.btConvexHullInternal::Vertex"** %v10, align 4
  %254 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h0.addr, align 4
  %minXy303 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %254, i32 0, i32 0
  %255 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %minXy303, align 4
  store %"class.btConvexHullInternal::Vertex"* %255, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %256 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h1.addr, align 4
  %minXy304 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %256, i32 0, i32 0
  %257 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %minXy304, align 4
  store %"class.btConvexHullInternal::Vertex"* %257, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  store i32 -1, i32* %sign, align 4
  br label %if.end305

if.end305:                                        ; preds = %if.then302, %if.end300
  br label %for.inc

for.inc:                                          ; preds = %if.end305
  %258 = load i32, i32* %side, align 4
  %inc = add nsw i32 %258, 1
  store i32 %inc, i32* %side, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %259 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %260 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %prev306 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %260, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* %259, %"class.btConvexHullInternal::Vertex"** %prev306, align 4
  %261 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v0, align 4
  %262 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v1, align 4
  %next307 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %262, i32 0, i32 0
  store %"class.btConvexHullInternal::Vertex"* %261, %"class.btConvexHullInternal::Vertex"** %next307, align 8
  %263 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v10, align 4
  %264 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v00, align 4
  %next308 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %264, i32 0, i32 0
  store %"class.btConvexHullInternal::Vertex"* %263, %"class.btConvexHullInternal::Vertex"** %next308, align 8
  %265 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v00, align 4
  %266 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v10, align 4
  %prev309 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %266, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* %265, %"class.btConvexHullInternal::Vertex"** %prev309, align 4
  %267 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h1.addr, align 4
  %minXy310 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %267, i32 0, i32 0
  %268 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %minXy310, align 4
  %point311 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %268, i32 0, i32 7
  %x312 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point311, i32 0, i32 0
  %269 = load i32, i32* %x312, align 8
  %270 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h0.addr, align 4
  %minXy313 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %270, i32 0, i32 0
  %271 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %minXy313, align 4
  %point314 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %271, i32 0, i32 7
  %x315 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point314, i32 0, i32 0
  %272 = load i32, i32* %x315, align 8
  %cmp316 = icmp slt i32 %269, %272
  br i1 %cmp316, label %if.then317, label %if.end320

if.then317:                                       ; preds = %for.end
  %273 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h1.addr, align 4
  %minXy318 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %273, i32 0, i32 0
  %274 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %minXy318, align 4
  %275 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h0.addr, align 4
  %minXy319 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %275, i32 0, i32 0
  store %"class.btConvexHullInternal::Vertex"* %274, %"class.btConvexHullInternal::Vertex"** %minXy319, align 4
  br label %if.end320

if.end320:                                        ; preds = %if.then317, %for.end
  %276 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h1.addr, align 4
  %maxXy321 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %276, i32 0, i32 1
  %277 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %maxXy321, align 4
  %point322 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %277, i32 0, i32 7
  %x323 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point322, i32 0, i32 0
  %278 = load i32, i32* %x323, align 8
  %279 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h0.addr, align 4
  %maxXy324 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %279, i32 0, i32 1
  %280 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %maxXy324, align 4
  %point325 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %280, i32 0, i32 7
  %x326 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point325, i32 0, i32 0
  %281 = load i32, i32* %x326, align 8
  %cmp327 = icmp sge i32 %278, %281
  br i1 %cmp327, label %if.then328, label %if.end331

if.then328:                                       ; preds = %if.end320
  %282 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h1.addr, align 4
  %maxXy329 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %282, i32 0, i32 1
  %283 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %maxXy329, align 4
  %284 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h0.addr, align 4
  %maxXy330 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %284, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* %283, %"class.btConvexHullInternal::Vertex"** %maxXy330, align 4
  br label %if.end331

if.end331:                                        ; preds = %if.then328, %if.end320
  %285 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h1.addr, align 4
  %maxYx332 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %285, i32 0, i32 3
  %286 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %maxYx332, align 4
  %287 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h0.addr, align 4
  %maxYx333 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %287, i32 0, i32 3
  store %"class.btConvexHullInternal::Vertex"* %286, %"class.btConvexHullInternal::Vertex"** %maxYx333, align 4
  %288 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v00, align 4
  %289 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %c0.addr, align 4
  store %"class.btConvexHullInternal::Vertex"* %288, %"class.btConvexHullInternal::Vertex"** %289, align 4
  %290 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v10, align 4
  %291 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %c1.addr, align 4
  store %"class.btConvexHullInternal::Vertex"* %290, %"class.btConvexHullInternal::Vertex"** %291, align 4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end331, %if.end
  %292 = load i1, i1* %retval, align 1
  ret i1 %292
}

; Function Attrs: noinline optnone
define hidden void @_ZN20btConvexHullInternal15computeInternalEiiRNS_16IntermediateHullE(%class.btConvexHullInternal* %this, i32 %start, i32 %end, %"class.btConvexHullInternal::IntermediateHull"* nonnull align 4 dereferenceable(16) %result) #2 {
entry:
  %this.addr = alloca %class.btConvexHullInternal*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %result.addr = alloca %"class.btConvexHullInternal::IntermediateHull"*, align 4
  %n = alloca i32, align 4
  %v = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %w = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %dx = alloca i32, align 4
  %dy = alloca i32, align 4
  %t = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %e = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %v56 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %split0 = alloca i32, align 4
  %p = alloca %"class.btConvexHullInternal::Point32", align 4
  %split1 = alloca i32, align 4
  %hull1 = alloca %"class.btConvexHullInternal::IntermediateHull", align 4
  store %class.btConvexHullInternal* %this, %class.btConvexHullInternal** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %"class.btConvexHullInternal::IntermediateHull"* %result, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %this1 = load %class.btConvexHullInternal*, %class.btConvexHullInternal** %this.addr, align 4
  %0 = load i32, i32* %end.addr, align 4
  %1 = load i32, i32* %start.addr, align 4
  %sub = sub nsw i32 %0, %1
  store i32 %sub, i32* %n, align 4
  %2 = load i32, i32* %n, align 4
  switch i32 %2, label %sw.epilog [
    i32 0, label %sw.bb
    i32 2, label %sw.bb2
    i32 1, label %sw.bb55
  ]

sw.bb:                                            ; preds = %entry
  %3 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %minXy = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %3, i32 0, i32 0
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %minXy, align 4
  %4 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %maxXy = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %4, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %maxXy, align 4
  %5 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %minYx = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %5, i32 0, i32 2
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %minYx, align 4
  %6 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %maxYx = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %6, i32 0, i32 3
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %maxYx, align 4
  br label %return

sw.bb2:                                           ; preds = %entry
  %originalVertices = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 5
  %7 = load i32, i32* %start.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.btConvexHullInternal::Vertex"** @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEixEi(%class.btAlignedObjectArray* %originalVertices, i32 %7)
  %8 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %call, align 4
  store %"class.btConvexHullInternal::Vertex"* %8, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %9 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %add.ptr = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %9, i32 1
  store %"class.btConvexHullInternal::Vertex"* %add.ptr, %"class.btConvexHullInternal::Vertex"** %w, align 4
  %10 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %point = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %10, i32 0, i32 7
  %11 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w, align 4
  %point3 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %11, i32 0, i32 7
  %call4 = call zeroext i1 @_ZNK20btConvexHullInternal7Point32neERKS0_(%"class.btConvexHullInternal::Point32"* %point, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %point3)
  br i1 %call4, label %if.then, label %if.end54

if.then:                                          ; preds = %sw.bb2
  %12 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %point5 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %12, i32 0, i32 7
  %x = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point5, i32 0, i32 0
  %13 = load i32, i32* %x, align 8
  %14 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w, align 4
  %point6 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %14, i32 0, i32 7
  %x7 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point6, i32 0, i32 0
  %15 = load i32, i32* %x7, align 8
  %sub8 = sub nsw i32 %13, %15
  store i32 %sub8, i32* %dx, align 4
  %16 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %point9 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %16, i32 0, i32 7
  %y = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point9, i32 0, i32 1
  %17 = load i32, i32* %y, align 4
  %18 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w, align 4
  %point10 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %18, i32 0, i32 7
  %y11 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point10, i32 0, i32 1
  %19 = load i32, i32* %y11, align 4
  %sub12 = sub nsw i32 %17, %19
  store i32 %sub12, i32* %dy, align 4
  %20 = load i32, i32* %dx, align 4
  %cmp = icmp eq i32 %20, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.then
  %21 = load i32, i32* %dy, align 4
  %cmp13 = icmp eq i32 %21, 0
  br i1 %cmp13, label %if.then14, label %if.else

if.then14:                                        ; preds = %land.lhs.true
  %22 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %point15 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %22, i32 0, i32 7
  %z = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point15, i32 0, i32 2
  %23 = load i32, i32* %z, align 8
  %24 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w, align 4
  %point16 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %24, i32 0, i32 7
  %z17 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point16, i32 0, i32 2
  %25 = load i32, i32* %z17, align 8
  %cmp18 = icmp sgt i32 %23, %25
  br i1 %cmp18, label %if.then19, label %if.end

if.then19:                                        ; preds = %if.then14
  %26 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w, align 4
  store %"class.btConvexHullInternal::Vertex"* %26, %"class.btConvexHullInternal::Vertex"** %t, align 4
  %27 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  store %"class.btConvexHullInternal::Vertex"* %27, %"class.btConvexHullInternal::Vertex"** %w, align 4
  %28 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %t, align 4
  store %"class.btConvexHullInternal::Vertex"* %28, %"class.btConvexHullInternal::Vertex"** %v, align 4
  br label %if.end

if.end:                                           ; preds = %if.then19, %if.then14
  %29 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %30 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %30, i32 0, i32 0
  store %"class.btConvexHullInternal::Vertex"* %29, %"class.btConvexHullInternal::Vertex"** %next, align 8
  %31 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %32 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %prev = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %32, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* %31, %"class.btConvexHullInternal::Vertex"** %prev, align 4
  %33 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %34 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %minXy20 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %34, i32 0, i32 0
  store %"class.btConvexHullInternal::Vertex"* %33, %"class.btConvexHullInternal::Vertex"** %minXy20, align 4
  %35 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %36 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %maxXy21 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %36, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* %35, %"class.btConvexHullInternal::Vertex"** %maxXy21, align 4
  %37 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %38 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %minYx22 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %38, i32 0, i32 2
  store %"class.btConvexHullInternal::Vertex"* %37, %"class.btConvexHullInternal::Vertex"** %minYx22, align 4
  %39 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %40 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %maxYx23 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %40, i32 0, i32 3
  store %"class.btConvexHullInternal::Vertex"* %39, %"class.btConvexHullInternal::Vertex"** %maxYx23, align 4
  br label %if.end51

if.else:                                          ; preds = %land.lhs.true, %if.then
  %41 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w, align 4
  %42 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %next24 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %42, i32 0, i32 0
  store %"class.btConvexHullInternal::Vertex"* %41, %"class.btConvexHullInternal::Vertex"** %next24, align 8
  %43 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w, align 4
  %44 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %prev25 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %44, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* %43, %"class.btConvexHullInternal::Vertex"** %prev25, align 4
  %45 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %46 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w, align 4
  %next26 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %46, i32 0, i32 0
  store %"class.btConvexHullInternal::Vertex"* %45, %"class.btConvexHullInternal::Vertex"** %next26, align 8
  %47 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %48 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w, align 4
  %prev27 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %48, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* %47, %"class.btConvexHullInternal::Vertex"** %prev27, align 4
  %49 = load i32, i32* %dx, align 4
  %cmp28 = icmp slt i32 %49, 0
  br i1 %cmp28, label %if.then32, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else
  %50 = load i32, i32* %dx, align 4
  %cmp29 = icmp eq i32 %50, 0
  br i1 %cmp29, label %land.lhs.true30, label %if.else35

land.lhs.true30:                                  ; preds = %lor.lhs.false
  %51 = load i32, i32* %dy, align 4
  %cmp31 = icmp slt i32 %51, 0
  br i1 %cmp31, label %if.then32, label %if.else35

if.then32:                                        ; preds = %land.lhs.true30, %if.else
  %52 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %53 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %minXy33 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %53, i32 0, i32 0
  store %"class.btConvexHullInternal::Vertex"* %52, %"class.btConvexHullInternal::Vertex"** %minXy33, align 4
  %54 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w, align 4
  %55 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %maxXy34 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %55, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* %54, %"class.btConvexHullInternal::Vertex"** %maxXy34, align 4
  br label %if.end38

if.else35:                                        ; preds = %land.lhs.true30, %lor.lhs.false
  %56 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w, align 4
  %57 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %minXy36 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %57, i32 0, i32 0
  store %"class.btConvexHullInternal::Vertex"* %56, %"class.btConvexHullInternal::Vertex"** %minXy36, align 4
  %58 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %59 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %maxXy37 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %59, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* %58, %"class.btConvexHullInternal::Vertex"** %maxXy37, align 4
  br label %if.end38

if.end38:                                         ; preds = %if.else35, %if.then32
  %60 = load i32, i32* %dy, align 4
  %cmp39 = icmp slt i32 %60, 0
  br i1 %cmp39, label %if.then44, label %lor.lhs.false40

lor.lhs.false40:                                  ; preds = %if.end38
  %61 = load i32, i32* %dy, align 4
  %cmp41 = icmp eq i32 %61, 0
  br i1 %cmp41, label %land.lhs.true42, label %if.else47

land.lhs.true42:                                  ; preds = %lor.lhs.false40
  %62 = load i32, i32* %dx, align 4
  %cmp43 = icmp slt i32 %62, 0
  br i1 %cmp43, label %if.then44, label %if.else47

if.then44:                                        ; preds = %land.lhs.true42, %if.end38
  %63 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %64 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %minYx45 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %64, i32 0, i32 2
  store %"class.btConvexHullInternal::Vertex"* %63, %"class.btConvexHullInternal::Vertex"** %minYx45, align 4
  %65 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w, align 4
  %66 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %maxYx46 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %66, i32 0, i32 3
  store %"class.btConvexHullInternal::Vertex"* %65, %"class.btConvexHullInternal::Vertex"** %maxYx46, align 4
  br label %if.end50

if.else47:                                        ; preds = %land.lhs.true42, %lor.lhs.false40
  %67 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w, align 4
  %68 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %minYx48 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %68, i32 0, i32 2
  store %"class.btConvexHullInternal::Vertex"* %67, %"class.btConvexHullInternal::Vertex"** %minYx48, align 4
  %69 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %70 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %maxYx49 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %70, i32 0, i32 3
  store %"class.btConvexHullInternal::Vertex"* %69, %"class.btConvexHullInternal::Vertex"** %maxYx49, align 4
  br label %if.end50

if.end50:                                         ; preds = %if.else47, %if.then44
  br label %if.end51

if.end51:                                         ; preds = %if.end50, %if.end
  %71 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %72 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w, align 4
  %call52 = call %"class.btConvexHullInternal::Edge"* @_ZN20btConvexHullInternal11newEdgePairEPNS_6VertexES1_(%class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::Vertex"* %71, %"class.btConvexHullInternal::Vertex"* %72)
  store %"class.btConvexHullInternal::Edge"* %call52, %"class.btConvexHullInternal::Edge"** %e, align 4
  %73 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %74 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %73, %"class.btConvexHullInternal::Edge"* %74)
  %75 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %76 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %edges = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %76, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* %75, %"class.btConvexHullInternal::Edge"** %edges, align 8
  %77 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %reverse = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %77, i32 0, i32 2
  %78 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse, align 4
  store %"class.btConvexHullInternal::Edge"* %78, %"class.btConvexHullInternal::Edge"** %e, align 4
  %79 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %80 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %79, %"class.btConvexHullInternal::Edge"* %80)
  %81 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %82 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %w, align 4
  %edges53 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %82, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* %81, %"class.btConvexHullInternal::Edge"** %edges53, align 8
  br label %return

if.end54:                                         ; preds = %sw.bb2
  br label %sw.bb55

sw.bb55:                                          ; preds = %entry, %if.end54
  %originalVertices57 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 5
  %83 = load i32, i32* %start.addr, align 4
  %call58 = call nonnull align 4 dereferenceable(4) %"class.btConvexHullInternal::Vertex"** @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEixEi(%class.btAlignedObjectArray* %originalVertices57, i32 %83)
  %84 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %call58, align 4
  store %"class.btConvexHullInternal::Vertex"* %84, %"class.btConvexHullInternal::Vertex"** %v56, align 4
  %85 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v56, align 4
  %edges59 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %85, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %edges59, align 8
  %86 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v56, align 4
  %87 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v56, align 4
  %next60 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %87, i32 0, i32 0
  store %"class.btConvexHullInternal::Vertex"* %86, %"class.btConvexHullInternal::Vertex"** %next60, align 8
  %88 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v56, align 4
  %89 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v56, align 4
  %prev61 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %89, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* %88, %"class.btConvexHullInternal::Vertex"** %prev61, align 4
  %90 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v56, align 4
  %91 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %minXy62 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %91, i32 0, i32 0
  store %"class.btConvexHullInternal::Vertex"* %90, %"class.btConvexHullInternal::Vertex"** %minXy62, align 4
  %92 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v56, align 4
  %93 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %maxXy63 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %93, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* %92, %"class.btConvexHullInternal::Vertex"** %maxXy63, align 4
  %94 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v56, align 4
  %95 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %minYx64 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %95, i32 0, i32 2
  store %"class.btConvexHullInternal::Vertex"* %94, %"class.btConvexHullInternal::Vertex"** %minYx64, align 4
  %96 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v56, align 4
  %97 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  %maxYx65 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %97, i32 0, i32 3
  store %"class.btConvexHullInternal::Vertex"* %96, %"class.btConvexHullInternal::Vertex"** %maxYx65, align 4
  br label %return

sw.epilog:                                        ; preds = %entry
  %98 = load i32, i32* %start.addr, align 4
  %99 = load i32, i32* %n, align 4
  %div = sdiv i32 %99, 2
  %add = add nsw i32 %98, %div
  store i32 %add, i32* %split0, align 4
  %originalVertices66 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 5
  %100 = load i32, i32* %split0, align 4
  %sub67 = sub nsw i32 %100, 1
  %call68 = call nonnull align 4 dereferenceable(4) %"class.btConvexHullInternal::Vertex"** @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEixEi(%class.btAlignedObjectArray* %originalVertices66, i32 %sub67)
  %101 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %call68, align 4
  %point69 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %101, i32 0, i32 7
  %102 = bitcast %"class.btConvexHullInternal::Point32"* %p to i8*
  %103 = bitcast %"class.btConvexHullInternal::Point32"* %point69 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %102, i8* align 8 %103, i32 16, i1 false)
  %104 = load i32, i32* %split0, align 4
  store i32 %104, i32* %split1, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %sw.epilog
  %105 = load i32, i32* %split1, align 4
  %106 = load i32, i32* %end.addr, align 4
  %cmp70 = icmp slt i32 %105, %106
  br i1 %cmp70, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %originalVertices71 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 5
  %107 = load i32, i32* %split1, align 4
  %call72 = call nonnull align 4 dereferenceable(4) %"class.btConvexHullInternal::Vertex"** @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEixEi(%class.btAlignedObjectArray* %originalVertices71, i32 %107)
  %108 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %call72, align 4
  %point73 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %108, i32 0, i32 7
  %call74 = call zeroext i1 @_ZNK20btConvexHullInternal7Point32eqERKS0_(%"class.btConvexHullInternal::Point32"* %point73, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %p)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %109 = phi i1 [ false, %while.cond ], [ %call74, %land.rhs ]
  br i1 %109, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %110 = load i32, i32* %split1, align 4
  %inc = add nsw i32 %110, 1
  store i32 %inc, i32* %split1, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  %111 = load i32, i32* %start.addr, align 4
  %112 = load i32, i32* %split0, align 4
  %113 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  call void @_ZN20btConvexHullInternal15computeInternalEiiRNS_16IntermediateHullE(%class.btConvexHullInternal* %this1, i32 %111, i32 %112, %"class.btConvexHullInternal::IntermediateHull"* nonnull align 4 dereferenceable(16) %113)
  %call75 = call %"class.btConvexHullInternal::IntermediateHull"* @_ZN20btConvexHullInternal16IntermediateHullC2Ev(%"class.btConvexHullInternal::IntermediateHull"* %hull1)
  %114 = load i32, i32* %split1, align 4
  %115 = load i32, i32* %end.addr, align 4
  call void @_ZN20btConvexHullInternal15computeInternalEiiRNS_16IntermediateHullE(%class.btConvexHullInternal* %this1, i32 %114, i32 %115, %"class.btConvexHullInternal::IntermediateHull"* nonnull align 4 dereferenceable(16) %hull1)
  %116 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %result.addr, align 4
  call void @_ZN20btConvexHullInternal5mergeERNS_16IntermediateHullES1_(%class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::IntermediateHull"* nonnull align 4 dereferenceable(16) %116, %"class.btConvexHullInternal::IntermediateHull"* nonnull align 4 dereferenceable(16) %hull1)
  br label %return

return:                                           ; preds = %while.end, %sw.bb55, %if.end51, %sw.bb
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.btConvexHullInternal::Vertex"** @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %0, i32 %1
  ret %"class.btConvexHullInternal::Vertex"** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK20btConvexHullInternal7Point32neERKS0_(%"class.btConvexHullInternal::Point32"* %this, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %b) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  store %"class.btConvexHullInternal::Point32"* %this, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  store %"class.btConvexHullInternal::Point32"* %b, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  %x = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 0
  %0 = load i32, i32* %x, align 4
  %1 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %x2 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %1, i32 0, i32 0
  %2 = load i32, i32* %x2, align 4
  %cmp = icmp ne i32 %0, %2
  br i1 %cmp, label %lor.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %y = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 1
  %3 = load i32, i32* %y, align 4
  %4 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %y3 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %4, i32 0, i32 1
  %5 = load i32, i32* %y3, align 4
  %cmp4 = icmp ne i32 %3, %5
  br i1 %cmp4, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %lor.lhs.false
  %z = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 2
  %6 = load i32, i32* %z, align 4
  %7 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %z5 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %7, i32 0, i32 2
  %8 = load i32, i32* %z5, align 4
  %cmp6 = icmp ne i32 %6, %8
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %lor.lhs.false, %entry
  %9 = phi i1 [ true, %lor.lhs.false ], [ true, %entry ], [ %cmp6, %lor.rhs ]
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %this, %"class.btConvexHullInternal::Edge"* %n) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %n.addr = alloca %"class.btConvexHullInternal::Edge"*, align 4
  store %"class.btConvexHullInternal::Edge"* %this, %"class.btConvexHullInternal::Edge"** %this.addr, align 4
  store %"class.btConvexHullInternal::Edge"* %n, %"class.btConvexHullInternal::Edge"** %n.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %this.addr, align 4
  %0 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %n.addr, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %this1, i32 0, i32 0
  store %"class.btConvexHullInternal::Edge"* %0, %"class.btConvexHullInternal::Edge"** %next, align 4
  %1 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %n.addr, align 4
  %prev = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %1, i32 0, i32 1
  store %"class.btConvexHullInternal::Edge"* %this1, %"class.btConvexHullInternal::Edge"** %prev, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK20btConvexHullInternal7Point32eqERKS0_(%"class.btConvexHullInternal::Point32"* %this, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %b) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  store %"class.btConvexHullInternal::Point32"* %this, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  store %"class.btConvexHullInternal::Point32"* %b, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  %x = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 0
  %0 = load i32, i32* %x, align 4
  %1 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %x2 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %1, i32 0, i32 0
  %2 = load i32, i32* %x2, align 4
  %cmp = icmp eq i32 %0, %2
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %y = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 1
  %3 = load i32, i32* %y, align 4
  %4 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %y3 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %4, i32 0, i32 1
  %5 = load i32, i32* %y3, align 4
  %cmp4 = icmp eq i32 %3, %5
  br i1 %cmp4, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true
  %z = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 2
  %6 = load i32, i32* %z, align 4
  %7 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %z5 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %7, i32 0, i32 2
  %8 = load i32, i32* %z5, align 4
  %cmp6 = icmp eq i32 %6, %8
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true, %entry
  %9 = phi i1 [ false, %land.lhs.true ], [ false, %entry ], [ %cmp6, %land.rhs ]
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::IntermediateHull"* @_ZN20btConvexHullInternal16IntermediateHullC2Ev(%"class.btConvexHullInternal::IntermediateHull"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::IntermediateHull"*, align 4
  store %"class.btConvexHullInternal::IntermediateHull"* %this, %"class.btConvexHullInternal::IntermediateHull"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %this.addr, align 4
  %minXy = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %this1, i32 0, i32 0
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %minXy, align 4
  %maxXy = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %this1, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %maxXy, align 4
  %minYx = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %this1, i32 0, i32 2
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %minYx, align 4
  %maxYx = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %this1, i32 0, i32 3
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %maxYx, align 4
  ret %"class.btConvexHullInternal::IntermediateHull"* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN20btConvexHullInternal5mergeERNS_16IntermediateHullES1_(%class.btConvexHullInternal* %this, %"class.btConvexHullInternal::IntermediateHull"* nonnull align 4 dereferenceable(16) %h0, %"class.btConvexHullInternal::IntermediateHull"* nonnull align 4 dereferenceable(16) %h1) #2 {
entry:
  %this.addr = alloca %class.btConvexHullInternal*, align 4
  %h0.addr = alloca %"class.btConvexHullInternal::IntermediateHull"*, align 4
  %h1.addr = alloca %"class.btConvexHullInternal::IntermediateHull"*, align 4
  %c0 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %toPrev0 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %firstNew0 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %pendingHead0 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %pendingTail0 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %c1 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %toPrev1 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %firstNew1 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %pendingHead1 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %pendingTail1 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %prevPoint = alloca %"class.btConvexHullInternal::Point32", align 4
  %s = alloca %"class.btConvexHullInternal::Point32", align 4
  %normal = alloca %"class.btConvexHullInternal::Point64", align 8
  %ref.tmp = alloca %"class.btConvexHullInternal::Point32", align 4
  %t = alloca %"class.btConvexHullInternal::Point64", align 8
  %e = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %start0 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %dot = alloca i64, align 8
  %ref.tmp11 = alloca %"class.btConvexHullInternal::Point32", align 4
  %ref.tmp13 = alloca %"class.btConvexHullInternal::Point32", align 4
  %ref.tmp19 = alloca %"class.btConvexHullInternal::Point32", align 4
  %start1 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %dot33 = alloca i64, align 8
  %ref.tmp34 = alloca %"class.btConvexHullInternal::Point32", align 4
  %ref.tmp39 = alloca %"class.btConvexHullInternal::Point32", align 4
  %ref.tmp47 = alloca %"class.btConvexHullInternal::Point32", align 4
  %first0 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %first1 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %firstRun = alloca i8, align 1
  %s76 = alloca %"class.btConvexHullInternal::Point32", align 4
  %r = alloca %"class.btConvexHullInternal::Point32", align 4
  %rxs = alloca %"class.btConvexHullInternal::Point64", align 8
  %sxrxs = alloca %"class.btConvexHullInternal::Point64", align 8
  %minCot0 = alloca %"class.btConvexHullInternal::Rational64", align 8
  %min0 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %minCot1 = alloca %"class.btConvexHullInternal::Rational64", align 8
  %min1 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %e85 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %cmp90 = alloca i32, align 4
  %e106 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %e0 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %e1 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %e130 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %n = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %e157 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %n159 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %e192 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %n194 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %e209 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %n211 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  store %class.btConvexHullInternal* %this, %class.btConvexHullInternal** %this.addr, align 4
  store %"class.btConvexHullInternal::IntermediateHull"* %h0, %"class.btConvexHullInternal::IntermediateHull"** %h0.addr, align 4
  store %"class.btConvexHullInternal::IntermediateHull"* %h1, %"class.btConvexHullInternal::IntermediateHull"** %h1.addr, align 4
  %this1 = load %class.btConvexHullInternal*, %class.btConvexHullInternal** %this.addr, align 4
  %0 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h1.addr, align 4
  %maxXy = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %0, i32 0, i32 1
  %1 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %maxXy, align 4
  %tobool = icmp ne %"class.btConvexHullInternal::Vertex"* %1, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h0.addr, align 4
  %maxXy2 = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %2, i32 0, i32 1
  %3 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %maxXy2, align 4
  %tobool3 = icmp ne %"class.btConvexHullInternal::Vertex"* %3, null
  br i1 %tobool3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %if.end
  %4 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h1.addr, align 4
  %5 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h0.addr, align 4
  %6 = bitcast %"class.btConvexHullInternal::IntermediateHull"* %5 to i8*
  %7 = bitcast %"class.btConvexHullInternal::IntermediateHull"* %4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  br label %return

if.end5:                                          ; preds = %if.end
  %mergeStamp = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 6
  %8 = load i32, i32* %mergeStamp, align 4
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %mergeStamp, align 4
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %toPrev0, align 4
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %firstNew0, align 4
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %pendingHead0, align 4
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %pendingTail0, align 4
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %toPrev1, align 4
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %firstNew1, align 4
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %pendingHead1, align 4
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %pendingTail1, align 4
  %call = call %"class.btConvexHullInternal::Point32"* @_ZN20btConvexHullInternal7Point32C2Ev(%"class.btConvexHullInternal::Point32"* %prevPoint)
  %9 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h0.addr, align 4
  %10 = load %"class.btConvexHullInternal::IntermediateHull"*, %"class.btConvexHullInternal::IntermediateHull"** %h1.addr, align 4
  %call6 = call zeroext i1 @_ZN20btConvexHullInternal15mergeProjectionERNS_16IntermediateHullES1_RPNS_6VertexES4_(%class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::IntermediateHull"* nonnull align 4 dereferenceable(16) %9, %"class.btConvexHullInternal::IntermediateHull"* nonnull align 4 dereferenceable(16) %10, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %c0, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %c1)
  br i1 %call6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.end5
  %11 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  %12 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  call void @_ZNK20btConvexHullInternal6VertexmiERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %s, %"class.btConvexHullInternal::Vertex"* %11, %"class.btConvexHullInternal::Vertex"* nonnull align 8 dereferenceable(108) %12)
  %call8 = call %"class.btConvexHullInternal::Point32"* @_ZN20btConvexHullInternal7Point32C2Eiii(%"class.btConvexHullInternal::Point32"* %ref.tmp, i32 0, i32 0, i32 -1)
  call void @_ZNK20btConvexHullInternal7Point325crossERKS0_(%"class.btConvexHullInternal::Point64"* sret align 8 %normal, %"class.btConvexHullInternal::Point32"* %ref.tmp, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %s)
  call void @_ZNK20btConvexHullInternal7Point325crossERKNS_7Point64E(%"class.btConvexHullInternal::Point64"* sret align 8 %t, %"class.btConvexHullInternal::Point32"* %s, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %normal)
  %13 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  %edges = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %13, i32 0, i32 2
  %14 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edges, align 8
  store %"class.btConvexHullInternal::Edge"* %14, %"class.btConvexHullInternal::Edge"** %e, align 4
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %start0, align 4
  %15 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %tobool9 = icmp ne %"class.btConvexHullInternal::Edge"* %15, null
  br i1 %tobool9, label %if.then10, label %if.end28

if.then10:                                        ; preds = %if.then7
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then10
  %16 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %target = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %16, i32 0, i32 3
  %17 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target, align 4
  %18 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  call void @_ZNK20btConvexHullInternal6VertexmiERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp11, %"class.btConvexHullInternal::Vertex"* %17, %"class.btConvexHullInternal::Vertex"* nonnull align 8 dereferenceable(108) %18)
  %call12 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %ref.tmp11, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %normal)
  store i64 %call12, i64* %dot, align 8
  %19 = load i64, i64* %dot, align 8
  %cmp = icmp eq i64 %19, 0
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %do.body
  %20 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %target14 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %20, i32 0, i32 3
  %21 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target14, align 4
  %22 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  call void @_ZNK20btConvexHullInternal6VertexmiERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp13, %"class.btConvexHullInternal::Vertex"* %21, %"class.btConvexHullInternal::Vertex"* nonnull align 8 dereferenceable(108) %22)
  %call15 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %ref.tmp13, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %t)
  %cmp16 = icmp sgt i64 %call15, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %do.body
  %23 = phi i1 [ false, %do.body ], [ %cmp16, %land.rhs ]
  br i1 %23, label %if.then17, label %if.end25

if.then17:                                        ; preds = %land.end
  %24 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %start0, align 4
  %tobool18 = icmp ne %"class.btConvexHullInternal::Edge"* %24, null
  br i1 %tobool18, label %lor.rhs, label %lor.end

lor.rhs:                                          ; preds = %if.then17
  %25 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %start0, align 4
  %26 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %call20 = call %"class.btConvexHullInternal::Point32"* @_ZN20btConvexHullInternal7Point32C2Eiii(%"class.btConvexHullInternal::Point32"* %ref.tmp19, i32 0, i32 0, i32 -1)
  %call21 = call i32 @_ZN20btConvexHullInternal14getOrientationEPKNS_4EdgeES2_RKNS_7Point32ES5_(%"class.btConvexHullInternal::Edge"* %25, %"class.btConvexHullInternal::Edge"* %26, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %s, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %ref.tmp19)
  %cmp22 = icmp eq i32 %call21, 1
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %if.then17
  %27 = phi i1 [ true, %if.then17 ], [ %cmp22, %lor.rhs ]
  br i1 %27, label %if.then23, label %if.end24

if.then23:                                        ; preds = %lor.end
  %28 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  store %"class.btConvexHullInternal::Edge"* %28, %"class.btConvexHullInternal::Edge"** %start0, align 4
  br label %if.end24

if.end24:                                         ; preds = %if.then23, %lor.end
  br label %if.end25

if.end25:                                         ; preds = %if.end24, %land.end
  %29 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %29, i32 0, i32 0
  %30 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next, align 4
  store %"class.btConvexHullInternal::Edge"* %30, %"class.btConvexHullInternal::Edge"** %e, align 4
  br label %do.cond

do.cond:                                          ; preds = %if.end25
  %31 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %32 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  %edges26 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %32, i32 0, i32 2
  %33 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edges26, align 8
  %cmp27 = icmp ne %"class.btConvexHullInternal::Edge"* %31, %33
  br i1 %cmp27, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  br label %if.end28

if.end28:                                         ; preds = %do.end, %if.then7
  %34 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  %edges29 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %34, i32 0, i32 2
  %35 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edges29, align 8
  store %"class.btConvexHullInternal::Edge"* %35, %"class.btConvexHullInternal::Edge"** %e, align 4
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %start1, align 4
  %36 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %tobool30 = icmp ne %"class.btConvexHullInternal::Edge"* %36, null
  br i1 %tobool30, label %if.then31, label %if.end60

if.then31:                                        ; preds = %if.end28
  br label %do.body32

do.body32:                                        ; preds = %do.cond56, %if.then31
  %37 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %target35 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %37, i32 0, i32 3
  %38 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target35, align 4
  %39 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  call void @_ZNK20btConvexHullInternal6VertexmiERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp34, %"class.btConvexHullInternal::Vertex"* %38, %"class.btConvexHullInternal::Vertex"* nonnull align 8 dereferenceable(108) %39)
  %call36 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %ref.tmp34, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %normal)
  store i64 %call36, i64* %dot33, align 8
  %40 = load i64, i64* %dot33, align 8
  %cmp37 = icmp eq i64 %40, 0
  br i1 %cmp37, label %land.rhs38, label %land.end43

land.rhs38:                                       ; preds = %do.body32
  %41 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %target40 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %41, i32 0, i32 3
  %42 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target40, align 4
  %43 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  call void @_ZNK20btConvexHullInternal6VertexmiERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp39, %"class.btConvexHullInternal::Vertex"* %42, %"class.btConvexHullInternal::Vertex"* nonnull align 8 dereferenceable(108) %43)
  %call41 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %ref.tmp39, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %t)
  %cmp42 = icmp sgt i64 %call41, 0
  br label %land.end43

land.end43:                                       ; preds = %land.rhs38, %do.body32
  %44 = phi i1 [ false, %do.body32 ], [ %cmp42, %land.rhs38 ]
  br i1 %44, label %if.then44, label %if.end54

if.then44:                                        ; preds = %land.end43
  %45 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %start1, align 4
  %tobool45 = icmp ne %"class.btConvexHullInternal::Edge"* %45, null
  br i1 %tobool45, label %lor.rhs46, label %lor.end51

lor.rhs46:                                        ; preds = %if.then44
  %46 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %start1, align 4
  %47 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %call48 = call %"class.btConvexHullInternal::Point32"* @_ZN20btConvexHullInternal7Point32C2Eiii(%"class.btConvexHullInternal::Point32"* %ref.tmp47, i32 0, i32 0, i32 -1)
  %call49 = call i32 @_ZN20btConvexHullInternal14getOrientationEPKNS_4EdgeES2_RKNS_7Point32ES5_(%"class.btConvexHullInternal::Edge"* %46, %"class.btConvexHullInternal::Edge"* %47, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %s, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %ref.tmp47)
  %cmp50 = icmp eq i32 %call49, 2
  br label %lor.end51

lor.end51:                                        ; preds = %lor.rhs46, %if.then44
  %48 = phi i1 [ true, %if.then44 ], [ %cmp50, %lor.rhs46 ]
  br i1 %48, label %if.then52, label %if.end53

if.then52:                                        ; preds = %lor.end51
  %49 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  store %"class.btConvexHullInternal::Edge"* %49, %"class.btConvexHullInternal::Edge"** %start1, align 4
  br label %if.end53

if.end53:                                         ; preds = %if.then52, %lor.end51
  br label %if.end54

if.end54:                                         ; preds = %if.end53, %land.end43
  %50 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %next55 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %50, i32 0, i32 0
  %51 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next55, align 4
  store %"class.btConvexHullInternal::Edge"* %51, %"class.btConvexHullInternal::Edge"** %e, align 4
  br label %do.cond56

do.cond56:                                        ; preds = %if.end54
  %52 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %53 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  %edges57 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %53, i32 0, i32 2
  %54 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edges57, align 8
  %cmp58 = icmp ne %"class.btConvexHullInternal::Edge"* %52, %54
  br i1 %cmp58, label %do.body32, label %do.end59

do.end59:                                         ; preds = %do.cond56
  br label %if.end60

if.end60:                                         ; preds = %do.end59, %if.end28
  %55 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %start0, align 4
  %tobool61 = icmp ne %"class.btConvexHullInternal::Edge"* %55, null
  br i1 %tobool61, label %if.then63, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end60
  %56 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %start1, align 4
  %tobool62 = icmp ne %"class.btConvexHullInternal::Edge"* %56, null
  br i1 %tobool62, label %if.then63, label %if.end72

if.then63:                                        ; preds = %lor.lhs.false, %if.end60
  %57 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  %58 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  call void @_ZN20btConvexHullInternal24findEdgeForCoplanarFacesEPNS_6VertexES1_RPNS_4EdgeES4_S1_S1_(%class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::Vertex"* %57, %"class.btConvexHullInternal::Vertex"* %58, %"class.btConvexHullInternal::Edge"** nonnull align 4 dereferenceable(4) %start0, %"class.btConvexHullInternal::Edge"** nonnull align 4 dereferenceable(4) %start1, %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"* null)
  %59 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %start0, align 4
  %tobool64 = icmp ne %"class.btConvexHullInternal::Edge"* %59, null
  br i1 %tobool64, label %if.then65, label %if.end67

if.then65:                                        ; preds = %if.then63
  %60 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %start0, align 4
  %target66 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %60, i32 0, i32 3
  %61 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target66, align 4
  store %"class.btConvexHullInternal::Vertex"* %61, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  br label %if.end67

if.end67:                                         ; preds = %if.then65, %if.then63
  %62 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %start1, align 4
  %tobool68 = icmp ne %"class.btConvexHullInternal::Edge"* %62, null
  br i1 %tobool68, label %if.then69, label %if.end71

if.then69:                                        ; preds = %if.end67
  %63 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %start1, align 4
  %target70 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %63, i32 0, i32 3
  %64 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target70, align 4
  store %"class.btConvexHullInternal::Vertex"* %64, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  br label %if.end71

if.end71:                                         ; preds = %if.then69, %if.end67
  br label %if.end72

if.end72:                                         ; preds = %if.end71, %lor.lhs.false
  %65 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  %point = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %65, i32 0, i32 7
  %66 = bitcast %"class.btConvexHullInternal::Point32"* %prevPoint to i8*
  %67 = bitcast %"class.btConvexHullInternal::Point32"* %point to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %66, i8* align 8 %67, i32 16, i1 false)
  %z = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %prevPoint, i32 0, i32 2
  %68 = load i32, i32* %z, align 4
  %inc = add nsw i32 %68, 1
  store i32 %inc, i32* %z, align 4
  br label %if.end75

if.else:                                          ; preds = %if.end5
  %69 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  %point73 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %69, i32 0, i32 7
  %70 = bitcast %"class.btConvexHullInternal::Point32"* %prevPoint to i8*
  %71 = bitcast %"class.btConvexHullInternal::Point32"* %point73 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %70, i8* align 8 %71, i32 16, i1 false)
  %x = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %prevPoint, i32 0, i32 0
  %72 = load i32, i32* %x, align 4
  %inc74 = add nsw i32 %72, 1
  store i32 %inc74, i32* %x, align 4
  br label %if.end75

if.end75:                                         ; preds = %if.else, %if.end72
  %73 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  store %"class.btConvexHullInternal::Vertex"* %73, %"class.btConvexHullInternal::Vertex"** %first0, align 4
  %74 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  store %"class.btConvexHullInternal::Vertex"* %74, %"class.btConvexHullInternal::Vertex"** %first1, align 4
  store i8 1, i8* %firstRun, align 1
  br label %while.body

while.body:                                       ; preds = %if.end75, %if.end222
  %75 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  %76 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  call void @_ZNK20btConvexHullInternal6VertexmiERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %s76, %"class.btConvexHullInternal::Vertex"* %75, %"class.btConvexHullInternal::Vertex"* nonnull align 8 dereferenceable(108) %76)
  %77 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  %point77 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %77, i32 0, i32 7
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %r, %"class.btConvexHullInternal::Point32"* %prevPoint, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %point77)
  call void @_ZNK20btConvexHullInternal7Point325crossERKS0_(%"class.btConvexHullInternal::Point64"* sret align 8 %rxs, %"class.btConvexHullInternal::Point32"* %r, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %s76)
  call void @_ZNK20btConvexHullInternal7Point325crossERKNS_7Point64E(%"class.btConvexHullInternal::Point64"* sret align 8 %sxrxs, %"class.btConvexHullInternal::Point32"* %s76, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %rxs)
  %call78 = call %"class.btConvexHullInternal::Rational64"* @_ZN20btConvexHullInternal10Rational64C2Exx(%"class.btConvexHullInternal::Rational64"* %minCot0, i64 0, i64 0)
  %78 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  %call79 = call %"class.btConvexHullInternal::Edge"* @_ZN20btConvexHullInternal12findMaxAngleEbPKNS_6VertexERKNS_7Point32ERKNS_7Point64ES8_RNS_10Rational64E(%class.btConvexHullInternal* %this1, i1 zeroext false, %"class.btConvexHullInternal::Vertex"* %78, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %s76, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %rxs, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %sxrxs, %"class.btConvexHullInternal::Rational64"* nonnull align 8 dereferenceable(20) %minCot0)
  store %"class.btConvexHullInternal::Edge"* %call79, %"class.btConvexHullInternal::Edge"** %min0, align 4
  %call80 = call %"class.btConvexHullInternal::Rational64"* @_ZN20btConvexHullInternal10Rational64C2Exx(%"class.btConvexHullInternal::Rational64"* %minCot1, i64 0, i64 0)
  %79 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  %call81 = call %"class.btConvexHullInternal::Edge"* @_ZN20btConvexHullInternal12findMaxAngleEbPKNS_6VertexERKNS_7Point32ERKNS_7Point64ES8_RNS_10Rational64E(%class.btConvexHullInternal* %this1, i1 zeroext true, %"class.btConvexHullInternal::Vertex"* %79, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %s76, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %rxs, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %sxrxs, %"class.btConvexHullInternal::Rational64"* nonnull align 8 dereferenceable(20) %minCot1)
  store %"class.btConvexHullInternal::Edge"* %call81, %"class.btConvexHullInternal::Edge"** %min1, align 4
  %80 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %min0, align 4
  %tobool82 = icmp ne %"class.btConvexHullInternal::Edge"* %80, null
  br i1 %tobool82, label %if.else89, label %land.lhs.true

land.lhs.true:                                    ; preds = %while.body
  %81 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %min1, align 4
  %tobool83 = icmp ne %"class.btConvexHullInternal::Edge"* %81, null
  br i1 %tobool83, label %if.else89, label %if.then84

if.then84:                                        ; preds = %land.lhs.true
  %82 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  %83 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  %call86 = call %"class.btConvexHullInternal::Edge"* @_ZN20btConvexHullInternal11newEdgePairEPNS_6VertexES1_(%class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::Vertex"* %82, %"class.btConvexHullInternal::Vertex"* %83)
  store %"class.btConvexHullInternal::Edge"* %call86, %"class.btConvexHullInternal::Edge"** %e85, align 4
  %84 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e85, align 4
  %85 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e85, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %84, %"class.btConvexHullInternal::Edge"* %85)
  %86 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e85, align 4
  %87 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  %edges87 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %87, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* %86, %"class.btConvexHullInternal::Edge"** %edges87, align 8
  %88 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e85, align 4
  %reverse = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %88, i32 0, i32 2
  %89 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse, align 4
  store %"class.btConvexHullInternal::Edge"* %89, %"class.btConvexHullInternal::Edge"** %e85, align 4
  %90 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e85, align 4
  %91 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e85, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %90, %"class.btConvexHullInternal::Edge"* %91)
  %92 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e85, align 4
  %93 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  %edges88 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %93, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* %92, %"class.btConvexHullInternal::Edge"** %edges88, align 8
  br label %return

if.else89:                                        ; preds = %land.lhs.true, %while.body
  %94 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %min0, align 4
  %tobool91 = icmp ne %"class.btConvexHullInternal::Edge"* %94, null
  br i1 %tobool91, label %cond.false, label %cond.true

cond.true:                                        ; preds = %if.else89
  br label %cond.end96

cond.false:                                       ; preds = %if.else89
  %95 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %min1, align 4
  %tobool92 = icmp ne %"class.btConvexHullInternal::Edge"* %95, null
  br i1 %tobool92, label %cond.false94, label %cond.true93

cond.true93:                                      ; preds = %cond.false
  br label %cond.end

cond.false94:                                     ; preds = %cond.false
  %call95 = call i32 @_ZNK20btConvexHullInternal10Rational647compareERKS0_(%"class.btConvexHullInternal::Rational64"* %minCot0, %"class.btConvexHullInternal::Rational64"* nonnull align 8 dereferenceable(20) %minCot1)
  br label %cond.end

cond.end:                                         ; preds = %cond.false94, %cond.true93
  %cond = phi i32 [ -1, %cond.true93 ], [ %call95, %cond.false94 ]
  br label %cond.end96

cond.end96:                                       ; preds = %cond.end, %cond.true
  %cond97 = phi i32 [ 1, %cond.true ], [ %cond, %cond.end ]
  store i32 %cond97, i32* %cmp90, align 4
  %96 = load i8, i8* %firstRun, align 1
  %tobool98 = trunc i8 %96 to i1
  br i1 %tobool98, label %if.then105, label %lor.lhs.false99

lor.lhs.false99:                                  ; preds = %cond.end96
  %97 = load i32, i32* %cmp90, align 4
  %cmp100 = icmp sge i32 %97, 0
  br i1 %cmp100, label %cond.true101, label %cond.false103

cond.true101:                                     ; preds = %lor.lhs.false99
  %call102 = call zeroext i1 @_ZNK20btConvexHullInternal10Rational6418isNegativeInfinityEv(%"class.btConvexHullInternal::Rational64"* %minCot1)
  br i1 %call102, label %if.end120, label %if.then105

cond.false103:                                    ; preds = %lor.lhs.false99
  %call104 = call zeroext i1 @_ZNK20btConvexHullInternal10Rational6418isNegativeInfinityEv(%"class.btConvexHullInternal::Rational64"* %minCot0)
  br i1 %call104, label %if.end120, label %if.then105

if.then105:                                       ; preds = %cond.false103, %cond.true101, %cond.end96
  %98 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  %99 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  %call107 = call %"class.btConvexHullInternal::Edge"* @_ZN20btConvexHullInternal11newEdgePairEPNS_6VertexES1_(%class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::Vertex"* %98, %"class.btConvexHullInternal::Vertex"* %99)
  store %"class.btConvexHullInternal::Edge"* %call107, %"class.btConvexHullInternal::Edge"** %e106, align 4
  %100 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingTail0, align 4
  %tobool108 = icmp ne %"class.btConvexHullInternal::Edge"* %100, null
  br i1 %tobool108, label %if.then109, label %if.else110

if.then109:                                       ; preds = %if.then105
  %101 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e106, align 4
  %102 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingTail0, align 4
  %prev = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %102, i32 0, i32 1
  store %"class.btConvexHullInternal::Edge"* %101, %"class.btConvexHullInternal::Edge"** %prev, align 4
  br label %if.end111

if.else110:                                       ; preds = %if.then105
  %103 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e106, align 4
  store %"class.btConvexHullInternal::Edge"* %103, %"class.btConvexHullInternal::Edge"** %pendingHead0, align 4
  br label %if.end111

if.end111:                                        ; preds = %if.else110, %if.then109
  %104 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingTail0, align 4
  %105 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e106, align 4
  %next112 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %105, i32 0, i32 0
  store %"class.btConvexHullInternal::Edge"* %104, %"class.btConvexHullInternal::Edge"** %next112, align 4
  %106 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e106, align 4
  store %"class.btConvexHullInternal::Edge"* %106, %"class.btConvexHullInternal::Edge"** %pendingTail0, align 4
  %107 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e106, align 4
  %reverse113 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %107, i32 0, i32 2
  %108 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse113, align 4
  store %"class.btConvexHullInternal::Edge"* %108, %"class.btConvexHullInternal::Edge"** %e106, align 4
  %109 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingTail1, align 4
  %tobool114 = icmp ne %"class.btConvexHullInternal::Edge"* %109, null
  br i1 %tobool114, label %if.then115, label %if.else117

if.then115:                                       ; preds = %if.end111
  %110 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e106, align 4
  %111 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingTail1, align 4
  %next116 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %111, i32 0, i32 0
  store %"class.btConvexHullInternal::Edge"* %110, %"class.btConvexHullInternal::Edge"** %next116, align 4
  br label %if.end118

if.else117:                                       ; preds = %if.end111
  %112 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e106, align 4
  store %"class.btConvexHullInternal::Edge"* %112, %"class.btConvexHullInternal::Edge"** %pendingHead1, align 4
  br label %if.end118

if.end118:                                        ; preds = %if.else117, %if.then115
  %113 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingTail1, align 4
  %114 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e106, align 4
  %prev119 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %114, i32 0, i32 1
  store %"class.btConvexHullInternal::Edge"* %113, %"class.btConvexHullInternal::Edge"** %prev119, align 4
  %115 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e106, align 4
  store %"class.btConvexHullInternal::Edge"* %115, %"class.btConvexHullInternal::Edge"** %pendingTail1, align 4
  br label %if.end120

if.end120:                                        ; preds = %if.end118, %cond.false103, %cond.true101
  %116 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %min0, align 4
  store %"class.btConvexHullInternal::Edge"* %116, %"class.btConvexHullInternal::Edge"** %e0, align 4
  %117 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %min1, align 4
  store %"class.btConvexHullInternal::Edge"* %117, %"class.btConvexHullInternal::Edge"** %e1, align 4
  %118 = load i32, i32* %cmp90, align 4
  %cmp121 = icmp eq i32 %118, 0
  br i1 %cmp121, label %if.then122, label %if.end123

if.then122:                                       ; preds = %if.end120
  %119 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  %120 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  call void @_ZN20btConvexHullInternal24findEdgeForCoplanarFacesEPNS_6VertexES1_RPNS_4EdgeES4_S1_S1_(%class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::Vertex"* %119, %"class.btConvexHullInternal::Vertex"* %120, %"class.btConvexHullInternal::Edge"** nonnull align 4 dereferenceable(4) %e0, %"class.btConvexHullInternal::Edge"** nonnull align 4 dereferenceable(4) %e1, %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"* null)
  br label %if.end123

if.end123:                                        ; preds = %if.then122, %if.end120
  %121 = load i32, i32* %cmp90, align 4
  %cmp124 = icmp sge i32 %121, 0
  br i1 %cmp124, label %land.lhs.true125, label %if.end150

land.lhs.true125:                                 ; preds = %if.end123
  %122 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e1, align 4
  %tobool126 = icmp ne %"class.btConvexHullInternal::Edge"* %122, null
  br i1 %tobool126, label %if.then127, label %if.end150

if.then127:                                       ; preds = %land.lhs.true125
  %123 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %toPrev1, align 4
  %tobool128 = icmp ne %"class.btConvexHullInternal::Edge"* %123, null
  br i1 %tobool128, label %if.then129, label %if.end134

if.then129:                                       ; preds = %if.then127
  %124 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %toPrev1, align 4
  %next131 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %124, i32 0, i32 0
  %125 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next131, align 4
  store %"class.btConvexHullInternal::Edge"* %125, %"class.btConvexHullInternal::Edge"** %e130, align 4
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %n, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then129
  %126 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e130, align 4
  %127 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %min1, align 4
  %cmp132 = icmp ne %"class.btConvexHullInternal::Edge"* %126, %127
  br i1 %cmp132, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %128 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e130, align 4
  %next133 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %128, i32 0, i32 0
  %129 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next133, align 4
  store %"class.btConvexHullInternal::Edge"* %129, %"class.btConvexHullInternal::Edge"** %n, align 4
  %130 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e130, align 4
  call void @_ZN20btConvexHullInternal14removeEdgePairEPNS_4EdgeE(%class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::Edge"* %130)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %131 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %n, align 4
  store %"class.btConvexHullInternal::Edge"* %131, %"class.btConvexHullInternal::Edge"** %e130, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end134

if.end134:                                        ; preds = %for.end, %if.then127
  %132 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingTail1, align 4
  %tobool135 = icmp ne %"class.btConvexHullInternal::Edge"* %132, null
  br i1 %tobool135, label %if.then136, label %if.else142

if.then136:                                       ; preds = %if.end134
  %133 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %toPrev1, align 4
  %tobool137 = icmp ne %"class.btConvexHullInternal::Edge"* %133, null
  br i1 %tobool137, label %if.then138, label %if.else139

if.then138:                                       ; preds = %if.then136
  %134 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %toPrev1, align 4
  %135 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingHead1, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %134, %"class.btConvexHullInternal::Edge"* %135)
  br label %if.end141

if.else139:                                       ; preds = %if.then136
  %136 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %min1, align 4
  %prev140 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %136, i32 0, i32 1
  %137 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev140, align 4
  %138 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingHead1, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %137, %"class.btConvexHullInternal::Edge"* %138)
  %139 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingHead1, align 4
  store %"class.btConvexHullInternal::Edge"* %139, %"class.btConvexHullInternal::Edge"** %firstNew1, align 4
  br label %if.end141

if.end141:                                        ; preds = %if.else139, %if.then138
  %140 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingTail1, align 4
  %141 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %min1, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %140, %"class.btConvexHullInternal::Edge"* %141)
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %pendingHead1, align 4
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %pendingTail1, align 4
  br label %if.end146

if.else142:                                       ; preds = %if.end134
  %142 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %toPrev1, align 4
  %tobool143 = icmp ne %"class.btConvexHullInternal::Edge"* %142, null
  br i1 %tobool143, label %if.end145, label %if.then144

if.then144:                                       ; preds = %if.else142
  %143 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %min1, align 4
  store %"class.btConvexHullInternal::Edge"* %143, %"class.btConvexHullInternal::Edge"** %firstNew1, align 4
  br label %if.end145

if.end145:                                        ; preds = %if.then144, %if.else142
  br label %if.end146

if.end146:                                        ; preds = %if.end145, %if.end141
  %144 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  %point147 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %144, i32 0, i32 7
  %145 = bitcast %"class.btConvexHullInternal::Point32"* %prevPoint to i8*
  %146 = bitcast %"class.btConvexHullInternal::Point32"* %point147 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %145, i8* align 8 %146, i32 16, i1 false)
  %147 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e1, align 4
  %target148 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %147, i32 0, i32 3
  %148 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target148, align 4
  store %"class.btConvexHullInternal::Vertex"* %148, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  %149 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e1, align 4
  %reverse149 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %149, i32 0, i32 2
  %150 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse149, align 4
  store %"class.btConvexHullInternal::Edge"* %150, %"class.btConvexHullInternal::Edge"** %toPrev1, align 4
  br label %if.end150

if.end150:                                        ; preds = %if.end146, %land.lhs.true125, %if.end123
  %151 = load i32, i32* %cmp90, align 4
  %cmp151 = icmp sle i32 %151, 0
  br i1 %cmp151, label %land.lhs.true152, label %if.end182

land.lhs.true152:                                 ; preds = %if.end150
  %152 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e0, align 4
  %tobool153 = icmp ne %"class.btConvexHullInternal::Edge"* %152, null
  br i1 %tobool153, label %if.then154, label %if.end182

if.then154:                                       ; preds = %land.lhs.true152
  %153 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %toPrev0, align 4
  %tobool155 = icmp ne %"class.btConvexHullInternal::Edge"* %153, null
  br i1 %tobool155, label %if.then156, label %if.end166

if.then156:                                       ; preds = %if.then154
  %154 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %toPrev0, align 4
  %prev158 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %154, i32 0, i32 1
  %155 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev158, align 4
  store %"class.btConvexHullInternal::Edge"* %155, %"class.btConvexHullInternal::Edge"** %e157, align 4
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %n159, align 4
  br label %for.cond160

for.cond160:                                      ; preds = %for.inc164, %if.then156
  %156 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e157, align 4
  %157 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %min0, align 4
  %cmp161 = icmp ne %"class.btConvexHullInternal::Edge"* %156, %157
  br i1 %cmp161, label %for.body162, label %for.end165

for.body162:                                      ; preds = %for.cond160
  %158 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e157, align 4
  %prev163 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %158, i32 0, i32 1
  %159 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev163, align 4
  store %"class.btConvexHullInternal::Edge"* %159, %"class.btConvexHullInternal::Edge"** %n159, align 4
  %160 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e157, align 4
  call void @_ZN20btConvexHullInternal14removeEdgePairEPNS_4EdgeE(%class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::Edge"* %160)
  br label %for.inc164

for.inc164:                                       ; preds = %for.body162
  %161 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %n159, align 4
  store %"class.btConvexHullInternal::Edge"* %161, %"class.btConvexHullInternal::Edge"** %e157, align 4
  br label %for.cond160

for.end165:                                       ; preds = %for.cond160
  br label %if.end166

if.end166:                                        ; preds = %for.end165, %if.then154
  %162 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingTail0, align 4
  %tobool167 = icmp ne %"class.btConvexHullInternal::Edge"* %162, null
  br i1 %tobool167, label %if.then168, label %if.else174

if.then168:                                       ; preds = %if.end166
  %163 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %toPrev0, align 4
  %tobool169 = icmp ne %"class.btConvexHullInternal::Edge"* %163, null
  br i1 %tobool169, label %if.then170, label %if.else171

if.then170:                                       ; preds = %if.then168
  %164 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingHead0, align 4
  %165 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %toPrev0, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %164, %"class.btConvexHullInternal::Edge"* %165)
  br label %if.end173

if.else171:                                       ; preds = %if.then168
  %166 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingHead0, align 4
  %167 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %min0, align 4
  %next172 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %167, i32 0, i32 0
  %168 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next172, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %166, %"class.btConvexHullInternal::Edge"* %168)
  %169 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingHead0, align 4
  store %"class.btConvexHullInternal::Edge"* %169, %"class.btConvexHullInternal::Edge"** %firstNew0, align 4
  br label %if.end173

if.end173:                                        ; preds = %if.else171, %if.then170
  %170 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %min0, align 4
  %171 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingTail0, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %170, %"class.btConvexHullInternal::Edge"* %171)
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %pendingHead0, align 4
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %pendingTail0, align 4
  br label %if.end178

if.else174:                                       ; preds = %if.end166
  %172 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %toPrev0, align 4
  %tobool175 = icmp ne %"class.btConvexHullInternal::Edge"* %172, null
  br i1 %tobool175, label %if.end177, label %if.then176

if.then176:                                       ; preds = %if.else174
  %173 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %min0, align 4
  store %"class.btConvexHullInternal::Edge"* %173, %"class.btConvexHullInternal::Edge"** %firstNew0, align 4
  br label %if.end177

if.end177:                                        ; preds = %if.then176, %if.else174
  br label %if.end178

if.end178:                                        ; preds = %if.end177, %if.end173
  %174 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  %point179 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %174, i32 0, i32 7
  %175 = bitcast %"class.btConvexHullInternal::Point32"* %prevPoint to i8*
  %176 = bitcast %"class.btConvexHullInternal::Point32"* %point179 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %175, i8* align 8 %176, i32 16, i1 false)
  %177 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e0, align 4
  %target180 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %177, i32 0, i32 3
  %178 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target180, align 4
  store %"class.btConvexHullInternal::Vertex"* %178, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  %179 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e0, align 4
  %reverse181 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %179, i32 0, i32 2
  %180 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse181, align 4
  store %"class.btConvexHullInternal::Edge"* %180, %"class.btConvexHullInternal::Edge"** %toPrev0, align 4
  br label %if.end182

if.end182:                                        ; preds = %if.end178, %land.lhs.true152, %if.end150
  br label %if.end183

if.end183:                                        ; preds = %if.end182
  %181 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  %182 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %first0, align 4
  %cmp184 = icmp eq %"class.btConvexHullInternal::Vertex"* %181, %182
  br i1 %cmp184, label %land.lhs.true185, label %if.end222

land.lhs.true185:                                 ; preds = %if.end183
  %183 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  %184 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %first1, align 4
  %cmp186 = icmp eq %"class.btConvexHullInternal::Vertex"* %183, %184
  br i1 %cmp186, label %if.then187, label %if.end222

if.then187:                                       ; preds = %land.lhs.true185
  %185 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %toPrev0, align 4
  %cmp188 = icmp eq %"class.btConvexHullInternal::Edge"* %185, null
  br i1 %cmp188, label %if.then189, label %if.else191

if.then189:                                       ; preds = %if.then187
  %186 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingHead0, align 4
  %187 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingTail0, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %186, %"class.btConvexHullInternal::Edge"* %187)
  %188 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingTail0, align 4
  %189 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0, align 4
  %edges190 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %189, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* %188, %"class.btConvexHullInternal::Edge"** %edges190, align 8
  br label %if.end204

if.else191:                                       ; preds = %if.then187
  %190 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %toPrev0, align 4
  %prev193 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %190, i32 0, i32 1
  %191 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev193, align 4
  store %"class.btConvexHullInternal::Edge"* %191, %"class.btConvexHullInternal::Edge"** %e192, align 4
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %n194, align 4
  br label %for.cond195

for.cond195:                                      ; preds = %for.inc199, %if.else191
  %192 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e192, align 4
  %193 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstNew0, align 4
  %cmp196 = icmp ne %"class.btConvexHullInternal::Edge"* %192, %193
  br i1 %cmp196, label %for.body197, label %for.end200

for.body197:                                      ; preds = %for.cond195
  %194 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e192, align 4
  %prev198 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %194, i32 0, i32 1
  %195 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev198, align 4
  store %"class.btConvexHullInternal::Edge"* %195, %"class.btConvexHullInternal::Edge"** %n194, align 4
  %196 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e192, align 4
  call void @_ZN20btConvexHullInternal14removeEdgePairEPNS_4EdgeE(%class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::Edge"* %196)
  br label %for.inc199

for.inc199:                                       ; preds = %for.body197
  %197 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %n194, align 4
  store %"class.btConvexHullInternal::Edge"* %197, %"class.btConvexHullInternal::Edge"** %e192, align 4
  br label %for.cond195

for.end200:                                       ; preds = %for.cond195
  %198 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingTail0, align 4
  %tobool201 = icmp ne %"class.btConvexHullInternal::Edge"* %198, null
  br i1 %tobool201, label %if.then202, label %if.end203

if.then202:                                       ; preds = %for.end200
  %199 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingHead0, align 4
  %200 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %toPrev0, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %199, %"class.btConvexHullInternal::Edge"* %200)
  %201 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstNew0, align 4
  %202 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingTail0, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %201, %"class.btConvexHullInternal::Edge"* %202)
  br label %if.end203

if.end203:                                        ; preds = %if.then202, %for.end200
  br label %if.end204

if.end204:                                        ; preds = %if.end203, %if.then189
  %203 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %toPrev1, align 4
  %cmp205 = icmp eq %"class.btConvexHullInternal::Edge"* %203, null
  br i1 %cmp205, label %if.then206, label %if.else208

if.then206:                                       ; preds = %if.end204
  %204 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingTail1, align 4
  %205 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingHead1, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %204, %"class.btConvexHullInternal::Edge"* %205)
  %206 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingTail1, align 4
  %207 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1, align 4
  %edges207 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %207, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* %206, %"class.btConvexHullInternal::Edge"** %edges207, align 8
  br label %if.end221

if.else208:                                       ; preds = %if.end204
  %208 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %toPrev1, align 4
  %next210 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %208, i32 0, i32 0
  %209 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next210, align 4
  store %"class.btConvexHullInternal::Edge"* %209, %"class.btConvexHullInternal::Edge"** %e209, align 4
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %n211, align 4
  br label %for.cond212

for.cond212:                                      ; preds = %for.inc216, %if.else208
  %210 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e209, align 4
  %211 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstNew1, align 4
  %cmp213 = icmp ne %"class.btConvexHullInternal::Edge"* %210, %211
  br i1 %cmp213, label %for.body214, label %for.end217

for.body214:                                      ; preds = %for.cond212
  %212 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e209, align 4
  %next215 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %212, i32 0, i32 0
  %213 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next215, align 4
  store %"class.btConvexHullInternal::Edge"* %213, %"class.btConvexHullInternal::Edge"** %n211, align 4
  %214 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e209, align 4
  call void @_ZN20btConvexHullInternal14removeEdgePairEPNS_4EdgeE(%class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::Edge"* %214)
  br label %for.inc216

for.inc216:                                       ; preds = %for.body214
  %215 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %n211, align 4
  store %"class.btConvexHullInternal::Edge"* %215, %"class.btConvexHullInternal::Edge"** %e209, align 4
  br label %for.cond212

for.end217:                                       ; preds = %for.cond212
  %216 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingTail1, align 4
  %tobool218 = icmp ne %"class.btConvexHullInternal::Edge"* %216, null
  br i1 %tobool218, label %if.then219, label %if.end220

if.then219:                                       ; preds = %for.end217
  %217 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %toPrev1, align 4
  %218 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingHead1, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %217, %"class.btConvexHullInternal::Edge"* %218)
  %219 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %pendingTail1, align 4
  %220 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstNew1, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %219, %"class.btConvexHullInternal::Edge"* %220)
  br label %if.end220

if.end220:                                        ; preds = %if.then219, %for.end217
  br label %if.end221

if.end221:                                        ; preds = %if.end220, %if.then206
  br label %return

if.end222:                                        ; preds = %land.lhs.true185, %if.end183
  store i8 0, i8* %firstRun, align 1
  br label %while.body

return:                                           ; preds = %if.end221, %if.then84, %if.then4, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN20btConvexHullInternal14getOrientationEPKNS_4EdgeES2_RKNS_7Point32ES5_(%"class.btConvexHullInternal::Edge"* %prev, %"class.btConvexHullInternal::Edge"* %next, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %s, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %t) #2 {
entry:
  %retval = alloca i32, align 4
  %prev.addr = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %next.addr = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %s.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  %t.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  %n = alloca %"class.btConvexHullInternal::Point64", align 8
  %m = alloca %"class.btConvexHullInternal::Point64", align 8
  %ref.tmp = alloca %"class.btConvexHullInternal::Point32", align 4
  %ref.tmp6 = alloca %"class.btConvexHullInternal::Point32", align 4
  %dot = alloca i64, align 8
  store %"class.btConvexHullInternal::Edge"* %prev, %"class.btConvexHullInternal::Edge"** %prev.addr, align 4
  store %"class.btConvexHullInternal::Edge"* %next, %"class.btConvexHullInternal::Edge"** %next.addr, align 4
  store %"class.btConvexHullInternal::Point32"* %s, %"class.btConvexHullInternal::Point32"** %s.addr, align 4
  store %"class.btConvexHullInternal::Point32"* %t, %"class.btConvexHullInternal::Point32"** %t.addr, align 4
  %0 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev.addr, align 4
  %next1 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %0, i32 0, i32 0
  %1 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next1, align 4
  %2 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next.addr, align 4
  %cmp = icmp eq %"class.btConvexHullInternal::Edge"* %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev.addr, align 4
  %prev2 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %3, i32 0, i32 1
  %4 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev2, align 4
  %5 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next.addr, align 4
  %cmp3 = icmp eq %"class.btConvexHullInternal::Edge"* %4, %5
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.then
  %6 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %t.addr, align 4
  %7 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %s.addr, align 4
  call void @_ZNK20btConvexHullInternal7Point325crossERKS0_(%"class.btConvexHullInternal::Point64"* sret align 8 %n, %"class.btConvexHullInternal::Point32"* %6, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %7)
  %8 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev.addr, align 4
  %target = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %8, i32 0, i32 3
  %9 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target, align 4
  %10 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next.addr, align 4
  %reverse = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %10, i32 0, i32 2
  %11 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse, align 4
  %target5 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %11, i32 0, i32 3
  %12 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target5, align 4
  call void @_ZNK20btConvexHullInternal6VertexmiERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp, %"class.btConvexHullInternal::Vertex"* %9, %"class.btConvexHullInternal::Vertex"* nonnull align 8 dereferenceable(108) %12)
  %13 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next.addr, align 4
  %target7 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %13, i32 0, i32 3
  %14 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target7, align 4
  %15 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next.addr, align 4
  %reverse8 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %15, i32 0, i32 2
  %16 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse8, align 4
  %target9 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %16, i32 0, i32 3
  %17 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target9, align 4
  call void @_ZNK20btConvexHullInternal6VertexmiERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp6, %"class.btConvexHullInternal::Vertex"* %14, %"class.btConvexHullInternal::Vertex"* nonnull align 8 dereferenceable(108) %17)
  call void @_ZNK20btConvexHullInternal7Point325crossERKS0_(%"class.btConvexHullInternal::Point64"* sret align 8 %m, %"class.btConvexHullInternal::Point32"* %ref.tmp, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %ref.tmp6)
  %call = call i64 @_ZNK20btConvexHullInternal7Point643dotERKS0_(%"class.btConvexHullInternal::Point64"* %n, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %m)
  store i64 %call, i64* %dot, align 8
  %18 = load i64, i64* %dot, align 8
  %cmp10 = icmp sgt i64 %18, 0
  %19 = zext i1 %cmp10 to i64
  %cond = select i1 %cmp10, i32 2, i32 1
  store i32 %cond, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  store i32 2, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %20 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev.addr, align 4
  %prev11 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %20, i32 0, i32 1
  %21 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev11, align 4
  %22 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next.addr, align 4
  %cmp12 = icmp eq %"class.btConvexHullInternal::Edge"* %21, %22
  br i1 %cmp12, label %if.then13, label %if.else14

if.then13:                                        ; preds = %if.else
  store i32 1, i32* %retval, align 4
  br label %return

if.else14:                                        ; preds = %if.else
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.else14, %if.then13, %if.end, %if.then4
  %23 = load i32, i32* %retval, align 4
  ret i32 %23
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btConvexHullInternal7Point325crossERKS0_(%"class.btConvexHullInternal::Point64"* noalias sret align 8 %agg.result, %"class.btConvexHullInternal::Point32"* %this, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %b) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  store %"class.btConvexHullInternal::Point32"* %this, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  store %"class.btConvexHullInternal::Point32"* %b, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  %y = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 1
  %0 = load i32, i32* %y, align 4
  %1 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %z = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %1, i32 0, i32 2
  %2 = load i32, i32* %z, align 4
  %mul = mul nsw i32 %0, %2
  %z2 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 2
  %3 = load i32, i32* %z2, align 4
  %4 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %y3 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %4, i32 0, i32 1
  %5 = load i32, i32* %y3, align 4
  %mul4 = mul nsw i32 %3, %5
  %sub = sub nsw i32 %mul, %mul4
  %conv = sext i32 %sub to i64
  %z5 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 2
  %6 = load i32, i32* %z5, align 4
  %7 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %x = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %7, i32 0, i32 0
  %8 = load i32, i32* %x, align 4
  %mul6 = mul nsw i32 %6, %8
  %x7 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 0
  %9 = load i32, i32* %x7, align 4
  %10 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %z8 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %10, i32 0, i32 2
  %11 = load i32, i32* %z8, align 4
  %mul9 = mul nsw i32 %9, %11
  %sub10 = sub nsw i32 %mul6, %mul9
  %conv11 = sext i32 %sub10 to i64
  %x12 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 0
  %12 = load i32, i32* %x12, align 4
  %13 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %y13 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %13, i32 0, i32 1
  %14 = load i32, i32* %y13, align 4
  %mul14 = mul nsw i32 %12, %14
  %y15 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 1
  %15 = load i32, i32* %y15, align 4
  %16 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %x16 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %16, i32 0, i32 0
  %17 = load i32, i32* %x16, align 4
  %mul17 = mul nsw i32 %15, %17
  %sub18 = sub nsw i32 %mul14, %mul17
  %conv19 = sext i32 %sub18 to i64
  %call = call %"class.btConvexHullInternal::Point64"* @_ZN20btConvexHullInternal7Point64C2Exxx(%"class.btConvexHullInternal::Point64"* %agg.result, i64 %conv, i64 %conv11, i64 %conv19)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btConvexHullInternal6VertexmiERKS0_(%"class.btConvexHullInternal::Point32"* noalias sret align 4 %agg.result, %"class.btConvexHullInternal::Vertex"* %this, %"class.btConvexHullInternal::Vertex"* nonnull align 8 dereferenceable(108) %b) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  store %"class.btConvexHullInternal::Vertex"* %this, %"class.btConvexHullInternal::Vertex"** %this.addr, align 4
  store %"class.btConvexHullInternal::Vertex"* %b, %"class.btConvexHullInternal::Vertex"** %b.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %this.addr, align 4
  %point = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 7
  %0 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %b.addr, align 4
  %point2 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %0, i32 0, i32 7
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %agg.result, %"class.btConvexHullInternal::Point32"* %point, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %point2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK20btConvexHullInternal7Point643dotERKS0_(%"class.btConvexHullInternal::Point64"* %this, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %b) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Point64"*, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Point64"*, align 4
  store %"class.btConvexHullInternal::Point64"* %this, %"class.btConvexHullInternal::Point64"** %this.addr, align 4
  store %"class.btConvexHullInternal::Point64"* %b, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %this.addr, align 4
  %x = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %this1, i32 0, i32 0
  %0 = load i64, i64* %x, align 8
  %1 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %x2 = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %1, i32 0, i32 0
  %2 = load i64, i64* %x2, align 8
  %mul = mul nsw i64 %0, %2
  %y = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %this1, i32 0, i32 1
  %3 = load i64, i64* %y, align 8
  %4 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %y3 = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %4, i32 0, i32 1
  %5 = load i64, i64* %y3, align 8
  %mul4 = mul nsw i64 %3, %5
  %add = add nsw i64 %mul, %mul4
  %z = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %this1, i32 0, i32 2
  %6 = load i64, i64* %z, align 8
  %7 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %z5 = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %7, i32 0, i32 2
  %8 = load i64, i64* %z5, align 8
  %mul6 = mul nsw i64 %6, %8
  %add7 = add nsw i64 %add, %mul6
  ret i64 %add7
}

; Function Attrs: noinline optnone
define hidden %"class.btConvexHullInternal::Edge"* @_ZN20btConvexHullInternal12findMaxAngleEbPKNS_6VertexERKNS_7Point32ERKNS_7Point64ES8_RNS_10Rational64E(%class.btConvexHullInternal* %this, i1 zeroext %ccw, %"class.btConvexHullInternal::Vertex"* %start, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %s, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %rxs, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %sxrxs, %"class.btConvexHullInternal::Rational64"* nonnull align 8 dereferenceable(20) %minCot) #2 {
entry:
  %this.addr = alloca %class.btConvexHullInternal*, align 4
  %ccw.addr = alloca i8, align 1
  %start.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %s.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  %rxs.addr = alloca %"class.btConvexHullInternal::Point64"*, align 4
  %sxrxs.addr = alloca %"class.btConvexHullInternal::Point64"*, align 4
  %minCot.addr = alloca %"class.btConvexHullInternal::Rational64"*, align 4
  %minEdge = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %e = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %t = alloca %"class.btConvexHullInternal::Point32", align 4
  %cot = alloca %"class.btConvexHullInternal::Rational64", align 8
  %cmp7 = alloca i32, align 4
  store %class.btConvexHullInternal* %this, %class.btConvexHullInternal** %this.addr, align 4
  %frombool = zext i1 %ccw to i8
  store i8 %frombool, i8* %ccw.addr, align 1
  store %"class.btConvexHullInternal::Vertex"* %start, %"class.btConvexHullInternal::Vertex"** %start.addr, align 4
  store %"class.btConvexHullInternal::Point32"* %s, %"class.btConvexHullInternal::Point32"** %s.addr, align 4
  store %"class.btConvexHullInternal::Point64"* %rxs, %"class.btConvexHullInternal::Point64"** %rxs.addr, align 4
  store %"class.btConvexHullInternal::Point64"* %sxrxs, %"class.btConvexHullInternal::Point64"** %sxrxs.addr, align 4
  store %"class.btConvexHullInternal::Rational64"* %minCot, %"class.btConvexHullInternal::Rational64"** %minCot.addr, align 4
  %this1 = load %class.btConvexHullInternal*, %class.btConvexHullInternal** %this.addr, align 4
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %minEdge, align 4
  %0 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %start.addr, align 4
  %edges = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %0, i32 0, i32 2
  %1 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edges, align 8
  store %"class.btConvexHullInternal::Edge"* %1, %"class.btConvexHullInternal::Edge"** %e, align 4
  %2 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %tobool = icmp ne %"class.btConvexHullInternal::Edge"* %2, null
  br i1 %tobool, label %if.then, label %if.end28

if.then:                                          ; preds = %entry
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then
  %3 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %copy = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %3, i32 0, i32 5
  %4 = load i32, i32* %copy, align 4
  %mergeStamp = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 6
  %5 = load i32, i32* %mergeStamp, align 4
  %cmp = icmp sgt i32 %4, %5
  br i1 %cmp, label %if.then2, label %if.end25

if.then2:                                         ; preds = %do.body
  %6 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %target = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %6, i32 0, i32 3
  %7 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target, align 4
  %8 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %start.addr, align 4
  call void @_ZNK20btConvexHullInternal6VertexmiERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %t, %"class.btConvexHullInternal::Vertex"* %7, %"class.btConvexHullInternal::Vertex"* nonnull align 8 dereferenceable(108) %8)
  %9 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %sxrxs.addr, align 4
  %call = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %t, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %9)
  %10 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %rxs.addr, align 4
  %call3 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %t, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %10)
  %call4 = call %"class.btConvexHullInternal::Rational64"* @_ZN20btConvexHullInternal10Rational64C2Exx(%"class.btConvexHullInternal::Rational64"* %cot, i64 %call, i64 %call3)
  %call5 = call zeroext i1 @_ZNK20btConvexHullInternal10Rational645isNaNEv(%"class.btConvexHullInternal::Rational64"* %cot)
  br i1 %call5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.then2
  br label %if.end24

if.else:                                          ; preds = %if.then2
  %11 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %minEdge, align 4
  %cmp8 = icmp eq %"class.btConvexHullInternal::Edge"* %11, null
  br i1 %cmp8, label %if.then9, label %if.else10

if.then9:                                         ; preds = %if.else
  %12 = load %"class.btConvexHullInternal::Rational64"*, %"class.btConvexHullInternal::Rational64"** %minCot.addr, align 4
  %13 = bitcast %"class.btConvexHullInternal::Rational64"* %12 to i8*
  %14 = bitcast %"class.btConvexHullInternal::Rational64"* %cot to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %13, i8* align 8 %14, i32 20, i1 false)
  %15 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  store %"class.btConvexHullInternal::Edge"* %15, %"class.btConvexHullInternal::Edge"** %minEdge, align 4
  br label %if.end23

if.else10:                                        ; preds = %if.else
  %16 = load %"class.btConvexHullInternal::Rational64"*, %"class.btConvexHullInternal::Rational64"** %minCot.addr, align 4
  %call11 = call i32 @_ZNK20btConvexHullInternal10Rational647compareERKS0_(%"class.btConvexHullInternal::Rational64"* %cot, %"class.btConvexHullInternal::Rational64"* nonnull align 8 dereferenceable(20) %16)
  store i32 %call11, i32* %cmp7, align 4
  %cmp12 = icmp slt i32 %call11, 0
  br i1 %cmp12, label %if.then13, label %if.else14

if.then13:                                        ; preds = %if.else10
  %17 = load %"class.btConvexHullInternal::Rational64"*, %"class.btConvexHullInternal::Rational64"** %minCot.addr, align 4
  %18 = bitcast %"class.btConvexHullInternal::Rational64"* %17 to i8*
  %19 = bitcast %"class.btConvexHullInternal::Rational64"* %cot to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %18, i8* align 8 %19, i32 20, i1 false)
  %20 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  store %"class.btConvexHullInternal::Edge"* %20, %"class.btConvexHullInternal::Edge"** %minEdge, align 4
  br label %if.end22

if.else14:                                        ; preds = %if.else10
  %21 = load i32, i32* %cmp7, align 4
  %cmp15 = icmp eq i32 %21, 0
  br i1 %cmp15, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.else14
  %22 = load i8, i8* %ccw.addr, align 1
  %tobool16 = trunc i8 %22 to i1
  %conv = zext i1 %tobool16 to i32
  %23 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %minEdge, align 4
  %24 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %25 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %s.addr, align 4
  %call17 = call i32 @_ZN20btConvexHullInternal14getOrientationEPKNS_4EdgeES2_RKNS_7Point32ES5_(%"class.btConvexHullInternal::Edge"* %23, %"class.btConvexHullInternal::Edge"* %24, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %25, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %t)
  %cmp18 = icmp eq i32 %call17, 2
  %conv19 = zext i1 %cmp18 to i32
  %cmp20 = icmp eq i32 %conv, %conv19
  br i1 %cmp20, label %if.then21, label %if.end

if.then21:                                        ; preds = %land.lhs.true
  %26 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  store %"class.btConvexHullInternal::Edge"* %26, %"class.btConvexHullInternal::Edge"** %minEdge, align 4
  br label %if.end

if.end:                                           ; preds = %if.then21, %land.lhs.true, %if.else14
  br label %if.end22

if.end22:                                         ; preds = %if.end, %if.then13
  br label %if.end23

if.end23:                                         ; preds = %if.end22, %if.then9
  br label %if.end24

if.end24:                                         ; preds = %if.end23, %if.then6
  br label %if.end25

if.end25:                                         ; preds = %if.end24, %do.body
  %27 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %27, i32 0, i32 0
  %28 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next, align 4
  store %"class.btConvexHullInternal::Edge"* %28, %"class.btConvexHullInternal::Edge"** %e, align 4
  br label %do.cond

do.cond:                                          ; preds = %if.end25
  %29 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %30 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %start.addr, align 4
  %edges26 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %30, i32 0, i32 2
  %31 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edges26, align 8
  %cmp27 = icmp ne %"class.btConvexHullInternal::Edge"* %29, %31
  br i1 %cmp27, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  br label %if.end28

if.end28:                                         ; preds = %do.end, %entry
  %32 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %minEdge, align 4
  ret %"class.btConvexHullInternal::Edge"* %32
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %this, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %b) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Point64"*, align 4
  store %"class.btConvexHullInternal::Point32"* %this, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  store %"class.btConvexHullInternal::Point64"* %b, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  %x = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 0
  %0 = load i32, i32* %x, align 4
  %conv = sext i32 %0 to i64
  %1 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %x2 = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %1, i32 0, i32 0
  %2 = load i64, i64* %x2, align 8
  %mul = mul nsw i64 %conv, %2
  %y = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 1
  %3 = load i32, i32* %y, align 4
  %conv3 = sext i32 %3 to i64
  %4 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %y4 = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %4, i32 0, i32 1
  %5 = load i64, i64* %y4, align 8
  %mul5 = mul nsw i64 %conv3, %5
  %add = add nsw i64 %mul, %mul5
  %z = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 2
  %6 = load i32, i32* %z, align 4
  %conv6 = sext i32 %6 to i64
  %7 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %z7 = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %7, i32 0, i32 2
  %8 = load i64, i64* %z7, align 8
  %mul8 = mul nsw i64 %conv6, %8
  %add9 = add nsw i64 %add, %mul8
  ret i64 %add9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Rational64"* @_ZN20btConvexHullInternal10Rational64C2Exx(%"class.btConvexHullInternal::Rational64"* returned %this, i64 %numerator, i64 %denominator) unnamed_addr #1 comdat {
entry:
  %retval = alloca %"class.btConvexHullInternal::Rational64"*, align 4
  %this.addr = alloca %"class.btConvexHullInternal::Rational64"*, align 4
  %numerator.addr = alloca i64, align 8
  %denominator.addr = alloca i64, align 8
  store %"class.btConvexHullInternal::Rational64"* %this, %"class.btConvexHullInternal::Rational64"** %this.addr, align 4
  store i64 %numerator, i64* %numerator.addr, align 8
  store i64 %denominator, i64* %denominator.addr, align 8
  %this1 = load %"class.btConvexHullInternal::Rational64"*, %"class.btConvexHullInternal::Rational64"** %this.addr, align 4
  store %"class.btConvexHullInternal::Rational64"* %this1, %"class.btConvexHullInternal::Rational64"** %retval, align 4
  %0 = load i64, i64* %numerator.addr, align 8
  %cmp = icmp sgt i64 %0, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %sign = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 2
  store i32 1, i32* %sign, align 8
  %1 = load i64, i64* %numerator.addr, align 8
  %m_numerator = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 0
  store i64 %1, i64* %m_numerator, align 8
  br label %if.end9

if.else:                                          ; preds = %entry
  %2 = load i64, i64* %numerator.addr, align 8
  %cmp2 = icmp slt i64 %2, 0
  br i1 %cmp2, label %if.then3, label %if.else6

if.then3:                                         ; preds = %if.else
  %sign4 = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 2
  store i32 -1, i32* %sign4, align 8
  %3 = load i64, i64* %numerator.addr, align 8
  %sub = sub nsw i64 0, %3
  %m_numerator5 = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 0
  store i64 %sub, i64* %m_numerator5, align 8
  br label %if.end

if.else6:                                         ; preds = %if.else
  %sign7 = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 2
  store i32 0, i32* %sign7, align 8
  %m_numerator8 = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 0
  store i64 0, i64* %m_numerator8, align 8
  br label %if.end

if.end:                                           ; preds = %if.else6, %if.then3
  br label %if.end9

if.end9:                                          ; preds = %if.end, %if.then
  %4 = load i64, i64* %denominator.addr, align 8
  %cmp10 = icmp sgt i64 %4, 0
  br i1 %cmp10, label %if.then11, label %if.else12

if.then11:                                        ; preds = %if.end9
  %5 = load i64, i64* %denominator.addr, align 8
  %m_denominator = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 1
  store i64 %5, i64* %m_denominator, align 8
  br label %if.end23

if.else12:                                        ; preds = %if.end9
  %6 = load i64, i64* %denominator.addr, align 8
  %cmp13 = icmp slt i64 %6, 0
  br i1 %cmp13, label %if.then14, label %if.else20

if.then14:                                        ; preds = %if.else12
  %sign15 = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 2
  %7 = load i32, i32* %sign15, align 8
  %sub16 = sub nsw i32 0, %7
  %sign17 = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 2
  store i32 %sub16, i32* %sign17, align 8
  %8 = load i64, i64* %denominator.addr, align 8
  %sub18 = sub nsw i64 0, %8
  %m_denominator19 = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 1
  store i64 %sub18, i64* %m_denominator19, align 8
  br label %if.end22

if.else20:                                        ; preds = %if.else12
  %m_denominator21 = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 1
  store i64 0, i64* %m_denominator21, align 8
  br label %if.end22

if.end22:                                         ; preds = %if.else20, %if.then14
  br label %if.end23

if.end23:                                         ; preds = %if.end22, %if.then11
  %9 = load %"class.btConvexHullInternal::Rational64"*, %"class.btConvexHullInternal::Rational64"** %retval, align 4
  ret %"class.btConvexHullInternal::Rational64"* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK20btConvexHullInternal10Rational645isNaNEv(%"class.btConvexHullInternal::Rational64"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Rational64"*, align 4
  store %"class.btConvexHullInternal::Rational64"* %this, %"class.btConvexHullInternal::Rational64"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Rational64"*, %"class.btConvexHullInternal::Rational64"** %this.addr, align 4
  %sign = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 2
  %0 = load i32, i32* %sign, align 8
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %m_denominator = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 1
  %1 = load i64, i64* %m_denominator, align 8
  %cmp2 = icmp eq i64 %1, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %cmp2, %land.rhs ]
  ret i1 %2
}

; Function Attrs: noinline optnone
define hidden void @_ZN20btConvexHullInternal24findEdgeForCoplanarFacesEPNS_6VertexES1_RPNS_4EdgeES4_S1_S1_(%class.btConvexHullInternal* %this, %"class.btConvexHullInternal::Vertex"* %c0, %"class.btConvexHullInternal::Vertex"* %c1, %"class.btConvexHullInternal::Edge"** nonnull align 4 dereferenceable(4) %e0, %"class.btConvexHullInternal::Edge"** nonnull align 4 dereferenceable(4) %e1, %"class.btConvexHullInternal::Vertex"* %stop0, %"class.btConvexHullInternal::Vertex"* %stop1) #2 {
entry:
  %this.addr = alloca %class.btConvexHullInternal*, align 4
  %c0.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %c1.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %e0.addr = alloca %"class.btConvexHullInternal::Edge"**, align 4
  %e1.addr = alloca %"class.btConvexHullInternal::Edge"**, align 4
  %stop0.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %stop1.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %start0 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %start1 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %et0 = alloca %"class.btConvexHullInternal::Point32", align 4
  %et1 = alloca %"class.btConvexHullInternal::Point32", align 4
  %s = alloca %"class.btConvexHullInternal::Point32", align 4
  %normal = alloca %"class.btConvexHullInternal::Point64", align 8
  %ref.tmp = alloca %"class.btConvexHullInternal::Point32", align 4
  %dist = alloca i64, align 8
  %perp = alloca %"class.btConvexHullInternal::Point64", align 8
  %maxDot0 = alloca i64, align 8
  %e = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %dot = alloca i64, align 8
  %maxDot1 = alloca i64, align 8
  %e48 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %dot61 = alloca i64, align 8
  %dx = alloca i64, align 8
  %dy = alloca i64, align 8
  %ref.tmp76 = alloca %"class.btConvexHullInternal::Point32", align 4
  %f0 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %dx0 = alloca i64, align 8
  %ref.tmp88 = alloca %"class.btConvexHullInternal::Point32", align 4
  %dy0 = alloca i64, align 8
  %ref.tmp92 = alloca %"class.btConvexHullInternal::Point32", align 4
  %ref.tmp101 = alloca %"class.btConvexHullInternal::Rational64", align 8
  %ref.tmp103 = alloca %"class.btConvexHullInternal::Rational64", align 8
  %ref.tmp112 = alloca %"class.btConvexHullInternal::Point32", align 4
  %f1 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %d1 = alloca %"class.btConvexHullInternal::Point32", align 4
  %dx1 = alloca i64, align 8
  %dy1 = alloca i64, align 8
  %dxn = alloca i64, align 8
  %ref.tmp140 = alloca %"class.btConvexHullInternal::Point32", align 4
  %ref.tmp152 = alloca %"class.btConvexHullInternal::Rational64", align 8
  %ref.tmp154 = alloca %"class.btConvexHullInternal::Rational64", align 8
  %dy175 = alloca i64, align 8
  %ref.tmp176 = alloca %"class.btConvexHullInternal::Point32", align 4
  %f1183 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %dx1190 = alloca i64, align 8
  %ref.tmp191 = alloca %"class.btConvexHullInternal::Point32", align 4
  %dy1195 = alloca i64, align 8
  %ref.tmp196 = alloca %"class.btConvexHullInternal::Point32", align 4
  %ref.tmp206 = alloca %"class.btConvexHullInternal::Rational64", align 8
  %ref.tmp208 = alloca %"class.btConvexHullInternal::Rational64", align 8
  %ref.tmp218 = alloca %"class.btConvexHullInternal::Point32", align 4
  %f0233 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %d0 = alloca %"class.btConvexHullInternal::Point32", align 4
  %dx0245 = alloca i64, align 8
  %dy0247 = alloca i64, align 8
  %dxn249 = alloca i64, align 8
  %ref.tmp250 = alloca %"class.btConvexHullInternal::Point32", align 4
  %ref.tmp262 = alloca %"class.btConvexHullInternal::Rational64", align 8
  %ref.tmp264 = alloca %"class.btConvexHullInternal::Rational64", align 8
  store %class.btConvexHullInternal* %this, %class.btConvexHullInternal** %this.addr, align 4
  store %"class.btConvexHullInternal::Vertex"* %c0, %"class.btConvexHullInternal::Vertex"** %c0.addr, align 4
  store %"class.btConvexHullInternal::Vertex"* %c1, %"class.btConvexHullInternal::Vertex"** %c1.addr, align 4
  store %"class.btConvexHullInternal::Edge"** %e0, %"class.btConvexHullInternal::Edge"*** %e0.addr, align 4
  store %"class.btConvexHullInternal::Edge"** %e1, %"class.btConvexHullInternal::Edge"*** %e1.addr, align 4
  store %"class.btConvexHullInternal::Vertex"* %stop0, %"class.btConvexHullInternal::Vertex"** %stop0.addr, align 4
  store %"class.btConvexHullInternal::Vertex"* %stop1, %"class.btConvexHullInternal::Vertex"** %stop1.addr, align 4
  %this1 = load %class.btConvexHullInternal*, %class.btConvexHullInternal** %this.addr, align 4
  %0 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e0.addr, align 4
  %1 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %0, align 4
  store %"class.btConvexHullInternal::Edge"* %1, %"class.btConvexHullInternal::Edge"** %start0, align 4
  %2 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e1.addr, align 4
  %3 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %2, align 4
  store %"class.btConvexHullInternal::Edge"* %3, %"class.btConvexHullInternal::Edge"** %start1, align 4
  %4 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %start0, align 4
  %tobool = icmp ne %"class.btConvexHullInternal::Edge"* %4, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %5 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %start0, align 4
  %target = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %5, i32 0, i32 3
  %6 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target, align 4
  %point = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %6, i32 0, i32 7
  br label %cond.end

cond.false:                                       ; preds = %entry
  %7 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0.addr, align 4
  %point2 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %7, i32 0, i32 7
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi %"class.btConvexHullInternal::Point32"* [ %point, %cond.true ], [ %point2, %cond.false ]
  %8 = bitcast %"class.btConvexHullInternal::Point32"* %et0 to i8*
  %9 = bitcast %"class.btConvexHullInternal::Point32"* %cond-lvalue to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 8 %9, i32 16, i1 false)
  %10 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %start1, align 4
  %tobool3 = icmp ne %"class.btConvexHullInternal::Edge"* %10, null
  br i1 %tobool3, label %cond.true4, label %cond.false7

cond.true4:                                       ; preds = %cond.end
  %11 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %start1, align 4
  %target5 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %11, i32 0, i32 3
  %12 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target5, align 4
  %point6 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %12, i32 0, i32 7
  br label %cond.end9

cond.false7:                                      ; preds = %cond.end
  %13 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1.addr, align 4
  %point8 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %13, i32 0, i32 7
  br label %cond.end9

cond.end9:                                        ; preds = %cond.false7, %cond.true4
  %cond-lvalue10 = phi %"class.btConvexHullInternal::Point32"* [ %point6, %cond.true4 ], [ %point8, %cond.false7 ]
  %14 = bitcast %"class.btConvexHullInternal::Point32"* %et1 to i8*
  %15 = bitcast %"class.btConvexHullInternal::Point32"* %cond-lvalue10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 8 %15, i32 16, i1 false)
  %16 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c1.addr, align 4
  %point11 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %16, i32 0, i32 7
  %17 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0.addr, align 4
  %point12 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %17, i32 0, i32 7
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %s, %"class.btConvexHullInternal::Point32"* %point11, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %point12)
  %18 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %start0, align 4
  %tobool13 = icmp ne %"class.btConvexHullInternal::Edge"* %18, null
  br i1 %tobool13, label %cond.true14, label %cond.false15

cond.true14:                                      ; preds = %cond.end9
  %19 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %start0, align 4
  br label %cond.end16

cond.false15:                                     ; preds = %cond.end9
  %20 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %start1, align 4
  br label %cond.end16

cond.end16:                                       ; preds = %cond.false15, %cond.true14
  %cond = phi %"class.btConvexHullInternal::Edge"* [ %19, %cond.true14 ], [ %20, %cond.false15 ]
  %target17 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %cond, i32 0, i32 3
  %21 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target17, align 4
  %point18 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %21, i32 0, i32 7
  %22 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0.addr, align 4
  %point19 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %22, i32 0, i32 7
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp, %"class.btConvexHullInternal::Point32"* %point18, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %point19)
  call void @_ZNK20btConvexHullInternal7Point325crossERKS0_(%"class.btConvexHullInternal::Point64"* sret align 8 %normal, %"class.btConvexHullInternal::Point32"* %ref.tmp, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %s)
  %23 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c0.addr, align 4
  %point20 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %23, i32 0, i32 7
  %call = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %point20, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %normal)
  store i64 %call, i64* %dist, align 8
  call void @_ZNK20btConvexHullInternal7Point325crossERKNS_7Point64E(%"class.btConvexHullInternal::Point64"* sret align 8 %perp, %"class.btConvexHullInternal::Point32"* %s, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %normal)
  %call21 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %et0, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %perp)
  store i64 %call21, i64* %maxDot0, align 8
  %24 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e0.addr, align 4
  %25 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %24, align 4
  %tobool22 = icmp ne %"class.btConvexHullInternal::Edge"* %25, null
  br i1 %tobool22, label %if.then, label %if.end40

if.then:                                          ; preds = %cond.end16
  br label %while.cond

while.cond:                                       ; preds = %if.end37, %if.then
  %26 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e0.addr, align 4
  %27 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %26, align 4
  %target23 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %27, i32 0, i32 3
  %28 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target23, align 4
  %29 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %stop0.addr, align 4
  %cmp = icmp ne %"class.btConvexHullInternal::Vertex"* %28, %29
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %30 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e0.addr, align 4
  %31 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %30, align 4
  %reverse = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %31, i32 0, i32 2
  %32 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse, align 4
  %prev = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %32, i32 0, i32 1
  %33 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev, align 4
  store %"class.btConvexHullInternal::Edge"* %33, %"class.btConvexHullInternal::Edge"** %e, align 4
  %34 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %target24 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %34, i32 0, i32 3
  %35 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target24, align 4
  %point25 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %35, i32 0, i32 7
  %call26 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %point25, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %normal)
  %36 = load i64, i64* %dist, align 8
  %cmp27 = icmp slt i64 %call26, %36
  br i1 %cmp27, label %if.then28, label %if.end

if.then28:                                        ; preds = %while.body
  br label %while.end

if.end:                                           ; preds = %while.body
  %37 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %copy = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %37, i32 0, i32 5
  %38 = load i32, i32* %copy, align 4
  %mergeStamp = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 6
  %39 = load i32, i32* %mergeStamp, align 4
  %cmp29 = icmp eq i32 %38, %39
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.end
  br label %while.end

if.end31:                                         ; preds = %if.end
  %40 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %target32 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %40, i32 0, i32 3
  %41 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target32, align 4
  %point33 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %41, i32 0, i32 7
  %call34 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %point33, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %perp)
  store i64 %call34, i64* %dot, align 8
  %42 = load i64, i64* %dot, align 8
  %43 = load i64, i64* %maxDot0, align 8
  %cmp35 = icmp sle i64 %42, %43
  br i1 %cmp35, label %if.then36, label %if.end37

if.then36:                                        ; preds = %if.end31
  br label %while.end

if.end37:                                         ; preds = %if.end31
  %44 = load i64, i64* %dot, align 8
  store i64 %44, i64* %maxDot0, align 8
  %45 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %46 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e0.addr, align 4
  store %"class.btConvexHullInternal::Edge"* %45, %"class.btConvexHullInternal::Edge"** %46, align 4
  %47 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %target38 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %47, i32 0, i32 3
  %48 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target38, align 4
  %point39 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %48, i32 0, i32 7
  %49 = bitcast %"class.btConvexHullInternal::Point32"* %et0 to i8*
  %50 = bitcast %"class.btConvexHullInternal::Point32"* %point39 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %49, i8* align 8 %50, i32 16, i1 false)
  br label %while.cond

while.end:                                        ; preds = %if.then36, %if.then30, %if.then28, %while.cond
  br label %if.end40

if.end40:                                         ; preds = %while.end, %cond.end16
  %call41 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %et1, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %perp)
  store i64 %call41, i64* %maxDot1, align 8
  %51 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e1.addr, align 4
  %52 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %51, align 4
  %tobool42 = icmp ne %"class.btConvexHullInternal::Edge"* %52, null
  br i1 %tobool42, label %if.then43, label %if.end71

if.then43:                                        ; preds = %if.end40
  br label %while.cond44

while.cond44:                                     ; preds = %if.end67, %if.then43
  %53 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e1.addr, align 4
  %54 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %53, align 4
  %target45 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %54, i32 0, i32 3
  %55 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target45, align 4
  %56 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %stop1.addr, align 4
  %cmp46 = icmp ne %"class.btConvexHullInternal::Vertex"* %55, %56
  br i1 %cmp46, label %while.body47, label %while.end70

while.body47:                                     ; preds = %while.cond44
  %57 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e1.addr, align 4
  %58 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %57, align 4
  %reverse49 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %58, i32 0, i32 2
  %59 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse49, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %59, i32 0, i32 0
  %60 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next, align 4
  store %"class.btConvexHullInternal::Edge"* %60, %"class.btConvexHullInternal::Edge"** %e48, align 4
  %61 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e48, align 4
  %target50 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %61, i32 0, i32 3
  %62 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target50, align 4
  %point51 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %62, i32 0, i32 7
  %call52 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %point51, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %normal)
  %63 = load i64, i64* %dist, align 8
  %cmp53 = icmp slt i64 %call52, %63
  br i1 %cmp53, label %if.then54, label %if.end55

if.then54:                                        ; preds = %while.body47
  br label %while.end70

if.end55:                                         ; preds = %while.body47
  %64 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e48, align 4
  %copy56 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %64, i32 0, i32 5
  %65 = load i32, i32* %copy56, align 4
  %mergeStamp57 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 6
  %66 = load i32, i32* %mergeStamp57, align 4
  %cmp58 = icmp eq i32 %65, %66
  br i1 %cmp58, label %if.then59, label %if.end60

if.then59:                                        ; preds = %if.end55
  br label %while.end70

if.end60:                                         ; preds = %if.end55
  %67 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e48, align 4
  %target62 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %67, i32 0, i32 3
  %68 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target62, align 4
  %point63 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %68, i32 0, i32 7
  %call64 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %point63, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %perp)
  store i64 %call64, i64* %dot61, align 8
  %69 = load i64, i64* %dot61, align 8
  %70 = load i64, i64* %maxDot1, align 8
  %cmp65 = icmp sle i64 %69, %70
  br i1 %cmp65, label %if.then66, label %if.end67

if.then66:                                        ; preds = %if.end60
  br label %while.end70

if.end67:                                         ; preds = %if.end60
  %71 = load i64, i64* %dot61, align 8
  store i64 %71, i64* %maxDot1, align 8
  %72 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e48, align 4
  %73 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e1.addr, align 4
  store %"class.btConvexHullInternal::Edge"* %72, %"class.btConvexHullInternal::Edge"** %73, align 4
  %74 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e48, align 4
  %target68 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %74, i32 0, i32 3
  %75 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target68, align 4
  %point69 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %75, i32 0, i32 7
  %76 = bitcast %"class.btConvexHullInternal::Point32"* %et1 to i8*
  %77 = bitcast %"class.btConvexHullInternal::Point32"* %point69 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %76, i8* align 8 %77, i32 16, i1 false)
  br label %while.cond44

while.end70:                                      ; preds = %if.then66, %if.then59, %if.then54, %while.cond44
  br label %if.end71

if.end71:                                         ; preds = %while.end70, %if.end40
  %78 = load i64, i64* %maxDot1, align 8
  %79 = load i64, i64* %maxDot0, align 8
  %sub = sub nsw i64 %78, %79
  store i64 %sub, i64* %dx, align 8
  %80 = load i64, i64* %dx, align 8
  %cmp72 = icmp sgt i64 %80, 0
  br i1 %cmp72, label %if.then73, label %if.else170

if.then73:                                        ; preds = %if.end71
  br label %while.body75

while.body75:                                     ; preds = %if.then73, %cond.end117, %if.then162
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp76, %"class.btConvexHullInternal::Point32"* %et1, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %et0)
  %call77 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKS0_(%"class.btConvexHullInternal::Point32"* %ref.tmp76, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %s)
  store i64 %call77, i64* %dy, align 8
  %81 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e0.addr, align 4
  %82 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %81, align 4
  %tobool78 = icmp ne %"class.btConvexHullInternal::Edge"* %82, null
  br i1 %tobool78, label %land.lhs.true, label %if.end121

land.lhs.true:                                    ; preds = %while.body75
  %83 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e0.addr, align 4
  %84 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %83, align 4
  %target79 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %84, i32 0, i32 3
  %85 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target79, align 4
  %86 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %stop0.addr, align 4
  %cmp80 = icmp ne %"class.btConvexHullInternal::Vertex"* %85, %86
  br i1 %cmp80, label %if.then81, label %if.end121

if.then81:                                        ; preds = %land.lhs.true
  %87 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e0.addr, align 4
  %88 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %87, align 4
  %next82 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %88, i32 0, i32 0
  %89 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next82, align 4
  %reverse83 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %89, i32 0, i32 2
  %90 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse83, align 4
  store %"class.btConvexHullInternal::Edge"* %90, %"class.btConvexHullInternal::Edge"** %f0, align 4
  %91 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f0, align 4
  %copy84 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %91, i32 0, i32 5
  %92 = load i32, i32* %copy84, align 4
  %mergeStamp85 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 6
  %93 = load i32, i32* %mergeStamp85, align 4
  %cmp86 = icmp sgt i32 %92, %93
  br i1 %cmp86, label %if.then87, label %if.end120

if.then87:                                        ; preds = %if.then81
  %94 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f0, align 4
  %target89 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %94, i32 0, i32 3
  %95 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target89, align 4
  %point90 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %95, i32 0, i32 7
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp88, %"class.btConvexHullInternal::Point32"* %point90, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %et0)
  %call91 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %ref.tmp88, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %perp)
  store i64 %call91, i64* %dx0, align 8
  %96 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f0, align 4
  %target93 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %96, i32 0, i32 3
  %97 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target93, align 4
  %point94 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %97, i32 0, i32 7
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp92, %"class.btConvexHullInternal::Point32"* %point94, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %et0)
  %call95 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKS0_(%"class.btConvexHullInternal::Point32"* %ref.tmp92, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %s)
  store i64 %call95, i64* %dy0, align 8
  %98 = load i64, i64* %dx0, align 8
  %cmp96 = icmp eq i64 %98, 0
  br i1 %cmp96, label %cond.true97, label %cond.false99

cond.true97:                                      ; preds = %if.then87
  %99 = load i64, i64* %dy0, align 8
  %cmp98 = icmp slt i64 %99, 0
  br label %cond.end107

cond.false99:                                     ; preds = %if.then87
  %100 = load i64, i64* %dx0, align 8
  %cmp100 = icmp slt i64 %100, 0
  br i1 %cmp100, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %cond.false99
  %101 = load i64, i64* %dy0, align 8
  %102 = load i64, i64* %dx0, align 8
  %call102 = call %"class.btConvexHullInternal::Rational64"* @_ZN20btConvexHullInternal10Rational64C2Exx(%"class.btConvexHullInternal::Rational64"* %ref.tmp101, i64 %101, i64 %102)
  %103 = load i64, i64* %dy, align 8
  %104 = load i64, i64* %dx, align 8
  %call104 = call %"class.btConvexHullInternal::Rational64"* @_ZN20btConvexHullInternal10Rational64C2Exx(%"class.btConvexHullInternal::Rational64"* %ref.tmp103, i64 %103, i64 %104)
  %call105 = call i32 @_ZNK20btConvexHullInternal10Rational647compareERKS0_(%"class.btConvexHullInternal::Rational64"* %ref.tmp101, %"class.btConvexHullInternal::Rational64"* nonnull align 8 dereferenceable(20) %ref.tmp103)
  %cmp106 = icmp sge i32 %call105, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %cond.false99
  %105 = phi i1 [ false, %cond.false99 ], [ %cmp106, %land.rhs ]
  br label %cond.end107

cond.end107:                                      ; preds = %land.end, %cond.true97
  %cond108 = phi i1 [ %cmp98, %cond.true97 ], [ %105, %land.end ]
  br i1 %cond108, label %if.then109, label %if.end119

if.then109:                                       ; preds = %cond.end107
  %106 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f0, align 4
  %target110 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %106, i32 0, i32 3
  %107 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target110, align 4
  %point111 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %107, i32 0, i32 7
  %108 = bitcast %"class.btConvexHullInternal::Point32"* %et0 to i8*
  %109 = bitcast %"class.btConvexHullInternal::Point32"* %point111 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %108, i8* align 8 %109, i32 16, i1 false)
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp112, %"class.btConvexHullInternal::Point32"* %et1, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %et0)
  %call113 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %ref.tmp112, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %perp)
  store i64 %call113, i64* %dx, align 8
  %110 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e0.addr, align 4
  %111 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %110, align 4
  %112 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %start0, align 4
  %cmp114 = icmp eq %"class.btConvexHullInternal::Edge"* %111, %112
  br i1 %cmp114, label %cond.true115, label %cond.false116

cond.true115:                                     ; preds = %if.then109
  br label %cond.end117

cond.false116:                                    ; preds = %if.then109
  %113 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f0, align 4
  br label %cond.end117

cond.end117:                                      ; preds = %cond.false116, %cond.true115
  %cond118 = phi %"class.btConvexHullInternal::Edge"* [ null, %cond.true115 ], [ %113, %cond.false116 ]
  %114 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e0.addr, align 4
  store %"class.btConvexHullInternal::Edge"* %cond118, %"class.btConvexHullInternal::Edge"** %114, align 4
  br label %while.body75

if.end119:                                        ; preds = %cond.end107
  br label %if.end120

if.end120:                                        ; preds = %if.end119, %if.then81
  br label %if.end121

if.end121:                                        ; preds = %if.end120, %land.lhs.true, %while.body75
  %115 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e1.addr, align 4
  %116 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %115, align 4
  %tobool122 = icmp ne %"class.btConvexHullInternal::Edge"* %116, null
  br i1 %tobool122, label %land.lhs.true123, label %if.end168

land.lhs.true123:                                 ; preds = %if.end121
  %117 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e1.addr, align 4
  %118 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %117, align 4
  %target124 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %118, i32 0, i32 3
  %119 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target124, align 4
  %120 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %stop1.addr, align 4
  %cmp125 = icmp ne %"class.btConvexHullInternal::Vertex"* %119, %120
  br i1 %cmp125, label %if.then126, label %if.end168

if.then126:                                       ; preds = %land.lhs.true123
  %121 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e1.addr, align 4
  %122 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %121, align 4
  %reverse127 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %122, i32 0, i32 2
  %123 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse127, align 4
  %next128 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %123, i32 0, i32 0
  %124 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next128, align 4
  store %"class.btConvexHullInternal::Edge"* %124, %"class.btConvexHullInternal::Edge"** %f1, align 4
  %125 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f1, align 4
  %copy129 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %125, i32 0, i32 5
  %126 = load i32, i32* %copy129, align 4
  %mergeStamp130 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 6
  %127 = load i32, i32* %mergeStamp130, align 4
  %cmp131 = icmp sgt i32 %126, %127
  br i1 %cmp131, label %if.then132, label %if.end167

if.then132:                                       ; preds = %if.then126
  %128 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f1, align 4
  %target133 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %128, i32 0, i32 3
  %129 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target133, align 4
  %point134 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %129, i32 0, i32 7
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %d1, %"class.btConvexHullInternal::Point32"* %point134, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %et1)
  %call135 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %d1, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %normal)
  %cmp136 = icmp eq i64 %call135, 0
  br i1 %cmp136, label %if.then137, label %if.else

if.then137:                                       ; preds = %if.then132
  %call138 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %d1, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %perp)
  store i64 %call138, i64* %dx1, align 8
  %call139 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKS0_(%"class.btConvexHullInternal::Point32"* %d1, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %s)
  store i64 %call139, i64* %dy1, align 8
  %130 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f1, align 4
  %target141 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %130, i32 0, i32 3
  %131 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target141, align 4
  %point142 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %131, i32 0, i32 7
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp140, %"class.btConvexHullInternal::Point32"* %point142, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %et0)
  %call143 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %ref.tmp140, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %perp)
  store i64 %call143, i64* %dxn, align 8
  %132 = load i64, i64* %dxn, align 8
  %cmp144 = icmp sgt i64 %132, 0
  br i1 %cmp144, label %land.rhs145, label %land.end161

land.rhs145:                                      ; preds = %if.then137
  %133 = load i64, i64* %dx1, align 8
  %cmp146 = icmp eq i64 %133, 0
  br i1 %cmp146, label %cond.true147, label %cond.false149

cond.true147:                                     ; preds = %land.rhs145
  %134 = load i64, i64* %dy1, align 8
  %cmp148 = icmp slt i64 %134, 0
  br label %cond.end159

cond.false149:                                    ; preds = %land.rhs145
  %135 = load i64, i64* %dx1, align 8
  %cmp150 = icmp slt i64 %135, 0
  br i1 %cmp150, label %land.rhs151, label %land.end158

land.rhs151:                                      ; preds = %cond.false149
  %136 = load i64, i64* %dy1, align 8
  %137 = load i64, i64* %dx1, align 8
  %call153 = call %"class.btConvexHullInternal::Rational64"* @_ZN20btConvexHullInternal10Rational64C2Exx(%"class.btConvexHullInternal::Rational64"* %ref.tmp152, i64 %136, i64 %137)
  %138 = load i64, i64* %dy, align 8
  %139 = load i64, i64* %dx, align 8
  %call155 = call %"class.btConvexHullInternal::Rational64"* @_ZN20btConvexHullInternal10Rational64C2Exx(%"class.btConvexHullInternal::Rational64"* %ref.tmp154, i64 %138, i64 %139)
  %call156 = call i32 @_ZNK20btConvexHullInternal10Rational647compareERKS0_(%"class.btConvexHullInternal::Rational64"* %ref.tmp152, %"class.btConvexHullInternal::Rational64"* nonnull align 8 dereferenceable(20) %ref.tmp154)
  %cmp157 = icmp sgt i32 %call156, 0
  br label %land.end158

land.end158:                                      ; preds = %land.rhs151, %cond.false149
  %140 = phi i1 [ false, %cond.false149 ], [ %cmp157, %land.rhs151 ]
  br label %cond.end159

cond.end159:                                      ; preds = %land.end158, %cond.true147
  %cond160 = phi i1 [ %cmp148, %cond.true147 ], [ %140, %land.end158 ]
  br label %land.end161

land.end161:                                      ; preds = %cond.end159, %if.then137
  %141 = phi i1 [ false, %if.then137 ], [ %cond160, %cond.end159 ]
  br i1 %141, label %if.then162, label %if.end165

if.then162:                                       ; preds = %land.end161
  %142 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f1, align 4
  %143 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e1.addr, align 4
  store %"class.btConvexHullInternal::Edge"* %142, %"class.btConvexHullInternal::Edge"** %143, align 4
  %144 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e1.addr, align 4
  %145 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %144, align 4
  %target163 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %145, i32 0, i32 3
  %146 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target163, align 4
  %point164 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %146, i32 0, i32 7
  %147 = bitcast %"class.btConvexHullInternal::Point32"* %et1 to i8*
  %148 = bitcast %"class.btConvexHullInternal::Point32"* %point164 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %147, i8* align 8 %148, i32 16, i1 false)
  %149 = load i64, i64* %dxn, align 8
  store i64 %149, i64* %dx, align 8
  br label %while.body75

if.end165:                                        ; preds = %land.end161
  br label %if.end166

if.else:                                          ; preds = %if.then132
  br label %if.end166

if.end166:                                        ; preds = %if.else, %if.end165
  br label %if.end167

if.end167:                                        ; preds = %if.end166, %if.then126
  br label %if.end168

if.end168:                                        ; preds = %if.end167, %land.lhs.true123, %if.end121
  br label %while.end169

while.end169:                                     ; preds = %if.end168
  br label %if.end282

if.else170:                                       ; preds = %if.end71
  %150 = load i64, i64* %dx, align 8
  %cmp171 = icmp slt i64 %150, 0
  br i1 %cmp171, label %if.then172, label %if.end281

if.then172:                                       ; preds = %if.else170
  br label %while.body174

while.body174:                                    ; preds = %if.then172, %cond.end223, %if.then272
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp176, %"class.btConvexHullInternal::Point32"* %et1, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %et0)
  %call177 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKS0_(%"class.btConvexHullInternal::Point32"* %ref.tmp176, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %s)
  store i64 %call177, i64* %dy175, align 8
  %151 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e1.addr, align 4
  %152 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %151, align 4
  %tobool178 = icmp ne %"class.btConvexHullInternal::Edge"* %152, null
  br i1 %tobool178, label %land.lhs.true179, label %if.end227

land.lhs.true179:                                 ; preds = %while.body174
  %153 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e1.addr, align 4
  %154 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %153, align 4
  %target180 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %154, i32 0, i32 3
  %155 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target180, align 4
  %156 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %stop1.addr, align 4
  %cmp181 = icmp ne %"class.btConvexHullInternal::Vertex"* %155, %156
  br i1 %cmp181, label %if.then182, label %if.end227

if.then182:                                       ; preds = %land.lhs.true179
  %157 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e1.addr, align 4
  %158 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %157, align 4
  %prev184 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %158, i32 0, i32 1
  %159 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev184, align 4
  %reverse185 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %159, i32 0, i32 2
  %160 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse185, align 4
  store %"class.btConvexHullInternal::Edge"* %160, %"class.btConvexHullInternal::Edge"** %f1183, align 4
  %161 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f1183, align 4
  %copy186 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %161, i32 0, i32 5
  %162 = load i32, i32* %copy186, align 4
  %mergeStamp187 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 6
  %163 = load i32, i32* %mergeStamp187, align 4
  %cmp188 = icmp sgt i32 %162, %163
  br i1 %cmp188, label %if.then189, label %if.end226

if.then189:                                       ; preds = %if.then182
  %164 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f1183, align 4
  %target192 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %164, i32 0, i32 3
  %165 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target192, align 4
  %point193 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %165, i32 0, i32 7
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp191, %"class.btConvexHullInternal::Point32"* %point193, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %et1)
  %call194 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %ref.tmp191, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %perp)
  store i64 %call194, i64* %dx1190, align 8
  %166 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f1183, align 4
  %target197 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %166, i32 0, i32 3
  %167 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target197, align 4
  %point198 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %167, i32 0, i32 7
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp196, %"class.btConvexHullInternal::Point32"* %point198, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %et1)
  %call199 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKS0_(%"class.btConvexHullInternal::Point32"* %ref.tmp196, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %s)
  store i64 %call199, i64* %dy1195, align 8
  %168 = load i64, i64* %dx1190, align 8
  %cmp200 = icmp eq i64 %168, 0
  br i1 %cmp200, label %cond.true201, label %cond.false203

cond.true201:                                     ; preds = %if.then189
  %169 = load i64, i64* %dy1195, align 8
  %cmp202 = icmp sgt i64 %169, 0
  br label %cond.end213

cond.false203:                                    ; preds = %if.then189
  %170 = load i64, i64* %dx1190, align 8
  %cmp204 = icmp slt i64 %170, 0
  br i1 %cmp204, label %land.rhs205, label %land.end212

land.rhs205:                                      ; preds = %cond.false203
  %171 = load i64, i64* %dy1195, align 8
  %172 = load i64, i64* %dx1190, align 8
  %call207 = call %"class.btConvexHullInternal::Rational64"* @_ZN20btConvexHullInternal10Rational64C2Exx(%"class.btConvexHullInternal::Rational64"* %ref.tmp206, i64 %171, i64 %172)
  %173 = load i64, i64* %dy175, align 8
  %174 = load i64, i64* %dx, align 8
  %call209 = call %"class.btConvexHullInternal::Rational64"* @_ZN20btConvexHullInternal10Rational64C2Exx(%"class.btConvexHullInternal::Rational64"* %ref.tmp208, i64 %173, i64 %174)
  %call210 = call i32 @_ZNK20btConvexHullInternal10Rational647compareERKS0_(%"class.btConvexHullInternal::Rational64"* %ref.tmp206, %"class.btConvexHullInternal::Rational64"* nonnull align 8 dereferenceable(20) %ref.tmp208)
  %cmp211 = icmp sle i32 %call210, 0
  br label %land.end212

land.end212:                                      ; preds = %land.rhs205, %cond.false203
  %175 = phi i1 [ false, %cond.false203 ], [ %cmp211, %land.rhs205 ]
  br label %cond.end213

cond.end213:                                      ; preds = %land.end212, %cond.true201
  %cond214 = phi i1 [ %cmp202, %cond.true201 ], [ %175, %land.end212 ]
  br i1 %cond214, label %if.then215, label %if.end225

if.then215:                                       ; preds = %cond.end213
  %176 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f1183, align 4
  %target216 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %176, i32 0, i32 3
  %177 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target216, align 4
  %point217 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %177, i32 0, i32 7
  %178 = bitcast %"class.btConvexHullInternal::Point32"* %et1 to i8*
  %179 = bitcast %"class.btConvexHullInternal::Point32"* %point217 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %178, i8* align 8 %179, i32 16, i1 false)
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp218, %"class.btConvexHullInternal::Point32"* %et1, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %et0)
  %call219 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %ref.tmp218, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %perp)
  store i64 %call219, i64* %dx, align 8
  %180 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e1.addr, align 4
  %181 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %180, align 4
  %182 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %start1, align 4
  %cmp220 = icmp eq %"class.btConvexHullInternal::Edge"* %181, %182
  br i1 %cmp220, label %cond.true221, label %cond.false222

cond.true221:                                     ; preds = %if.then215
  br label %cond.end223

cond.false222:                                    ; preds = %if.then215
  %183 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f1183, align 4
  br label %cond.end223

cond.end223:                                      ; preds = %cond.false222, %cond.true221
  %cond224 = phi %"class.btConvexHullInternal::Edge"* [ null, %cond.true221 ], [ %183, %cond.false222 ]
  %184 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e1.addr, align 4
  store %"class.btConvexHullInternal::Edge"* %cond224, %"class.btConvexHullInternal::Edge"** %184, align 4
  br label %while.body174

if.end225:                                        ; preds = %cond.end213
  br label %if.end226

if.end226:                                        ; preds = %if.end225, %if.then182
  br label %if.end227

if.end227:                                        ; preds = %if.end226, %land.lhs.true179, %while.body174
  %185 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e0.addr, align 4
  %186 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %185, align 4
  %tobool228 = icmp ne %"class.btConvexHullInternal::Edge"* %186, null
  br i1 %tobool228, label %land.lhs.true229, label %if.end279

land.lhs.true229:                                 ; preds = %if.end227
  %187 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e0.addr, align 4
  %188 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %187, align 4
  %target230 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %188, i32 0, i32 3
  %189 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target230, align 4
  %190 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %stop0.addr, align 4
  %cmp231 = icmp ne %"class.btConvexHullInternal::Vertex"* %189, %190
  br i1 %cmp231, label %if.then232, label %if.end279

if.then232:                                       ; preds = %land.lhs.true229
  %191 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e0.addr, align 4
  %192 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %191, align 4
  %reverse234 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %192, i32 0, i32 2
  %193 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse234, align 4
  %prev235 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %193, i32 0, i32 1
  %194 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev235, align 4
  store %"class.btConvexHullInternal::Edge"* %194, %"class.btConvexHullInternal::Edge"** %f0233, align 4
  %195 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f0233, align 4
  %copy236 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %195, i32 0, i32 5
  %196 = load i32, i32* %copy236, align 4
  %mergeStamp237 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 6
  %197 = load i32, i32* %mergeStamp237, align 4
  %cmp238 = icmp sgt i32 %196, %197
  br i1 %cmp238, label %if.then239, label %if.end278

if.then239:                                       ; preds = %if.then232
  %198 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f0233, align 4
  %target240 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %198, i32 0, i32 3
  %199 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target240, align 4
  %point241 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %199, i32 0, i32 7
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %d0, %"class.btConvexHullInternal::Point32"* %point241, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %et0)
  %call242 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %d0, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %normal)
  %cmp243 = icmp eq i64 %call242, 0
  br i1 %cmp243, label %if.then244, label %if.else276

if.then244:                                       ; preds = %if.then239
  %call246 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %d0, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %perp)
  store i64 %call246, i64* %dx0245, align 8
  %call248 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKS0_(%"class.btConvexHullInternal::Point32"* %d0, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %s)
  store i64 %call248, i64* %dy0247, align 8
  %200 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f0233, align 4
  %target251 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %200, i32 0, i32 3
  %201 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target251, align 4
  %point252 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %201, i32 0, i32 7
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp250, %"class.btConvexHullInternal::Point32"* %et1, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %point252)
  %call253 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %ref.tmp250, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %perp)
  store i64 %call253, i64* %dxn249, align 8
  %202 = load i64, i64* %dxn249, align 8
  %cmp254 = icmp slt i64 %202, 0
  br i1 %cmp254, label %land.rhs255, label %land.end271

land.rhs255:                                      ; preds = %if.then244
  %203 = load i64, i64* %dx0245, align 8
  %cmp256 = icmp eq i64 %203, 0
  br i1 %cmp256, label %cond.true257, label %cond.false259

cond.true257:                                     ; preds = %land.rhs255
  %204 = load i64, i64* %dy0247, align 8
  %cmp258 = icmp sgt i64 %204, 0
  br label %cond.end269

cond.false259:                                    ; preds = %land.rhs255
  %205 = load i64, i64* %dx0245, align 8
  %cmp260 = icmp slt i64 %205, 0
  br i1 %cmp260, label %land.rhs261, label %land.end268

land.rhs261:                                      ; preds = %cond.false259
  %206 = load i64, i64* %dy0247, align 8
  %207 = load i64, i64* %dx0245, align 8
  %call263 = call %"class.btConvexHullInternal::Rational64"* @_ZN20btConvexHullInternal10Rational64C2Exx(%"class.btConvexHullInternal::Rational64"* %ref.tmp262, i64 %206, i64 %207)
  %208 = load i64, i64* %dy175, align 8
  %209 = load i64, i64* %dx, align 8
  %call265 = call %"class.btConvexHullInternal::Rational64"* @_ZN20btConvexHullInternal10Rational64C2Exx(%"class.btConvexHullInternal::Rational64"* %ref.tmp264, i64 %208, i64 %209)
  %call266 = call i32 @_ZNK20btConvexHullInternal10Rational647compareERKS0_(%"class.btConvexHullInternal::Rational64"* %ref.tmp262, %"class.btConvexHullInternal::Rational64"* nonnull align 8 dereferenceable(20) %ref.tmp264)
  %cmp267 = icmp slt i32 %call266, 0
  br label %land.end268

land.end268:                                      ; preds = %land.rhs261, %cond.false259
  %210 = phi i1 [ false, %cond.false259 ], [ %cmp267, %land.rhs261 ]
  br label %cond.end269

cond.end269:                                      ; preds = %land.end268, %cond.true257
  %cond270 = phi i1 [ %cmp258, %cond.true257 ], [ %210, %land.end268 ]
  br label %land.end271

land.end271:                                      ; preds = %cond.end269, %if.then244
  %211 = phi i1 [ false, %if.then244 ], [ %cond270, %cond.end269 ]
  br i1 %211, label %if.then272, label %if.end275

if.then272:                                       ; preds = %land.end271
  %212 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f0233, align 4
  %213 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e0.addr, align 4
  store %"class.btConvexHullInternal::Edge"* %212, %"class.btConvexHullInternal::Edge"** %213, align 4
  %214 = load %"class.btConvexHullInternal::Edge"**, %"class.btConvexHullInternal::Edge"*** %e0.addr, align 4
  %215 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %214, align 4
  %target273 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %215, i32 0, i32 3
  %216 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target273, align 4
  %point274 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %216, i32 0, i32 7
  %217 = bitcast %"class.btConvexHullInternal::Point32"* %et0 to i8*
  %218 = bitcast %"class.btConvexHullInternal::Point32"* %point274 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %217, i8* align 8 %218, i32 16, i1 false)
  %219 = load i64, i64* %dxn249, align 8
  store i64 %219, i64* %dx, align 8
  br label %while.body174

if.end275:                                        ; preds = %land.end271
  br label %if.end277

if.else276:                                       ; preds = %if.then239
  br label %if.end277

if.end277:                                        ; preds = %if.else276, %if.end275
  br label %if.end278

if.end278:                                        ; preds = %if.end277, %if.then232
  br label %if.end279

if.end279:                                        ; preds = %if.end278, %land.lhs.true229, %if.end227
  br label %while.end280

while.end280:                                     ; preds = %if.end279
  br label %if.end281

if.end281:                                        ; preds = %while.end280, %if.else170
  br label %if.end282

if.end282:                                        ; preds = %if.end281, %while.end169
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* noalias sret align 4 %agg.result, %"class.btConvexHullInternal::Point32"* %this, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %b) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  store %"class.btConvexHullInternal::Point32"* %this, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  store %"class.btConvexHullInternal::Point32"* %b, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  %x = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 0
  %0 = load i32, i32* %x, align 4
  %1 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %x2 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %1, i32 0, i32 0
  %2 = load i32, i32* %x2, align 4
  %sub = sub nsw i32 %0, %2
  %y = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 1
  %3 = load i32, i32* %y, align 4
  %4 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %y3 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %4, i32 0, i32 1
  %5 = load i32, i32* %y3, align 4
  %sub4 = sub nsw i32 %3, %5
  %z = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 2
  %6 = load i32, i32* %z, align 4
  %7 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %z5 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %7, i32 0, i32 2
  %8 = load i32, i32* %z5, align 4
  %sub6 = sub nsw i32 %6, %8
  %call = call %"class.btConvexHullInternal::Point32"* @_ZN20btConvexHullInternal7Point32C2Eiii(%"class.btConvexHullInternal::Point32"* %agg.result, i32 %sub, i32 %sub4, i32 %sub6)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btConvexHullInternal7Point325crossERKNS_7Point64E(%"class.btConvexHullInternal::Point64"* noalias sret align 8 %agg.result, %"class.btConvexHullInternal::Point32"* %this, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %b) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Point64"*, align 4
  store %"class.btConvexHullInternal::Point32"* %this, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  store %"class.btConvexHullInternal::Point64"* %b, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  %y = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 1
  %0 = load i32, i32* %y, align 4
  %conv = sext i32 %0 to i64
  %1 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %z = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %1, i32 0, i32 2
  %2 = load i64, i64* %z, align 8
  %mul = mul nsw i64 %conv, %2
  %z2 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 2
  %3 = load i32, i32* %z2, align 4
  %conv3 = sext i32 %3 to i64
  %4 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %y4 = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %4, i32 0, i32 1
  %5 = load i64, i64* %y4, align 8
  %mul5 = mul nsw i64 %conv3, %5
  %sub = sub nsw i64 %mul, %mul5
  %z6 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 2
  %6 = load i32, i32* %z6, align 4
  %conv7 = sext i32 %6 to i64
  %7 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %x = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %7, i32 0, i32 0
  %8 = load i64, i64* %x, align 8
  %mul8 = mul nsw i64 %conv7, %8
  %x9 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 0
  %9 = load i32, i32* %x9, align 4
  %conv10 = sext i32 %9 to i64
  %10 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %z11 = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %10, i32 0, i32 2
  %11 = load i64, i64* %z11, align 8
  %mul12 = mul nsw i64 %conv10, %11
  %sub13 = sub nsw i64 %mul8, %mul12
  %x14 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 0
  %12 = load i32, i32* %x14, align 4
  %conv15 = sext i32 %12 to i64
  %13 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %y16 = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %13, i32 0, i32 1
  %14 = load i64, i64* %y16, align 8
  %mul17 = mul nsw i64 %conv15, %14
  %y18 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 1
  %15 = load i32, i32* %y18, align 4
  %conv19 = sext i32 %15 to i64
  %16 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %x20 = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %16, i32 0, i32 0
  %17 = load i64, i64* %x20, align 8
  %mul21 = mul nsw i64 %conv19, %17
  %sub22 = sub nsw i64 %mul17, %mul21
  %call = call %"class.btConvexHullInternal::Point64"* @_ZN20btConvexHullInternal7Point64C2Exxx(%"class.btConvexHullInternal::Point64"* %agg.result, i64 %sub, i64 %sub13, i64 %sub22)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK20btConvexHullInternal7Point323dotERKS0_(%"class.btConvexHullInternal::Point32"* %this, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %b) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  store %"class.btConvexHullInternal::Point32"* %this, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  store %"class.btConvexHullInternal::Point32"* %b, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  %x = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 0
  %0 = load i32, i32* %x, align 4
  %1 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %x2 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %1, i32 0, i32 0
  %2 = load i32, i32* %x2, align 4
  %mul = mul nsw i32 %0, %2
  %y = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 1
  %3 = load i32, i32* %y, align 4
  %4 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %y3 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %4, i32 0, i32 1
  %5 = load i32, i32* %y3, align 4
  %mul4 = mul nsw i32 %3, %5
  %add = add nsw i32 %mul, %mul4
  %z = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 2
  %6 = load i32, i32* %z, align 4
  %7 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %z5 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %7, i32 0, i32 2
  %8 = load i32, i32* %z5, align 4
  %mul6 = mul nsw i32 %6, %8
  %add7 = add nsw i32 %add, %mul6
  %conv = sext i32 %add7 to i64
  ret i64 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Point32"* @_ZN20btConvexHullInternal7Point32C2Ev(%"class.btConvexHullInternal::Point32"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  store %"class.btConvexHullInternal::Point32"* %this, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  ret %"class.btConvexHullInternal::Point32"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Point32"* @_ZN20btConvexHullInternal7Point32C2Eiii(%"class.btConvexHullInternal::Point32"* returned %this, i32 %x, i32 %y, i32 %z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  %x.addr = alloca i32, align 4
  %y.addr = alloca i32, align 4
  %z.addr = alloca i32, align 4
  store %"class.btConvexHullInternal::Point32"* %this, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  store i32 %x, i32* %x.addr, align 4
  store i32 %y, i32* %y.addr, align 4
  store i32 %z, i32* %z.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  %x2 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 0
  %0 = load i32, i32* %x.addr, align 4
  store i32 %0, i32* %x2, align 4
  %y3 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 1
  %1 = load i32, i32* %y.addr, align 4
  store i32 %1, i32* %y3, align 4
  %z4 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 2
  %2 = load i32, i32* %z.addr, align 4
  store i32 %2, i32* %z4, align 4
  %index = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 3
  store i32 -1, i32* %index, align 4
  ret %"class.btConvexHullInternal::Point32"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK20btConvexHullInternal10Rational6418isNegativeInfinityEv(%"class.btConvexHullInternal::Rational64"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Rational64"*, align 4
  store %"class.btConvexHullInternal::Rational64"* %this, %"class.btConvexHullInternal::Rational64"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Rational64"*, %"class.btConvexHullInternal::Rational64"** %this.addr, align 4
  %sign = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 2
  %0 = load i32, i32* %sign, align 8
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %m_denominator = getelementptr inbounds %"class.btConvexHullInternal::Rational64", %"class.btConvexHullInternal::Rational64"* %this1, i32 0, i32 1
  %1 = load i64, i64* %m_denominator, align 8
  %cmp2 = icmp eq i64 %1, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %cmp2, %land.rhs ]
  ret i1 %2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btConvexHullInternal14removeEdgePairEPNS_4EdgeE(%class.btConvexHullInternal* %this, %"class.btConvexHullInternal::Edge"* %edge) #2 comdat {
entry:
  %this.addr = alloca %class.btConvexHullInternal*, align 4
  %edge.addr = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %n = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %r = alloca %"class.btConvexHullInternal::Edge"*, align 4
  store %class.btConvexHullInternal* %this, %class.btConvexHullInternal** %this.addr, align 4
  store %"class.btConvexHullInternal::Edge"* %edge, %"class.btConvexHullInternal::Edge"** %edge.addr, align 4
  %this1 = load %class.btConvexHullInternal*, %class.btConvexHullInternal** %this.addr, align 4
  %0 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edge.addr, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %0, i32 0, i32 0
  %1 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next, align 4
  store %"class.btConvexHullInternal::Edge"* %1, %"class.btConvexHullInternal::Edge"** %n, align 4
  %2 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edge.addr, align 4
  %reverse = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %2, i32 0, i32 2
  %3 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse, align 4
  store %"class.btConvexHullInternal::Edge"* %3, %"class.btConvexHullInternal::Edge"** %r, align 4
  %4 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %n, align 4
  %5 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edge.addr, align 4
  %cmp = icmp ne %"class.btConvexHullInternal::Edge"* %4, %5
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edge.addr, align 4
  %prev = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %6, i32 0, i32 1
  %7 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev, align 4
  %8 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %n, align 4
  %prev2 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %8, i32 0, i32 1
  store %"class.btConvexHullInternal::Edge"* %7, %"class.btConvexHullInternal::Edge"** %prev2, align 4
  %9 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %n, align 4
  %10 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edge.addr, align 4
  %prev3 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %10, i32 0, i32 1
  %11 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev3, align 4
  %next4 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %11, i32 0, i32 0
  store %"class.btConvexHullInternal::Edge"* %9, %"class.btConvexHullInternal::Edge"** %next4, align 4
  %12 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %n, align 4
  %13 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %r, align 4
  %target = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %13, i32 0, i32 3
  %14 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target, align 4
  %edges = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %14, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* %12, %"class.btConvexHullInternal::Edge"** %edges, align 8
  br label %if.end

if.else:                                          ; preds = %entry
  %15 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %r, align 4
  %target5 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %15, i32 0, i32 3
  %16 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target5, align 4
  %edges6 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %16, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %edges6, align 8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %17 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %r, align 4
  %next7 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %17, i32 0, i32 0
  %18 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next7, align 4
  store %"class.btConvexHullInternal::Edge"* %18, %"class.btConvexHullInternal::Edge"** %n, align 4
  %19 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %n, align 4
  %20 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %r, align 4
  %cmp8 = icmp ne %"class.btConvexHullInternal::Edge"* %19, %20
  br i1 %cmp8, label %if.then9, label %if.else16

if.then9:                                         ; preds = %if.end
  %21 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %r, align 4
  %prev10 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %21, i32 0, i32 1
  %22 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev10, align 4
  %23 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %n, align 4
  %prev11 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %23, i32 0, i32 1
  store %"class.btConvexHullInternal::Edge"* %22, %"class.btConvexHullInternal::Edge"** %prev11, align 4
  %24 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %n, align 4
  %25 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %r, align 4
  %prev12 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %25, i32 0, i32 1
  %26 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev12, align 4
  %next13 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %26, i32 0, i32 0
  store %"class.btConvexHullInternal::Edge"* %24, %"class.btConvexHullInternal::Edge"** %next13, align 4
  %27 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %n, align 4
  %28 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edge.addr, align 4
  %target14 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %28, i32 0, i32 3
  %29 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target14, align 4
  %edges15 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %29, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* %27, %"class.btConvexHullInternal::Edge"** %edges15, align 8
  br label %if.end19

if.else16:                                        ; preds = %if.end
  %30 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edge.addr, align 4
  %target17 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %30, i32 0, i32 3
  %31 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target17, align 4
  %edges18 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %31, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %edges18, align 8
  br label %if.end19

if.end19:                                         ; preds = %if.else16, %if.then9
  %edgePool = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 3
  %32 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edge.addr, align 4
  call void @_ZN20btConvexHullInternal4PoolINS_4EdgeEE10freeObjectEPS1_(%"class.btConvexHullInternal::Pool.0"* %edgePool, %"class.btConvexHullInternal::Edge"* %32)
  %edgePool20 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 3
  %33 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %r, align 4
  call void @_ZN20btConvexHullInternal4PoolINS_4EdgeEE10freeObjectEPS1_(%"class.btConvexHullInternal::Pool.0"* %edgePool20, %"class.btConvexHullInternal::Edge"* %33)
  %usedEdgePairs = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 10
  %34 = load i32, i32* %usedEdgePairs, align 4
  %dec = add nsw i32 %34, -1
  store i32 %dec, i32* %usedEdgePairs, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN20btConvexHullInternal7computeEPKvbii(%class.btConvexHullInternal* %this, i8* %coords, i1 zeroext %doubleCoords, i32 %stride, i32 %count) #2 {
entry:
  %this.addr = alloca %class.btConvexHullInternal*, align 4
  %coords.addr = alloca i8*, align 4
  %doubleCoords.addr = alloca i8, align 1
  %stride.addr = alloca i32, align 4
  %count.addr = alloca i32, align 4
  %min = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %max = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ptr = alloca i8*, align 4
  %i = alloca i32, align 4
  %v = alloca double*, align 4
  %p = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %i16 = alloca i32, align 4
  %v20 = alloca float*, align 4
  %p21 = alloca %class.btVector3, align 4
  %s = alloca %class.btVector3, align 4
  %ref.tmp42 = alloca float, align 4
  %ref.tmp50 = alloca float, align 4
  %ref.tmp82 = alloca %class.btVector3, align 4
  %ref.tmp83 = alloca %class.btVector3, align 4
  %ref.tmp84 = alloca float, align 4
  %points = alloca %class.btAlignedObjectArray.4, align 4
  %ref.tmp86 = alloca %"class.btConvexHullInternal::Point32", align 4
  %i90 = alloca i32, align 4
  %v94 = alloca double*, align 4
  %p95 = alloca %class.btVector3, align 4
  %ref.tmp96 = alloca float, align 4
  %ref.tmp99 = alloca float, align 4
  %ref.tmp102 = alloca float, align 4
  %ref.tmp107 = alloca %class.btVector3, align 4
  %ref.tmp108 = alloca %class.btVector3, align 4
  %i130 = alloca i32, align 4
  %v134 = alloca float*, align 4
  %p135 = alloca %class.btVector3, align 4
  %ref.tmp141 = alloca %class.btVector3, align 4
  %ref.tmp142 = alloca %class.btVector3, align 4
  %ref.tmp168 = alloca %class.pointCmp, align 1
  %ref.tmp170 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %i171 = alloca i32, align 4
  %v175 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %hull = alloca %"class.btConvexHullInternal::IntermediateHull", align 4
  store %class.btConvexHullInternal* %this, %class.btConvexHullInternal** %this.addr, align 4
  store i8* %coords, i8** %coords.addr, align 4
  %frombool = zext i1 %doubleCoords to i8
  store i8 %frombool, i8* %doubleCoords.addr, align 1
  store i32 %stride, i32* %stride.addr, align 4
  store i32 %count, i32* %count.addr, align 4
  %this1 = load %class.btConvexHullInternal*, %class.btConvexHullInternal** %this.addr, align 4
  store float 0x46293E5940000000, float* %ref.tmp, align 4
  store float 0x46293E5940000000, float* %ref.tmp2, align 4
  store float 0x46293E5940000000, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %min, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  store float 0xC6293E5940000000, float* %ref.tmp4, align 4
  store float 0xC6293E5940000000, float* %ref.tmp5, align 4
  store float 0xC6293E5940000000, float* %ref.tmp6, align 4
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %max, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %0 = load i8*, i8** %coords.addr, align 4
  store i8* %0, i8** %ptr, align 4
  %1 = load i8, i8* %doubleCoords.addr, align 1
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %2 = load i32, i32* %i, align 4
  %3 = load i32, i32* %count.addr, align 4
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8*, i8** %ptr, align 4
  %5 = bitcast i8* %4 to double*
  store double* %5, double** %v, align 4
  %6 = load double*, double** %v, align 4
  %arrayidx = getelementptr inbounds double, double* %6, i32 0
  %7 = load double, double* %arrayidx, align 8
  %conv = fptrunc double %7 to float
  store float %conv, float* %ref.tmp8, align 4
  %8 = load double*, double** %v, align 4
  %arrayidx10 = getelementptr inbounds double, double* %8, i32 1
  %9 = load double, double* %arrayidx10, align 8
  %conv11 = fptrunc double %9 to float
  store float %conv11, float* %ref.tmp9, align 4
  %10 = load double*, double** %v, align 4
  %arrayidx13 = getelementptr inbounds double, double* %10, i32 2
  %11 = load double, double* %arrayidx13, align 8
  %conv14 = fptrunc double %11 to float
  store float %conv14, float* %ref.tmp12, align 4
  %call15 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %p, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp12)
  %12 = load i32, i32* %stride.addr, align 4
  %13 = load i8*, i8** %ptr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %13, i32 %12
  store i8* %add.ptr, i8** %ptr, align 4
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %min, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %max, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %i, align 4
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.else:                                          ; preds = %entry
  store i32 0, i32* %i16, align 4
  br label %for.cond17

for.cond17:                                       ; preds = %for.inc27, %if.else
  %15 = load i32, i32* %i16, align 4
  %16 = load i32, i32* %count.addr, align 4
  %cmp18 = icmp slt i32 %15, %16
  br i1 %cmp18, label %for.body19, label %for.end29

for.body19:                                       ; preds = %for.cond17
  %17 = load i8*, i8** %ptr, align 4
  %18 = bitcast i8* %17 to float*
  store float* %18, float** %v20, align 4
  %19 = load float*, float** %v20, align 4
  %arrayidx22 = getelementptr inbounds float, float* %19, i32 0
  %20 = load float*, float** %v20, align 4
  %arrayidx23 = getelementptr inbounds float, float* %20, i32 1
  %21 = load float*, float** %v20, align 4
  %arrayidx24 = getelementptr inbounds float, float* %21, i32 2
  %call25 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %p21, float* nonnull align 4 dereferenceable(4) %arrayidx22, float* nonnull align 4 dereferenceable(4) %arrayidx23, float* nonnull align 4 dereferenceable(4) %arrayidx24)
  %22 = load i32, i32* %stride.addr, align 4
  %23 = load i8*, i8** %ptr, align 4
  %add.ptr26 = getelementptr inbounds i8, i8* %23, i32 %22
  store i8* %add.ptr26, i8** %ptr, align 4
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %min, %class.btVector3* nonnull align 4 dereferenceable(16) %p21)
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %max, %class.btVector3* nonnull align 4 dereferenceable(16) %p21)
  br label %for.inc27

for.inc27:                                        ; preds = %for.body19
  %24 = load i32, i32* %i16, align 4
  %inc28 = add nsw i32 %24, 1
  store i32 %inc28, i32* %i16, align 4
  br label %for.cond17

for.end29:                                        ; preds = %for.cond17
  br label %if.end

if.end:                                           ; preds = %for.end29, %for.end
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %s, %class.btVector3* nonnull align 4 dereferenceable(16) %max, %class.btVector3* nonnull align 4 dereferenceable(16) %min)
  %call30 = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %s)
  %maxAxis = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 9
  store i32 %call30, i32* %maxAxis, align 4
  %call31 = call i32 @_ZNK9btVector37minAxisEv(%class.btVector3* %s)
  %minAxis = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 7
  store i32 %call31, i32* %minAxis, align 4
  %minAxis32 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 7
  %25 = load i32, i32* %minAxis32, align 4
  %maxAxis33 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 9
  %26 = load i32, i32* %maxAxis33, align 4
  %cmp34 = icmp eq i32 %25, %26
  br i1 %cmp34, label %if.then35, label %if.end38

if.then35:                                        ; preds = %if.end
  %maxAxis36 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 9
  %27 = load i32, i32* %maxAxis36, align 4
  %add = add nsw i32 %27, 1
  %rem = srem i32 %add, 3
  %minAxis37 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 7
  store i32 %rem, i32* %minAxis37, align 4
  br label %if.end38

if.end38:                                         ; preds = %if.then35, %if.end
  %maxAxis39 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 9
  %28 = load i32, i32* %maxAxis39, align 4
  %sub = sub nsw i32 3, %28
  %minAxis40 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 7
  %29 = load i32, i32* %minAxis40, align 4
  %sub41 = sub nsw i32 %sub, %29
  %medAxis = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 8
  store i32 %sub41, i32* %medAxis, align 4
  store float 1.021600e+04, float* %ref.tmp42, align 4
  %call43 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %s, float* nonnull align 4 dereferenceable(4) %ref.tmp42)
  %medAxis44 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 8
  %30 = load i32, i32* %medAxis44, align 4
  %add45 = add nsw i32 %30, 1
  %rem46 = srem i32 %add45, 3
  %maxAxis47 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 9
  %31 = load i32, i32* %maxAxis47, align 4
  %cmp48 = icmp ne i32 %rem46, %31
  br i1 %cmp48, label %if.then49, label %if.end52

if.then49:                                        ; preds = %if.end38
  store float -1.000000e+00, float* %ref.tmp50, align 4
  %call51 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %s, float* nonnull align 4 dereferenceable(4) %ref.tmp50)
  br label %if.end52

if.end52:                                         ; preds = %if.then49, %if.end38
  %scaling = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 0
  %32 = bitcast %class.btVector3* %scaling to i8*
  %33 = bitcast %class.btVector3* %s to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %32, i8* align 4 %33, i32 16, i1 false)
  %call53 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %s)
  %arrayidx54 = getelementptr inbounds float, float* %call53, i32 0
  %34 = load float, float* %arrayidx54, align 4
  %cmp55 = fcmp une float %34, 0.000000e+00
  br i1 %cmp55, label %if.then56, label %if.end61

if.then56:                                        ; preds = %if.end52
  %call57 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %s)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 0
  %35 = load float, float* %arrayidx58, align 4
  %div = fdiv float 1.000000e+00, %35
  %call59 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %s)
  %arrayidx60 = getelementptr inbounds float, float* %call59, i32 0
  store float %div, float* %arrayidx60, align 4
  br label %if.end61

if.end61:                                         ; preds = %if.then56, %if.end52
  %call62 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %s)
  %arrayidx63 = getelementptr inbounds float, float* %call62, i32 1
  %36 = load float, float* %arrayidx63, align 4
  %cmp64 = fcmp une float %36, 0.000000e+00
  br i1 %cmp64, label %if.then65, label %if.end71

if.then65:                                        ; preds = %if.end61
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %s)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 1
  %37 = load float, float* %arrayidx67, align 4
  %div68 = fdiv float 1.000000e+00, %37
  %call69 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %s)
  %arrayidx70 = getelementptr inbounds float, float* %call69, i32 1
  store float %div68, float* %arrayidx70, align 4
  br label %if.end71

if.end71:                                         ; preds = %if.then65, %if.end61
  %call72 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %s)
  %arrayidx73 = getelementptr inbounds float, float* %call72, i32 2
  %38 = load float, float* %arrayidx73, align 4
  %cmp74 = fcmp une float %38, 0.000000e+00
  br i1 %cmp74, label %if.then75, label %if.end81

if.then75:                                        ; preds = %if.end71
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %s)
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 2
  %39 = load float, float* %arrayidx77, align 4
  %div78 = fdiv float 1.000000e+00, %39
  %call79 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %s)
  %arrayidx80 = getelementptr inbounds float, float* %call79, i32 2
  store float %div78, float* %arrayidx80, align 4
  br label %if.end81

if.end81:                                         ; preds = %if.then75, %if.end71
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp83, %class.btVector3* nonnull align 4 dereferenceable(16) %min, %class.btVector3* nonnull align 4 dereferenceable(16) %max)
  store float 5.000000e-01, float* %ref.tmp84, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp82, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp83, float* nonnull align 4 dereferenceable(4) %ref.tmp84)
  %center = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 1
  %40 = bitcast %class.btVector3* %center to i8*
  %41 = bitcast %class.btVector3* %ref.tmp82 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %40, i8* align 4 %41, i32 16, i1 false)
  %call85 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EEC2Ev(%class.btAlignedObjectArray.4* %points)
  %42 = load i32, i32* %count.addr, align 4
  %call87 = call %"class.btConvexHullInternal::Point32"* @_ZN20btConvexHullInternal7Point32C2Ev(%"class.btConvexHullInternal::Point32"* %ref.tmp86)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE6resizeEiRKS1_(%class.btAlignedObjectArray.4* %points, i32 %42, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %ref.tmp86)
  %43 = load i8*, i8** %coords.addr, align 4
  store i8* %43, i8** %ptr, align 4
  %44 = load i8, i8* %doubleCoords.addr, align 1
  %tobool88 = trunc i8 %44 to i1
  br i1 %tobool88, label %if.then89, label %if.else129

if.then89:                                        ; preds = %if.end81
  store i32 0, i32* %i90, align 4
  br label %for.cond91

for.cond91:                                       ; preds = %for.inc126, %if.then89
  %45 = load i32, i32* %i90, align 4
  %46 = load i32, i32* %count.addr, align 4
  %cmp92 = icmp slt i32 %45, %46
  br i1 %cmp92, label %for.body93, label %for.end128

for.body93:                                       ; preds = %for.cond91
  %47 = load i8*, i8** %ptr, align 4
  %48 = bitcast i8* %47 to double*
  store double* %48, double** %v94, align 4
  %49 = load double*, double** %v94, align 4
  %arrayidx97 = getelementptr inbounds double, double* %49, i32 0
  %50 = load double, double* %arrayidx97, align 8
  %conv98 = fptrunc double %50 to float
  store float %conv98, float* %ref.tmp96, align 4
  %51 = load double*, double** %v94, align 4
  %arrayidx100 = getelementptr inbounds double, double* %51, i32 1
  %52 = load double, double* %arrayidx100, align 8
  %conv101 = fptrunc double %52 to float
  store float %conv101, float* %ref.tmp99, align 4
  %53 = load double*, double** %v94, align 4
  %arrayidx103 = getelementptr inbounds double, double* %53, i32 2
  %54 = load double, double* %arrayidx103, align 8
  %conv104 = fptrunc double %54 to float
  store float %conv104, float* %ref.tmp102, align 4
  %call105 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %p95, float* nonnull align 4 dereferenceable(4) %ref.tmp96, float* nonnull align 4 dereferenceable(4) %ref.tmp99, float* nonnull align 4 dereferenceable(4) %ref.tmp102)
  %55 = load i32, i32* %stride.addr, align 4
  %56 = load i8*, i8** %ptr, align 4
  %add.ptr106 = getelementptr inbounds i8, i8* %56, i32 %55
  store i8* %add.ptr106, i8** %ptr, align 4
  %center109 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp108, %class.btVector3* nonnull align 4 dereferenceable(16) %p95, %class.btVector3* nonnull align 4 dereferenceable(16) %center109)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp107, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp108, %class.btVector3* nonnull align 4 dereferenceable(16) %s)
  %57 = bitcast %class.btVector3* %p95 to i8*
  %58 = bitcast %class.btVector3* %ref.tmp107 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %57, i8* align 4 %58, i32 16, i1 false)
  %call110 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p95)
  %medAxis111 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 8
  %59 = load i32, i32* %medAxis111, align 4
  %arrayidx112 = getelementptr inbounds float, float* %call110, i32 %59
  %60 = load float, float* %arrayidx112, align 4
  %conv113 = fptosi float %60 to i32
  %61 = load i32, i32* %i90, align 4
  %call114 = call nonnull align 4 dereferenceable(16) %"class.btConvexHullInternal::Point32"* @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EEixEi(%class.btAlignedObjectArray.4* %points, i32 %61)
  %x = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %call114, i32 0, i32 0
  store i32 %conv113, i32* %x, align 4
  %call115 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p95)
  %maxAxis116 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 9
  %62 = load i32, i32* %maxAxis116, align 4
  %arrayidx117 = getelementptr inbounds float, float* %call115, i32 %62
  %63 = load float, float* %arrayidx117, align 4
  %conv118 = fptosi float %63 to i32
  %64 = load i32, i32* %i90, align 4
  %call119 = call nonnull align 4 dereferenceable(16) %"class.btConvexHullInternal::Point32"* @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EEixEi(%class.btAlignedObjectArray.4* %points, i32 %64)
  %y = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %call119, i32 0, i32 1
  store i32 %conv118, i32* %y, align 4
  %call120 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p95)
  %minAxis121 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 7
  %65 = load i32, i32* %minAxis121, align 4
  %arrayidx122 = getelementptr inbounds float, float* %call120, i32 %65
  %66 = load float, float* %arrayidx122, align 4
  %conv123 = fptosi float %66 to i32
  %67 = load i32, i32* %i90, align 4
  %call124 = call nonnull align 4 dereferenceable(16) %"class.btConvexHullInternal::Point32"* @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EEixEi(%class.btAlignedObjectArray.4* %points, i32 %67)
  %z = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %call124, i32 0, i32 2
  store i32 %conv123, i32* %z, align 4
  %68 = load i32, i32* %i90, align 4
  %69 = load i32, i32* %i90, align 4
  %call125 = call nonnull align 4 dereferenceable(16) %"class.btConvexHullInternal::Point32"* @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EEixEi(%class.btAlignedObjectArray.4* %points, i32 %69)
  %index = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %call125, i32 0, i32 3
  store i32 %68, i32* %index, align 4
  br label %for.inc126

for.inc126:                                       ; preds = %for.body93
  %70 = load i32, i32* %i90, align 4
  %inc127 = add nsw i32 %70, 1
  store i32 %inc127, i32* %i90, align 4
  br label %for.cond91

for.end128:                                       ; preds = %for.cond91
  br label %if.end167

if.else129:                                       ; preds = %if.end81
  store i32 0, i32* %i130, align 4
  br label %for.cond131

for.cond131:                                      ; preds = %for.inc164, %if.else129
  %71 = load i32, i32* %i130, align 4
  %72 = load i32, i32* %count.addr, align 4
  %cmp132 = icmp slt i32 %71, %72
  br i1 %cmp132, label %for.body133, label %for.end166

for.body133:                                      ; preds = %for.cond131
  %73 = load i8*, i8** %ptr, align 4
  %74 = bitcast i8* %73 to float*
  store float* %74, float** %v134, align 4
  %75 = load float*, float** %v134, align 4
  %arrayidx136 = getelementptr inbounds float, float* %75, i32 0
  %76 = load float*, float** %v134, align 4
  %arrayidx137 = getelementptr inbounds float, float* %76, i32 1
  %77 = load float*, float** %v134, align 4
  %arrayidx138 = getelementptr inbounds float, float* %77, i32 2
  %call139 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %p135, float* nonnull align 4 dereferenceable(4) %arrayidx136, float* nonnull align 4 dereferenceable(4) %arrayidx137, float* nonnull align 4 dereferenceable(4) %arrayidx138)
  %78 = load i32, i32* %stride.addr, align 4
  %79 = load i8*, i8** %ptr, align 4
  %add.ptr140 = getelementptr inbounds i8, i8* %79, i32 %78
  store i8* %add.ptr140, i8** %ptr, align 4
  %center143 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp142, %class.btVector3* nonnull align 4 dereferenceable(16) %p135, %class.btVector3* nonnull align 4 dereferenceable(16) %center143)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp141, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp142, %class.btVector3* nonnull align 4 dereferenceable(16) %s)
  %80 = bitcast %class.btVector3* %p135 to i8*
  %81 = bitcast %class.btVector3* %ref.tmp141 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %80, i8* align 4 %81, i32 16, i1 false)
  %call144 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p135)
  %medAxis145 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 8
  %82 = load i32, i32* %medAxis145, align 4
  %arrayidx146 = getelementptr inbounds float, float* %call144, i32 %82
  %83 = load float, float* %arrayidx146, align 4
  %conv147 = fptosi float %83 to i32
  %84 = load i32, i32* %i130, align 4
  %call148 = call nonnull align 4 dereferenceable(16) %"class.btConvexHullInternal::Point32"* @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EEixEi(%class.btAlignedObjectArray.4* %points, i32 %84)
  %x149 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %call148, i32 0, i32 0
  store i32 %conv147, i32* %x149, align 4
  %call150 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p135)
  %maxAxis151 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 9
  %85 = load i32, i32* %maxAxis151, align 4
  %arrayidx152 = getelementptr inbounds float, float* %call150, i32 %85
  %86 = load float, float* %arrayidx152, align 4
  %conv153 = fptosi float %86 to i32
  %87 = load i32, i32* %i130, align 4
  %call154 = call nonnull align 4 dereferenceable(16) %"class.btConvexHullInternal::Point32"* @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EEixEi(%class.btAlignedObjectArray.4* %points, i32 %87)
  %y155 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %call154, i32 0, i32 1
  store i32 %conv153, i32* %y155, align 4
  %call156 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p135)
  %minAxis157 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 7
  %88 = load i32, i32* %minAxis157, align 4
  %arrayidx158 = getelementptr inbounds float, float* %call156, i32 %88
  %89 = load float, float* %arrayidx158, align 4
  %conv159 = fptosi float %89 to i32
  %90 = load i32, i32* %i130, align 4
  %call160 = call nonnull align 4 dereferenceable(16) %"class.btConvexHullInternal::Point32"* @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EEixEi(%class.btAlignedObjectArray.4* %points, i32 %90)
  %z161 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %call160, i32 0, i32 2
  store i32 %conv159, i32* %z161, align 4
  %91 = load i32, i32* %i130, align 4
  %92 = load i32, i32* %i130, align 4
  %call162 = call nonnull align 4 dereferenceable(16) %"class.btConvexHullInternal::Point32"* @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EEixEi(%class.btAlignedObjectArray.4* %points, i32 %92)
  %index163 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %call162, i32 0, i32 3
  store i32 %91, i32* %index163, align 4
  br label %for.inc164

for.inc164:                                       ; preds = %for.body133
  %93 = load i32, i32* %i130, align 4
  %inc165 = add nsw i32 %93, 1
  store i32 %inc165, i32* %i130, align 4
  br label %for.cond131

for.end166:                                       ; preds = %for.cond131
  br label %if.end167

if.end167:                                        ; preds = %for.end166, %for.end128
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE9quickSortI8pointCmpEEvRKT_(%class.btAlignedObjectArray.4* %points, %class.pointCmp* nonnull align 1 dereferenceable(1) %ref.tmp168)
  %vertexPool = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 2
  call void @_ZN20btConvexHullInternal4PoolINS_6VertexEE5resetEv(%"class.btConvexHullInternal::Pool"* %vertexPool)
  %vertexPool169 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 2
  %94 = load i32, i32* %count.addr, align 4
  call void @_ZN20btConvexHullInternal4PoolINS_6VertexEE12setArraySizeEi(%"class.btConvexHullInternal::Pool"* %vertexPool169, i32 %94)
  %originalVertices = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 5
  %95 = load i32, i32* %count.addr, align 4
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %ref.tmp170, align 4
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE6resizeEiRKS2_(%class.btAlignedObjectArray* %originalVertices, i32 %95, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %ref.tmp170)
  store i32 0, i32* %i171, align 4
  br label %for.cond172

for.cond172:                                      ; preds = %for.inc181, %if.end167
  %96 = load i32, i32* %i171, align 4
  %97 = load i32, i32* %count.addr, align 4
  %cmp173 = icmp slt i32 %96, %97
  br i1 %cmp173, label %for.body174, label %for.end183

for.body174:                                      ; preds = %for.cond172
  %vertexPool176 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 2
  %call177 = call %"class.btConvexHullInternal::Vertex"* @_ZN20btConvexHullInternal4PoolINS_6VertexEE9newObjectEv(%"class.btConvexHullInternal::Pool"* %vertexPool176)
  store %"class.btConvexHullInternal::Vertex"* %call177, %"class.btConvexHullInternal::Vertex"** %v175, align 4
  %98 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v175, align 4
  %edges = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %98, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %edges, align 8
  %99 = load i32, i32* %i171, align 4
  %call178 = call nonnull align 4 dereferenceable(16) %"class.btConvexHullInternal::Point32"* @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EEixEi(%class.btAlignedObjectArray.4* %points, i32 %99)
  %100 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v175, align 4
  %point = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %100, i32 0, i32 7
  %101 = bitcast %"class.btConvexHullInternal::Point32"* %point to i8*
  %102 = bitcast %"class.btConvexHullInternal::Point32"* %call178 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %101, i8* align 4 %102, i32 16, i1 false)
  %103 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v175, align 4
  %copy = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %103, i32 0, i32 8
  store i32 -1, i32* %copy, align 8
  %104 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v175, align 4
  %originalVertices179 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 5
  %105 = load i32, i32* %i171, align 4
  %call180 = call nonnull align 4 dereferenceable(4) %"class.btConvexHullInternal::Vertex"** @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEixEi(%class.btAlignedObjectArray* %originalVertices179, i32 %105)
  store %"class.btConvexHullInternal::Vertex"* %104, %"class.btConvexHullInternal::Vertex"** %call180, align 4
  br label %for.inc181

for.inc181:                                       ; preds = %for.body174
  %106 = load i32, i32* %i171, align 4
  %inc182 = add nsw i32 %106, 1
  store i32 %inc182, i32* %i171, align 4
  br label %for.cond172

for.end183:                                       ; preds = %for.cond172
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE5clearEv(%class.btAlignedObjectArray.4* %points)
  %edgePool = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 3
  call void @_ZN20btConvexHullInternal4PoolINS_4EdgeEE5resetEv(%"class.btConvexHullInternal::Pool.0"* %edgePool)
  %edgePool184 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 3
  %107 = load i32, i32* %count.addr, align 4
  %mul = mul nsw i32 6, %107
  call void @_ZN20btConvexHullInternal4PoolINS_4EdgeEE12setArraySizeEi(%"class.btConvexHullInternal::Pool.0"* %edgePool184, i32 %mul)
  %usedEdgePairs = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 10
  store i32 0, i32* %usedEdgePairs, align 4
  %maxUsedEdgePairs = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 11
  store i32 0, i32* %maxUsedEdgePairs, align 4
  %mergeStamp = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 6
  store i32 -3, i32* %mergeStamp, align 4
  %call185 = call %"class.btConvexHullInternal::IntermediateHull"* @_ZN20btConvexHullInternal16IntermediateHullC2Ev(%"class.btConvexHullInternal::IntermediateHull"* %hull)
  %108 = load i32, i32* %count.addr, align 4
  call void @_ZN20btConvexHullInternal15computeInternalEiiRNS_16IntermediateHullE(%class.btConvexHullInternal* %this1, i32 0, i32 %108, %"class.btConvexHullInternal::IntermediateHull"* nonnull align 4 dereferenceable(16) %hull)
  %minXy = getelementptr inbounds %"class.btConvexHullInternal::IntermediateHull", %"class.btConvexHullInternal::IntermediateHull"* %hull, i32 0, i32 0
  %109 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %minXy, align 4
  %vertexList = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 12
  store %"class.btConvexHullInternal::Vertex"* %109, %"class.btConvexHullInternal::Vertex"** %vertexList, align 4
  %call186 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EED2Ev(%class.btAlignedObjectArray.4* %points) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btVector36setMinERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btVector36setMaxERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %2 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %3 = load float, float* %arrayidx7, align 4
  %cmp8 = fcmp olt float %2, %3
  %4 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 0
  %5 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %6 = load float, float* %arrayidx12, align 4
  %cmp13 = fcmp olt float %5, %6
  %7 = zext i1 %cmp13 to i64
  %cond14 = select i1 %cmp13, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond15 = phi i32 [ %cond, %cond.true ], [ %cond14, %cond.false ]
  ret i32 %cond15
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btVector37minAxisEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 0
  %2 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %3 = load float, float* %arrayidx7, align 4
  %cmp8 = fcmp olt float %2, %3
  %4 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 0, i32 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 1
  %5 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %6 = load float, float* %arrayidx12, align 4
  %cmp13 = fcmp olt float %5, %6
  %7 = zext i1 %cmp13 to i64
  %cond14 = select i1 %cmp13, i32 1, i32 2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond15 = phi i32 [ %cond, %cond.true ], [ %cond14, %cond.false ]
  ret i32 %cond15
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EEC2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIN20btConvexHullInternal7Point32ELj16EEC2Ev(%class.btAlignedAllocator.5* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE6resizeEiRKS1_(%class.btAlignedObjectArray.4* %this, i32 %newsize, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %"class.btConvexHullInternal::Point32"* %fillData, %"class.btConvexHullInternal::Point32"** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %5 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %14 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %14, i32 %15
  %16 = bitcast %"class.btConvexHullInternal::Point32"* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %"class.btConvexHullInternal::Point32"*
  %18 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %fillData.addr, align 4
  %19 = bitcast %"class.btConvexHullInternal::Point32"* %17 to i8*
  %20 = bitcast %"class.btConvexHullInternal::Point32"* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %21, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %"class.btConvexHullInternal::Point32"* @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %0, i32 %1
  ret %"class.btConvexHullInternal::Point32"* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE9quickSortI8pointCmpEEvRKT_(%class.btAlignedObjectArray.4* %this, %class.pointCmp* nonnull align 1 dereferenceable(1) %CompareFunc) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %CompareFunc.addr = alloca %class.pointCmp*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store %class.pointCmp* %CompareFunc, %class.pointCmp** %CompareFunc.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.pointCmp*, %class.pointCmp** %CompareFunc.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE17quickSortInternalI8pointCmpEEvRKT_ii(%class.btAlignedObjectArray.4* %this1, %class.pointCmp* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btConvexHullInternal4PoolINS_6VertexEE5resetEv(%"class.btConvexHullInternal::Pool"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Pool"*, align 4
  store %"class.btConvexHullInternal::Pool"* %this, %"class.btConvexHullInternal::Pool"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Pool"*, %"class.btConvexHullInternal::Pool"** %this.addr, align 4
  %arrays = getelementptr inbounds %"class.btConvexHullInternal::Pool", %"class.btConvexHullInternal::Pool"* %this1, i32 0, i32 0
  %0 = load %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::PoolArray"** %arrays, align 4
  %nextArray = getelementptr inbounds %"class.btConvexHullInternal::Pool", %"class.btConvexHullInternal::Pool"* %this1, i32 0, i32 1
  store %"class.btConvexHullInternal::PoolArray"* %0, %"class.btConvexHullInternal::PoolArray"** %nextArray, align 4
  %freeObjects = getelementptr inbounds %"class.btConvexHullInternal::Pool", %"class.btConvexHullInternal::Pool"* %this1, i32 0, i32 2
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %freeObjects, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btConvexHullInternal4PoolINS_6VertexEE12setArraySizeEi(%"class.btConvexHullInternal::Pool"* %this, i32 %arraySize) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Pool"*, align 4
  %arraySize.addr = alloca i32, align 4
  store %"class.btConvexHullInternal::Pool"* %this, %"class.btConvexHullInternal::Pool"** %this.addr, align 4
  store i32 %arraySize, i32* %arraySize.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Pool"*, %"class.btConvexHullInternal::Pool"** %this.addr, align 4
  %0 = load i32, i32* %arraySize.addr, align 4
  %arraySize2 = getelementptr inbounds %"class.btConvexHullInternal::Pool", %"class.btConvexHullInternal::Pool"* %this1, i32 0, i32 3
  store i32 %0, i32* %arraySize2, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE6resizeEiRKS2_(%class.btAlignedObjectArray* %this, i32 %newsize, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %"class.btConvexHullInternal::Vertex"**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %"class.btConvexHullInternal::Vertex"** %fillData, %"class.btConvexHullInternal::Vertex"*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %14 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %14, i32 %15
  %16 = bitcast %"class.btConvexHullInternal::Vertex"** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %"class.btConvexHullInternal::Vertex"**
  %18 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %fillData.addr, align 4
  %19 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %18, align 4
  store %"class.btConvexHullInternal::Vertex"* %19, %"class.btConvexHullInternal::Vertex"** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Vertex"* @_ZN20btConvexHullInternal4PoolINS_6VertexEE9newObjectEv(%"class.btConvexHullInternal::Pool"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Pool"*, align 4
  %o = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %p = alloca %"class.btConvexHullInternal::PoolArray"*, align 4
  store %"class.btConvexHullInternal::Pool"* %this, %"class.btConvexHullInternal::Pool"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Pool"*, %"class.btConvexHullInternal::Pool"** %this.addr, align 4
  %freeObjects = getelementptr inbounds %"class.btConvexHullInternal::Pool", %"class.btConvexHullInternal::Pool"* %this1, i32 0, i32 2
  %0 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %freeObjects, align 4
  store %"class.btConvexHullInternal::Vertex"* %0, %"class.btConvexHullInternal::Vertex"** %o, align 4
  %1 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %o, align 4
  %tobool = icmp ne %"class.btConvexHullInternal::Vertex"* %1, null
  br i1 %tobool, label %if.end9, label %if.then

if.then:                                          ; preds = %entry
  %nextArray = getelementptr inbounds %"class.btConvexHullInternal::Pool", %"class.btConvexHullInternal::Pool"* %this1, i32 0, i32 1
  %2 = load %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::PoolArray"** %nextArray, align 4
  store %"class.btConvexHullInternal::PoolArray"* %2, %"class.btConvexHullInternal::PoolArray"** %p, align 4
  %3 = load %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::PoolArray"** %p, align 4
  %tobool2 = icmp ne %"class.btConvexHullInternal::PoolArray"* %3, null
  br i1 %tobool2, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.then
  %4 = load %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::PoolArray"** %p, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::PoolArray", %"class.btConvexHullInternal::PoolArray"* %4, i32 0, i32 2
  %5 = load %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::PoolArray"** %next, align 4
  %nextArray4 = getelementptr inbounds %"class.btConvexHullInternal::Pool", %"class.btConvexHullInternal::Pool"* %this1, i32 0, i32 1
  store %"class.btConvexHullInternal::PoolArray"* %5, %"class.btConvexHullInternal::PoolArray"** %nextArray4, align 4
  br label %if.end

if.else:                                          ; preds = %if.then
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 12, i32 16)
  %6 = bitcast i8* %call to %"class.btConvexHullInternal::PoolArray"*
  %arraySize = getelementptr inbounds %"class.btConvexHullInternal::Pool", %"class.btConvexHullInternal::Pool"* %this1, i32 0, i32 3
  %7 = load i32, i32* %arraySize, align 4
  %call5 = call %"class.btConvexHullInternal::PoolArray"* @_ZN20btConvexHullInternal9PoolArrayINS_6VertexEEC2Ei(%"class.btConvexHullInternal::PoolArray"* %6, i32 %7)
  store %"class.btConvexHullInternal::PoolArray"* %6, %"class.btConvexHullInternal::PoolArray"** %p, align 4
  %arrays = getelementptr inbounds %"class.btConvexHullInternal::Pool", %"class.btConvexHullInternal::Pool"* %this1, i32 0, i32 0
  %8 = load %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::PoolArray"** %arrays, align 4
  %9 = load %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::PoolArray"** %p, align 4
  %next6 = getelementptr inbounds %"class.btConvexHullInternal::PoolArray", %"class.btConvexHullInternal::PoolArray"* %9, i32 0, i32 2
  store %"class.btConvexHullInternal::PoolArray"* %8, %"class.btConvexHullInternal::PoolArray"** %next6, align 4
  %10 = load %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::PoolArray"** %p, align 4
  %arrays7 = getelementptr inbounds %"class.btConvexHullInternal::Pool", %"class.btConvexHullInternal::Pool"* %this1, i32 0, i32 0
  store %"class.btConvexHullInternal::PoolArray"* %10, %"class.btConvexHullInternal::PoolArray"** %arrays7, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then3
  %11 = load %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::PoolArray"** %p, align 4
  %call8 = call %"class.btConvexHullInternal::Vertex"* @_ZN20btConvexHullInternal9PoolArrayINS_6VertexEE4initEv(%"class.btConvexHullInternal::PoolArray"* %11)
  store %"class.btConvexHullInternal::Vertex"* %call8, %"class.btConvexHullInternal::Vertex"** %o, align 4
  br label %if.end9

if.end9:                                          ; preds = %if.end, %entry
  %12 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %o, align 4
  %next10 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %12, i32 0, i32 0
  %13 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %next10, align 8
  %freeObjects11 = getelementptr inbounds %"class.btConvexHullInternal::Pool", %"class.btConvexHullInternal::Pool"* %this1, i32 0, i32 2
  store %"class.btConvexHullInternal::Vertex"* %13, %"class.btConvexHullInternal::Vertex"** %freeObjects11, align 4
  %14 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %o, align 4
  %15 = bitcast %"class.btConvexHullInternal::Vertex"* %14 to i8*
  %16 = bitcast i8* %15 to %"class.btConvexHullInternal::Vertex"*
  %call12 = call %"class.btConvexHullInternal::Vertex"* @_ZN20btConvexHullInternal6VertexC2Ev(%"class.btConvexHullInternal::Vertex"* %16)
  ret %"class.btConvexHullInternal::Vertex"* %16
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE5clearEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btConvexHullInternal4PoolINS_4EdgeEE5resetEv(%"class.btConvexHullInternal::Pool.0"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Pool.0"*, align 4
  store %"class.btConvexHullInternal::Pool.0"* %this, %"class.btConvexHullInternal::Pool.0"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Pool.0"*, %"class.btConvexHullInternal::Pool.0"** %this.addr, align 4
  %arrays = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 0
  %0 = load %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::PoolArray.1"** %arrays, align 4
  %nextArray = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 1
  store %"class.btConvexHullInternal::PoolArray.1"* %0, %"class.btConvexHullInternal::PoolArray.1"** %nextArray, align 4
  %freeObjects = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %freeObjects, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btConvexHullInternal4PoolINS_4EdgeEE12setArraySizeEi(%"class.btConvexHullInternal::Pool.0"* %this, i32 %arraySize) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Pool.0"*, align 4
  %arraySize.addr = alloca i32, align 4
  store %"class.btConvexHullInternal::Pool.0"* %this, %"class.btConvexHullInternal::Pool.0"** %this.addr, align 4
  store i32 %arraySize, i32* %arraySize.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Pool.0"*, %"class.btConvexHullInternal::Pool.0"** %this.addr, align 4
  %0 = load i32, i32* %arraySize.addr, align 4
  %arraySize2 = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 3
  store i32 %0, i32* %arraySize2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EED2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE5clearEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN20btConvexHullInternal10toBtVectorERKNS_7Point32E(%class.btVector3* noalias sret align 4 %agg.result, %class.btConvexHullInternal* %this, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %v) #2 {
entry:
  %this.addr = alloca %class.btConvexHullInternal*, align 4
  %v.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  %p = alloca %class.btVector3, align 4
  store %class.btConvexHullInternal* %this, %class.btConvexHullInternal** %this.addr, align 4
  store %"class.btConvexHullInternal::Point32"* %v, %"class.btConvexHullInternal::Point32"** %v.addr, align 4
  %this1 = load %class.btConvexHullInternal*, %class.btConvexHullInternal** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %p)
  %0 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %v.addr, align 4
  %x = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %0, i32 0, i32 0
  %1 = load i32, i32* %x, align 4
  %conv = sitofp i32 %1 to float
  %call2 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %medAxis = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 8
  %2 = load i32, i32* %medAxis, align 4
  %arrayidx = getelementptr inbounds float, float* %call2, i32 %2
  store float %conv, float* %arrayidx, align 4
  %3 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %v.addr, align 4
  %y = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %3, i32 0, i32 1
  %4 = load i32, i32* %y, align 4
  %conv3 = sitofp i32 %4 to float
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %maxAxis = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 9
  %5 = load i32, i32* %maxAxis, align 4
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 %5
  store float %conv3, float* %arrayidx5, align 4
  %6 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %v.addr, align 4
  %z = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %6, i32 0, i32 2
  %7 = load i32, i32* %z, align 4
  %conv6 = sitofp i32 %7 to float
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %minAxis = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 7
  %8 = load i32, i32* %minAxis, align 4
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 %8
  store float %conv6, float* %arrayidx8, align 4
  %scaling = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 0
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN20btConvexHullInternal11getBtNormalEPNS_4FaceE(%class.btVector3* noalias sret align 4 %agg.result, %class.btConvexHullInternal* %this, %"class.btConvexHullInternal::Face"* %face) #2 {
entry:
  %this.addr = alloca %class.btConvexHullInternal*, align 4
  %face.addr = alloca %"class.btConvexHullInternal::Face"*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  store %class.btConvexHullInternal* %this, %class.btConvexHullInternal** %this.addr, align 4
  store %"class.btConvexHullInternal::Face"* %face, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %this1 = load %class.btConvexHullInternal*, %class.btConvexHullInternal** %this.addr, align 4
  %0 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %dir0 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %0, i32 0, i32 4
  call void @_ZN20btConvexHullInternal10toBtVectorERKNS_7Point32E(%class.btVector3* sret align 4 %ref.tmp2, %class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %dir0)
  %1 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %dir1 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %1, i32 0, i32 5
  call void @_ZN20btConvexHullInternal10toBtVectorERKNS_7Point32E(%class.btVector3* sret align 4 %ref.tmp3, %class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %dir1)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %agg.result, %class.btVector3* %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector310normalizedEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %nrm = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast %class.btVector3* %nrm to i8*
  %1 = bitcast %class.btVector3* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %nrm)
  %2 = bitcast %class.btVector3* %agg.result to i8*
  %3 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN20btConvexHullInternal14getCoordinatesEPKNS_6VertexE(%class.btVector3* noalias sret align 4 %agg.result, %class.btConvexHullInternal* %this, %"class.btConvexHullInternal::Vertex"* %v) #2 {
entry:
  %this.addr = alloca %class.btConvexHullInternal*, align 4
  %v.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %p = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btConvexHullInternal* %this, %class.btConvexHullInternal** %this.addr, align 4
  store %"class.btConvexHullInternal::Vertex"* %v, %"class.btConvexHullInternal::Vertex"** %v.addr, align 4
  %this1 = load %class.btConvexHullInternal*, %class.btConvexHullInternal** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %p)
  %0 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v.addr, align 4
  %call2 = call float @_ZNK20btConvexHullInternal6Vertex6xvalueEv(%"class.btConvexHullInternal::Vertex"* %0)
  %call3 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %medAxis = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 8
  %1 = load i32, i32* %medAxis, align 4
  %arrayidx = getelementptr inbounds float, float* %call3, i32 %1
  store float %call2, float* %arrayidx, align 4
  %2 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v.addr, align 4
  %call4 = call float @_ZNK20btConvexHullInternal6Vertex6yvalueEv(%"class.btConvexHullInternal::Vertex"* %2)
  %call5 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %maxAxis = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 9
  %3 = load i32, i32* %maxAxis, align 4
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %3
  store float %call4, float* %arrayidx6, align 4
  %4 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v.addr, align 4
  %call7 = call float @_ZNK20btConvexHullInternal6Vertex6zvalueEv(%"class.btConvexHullInternal::Vertex"* %4)
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %minAxis = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 7
  %5 = load i32, i32* %minAxis, align 4
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 %5
  store float %call7, float* %arrayidx9, align 4
  %scaling = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 0
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling)
  %center = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK20btConvexHullInternal6Vertex6xvalueEv(%"class.btConvexHullInternal::Vertex"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  store %"class.btConvexHullInternal::Vertex"* %this, %"class.btConvexHullInternal::Vertex"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %this.addr, align 4
  %point = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 7
  %index = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point, i32 0, i32 3
  %0 = load i32, i32* %index, align 4
  %cmp = icmp sge i32 %0, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %point2 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 7
  %x = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point2, i32 0, i32 0
  %1 = load i32, i32* %x, align 8
  %conv = sitofp i32 %1 to float
  br label %cond.end

cond.false:                                       ; preds = %entry
  %point128 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 6
  %call = call float @_ZNK20btConvexHullInternal9PointR1286xvalueEv(%"class.btConvexHullInternal::PointR128"* %point128)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %conv, %cond.true ], [ %call, %cond.false ]
  ret float %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK20btConvexHullInternal6Vertex6yvalueEv(%"class.btConvexHullInternal::Vertex"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  store %"class.btConvexHullInternal::Vertex"* %this, %"class.btConvexHullInternal::Vertex"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %this.addr, align 4
  %point = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 7
  %index = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point, i32 0, i32 3
  %0 = load i32, i32* %index, align 4
  %cmp = icmp sge i32 %0, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %point2 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 7
  %y = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point2, i32 0, i32 1
  %1 = load i32, i32* %y, align 4
  %conv = sitofp i32 %1 to float
  br label %cond.end

cond.false:                                       ; preds = %entry
  %point128 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 6
  %call = call float @_ZNK20btConvexHullInternal9PointR1286yvalueEv(%"class.btConvexHullInternal::PointR128"* %point128)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %conv, %cond.true ], [ %call, %cond.false ]
  ret float %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK20btConvexHullInternal6Vertex6zvalueEv(%"class.btConvexHullInternal::Vertex"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  store %"class.btConvexHullInternal::Vertex"* %this, %"class.btConvexHullInternal::Vertex"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %this.addr, align 4
  %point = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 7
  %index = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point, i32 0, i32 3
  %0 = load i32, i32* %index, align 4
  %cmp = icmp sge i32 %0, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %point2 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 7
  %z = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point2, i32 0, i32 2
  %1 = load i32, i32* %z, align 8
  %conv = sitofp i32 %1 to float
  br label %cond.end

cond.false:                                       ; preds = %entry
  %point128 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 6
  %call = call float @_ZNK20btConvexHullInternal9PointR1286zvalueEv(%"class.btConvexHullInternal::PointR128"* %point128)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %conv, %cond.true ], [ %call, %cond.false ]
  ret float %cond
}

; Function Attrs: noinline optnone
define hidden float @_ZN20btConvexHullInternal6shrinkEff(%class.btConvexHullInternal* %this, float %amount, float %clampAmount) #2 {
entry:
  %retval = alloca float, align 4
  %this.addr = alloca %class.btConvexHullInternal*, align 4
  %amount.addr = alloca float, align 4
  %clampAmount.addr = alloca float, align 4
  %stamp = alloca i32, align 4
  %stack = alloca %class.btAlignedObjectArray, align 4
  %faces = alloca %class.btAlignedObjectArray.8, align 4
  %ref = alloca %"class.btConvexHullInternal::Point32", align 4
  %hullCenterX = alloca %"class.btConvexHullInternal::Int128", align 8
  %hullCenterY = alloca %"class.btConvexHullInternal::Int128", align 8
  %hullCenterZ = alloca %"class.btConvexHullInternal::Int128", align 8
  %volume = alloca %"class.btConvexHullInternal::Int128", align 8
  %v = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %e = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %face = alloca %"class.btConvexHullInternal::Face"*, align 4
  %f = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %a = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %b = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %vol = alloca i64, align 8
  %ref.tmp = alloca %"class.btConvexHullInternal::Point32", align 4
  %ref.tmp33 = alloca %"class.btConvexHullInternal::Point64", align 8
  %ref.tmp34 = alloca %"class.btConvexHullInternal::Point32", align 4
  %ref.tmp36 = alloca %"class.btConvexHullInternal::Point32", align 4
  %c = alloca %"class.btConvexHullInternal::Point32", align 4
  %ref.tmp39 = alloca %"class.btConvexHullInternal::Point32", align 4
  %ref.tmp40 = alloca %"class.btConvexHullInternal::Point32", align 4
  %ref.tmp44 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp47 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp52 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp57 = alloca %"class.btConvexHullInternal::Int128", align 8
  %cleanup.dest.slot = alloca i32, align 4
  %hullCenter = alloca %class.btVector3, align 4
  %ref.tmp86 = alloca float, align 4
  %faceCount = alloca i32, align 4
  %minDist = alloca float, align 4
  %i = alloca i32, align 4
  %normal = alloca %class.btVector3, align 4
  %dist = alloca float, align 4
  %ref.tmp96 = alloca %class.btVector3, align 4
  %ref.tmp97 = alloca %class.btVector3, align 4
  %ref.tmp106 = alloca float, align 4
  %seed = alloca i32, align 4
  %i110 = alloca i32, align 4
  %i120 = alloca i32, align 4
  %agg.tmp = alloca %class.btAlignedObjectArray, align 4
  store %class.btConvexHullInternal* %this, %class.btConvexHullInternal** %this.addr, align 4
  store float %amount, float* %amount.addr, align 4
  store float %clampAmount, float* %clampAmount.addr, align 4
  %this1 = load %class.btConvexHullInternal*, %class.btConvexHullInternal** %this.addr, align 4
  %vertexList = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 12
  %0 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %vertexList, align 4
  %tobool = icmp ne %"class.btConvexHullInternal::Vertex"* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %mergeStamp = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 6
  %1 = load i32, i32* %mergeStamp, align 4
  %dec = add nsw i32 %1, -1
  store i32 %dec, i32* %mergeStamp, align 4
  store i32 %dec, i32* %stamp, align 4
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEC2Ev(%class.btAlignedObjectArray* %stack)
  %2 = load i32, i32* %stamp, align 4
  %vertexList2 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 12
  %3 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %vertexList2, align 4
  %copy = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %3, i32 0, i32 8
  store i32 %2, i32* %copy, align 8
  %vertexList3 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 12
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9push_backERKS2_(%class.btAlignedObjectArray* %stack, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %vertexList3)
  %call4 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEEC2Ev(%class.btAlignedObjectArray.8* %faces)
  %vertexList5 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 12
  %4 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %vertexList5, align 4
  %point = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %4, i32 0, i32 7
  %5 = bitcast %"class.btConvexHullInternal::Point32"* %ref to i8*
  %6 = bitcast %"class.btConvexHullInternal::Point32"* %point to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 8 %6, i32 16, i1 false)
  %call6 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Eyy(%"class.btConvexHullInternal::Int128"* %hullCenterX, i64 0, i64 0)
  %call7 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Eyy(%"class.btConvexHullInternal::Int128"* %hullCenterY, i64 0, i64 0)
  %call8 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Eyy(%"class.btConvexHullInternal::Int128"* %hullCenterZ, i64 0, i64 0)
  %call9 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Eyy(%"class.btConvexHullInternal::Int128"* %volume, i64 0, i64 0)
  br label %while.cond

while.cond:                                       ; preds = %if.end72, %if.end
  %call10 = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4sizeEv(%class.btAlignedObjectArray* %stack)
  %cmp = icmp sgt i32 %call10, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call11 = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4sizeEv(%class.btAlignedObjectArray* %stack)
  %sub = sub nsw i32 %call11, 1
  %call12 = call nonnull align 4 dereferenceable(4) %"class.btConvexHullInternal::Vertex"** @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEixEi(%class.btAlignedObjectArray* %stack, i32 %sub)
  %7 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %call12, align 4
  store %"class.btConvexHullInternal::Vertex"* %7, %"class.btConvexHullInternal::Vertex"** %v, align 4
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE8pop_backEv(%class.btAlignedObjectArray* %stack)
  %8 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %edges = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %8, i32 0, i32 2
  %9 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edges, align 8
  store %"class.btConvexHullInternal::Edge"* %9, %"class.btConvexHullInternal::Edge"** %e, align 4
  %10 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %tobool13 = icmp ne %"class.btConvexHullInternal::Edge"* %10, null
  br i1 %tobool13, label %if.then14, label %if.end72

if.then14:                                        ; preds = %while.body
  br label %do.body

do.body:                                          ; preds = %do.cond68, %if.then14
  %11 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %target = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %11, i32 0, i32 3
  %12 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target, align 4
  %copy15 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %12, i32 0, i32 8
  %13 = load i32, i32* %copy15, align 8
  %14 = load i32, i32* %stamp, align 4
  %cmp16 = icmp ne i32 %13, %14
  br i1 %cmp16, label %if.then17, label %if.end21

if.then17:                                        ; preds = %do.body
  %15 = load i32, i32* %stamp, align 4
  %16 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %target18 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %16, i32 0, i32 3
  %17 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target18, align 4
  %copy19 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %17, i32 0, i32 8
  store i32 %15, i32* %copy19, align 8
  %18 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %target20 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %18, i32 0, i32 3
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9push_backERKS2_(%class.btAlignedObjectArray* %stack, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %target20)
  br label %if.end21

if.end21:                                         ; preds = %if.then17, %do.body
  %19 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %copy22 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %19, i32 0, i32 5
  %20 = load i32, i32* %copy22, align 4
  %21 = load i32, i32* %stamp, align 4
  %cmp23 = icmp ne i32 %20, %21
  br i1 %cmp23, label %if.then24, label %if.end67

if.then24:                                        ; preds = %if.end21
  %facePool = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 4
  %call25 = call %"class.btConvexHullInternal::Face"* @_ZN20btConvexHullInternal4PoolINS_4FaceEE9newObjectEv(%"class.btConvexHullInternal::Pool.2"* %facePool)
  store %"class.btConvexHullInternal::Face"* %call25, %"class.btConvexHullInternal::Face"** %face, align 4
  %22 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face, align 4
  %23 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %target26 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %23, i32 0, i32 3
  %24 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target26, align 4
  %25 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %reverse = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %25, i32 0, i32 2
  %26 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse, align 4
  %prev = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %26, i32 0, i32 1
  %27 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev, align 4
  %target27 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %27, i32 0, i32 3
  %28 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target27, align 4
  %29 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  call void @_ZN20btConvexHullInternal4Face4initEPNS_6VertexES2_S2_(%"class.btConvexHullInternal::Face"* %22, %"class.btConvexHullInternal::Vertex"* %24, %"class.btConvexHullInternal::Vertex"* %28, %"class.btConvexHullInternal::Vertex"* %29)
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE9push_backERKS2_(%class.btAlignedObjectArray.8* %faces, %"class.btConvexHullInternal::Face"** nonnull align 4 dereferenceable(4) %face)
  %30 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  store %"class.btConvexHullInternal::Edge"* %30, %"class.btConvexHullInternal::Edge"** %f, align 4
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %a, align 4
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %b, align 4
  br label %do.body28

do.body28:                                        ; preds = %do.cond, %if.then24
  %31 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %a, align 4
  %tobool29 = icmp ne %"class.btConvexHullInternal::Vertex"* %31, null
  br i1 %tobool29, label %land.lhs.true, label %if.end60

land.lhs.true:                                    ; preds = %do.body28
  %32 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %b, align 4
  %tobool30 = icmp ne %"class.btConvexHullInternal::Vertex"* %32, null
  br i1 %tobool30, label %if.then31, label %if.end60

if.then31:                                        ; preds = %land.lhs.true
  %33 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %point32 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %33, i32 0, i32 7
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp, %"class.btConvexHullInternal::Point32"* %point32, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %ref)
  %34 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %a, align 4
  %point35 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %34, i32 0, i32 7
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp34, %"class.btConvexHullInternal::Point32"* %point35, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %ref)
  %35 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %b, align 4
  %point37 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %35, i32 0, i32 7
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp36, %"class.btConvexHullInternal::Point32"* %point37, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %ref)
  call void @_ZNK20btConvexHullInternal7Point325crossERKS0_(%"class.btConvexHullInternal::Point64"* sret align 8 %ref.tmp33, %"class.btConvexHullInternal::Point32"* %ref.tmp34, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %ref.tmp36)
  %call38 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %ref.tmp, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %ref.tmp33)
  store i64 %call38, i64* %vol, align 8
  %36 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %point41 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %36, i32 0, i32 7
  %37 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %a, align 4
  %point42 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %37, i32 0, i32 7
  call void @_ZNK20btConvexHullInternal7Point32plERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp40, %"class.btConvexHullInternal::Point32"* %point41, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %point42)
  %38 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %b, align 4
  %point43 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %38, i32 0, i32 7
  call void @_ZNK20btConvexHullInternal7Point32plERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp39, %"class.btConvexHullInternal::Point32"* %ref.tmp40, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %point43)
  call void @_ZNK20btConvexHullInternal7Point32plERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %c, %"class.btConvexHullInternal::Point32"* %ref.tmp39, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %ref)
  %39 = load i64, i64* %vol, align 8
  %x = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %c, i32 0, i32 0
  %40 = load i32, i32* %x, align 4
  %conv = sext i32 %40 to i64
  %mul = mul nsw i64 %39, %conv
  %call45 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ex(%"class.btConvexHullInternal::Int128"* %ref.tmp44, i64 %mul)
  %call46 = call nonnull align 8 dereferenceable(16) %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128pLERKS0_(%"class.btConvexHullInternal::Int128"* %hullCenterX, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp44)
  %41 = load i64, i64* %vol, align 8
  %y = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %c, i32 0, i32 1
  %42 = load i32, i32* %y, align 4
  %conv48 = sext i32 %42 to i64
  %mul49 = mul nsw i64 %41, %conv48
  %call50 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ex(%"class.btConvexHullInternal::Int128"* %ref.tmp47, i64 %mul49)
  %call51 = call nonnull align 8 dereferenceable(16) %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128pLERKS0_(%"class.btConvexHullInternal::Int128"* %hullCenterY, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp47)
  %43 = load i64, i64* %vol, align 8
  %z = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %c, i32 0, i32 2
  %44 = load i32, i32* %z, align 4
  %conv53 = sext i32 %44 to i64
  %mul54 = mul nsw i64 %43, %conv53
  %call55 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ex(%"class.btConvexHullInternal::Int128"* %ref.tmp52, i64 %mul54)
  %call56 = call nonnull align 8 dereferenceable(16) %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128pLERKS0_(%"class.btConvexHullInternal::Int128"* %hullCenterZ, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp52)
  %45 = load i64, i64* %vol, align 8
  %call58 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ex(%"class.btConvexHullInternal::Int128"* %ref.tmp57, i64 %45)
  %call59 = call nonnull align 8 dereferenceable(16) %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128pLERKS0_(%"class.btConvexHullInternal::Int128"* %volume, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp57)
  br label %if.end60

if.end60:                                         ; preds = %if.then31, %land.lhs.true, %do.body28
  %46 = load i32, i32* %stamp, align 4
  %47 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f, align 4
  %copy61 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %47, i32 0, i32 5
  store i32 %46, i32* %copy61, align 4
  %48 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face, align 4
  %49 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f, align 4
  %face62 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %49, i32 0, i32 4
  store %"class.btConvexHullInternal::Face"* %48, %"class.btConvexHullInternal::Face"** %face62, align 4
  %50 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %b, align 4
  store %"class.btConvexHullInternal::Vertex"* %50, %"class.btConvexHullInternal::Vertex"** %a, align 4
  %51 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f, align 4
  %target63 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %51, i32 0, i32 3
  %52 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target63, align 4
  store %"class.btConvexHullInternal::Vertex"* %52, %"class.btConvexHullInternal::Vertex"** %b, align 4
  %53 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f, align 4
  %reverse64 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %53, i32 0, i32 2
  %54 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse64, align 4
  %prev65 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %54, i32 0, i32 1
  %55 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev65, align 4
  store %"class.btConvexHullInternal::Edge"* %55, %"class.btConvexHullInternal::Edge"** %f, align 4
  br label %do.cond

do.cond:                                          ; preds = %if.end60
  %56 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f, align 4
  %57 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %cmp66 = icmp ne %"class.btConvexHullInternal::Edge"* %56, %57
  br i1 %cmp66, label %do.body28, label %do.end

do.end:                                           ; preds = %do.cond
  br label %if.end67

if.end67:                                         ; preds = %do.end, %if.end21
  %58 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %58, i32 0, i32 0
  %59 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next, align 4
  store %"class.btConvexHullInternal::Edge"* %59, %"class.btConvexHullInternal::Edge"** %e, align 4
  br label %do.cond68

do.cond68:                                        ; preds = %if.end67
  %60 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %61 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %edges69 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %61, i32 0, i32 2
  %62 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edges69, align 8
  %cmp70 = icmp ne %"class.btConvexHullInternal::Edge"* %60, %62
  br i1 %cmp70, label %do.body, label %do.end71

do.end71:                                         ; preds = %do.cond68
  br label %if.end72

if.end72:                                         ; preds = %do.end71, %while.body
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %call73 = call i32 @_ZNK20btConvexHullInternal6Int1287getSignEv(%"class.btConvexHullInternal::Int128"* %volume)
  %cmp74 = icmp sle i32 %call73, 0
  br i1 %cmp74, label %if.then75, label %if.end76

if.then75:                                        ; preds = %while.end
  store float 0.000000e+00, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end76:                                         ; preds = %while.end
  %call77 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %hullCenter)
  %call78 = call float @_ZNK20btConvexHullInternal6Int1288toScalarEv(%"class.btConvexHullInternal::Int128"* %hullCenterX)
  %call79 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %hullCenter)
  %medAxis = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 8
  %63 = load i32, i32* %medAxis, align 4
  %arrayidx = getelementptr inbounds float, float* %call79, i32 %63
  store float %call78, float* %arrayidx, align 4
  %call80 = call float @_ZNK20btConvexHullInternal6Int1288toScalarEv(%"class.btConvexHullInternal::Int128"* %hullCenterY)
  %call81 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %hullCenter)
  %maxAxis = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 9
  %64 = load i32, i32* %maxAxis, align 4
  %arrayidx82 = getelementptr inbounds float, float* %call81, i32 %64
  store float %call80, float* %arrayidx82, align 4
  %call83 = call float @_ZNK20btConvexHullInternal6Int1288toScalarEv(%"class.btConvexHullInternal::Int128"* %hullCenterZ)
  %call84 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %hullCenter)
  %minAxis = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 7
  %65 = load i32, i32* %minAxis, align 4
  %arrayidx85 = getelementptr inbounds float, float* %call84, i32 %65
  store float %call83, float* %arrayidx85, align 4
  %call87 = call float @_ZNK20btConvexHullInternal6Int1288toScalarEv(%"class.btConvexHullInternal::Int128"* %volume)
  %mul88 = fmul float 4.000000e+00, %call87
  store float %mul88, float* %ref.tmp86, align 4
  %call89 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %hullCenter, float* nonnull align 4 dereferenceable(4) %ref.tmp86)
  %scaling = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 0
  %call90 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKS_(%class.btVector3* %hullCenter, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling)
  %call91 = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE4sizeEv(%class.btAlignedObjectArray.8* %faces)
  store i32 %call91, i32* %faceCount, align 4
  %66 = load float, float* %clampAmount.addr, align 4
  %cmp92 = fcmp ogt float %66, 0.000000e+00
  br i1 %cmp92, label %if.then93, label %if.end109

if.then93:                                        ; preds = %if.end76
  store float 0x47EFFFFFE0000000, float* %minDist, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then93
  %67 = load i32, i32* %i, align 4
  %68 = load i32, i32* %faceCount, align 4
  %cmp94 = icmp slt i32 %67, %68
  br i1 %cmp94, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %69 = load i32, i32* %i, align 4
  %call95 = call nonnull align 4 dereferenceable(4) %"class.btConvexHullInternal::Face"** @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEEixEi(%class.btAlignedObjectArray.8* %faces, i32 %69)
  %70 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %call95, align 4
  call void @_ZN20btConvexHullInternal11getBtNormalEPNS_4FaceE(%class.btVector3* sret align 4 %normal, %class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::Face"* %70)
  %71 = load i32, i32* %i, align 4
  %call98 = call nonnull align 4 dereferenceable(4) %"class.btConvexHullInternal::Face"** @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEEixEi(%class.btAlignedObjectArray.8* %faces, i32 %71)
  %72 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %call98, align 4
  %origin = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %72, i32 0, i32 3
  call void @_ZN20btConvexHullInternal10toBtVectorERKNS_7Point32E(%class.btVector3* sret align 4 %ref.tmp97, %class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %origin)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp96, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp97, %class.btVector3* nonnull align 4 dereferenceable(16) %hullCenter)
  %call99 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %normal, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp96)
  store float %call99, float* %dist, align 4
  %73 = load float, float* %dist, align 4
  %74 = load float, float* %minDist, align 4
  %cmp100 = fcmp olt float %73, %74
  br i1 %cmp100, label %if.then101, label %if.end102

if.then101:                                       ; preds = %for.body
  %75 = load float, float* %dist, align 4
  store float %75, float* %minDist, align 4
  br label %if.end102

if.end102:                                        ; preds = %if.then101, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end102
  %76 = load i32, i32* %i, align 4
  %inc = add nsw i32 %76, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %77 = load float, float* %minDist, align 4
  %cmp103 = fcmp ole float %77, 0.000000e+00
  br i1 %cmp103, label %if.then104, label %if.end105

if.then104:                                       ; preds = %for.end
  store float 0.000000e+00, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end105:                                        ; preds = %for.end
  %78 = load float, float* %minDist, align 4
  %79 = load float, float* %clampAmount.addr, align 4
  %mul107 = fmul float %78, %79
  store float %mul107, float* %ref.tmp106, align 4
  %call108 = call nonnull align 4 dereferenceable(4) float* @_Z5btMinIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %amount.addr, float* nonnull align 4 dereferenceable(4) %ref.tmp106)
  %80 = load float, float* %call108, align 4
  store float %80, float* %amount.addr, align 4
  br label %if.end109

if.end109:                                        ; preds = %if.end105, %if.end76
  store i32 243703, i32* %seed, align 4
  store i32 0, i32* %i110, align 4
  br label %for.cond111

for.cond111:                                      ; preds = %for.inc116, %if.end109
  %81 = load i32, i32* %i110, align 4
  %82 = load i32, i32* %faceCount, align 4
  %cmp112 = icmp slt i32 %81, %82
  br i1 %cmp112, label %for.body113, label %for.end119

for.body113:                                      ; preds = %for.cond111
  %83 = load i32, i32* %i110, align 4
  %call114 = call nonnull align 4 dereferenceable(4) %"class.btConvexHullInternal::Face"** @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEEixEi(%class.btAlignedObjectArray.8* %faces, i32 %83)
  %84 = load i32, i32* %seed, align 4
  %85 = load i32, i32* %faceCount, align 4
  %rem = urem i32 %84, %85
  %call115 = call nonnull align 4 dereferenceable(4) %"class.btConvexHullInternal::Face"** @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEEixEi(%class.btAlignedObjectArray.8* %faces, i32 %rem)
  call void @_Z6btSwapIPN20btConvexHullInternal4FaceEEvRT_S4_(%"class.btConvexHullInternal::Face"** nonnull align 4 dereferenceable(4) %call114, %"class.btConvexHullInternal::Face"** nonnull align 4 dereferenceable(4) %call115)
  br label %for.inc116

for.inc116:                                       ; preds = %for.body113
  %86 = load i32, i32* %i110, align 4
  %inc117 = add nsw i32 %86, 1
  store i32 %inc117, i32* %i110, align 4
  %87 = load i32, i32* %seed, align 4
  %mul118 = mul i32 1664525, %87
  %add = add i32 %mul118, 1013904223
  store i32 %add, i32* %seed, align 4
  br label %for.cond111

for.end119:                                       ; preds = %for.cond111
  store i32 0, i32* %i120, align 4
  br label %for.cond121

for.cond121:                                      ; preds = %for.inc130, %for.end119
  %88 = load i32, i32* %i120, align 4
  %89 = load i32, i32* %faceCount, align 4
  %cmp122 = icmp slt i32 %88, %89
  br i1 %cmp122, label %for.body123, label %for.end132

for.body123:                                      ; preds = %for.cond121
  %90 = load i32, i32* %i120, align 4
  %call124 = call nonnull align 4 dereferenceable(4) %"class.btConvexHullInternal::Face"** @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEEixEi(%class.btAlignedObjectArray.8* %faces, i32 %90)
  %91 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %call124, align 4
  %92 = load float, float* %amount.addr, align 4
  %call125 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEC2ERKS3_(%class.btAlignedObjectArray* %agg.tmp, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %stack)
  %call126 = call zeroext i1 @_ZN20btConvexHullInternal9shiftFaceEPNS_4FaceEf20btAlignedObjectArrayIPNS_6VertexEE(%class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::Face"* %91, float %92, %class.btAlignedObjectArray* %agg.tmp)
  %lnot = xor i1 %call126, true
  %call127 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEED2Ev(%class.btAlignedObjectArray* %agg.tmp) #7
  br i1 %lnot, label %if.then128, label %if.end129

if.then128:                                       ; preds = %for.body123
  %93 = load float, float* %amount.addr, align 4
  %fneg = fneg float %93
  store float %fneg, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end129:                                        ; preds = %for.body123
  br label %for.inc130

for.inc130:                                       ; preds = %if.end129
  %94 = load i32, i32* %i120, align 4
  %inc131 = add nsw i32 %94, 1
  store i32 %inc131, i32* %i120, align 4
  br label %for.cond121

for.end132:                                       ; preds = %for.cond121
  %95 = load float, float* %amount.addr, align 4
  store float %95, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end132, %if.then128, %if.then104, %if.then75
  %call133 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEED2Ev(%class.btAlignedObjectArray.8* %faces) #7
  %call135 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEED2Ev(%class.btAlignedObjectArray* %stack) #7
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %96 = load float, float* %retval, align 4
  ret float %96
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIPN20btConvexHullInternal6VertexELj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9push_backERKS2_(%class.btAlignedObjectArray* %this, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %"class.btConvexHullInternal::Vertex"**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %"class.btConvexHullInternal::Vertex"** %_Val, %"class.btConvexHullInternal::Vertex"*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %1, i32 %2
  %3 = bitcast %"class.btConvexHullInternal::Vertex"** %arrayidx to i8*
  %4 = bitcast i8* %3 to %"class.btConvexHullInternal::Vertex"**
  %5 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %_Val.addr, align 4
  %6 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %5, align 4
  store %"class.btConvexHullInternal::Vertex"* %6, %"class.btConvexHullInternal::Vertex"** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEEC2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorIPN20btConvexHullInternal4FaceELj16EEC2Ev(%class.btAlignedAllocator.9* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE4initEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Eyy(%"class.btConvexHullInternal::Int128"* returned %this, i64 %low, i64 %high) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  %low.addr = alloca i64, align 8
  %high.addr = alloca i64, align 8
  store %"class.btConvexHullInternal::Int128"* %this, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  store i64 %low, i64* %low.addr, align 8
  store i64 %high, i64* %high.addr, align 8
  %this1 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  %low2 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 0
  %0 = load i64, i64* %low.addr, align 8
  store i64 %0, i64* %low2, align 8
  %high3 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 1
  %1 = load i64, i64* %high.addr, align 8
  store i64 %1, i64* %high3, align 8
  ret %"class.btConvexHullInternal::Int128"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE8pop_backEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %1, i32 %2
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Face"* @_ZN20btConvexHullInternal4PoolINS_4FaceEE9newObjectEv(%"class.btConvexHullInternal::Pool.2"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Pool.2"*, align 4
  %o = alloca %"class.btConvexHullInternal::Face"*, align 4
  %p = alloca %"class.btConvexHullInternal::PoolArray.3"*, align 4
  store %"class.btConvexHullInternal::Pool.2"* %this, %"class.btConvexHullInternal::Pool.2"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Pool.2"*, %"class.btConvexHullInternal::Pool.2"** %this.addr, align 4
  %freeObjects = getelementptr inbounds %"class.btConvexHullInternal::Pool.2", %"class.btConvexHullInternal::Pool.2"* %this1, i32 0, i32 2
  %0 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %freeObjects, align 4
  store %"class.btConvexHullInternal::Face"* %0, %"class.btConvexHullInternal::Face"** %o, align 4
  %1 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %o, align 4
  %tobool = icmp ne %"class.btConvexHullInternal::Face"* %1, null
  br i1 %tobool, label %if.end9, label %if.then

if.then:                                          ; preds = %entry
  %nextArray = getelementptr inbounds %"class.btConvexHullInternal::Pool.2", %"class.btConvexHullInternal::Pool.2"* %this1, i32 0, i32 1
  %2 = load %"class.btConvexHullInternal::PoolArray.3"*, %"class.btConvexHullInternal::PoolArray.3"** %nextArray, align 4
  store %"class.btConvexHullInternal::PoolArray.3"* %2, %"class.btConvexHullInternal::PoolArray.3"** %p, align 4
  %3 = load %"class.btConvexHullInternal::PoolArray.3"*, %"class.btConvexHullInternal::PoolArray.3"** %p, align 4
  %tobool2 = icmp ne %"class.btConvexHullInternal::PoolArray.3"* %3, null
  br i1 %tobool2, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.then
  %4 = load %"class.btConvexHullInternal::PoolArray.3"*, %"class.btConvexHullInternal::PoolArray.3"** %p, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.3", %"class.btConvexHullInternal::PoolArray.3"* %4, i32 0, i32 2
  %5 = load %"class.btConvexHullInternal::PoolArray.3"*, %"class.btConvexHullInternal::PoolArray.3"** %next, align 4
  %nextArray4 = getelementptr inbounds %"class.btConvexHullInternal::Pool.2", %"class.btConvexHullInternal::Pool.2"* %this1, i32 0, i32 1
  store %"class.btConvexHullInternal::PoolArray.3"* %5, %"class.btConvexHullInternal::PoolArray.3"** %nextArray4, align 4
  br label %if.end

if.else:                                          ; preds = %if.then
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 12, i32 16)
  %6 = bitcast i8* %call to %"class.btConvexHullInternal::PoolArray.3"*
  %arraySize = getelementptr inbounds %"class.btConvexHullInternal::Pool.2", %"class.btConvexHullInternal::Pool.2"* %this1, i32 0, i32 3
  %7 = load i32, i32* %arraySize, align 4
  %call5 = call %"class.btConvexHullInternal::PoolArray.3"* @_ZN20btConvexHullInternal9PoolArrayINS_4FaceEEC2Ei(%"class.btConvexHullInternal::PoolArray.3"* %6, i32 %7)
  store %"class.btConvexHullInternal::PoolArray.3"* %6, %"class.btConvexHullInternal::PoolArray.3"** %p, align 4
  %arrays = getelementptr inbounds %"class.btConvexHullInternal::Pool.2", %"class.btConvexHullInternal::Pool.2"* %this1, i32 0, i32 0
  %8 = load %"class.btConvexHullInternal::PoolArray.3"*, %"class.btConvexHullInternal::PoolArray.3"** %arrays, align 4
  %9 = load %"class.btConvexHullInternal::PoolArray.3"*, %"class.btConvexHullInternal::PoolArray.3"** %p, align 4
  %next6 = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.3", %"class.btConvexHullInternal::PoolArray.3"* %9, i32 0, i32 2
  store %"class.btConvexHullInternal::PoolArray.3"* %8, %"class.btConvexHullInternal::PoolArray.3"** %next6, align 4
  %10 = load %"class.btConvexHullInternal::PoolArray.3"*, %"class.btConvexHullInternal::PoolArray.3"** %p, align 4
  %arrays7 = getelementptr inbounds %"class.btConvexHullInternal::Pool.2", %"class.btConvexHullInternal::Pool.2"* %this1, i32 0, i32 0
  store %"class.btConvexHullInternal::PoolArray.3"* %10, %"class.btConvexHullInternal::PoolArray.3"** %arrays7, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then3
  %11 = load %"class.btConvexHullInternal::PoolArray.3"*, %"class.btConvexHullInternal::PoolArray.3"** %p, align 4
  %call8 = call %"class.btConvexHullInternal::Face"* @_ZN20btConvexHullInternal9PoolArrayINS_4FaceEE4initEv(%"class.btConvexHullInternal::PoolArray.3"* %11)
  store %"class.btConvexHullInternal::Face"* %call8, %"class.btConvexHullInternal::Face"** %o, align 4
  br label %if.end9

if.end9:                                          ; preds = %if.end, %entry
  %12 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %o, align 4
  %next10 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %12, i32 0, i32 0
  %13 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %next10, align 4
  %freeObjects11 = getelementptr inbounds %"class.btConvexHullInternal::Pool.2", %"class.btConvexHullInternal::Pool.2"* %this1, i32 0, i32 2
  store %"class.btConvexHullInternal::Face"* %13, %"class.btConvexHullInternal::Face"** %freeObjects11, align 4
  %14 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %o, align 4
  %15 = bitcast %"class.btConvexHullInternal::Face"* %14 to i8*
  %16 = bitcast i8* %15 to %"class.btConvexHullInternal::Face"*
  %call12 = call %"class.btConvexHullInternal::Face"* @_ZN20btConvexHullInternal4FaceC2Ev(%"class.btConvexHullInternal::Face"* %16)
  ret %"class.btConvexHullInternal::Face"* %16
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btConvexHullInternal4Face4initEPNS_6VertexES2_S2_(%"class.btConvexHullInternal::Face"* %this, %"class.btConvexHullInternal::Vertex"* %a, %"class.btConvexHullInternal::Vertex"* %b, %"class.btConvexHullInternal::Vertex"* %c) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Face"*, align 4
  %a.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %c.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %ref.tmp = alloca %"class.btConvexHullInternal::Point32", align 4
  %ref.tmp2 = alloca %"class.btConvexHullInternal::Point32", align 4
  store %"class.btConvexHullInternal::Face"* %this, %"class.btConvexHullInternal::Face"** %this.addr, align 4
  store %"class.btConvexHullInternal::Vertex"* %a, %"class.btConvexHullInternal::Vertex"** %a.addr, align 4
  store %"class.btConvexHullInternal::Vertex"* %b, %"class.btConvexHullInternal::Vertex"** %b.addr, align 4
  store %"class.btConvexHullInternal::Vertex"* %c, %"class.btConvexHullInternal::Vertex"** %c.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %this.addr, align 4
  %0 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %a.addr, align 4
  %nearbyVertex = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %this1, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* %0, %"class.btConvexHullInternal::Vertex"** %nearbyVertex, align 4
  %1 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %a.addr, align 4
  %point = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %1, i32 0, i32 7
  %origin = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %this1, i32 0, i32 3
  %2 = bitcast %"class.btConvexHullInternal::Point32"* %origin to i8*
  %3 = bitcast %"class.btConvexHullInternal::Point32"* %point to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 8 %3, i32 16, i1 false)
  %4 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %b.addr, align 4
  %5 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %a.addr, align 4
  call void @_ZNK20btConvexHullInternal6VertexmiERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp, %"class.btConvexHullInternal::Vertex"* %4, %"class.btConvexHullInternal::Vertex"* nonnull align 8 dereferenceable(108) %5)
  %dir0 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %this1, i32 0, i32 4
  %6 = bitcast %"class.btConvexHullInternal::Point32"* %dir0 to i8*
  %7 = bitcast %"class.btConvexHullInternal::Point32"* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %8 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %c.addr, align 4
  %9 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %a.addr, align 4
  call void @_ZNK20btConvexHullInternal6VertexmiERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp2, %"class.btConvexHullInternal::Vertex"* %8, %"class.btConvexHullInternal::Vertex"* nonnull align 8 dereferenceable(108) %9)
  %dir1 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %this1, i32 0, i32 5
  %10 = bitcast %"class.btConvexHullInternal::Point32"* %dir1 to i8*
  %11 = bitcast %"class.btConvexHullInternal::Point32"* %ref.tmp2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  %12 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %a.addr, align 4
  %lastNearbyFace = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %12, i32 0, i32 4
  %13 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %lastNearbyFace, align 8
  %tobool = icmp ne %"class.btConvexHullInternal::Face"* %13, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %14 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %a.addr, align 4
  %lastNearbyFace3 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %14, i32 0, i32 4
  %15 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %lastNearbyFace3, align 8
  %nextWithSameNearbyVertex = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %15, i32 0, i32 2
  store %"class.btConvexHullInternal::Face"* %this1, %"class.btConvexHullInternal::Face"** %nextWithSameNearbyVertex, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %16 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %a.addr, align 4
  %firstNearbyFace = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %16, i32 0, i32 3
  store %"class.btConvexHullInternal::Face"* %this1, %"class.btConvexHullInternal::Face"** %firstNearbyFace, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %17 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %a.addr, align 4
  %lastNearbyFace4 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %17, i32 0, i32 4
  store %"class.btConvexHullInternal::Face"* %this1, %"class.btConvexHullInternal::Face"** %lastNearbyFace4, align 8
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE9push_backERKS2_(%class.btAlignedObjectArray.8* %this, %"class.btConvexHullInternal::Face"** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Val.addr = alloca %"class.btConvexHullInternal::Face"**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store %"class.btConvexHullInternal::Face"** %_Val, %"class.btConvexHullInternal::Face"*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE9allocSizeEi(%class.btAlignedObjectArray.8* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE7reserveEi(%class.btAlignedObjectArray.8* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %1 = load %"class.btConvexHullInternal::Face"**, %"class.btConvexHullInternal::Face"*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %1, i32 %2
  %3 = bitcast %"class.btConvexHullInternal::Face"** %arrayidx to i8*
  %4 = bitcast i8* %3 to %"class.btConvexHullInternal::Face"**
  %5 = load %"class.btConvexHullInternal::Face"**, %"class.btConvexHullInternal::Face"*** %_Val.addr, align 4
  %6 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %5, align 4
  store %"class.btConvexHullInternal::Face"* %6, %"class.btConvexHullInternal::Face"** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btConvexHullInternal7Point32plERKS0_(%"class.btConvexHullInternal::Point32"* noalias sret align 4 %agg.result, %"class.btConvexHullInternal::Point32"* %this, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %b) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  store %"class.btConvexHullInternal::Point32"* %this, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  store %"class.btConvexHullInternal::Point32"* %b, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  %x = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 0
  %0 = load i32, i32* %x, align 4
  %1 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %x2 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %1, i32 0, i32 0
  %2 = load i32, i32* %x2, align 4
  %add = add nsw i32 %0, %2
  %y = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 1
  %3 = load i32, i32* %y, align 4
  %4 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %y3 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %4, i32 0, i32 1
  %5 = load i32, i32* %y3, align 4
  %add4 = add nsw i32 %3, %5
  %z = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 2
  %6 = load i32, i32* %z, align 4
  %7 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %b.addr, align 4
  %z5 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %7, i32 0, i32 2
  %8 = load i32, i32* %z5, align 4
  %add6 = add nsw i32 %6, %8
  %call = call %"class.btConvexHullInternal::Point32"* @_ZN20btConvexHullInternal7Point32C2Eiii(%"class.btConvexHullInternal::Point32"* %agg.result, i32 %add, i32 %add4, i32 %add6)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ex(%"class.btConvexHullInternal::Int128"* returned %this, i64 %value) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  %value.addr = alloca i64, align 8
  store %"class.btConvexHullInternal::Int128"* %this, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  store i64 %value, i64* %value.addr, align 8
  %this1 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  %low = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 0
  %0 = load i64, i64* %value.addr, align 8
  store i64 %0, i64* %low, align 8
  %high = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 1
  %1 = load i64, i64* %value.addr, align 8
  %cmp = icmp sge i64 %1, 0
  %2 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i64 0, i64 -1
  store i64 %cond, i64* %high, align 8
  ret %"class.btConvexHullInternal::Int128"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 8 dereferenceable(16) %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128pLERKS0_(%"class.btConvexHullInternal::Int128"* %this, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %b) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  %lo = alloca i64, align 8
  store %"class.btConvexHullInternal::Int128"* %this, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  store %"class.btConvexHullInternal::Int128"* %b, %"class.btConvexHullInternal::Int128"** %b.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  %low = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 0
  %0 = load i64, i64* %low, align 8
  %1 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %b.addr, align 4
  %low2 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %1, i32 0, i32 0
  %2 = load i64, i64* %low2, align 8
  %add = add i64 %0, %2
  store i64 %add, i64* %lo, align 8
  %3 = load i64, i64* %lo, align 8
  %low3 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 0
  %4 = load i64, i64* %low3, align 8
  %cmp = icmp ult i64 %3, %4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %high = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 1
  %5 = load i64, i64* %high, align 8
  %inc = add i64 %5, 1
  store i64 %inc, i64* %high, align 8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = load i64, i64* %lo, align 8
  %low4 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 0
  store i64 %6, i64* %low4, align 8
  %7 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %b.addr, align 4
  %high5 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %7, i32 0, i32 1
  %8 = load i64, i64* %high5, align 8
  %high6 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 1
  %9 = load i64, i64* %high6, align 8
  %add7 = add i64 %9, %8
  store i64 %add7, i64* %high6, align 8
  ret %"class.btConvexHullInternal::Int128"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btConvexHullInternal6Int1287getSignEv(%"class.btConvexHullInternal::Int128"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  store %"class.btConvexHullInternal::Int128"* %this, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  %high = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 1
  %0 = load i64, i64* %high, align 8
  %cmp = icmp slt i64 %0, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %high2 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 1
  %1 = load i64, i64* %high2, align 8
  %tobool = icmp ne i64 %1, 0
  br i1 %tobool, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %cond.false
  %low = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 0
  %2 = load i64, i64* %low, align 8
  %tobool3 = icmp ne i64 %2, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %cond.false
  %3 = phi i1 [ true, %cond.false ], [ %tobool3, %lor.rhs ]
  %4 = zext i1 %3 to i64
  %cond = select i1 %3, i32 1, i32 0
  br label %cond.end

cond.end:                                         ; preds = %lor.end, %cond.true
  %cond4 = phi i32 [ -1, %cond.true ], [ %cond, %lor.end ]
  ret i32 %cond4
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK20btConvexHullInternal6Int1288toScalarEv(%"class.btConvexHullInternal::Int128"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  %ref.tmp = alloca %"class.btConvexHullInternal::Int128", align 8
  store %"class.btConvexHullInternal::Int128"* %this, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  %high = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 1
  %0 = load i64, i64* %high, align 8
  %cmp = icmp sge i64 %0, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %high2 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 1
  %1 = load i64, i64* %high2, align 8
  %conv = uitofp i64 %1 to float
  %mul = fmul float %conv, 0x43F0000000000000
  %low = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 0
  %2 = load i64, i64* %low, align 8
  %conv3 = uitofp i64 %2 to float
  %add = fadd float %mul, %conv3
  br label %cond.end

cond.false:                                       ; preds = %entry
  call void @_ZNK20btConvexHullInternal6Int128ngEv(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp, %"class.btConvexHullInternal::Int128"* %this1)
  %call = call float @_ZNK20btConvexHullInternal6Int1288toScalarEv(%"class.btConvexHullInternal::Int128"* %ref.tmp)
  %fneg = fneg float %call
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %add, %cond.true ], [ %fneg, %cond.false ]
  ret float %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %4
  store float %mul8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %8, %7
  store float %mul13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE4sizeEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.btConvexHullInternal::Face"** @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEEixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %"class.btConvexHullInternal::Face"**, %"class.btConvexHullInternal::Face"*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %0, i32 %1
  ret %"class.btConvexHullInternal::Face"** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_Z5btMinIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %b.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load float*, float** %a.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load float*, float** %b.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi float* [ %4, %cond.true ], [ %5, %cond.false ]
  ret float* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z6btSwapIPN20btConvexHullInternal4FaceEEvRT_S4_(%"class.btConvexHullInternal::Face"** nonnull align 4 dereferenceable(4) %a, %"class.btConvexHullInternal::Face"** nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca %"class.btConvexHullInternal::Face"**, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Face"**, align 4
  %tmp = alloca %"class.btConvexHullInternal::Face"*, align 4
  store %"class.btConvexHullInternal::Face"** %a, %"class.btConvexHullInternal::Face"*** %a.addr, align 4
  store %"class.btConvexHullInternal::Face"** %b, %"class.btConvexHullInternal::Face"*** %b.addr, align 4
  %0 = load %"class.btConvexHullInternal::Face"**, %"class.btConvexHullInternal::Face"*** %a.addr, align 4
  %1 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %0, align 4
  store %"class.btConvexHullInternal::Face"* %1, %"class.btConvexHullInternal::Face"** %tmp, align 4
  %2 = load %"class.btConvexHullInternal::Face"**, %"class.btConvexHullInternal::Face"*** %b.addr, align 4
  %3 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %2, align 4
  %4 = load %"class.btConvexHullInternal::Face"**, %"class.btConvexHullInternal::Face"*** %a.addr, align 4
  store %"class.btConvexHullInternal::Face"* %3, %"class.btConvexHullInternal::Face"** %4, align 4
  %5 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %tmp, align 4
  %6 = load %"class.btConvexHullInternal::Face"**, %"class.btConvexHullInternal::Face"*** %b.addr, align 4
  store %"class.btConvexHullInternal::Face"* %5, %"class.btConvexHullInternal::Face"** %6, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN20btConvexHullInternal9shiftFaceEPNS_4FaceEf20btAlignedObjectArrayIPNS_6VertexEE(%class.btConvexHullInternal* %this, %"class.btConvexHullInternal::Face"* %face, float %amount, %class.btAlignedObjectArray* %stack) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btConvexHullInternal*, align 4
  %face.addr = alloca %"class.btConvexHullInternal::Face"*, align 4
  %amount.addr = alloca float, align 4
  %origShift = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %shift = alloca %"class.btConvexHullInternal::Point32", align 4
  %normal = alloca %"class.btConvexHullInternal::Point64", align 8
  %origDot = alloca i64, align 8
  %shiftedOrigin = alloca %"class.btConvexHullInternal::Point32", align 4
  %shiftedDot = alloca i64, align 8
  %intersection = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %startEdge = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %optDot = alloca %"class.btConvexHullInternal::Rational128", align 8
  %cmp51 = alloca i32, align 4
  %e = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %dot = alloca %"class.btConvexHullInternal::Rational128", align 8
  %c = alloca i32, align 4
  %e66 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %dot68 = alloca %"class.btConvexHullInternal::Rational128", align 8
  %e89 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %ref.tmp91 = alloca %"class.btConvexHullInternal::Rational128", align 8
  %firstIntersection = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %faceEdge = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %firstFaceEdge = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %e105 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %ref.tmp110 = alloca %"class.btConvexHullInternal::Rational128", align 8
  %prevCmp = alloca i32, align 4
  %prevIntersection = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %prevFaceEdge = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %e130 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %ref.tmp136 = alloca %"class.btConvexHullInternal::Rational128", align 8
  %removed = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %n0 = alloca %"class.btConvexHullInternal::Point64", align 8
  %n1 = alloca %"class.btConvexHullInternal::Point64", align 8
  %m00 = alloca i64, align 8
  %m01 = alloca i64, align 8
  %m10 = alloca i64, align 8
  %m11 = alloca i64, align 8
  %r0 = alloca i64, align 8
  %ref.tmp166 = alloca %"class.btConvexHullInternal::Point32", align 4
  %r1 = alloca i64, align 8
  %ref.tmp170 = alloca %"class.btConvexHullInternal::Point32", align 4
  %det = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp175 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp176 = alloca %"class.btConvexHullInternal::Int128", align 8
  %v = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %ref.tmp178 = alloca %"class.btConvexHullInternal::PointR128", align 8
  %agg.tmp = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp179 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp180 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp181 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp182 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp185 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp190 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp195 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp200 = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp203 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp204 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp205 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp206 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp207 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp211 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp216 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp221 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp226 = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp229 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp230 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp231 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp232 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp233 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp237 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp242 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp247 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp252 = alloca %"class.btConvexHullInternal::Int128", align 8
  %agg.tmp255 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp274 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %removed326 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %ref.tmp331 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %removed360 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %ref.tmp365 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %pos = alloca i32, align 4
  %end = alloca i32, align 4
  %kept = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %deeper = alloca i8, align 1
  %removed378 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %ref.tmp398 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %ref.tmp402 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  store %class.btConvexHullInternal* %this, %class.btConvexHullInternal** %this.addr, align 4
  store %"class.btConvexHullInternal::Face"* %face, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  store float %amount, float* %amount.addr, align 4
  %this1 = load %class.btConvexHullInternal*, %class.btConvexHullInternal** %this.addr, align 4
  %0 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  call void @_ZN20btConvexHullInternal11getBtNormalEPNS_4FaceE(%class.btVector3* sret align 4 %ref.tmp, %class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::Face"* %0)
  %1 = load float, float* %amount.addr, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp2, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %origShift, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %scaling = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 0
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaling)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %2 = load float, float* %arrayidx, align 4
  %cmp = fcmp une float %2, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %scaling3 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 0
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaling3)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 0
  %3 = load float, float* %arrayidx5, align 4
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %origShift)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 0
  %4 = load float, float* %arrayidx7, align 4
  %div = fdiv float %4, %3
  store float %div, float* %arrayidx7, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %scaling8 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 0
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaling8)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 1
  %5 = load float, float* %arrayidx10, align 4
  %cmp11 = fcmp une float %5, 0.000000e+00
  br i1 %cmp11, label %if.then12, label %if.end19

if.then12:                                        ; preds = %if.end
  %scaling13 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 0
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaling13)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 1
  %6 = load float, float* %arrayidx15, align 4
  %call16 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %origShift)
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 1
  %7 = load float, float* %arrayidx17, align 4
  %div18 = fdiv float %7, %6
  store float %div18, float* %arrayidx17, align 4
  br label %if.end19

if.end19:                                         ; preds = %if.then12, %if.end
  %scaling20 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 0
  %call21 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaling20)
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 2
  %8 = load float, float* %arrayidx22, align 4
  %cmp23 = fcmp une float %8, 0.000000e+00
  br i1 %cmp23, label %if.then24, label %if.end31

if.then24:                                        ; preds = %if.end19
  %scaling25 = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 0
  %call26 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaling25)
  %arrayidx27 = getelementptr inbounds float, float* %call26, i32 2
  %9 = load float, float* %arrayidx27, align 4
  %call28 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %origShift)
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 2
  %10 = load float, float* %arrayidx29, align 4
  %div30 = fdiv float %10, %9
  store float %div30, float* %arrayidx29, align 4
  br label %if.end31

if.end31:                                         ; preds = %if.then24, %if.end19
  %call32 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %origShift)
  %medAxis = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 8
  %11 = load i32, i32* %medAxis, align 4
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 %11
  %12 = load float, float* %arrayidx33, align 4
  %conv = fptosi float %12 to i32
  %call34 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %origShift)
  %maxAxis = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 9
  %13 = load i32, i32* %maxAxis, align 4
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 %13
  %14 = load float, float* %arrayidx35, align 4
  %conv36 = fptosi float %14 to i32
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %origShift)
  %minAxis = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 7
  %15 = load i32, i32* %minAxis, align 4
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 %15
  %16 = load float, float* %arrayidx38, align 4
  %conv39 = fptosi float %16 to i32
  %call40 = call %"class.btConvexHullInternal::Point32"* @_ZN20btConvexHullInternal7Point32C2Eiii(%"class.btConvexHullInternal::Point32"* %shift, i32 %conv, i32 %conv36, i32 %conv39)
  %call41 = call zeroext i1 @_ZN20btConvexHullInternal7Point326isZeroEv(%"class.btConvexHullInternal::Point32"* %shift)
  br i1 %call41, label %if.then42, label %if.end43

if.then42:                                        ; preds = %if.end31
  store i1 true, i1* %retval, align 1
  br label %return

if.end43:                                         ; preds = %if.end31
  %17 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  call void @_ZN20btConvexHullInternal4Face9getNormalEv(%"class.btConvexHullInternal::Point64"* sret align 8 %normal, %"class.btConvexHullInternal::Face"* %17)
  %18 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %origin = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %18, i32 0, i32 3
  %call44 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %origin, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %normal)
  store i64 %call44, i64* %origDot, align 8
  %19 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %origin45 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %19, i32 0, i32 3
  call void @_ZNK20btConvexHullInternal7Point32plERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %shiftedOrigin, %"class.btConvexHullInternal::Point32"* %origin45, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %shift)
  %call46 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %shiftedOrigin, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %normal)
  store i64 %call46, i64* %shiftedDot, align 8
  %20 = load i64, i64* %shiftedDot, align 8
  %21 = load i64, i64* %origDot, align 8
  %cmp47 = icmp sge i64 %20, %21
  br i1 %cmp47, label %if.then48, label %if.end49

if.then48:                                        ; preds = %if.end43
  store i1 false, i1* %retval, align 1
  br label %return

if.end49:                                         ; preds = %if.end43
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %22 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %nearbyVertex = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %22, i32 0, i32 1
  %23 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %nearbyVertex, align 4
  %edges = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %23, i32 0, i32 2
  %24 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edges, align 8
  store %"class.btConvexHullInternal::Edge"* %24, %"class.btConvexHullInternal::Edge"** %startEdge, align 4
  %25 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %nearbyVertex50 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %25, i32 0, i32 1
  %26 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %nearbyVertex50, align 4
  call void @_ZNK20btConvexHullInternal6Vertex3dotERKNS_7Point64E(%"class.btConvexHullInternal::Rational128"* sret align 8 %optDot, %"class.btConvexHullInternal::Vertex"* %26, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %normal)
  %27 = load i64, i64* %shiftedDot, align 8
  %call52 = call i32 @_ZNK20btConvexHullInternal11Rational1287compareEx(%"class.btConvexHullInternal::Rational128"* %optDot, i64 %27)
  store i32 %call52, i32* %cmp51, align 4
  %28 = load i32, i32* %cmp51, align 4
  %cmp53 = icmp sge i32 %28, 0
  br i1 %cmp53, label %if.then54, label %if.else

if.then54:                                        ; preds = %if.end49
  %29 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %startEdge, align 4
  store %"class.btConvexHullInternal::Edge"* %29, %"class.btConvexHullInternal::Edge"** %e, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then54
  %30 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %target = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %30, i32 0, i32 3
  %31 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target, align 4
  call void @_ZNK20btConvexHullInternal6Vertex3dotERKNS_7Point64E(%"class.btConvexHullInternal::Rational128"* sret align 8 %dot, %"class.btConvexHullInternal::Vertex"* %31, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %normal)
  %call55 = call i32 @_ZNK20btConvexHullInternal11Rational1287compareERKS0_(%"class.btConvexHullInternal::Rational128"* %dot, %"class.btConvexHullInternal::Rational128"* nonnull align 8 dereferenceable(37) %optDot)
  %cmp56 = icmp slt i32 %call55, 0
  br i1 %cmp56, label %if.then57, label %if.end62

if.then57:                                        ; preds = %do.body
  %32 = load i64, i64* %shiftedDot, align 8
  %call58 = call i32 @_ZNK20btConvexHullInternal11Rational1287compareEx(%"class.btConvexHullInternal::Rational128"* %dot, i64 %32)
  store i32 %call58, i32* %c, align 4
  %33 = bitcast %"class.btConvexHullInternal::Rational128"* %optDot to i8*
  %34 = bitcast %"class.btConvexHullInternal::Rational128"* %dot to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %33, i8* align 8 %34, i32 37, i1 false)
  %35 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %reverse = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %35, i32 0, i32 2
  %36 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse, align 4
  store %"class.btConvexHullInternal::Edge"* %36, %"class.btConvexHullInternal::Edge"** %e, align 4
  %37 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  store %"class.btConvexHullInternal::Edge"* %37, %"class.btConvexHullInternal::Edge"** %startEdge, align 4
  %38 = load i32, i32* %c, align 4
  %cmp59 = icmp slt i32 %38, 0
  br i1 %cmp59, label %if.then60, label %if.end61

if.then60:                                        ; preds = %if.then57
  %39 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  store %"class.btConvexHullInternal::Edge"* %39, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  br label %do.end

if.end61:                                         ; preds = %if.then57
  %40 = load i32, i32* %c, align 4
  store i32 %40, i32* %cmp51, align 4
  br label %if.end62

if.end62:                                         ; preds = %if.end61, %do.body
  %41 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %prev = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %41, i32 0, i32 1
  %42 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev, align 4
  store %"class.btConvexHullInternal::Edge"* %42, %"class.btConvexHullInternal::Edge"** %e, align 4
  br label %do.cond

do.cond:                                          ; preds = %if.end62
  %43 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %44 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %startEdge, align 4
  %cmp63 = icmp ne %"class.btConvexHullInternal::Edge"* %43, %44
  br i1 %cmp63, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond, %if.then60
  %45 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %tobool = icmp ne %"class.btConvexHullInternal::Edge"* %45, null
  br i1 %tobool, label %if.end65, label %if.then64

if.then64:                                        ; preds = %do.end
  store i1 false, i1* %retval, align 1
  br label %return

if.end65:                                         ; preds = %do.end
  br label %if.end86

if.else:                                          ; preds = %if.end49
  %46 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %startEdge, align 4
  store %"class.btConvexHullInternal::Edge"* %46, %"class.btConvexHullInternal::Edge"** %e66, align 4
  br label %do.body67

do.body67:                                        ; preds = %do.cond80, %if.else
  %47 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e66, align 4
  %target69 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %47, i32 0, i32 3
  %48 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target69, align 4
  call void @_ZNK20btConvexHullInternal6Vertex3dotERKNS_7Point64E(%"class.btConvexHullInternal::Rational128"* sret align 8 %dot68, %"class.btConvexHullInternal::Vertex"* %48, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %normal)
  %call70 = call i32 @_ZNK20btConvexHullInternal11Rational1287compareERKS0_(%"class.btConvexHullInternal::Rational128"* %dot68, %"class.btConvexHullInternal::Rational128"* nonnull align 8 dereferenceable(37) %optDot)
  %cmp71 = icmp sgt i32 %call70, 0
  br i1 %cmp71, label %if.then72, label %if.end78

if.then72:                                        ; preds = %do.body67
  %49 = load i64, i64* %shiftedDot, align 8
  %call73 = call i32 @_ZNK20btConvexHullInternal11Rational1287compareEx(%"class.btConvexHullInternal::Rational128"* %dot68, i64 %49)
  store i32 %call73, i32* %cmp51, align 4
  %50 = load i32, i32* %cmp51, align 4
  %cmp74 = icmp sge i32 %50, 0
  br i1 %cmp74, label %if.then75, label %if.end76

if.then75:                                        ; preds = %if.then72
  %51 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e66, align 4
  store %"class.btConvexHullInternal::Edge"* %51, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  br label %do.end82

if.end76:                                         ; preds = %if.then72
  %52 = bitcast %"class.btConvexHullInternal::Rational128"* %optDot to i8*
  %53 = bitcast %"class.btConvexHullInternal::Rational128"* %dot68 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %52, i8* align 8 %53, i32 37, i1 false)
  %54 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e66, align 4
  %reverse77 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %54, i32 0, i32 2
  %55 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse77, align 4
  store %"class.btConvexHullInternal::Edge"* %55, %"class.btConvexHullInternal::Edge"** %e66, align 4
  %56 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e66, align 4
  store %"class.btConvexHullInternal::Edge"* %56, %"class.btConvexHullInternal::Edge"** %startEdge, align 4
  br label %if.end78

if.end78:                                         ; preds = %if.end76, %do.body67
  %57 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e66, align 4
  %prev79 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %57, i32 0, i32 1
  %58 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev79, align 4
  store %"class.btConvexHullInternal::Edge"* %58, %"class.btConvexHullInternal::Edge"** %e66, align 4
  br label %do.cond80

do.cond80:                                        ; preds = %if.end78
  %59 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e66, align 4
  %60 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %startEdge, align 4
  %cmp81 = icmp ne %"class.btConvexHullInternal::Edge"* %59, %60
  br i1 %cmp81, label %do.body67, label %do.end82

do.end82:                                         ; preds = %do.cond80, %if.then75
  %61 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %tobool83 = icmp ne %"class.btConvexHullInternal::Edge"* %61, null
  br i1 %tobool83, label %if.end85, label %if.then84

if.then84:                                        ; preds = %do.end82
  store i1 true, i1* %retval, align 1
  br label %return

if.end85:                                         ; preds = %do.end82
  br label %if.end86

if.end86:                                         ; preds = %if.end85, %if.end65
  %62 = load i32, i32* %cmp51, align 4
  %cmp87 = icmp eq i32 %62, 0
  br i1 %cmp87, label %if.then88, label %if.end100

if.then88:                                        ; preds = %if.end86
  %63 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %reverse90 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %63, i32 0, i32 2
  %64 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse90, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %64, i32 0, i32 0
  %65 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next, align 4
  store %"class.btConvexHullInternal::Edge"* %65, %"class.btConvexHullInternal::Edge"** %e89, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end99, %if.then88
  %66 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e89, align 4
  %target92 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %66, i32 0, i32 3
  %67 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target92, align 4
  call void @_ZNK20btConvexHullInternal6Vertex3dotERKNS_7Point64E(%"class.btConvexHullInternal::Rational128"* sret align 8 %ref.tmp91, %"class.btConvexHullInternal::Vertex"* %67, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %normal)
  %68 = load i64, i64* %shiftedDot, align 8
  %call93 = call i32 @_ZNK20btConvexHullInternal11Rational1287compareEx(%"class.btConvexHullInternal::Rational128"* %ref.tmp91, i64 %68)
  %cmp94 = icmp sle i32 %call93, 0
  br i1 %cmp94, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %69 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e89, align 4
  %next95 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %69, i32 0, i32 0
  %70 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next95, align 4
  store %"class.btConvexHullInternal::Edge"* %70, %"class.btConvexHullInternal::Edge"** %e89, align 4
  %71 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e89, align 4
  %72 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %reverse96 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %72, i32 0, i32 2
  %73 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse96, align 4
  %cmp97 = icmp eq %"class.btConvexHullInternal::Edge"* %71, %73
  br i1 %cmp97, label %if.then98, label %if.end99

if.then98:                                        ; preds = %while.body
  store i1 true, i1* %retval, align 1
  br label %return

if.end99:                                         ; preds = %while.body
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end100

if.end100:                                        ; preds = %while.end, %if.end86
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %firstIntersection, align 4
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %firstFaceEdge, align 4
  br label %while.body102

while.body102:                                    ; preds = %if.end100, %if.end341
  %74 = load i32, i32* %cmp51, align 4
  %cmp103 = icmp eq i32 %74, 0
  br i1 %cmp103, label %if.then104, label %if.end122

if.then104:                                       ; preds = %while.body102
  %75 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %reverse106 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %75, i32 0, i32 2
  %76 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse106, align 4
  %next107 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %76, i32 0, i32 0
  %77 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next107, align 4
  store %"class.btConvexHullInternal::Edge"* %77, %"class.btConvexHullInternal::Edge"** %e105, align 4
  %78 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e105, align 4
  store %"class.btConvexHullInternal::Edge"* %78, %"class.btConvexHullInternal::Edge"** %startEdge, align 4
  br label %while.body109

while.body109:                                    ; preds = %if.then104, %if.end120
  %79 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e105, align 4
  %target111 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %79, i32 0, i32 3
  %80 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target111, align 4
  call void @_ZNK20btConvexHullInternal6Vertex3dotERKNS_7Point64E(%"class.btConvexHullInternal::Rational128"* sret align 8 %ref.tmp110, %"class.btConvexHullInternal::Vertex"* %80, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %normal)
  %81 = load i64, i64* %shiftedDot, align 8
  %call112 = call i32 @_ZNK20btConvexHullInternal11Rational1287compareEx(%"class.btConvexHullInternal::Rational128"* %ref.tmp110, i64 %81)
  %cmp113 = icmp sge i32 %call112, 0
  br i1 %cmp113, label %if.then114, label %if.end115

if.then114:                                       ; preds = %while.body109
  br label %while.end121

if.end115:                                        ; preds = %while.body109
  %82 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e105, align 4
  %reverse116 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %82, i32 0, i32 2
  %83 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse116, align 4
  store %"class.btConvexHullInternal::Edge"* %83, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %84 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e105, align 4
  %next117 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %84, i32 0, i32 0
  %85 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next117, align 4
  store %"class.btConvexHullInternal::Edge"* %85, %"class.btConvexHullInternal::Edge"** %e105, align 4
  %86 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e105, align 4
  %87 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %startEdge, align 4
  %cmp118 = icmp eq %"class.btConvexHullInternal::Edge"* %86, %87
  br i1 %cmp118, label %if.then119, label %if.end120

if.then119:                                       ; preds = %if.end115
  store i1 true, i1* %retval, align 1
  br label %return

if.end120:                                        ; preds = %if.end115
  br label %while.body109

while.end121:                                     ; preds = %if.then114
  br label %if.end122

if.end122:                                        ; preds = %while.end121, %while.body102
  %88 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstIntersection, align 4
  %tobool123 = icmp ne %"class.btConvexHullInternal::Edge"* %88, null
  br i1 %tobool123, label %if.else125, label %if.then124

if.then124:                                       ; preds = %if.end122
  %89 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  store %"class.btConvexHullInternal::Edge"* %89, %"class.btConvexHullInternal::Edge"** %firstIntersection, align 4
  br label %if.end129

if.else125:                                       ; preds = %if.end122
  %90 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %91 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstIntersection, align 4
  %cmp126 = icmp eq %"class.btConvexHullInternal::Edge"* %90, %91
  br i1 %cmp126, label %if.then127, label %if.end128

if.then127:                                       ; preds = %if.else125
  br label %while.end342

if.end128:                                        ; preds = %if.else125
  br label %if.end129

if.end129:                                        ; preds = %if.end128, %if.then124
  %92 = load i32, i32* %cmp51, align 4
  store i32 %92, i32* %prevCmp, align 4
  %93 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  store %"class.btConvexHullInternal::Edge"* %93, %"class.btConvexHullInternal::Edge"** %prevIntersection, align 4
  %94 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  store %"class.btConvexHullInternal::Edge"* %94, %"class.btConvexHullInternal::Edge"** %prevFaceEdge, align 4
  %95 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %reverse131 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %95, i32 0, i32 2
  %96 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse131, align 4
  store %"class.btConvexHullInternal::Edge"* %96, %"class.btConvexHullInternal::Edge"** %e130, align 4
  br label %while.body133

while.body133:                                    ; preds = %if.end129, %if.end141
  %97 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e130, align 4
  %reverse134 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %97, i32 0, i32 2
  %98 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse134, align 4
  %prev135 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %98, i32 0, i32 1
  %99 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev135, align 4
  store %"class.btConvexHullInternal::Edge"* %99, %"class.btConvexHullInternal::Edge"** %e130, align 4
  %100 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e130, align 4
  %target137 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %100, i32 0, i32 3
  %101 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target137, align 4
  call void @_ZNK20btConvexHullInternal6Vertex3dotERKNS_7Point64E(%"class.btConvexHullInternal::Rational128"* sret align 8 %ref.tmp136, %"class.btConvexHullInternal::Vertex"* %101, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %normal)
  %102 = load i64, i64* %shiftedDot, align 8
  %call138 = call i32 @_ZNK20btConvexHullInternal11Rational1287compareEx(%"class.btConvexHullInternal::Rational128"* %ref.tmp136, i64 %102)
  store i32 %call138, i32* %cmp51, align 4
  %103 = load i32, i32* %cmp51, align 4
  %cmp139 = icmp sge i32 %103, 0
  br i1 %cmp139, label %if.then140, label %if.end141

if.then140:                                       ; preds = %while.body133
  %104 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e130, align 4
  store %"class.btConvexHullInternal::Edge"* %104, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  br label %while.end142

if.end141:                                        ; preds = %while.body133
  br label %while.body133

while.end142:                                     ; preds = %if.then140
  %105 = load i32, i32* %cmp51, align 4
  %cmp143 = icmp sgt i32 %105, 0
  br i1 %cmp143, label %if.then144, label %if.end275

if.then144:                                       ; preds = %while.end142
  %106 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %target145 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %106, i32 0, i32 3
  %107 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target145, align 4
  store %"class.btConvexHullInternal::Vertex"* %107, %"class.btConvexHullInternal::Vertex"** %removed, align 4
  %108 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %reverse146 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %108, i32 0, i32 2
  %109 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse146, align 4
  store %"class.btConvexHullInternal::Edge"* %109, %"class.btConvexHullInternal::Edge"** %e130, align 4
  %110 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e130, align 4
  %prev147 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %110, i32 0, i32 1
  %111 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev147, align 4
  %112 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e130, align 4
  %cmp148 = icmp eq %"class.btConvexHullInternal::Edge"* %111, %112
  br i1 %cmp148, label %if.then149, label %if.else151

if.then149:                                       ; preds = %if.then144
  %113 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %removed, align 4
  %edges150 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %113, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %edges150, align 8
  br label %if.end156

if.else151:                                       ; preds = %if.then144
  %114 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e130, align 4
  %prev152 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %114, i32 0, i32 1
  %115 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev152, align 4
  %116 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %removed, align 4
  %edges153 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %116, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* %115, %"class.btConvexHullInternal::Edge"** %edges153, align 8
  %117 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e130, align 4
  %prev154 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %117, i32 0, i32 1
  %118 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev154, align 4
  %119 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e130, align 4
  %next155 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %119, i32 0, i32 0
  %120 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next155, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %118, %"class.btConvexHullInternal::Edge"* %120)
  %121 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e130, align 4
  %122 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e130, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %121, %"class.btConvexHullInternal::Edge"* %122)
  br label %if.end156

if.end156:                                        ; preds = %if.else151, %if.then149
  %123 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %face157 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %123, i32 0, i32 4
  %124 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face157, align 4
  call void @_ZN20btConvexHullInternal4Face9getNormalEv(%"class.btConvexHullInternal::Point64"* sret align 8 %n0, %"class.btConvexHullInternal::Face"* %124)
  %125 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %reverse158 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %125, i32 0, i32 2
  %126 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse158, align 4
  %face159 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %126, i32 0, i32 4
  %127 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face159, align 4
  call void @_ZN20btConvexHullInternal4Face9getNormalEv(%"class.btConvexHullInternal::Point64"* sret align 8 %n1, %"class.btConvexHullInternal::Face"* %127)
  %128 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %dir0 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %128, i32 0, i32 4
  %call160 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %dir0, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %n0)
  store i64 %call160, i64* %m00, align 8
  %129 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %dir1 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %129, i32 0, i32 5
  %call161 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %dir1, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %n0)
  store i64 %call161, i64* %m01, align 8
  %130 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %dir0162 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %130, i32 0, i32 4
  %call163 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %dir0162, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %n1)
  store i64 %call163, i64* %m10, align 8
  %131 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %dir1164 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %131, i32 0, i32 5
  %call165 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %dir1164, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %n1)
  store i64 %call165, i64* %m11, align 8
  %132 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %face167 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %132, i32 0, i32 4
  %133 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face167, align 4
  %origin168 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %133, i32 0, i32 3
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp166, %"class.btConvexHullInternal::Point32"* %origin168, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %shiftedOrigin)
  %call169 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %ref.tmp166, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %n0)
  store i64 %call169, i64* %r0, align 8
  %134 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %reverse171 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %134, i32 0, i32 2
  %135 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse171, align 4
  %face172 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %135, i32 0, i32 4
  %136 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face172, align 4
  %origin173 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %136, i32 0, i32 3
  call void @_ZNK20btConvexHullInternal7Point32miERKS0_(%"class.btConvexHullInternal::Point32"* sret align 4 %ref.tmp170, %"class.btConvexHullInternal::Point32"* %origin173, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %shiftedOrigin)
  %call174 = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %ref.tmp170, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %n1)
  store i64 %call174, i64* %r1, align 8
  %137 = load i64, i64* %m00, align 8
  %138 = load i64, i64* %m11, align 8
  call void @_ZN20btConvexHullInternal6Int1283mulExx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp175, i64 %137, i64 %138)
  %139 = load i64, i64* %m01, align 8
  %140 = load i64, i64* %m10, align 8
  call void @_ZN20btConvexHullInternal6Int1283mulExx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp176, i64 %139, i64 %140)
  call void @_ZNK20btConvexHullInternal6Int128miERKS0_(%"class.btConvexHullInternal::Int128"* sret align 8 %det, %"class.btConvexHullInternal::Int128"* %ref.tmp175, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp176)
  %vertexPool = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 2
  %call177 = call %"class.btConvexHullInternal::Vertex"* @_ZN20btConvexHullInternal4PoolINS_6VertexEE9newObjectEv(%"class.btConvexHullInternal::Pool"* %vertexPool)
  store %"class.btConvexHullInternal::Vertex"* %call177, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %141 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %point = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %141, i32 0, i32 7
  %index = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point, i32 0, i32 3
  store i32 -1, i32* %index, align 4
  %142 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %copy = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %142, i32 0, i32 8
  store i32 -1, i32* %copy, align 8
  %143 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %dir0183 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %143, i32 0, i32 4
  %x = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %dir0183, i32 0, i32 0
  %144 = load i32, i32* %x, align 4
  %conv184 = sext i32 %144 to i64
  %145 = load i64, i64* %r0, align 8
  %mul = mul nsw i64 %conv184, %145
  %146 = load i64, i64* %m11, align 8
  call void @_ZN20btConvexHullInternal6Int1283mulExx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp182, i64 %mul, i64 %146)
  %147 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %dir0186 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %147, i32 0, i32 4
  %x187 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %dir0186, i32 0, i32 0
  %148 = load i32, i32* %x187, align 4
  %conv188 = sext i32 %148 to i64
  %149 = load i64, i64* %r1, align 8
  %mul189 = mul nsw i64 %conv188, %149
  %150 = load i64, i64* %m01, align 8
  call void @_ZN20btConvexHullInternal6Int1283mulExx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp185, i64 %mul189, i64 %150)
  call void @_ZNK20btConvexHullInternal6Int128miERKS0_(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp181, %"class.btConvexHullInternal::Int128"* %ref.tmp182, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp185)
  %151 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %dir1191 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %151, i32 0, i32 5
  %x192 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %dir1191, i32 0, i32 0
  %152 = load i32, i32* %x192, align 4
  %conv193 = sext i32 %152 to i64
  %153 = load i64, i64* %r1, align 8
  %mul194 = mul nsw i64 %conv193, %153
  %154 = load i64, i64* %m00, align 8
  call void @_ZN20btConvexHullInternal6Int1283mulExx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp190, i64 %mul194, i64 %154)
  call void @_ZNK20btConvexHullInternal6Int128plERKS0_(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp180, %"class.btConvexHullInternal::Int128"* %ref.tmp181, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp190)
  %155 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %dir1196 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %155, i32 0, i32 5
  %x197 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %dir1196, i32 0, i32 0
  %156 = load i32, i32* %x197, align 4
  %conv198 = sext i32 %156 to i64
  %157 = load i64, i64* %r0, align 8
  %mul199 = mul nsw i64 %conv198, %157
  %158 = load i64, i64* %m10, align 8
  call void @_ZN20btConvexHullInternal6Int1283mulExx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp195, i64 %mul199, i64 %158)
  call void @_ZNK20btConvexHullInternal6Int128miERKS0_(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp179, %"class.btConvexHullInternal::Int128"* %ref.tmp180, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp195)
  %x201 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %shiftedOrigin, i32 0, i32 0
  %159 = load i32, i32* %x201, align 4
  %conv202 = sext i32 %159 to i64
  call void @_ZNK20btConvexHullInternal6Int128mlEx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp200, %"class.btConvexHullInternal::Int128"* %det, i64 %conv202)
  call void @_ZNK20btConvexHullInternal6Int128plERKS0_(%"class.btConvexHullInternal::Int128"* sret align 8 %agg.tmp, %"class.btConvexHullInternal::Int128"* %ref.tmp179, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp200)
  %160 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %dir0208 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %160, i32 0, i32 4
  %y = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %dir0208, i32 0, i32 1
  %161 = load i32, i32* %y, align 4
  %conv209 = sext i32 %161 to i64
  %162 = load i64, i64* %r0, align 8
  %mul210 = mul nsw i64 %conv209, %162
  %163 = load i64, i64* %m11, align 8
  call void @_ZN20btConvexHullInternal6Int1283mulExx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp207, i64 %mul210, i64 %163)
  %164 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %dir0212 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %164, i32 0, i32 4
  %y213 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %dir0212, i32 0, i32 1
  %165 = load i32, i32* %y213, align 4
  %conv214 = sext i32 %165 to i64
  %166 = load i64, i64* %r1, align 8
  %mul215 = mul nsw i64 %conv214, %166
  %167 = load i64, i64* %m01, align 8
  call void @_ZN20btConvexHullInternal6Int1283mulExx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp211, i64 %mul215, i64 %167)
  call void @_ZNK20btConvexHullInternal6Int128miERKS0_(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp206, %"class.btConvexHullInternal::Int128"* %ref.tmp207, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp211)
  %168 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %dir1217 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %168, i32 0, i32 5
  %y218 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %dir1217, i32 0, i32 1
  %169 = load i32, i32* %y218, align 4
  %conv219 = sext i32 %169 to i64
  %170 = load i64, i64* %r1, align 8
  %mul220 = mul nsw i64 %conv219, %170
  %171 = load i64, i64* %m00, align 8
  call void @_ZN20btConvexHullInternal6Int1283mulExx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp216, i64 %mul220, i64 %171)
  call void @_ZNK20btConvexHullInternal6Int128plERKS0_(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp205, %"class.btConvexHullInternal::Int128"* %ref.tmp206, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp216)
  %172 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %dir1222 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %172, i32 0, i32 5
  %y223 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %dir1222, i32 0, i32 1
  %173 = load i32, i32* %y223, align 4
  %conv224 = sext i32 %173 to i64
  %174 = load i64, i64* %r0, align 8
  %mul225 = mul nsw i64 %conv224, %174
  %175 = load i64, i64* %m10, align 8
  call void @_ZN20btConvexHullInternal6Int1283mulExx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp221, i64 %mul225, i64 %175)
  call void @_ZNK20btConvexHullInternal6Int128miERKS0_(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp204, %"class.btConvexHullInternal::Int128"* %ref.tmp205, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp221)
  %y227 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %shiftedOrigin, i32 0, i32 1
  %176 = load i32, i32* %y227, align 4
  %conv228 = sext i32 %176 to i64
  call void @_ZNK20btConvexHullInternal6Int128mlEx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp226, %"class.btConvexHullInternal::Int128"* %det, i64 %conv228)
  call void @_ZNK20btConvexHullInternal6Int128plERKS0_(%"class.btConvexHullInternal::Int128"* sret align 8 %agg.tmp203, %"class.btConvexHullInternal::Int128"* %ref.tmp204, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp226)
  %177 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %dir0234 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %177, i32 0, i32 4
  %z = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %dir0234, i32 0, i32 2
  %178 = load i32, i32* %z, align 4
  %conv235 = sext i32 %178 to i64
  %179 = load i64, i64* %r0, align 8
  %mul236 = mul nsw i64 %conv235, %179
  %180 = load i64, i64* %m11, align 8
  call void @_ZN20btConvexHullInternal6Int1283mulExx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp233, i64 %mul236, i64 %180)
  %181 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %dir0238 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %181, i32 0, i32 4
  %z239 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %dir0238, i32 0, i32 2
  %182 = load i32, i32* %z239, align 4
  %conv240 = sext i32 %182 to i64
  %183 = load i64, i64* %r1, align 8
  %mul241 = mul nsw i64 %conv240, %183
  %184 = load i64, i64* %m01, align 8
  call void @_ZN20btConvexHullInternal6Int1283mulExx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp237, i64 %mul241, i64 %184)
  call void @_ZNK20btConvexHullInternal6Int128miERKS0_(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp232, %"class.btConvexHullInternal::Int128"* %ref.tmp233, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp237)
  %185 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %dir1243 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %185, i32 0, i32 5
  %z244 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %dir1243, i32 0, i32 2
  %186 = load i32, i32* %z244, align 4
  %conv245 = sext i32 %186 to i64
  %187 = load i64, i64* %r1, align 8
  %mul246 = mul nsw i64 %conv245, %187
  %188 = load i64, i64* %m00, align 8
  call void @_ZN20btConvexHullInternal6Int1283mulExx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp242, i64 %mul246, i64 %188)
  call void @_ZNK20btConvexHullInternal6Int128plERKS0_(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp231, %"class.btConvexHullInternal::Int128"* %ref.tmp232, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp242)
  %189 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %dir1248 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %189, i32 0, i32 5
  %z249 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %dir1248, i32 0, i32 2
  %190 = load i32, i32* %z249, align 4
  %conv250 = sext i32 %190 to i64
  %191 = load i64, i64* %r0, align 8
  %mul251 = mul nsw i64 %conv250, %191
  %192 = load i64, i64* %m10, align 8
  call void @_ZN20btConvexHullInternal6Int1283mulExx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp247, i64 %mul251, i64 %192)
  call void @_ZNK20btConvexHullInternal6Int128miERKS0_(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp230, %"class.btConvexHullInternal::Int128"* %ref.tmp231, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp247)
  %z253 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %shiftedOrigin, i32 0, i32 2
  %193 = load i32, i32* %z253, align 4
  %conv254 = sext i32 %193 to i64
  call void @_ZNK20btConvexHullInternal6Int128mlEx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp252, %"class.btConvexHullInternal::Int128"* %det, i64 %conv254)
  call void @_ZNK20btConvexHullInternal6Int128plERKS0_(%"class.btConvexHullInternal::Int128"* sret align 8 %agg.tmp229, %"class.btConvexHullInternal::Int128"* %ref.tmp230, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp252)
  %194 = bitcast %"class.btConvexHullInternal::Int128"* %agg.tmp255 to i8*
  %195 = bitcast %"class.btConvexHullInternal::Int128"* %det to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %194, i8* align 8 %195, i32 16, i1 false)
  %call256 = call %"class.btConvexHullInternal::PointR128"* @_ZN20btConvexHullInternal9PointR128C2ENS_6Int128ES1_S1_S1_(%"class.btConvexHullInternal::PointR128"* %ref.tmp178, %"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp, %"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp203, %"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp229, %"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %agg.tmp255)
  %196 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %point128 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %196, i32 0, i32 6
  %197 = bitcast %"class.btConvexHullInternal::PointR128"* %point128 to i8*
  %198 = bitcast %"class.btConvexHullInternal::PointR128"* %ref.tmp178 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %197, i8* align 8 %198, i32 64, i1 false)
  %199 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %point128257 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %199, i32 0, i32 6
  %call258 = call float @_ZNK20btConvexHullInternal9PointR1286xvalueEv(%"class.btConvexHullInternal::PointR128"* %point128257)
  %conv259 = fptosi float %call258 to i32
  %200 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %point260 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %200, i32 0, i32 7
  %x261 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point260, i32 0, i32 0
  store i32 %conv259, i32* %x261, align 8
  %201 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %point128262 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %201, i32 0, i32 6
  %call263 = call float @_ZNK20btConvexHullInternal9PointR1286yvalueEv(%"class.btConvexHullInternal::PointR128"* %point128262)
  %conv264 = fptosi float %call263 to i32
  %202 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %point265 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %202, i32 0, i32 7
  %y266 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point265, i32 0, i32 1
  store i32 %conv264, i32* %y266, align 4
  %203 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %point128267 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %203, i32 0, i32 6
  %call268 = call float @_ZNK20btConvexHullInternal9PointR1286zvalueEv(%"class.btConvexHullInternal::PointR128"* %point128267)
  %conv269 = fptosi float %call268 to i32
  %204 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %point270 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %204, i32 0, i32 7
  %z271 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point270, i32 0, i32 2
  store i32 %conv269, i32* %z271, align 8
  %205 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %206 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %target272 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %206, i32 0, i32 3
  store %"class.btConvexHullInternal::Vertex"* %205, %"class.btConvexHullInternal::Vertex"** %target272, align 4
  %207 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e130, align 4
  %208 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %edges273 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %208, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* %207, %"class.btConvexHullInternal::Edge"** %edges273, align 8
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9push_backERKS2_(%class.btAlignedObjectArray* %stack, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %v)
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9push_backERKS2_(%class.btAlignedObjectArray* %stack, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %removed)
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %ref.tmp274, align 4
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9push_backERKS2_(%class.btAlignedObjectArray* %stack, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %ref.tmp274)
  br label %if.end275

if.end275:                                        ; preds = %if.end156, %while.end142
  %209 = load i32, i32* %cmp51, align 4
  %tobool276 = icmp ne i32 %209, 0
  br i1 %tobool276, label %if.then284, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end275
  %210 = load i32, i32* %prevCmp, align 4
  %tobool277 = icmp ne i32 %210, 0
  br i1 %tobool277, label %if.then284, label %lor.lhs.false278

lor.lhs.false278:                                 ; preds = %lor.lhs.false
  %211 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prevIntersection, align 4
  %reverse279 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %211, i32 0, i32 2
  %212 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse279, align 4
  %next280 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %212, i32 0, i32 0
  %213 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next280, align 4
  %target281 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %213, i32 0, i32 3
  %214 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target281, align 4
  %215 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %target282 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %215, i32 0, i32 3
  %216 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target282, align 4
  %cmp283 = icmp ne %"class.btConvexHullInternal::Vertex"* %214, %216
  br i1 %cmp283, label %if.then284, label %if.else307

if.then284:                                       ; preds = %lor.lhs.false278, %lor.lhs.false, %if.end275
  %217 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prevIntersection, align 4
  %target285 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %217, i32 0, i32 3
  %218 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target285, align 4
  %219 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %target286 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %219, i32 0, i32 3
  %220 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target286, align 4
  %call287 = call %"class.btConvexHullInternal::Edge"* @_ZN20btConvexHullInternal11newEdgePairEPNS_6VertexES1_(%class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::Vertex"* %218, %"class.btConvexHullInternal::Vertex"* %220)
  store %"class.btConvexHullInternal::Edge"* %call287, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  %221 = load i32, i32* %prevCmp, align 4
  %cmp288 = icmp eq i32 %221, 0
  br i1 %cmp288, label %if.then289, label %if.end292

if.then289:                                       ; preds = %if.then284
  %222 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  %223 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prevIntersection, align 4
  %reverse290 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %223, i32 0, i32 2
  %224 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse290, align 4
  %next291 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %224, i32 0, i32 0
  %225 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next291, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %222, %"class.btConvexHullInternal::Edge"* %225)
  br label %if.end292

if.end292:                                        ; preds = %if.then289, %if.then284
  %226 = load i32, i32* %prevCmp, align 4
  %cmp293 = icmp eq i32 %226, 0
  br i1 %cmp293, label %if.then296, label %lor.lhs.false294

lor.lhs.false294:                                 ; preds = %if.end292
  %227 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prevFaceEdge, align 4
  %tobool295 = icmp ne %"class.btConvexHullInternal::Edge"* %227, null
  br i1 %tobool295, label %if.then296, label %if.end298

if.then296:                                       ; preds = %lor.lhs.false294, %if.end292
  %228 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prevIntersection, align 4
  %reverse297 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %228, i32 0, i32 2
  %229 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse297, align 4
  %230 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %229, %"class.btConvexHullInternal::Edge"* %230)
  br label %if.end298

if.end298:                                        ; preds = %if.then296, %lor.lhs.false294
  %231 = load i32, i32* %cmp51, align 4
  %cmp299 = icmp eq i32 %231, 0
  br i1 %cmp299, label %if.then300, label %if.end304

if.then300:                                       ; preds = %if.end298
  %232 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %reverse301 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %232, i32 0, i32 2
  %233 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse301, align 4
  %prev302 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %233, i32 0, i32 1
  %234 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev302, align 4
  %235 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  %reverse303 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %235, i32 0, i32 2
  %236 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse303, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %234, %"class.btConvexHullInternal::Edge"* %236)
  br label %if.end304

if.end304:                                        ; preds = %if.then300, %if.end298
  %237 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  %reverse305 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %237, i32 0, i32 2
  %238 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse305, align 4
  %239 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %reverse306 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %239, i32 0, i32 2
  %240 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse306, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %238, %"class.btConvexHullInternal::Edge"* %240)
  br label %if.end310

if.else307:                                       ; preds = %lor.lhs.false278
  %241 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prevIntersection, align 4
  %reverse308 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %241, i32 0, i32 2
  %242 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse308, align 4
  %next309 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %242, i32 0, i32 0
  %243 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next309, align 4
  store %"class.btConvexHullInternal::Edge"* %243, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  br label %if.end310

if.end310:                                        ; preds = %if.else307, %if.end304
  %244 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prevFaceEdge, align 4
  %tobool311 = icmp ne %"class.btConvexHullInternal::Edge"* %244, null
  br i1 %tobool311, label %if.then312, label %if.end334

if.then312:                                       ; preds = %if.end310
  %245 = load i32, i32* %prevCmp, align 4
  %cmp313 = icmp sgt i32 %245, 0
  br i1 %cmp313, label %if.then314, label %if.else316

if.then314:                                       ; preds = %if.then312
  %246 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  %247 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prevFaceEdge, align 4
  %reverse315 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %247, i32 0, i32 2
  %248 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse315, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %246, %"class.btConvexHullInternal::Edge"* %248)
  br label %if.end333

if.else316:                                       ; preds = %if.then312
  %249 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  %250 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prevFaceEdge, align 4
  %reverse317 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %250, i32 0, i32 2
  %251 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse317, align 4
  %cmp318 = icmp ne %"class.btConvexHullInternal::Edge"* %249, %251
  br i1 %cmp318, label %if.then319, label %if.end332

if.then319:                                       ; preds = %if.else316
  %252 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prevFaceEdge, align 4
  %target320 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %252, i32 0, i32 3
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9push_backERKS2_(%class.btAlignedObjectArray* %stack, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %target320)
  br label %while.cond321

while.cond321:                                    ; preds = %while.body325, %if.then319
  %253 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  %next322 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %253, i32 0, i32 0
  %254 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next322, align 4
  %255 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prevFaceEdge, align 4
  %reverse323 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %255, i32 0, i32 2
  %256 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse323, align 4
  %cmp324 = icmp ne %"class.btConvexHullInternal::Edge"* %254, %256
  br i1 %cmp324, label %while.body325, label %while.end330

while.body325:                                    ; preds = %while.cond321
  %257 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  %next327 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %257, i32 0, i32 0
  %258 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next327, align 4
  %target328 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %258, i32 0, i32 3
  %259 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target328, align 4
  store %"class.btConvexHullInternal::Vertex"* %259, %"class.btConvexHullInternal::Vertex"** %removed326, align 4
  %260 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  %next329 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %260, i32 0, i32 0
  %261 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next329, align 4
  call void @_ZN20btConvexHullInternal14removeEdgePairEPNS_4EdgeE(%class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::Edge"* %261)
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9push_backERKS2_(%class.btAlignedObjectArray* %stack, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %removed326)
  br label %while.cond321

while.end330:                                     ; preds = %while.cond321
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %ref.tmp331, align 4
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9push_backERKS2_(%class.btAlignedObjectArray* %stack, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %ref.tmp331)
  br label %if.end332

if.end332:                                        ; preds = %while.end330, %if.else316
  br label %if.end333

if.end333:                                        ; preds = %if.end332, %if.then314
  br label %if.end334

if.end334:                                        ; preds = %if.end333, %if.end310
  %262 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %263 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  %face335 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %263, i32 0, i32 4
  store %"class.btConvexHullInternal::Face"* %262, %"class.btConvexHullInternal::Face"** %face335, align 4
  %264 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %intersection, align 4
  %face336 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %264, i32 0, i32 4
  %265 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face336, align 4
  %266 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  %reverse337 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %266, i32 0, i32 2
  %267 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse337, align 4
  %face338 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %267, i32 0, i32 4
  store %"class.btConvexHullInternal::Face"* %265, %"class.btConvexHullInternal::Face"** %face338, align 4
  %268 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstFaceEdge, align 4
  %tobool339 = icmp ne %"class.btConvexHullInternal::Edge"* %268, null
  br i1 %tobool339, label %if.end341, label %if.then340

if.then340:                                       ; preds = %if.end334
  %269 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  store %"class.btConvexHullInternal::Edge"* %269, %"class.btConvexHullInternal::Edge"** %firstFaceEdge, align 4
  br label %if.end341

if.end341:                                        ; preds = %if.then340, %if.end334
  br label %while.body102

while.end342:                                     ; preds = %if.then127
  %270 = load i32, i32* %cmp51, align 4
  %cmp343 = icmp sgt i32 %270, 0
  br i1 %cmp343, label %if.then344, label %if.else350

if.then344:                                       ; preds = %while.end342
  %271 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  %target345 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %271, i32 0, i32 3
  %272 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target345, align 4
  %273 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstFaceEdge, align 4
  %reverse346 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %273, i32 0, i32 2
  %274 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse346, align 4
  %target347 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %274, i32 0, i32 3
  store %"class.btConvexHullInternal::Vertex"* %272, %"class.btConvexHullInternal::Vertex"** %target347, align 4
  %275 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstIntersection, align 4
  %reverse348 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %275, i32 0, i32 2
  %276 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse348, align 4
  %277 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstFaceEdge, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %276, %"class.btConvexHullInternal::Edge"* %277)
  %278 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstFaceEdge, align 4
  %279 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  %reverse349 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %279, i32 0, i32 2
  %280 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse349, align 4
  call void @_ZN20btConvexHullInternal4Edge4linkEPS0_(%"class.btConvexHullInternal::Edge"* %278, %"class.btConvexHullInternal::Edge"* %280)
  br label %if.end367

if.else350:                                       ; preds = %while.end342
  %281 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstFaceEdge, align 4
  %282 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  %reverse351 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %282, i32 0, i32 2
  %283 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse351, align 4
  %cmp352 = icmp ne %"class.btConvexHullInternal::Edge"* %281, %283
  br i1 %cmp352, label %if.then353, label %if.end366

if.then353:                                       ; preds = %if.else350
  %284 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  %target354 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %284, i32 0, i32 3
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9push_backERKS2_(%class.btAlignedObjectArray* %stack, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %target354)
  br label %while.cond355

while.cond355:                                    ; preds = %while.body359, %if.then353
  %285 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstFaceEdge, align 4
  %next356 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %285, i32 0, i32 0
  %286 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next356, align 4
  %287 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %faceEdge, align 4
  %reverse357 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %287, i32 0, i32 2
  %288 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse357, align 4
  %cmp358 = icmp ne %"class.btConvexHullInternal::Edge"* %286, %288
  br i1 %cmp358, label %while.body359, label %while.end364

while.body359:                                    ; preds = %while.cond355
  %289 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstFaceEdge, align 4
  %next361 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %289, i32 0, i32 0
  %290 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next361, align 4
  %target362 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %290, i32 0, i32 3
  %291 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target362, align 4
  store %"class.btConvexHullInternal::Vertex"* %291, %"class.btConvexHullInternal::Vertex"** %removed360, align 4
  %292 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstFaceEdge, align 4
  %next363 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %292, i32 0, i32 0
  %293 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next363, align 4
  call void @_ZN20btConvexHullInternal14removeEdgePairEPNS_4EdgeE(%class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::Edge"* %293)
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9push_backERKS2_(%class.btAlignedObjectArray* %stack, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %removed360)
  br label %while.cond355

while.end364:                                     ; preds = %while.cond355
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %ref.tmp365, align 4
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9push_backERKS2_(%class.btAlignedObjectArray* %stack, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %ref.tmp365)
  br label %if.end366

if.end366:                                        ; preds = %while.end364, %if.else350
  br label %if.end367

if.end367:                                        ; preds = %if.end366, %if.then344
  %call368 = call nonnull align 4 dereferenceable(4) %"class.btConvexHullInternal::Vertex"** @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEixEi(%class.btAlignedObjectArray* %stack, i32 0)
  %294 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %call368, align 4
  %vertexList = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 12
  store %"class.btConvexHullInternal::Vertex"* %294, %"class.btConvexHullInternal::Vertex"** %vertexList, align 4
  store i32 0, i32* %pos, align 4
  br label %while.cond369

while.cond369:                                    ; preds = %while.end400, %if.end367
  %295 = load i32, i32* %pos, align 4
  %call370 = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4sizeEv(%class.btAlignedObjectArray* %stack)
  %cmp371 = icmp slt i32 %295, %call370
  br i1 %cmp371, label %while.body372, label %while.end401

while.body372:                                    ; preds = %while.cond369
  %call373 = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4sizeEv(%class.btAlignedObjectArray* %stack)
  store i32 %call373, i32* %end, align 4
  br label %while.cond374

while.cond374:                                    ; preds = %if.end399, %while.body372
  %296 = load i32, i32* %pos, align 4
  %297 = load i32, i32* %end, align 4
  %cmp375 = icmp slt i32 %296, %297
  br i1 %cmp375, label %while.body376, label %while.end400

while.body376:                                    ; preds = %while.cond374
  %298 = load i32, i32* %pos, align 4
  %inc = add nsw i32 %298, 1
  store i32 %inc, i32* %pos, align 4
  %call377 = call nonnull align 4 dereferenceable(4) %"class.btConvexHullInternal::Vertex"** @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEixEi(%class.btAlignedObjectArray* %stack, i32 %298)
  %299 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %call377, align 4
  store %"class.btConvexHullInternal::Vertex"* %299, %"class.btConvexHullInternal::Vertex"** %kept, align 4
  store i8 0, i8* %deeper, align 1
  br label %while.cond379

while.cond379:                                    ; preds = %while.end394, %while.body376
  %300 = load i32, i32* %pos, align 4
  %inc380 = add nsw i32 %300, 1
  store i32 %inc380, i32* %pos, align 4
  %call381 = call nonnull align 4 dereferenceable(4) %"class.btConvexHullInternal::Vertex"** @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEixEi(%class.btAlignedObjectArray* %stack, i32 %300)
  %301 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %call381, align 4
  store %"class.btConvexHullInternal::Vertex"* %301, %"class.btConvexHullInternal::Vertex"** %removed378, align 4
  %cmp382 = icmp ne %"class.btConvexHullInternal::Vertex"* %301, null
  br i1 %cmp382, label %while.body383, label %while.end395

while.body383:                                    ; preds = %while.cond379
  %302 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %kept, align 4
  %303 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %removed378, align 4
  call void @_ZN20btConvexHullInternal6Vertex18receiveNearbyFacesEPS0_(%"class.btConvexHullInternal::Vertex"* %302, %"class.btConvexHullInternal::Vertex"* %303)
  br label %while.cond384

while.cond384:                                    ; preds = %if.end390, %while.body383
  %304 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %removed378, align 4
  %edges385 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %304, i32 0, i32 2
  %305 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edges385, align 8
  %tobool386 = icmp ne %"class.btConvexHullInternal::Edge"* %305, null
  br i1 %tobool386, label %while.body387, label %while.end394

while.body387:                                    ; preds = %while.cond384
  %306 = load i8, i8* %deeper, align 1
  %tobool388 = trunc i8 %306 to i1
  br i1 %tobool388, label %if.end390, label %if.then389

if.then389:                                       ; preds = %while.body387
  store i8 1, i8* %deeper, align 1
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9push_backERKS2_(%class.btAlignedObjectArray* %stack, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %kept)
  br label %if.end390

if.end390:                                        ; preds = %if.then389, %while.body387
  %307 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %removed378, align 4
  %edges391 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %307, i32 0, i32 2
  %308 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edges391, align 8
  %target392 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %308, i32 0, i32 3
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9push_backERKS2_(%class.btAlignedObjectArray* %stack, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %target392)
  %309 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %removed378, align 4
  %edges393 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %309, i32 0, i32 2
  %310 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edges393, align 8
  call void @_ZN20btConvexHullInternal14removeEdgePairEPNS_4EdgeE(%class.btConvexHullInternal* %this1, %"class.btConvexHullInternal::Edge"* %310)
  br label %while.cond384

while.end394:                                     ; preds = %while.cond384
  br label %while.cond379

while.end395:                                     ; preds = %while.cond379
  %311 = load i8, i8* %deeper, align 1
  %tobool396 = trunc i8 %311 to i1
  br i1 %tobool396, label %if.then397, label %if.end399

if.then397:                                       ; preds = %while.end395
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %ref.tmp398, align 4
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9push_backERKS2_(%class.btAlignedObjectArray* %stack, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %ref.tmp398)
  br label %if.end399

if.end399:                                        ; preds = %if.then397, %while.end395
  br label %while.cond374

while.end400:                                     ; preds = %while.cond374
  br label %while.cond369

while.end401:                                     ; preds = %while.cond369
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %ref.tmp402, align 4
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE6resizeEiRKS2_(%class.btAlignedObjectArray* %stack, i32 0, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %ref.tmp402)
  %312 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %face.addr, align 4
  %origin403 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %312, i32 0, i32 3
  %313 = bitcast %"class.btConvexHullInternal::Point32"* %origin403 to i8*
  %314 = bitcast %"class.btConvexHullInternal::Point32"* %shiftedOrigin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %313, i8* align 4 %314, i32 16, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %while.end401, %if.then119, %if.then98, %if.then84, %if.then64, %if.then48, %if.then42
  %315 = load i1, i1* %retval, align 1
  ret i1 %315
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEC2ERKS3_(%class.btAlignedObjectArray* returned %this, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %otherArray) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %otherArray.addr = alloca %class.btAlignedObjectArray*, align 4
  %otherSize = alloca i32, align 4
  %ref.tmp = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btAlignedObjectArray* %otherArray, %class.btAlignedObjectArray** %otherArray.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIPN20btConvexHullInternal6VertexELj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4initEv(%class.btAlignedObjectArray* %this1)
  %0 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %otherArray.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4sizeEv(%class.btAlignedObjectArray* %0)
  store i32 %call2, i32* %otherSize, align 4
  %1 = load i32, i32* %otherSize, align 4
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE6resizeEiRKS2_(%class.btAlignedObjectArray* %this1, i32 %1, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %ref.tmp)
  %2 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %otherArray.addr, align 4
  %3 = load i32, i32* %otherSize, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %m_data, align 4
  call void @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4copyEiiPS2_(%class.btAlignedObjectArray* %2, i32 0, i32 %3, %"class.btConvexHullInternal::Vertex"** %4)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEED2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE5clearEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN20btConvexHullInternal7Point326isZeroEv(%"class.btConvexHullInternal::Point32"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  store %"class.btConvexHullInternal::Point32"* %this, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %this.addr, align 4
  %x = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 0
  %0 = load i32, i32* %x, align 4
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %y = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 1
  %1 = load i32, i32* %y, align 4
  %cmp2 = icmp eq i32 %1, 0
  br i1 %cmp2, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true
  %z = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %this1, i32 0, i32 2
  %2 = load i32, i32* %z, align 4
  %cmp3 = icmp eq i32 %2, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true, %entry
  %3 = phi i1 [ false, %land.lhs.true ], [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btConvexHullInternal4Face9getNormalEv(%"class.btConvexHullInternal::Point64"* noalias sret align 8 %agg.result, %"class.btConvexHullInternal::Face"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Face"*, align 4
  store %"class.btConvexHullInternal::Face"* %this, %"class.btConvexHullInternal::Face"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %this.addr, align 4
  %dir0 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %this1, i32 0, i32 4
  %dir1 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %this1, i32 0, i32 5
  call void @_ZNK20btConvexHullInternal7Point325crossERKS0_(%"class.btConvexHullInternal::Point64"* sret align 8 %agg.result, %"class.btConvexHullInternal::Point32"* %dir0, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %dir1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btConvexHullInternal6Vertex3dotERKNS_7Point64E(%"class.btConvexHullInternal::Rational128"* noalias sret align 8 %agg.result, %"class.btConvexHullInternal::Vertex"* %this, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %b) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Point64"*, align 4
  %ref.tmp = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp4 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp5 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp7 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp10 = alloca %"class.btConvexHullInternal::Int128", align 8
  store %"class.btConvexHullInternal::Vertex"* %this, %"class.btConvexHullInternal::Vertex"** %this.addr, align 4
  store %"class.btConvexHullInternal::Point64"* %b, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %this.addr, align 4
  %point = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 7
  %index = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %point, i32 0, i32 3
  %0 = load i32, i32* %index, align 4
  %cmp = icmp sge i32 %0, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %point2 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 7
  %1 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %call = call i64 @_ZNK20btConvexHullInternal7Point323dotERKNS_7Point64E(%"class.btConvexHullInternal::Point32"* %point2, %"class.btConvexHullInternal::Point64"* nonnull align 8 dereferenceable(24) %1)
  %call3 = call %"class.btConvexHullInternal::Rational128"* @_ZN20btConvexHullInternal11Rational128C2Ex(%"class.btConvexHullInternal::Rational128"* %agg.result, i64 %call)
  br label %cond.end

cond.false:                                       ; preds = %entry
  %point128 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 6
  %x = getelementptr inbounds %"class.btConvexHullInternal::PointR128", %"class.btConvexHullInternal::PointR128"* %point128, i32 0, i32 0
  %2 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %x6 = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %2, i32 0, i32 0
  %3 = load i64, i64* %x6, align 8
  call void @_ZNK20btConvexHullInternal6Int128mlEx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp5, %"class.btConvexHullInternal::Int128"* %x, i64 %3)
  %point1288 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 6
  %y = getelementptr inbounds %"class.btConvexHullInternal::PointR128", %"class.btConvexHullInternal::PointR128"* %point1288, i32 0, i32 1
  %4 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %y9 = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %4, i32 0, i32 1
  %5 = load i64, i64* %y9, align 8
  call void @_ZNK20btConvexHullInternal6Int128mlEx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp7, %"class.btConvexHullInternal::Int128"* %y, i64 %5)
  call void @_ZNK20btConvexHullInternal6Int128plERKS0_(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp4, %"class.btConvexHullInternal::Int128"* %ref.tmp5, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp7)
  %point12811 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 6
  %z = getelementptr inbounds %"class.btConvexHullInternal::PointR128", %"class.btConvexHullInternal::PointR128"* %point12811, i32 0, i32 2
  %6 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %b.addr, align 4
  %z12 = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %6, i32 0, i32 2
  %7 = load i64, i64* %z12, align 8
  call void @_ZNK20btConvexHullInternal6Int128mlEx(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp10, %"class.btConvexHullInternal::Int128"* %z, i64 %7)
  call void @_ZNK20btConvexHullInternal6Int128plERKS0_(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp, %"class.btConvexHullInternal::Int128"* %ref.tmp4, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp10)
  %point12813 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 6
  %denominator = getelementptr inbounds %"class.btConvexHullInternal::PointR128", %"class.btConvexHullInternal::PointR128"* %point12813, i32 0, i32 3
  %call14 = call %"class.btConvexHullInternal::Rational128"* @_ZN20btConvexHullInternal11Rational128C2ERKNS_6Int128ES3_(%"class.btConvexHullInternal::Rational128"* %agg.result, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %denominator)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btConvexHullInternal6Int128miERKS0_(%"class.btConvexHullInternal::Int128"* noalias sret align 8 %agg.result, %"class.btConvexHullInternal::Int128"* %this, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %b) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  %ref.tmp = alloca %"class.btConvexHullInternal::Int128", align 8
  store %"class.btConvexHullInternal::Int128"* %this, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  store %"class.btConvexHullInternal::Int128"* %b, %"class.btConvexHullInternal::Int128"** %b.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  %0 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %b.addr, align 4
  call void @_ZNK20btConvexHullInternal6Int128ngEv(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp, %"class.btConvexHullInternal::Int128"* %0)
  call void @_ZNK20btConvexHullInternal6Int128plERKS0_(%"class.btConvexHullInternal::Int128"* sret align 8 %agg.result, %"class.btConvexHullInternal::Int128"* %this1, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btConvexHullInternal6Int128plERKS0_(%"class.btConvexHullInternal::Int128"* noalias sret align 8 %agg.result, %"class.btConvexHullInternal::Int128"* %this, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %b) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  %lo = alloca i64, align 8
  store %"class.btConvexHullInternal::Int128"* %this, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  store %"class.btConvexHullInternal::Int128"* %b, %"class.btConvexHullInternal::Int128"** %b.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  %low = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 0
  %0 = load i64, i64* %low, align 8
  %1 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %b.addr, align 4
  %low2 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %1, i32 0, i32 0
  %2 = load i64, i64* %low2, align 8
  %add = add i64 %0, %2
  store i64 %add, i64* %lo, align 8
  %3 = load i64, i64* %lo, align 8
  %high = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 1
  %4 = load i64, i64* %high, align 8
  %5 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %b.addr, align 4
  %high3 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %5, i32 0, i32 1
  %6 = load i64, i64* %high3, align 8
  %add4 = add i64 %4, %6
  %7 = load i64, i64* %lo, align 8
  %low5 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 0
  %8 = load i64, i64* %low5, align 8
  %cmp = icmp ult i64 %7, %8
  %conv = zext i1 %cmp to i64
  %add6 = add i64 %add4, %conv
  %call = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Eyy(%"class.btConvexHullInternal::Int128"* %agg.result, i64 %3, i64 %add6)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::PointR128"* @_ZN20btConvexHullInternal9PointR128C2ENS_6Int128ES1_S1_S1_(%"class.btConvexHullInternal::PointR128"* returned %this, %"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %x, %"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %y, %"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %z, %"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %denominator) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::PointR128"*, align 4
  store %"class.btConvexHullInternal::PointR128"* %this, %"class.btConvexHullInternal::PointR128"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::PointR128"*, %"class.btConvexHullInternal::PointR128"** %this.addr, align 4
  %x2 = getelementptr inbounds %"class.btConvexHullInternal::PointR128", %"class.btConvexHullInternal::PointR128"* %this1, i32 0, i32 0
  %0 = bitcast %"class.btConvexHullInternal::Int128"* %x2 to i8*
  %1 = bitcast %"class.btConvexHullInternal::Int128"* %x to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %0, i8* align 8 %1, i32 16, i1 false)
  %y3 = getelementptr inbounds %"class.btConvexHullInternal::PointR128", %"class.btConvexHullInternal::PointR128"* %this1, i32 0, i32 1
  %2 = bitcast %"class.btConvexHullInternal::Int128"* %y3 to i8*
  %3 = bitcast %"class.btConvexHullInternal::Int128"* %y to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %2, i8* align 8 %3, i32 16, i1 false)
  %z4 = getelementptr inbounds %"class.btConvexHullInternal::PointR128", %"class.btConvexHullInternal::PointR128"* %this1, i32 0, i32 2
  %4 = bitcast %"class.btConvexHullInternal::Int128"* %z4 to i8*
  %5 = bitcast %"class.btConvexHullInternal::Int128"* %z to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %4, i8* align 8 %5, i32 16, i1 false)
  %denominator5 = getelementptr inbounds %"class.btConvexHullInternal::PointR128", %"class.btConvexHullInternal::PointR128"* %this1, i32 0, i32 3
  %6 = bitcast %"class.btConvexHullInternal::Int128"* %denominator5 to i8*
  %7 = bitcast %"class.btConvexHullInternal::Int128"* %denominator to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %6, i8* align 8 %7, i32 16, i1 false)
  ret %"class.btConvexHullInternal::PointR128"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK20btConvexHullInternal9PointR1286xvalueEv(%"class.btConvexHullInternal::PointR128"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::PointR128"*, align 4
  store %"class.btConvexHullInternal::PointR128"* %this, %"class.btConvexHullInternal::PointR128"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::PointR128"*, %"class.btConvexHullInternal::PointR128"** %this.addr, align 4
  %x = getelementptr inbounds %"class.btConvexHullInternal::PointR128", %"class.btConvexHullInternal::PointR128"* %this1, i32 0, i32 0
  %call = call float @_ZNK20btConvexHullInternal6Int1288toScalarEv(%"class.btConvexHullInternal::Int128"* %x)
  %denominator = getelementptr inbounds %"class.btConvexHullInternal::PointR128", %"class.btConvexHullInternal::PointR128"* %this1, i32 0, i32 3
  %call2 = call float @_ZNK20btConvexHullInternal6Int1288toScalarEv(%"class.btConvexHullInternal::Int128"* %denominator)
  %div = fdiv float %call, %call2
  ret float %div
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK20btConvexHullInternal9PointR1286yvalueEv(%"class.btConvexHullInternal::PointR128"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::PointR128"*, align 4
  store %"class.btConvexHullInternal::PointR128"* %this, %"class.btConvexHullInternal::PointR128"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::PointR128"*, %"class.btConvexHullInternal::PointR128"** %this.addr, align 4
  %y = getelementptr inbounds %"class.btConvexHullInternal::PointR128", %"class.btConvexHullInternal::PointR128"* %this1, i32 0, i32 1
  %call = call float @_ZNK20btConvexHullInternal6Int1288toScalarEv(%"class.btConvexHullInternal::Int128"* %y)
  %denominator = getelementptr inbounds %"class.btConvexHullInternal::PointR128", %"class.btConvexHullInternal::PointR128"* %this1, i32 0, i32 3
  %call2 = call float @_ZNK20btConvexHullInternal6Int1288toScalarEv(%"class.btConvexHullInternal::Int128"* %denominator)
  %div = fdiv float %call, %call2
  ret float %div
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK20btConvexHullInternal9PointR1286zvalueEv(%"class.btConvexHullInternal::PointR128"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::PointR128"*, align 4
  store %"class.btConvexHullInternal::PointR128"* %this, %"class.btConvexHullInternal::PointR128"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::PointR128"*, %"class.btConvexHullInternal::PointR128"** %this.addr, align 4
  %z = getelementptr inbounds %"class.btConvexHullInternal::PointR128", %"class.btConvexHullInternal::PointR128"* %this1, i32 0, i32 2
  %call = call float @_ZNK20btConvexHullInternal6Int1288toScalarEv(%"class.btConvexHullInternal::Int128"* %z)
  %denominator = getelementptr inbounds %"class.btConvexHullInternal::PointR128", %"class.btConvexHullInternal::PointR128"* %this1, i32 0, i32 3
  %call2 = call float @_ZNK20btConvexHullInternal6Int1288toScalarEv(%"class.btConvexHullInternal::Int128"* %denominator)
  %div = fdiv float %call, %call2
  ret float %div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btConvexHullInternal6Vertex18receiveNearbyFacesEPS0_(%"class.btConvexHullInternal::Vertex"* %this, %"class.btConvexHullInternal::Vertex"* %src) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %src.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %f = alloca %"class.btConvexHullInternal::Face"*, align 4
  store %"class.btConvexHullInternal::Vertex"* %this, %"class.btConvexHullInternal::Vertex"** %this.addr, align 4
  store %"class.btConvexHullInternal::Vertex"* %src, %"class.btConvexHullInternal::Vertex"** %src.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %this.addr, align 4
  %lastNearbyFace = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 4
  %0 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %lastNearbyFace, align 8
  %tobool = icmp ne %"class.btConvexHullInternal::Face"* %0, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %src.addr, align 4
  %firstNearbyFace = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %1, i32 0, i32 3
  %2 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %firstNearbyFace, align 4
  %lastNearbyFace2 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 4
  %3 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %lastNearbyFace2, align 8
  %nextWithSameNearbyVertex = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %3, i32 0, i32 2
  store %"class.btConvexHullInternal::Face"* %2, %"class.btConvexHullInternal::Face"** %nextWithSameNearbyVertex, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %4 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %src.addr, align 4
  %firstNearbyFace3 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %4, i32 0, i32 3
  %5 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %firstNearbyFace3, align 4
  %firstNearbyFace4 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 3
  store %"class.btConvexHullInternal::Face"* %5, %"class.btConvexHullInternal::Face"** %firstNearbyFace4, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %6 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %src.addr, align 4
  %lastNearbyFace5 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %6, i32 0, i32 4
  %7 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %lastNearbyFace5, align 8
  %tobool6 = icmp ne %"class.btConvexHullInternal::Face"* %7, null
  br i1 %tobool6, label %if.then7, label %if.end10

if.then7:                                         ; preds = %if.end
  %8 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %src.addr, align 4
  %lastNearbyFace8 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %8, i32 0, i32 4
  %9 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %lastNearbyFace8, align 8
  %lastNearbyFace9 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 4
  store %"class.btConvexHullInternal::Face"* %9, %"class.btConvexHullInternal::Face"** %lastNearbyFace9, align 8
  br label %if.end10

if.end10:                                         ; preds = %if.then7, %if.end
  %10 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %src.addr, align 4
  %firstNearbyFace11 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %10, i32 0, i32 3
  %11 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %firstNearbyFace11, align 4
  store %"class.btConvexHullInternal::Face"* %11, %"class.btConvexHullInternal::Face"** %f, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end10
  %12 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %f, align 4
  %tobool12 = icmp ne %"class.btConvexHullInternal::Face"* %12, null
  br i1 %tobool12, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %f, align 4
  %nearbyVertex = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %13, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* %this1, %"class.btConvexHullInternal::Vertex"** %nearbyVertex, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %f, align 4
  %nextWithSameNearbyVertex13 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %14, i32 0, i32 2
  %15 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %nextWithSameNearbyVertex13, align 4
  store %"class.btConvexHullInternal::Face"* %15, %"class.btConvexHullInternal::Face"** %f, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %16 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %src.addr, align 4
  %firstNearbyFace14 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %16, i32 0, i32 3
  store %"class.btConvexHullInternal::Face"* null, %"class.btConvexHullInternal::Face"** %firstNearbyFace14, align 4
  %17 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %src.addr, align 4
  %lastNearbyFace15 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %17, i32 0, i32 4
  store %"class.btConvexHullInternal::Face"* null, %"class.btConvexHullInternal::Face"** %lastNearbyFace15, align 8
  ret void
}

; Function Attrs: noinline optnone
define hidden float @_ZN20btConvexHullComputer7computeEPKvbiiff(%class.btConvexHullComputer* %this, i8* %coords, i1 zeroext %doubleCoords, i32 %stride, i32 %count, float %shrink, float %shrinkClamp) #2 {
entry:
  %retval = alloca float, align 4
  %this.addr = alloca %class.btConvexHullComputer*, align 4
  %coords.addr = alloca i8*, align 4
  %doubleCoords.addr = alloca i8, align 1
  %stride.addr = alloca i32, align 4
  %count.addr = alloca i32, align 4
  %shrink.addr = alloca float, align 4
  %shrinkClamp.addr = alloca float, align 4
  %hull = alloca %class.btConvexHullInternal, align 4
  %shift = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %"class.btConvexHullComputer::Edge", align 4
  %ref.tmp15 = alloca i32, align 4
  %oldVertices = alloca %class.btAlignedObjectArray, align 4
  %copied = alloca i32, align 4
  %v = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %ref.tmp22 = alloca %class.btVector3, align 4
  %firstEdge = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %firstCopy = alloca i32, align 4
  %prevCopy = alloca i32, align 4
  %e = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %s = alloca i32, align 4
  %ref.tmp31 = alloca %"class.btConvexHullComputer::Edge", align 4
  %ref.tmp33 = alloca %"class.btConvexHullComputer::Edge", align 4
  %c = alloca %"class.btConvexHullComputer::Edge"*, align 4
  %r = alloca %"class.btConvexHullComputer::Edge"*, align 4
  %i = alloca i32, align 4
  %v63 = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %firstEdge65 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %e69 = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %f = alloca %"class.btConvexHullInternal::Edge"*, align 4
  store %class.btConvexHullComputer* %this, %class.btConvexHullComputer** %this.addr, align 4
  store i8* %coords, i8** %coords.addr, align 4
  %frombool = zext i1 %doubleCoords to i8
  store i8 %frombool, i8* %doubleCoords.addr, align 1
  store i32 %stride, i32* %stride.addr, align 4
  store i32 %count, i32* %count.addr, align 4
  store float %shrink, float* %shrink.addr, align 4
  store float %shrinkClamp, float* %shrinkClamp.addr, align 4
  %this1 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %this.addr, align 4
  %0 = load i32, i32* %count.addr, align 4
  %cmp = icmp sle i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %vertices = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 0
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.12* %vertices)
  %edges = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE5clearEv(%class.btAlignedObjectArray.16* %edges)
  %faces = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.20* %faces)
  store float 0.000000e+00, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %call = call %class.btConvexHullInternal* @_ZN20btConvexHullInternalC2Ev(%class.btConvexHullInternal* %hull)
  %1 = load i8*, i8** %coords.addr, align 4
  %2 = load i8, i8* %doubleCoords.addr, align 1
  %tobool = trunc i8 %2 to i1
  %3 = load i32, i32* %stride.addr, align 4
  %4 = load i32, i32* %count.addr, align 4
  call void @_ZN20btConvexHullInternal7computeEPKvbii(%class.btConvexHullInternal* %hull, i8* %1, i1 zeroext %tobool, i32 %3, i32 %4)
  store float 0.000000e+00, float* %shift, align 4
  %5 = load float, float* %shrink.addr, align 4
  %cmp2 = fcmp ogt float %5, 0.000000e+00
  br i1 %cmp2, label %land.lhs.true, label %if.end9

land.lhs.true:                                    ; preds = %if.end
  %6 = load float, float* %shrink.addr, align 4
  %7 = load float, float* %shrinkClamp.addr, align 4
  %call3 = call float @_ZN20btConvexHullInternal6shrinkEff(%class.btConvexHullInternal* %hull, float %6, float %7)
  store float %call3, float* %shift, align 4
  %cmp4 = fcmp olt float %call3, 0.000000e+00
  br i1 %cmp4, label %if.then5, label %if.end9

if.then5:                                         ; preds = %land.lhs.true
  %vertices6 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 0
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.12* %vertices6)
  %edges7 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE5clearEv(%class.btAlignedObjectArray.16* %edges7)
  %faces8 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.20* %faces8)
  %8 = load float, float* %shift, align 4
  store float %8, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end9:                                          ; preds = %land.lhs.true, %if.end
  %vertices10 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 0
  %call11 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.12* %vertices10, i32 0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %edges12 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 1
  %9 = bitcast %"class.btConvexHullComputer::Edge"* %ref.tmp13 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %9, i8 0, i32 12, i1 false)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE6resizeEiRKS1_(%class.btAlignedObjectArray.16* %edges12, i32 0, %"class.btConvexHullComputer::Edge"* nonnull align 4 dereferenceable(12) %ref.tmp13)
  %faces14 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 2
  store i32 0, i32* %ref.tmp15, align 4
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.20* %faces14, i32 0, i32* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %call16 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEC2Ev(%class.btAlignedObjectArray* %oldVertices)
  %vertexList = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %hull, i32 0, i32 12
  %10 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %vertexList, align 4
  %call17 = call i32 @_ZL13getVertexCopyPN20btConvexHullInternal6VertexER20btAlignedObjectArrayIS1_E(%"class.btConvexHullInternal::Vertex"* %10, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %oldVertices)
  store i32 0, i32* %copied, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end61, %if.end9
  %11 = load i32, i32* %copied, align 4
  %call18 = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4sizeEv(%class.btAlignedObjectArray* %oldVertices)
  %cmp19 = icmp slt i32 %11, %call18
  br i1 %cmp19, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %12 = load i32, i32* %copied, align 4
  %call20 = call nonnull align 4 dereferenceable(4) %"class.btConvexHullInternal::Vertex"** @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEixEi(%class.btAlignedObjectArray* %oldVertices, i32 %12)
  %13 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %call20, align 4
  store %"class.btConvexHullInternal::Vertex"* %13, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %vertices21 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 0
  %14 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  call void @_ZN20btConvexHullInternal14getCoordinatesEPKNS_6VertexE(%class.btVector3* sret align 4 %ref.tmp22, %class.btConvexHullInternal* %hull, %"class.btConvexHullInternal::Vertex"* %14)
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray.12* %vertices21, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp22)
  %15 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v, align 4
  %edges23 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %15, i32 0, i32 2
  %16 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edges23, align 8
  store %"class.btConvexHullInternal::Edge"* %16, %"class.btConvexHullInternal::Edge"** %firstEdge, align 4
  %17 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstEdge, align 4
  %tobool24 = icmp ne %"class.btConvexHullInternal::Edge"* %17, null
  br i1 %tobool24, label %if.then25, label %if.end61

if.then25:                                        ; preds = %while.body
  store i32 -1, i32* %firstCopy, align 4
  store i32 -1, i32* %prevCopy, align 4
  %18 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstEdge, align 4
  store %"class.btConvexHullInternal::Edge"* %18, %"class.btConvexHullInternal::Edge"** %e, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then25
  %19 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %copy = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %19, i32 0, i32 5
  %20 = load i32, i32* %copy, align 4
  %cmp26 = icmp slt i32 %20, 0
  br i1 %cmp26, label %if.then27, label %if.end45

if.then27:                                        ; preds = %do.body
  %edges28 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 1
  %call29 = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv(%class.btAlignedObjectArray.16* %edges28)
  store i32 %call29, i32* %s, align 4
  %edges30 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 1
  %21 = bitcast %"class.btConvexHullComputer::Edge"* %ref.tmp31 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %21, i8 0, i32 12, i1 false)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE9push_backERKS1_(%class.btAlignedObjectArray.16* %edges30, %"class.btConvexHullComputer::Edge"* nonnull align 4 dereferenceable(12) %ref.tmp31)
  %edges32 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 1
  %22 = bitcast %"class.btConvexHullComputer::Edge"* %ref.tmp33 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %22, i8 0, i32 12, i1 false)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE9push_backERKS1_(%class.btAlignedObjectArray.16* %edges32, %"class.btConvexHullComputer::Edge"* nonnull align 4 dereferenceable(12) %ref.tmp33)
  %edges34 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 1
  %23 = load i32, i32* %s, align 4
  %call35 = call nonnull align 4 dereferenceable(12) %"class.btConvexHullComputer::Edge"* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEixEi(%class.btAlignedObjectArray.16* %edges34, i32 %23)
  store %"class.btConvexHullComputer::Edge"* %call35, %"class.btConvexHullComputer::Edge"** %c, align 4
  %edges36 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 1
  %24 = load i32, i32* %s, align 4
  %add = add nsw i32 %24, 1
  %call37 = call nonnull align 4 dereferenceable(12) %"class.btConvexHullComputer::Edge"* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEixEi(%class.btAlignedObjectArray.16* %edges36, i32 %add)
  store %"class.btConvexHullComputer::Edge"* %call37, %"class.btConvexHullComputer::Edge"** %r, align 4
  %25 = load i32, i32* %s, align 4
  %26 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %copy38 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %26, i32 0, i32 5
  store i32 %25, i32* %copy38, align 4
  %27 = load i32, i32* %s, align 4
  %add39 = add nsw i32 %27, 1
  %28 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %reverse = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %28, i32 0, i32 2
  %29 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse, align 4
  %copy40 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %29, i32 0, i32 5
  store i32 %add39, i32* %copy40, align 4
  %30 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %c, align 4
  %reverse41 = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %30, i32 0, i32 1
  store i32 1, i32* %reverse41, align 4
  %31 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %r, align 4
  %reverse42 = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %31, i32 0, i32 1
  store i32 -1, i32* %reverse42, align 4
  %32 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %target = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %32, i32 0, i32 3
  %33 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %target, align 4
  %call43 = call i32 @_ZL13getVertexCopyPN20btConvexHullInternal6VertexER20btAlignedObjectArrayIS1_E(%"class.btConvexHullInternal::Vertex"* %33, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %oldVertices)
  %34 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %c, align 4
  %targetVertex = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %34, i32 0, i32 2
  store i32 %call43, i32* %targetVertex, align 4
  %35 = load i32, i32* %copied, align 4
  %36 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %r, align 4
  %targetVertex44 = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %36, i32 0, i32 2
  store i32 %35, i32* %targetVertex44, align 4
  br label %if.end45

if.end45:                                         ; preds = %if.then27, %do.body
  %37 = load i32, i32* %prevCopy, align 4
  %cmp46 = icmp sge i32 %37, 0
  br i1 %cmp46, label %if.then47, label %if.else

if.then47:                                        ; preds = %if.end45
  %38 = load i32, i32* %prevCopy, align 4
  %39 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %copy48 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %39, i32 0, i32 5
  %40 = load i32, i32* %copy48, align 4
  %sub = sub nsw i32 %38, %40
  %edges49 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 1
  %41 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %copy50 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %41, i32 0, i32 5
  %42 = load i32, i32* %copy50, align 4
  %call51 = call nonnull align 4 dereferenceable(12) %"class.btConvexHullComputer::Edge"* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEixEi(%class.btAlignedObjectArray.16* %edges49, i32 %42)
  %next = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %call51, i32 0, i32 0
  store i32 %sub, i32* %next, align 4
  br label %if.end53

if.else:                                          ; preds = %if.end45
  %43 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %copy52 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %43, i32 0, i32 5
  %44 = load i32, i32* %copy52, align 4
  store i32 %44, i32* %firstCopy, align 4
  br label %if.end53

if.end53:                                         ; preds = %if.else, %if.then47
  %45 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %copy54 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %45, i32 0, i32 5
  %46 = load i32, i32* %copy54, align 4
  store i32 %46, i32* %prevCopy, align 4
  %47 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %next55 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %47, i32 0, i32 0
  %48 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next55, align 4
  store %"class.btConvexHullInternal::Edge"* %48, %"class.btConvexHullInternal::Edge"** %e, align 4
  br label %do.cond

do.cond:                                          ; preds = %if.end53
  %49 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e, align 4
  %50 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstEdge, align 4
  %cmp56 = icmp ne %"class.btConvexHullInternal::Edge"* %49, %50
  br i1 %cmp56, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %51 = load i32, i32* %prevCopy, align 4
  %52 = load i32, i32* %firstCopy, align 4
  %sub57 = sub nsw i32 %51, %52
  %edges58 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 1
  %53 = load i32, i32* %firstCopy, align 4
  %call59 = call nonnull align 4 dereferenceable(12) %"class.btConvexHullComputer::Edge"* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEixEi(%class.btAlignedObjectArray.16* %edges58, i32 %53)
  %next60 = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %call59, i32 0, i32 0
  store i32 %sub57, i32* %next60, align 4
  br label %if.end61

if.end61:                                         ; preds = %do.end, %while.body
  %54 = load i32, i32* %copied, align 4
  %inc = add nsw i32 %54, 1
  store i32 %inc, i32* %copied, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.end
  %55 = load i32, i32* %i, align 4
  %56 = load i32, i32* %copied, align 4
  %cmp62 = icmp slt i32 %55, %56
  br i1 %cmp62, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %57 = load i32, i32* %i, align 4
  %call64 = call nonnull align 4 dereferenceable(4) %"class.btConvexHullInternal::Vertex"** @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEixEi(%class.btAlignedObjectArray* %oldVertices, i32 %57)
  %58 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %call64, align 4
  store %"class.btConvexHullInternal::Vertex"* %58, %"class.btConvexHullInternal::Vertex"** %v63, align 4
  %59 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %v63, align 4
  %edges66 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %59, i32 0, i32 2
  %60 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %edges66, align 8
  store %"class.btConvexHullInternal::Edge"* %60, %"class.btConvexHullInternal::Edge"** %firstEdge65, align 4
  %61 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstEdge65, align 4
  %tobool67 = icmp ne %"class.btConvexHullInternal::Edge"* %61, null
  br i1 %tobool67, label %if.then68, label %if.end87

if.then68:                                        ; preds = %for.body
  %62 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstEdge65, align 4
  store %"class.btConvexHullInternal::Edge"* %62, %"class.btConvexHullInternal::Edge"** %e69, align 4
  br label %do.body70

do.body70:                                        ; preds = %do.cond84, %if.then68
  %63 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e69, align 4
  %copy71 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %63, i32 0, i32 5
  %64 = load i32, i32* %copy71, align 4
  %cmp72 = icmp sge i32 %64, 0
  br i1 %cmp72, label %if.then73, label %if.end82

if.then73:                                        ; preds = %do.body70
  %faces74 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 2
  %65 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e69, align 4
  %copy75 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %65, i32 0, i32 5
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.20* %faces74, i32* nonnull align 4 dereferenceable(4) %copy75)
  %66 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e69, align 4
  store %"class.btConvexHullInternal::Edge"* %66, %"class.btConvexHullInternal::Edge"** %f, align 4
  br label %do.body76

do.body76:                                        ; preds = %do.cond79, %if.then73
  %67 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f, align 4
  %copy77 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %67, i32 0, i32 5
  store i32 -1, i32* %copy77, align 4
  %68 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f, align 4
  %reverse78 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %68, i32 0, i32 2
  %69 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %reverse78, align 4
  %prev = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %69, i32 0, i32 1
  %70 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %prev, align 4
  store %"class.btConvexHullInternal::Edge"* %70, %"class.btConvexHullInternal::Edge"** %f, align 4
  br label %do.cond79

do.cond79:                                        ; preds = %do.body76
  %71 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %f, align 4
  %72 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e69, align 4
  %cmp80 = icmp ne %"class.btConvexHullInternal::Edge"* %71, %72
  br i1 %cmp80, label %do.body76, label %do.end81

do.end81:                                         ; preds = %do.cond79
  br label %if.end82

if.end82:                                         ; preds = %do.end81, %do.body70
  %73 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e69, align 4
  %next83 = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %73, i32 0, i32 0
  %74 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %next83, align 4
  store %"class.btConvexHullInternal::Edge"* %74, %"class.btConvexHullInternal::Edge"** %e69, align 4
  br label %do.cond84

do.cond84:                                        ; preds = %if.end82
  %75 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %e69, align 4
  %76 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %firstEdge65, align 4
  %cmp85 = icmp ne %"class.btConvexHullInternal::Edge"* %75, %76
  br i1 %cmp85, label %do.body70, label %do.end86

do.end86:                                         ; preds = %do.cond84
  br label %if.end87

if.end87:                                         ; preds = %do.end86, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end87
  %77 = load i32, i32* %i, align 4
  %inc88 = add nsw i32 %77, 1
  store i32 %inc88, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %78 = load float, float* %shift, align 4
  store float %78, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %call89 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEED2Ev(%class.btAlignedObjectArray* %oldVertices) #7
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then5
  %call90 = call %class.btConvexHullInternal* @_ZN20btConvexHullInternalD2Ev(%class.btConvexHullInternal* %hull) #7
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %79 = load float, float* %retval, align 4
  ret float %79
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.12* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.12* %this1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE5clearEv(%class.btAlignedObjectArray.16* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7destroyEii(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE10deallocateEv(%class.btAlignedObjectArray.16* %this1)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4initEv(%class.btAlignedObjectArray.16* %this1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.20* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.20* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.20* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.20* %this1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btConvexHullInternal* @_ZN20btConvexHullInternalC2Ev(%class.btConvexHullInternal* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexHullInternal*, align 4
  store %class.btConvexHullInternal* %this, %class.btConvexHullInternal** %this.addr, align 4
  %this1 = load %class.btConvexHullInternal*, %class.btConvexHullInternal** %this.addr, align 4
  %scaling = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %scaling)
  %center = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %center)
  %vertexPool = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 2
  %call3 = call %"class.btConvexHullInternal::Pool"* @_ZN20btConvexHullInternal4PoolINS_6VertexEEC2Ev(%"class.btConvexHullInternal::Pool"* %vertexPool)
  %edgePool = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 3
  %call4 = call %"class.btConvexHullInternal::Pool.0"* @_ZN20btConvexHullInternal4PoolINS_4EdgeEEC2Ev(%"class.btConvexHullInternal::Pool.0"* %edgePool)
  %facePool = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 4
  %call5 = call %"class.btConvexHullInternal::Pool.2"* @_ZN20btConvexHullInternal4PoolINS_4FaceEEC2Ev(%"class.btConvexHullInternal::Pool.2"* %facePool)
  %originalVertices = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 5
  %call6 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEEC2Ev(%class.btAlignedObjectArray* %originalVertices)
  ret %class.btConvexHullInternal* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.12* %this, i32 %newsize, %class.btVector3* nonnull align 4 dereferenceable(16) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btVector3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btVector3* %fillData, %class.btVector3** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %5 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end15

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.12* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %14 = load %class.btVector3*, %class.btVector3** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btVector3, %class.btVector3* %14, i32 %15
  %16 = bitcast %class.btVector3* %arrayidx10 to i8*
  %call11 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %16)
  %17 = bitcast i8* %call11 to %class.btVector3*
  %18 = load %class.btVector3*, %class.btVector3** %fillData.addr, align 4
  %19 = bitcast %class.btVector3* %17 to i8*
  %20 = bitcast %class.btVector3* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc13 = add nsw i32 %21, 1
  store i32 %inc13, i32* %i5, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  br label %if.end15

if.end15:                                         ; preds = %for.end14, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE6resizeEiRKS1_(%class.btAlignedObjectArray.16* %this, i32 %newsize, %"class.btConvexHullComputer::Edge"* nonnull align 4 dereferenceable(12) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %"class.btConvexHullComputer::Edge"* %fillData, %"class.btConvexHullComputer::Edge"** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %5 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7reserveEi(%class.btAlignedObjectArray.16* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %14 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %14, i32 %15
  %16 = bitcast %"class.btConvexHullComputer::Edge"* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %"class.btConvexHullComputer::Edge"*
  %18 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %fillData.addr, align 4
  %19 = bitcast %"class.btConvexHullComputer::Edge"* %17 to i8*
  %20 = bitcast %"class.btConvexHullComputer::Edge"* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 12, i1 false)
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %21, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.20* %this, i32 %newsize, i32* nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i32*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store i32* %fillData, i32** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %5 = load i32*, i32** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.20* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %14 = load i32*, i32** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds i32, i32* %14, i32 %15
  %16 = bitcast i32* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to i32*
  %18 = load i32*, i32** %fillData.addr, align 4
  %19 = load i32, i32* %18, align 4
  store i32 %19, i32* %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define internal i32 @_ZL13getVertexCopyPN20btConvexHullInternal6VertexER20btAlignedObjectArrayIS1_E(%"class.btConvexHullInternal::Vertex"* %vertex, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %vertices) #2 {
entry:
  %vertex.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %vertices.addr = alloca %class.btAlignedObjectArray*, align 4
  %index = alloca i32, align 4
  store %"class.btConvexHullInternal::Vertex"* %vertex, %"class.btConvexHullInternal::Vertex"** %vertex.addr, align 4
  store %class.btAlignedObjectArray* %vertices, %class.btAlignedObjectArray** %vertices.addr, align 4
  %0 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %vertex.addr, align 4
  %copy = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %0, i32 0, i32 8
  %1 = load i32, i32* %copy, align 8
  store i32 %1, i32* %index, align 4
  %2 = load i32, i32* %index, align 4
  %cmp = icmp slt i32 %2, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %vertices.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4sizeEv(%class.btAlignedObjectArray* %3)
  store i32 %call, i32* %index, align 4
  %4 = load i32, i32* %index, align 4
  %5 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %vertex.addr, align 4
  %copy1 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %5, i32 0, i32 8
  store i32 %4, i32* %copy1, align 8
  %6 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %vertices.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9push_backERKS2_(%class.btAlignedObjectArray* %6, %"class.btConvexHullInternal::Vertex"** nonnull align 4 dereferenceable(4) %vertex.addr)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load i32, i32* %index, align 4
  ret i32 %7
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray.12* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Val.addr = alloca %class.btVector3*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store %class.btVector3* %_Val, %class.btVector3** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray.12* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.12* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %1 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 %2
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  %call5 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %3)
  %4 = bitcast i8* %call5 to %class.btVector3*
  %5 = load %class.btVector3*, %class.btVector3** %_Val.addr, align 4
  %6 = bitcast %class.btVector3* %4 to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size6, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv(%class.btAlignedObjectArray.16* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE9push_backERKS1_(%class.btAlignedObjectArray.16* %this, %"class.btConvexHullComputer::Edge"* nonnull align 4 dereferenceable(12) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %_Val.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store %"class.btConvexHullComputer::Edge"* %_Val, %"class.btConvexHullComputer::Edge"** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE8capacityEv(%class.btAlignedObjectArray.16* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE9allocSizeEi(%class.btAlignedObjectArray.16* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7reserveEi(%class.btAlignedObjectArray.16* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %1 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %1, i32 %2
  %3 = bitcast %"class.btConvexHullComputer::Edge"* %arrayidx to i8*
  %4 = bitcast i8* %3 to %"class.btConvexHullComputer::Edge"*
  %5 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %_Val.addr, align 4
  %6 = bitcast %"class.btConvexHullComputer::Edge"* %4 to i8*
  %7 = bitcast %"class.btConvexHullComputer::Edge"* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 12, i1 false)
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.btConvexHullComputer::Edge"* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEixEi(%class.btAlignedObjectArray.16* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %0, i32 %1
  ret %"class.btConvexHullComputer::Edge"* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.20* %this, i32* nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %_Val.addr = alloca i32*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  store i32* %_Val, i32** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.20* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIiE9allocSizeEi(%class.btAlignedObjectArray.20* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.20* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %1 = load i32*, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  %3 = bitcast i32* %arrayidx to i8*
  %4 = bitcast i8* %3 to i32*
  %5 = load i32*, i32** %_Val.addr, align 4
  %6 = load i32, i32* %5, align 4
  store i32 %6, i32* %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btConvexHullInternal* @_ZN20btConvexHullInternalD2Ev(%class.btConvexHullInternal* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexHullInternal*, align 4
  store %class.btConvexHullInternal* %this, %class.btConvexHullInternal** %this.addr, align 4
  %this1 = load %class.btConvexHullInternal*, %class.btConvexHullInternal** %this.addr, align 4
  %originalVertices = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 5
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEED2Ev(%class.btAlignedObjectArray* %originalVertices) #7
  %facePool = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 4
  %call2 = call %"class.btConvexHullInternal::Pool.2"* @_ZN20btConvexHullInternal4PoolINS_4FaceEED2Ev(%"class.btConvexHullInternal::Pool.2"* %facePool) #7
  %edgePool = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 3
  %call3 = call %"class.btConvexHullInternal::Pool.0"* @_ZN20btConvexHullInternal4PoolINS_4EdgeEED2Ev(%"class.btConvexHullInternal::Pool.0"* %edgePool) #7
  %vertexPool = getelementptr inbounds %class.btConvexHullInternal, %class.btConvexHullInternal* %this1, i32 0, i32 2
  %call4 = call %"class.btConvexHullInternal::Pool"* @_ZN20btConvexHullInternal4PoolINS_6VertexEED2Ev(%"class.btConvexHullInternal::Pool"* %vertexPool) #7
  ret %class.btConvexHullInternal* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Point64"* @_ZN20btConvexHullInternal7Point64C2Exxx(%"class.btConvexHullInternal::Point64"* returned %this, i64 %x, i64 %y, i64 %z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Point64"*, align 4
  %x.addr = alloca i64, align 8
  %y.addr = alloca i64, align 8
  %z.addr = alloca i64, align 8
  store %"class.btConvexHullInternal::Point64"* %this, %"class.btConvexHullInternal::Point64"** %this.addr, align 4
  store i64 %x, i64* %x.addr, align 8
  store i64 %y, i64* %y.addr, align 8
  store i64 %z, i64* %z.addr, align 8
  %this1 = load %"class.btConvexHullInternal::Point64"*, %"class.btConvexHullInternal::Point64"** %this.addr, align 4
  %x2 = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %this1, i32 0, i32 0
  %0 = load i64, i64* %x.addr, align 8
  store i64 %0, i64* %x2, align 8
  %y3 = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %this1, i32 0, i32 1
  %1 = load i64, i64* %y.addr, align 8
  store i64 %1, i64* %y3, align 8
  %z4 = getelementptr inbounds %"class.btConvexHullInternal::Point64", %"class.btConvexHullInternal::Point64"* %this1, i32 0, i32 2
  %2 = load i64, i64* %z.addr, align 8
  store i64 %2, i64* %z4, align 8
  ret %"class.btConvexHullInternal::Point64"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btConvexHullInternal4PoolINS_4EdgeEE10freeObjectEPS1_(%"class.btConvexHullInternal::Pool.0"* %this, %"class.btConvexHullInternal::Edge"* %object) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Pool.0"*, align 4
  %object.addr = alloca %"class.btConvexHullInternal::Edge"*, align 4
  store %"class.btConvexHullInternal::Pool.0"* %this, %"class.btConvexHullInternal::Pool.0"** %this.addr, align 4
  store %"class.btConvexHullInternal::Edge"* %object, %"class.btConvexHullInternal::Edge"** %object.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Pool.0"*, %"class.btConvexHullInternal::Pool.0"** %this.addr, align 4
  %0 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %object.addr, align 4
  %call = call %"class.btConvexHullInternal::Edge"* @_ZN20btConvexHullInternal4EdgeD2Ev(%"class.btConvexHullInternal::Edge"* %0) #7
  %freeObjects = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 2
  %1 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %freeObjects, align 4
  %2 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %object.addr, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %2, i32 0, i32 0
  store %"class.btConvexHullInternal::Edge"* %1, %"class.btConvexHullInternal::Edge"** %next, align 4
  %3 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %object.addr, align 4
  %freeObjects2 = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* %3, %"class.btConvexHullInternal::Edge"** %freeObjects2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Edge"* @_ZN20btConvexHullInternal4EdgeD2Ev(%"class.btConvexHullInternal::Edge"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Edge"*, align 4
  store %"class.btConvexHullInternal::Edge"* %this, %"class.btConvexHullInternal::Edge"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %this.addr, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %this1, i32 0, i32 0
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %next, align 4
  %prev = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %this1, i32 0, i32 1
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %prev, align 4
  %reverse = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %this1, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %reverse, align 4
  %target = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %this1, i32 0, i32 3
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %target, align 4
  %face = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %this1, i32 0, i32 4
  store %"class.btConvexHullInternal::Face"* null, %"class.btConvexHullInternal::Face"** %face, align 4
  ret %"class.btConvexHullInternal::Edge"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %b.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %a.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = load float*, float** %a.addr, align 4
  store float %5, float* %6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %b.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = load float*, float** %a.addr, align 4
  store float %5, float* %6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Rational128"* @_ZN20btConvexHullInternal11Rational128C2Ex(%"class.btConvexHullInternal::Rational128"* returned %this, i64 %value) unnamed_addr #2 comdat {
entry:
  %retval = alloca %"class.btConvexHullInternal::Rational128"*, align 4
  %this.addr = alloca %"class.btConvexHullInternal::Rational128"*, align 4
  %value.addr = alloca i64, align 8
  %ref.tmp = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp8 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp13 = alloca %"class.btConvexHullInternal::Int128", align 8
  %ref.tmp17 = alloca %"class.btConvexHullInternal::Int128", align 8
  store %"class.btConvexHullInternal::Rational128"* %this, %"class.btConvexHullInternal::Rational128"** %this.addr, align 4
  store i64 %value, i64* %value.addr, align 8
  %this1 = load %"class.btConvexHullInternal::Rational128"*, %"class.btConvexHullInternal::Rational128"** %this.addr, align 4
  store %"class.btConvexHullInternal::Rational128"* %this1, %"class.btConvexHullInternal::Rational128"** %retval, align 4
  %numerator = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 0
  %call = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ev(%"class.btConvexHullInternal::Int128"* %numerator)
  %denominator = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 1
  %call2 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ev(%"class.btConvexHullInternal::Int128"* %denominator)
  %0 = load i64, i64* %value.addr, align 8
  %cmp = icmp sgt i64 %0, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %sign = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 2
  store i32 1, i32* %sign, align 8
  %1 = load i64, i64* %value.addr, align 8
  %call3 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ex(%"class.btConvexHullInternal::Int128"* %ref.tmp, i64 %1)
  %numerator4 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 0
  %2 = bitcast %"class.btConvexHullInternal::Int128"* %numerator4 to i8*
  %3 = bitcast %"class.btConvexHullInternal::Int128"* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %2, i8* align 8 %3, i32 16, i1 false)
  br label %if.end16

if.else:                                          ; preds = %entry
  %4 = load i64, i64* %value.addr, align 8
  %cmp5 = icmp slt i64 %4, 0
  br i1 %cmp5, label %if.then6, label %if.else11

if.then6:                                         ; preds = %if.else
  %sign7 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 2
  store i32 -1, i32* %sign7, align 8
  %5 = load i64, i64* %value.addr, align 8
  %sub = sub nsw i64 0, %5
  %call9 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ex(%"class.btConvexHullInternal::Int128"* %ref.tmp8, i64 %sub)
  %numerator10 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 0
  %6 = bitcast %"class.btConvexHullInternal::Int128"* %numerator10 to i8*
  %7 = bitcast %"class.btConvexHullInternal::Int128"* %ref.tmp8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %6, i8* align 8 %7, i32 16, i1 false)
  br label %if.end

if.else11:                                        ; preds = %if.else
  %sign12 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 2
  store i32 0, i32* %sign12, align 8
  %call14 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ey(%"class.btConvexHullInternal::Int128"* %ref.tmp13, i64 0)
  %numerator15 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 0
  %8 = bitcast %"class.btConvexHullInternal::Int128"* %numerator15 to i8*
  %9 = bitcast %"class.btConvexHullInternal::Int128"* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %8, i8* align 8 %9, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else11, %if.then6
  br label %if.end16

if.end16:                                         ; preds = %if.end, %if.then
  %call18 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ey(%"class.btConvexHullInternal::Int128"* %ref.tmp17, i64 1)
  %denominator19 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 1
  %10 = bitcast %"class.btConvexHullInternal::Int128"* %denominator19 to i8*
  %11 = bitcast %"class.btConvexHullInternal::Int128"* %ref.tmp17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %10, i8* align 8 %11, i32 16, i1 false)
  %isInt64 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 3
  store i8 1, i8* %isInt64, align 4
  %12 = load %"class.btConvexHullInternal::Rational128"*, %"class.btConvexHullInternal::Rational128"** %retval, align 4
  ret %"class.btConvexHullInternal::Rational128"* %12
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Rational128"* @_ZN20btConvexHullInternal11Rational128C2ERKNS_6Int128ES3_(%"class.btConvexHullInternal::Rational128"* returned %this, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %numerator, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %denominator) unnamed_addr #2 comdat {
entry:
  %retval = alloca %"class.btConvexHullInternal::Rational128"*, align 4
  %this.addr = alloca %"class.btConvexHullInternal::Rational128"*, align 4
  %numerator.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  %denominator.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  %ref.tmp = alloca %"class.btConvexHullInternal::Int128", align 8
  %dsign = alloca i32, align 4
  %ref.tmp16 = alloca %"class.btConvexHullInternal::Int128", align 8
  store %"class.btConvexHullInternal::Rational128"* %this, %"class.btConvexHullInternal::Rational128"** %this.addr, align 4
  store %"class.btConvexHullInternal::Int128"* %numerator, %"class.btConvexHullInternal::Int128"** %numerator.addr, align 4
  store %"class.btConvexHullInternal::Int128"* %denominator, %"class.btConvexHullInternal::Int128"** %denominator.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Rational128"*, %"class.btConvexHullInternal::Rational128"** %this.addr, align 4
  store %"class.btConvexHullInternal::Rational128"* %this1, %"class.btConvexHullInternal::Rational128"** %retval, align 4
  %numerator2 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 0
  %call = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ev(%"class.btConvexHullInternal::Int128"* %numerator2)
  %denominator3 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 1
  %call4 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ev(%"class.btConvexHullInternal::Int128"* %denominator3)
  %0 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %numerator.addr, align 4
  %call5 = call i32 @_ZNK20btConvexHullInternal6Int1287getSignEv(%"class.btConvexHullInternal::Int128"* %0)
  %sign = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 2
  store i32 %call5, i32* %sign, align 8
  %sign6 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 2
  %1 = load i32, i32* %sign6, align 8
  %cmp = icmp sge i32 %1, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %numerator.addr, align 4
  %numerator7 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 0
  %3 = bitcast %"class.btConvexHullInternal::Int128"* %numerator7 to i8*
  %4 = bitcast %"class.btConvexHullInternal::Int128"* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %3, i8* align 8 %4, i32 16, i1 false)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %numerator.addr, align 4
  call void @_ZNK20btConvexHullInternal6Int128ngEv(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp, %"class.btConvexHullInternal::Int128"* %5)
  %numerator8 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 0
  %6 = bitcast %"class.btConvexHullInternal::Int128"* %numerator8 to i8*
  %7 = bitcast %"class.btConvexHullInternal::Int128"* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %6, i8* align 8 %7, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %8 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %denominator.addr, align 4
  %call9 = call i32 @_ZNK20btConvexHullInternal6Int1287getSignEv(%"class.btConvexHullInternal::Int128"* %8)
  store i32 %call9, i32* %dsign, align 4
  %9 = load i32, i32* %dsign, align 4
  %cmp10 = icmp sge i32 %9, 0
  br i1 %cmp10, label %if.then11, label %if.else13

if.then11:                                        ; preds = %if.end
  %10 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %denominator.addr, align 4
  %denominator12 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 1
  %11 = bitcast %"class.btConvexHullInternal::Int128"* %denominator12 to i8*
  %12 = bitcast %"class.btConvexHullInternal::Int128"* %10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %11, i8* align 8 %12, i32 16, i1 false)
  br label %if.end18

if.else13:                                        ; preds = %if.end
  %sign14 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 2
  %13 = load i32, i32* %sign14, align 8
  %sub = sub nsw i32 0, %13
  %sign15 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 2
  store i32 %sub, i32* %sign15, align 8
  %14 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %denominator.addr, align 4
  call void @_ZNK20btConvexHullInternal6Int128ngEv(%"class.btConvexHullInternal::Int128"* sret align 8 %ref.tmp16, %"class.btConvexHullInternal::Int128"* %14)
  %denominator17 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 1
  %15 = bitcast %"class.btConvexHullInternal::Int128"* %denominator17 to i8*
  %16 = bitcast %"class.btConvexHullInternal::Int128"* %ref.tmp16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %15, i8* align 8 %16, i32 16, i1 false)
  br label %if.end18

if.end18:                                         ; preds = %if.else13, %if.then11
  %isInt64 = getelementptr inbounds %"class.btConvexHullInternal::Rational128", %"class.btConvexHullInternal::Rational128"* %this1, i32 0, i32 3
  store i8 0, i8* %isInt64, align 4
  %17 = load %"class.btConvexHullInternal::Rational128"*, %"class.btConvexHullInternal::Rational128"** %retval, align 4
  ret %"class.btConvexHullInternal::Rational128"* %17
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ey(%"class.btConvexHullInternal::Int128"* returned %this, i64 %low) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  %low.addr = alloca i64, align 8
  store %"class.btConvexHullInternal::Int128"* %this, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  store i64 %low, i64* %low.addr, align 8
  %this1 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  %low2 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 0
  %0 = load i64, i64* %low.addr, align 8
  store i64 %0, i64* %low2, align 8
  %high = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 1
  store i64 0, i64* %high, align 8
  ret %"class.btConvexHullInternal::Int128"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Pool"* @_ZN20btConvexHullInternal4PoolINS_6VertexEEC2Ev(%"class.btConvexHullInternal::Pool"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Pool"*, align 4
  store %"class.btConvexHullInternal::Pool"* %this, %"class.btConvexHullInternal::Pool"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Pool"*, %"class.btConvexHullInternal::Pool"** %this.addr, align 4
  %arrays = getelementptr inbounds %"class.btConvexHullInternal::Pool", %"class.btConvexHullInternal::Pool"* %this1, i32 0, i32 0
  store %"class.btConvexHullInternal::PoolArray"* null, %"class.btConvexHullInternal::PoolArray"** %arrays, align 4
  %nextArray = getelementptr inbounds %"class.btConvexHullInternal::Pool", %"class.btConvexHullInternal::Pool"* %this1, i32 0, i32 1
  store %"class.btConvexHullInternal::PoolArray"* null, %"class.btConvexHullInternal::PoolArray"** %nextArray, align 4
  %freeObjects = getelementptr inbounds %"class.btConvexHullInternal::Pool", %"class.btConvexHullInternal::Pool"* %this1, i32 0, i32 2
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %freeObjects, align 4
  %arraySize = getelementptr inbounds %"class.btConvexHullInternal::Pool", %"class.btConvexHullInternal::Pool"* %this1, i32 0, i32 3
  store i32 256, i32* %arraySize, align 4
  ret %"class.btConvexHullInternal::Pool"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Pool.0"* @_ZN20btConvexHullInternal4PoolINS_4EdgeEEC2Ev(%"class.btConvexHullInternal::Pool.0"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Pool.0"*, align 4
  store %"class.btConvexHullInternal::Pool.0"* %this, %"class.btConvexHullInternal::Pool.0"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Pool.0"*, %"class.btConvexHullInternal::Pool.0"** %this.addr, align 4
  %arrays = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 0
  store %"class.btConvexHullInternal::PoolArray.1"* null, %"class.btConvexHullInternal::PoolArray.1"** %arrays, align 4
  %nextArray = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 1
  store %"class.btConvexHullInternal::PoolArray.1"* null, %"class.btConvexHullInternal::PoolArray.1"** %nextArray, align 4
  %freeObjects = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %freeObjects, align 4
  %arraySize = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 3
  store i32 256, i32* %arraySize, align 4
  ret %"class.btConvexHullInternal::Pool.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Pool.2"* @_ZN20btConvexHullInternal4PoolINS_4FaceEEC2Ev(%"class.btConvexHullInternal::Pool.2"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Pool.2"*, align 4
  store %"class.btConvexHullInternal::Pool.2"* %this, %"class.btConvexHullInternal::Pool.2"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Pool.2"*, %"class.btConvexHullInternal::Pool.2"** %this.addr, align 4
  %arrays = getelementptr inbounds %"class.btConvexHullInternal::Pool.2", %"class.btConvexHullInternal::Pool.2"* %this1, i32 0, i32 0
  store %"class.btConvexHullInternal::PoolArray.3"* null, %"class.btConvexHullInternal::PoolArray.3"** %arrays, align 4
  %nextArray = getelementptr inbounds %"class.btConvexHullInternal::Pool.2", %"class.btConvexHullInternal::Pool.2"* %this1, i32 0, i32 1
  store %"class.btConvexHullInternal::PoolArray.3"* null, %"class.btConvexHullInternal::PoolArray.3"** %nextArray, align 4
  %freeObjects = getelementptr inbounds %"class.btConvexHullInternal::Pool.2", %"class.btConvexHullInternal::Pool.2"* %this1, i32 0, i32 2
  store %"class.btConvexHullInternal::Face"* null, %"class.btConvexHullInternal::Face"** %freeObjects, align 4
  %arraySize = getelementptr inbounds %"class.btConvexHullInternal::Pool.2", %"class.btConvexHullInternal::Pool.2"* %this1, i32 0, i32 3
  store i32 256, i32* %arraySize, align 4
  ret %"class.btConvexHullInternal::Pool.2"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Pool.2"* @_ZN20btConvexHullInternal4PoolINS_4FaceEED2Ev(%"class.btConvexHullInternal::Pool.2"* returned %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca %"class.btConvexHullInternal::Pool.2"*, align 4
  %this.addr = alloca %"class.btConvexHullInternal::Pool.2"*, align 4
  %p = alloca %"class.btConvexHullInternal::PoolArray.3"*, align 4
  store %"class.btConvexHullInternal::Pool.2"* %this, %"class.btConvexHullInternal::Pool.2"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Pool.2"*, %"class.btConvexHullInternal::Pool.2"** %this.addr, align 4
  store %"class.btConvexHullInternal::Pool.2"* %this1, %"class.btConvexHullInternal::Pool.2"** %retval, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %arrays = getelementptr inbounds %"class.btConvexHullInternal::Pool.2", %"class.btConvexHullInternal::Pool.2"* %this1, i32 0, i32 0
  %0 = load %"class.btConvexHullInternal::PoolArray.3"*, %"class.btConvexHullInternal::PoolArray.3"** %arrays, align 4
  %tobool = icmp ne %"class.btConvexHullInternal::PoolArray.3"* %0, null
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %arrays2 = getelementptr inbounds %"class.btConvexHullInternal::Pool.2", %"class.btConvexHullInternal::Pool.2"* %this1, i32 0, i32 0
  %1 = load %"class.btConvexHullInternal::PoolArray.3"*, %"class.btConvexHullInternal::PoolArray.3"** %arrays2, align 4
  store %"class.btConvexHullInternal::PoolArray.3"* %1, %"class.btConvexHullInternal::PoolArray.3"** %p, align 4
  %2 = load %"class.btConvexHullInternal::PoolArray.3"*, %"class.btConvexHullInternal::PoolArray.3"** %p, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.3", %"class.btConvexHullInternal::PoolArray.3"* %2, i32 0, i32 2
  %3 = load %"class.btConvexHullInternal::PoolArray.3"*, %"class.btConvexHullInternal::PoolArray.3"** %next, align 4
  %arrays3 = getelementptr inbounds %"class.btConvexHullInternal::Pool.2", %"class.btConvexHullInternal::Pool.2"* %this1, i32 0, i32 0
  store %"class.btConvexHullInternal::PoolArray.3"* %3, %"class.btConvexHullInternal::PoolArray.3"** %arrays3, align 4
  %4 = load %"class.btConvexHullInternal::PoolArray.3"*, %"class.btConvexHullInternal::PoolArray.3"** %p, align 4
  %call = call %"class.btConvexHullInternal::PoolArray.3"* @_ZN20btConvexHullInternal9PoolArrayINS_4FaceEED2Ev(%"class.btConvexHullInternal::PoolArray.3"* %4) #7
  %5 = load %"class.btConvexHullInternal::PoolArray.3"*, %"class.btConvexHullInternal::PoolArray.3"** %p, align 4
  %6 = bitcast %"class.btConvexHullInternal::PoolArray.3"* %5 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %6)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %7 = load %"class.btConvexHullInternal::Pool.2"*, %"class.btConvexHullInternal::Pool.2"** %retval, align 4
  ret %"class.btConvexHullInternal::Pool.2"* %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Pool.0"* @_ZN20btConvexHullInternal4PoolINS_4EdgeEED2Ev(%"class.btConvexHullInternal::Pool.0"* returned %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca %"class.btConvexHullInternal::Pool.0"*, align 4
  %this.addr = alloca %"class.btConvexHullInternal::Pool.0"*, align 4
  %p = alloca %"class.btConvexHullInternal::PoolArray.1"*, align 4
  store %"class.btConvexHullInternal::Pool.0"* %this, %"class.btConvexHullInternal::Pool.0"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Pool.0"*, %"class.btConvexHullInternal::Pool.0"** %this.addr, align 4
  store %"class.btConvexHullInternal::Pool.0"* %this1, %"class.btConvexHullInternal::Pool.0"** %retval, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %arrays = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 0
  %0 = load %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::PoolArray.1"** %arrays, align 4
  %tobool = icmp ne %"class.btConvexHullInternal::PoolArray.1"* %0, null
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %arrays2 = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 0
  %1 = load %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::PoolArray.1"** %arrays2, align 4
  store %"class.btConvexHullInternal::PoolArray.1"* %1, %"class.btConvexHullInternal::PoolArray.1"** %p, align 4
  %2 = load %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::PoolArray.1"** %p, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.1", %"class.btConvexHullInternal::PoolArray.1"* %2, i32 0, i32 2
  %3 = load %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::PoolArray.1"** %next, align 4
  %arrays3 = getelementptr inbounds %"class.btConvexHullInternal::Pool.0", %"class.btConvexHullInternal::Pool.0"* %this1, i32 0, i32 0
  store %"class.btConvexHullInternal::PoolArray.1"* %3, %"class.btConvexHullInternal::PoolArray.1"** %arrays3, align 4
  %4 = load %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::PoolArray.1"** %p, align 4
  %call = call %"class.btConvexHullInternal::PoolArray.1"* @_ZN20btConvexHullInternal9PoolArrayINS_4EdgeEED2Ev(%"class.btConvexHullInternal::PoolArray.1"* %4) #7
  %5 = load %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::PoolArray.1"** %p, align 4
  %6 = bitcast %"class.btConvexHullInternal::PoolArray.1"* %5 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %6)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %7 = load %"class.btConvexHullInternal::Pool.0"*, %"class.btConvexHullInternal::Pool.0"** %retval, align 4
  ret %"class.btConvexHullInternal::Pool.0"* %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Pool"* @_ZN20btConvexHullInternal4PoolINS_6VertexEED2Ev(%"class.btConvexHullInternal::Pool"* returned %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca %"class.btConvexHullInternal::Pool"*, align 4
  %this.addr = alloca %"class.btConvexHullInternal::Pool"*, align 4
  %p = alloca %"class.btConvexHullInternal::PoolArray"*, align 4
  store %"class.btConvexHullInternal::Pool"* %this, %"class.btConvexHullInternal::Pool"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Pool"*, %"class.btConvexHullInternal::Pool"** %this.addr, align 4
  store %"class.btConvexHullInternal::Pool"* %this1, %"class.btConvexHullInternal::Pool"** %retval, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %arrays = getelementptr inbounds %"class.btConvexHullInternal::Pool", %"class.btConvexHullInternal::Pool"* %this1, i32 0, i32 0
  %0 = load %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::PoolArray"** %arrays, align 4
  %tobool = icmp ne %"class.btConvexHullInternal::PoolArray"* %0, null
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %arrays2 = getelementptr inbounds %"class.btConvexHullInternal::Pool", %"class.btConvexHullInternal::Pool"* %this1, i32 0, i32 0
  %1 = load %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::PoolArray"** %arrays2, align 4
  store %"class.btConvexHullInternal::PoolArray"* %1, %"class.btConvexHullInternal::PoolArray"** %p, align 4
  %2 = load %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::PoolArray"** %p, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::PoolArray", %"class.btConvexHullInternal::PoolArray"* %2, i32 0, i32 2
  %3 = load %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::PoolArray"** %next, align 4
  %arrays3 = getelementptr inbounds %"class.btConvexHullInternal::Pool", %"class.btConvexHullInternal::Pool"* %this1, i32 0, i32 0
  store %"class.btConvexHullInternal::PoolArray"* %3, %"class.btConvexHullInternal::PoolArray"** %arrays3, align 4
  %4 = load %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::PoolArray"** %p, align 4
  %call = call %"class.btConvexHullInternal::PoolArray"* @_ZN20btConvexHullInternal9PoolArrayINS_6VertexEED2Ev(%"class.btConvexHullInternal::PoolArray"* %4) #7
  %5 = load %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::PoolArray"** %p, align 4
  %6 = bitcast %"class.btConvexHullInternal::PoolArray"* %5 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %6)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %7 = load %"class.btConvexHullInternal::Pool"*, %"class.btConvexHullInternal::Pool"** %retval, align 4
  ret %"class.btConvexHullInternal::Pool"* %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::PoolArray.3"* @_ZN20btConvexHullInternal9PoolArrayINS_4FaceEED2Ev(%"class.btConvexHullInternal::PoolArray.3"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::PoolArray.3"*, align 4
  store %"class.btConvexHullInternal::PoolArray.3"* %this, %"class.btConvexHullInternal::PoolArray.3"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::PoolArray.3"*, %"class.btConvexHullInternal::PoolArray.3"** %this.addr, align 4
  %array = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.3", %"class.btConvexHullInternal::PoolArray.3"* %this1, i32 0, i32 0
  %0 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %array, align 4
  %1 = bitcast %"class.btConvexHullInternal::Face"* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret %"class.btConvexHullInternal::PoolArray.3"* %this1
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::PoolArray.1"* @_ZN20btConvexHullInternal9PoolArrayINS_4EdgeEED2Ev(%"class.btConvexHullInternal::PoolArray.1"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::PoolArray.1"*, align 4
  store %"class.btConvexHullInternal::PoolArray.1"* %this, %"class.btConvexHullInternal::PoolArray.1"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::PoolArray.1"** %this.addr, align 4
  %array = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.1", %"class.btConvexHullInternal::PoolArray.1"* %this1, i32 0, i32 0
  %0 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %array, align 4
  %1 = bitcast %"class.btConvexHullInternal::Edge"* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret %"class.btConvexHullInternal::PoolArray.1"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::PoolArray"* @_ZN20btConvexHullInternal9PoolArrayINS_6VertexEED2Ev(%"class.btConvexHullInternal::PoolArray"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::PoolArray"*, align 4
  store %"class.btConvexHullInternal::PoolArray"* %this, %"class.btConvexHullInternal::PoolArray"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::PoolArray"** %this.addr, align 4
  %array = getelementptr inbounds %"class.btConvexHullInternal::PoolArray", %"class.btConvexHullInternal::PoolArray"* %this1, i32 0, i32 0
  %0 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %array, align 4
  %1 = bitcast %"class.btConvexHullInternal::Vertex"* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret %"class.btConvexHullInternal::PoolArray"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZN20btConvexHullInternal4DMulIyjE3mulEjj(i32 %a, i32 %b) #1 comdat {
entry:
  %a.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  store i32 %a, i32* %a.addr, align 4
  store i32 %b, i32* %b.addr, align 4
  %0 = load i32, i32* %a.addr, align 4
  %conv = zext i32 %0 to i64
  %1 = load i32, i32* %b.addr, align 4
  %conv1 = zext i32 %1 to i64
  %mul = mul i64 %conv, %conv1
  ret i64 %mul
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btConvexHullInternal4DMulIyjE3lowEy(i64 %value) #1 comdat {
entry:
  %value.addr = alloca i64, align 8
  store i64 %value, i64* %value.addr, align 8
  %0 = load i64, i64* %value.addr, align 8
  %conv = trunc i64 %0 to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btConvexHullInternal4DMulIyjE4highEy(i64 %value) #1 comdat {
entry:
  %value.addr = alloca i64, align 8
  store i64 %value, i64* %value.addr, align 8
  %0 = load i64, i64* %value.addr, align 8
  %shr = lshr i64 %0, 32
  %conv = trunc i64 %shr to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btConvexHullInternal4DMulIyjE7shlHalfERy(i64* nonnull align 8 dereferenceable(8) %value) #1 comdat {
entry:
  %value.addr = alloca i64*, align 4
  store i64* %value, i64** %value.addr, align 4
  %0 = load i64*, i64** %value.addr, align 4
  %1 = load i64, i64* %0, align 8
  %shl = shl i64 %1, 32
  store i64 %shl, i64* %0, align 8
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btConvexHullInternal4DMulINS_6Int128EyE3mulEyy(%"class.btConvexHullInternal::Int128"* noalias sret align 8 %agg.result, i64 %a, i64 %b) #2 comdat {
entry:
  %a.addr = alloca i64, align 8
  %b.addr = alloca i64, align 8
  store i64 %a, i64* %a.addr, align 8
  store i64 %b, i64* %b.addr, align 8
  %0 = load i64, i64* %a.addr, align 8
  %1 = load i64, i64* %b.addr, align 8
  call void @_ZN20btConvexHullInternal6Int1283mulEyy(%"class.btConvexHullInternal::Int128"* sret align 8 %agg.result, i64 %0, i64 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZN20btConvexHullInternal4DMulINS_6Int128EyE3lowES1_(%"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %value) #1 comdat {
entry:
  %low = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %value, i32 0, i32 0
  %0 = load i64, i64* %low, align 8
  ret i64 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZN20btConvexHullInternal4DMulINS_6Int128EyE4highES1_(%"class.btConvexHullInternal::Int128"* byval(%"class.btConvexHullInternal::Int128") align 8 %value) #1 comdat {
entry:
  %high = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %value, i32 0, i32 1
  %0 = load i64, i64* %high, align 8
  ret i64 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btConvexHullInternal4DMulINS_6Int128EyE7shlHalfERS1_(%"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %value) #1 comdat {
entry:
  %value.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  store %"class.btConvexHullInternal::Int128"* %value, %"class.btConvexHullInternal::Int128"** %value.addr, align 4
  %0 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %value.addr, align 4
  %low = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %0, i32 0, i32 0
  %1 = load i64, i64* %low, align 8
  %2 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %value.addr, align 4
  %high = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %2, i32 0, i32 1
  store i64 %1, i64* %high, align 8
  %3 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %value.addr, align 4
  %low1 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %3, i32 0, i32 0
  store i64 0, i64* %low1, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK20btConvexHullInternal6Int128ltERKS0_(%"class.btConvexHullInternal::Int128"* %this, %"class.btConvexHullInternal::Int128"* nonnull align 8 dereferenceable(16) %b) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  %b.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  store %"class.btConvexHullInternal::Int128"* %this, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  store %"class.btConvexHullInternal::Int128"* %b, %"class.btConvexHullInternal::Int128"** %b.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  %high = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 1
  %0 = load i64, i64* %high, align 8
  %1 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %b.addr, align 4
  %high2 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %1, i32 0, i32 1
  %2 = load i64, i64* %high2, align 8
  %cmp = icmp ult i64 %0, %2
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %high3 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 1
  %3 = load i64, i64* %high3, align 8
  %4 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %b.addr, align 4
  %high4 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %4, i32 0, i32 1
  %5 = load i64, i64* %high4, align 8
  %cmp5 = icmp eq i64 %3, %5
  br i1 %cmp5, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %lor.rhs
  %low = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 0
  %6 = load i64, i64* %low, align 8
  %7 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %b.addr, align 4
  %low6 = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %7, i32 0, i32 0
  %8 = load i64, i64* %low6, align 8
  %cmp7 = icmp ult i64 %6, %8
  br label %land.end

land.end:                                         ; preds = %land.rhs, %lor.rhs
  %9 = phi i1 [ false, %lor.rhs ], [ %cmp7, %land.rhs ]
  br label %lor.end

lor.end:                                          ; preds = %land.end, %entry
  %10 = phi i1 [ true, %entry ], [ %9, %land.end ]
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 8 dereferenceable(16) %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128ppEv(%"class.btConvexHullInternal::Int128"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Int128"*, align 4
  store %"class.btConvexHullInternal::Int128"* %this, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Int128"*, %"class.btConvexHullInternal::Int128"** %this.addr, align 4
  %low = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 0
  %0 = load i64, i64* %low, align 8
  %inc = add i64 %0, 1
  store i64 %inc, i64* %low, align 8
  %cmp = icmp eq i64 %inc, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %high = getelementptr inbounds %"class.btConvexHullInternal::Int128", %"class.btConvexHullInternal::Int128"* %this1, i32 0, i32 1
  %1 = load i64, i64* %high, align 8
  %inc2 = add i64 %1, 1
  store i64 %inc2, i64* %high, align 8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret %"class.btConvexHullInternal::Int128"* %this1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #6

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btConvexHullInternal::PoolArray.1"* @_ZN20btConvexHullInternal9PoolArrayINS_4EdgeEEC2Ei(%"class.btConvexHullInternal::PoolArray.1"* returned %this, i32 %size) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::PoolArray.1"*, align 4
  %size.addr = alloca i32, align 4
  store %"class.btConvexHullInternal::PoolArray.1"* %this, %"class.btConvexHullInternal::PoolArray.1"** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::PoolArray.1"** %this.addr, align 4
  %size2 = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.1", %"class.btConvexHullInternal::PoolArray.1"* %this1, i32 0, i32 1
  %0 = load i32, i32* %size.addr, align 4
  store i32 %0, i32* %size2, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.1", %"class.btConvexHullInternal::PoolArray.1"* %this1, i32 0, i32 2
  store %"class.btConvexHullInternal::PoolArray.1"* null, %"class.btConvexHullInternal::PoolArray.1"** %next, align 4
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul i32 24, %1
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %2 = bitcast i8* %call to %"class.btConvexHullInternal::Edge"*
  %array = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.1", %"class.btConvexHullInternal::PoolArray.1"* %this1, i32 0, i32 0
  store %"class.btConvexHullInternal::Edge"* %2, %"class.btConvexHullInternal::Edge"** %array, align 4
  ret %"class.btConvexHullInternal::PoolArray.1"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Edge"* @_ZN20btConvexHullInternal9PoolArrayINS_4EdgeEE4initEv(%"class.btConvexHullInternal::PoolArray.1"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::PoolArray.1"*, align 4
  %o = alloca %"class.btConvexHullInternal::Edge"*, align 4
  %i = alloca i32, align 4
  store %"class.btConvexHullInternal::PoolArray.1"* %this, %"class.btConvexHullInternal::PoolArray.1"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::PoolArray.1"*, %"class.btConvexHullInternal::PoolArray.1"** %this.addr, align 4
  %array = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.1", %"class.btConvexHullInternal::PoolArray.1"* %this1, i32 0, i32 0
  %0 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %array, align 4
  store %"class.btConvexHullInternal::Edge"* %0, %"class.btConvexHullInternal::Edge"** %o, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %size = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.1", %"class.btConvexHullInternal::PoolArray.1"* %this1, i32 0, i32 1
  %2 = load i32, i32* %size, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32, i32* %i, align 4
  %add = add nsw i32 %3, 1
  %size2 = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.1", %"class.btConvexHullInternal::PoolArray.1"* %this1, i32 0, i32 1
  %4 = load i32, i32* %size2, align 4
  %cmp3 = icmp slt i32 %add, %4
  br i1 %cmp3, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %5 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %o, align 4
  %add.ptr = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %5, i32 1
  br label %cond.end

cond.false:                                       ; preds = %for.body
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"class.btConvexHullInternal::Edge"* [ %add.ptr, %cond.true ], [ null, %cond.false ]
  %6 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %o, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %6, i32 0, i32 0
  store %"class.btConvexHullInternal::Edge"* %cond, %"class.btConvexHullInternal::Edge"** %next, align 4
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  %8 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %o, align 4
  %incdec.ptr = getelementptr inbounds %"class.btConvexHullInternal::Edge", %"class.btConvexHullInternal::Edge"* %8, i32 1
  store %"class.btConvexHullInternal::Edge"* %incdec.ptr, %"class.btConvexHullInternal::Edge"** %o, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %array4 = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.1", %"class.btConvexHullInternal::PoolArray.1"* %this1, i32 0, i32 0
  %9 = load %"class.btConvexHullInternal::Edge"*, %"class.btConvexHullInternal::Edge"** %array4, align 4
  ret %"class.btConvexHullInternal::Edge"* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIN20btConvexHullInternal7Point32ELj16EEC2Ev(%class.btAlignedAllocator.5* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  ret %class.btAlignedAllocator.5* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE4initEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %"class.btConvexHullInternal::Point32"* null, %"class.btConvexHullInternal::Point32"** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE4sizeEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %"class.btConvexHullInternal::Point32"*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %"class.btConvexHullInternal::Point32"*
  store %"class.btConvexHullInternal::Point32"* %2, %"class.btConvexHullInternal::Point32"** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %3 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE4copyEiiPS1_(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, %"class.btConvexHullInternal::Point32"* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %"class.btConvexHullInternal::Point32"* %4, %"class.btConvexHullInternal::Point32"** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE8capacityEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %"class.btConvexHullInternal::Point32"* @_ZN18btAlignedAllocatorIN20btConvexHullInternal7Point32ELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.5* %m_allocator, i32 %1, %"class.btConvexHullInternal::Point32"** null)
  %2 = bitcast %"class.btConvexHullInternal::Point32"* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE4copyEiiPS1_(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, %"class.btConvexHullInternal::Point32"* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %"class.btConvexHullInternal::Point32"* %dest, %"class.btConvexHullInternal::Point32"** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %3, i32 %4
  %5 = bitcast %"class.btConvexHullInternal::Point32"* %arrayidx to i8*
  %6 = bitcast i8* %5 to %"class.btConvexHullInternal::Point32"*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %7 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %7, i32 %8
  %9 = bitcast %"class.btConvexHullInternal::Point32"* %6 to i8*
  %10 = bitcast %"class.btConvexHullInternal::Point32"* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %3 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE10deallocateEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %m_data, align 4
  %tobool = icmp ne %"class.btConvexHullInternal::Point32"* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIN20btConvexHullInternal7Point32ELj16EE10deallocateEPS1_(%class.btAlignedAllocator.5* %m_allocator, %"class.btConvexHullInternal::Point32"* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %"class.btConvexHullInternal::Point32"* null, %"class.btConvexHullInternal::Point32"** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Point32"* @_ZN18btAlignedAllocatorIN20btConvexHullInternal7Point32ELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.5* %this, i32 %n, %"class.btConvexHullInternal::Point32"** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %"class.btConvexHullInternal::Point32"**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %"class.btConvexHullInternal::Point32"** %hint, %"class.btConvexHullInternal::Point32"*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %"class.btConvexHullInternal::Point32"*
  ret %"class.btConvexHullInternal::Point32"* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIN20btConvexHullInternal7Point32ELj16EE10deallocateEPS1_(%class.btAlignedAllocator.5* %this, %"class.btConvexHullInternal::Point32"* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store %"class.btConvexHullInternal::Point32"* %ptr, %"class.btConvexHullInternal::Point32"** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %ptr.addr, align 4
  %1 = bitcast %"class.btConvexHullInternal::Point32"* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE17quickSortInternalI8pointCmpEEvRKT_ii(%class.btAlignedObjectArray.4* %this, %class.pointCmp* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %CompareFunc.addr = alloca %class.pointCmp*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %"class.btConvexHullInternal::Point32", align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store %class.pointCmp* %CompareFunc, %class.pointCmp** %CompareFunc.addr, align 4
  store i32 %lo, i32* %lo.addr, align 4
  store i32 %hi, i32* %hi.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %lo.addr, align 4
  store i32 %0, i32* %i, align 4
  %1 = load i32, i32* %hi.addr, align 4
  store i32 %1, i32* %j, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %m_data, align 4
  %3 = load i32, i32* %lo.addr, align 4
  %4 = load i32, i32* %hi.addr, align 4
  %add = add nsw i32 %3, %4
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %2, i32 %div
  %5 = bitcast %"class.btConvexHullInternal::Point32"* %x to i8*
  %6 = bitcast %"class.btConvexHullInternal::Point32"* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %7 = load %class.pointCmp*, %class.pointCmp** %CompareFunc.addr, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %8 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %m_data2, align 4
  %9 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %8, i32 %9
  %call = call zeroext i1 @_ZNK8pointCmpclERKN20btConvexHullInternal7Point32ES3_(%class.pointCmp* %7, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %arrayidx3, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %x)
  br i1 %call, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond4

while.cond4:                                      ; preds = %while.body8, %while.end
  %11 = load %class.pointCmp*, %class.pointCmp** %CompareFunc.addr, align 4
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %12 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %m_data5, align 4
  %13 = load i32, i32* %j, align 4
  %arrayidx6 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %12, i32 %13
  %call7 = call zeroext i1 @_ZNK8pointCmpclERKN20btConvexHullInternal7Point32ES3_(%class.pointCmp* %11, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %x, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %arrayidx6)
  br i1 %call7, label %while.body8, label %while.end9

while.body8:                                      ; preds = %while.cond4
  %14 = load i32, i32* %j, align 4
  %dec = add nsw i32 %14, -1
  store i32 %dec, i32* %j, align 4
  br label %while.cond4

while.end9:                                       ; preds = %while.cond4
  %15 = load i32, i32* %i, align 4
  %16 = load i32, i32* %j, align 4
  %cmp = icmp sle i32 %15, %16
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end9
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE4swapEii(%class.btAlignedObjectArray.4* %this1, i32 %17, i32 %18)
  %19 = load i32, i32* %i, align 4
  %inc10 = add nsw i32 %19, 1
  store i32 %inc10, i32* %i, align 4
  %20 = load i32, i32* %j, align 4
  %dec11 = add nsw i32 %20, -1
  store i32 %dec11, i32* %j, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end9
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %21 = load i32, i32* %i, align 4
  %22 = load i32, i32* %j, align 4
  %cmp12 = icmp sle i32 %21, %22
  br i1 %cmp12, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %23 = load i32, i32* %lo.addr, align 4
  %24 = load i32, i32* %j, align 4
  %cmp13 = icmp slt i32 %23, %24
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %do.end
  %25 = load %class.pointCmp*, %class.pointCmp** %CompareFunc.addr, align 4
  %26 = load i32, i32* %lo.addr, align 4
  %27 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE17quickSortInternalI8pointCmpEEvRKT_ii(%class.btAlignedObjectArray.4* %this1, %class.pointCmp* nonnull align 1 dereferenceable(1) %25, i32 %26, i32 %27)
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %do.end
  %28 = load i32, i32* %i, align 4
  %29 = load i32, i32* %hi.addr, align 4
  %cmp16 = icmp slt i32 %28, %29
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end15
  %30 = load %class.pointCmp*, %class.pointCmp** %CompareFunc.addr, align 4
  %31 = load i32, i32* %i, align 4
  %32 = load i32, i32* %hi.addr, align 4
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE17quickSortInternalI8pointCmpEEvRKT_ii(%class.btAlignedObjectArray.4* %this1, %class.pointCmp* nonnull align 1 dereferenceable(1) %30, i32 %31, i32 %32)
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %if.end15
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK8pointCmpclERKN20btConvexHullInternal7Point32ES3_(%class.pointCmp* %this, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %p, %"class.btConvexHullInternal::Point32"* nonnull align 4 dereferenceable(16) %q) #1 comdat {
entry:
  %this.addr = alloca %class.pointCmp*, align 4
  %p.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  %q.addr = alloca %"class.btConvexHullInternal::Point32"*, align 4
  store %class.pointCmp* %this, %class.pointCmp** %this.addr, align 4
  store %"class.btConvexHullInternal::Point32"* %p, %"class.btConvexHullInternal::Point32"** %p.addr, align 4
  store %"class.btConvexHullInternal::Point32"* %q, %"class.btConvexHullInternal::Point32"** %q.addr, align 4
  %this1 = load %class.pointCmp*, %class.pointCmp** %this.addr, align 4
  %0 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %p.addr, align 4
  %y = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %0, i32 0, i32 1
  %1 = load i32, i32* %y, align 4
  %2 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %q.addr, align 4
  %y2 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %2, i32 0, i32 1
  %3 = load i32, i32* %y2, align 4
  %cmp = icmp slt i32 %1, %3
  br i1 %cmp, label %lor.end16, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %4 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %p.addr, align 4
  %y3 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %4, i32 0, i32 1
  %5 = load i32, i32* %y3, align 4
  %6 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %q.addr, align 4
  %y4 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %6, i32 0, i32 1
  %7 = load i32, i32* %y4, align 4
  %cmp5 = icmp eq i32 %5, %7
  br i1 %cmp5, label %land.rhs, label %land.end15

land.rhs:                                         ; preds = %lor.rhs
  %8 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %p.addr, align 4
  %x = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %8, i32 0, i32 0
  %9 = load i32, i32* %x, align 4
  %10 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %q.addr, align 4
  %x6 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %10, i32 0, i32 0
  %11 = load i32, i32* %x6, align 4
  %cmp7 = icmp slt i32 %9, %11
  br i1 %cmp7, label %lor.end, label %lor.rhs8

lor.rhs8:                                         ; preds = %land.rhs
  %12 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %p.addr, align 4
  %x9 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %12, i32 0, i32 0
  %13 = load i32, i32* %x9, align 4
  %14 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %q.addr, align 4
  %x10 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %14, i32 0, i32 0
  %15 = load i32, i32* %x10, align 4
  %cmp11 = icmp eq i32 %13, %15
  br i1 %cmp11, label %land.rhs12, label %land.end

land.rhs12:                                       ; preds = %lor.rhs8
  %16 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %p.addr, align 4
  %z = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %16, i32 0, i32 2
  %17 = load i32, i32* %z, align 4
  %18 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %q.addr, align 4
  %z13 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %18, i32 0, i32 2
  %19 = load i32, i32* %z13, align 4
  %cmp14 = icmp slt i32 %17, %19
  br label %land.end

land.end:                                         ; preds = %land.rhs12, %lor.rhs8
  %20 = phi i1 [ false, %lor.rhs8 ], [ %cmp14, %land.rhs12 ]
  br label %lor.end

lor.end:                                          ; preds = %land.end, %land.rhs
  %21 = phi i1 [ true, %land.rhs ], [ %20, %land.end ]
  br label %land.end15

land.end15:                                       ; preds = %lor.end, %lor.rhs
  %22 = phi i1 [ false, %lor.rhs ], [ %21, %lor.end ]
  br label %lor.end16

lor.end16:                                        ; preds = %land.end15, %entry
  %23 = phi i1 [ true, %entry ], [ %22, %land.end15 ]
  ret i1 %23
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullInternal7Point32EE4swapEii(%class.btAlignedObjectArray.4* %this, i32 %index0, i32 %index1) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %"class.btConvexHullInternal::Point32", align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %0, i32 %1
  %2 = bitcast %"class.btConvexHullInternal::Point32"* %temp to i8*
  %3 = bitcast %"class.btConvexHullInternal::Point32"* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %4 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %m_data2, align 4
  %5 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %4, i32 %5
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %6 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %m_data4, align 4
  %7 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %6, i32 %7
  %8 = bitcast %"class.btConvexHullInternal::Point32"* %arrayidx5 to i8*
  %9 = bitcast %"class.btConvexHullInternal::Point32"* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %10 = load %"class.btConvexHullInternal::Point32"*, %"class.btConvexHullInternal::Point32"** %m_data6, align 4
  %11 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %"class.btConvexHullInternal::Point32", %"class.btConvexHullInternal::Point32"* %10, i32 %11
  %12 = bitcast %"class.btConvexHullInternal::Point32"* %arrayidx7 to i8*
  %13 = bitcast %"class.btConvexHullInternal::Point32"* %temp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %"class.btConvexHullInternal::Vertex"**, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %"class.btConvexHullInternal::Vertex"**
  store %"class.btConvexHullInternal::Vertex"** %2, %"class.btConvexHullInternal::Vertex"*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4copyEiiPS2_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %"class.btConvexHullInternal::Vertex"** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %"class.btConvexHullInternal::Vertex"** %4, %"class.btConvexHullInternal::Vertex"*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %"class.btConvexHullInternal::Vertex"** @_ZN18btAlignedAllocatorIPN20btConvexHullInternal6VertexELj16EE8allocateEiPPKS2_(%class.btAlignedAllocator* %m_allocator, i32 %1, %"class.btConvexHullInternal::Vertex"*** null)
  %2 = bitcast %"class.btConvexHullInternal::Vertex"** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4copyEiiPS2_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %"class.btConvexHullInternal::Vertex"** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %"class.btConvexHullInternal::Vertex"**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %"class.btConvexHullInternal::Vertex"** %dest, %"class.btConvexHullInternal::Vertex"*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %3, i32 %4
  %5 = bitcast %"class.btConvexHullInternal::Vertex"** %arrayidx to i8*
  %6 = bitcast i8* %5 to %"class.btConvexHullInternal::Vertex"**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %7, i32 %8
  %9 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %arrayidx2, align 4
  store %"class.btConvexHullInternal::Vertex"* %9, %"class.btConvexHullInternal::Vertex"** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %m_data, align 4
  %tobool = icmp ne %"class.btConvexHullInternal::Vertex"** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIPN20btConvexHullInternal6VertexELj16EE10deallocateEPS2_(%class.btAlignedAllocator* %m_allocator, %"class.btConvexHullInternal::Vertex"** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %"class.btConvexHullInternal::Vertex"** null, %"class.btConvexHullInternal::Vertex"*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Vertex"** @_ZN18btAlignedAllocatorIPN20btConvexHullInternal6VertexELj16EE8allocateEiPPKS2_(%class.btAlignedAllocator* %this, i32 %n, %"class.btConvexHullInternal::Vertex"*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %"class.btConvexHullInternal::Vertex"***, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %"class.btConvexHullInternal::Vertex"*** %hint, %"class.btConvexHullInternal::Vertex"**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %"class.btConvexHullInternal::Vertex"**
  ret %"class.btConvexHullInternal::Vertex"** %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIPN20btConvexHullInternal6VertexELj16EE10deallocateEPS2_(%class.btAlignedAllocator* %this, %"class.btConvexHullInternal::Vertex"** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %"class.btConvexHullInternal::Vertex"**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %"class.btConvexHullInternal::Vertex"** %ptr, %"class.btConvexHullInternal::Vertex"*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %"class.btConvexHullInternal::Vertex"**, %"class.btConvexHullInternal::Vertex"*** %ptr.addr, align 4
  %1 = bitcast %"class.btConvexHullInternal::Vertex"** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btConvexHullInternal::PoolArray"* @_ZN20btConvexHullInternal9PoolArrayINS_6VertexEEC2Ei(%"class.btConvexHullInternal::PoolArray"* returned %this, i32 %size) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::PoolArray"*, align 4
  %size.addr = alloca i32, align 4
  store %"class.btConvexHullInternal::PoolArray"* %this, %"class.btConvexHullInternal::PoolArray"** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::PoolArray"** %this.addr, align 4
  %size2 = getelementptr inbounds %"class.btConvexHullInternal::PoolArray", %"class.btConvexHullInternal::PoolArray"* %this1, i32 0, i32 1
  %0 = load i32, i32* %size.addr, align 4
  store i32 %0, i32* %size2, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::PoolArray", %"class.btConvexHullInternal::PoolArray"* %this1, i32 0, i32 2
  store %"class.btConvexHullInternal::PoolArray"* null, %"class.btConvexHullInternal::PoolArray"** %next, align 4
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul i32 112, %1
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %2 = bitcast i8* %call to %"class.btConvexHullInternal::Vertex"*
  %array = getelementptr inbounds %"class.btConvexHullInternal::PoolArray", %"class.btConvexHullInternal::PoolArray"* %this1, i32 0, i32 0
  store %"class.btConvexHullInternal::Vertex"* %2, %"class.btConvexHullInternal::Vertex"** %array, align 4
  ret %"class.btConvexHullInternal::PoolArray"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Vertex"* @_ZN20btConvexHullInternal9PoolArrayINS_6VertexEE4initEv(%"class.btConvexHullInternal::PoolArray"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::PoolArray"*, align 4
  %o = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  %i = alloca i32, align 4
  store %"class.btConvexHullInternal::PoolArray"* %this, %"class.btConvexHullInternal::PoolArray"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::PoolArray"*, %"class.btConvexHullInternal::PoolArray"** %this.addr, align 4
  %array = getelementptr inbounds %"class.btConvexHullInternal::PoolArray", %"class.btConvexHullInternal::PoolArray"* %this1, i32 0, i32 0
  %0 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %array, align 4
  store %"class.btConvexHullInternal::Vertex"* %0, %"class.btConvexHullInternal::Vertex"** %o, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %size = getelementptr inbounds %"class.btConvexHullInternal::PoolArray", %"class.btConvexHullInternal::PoolArray"* %this1, i32 0, i32 1
  %2 = load i32, i32* %size, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32, i32* %i, align 4
  %add = add nsw i32 %3, 1
  %size2 = getelementptr inbounds %"class.btConvexHullInternal::PoolArray", %"class.btConvexHullInternal::PoolArray"* %this1, i32 0, i32 1
  %4 = load i32, i32* %size2, align 4
  %cmp3 = icmp slt i32 %add, %4
  br i1 %cmp3, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %5 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %o, align 4
  %add.ptr = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %5, i32 1
  br label %cond.end

cond.false:                                       ; preds = %for.body
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"class.btConvexHullInternal::Vertex"* [ %add.ptr, %cond.true ], [ null, %cond.false ]
  %6 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %o, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %6, i32 0, i32 0
  store %"class.btConvexHullInternal::Vertex"* %cond, %"class.btConvexHullInternal::Vertex"** %next, align 8
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  %8 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %o, align 4
  %incdec.ptr = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %8, i32 1
  store %"class.btConvexHullInternal::Vertex"* %incdec.ptr, %"class.btConvexHullInternal::Vertex"** %o, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %array4 = getelementptr inbounds %"class.btConvexHullInternal::PoolArray", %"class.btConvexHullInternal::PoolArray"* %this1, i32 0, i32 0
  %9 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %array4, align 4
  ret %"class.btConvexHullInternal::Vertex"* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Vertex"* @_ZN20btConvexHullInternal6VertexC2Ev(%"class.btConvexHullInternal::Vertex"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Vertex"*, align 4
  store %"class.btConvexHullInternal::Vertex"* %this, %"class.btConvexHullInternal::Vertex"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Vertex"*, %"class.btConvexHullInternal::Vertex"** %this.addr, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 0
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %next, align 8
  %prev = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %prev, align 4
  %edges = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 2
  store %"class.btConvexHullInternal::Edge"* null, %"class.btConvexHullInternal::Edge"** %edges, align 8
  %firstNearbyFace = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 3
  store %"class.btConvexHullInternal::Face"* null, %"class.btConvexHullInternal::Face"** %firstNearbyFace, align 4
  %lastNearbyFace = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 4
  store %"class.btConvexHullInternal::Face"* null, %"class.btConvexHullInternal::Face"** %lastNearbyFace, align 8
  %point128 = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 6
  %call = call %"class.btConvexHullInternal::PointR128"* @_ZN20btConvexHullInternal9PointR128C2Ev(%"class.btConvexHullInternal::PointR128"* %point128)
  %point = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 7
  %call2 = call %"class.btConvexHullInternal::Point32"* @_ZN20btConvexHullInternal7Point32C2Ev(%"class.btConvexHullInternal::Point32"* %point)
  %copy = getelementptr inbounds %"class.btConvexHullInternal::Vertex", %"class.btConvexHullInternal::Vertex"* %this1, i32 0, i32 8
  store i32 -1, i32* %copy, align 8
  ret %"class.btConvexHullInternal::Vertex"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btConvexHullInternal::PointR128"* @_ZN20btConvexHullInternal9PointR128C2Ev(%"class.btConvexHullInternal::PointR128"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::PointR128"*, align 4
  store %"class.btConvexHullInternal::PointR128"* %this, %"class.btConvexHullInternal::PointR128"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::PointR128"*, %"class.btConvexHullInternal::PointR128"** %this.addr, align 4
  %x = getelementptr inbounds %"class.btConvexHullInternal::PointR128", %"class.btConvexHullInternal::PointR128"* %this1, i32 0, i32 0
  %call = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ev(%"class.btConvexHullInternal::Int128"* %x)
  %y = getelementptr inbounds %"class.btConvexHullInternal::PointR128", %"class.btConvexHullInternal::PointR128"* %this1, i32 0, i32 1
  %call2 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ev(%"class.btConvexHullInternal::Int128"* %y)
  %z = getelementptr inbounds %"class.btConvexHullInternal::PointR128", %"class.btConvexHullInternal::PointR128"* %this1, i32 0, i32 2
  %call3 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ev(%"class.btConvexHullInternal::Int128"* %z)
  %denominator = getelementptr inbounds %"class.btConvexHullInternal::PointR128", %"class.btConvexHullInternal::PointR128"* %this1, i32 0, i32 3
  %call4 = call %"class.btConvexHullInternal::Int128"* @_ZN20btConvexHullInternal6Int128C2Ev(%"class.btConvexHullInternal::Int128"* %denominator)
  ret %"class.btConvexHullInternal::PointR128"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIPN20btConvexHullInternal6VertexELj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %"class.btConvexHullInternal::Vertex"** null, %"class.btConvexHullInternal::Vertex"*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal6VertexEE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorIPN20btConvexHullInternal4FaceELj16EEC2Ev(%class.btAlignedAllocator.9* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  ret %class.btAlignedAllocator.9* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE4initEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %"class.btConvexHullInternal::Face"** null, %"class.btConvexHullInternal::Face"*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE5clearEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE4initEv(%class.btAlignedObjectArray.8* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %3 = load %"class.btConvexHullInternal::Face"**, %"class.btConvexHullInternal::Face"*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE10deallocateEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %"class.btConvexHullInternal::Face"**, %"class.btConvexHullInternal::Face"*** %m_data, align 4
  %tobool = icmp ne %"class.btConvexHullInternal::Face"** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load %"class.btConvexHullInternal::Face"**, %"class.btConvexHullInternal::Face"*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIPN20btConvexHullInternal4FaceELj16EE10deallocateEPS2_(%class.btAlignedAllocator.9* %m_allocator, %"class.btConvexHullInternal::Face"** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %"class.btConvexHullInternal::Face"** null, %"class.btConvexHullInternal::Face"*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIPN20btConvexHullInternal4FaceELj16EE10deallocateEPS2_(%class.btAlignedAllocator.9* %this, %"class.btConvexHullInternal::Face"** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca %"class.btConvexHullInternal::Face"**, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store %"class.btConvexHullInternal::Face"** %ptr, %"class.btConvexHullInternal::Face"*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load %"class.btConvexHullInternal::Face"**, %"class.btConvexHullInternal::Face"*** %ptr.addr, align 4
  %1 = bitcast %"class.btConvexHullInternal::Face"** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btConvexHullInternal::PoolArray.3"* @_ZN20btConvexHullInternal9PoolArrayINS_4FaceEEC2Ei(%"class.btConvexHullInternal::PoolArray.3"* returned %this, i32 %size) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::PoolArray.3"*, align 4
  %size.addr = alloca i32, align 4
  store %"class.btConvexHullInternal::PoolArray.3"* %this, %"class.btConvexHullInternal::PoolArray.3"** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %"class.btConvexHullInternal::PoolArray.3"*, %"class.btConvexHullInternal::PoolArray.3"** %this.addr, align 4
  %size2 = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.3", %"class.btConvexHullInternal::PoolArray.3"* %this1, i32 0, i32 1
  %0 = load i32, i32* %size.addr, align 4
  store i32 %0, i32* %size2, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.3", %"class.btConvexHullInternal::PoolArray.3"* %this1, i32 0, i32 2
  store %"class.btConvexHullInternal::PoolArray.3"* null, %"class.btConvexHullInternal::PoolArray.3"** %next, align 4
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul i32 60, %1
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %2 = bitcast i8* %call to %"class.btConvexHullInternal::Face"*
  %array = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.3", %"class.btConvexHullInternal::PoolArray.3"* %this1, i32 0, i32 0
  store %"class.btConvexHullInternal::Face"* %2, %"class.btConvexHullInternal::Face"** %array, align 4
  ret %"class.btConvexHullInternal::PoolArray.3"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Face"* @_ZN20btConvexHullInternal9PoolArrayINS_4FaceEE4initEv(%"class.btConvexHullInternal::PoolArray.3"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::PoolArray.3"*, align 4
  %o = alloca %"class.btConvexHullInternal::Face"*, align 4
  %i = alloca i32, align 4
  store %"class.btConvexHullInternal::PoolArray.3"* %this, %"class.btConvexHullInternal::PoolArray.3"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::PoolArray.3"*, %"class.btConvexHullInternal::PoolArray.3"** %this.addr, align 4
  %array = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.3", %"class.btConvexHullInternal::PoolArray.3"* %this1, i32 0, i32 0
  %0 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %array, align 4
  store %"class.btConvexHullInternal::Face"* %0, %"class.btConvexHullInternal::Face"** %o, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %size = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.3", %"class.btConvexHullInternal::PoolArray.3"* %this1, i32 0, i32 1
  %2 = load i32, i32* %size, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32, i32* %i, align 4
  %add = add nsw i32 %3, 1
  %size2 = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.3", %"class.btConvexHullInternal::PoolArray.3"* %this1, i32 0, i32 1
  %4 = load i32, i32* %size2, align 4
  %cmp3 = icmp slt i32 %add, %4
  br i1 %cmp3, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %5 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %o, align 4
  %add.ptr = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %5, i32 1
  br label %cond.end

cond.false:                                       ; preds = %for.body
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"class.btConvexHullInternal::Face"* [ %add.ptr, %cond.true ], [ null, %cond.false ]
  %6 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %o, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %6, i32 0, i32 0
  store %"class.btConvexHullInternal::Face"* %cond, %"class.btConvexHullInternal::Face"** %next, align 4
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  %8 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %o, align 4
  %incdec.ptr = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %8, i32 1
  store %"class.btConvexHullInternal::Face"* %incdec.ptr, %"class.btConvexHullInternal::Face"** %o, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %array4 = getelementptr inbounds %"class.btConvexHullInternal::PoolArray.3", %"class.btConvexHullInternal::PoolArray.3"* %this1, i32 0, i32 0
  %9 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %array4, align 4
  ret %"class.btConvexHullInternal::Face"* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Face"* @_ZN20btConvexHullInternal4FaceC2Ev(%"class.btConvexHullInternal::Face"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullInternal::Face"*, align 4
  store %"class.btConvexHullInternal::Face"* %this, %"class.btConvexHullInternal::Face"** %this.addr, align 4
  %this1 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %this.addr, align 4
  %next = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %this1, i32 0, i32 0
  store %"class.btConvexHullInternal::Face"* null, %"class.btConvexHullInternal::Face"** %next, align 4
  %nearbyVertex = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %this1, i32 0, i32 1
  store %"class.btConvexHullInternal::Vertex"* null, %"class.btConvexHullInternal::Vertex"** %nearbyVertex, align 4
  %nextWithSameNearbyVertex = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %this1, i32 0, i32 2
  store %"class.btConvexHullInternal::Face"* null, %"class.btConvexHullInternal::Face"** %nextWithSameNearbyVertex, align 4
  %origin = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %this1, i32 0, i32 3
  %call = call %"class.btConvexHullInternal::Point32"* @_ZN20btConvexHullInternal7Point32C2Ev(%"class.btConvexHullInternal::Point32"* %origin)
  %dir0 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %this1, i32 0, i32 4
  %call2 = call %"class.btConvexHullInternal::Point32"* @_ZN20btConvexHullInternal7Point32C2Ev(%"class.btConvexHullInternal::Point32"* %dir0)
  %dir1 = getelementptr inbounds %"class.btConvexHullInternal::Face", %"class.btConvexHullInternal::Face"* %this1, i32 0, i32 5
  %call3 = call %"class.btConvexHullInternal::Point32"* @_ZN20btConvexHullInternal7Point32C2Ev(%"class.btConvexHullInternal::Point32"* %dir1)
  ret %"class.btConvexHullInternal::Face"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE8capacityEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE7reserveEi(%class.btAlignedObjectArray.8* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %"class.btConvexHullInternal::Face"**, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE8allocateEi(%class.btAlignedObjectArray.8* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %"class.btConvexHullInternal::Face"**
  store %"class.btConvexHullInternal::Face"** %2, %"class.btConvexHullInternal::Face"*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %3 = load %"class.btConvexHullInternal::Face"**, %"class.btConvexHullInternal::Face"*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE4copyEiiPS2_(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call3, %"class.btConvexHullInternal::Face"** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %"class.btConvexHullInternal::Face"**, %"class.btConvexHullInternal::Face"*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %"class.btConvexHullInternal::Face"** %4, %"class.btConvexHullInternal::Face"*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE9allocSizeEi(%class.btAlignedObjectArray.8* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE8allocateEi(%class.btAlignedObjectArray.8* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %"class.btConvexHullInternal::Face"** @_ZN18btAlignedAllocatorIPN20btConvexHullInternal4FaceELj16EE8allocateEiPPKS2_(%class.btAlignedAllocator.9* %m_allocator, i32 %1, %"class.btConvexHullInternal::Face"*** null)
  %2 = bitcast %"class.btConvexHullInternal::Face"** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIPN20btConvexHullInternal4FaceEE4copyEiiPS2_(%class.btAlignedObjectArray.8* %this, i32 %start, i32 %end, %"class.btConvexHullInternal::Face"** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %"class.btConvexHullInternal::Face"**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %"class.btConvexHullInternal::Face"** %dest, %"class.btConvexHullInternal::Face"*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %"class.btConvexHullInternal::Face"**, %"class.btConvexHullInternal::Face"*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %3, i32 %4
  %5 = bitcast %"class.btConvexHullInternal::Face"** %arrayidx to i8*
  %6 = bitcast i8* %5 to %"class.btConvexHullInternal::Face"**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %7 = load %"class.btConvexHullInternal::Face"**, %"class.btConvexHullInternal::Face"*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %7, i32 %8
  %9 = load %"class.btConvexHullInternal::Face"*, %"class.btConvexHullInternal::Face"** %arrayidx2, align 4
  store %"class.btConvexHullInternal::Face"* %9, %"class.btConvexHullInternal::Face"** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btConvexHullInternal::Face"** @_ZN18btAlignedAllocatorIPN20btConvexHullInternal4FaceELj16EE8allocateEiPPKS2_(%class.btAlignedAllocator.9* %this, i32 %n, %"class.btConvexHullInternal::Face"*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %"class.btConvexHullInternal::Face"***, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %"class.btConvexHullInternal::Face"*** %hint, %"class.btConvexHullInternal::Face"**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %"class.btConvexHullInternal::Face"**
  ret %"class.btConvexHullInternal::Face"** %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.12* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.12* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.13* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.13* %this, %class.btVector3* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7destroyEii(%class.btAlignedObjectArray.16* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %3 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE10deallocateEv(%class.btAlignedObjectArray.16* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data, align 4
  %tobool = icmp ne %"class.btConvexHullComputer::Edge"* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %2 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE10deallocateEPS1_(%class.btAlignedAllocator.17* %m_allocator, %"class.btConvexHullComputer::Edge"* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store %"class.btConvexHullComputer::Edge"* null, %"class.btConvexHullComputer::Edge"** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4initEv(%class.btAlignedObjectArray.16* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store %"class.btConvexHullComputer::Edge"* null, %"class.btConvexHullComputer::Edge"** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE10deallocateEPS1_(%class.btAlignedAllocator.17* %this, %"class.btConvexHullComputer::Edge"* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  %ptr.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4
  store %"class.btConvexHullComputer::Edge"* %ptr, %"class.btConvexHullComputer::Edge"** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  %0 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %ptr.addr, align 4
  %1 = bitcast %"class.btConvexHullComputer::Edge"* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.20* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.20* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.20* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.21* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.20* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.21* %this, i32* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.21*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.21* %this, %class.btAlignedAllocator.21** %this.addr, align 4
  store i32* %ptr, i32** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.21*, %class.btAlignedAllocator.21** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.12* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray.12* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %2, %class.btVector3** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %3 = load %class.btVector3*, %class.btVector3** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call3, %class.btVector3* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %class.btVector3* %4, %class.btVector3** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray.12* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.13* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray.12* %this, i32 %start, i32 %end, %class.btVector3* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  %5 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %7 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  %9 = bitcast %class.btVector3* %6 to i8*
  %10 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.13* %this, i32 %n, %class.btVector3** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7reserveEi(%class.btAlignedObjectArray.16* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE8capacityEv(%class.btAlignedObjectArray.16* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE8allocateEi(%class.btAlignedObjectArray.16* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %"class.btConvexHullComputer::Edge"*
  store %"class.btConvexHullComputer::Edge"* %2, %"class.btConvexHullComputer::Edge"** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %3 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4copyEiiPS1_(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call3, %"class.btConvexHullComputer::Edge"* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7destroyEii(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE10deallocateEv(%class.btAlignedObjectArray.16* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store %"class.btConvexHullComputer::Edge"* %4, %"class.btConvexHullComputer::Edge"** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE8capacityEv(%class.btAlignedObjectArray.16* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE8allocateEi(%class.btAlignedObjectArray.16* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %"class.btConvexHullComputer::Edge"* @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.17* %m_allocator, i32 %1, %"class.btConvexHullComputer::Edge"** null)
  %2 = bitcast %"class.btConvexHullComputer::Edge"* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4copyEiiPS1_(%class.btAlignedObjectArray.16* %this, i32 %start, i32 %end, %"class.btConvexHullComputer::Edge"* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %"class.btConvexHullComputer::Edge"* %dest, %"class.btConvexHullComputer::Edge"** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %3, i32 %4
  %5 = bitcast %"class.btConvexHullComputer::Edge"* %arrayidx to i8*
  %6 = bitcast i8* %5 to %"class.btConvexHullComputer::Edge"*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %7 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %7, i32 %8
  %9 = bitcast %"class.btConvexHullComputer::Edge"* %6 to i8*
  %10 = bitcast %"class.btConvexHullComputer::Edge"* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 12, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btConvexHullComputer::Edge"* @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.17* %this, i32 %n, %"class.btConvexHullComputer::Edge"** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %"class.btConvexHullComputer::Edge"**, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %"class.btConvexHullComputer::Edge"** %hint, %"class.btConvexHullComputer::Edge"*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 12, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %"class.btConvexHullComputer::Edge"*
  ret %"class.btConvexHullComputer::Edge"* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.20* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.20* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.20* %this1, i32 %1)
  %2 = bitcast i8* %call2 to i32*
  store i32* %2, i32** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  %3 = load i32*, i32** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.20* %this1, i32 0, i32 %call3, i32* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.20* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.20* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load i32*, i32** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  store i32* %4, i32** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.20* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.20* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.21* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.20* %this, i32 %start, i32 %end, i32* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store i32* %dest, i32** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = bitcast i32* %arrayidx to i8*
  %6 = bitcast i8* %5 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %7 = load i32*, i32** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %7, i32 %8
  %9 = load i32, i32* %arrayidx2, align 4
  store i32 %9, i32* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.21* %this, i32 %n, i32** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.21*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.21* %this, %class.btAlignedAllocator.21** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32** %hint, i32*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.21*, %class.btAlignedAllocator.21** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray.12* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE9allocSizeEi(%class.btAlignedObjectArray.16* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIiE9allocSizeEi(%class.btAlignedObjectArray.20* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btConvexHullComputer.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
