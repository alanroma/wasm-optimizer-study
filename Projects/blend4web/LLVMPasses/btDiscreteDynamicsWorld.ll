; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Dynamics/btDiscreteDynamicsWorld.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Dynamics/btDiscreteDynamicsWorld.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btDiscreteDynamicsWorld = type { %class.btDynamicsWorld, %class.btAlignedObjectArray.4, %struct.InplaceSolverIslandCallback*, %class.btConstraintSolver*, %class.btSimulationIslandManager*, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.19, %class.btVector3, float, float, i8, i8, i8, i8, %class.btAlignedObjectArray.23, i32, i8, [3 x i8], %class.btAlignedObjectArray.9, %class.btSpinMutex }
%class.btDynamicsWorld = type { %class.btCollisionWorld.base, void (%class.btDynamicsWorld*, float)*, void (%class.btDynamicsWorld*, float)*, i8*, %struct.btContactSolverInfo }
%class.btCollisionWorld.base = type <{ i32 (...)**, %class.btAlignedObjectArray, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8 }>
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray.0, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type { i32 (...)** }
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float, float }
%struct.InplaceSolverIslandCallback = type { %"struct.btSimulationIslandManager::IslandCallback", %struct.btContactSolverInfo*, %class.btConstraintSolver*, %class.btTypedConstraint**, i32, %class.btIDebugDraw*, %class.btDispatcher*, %class.btAlignedObjectArray, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.4 }
%"struct.btSimulationIslandManager::IslandCallback" = type { i32 (...)** }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.7, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.7 = type { i32 }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.4, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btMotionState = type { i32 (...)** }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btSimulationIslandManager = type <{ i32 (...)**, %class.btUnionFind, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray, i8, [3 x i8] }>
%class.btUnionFind = type { %class.btAlignedObjectArray.15 }
%class.btAlignedObjectArray.15 = type <{ %class.btAlignedAllocator.16, [3 x i8], i32, i32, %struct.btElement*, i8, [3 x i8] }>
%class.btAlignedAllocator.16 = type { i8 }
%struct.btElement = type { i32, i32 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btAlignedObjectArray.19 = type <{ %class.btAlignedAllocator.20, [3 x i8], i32, i32, %class.btRigidBody**, i8, [3 x i8] }>
%class.btAlignedAllocator.20 = type { i8 }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray.23 = type <{ %class.btAlignedAllocator.24, [3 x i8], i32, i32, %class.btActionInterface**, i8, [3 x i8] }>
%class.btAlignedAllocator.24 = type { i8 }
%class.btActionInterface = type { i32 (...)** }
%class.btAlignedObjectArray.9 = type <{ %class.btAlignedAllocator.10, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.10 = type { i8 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.12, %union.anon.13, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.12 = type { float }
%union.anon.13 = type { float }
%class.btSpinMutex = type { i32 }
%class.btDispatcher = type { i32 (...)** }
%class.btBroadphaseInterface = type { i32 (...)** }
%class.btConstraintSolver = type { i32 (...)** }
%class.btCollisionConfiguration = type opaque
%class.btSequentialImpulseConstraintSolver = type { %class.btConstraintSolver, %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.31, %class.btAlignedObjectArray.31, %class.btAlignedObjectArray.31, %class.btAlignedObjectArray.31, %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.40, i32, i32, %class.btAlignedObjectArray.36, float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)*, float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)*, float, i32 }
%class.btAlignedObjectArray.27 = type <{ %class.btAlignedAllocator.28, [3 x i8], i32, i32, %struct.btSolverBody*, i8, [3 x i8] }>
%class.btAlignedAllocator.28 = type { i8 }
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btAlignedObjectArray.31 = type <{ %class.btAlignedAllocator.32, [3 x i8], i32, i32, %struct.btSolverConstraint*, i8, [3 x i8] }>
%class.btAlignedAllocator.32 = type { i8 }
%struct.btSolverConstraint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.34, i32, i32, i32, i32 }
%union.anon.34 = type { i8* }
%class.btAlignedObjectArray.40 = type <{ %class.btAlignedAllocator.41, [3 x i8], i32, i32, %"struct.btTypedConstraint::btConstraintInfo1"*, i8, [3 x i8] }>
%class.btAlignedAllocator.41 = type { i8 }
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%class.btAlignedObjectArray.36 = type <{ %class.btAlignedAllocator.37, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.37 = type { i8 }
%class.btStackAlloc = type opaque
%class.btCollisionWorld = type <{ i32 (...)**, %class.btAlignedObjectArray, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8, [3 x i8] }>
%class.CProfileSample = type { i8 }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btSortConstraintOnIslandPredicate = type { i8 }
%class.btClosestNotMeConvexResultCallback = type { %"struct.btCollisionWorld::ClosestConvexResultCallback", %class.btCollisionObject*, float, %class.btOverlappingPairCache*, %class.btDispatcher* }
%"struct.btCollisionWorld::ClosestConvexResultCallback" = type { %"struct.btCollisionWorld::ConvexResultCallback", %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btCollisionObject* }
%"struct.btCollisionWorld::ConvexResultCallback" = type { i32 (...)**, float, i32, i32 }
%class.btOverlappingPairCache = type { %class.btOverlappingPairCallback }
%class.btOverlappingPairCallback = type { i32 (...)** }
%class.btSphereShape = type { %class.btConvexInternalShape }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btPoint2PointConstraint = type { %class.btTypedConstraint, [3 x %class.btJacobianEntry], %class.btVector3, %class.btVector3, i32, float, float, i8, %struct.btConstraintSetting }
%class.btJacobianEntry = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float }
%struct.btConstraintSetting = type { float, float, float }
%class.btHingeConstraint = type { %class.btTypedConstraint, [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry], %class.btTransform, %class.btTransform, float, float, %class.btAngularLimit, float, float, float, float, i8, i8, i8, i8, i8, float, i32, float, float, float, float }
%class.btAngularLimit = type <{ float, float, float, float, float, float, float, i8, [3 x i8] }>
%class.btConeTwistConstraint = type { %class.btTypedConstraint, [3 x %class.btJacobianEntry], %class.btTransform, %class.btTransform, float, float, float, float, float, float, float, float, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, i8, i8, i8, i8, float, float, %class.btVector3, i8, i8, %class.btQuaternion, float, %class.btVector3, i32, float, float, float }
%class.btGeneric6DofConstraint = type <{ %class.btTypedConstraint, %class.btTransform, %class.btTransform, [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry], %class.btTranslationalLimitMotor, [3 x %class.btRotationalLimitMotor], float, %class.btTransform, %class.btTransform, %class.btVector3, [3 x %class.btVector3], %class.btVector3, float, float, i8, [3 x i8], %class.btVector3, i8, i8, [2 x i8], i32, i8, [3 x i8] }>
%class.btTranslationalLimitMotor = type { %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, %class.btVector3, %class.btVector3, %class.btVector3, [3 x i8], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, [3 x i32] }
%class.btRotationalLimitMotor = type { float, float, float, float, float, float, float, float, float, float, float, i8, float, float, i32, float }
%class.btGeneric6DofSpring2Constraint = type { %class.btTypedConstraint, %class.btTransform, %class.btTransform, [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry], %class.btTranslationalLimitMotor2, [3 x %class.btRotationalLimitMotor2], i32, %class.btTransform, %class.btTransform, %class.btVector3, [3 x %class.btVector3], %class.btVector3, float, float, i8, i32 }
%class.btTranslationalLimitMotor2 = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, [3 x i8], [3 x i8], [3 x i8], %class.btVector3, %class.btVector3, [3 x i8], %class.btVector3, [3 x i8], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, [3 x i32] }
%class.btRotationalLimitMotor2 = type { float, float, float, float, float, float, float, i8, float, float, i8, float, i8, float, i8, float, i8, float, float, float, float, i32 }
%class.btSliderConstraint = type { %class.btTypedConstraint, i8, i8, %class.btTransform, %class.btTransform, i8, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, i8, i8, i32, [3 x %class.btJacobianEntry], [3 x float], [3 x %class.btJacobianEntry], float, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i8, float, float, float, i8, float, float, float }
%class.btSerializer = type { i32 (...)** }
%class.btChunk = type { i32, i32, i8*, i32, i32 }
%struct.btDynamicsWorldFloatData = type { %struct.btContactSolverInfoFloatData, %struct.btVector3FloatData }
%struct.btContactSolverInfoFloatData = type { float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, [4 x i8] }
%struct.btVector3FloatData = type { [4 x float] }
%"struct.btCollisionWorld::RayResultCallback" = type { i32 (...)**, float, %class.btCollisionObject*, i32, i32, i32 }
%"struct.btCollisionWorld::LocalConvexResult" = type { %class.btCollisionObject*, %"struct.btCollisionWorld::LocalShapeInfo"*, %class.btVector3, %class.btVector3, float }
%"struct.btCollisionWorld::LocalShapeInfo" = type { i32, i32 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN15btDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP11btRigidBodyEC2Ev = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btActionInterfaceEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev = comdat any

$_ZN11btSpinMutexC2Ev = comdat any

$_ZN35btSequentialImpulseConstraintSolvernwEmPv = comdat any

$_ZN27InplaceSolverIslandCallbackC2EP18btConstraintSolverP12btStackAllocP12btDispatcher = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btActionInterfaceED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP11btRigidBodyED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev = comdat any

$_ZN23btDiscreteDynamicsWorlddlEPv = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi = comdat any

$_ZN11btRigidBody6upcastEP17btCollisionObject = comdat any

$_ZNK17btCollisionObject18getActivationStateEv = comdat any

$_ZNK17btCollisionObject17isKinematicObjectEv = comdat any

$_ZNK20btAlignedObjectArrayIP17btActionInterfaceE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btActionInterfaceEixEi = comdat any

$_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP11btRigidBodyEixEi = comdat any

$_ZN11btRigidBody11clearForcesEv = comdat any

$_ZNK17btCollisionObject8isActiveEv = comdat any

$_ZN11btRigidBody14getMotionStateEv = comdat any

$_ZNK17btCollisionObject25isStaticOrKinematicObjectEv = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_ = comdat any

$_ZN17btCollisionObject30getInterpolationWorldTransformEv = comdat any

$_ZNK17btCollisionObject30getInterpolationLinearVelocityEv = comdat any

$_ZNK17btCollisionObject31getInterpolationAngularVelocityEv = comdat any

$_ZNK17btCollisionObject14getHitFractionEv = comdat any

$_Z11btFuzzyZerof = comdat any

$_ZN16btCollisionWorld15getDispatchInfoEv = comdat any

$_ZN15btDynamicsWorld13getSolverInfoEv = comdat any

$_ZNK11btRigidBody8getFlagsEv = comdat any

$_ZN20btAlignedObjectArrayIP11btRigidBodyE6removeERKS1_ = comdat any

$_ZN11btRigidBody17getCollisionShapeEv = comdat any

$_ZNK17btCollisionObject14isStaticObjectEv = comdat any

$_ZN20btAlignedObjectArrayIP11btRigidBodyE9push_backERKS1_ = comdat any

$_ZN11btRigidBody18updateDeactivationEf = comdat any

$_ZN11btRigidBody13wantsSleepingEv = comdat any

$_ZN11btRigidBody18setAngularVelocityERK9btVector3 = comdat any

$_ZN11btRigidBody17setLinearVelocityERK9btVector3 = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_ = comdat any

$_ZN17btTypedConstraint13getRigidBodyAEv = comdat any

$_ZN17btTypedConstraint13getRigidBodyBEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE6removeERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btActionInterfaceE9push_backERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btActionInterfaceE6removeERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE6resizeEiRKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE9quickSortI33btSortConstraintOnIslandPredicateEEvRKT_ = comdat any

$_ZN27InplaceSolverIslandCallback5setupEP19btContactSolverInfoPP17btTypedConstraintiP12btIDebugDraw = comdat any

$_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv = comdat any

$_ZNK16btCollisionWorld22getNumCollisionObjectsEv = comdat any

$_ZN16btCollisionWorld13getDispatcherEv = comdat any

$_ZN27InplaceSolverIslandCallback18processConstraintsEv = comdat any

$_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK20btPersistentManifold8getBody1Ev = comdat any

$_ZN25btSimulationIslandManager12getUnionFindEv = comdat any

$_ZN11btUnionFind5uniteEii = comdat any

$_ZNK17btCollisionObject12getIslandTagEv = comdat any

$_ZNK17btTypedConstraint9isEnabledEv = comdat any

$_ZN17btCollisionObject14setHitFractionEf = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZN17btCollisionObject17getWorldTransformEv = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK17btCollisionObject27getCcdSquareMotionThresholdEv = comdat any

$_ZNK16btCollisionShape8isConvexEv = comdat any

$_ZN16btCollisionWorld13getBroadphaseEv = comdat any

$_ZN34btClosestNotMeConvexResultCallbackC2EP17btCollisionObjectRK9btVector3S4_P22btOverlappingPairCacheP12btDispatcher = comdat any

$_ZNK17btCollisionObject23getCcdSweptSphereRadiusEv = comdat any

$_ZN13btSphereShapeC2Ef = comdat any

$_ZN11btRigidBody18getBroadphaseProxyEv = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN11btTransform8setBasisERK11btMatrix3x3 = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZngRK9btVector3 = comdat any

$_Z11btMutexLockP11btSpinMutex = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_ = comdat any

$_Z13btMutexUnlockP11btSpinMutex = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK17btCollisionObject17getWorldTransformEv = comdat any

$_ZNK11btTransform7inverseEv = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZN15btManifoldPointC2ERK9btVector3S2_S2_f = comdat any

$_ZN20btPersistentManifold15getContactPointEi = comdat any

$_ZN13btSphereShapeD2Ev = comdat any

$_ZN34btClosestNotMeConvexResultCallbackD2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv = comdat any

$_ZNK20btPersistentManifold14getNumContactsEv = comdat any

$_ZNK15btManifoldPoint19getPositionWorldOnAEv = comdat any

$_ZNK15btManifoldPoint19getPositionWorldOnBEv = comdat any

$_ZN11btRigidBody12applyImpulseERK9btVector3S2_ = comdat any

$_ZN17btTypedConstraint14getDbgDrawSizeEv = comdat any

$_ZNK17btTypedConstraint17getConstraintTypeEv = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZNK23btPoint2PointConstraint11getPivotInAEv = comdat any

$_ZNK11btRigidBody24getCenterOfMassTransformEv = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZNK23btPoint2PointConstraint11getPivotInBEv = comdat any

$_ZN17btHingeConstraint13getRigidBodyAEv = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZN17btHingeConstraint9getAFrameEv = comdat any

$_ZN17btHingeConstraint13getRigidBodyBEv = comdat any

$_ZN17btHingeConstraint9getBFrameEv = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZNK17btHingeConstraint13getLowerLimitEv = comdat any

$_ZNK17btHingeConstraint13getUpperLimitEv = comdat any

$_ZNK17btHingeConstraint8hasLimitEv = comdat any

$_ZNK11btMatrix3x39getColumnEi = comdat any

$_ZNK21btConeTwistConstraint13getRigidBodyAEv = comdat any

$_ZNK21btConeTwistConstraint9getAFrameEv = comdat any

$_ZNK21btConeTwistConstraint13getRigidBodyBEv = comdat any

$_ZNK21btConeTwistConstraint9getBFrameEv = comdat any

$_ZNK21btConeTwistConstraint12getTwistSpanEv = comdat any

$_ZNK21btConeTwistConstraint13getTwistAngleEv = comdat any

$_ZNK11btRigidBody10getInvMassEv = comdat any

$_ZNK23btGeneric6DofConstraint23getCalculatedTransformAEv = comdat any

$_ZNK23btGeneric6DofConstraint23getCalculatedTransformBEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZN23btGeneric6DofConstraint23getRotationalLimitMotorEi = comdat any

$_Z5btCosf = comdat any

$_Z5btSinf = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN23btGeneric6DofConstraint26getTranslationalLimitMotorEv = comdat any

$_ZNK30btGeneric6DofSpring2Constraint23getCalculatedTransformAEv = comdat any

$_ZNK30btGeneric6DofSpring2Constraint23getCalculatedTransformBEv = comdat any

$_ZN30btGeneric6DofSpring2Constraint23getRotationalLimitMotorEi = comdat any

$_ZNK30btGeneric6DofSpring2Constraint8getAngleEi = comdat any

$_ZN30btGeneric6DofSpring2Constraint26getTranslationalLimitMotorEv = comdat any

$_ZNK18btSliderConstraint23getCalculatedTransformAEv = comdat any

$_ZNK18btSliderConstraint23getCalculatedTransformBEv = comdat any

$_ZN18btSliderConstraint27getUseLinearReferenceFrameAEv = comdat any

$_ZN18btSliderConstraint16getLowerLinLimitEv = comdat any

$_ZN18btSliderConstraint16getUpperLinLimitEv = comdat any

$_ZN18btSliderConstraint16getLowerAngLimitEv = comdat any

$_ZN18btSliderConstraint16getUpperAngLimitEv = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintEixEi = comdat any

$_ZNK17btCollisionObject15getInternalTypeEv = comdat any

$_ZNK9btVector39serializeER18btVector3FloatData = comdat any

$_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw = comdat any

$_ZN16btCollisionWorld14getDebugDrawerEv = comdat any

$_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv = comdat any

$_ZN23btDiscreteDynamicsWorld11setNumTasksEi = comdat any

$_ZN23btDiscreteDynamicsWorld14updateVehiclesEf = comdat any

$_ZN19btContactSolverInfoC2Ev = comdat any

$_ZN15btDynamicsWorldD2Ev = comdat any

$_ZN15btDynamicsWorldD0Ev = comdat any

$_ZN15btDynamicsWorld13addConstraintEP17btTypedConstraintb = comdat any

$_ZN15btDynamicsWorld16removeConstraintEP17btTypedConstraint = comdat any

$_ZNK15btDynamicsWorld17getNumConstraintsEv = comdat any

$_ZN15btDynamicsWorld13getConstraintEi = comdat any

$_ZNK15btDynamicsWorld13getConstraintEi = comdat any

$_ZN15btDynamicsWorld10addVehicleEP17btActionInterface = comdat any

$_ZN15btDynamicsWorld13removeVehicleEP17btActionInterface = comdat any

$_ZN15btDynamicsWorld12addCharacterEP17btActionInterface = comdat any

$_ZN15btDynamicsWorld15removeCharacterEP17btActionInterface = comdat any

$_ZN25btSimulationIslandManager14IslandCallbackC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev = comdat any

$_ZN27InplaceSolverIslandCallbackD2Ev = comdat any

$_ZN27InplaceSolverIslandCallbackD0Ev = comdat any

$_ZN27InplaceSolverIslandCallback13processIslandEPP17btCollisionObjectiPP20btPersistentManifoldii = comdat any

$_ZN25btSimulationIslandManager14IslandCallbackD2Ev = comdat any

$_ZN25btSimulationIslandManager14IslandCallbackD0Ev = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_ = comdat any

$_Z23btGetConstraintIslandIdPK17btTypedConstraint = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_ = comdat any

$_ZNK17btTypedConstraint13getRigidBodyAEv = comdat any

$_ZNK17btTypedConstraint13getRigidBodyBEv = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZNK11btTransform11getRotationEv = comdat any

$_ZmlRK12btQuaternionS1_ = comdat any

$_ZN12btQuaternion9normalizeEv = comdat any

$_ZN11btTransform11setRotationERK12btQuaternion = comdat any

$_Z6btSqrtf = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_ZN12btQuaternionC2Ev = comdat any

$_ZNK11btMatrix3x311getRotationER12btQuaternion = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZNK12btQuaternion6lengthEv = comdat any

$_ZN12btQuaterniondVERKf = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN12btQuaternionmLERKf = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_Z6btFabsf = comdat any

$_ZNK11btRigidBody17getLinearVelocityEv = comdat any

$_ZNK11btRigidBody18getAngularVelocityEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

$_ZN11btUnionFind4findEi = comdat any

$_ZN20btAlignedObjectArrayI9btElementEixEi = comdat any

$_ZN17btBroadphaseProxy8isConvexEi = comdat any

$_ZNK16btCollisionShape12getShapeTypeEv = comdat any

$_ZN16btCollisionWorld27ClosestConvexResultCallbackC2ERK9btVector3S3_ = comdat any

$_ZN34btClosestNotMeConvexResultCallbackD0Ev = comdat any

$_ZNK34btClosestNotMeConvexResultCallback14needsCollisionEP17btBroadphaseProxy = comdat any

$_ZN34btClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb = comdat any

$_ZN16btCollisionWorld20ConvexResultCallbackC2Ev = comdat any

$_ZN16btCollisionWorld27ClosestConvexResultCallbackD2Ev = comdat any

$_ZN16btCollisionWorld27ClosestConvexResultCallbackD0Ev = comdat any

$_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy = comdat any

$_ZN16btCollisionWorld27ClosestConvexResultCallback15addSingleResultERNS_17LocalConvexResultEb = comdat any

$_ZN16btCollisionWorld20ConvexResultCallbackD2Ev = comdat any

$_ZN16btCollisionWorld20ConvexResultCallbackD0Ev = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK17btCollisionObject18hasContactResponseEv = comdat any

$_ZN9btVector34setXEf = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN21btConvexInternalShapeD2Ev = comdat any

$_ZN11btRigidBody19applyCentralImpulseERK9btVector3 = comdat any

$_ZN11btRigidBody18applyTorqueImpulseERK9btVector3 = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK14btAngularLimit12getHalfRangeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE8allocateEiPPKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE9allocSizeEi = comdat any

$_ZN18btAlignedAllocatorIP11btRigidBodyLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP11btRigidBodyE4initEv = comdat any

$_ZN18btAlignedAllocatorIP17btActionInterfaceLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btActionInterfaceE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP11btRigidBodyE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP11btRigidBodyE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP11btRigidBodyE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP11btRigidBodyLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btActionInterfaceE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP17btActionInterfaceE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP17btActionInterfaceE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP17btActionInterfaceLj16EE10deallocateEPS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP11btRigidBodyE16findLinearSearchERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP11btRigidBodyE13removeAtIndexEi = comdat any

$_ZN20btAlignedObjectArrayIP11btRigidBodyE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIP11btRigidBodyE8pop_backEv = comdat any

$_ZNK20btAlignedObjectArrayIP11btRigidBodyE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP11btRigidBodyE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP11btRigidBodyE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP11btRigidBodyE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP11btRigidBodyE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP11btRigidBodyLj16EE8allocateEiPPKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE16findLinearSearchERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE13removeAtIndexEi = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE8pop_backEv = comdat any

$_ZNK20btAlignedObjectArrayIP17btActionInterfaceE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP17btActionInterfaceE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP17btActionInterfaceE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP17btActionInterfaceE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP17btActionInterfaceE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP17btActionInterfaceLj16EE8allocateEiPPKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP17btActionInterfaceE16findLinearSearchERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btActionInterfaceE13removeAtIndexEi = comdat any

$_ZN20btAlignedObjectArrayIP17btActionInterfaceE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIP17btActionInterfaceE8pop_backEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI33btSortConstraintOnIslandPredicateEEvRKT_ii = comdat any

$_ZNK33btSortConstraintOnIslandPredicateclEPK17btTypedConstraintS2_ = comdat any

$_ZTS15btDynamicsWorld = comdat any

$_ZTI15btDynamicsWorld = comdat any

$_ZTV15btDynamicsWorld = comdat any

$_ZTV27InplaceSolverIslandCallback = comdat any

$_ZTS27InplaceSolverIslandCallback = comdat any

$_ZTSN25btSimulationIslandManager14IslandCallbackE = comdat any

$_ZTIN25btSimulationIslandManager14IslandCallbackE = comdat any

$_ZTI27InplaceSolverIslandCallback = comdat any

$_ZTVN25btSimulationIslandManager14IslandCallbackE = comdat any

$_ZTV34btClosestNotMeConvexResultCallback = comdat any

$_ZTS34btClosestNotMeConvexResultCallback = comdat any

$_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE = comdat any

$_ZTSN16btCollisionWorld20ConvexResultCallbackE = comdat any

$_ZTIN16btCollisionWorld20ConvexResultCallbackE = comdat any

$_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE = comdat any

$_ZTI34btClosestNotMeConvexResultCallback = comdat any

$_ZTVN16btCollisionWorld27ClosestConvexResultCallbackE = comdat any

$_ZTVN16btCollisionWorld20ConvexResultCallbackE = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV23btDiscreteDynamicsWorld = hidden unnamed_addr constant { [49 x i8*] } { [49 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btDiscreteDynamicsWorld to i8*), i8* bitcast (%class.btDiscreteDynamicsWorld* (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorldD1Ev to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorldD0Ev to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld11updateAabbsEv to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld23computeOverlappingPairsEv to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btIDebugDraw*)* @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw to i8*), i8* bitcast (%class.btIDebugDraw* (%class.btCollisionWorld*)* @_ZN16btCollisionWorld14getDebugDrawerEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btTransform*, %class.btCollisionShape*, %class.btVector3*)* @_ZN16btCollisionWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3 to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)* @_ZNK16btCollisionWorld7rayTestERK9btVector3S2_RNS_17RayResultCallbackE to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32)* @_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectii to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*)* @_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btSerializer*)* @_ZN23btDiscreteDynamicsWorld9serializeEP12btSerializer to i8*), i8* bitcast (i32 (%class.btDiscreteDynamicsWorld*, float, i32, float)* @_ZN23btDiscreteDynamicsWorld14stepSimulationEfif to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*, i1)* @_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*)* @_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btVector3*)* @_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btDiscreteDynamicsWorld*)* @_ZNK23btDiscreteDynamicsWorld10getGravityEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*)* @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*, i32, i32)* @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyii to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*)* @_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btConstraintSolver*)* @_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver to i8*), i8* bitcast (%class.btConstraintSolver* (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv to i8*), i8* bitcast (i32 (%class.btDiscreteDynamicsWorld*)* @_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btDiscreteDynamicsWorld*, i32)* @_ZN23btDiscreteDynamicsWorld13getConstraintEi to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btDiscreteDynamicsWorld*, i32)* @_ZNK23btDiscreteDynamicsWorld13getConstraintEi to i8*), i8* bitcast (i32 (%class.btDiscreteDynamicsWorld*)* @_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld11clearForcesEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld19integrateTransformsEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld26calculateSimulationIslandsEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %struct.btContactSolverInfo*)* @_ZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfo to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld21updateActivationStateEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld24createPredictiveContactsEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*)* @_ZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraint to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld12applyGravityEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, i32)* @_ZN23btDiscreteDynamicsWorld11setNumTasksEi to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld14updateVehiclesEf to i8*)] }, align 4
@.str = private unnamed_addr constant [15 x i8] c"debugDrawWorld\00", align 1
@.str.1 = private unnamed_addr constant [24 x i8] c"synchronizeMotionStates\00", align 1
@.str.2 = private unnamed_addr constant [15 x i8] c"stepSimulation\00", align 1
@gDisableDeactivation = external global i8, align 1
@.str.3 = private unnamed_addr constant [29 x i8] c"internalSingleStepSimulation\00", align 1
@.str.4 = private unnamed_addr constant [14 x i8] c"updateActions\00", align 1
@.str.5 = private unnamed_addr constant [22 x i8] c"updateActivationState\00", align 1
@.str.6 = private unnamed_addr constant [17 x i8] c"solveConstraints\00", align 1
@.str.7 = private unnamed_addr constant [27 x i8] c"calculateSimulationIslands\00", align 1
@gNumClampedCcdMotions = hidden global i32 0, align 4
@.str.8 = private unnamed_addr constant [27 x i8] c"predictive convexSweepTest\00", align 1
@.str.9 = private unnamed_addr constant [37 x i8] c"release predictive contact manifolds\00", align 1
@.str.10 = private unnamed_addr constant [25 x i8] c"createPredictiveContacts\00", align 1
@.str.11 = private unnamed_addr constant [20 x i8] c"CCD motion clamping\00", align 1
@.str.12 = private unnamed_addr constant [20 x i8] c"integrateTransforms\00", align 1
@.str.13 = private unnamed_addr constant [38 x i8] c"apply speculative contact restitution\00", align 1
@.str.14 = private unnamed_addr constant [26 x i8] c"predictUnconstraintMotion\00", align 1
@_ZZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraintE9nSegments = internal global i32 32, align 4
@.str.15 = private unnamed_addr constant [25 x i8] c"btDynamicsWorldFloatData\00", align 1
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS23btDiscreteDynamicsWorld = hidden constant [26 x i8] c"23btDiscreteDynamicsWorld\00", align 1
@_ZTS15btDynamicsWorld = linkonce_odr hidden constant [18 x i8] c"15btDynamicsWorld\00", comdat, align 1
@_ZTI16btCollisionWorld = external constant i8*
@_ZTI15btDynamicsWorld = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @_ZTS15btDynamicsWorld, i32 0, i32 0), i8* bitcast (i8** @_ZTI16btCollisionWorld to i8*) }, comdat, align 4
@_ZTI23btDiscreteDynamicsWorld = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btDiscreteDynamicsWorld, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btDynamicsWorld to i8*) }, align 4
@_ZTV15btDynamicsWorld = linkonce_odr hidden unnamed_addr constant { [37 x i8*] } { [37 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btDynamicsWorld to i8*), i8* bitcast (%class.btDynamicsWorld* (%class.btDynamicsWorld*)* @_ZN15btDynamicsWorldD2Ev to i8*), i8* bitcast (void (%class.btDynamicsWorld*)* @_ZN15btDynamicsWorldD0Ev to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld11updateAabbsEv to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld23computeOverlappingPairsEv to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btIDebugDraw*)* @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw to i8*), i8* bitcast (%class.btIDebugDraw* (%class.btCollisionWorld*)* @_ZN16btCollisionWorld14getDebugDrawerEv to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btTransform*, %class.btCollisionShape*, %class.btVector3*)* @_ZN16btCollisionWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3 to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)* @_ZNK16btCollisionWorld7rayTestERK9btVector3S2_RNS_17RayResultCallbackE to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btCollisionObject*, i32, i32)* @_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectii to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btCollisionObject*)* @_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btSerializer*)* @_ZN16btCollisionWorld9serializeEP12btSerializer to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btDynamicsWorld*, %class.btTypedConstraint*, i1)* @_ZN15btDynamicsWorld13addConstraintEP17btTypedConstraintb to i8*), i8* bitcast (void (%class.btDynamicsWorld*, %class.btTypedConstraint*)* @_ZN15btDynamicsWorld16removeConstraintEP17btTypedConstraint to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (i32 (%class.btDynamicsWorld*)* @_ZNK15btDynamicsWorld17getNumConstraintsEv to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btDynamicsWorld*, i32)* @_ZN15btDynamicsWorld13getConstraintEi to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btDynamicsWorld*, i32)* @_ZNK15btDynamicsWorld13getConstraintEi to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btDynamicsWorld*, %class.btActionInterface*)* @_ZN15btDynamicsWorld10addVehicleEP17btActionInterface to i8*), i8* bitcast (void (%class.btDynamicsWorld*, %class.btActionInterface*)* @_ZN15btDynamicsWorld13removeVehicleEP17btActionInterface to i8*), i8* bitcast (void (%class.btDynamicsWorld*, %class.btActionInterface*)* @_ZN15btDynamicsWorld12addCharacterEP17btActionInterface to i8*), i8* bitcast (void (%class.btDynamicsWorld*, %class.btActionInterface*)* @_ZN15btDynamicsWorld15removeCharacterEP17btActionInterface to i8*)] }, comdat, align 4
@_ZTV27InplaceSolverIslandCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI27InplaceSolverIslandCallback to i8*), i8* bitcast (%struct.InplaceSolverIslandCallback* (%struct.InplaceSolverIslandCallback*)* @_ZN27InplaceSolverIslandCallbackD2Ev to i8*), i8* bitcast (void (%struct.InplaceSolverIslandCallback*)* @_ZN27InplaceSolverIslandCallbackD0Ev to i8*), i8* bitcast (void (%struct.InplaceSolverIslandCallback*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)* @_ZN27InplaceSolverIslandCallback13processIslandEPP17btCollisionObjectiPP20btPersistentManifoldii to i8*)] }, comdat, align 4
@_ZTS27InplaceSolverIslandCallback = linkonce_odr hidden constant [30 x i8] c"27InplaceSolverIslandCallback\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTSN25btSimulationIslandManager14IslandCallbackE = linkonce_odr hidden constant [46 x i8] c"N25btSimulationIslandManager14IslandCallbackE\00", comdat, align 1
@_ZTIN25btSimulationIslandManager14IslandCallbackE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([46 x i8], [46 x i8]* @_ZTSN25btSimulationIslandManager14IslandCallbackE, i32 0, i32 0) }, comdat, align 4
@_ZTI27InplaceSolverIslandCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([30 x i8], [30 x i8]* @_ZTS27InplaceSolverIslandCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN25btSimulationIslandManager14IslandCallbackE to i8*) }, comdat, align 4
@_ZTVN25btSimulationIslandManager14IslandCallbackE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN25btSimulationIslandManager14IslandCallbackE to i8*), i8* bitcast (%"struct.btSimulationIslandManager::IslandCallback"* (%"struct.btSimulationIslandManager::IslandCallback"*)* @_ZN25btSimulationIslandManager14IslandCallbackD2Ev to i8*), i8* bitcast (void (%"struct.btSimulationIslandManager::IslandCallback"*)* @_ZN25btSimulationIslandManager14IslandCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@gDeactivationTime = external global float, align 4
@_ZTV34btClosestNotMeConvexResultCallback = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI34btClosestNotMeConvexResultCallback to i8*), i8* bitcast (%class.btClosestNotMeConvexResultCallback* (%class.btClosestNotMeConvexResultCallback*)* @_ZN34btClosestNotMeConvexResultCallbackD2Ev to i8*), i8* bitcast (void (%class.btClosestNotMeConvexResultCallback*)* @_ZN34btClosestNotMeConvexResultCallbackD0Ev to i8*), i8* bitcast (i1 (%class.btClosestNotMeConvexResultCallback*, %struct.btBroadphaseProxy*)* @_ZNK34btClosestNotMeConvexResultCallback14needsCollisionEP17btBroadphaseProxy to i8*), i8* bitcast (float (%class.btClosestNotMeConvexResultCallback*, %"struct.btCollisionWorld::LocalConvexResult"*, i1)* @_ZN34btClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb to i8*)] }, comdat, align 4
@_ZTS34btClosestNotMeConvexResultCallback = linkonce_odr hidden constant [37 x i8] c"34btClosestNotMeConvexResultCallback\00", comdat, align 1
@_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE = linkonce_odr hidden constant [50 x i8] c"N16btCollisionWorld27ClosestConvexResultCallbackE\00", comdat, align 1
@_ZTSN16btCollisionWorld20ConvexResultCallbackE = linkonce_odr hidden constant [43 x i8] c"N16btCollisionWorld20ConvexResultCallbackE\00", comdat, align 1
@_ZTIN16btCollisionWorld20ConvexResultCallbackE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([43 x i8], [43 x i8]* @_ZTSN16btCollisionWorld20ConvexResultCallbackE, i32 0, i32 0) }, comdat, align 4
@_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([50 x i8], [50 x i8]* @_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN16btCollisionWorld20ConvexResultCallbackE to i8*) }, comdat, align 4
@_ZTI34btClosestNotMeConvexResultCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([37 x i8], [37 x i8]* @_ZTS34btClosestNotMeConvexResultCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE to i8*) }, comdat, align 4
@_ZTVN16btCollisionWorld27ClosestConvexResultCallbackE = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE to i8*), i8* bitcast (%"struct.btCollisionWorld::ClosestConvexResultCallback"* (%"struct.btCollisionWorld::ClosestConvexResultCallback"*)* @_ZN16btCollisionWorld27ClosestConvexResultCallbackD2Ev to i8*), i8* bitcast (void (%"struct.btCollisionWorld::ClosestConvexResultCallback"*)* @_ZN16btCollisionWorld27ClosestConvexResultCallbackD0Ev to i8*), i8* bitcast (i1 (%"struct.btCollisionWorld::ConvexResultCallback"*, %struct.btBroadphaseProxy*)* @_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy to i8*), i8* bitcast (float (%"struct.btCollisionWorld::ClosestConvexResultCallback"*, %"struct.btCollisionWorld::LocalConvexResult"*, i1)* @_ZN16btCollisionWorld27ClosestConvexResultCallback15addSingleResultERNS_17LocalConvexResultEb to i8*)] }, comdat, align 4
@_ZTVN16btCollisionWorld20ConvexResultCallbackE = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN16btCollisionWorld20ConvexResultCallbackE to i8*), i8* bitcast (%"struct.btCollisionWorld::ConvexResultCallback"* (%"struct.btCollisionWorld::ConvexResultCallback"*)* @_ZN16btCollisionWorld20ConvexResultCallbackD2Ev to i8*), i8* bitcast (void (%"struct.btCollisionWorld::ConvexResultCallback"*)* @_ZN16btCollisionWorld20ConvexResultCallbackD0Ev to i8*), i8* bitcast (i1 (%"struct.btCollisionWorld::ConvexResultCallback"*, %struct.btBroadphaseProxy*)* @_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTV13btSphereShape = external unnamed_addr constant { [25 x i8*] }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btDiscreteDynamicsWorld.cpp, i8* null }]

@_ZN23btDiscreteDynamicsWorldC1EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration = hidden unnamed_addr alias %class.btDiscreteDynamicsWorld* (%class.btDiscreteDynamicsWorld*, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btConstraintSolver*, %class.btCollisionConfiguration*), %class.btDiscreteDynamicsWorld* (%class.btDiscreteDynamicsWorld*, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btConstraintSolver*, %class.btCollisionConfiguration*)* @_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
@_ZN23btDiscreteDynamicsWorldD1Ev = hidden unnamed_addr alias %class.btDiscreteDynamicsWorld* (%class.btDiscreteDynamicsWorld*), %class.btDiscreteDynamicsWorld* (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorldD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration(%class.btDiscreteDynamicsWorld* returned %this, %class.btDispatcher* %dispatcher, %class.btBroadphaseInterface* %pairCache, %class.btConstraintSolver* %constraintSolver, %class.btCollisionConfiguration* %collisionConfiguration) unnamed_addr #2 {
entry:
  %retval = alloca %class.btDiscreteDynamicsWorld*, align 4
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %pairCache.addr = alloca %class.btBroadphaseInterface*, align 4
  %constraintSolver.addr = alloca %class.btConstraintSolver*, align 4
  %collisionConfiguration.addr = alloca %class.btCollisionConfiguration*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %mem = alloca i8*, align 4
  %mem17 = alloca i8*, align 4
  %mem20 = alloca i8*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  store %class.btBroadphaseInterface* %pairCache, %class.btBroadphaseInterface** %pairCache.addr, align 4
  store %class.btConstraintSolver* %constraintSolver, %class.btConstraintSolver** %constraintSolver.addr, align 4
  store %class.btCollisionConfiguration* %collisionConfiguration, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btDiscreteDynamicsWorld* %this1, %class.btDiscreteDynamicsWorld** %retval, align 4
  %0 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %1 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %2 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %pairCache.addr, align 4
  %3 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4
  %call = call %class.btDynamicsWorld* @_ZN15btDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration(%class.btDynamicsWorld* %0, %class.btDispatcher* %1, %class.btBroadphaseInterface* %2, %class.btCollisionConfiguration* %3)
  %4 = bitcast %class.btDiscreteDynamicsWorld* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [49 x i8*] }, { [49 x i8*] }* @_ZTV23btDiscreteDynamicsWorld, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %4, align 4
  %m_sortedConstraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev(%class.btAlignedObjectArray.4* %m_sortedConstraints)
  %m_solverIslandCallback = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 2
  store %struct.InplaceSolverIslandCallback* null, %struct.InplaceSolverIslandCallback** %m_solverIslandCallback, align 4
  %m_constraintSolver = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 3
  %5 = load %class.btConstraintSolver*, %class.btConstraintSolver** %constraintSolver.addr, align 4
  store %class.btConstraintSolver* %5, %class.btConstraintSolver** %m_constraintSolver, align 4
  %m_constraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 5
  %call3 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev(%class.btAlignedObjectArray.4* %m_constraints)
  %m_nonStaticRigidBodies = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %call4 = call %class.btAlignedObjectArray.19* @_ZN20btAlignedObjectArrayIP11btRigidBodyEC2Ev(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies)
  %m_gravity = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 7
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float -1.000000e+01, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_gravity, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %m_localTime = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 8
  store float 0.000000e+00, float* %m_localTime, align 4
  %m_fixedTimeStep = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 9
  store float 0.000000e+00, float* %m_fixedTimeStep, align 4
  %m_synchronizeAllMotionStates = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 12
  store i8 0, i8* %m_synchronizeAllMotionStates, align 2
  %m_applySpeculativeContactRestitution = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 13
  store i8 0, i8* %m_applySpeculativeContactRestitution, align 1
  %m_actions = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 14
  %call8 = call %class.btAlignedObjectArray.23* @_ZN20btAlignedObjectArrayIP17btActionInterfaceEC2Ev(%class.btAlignedObjectArray.23* %m_actions)
  %m_profileTimings = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 15
  store i32 0, i32* %m_profileTimings, align 4
  %m_latencyMotionStateInterpolation = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 16
  store i8 1, i8* %m_latencyMotionStateInterpolation, align 4
  %m_predictiveManifolds = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 18
  %call9 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.9* %m_predictiveManifolds)
  %m_predictiveManifoldsMutex = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 19
  %call10 = call %class.btSpinMutex* @_ZN11btSpinMutexC2Ev(%class.btSpinMutex* %m_predictiveManifoldsMutex)
  %m_constraintSolver11 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 3
  %6 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver11, align 4
  %tobool = icmp ne %class.btConstraintSolver* %6, null
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  %call12 = call i8* @_Z22btAlignedAllocInternalmi(i32 228, i32 16)
  store i8* %call12, i8** %mem, align 4
  %7 = load i8*, i8** %mem, align 4
  %call13 = call i8* @_ZN35btSequentialImpulseConstraintSolvernwEmPv(i32 228, i8* %7)
  %8 = bitcast i8* %call13 to %class.btSequentialImpulseConstraintSolver*
  %call14 = call %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverC1Ev(%class.btSequentialImpulseConstraintSolver* %8)
  %9 = bitcast %class.btSequentialImpulseConstraintSolver* %8 to %class.btConstraintSolver*
  %m_constraintSolver15 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 3
  store %class.btConstraintSolver* %9, %class.btConstraintSolver** %m_constraintSolver15, align 4
  %m_ownsConstraintSolver = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 11
  store i8 1, i8* %m_ownsConstraintSolver, align 1
  br label %if.end

if.else:                                          ; preds = %entry
  %m_ownsConstraintSolver16 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 11
  store i8 0, i8* %m_ownsConstraintSolver16, align 1
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %call18 = call i8* @_Z22btAlignedAllocInternalmi(i32 68, i32 16)
  store i8* %call18, i8** %mem17, align 4
  %10 = load i8*, i8** %mem17, align 4
  %11 = bitcast i8* %10 to %class.btSimulationIslandManager*
  %call19 = call %class.btSimulationIslandManager* @_ZN25btSimulationIslandManagerC1Ev(%class.btSimulationIslandManager* %11)
  %m_islandManager = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 4
  store %class.btSimulationIslandManager* %11, %class.btSimulationIslandManager** %m_islandManager, align 4
  %m_ownsIslandManager = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 10
  store i8 1, i8* %m_ownsIslandManager, align 4
  %call21 = call i8* @_Z22btAlignedAllocInternalmi(i32 88, i32 16)
  store i8* %call21, i8** %mem20, align 4
  %12 = load i8*, i8** %mem20, align 4
  %13 = bitcast i8* %12 to %struct.InplaceSolverIslandCallback*
  %m_constraintSolver22 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 3
  %14 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver22, align 4
  %15 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %call23 = call %struct.InplaceSolverIslandCallback* @_ZN27InplaceSolverIslandCallbackC2EP18btConstraintSolverP12btStackAllocP12btDispatcher(%struct.InplaceSolverIslandCallback* %13, %class.btConstraintSolver* %14, %class.btStackAlloc* null, %class.btDispatcher* %15)
  %m_solverIslandCallback24 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 2
  store %struct.InplaceSolverIslandCallback* %13, %struct.InplaceSolverIslandCallback** %m_solverIslandCallback24, align 4
  %16 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %retval, align 4
  ret %class.btDiscreteDynamicsWorld* %16
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btDynamicsWorld* @_ZN15btDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration(%class.btDynamicsWorld* returned %this, %class.btDispatcher* %dispatcher, %class.btBroadphaseInterface* %broadphase, %class.btCollisionConfiguration* %collisionConfiguration) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %broadphase.addr = alloca %class.btBroadphaseInterface*, align 4
  %collisionConfiguration.addr = alloca %class.btCollisionConfiguration*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  store %class.btBroadphaseInterface* %broadphase, %class.btBroadphaseInterface** %broadphase.addr, align 4
  store %class.btCollisionConfiguration* %collisionConfiguration, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btDynamicsWorld* %this1 to %class.btCollisionWorld*
  %1 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %2 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %broadphase.addr, align 4
  %3 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4
  %call = call %class.btCollisionWorld* @_ZN16btCollisionWorldC2EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration(%class.btCollisionWorld* %0, %class.btDispatcher* %1, %class.btBroadphaseInterface* %2, %class.btCollisionConfiguration* %3)
  %4 = bitcast %class.btDynamicsWorld* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [37 x i8*] }, { [37 x i8*] }* @_ZTV15btDynamicsWorld, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %4, align 4
  %m_internalTickCallback = getelementptr inbounds %class.btDynamicsWorld, %class.btDynamicsWorld* %this1, i32 0, i32 1
  store void (%class.btDynamicsWorld*, float)* null, void (%class.btDynamicsWorld*, float)** %m_internalTickCallback, align 4
  %m_internalPreTickCallback = getelementptr inbounds %class.btDynamicsWorld, %class.btDynamicsWorld* %this1, i32 0, i32 2
  store void (%class.btDynamicsWorld*, float)* null, void (%class.btDynamicsWorld*, float)** %m_internalPreTickCallback, align 4
  %m_worldUserInfo = getelementptr inbounds %class.btDynamicsWorld, %class.btDynamicsWorld* %this1, i32 0, i32 3
  store i8* null, i8** %m_worldUserInfo, align 4
  %m_solverInfo = getelementptr inbounds %class.btDynamicsWorld, %class.btDynamicsWorld* %this1, i32 0, i32 4
  %call2 = call %struct.btContactSolverInfo* @_ZN19btContactSolverInfoC2Ev(%struct.btContactSolverInfo* %m_solverInfo)
  ret %class.btDynamicsWorld* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EEC2Ev(%class.btAlignedAllocator.5* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.19* @_ZN20btAlignedObjectArrayIP11btRigidBodyEC2Ev(%class.btAlignedObjectArray.19* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.19*, align 4
  store %class.btAlignedObjectArray.19* %this, %class.btAlignedObjectArray.19** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.19*, %class.btAlignedObjectArray.19** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.20* @_ZN18btAlignedAllocatorIP11btRigidBodyLj16EEC2Ev(%class.btAlignedAllocator.20* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP11btRigidBodyE4initEv(%class.btAlignedObjectArray.19* %this1)
  ret %class.btAlignedObjectArray.19* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.23* @_ZN20btAlignedObjectArrayIP17btActionInterfaceEC2Ev(%class.btAlignedObjectArray.23* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.23*, align 4
  store %class.btAlignedObjectArray.23* %this, %class.btAlignedObjectArray.23** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.23*, %class.btAlignedObjectArray.23** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.24* @_ZN18btAlignedAllocatorIP17btActionInterfaceLj16EEC2Ev(%class.btAlignedAllocator.24* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE4initEv(%class.btAlignedObjectArray.23* %this1)
  ret %class.btAlignedObjectArray.23* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.9* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.10* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.10* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.9* %this1)
  ret %class.btAlignedObjectArray.9* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btSpinMutex* @_ZN11btSpinMutexC2Ev(%class.btSpinMutex* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSpinMutex*, align 4
  store %class.btSpinMutex* %this, %class.btSpinMutex** %this.addr, align 4
  %this1 = load %class.btSpinMutex*, %class.btSpinMutex** %this.addr, align 4
  %mLock = getelementptr inbounds %class.btSpinMutex, %class.btSpinMutex* %this1, i32 0, i32 0
  store i32 0, i32* %mLock, align 4
  ret %class.btSpinMutex* %this1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN35btSequentialImpulseConstraintSolvernwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

declare %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverC1Ev(%class.btSequentialImpulseConstraintSolver* returned) unnamed_addr #3

declare %class.btSimulationIslandManager* @_ZN25btSimulationIslandManagerC1Ev(%class.btSimulationIslandManager* returned) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.InplaceSolverIslandCallback* @_ZN27InplaceSolverIslandCallbackC2EP18btConstraintSolverP12btStackAllocP12btDispatcher(%struct.InplaceSolverIslandCallback* returned %this, %class.btConstraintSolver* %solver, %class.btStackAlloc* %stackAlloc, %class.btDispatcher* %dispatcher) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.InplaceSolverIslandCallback*, align 4
  %solver.addr = alloca %class.btConstraintSolver*, align 4
  %stackAlloc.addr = alloca %class.btStackAlloc*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %struct.InplaceSolverIslandCallback* %this, %struct.InplaceSolverIslandCallback** %this.addr, align 4
  store %class.btConstraintSolver* %solver, %class.btConstraintSolver** %solver.addr, align 4
  store %class.btStackAlloc* %stackAlloc, %class.btStackAlloc** %stackAlloc.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %struct.InplaceSolverIslandCallback*, %struct.InplaceSolverIslandCallback** %this.addr, align 4
  %0 = bitcast %struct.InplaceSolverIslandCallback* %this1 to %"struct.btSimulationIslandManager::IslandCallback"*
  %call = call %"struct.btSimulationIslandManager::IslandCallback"* @_ZN25btSimulationIslandManager14IslandCallbackC2Ev(%"struct.btSimulationIslandManager::IslandCallback"* %0) #10
  %1 = bitcast %struct.InplaceSolverIslandCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV27InplaceSolverIslandCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_solverInfo = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 1
  store %struct.btContactSolverInfo* null, %struct.btContactSolverInfo** %m_solverInfo, align 4
  %m_solver = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 2
  %2 = load %class.btConstraintSolver*, %class.btConstraintSolver** %solver.addr, align 4
  store %class.btConstraintSolver* %2, %class.btConstraintSolver** %m_solver, align 4
  %m_sortedConstraints = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 3
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %m_sortedConstraints, align 4
  %m_numConstraints = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 4
  store i32 0, i32* %m_numConstraints, align 4
  %m_debugDrawer = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 5
  store %class.btIDebugDraw* null, %class.btIDebugDraw** %m_debugDrawer, align 4
  %m_dispatcher = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 6
  %3 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  store %class.btDispatcher* %3, %class.btDispatcher** %m_dispatcher, align 4
  %m_bodies = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 7
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev(%class.btAlignedObjectArray* %m_bodies)
  %m_manifolds = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 8
  %call3 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.9* %m_manifolds)
  %m_constraints = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 9
  %call4 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev(%class.btAlignedObjectArray.4* %m_constraints)
  ret %struct.InplaceSolverIslandCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldD2Ev(%class.btDiscreteDynamicsWorld* returned %this) unnamed_addr #1 {
entry:
  %retval = alloca %class.btDiscreteDynamicsWorld*, align 4
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btDiscreteDynamicsWorld* %this1, %class.btDiscreteDynamicsWorld** %retval, align 4
  %0 = bitcast %class.btDiscreteDynamicsWorld* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [49 x i8*] }, { [49 x i8*] }* @_ZTV23btDiscreteDynamicsWorld, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_ownsIslandManager = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 10
  %1 = load i8, i8* %m_ownsIslandManager, align 4
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_islandManager = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 4
  %2 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %m_islandManager, align 4
  %3 = bitcast %class.btSimulationIslandManager* %2 to %class.btSimulationIslandManager* (%class.btSimulationIslandManager*)***
  %vtable = load %class.btSimulationIslandManager* (%class.btSimulationIslandManager*)**, %class.btSimulationIslandManager* (%class.btSimulationIslandManager*)*** %3, align 4
  %vfn = getelementptr inbounds %class.btSimulationIslandManager* (%class.btSimulationIslandManager*)*, %class.btSimulationIslandManager* (%class.btSimulationIslandManager*)** %vtable, i64 0
  %4 = load %class.btSimulationIslandManager* (%class.btSimulationIslandManager*)*, %class.btSimulationIslandManager* (%class.btSimulationIslandManager*)** %vfn, align 4
  %call = call %class.btSimulationIslandManager* %4(%class.btSimulationIslandManager* %2) #10
  %m_islandManager2 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 4
  %5 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %m_islandManager2, align 4
  %6 = bitcast %class.btSimulationIslandManager* %5 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_solverIslandCallback = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 2
  %7 = load %struct.InplaceSolverIslandCallback*, %struct.InplaceSolverIslandCallback** %m_solverIslandCallback, align 4
  %tobool3 = icmp ne %struct.InplaceSolverIslandCallback* %7, null
  br i1 %tobool3, label %if.then4, label %if.end10

if.then4:                                         ; preds = %if.end
  %m_solverIslandCallback5 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 2
  %8 = load %struct.InplaceSolverIslandCallback*, %struct.InplaceSolverIslandCallback** %m_solverIslandCallback5, align 4
  %9 = bitcast %struct.InplaceSolverIslandCallback* %8 to %struct.InplaceSolverIslandCallback* (%struct.InplaceSolverIslandCallback*)***
  %vtable6 = load %struct.InplaceSolverIslandCallback* (%struct.InplaceSolverIslandCallback*)**, %struct.InplaceSolverIslandCallback* (%struct.InplaceSolverIslandCallback*)*** %9, align 4
  %vfn7 = getelementptr inbounds %struct.InplaceSolverIslandCallback* (%struct.InplaceSolverIslandCallback*)*, %struct.InplaceSolverIslandCallback* (%struct.InplaceSolverIslandCallback*)** %vtable6, i64 0
  %10 = load %struct.InplaceSolverIslandCallback* (%struct.InplaceSolverIslandCallback*)*, %struct.InplaceSolverIslandCallback* (%struct.InplaceSolverIslandCallback*)** %vfn7, align 4
  %call8 = call %struct.InplaceSolverIslandCallback* %10(%struct.InplaceSolverIslandCallback* %8) #10
  %m_solverIslandCallback9 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 2
  %11 = load %struct.InplaceSolverIslandCallback*, %struct.InplaceSolverIslandCallback** %m_solverIslandCallback9, align 4
  %12 = bitcast %struct.InplaceSolverIslandCallback* %11 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %12)
  br label %if.end10

if.end10:                                         ; preds = %if.then4, %if.end
  %m_ownsConstraintSolver = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 11
  %13 = load i8, i8* %m_ownsConstraintSolver, align 1
  %tobool11 = trunc i8 %13 to i1
  br i1 %tobool11, label %if.then12, label %if.end17

if.then12:                                        ; preds = %if.end10
  %m_constraintSolver = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 3
  %14 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver, align 4
  %15 = bitcast %class.btConstraintSolver* %14 to %class.btConstraintSolver* (%class.btConstraintSolver*)***
  %vtable13 = load %class.btConstraintSolver* (%class.btConstraintSolver*)**, %class.btConstraintSolver* (%class.btConstraintSolver*)*** %15, align 4
  %vfn14 = getelementptr inbounds %class.btConstraintSolver* (%class.btConstraintSolver*)*, %class.btConstraintSolver* (%class.btConstraintSolver*)** %vtable13, i64 0
  %16 = load %class.btConstraintSolver* (%class.btConstraintSolver*)*, %class.btConstraintSolver* (%class.btConstraintSolver*)** %vfn14, align 4
  %call15 = call %class.btConstraintSolver* %16(%class.btConstraintSolver* %14) #10
  %m_constraintSolver16 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 3
  %17 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver16, align 4
  %18 = bitcast %class.btConstraintSolver* %17 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %18)
  br label %if.end17

if.end17:                                         ; preds = %if.then12, %if.end10
  %m_predictiveManifolds = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 18
  %call18 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.9* %m_predictiveManifolds) #10
  %m_actions = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 14
  %call19 = call %class.btAlignedObjectArray.23* @_ZN20btAlignedObjectArrayIP17btActionInterfaceED2Ev(%class.btAlignedObjectArray.23* %m_actions) #10
  %m_nonStaticRigidBodies = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %call20 = call %class.btAlignedObjectArray.19* @_ZN20btAlignedObjectArrayIP11btRigidBodyED2Ev(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies) #10
  %m_constraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 5
  %call21 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray.4* %m_constraints) #10
  %m_sortedConstraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 1
  %call22 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray.4* %m_sortedConstraints) #10
  %19 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call23 = call %class.btDynamicsWorld* @_ZN15btDynamicsWorldD2Ev(%class.btDynamicsWorld* %19) #10
  %20 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %retval, align 4
  ret %class.btDiscreteDynamicsWorld* %20
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.9* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.9* %this1)
  ret %class.btAlignedObjectArray.9* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.23* @_ZN20btAlignedObjectArrayIP17btActionInterfaceED2Ev(%class.btAlignedObjectArray.23* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.23*, align 4
  store %class.btAlignedObjectArray.23* %this, %class.btAlignedObjectArray.23** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.23*, %class.btAlignedObjectArray.23** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE5clearEv(%class.btAlignedObjectArray.23* %this1)
  ret %class.btAlignedObjectArray.23* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.19* @_ZN20btAlignedObjectArrayIP11btRigidBodyED2Ev(%class.btAlignedObjectArray.19* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.19*, align 4
  store %class.btAlignedObjectArray.19* %this, %class.btAlignedObjectArray.19** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.19*, %class.btAlignedObjectArray.19** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP11btRigidBodyE5clearEv(%class.btAlignedObjectArray.19* %this1)
  ret %class.btAlignedObjectArray.19* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN23btDiscreteDynamicsWorldD0Ev(%class.btDiscreteDynamicsWorld* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %call = call %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldD1Ev(%class.btDiscreteDynamicsWorld* %this1) #10
  %0 = bitcast %class.btDiscreteDynamicsWorld* %this1 to i8*
  call void @_ZN23btDiscreteDynamicsWorlddlEPv(i8* %0) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN23btDiscreteDynamicsWorlddlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf(%class.btDiscreteDynamicsWorld* %this, float %timeStep) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %i = alloca i32, align 4
  %colObj = alloca %class.btCollisionObject*, align 4
  %body = alloca %class.btRigidBody*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_collisionObjects)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects2 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %2, i32 0, i32 1
  %3 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_collisionObjects2, i32 %3)
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %call3, align 4
  store %class.btCollisionObject* %4, %class.btCollisionObject** %colObj, align 4
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4
  %call4 = call %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %5)
  store %class.btRigidBody* %call4, %class.btRigidBody** %body, align 4
  %6 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %tobool = icmp ne %class.btRigidBody* %6, null
  br i1 %tobool, label %land.lhs.true, label %if.end9

land.lhs.true:                                    ; preds = %for.body
  %7 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %8 = bitcast %class.btRigidBody* %7 to %class.btCollisionObject*
  %call5 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %8)
  %cmp6 = icmp ne i32 %call5, 2
  br i1 %cmp6, label %if.then, label %if.end9

if.then:                                          ; preds = %land.lhs.true
  %9 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %10 = bitcast %class.btRigidBody* %9 to %class.btCollisionObject*
  %call7 = call zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %10)
  br i1 %call7, label %if.then8, label %if.end

if.then8:                                         ; preds = %if.then
  %11 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %12 = load float, float* %timeStep.addr, align 4
  call void @_ZN11btRigidBody18saveKinematicStateEf(%class.btRigidBody* %11, float %12)
  br label %if.end

if.end:                                           ; preds = %if.then8, %if.then
  br label %if.end9

if.end9:                                          ; preds = %if.end, %land.lhs.true, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end9
  %13 = load i32, i32* %i, align 4
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %0, i32 %1
  ret %class.btCollisionObject** %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %colObj) #2 comdat {
entry:
  %retval = alloca %class.btRigidBody*, align 4
  %colObj.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %colObj, %class.btCollisionObject** %colObj.addr, align 4
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4
  %call = call i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %0)
  %and = and i32 %call, 2
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4
  %2 = bitcast %class.btCollisionObject* %1 to %class.btRigidBody*
  store %class.btRigidBody* %2, %class.btRigidBody** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store %class.btRigidBody* null, %class.btRigidBody** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load %class.btRigidBody*, %class.btRigidBody** %retval, align 4
  ret %class.btRigidBody* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_activationState1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 16
  %0 = load i32, i32* %m_activationState1, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4
  %and = and i32 %0, 2
  %cmp = icmp ne i32 %and, 0
  ret i1 %cmp
}

declare void @_ZN11btRigidBody18saveKinematicStateEf(%class.btRigidBody*, float) #3

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv(%class.btDiscreteDynamicsWorld* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %drawConstraints = alloca i8, align 1
  %mode = alloca i32, align 4
  %i = alloca i32, align 4
  %constraint = alloca %class.btTypedConstraint*, align 4
  %i36 = alloca i32, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str, i32 0, i32 0))
  %0 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  call void @_ZN16btCollisionWorld14debugDrawWorldEv(%class.btCollisionWorld* %0)
  store i8 0, i8* %drawConstraints, align 1
  %1 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %2 = bitcast %class.btCollisionWorld* %1 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %2, align 4
  %vfn = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable, i64 5
  %3 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn, align 4
  %call2 = call %class.btIDebugDraw* %3(%class.btCollisionWorld* %1)
  %tobool = icmp ne %class.btIDebugDraw* %call2, null
  br i1 %tobool, label %if.then, label %if.end11

if.then:                                          ; preds = %entry
  %4 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %5 = bitcast %class.btCollisionWorld* %4 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable3 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %5, align 4
  %vfn4 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable3, i64 5
  %6 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn4, align 4
  %call5 = call %class.btIDebugDraw* %6(%class.btCollisionWorld* %4)
  %7 = bitcast %class.btIDebugDraw* %call5 to i32 (%class.btIDebugDraw*)***
  %vtable6 = load i32 (%class.btIDebugDraw*)**, i32 (%class.btIDebugDraw*)*** %7, align 4
  %vfn7 = getelementptr inbounds i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vtable6, i64 14
  %8 = load i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vfn7, align 4
  %call8 = call i32 %8(%class.btIDebugDraw* %call5)
  store i32 %call8, i32* %mode, align 4
  %9 = load i32, i32* %mode, align 4
  %and = and i32 %9, 6144
  %tobool9 = icmp ne i32 %and, 0
  br i1 %tobool9, label %if.then10, label %if.end

if.then10:                                        ; preds = %if.then
  store i8 1, i8* %drawConstraints, align 1
  br label %if.end

if.end:                                           ; preds = %if.then10, %if.then
  br label %if.end11

if.end11:                                         ; preds = %if.end, %entry
  %10 = load i8, i8* %drawConstraints, align 1
  %tobool12 = trunc i8 %10 to i1
  br i1 %tobool12, label %if.then13, label %if.end22

if.then13:                                        ; preds = %if.end11
  %11 = bitcast %class.btDiscreteDynamicsWorld* %this1 to i32 (%class.btDiscreteDynamicsWorld*)***
  %vtable14 = load i32 (%class.btDiscreteDynamicsWorld*)**, i32 (%class.btDiscreteDynamicsWorld*)*** %11, align 4
  %vfn15 = getelementptr inbounds i32 (%class.btDiscreteDynamicsWorld*)*, i32 (%class.btDiscreteDynamicsWorld*)** %vtable14, i64 26
  %12 = load i32 (%class.btDiscreteDynamicsWorld*)*, i32 (%class.btDiscreteDynamicsWorld*)** %vfn15, align 4
  %call16 = call i32 %12(%class.btDiscreteDynamicsWorld* %this1)
  %sub = sub nsw i32 %call16, 1
  store i32 %sub, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then13
  %13 = load i32, i32* %i, align 4
  %cmp = icmp sge i32 %13, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load i32, i32* %i, align 4
  %15 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btTypedConstraint* (%class.btDiscreteDynamicsWorld*, i32)***
  %vtable17 = load %class.btTypedConstraint* (%class.btDiscreteDynamicsWorld*, i32)**, %class.btTypedConstraint* (%class.btDiscreteDynamicsWorld*, i32)*** %15, align 4
  %vfn18 = getelementptr inbounds %class.btTypedConstraint* (%class.btDiscreteDynamicsWorld*, i32)*, %class.btTypedConstraint* (%class.btDiscreteDynamicsWorld*, i32)** %vtable17, i64 27
  %16 = load %class.btTypedConstraint* (%class.btDiscreteDynamicsWorld*, i32)*, %class.btTypedConstraint* (%class.btDiscreteDynamicsWorld*, i32)** %vfn18, align 4
  %call19 = call %class.btTypedConstraint* %16(%class.btDiscreteDynamicsWorld* %this1, i32 %14)
  store %class.btTypedConstraint* %call19, %class.btTypedConstraint** %constraint, align 4
  %17 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint, align 4
  %18 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*)***
  %vtable20 = load void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*)**, void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*)*** %18, align 4
  %vfn21 = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*)*, void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*)** %vtable20, i64 43
  %19 = load void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*)*, void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*)** %vfn21, align 4
  call void %19(%class.btDiscreteDynamicsWorld* %this1, %class.btTypedConstraint* %17)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %20 = load i32, i32* %i, align 4
  %dec = add nsw i32 %20, -1
  store i32 %dec, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end22

if.end22:                                         ; preds = %for.end, %if.end11
  %21 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %22 = bitcast %class.btCollisionWorld* %21 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable23 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %22, align 4
  %vfn24 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable23, i64 5
  %23 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn24, align 4
  %call25 = call %class.btIDebugDraw* %23(%class.btCollisionWorld* %21)
  %tobool26 = icmp ne %class.btIDebugDraw* %call25, null
  br i1 %tobool26, label %land.lhs.true, label %if.end61

land.lhs.true:                                    ; preds = %if.end22
  %24 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %25 = bitcast %class.btCollisionWorld* %24 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable27 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %25, align 4
  %vfn28 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable27, i64 5
  %26 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn28, align 4
  %call29 = call %class.btIDebugDraw* %26(%class.btCollisionWorld* %24)
  %27 = bitcast %class.btIDebugDraw* %call29 to i32 (%class.btIDebugDraw*)***
  %vtable30 = load i32 (%class.btIDebugDraw*)**, i32 (%class.btIDebugDraw*)*** %27, align 4
  %vfn31 = getelementptr inbounds i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vtable30, i64 14
  %28 = load i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vfn31, align 4
  %call32 = call i32 %28(%class.btIDebugDraw* %call29)
  %and33 = and i32 %call32, 16387
  %tobool34 = icmp ne i32 %and33, 0
  br i1 %tobool34, label %if.then35, label %if.end61

if.then35:                                        ; preds = %land.lhs.true
  %29 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %30 = bitcast %class.btCollisionWorld* %29 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable37 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %30, align 4
  %vfn38 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable37, i64 5
  %31 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn38, align 4
  %call39 = call %class.btIDebugDraw* %31(%class.btCollisionWorld* %29)
  %tobool40 = icmp ne %class.btIDebugDraw* %call39, null
  br i1 %tobool40, label %land.lhs.true41, label %if.end60

land.lhs.true41:                                  ; preds = %if.then35
  %32 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %33 = bitcast %class.btCollisionWorld* %32 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable42 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %33, align 4
  %vfn43 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable42, i64 5
  %34 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn43, align 4
  %call44 = call %class.btIDebugDraw* %34(%class.btCollisionWorld* %32)
  %35 = bitcast %class.btIDebugDraw* %call44 to i32 (%class.btIDebugDraw*)***
  %vtable45 = load i32 (%class.btIDebugDraw*)**, i32 (%class.btIDebugDraw*)*** %35, align 4
  %vfn46 = getelementptr inbounds i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vtable45, i64 14
  %36 = load i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vfn46, align 4
  %call47 = call i32 %36(%class.btIDebugDraw* %call44)
  %tobool48 = icmp ne i32 %call47, 0
  br i1 %tobool48, label %if.then49, label %if.end60

if.then49:                                        ; preds = %land.lhs.true41
  store i32 0, i32* %i36, align 4
  br label %for.cond50

for.cond50:                                       ; preds = %for.inc58, %if.then49
  %37 = load i32, i32* %i36, align 4
  %m_actions = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 14
  %call51 = call i32 @_ZNK20btAlignedObjectArrayIP17btActionInterfaceE4sizeEv(%class.btAlignedObjectArray.23* %m_actions)
  %cmp52 = icmp slt i32 %37, %call51
  br i1 %cmp52, label %for.body53, label %for.end59

for.body53:                                       ; preds = %for.cond50
  %m_actions54 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 14
  %38 = load i32, i32* %i36, align 4
  %call55 = call nonnull align 4 dereferenceable(4) %class.btActionInterface** @_ZN20btAlignedObjectArrayIP17btActionInterfaceEixEi(%class.btAlignedObjectArray.23* %m_actions54, i32 %38)
  %39 = load %class.btActionInterface*, %class.btActionInterface** %call55, align 4
  %40 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %40, i32 0, i32 5
  %41 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4
  %42 = bitcast %class.btActionInterface* %39 to void (%class.btActionInterface*, %class.btIDebugDraw*)***
  %vtable56 = load void (%class.btActionInterface*, %class.btIDebugDraw*)**, void (%class.btActionInterface*, %class.btIDebugDraw*)*** %42, align 4
  %vfn57 = getelementptr inbounds void (%class.btActionInterface*, %class.btIDebugDraw*)*, void (%class.btActionInterface*, %class.btIDebugDraw*)** %vtable56, i64 3
  %43 = load void (%class.btActionInterface*, %class.btIDebugDraw*)*, void (%class.btActionInterface*, %class.btIDebugDraw*)** %vfn57, align 4
  call void %43(%class.btActionInterface* %39, %class.btIDebugDraw* %41)
  br label %for.inc58

for.inc58:                                        ; preds = %for.body53
  %44 = load i32, i32* %i36, align 4
  %inc = add nsw i32 %44, 1
  store i32 %inc, i32* %i36, align 4
  br label %for.cond50

for.end59:                                        ; preds = %for.cond50
  br label %if.end60

if.end60:                                         ; preds = %for.end59, %land.lhs.true41, %if.then35
  br label %if.end61

if.end61:                                         ; preds = %if.end60, %land.lhs.true, %if.end22
  %45 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %46 = bitcast %class.btCollisionWorld* %45 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable62 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %46, align 4
  %vfn63 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable62, i64 5
  %47 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn63, align 4
  %call64 = call %class.btIDebugDraw* %47(%class.btCollisionWorld* %45)
  %tobool65 = icmp ne %class.btIDebugDraw* %call64, null
  br i1 %tobool65, label %if.then66, label %if.end72

if.then66:                                        ; preds = %if.end61
  %48 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %49 = bitcast %class.btCollisionWorld* %48 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable67 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %49, align 4
  %vfn68 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable67, i64 5
  %50 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn68, align 4
  %call69 = call %class.btIDebugDraw* %50(%class.btCollisionWorld* %48)
  %51 = bitcast %class.btIDebugDraw* %call69 to void (%class.btIDebugDraw*)***
  %vtable70 = load void (%class.btIDebugDraw*)**, void (%class.btIDebugDraw*)*** %51, align 4
  %vfn71 = getelementptr inbounds void (%class.btIDebugDraw*)*, void (%class.btIDebugDraw*)** %vtable70, i64 25
  %52 = load void (%class.btIDebugDraw*)*, void (%class.btIDebugDraw*)** %vfn71, align 4
  call void %52(%class.btIDebugDraw* %call69)
  br label %if.end72

if.end72:                                         ; preds = %if.then66, %if.end61
  %call73 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

declare %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* returned, i8*) unnamed_addr #3

declare void @_ZN16btCollisionWorld14debugDrawWorldEv(%class.btCollisionWorld*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btActionInterfaceE4sizeEv(%class.btAlignedObjectArray.23* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.23*, align 4
  store %class.btAlignedObjectArray.23* %this, %class.btAlignedObjectArray.23** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.23*, %class.btAlignedObjectArray.23** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btActionInterface** @_ZN20btAlignedObjectArrayIP17btActionInterfaceEixEi(%class.btAlignedObjectArray.23* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.23*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.23* %this, %class.btAlignedObjectArray.23** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.23*, %class.btAlignedObjectArray.23** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 4
  %0 = load %class.btActionInterface**, %class.btActionInterface*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btActionInterface*, %class.btActionInterface** %0, i32 %1
  ret %class.btActionInterface** %arrayidx
}

; Function Attrs: nounwind
declare %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* returned) unnamed_addr #4

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld11clearForcesEv(%class.btDiscreteDynamicsWorld* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %i = alloca i32, align 4
  %body = alloca %class.btRigidBody*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_nonStaticRigidBodies = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_nonStaticRigidBodies2 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %1 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btRigidBody** @_ZN20btAlignedObjectArrayIP11btRigidBodyEixEi(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies2, i32 %1)
  %2 = load %class.btRigidBody*, %class.btRigidBody** %call3, align 4
  store %class.btRigidBody* %2, %class.btRigidBody** %body, align 4
  %3 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  call void @_ZN11btRigidBody11clearForcesEv(%class.btRigidBody* %3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.19*, align 4
  store %class.btAlignedObjectArray.19* %this, %class.btAlignedObjectArray.19** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.19*, %class.btAlignedObjectArray.19** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btRigidBody** @_ZN20btAlignedObjectArrayIP11btRigidBodyEixEi(%class.btAlignedObjectArray.19* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.19*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.19* %this, %class.btAlignedObjectArray.19** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.19*, %class.btAlignedObjectArray.19** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 4
  %0 = load %class.btRigidBody**, %class.btRigidBody*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btRigidBody*, %class.btRigidBody** %0, i32 %1
  ret %class.btRigidBody** %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btRigidBody11clearForcesEv(%class.btRigidBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_totalForce = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 9
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_totalForce, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %m_totalTorque = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 10
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 0.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_totalTorque, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld12applyGravityEv(%class.btDiscreteDynamicsWorld* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %i = alloca i32, align 4
  %body = alloca %class.btRigidBody*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_nonStaticRigidBodies = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_nonStaticRigidBodies2 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %1 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btRigidBody** @_ZN20btAlignedObjectArrayIP11btRigidBodyEixEi(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies2, i32 %1)
  %2 = load %class.btRigidBody*, %class.btRigidBody** %call3, align 4
  store %class.btRigidBody* %2, %class.btRigidBody** %body, align 4
  %3 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %4 = bitcast %class.btRigidBody* %3 to %class.btCollisionObject*
  %call4 = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %4)
  br i1 %call4, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %5 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  call void @_ZN11btRigidBody12applyGravityEv(%class.btRigidBody* %5)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %call = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this1)
  %cmp = icmp ne i32 %call, 2
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %call2 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this1)
  %cmp3 = icmp ne i32 %call2, 5
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %0 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %0
}

declare void @_ZN11btRigidBody12applyGravityEv(%class.btRigidBody*) #3

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld28synchronizeSingleMotionStateEP11btRigidBody(%class.btDiscreteDynamicsWorld* %this, %class.btRigidBody* %body) #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %body.addr = alloca %class.btRigidBody*, align 4
  %interpolatedTransform = alloca %class.btTransform, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btRigidBody* %body, %class.btRigidBody** %body.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %call = call %class.btMotionState* @_ZN11btRigidBody14getMotionStateEv(%class.btRigidBody* %0)
  %tobool = icmp ne %class.btMotionState* %call, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %2 = bitcast %class.btRigidBody* %1 to %class.btCollisionObject*
  %call2 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %2)
  br i1 %call2, label %if.end, label %if.then

if.then:                                          ; preds = %land.lhs.true
  %call3 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %interpolatedTransform)
  %3 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %4 = bitcast %class.btRigidBody* %3 to %class.btCollisionObject*
  %call4 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject30getInterpolationWorldTransformEv(%class.btCollisionObject* %4)
  %5 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %6 = bitcast %class.btRigidBody* %5 to %class.btCollisionObject*
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK17btCollisionObject30getInterpolationLinearVelocityEv(%class.btCollisionObject* %6)
  %7 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %8 = bitcast %class.btRigidBody* %7 to %class.btCollisionObject*
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK17btCollisionObject31getInterpolationAngularVelocityEv(%class.btCollisionObject* %8)
  %m_latencyMotionStateInterpolation = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 16
  %9 = load i8, i8* %m_latencyMotionStateInterpolation, align 4
  %tobool7 = trunc i8 %9 to i1
  br i1 %tobool7, label %land.lhs.true8, label %cond.false

land.lhs.true8:                                   ; preds = %if.then
  %m_fixedTimeStep = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 9
  %10 = load float, float* %m_fixedTimeStep, align 4
  %tobool9 = fcmp une float %10, 0.000000e+00
  br i1 %tobool9, label %cond.true, label %cond.false

cond.true:                                        ; preds = %land.lhs.true8
  %m_localTime = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 8
  %11 = load float, float* %m_localTime, align 4
  %m_fixedTimeStep10 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 9
  %12 = load float, float* %m_fixedTimeStep10, align 4
  %sub = fsub float %11, %12
  br label %cond.end

cond.false:                                       ; preds = %land.lhs.true8, %if.then
  %m_localTime11 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 8
  %13 = load float, float* %m_localTime11, align 4
  %14 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %15 = bitcast %class.btRigidBody* %14 to %class.btCollisionObject*
  %call12 = call float @_ZNK17btCollisionObject14getHitFractionEv(%class.btCollisionObject* %15)
  %mul = fmul float %13, %call12
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %sub, %cond.true ], [ %mul, %cond.false ]
  call void @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_(%class.btTransform* nonnull align 4 dereferenceable(64) %call4, %class.btVector3* nonnull align 4 dereferenceable(16) %call5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6, float %cond, %class.btTransform* nonnull align 4 dereferenceable(64) %interpolatedTransform)
  %16 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %call13 = call %class.btMotionState* @_ZN11btRigidBody14getMotionStateEv(%class.btRigidBody* %16)
  %17 = bitcast %class.btMotionState* %call13 to void (%class.btMotionState*, %class.btTransform*)***
  %vtable = load void (%class.btMotionState*, %class.btTransform*)**, void (%class.btMotionState*, %class.btTransform*)*** %17, align 4
  %vfn = getelementptr inbounds void (%class.btMotionState*, %class.btTransform*)*, void (%class.btMotionState*, %class.btTransform*)** %vtable, i64 3
  %18 = load void (%class.btMotionState*, %class.btTransform*)*, void (%class.btMotionState*, %class.btTransform*)** %vfn, align 4
  call void %18(%class.btMotionState* %call13, %class.btTransform* nonnull align 4 dereferenceable(64) %interpolatedTransform)
  br label %if.end

if.end:                                           ; preds = %cond.end, %land.lhs.true, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btMotionState* @_ZN11btRigidBody14getMotionStateEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_optionalMotionState = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 20
  %0 = load %class.btMotionState*, %class.btMotionState** %m_optionalMotionState, align 4
  ret %class.btMotionState* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4
  %and = and i32 %0, 3
  %cmp = icmp ne i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_(%class.btTransform* nonnull align 4 dereferenceable(64) %curTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %linvel, %class.btVector3* nonnull align 4 dereferenceable(16) %angvel, float %timeStep, %class.btTransform* nonnull align 4 dereferenceable(64) %predictedTransform) #2 comdat {
entry:
  %curTrans.addr = alloca %class.btTransform*, align 4
  %linvel.addr = alloca %class.btVector3*, align 4
  %angvel.addr = alloca %class.btVector3*, align 4
  %timeStep.addr = alloca float, align 4
  %predictedTransform.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %axis = alloca %class.btVector3, align 4
  %fAngle = alloca float, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca float, align 4
  %dorn = alloca %class.btQuaternion, align 4
  %ref.tmp24 = alloca float, align 4
  %orn0 = alloca %class.btQuaternion, align 4
  %predictedOrn = alloca %class.btQuaternion, align 4
  store %class.btTransform* %curTrans, %class.btTransform** %curTrans.addr, align 4
  store %class.btVector3* %linvel, %class.btVector3** %linvel.addr, align 4
  store %class.btVector3* %angvel, %class.btVector3** %angvel.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  store %class.btTransform* %predictedTransform, %class.btTransform** %predictedTransform.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %predictedTransform.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %curTrans.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %1)
  %2 = load %class.btVector3*, %class.btVector3** %linvel.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis)
  %3 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4
  %call3 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %3)
  store float %call3, float* %fAngle, align 4
  %4 = load float, float* %fAngle, align 4
  %5 = load float, float* %timeStep.addr, align 4
  %mul = fmul float %4, %5
  %cmp = fcmp ogt float %mul, 0x3FE921FB60000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load float, float* %timeStep.addr, align 4
  %div = fdiv float 0x3FE921FB60000000, %6
  store float %div, float* %fAngle, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load float, float* %fAngle, align 4
  %cmp4 = fcmp olt float %7, 0x3F50624DE0000000
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %8 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4
  %9 = load float, float* %timeStep.addr, align 4
  %mul8 = fmul float 5.000000e-01, %9
  %10 = load float, float* %timeStep.addr, align 4
  %11 = load float, float* %timeStep.addr, align 4
  %mul9 = fmul float %10, %11
  %12 = load float, float* %timeStep.addr, align 4
  %mul10 = fmul float %mul9, %12
  %mul11 = fmul float %mul10, 0x3F95555560000000
  %13 = load float, float* %fAngle, align 4
  %mul12 = fmul float %mul11, %13
  %14 = load float, float* %fAngle, align 4
  %mul13 = fmul float %mul12, %14
  %sub = fsub float %mul8, %mul13
  store float %sub, float* %ref.tmp7, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %8, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %15 = bitcast %class.btVector3* %axis to i8*
  %16 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false)
  br label %if.end20

if.else:                                          ; preds = %if.end
  %17 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4
  %18 = load float, float* %fAngle, align 4
  %mul16 = fmul float 5.000000e-01, %18
  %19 = load float, float* %timeStep.addr, align 4
  %mul17 = fmul float %mul16, %19
  %call18 = call float @_Z5btSinf(float %mul17)
  %20 = load float, float* %fAngle, align 4
  %div19 = fdiv float %call18, %20
  store float %div19, float* %ref.tmp15, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %17, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %21 = bitcast %class.btVector3* %axis to i8*
  %22 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false)
  br label %if.end20

if.end20:                                         ; preds = %if.else, %if.then5
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %axis)
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %axis)
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %axis)
  %23 = load float, float* %fAngle, align 4
  %24 = load float, float* %timeStep.addr, align 4
  %mul25 = fmul float %23, %24
  %mul26 = fmul float %mul25, 5.000000e-01
  %call27 = call float @_Z5btCosf(float %mul26)
  store float %call27, float* %ref.tmp24, align 4
  %call28 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %dorn, float* nonnull align 4 dereferenceable(4) %call21, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call23, float* nonnull align 4 dereferenceable(4) %ref.tmp24)
  %25 = load %class.btTransform*, %class.btTransform** %curTrans.addr, align 4
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %orn0, %class.btTransform* %25)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %predictedOrn, %class.btQuaternion* nonnull align 4 dereferenceable(16) %dorn, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn0)
  %call29 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %predictedOrn)
  %26 = load %class.btTransform*, %class.btTransform** %predictedTransform.addr, align 4
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %26, %class.btQuaternion* nonnull align 4 dereferenceable(16) %predictedOrn)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject30getInterpolationWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_interpolationWorldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 2
  ret %class.btTransform* %m_interpolationWorldTransform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK17btCollisionObject30getInterpolationLinearVelocityEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_interpolationLinearVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 3
  ret %class.btVector3* %m_interpolationLinearVelocity
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK17btCollisionObject31getInterpolationAngularVelocityEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_interpolationAngularVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 4
  ret %class.btVector3* %m_interpolationAngularVelocity
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK17btCollisionObject14getHitFractionEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_hitFraction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 28
  %0 = load float, float* %m_hitFraction, align 4
  ret float %0
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv(%class.btDiscreteDynamicsWorld* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %i = alloca i32, align 4
  %colObj = alloca %class.btCollisionObject*, align 4
  %body = alloca %class.btRigidBody*, align 4
  %i8 = alloca i32, align 4
  %body13 = alloca %class.btRigidBody*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.1, i32 0, i32 0))
  %m_synchronizeAllMotionStates = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 12
  %0 = load i8, i8* %m_synchronizeAllMotionStates, align 2
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %1 = load i32, i32* %i, align 4
  %2 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %2, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_collisionObjects)
  %cmp = icmp slt i32 %1, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects3 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %3, i32 0, i32 1
  %4 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_collisionObjects3, i32 %4)
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %call4, align 4
  store %class.btCollisionObject* %5, %class.btCollisionObject** %colObj, align 4
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4
  %call5 = call %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %6)
  store %class.btRigidBody* %call5, %class.btRigidBody** %body, align 4
  %7 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %tobool6 = icmp ne %class.btRigidBody* %7, null
  br i1 %tobool6, label %if.then7, label %if.end

if.then7:                                         ; preds = %for.body
  %8 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  call void @_ZN23btDiscreteDynamicsWorld28synchronizeSingleMotionStateEP11btRigidBody(%class.btDiscreteDynamicsWorld* %this1, %class.btRigidBody* %8)
  br label %if.end

if.end:                                           ; preds = %if.then7, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end22

if.else:                                          ; preds = %entry
  store i32 0, i32* %i8, align 4
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc19, %if.else
  %10 = load i32, i32* %i8, align 4
  %m_nonStaticRigidBodies = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %call10 = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies)
  %cmp11 = icmp slt i32 %10, %call10
  br i1 %cmp11, label %for.body12, label %for.end21

for.body12:                                       ; preds = %for.cond9
  %m_nonStaticRigidBodies14 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %11 = load i32, i32* %i8, align 4
  %call15 = call nonnull align 4 dereferenceable(4) %class.btRigidBody** @_ZN20btAlignedObjectArrayIP11btRigidBodyEixEi(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies14, i32 %11)
  %12 = load %class.btRigidBody*, %class.btRigidBody** %call15, align 4
  store %class.btRigidBody* %12, %class.btRigidBody** %body13, align 4
  %13 = load %class.btRigidBody*, %class.btRigidBody** %body13, align 4
  %14 = bitcast %class.btRigidBody* %13 to %class.btCollisionObject*
  %call16 = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %14)
  br i1 %call16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %for.body12
  %15 = load %class.btRigidBody*, %class.btRigidBody** %body13, align 4
  call void @_ZN23btDiscreteDynamicsWorld28synchronizeSingleMotionStateEP11btRigidBody(%class.btDiscreteDynamicsWorld* %this1, %class.btRigidBody* %15)
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %for.body12
  br label %for.inc19

for.inc19:                                        ; preds = %if.end18
  %16 = load i32, i32* %i8, align 4
  %inc20 = add nsw i32 %16, 1
  store i32 %inc20, i32* %i8, align 4
  br label %for.cond9

for.end21:                                        ; preds = %for.cond9
  br label %if.end22

if.end22:                                         ; preds = %for.end21, %for.end
  %call23 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN23btDiscreteDynamicsWorld14stepSimulationEfif(%class.btDiscreteDynamicsWorld* %this, float %timeStep, i32 %maxSubSteps, float %fixedTimeStep) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %maxSubSteps.addr = alloca i32, align 4
  %fixedTimeStep.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %numSimulationSubSteps = alloca i32, align 4
  %debugDrawer = alloca %class.btIDebugDraw*, align 4
  %clampedSimulationSteps = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  store i32 %maxSubSteps, i32* %maxSubSteps.addr, align 4
  store float %fixedTimeStep, float* %fixedTimeStep.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = load float, float* %timeStep.addr, align 4
  call void @_ZN23btDiscreteDynamicsWorld14startProfilingEf(%class.btDiscreteDynamicsWorld* %this1, float %0)
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i32 0, i32 0))
  store i32 0, i32* %numSimulationSubSteps, align 4
  %1 = load i32, i32* %maxSubSteps.addr, align 4
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load float, float* %fixedTimeStep.addr, align 4
  %m_fixedTimeStep = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 9
  store float %2, float* %m_fixedTimeStep, align 4
  %3 = load float, float* %timeStep.addr, align 4
  %m_localTime = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 8
  %4 = load float, float* %m_localTime, align 4
  %add = fadd float %4, %3
  store float %add, float* %m_localTime, align 4
  %m_localTime2 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 8
  %5 = load float, float* %m_localTime2, align 4
  %6 = load float, float* %fixedTimeStep.addr, align 4
  %cmp = fcmp oge float %5, %6
  br i1 %cmp, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_localTime4 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 8
  %7 = load float, float* %m_localTime4, align 4
  %8 = load float, float* %fixedTimeStep.addr, align 4
  %div = fdiv float %7, %8
  %conv = fptosi float %div to i32
  store i32 %conv, i32* %numSimulationSubSteps, align 4
  %9 = load i32, i32* %numSimulationSubSteps, align 4
  %conv5 = sitofp i32 %9 to float
  %10 = load float, float* %fixedTimeStep.addr, align 4
  %mul = fmul float %conv5, %10
  %m_localTime6 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 8
  %11 = load float, float* %m_localTime6, align 4
  %sub = fsub float %11, %mul
  store float %sub, float* %m_localTime6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  br label %if.end14

if.else:                                          ; preds = %entry
  %12 = load float, float* %timeStep.addr, align 4
  store float %12, float* %fixedTimeStep.addr, align 4
  %m_latencyMotionStateInterpolation = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 16
  %13 = load i8, i8* %m_latencyMotionStateInterpolation, align 4
  %tobool7 = trunc i8 %13 to i1
  br i1 %tobool7, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %14 = load float, float* %timeStep.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ 0.000000e+00, %cond.true ], [ %14, %cond.false ]
  %m_localTime8 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 8
  store float %cond, float* %m_localTime8, align 4
  %m_fixedTimeStep9 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 9
  store float 0.000000e+00, float* %m_fixedTimeStep9, align 4
  %15 = load float, float* %timeStep.addr, align 4
  %call10 = call zeroext i1 @_Z11btFuzzyZerof(float %15)
  br i1 %call10, label %if.then11, label %if.else12

if.then11:                                        ; preds = %cond.end
  store i32 0, i32* %numSimulationSubSteps, align 4
  store i32 0, i32* %maxSubSteps.addr, align 4
  br label %if.end13

if.else12:                                        ; preds = %cond.end
  store i32 1, i32* %numSimulationSubSteps, align 4
  store i32 1, i32* %maxSubSteps.addr, align 4
  br label %if.end13

if.end13:                                         ; preds = %if.else12, %if.then11
  br label %if.end14

if.end14:                                         ; preds = %if.end13, %if.end
  %16 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %17 = bitcast %class.btCollisionWorld* %16 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %17, align 4
  %vfn = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable, i64 5
  %18 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn, align 4
  %call15 = call %class.btIDebugDraw* %18(%class.btCollisionWorld* %16)
  %tobool16 = icmp ne %class.btIDebugDraw* %call15, null
  br i1 %tobool16, label %if.then17, label %if.end25

if.then17:                                        ; preds = %if.end14
  %19 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %20 = bitcast %class.btCollisionWorld* %19 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable18 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %20, align 4
  %vfn19 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable18, i64 5
  %21 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn19, align 4
  %call20 = call %class.btIDebugDraw* %21(%class.btCollisionWorld* %19)
  store %class.btIDebugDraw* %call20, %class.btIDebugDraw** %debugDrawer, align 4
  %22 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer, align 4
  %23 = bitcast %class.btIDebugDraw* %22 to i32 (%class.btIDebugDraw*)***
  %vtable21 = load i32 (%class.btIDebugDraw*)**, i32 (%class.btIDebugDraw*)*** %23, align 4
  %vfn22 = getelementptr inbounds i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vtable21, i64 14
  %24 = load i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vfn22, align 4
  %call23 = call i32 %24(%class.btIDebugDraw* %22)
  %and = and i32 %call23, 16
  %cmp24 = icmp ne i32 %and, 0
  %frombool = zext i1 %cmp24 to i8
  store i8 %frombool, i8* @gDisableDeactivation, align 1
  br label %if.end25

if.end25:                                         ; preds = %if.then17, %if.end14
  %25 = load i32, i32* %numSimulationSubSteps, align 4
  %tobool26 = icmp ne i32 %25, 0
  br i1 %tobool26, label %if.then27, label %if.else44

if.then27:                                        ; preds = %if.end25
  %26 = load i32, i32* %numSimulationSubSteps, align 4
  %27 = load i32, i32* %maxSubSteps.addr, align 4
  %cmp28 = icmp sgt i32 %26, %27
  br i1 %cmp28, label %cond.true29, label %cond.false30

cond.true29:                                      ; preds = %if.then27
  %28 = load i32, i32* %maxSubSteps.addr, align 4
  br label %cond.end31

cond.false30:                                     ; preds = %if.then27
  %29 = load i32, i32* %numSimulationSubSteps, align 4
  br label %cond.end31

cond.end31:                                       ; preds = %cond.false30, %cond.true29
  %cond32 = phi i32 [ %28, %cond.true29 ], [ %29, %cond.false30 ]
  store i32 %cond32, i32* %clampedSimulationSteps, align 4
  %30 = load float, float* %fixedTimeStep.addr, align 4
  %31 = load i32, i32* %clampedSimulationSteps, align 4
  %conv33 = sitofp i32 %31 to float
  %mul34 = fmul float %30, %conv33
  %32 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*, float)***
  %vtable35 = load void (%class.btDiscreteDynamicsWorld*, float)**, void (%class.btDiscreteDynamicsWorld*, float)*** %32, align 4
  %vfn36 = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*, float)*, void (%class.btDiscreteDynamicsWorld*, float)** %vtable35, i64 42
  %33 = load void (%class.btDiscreteDynamicsWorld*, float)*, void (%class.btDiscreteDynamicsWorld*, float)** %vfn36, align 4
  call void %33(%class.btDiscreteDynamicsWorld* %this1, float %mul34)
  %34 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*)***
  %vtable37 = load void (%class.btDiscreteDynamicsWorld*)**, void (%class.btDiscreteDynamicsWorld*)*** %34, align 4
  %vfn38 = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*)*, void (%class.btDiscreteDynamicsWorld*)** %vtable37, i64 44
  %35 = load void (%class.btDiscreteDynamicsWorld*)*, void (%class.btDiscreteDynamicsWorld*)** %vfn38, align 4
  call void %35(%class.btDiscreteDynamicsWorld* %this1)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end31
  %36 = load i32, i32* %i, align 4
  %37 = load i32, i32* %clampedSimulationSteps, align 4
  %cmp39 = icmp slt i32 %36, %37
  br i1 %cmp39, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %38 = load float, float* %fixedTimeStep.addr, align 4
  %39 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*, float)***
  %vtable40 = load void (%class.btDiscreteDynamicsWorld*, float)**, void (%class.btDiscreteDynamicsWorld*, float)*** %39, align 4
  %vfn41 = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*, float)*, void (%class.btDiscreteDynamicsWorld*, float)** %vtable40, i64 40
  %40 = load void (%class.btDiscreteDynamicsWorld*, float)*, void (%class.btDiscreteDynamicsWorld*, float)** %vfn41, align 4
  call void %40(%class.btDiscreteDynamicsWorld* %this1, float %38)
  %41 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*)***
  %vtable42 = load void (%class.btDiscreteDynamicsWorld*)**, void (%class.btDiscreteDynamicsWorld*)*** %41, align 4
  %vfn43 = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*)*, void (%class.btDiscreteDynamicsWorld*)** %vtable42, i64 20
  %42 = load void (%class.btDiscreteDynamicsWorld*)*, void (%class.btDiscreteDynamicsWorld*)** %vfn43, align 4
  call void %42(%class.btDiscreteDynamicsWorld* %this1)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %43 = load i32, i32* %i, align 4
  %inc = add nsw i32 %43, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end47

if.else44:                                        ; preds = %if.end25
  %44 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*)***
  %vtable45 = load void (%class.btDiscreteDynamicsWorld*)**, void (%class.btDiscreteDynamicsWorld*)*** %44, align 4
  %vfn46 = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*)*, void (%class.btDiscreteDynamicsWorld*)** %vtable45, i64 20
  %45 = load void (%class.btDiscreteDynamicsWorld*)*, void (%class.btDiscreteDynamicsWorld*)** %vfn46, align 4
  call void %45(%class.btDiscreteDynamicsWorld* %this1)
  br label %if.end47

if.end47:                                         ; preds = %if.else44, %for.end
  %46 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*)***
  %vtable48 = load void (%class.btDiscreteDynamicsWorld*)**, void (%class.btDiscreteDynamicsWorld*)*** %46, align 4
  %vfn49 = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*)*, void (%class.btDiscreteDynamicsWorld*)** %vtable48, i64 30
  %47 = load void (%class.btDiscreteDynamicsWorld*)*, void (%class.btDiscreteDynamicsWorld*)** %vfn49, align 4
  call void %47(%class.btDiscreteDynamicsWorld* %this1)
  call void @_ZN15CProfileManager23Increment_Frame_CounterEv()
  %48 = load i32, i32* %numSimulationSubSteps, align 4
  %call50 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret i32 %48
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld14startProfilingEf(%class.btDiscreteDynamicsWorld* %this, float %timeStep) #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  call void @_ZN15CProfileManager5ResetEv()
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_Z11btFuzzyZerof(float %x) #2 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %call = call float @_Z6btFabsf(float %0)
  %cmp = fcmp olt float %call, 0x3E80000000000000
  ret i1 %cmp
}

declare void @_ZN15CProfileManager23Increment_Frame_CounterEv() #3

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf(%class.btDiscreteDynamicsWorld* %this, float %timeStep) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %dispatchInfo = alloca %struct.btDispatcherInfo*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.3, i32 0, i32 0))
  %0 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %m_internalPreTickCallback = getelementptr inbounds %class.btDynamicsWorld, %class.btDynamicsWorld* %0, i32 0, i32 2
  %1 = load void (%class.btDynamicsWorld*, float)*, void (%class.btDynamicsWorld*, float)** %m_internalPreTickCallback, align 4
  %cmp = icmp ne void (%class.btDynamicsWorld*, float)* null, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %m_internalPreTickCallback2 = getelementptr inbounds %class.btDynamicsWorld, %class.btDynamicsWorld* %2, i32 0, i32 2
  %3 = load void (%class.btDynamicsWorld*, float)*, void (%class.btDynamicsWorld*, float)** %m_internalPreTickCallback2, align 4
  %4 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %5 = load float, float* %timeStep.addr, align 4
  call void %3(%class.btDynamicsWorld* %4, float %5)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = load float, float* %timeStep.addr, align 4
  %7 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*, float)***
  %vtable = load void (%class.btDiscreteDynamicsWorld*, float)**, void (%class.btDiscreteDynamicsWorld*, float)*** %7, align 4
  %vfn = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*, float)*, void (%class.btDiscreteDynamicsWorld*, float)** %vtable, i64 35
  %8 = load void (%class.btDiscreteDynamicsWorld*, float)*, void (%class.btDiscreteDynamicsWorld*, float)** %vfn, align 4
  call void %8(%class.btDiscreteDynamicsWorld* %this1, float %6)
  %9 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %call3 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %9)
  store %struct.btDispatcherInfo* %call3, %struct.btDispatcherInfo** %dispatchInfo, align 4
  %10 = load float, float* %timeStep.addr, align 4
  %11 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo, align 4
  %m_timeStep = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %11, i32 0, i32 0
  store float %10, float* %m_timeStep, align 4
  %12 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo, align 4
  %m_stepCount = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %12, i32 0, i32 1
  store i32 0, i32* %m_stepCount, align 4
  %13 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %14 = bitcast %class.btCollisionWorld* %13 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable4 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %14, align 4
  %vfn5 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable4, i64 5
  %15 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn5, align 4
  %call6 = call %class.btIDebugDraw* %15(%class.btCollisionWorld* %13)
  %16 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo, align 4
  %m_debugDraw = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %16, i32 0, i32 5
  store %class.btIDebugDraw* %call6, %class.btIDebugDraw** %m_debugDraw, align 4
  %17 = load float, float* %timeStep.addr, align 4
  %18 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*, float)***
  %vtable7 = load void (%class.btDiscreteDynamicsWorld*, float)**, void (%class.btDiscreteDynamicsWorld*, float)*** %18, align 4
  %vfn8 = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*, float)*, void (%class.btDiscreteDynamicsWorld*, float)** %vtable7, i64 41
  %19 = load void (%class.btDiscreteDynamicsWorld*, float)*, void (%class.btDiscreteDynamicsWorld*, float)** %vfn8, align 4
  call void %19(%class.btDiscreteDynamicsWorld* %this1, float %17)
  %20 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %21 = bitcast %class.btCollisionWorld* %20 to void (%class.btCollisionWorld*)***
  %vtable9 = load void (%class.btCollisionWorld*)**, void (%class.btCollisionWorld*)*** %21, align 4
  %vfn10 = getelementptr inbounds void (%class.btCollisionWorld*)*, void (%class.btCollisionWorld*)** %vtable9, i64 11
  %22 = load void (%class.btCollisionWorld*)*, void (%class.btCollisionWorld*)** %vfn10, align 4
  call void %22(%class.btCollisionWorld* %20)
  %23 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*)***
  %vtable11 = load void (%class.btDiscreteDynamicsWorld*)**, void (%class.btDiscreteDynamicsWorld*)*** %23, align 4
  %vfn12 = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*)*, void (%class.btDiscreteDynamicsWorld*)** %vtable11, i64 37
  %24 = load void (%class.btDiscreteDynamicsWorld*)*, void (%class.btDiscreteDynamicsWorld*)** %vfn12, align 4
  call void %24(%class.btDiscreteDynamicsWorld* %this1)
  %25 = load float, float* %timeStep.addr, align 4
  %26 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call13 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %26)
  %27 = bitcast %struct.btContactSolverInfo* %call13 to %struct.btContactSolverInfoData*
  %m_timeStep14 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %27, i32 0, i32 3
  store float %25, float* %m_timeStep14, align 4
  %28 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call15 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %28)
  %29 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*, %struct.btContactSolverInfo*)***
  %vtable16 = load void (%class.btDiscreteDynamicsWorld*, %struct.btContactSolverInfo*)**, void (%class.btDiscreteDynamicsWorld*, %struct.btContactSolverInfo*)*** %29, align 4
  %vfn17 = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*, %struct.btContactSolverInfo*)*, void (%class.btDiscreteDynamicsWorld*, %struct.btContactSolverInfo*)** %vtable16, i64 38
  %30 = load void (%class.btDiscreteDynamicsWorld*, %struct.btContactSolverInfo*)*, void (%class.btDiscreteDynamicsWorld*, %struct.btContactSolverInfo*)** %vfn17, align 4
  call void %30(%class.btDiscreteDynamicsWorld* %this1, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %call15)
  %31 = load float, float* %timeStep.addr, align 4
  %32 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*, float)***
  %vtable18 = load void (%class.btDiscreteDynamicsWorld*, float)**, void (%class.btDiscreteDynamicsWorld*, float)*** %32, align 4
  %vfn19 = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*, float)*, void (%class.btDiscreteDynamicsWorld*, float)** %vtable18, i64 36
  %33 = load void (%class.btDiscreteDynamicsWorld*, float)*, void (%class.btDiscreteDynamicsWorld*, float)** %vfn19, align 4
  call void %33(%class.btDiscreteDynamicsWorld* %this1, float %31)
  %34 = load float, float* %timeStep.addr, align 4
  call void @_ZN23btDiscreteDynamicsWorld13updateActionsEf(%class.btDiscreteDynamicsWorld* %this1, float %34)
  %35 = load float, float* %timeStep.addr, align 4
  %36 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*, float)***
  %vtable20 = load void (%class.btDiscreteDynamicsWorld*, float)**, void (%class.btDiscreteDynamicsWorld*, float)*** %36, align 4
  %vfn21 = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*, float)*, void (%class.btDiscreteDynamicsWorld*, float)** %vtable20, i64 39
  %37 = load void (%class.btDiscreteDynamicsWorld*, float)*, void (%class.btDiscreteDynamicsWorld*, float)** %vfn21, align 4
  call void %37(%class.btDiscreteDynamicsWorld* %this1, float %35)
  %38 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %m_internalTickCallback = getelementptr inbounds %class.btDynamicsWorld, %class.btDynamicsWorld* %38, i32 0, i32 1
  %39 = load void (%class.btDynamicsWorld*, float)*, void (%class.btDynamicsWorld*, float)** %m_internalTickCallback, align 4
  %cmp22 = icmp ne void (%class.btDynamicsWorld*, float)* null, %39
  br i1 %cmp22, label %if.then23, label %if.end25

if.then23:                                        ; preds = %if.end
  %40 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %m_internalTickCallback24 = getelementptr inbounds %class.btDynamicsWorld, %class.btDynamicsWorld* %40, i32 0, i32 1
  %41 = load void (%class.btDynamicsWorld*, float)*, void (%class.btDynamicsWorld*, float)** %m_internalTickCallback24, align 4
  %42 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %43 = load float, float* %timeStep.addr, align 4
  call void %41(%class.btDynamicsWorld* %42, float %43)
  br label %if.end25

if.end25:                                         ; preds = %if.then23, %if.end
  %call26 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_dispatchInfo = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 3
  ret %struct.btDispatcherInfo* %m_dispatchInfo
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  %m_solverInfo = getelementptr inbounds %class.btDynamicsWorld, %class.btDynamicsWorld* %this1, i32 0, i32 4
  ret %struct.btContactSolverInfo* %m_solverInfo
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld13updateActionsEf(%class.btDiscreteDynamicsWorld* %this, float %timeStep) #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %i = alloca i32, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.4, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_actions = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 14
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btActionInterfaceE4sizeEv(%class.btAlignedObjectArray.23* %m_actions)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_actions3 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 14
  %1 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %class.btActionInterface** @_ZN20btAlignedObjectArrayIP17btActionInterfaceEixEi(%class.btAlignedObjectArray.23* %m_actions3, i32 %1)
  %2 = load %class.btActionInterface*, %class.btActionInterface** %call4, align 4
  %3 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %4 = load float, float* %timeStep.addr, align 4
  %5 = bitcast %class.btActionInterface* %2 to void (%class.btActionInterface*, %class.btCollisionWorld*, float)***
  %vtable = load void (%class.btActionInterface*, %class.btCollisionWorld*, float)**, void (%class.btActionInterface*, %class.btCollisionWorld*, float)*** %5, align 4
  %vfn = getelementptr inbounds void (%class.btActionInterface*, %class.btCollisionWorld*, float)*, void (%class.btActionInterface*, %class.btCollisionWorld*, float)** %vtable, i64 2
  %6 = load void (%class.btActionInterface*, %class.btCollisionWorld*, float)*, void (%class.btActionInterface*, %class.btCollisionWorld*, float)** %vfn, align 4
  call void %6(%class.btActionInterface* %2, %class.btCollisionWorld* %3, float %4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call5 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3(%class.btDiscreteDynamicsWorld* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %gravity) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %gravity.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  %body = alloca %class.btRigidBody*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btVector3* %gravity, %class.btVector3** %gravity.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %gravity.addr, align 4
  %m_gravity = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 7
  %1 = bitcast %class.btVector3* %m_gravity to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4
  %m_nonStaticRigidBodies = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies)
  %cmp = icmp slt i32 %3, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_nonStaticRigidBodies2 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %4 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btRigidBody** @_ZN20btAlignedObjectArrayIP11btRigidBodyEixEi(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies2, i32 %4)
  %5 = load %class.btRigidBody*, %class.btRigidBody** %call3, align 4
  store %class.btRigidBody* %5, %class.btRigidBody** %body, align 4
  %6 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %7 = bitcast %class.btRigidBody* %6 to %class.btCollisionObject*
  %call4 = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %7)
  br i1 %call4, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %8 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %call5 = call i32 @_ZNK11btRigidBody8getFlagsEv(%class.btRigidBody* %8)
  %and = and i32 %call5, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %land.lhs.true
  %9 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %10 = load %class.btVector3*, %class.btVector3** %gravity.addr, align 4
  call void @_ZN11btRigidBody10setGravityERK9btVector3(%class.btRigidBody* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %10)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK11btRigidBody8getFlagsEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_rigidbodyFlags = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 22
  %0 = load i32, i32* %m_rigidbodyFlags, align 4
  ret i32 %0
}

declare void @_ZN11btRigidBody10setGravityERK9btVector3(%class.btRigidBody*, %class.btVector3* nonnull align 4 dereferenceable(16)) #3

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK23btDiscreteDynamicsWorld10getGravityEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btDiscreteDynamicsWorld* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %m_gravity = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 7
  %0 = bitcast %class.btVector3* %agg.result to i8*
  %1 = bitcast %class.btVector3* %m_gravity to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectii(%class.btDiscreteDynamicsWorld* %this, %class.btCollisionObject* %collisionObject, i32 %collisionFilterGroup, i32 %collisionFilterMask) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %collisionObject.addr = alloca %class.btCollisionObject*, align 4
  %collisionFilterGroup.addr = alloca i32, align 4
  %collisionFilterMask.addr = alloca i32, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btCollisionObject* %collisionObject, %class.btCollisionObject** %collisionObject.addr, align 4
  store i32 %collisionFilterGroup, i32* %collisionFilterGroup.addr, align 4
  store i32 %collisionFilterMask, i32* %collisionFilterMask.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4
  %2 = load i32, i32* %collisionFilterGroup.addr, align 4
  %3 = load i32, i32* %collisionFilterMask.addr, align 4
  call void @_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectii(%class.btCollisionWorld* %0, %class.btCollisionObject* %1, i32 %2, i32 %3)
  ret void
}

declare void @_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectii(%class.btCollisionWorld*, %class.btCollisionObject*, i32, i32) unnamed_addr #3

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject(%class.btDiscreteDynamicsWorld* %this, %class.btCollisionObject* %collisionObject) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %collisionObject.addr = alloca %class.btCollisionObject*, align 4
  %body = alloca %class.btRigidBody*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btCollisionObject* %collisionObject, %class.btCollisionObject** %collisionObject.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4
  %call = call %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %0)
  store %class.btRigidBody* %call, %class.btRigidBody** %body, align 4
  %1 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %tobool = icmp ne %class.btRigidBody* %1, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %3 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*)***
  %vtable = load void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*)**, void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*)*** %3, align 4
  %vfn = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*)*, void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*)** %vtable, i64 23
  %4 = load void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*)*, void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*)** %vfn, align 4
  call void %4(%class.btDiscreteDynamicsWorld* %this1, %class.btRigidBody* %2)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4
  call void @_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject(%class.btCollisionWorld* %5, %class.btCollisionObject* %6)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

declare void @_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject(%class.btCollisionWorld*, %class.btCollisionObject*) unnamed_addr #3

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody(%class.btDiscreteDynamicsWorld* %this, %class.btRigidBody* %body) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %body.addr = alloca %class.btRigidBody*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btRigidBody* %body, %class.btRigidBody** %body.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %m_nonStaticRigidBodies = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  call void @_ZN20btAlignedObjectArrayIP11btRigidBodyE6removeERKS1_(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies, %class.btRigidBody** nonnull align 4 dereferenceable(4) %body.addr)
  %0 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %2 = bitcast %class.btRigidBody* %1 to %class.btCollisionObject*
  call void @_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject(%class.btCollisionWorld* %0, %class.btCollisionObject* %2)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btRigidBodyE6removeERKS1_(%class.btAlignedObjectArray.19* %this, %class.btRigidBody** nonnull align 4 dereferenceable(4) %key) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.19*, align 4
  %key.addr = alloca %class.btRigidBody**, align 4
  %findIndex = alloca i32, align 4
  store %class.btAlignedObjectArray.19* %this, %class.btAlignedObjectArray.19** %this.addr, align 4
  store %class.btRigidBody** %key, %class.btRigidBody*** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray.19*, %class.btAlignedObjectArray.19** %this.addr, align 4
  %0 = load %class.btRigidBody**, %class.btRigidBody*** %key.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE16findLinearSearchERKS1_(%class.btAlignedObjectArray.19* %this1, %class.btRigidBody** nonnull align 4 dereferenceable(4) %0)
  store i32 %call, i32* %findIndex, align 4
  %1 = load i32, i32* %findIndex, align 4
  call void @_ZN20btAlignedObjectArrayIP11btRigidBodyE13removeAtIndexEi(%class.btAlignedObjectArray.19* %this1, i32 %1)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody(%class.btDiscreteDynamicsWorld* %this, %class.btRigidBody* %body) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %body.addr = alloca %class.btRigidBody*, align 4
  %isDynamic = alloca i8, align 1
  %collisionFilterGroup = alloca i32, align 4
  %collisionFilterMask = alloca i32, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btRigidBody* %body, %class.btRigidBody** %body.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %1 = bitcast %class.btRigidBody* %0 to %class.btCollisionObject*
  %call = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %1)
  br i1 %call, label %if.end, label %land.lhs.true

land.lhs.true:                                    ; preds = %entry
  %2 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %call2 = call i32 @_ZNK11btRigidBody8getFlagsEv(%class.btRigidBody* %2)
  %and = and i32 %call2, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %land.lhs.true
  %3 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %m_gravity = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 7
  call void @_ZN11btRigidBody10setGravityERK9btVector3(%class.btRigidBody* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %m_gravity)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %4 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %call3 = call %class.btCollisionShape* @_ZN11btRigidBody17getCollisionShapeEv(%class.btRigidBody* %4)
  %tobool4 = icmp ne %class.btCollisionShape* %call3, null
  br i1 %tobool4, label %if.then5, label %if.end14

if.then5:                                         ; preds = %if.end
  %5 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %6 = bitcast %class.btRigidBody* %5 to %class.btCollisionObject*
  %call6 = call zeroext i1 @_ZNK17btCollisionObject14isStaticObjectEv(%class.btCollisionObject* %6)
  br i1 %call6, label %if.else, label %if.then7

if.then7:                                         ; preds = %if.then5
  %m_nonStaticRigidBodies = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  call void @_ZN20btAlignedObjectArrayIP11btRigidBodyE9push_backERKS1_(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies, %class.btRigidBody** nonnull align 4 dereferenceable(4) %body.addr)
  br label %if.end8

if.else:                                          ; preds = %if.then5
  %7 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %8 = bitcast %class.btRigidBody* %7 to %class.btCollisionObject*
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %8, i32 2)
  br label %if.end8

if.end8:                                          ; preds = %if.else, %if.then7
  %9 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %10 = bitcast %class.btRigidBody* %9 to %class.btCollisionObject*
  %call9 = call zeroext i1 @_ZNK17btCollisionObject14isStaticObjectEv(%class.btCollisionObject* %10)
  br i1 %call9, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.end8
  %11 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %12 = bitcast %class.btRigidBody* %11 to %class.btCollisionObject*
  %call10 = call zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %12)
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %if.end8
  %13 = phi i1 [ true, %if.end8 ], [ %call10, %lor.rhs ]
  %lnot = xor i1 %13, true
  %frombool = zext i1 %lnot to i8
  store i8 %frombool, i8* %isDynamic, align 1
  %14 = load i8, i8* %isDynamic, align 1
  %tobool11 = trunc i8 %14 to i1
  %15 = zext i1 %tobool11 to i64
  %cond = select i1 %tobool11, i32 1, i32 2
  store i32 %cond, i32* %collisionFilterGroup, align 4
  %16 = load i8, i8* %isDynamic, align 1
  %tobool12 = trunc i8 %16 to i1
  %17 = zext i1 %tobool12 to i64
  %cond13 = select i1 %tobool12, i32 -1, i32 -3
  store i32 %cond13, i32* %collisionFilterMask, align 4
  %18 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %19 = bitcast %class.btRigidBody* %18 to %class.btCollisionObject*
  %20 = load i32, i32* %collisionFilterGroup, align 4
  %21 = load i32, i32* %collisionFilterMask, align 4
  %22 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32)***
  %vtable = load void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32)**, void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32)*** %22, align 4
  %vfn = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32)*, void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32)** %vtable, i64 9
  %23 = load void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32)*, void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32)** %vfn, align 4
  call void %23(%class.btDiscreteDynamicsWorld* %this1, %class.btCollisionObject* %19, i32 %20, i32 %21)
  br label %if.end14

if.end14:                                         ; preds = %lor.end, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZN11btRigidBody17getCollisionShapeEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 9
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4
  ret %class.btCollisionShape* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject14isStaticObjectEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4
  %and = and i32 %0, 1
  %cmp = icmp ne i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btRigidBodyE9push_backERKS1_(%class.btAlignedObjectArray.19* %this, %class.btRigidBody** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.19*, align 4
  %_Val.addr = alloca %class.btRigidBody**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.19* %this, %class.btAlignedObjectArray.19** %this.addr, align 4
  store %class.btRigidBody** %_Val, %class.btRigidBody*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.19*, %class.btAlignedObjectArray.19** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE8capacityEv(%class.btAlignedObjectArray.19* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP11btRigidBodyE9allocSizeEi(%class.btAlignedObjectArray.19* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP11btRigidBodyE7reserveEi(%class.btAlignedObjectArray.19* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 4
  %1 = load %class.btRigidBody**, %class.btRigidBody*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btRigidBody*, %class.btRigidBody** %1, i32 %2
  %3 = bitcast %class.btRigidBody** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btRigidBody**
  %5 = load %class.btRigidBody**, %class.btRigidBody*** %_Val.addr, align 4
  %6 = load %class.btRigidBody*, %class.btRigidBody** %5, align 4
  store %class.btRigidBody* %6, %class.btRigidBody** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

declare void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject*, i32) #3

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyii(%class.btDiscreteDynamicsWorld* %this, %class.btRigidBody* %body, i32 %group, i32 %mask) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %body.addr = alloca %class.btRigidBody*, align 4
  %group.addr = alloca i32, align 4
  %mask.addr = alloca i32, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btRigidBody* %body, %class.btRigidBody** %body.addr, align 4
  store i32 %group, i32* %group.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %1 = bitcast %class.btRigidBody* %0 to %class.btCollisionObject*
  %call = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %1)
  br i1 %call, label %if.end, label %land.lhs.true

land.lhs.true:                                    ; preds = %entry
  %2 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %call2 = call i32 @_ZNK11btRigidBody8getFlagsEv(%class.btRigidBody* %2)
  %and = and i32 %call2, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %land.lhs.true
  %3 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %m_gravity = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 7
  call void @_ZN11btRigidBody10setGravityERK9btVector3(%class.btRigidBody* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %m_gravity)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %4 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %call3 = call %class.btCollisionShape* @_ZN11btRigidBody17getCollisionShapeEv(%class.btRigidBody* %4)
  %tobool4 = icmp ne %class.btCollisionShape* %call3, null
  br i1 %tobool4, label %if.then5, label %if.end9

if.then5:                                         ; preds = %if.end
  %5 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %6 = bitcast %class.btRigidBody* %5 to %class.btCollisionObject*
  %call6 = call zeroext i1 @_ZNK17btCollisionObject14isStaticObjectEv(%class.btCollisionObject* %6)
  br i1 %call6, label %if.else, label %if.then7

if.then7:                                         ; preds = %if.then5
  %m_nonStaticRigidBodies = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  call void @_ZN20btAlignedObjectArrayIP11btRigidBodyE9push_backERKS1_(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies, %class.btRigidBody** nonnull align 4 dereferenceable(4) %body.addr)
  br label %if.end8

if.else:                                          ; preds = %if.then5
  %7 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %8 = bitcast %class.btRigidBody* %7 to %class.btCollisionObject*
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %8, i32 2)
  br label %if.end8

if.end8:                                          ; preds = %if.else, %if.then7
  %9 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4
  %10 = bitcast %class.btRigidBody* %9 to %class.btCollisionObject*
  %11 = load i32, i32* %group.addr, align 4
  %12 = load i32, i32* %mask.addr, align 4
  %13 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32)***
  %vtable = load void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32)**, void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32)*** %13, align 4
  %vfn = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32)*, void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32)** %vtable, i64 9
  %14 = load void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32)*, void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32)** %vfn, align 4
  call void %14(%class.btDiscreteDynamicsWorld* %this1, %class.btCollisionObject* %10, i32 %11, i32 %12)
  br label %if.end9

if.end9:                                          ; preds = %if.end8, %if.end
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld21updateActivationStateEf(%class.btDiscreteDynamicsWorld* %this, float %timeStep) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %i = alloca i32, align 4
  %body = alloca %class.btRigidBody*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp22 = alloca float, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.5, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_nonStaticRigidBodies = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_nonStaticRigidBodies3 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %1 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %class.btRigidBody** @_ZN20btAlignedObjectArrayIP11btRigidBodyEixEi(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies3, i32 %1)
  %2 = load %class.btRigidBody*, %class.btRigidBody** %call4, align 4
  store %class.btRigidBody* %2, %class.btRigidBody** %body, align 4
  %3 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %tobool = icmp ne %class.btRigidBody* %3, null
  br i1 %tobool, label %if.then, label %if.end32

if.then:                                          ; preds = %for.body
  %4 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %5 = load float, float* %timeStep.addr, align 4
  call void @_ZN11btRigidBody18updateDeactivationEf(%class.btRigidBody* %4, float %5)
  %6 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %call5 = call zeroext i1 @_ZN11btRigidBody13wantsSleepingEv(%class.btRigidBody* %6)
  br i1 %call5, label %if.then6, label %if.else26

if.then6:                                         ; preds = %if.then
  %7 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %8 = bitcast %class.btRigidBody* %7 to %class.btCollisionObject*
  %call7 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %8)
  br i1 %call7, label %if.then8, label %if.else

if.then8:                                         ; preds = %if.then6
  %9 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %10 = bitcast %class.btRigidBody* %9 to %class.btCollisionObject*
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %10, i32 2)
  br label %if.end25

if.else:                                          ; preds = %if.then6
  %11 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %12 = bitcast %class.btRigidBody* %11 to %class.btCollisionObject*
  %call9 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %12)
  %cmp10 = icmp eq i32 %call9, 1
  br i1 %cmp10, label %if.then11, label %if.end

if.then11:                                        ; preds = %if.else
  %13 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %14 = bitcast %class.btRigidBody* %13 to %class.btCollisionObject*
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %14, i32 3)
  br label %if.end

if.end:                                           ; preds = %if.then11, %if.else
  %15 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %16 = bitcast %class.btRigidBody* %15 to %class.btCollisionObject*
  %call12 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %16)
  %cmp13 = icmp eq i32 %call12, 2
  br i1 %cmp13, label %if.then14, label %if.end24

if.then14:                                        ; preds = %if.end
  %17 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  store float 0.000000e+00, float* %ref.tmp15, align 4
  store float 0.000000e+00, float* %ref.tmp16, align 4
  store float 0.000000e+00, float* %ref.tmp17, align 4
  %call18 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  call void @_ZN11btRigidBody18setAngularVelocityERK9btVector3(%class.btRigidBody* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %18 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  store float 0.000000e+00, float* %ref.tmp20, align 4
  store float 0.000000e+00, float* %ref.tmp21, align 4
  store float 0.000000e+00, float* %ref.tmp22, align 4
  %call23 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp22)
  call void @_ZN11btRigidBody17setLinearVelocityERK9btVector3(%class.btRigidBody* %18, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp19)
  br label %if.end24

if.end24:                                         ; preds = %if.then14, %if.end
  br label %if.end25

if.end25:                                         ; preds = %if.end24, %if.then8
  br label %if.end31

if.else26:                                        ; preds = %if.then
  %19 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %20 = bitcast %class.btRigidBody* %19 to %class.btCollisionObject*
  %call27 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %20)
  %cmp28 = icmp ne i32 %call27, 4
  br i1 %cmp28, label %if.then29, label %if.end30

if.then29:                                        ; preds = %if.else26
  %21 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %22 = bitcast %class.btRigidBody* %21 to %class.btCollisionObject*
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %22, i32 1)
  br label %if.end30

if.end30:                                         ; preds = %if.then29, %if.else26
  br label %if.end31

if.end31:                                         ; preds = %if.end30, %if.end25
  br label %if.end32

if.end32:                                         ; preds = %if.end31, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end32
  %23 = load i32, i32* %i, align 4
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call33 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btRigidBody18updateDeactivationEf(%class.btRigidBody* %this, float %timeStep) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %0)
  %cmp = icmp eq i32 %call, 2
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call2 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %1)
  %cmp3 = icmp eq i32 %call2, 4
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  br label %if.end15

if.end:                                           ; preds = %lor.lhs.false
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %this1)
  %call5 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %call4)
  %m_linearSleepingThreshold = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 18
  %2 = load float, float* %m_linearSleepingThreshold, align 4
  %m_linearSleepingThreshold6 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 18
  %3 = load float, float* %m_linearSleepingThreshold6, align 4
  %mul = fmul float %2, %3
  %cmp7 = fcmp olt float %call5, %mul
  br i1 %cmp7, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.end
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %this1)
  %call9 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %call8)
  %m_angularSleepingThreshold = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 19
  %4 = load float, float* %m_angularSleepingThreshold, align 4
  %m_angularSleepingThreshold10 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 19
  %5 = load float, float* %m_angularSleepingThreshold10, align 4
  %mul11 = fmul float %4, %5
  %cmp12 = fcmp olt float %call9, %mul11
  br i1 %cmp12, label %if.then13, label %if.else

if.then13:                                        ; preds = %land.lhs.true
  %6 = load float, float* %timeStep.addr, align 4
  %7 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_deactivationTime = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %7, i32 0, i32 17
  %8 = load float, float* %m_deactivationTime, align 4
  %add = fadd float %8, %6
  store float %add, float* %m_deactivationTime, align 4
  br label %if.end15

if.else:                                          ; preds = %land.lhs.true, %if.end
  %9 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_deactivationTime14 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %9, i32 0, i32 17
  store float 0.000000e+00, float* %m_deactivationTime14, align 4
  %10 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %10, i32 0)
  br label %if.end15

if.end15:                                         ; preds = %if.then, %if.else, %if.then13
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN11btRigidBody13wantsSleepingEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %0)
  %cmp = icmp eq i32 %call, 4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8, i8* @gDisableDeactivation, align 1
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then3, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %2 = load float, float* @gDeactivationTime, align 4
  %cmp2 = fcmp oeq float %2, 0.000000e+00
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %lor.lhs.false, %if.end
  store i1 false, i1* %retval, align 1
  br label %return

if.end4:                                          ; preds = %lor.lhs.false
  %3 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call5 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %3)
  %cmp6 = icmp eq i32 %call5, 2
  br i1 %cmp6, label %if.then10, label %lor.lhs.false7

lor.lhs.false7:                                   ; preds = %if.end4
  %4 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call8 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %4)
  %cmp9 = icmp eq i32 %call8, 3
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %lor.lhs.false7, %if.end4
  store i1 true, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %lor.lhs.false7
  %5 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_deactivationTime = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %5, i32 0, i32 17
  %6 = load float, float* %m_deactivationTime, align 4
  %7 = load float, float* @gDeactivationTime, align 4
  %cmp12 = fcmp ogt float %6, %7
  br i1 %cmp12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %if.end11
  store i1 true, i1* %retval, align 1
  br label %return

if.end14:                                         ; preds = %if.end11
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end14, %if.then13, %if.then10, %if.then3, %if.then
  %8 = load i1, i1* %retval, align 1
  ret i1 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btRigidBody18setAngularVelocityERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %ang_vel) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %ang_vel.addr = alloca %class.btVector3*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %ang_vel, %class.btVector3** %ang_vel.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_updateRevision = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 33
  %1 = load i32, i32* %m_updateRevision, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %m_updateRevision, align 4
  %2 = load %class.btVector3*, %class.btVector3** %ang_vel.addr, align 4
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %3 = bitcast %class.btVector3* %m_angularVelocity to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btRigidBody17setLinearVelocityERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %lin_vel) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %lin_vel.addr = alloca %class.btVector3*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %lin_vel, %class.btVector3** %lin_vel.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_updateRevision = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 33
  %1 = load i32, i32* %m_updateRevision, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %m_updateRevision, align 4
  %2 = load %class.btVector3*, %class.btVector3** %lin_vel.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %3 = bitcast %class.btVector3* %m_linearVelocity to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb(%class.btDiscreteDynamicsWorld* %this, %class.btTypedConstraint* %constraint, i1 zeroext %disableCollisionsBetweenLinkedBodies) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %constraint.addr = alloca %class.btTypedConstraint*, align 4
  %disableCollisionsBetweenLinkedBodies.addr = alloca i8, align 1
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btTypedConstraint* %constraint, %class.btTypedConstraint** %constraint.addr, align 4
  %frombool = zext i1 %disableCollisionsBetweenLinkedBodies to i8
  store i8 %frombool, i8* %disableCollisionsBetweenLinkedBodies.addr, align 1
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %m_constraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 5
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_(%class.btAlignedObjectArray.4* %m_constraints, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %constraint.addr)
  %0 = load i8, i8* %disableCollisionsBetweenLinkedBodies.addr, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint.addr, align 4
  %call = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %1)
  %2 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint.addr, align 4
  call void @_ZN11btRigidBody16addConstraintRefEP17btTypedConstraint(%class.btRigidBody* %call, %class.btTypedConstraint* %2)
  %3 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %3)
  %4 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint.addr, align 4
  call void @_ZN11btRigidBody16addConstraintRefEP17btTypedConstraint(%class.btRigidBody* %call2, %class.btTypedConstraint* %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_(%class.btAlignedObjectArray.4* %this, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Val.addr = alloca %class.btTypedConstraint**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store %class.btTypedConstraint** %_Val, %class.btTypedConstraint*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9allocSizeEi(%class.btAlignedObjectArray.4* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %1 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %1, i32 %2
  %3 = bitcast %class.btTypedConstraint** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btTypedConstraint**
  %5 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %_Val.addr, align 4
  %6 = load %class.btTypedConstraint*, %class.btTypedConstraint** %5, align 4
  store %class.btTypedConstraint* %6, %class.btTypedConstraint** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 8
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  ret %class.btRigidBody* %0
}

declare void @_ZN11btRigidBody16addConstraintRefEP17btTypedConstraint(%class.btRigidBody*, %class.btTypedConstraint*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 9
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  ret %class.btRigidBody* %0
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint(%class.btDiscreteDynamicsWorld* %this, %class.btTypedConstraint* %constraint) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %constraint.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btTypedConstraint* %constraint, %class.btTypedConstraint** %constraint.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %m_constraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 5
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6removeERKS1_(%class.btAlignedObjectArray.4* %m_constraints, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %constraint.addr)
  %0 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint.addr, align 4
  %call = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %0)
  %1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint.addr, align 4
  call void @_ZN11btRigidBody19removeConstraintRefEP17btTypedConstraint(%class.btRigidBody* %call, %class.btTypedConstraint* %1)
  %2 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %2)
  %3 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint.addr, align 4
  call void @_ZN11btRigidBody19removeConstraintRefEP17btTypedConstraint(%class.btRigidBody* %call2, %class.btTypedConstraint* %3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6removeERKS1_(%class.btAlignedObjectArray.4* %this, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %key) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %key.addr = alloca %class.btTypedConstraint**, align 4
  %findIndex = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store %class.btTypedConstraint** %key, %class.btTypedConstraint*** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %key.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE16findLinearSearchERKS1_(%class.btAlignedObjectArray.4* %this1, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %0)
  store i32 %call, i32* %findIndex, align 4
  %1 = load i32, i32* %findIndex, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE13removeAtIndexEi(%class.btAlignedObjectArray.4* %this1, i32 %1)
  ret void
}

declare void @_ZN11btRigidBody19removeConstraintRefEP17btTypedConstraint(%class.btRigidBody*, %class.btTypedConstraint*) #3

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface(%class.btDiscreteDynamicsWorld* %this, %class.btActionInterface* %action) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %action.addr = alloca %class.btActionInterface*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btActionInterface* %action, %class.btActionInterface** %action.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %m_actions = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 14
  call void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE9push_backERKS1_(%class.btAlignedObjectArray.23* %m_actions, %class.btActionInterface** nonnull align 4 dereferenceable(4) %action.addr)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE9push_backERKS1_(%class.btAlignedObjectArray.23* %this, %class.btActionInterface** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.23*, align 4
  %_Val.addr = alloca %class.btActionInterface**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.23* %this, %class.btAlignedObjectArray.23** %this.addr, align 4
  store %class.btActionInterface** %_Val, %class.btActionInterface*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.23*, %class.btAlignedObjectArray.23** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btActionInterfaceE4sizeEv(%class.btAlignedObjectArray.23* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btActionInterfaceE8capacityEv(%class.btAlignedObjectArray.23* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btActionInterfaceE4sizeEv(%class.btAlignedObjectArray.23* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP17btActionInterfaceE9allocSizeEi(%class.btAlignedObjectArray.23* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE7reserveEi(%class.btAlignedObjectArray.23* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 4
  %1 = load %class.btActionInterface**, %class.btActionInterface*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btActionInterface*, %class.btActionInterface** %1, i32 %2
  %3 = bitcast %class.btActionInterface** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btActionInterface**
  %5 = load %class.btActionInterface**, %class.btActionInterface*** %_Val.addr, align 4
  %6 = load %class.btActionInterface*, %class.btActionInterface** %5, align 4
  store %class.btActionInterface* %6, %class.btActionInterface** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface(%class.btDiscreteDynamicsWorld* %this, %class.btActionInterface* %action) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %action.addr = alloca %class.btActionInterface*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btActionInterface* %action, %class.btActionInterface** %action.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %m_actions = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 14
  call void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE6removeERKS1_(%class.btAlignedObjectArray.23* %m_actions, %class.btActionInterface** nonnull align 4 dereferenceable(4) %action.addr)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE6removeERKS1_(%class.btAlignedObjectArray.23* %this, %class.btActionInterface** nonnull align 4 dereferenceable(4) %key) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.23*, align 4
  %key.addr = alloca %class.btActionInterface**, align 4
  %findIndex = alloca i32, align 4
  store %class.btAlignedObjectArray.23* %this, %class.btAlignedObjectArray.23** %this.addr, align 4
  store %class.btActionInterface** %key, %class.btActionInterface*** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray.23*, %class.btAlignedObjectArray.23** %this.addr, align 4
  %0 = load %class.btActionInterface**, %class.btActionInterface*** %key.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btActionInterfaceE16findLinearSearchERKS1_(%class.btAlignedObjectArray.23* %this1, %class.btActionInterface** nonnull align 4 dereferenceable(4) %0)
  store i32 %call, i32* %findIndex, align 4
  %1 = load i32, i32* %findIndex, align 4
  call void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE13removeAtIndexEi(%class.btAlignedObjectArray.23* %this1, i32 %1)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface(%class.btDiscreteDynamicsWorld* %this, %class.btActionInterface* %vehicle) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %vehicle.addr = alloca %class.btActionInterface*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btActionInterface* %vehicle, %class.btActionInterface** %vehicle.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = load %class.btActionInterface*, %class.btActionInterface** %vehicle.addr, align 4
  %1 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)***
  %vtable = load void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)**, void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)*** %1, align 4
  %vfn = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)*, void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)** %vtable, i64 16
  %2 = load void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)*, void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)** %vfn, align 4
  call void %2(%class.btDiscreteDynamicsWorld* %this1, %class.btActionInterface* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface(%class.btDiscreteDynamicsWorld* %this, %class.btActionInterface* %vehicle) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %vehicle.addr = alloca %class.btActionInterface*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btActionInterface* %vehicle, %class.btActionInterface** %vehicle.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = load %class.btActionInterface*, %class.btActionInterface** %vehicle.addr, align 4
  %1 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)***
  %vtable = load void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)**, void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)*** %1, align 4
  %vfn = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)*, void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)** %vtable, i64 17
  %2 = load void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)*, void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)** %vfn, align 4
  call void %2(%class.btDiscreteDynamicsWorld* %this1, %class.btActionInterface* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface(%class.btDiscreteDynamicsWorld* %this, %class.btActionInterface* %character) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %character.addr = alloca %class.btActionInterface*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btActionInterface* %character, %class.btActionInterface** %character.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = load %class.btActionInterface*, %class.btActionInterface** %character.addr, align 4
  %1 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)***
  %vtable = load void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)**, void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)*** %1, align 4
  %vfn = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)*, void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)** %vtable, i64 16
  %2 = load void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)*, void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)** %vfn, align 4
  call void %2(%class.btDiscreteDynamicsWorld* %this1, %class.btActionInterface* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface(%class.btDiscreteDynamicsWorld* %this, %class.btActionInterface* %character) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %character.addr = alloca %class.btActionInterface*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btActionInterface* %character, %class.btActionInterface** %character.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = load %class.btActionInterface*, %class.btActionInterface** %character.addr, align 4
  %1 = bitcast %class.btDiscreteDynamicsWorld* %this1 to void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)***
  %vtable = load void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)**, void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)*** %1, align 4
  %vfn = getelementptr inbounds void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)*, void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)** %vtable, i64 17
  %2 = load void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)*, void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)** %vfn, align 4
  call void %2(%class.btDiscreteDynamicsWorld* %this1, %class.btActionInterface* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfo(%class.btDiscreteDynamicsWorld* %this, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %solverInfo) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %solverInfo.addr = alloca %struct.btContactSolverInfo*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %ref.tmp = alloca %class.btTypedConstraint*, align 4
  %i = alloca i32, align 4
  %ref.tmp9 = alloca %class.btSortConstraintOnIslandPredicate, align 1
  %constraintsPtr = alloca %class.btTypedConstraint**, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %struct.btContactSolverInfo* %solverInfo, %struct.btContactSolverInfo** %solverInfo.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.6, i32 0, i32 0))
  %m_sortedConstraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 1
  %m_constraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 5
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %m_constraints)
  store %class.btTypedConstraint* null, %class.btTypedConstraint** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.4* %m_sortedConstraints, i32 %call2, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %ref.tmp)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = bitcast %class.btDiscreteDynamicsWorld* %this1 to i32 (%class.btDiscreteDynamicsWorld*)***
  %vtable = load i32 (%class.btDiscreteDynamicsWorld*)**, i32 (%class.btDiscreteDynamicsWorld*)*** %1, align 4
  %vfn = getelementptr inbounds i32 (%class.btDiscreteDynamicsWorld*)*, i32 (%class.btDiscreteDynamicsWorld*)** %vtable, i64 26
  %2 = load i32 (%class.btDiscreteDynamicsWorld*)*, i32 (%class.btDiscreteDynamicsWorld*)** %vfn, align 4
  %call3 = call i32 %2(%class.btDiscreteDynamicsWorld* %this1)
  %cmp = icmp slt i32 %0, %call3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_constraints4 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 5
  %3 = load i32, i32* %i, align 4
  %call5 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.4* %m_constraints4, i32 %3)
  %4 = load %class.btTypedConstraint*, %class.btTypedConstraint** %call5, align 4
  %m_sortedConstraints6 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 1
  %5 = load i32, i32* %i, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.4* %m_sortedConstraints6, i32 %5)
  store %class.btTypedConstraint* %4, %class.btTypedConstraint** %call7, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_sortedConstraints8 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9quickSortI33btSortConstraintOnIslandPredicateEEvRKT_(%class.btAlignedObjectArray.4* %m_sortedConstraints8, %class.btSortConstraintOnIslandPredicate* nonnull align 1 dereferenceable(1) %ref.tmp9)
  %7 = bitcast %class.btDiscreteDynamicsWorld* %this1 to i32 (%class.btDiscreteDynamicsWorld*)***
  %vtable10 = load i32 (%class.btDiscreteDynamicsWorld*)**, i32 (%class.btDiscreteDynamicsWorld*)*** %7, align 4
  %vfn11 = getelementptr inbounds i32 (%class.btDiscreteDynamicsWorld*)*, i32 (%class.btDiscreteDynamicsWorld*)** %vtable10, i64 26
  %8 = load i32 (%class.btDiscreteDynamicsWorld*)*, i32 (%class.btDiscreteDynamicsWorld*)** %vfn11, align 4
  %call12 = call i32 %8(%class.btDiscreteDynamicsWorld* %this1)
  %tobool = icmp ne i32 %call12, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end
  %m_sortedConstraints13 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 1
  %call14 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.4* %m_sortedConstraints13, i32 0)
  br label %cond.end

cond.false:                                       ; preds = %for.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btTypedConstraint** [ %call14, %cond.true ], [ null, %cond.false ]
  store %class.btTypedConstraint** %cond, %class.btTypedConstraint*** %constraintsPtr, align 4
  %m_solverIslandCallback = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 2
  %9 = load %struct.InplaceSolverIslandCallback*, %struct.InplaceSolverIslandCallback** %m_solverIslandCallback, align 4
  %10 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %solverInfo.addr, align 4
  %11 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraintsPtr, align 4
  %m_sortedConstraints15 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 1
  %call16 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %m_sortedConstraints15)
  %12 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %13 = bitcast %class.btCollisionWorld* %12 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable17 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %13, align 4
  %vfn18 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable17, i64 5
  %14 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn18, align 4
  %call19 = call %class.btIDebugDraw* %14(%class.btCollisionWorld* %12)
  call void @_ZN27InplaceSolverIslandCallback5setupEP19btContactSolverInfoPP17btTypedConstraintiP12btIDebugDraw(%struct.InplaceSolverIslandCallback* %9, %struct.btContactSolverInfo* %10, %class.btTypedConstraint** %11, i32 %call16, %class.btIDebugDraw* %call19)
  %m_constraintSolver = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 3
  %15 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver, align 4
  %call20 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %this1)
  %call21 = call i32 @_ZNK16btCollisionWorld22getNumCollisionObjectsEv(%class.btCollisionWorld* %call20)
  %call22 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %this1)
  %call23 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %call22)
  %16 = bitcast %class.btDispatcher* %call23 to i32 (%class.btDispatcher*)***
  %vtable24 = load i32 (%class.btDispatcher*)**, i32 (%class.btDispatcher*)*** %16, align 4
  %vfn25 = getelementptr inbounds i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vtable24, i64 9
  %17 = load i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vfn25, align 4
  %call26 = call i32 %17(%class.btDispatcher* %call23)
  %18 = bitcast %class.btConstraintSolver* %15 to void (%class.btConstraintSolver*, i32, i32)***
  %vtable27 = load void (%class.btConstraintSolver*, i32, i32)**, void (%class.btConstraintSolver*, i32, i32)*** %18, align 4
  %vfn28 = getelementptr inbounds void (%class.btConstraintSolver*, i32, i32)*, void (%class.btConstraintSolver*, i32, i32)** %vtable27, i64 2
  %19 = load void (%class.btConstraintSolver*, i32, i32)*, void (%class.btConstraintSolver*, i32, i32)** %vfn28, align 4
  call void %19(%class.btConstraintSolver* %15, i32 %call21, i32 %call26)
  %m_islandManager = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 4
  %20 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %m_islandManager, align 4
  %call29 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %this1)
  %call30 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %call29)
  %call31 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %this1)
  %m_solverIslandCallback32 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 2
  %21 = load %struct.InplaceSolverIslandCallback*, %struct.InplaceSolverIslandCallback** %m_solverIslandCallback32, align 4
  %22 = bitcast %struct.InplaceSolverIslandCallback* %21 to %"struct.btSimulationIslandManager::IslandCallback"*
  call void @_ZN25btSimulationIslandManager22buildAndProcessIslandsEP12btDispatcherP16btCollisionWorldPNS_14IslandCallbackE(%class.btSimulationIslandManager* %20, %class.btDispatcher* %call30, %class.btCollisionWorld* %call31, %"struct.btSimulationIslandManager::IslandCallback"* %22)
  %m_solverIslandCallback33 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 2
  %23 = load %struct.InplaceSolverIslandCallback*, %struct.InplaceSolverIslandCallback** %m_solverIslandCallback33, align 4
  call void @_ZN27InplaceSolverIslandCallback18processConstraintsEv(%struct.InplaceSolverIslandCallback* %23)
  %m_constraintSolver34 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 3
  %24 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver34, align 4
  %25 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %solverInfo.addr, align 4
  %26 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %26, i32 0, i32 5
  %27 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4
  %28 = bitcast %class.btConstraintSolver* %24 to void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)***
  %vtable35 = load void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)**, void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*** %28, align 4
  %vfn36 = getelementptr inbounds void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*, void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)** %vtable35, i64 4
  %29 = load void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*, void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)** %vfn36, align 4
  call void %29(%class.btConstraintSolver* %24, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %25, %class.btIDebugDraw* %27)
  %call37 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.4* %this, i32 %newsize, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btTypedConstraint**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btTypedConstraint** %fillData, %class.btTypedConstraint*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %5 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %14 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %14, i32 %15
  %16 = bitcast %class.btTypedConstraint** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btTypedConstraint**
  %18 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %fillData.addr, align 4
  %19 = load %class.btTypedConstraint*, %class.btTypedConstraint** %18, align 4
  store %class.btTypedConstraint* %19, %class.btTypedConstraint** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %0, i32 %1
  ret %class.btTypedConstraint** %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9quickSortI33btSortConstraintOnIslandPredicateEEvRKT_(%class.btAlignedObjectArray.4* %this, %class.btSortConstraintOnIslandPredicate* nonnull align 1 dereferenceable(1) %CompareFunc) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %CompareFunc.addr = alloca %class.btSortConstraintOnIslandPredicate*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store %class.btSortConstraintOnIslandPredicate* %CompareFunc, %class.btSortConstraintOnIslandPredicate** %CompareFunc.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.btSortConstraintOnIslandPredicate*, %class.btSortConstraintOnIslandPredicate** %CompareFunc.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI33btSortConstraintOnIslandPredicateEEvRKT_ii(%class.btAlignedObjectArray.4* %this1, %class.btSortConstraintOnIslandPredicate* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN27InplaceSolverIslandCallback5setupEP19btContactSolverInfoPP17btTypedConstraintiP12btIDebugDraw(%struct.InplaceSolverIslandCallback* %this, %struct.btContactSolverInfo* %solverInfo, %class.btTypedConstraint** %sortedConstraints, i32 %numConstraints, %class.btIDebugDraw* %debugDrawer) #2 comdat {
entry:
  %this.addr = alloca %struct.InplaceSolverIslandCallback*, align 4
  %solverInfo.addr = alloca %struct.btContactSolverInfo*, align 4
  %sortedConstraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %ref.tmp = alloca %class.btCollisionObject*, align 4
  %ref.tmp2 = alloca %class.btPersistentManifold*, align 4
  %ref.tmp3 = alloca %class.btTypedConstraint*, align 4
  store %struct.InplaceSolverIslandCallback* %this, %struct.InplaceSolverIslandCallback** %this.addr, align 4
  store %struct.btContactSolverInfo* %solverInfo, %struct.btContactSolverInfo** %solverInfo.addr, align 4
  store %class.btTypedConstraint** %sortedConstraints, %class.btTypedConstraint*** %sortedConstraints.addr, align 4
  store i32 %numConstraints, i32* %numConstraints.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %this1 = load %struct.InplaceSolverIslandCallback*, %struct.InplaceSolverIslandCallback** %this.addr, align 4
  %0 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %solverInfo.addr, align 4
  %m_solverInfo = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 1
  store %struct.btContactSolverInfo* %0, %struct.btContactSolverInfo** %m_solverInfo, align 4
  %1 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %sortedConstraints.addr, align 4
  %m_sortedConstraints = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 3
  store %class.btTypedConstraint** %1, %class.btTypedConstraint*** %m_sortedConstraints, align 4
  %2 = load i32, i32* %numConstraints.addr, align 4
  %m_numConstraints = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 4
  store i32 %2, i32* %m_numConstraints, align 4
  %3 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %m_debugDrawer = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 5
  store %class.btIDebugDraw* %3, %class.btIDebugDraw** %m_debugDrawer, align 4
  %m_bodies = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 7
  store %class.btCollisionObject* null, %class.btCollisionObject** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_(%class.btAlignedObjectArray* %m_bodies, i32 0, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %ref.tmp)
  %m_manifolds = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 8
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %ref.tmp2, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.9* %m_manifolds, i32 0, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %ref.tmp2)
  %m_constraints = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 9
  store %class.btTypedConstraint* null, %class.btTypedConstraint** %ref.tmp3, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.4* %m_constraints, i32 0, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  ret %class.btCollisionWorld* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK16btCollisionWorld22getNumCollisionObjectsEv(%class.btCollisionWorld* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_collisionObjects)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 2
  %0 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  ret %class.btDispatcher* %0
}

declare void @_ZN25btSimulationIslandManager22buildAndProcessIslandsEP12btDispatcherP16btCollisionWorldPNS_14IslandCallbackE(%class.btSimulationIslandManager*, %class.btDispatcher*, %class.btCollisionWorld*, %"struct.btSimulationIslandManager::IslandCallback"*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN27InplaceSolverIslandCallback18processConstraintsEv(%struct.InplaceSolverIslandCallback* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.InplaceSolverIslandCallback*, align 4
  %bodies = alloca %class.btCollisionObject**, align 4
  %manifold = alloca %class.btPersistentManifold**, align 4
  %constraints = alloca %class.btTypedConstraint**, align 4
  %ref.tmp = alloca %class.btCollisionObject*, align 4
  %ref.tmp29 = alloca %class.btPersistentManifold*, align 4
  %ref.tmp31 = alloca %class.btTypedConstraint*, align 4
  store %struct.InplaceSolverIslandCallback* %this, %struct.InplaceSolverIslandCallback** %this.addr, align 4
  %this1 = load %struct.InplaceSolverIslandCallback*, %struct.InplaceSolverIslandCallback** %this.addr, align 4
  %m_bodies = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 7
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_bodies)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_bodies2 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 7
  %call3 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_bodies2, i32 0)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btCollisionObject** [ %call3, %cond.true ], [ null, %cond.false ]
  store %class.btCollisionObject** %cond, %class.btCollisionObject*** %bodies, align 4
  %m_manifolds = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 8
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %m_manifolds)
  %tobool5 = icmp ne i32 %call4, 0
  br i1 %tobool5, label %cond.true6, label %cond.false9

cond.true6:                                       ; preds = %cond.end
  %m_manifolds7 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 8
  %call8 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.9* %m_manifolds7, i32 0)
  br label %cond.end10

cond.false9:                                      ; preds = %cond.end
  br label %cond.end10

cond.end10:                                       ; preds = %cond.false9, %cond.true6
  %cond11 = phi %class.btPersistentManifold** [ %call8, %cond.true6 ], [ null, %cond.false9 ]
  store %class.btPersistentManifold** %cond11, %class.btPersistentManifold*** %manifold, align 4
  %m_constraints = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 9
  %call12 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %m_constraints)
  %tobool13 = icmp ne i32 %call12, 0
  br i1 %tobool13, label %cond.true14, label %cond.false17

cond.true14:                                      ; preds = %cond.end10
  %m_constraints15 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 9
  %call16 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.4* %m_constraints15, i32 0)
  br label %cond.end18

cond.false17:                                     ; preds = %cond.end10
  br label %cond.end18

cond.end18:                                       ; preds = %cond.false17, %cond.true14
  %cond19 = phi %class.btTypedConstraint** [ %call16, %cond.true14 ], [ null, %cond.false17 ]
  store %class.btTypedConstraint** %cond19, %class.btTypedConstraint*** %constraints, align 4
  %m_solver = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 2
  %0 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_solver, align 4
  %1 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies, align 4
  %m_bodies20 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 7
  %call21 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_bodies20)
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifold, align 4
  %m_manifolds22 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 8
  %call23 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %m_manifolds22)
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints, align 4
  %m_constraints24 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 9
  %call25 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %m_constraints24)
  %m_solverInfo = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 1
  %4 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %m_solverInfo, align 4
  %m_debugDrawer = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 5
  %5 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4
  %m_dispatcher = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 6
  %6 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %7 = bitcast %class.btConstraintSolver* %0 to float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)***
  %vtable = load float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)**, float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*** %7, align 4
  %vfn = getelementptr inbounds float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vtable, i64 3
  %8 = load float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vfn, align 4
  %call26 = call float %8(%class.btConstraintSolver* %0, %class.btCollisionObject** %1, i32 %call21, %class.btPersistentManifold** %2, i32 %call23, %class.btTypedConstraint** %3, i32 %call25, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %4, %class.btIDebugDraw* %5, %class.btDispatcher* %6)
  %m_bodies27 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 7
  store %class.btCollisionObject* null, %class.btCollisionObject** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_(%class.btAlignedObjectArray* %m_bodies27, i32 0, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %ref.tmp)
  %m_manifolds28 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 8
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %ref.tmp29, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.9* %m_manifolds28, i32 0, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %ref.tmp29)
  %m_constraints30 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 9
  store %class.btTypedConstraint* null, %class.btTypedConstraint** %ref.tmp31, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.4* %m_constraints30, i32 0, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %ref.tmp31)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld26calculateSimulationIslandsEv(%class.btDiscreteDynamicsWorld* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %i = alloca i32, align 4
  %manifold = alloca %class.btPersistentManifold*, align 4
  %colObj0 = alloca %class.btCollisionObject*, align 4
  %colObj1 = alloca %class.btCollisionObject*, align 4
  %i20 = alloca i32, align 4
  %numConstraints = alloca i32, align 4
  %constraint = alloca %class.btTypedConstraint*, align 4
  %colObj029 = alloca %class.btRigidBody*, align 4
  %colObj131 = alloca %class.btRigidBody*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.7, i32 0, i32 0))
  %call2 = call %class.btSimulationIslandManager* @_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv(%class.btDiscreteDynamicsWorld* %this1)
  %call3 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %this1)
  %call4 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %this1)
  %call5 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %call4)
  %0 = bitcast %class.btSimulationIslandManager* %call2 to void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)***
  %vtable = load void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)**, void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)*** %0, align 4
  %vfn = getelementptr inbounds void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)*, void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)** %vtable, i64 2
  %1 = load void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)*, void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)** %vfn, align 4
  call void %1(%class.btSimulationIslandManager* %call2, %class.btCollisionWorld* %call3, %class.btDispatcher* %call5)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4
  %m_predictiveManifolds = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 18
  %call6 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %m_predictiveManifolds)
  %cmp = icmp slt i32 %2, %call6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_predictiveManifolds7 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 18
  %3 = load i32, i32* %i, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.9* %m_predictiveManifolds7, i32 %3)
  %4 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call8, align 4
  store %class.btPersistentManifold* %4, %class.btPersistentManifold** %manifold, align 4
  %5 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %call9 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %5)
  store %class.btCollisionObject* %call9, %class.btCollisionObject** %colObj0, align 4
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %call10 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %6)
  store %class.btCollisionObject* %call10, %class.btCollisionObject** %colObj1, align 4
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %tobool = icmp ne %class.btCollisionObject* %7, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %8 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call11 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %8)
  br i1 %call11, label %if.end, label %land.lhs.true12

land.lhs.true12:                                  ; preds = %land.lhs.true
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %tobool13 = icmp ne %class.btCollisionObject* %9, null
  br i1 %tobool13, label %land.lhs.true14, label %if.end

land.lhs.true14:                                  ; preds = %land.lhs.true12
  %10 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call15 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %10)
  br i1 %call15, label %if.end, label %if.then

if.then:                                          ; preds = %land.lhs.true14
  %call16 = call %class.btSimulationIslandManager* @_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv(%class.btDiscreteDynamicsWorld* %this1)
  %call17 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %call16)
  %11 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call18 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %11)
  %12 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call19 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %12)
  call void @_ZN11btUnionFind5uniteEii(%class.btUnionFind* %call17, i32 %call18, i32 %call19)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true14, %land.lhs.true12, %land.lhs.true, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %13 = load i32, i32* %i, align 4
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_constraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 5
  %call21 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %m_constraints)
  store i32 %call21, i32* %numConstraints, align 4
  store i32 0, i32* %i20, align 4
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc47, %for.end
  %14 = load i32, i32* %i20, align 4
  %15 = load i32, i32* %numConstraints, align 4
  %cmp23 = icmp slt i32 %14, %15
  br i1 %cmp23, label %for.body24, label %for.end49

for.body24:                                       ; preds = %for.cond22
  %m_constraints25 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 5
  %16 = load i32, i32* %i20, align 4
  %call26 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.4* %m_constraints25, i32 %16)
  %17 = load %class.btTypedConstraint*, %class.btTypedConstraint** %call26, align 4
  store %class.btTypedConstraint* %17, %class.btTypedConstraint** %constraint, align 4
  %18 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint, align 4
  %call27 = call zeroext i1 @_ZNK17btTypedConstraint9isEnabledEv(%class.btTypedConstraint* %18)
  br i1 %call27, label %if.then28, label %if.end46

if.then28:                                        ; preds = %for.body24
  %19 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint, align 4
  %call30 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %19)
  store %class.btRigidBody* %call30, %class.btRigidBody** %colObj029, align 4
  %20 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint, align 4
  %call32 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %20)
  store %class.btRigidBody* %call32, %class.btRigidBody** %colObj131, align 4
  %21 = load %class.btRigidBody*, %class.btRigidBody** %colObj029, align 4
  %tobool33 = icmp ne %class.btRigidBody* %21, null
  br i1 %tobool33, label %land.lhs.true34, label %if.end45

land.lhs.true34:                                  ; preds = %if.then28
  %22 = load %class.btRigidBody*, %class.btRigidBody** %colObj029, align 4
  %23 = bitcast %class.btRigidBody* %22 to %class.btCollisionObject*
  %call35 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %23)
  br i1 %call35, label %if.end45, label %land.lhs.true36

land.lhs.true36:                                  ; preds = %land.lhs.true34
  %24 = load %class.btRigidBody*, %class.btRigidBody** %colObj131, align 4
  %tobool37 = icmp ne %class.btRigidBody* %24, null
  br i1 %tobool37, label %land.lhs.true38, label %if.end45

land.lhs.true38:                                  ; preds = %land.lhs.true36
  %25 = load %class.btRigidBody*, %class.btRigidBody** %colObj131, align 4
  %26 = bitcast %class.btRigidBody* %25 to %class.btCollisionObject*
  %call39 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %26)
  br i1 %call39, label %if.end45, label %if.then40

if.then40:                                        ; preds = %land.lhs.true38
  %call41 = call %class.btSimulationIslandManager* @_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv(%class.btDiscreteDynamicsWorld* %this1)
  %call42 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %call41)
  %27 = load %class.btRigidBody*, %class.btRigidBody** %colObj029, align 4
  %28 = bitcast %class.btRigidBody* %27 to %class.btCollisionObject*
  %call43 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %28)
  %29 = load %class.btRigidBody*, %class.btRigidBody** %colObj131, align 4
  %30 = bitcast %class.btRigidBody* %29 to %class.btCollisionObject*
  %call44 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %30)
  call void @_ZN11btUnionFind5uniteEii(%class.btUnionFind* %call42, i32 %call43, i32 %call44)
  br label %if.end45

if.end45:                                         ; preds = %if.then40, %land.lhs.true38, %land.lhs.true36, %land.lhs.true34, %if.then28
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %for.body24
  br label %for.inc47

for.inc47:                                        ; preds = %if.end46
  %31 = load i32, i32* %i20, align 4
  %inc48 = add nsw i32 %31, 1
  store i32 %inc48, i32* %i20, align 4
  br label %for.cond22

for.end49:                                        ; preds = %for.cond22
  %call50 = call %class.btSimulationIslandManager* @_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv(%class.btDiscreteDynamicsWorld* %this1)
  %call51 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %this1)
  %32 = bitcast %class.btSimulationIslandManager* %call50 to void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)***
  %vtable52 = load void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)**, void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)*** %32, align 4
  %vfn53 = getelementptr inbounds void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)*, void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)** %vtable52, i64 3
  %33 = load void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)*, void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)** %vfn53, align 4
  call void %33(%class.btSimulationIslandManager* %call50, %class.btCollisionWorld* %call51)
  %call54 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btSimulationIslandManager* @_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv(%class.btDiscreteDynamicsWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %m_islandManager = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 4
  %0 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %m_islandManager, align 4
  ret %class.btSimulationIslandManager* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.9* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %0, i32 %1
  ret %class.btPersistentManifold** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4
  ret %class.btCollisionObject* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body1 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 3
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body1, align 4
  ret %class.btCollisionObject* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %m_unionFind = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 1
  ret %class.btUnionFind* %m_unionFind
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btUnionFind5uniteEii(%class.btUnionFind* %this, i32 %p, i32 %q) #2 comdat {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %p.addr = alloca i32, align 4
  %q.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4
  store i32 %p, i32* %p.addr, align 4
  store i32 %q, i32* %q.addr, align 4
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %0 = load i32, i32* %p.addr, align 4
  %call = call i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %this1, i32 %0)
  store i32 %call, i32* %i, align 4
  %1 = load i32, i32* %q.addr, align 4
  %call2 = call i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %this1, i32 %1)
  store i32 %call2, i32* %j, align 4
  %2 = load i32, i32* %i, align 4
  %3 = load i32, i32* %j, align 4
  %cmp = icmp eq i32 %2, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %4 = load i32, i32* %j, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %5 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.15* %m_elements, i32 %5)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call3, i32 0, i32 0
  store i32 %4, i32* %m_id, align 4
  %m_elements4 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %6 = load i32, i32* %i, align 4
  %call5 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.15* %m_elements4, i32 %6)
  %m_sz = getelementptr inbounds %struct.btElement, %struct.btElement* %call5, i32 0, i32 1
  %7 = load i32, i32* %m_sz, align 4
  %m_elements6 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %8 = load i32, i32* %j, align 4
  %call7 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.15* %m_elements6, i32 %8)
  %m_sz8 = getelementptr inbounds %struct.btElement, %struct.btElement* %call7, i32 0, i32 1
  %9 = load i32, i32* %m_sz8, align 4
  %add = add nsw i32 %9, %7
  store i32 %add, i32* %m_sz8, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_islandTag1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 13
  %0 = load i32, i32* %m_islandTag1, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btTypedConstraint9isEnabledEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_isEnabled = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 5
  %0 = load i8, i8* %m_isEnabled, align 4
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld32createPredictiveContactsInternalEPP11btRigidBodyif(%class.btDiscreteDynamicsWorld* %this, %class.btRigidBody** %bodies, i32 %numBodies, float %timeStep) #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %bodies.addr = alloca %class.btRigidBody**, align 4
  %numBodies.addr = alloca i32, align 4
  %timeStep.addr = alloca float, align 4
  %predictedTrans = alloca %class.btTransform, align 4
  %i = alloca i32, align 4
  %body = alloca %class.btRigidBody*, align 4
  %squareMotion = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %sweepResults = alloca %class.btClosestNotMeConvexResultCallback, align 4
  %tmpSphere = alloca %class.btSphereShape, align 4
  %modifiedPredictedTrans = alloca %class.btTransform, align 4
  %distVec = alloca %class.btVector3, align 4
  %ref.tmp42 = alloca %class.btVector3, align 4
  %distance = alloca float, align 4
  %ref.tmp47 = alloca %class.btVector3, align 4
  %manifold = alloca %class.btPersistentManifold*, align 4
  %worldPointB = alloca %class.btVector3, align 4
  %localPointB = alloca %class.btVector3, align 4
  %ref.tmp55 = alloca %class.btTransform, align 4
  %newPoint = alloca %class.btManifoldPoint, align 4
  %ref.tmp58 = alloca %class.btVector3, align 4
  %ref.tmp59 = alloca float, align 4
  %ref.tmp60 = alloca float, align 4
  %ref.tmp61 = alloca float, align 4
  %isPredictive = alloca i8, align 1
  %index = alloca i32, align 4
  %pt = alloca %class.btManifoldPoint*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btRigidBody** %bodies, %class.btRigidBody*** %bodies.addr, align 4
  store i32 %numBodies, i32* %numBodies.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %predictedTrans)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %numBodies.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btRigidBody**, %class.btRigidBody*** %bodies.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btRigidBody*, %class.btRigidBody** %2, i32 %3
  %4 = load %class.btRigidBody*, %class.btRigidBody** %arrayidx, align 4
  store %class.btRigidBody* %4, %class.btRigidBody** %body, align 4
  %5 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %6 = bitcast %class.btRigidBody* %5 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject14setHitFractionEf(%class.btCollisionObject* %6, float 1.000000e+00)
  %7 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %8 = bitcast %class.btRigidBody* %7 to %class.btCollisionObject*
  %call2 = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %8)
  br i1 %call2, label %land.lhs.true, label %if.end77

land.lhs.true:                                    ; preds = %for.body
  %9 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %10 = bitcast %class.btRigidBody* %9 to %class.btCollisionObject*
  %call3 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %10)
  br i1 %call3, label %if.end77, label %if.then

if.then:                                          ; preds = %land.lhs.true
  %11 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %12 = load float, float* %timeStep.addr, align 4
  call void @_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform(%class.btRigidBody* %11, float %12, %class.btTransform* nonnull align 4 dereferenceable(64) %predictedTrans)
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %predictedTrans)
  %13 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %14 = bitcast %class.btRigidBody* %13 to %class.btCollisionObject*
  %call5 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %14)
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call5)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call4, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  %call7 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp)
  store float %call7, float* %squareMotion, align 4
  %15 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %call8 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %15)
  %m_useContinuous = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %call8, i32 0, i32 4
  %16 = load i8, i8* %m_useContinuous, align 4
  %tobool = trunc i8 %16 to i1
  br i1 %tobool, label %land.lhs.true9, label %if.end76

land.lhs.true9:                                   ; preds = %if.then
  %17 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %18 = bitcast %class.btRigidBody* %17 to %class.btCollisionObject*
  %call10 = call float @_ZNK17btCollisionObject27getCcdSquareMotionThresholdEv(%class.btCollisionObject* %18)
  %tobool11 = fcmp une float %call10, 0.000000e+00
  br i1 %tobool11, label %land.lhs.true12, label %if.end76

land.lhs.true12:                                  ; preds = %land.lhs.true9
  %19 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %20 = bitcast %class.btRigidBody* %19 to %class.btCollisionObject*
  %call13 = call float @_ZNK17btCollisionObject27getCcdSquareMotionThresholdEv(%class.btCollisionObject* %20)
  %21 = load float, float* %squareMotion, align 4
  %cmp14 = fcmp olt float %call13, %21
  br i1 %cmp14, label %if.then15, label %if.end76

if.then15:                                        ; preds = %land.lhs.true12
  %call16 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.8, i32 0, i32 0))
  %22 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %call17 = call %class.btCollisionShape* @_ZN11btRigidBody17getCollisionShapeEv(%class.btRigidBody* %22)
  %call18 = call zeroext i1 @_ZNK16btCollisionShape8isConvexEv(%class.btCollisionShape* %call17)
  br i1 %call18, label %if.then19, label %if.end74

if.then19:                                        ; preds = %if.then15
  %23 = load i32, i32* @gNumClampedCcdMotions, align 4
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* @gNumClampedCcdMotions, align 4
  %24 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %25 = bitcast %class.btRigidBody* %24 to %class.btCollisionObject*
  %26 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %27 = bitcast %class.btRigidBody* %26 to %class.btCollisionObject*
  %call20 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %27)
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call20)
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %predictedTrans)
  %28 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %call23 = call %class.btBroadphaseInterface* @_ZN16btCollisionWorld13getBroadphaseEv(%class.btCollisionWorld* %28)
  %29 = bitcast %class.btBroadphaseInterface* %call23 to %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)***
  %vtable = load %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)**, %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)*** %29, align 4
  %vfn = getelementptr inbounds %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)*, %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)** %vtable, i64 9
  %30 = load %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)*, %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)** %vfn, align 4
  %call24 = call %class.btOverlappingPairCache* %30(%class.btBroadphaseInterface* %call23)
  %31 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %call25 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %31)
  %call26 = call %class.btClosestNotMeConvexResultCallback* @_ZN34btClosestNotMeConvexResultCallbackC2EP17btCollisionObjectRK9btVector3S4_P22btOverlappingPairCacheP12btDispatcher(%class.btClosestNotMeConvexResultCallback* %sweepResults, %class.btCollisionObject* %25, %class.btVector3* nonnull align 4 dereferenceable(16) %call21, %class.btVector3* nonnull align 4 dereferenceable(16) %call22, %class.btOverlappingPairCache* %call24, %class.btDispatcher* %call25)
  %32 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %33 = bitcast %class.btRigidBody* %32 to %class.btCollisionObject*
  %call27 = call float @_ZNK17btCollisionObject23getCcdSweptSphereRadiusEv(%class.btCollisionObject* %33)
  %call28 = call %class.btSphereShape* @_ZN13btSphereShapeC2Ef(%class.btSphereShape* %tmpSphere, float %call27)
  %34 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %call29 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %34)
  %m_allowedCcdPenetration = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %call29, i32 0, i32 9
  %35 = load float, float* %m_allowedCcdPenetration, align 4
  %m_allowedPenetration = getelementptr inbounds %class.btClosestNotMeConvexResultCallback, %class.btClosestNotMeConvexResultCallback* %sweepResults, i32 0, i32 2
  store float %35, float* %m_allowedPenetration, align 4
  %36 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %call30 = call %struct.btBroadphaseProxy* @_ZN11btRigidBody18getBroadphaseProxyEv(%class.btRigidBody* %36)
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call30, i32 0, i32 1
  %37 = load i32, i32* %m_collisionFilterGroup, align 4
  %38 = bitcast %class.btClosestNotMeConvexResultCallback* %sweepResults to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterGroup31 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %38, i32 0, i32 2
  store i32 %37, i32* %m_collisionFilterGroup31, align 4
  %39 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %call32 = call %struct.btBroadphaseProxy* @_ZN11btRigidBody18getBroadphaseProxyEv(%class.btRigidBody* %39)
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call32, i32 0, i32 2
  %40 = load i32, i32* %m_collisionFilterMask, align 4
  %41 = bitcast %class.btClosestNotMeConvexResultCallback* %sweepResults to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterMask33 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %41, i32 0, i32 3
  store i32 %40, i32* %m_collisionFilterMask33, align 4
  %call34 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %modifiedPredictedTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %predictedTrans)
  %42 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %43 = bitcast %class.btRigidBody* %42 to %class.btCollisionObject*
  %call35 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %43)
  %call36 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %call35)
  call void @_ZN11btTransform8setBasisERK11btMatrix3x3(%class.btTransform* %modifiedPredictedTrans, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call36)
  %44 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %45 = bitcast %class.btSphereShape* %tmpSphere to %class.btConvexShape*
  %46 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %47 = bitcast %class.btRigidBody* %46 to %class.btCollisionObject*
  %call37 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %47)
  %48 = bitcast %class.btClosestNotMeConvexResultCallback* %sweepResults to %"struct.btCollisionWorld::ConvexResultCallback"*
  call void @_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf(%class.btCollisionWorld* %44, %class.btConvexShape* %45, %class.btTransform* nonnull align 4 dereferenceable(64) %call37, %class.btTransform* nonnull align 4 dereferenceable(64) %modifiedPredictedTrans, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(16) %48, float 0.000000e+00)
  %49 = bitcast %class.btClosestNotMeConvexResultCallback* %sweepResults to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call38 = call zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %49)
  br i1 %call38, label %land.lhs.true39, label %if.end

land.lhs.true39:                                  ; preds = %if.then19
  %50 = bitcast %class.btClosestNotMeConvexResultCallback* %sweepResults to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %50, i32 0, i32 1
  %51 = load float, float* %m_closestHitFraction, align 4
  %cmp40 = fcmp olt float %51, 1.000000e+00
  br i1 %cmp40, label %if.then41, label %if.end

if.then41:                                        ; preds = %land.lhs.true39
  %call43 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %predictedTrans)
  %52 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %53 = bitcast %class.btRigidBody* %52 to %class.btCollisionObject*
  %call44 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %53)
  %call45 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call44)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp42, %class.btVector3* nonnull align 4 dereferenceable(16) %call43, %class.btVector3* nonnull align 4 dereferenceable(16) %call45)
  %54 = bitcast %class.btClosestNotMeConvexResultCallback* %sweepResults to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_closestHitFraction46 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %54, i32 0, i32 1
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %distVec, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp42, float* nonnull align 4 dereferenceable(4) %m_closestHitFraction46)
  %55 = bitcast %class.btClosestNotMeConvexResultCallback* %sweepResults to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %m_hitNormalWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %55, i32 0, i32 3
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp47, %class.btVector3* nonnull align 4 dereferenceable(16) %m_hitNormalWorld)
  %call48 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %distVec, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp47)
  store float %call48, float* %distance, align 4
  %56 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_dispatcher1 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %56, i32 0, i32 2
  %57 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  %58 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %59 = bitcast %class.btRigidBody* %58 to %class.btCollisionObject*
  %60 = bitcast %class.btClosestNotMeConvexResultCallback* %sweepResults to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %m_hitCollisionObject = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %60, i32 0, i32 5
  %61 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject, align 4
  %62 = bitcast %class.btDispatcher* %57 to %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable49 = load %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)**, %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*** %62, align 4
  %vfn50 = getelementptr inbounds %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vtable49, i64 3
  %63 = load %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vfn50, align 4
  %call51 = call %class.btPersistentManifold* %63(%class.btDispatcher* %57, %class.btCollisionObject* %59, %class.btCollisionObject* %61)
  store %class.btPersistentManifold* %call51, %class.btPersistentManifold** %manifold, align 4
  %m_predictiveManifoldsMutex = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 19
  call void @_Z11btMutexLockP11btSpinMutex(%class.btSpinMutex* %m_predictiveManifoldsMutex)
  %m_predictiveManifolds = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 18
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.9* %m_predictiveManifolds, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %manifold)
  %m_predictiveManifoldsMutex52 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 19
  call void @_Z13btMutexUnlockP11btSpinMutex(%class.btSpinMutex* %m_predictiveManifoldsMutex52)
  %64 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %65 = bitcast %class.btRigidBody* %64 to %class.btCollisionObject*
  %call53 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %65)
  %call54 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call53)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %worldPointB, %class.btVector3* nonnull align 4 dereferenceable(16) %call54, %class.btVector3* nonnull align 4 dereferenceable(16) %distVec)
  %66 = bitcast %class.btClosestNotMeConvexResultCallback* %sweepResults to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %m_hitCollisionObject56 = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %66, i32 0, i32 5
  %67 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject56, align 4
  %call57 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %67)
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp55, %class.btTransform* %call57)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %localPointB, %class.btTransform* %ref.tmp55, %class.btVector3* nonnull align 4 dereferenceable(16) %worldPointB)
  store float 0.000000e+00, float* %ref.tmp59, align 4
  store float 0.000000e+00, float* %ref.tmp60, align 4
  store float 0.000000e+00, float* %ref.tmp61, align 4
  %call62 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp58, float* nonnull align 4 dereferenceable(4) %ref.tmp59, float* nonnull align 4 dereferenceable(4) %ref.tmp60, float* nonnull align 4 dereferenceable(4) %ref.tmp61)
  %68 = bitcast %class.btClosestNotMeConvexResultCallback* %sweepResults to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %m_hitNormalWorld63 = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %68, i32 0, i32 3
  %69 = load float, float* %distance, align 4
  %call64 = call %class.btManifoldPoint* @_ZN15btManifoldPointC2ERK9btVector3S2_S2_f(%class.btManifoldPoint* %newPoint, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp58, %class.btVector3* nonnull align 4 dereferenceable(16) %localPointB, %class.btVector3* nonnull align 4 dereferenceable(16) %m_hitNormalWorld63, float %69)
  store i8 1, i8* %isPredictive, align 1
  %70 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %71 = load i8, i8* %isPredictive, align 1
  %tobool65 = trunc i8 %71 to i1
  %call66 = call i32 @_ZN20btPersistentManifold16addManifoldPointERK15btManifoldPointb(%class.btPersistentManifold* %70, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %newPoint, i1 zeroext %tobool65)
  store i32 %call66, i32* %index, align 4
  %72 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %73 = load i32, i32* %index, align 4
  %call67 = call nonnull align 4 dereferenceable(192) %class.btManifoldPoint* @_ZN20btPersistentManifold15getContactPointEi(%class.btPersistentManifold* %72, i32 %73)
  store %class.btManifoldPoint* %call67, %class.btManifoldPoint** %pt, align 4
  %74 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4
  %m_combinedRestitution = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %74, i32 0, i32 9
  store float 0.000000e+00, float* %m_combinedRestitution, align 4
  %75 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %76 = bitcast %class.btRigidBody* %75 to %class.btCollisionObject*
  %77 = bitcast %class.btClosestNotMeConvexResultCallback* %sweepResults to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %m_hitCollisionObject68 = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %77, i32 0, i32 5
  %78 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject68, align 4
  %call69 = call float @_ZN16btManifoldResult25calculateCombinedFrictionEPK17btCollisionObjectS2_(%class.btCollisionObject* %76, %class.btCollisionObject* %78)
  %79 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4
  %m_combinedFriction = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %79, i32 0, i32 6
  store float %call69, float* %m_combinedFriction, align 4
  %80 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %81 = bitcast %class.btRigidBody* %80 to %class.btCollisionObject*
  %call70 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %81)
  %call71 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call70)
  %82 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4
  %m_positionWorldOnA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %82, i32 0, i32 3
  %83 = bitcast %class.btVector3* %m_positionWorldOnA to i8*
  %84 = bitcast %class.btVector3* %call71 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %83, i8* align 4 %84, i32 16, i1 false)
  %85 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4
  %m_positionWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %85, i32 0, i32 2
  %86 = bitcast %class.btVector3* %m_positionWorldOnB to i8*
  %87 = bitcast %class.btVector3* %worldPointB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %86, i8* align 4 %87, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then41, %land.lhs.true39, %if.then19
  %call72 = call %class.btSphereShape* @_ZN13btSphereShapeD2Ev(%class.btSphereShape* %tmpSphere) #10
  %call73 = call %class.btClosestNotMeConvexResultCallback* @_ZN34btClosestNotMeConvexResultCallbackD2Ev(%class.btClosestNotMeConvexResultCallback* %sweepResults) #10
  br label %if.end74

if.end74:                                         ; preds = %if.end, %if.then15
  %call75 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  br label %if.end76

if.end76:                                         ; preds = %if.end74, %land.lhs.true12, %land.lhs.true9, %if.then
  br label %if.end77

if.end77:                                         ; preds = %if.end76, %land.lhs.true, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end77
  %88 = load i32, i32* %i, align 4
  %inc78 = add nsw i32 %88, 1
  store i32 %inc78, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btCollisionObject14setHitFractionEf(%class.btCollisionObject* %this, float %hitFraction) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %hitFraction.addr = alloca float, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store float %hitFraction, float* %hitFraction.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load float, float* %hitFraction.addr, align 4
  %m_hitFraction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 28
  store float %0, float* %m_hitFraction, align 4
  ret void
}

declare void @_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform(%class.btRigidBody*, float, %class.btTransform* nonnull align 4 dereferenceable(64)) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK17btCollisionObject27getCcdSquareMotionThresholdEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_ccdMotionThreshold = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 30
  %0 = load float, float* %m_ccdMotionThreshold, align 4
  %m_ccdMotionThreshold2 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 30
  %1 = load float, float* %m_ccdMotionThreshold2, align 4
  %mul = fmul float %0, %1
  ret float %mul
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionShape8isConvexEv(%class.btCollisionShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %call = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this1)
  %call2 = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %call)
  ret i1 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btBroadphaseInterface* @_ZN16btCollisionWorld13getBroadphaseEv(%class.btCollisionWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_broadphasePairCache = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 4
  %0 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %m_broadphasePairCache, align 4
  ret %class.btBroadphaseInterface* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btClosestNotMeConvexResultCallback* @_ZN34btClosestNotMeConvexResultCallbackC2EP17btCollisionObjectRK9btVector3S4_P22btOverlappingPairCacheP12btDispatcher(%class.btClosestNotMeConvexResultCallback* returned %this, %class.btCollisionObject* %me, %class.btVector3* nonnull align 4 dereferenceable(16) %fromA, %class.btVector3* nonnull align 4 dereferenceable(16) %toA, %class.btOverlappingPairCache* %pairCache, %class.btDispatcher* %dispatcher) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btClosestNotMeConvexResultCallback*, align 4
  %me.addr = alloca %class.btCollisionObject*, align 4
  %fromA.addr = alloca %class.btVector3*, align 4
  %toA.addr = alloca %class.btVector3*, align 4
  %pairCache.addr = alloca %class.btOverlappingPairCache*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btClosestNotMeConvexResultCallback* %this, %class.btClosestNotMeConvexResultCallback** %this.addr, align 4
  store %class.btCollisionObject* %me, %class.btCollisionObject** %me.addr, align 4
  store %class.btVector3* %fromA, %class.btVector3** %fromA.addr, align 4
  store %class.btVector3* %toA, %class.btVector3** %toA.addr, align 4
  store %class.btOverlappingPairCache* %pairCache, %class.btOverlappingPairCache** %pairCache.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btClosestNotMeConvexResultCallback*, %class.btClosestNotMeConvexResultCallback** %this.addr, align 4
  %0 = bitcast %class.btClosestNotMeConvexResultCallback* %this1 to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %1 = load %class.btVector3*, %class.btVector3** %fromA.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %toA.addr, align 4
  %call = call %"struct.btCollisionWorld::ClosestConvexResultCallback"* @_ZN16btCollisionWorld27ClosestConvexResultCallbackC2ERK9btVector3S3_(%"struct.btCollisionWorld::ClosestConvexResultCallback"* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %3 = bitcast %class.btClosestNotMeConvexResultCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTV34btClosestNotMeConvexResultCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %3, align 4
  %m_me = getelementptr inbounds %class.btClosestNotMeConvexResultCallback, %class.btClosestNotMeConvexResultCallback* %this1, i32 0, i32 1
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %me.addr, align 4
  store %class.btCollisionObject* %4, %class.btCollisionObject** %m_me, align 4
  %m_allowedPenetration = getelementptr inbounds %class.btClosestNotMeConvexResultCallback, %class.btClosestNotMeConvexResultCallback* %this1, i32 0, i32 2
  store float 0.000000e+00, float* %m_allowedPenetration, align 4
  %m_pairCache = getelementptr inbounds %class.btClosestNotMeConvexResultCallback, %class.btClosestNotMeConvexResultCallback* %this1, i32 0, i32 3
  %5 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCache.addr, align 4
  store %class.btOverlappingPairCache* %5, %class.btOverlappingPairCache** %m_pairCache, align 4
  %m_dispatcher = getelementptr inbounds %class.btClosestNotMeConvexResultCallback, %class.btClosestNotMeConvexResultCallback* %this1, i32 0, i32 4
  %6 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  store %class.btDispatcher* %6, %class.btDispatcher** %m_dispatcher, align 4
  ret %class.btClosestNotMeConvexResultCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK17btCollisionObject23getCcdSweptSphereRadiusEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_ccdSweptSphereRadius = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 29
  %0 = load float, float* %m_ccdSweptSphereRadius, align 4
  ret float %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btSphereShape* @_ZN13btSphereShapeC2Ef(%class.btSphereShape* returned %this, float %radius) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btSphereShape*, align 4
  %radius.addr = alloca float, align 4
  store %class.btSphereShape* %this, %class.btSphereShape** %this.addr, align 4
  store float %radius, float* %radius.addr, align 4
  %this1 = load %class.btSphereShape*, %class.btSphereShape** %this.addr, align 4
  %0 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %call = call %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* %0)
  %1 = bitcast %class.btSphereShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [25 x i8*] }, { [25 x i8*] }* @_ZTV13btSphereShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %2 = bitcast %class.btSphereShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 8, i32* %m_shapeType, align 4
  %3 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %3, i32 0, i32 2
  %4 = load float, float* %radius.addr, align 4
  call void @_ZN9btVector34setXEf(%class.btVector3* %m_implicitShapeDimensions, float %4)
  %5 = load float, float* %radius.addr, align 4
  %6 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %6, i32 0, i32 3
  store float %5, float* %m_collisionMargin, align 4
  ret %class.btSphereShape* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZN11btRigidBody18getBroadphaseProxyEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_broadphaseHandle = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 8
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_broadphaseHandle, align 4
  ret %struct.btBroadphaseProxy* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform8setBasisERK11btMatrix3x3(%class.btTransform* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %basis) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %basis.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btMatrix3x3* %basis, %class.btMatrix3x3** %basis.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %basis.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

declare void @_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf(%class.btCollisionWorld*, %class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64), %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(16), float) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ConvexResultCallback"*, align 4
  store %"struct.btCollisionWorld::ConvexResultCallback"* %this, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  %this1 = load %"struct.btCollisionWorld::ConvexResultCallback"*, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %this1, i32 0, i32 1
  %0 = load float, float* %m_closestHitFraction, align 4
  %cmp = fcmp olt float %0, 1.000000e+00
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z11btMutexLockP11btSpinMutex(%class.btSpinMutex* %0) #1 comdat {
entry:
  %.addr = alloca %class.btSpinMutex*, align 4
  store %class.btSpinMutex* %0, %class.btSpinMutex** %.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.9* %this, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %_Val.addr = alloca %class.btPersistentManifold**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store %class.btPersistentManifold** %_Val, %class.btPersistentManifold*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.9* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.9* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.9* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %1 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %1, i32 %2
  %3 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btPersistentManifold**
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %_Val.addr, align 4
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %5, align 4
  store %class.btPersistentManifold* %6, %class.btPersistentManifold** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z13btMutexUnlockP11btSpinMutex(%class.btSpinMutex* %0) #1 comdat {
entry:
  %.addr = alloca %class.btSpinMutex*, align 4
  store %class.btSpinMutex* %0, %class.btSpinMutex** %.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform7inverseEv(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %inv = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %inv, %class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btManifoldPoint* @_ZN15btManifoldPointC2ERK9btVector3S2_S2_f(%class.btManifoldPoint* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %pointA, %class.btVector3* nonnull align 4 dereferenceable(16) %pointB, %class.btVector3* nonnull align 4 dereferenceable(16) %normal, float %distance) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  %pointA.addr = alloca %class.btVector3*, align 4
  %pointB.addr = alloca %class.btVector3*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %distance.addr = alloca float, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4
  store %class.btVector3* %pointA, %class.btVector3** %pointA.addr, align 4
  store %class.btVector3* %pointB, %class.btVector3** %pointB.addr, align 4
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4
  store float %distance, float* %distance.addr, align 4
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_localPointA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %pointA.addr, align 4
  %1 = bitcast %class.btVector3* %m_localPointA to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %m_localPointB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 1
  %3 = load %class.btVector3*, %class.btVector3** %pointB.addr, align 4
  %4 = bitcast %class.btVector3* %m_localPointB to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %m_positionWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 2
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_positionWorldOnB)
  %m_positionWorldOnA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 3
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_positionWorldOnA)
  %m_normalWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 4
  %6 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %7 = bitcast %class.btVector3* %m_normalWorldOnB to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %m_distance1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 5
  %9 = load float, float* %distance.addr, align 4
  store float %9, float* %m_distance1, align 4
  %m_combinedFriction = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 6
  store float 0.000000e+00, float* %m_combinedFriction, align 4
  %m_combinedRollingFriction = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 7
  store float 0.000000e+00, float* %m_combinedRollingFriction, align 4
  %m_combinedSpinningFriction = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 8
  store float 0.000000e+00, float* %m_combinedSpinningFriction, align 4
  %m_combinedRestitution = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 9
  store float 0.000000e+00, float* %m_combinedRestitution, align 4
  %m_userPersistentData = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 14
  store i8* null, i8** %m_userPersistentData, align 4
  %m_contactPointFlags = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 15
  store i32 0, i32* %m_contactPointFlags, align 4
  %m_appliedImpulse = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 16
  store float 0.000000e+00, float* %m_appliedImpulse, align 4
  %m_appliedImpulseLateral1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 17
  store float 0.000000e+00, float* %m_appliedImpulseLateral1, align 4
  %m_appliedImpulseLateral2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 18
  store float 0.000000e+00, float* %m_appliedImpulseLateral2, align 4
  %m_contactMotion1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 19
  store float 0.000000e+00, float* %m_contactMotion1, align 4
  %m_contactMotion2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 20
  store float 0.000000e+00, float* %m_contactMotion2, align 4
  %10 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 21
  %m_contactCFM = bitcast %union.anon.12* %10 to float*
  store float 0.000000e+00, float* %m_contactCFM, align 4
  %11 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 22
  %m_contactERP = bitcast %union.anon.13* %11 to float*
  store float 0.000000e+00, float* %m_contactERP, align 4
  %m_frictionCFM = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 23
  store float 0.000000e+00, float* %m_frictionCFM, align 4
  %m_lifeTime = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 24
  store i32 0, i32* %m_lifeTime, align 4
  %m_lateralFrictionDir1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 25
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_lateralFrictionDir1)
  %m_lateralFrictionDir2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 26
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_lateralFrictionDir2)
  ret %class.btManifoldPoint* %this1
}

declare i32 @_ZN20btPersistentManifold16addManifoldPointERK15btManifoldPointb(%class.btPersistentManifold*, %class.btManifoldPoint* nonnull align 4 dereferenceable(192), i1 zeroext) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(192) %class.btManifoldPoint* @_ZN20btPersistentManifold15getContactPointEi(%class.btPersistentManifold* %this, i32 %index) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  %index.addr = alloca i32, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_pointCache = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache, i32 0, i32 %0
  ret %class.btManifoldPoint* %arrayidx
}

declare float @_ZN16btManifoldResult25calculateCombinedFrictionEPK17btCollisionObjectS2_(%class.btCollisionObject*, %class.btCollisionObject*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btSphereShape* @_ZN13btSphereShapeD2Ev(%class.btSphereShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSphereShape*, align 4
  store %class.btSphereShape* %this, %class.btSphereShape** %this.addr, align 4
  %this1 = load %class.btSphereShape*, %class.btSphereShape** %this.addr, align 4
  %0 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %call = call %class.btConvexInternalShape* @_ZN21btConvexInternalShapeD2Ev(%class.btConvexInternalShape* %0) #10
  ret %class.btSphereShape* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btClosestNotMeConvexResultCallback* @_ZN34btClosestNotMeConvexResultCallbackD2Ev(%class.btClosestNotMeConvexResultCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btClosestNotMeConvexResultCallback*, align 4
  store %class.btClosestNotMeConvexResultCallback* %this, %class.btClosestNotMeConvexResultCallback** %this.addr, align 4
  %this1 = load %class.btClosestNotMeConvexResultCallback*, %class.btClosestNotMeConvexResultCallback** %this.addr, align 4
  %0 = bitcast %class.btClosestNotMeConvexResultCallback* %this1 to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %call = call %"struct.btCollisionWorld::ClosestConvexResultCallback"* @_ZN16btCollisionWorld27ClosestConvexResultCallbackD2Ev(%"struct.btCollisionWorld::ClosestConvexResultCallback"* %0) #10
  ret %class.btClosestNotMeConvexResultCallback* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld25releasePredictiveContactsEv(%class.btDiscreteDynamicsWorld* %this) #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %i = alloca i32, align 4
  %manifold = alloca %class.btPersistentManifold*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.9, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_predictiveManifolds = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 18
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %m_predictiveManifolds)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_predictiveManifolds3 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 18
  %1 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.9* %m_predictiveManifolds3, i32 %1)
  %2 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call4, align 4
  store %class.btPersistentManifold* %2, %class.btPersistentManifold** %manifold, align 4
  %3 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_dispatcher1 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %3, i32 0, i32 2
  %4 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  %5 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %6 = bitcast %class.btDispatcher* %4 to void (%class.btDispatcher*, %class.btPersistentManifold*)***
  %vtable = load void (%class.btDispatcher*, %class.btPersistentManifold*)**, void (%class.btDispatcher*, %class.btPersistentManifold*)*** %6, align 4
  %vfn = getelementptr inbounds void (%class.btDispatcher*, %class.btPersistentManifold*)*, void (%class.btDispatcher*, %class.btPersistentManifold*)** %vtable, i64 4
  %7 = load void (%class.btDispatcher*, %class.btPersistentManifold*)*, void (%class.btDispatcher*, %class.btPersistentManifold*)** %vfn, align 4
  call void %7(%class.btDispatcher* %4, %class.btPersistentManifold* %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_predictiveManifolds5 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 18
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.9* %m_predictiveManifolds5)
  %call6 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.9* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.9* %this1)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld24createPredictiveContactsEf(%class.btDiscreteDynamicsWorld* %this, float %timeStep) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.10, i32 0, i32 0))
  call void @_ZN23btDiscreteDynamicsWorld25releasePredictiveContactsEv(%class.btDiscreteDynamicsWorld* %this1)
  %m_nonStaticRigidBodies = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies)
  %cmp = icmp sgt i32 %call2, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_nonStaticRigidBodies3 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %call4 = call nonnull align 4 dereferenceable(4) %class.btRigidBody** @_ZN20btAlignedObjectArrayIP11btRigidBodyEixEi(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies3, i32 0)
  %m_nonStaticRigidBodies5 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %call6 = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies5)
  %0 = load float, float* %timeStep.addr, align 4
  call void @_ZN23btDiscreteDynamicsWorld32createPredictiveContactsInternalEPP11btRigidBodyif(%class.btDiscreteDynamicsWorld* %this1, %class.btRigidBody** %call4, i32 %call6, float %0)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %call7 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld27integrateTransformsInternalEPP11btRigidBodyif(%class.btDiscreteDynamicsWorld* %this, %class.btRigidBody** %bodies, i32 %numBodies, float %timeStep) #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %bodies.addr = alloca %class.btRigidBody**, align 4
  %numBodies.addr = alloca i32, align 4
  %timeStep.addr = alloca float, align 4
  %predictedTrans = alloca %class.btTransform, align 4
  %i = alloca i32, align 4
  %body = alloca %class.btRigidBody*, align 4
  %squareMotion = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %sweepResults = alloca %class.btClosestNotMeConvexResultCallback, align 4
  %tmpSphere = alloca %class.btSphereShape, align 4
  %modifiedPredictedTrans = alloca %class.btTransform, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btRigidBody** %bodies, %class.btRigidBody*** %bodies.addr, align 4
  store i32 %numBodies, i32* %numBodies.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %predictedTrans)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %numBodies.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btRigidBody**, %class.btRigidBody*** %bodies.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btRigidBody*, %class.btRigidBody** %2, i32 %3
  %4 = load %class.btRigidBody*, %class.btRigidBody** %arrayidx, align 4
  store %class.btRigidBody* %4, %class.btRigidBody** %body, align 4
  %5 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %6 = bitcast %class.btRigidBody* %5 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject14setHitFractionEf(%class.btCollisionObject* %6, float 1.000000e+00)
  %7 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %8 = bitcast %class.btRigidBody* %7 to %class.btCollisionObject*
  %call2 = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %8)
  br i1 %call2, label %land.lhs.true, label %if.end53

land.lhs.true:                                    ; preds = %for.body
  %9 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %10 = bitcast %class.btRigidBody* %9 to %class.btCollisionObject*
  %call3 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %10)
  br i1 %call3, label %if.end53, label %if.then

if.then:                                          ; preds = %land.lhs.true
  %11 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %12 = load float, float* %timeStep.addr, align 4
  call void @_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform(%class.btRigidBody* %11, float %12, %class.btTransform* nonnull align 4 dereferenceable(64) %predictedTrans)
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %predictedTrans)
  %13 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %14 = bitcast %class.btRigidBody* %13 to %class.btCollisionObject*
  %call5 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %14)
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call5)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call4, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  %call7 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp)
  store float %call7, float* %squareMotion, align 4
  %15 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %call8 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %15)
  %m_useContinuous = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %call8, i32 0, i32 4
  %16 = load i8, i8* %m_useContinuous, align 4
  %tobool = trunc i8 %16 to i1
  br i1 %tobool, label %land.lhs.true9, label %if.end52

land.lhs.true9:                                   ; preds = %if.then
  %17 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %18 = bitcast %class.btRigidBody* %17 to %class.btCollisionObject*
  %call10 = call float @_ZNK17btCollisionObject27getCcdSquareMotionThresholdEv(%class.btCollisionObject* %18)
  %tobool11 = fcmp une float %call10, 0.000000e+00
  br i1 %tobool11, label %land.lhs.true12, label %if.end52

land.lhs.true12:                                  ; preds = %land.lhs.true9
  %19 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %20 = bitcast %class.btRigidBody* %19 to %class.btCollisionObject*
  %call13 = call float @_ZNK17btCollisionObject27getCcdSquareMotionThresholdEv(%class.btCollisionObject* %20)
  %21 = load float, float* %squareMotion, align 4
  %cmp14 = fcmp olt float %call13, %21
  br i1 %cmp14, label %if.then15, label %if.end52

if.then15:                                        ; preds = %land.lhs.true12
  %call16 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.11, i32 0, i32 0))
  %22 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %call17 = call %class.btCollisionShape* @_ZN11btRigidBody17getCollisionShapeEv(%class.btRigidBody* %22)
  %call18 = call zeroext i1 @_ZNK16btCollisionShape8isConvexEv(%class.btCollisionShape* %call17)
  br i1 %call18, label %if.then19, label %if.end47

if.then19:                                        ; preds = %if.then15
  %23 = load i32, i32* @gNumClampedCcdMotions, align 4
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* @gNumClampedCcdMotions, align 4
  %24 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %25 = bitcast %class.btRigidBody* %24 to %class.btCollisionObject*
  %26 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %27 = bitcast %class.btRigidBody* %26 to %class.btCollisionObject*
  %call20 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %27)
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call20)
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %predictedTrans)
  %28 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %call23 = call %class.btBroadphaseInterface* @_ZN16btCollisionWorld13getBroadphaseEv(%class.btCollisionWorld* %28)
  %29 = bitcast %class.btBroadphaseInterface* %call23 to %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)***
  %vtable = load %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)**, %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)*** %29, align 4
  %vfn = getelementptr inbounds %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)*, %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)** %vtable, i64 9
  %30 = load %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)*, %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)** %vfn, align 4
  %call24 = call %class.btOverlappingPairCache* %30(%class.btBroadphaseInterface* %call23)
  %31 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %call25 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %31)
  %call26 = call %class.btClosestNotMeConvexResultCallback* @_ZN34btClosestNotMeConvexResultCallbackC2EP17btCollisionObjectRK9btVector3S4_P22btOverlappingPairCacheP12btDispatcher(%class.btClosestNotMeConvexResultCallback* %sweepResults, %class.btCollisionObject* %25, %class.btVector3* nonnull align 4 dereferenceable(16) %call21, %class.btVector3* nonnull align 4 dereferenceable(16) %call22, %class.btOverlappingPairCache* %call24, %class.btDispatcher* %call25)
  %32 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %33 = bitcast %class.btRigidBody* %32 to %class.btCollisionObject*
  %call27 = call float @_ZNK17btCollisionObject23getCcdSweptSphereRadiusEv(%class.btCollisionObject* %33)
  %call28 = call %class.btSphereShape* @_ZN13btSphereShapeC2Ef(%class.btSphereShape* %tmpSphere, float %call27)
  %34 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %call29 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %34)
  %m_allowedCcdPenetration = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %call29, i32 0, i32 9
  %35 = load float, float* %m_allowedCcdPenetration, align 4
  %m_allowedPenetration = getelementptr inbounds %class.btClosestNotMeConvexResultCallback, %class.btClosestNotMeConvexResultCallback* %sweepResults, i32 0, i32 2
  store float %35, float* %m_allowedPenetration, align 4
  %36 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %call30 = call %struct.btBroadphaseProxy* @_ZN11btRigidBody18getBroadphaseProxyEv(%class.btRigidBody* %36)
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call30, i32 0, i32 1
  %37 = load i32, i32* %m_collisionFilterGroup, align 4
  %38 = bitcast %class.btClosestNotMeConvexResultCallback* %sweepResults to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterGroup31 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %38, i32 0, i32 2
  store i32 %37, i32* %m_collisionFilterGroup31, align 4
  %39 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %call32 = call %struct.btBroadphaseProxy* @_ZN11btRigidBody18getBroadphaseProxyEv(%class.btRigidBody* %39)
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call32, i32 0, i32 2
  %40 = load i32, i32* %m_collisionFilterMask, align 4
  %41 = bitcast %class.btClosestNotMeConvexResultCallback* %sweepResults to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterMask33 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %41, i32 0, i32 3
  store i32 %40, i32* %m_collisionFilterMask33, align 4
  %call34 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %modifiedPredictedTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %predictedTrans)
  %42 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %43 = bitcast %class.btRigidBody* %42 to %class.btCollisionObject*
  %call35 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %43)
  %call36 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %call35)
  call void @_ZN11btTransform8setBasisERK11btMatrix3x3(%class.btTransform* %modifiedPredictedTrans, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call36)
  %44 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %45 = bitcast %class.btSphereShape* %tmpSphere to %class.btConvexShape*
  %46 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %47 = bitcast %class.btRigidBody* %46 to %class.btCollisionObject*
  %call37 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %47)
  %48 = bitcast %class.btClosestNotMeConvexResultCallback* %sweepResults to %"struct.btCollisionWorld::ConvexResultCallback"*
  call void @_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf(%class.btCollisionWorld* %44, %class.btConvexShape* %45, %class.btTransform* nonnull align 4 dereferenceable(64) %call37, %class.btTransform* nonnull align 4 dereferenceable(64) %modifiedPredictedTrans, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(16) %48, float 0.000000e+00)
  %49 = bitcast %class.btClosestNotMeConvexResultCallback* %sweepResults to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call38 = call zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %49)
  br i1 %call38, label %land.lhs.true39, label %if.end

land.lhs.true39:                                  ; preds = %if.then19
  %50 = bitcast %class.btClosestNotMeConvexResultCallback* %sweepResults to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %50, i32 0, i32 1
  %51 = load float, float* %m_closestHitFraction, align 4
  %cmp40 = fcmp olt float %51, 1.000000e+00
  br i1 %cmp40, label %if.then41, label %if.end

if.then41:                                        ; preds = %land.lhs.true39
  %52 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %53 = bitcast %class.btRigidBody* %52 to %class.btCollisionObject*
  %54 = bitcast %class.btClosestNotMeConvexResultCallback* %sweepResults to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_closestHitFraction42 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %54, i32 0, i32 1
  %55 = load float, float* %m_closestHitFraction42, align 4
  call void @_ZN17btCollisionObject14setHitFractionEf(%class.btCollisionObject* %53, float %55)
  %56 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %57 = load float, float* %timeStep.addr, align 4
  %58 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %59 = bitcast %class.btRigidBody* %58 to %class.btCollisionObject*
  %call43 = call float @_ZNK17btCollisionObject14getHitFractionEv(%class.btCollisionObject* %59)
  %mul = fmul float %57, %call43
  call void @_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform(%class.btRigidBody* %56, float %mul, %class.btTransform* nonnull align 4 dereferenceable(64) %predictedTrans)
  %60 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %61 = bitcast %class.btRigidBody* %60 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject14setHitFractionEf(%class.btCollisionObject* %61, float 0.000000e+00)
  %62 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  call void @_ZN11btRigidBody18proceedToTransformERK11btTransform(%class.btRigidBody* %62, %class.btTransform* nonnull align 4 dereferenceable(64) %predictedTrans)
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %land.lhs.true39, %if.then19
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then41
  %call44 = call %class.btSphereShape* @_ZN13btSphereShapeD2Ev(%class.btSphereShape* %tmpSphere) #10
  %call46 = call %class.btClosestNotMeConvexResultCallback* @_ZN34btClosestNotMeConvexResultCallbackD2Ev(%class.btClosestNotMeConvexResultCallback* %sweepResults) #10
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup48 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end47

if.end47:                                         ; preds = %cleanup.cont, %if.then15
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup48

cleanup48:                                        ; preds = %if.end47, %cleanup
  %call49 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  %cleanup.dest50 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest50, label %unreachable [
    i32 0, label %cleanup.cont51
    i32 4, label %for.inc
  ]

cleanup.cont51:                                   ; preds = %cleanup48
  br label %if.end52

if.end52:                                         ; preds = %cleanup.cont51, %land.lhs.true12, %land.lhs.true9, %if.then
  %63 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  call void @_ZN11btRigidBody18proceedToTransformERK11btTransform(%class.btRigidBody* %63, %class.btTransform* nonnull align 4 dereferenceable(64) %predictedTrans)
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %land.lhs.true, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end53, %cleanup48
  %64 = load i32, i32* %i, align 4
  %inc54 = add nsw i32 %64, 1
  store i32 %inc54, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void

unreachable:                                      ; preds = %cleanup48
  unreachable
}

declare void @_ZN11btRigidBody18proceedToTransformERK11btTransform(%class.btRigidBody*, %class.btTransform* nonnull align 4 dereferenceable(64)) #3

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld19integrateTransformsEf(%class.btDiscreteDynamicsWorld* %this, float %timeStep) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %__profile8 = alloca %class.CProfileSample, align 1
  %i = alloca i32, align 4
  %manifold = alloca %class.btPersistentManifold*, align 4
  %body0 = alloca %class.btRigidBody*, align 4
  %body1 = alloca %class.btRigidBody*, align 4
  %p = alloca i32, align 4
  %pt = alloca %class.btManifoldPoint*, align 4
  %combinedRestitution = alloca float, align 4
  %imp = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %pos1 = alloca %class.btVector3*, align 4
  %pos2 = alloca %class.btVector3*, align 4
  %rel_pos0 = alloca %class.btVector3, align 4
  %rel_pos1 = alloca %class.btVector3, align 4
  %ref.tmp40 = alloca %class.btVector3, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.12, i32 0, i32 0))
  %m_nonStaticRigidBodies = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies)
  %cmp = icmp sgt i32 %call2, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_nonStaticRigidBodies3 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %call4 = call nonnull align 4 dereferenceable(4) %class.btRigidBody** @_ZN20btAlignedObjectArrayIP11btRigidBodyEixEi(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies3, i32 0)
  %m_nonStaticRigidBodies5 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %call6 = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies5)
  %0 = load float, float* %timeStep.addr, align 4
  call void @_ZN23btDiscreteDynamicsWorld27integrateTransformsInternalEPP11btRigidBodyif(%class.btDiscreteDynamicsWorld* %this1, %class.btRigidBody** %call4, i32 %call6, float %0)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_applySpeculativeContactRestitution = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 13
  %1 = load i8, i8* %m_applySpeculativeContactRestitution, align 1
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then7, label %if.end47

if.then7:                                         ; preds = %if.end
  %call9 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile8, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.13, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc43, %if.then7
  %2 = load i32, i32* %i, align 4
  %m_predictiveManifolds = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 18
  %call10 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %m_predictiveManifolds)
  %cmp11 = icmp slt i32 %2, %call10
  br i1 %cmp11, label %for.body, label %for.end45

for.body:                                         ; preds = %for.cond
  %m_predictiveManifolds12 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 18
  %3 = load i32, i32* %i, align 4
  %call13 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.9* %m_predictiveManifolds12, i32 %3)
  %4 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call13, align 4
  store %class.btPersistentManifold* %4, %class.btPersistentManifold** %manifold, align 4
  %5 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %call14 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %5)
  %call15 = call %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %call14)
  store %class.btRigidBody* %call15, %class.btRigidBody** %body0, align 4
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %call16 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %6)
  %call17 = call %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %call16)
  store %class.btRigidBody* %call17, %class.btRigidBody** %body1, align 4
  store i32 0, i32* %p, align 4
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc, %for.body
  %7 = load i32, i32* %p, align 4
  %8 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %call19 = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %8)
  %cmp20 = icmp slt i32 %7, %call19
  br i1 %cmp20, label %for.body21, label %for.end

for.body21:                                       ; preds = %for.cond18
  %9 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %10 = load i32, i32* %p, align 4
  %call22 = call nonnull align 4 dereferenceable(192) %class.btManifoldPoint* @_ZN20btPersistentManifold15getContactPointEi(%class.btPersistentManifold* %9, i32 %10)
  store %class.btManifoldPoint* %call22, %class.btManifoldPoint** %pt, align 4
  %11 = load %class.btRigidBody*, %class.btRigidBody** %body0, align 4
  %12 = bitcast %class.btRigidBody* %11 to %class.btCollisionObject*
  %13 = load %class.btRigidBody*, %class.btRigidBody** %body1, align 4
  %14 = bitcast %class.btRigidBody* %13 to %class.btCollisionObject*
  %call23 = call float @_ZN16btManifoldResult28calculateCombinedRestitutionEPK17btCollisionObjectS2_(%class.btCollisionObject* %12, %class.btCollisionObject* %14)
  store float %call23, float* %combinedRestitution, align 4
  %15 = load float, float* %combinedRestitution, align 4
  %cmp24 = fcmp ogt float %15, 0.000000e+00
  br i1 %cmp24, label %land.lhs.true, label %if.end42

land.lhs.true:                                    ; preds = %for.body21
  %16 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4
  %m_appliedImpulse = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %16, i32 0, i32 16
  %17 = load float, float* %m_appliedImpulse, align 4
  %cmp25 = fcmp une float %17, 0.000000e+00
  br i1 %cmp25, label %if.then26, label %if.end42

if.then26:                                        ; preds = %land.lhs.true
  %18 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4
  %m_normalWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %18, i32 0, i32 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB)
  %19 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4
  %m_appliedImpulse28 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %19, i32 0, i32 16
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %m_appliedImpulse28)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %imp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %combinedRestitution)
  %20 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4
  %call29 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnAEv(%class.btManifoldPoint* %20)
  store %class.btVector3* %call29, %class.btVector3** %pos1, align 4
  %21 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4
  %call30 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnBEv(%class.btManifoldPoint* %21)
  store %class.btVector3* %call30, %class.btVector3** %pos2, align 4
  %22 = load %class.btVector3*, %class.btVector3** %pos1, align 4
  %23 = load %class.btRigidBody*, %class.btRigidBody** %body0, align 4
  %24 = bitcast %class.btRigidBody* %23 to %class.btCollisionObject*
  %call31 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %24)
  %call32 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call31)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rel_pos0, %class.btVector3* nonnull align 4 dereferenceable(16) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %call32)
  %25 = load %class.btVector3*, %class.btVector3** %pos2, align 4
  %26 = load %class.btRigidBody*, %class.btRigidBody** %body1, align 4
  %27 = bitcast %class.btRigidBody* %26 to %class.btCollisionObject*
  %call33 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %27)
  %call34 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call33)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %25, %class.btVector3* nonnull align 4 dereferenceable(16) %call34)
  %28 = load %class.btRigidBody*, %class.btRigidBody** %body0, align 4
  %tobool35 = icmp ne %class.btRigidBody* %28, null
  br i1 %tobool35, label %if.then36, label %if.end37

if.then36:                                        ; preds = %if.then26
  %29 = load %class.btRigidBody*, %class.btRigidBody** %body0, align 4
  call void @_ZN11btRigidBody12applyImpulseERK9btVector3S2_(%class.btRigidBody* %29, %class.btVector3* nonnull align 4 dereferenceable(16) %imp, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos0)
  br label %if.end37

if.end37:                                         ; preds = %if.then36, %if.then26
  %30 = load %class.btRigidBody*, %class.btRigidBody** %body1, align 4
  %tobool38 = icmp ne %class.btRigidBody* %30, null
  br i1 %tobool38, label %if.then39, label %if.end41

if.then39:                                        ; preds = %if.end37
  %31 = load %class.btRigidBody*, %class.btRigidBody** %body1, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp40, %class.btVector3* nonnull align 4 dereferenceable(16) %imp)
  call void @_ZN11btRigidBody12applyImpulseERK9btVector3S2_(%class.btRigidBody* %31, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp40, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1)
  br label %if.end41

if.end41:                                         ; preds = %if.then39, %if.end37
  br label %if.end42

if.end42:                                         ; preds = %if.end41, %land.lhs.true, %for.body21
  br label %for.inc

for.inc:                                          ; preds = %if.end42
  %32 = load i32, i32* %p, align 4
  %inc = add nsw i32 %32, 1
  store i32 %inc, i32* %p, align 4
  br label %for.cond18

for.end:                                          ; preds = %for.cond18
  br label %for.inc43

for.inc43:                                        ; preds = %for.end
  %33 = load i32, i32* %i, align 4
  %inc44 = add nsw i32 %33, 1
  store i32 %inc44, i32* %i, align 4
  br label %for.cond

for.end45:                                        ; preds = %for.cond
  %call46 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile8) #10
  br label %if.end47

if.end47:                                         ; preds = %for.end45, %if.end
  %call48 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_cachedPoints, align 4
  ret i32 %0
}

declare float @_ZN16btManifoldResult28calculateCombinedRestitutionEPK17btCollisionObjectS2_(%class.btCollisionObject*, %class.btCollisionObject*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnAEv(%class.btManifoldPoint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_positionWorldOnA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 3
  ret %class.btVector3* %m_positionWorldOnA
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnBEv(%class.btManifoldPoint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_positionWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 2
  ret %class.btVector3* %m_positionWorldOnB
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btRigidBody12applyImpulseERK9btVector3S2_(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %impulse.addr = alloca %class.btVector3*, align 4
  %rel_pos.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %impulse, %class.btVector3** %impulse.addr, align 4
  store %class.btVector3* %rel_pos, %class.btVector3** %rel_pos.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4
  %cmp = fcmp une float %0, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load %class.btVector3*, %class.btVector3** %impulse.addr, align 4
  call void @_ZN11btRigidBody19applyCentralImpulseERK9btVector3(%class.btRigidBody* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_angularFactor)
  %tobool = icmp ne float* %call, null
  br i1 %tobool, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %2 = load %class.btVector3*, %class.btVector3** %rel_pos.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %impulse.addr, align 4
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  call void @_ZN11btRigidBody18applyTorqueImpulseERK9btVector3(%class.btRigidBody* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf(%class.btDiscreteDynamicsWorld* %this, float %timeStep) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %i = alloca i32, align 4
  %body = alloca %class.btRigidBody*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.14, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_nonStaticRigidBodies = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_nonStaticRigidBodies3 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 6
  %1 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %class.btRigidBody** @_ZN20btAlignedObjectArrayIP11btRigidBodyEixEi(%class.btAlignedObjectArray.19* %m_nonStaticRigidBodies3, i32 %1)
  %2 = load %class.btRigidBody*, %class.btRigidBody** %call4, align 4
  store %class.btRigidBody* %2, %class.btRigidBody** %body, align 4
  %3 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %4 = bitcast %class.btRigidBody* %3 to %class.btCollisionObject*
  %call5 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %4)
  br i1 %call5, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  %5 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %6 = load float, float* %timeStep.addr, align 4
  call void @_ZN11btRigidBody12applyDampingEf(%class.btRigidBody* %5, float %6)
  %7 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %8 = load float, float* %timeStep.addr, align 4
  %9 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %10 = bitcast %class.btRigidBody* %9 to %class.btCollisionObject*
  %call6 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject30getInterpolationWorldTransformEv(%class.btCollisionObject* %10)
  call void @_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform(%class.btRigidBody* %7, float %8, %class.btTransform* nonnull align 4 dereferenceable(64) %call6)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

declare void @_ZN11btRigidBody12applyDampingEf(%class.btRigidBody*, float) #3

declare void @_ZN15CProfileManager5ResetEv() #3

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraint(%class.btDiscreteDynamicsWorld* %this, %class.btTypedConstraint* %constraint) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %constraint.addr = alloca %class.btTypedConstraint*, align 4
  %drawFrames = alloca i8, align 1
  %drawLimits = alloca i8, align 1
  %dbgDrawSize = alloca float, align 4
  %p2pC = alloca %class.btPoint2PointConstraint*, align 4
  %tr = alloca %class.btTransform, align 4
  %pivot = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %pHinge = alloca %class.btHingeConstraint*, align 4
  %tr38 = alloca %class.btTransform, align 4
  %ref.tmp50 = alloca %class.btTransform, align 4
  %minAng = alloca float, align 4
  %maxAng = alloca float, align 4
  %drawSect = alloca i8, align 1
  %center = alloca %class.btVector3*, align 4
  %normal = alloca %class.btVector3, align 4
  %axis = alloca %class.btVector3, align 4
  %ref.tmp79 = alloca %class.btVector3, align 4
  %ref.tmp80 = alloca float, align 4
  %ref.tmp81 = alloca float, align 4
  %ref.tmp82 = alloca float, align 4
  %pCT = alloca %class.btConeTwistConstraint*, align 4
  %tr89 = alloca %class.btTransform, align 4
  %ref.tmp101 = alloca %class.btTransform, align 4
  %length = alloca float, align 4
  %fAngleInRadians = alloca float, align 4
  %pPrev = alloca %class.btVector3, align 4
  %ref.tmp117 = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %pCur = alloca %class.btVector3, align 4
  %ref.tmp123 = alloca %class.btVector3, align 4
  %ref.tmp127 = alloca %class.btVector3, align 4
  %ref.tmp128 = alloca float, align 4
  %ref.tmp129 = alloca float, align 4
  %ref.tmp130 = alloca float, align 4
  %ref.tmp141 = alloca %class.btVector3, align 4
  %ref.tmp142 = alloca float, align 4
  %ref.tmp143 = alloca float, align 4
  %ref.tmp144 = alloca float, align 4
  %tws = alloca float, align 4
  %twa = alloca float, align 4
  %useFrameB = alloca i8, align 1
  %ref.tmp157 = alloca %class.btTransform, align 4
  %ref.tmp162 = alloca %class.btTransform, align 4
  %pivot168 = alloca %class.btVector3, align 4
  %normal170 = alloca %class.btVector3, align 4
  %axis1 = alloca %class.btVector3, align 4
  %ref.tmp178 = alloca %class.btVector3, align 4
  %ref.tmp179 = alloca float, align 4
  %ref.tmp180 = alloca float, align 4
  %ref.tmp181 = alloca float, align 4
  %p6DOF = alloca %class.btGeneric6DofConstraint*, align 4
  %tr187 = alloca %class.btTransform, align 4
  %center212 = alloca %class.btVector3*, align 4
  %up = alloca %class.btVector3, align 4
  %axis216 = alloca %class.btVector3, align 4
  %minTh = alloca float, align 4
  %maxTh = alloca float, align 4
  %minPs = alloca float, align 4
  %maxPs = alloca float, align 4
  %ref.tmp228 = alloca %class.btVector3, align 4
  %ref.tmp229 = alloca float, align 4
  %ref.tmp230 = alloca float, align 4
  %ref.tmp231 = alloca float, align 4
  %ref.tmp235 = alloca %class.btVector3, align 4
  %ay = alloca float, align 4
  %az = alloca float, align 4
  %cy = alloca float, align 4
  %sy = alloca float, align 4
  %cz = alloca float, align 4
  %sz = alloca float, align 4
  %ref = alloca %class.btVector3, align 4
  %normal285 = alloca %class.btVector3, align 4
  %ref.tmp286 = alloca %class.btVector3, align 4
  %minFi = alloca float, align 4
  %maxFi = alloca float, align 4
  %ref.tmp297 = alloca %class.btVector3, align 4
  %ref.tmp298 = alloca float, align 4
  %ref.tmp299 = alloca float, align 4
  %ref.tmp300 = alloca float, align 4
  %ref.tmp310 = alloca %class.btVector3, align 4
  %ref.tmp311 = alloca float, align 4
  %ref.tmp312 = alloca float, align 4
  %ref.tmp313 = alloca float, align 4
  %bbMin = alloca %class.btVector3, align 4
  %bbMax = alloca %class.btVector3, align 4
  %ref.tmp326 = alloca %class.btVector3, align 4
  %ref.tmp327 = alloca float, align 4
  %ref.tmp328 = alloca float, align 4
  %ref.tmp329 = alloca float, align 4
  %p6DOF335 = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %tr336 = alloca %class.btTransform, align 4
  %center361 = alloca %class.btVector3*, align 4
  %up364 = alloca %class.btVector3, align 4
  %axis366 = alloca %class.btVector3, align 4
  %minTh368 = alloca float, align 4
  %maxTh371 = alloca float, align 4
  %minPs374 = alloca float, align 4
  %maxPs377 = alloca float, align 4
  %ref.tmp384 = alloca %class.btVector3, align 4
  %ref.tmp385 = alloca float, align 4
  %ref.tmp386 = alloca float, align 4
  %ref.tmp387 = alloca float, align 4
  %ref.tmp391 = alloca %class.btVector3, align 4
  %ay393 = alloca float, align 4
  %az395 = alloca float, align 4
  %cy397 = alloca float, align 4
  %sy399 = alloca float, align 4
  %cz401 = alloca float, align 4
  %sz403 = alloca float, align 4
  %ref405 = alloca %class.btVector3, align 4
  %normal449 = alloca %class.btVector3, align 4
  %ref.tmp450 = alloca %class.btVector3, align 4
  %minFi452 = alloca float, align 4
  %maxFi455 = alloca float, align 4
  %ref.tmp463 = alloca %class.btVector3, align 4
  %ref.tmp464 = alloca float, align 4
  %ref.tmp465 = alloca float, align 4
  %ref.tmp466 = alloca float, align 4
  %ref.tmp476 = alloca %class.btVector3, align 4
  %ref.tmp477 = alloca float, align 4
  %ref.tmp478 = alloca float, align 4
  %ref.tmp479 = alloca float, align 4
  %bbMin487 = alloca %class.btVector3, align 4
  %bbMax490 = alloca %class.btVector3, align 4
  %ref.tmp496 = alloca %class.btVector3, align 4
  %ref.tmp497 = alloca float, align 4
  %ref.tmp498 = alloca float, align 4
  %ref.tmp499 = alloca float, align 4
  %pSlider = alloca %class.btSliderConstraint*, align 4
  %tr505 = alloca %class.btTransform, align 4
  %tr528 = alloca %class.btTransform, align 4
  %li_min = alloca %class.btVector3, align 4
  %ref.tmp533 = alloca %class.btVector3, align 4
  %ref.tmp534 = alloca float, align 4
  %ref.tmp536 = alloca float, align 4
  %ref.tmp537 = alloca float, align 4
  %li_max = alloca %class.btVector3, align 4
  %ref.tmp539 = alloca %class.btVector3, align 4
  %ref.tmp540 = alloca float, align 4
  %ref.tmp542 = alloca float, align 4
  %ref.tmp543 = alloca float, align 4
  %ref.tmp548 = alloca %class.btVector3, align 4
  %ref.tmp549 = alloca float, align 4
  %ref.tmp550 = alloca float, align 4
  %ref.tmp551 = alloca float, align 4
  %normal555 = alloca %class.btVector3, align 4
  %axis557 = alloca %class.btVector3, align 4
  %a_min = alloca float, align 4
  %a_max = alloca float, align 4
  %center561 = alloca %class.btVector3*, align 4
  %ref.tmp567 = alloca %class.btVector3, align 4
  %ref.tmp568 = alloca float, align 4
  %ref.tmp569 = alloca float, align 4
  %ref.tmp570 = alloca float, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btTypedConstraint* %constraint, %class.btTypedConstraint** %constraint.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %1 = bitcast %class.btCollisionWorld* %0 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %1, align 4
  %vfn = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable, i64 5
  %2 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn, align 4
  %call = call %class.btIDebugDraw* %2(%class.btCollisionWorld* %0)
  %3 = bitcast %class.btIDebugDraw* %call to i32 (%class.btIDebugDraw*)***
  %vtable2 = load i32 (%class.btIDebugDraw*)**, i32 (%class.btIDebugDraw*)*** %3, align 4
  %vfn3 = getelementptr inbounds i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vtable2, i64 14
  %4 = load i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vfn3, align 4
  %call4 = call i32 %4(%class.btIDebugDraw* %call)
  %and = and i32 %call4, 2048
  %cmp = icmp ne i32 %and, 0
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %drawFrames, align 1
  %5 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %6 = bitcast %class.btCollisionWorld* %5 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable5 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %6, align 4
  %vfn6 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable5, i64 5
  %7 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn6, align 4
  %call7 = call %class.btIDebugDraw* %7(%class.btCollisionWorld* %5)
  %8 = bitcast %class.btIDebugDraw* %call7 to i32 (%class.btIDebugDraw*)***
  %vtable8 = load i32 (%class.btIDebugDraw*)**, i32 (%class.btIDebugDraw*)*** %8, align 4
  %vfn9 = getelementptr inbounds i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vtable8, i64 14
  %9 = load i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vfn9, align 4
  %call10 = call i32 %9(%class.btIDebugDraw* %call7)
  %and11 = and i32 %call10, 4096
  %cmp12 = icmp ne i32 %and11, 0
  %frombool13 = zext i1 %cmp12 to i8
  store i8 %frombool13, i8* %drawLimits, align 1
  %10 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint.addr, align 4
  %call14 = call float @_ZN17btTypedConstraint14getDbgDrawSizeEv(%class.btTypedConstraint* %10)
  store float %call14, float* %dbgDrawSize, align 4
  %11 = load float, float* %dbgDrawSize, align 4
  %cmp15 = fcmp ole float %11, 0.000000e+00
  br i1 %cmp15, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %12 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint.addr, align 4
  %call16 = call i32 @_ZNK17btTypedConstraint17getConstraintTypeEv(%class.btTypedConstraint* %12)
  switch i32 %call16, label %sw.default [
    i32 3, label %sw.bb
    i32 4, label %sw.bb37
    i32 5, label %sw.bb88
    i32 9, label %sw.bb186
    i32 6, label %sw.bb186
    i32 12, label %sw.bb334
    i32 7, label %sw.bb504
  ]

sw.bb:                                            ; preds = %if.end
  %13 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint.addr, align 4
  %14 = bitcast %class.btTypedConstraint* %13 to %class.btPoint2PointConstraint*
  store %class.btPoint2PointConstraint* %14, %class.btPoint2PointConstraint** %p2pC, align 4
  %call17 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %tr)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %tr)
  %15 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %p2pC, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btPoint2PointConstraint11getPivotInAEv(%class.btPoint2PointConstraint* %15)
  %16 = bitcast %class.btVector3* %pivot to i8*
  %17 = bitcast %class.btVector3* %call18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false)
  %18 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %p2pC, align 4
  %19 = bitcast %class.btPoint2PointConstraint* %18 to %class.btTypedConstraint*
  %call19 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %19)
  %call20 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call19)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btTransform* %call20, %class.btVector3* nonnull align 4 dereferenceable(16) %pivot)
  %20 = bitcast %class.btVector3* %pivot to i8*
  %21 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %tr, %class.btVector3* nonnull align 4 dereferenceable(16) %pivot)
  %22 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %23 = bitcast %class.btCollisionWorld* %22 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable21 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %23, align 4
  %vfn22 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable21, i64 5
  %24 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn22, align 4
  %call23 = call %class.btIDebugDraw* %24(%class.btCollisionWorld* %22)
  %25 = load float, float* %dbgDrawSize, align 4
  %26 = bitcast %class.btIDebugDraw* %call23 to void (%class.btIDebugDraw*, %class.btTransform*, float)***
  %vtable24 = load void (%class.btIDebugDraw*, %class.btTransform*, float)**, void (%class.btIDebugDraw*, %class.btTransform*, float)*** %26, align 4
  %vfn25 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vtable24, i64 16
  %27 = load void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vfn25, align 4
  call void %27(%class.btIDebugDraw* %call23, %class.btTransform* nonnull align 4 dereferenceable(64) %tr, float %25)
  %28 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %p2pC, align 4
  %call26 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btPoint2PointConstraint11getPivotInBEv(%class.btPoint2PointConstraint* %28)
  %29 = bitcast %class.btVector3* %pivot to i8*
  %30 = bitcast %class.btVector3* %call26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %29, i8* align 4 %30, i32 16, i1 false)
  %31 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %p2pC, align 4
  %32 = bitcast %class.btPoint2PointConstraint* %31 to %class.btTypedConstraint*
  %call28 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %32)
  %call29 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call28)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp27, %class.btTransform* %call29, %class.btVector3* nonnull align 4 dereferenceable(16) %pivot)
  %33 = bitcast %class.btVector3* %pivot to i8*
  %34 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 4 %34, i32 16, i1 false)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %tr, %class.btVector3* nonnull align 4 dereferenceable(16) %pivot)
  %35 = load i8, i8* %drawFrames, align 1
  %tobool = trunc i8 %35 to i1
  br i1 %tobool, label %if.then30, label %if.end36

if.then30:                                        ; preds = %sw.bb
  %36 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %37 = bitcast %class.btCollisionWorld* %36 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable31 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %37, align 4
  %vfn32 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable31, i64 5
  %38 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn32, align 4
  %call33 = call %class.btIDebugDraw* %38(%class.btCollisionWorld* %36)
  %39 = load float, float* %dbgDrawSize, align 4
  %40 = bitcast %class.btIDebugDraw* %call33 to void (%class.btIDebugDraw*, %class.btTransform*, float)***
  %vtable34 = load void (%class.btIDebugDraw*, %class.btTransform*, float)**, void (%class.btIDebugDraw*, %class.btTransform*, float)*** %40, align 4
  %vfn35 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vtable34, i64 16
  %41 = load void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vfn35, align 4
  call void %41(%class.btIDebugDraw* %call33, %class.btTransform* nonnull align 4 dereferenceable(64) %tr, float %39)
  br label %if.end36

if.end36:                                         ; preds = %if.then30, %sw.bb
  br label %sw.epilog

sw.bb37:                                          ; preds = %if.end
  %42 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint.addr, align 4
  %43 = bitcast %class.btTypedConstraint* %42 to %class.btHingeConstraint*
  store %class.btHingeConstraint* %43, %class.btHingeConstraint** %pHinge, align 4
  %44 = load %class.btHingeConstraint*, %class.btHingeConstraint** %pHinge, align 4
  %call39 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyAEv(%class.btHingeConstraint* %44)
  %call40 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call39)
  %45 = load %class.btHingeConstraint*, %class.btHingeConstraint** %pHinge, align 4
  %call41 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btHingeConstraint9getAFrameEv(%class.btHingeConstraint* %45)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %tr38, %class.btTransform* %call40, %class.btTransform* nonnull align 4 dereferenceable(64) %call41)
  %46 = load i8, i8* %drawFrames, align 1
  %tobool42 = trunc i8 %46 to i1
  br i1 %tobool42, label %if.then43, label %if.end49

if.then43:                                        ; preds = %sw.bb37
  %47 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %48 = bitcast %class.btCollisionWorld* %47 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable44 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %48, align 4
  %vfn45 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable44, i64 5
  %49 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn45, align 4
  %call46 = call %class.btIDebugDraw* %49(%class.btCollisionWorld* %47)
  %50 = load float, float* %dbgDrawSize, align 4
  %51 = bitcast %class.btIDebugDraw* %call46 to void (%class.btIDebugDraw*, %class.btTransform*, float)***
  %vtable47 = load void (%class.btIDebugDraw*, %class.btTransform*, float)**, void (%class.btIDebugDraw*, %class.btTransform*, float)*** %51, align 4
  %vfn48 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vtable47, i64 16
  %52 = load void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vfn48, align 4
  call void %52(%class.btIDebugDraw* %call46, %class.btTransform* nonnull align 4 dereferenceable(64) %tr38, float %50)
  br label %if.end49

if.end49:                                         ; preds = %if.then43, %sw.bb37
  %53 = load %class.btHingeConstraint*, %class.btHingeConstraint** %pHinge, align 4
  %call51 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyBEv(%class.btHingeConstraint* %53)
  %call52 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call51)
  %54 = load %class.btHingeConstraint*, %class.btHingeConstraint** %pHinge, align 4
  %call53 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btHingeConstraint9getBFrameEv(%class.btHingeConstraint* %54)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp50, %class.btTransform* %call52, %class.btTransform* nonnull align 4 dereferenceable(64) %call53)
  %call54 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %tr38, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp50)
  %55 = load i8, i8* %drawFrames, align 1
  %tobool55 = trunc i8 %55 to i1
  br i1 %tobool55, label %if.then56, label %if.end62

if.then56:                                        ; preds = %if.end49
  %56 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %57 = bitcast %class.btCollisionWorld* %56 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable57 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %57, align 4
  %vfn58 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable57, i64 5
  %58 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn58, align 4
  %call59 = call %class.btIDebugDraw* %58(%class.btCollisionWorld* %56)
  %59 = load float, float* %dbgDrawSize, align 4
  %60 = bitcast %class.btIDebugDraw* %call59 to void (%class.btIDebugDraw*, %class.btTransform*, float)***
  %vtable60 = load void (%class.btIDebugDraw*, %class.btTransform*, float)**, void (%class.btIDebugDraw*, %class.btTransform*, float)*** %60, align 4
  %vfn61 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vtable60, i64 16
  %61 = load void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vfn61, align 4
  call void %61(%class.btIDebugDraw* %call59, %class.btTransform* nonnull align 4 dereferenceable(64) %tr38, float %59)
  br label %if.end62

if.end62:                                         ; preds = %if.then56, %if.end49
  %62 = load %class.btHingeConstraint*, %class.btHingeConstraint** %pHinge, align 4
  %call63 = call float @_ZNK17btHingeConstraint13getLowerLimitEv(%class.btHingeConstraint* %62)
  store float %call63, float* %minAng, align 4
  %63 = load %class.btHingeConstraint*, %class.btHingeConstraint** %pHinge, align 4
  %call64 = call float @_ZNK17btHingeConstraint13getUpperLimitEv(%class.btHingeConstraint* %63)
  store float %call64, float* %maxAng, align 4
  %64 = load float, float* %minAng, align 4
  %65 = load float, float* %maxAng, align 4
  %cmp65 = fcmp oeq float %64, %65
  br i1 %cmp65, label %if.then66, label %if.end67

if.then66:                                        ; preds = %if.end62
  br label %sw.epilog

if.end67:                                         ; preds = %if.end62
  store i8 1, i8* %drawSect, align 1
  %66 = load %class.btHingeConstraint*, %class.btHingeConstraint** %pHinge, align 4
  %call68 = call zeroext i1 @_ZNK17btHingeConstraint8hasLimitEv(%class.btHingeConstraint* %66)
  br i1 %call68, label %if.end70, label %if.then69

if.then69:                                        ; preds = %if.end67
  store float 0.000000e+00, float* %minAng, align 4
  store float 0x401921FB60000000, float* %maxAng, align 4
  store i8 0, i8* %drawSect, align 1
  br label %if.end70

if.end70:                                         ; preds = %if.then69, %if.end67
  %67 = load i8, i8* %drawLimits, align 1
  %tobool71 = trunc i8 %67 to i1
  br i1 %tobool71, label %if.then72, label %if.end87

if.then72:                                        ; preds = %if.end70
  %call73 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %tr38)
  store %class.btVector3* %call73, %class.btVector3** %center, align 4
  %call74 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %tr38)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %normal, %class.btMatrix3x3* %call74, i32 2)
  %call75 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %tr38)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis, %class.btMatrix3x3* %call75, i32 0)
  %68 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %69 = bitcast %class.btCollisionWorld* %68 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable76 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %69, align 4
  %vfn77 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable76, i64 5
  %70 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn77, align 4
  %call78 = call %class.btIDebugDraw* %70(%class.btCollisionWorld* %68)
  %71 = load %class.btVector3*, %class.btVector3** %center, align 4
  %72 = load float, float* %dbgDrawSize, align 4
  %73 = load float, float* %dbgDrawSize, align 4
  %74 = load float, float* %minAng, align 4
  %75 = load float, float* %maxAng, align 4
  store float 0.000000e+00, float* %ref.tmp80, align 4
  store float 0.000000e+00, float* %ref.tmp81, align 4
  store float 0.000000e+00, float* %ref.tmp82, align 4
  %call83 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp79, float* nonnull align 4 dereferenceable(4) %ref.tmp80, float* nonnull align 4 dereferenceable(4) %ref.tmp81, float* nonnull align 4 dereferenceable(4) %ref.tmp82)
  %76 = load i8, i8* %drawSect, align 1
  %tobool84 = trunc i8 %76 to i1
  %77 = bitcast %class.btIDebugDraw* %call78 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)***
  %vtable85 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*** %77, align 4
  %vfn86 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)** %vtable85, i64 17
  %78 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)** %vfn86, align 4
  call void %78(%class.btIDebugDraw* %call78, %class.btVector3* nonnull align 4 dereferenceable(16) %71, %class.btVector3* nonnull align 4 dereferenceable(16) %normal, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float %72, float %73, float %74, float %75, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp79, i1 zeroext %tobool84, float 1.000000e+01)
  br label %if.end87

if.end87:                                         ; preds = %if.then72, %if.end70
  br label %sw.epilog

sw.bb88:                                          ; preds = %if.end
  %79 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint.addr, align 4
  %80 = bitcast %class.btTypedConstraint* %79 to %class.btConeTwistConstraint*
  store %class.btConeTwistConstraint* %80, %class.btConeTwistConstraint** %pCT, align 4
  %81 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %pCT, align 4
  %call90 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyAEv(%class.btConeTwistConstraint* %81)
  %call91 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call90)
  %82 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %pCT, align 4
  %call92 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK21btConeTwistConstraint9getAFrameEv(%class.btConeTwistConstraint* %82)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %tr89, %class.btTransform* %call91, %class.btTransform* nonnull align 4 dereferenceable(64) %call92)
  %83 = load i8, i8* %drawFrames, align 1
  %tobool93 = trunc i8 %83 to i1
  br i1 %tobool93, label %if.then94, label %if.end100

if.then94:                                        ; preds = %sw.bb88
  %84 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %85 = bitcast %class.btCollisionWorld* %84 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable95 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %85, align 4
  %vfn96 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable95, i64 5
  %86 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn96, align 4
  %call97 = call %class.btIDebugDraw* %86(%class.btCollisionWorld* %84)
  %87 = load float, float* %dbgDrawSize, align 4
  %88 = bitcast %class.btIDebugDraw* %call97 to void (%class.btIDebugDraw*, %class.btTransform*, float)***
  %vtable98 = load void (%class.btIDebugDraw*, %class.btTransform*, float)**, void (%class.btIDebugDraw*, %class.btTransform*, float)*** %88, align 4
  %vfn99 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vtable98, i64 16
  %89 = load void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vfn99, align 4
  call void %89(%class.btIDebugDraw* %call97, %class.btTransform* nonnull align 4 dereferenceable(64) %tr89, float %87)
  br label %if.end100

if.end100:                                        ; preds = %if.then94, %sw.bb88
  %90 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %pCT, align 4
  %call102 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyBEv(%class.btConeTwistConstraint* %90)
  %call103 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call102)
  %91 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %pCT, align 4
  %call104 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK21btConeTwistConstraint9getBFrameEv(%class.btConeTwistConstraint* %91)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp101, %class.btTransform* %call103, %class.btTransform* nonnull align 4 dereferenceable(64) %call104)
  %call105 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %tr89, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp101)
  %92 = load i8, i8* %drawFrames, align 1
  %tobool106 = trunc i8 %92 to i1
  br i1 %tobool106, label %if.then107, label %if.end113

if.then107:                                       ; preds = %if.end100
  %93 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %94 = bitcast %class.btCollisionWorld* %93 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable108 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %94, align 4
  %vfn109 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable108, i64 5
  %95 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn109, align 4
  %call110 = call %class.btIDebugDraw* %95(%class.btCollisionWorld* %93)
  %96 = load float, float* %dbgDrawSize, align 4
  %97 = bitcast %class.btIDebugDraw* %call110 to void (%class.btIDebugDraw*, %class.btTransform*, float)***
  %vtable111 = load void (%class.btIDebugDraw*, %class.btTransform*, float)**, void (%class.btIDebugDraw*, %class.btTransform*, float)*** %97, align 4
  %vfn112 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vtable111, i64 16
  %98 = load void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vfn112, align 4
  call void %98(%class.btIDebugDraw* %call110, %class.btTransform* nonnull align 4 dereferenceable(64) %tr89, float %96)
  br label %if.end113

if.end113:                                        ; preds = %if.then107, %if.end100
  %99 = load i8, i8* %drawLimits, align 1
  %tobool114 = trunc i8 %99 to i1
  br i1 %tobool114, label %if.then115, label %if.end185

if.then115:                                       ; preds = %if.end113
  %100 = load float, float* %dbgDrawSize, align 4
  store float %100, float* %length, align 4
  %101 = load i32, i32* @_ZZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraintE9nSegments, align 4
  %sub = sub nsw i32 %101, 1
  %conv = sitofp i32 %sub to float
  %mul = fmul float 0x401921FB40000000, %conv
  %102 = load i32, i32* @_ZZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraintE9nSegments, align 4
  %conv116 = sitofp i32 %102 to float
  %div = fdiv float %mul, %conv116
  store float %div, float* %fAngleInRadians, align 4
  %103 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %pCT, align 4
  %104 = load float, float* %fAngleInRadians, align 4
  %105 = load float, float* %length, align 4
  call void @_ZNK21btConeTwistConstraint16GetPointForAngleEff(%class.btVector3* sret align 4 %pPrev, %class.btConeTwistConstraint* %103, float %104, float %105)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp117, %class.btTransform* %tr89, %class.btVector3* nonnull align 4 dereferenceable(16) %pPrev)
  %106 = bitcast %class.btVector3* %pPrev to i8*
  %107 = bitcast %class.btVector3* %ref.tmp117 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %106, i8* align 4 %107, i32 16, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then115
  %108 = load i32, i32* %i, align 4
  %109 = load i32, i32* @_ZZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraintE9nSegments, align 4
  %cmp118 = icmp slt i32 %108, %109
  br i1 %cmp118, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %110 = load i32, i32* %i, align 4
  %conv119 = sitofp i32 %110 to float
  %mul120 = fmul float 0x401921FB40000000, %conv119
  %111 = load i32, i32* @_ZZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraintE9nSegments, align 4
  %conv121 = sitofp i32 %111 to float
  %div122 = fdiv float %mul120, %conv121
  store float %div122, float* %fAngleInRadians, align 4
  %112 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %pCT, align 4
  %113 = load float, float* %fAngleInRadians, align 4
  %114 = load float, float* %length, align 4
  call void @_ZNK21btConeTwistConstraint16GetPointForAngleEff(%class.btVector3* sret align 4 %pCur, %class.btConeTwistConstraint* %112, float %113, float %114)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp123, %class.btTransform* %tr89, %class.btVector3* nonnull align 4 dereferenceable(16) %pCur)
  %115 = bitcast %class.btVector3* %pCur to i8*
  %116 = bitcast %class.btVector3* %ref.tmp123 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %115, i8* align 4 %116, i32 16, i1 false)
  %117 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %118 = bitcast %class.btCollisionWorld* %117 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable124 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %118, align 4
  %vfn125 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable124, i64 5
  %119 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn125, align 4
  %call126 = call %class.btIDebugDraw* %119(%class.btCollisionWorld* %117)
  store float 0.000000e+00, float* %ref.tmp128, align 4
  store float 0.000000e+00, float* %ref.tmp129, align 4
  store float 0.000000e+00, float* %ref.tmp130, align 4
  %call131 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp127, float* nonnull align 4 dereferenceable(4) %ref.tmp128, float* nonnull align 4 dereferenceable(4) %ref.tmp129, float* nonnull align 4 dereferenceable(4) %ref.tmp130)
  %120 = bitcast %class.btIDebugDraw* %call126 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable132 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %120, align 4
  %vfn133 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable132, i64 4
  %121 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn133, align 4
  call void %121(%class.btIDebugDraw* %call126, %class.btVector3* nonnull align 4 dereferenceable(16) %pPrev, %class.btVector3* nonnull align 4 dereferenceable(16) %pCur, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp127)
  %122 = load i32, i32* %i, align 4
  %123 = load i32, i32* @_ZZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraintE9nSegments, align 4
  %div134 = sdiv i32 %123, 8
  %rem = srem i32 %122, %div134
  %cmp135 = icmp eq i32 %rem, 0
  br i1 %cmp135, label %if.then136, label %if.end148

if.then136:                                       ; preds = %for.body
  %124 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %125 = bitcast %class.btCollisionWorld* %124 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable137 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %125, align 4
  %vfn138 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable137, i64 5
  %126 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn138, align 4
  %call139 = call %class.btIDebugDraw* %126(%class.btCollisionWorld* %124)
  %call140 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %tr89)
  store float 0.000000e+00, float* %ref.tmp142, align 4
  store float 0.000000e+00, float* %ref.tmp143, align 4
  store float 0.000000e+00, float* %ref.tmp144, align 4
  %call145 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp141, float* nonnull align 4 dereferenceable(4) %ref.tmp142, float* nonnull align 4 dereferenceable(4) %ref.tmp143, float* nonnull align 4 dereferenceable(4) %ref.tmp144)
  %127 = bitcast %class.btIDebugDraw* %call139 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable146 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %127, align 4
  %vfn147 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable146, i64 4
  %128 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn147, align 4
  call void %128(%class.btIDebugDraw* %call139, %class.btVector3* nonnull align 4 dereferenceable(16) %call140, %class.btVector3* nonnull align 4 dereferenceable(16) %pCur, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp141)
  br label %if.end148

if.end148:                                        ; preds = %if.then136, %for.body
  %129 = bitcast %class.btVector3* %pPrev to i8*
  %130 = bitcast %class.btVector3* %pCur to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %129, i8* align 4 %130, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %if.end148
  %131 = load i32, i32* %i, align 4
  %inc = add nsw i32 %131, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %132 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %pCT, align 4
  %call149 = call float @_ZNK21btConeTwistConstraint12getTwistSpanEv(%class.btConeTwistConstraint* %132)
  store float %call149, float* %tws, align 4
  %133 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %pCT, align 4
  %call150 = call float @_ZNK21btConeTwistConstraint13getTwistAngleEv(%class.btConeTwistConstraint* %133)
  store float %call150, float* %twa, align 4
  %134 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %pCT, align 4
  %call151 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyBEv(%class.btConeTwistConstraint* %134)
  %call152 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %call151)
  %cmp153 = fcmp ogt float %call152, 0.000000e+00
  %frombool154 = zext i1 %cmp153 to i8
  store i8 %frombool154, i8* %useFrameB, align 1
  %135 = load i8, i8* %useFrameB, align 1
  %tobool155 = trunc i8 %135 to i1
  br i1 %tobool155, label %if.then156, label %if.else

if.then156:                                       ; preds = %for.end
  %136 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %pCT, align 4
  %call158 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyBEv(%class.btConeTwistConstraint* %136)
  %call159 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call158)
  %137 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %pCT, align 4
  %call160 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK21btConeTwistConstraint9getBFrameEv(%class.btConeTwistConstraint* %137)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp157, %class.btTransform* %call159, %class.btTransform* nonnull align 4 dereferenceable(64) %call160)
  %call161 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %tr89, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp157)
  br label %if.end167

if.else:                                          ; preds = %for.end
  %138 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %pCT, align 4
  %call163 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyAEv(%class.btConeTwistConstraint* %138)
  %call164 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call163)
  %139 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %pCT, align 4
  %call165 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK21btConeTwistConstraint9getAFrameEv(%class.btConeTwistConstraint* %139)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp162, %class.btTransform* %call164, %class.btTransform* nonnull align 4 dereferenceable(64) %call165)
  %call166 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %tr89, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp162)
  br label %if.end167

if.end167:                                        ; preds = %if.else, %if.then156
  %call169 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %tr89)
  %140 = bitcast %class.btVector3* %pivot168 to i8*
  %141 = bitcast %class.btVector3* %call169 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %140, i8* align 4 %141, i32 16, i1 false)
  %call171 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %tr89)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %normal170, %class.btMatrix3x3* %call171, i32 0)
  %call172 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %tr89)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis1, %class.btMatrix3x3* %call172, i32 1)
  %142 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %143 = bitcast %class.btCollisionWorld* %142 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable173 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %143, align 4
  %vfn174 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable173, i64 5
  %144 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn174, align 4
  %call175 = call %class.btIDebugDraw* %144(%class.btCollisionWorld* %142)
  %145 = load float, float* %dbgDrawSize, align 4
  %146 = load float, float* %dbgDrawSize, align 4
  %147 = load float, float* %twa, align 4
  %fneg = fneg float %147
  %148 = load float, float* %tws, align 4
  %sub176 = fsub float %fneg, %148
  %149 = load float, float* %twa, align 4
  %fneg177 = fneg float %149
  %150 = load float, float* %tws, align 4
  %add = fadd float %fneg177, %150
  store float 0.000000e+00, float* %ref.tmp179, align 4
  store float 0.000000e+00, float* %ref.tmp180, align 4
  store float 0.000000e+00, float* %ref.tmp181, align 4
  %call182 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp178, float* nonnull align 4 dereferenceable(4) %ref.tmp179, float* nonnull align 4 dereferenceable(4) %ref.tmp180, float* nonnull align 4 dereferenceable(4) %ref.tmp181)
  %151 = bitcast %class.btIDebugDraw* %call175 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)***
  %vtable183 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*** %151, align 4
  %vfn184 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)** %vtable183, i64 17
  %152 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)** %vfn184, align 4
  call void %152(%class.btIDebugDraw* %call175, %class.btVector3* nonnull align 4 dereferenceable(16) %pivot168, %class.btVector3* nonnull align 4 dereferenceable(16) %normal170, %class.btVector3* nonnull align 4 dereferenceable(16) %axis1, float %145, float %146, float %sub176, float %add, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp178, i1 zeroext true, float 1.000000e+01)
  br label %if.end185

if.end185:                                        ; preds = %if.end167, %if.end113
  br label %sw.epilog

sw.bb186:                                         ; preds = %if.end, %if.end
  %153 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint.addr, align 4
  %154 = bitcast %class.btTypedConstraint* %153 to %class.btGeneric6DofConstraint*
  store %class.btGeneric6DofConstraint* %154, %class.btGeneric6DofConstraint** %p6DOF, align 4
  %155 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %p6DOF, align 4
  %call188 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK23btGeneric6DofConstraint23getCalculatedTransformAEv(%class.btGeneric6DofConstraint* %155)
  %call189 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %tr187, %class.btTransform* nonnull align 4 dereferenceable(64) %call188)
  %156 = load i8, i8* %drawFrames, align 1
  %tobool190 = trunc i8 %156 to i1
  br i1 %tobool190, label %if.then191, label %if.end197

if.then191:                                       ; preds = %sw.bb186
  %157 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %158 = bitcast %class.btCollisionWorld* %157 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable192 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %158, align 4
  %vfn193 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable192, i64 5
  %159 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn193, align 4
  %call194 = call %class.btIDebugDraw* %159(%class.btCollisionWorld* %157)
  %160 = load float, float* %dbgDrawSize, align 4
  %161 = bitcast %class.btIDebugDraw* %call194 to void (%class.btIDebugDraw*, %class.btTransform*, float)***
  %vtable195 = load void (%class.btIDebugDraw*, %class.btTransform*, float)**, void (%class.btIDebugDraw*, %class.btTransform*, float)*** %161, align 4
  %vfn196 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vtable195, i64 16
  %162 = load void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vfn196, align 4
  call void %162(%class.btIDebugDraw* %call194, %class.btTransform* nonnull align 4 dereferenceable(64) %tr187, float %160)
  br label %if.end197

if.end197:                                        ; preds = %if.then191, %sw.bb186
  %163 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %p6DOF, align 4
  %call198 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK23btGeneric6DofConstraint23getCalculatedTransformBEv(%class.btGeneric6DofConstraint* %163)
  %call199 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %tr187, %class.btTransform* nonnull align 4 dereferenceable(64) %call198)
  %164 = load i8, i8* %drawFrames, align 1
  %tobool200 = trunc i8 %164 to i1
  br i1 %tobool200, label %if.then201, label %if.end207

if.then201:                                       ; preds = %if.end197
  %165 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %166 = bitcast %class.btCollisionWorld* %165 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable202 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %166, align 4
  %vfn203 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable202, i64 5
  %167 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn203, align 4
  %call204 = call %class.btIDebugDraw* %167(%class.btCollisionWorld* %165)
  %168 = load float, float* %dbgDrawSize, align 4
  %169 = bitcast %class.btIDebugDraw* %call204 to void (%class.btIDebugDraw*, %class.btTransform*, float)***
  %vtable205 = load void (%class.btIDebugDraw*, %class.btTransform*, float)**, void (%class.btIDebugDraw*, %class.btTransform*, float)*** %169, align 4
  %vfn206 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vtable205, i64 16
  %170 = load void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vfn206, align 4
  call void %170(%class.btIDebugDraw* %call204, %class.btTransform* nonnull align 4 dereferenceable(64) %tr187, float %168)
  br label %if.end207

if.end207:                                        ; preds = %if.then201, %if.end197
  %171 = load i8, i8* %drawLimits, align 1
  %tobool208 = trunc i8 %171 to i1
  br i1 %tobool208, label %if.then209, label %if.end333

if.then209:                                       ; preds = %if.end207
  %172 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %p6DOF, align 4
  %call210 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK23btGeneric6DofConstraint23getCalculatedTransformAEv(%class.btGeneric6DofConstraint* %172)
  %call211 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %tr187, %class.btTransform* nonnull align 4 dereferenceable(64) %call210)
  %173 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %p6DOF, align 4
  %call213 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK23btGeneric6DofConstraint23getCalculatedTransformBEv(%class.btGeneric6DofConstraint* %173)
  %call214 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call213)
  store %class.btVector3* %call214, %class.btVector3** %center212, align 4
  %call215 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %tr187)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %up, %class.btMatrix3x3* %call215, i32 2)
  %call217 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %tr187)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis216, %class.btMatrix3x3* %call217, i32 0)
  %174 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %p6DOF, align 4
  %call218 = call %class.btRotationalLimitMotor* @_ZN23btGeneric6DofConstraint23getRotationalLimitMotorEi(%class.btGeneric6DofConstraint* %174, i32 1)
  %m_loLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %call218, i32 0, i32 0
  %175 = load float, float* %m_loLimit, align 4
  store float %175, float* %minTh, align 4
  %176 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %p6DOF, align 4
  %call219 = call %class.btRotationalLimitMotor* @_ZN23btGeneric6DofConstraint23getRotationalLimitMotorEi(%class.btGeneric6DofConstraint* %176, i32 1)
  %m_hiLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %call219, i32 0, i32 1
  %177 = load float, float* %m_hiLimit, align 4
  store float %177, float* %maxTh, align 4
  %178 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %p6DOF, align 4
  %call220 = call %class.btRotationalLimitMotor* @_ZN23btGeneric6DofConstraint23getRotationalLimitMotorEi(%class.btGeneric6DofConstraint* %178, i32 2)
  %m_loLimit221 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %call220, i32 0, i32 0
  %179 = load float, float* %m_loLimit221, align 4
  store float %179, float* %minPs, align 4
  %180 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %p6DOF, align 4
  %call222 = call %class.btRotationalLimitMotor* @_ZN23btGeneric6DofConstraint23getRotationalLimitMotorEi(%class.btGeneric6DofConstraint* %180, i32 2)
  %m_hiLimit223 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %call222, i32 0, i32 1
  %181 = load float, float* %m_hiLimit223, align 4
  store float %181, float* %maxPs, align 4
  %182 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %183 = bitcast %class.btCollisionWorld* %182 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable224 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %183, align 4
  %vfn225 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable224, i64 5
  %184 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn225, align 4
  %call226 = call %class.btIDebugDraw* %184(%class.btCollisionWorld* %182)
  %185 = load %class.btVector3*, %class.btVector3** %center212, align 4
  %186 = load float, float* %dbgDrawSize, align 4
  %mul227 = fmul float %186, 0x3FECCCCCC0000000
  %187 = load float, float* %minTh, align 4
  %188 = load float, float* %maxTh, align 4
  %189 = load float, float* %minPs, align 4
  %190 = load float, float* %maxPs, align 4
  store float 0.000000e+00, float* %ref.tmp229, align 4
  store float 0.000000e+00, float* %ref.tmp230, align 4
  store float 0.000000e+00, float* %ref.tmp231, align 4
  %call232 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp228, float* nonnull align 4 dereferenceable(4) %ref.tmp229, float* nonnull align 4 dereferenceable(4) %ref.tmp230, float* nonnull align 4 dereferenceable(4) %ref.tmp231)
  %191 = bitcast %class.btIDebugDraw* %call226 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, float, %class.btVector3*, float, i1)***
  %vtable233 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, float, %class.btVector3*, float, i1)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, float, %class.btVector3*, float, i1)*** %191, align 4
  %vfn234 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, float, %class.btVector3*, float, i1)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, float, %class.btVector3*, float, i1)** %vtable233, i64 18
  %192 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, float, %class.btVector3*, float, i1)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, float, %class.btVector3*, float, i1)** %vfn234, align 4
  call void %192(%class.btIDebugDraw* %call226, %class.btVector3* nonnull align 4 dereferenceable(16) %185, %class.btVector3* nonnull align 4 dereferenceable(16) %up, %class.btVector3* nonnull align 4 dereferenceable(16) %axis216, float %mul227, float %187, float %188, float %189, float %190, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp228, float 1.000000e+01, i1 zeroext true)
  %call236 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %tr187)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp235, %class.btMatrix3x3* %call236, i32 1)
  %193 = bitcast %class.btVector3* %axis216 to i8*
  %194 = bitcast %class.btVector3* %ref.tmp235 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %193, i8* align 4 %194, i32 16, i1 false)
  %195 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %p6DOF, align 4
  %call237 = call float @_ZNK23btGeneric6DofConstraint8getAngleEi(%class.btGeneric6DofConstraint* %195, i32 1)
  store float %call237, float* %ay, align 4
  %196 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %p6DOF, align 4
  %call238 = call float @_ZNK23btGeneric6DofConstraint8getAngleEi(%class.btGeneric6DofConstraint* %196, i32 2)
  store float %call238, float* %az, align 4
  %197 = load float, float* %ay, align 4
  %call239 = call float @_Z5btCosf(float %197)
  store float %call239, float* %cy, align 4
  %198 = load float, float* %ay, align 4
  %call240 = call float @_Z5btSinf(float %198)
  store float %call240, float* %sy, align 4
  %199 = load float, float* %az, align 4
  %call241 = call float @_Z5btCosf(float %199)
  store float %call241, float* %cz, align 4
  %200 = load float, float* %az, align 4
  %call242 = call float @_Z5btSinf(float %200)
  store float %call242, float* %sz, align 4
  %call243 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref)
  %201 = load float, float* %cy, align 4
  %202 = load float, float* %cz, align 4
  %mul244 = fmul float %201, %202
  %call245 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis216)
  %arrayidx = getelementptr inbounds float, float* %call245, i32 0
  %203 = load float, float* %arrayidx, align 4
  %mul246 = fmul float %mul244, %203
  %204 = load float, float* %cy, align 4
  %205 = load float, float* %sz, align 4
  %mul247 = fmul float %204, %205
  %call248 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis216)
  %arrayidx249 = getelementptr inbounds float, float* %call248, i32 1
  %206 = load float, float* %arrayidx249, align 4
  %mul250 = fmul float %mul247, %206
  %add251 = fadd float %mul246, %mul250
  %207 = load float, float* %sy, align 4
  %call252 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis216)
  %arrayidx253 = getelementptr inbounds float, float* %call252, i32 2
  %208 = load float, float* %arrayidx253, align 4
  %mul254 = fmul float %207, %208
  %sub255 = fsub float %add251, %mul254
  %call256 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ref)
  %arrayidx257 = getelementptr inbounds float, float* %call256, i32 0
  store float %sub255, float* %arrayidx257, align 4
  %209 = load float, float* %sz, align 4
  %fneg258 = fneg float %209
  %call259 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis216)
  %arrayidx260 = getelementptr inbounds float, float* %call259, i32 0
  %210 = load float, float* %arrayidx260, align 4
  %mul261 = fmul float %fneg258, %210
  %211 = load float, float* %cz, align 4
  %call262 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis216)
  %arrayidx263 = getelementptr inbounds float, float* %call262, i32 1
  %212 = load float, float* %arrayidx263, align 4
  %mul264 = fmul float %211, %212
  %add265 = fadd float %mul261, %mul264
  %call266 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ref)
  %arrayidx267 = getelementptr inbounds float, float* %call266, i32 1
  store float %add265, float* %arrayidx267, align 4
  %213 = load float, float* %cz, align 4
  %214 = load float, float* %sy, align 4
  %mul268 = fmul float %213, %214
  %call269 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis216)
  %arrayidx270 = getelementptr inbounds float, float* %call269, i32 0
  %215 = load float, float* %arrayidx270, align 4
  %mul271 = fmul float %mul268, %215
  %216 = load float, float* %sz, align 4
  %217 = load float, float* %sy, align 4
  %mul272 = fmul float %216, %217
  %call273 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis216)
  %arrayidx274 = getelementptr inbounds float, float* %call273, i32 1
  %218 = load float, float* %arrayidx274, align 4
  %mul275 = fmul float %mul272, %218
  %add276 = fadd float %mul271, %mul275
  %219 = load float, float* %cy, align 4
  %call277 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis216)
  %arrayidx278 = getelementptr inbounds float, float* %call277, i32 2
  %220 = load float, float* %arrayidx278, align 4
  %mul279 = fmul float %219, %220
  %add280 = fadd float %add276, %mul279
  %call281 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ref)
  %arrayidx282 = getelementptr inbounds float, float* %call281, i32 2
  store float %add280, float* %arrayidx282, align 4
  %221 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %p6DOF, align 4
  %call283 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK23btGeneric6DofConstraint23getCalculatedTransformBEv(%class.btGeneric6DofConstraint* %221)
  %call284 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %tr187, %class.btTransform* nonnull align 4 dereferenceable(64) %call283)
  %call287 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %tr187)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp286, %class.btMatrix3x3* %call287, i32 0)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %normal285, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp286)
  %222 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %p6DOF, align 4
  %call288 = call %class.btRotationalLimitMotor* @_ZN23btGeneric6DofConstraint23getRotationalLimitMotorEi(%class.btGeneric6DofConstraint* %222, i32 0)
  %m_loLimit289 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %call288, i32 0, i32 0
  %223 = load float, float* %m_loLimit289, align 4
  store float %223, float* %minFi, align 4
  %224 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %p6DOF, align 4
  %call290 = call %class.btRotationalLimitMotor* @_ZN23btGeneric6DofConstraint23getRotationalLimitMotorEi(%class.btGeneric6DofConstraint* %224, i32 0)
  %m_hiLimit291 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %call290, i32 0, i32 1
  %225 = load float, float* %m_hiLimit291, align 4
  store float %225, float* %maxFi, align 4
  %226 = load float, float* %minFi, align 4
  %227 = load float, float* %maxFi, align 4
  %cmp292 = fcmp ogt float %226, %227
  br i1 %cmp292, label %if.then293, label %if.else304

if.then293:                                       ; preds = %if.then209
  %228 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %229 = bitcast %class.btCollisionWorld* %228 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable294 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %229, align 4
  %vfn295 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable294, i64 5
  %230 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn295, align 4
  %call296 = call %class.btIDebugDraw* %230(%class.btCollisionWorld* %228)
  %231 = load %class.btVector3*, %class.btVector3** %center212, align 4
  %232 = load float, float* %dbgDrawSize, align 4
  %233 = load float, float* %dbgDrawSize, align 4
  store float 0.000000e+00, float* %ref.tmp298, align 4
  store float 0.000000e+00, float* %ref.tmp299, align 4
  store float 0.000000e+00, float* %ref.tmp300, align 4
  %call301 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp297, float* nonnull align 4 dereferenceable(4) %ref.tmp298, float* nonnull align 4 dereferenceable(4) %ref.tmp299, float* nonnull align 4 dereferenceable(4) %ref.tmp300)
  %234 = bitcast %class.btIDebugDraw* %call296 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)***
  %vtable302 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*** %234, align 4
  %vfn303 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)** %vtable302, i64 17
  %235 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)** %vfn303, align 4
  call void %235(%class.btIDebugDraw* %call296, %class.btVector3* nonnull align 4 dereferenceable(16) %231, %class.btVector3* nonnull align 4 dereferenceable(16) %normal285, %class.btVector3* nonnull align 4 dereferenceable(16) %ref, float %232, float %233, float 0xC00921FB60000000, float 0x400921FB60000000, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp297, i1 zeroext false, float 1.000000e+01)
  br label %if.end318

if.else304:                                       ; preds = %if.then209
  %236 = load float, float* %minFi, align 4
  %237 = load float, float* %maxFi, align 4
  %cmp305 = fcmp olt float %236, %237
  br i1 %cmp305, label %if.then306, label %if.end317

if.then306:                                       ; preds = %if.else304
  %238 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %239 = bitcast %class.btCollisionWorld* %238 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable307 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %239, align 4
  %vfn308 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable307, i64 5
  %240 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn308, align 4
  %call309 = call %class.btIDebugDraw* %240(%class.btCollisionWorld* %238)
  %241 = load %class.btVector3*, %class.btVector3** %center212, align 4
  %242 = load float, float* %dbgDrawSize, align 4
  %243 = load float, float* %dbgDrawSize, align 4
  %244 = load float, float* %minFi, align 4
  %245 = load float, float* %maxFi, align 4
  store float 0.000000e+00, float* %ref.tmp311, align 4
  store float 0.000000e+00, float* %ref.tmp312, align 4
  store float 0.000000e+00, float* %ref.tmp313, align 4
  %call314 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp310, float* nonnull align 4 dereferenceable(4) %ref.tmp311, float* nonnull align 4 dereferenceable(4) %ref.tmp312, float* nonnull align 4 dereferenceable(4) %ref.tmp313)
  %246 = bitcast %class.btIDebugDraw* %call309 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)***
  %vtable315 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*** %246, align 4
  %vfn316 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)** %vtable315, i64 17
  %247 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)** %vfn316, align 4
  call void %247(%class.btIDebugDraw* %call309, %class.btVector3* nonnull align 4 dereferenceable(16) %241, %class.btVector3* nonnull align 4 dereferenceable(16) %normal285, %class.btVector3* nonnull align 4 dereferenceable(16) %ref, float %242, float %243, float %244, float %245, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp310, i1 zeroext true, float 1.000000e+01)
  br label %if.end317

if.end317:                                        ; preds = %if.then306, %if.else304
  br label %if.end318

if.end318:                                        ; preds = %if.end317, %if.then293
  %248 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %p6DOF, align 4
  %call319 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK23btGeneric6DofConstraint23getCalculatedTransformAEv(%class.btGeneric6DofConstraint* %248)
  %call320 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %tr187, %class.btTransform* nonnull align 4 dereferenceable(64) %call319)
  %249 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %p6DOF, align 4
  %call321 = call %class.btTranslationalLimitMotor* @_ZN23btGeneric6DofConstraint26getTranslationalLimitMotorEv(%class.btGeneric6DofConstraint* %249)
  %m_lowerLimit = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %call321, i32 0, i32 0
  %250 = bitcast %class.btVector3* %bbMin to i8*
  %251 = bitcast %class.btVector3* %m_lowerLimit to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %250, i8* align 4 %251, i32 16, i1 false)
  %252 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %p6DOF, align 4
  %call322 = call %class.btTranslationalLimitMotor* @_ZN23btGeneric6DofConstraint26getTranslationalLimitMotorEv(%class.btGeneric6DofConstraint* %252)
  %m_upperLimit = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %call322, i32 0, i32 1
  %253 = bitcast %class.btVector3* %bbMax to i8*
  %254 = bitcast %class.btVector3* %m_upperLimit to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %253, i8* align 4 %254, i32 16, i1 false)
  %255 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %256 = bitcast %class.btCollisionWorld* %255 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable323 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %256, align 4
  %vfn324 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable323, i64 5
  %257 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn324, align 4
  %call325 = call %class.btIDebugDraw* %257(%class.btCollisionWorld* %255)
  store float 0.000000e+00, float* %ref.tmp327, align 4
  store float 0.000000e+00, float* %ref.tmp328, align 4
  store float 0.000000e+00, float* %ref.tmp329, align 4
  %call330 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp326, float* nonnull align 4 dereferenceable(4) %ref.tmp327, float* nonnull align 4 dereferenceable(4) %ref.tmp328, float* nonnull align 4 dereferenceable(4) %ref.tmp329)
  %258 = bitcast %class.btIDebugDraw* %call325 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btTransform*, %class.btVector3*)***
  %vtable331 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btTransform*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btTransform*, %class.btVector3*)*** %258, align 4
  %vfn332 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btTransform*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btTransform*, %class.btVector3*)** %vtable331, i64 20
  %259 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btTransform*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btTransform*, %class.btVector3*)** %vfn332, align 4
  call void %259(%class.btIDebugDraw* %call325, %class.btVector3* nonnull align 4 dereferenceable(16) %bbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %bbMax, %class.btTransform* nonnull align 4 dereferenceable(64) %tr187, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp326)
  br label %if.end333

if.end333:                                        ; preds = %if.end318, %if.end207
  br label %sw.epilog

sw.bb334:                                         ; preds = %if.end
  %260 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint.addr, align 4
  %261 = bitcast %class.btTypedConstraint* %260 to %class.btGeneric6DofSpring2Constraint*
  store %class.btGeneric6DofSpring2Constraint* %261, %class.btGeneric6DofSpring2Constraint** %p6DOF335, align 4
  %262 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %p6DOF335, align 4
  %call337 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK30btGeneric6DofSpring2Constraint23getCalculatedTransformAEv(%class.btGeneric6DofSpring2Constraint* %262)
  %call338 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %tr336, %class.btTransform* nonnull align 4 dereferenceable(64) %call337)
  %263 = load i8, i8* %drawFrames, align 1
  %tobool339 = trunc i8 %263 to i1
  br i1 %tobool339, label %if.then340, label %if.end346

if.then340:                                       ; preds = %sw.bb334
  %264 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %265 = bitcast %class.btCollisionWorld* %264 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable341 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %265, align 4
  %vfn342 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable341, i64 5
  %266 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn342, align 4
  %call343 = call %class.btIDebugDraw* %266(%class.btCollisionWorld* %264)
  %267 = load float, float* %dbgDrawSize, align 4
  %268 = bitcast %class.btIDebugDraw* %call343 to void (%class.btIDebugDraw*, %class.btTransform*, float)***
  %vtable344 = load void (%class.btIDebugDraw*, %class.btTransform*, float)**, void (%class.btIDebugDraw*, %class.btTransform*, float)*** %268, align 4
  %vfn345 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vtable344, i64 16
  %269 = load void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vfn345, align 4
  call void %269(%class.btIDebugDraw* %call343, %class.btTransform* nonnull align 4 dereferenceable(64) %tr336, float %267)
  br label %if.end346

if.end346:                                        ; preds = %if.then340, %sw.bb334
  %270 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %p6DOF335, align 4
  %call347 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK30btGeneric6DofSpring2Constraint23getCalculatedTransformBEv(%class.btGeneric6DofSpring2Constraint* %270)
  %call348 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %tr336, %class.btTransform* nonnull align 4 dereferenceable(64) %call347)
  %271 = load i8, i8* %drawFrames, align 1
  %tobool349 = trunc i8 %271 to i1
  br i1 %tobool349, label %if.then350, label %if.end356

if.then350:                                       ; preds = %if.end346
  %272 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %273 = bitcast %class.btCollisionWorld* %272 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable351 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %273, align 4
  %vfn352 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable351, i64 5
  %274 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn352, align 4
  %call353 = call %class.btIDebugDraw* %274(%class.btCollisionWorld* %272)
  %275 = load float, float* %dbgDrawSize, align 4
  %276 = bitcast %class.btIDebugDraw* %call353 to void (%class.btIDebugDraw*, %class.btTransform*, float)***
  %vtable354 = load void (%class.btIDebugDraw*, %class.btTransform*, float)**, void (%class.btIDebugDraw*, %class.btTransform*, float)*** %276, align 4
  %vfn355 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vtable354, i64 16
  %277 = load void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vfn355, align 4
  call void %277(%class.btIDebugDraw* %call353, %class.btTransform* nonnull align 4 dereferenceable(64) %tr336, float %275)
  br label %if.end356

if.end356:                                        ; preds = %if.then350, %if.end346
  %278 = load i8, i8* %drawLimits, align 1
  %tobool357 = trunc i8 %278 to i1
  br i1 %tobool357, label %if.then358, label %if.end503

if.then358:                                       ; preds = %if.end356
  %279 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %p6DOF335, align 4
  %call359 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK30btGeneric6DofSpring2Constraint23getCalculatedTransformAEv(%class.btGeneric6DofSpring2Constraint* %279)
  %call360 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %tr336, %class.btTransform* nonnull align 4 dereferenceable(64) %call359)
  %280 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %p6DOF335, align 4
  %call362 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK30btGeneric6DofSpring2Constraint23getCalculatedTransformBEv(%class.btGeneric6DofSpring2Constraint* %280)
  %call363 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call362)
  store %class.btVector3* %call363, %class.btVector3** %center361, align 4
  %call365 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %tr336)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %up364, %class.btMatrix3x3* %call365, i32 2)
  %call367 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %tr336)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis366, %class.btMatrix3x3* %call367, i32 0)
  %281 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %p6DOF335, align 4
  %call369 = call %class.btRotationalLimitMotor2* @_ZN30btGeneric6DofSpring2Constraint23getRotationalLimitMotorEi(%class.btGeneric6DofSpring2Constraint* %281, i32 1)
  %m_loLimit370 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %call369, i32 0, i32 0
  %282 = load float, float* %m_loLimit370, align 4
  store float %282, float* %minTh368, align 4
  %283 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %p6DOF335, align 4
  %call372 = call %class.btRotationalLimitMotor2* @_ZN30btGeneric6DofSpring2Constraint23getRotationalLimitMotorEi(%class.btGeneric6DofSpring2Constraint* %283, i32 1)
  %m_hiLimit373 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %call372, i32 0, i32 1
  %284 = load float, float* %m_hiLimit373, align 4
  store float %284, float* %maxTh371, align 4
  %285 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %p6DOF335, align 4
  %call375 = call %class.btRotationalLimitMotor2* @_ZN30btGeneric6DofSpring2Constraint23getRotationalLimitMotorEi(%class.btGeneric6DofSpring2Constraint* %285, i32 2)
  %m_loLimit376 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %call375, i32 0, i32 0
  %286 = load float, float* %m_loLimit376, align 4
  store float %286, float* %minPs374, align 4
  %287 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %p6DOF335, align 4
  %call378 = call %class.btRotationalLimitMotor2* @_ZN30btGeneric6DofSpring2Constraint23getRotationalLimitMotorEi(%class.btGeneric6DofSpring2Constraint* %287, i32 2)
  %m_hiLimit379 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %call378, i32 0, i32 1
  %288 = load float, float* %m_hiLimit379, align 4
  store float %288, float* %maxPs377, align 4
  %289 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %290 = bitcast %class.btCollisionWorld* %289 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable380 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %290, align 4
  %vfn381 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable380, i64 5
  %291 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn381, align 4
  %call382 = call %class.btIDebugDraw* %291(%class.btCollisionWorld* %289)
  %292 = load %class.btVector3*, %class.btVector3** %center361, align 4
  %293 = load float, float* %dbgDrawSize, align 4
  %mul383 = fmul float %293, 0x3FECCCCCC0000000
  %294 = load float, float* %minTh368, align 4
  %295 = load float, float* %maxTh371, align 4
  %296 = load float, float* %minPs374, align 4
  %297 = load float, float* %maxPs377, align 4
  store float 0.000000e+00, float* %ref.tmp385, align 4
  store float 0.000000e+00, float* %ref.tmp386, align 4
  store float 0.000000e+00, float* %ref.tmp387, align 4
  %call388 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp384, float* nonnull align 4 dereferenceable(4) %ref.tmp385, float* nonnull align 4 dereferenceable(4) %ref.tmp386, float* nonnull align 4 dereferenceable(4) %ref.tmp387)
  %298 = bitcast %class.btIDebugDraw* %call382 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, float, %class.btVector3*, float, i1)***
  %vtable389 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, float, %class.btVector3*, float, i1)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, float, %class.btVector3*, float, i1)*** %298, align 4
  %vfn390 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, float, %class.btVector3*, float, i1)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, float, %class.btVector3*, float, i1)** %vtable389, i64 18
  %299 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, float, %class.btVector3*, float, i1)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, float, %class.btVector3*, float, i1)** %vfn390, align 4
  call void %299(%class.btIDebugDraw* %call382, %class.btVector3* nonnull align 4 dereferenceable(16) %292, %class.btVector3* nonnull align 4 dereferenceable(16) %up364, %class.btVector3* nonnull align 4 dereferenceable(16) %axis366, float %mul383, float %294, float %295, float %296, float %297, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp384, float 1.000000e+01, i1 zeroext true)
  %call392 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %tr336)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp391, %class.btMatrix3x3* %call392, i32 1)
  %300 = bitcast %class.btVector3* %axis366 to i8*
  %301 = bitcast %class.btVector3* %ref.tmp391 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %300, i8* align 4 %301, i32 16, i1 false)
  %302 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %p6DOF335, align 4
  %call394 = call float @_ZNK30btGeneric6DofSpring2Constraint8getAngleEi(%class.btGeneric6DofSpring2Constraint* %302, i32 1)
  store float %call394, float* %ay393, align 4
  %303 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %p6DOF335, align 4
  %call396 = call float @_ZNK30btGeneric6DofSpring2Constraint8getAngleEi(%class.btGeneric6DofSpring2Constraint* %303, i32 2)
  store float %call396, float* %az395, align 4
  %304 = load float, float* %ay393, align 4
  %call398 = call float @_Z5btCosf(float %304)
  store float %call398, float* %cy397, align 4
  %305 = load float, float* %ay393, align 4
  %call400 = call float @_Z5btSinf(float %305)
  store float %call400, float* %sy399, align 4
  %306 = load float, float* %az395, align 4
  %call402 = call float @_Z5btCosf(float %306)
  store float %call402, float* %cz401, align 4
  %307 = load float, float* %az395, align 4
  %call404 = call float @_Z5btSinf(float %307)
  store float %call404, float* %sz403, align 4
  %call406 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref405)
  %308 = load float, float* %cy397, align 4
  %309 = load float, float* %cz401, align 4
  %mul407 = fmul float %308, %309
  %call408 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis366)
  %arrayidx409 = getelementptr inbounds float, float* %call408, i32 0
  %310 = load float, float* %arrayidx409, align 4
  %mul410 = fmul float %mul407, %310
  %311 = load float, float* %cy397, align 4
  %312 = load float, float* %sz403, align 4
  %mul411 = fmul float %311, %312
  %call412 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis366)
  %arrayidx413 = getelementptr inbounds float, float* %call412, i32 1
  %313 = load float, float* %arrayidx413, align 4
  %mul414 = fmul float %mul411, %313
  %add415 = fadd float %mul410, %mul414
  %314 = load float, float* %sy399, align 4
  %call416 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis366)
  %arrayidx417 = getelementptr inbounds float, float* %call416, i32 2
  %315 = load float, float* %arrayidx417, align 4
  %mul418 = fmul float %314, %315
  %sub419 = fsub float %add415, %mul418
  %call420 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ref405)
  %arrayidx421 = getelementptr inbounds float, float* %call420, i32 0
  store float %sub419, float* %arrayidx421, align 4
  %316 = load float, float* %sz403, align 4
  %fneg422 = fneg float %316
  %call423 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis366)
  %arrayidx424 = getelementptr inbounds float, float* %call423, i32 0
  %317 = load float, float* %arrayidx424, align 4
  %mul425 = fmul float %fneg422, %317
  %318 = load float, float* %cz401, align 4
  %call426 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis366)
  %arrayidx427 = getelementptr inbounds float, float* %call426, i32 1
  %319 = load float, float* %arrayidx427, align 4
  %mul428 = fmul float %318, %319
  %add429 = fadd float %mul425, %mul428
  %call430 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ref405)
  %arrayidx431 = getelementptr inbounds float, float* %call430, i32 1
  store float %add429, float* %arrayidx431, align 4
  %320 = load float, float* %cz401, align 4
  %321 = load float, float* %sy399, align 4
  %mul432 = fmul float %320, %321
  %call433 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis366)
  %arrayidx434 = getelementptr inbounds float, float* %call433, i32 0
  %322 = load float, float* %arrayidx434, align 4
  %mul435 = fmul float %mul432, %322
  %323 = load float, float* %sz403, align 4
  %324 = load float, float* %sy399, align 4
  %mul436 = fmul float %323, %324
  %call437 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis366)
  %arrayidx438 = getelementptr inbounds float, float* %call437, i32 1
  %325 = load float, float* %arrayidx438, align 4
  %mul439 = fmul float %mul436, %325
  %add440 = fadd float %mul435, %mul439
  %326 = load float, float* %cy397, align 4
  %call441 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis366)
  %arrayidx442 = getelementptr inbounds float, float* %call441, i32 2
  %327 = load float, float* %arrayidx442, align 4
  %mul443 = fmul float %326, %327
  %add444 = fadd float %add440, %mul443
  %call445 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ref405)
  %arrayidx446 = getelementptr inbounds float, float* %call445, i32 2
  store float %add444, float* %arrayidx446, align 4
  %328 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %p6DOF335, align 4
  %call447 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK30btGeneric6DofSpring2Constraint23getCalculatedTransformBEv(%class.btGeneric6DofSpring2Constraint* %328)
  %call448 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %tr336, %class.btTransform* nonnull align 4 dereferenceable(64) %call447)
  %call451 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %tr336)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp450, %class.btMatrix3x3* %call451, i32 0)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %normal449, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp450)
  %329 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %p6DOF335, align 4
  %call453 = call %class.btRotationalLimitMotor2* @_ZN30btGeneric6DofSpring2Constraint23getRotationalLimitMotorEi(%class.btGeneric6DofSpring2Constraint* %329, i32 0)
  %m_loLimit454 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %call453, i32 0, i32 0
  %330 = load float, float* %m_loLimit454, align 4
  store float %330, float* %minFi452, align 4
  %331 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %p6DOF335, align 4
  %call456 = call %class.btRotationalLimitMotor2* @_ZN30btGeneric6DofSpring2Constraint23getRotationalLimitMotorEi(%class.btGeneric6DofSpring2Constraint* %331, i32 0)
  %m_hiLimit457 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %call456, i32 0, i32 1
  %332 = load float, float* %m_hiLimit457, align 4
  store float %332, float* %maxFi455, align 4
  %333 = load float, float* %minFi452, align 4
  %334 = load float, float* %maxFi455, align 4
  %cmp458 = fcmp ogt float %333, %334
  br i1 %cmp458, label %if.then459, label %if.else470

if.then459:                                       ; preds = %if.then358
  %335 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %336 = bitcast %class.btCollisionWorld* %335 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable460 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %336, align 4
  %vfn461 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable460, i64 5
  %337 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn461, align 4
  %call462 = call %class.btIDebugDraw* %337(%class.btCollisionWorld* %335)
  %338 = load %class.btVector3*, %class.btVector3** %center361, align 4
  %339 = load float, float* %dbgDrawSize, align 4
  %340 = load float, float* %dbgDrawSize, align 4
  store float 0.000000e+00, float* %ref.tmp464, align 4
  store float 0.000000e+00, float* %ref.tmp465, align 4
  store float 0.000000e+00, float* %ref.tmp466, align 4
  %call467 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp463, float* nonnull align 4 dereferenceable(4) %ref.tmp464, float* nonnull align 4 dereferenceable(4) %ref.tmp465, float* nonnull align 4 dereferenceable(4) %ref.tmp466)
  %341 = bitcast %class.btIDebugDraw* %call462 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)***
  %vtable468 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*** %341, align 4
  %vfn469 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)** %vtable468, i64 17
  %342 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)** %vfn469, align 4
  call void %342(%class.btIDebugDraw* %call462, %class.btVector3* nonnull align 4 dereferenceable(16) %338, %class.btVector3* nonnull align 4 dereferenceable(16) %normal449, %class.btVector3* nonnull align 4 dereferenceable(16) %ref405, float %339, float %340, float 0xC00921FB60000000, float 0x400921FB60000000, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp463, i1 zeroext false, float 1.000000e+01)
  br label %if.end484

if.else470:                                       ; preds = %if.then358
  %343 = load float, float* %minFi452, align 4
  %344 = load float, float* %maxFi455, align 4
  %cmp471 = fcmp olt float %343, %344
  br i1 %cmp471, label %if.then472, label %if.end483

if.then472:                                       ; preds = %if.else470
  %345 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %346 = bitcast %class.btCollisionWorld* %345 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable473 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %346, align 4
  %vfn474 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable473, i64 5
  %347 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn474, align 4
  %call475 = call %class.btIDebugDraw* %347(%class.btCollisionWorld* %345)
  %348 = load %class.btVector3*, %class.btVector3** %center361, align 4
  %349 = load float, float* %dbgDrawSize, align 4
  %350 = load float, float* %dbgDrawSize, align 4
  %351 = load float, float* %minFi452, align 4
  %352 = load float, float* %maxFi455, align 4
  store float 0.000000e+00, float* %ref.tmp477, align 4
  store float 0.000000e+00, float* %ref.tmp478, align 4
  store float 0.000000e+00, float* %ref.tmp479, align 4
  %call480 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp476, float* nonnull align 4 dereferenceable(4) %ref.tmp477, float* nonnull align 4 dereferenceable(4) %ref.tmp478, float* nonnull align 4 dereferenceable(4) %ref.tmp479)
  %353 = bitcast %class.btIDebugDraw* %call475 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)***
  %vtable481 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*** %353, align 4
  %vfn482 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)** %vtable481, i64 17
  %354 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)** %vfn482, align 4
  call void %354(%class.btIDebugDraw* %call475, %class.btVector3* nonnull align 4 dereferenceable(16) %348, %class.btVector3* nonnull align 4 dereferenceable(16) %normal449, %class.btVector3* nonnull align 4 dereferenceable(16) %ref405, float %349, float %350, float %351, float %352, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp476, i1 zeroext true, float 1.000000e+01)
  br label %if.end483

if.end483:                                        ; preds = %if.then472, %if.else470
  br label %if.end484

if.end484:                                        ; preds = %if.end483, %if.then459
  %355 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %p6DOF335, align 4
  %call485 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK30btGeneric6DofSpring2Constraint23getCalculatedTransformAEv(%class.btGeneric6DofSpring2Constraint* %355)
  %call486 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %tr336, %class.btTransform* nonnull align 4 dereferenceable(64) %call485)
  %356 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %p6DOF335, align 4
  %call488 = call %class.btTranslationalLimitMotor2* @_ZN30btGeneric6DofSpring2Constraint26getTranslationalLimitMotorEv(%class.btGeneric6DofSpring2Constraint* %356)
  %m_lowerLimit489 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %call488, i32 0, i32 0
  %357 = bitcast %class.btVector3* %bbMin487 to i8*
  %358 = bitcast %class.btVector3* %m_lowerLimit489 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %357, i8* align 4 %358, i32 16, i1 false)
  %359 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %p6DOF335, align 4
  %call491 = call %class.btTranslationalLimitMotor2* @_ZN30btGeneric6DofSpring2Constraint26getTranslationalLimitMotorEv(%class.btGeneric6DofSpring2Constraint* %359)
  %m_upperLimit492 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %call491, i32 0, i32 1
  %360 = bitcast %class.btVector3* %bbMax490 to i8*
  %361 = bitcast %class.btVector3* %m_upperLimit492 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %360, i8* align 4 %361, i32 16, i1 false)
  %362 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %363 = bitcast %class.btCollisionWorld* %362 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable493 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %363, align 4
  %vfn494 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable493, i64 5
  %364 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn494, align 4
  %call495 = call %class.btIDebugDraw* %364(%class.btCollisionWorld* %362)
  store float 0.000000e+00, float* %ref.tmp497, align 4
  store float 0.000000e+00, float* %ref.tmp498, align 4
  store float 0.000000e+00, float* %ref.tmp499, align 4
  %call500 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp496, float* nonnull align 4 dereferenceable(4) %ref.tmp497, float* nonnull align 4 dereferenceable(4) %ref.tmp498, float* nonnull align 4 dereferenceable(4) %ref.tmp499)
  %365 = bitcast %class.btIDebugDraw* %call495 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btTransform*, %class.btVector3*)***
  %vtable501 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btTransform*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btTransform*, %class.btVector3*)*** %365, align 4
  %vfn502 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btTransform*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btTransform*, %class.btVector3*)** %vtable501, i64 20
  %366 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btTransform*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btTransform*, %class.btVector3*)** %vfn502, align 4
  call void %366(%class.btIDebugDraw* %call495, %class.btVector3* nonnull align 4 dereferenceable(16) %bbMin487, %class.btVector3* nonnull align 4 dereferenceable(16) %bbMax490, %class.btTransform* nonnull align 4 dereferenceable(64) %tr336, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp496)
  br label %if.end503

if.end503:                                        ; preds = %if.end484, %if.end356
  br label %sw.epilog

sw.bb504:                                         ; preds = %if.end
  %367 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint.addr, align 4
  %368 = bitcast %class.btTypedConstraint* %367 to %class.btSliderConstraint*
  store %class.btSliderConstraint* %368, %class.btSliderConstraint** %pSlider, align 4
  %369 = load %class.btSliderConstraint*, %class.btSliderConstraint** %pSlider, align 4
  %call506 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK18btSliderConstraint23getCalculatedTransformAEv(%class.btSliderConstraint* %369)
  %call507 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %tr505, %class.btTransform* nonnull align 4 dereferenceable(64) %call506)
  %370 = load i8, i8* %drawFrames, align 1
  %tobool508 = trunc i8 %370 to i1
  br i1 %tobool508, label %if.then509, label %if.end515

if.then509:                                       ; preds = %sw.bb504
  %371 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %372 = bitcast %class.btCollisionWorld* %371 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable510 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %372, align 4
  %vfn511 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable510, i64 5
  %373 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn511, align 4
  %call512 = call %class.btIDebugDraw* %373(%class.btCollisionWorld* %371)
  %374 = load float, float* %dbgDrawSize, align 4
  %375 = bitcast %class.btIDebugDraw* %call512 to void (%class.btIDebugDraw*, %class.btTransform*, float)***
  %vtable513 = load void (%class.btIDebugDraw*, %class.btTransform*, float)**, void (%class.btIDebugDraw*, %class.btTransform*, float)*** %375, align 4
  %vfn514 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vtable513, i64 16
  %376 = load void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vfn514, align 4
  call void %376(%class.btIDebugDraw* %call512, %class.btTransform* nonnull align 4 dereferenceable(64) %tr505, float %374)
  br label %if.end515

if.end515:                                        ; preds = %if.then509, %sw.bb504
  %377 = load %class.btSliderConstraint*, %class.btSliderConstraint** %pSlider, align 4
  %call516 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK18btSliderConstraint23getCalculatedTransformBEv(%class.btSliderConstraint* %377)
  %call517 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %tr505, %class.btTransform* nonnull align 4 dereferenceable(64) %call516)
  %378 = load i8, i8* %drawFrames, align 1
  %tobool518 = trunc i8 %378 to i1
  br i1 %tobool518, label %if.then519, label %if.end525

if.then519:                                       ; preds = %if.end515
  %379 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %380 = bitcast %class.btCollisionWorld* %379 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable520 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %380, align 4
  %vfn521 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable520, i64 5
  %381 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn521, align 4
  %call522 = call %class.btIDebugDraw* %381(%class.btCollisionWorld* %379)
  %382 = load float, float* %dbgDrawSize, align 4
  %383 = bitcast %class.btIDebugDraw* %call522 to void (%class.btIDebugDraw*, %class.btTransform*, float)***
  %vtable523 = load void (%class.btIDebugDraw*, %class.btTransform*, float)**, void (%class.btIDebugDraw*, %class.btTransform*, float)*** %383, align 4
  %vfn524 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vtable523, i64 16
  %384 = load void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vfn524, align 4
  call void %384(%class.btIDebugDraw* %call522, %class.btTransform* nonnull align 4 dereferenceable(64) %tr505, float %382)
  br label %if.end525

if.end525:                                        ; preds = %if.then519, %if.end515
  %385 = load i8, i8* %drawLimits, align 1
  %tobool526 = trunc i8 %385 to i1
  br i1 %tobool526, label %if.then527, label %if.end574

if.then527:                                       ; preds = %if.end525
  %386 = load %class.btSliderConstraint*, %class.btSliderConstraint** %pSlider, align 4
  %call529 = call zeroext i1 @_ZN18btSliderConstraint27getUseLinearReferenceFrameAEv(%class.btSliderConstraint* %386)
  br i1 %call529, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then527
  %387 = load %class.btSliderConstraint*, %class.btSliderConstraint** %pSlider, align 4
  %call530 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK18btSliderConstraint23getCalculatedTransformAEv(%class.btSliderConstraint* %387)
  br label %cond.end

cond.false:                                       ; preds = %if.then527
  %388 = load %class.btSliderConstraint*, %class.btSliderConstraint** %pSlider, align 4
  %call531 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK18btSliderConstraint23getCalculatedTransformBEv(%class.btSliderConstraint* %388)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi %class.btTransform* [ %call530, %cond.true ], [ %call531, %cond.false ]
  %call532 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %tr528, %class.btTransform* nonnull align 4 dereferenceable(64) %cond-lvalue)
  %389 = load %class.btSliderConstraint*, %class.btSliderConstraint** %pSlider, align 4
  %call535 = call float @_ZN18btSliderConstraint16getLowerLinLimitEv(%class.btSliderConstraint* %389)
  store float %call535, float* %ref.tmp534, align 4
  store float 0.000000e+00, float* %ref.tmp536, align 4
  store float 0.000000e+00, float* %ref.tmp537, align 4
  %call538 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp533, float* nonnull align 4 dereferenceable(4) %ref.tmp534, float* nonnull align 4 dereferenceable(4) %ref.tmp536, float* nonnull align 4 dereferenceable(4) %ref.tmp537)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %li_min, %class.btTransform* %tr528, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp533)
  %390 = load %class.btSliderConstraint*, %class.btSliderConstraint** %pSlider, align 4
  %call541 = call float @_ZN18btSliderConstraint16getUpperLinLimitEv(%class.btSliderConstraint* %390)
  store float %call541, float* %ref.tmp540, align 4
  store float 0.000000e+00, float* %ref.tmp542, align 4
  store float 0.000000e+00, float* %ref.tmp543, align 4
  %call544 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp539, float* nonnull align 4 dereferenceable(4) %ref.tmp540, float* nonnull align 4 dereferenceable(4) %ref.tmp542, float* nonnull align 4 dereferenceable(4) %ref.tmp543)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %li_max, %class.btTransform* %tr528, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp539)
  %391 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %392 = bitcast %class.btCollisionWorld* %391 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable545 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %392, align 4
  %vfn546 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable545, i64 5
  %393 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn546, align 4
  %call547 = call %class.btIDebugDraw* %393(%class.btCollisionWorld* %391)
  store float 0.000000e+00, float* %ref.tmp549, align 4
  store float 0.000000e+00, float* %ref.tmp550, align 4
  store float 0.000000e+00, float* %ref.tmp551, align 4
  %call552 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp548, float* nonnull align 4 dereferenceable(4) %ref.tmp549, float* nonnull align 4 dereferenceable(4) %ref.tmp550, float* nonnull align 4 dereferenceable(4) %ref.tmp551)
  %394 = bitcast %class.btIDebugDraw* %call547 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable553 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %394, align 4
  %vfn554 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable553, i64 4
  %395 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn554, align 4
  call void %395(%class.btIDebugDraw* %call547, %class.btVector3* nonnull align 4 dereferenceable(16) %li_min, %class.btVector3* nonnull align 4 dereferenceable(16) %li_max, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp548)
  %call556 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %tr528)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %normal555, %class.btMatrix3x3* %call556, i32 0)
  %call558 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %tr528)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis557, %class.btMatrix3x3* %call558, i32 1)
  %396 = load %class.btSliderConstraint*, %class.btSliderConstraint** %pSlider, align 4
  %call559 = call float @_ZN18btSliderConstraint16getLowerAngLimitEv(%class.btSliderConstraint* %396)
  store float %call559, float* %a_min, align 4
  %397 = load %class.btSliderConstraint*, %class.btSliderConstraint** %pSlider, align 4
  %call560 = call float @_ZN18btSliderConstraint16getUpperAngLimitEv(%class.btSliderConstraint* %397)
  store float %call560, float* %a_max, align 4
  %398 = load %class.btSliderConstraint*, %class.btSliderConstraint** %pSlider, align 4
  %call562 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK18btSliderConstraint23getCalculatedTransformBEv(%class.btSliderConstraint* %398)
  %call563 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call562)
  store %class.btVector3* %call563, %class.btVector3** %center561, align 4
  %399 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %400 = bitcast %class.btCollisionWorld* %399 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable564 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %400, align 4
  %vfn565 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable564, i64 5
  %401 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn565, align 4
  %call566 = call %class.btIDebugDraw* %401(%class.btCollisionWorld* %399)
  %402 = load %class.btVector3*, %class.btVector3** %center561, align 4
  %403 = load float, float* %dbgDrawSize, align 4
  %404 = load float, float* %dbgDrawSize, align 4
  %405 = load float, float* %a_min, align 4
  %406 = load float, float* %a_max, align 4
  store float 0.000000e+00, float* %ref.tmp568, align 4
  store float 0.000000e+00, float* %ref.tmp569, align 4
  store float 0.000000e+00, float* %ref.tmp570, align 4
  %call571 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp567, float* nonnull align 4 dereferenceable(4) %ref.tmp568, float* nonnull align 4 dereferenceable(4) %ref.tmp569, float* nonnull align 4 dereferenceable(4) %ref.tmp570)
  %407 = bitcast %class.btIDebugDraw* %call566 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)***
  %vtable572 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*** %407, align 4
  %vfn573 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)** %vtable572, i64 17
  %408 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float, float, float, float, %class.btVector3*, i1, float)** %vfn573, align 4
  call void %408(%class.btIDebugDraw* %call566, %class.btVector3* nonnull align 4 dereferenceable(16) %402, %class.btVector3* nonnull align 4 dereferenceable(16) %normal555, %class.btVector3* nonnull align 4 dereferenceable(16) %axis557, float %403, float %404, float %405, float %406, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp567, i1 zeroext true, float 1.000000e+01)
  br label %if.end574

if.end574:                                        ; preds = %cond.end, %if.end525
  br label %sw.epilog

sw.default:                                       ; preds = %if.end
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %if.end574, %if.end503, %if.end333, %if.end185, %if.end87, %if.then66, %if.end36
  br label %return

return:                                           ; preds = %sw.epilog, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN17btTypedConstraint14getDbgDrawSizeEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_dbgDrawSize = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 11
  %0 = load float, float* %m_dbgDrawSize, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btTypedConstraint17getConstraintTypeEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = bitcast %class.btTypedConstraint* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %struct.btTypedObject*
  %m_objectType = getelementptr inbounds %struct.btTypedObject, %struct.btTypedObject* %1, i32 0, i32 0
  %2 = load i32, i32* %m_objectType, align 4
  ret i32 %2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btPoint2PointConstraint11getPivotInAEv(%class.btPoint2PointConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPoint2PointConstraint*, align 4
  store %class.btPoint2PointConstraint* %this, %class.btPoint2PointConstraint** %this.addr, align 4
  %this1 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %this.addr, align 4
  %m_pivotInA = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 2
  ret %class.btVector3* %m_pivotInA
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btPoint2PointConstraint11getPivotInBEv(%class.btPoint2PointConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPoint2PointConstraint*, align 4
  store %class.btPoint2PointConstraint* %this, %class.btPoint2PointConstraint** %this.addr, align 4
  %this1 = load %class.btPoint2PointConstraint*, %class.btPoint2PointConstraint** %this.addr, align 4
  %m_pivotInB = getelementptr inbounds %class.btPoint2PointConstraint, %class.btPoint2PointConstraint* %this1, i32 0, i32 3
  ret %class.btVector3* %m_pivotInB
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyAEv(%class.btHingeConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 8
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  ret %class.btRigidBody* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btHingeConstraint9getAFrameEv(%class.btHingeConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  ret %class.btTransform* %m_rbAFrame
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyBEv(%class.btHingeConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 9
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  ret %class.btRigidBody* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btHingeConstraint9getBFrameEv(%class.btHingeConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  ret %class.btTransform* %m_rbBFrame
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK17btHingeConstraint13getLowerLimitEv(%class.btHingeConstraint* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call = call float @_ZNK14btAngularLimit6getLowEv(%class.btAngularLimit* %m_limit)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK17btHingeConstraint13getUpperLimitEv(%class.btHingeConstraint* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call = call float @_ZNK14btAngularLimit7getHighEv(%class.btAngularLimit* %m_limit)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btHingeConstraint8hasLimitEv(%class.btHingeConstraint* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call = call float @_ZNK14btAngularLimit12getHalfRangeEv(%class.btAngularLimit* %m_limit)
  %cmp = fcmp ogt float %call, 0.000000e+00
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %1 = load i32, i32* %i.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 2
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %2 = load i32, i32* %i.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %2
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %arrayidx2, float* nonnull align 4 dereferenceable(4) %arrayidx6, float* nonnull align 4 dereferenceable(4) %arrayidx10)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyAEv(%class.btConeTwistConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 8
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  ret %class.btRigidBody* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK21btConeTwistConstraint9getAFrameEv(%class.btConeTwistConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  ret %class.btTransform* %m_rbAFrame
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyBEv(%class.btConeTwistConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 9
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  ret %class.btRigidBody* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK21btConeTwistConstraint9getBFrameEv(%class.btConeTwistConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  ret %class.btTransform* %m_rbBFrame
}

declare void @_ZNK21btConeTwistConstraint16GetPointForAngleEff(%class.btVector3* sret align 4, %class.btConeTwistConstraint*, float, float) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK21btConeTwistConstraint12getTwistSpanEv(%class.btConeTwistConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %m_twistSpan = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %0 = load float, float* %m_twistSpan, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK21btConeTwistConstraint13getTwistAngleEv(%class.btConeTwistConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %m_twistAngle = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 19
  %0 = load float, float* %m_twistAngle, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK23btGeneric6DofConstraint23getCalculatedTransformAEv(%class.btGeneric6DofConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %m_calculatedTransformA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 8
  ret %class.btTransform* %m_calculatedTransformA
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK23btGeneric6DofConstraint23getCalculatedTransformBEv(%class.btGeneric6DofConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %m_calculatedTransformB = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 9
  ret %class.btTransform* %m_calculatedTransformB
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btRotationalLimitMotor* @_ZN23btGeneric6DofConstraint23getRotationalLimitMotorEi(%class.btGeneric6DofConstraint* %this, i32 %index) #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %index.addr = alloca i32, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %0 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits, i32 0, i32 %0
  ret %class.btRotationalLimitMotor* %arrayidx
}

declare float @_ZNK23btGeneric6DofConstraint8getAngleEi(%class.btGeneric6DofConstraint*, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btCosf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btSinf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.sin.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btTranslationalLimitMotor* @_ZN23btGeneric6DofConstraint26getTranslationalLimitMotorEv(%class.btGeneric6DofConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  ret %class.btTranslationalLimitMotor* %m_linearLimits
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK30btGeneric6DofSpring2Constraint23getCalculatedTransformAEv(%class.btGeneric6DofSpring2Constraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %m_calculatedTransformA = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 8
  ret %class.btTransform* %m_calculatedTransformA
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK30btGeneric6DofSpring2Constraint23getCalculatedTransformBEv(%class.btGeneric6DofSpring2Constraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %m_calculatedTransformB = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 9
  ret %class.btTransform* %m_calculatedTransformB
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btRotationalLimitMotor2* @_ZN30btGeneric6DofSpring2Constraint23getRotationalLimitMotorEi(%class.btGeneric6DofSpring2Constraint* %this, i32 %index) #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %index.addr = alloca i32, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %0 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %0
  ret %class.btRotationalLimitMotor2* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK30btGeneric6DofSpring2Constraint8getAngleEi(%class.btGeneric6DofSpring2Constraint* %this, i32 %axis_index) #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %axis_index.addr = alloca i32, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 %axis_index, i32* %axis_index.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %m_calculatedAxisAngleDiff = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 10
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_calculatedAxisAngleDiff)
  %0 = load i32, i32* %axis_index.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %0
  %1 = load float, float* %arrayidx, align 4
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btTranslationalLimitMotor2* @_ZN30btGeneric6DofSpring2Constraint26getTranslationalLimitMotorEv(%class.btGeneric6DofSpring2Constraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  ret %class.btTranslationalLimitMotor2* %m_linearLimits
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK18btSliderConstraint23getCalculatedTransformAEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_calculatedTransformA = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 41
  ret %class.btTransform* %m_calculatedTransformA
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK18btSliderConstraint23getCalculatedTransformBEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_calculatedTransformB = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 42
  ret %class.btTransform* %m_calculatedTransformB
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN18btSliderConstraint27getUseLinearReferenceFrameAEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_useLinearReferenceFrameA = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 5
  %0 = load i8, i8* %m_useLinearReferenceFrameA, align 4
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN18btSliderConstraint16getLowerLinLimitEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_lowerLinLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 6
  %0 = load float, float* %m_lowerLinLimit, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN18btSliderConstraint16getUpperLinLimitEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_upperLinLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 7
  %0 = load float, float* %m_upperLinLimit, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN18btSliderConstraint16getLowerAngLimitEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_lowerAngLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 8
  %0 = load float, float* %m_lowerAngLimit, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN18btSliderConstraint16getUpperAngLimitEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_upperAngLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 9
  %0 = load float, float* %m_upperAngLimit, align 4
  ret float %0
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver(%class.btDiscreteDynamicsWorld* %this, %class.btConstraintSolver* %solver) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %solver.addr = alloca %class.btConstraintSolver*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btConstraintSolver* %solver, %class.btConstraintSolver** %solver.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %m_ownsConstraintSolver = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 11
  %0 = load i8, i8* %m_ownsConstraintSolver, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_constraintSolver = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 3
  %1 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver, align 4
  %2 = bitcast %class.btConstraintSolver* %1 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %2)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_ownsConstraintSolver2 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 11
  store i8 0, i8* %m_ownsConstraintSolver2, align 1
  %3 = load %class.btConstraintSolver*, %class.btConstraintSolver** %solver.addr, align 4
  %m_constraintSolver3 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 3
  store %class.btConstraintSolver* %3, %class.btConstraintSolver** %m_constraintSolver3, align 4
  %4 = load %class.btConstraintSolver*, %class.btConstraintSolver** %solver.addr, align 4
  %m_solverIslandCallback = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 2
  %5 = load %struct.InplaceSolverIslandCallback*, %struct.InplaceSolverIslandCallback** %m_solverIslandCallback, align 4
  %m_solver = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %5, i32 0, i32 2
  store %class.btConstraintSolver* %4, %class.btConstraintSolver** %m_solver, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btConstraintSolver* @_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv(%class.btDiscreteDynamicsWorld* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %m_constraintSolver = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 3
  %0 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver, align 4
  ret %class.btConstraintSolver* %0
}

; Function Attrs: noinline optnone
define hidden i32 @_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv(%class.btDiscreteDynamicsWorld* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %m_constraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 5
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %m_constraints)
  ret i32 %call
}

; Function Attrs: noinline optnone
define hidden %class.btTypedConstraint* @_ZN23btDiscreteDynamicsWorld13getConstraintEi(%class.btDiscreteDynamicsWorld* %this, i32 %index) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %index.addr = alloca i32, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %m_constraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 5
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.4* %m_constraints, i32 %0)
  %1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %call, align 4
  ret %class.btTypedConstraint* %1
}

; Function Attrs: noinline optnone
define hidden %class.btTypedConstraint* @_ZNK23btDiscreteDynamicsWorld13getConstraintEi(%class.btDiscreteDynamicsWorld* %this, i32 %index) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %index.addr = alloca i32, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %m_constraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 5
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZNK20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.4* %m_constraints, i32 %0)
  %1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %call, align 4
  ret %class.btTypedConstraint* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZNK20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %0, i32 %1
  ret %class.btTypedConstraint** %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld20serializeRigidBodiesEP12btSerializer(%class.btDiscreteDynamicsWorld* %this, %class.btSerializer* %serializer) #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %i = alloca i32, align 4
  %colObj = alloca %class.btCollisionObject*, align 4
  %len = alloca i32, align 4
  %chunk = alloca %class.btChunk*, align 4
  %structType = alloca i8*, align 4
  %constraint = alloca %class.btTypedConstraint*, align 4
  %size = alloca i32, align 4
  %chunk23 = alloca %class.btChunk*, align 4
  %structType27 = alloca i8*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_collisionObjects)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects2 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %2, i32 0, i32 1
  %3 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_collisionObjects2, i32 %3)
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %call3, align 4
  store %class.btCollisionObject* %4, %class.btCollisionObject** %colObj, align 4
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4
  %call4 = call i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %5)
  %and = and i32 %call4, 2
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4
  %7 = bitcast %class.btCollisionObject* %6 to i32 (%class.btCollisionObject*)***
  %vtable = load i32 (%class.btCollisionObject*)**, i32 (%class.btCollisionObject*)*** %7, align 4
  %vfn = getelementptr inbounds i32 (%class.btCollisionObject*)*, i32 (%class.btCollisionObject*)** %vtable, i64 4
  %8 = load i32 (%class.btCollisionObject*)*, i32 (%class.btCollisionObject*)** %vfn, align 4
  %call5 = call i32 %8(%class.btCollisionObject* %6)
  store i32 %call5, i32* %len, align 4
  %9 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %10 = load i32, i32* %len, align 4
  %11 = bitcast %class.btSerializer* %9 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable6 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %11, align 4
  %vfn7 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable6, i64 4
  %12 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn7, align 4
  %call8 = call %class.btChunk* %12(%class.btSerializer* %9, i32 %10, i32 1)
  store %class.btChunk* %call8, %class.btChunk** %chunk, align 4
  %13 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4
  %14 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %14, i32 0, i32 2
  %15 = load i8*, i8** %m_oldPtr, align 4
  %16 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %17 = bitcast %class.btCollisionObject* %13 to i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)***
  %vtable9 = load i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)**, i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)*** %17, align 4
  %vfn10 = getelementptr inbounds i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)*, i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)** %vtable9, i64 5
  %18 = load i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)*, i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)** %vfn10, align 4
  %call11 = call i8* %18(%class.btCollisionObject* %13, i8* %15, %class.btSerializer* %16)
  store i8* %call11, i8** %structType, align 4
  %19 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %20 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %21 = load i8*, i8** %structType, align 4
  %22 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4
  %23 = bitcast %class.btCollisionObject* %22 to i8*
  %24 = bitcast %class.btSerializer* %19 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable12 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %24, align 4
  %vfn13 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable12, i64 5
  %25 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn13, align 4
  call void %25(%class.btSerializer* %19, %class.btChunk* %20, i8* %21, i32 1497645650, i8* %23)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %26 = load i32, i32* %i, align 4
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc34, %for.end
  %27 = load i32, i32* %i, align 4
  %m_constraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 5
  %call15 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %m_constraints)
  %cmp16 = icmp slt i32 %27, %call15
  br i1 %cmp16, label %for.body17, label %for.end36

for.body17:                                       ; preds = %for.cond14
  %m_constraints18 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 5
  %28 = load i32, i32* %i, align 4
  %call19 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.4* %m_constraints18, i32 %28)
  %29 = load %class.btTypedConstraint*, %class.btTypedConstraint** %call19, align 4
  store %class.btTypedConstraint* %29, %class.btTypedConstraint** %constraint, align 4
  %30 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint, align 4
  %31 = bitcast %class.btTypedConstraint* %30 to i32 (%class.btTypedConstraint*)***
  %vtable20 = load i32 (%class.btTypedConstraint*)**, i32 (%class.btTypedConstraint*)*** %31, align 4
  %vfn21 = getelementptr inbounds i32 (%class.btTypedConstraint*)*, i32 (%class.btTypedConstraint*)** %vtable20, i64 9
  %32 = load i32 (%class.btTypedConstraint*)*, i32 (%class.btTypedConstraint*)** %vfn21, align 4
  %call22 = call i32 %32(%class.btTypedConstraint* %30)
  store i32 %call22, i32* %size, align 4
  %33 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %34 = load i32, i32* %size, align 4
  %35 = bitcast %class.btSerializer* %33 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable24 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %35, align 4
  %vfn25 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable24, i64 4
  %36 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn25, align 4
  %call26 = call %class.btChunk* %36(%class.btSerializer* %33, i32 %34, i32 1)
  store %class.btChunk* %call26, %class.btChunk** %chunk23, align 4
  %37 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint, align 4
  %38 = load %class.btChunk*, %class.btChunk** %chunk23, align 4
  %m_oldPtr28 = getelementptr inbounds %class.btChunk, %class.btChunk* %38, i32 0, i32 2
  %39 = load i8*, i8** %m_oldPtr28, align 4
  %40 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %41 = bitcast %class.btTypedConstraint* %37 to i8* (%class.btTypedConstraint*, i8*, %class.btSerializer*)***
  %vtable29 = load i8* (%class.btTypedConstraint*, i8*, %class.btSerializer*)**, i8* (%class.btTypedConstraint*, i8*, %class.btSerializer*)*** %41, align 4
  %vfn30 = getelementptr inbounds i8* (%class.btTypedConstraint*, i8*, %class.btSerializer*)*, i8* (%class.btTypedConstraint*, i8*, %class.btSerializer*)** %vtable29, i64 10
  %42 = load i8* (%class.btTypedConstraint*, i8*, %class.btSerializer*)*, i8* (%class.btTypedConstraint*, i8*, %class.btSerializer*)** %vfn30, align 4
  %call31 = call i8* %42(%class.btTypedConstraint* %37, i8* %39, %class.btSerializer* %40)
  store i8* %call31, i8** %structType27, align 4
  %43 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %44 = load %class.btChunk*, %class.btChunk** %chunk23, align 4
  %45 = load i8*, i8** %structType27, align 4
  %46 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint, align 4
  %47 = bitcast %class.btTypedConstraint* %46 to i8*
  %48 = bitcast %class.btSerializer* %43 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable32 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %48, align 4
  %vfn33 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable32, i64 5
  %49 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn33, align 4
  call void %49(%class.btSerializer* %43, %class.btChunk* %44, i8* %45, i32 1397641027, i8* %47)
  br label %for.inc34

for.inc34:                                        ; preds = %for.body17
  %50 = load i32, i32* %i, align 4
  %inc35 = add nsw i32 %50, 1
  store i32 %inc35, i32* %i, align 4
  br label %for.cond14

for.end36:                                        ; preds = %for.cond14
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_internalType = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 24
  %0 = load i32, i32* %m_internalType, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld26serializeDynamicsWorldInfoEP12btSerializer(%class.btDiscreteDynamicsWorld* %this, %class.btSerializer* %serializer) #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %len = alloca i32, align 4
  %chunk = alloca %class.btChunk*, align 4
  %worldInfo = alloca %struct.btDynamicsWorldFloatData*, align 4
  %structType = alloca i8*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store i32 104, i32* %len, align 4
  %0 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %1 = load i32, i32* %len, align 4
  %2 = bitcast %class.btSerializer* %0 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %2, align 4
  %vfn = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable, i64 4
  %3 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn, align 4
  %call = call %class.btChunk* %3(%class.btSerializer* %0, i32 %1, i32 1)
  store %class.btChunk* %call, %class.btChunk** %chunk, align 4
  %4 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %4, i32 0, i32 2
  %5 = load i8*, i8** %m_oldPtr, align 4
  %6 = bitcast i8* %5 to %struct.btDynamicsWorldFloatData*
  store %struct.btDynamicsWorldFloatData* %6, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %7 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %8 = bitcast %struct.btDynamicsWorldFloatData* %7 to i8*
  %9 = load i32, i32* %len, align 4
  call void @llvm.memset.p0i8.i32(i8* align 4 %8, i8 0, i32 %9, i1 false)
  %m_gravity = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 7
  %10 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_gravity2 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %10, i32 0, i32 1
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_gravity, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_gravity2)
  %11 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call3 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %11)
  %12 = bitcast %struct.btContactSolverInfo* %call3 to %struct.btContactSolverInfoData*
  %m_tau = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %12, i32 0, i32 0
  %13 = load float, float* %m_tau, align 4
  %14 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %14, i32 0, i32 0
  %m_tau4 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo, i32 0, i32 0
  store float %13, float* %m_tau4, align 4
  %15 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call5 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %15)
  %16 = bitcast %struct.btContactSolverInfo* %call5 to %struct.btContactSolverInfoData*
  %m_damping = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %16, i32 0, i32 1
  %17 = load float, float* %m_damping, align 4
  %18 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo6 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %18, i32 0, i32 0
  %m_damping7 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo6, i32 0, i32 1
  store float %17, float* %m_damping7, align 4
  %19 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call8 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %19)
  %20 = bitcast %struct.btContactSolverInfo* %call8 to %struct.btContactSolverInfoData*
  %m_friction = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %20, i32 0, i32 2
  %21 = load float, float* %m_friction, align 4
  %22 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo9 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %22, i32 0, i32 0
  %m_friction10 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo9, i32 0, i32 2
  store float %21, float* %m_friction10, align 4
  %23 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call11 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %23)
  %24 = bitcast %struct.btContactSolverInfo* %call11 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %24, i32 0, i32 3
  %25 = load float, float* %m_timeStep, align 4
  %26 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo12 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %26, i32 0, i32 0
  %m_timeStep13 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo12, i32 0, i32 3
  store float %25, float* %m_timeStep13, align 4
  %27 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call14 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %27)
  %28 = bitcast %struct.btContactSolverInfo* %call14 to %struct.btContactSolverInfoData*
  %m_restitution = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %28, i32 0, i32 4
  %29 = load float, float* %m_restitution, align 4
  %30 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo15 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %30, i32 0, i32 0
  %m_restitution16 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo15, i32 0, i32 4
  store float %29, float* %m_restitution16, align 4
  %31 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call17 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %31)
  %32 = bitcast %struct.btContactSolverInfo* %call17 to %struct.btContactSolverInfoData*
  %m_maxErrorReduction = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %32, i32 0, i32 6
  %33 = load float, float* %m_maxErrorReduction, align 4
  %34 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo18 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %34, i32 0, i32 0
  %m_maxErrorReduction19 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo18, i32 0, i32 5
  store float %33, float* %m_maxErrorReduction19, align 4
  %35 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call20 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %35)
  %36 = bitcast %struct.btContactSolverInfo* %call20 to %struct.btContactSolverInfoData*
  %m_sor = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %36, i32 0, i32 7
  %37 = load float, float* %m_sor, align 4
  %38 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo21 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %38, i32 0, i32 0
  %m_sor22 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo21, i32 0, i32 6
  store float %37, float* %m_sor22, align 4
  %39 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call23 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %39)
  %40 = bitcast %struct.btContactSolverInfo* %call23 to %struct.btContactSolverInfoData*
  %m_erp = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %40, i32 0, i32 8
  %41 = load float, float* %m_erp, align 4
  %42 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo24 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %42, i32 0, i32 0
  %m_erp25 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo24, i32 0, i32 7
  store float %41, float* %m_erp25, align 4
  %43 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call26 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %43)
  %44 = bitcast %struct.btContactSolverInfo* %call26 to %struct.btContactSolverInfoData*
  %m_erp2 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %44, i32 0, i32 9
  %45 = load float, float* %m_erp2, align 4
  %46 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo27 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %46, i32 0, i32 0
  %m_erp228 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo27, i32 0, i32 8
  store float %45, float* %m_erp228, align 4
  %47 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call29 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %47)
  %48 = bitcast %struct.btContactSolverInfo* %call29 to %struct.btContactSolverInfoData*
  %m_globalCfm = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %48, i32 0, i32 10
  %49 = load float, float* %m_globalCfm, align 4
  %50 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo30 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %50, i32 0, i32 0
  %m_globalCfm31 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo30, i32 0, i32 9
  store float %49, float* %m_globalCfm31, align 4
  %51 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call32 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %51)
  %52 = bitcast %struct.btContactSolverInfo* %call32 to %struct.btContactSolverInfoData*
  %m_splitImpulsePenetrationThreshold = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %52, i32 0, i32 12
  %53 = load float, float* %m_splitImpulsePenetrationThreshold, align 4
  %54 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo33 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %54, i32 0, i32 0
  %m_splitImpulsePenetrationThreshold34 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo33, i32 0, i32 10
  store float %53, float* %m_splitImpulsePenetrationThreshold34, align 4
  %55 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call35 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %55)
  %56 = bitcast %struct.btContactSolverInfo* %call35 to %struct.btContactSolverInfoData*
  %m_splitImpulseTurnErp = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %56, i32 0, i32 13
  %57 = load float, float* %m_splitImpulseTurnErp, align 4
  %58 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo36 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %58, i32 0, i32 0
  %m_splitImpulseTurnErp37 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo36, i32 0, i32 11
  store float %57, float* %m_splitImpulseTurnErp37, align 4
  %59 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call38 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %59)
  %60 = bitcast %struct.btContactSolverInfo* %call38 to %struct.btContactSolverInfoData*
  %m_linearSlop = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %60, i32 0, i32 14
  %61 = load float, float* %m_linearSlop, align 4
  %62 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo39 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %62, i32 0, i32 0
  %m_linearSlop40 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo39, i32 0, i32 12
  store float %61, float* %m_linearSlop40, align 4
  %63 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call41 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %63)
  %64 = bitcast %struct.btContactSolverInfo* %call41 to %struct.btContactSolverInfoData*
  %m_warmstartingFactor = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %64, i32 0, i32 15
  %65 = load float, float* %m_warmstartingFactor, align 4
  %66 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo42 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %66, i32 0, i32 0
  %m_warmstartingFactor43 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo42, i32 0, i32 13
  store float %65, float* %m_warmstartingFactor43, align 4
  %67 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call44 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %67)
  %68 = bitcast %struct.btContactSolverInfo* %call44 to %struct.btContactSolverInfoData*
  %m_maxGyroscopicForce = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %68, i32 0, i32 19
  %69 = load float, float* %m_maxGyroscopicForce, align 4
  %70 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo45 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %70, i32 0, i32 0
  %m_maxGyroscopicForce46 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo45, i32 0, i32 14
  store float %69, float* %m_maxGyroscopicForce46, align 4
  %71 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call47 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %71)
  %72 = bitcast %struct.btContactSolverInfo* %call47 to %struct.btContactSolverInfoData*
  %m_singleAxisRollingFrictionThreshold = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %72, i32 0, i32 20
  %73 = load float, float* %m_singleAxisRollingFrictionThreshold, align 4
  %74 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo48 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %74, i32 0, i32 0
  %m_singleAxisRollingFrictionThreshold49 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo48, i32 0, i32 15
  store float %73, float* %m_singleAxisRollingFrictionThreshold49, align 4
  %75 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call50 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %75)
  %76 = bitcast %struct.btContactSolverInfo* %call50 to %struct.btContactSolverInfoData*
  %m_numIterations = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %76, i32 0, i32 5
  %77 = load i32, i32* %m_numIterations, align 4
  %78 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo51 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %78, i32 0, i32 0
  %m_numIterations52 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo51, i32 0, i32 16
  store i32 %77, i32* %m_numIterations52, align 4
  %79 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call53 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %79)
  %80 = bitcast %struct.btContactSolverInfo* %call53 to %struct.btContactSolverInfoData*
  %m_solverMode = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %80, i32 0, i32 16
  %81 = load i32, i32* %m_solverMode, align 4
  %82 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo54 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %82, i32 0, i32 0
  %m_solverMode55 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo54, i32 0, i32 17
  store i32 %81, i32* %m_solverMode55, align 4
  %83 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call56 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %83)
  %84 = bitcast %struct.btContactSolverInfo* %call56 to %struct.btContactSolverInfoData*
  %m_restingContactRestitutionThreshold = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %84, i32 0, i32 17
  %85 = load i32, i32* %m_restingContactRestitutionThreshold, align 4
  %86 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo57 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %86, i32 0, i32 0
  %m_restingContactRestitutionThreshold58 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo57, i32 0, i32 18
  store i32 %85, i32* %m_restingContactRestitutionThreshold58, align 4
  %87 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call59 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %87)
  %88 = bitcast %struct.btContactSolverInfo* %call59 to %struct.btContactSolverInfoData*
  %m_minimumSolverBatchSize = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %88, i32 0, i32 18
  %89 = load i32, i32* %m_minimumSolverBatchSize, align 4
  %90 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo60 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %90, i32 0, i32 0
  %m_minimumSolverBatchSize61 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo60, i32 0, i32 19
  store i32 %89, i32* %m_minimumSolverBatchSize61, align 4
  %91 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call62 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %91)
  %92 = bitcast %struct.btContactSolverInfo* %call62 to %struct.btContactSolverInfoData*
  %m_splitImpulse = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %92, i32 0, i32 11
  %93 = load i32, i32* %m_splitImpulse, align 4
  %94 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %m_solverInfo63 = getelementptr inbounds %struct.btDynamicsWorldFloatData, %struct.btDynamicsWorldFloatData* %94, i32 0, i32 0
  %m_splitImpulse64 = getelementptr inbounds %struct.btContactSolverInfoFloatData, %struct.btContactSolverInfoFloatData* %m_solverInfo63, i32 0, i32 20
  store i32 %93, i32* %m_splitImpulse64, align 4
  store i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.15, i32 0, i32 0), i8** %structType, align 4
  %95 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %96 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %97 = load i8*, i8** %structType, align 4
  %98 = load %struct.btDynamicsWorldFloatData*, %struct.btDynamicsWorldFloatData** %worldInfo, align 4
  %99 = bitcast %struct.btDynamicsWorldFloatData* %98 to i8*
  %100 = bitcast %class.btSerializer* %95 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable65 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %100, align 4
  %vfn66 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable65, i64 5
  %101 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn66, align 4
  call void %101(%class.btSerializer* %95, %class.btChunk* %96, i8* %97, i32 1145853764, i8* %99)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %2, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDiscreteDynamicsWorld9serializeEP12btSerializer(%class.btDiscreteDynamicsWorld* %this, %class.btSerializer* %serializer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %1 = bitcast %class.btSerializer* %0 to void (%class.btSerializer*)***
  %vtable = load void (%class.btSerializer*)**, void (%class.btSerializer*)*** %1, align 4
  %vfn = getelementptr inbounds void (%class.btSerializer*)*, void (%class.btSerializer*)** %vtable, i64 8
  %2 = load void (%class.btSerializer*)*, void (%class.btSerializer*)** %vfn, align 4
  call void %2(%class.btSerializer* %0)
  %3 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  call void @_ZN23btDiscreteDynamicsWorld26serializeDynamicsWorldInfoEP12btSerializer(%class.btDiscreteDynamicsWorld* %this1, %class.btSerializer* %3)
  %4 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  call void @_ZN16btCollisionWorld25serializeCollisionObjectsEP12btSerializer(%class.btCollisionWorld* %4, %class.btSerializer* %5)
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  call void @_ZN23btDiscreteDynamicsWorld20serializeRigidBodiesEP12btSerializer(%class.btDiscreteDynamicsWorld* %this1, %class.btSerializer* %6)
  %7 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %8 = bitcast %class.btSerializer* %7 to void (%class.btSerializer*)***
  %vtable2 = load void (%class.btSerializer*)**, void (%class.btSerializer*)*** %8, align 4
  %vfn3 = getelementptr inbounds void (%class.btSerializer*)*, void (%class.btSerializer*)** %vtable2, i64 9
  %9 = load void (%class.btSerializer*)*, void (%class.btSerializer*)** %vfn3, align 4
  call void %9(%class.btSerializer* %7)
  ret void
}

declare void @_ZN16btCollisionWorld25serializeCollisionObjectsEP12btSerializer(%class.btCollisionWorld*, %class.btSerializer*) #3

declare void @_ZN16btCollisionWorld11updateAabbsEv(%class.btCollisionWorld*) unnamed_addr #3

declare void @_ZN16btCollisionWorld23computeOverlappingPairsEv(%class.btCollisionWorld*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw(%class.btCollisionWorld* %this, %class.btIDebugDraw* %debugDrawer) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 5
  store %class.btIDebugDraw* %0, %class.btIDebugDraw** %m_debugDrawer, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btIDebugDraw* @_ZN16btCollisionWorld14getDebugDrawerEv(%class.btCollisionWorld* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 5
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4
  ret %class.btIDebugDraw* %0
}

declare void @_ZN16btCollisionWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3(%class.btCollisionWorld*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

declare void @_ZNK16btCollisionWorld7rayTestERK9btVector3S2_RNS_17RayResultCallbackE(%class.btCollisionWorld*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(24)) unnamed_addr #3

declare void @_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv(%class.btCollisionWorld*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv(%class.btDiscreteDynamicsWorld* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  ret i32 2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN23btDiscreteDynamicsWorld11setNumTasksEi(%class.btDiscreteDynamicsWorld* %this, i32 %numTasks) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %numTasks.addr = alloca i32, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store i32 %numTasks, i32* %numTasks.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN23btDiscreteDynamicsWorld14updateVehiclesEf(%class.btDiscreteDynamicsWorld* %this, float %timeStep) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = load float, float* %timeStep.addr, align 4
  call void @_ZN23btDiscreteDynamicsWorld13updateActionsEf(%class.btDiscreteDynamicsWorld* %this1, float %0)
  ret void
}

declare %class.btCollisionWorld* @_ZN16btCollisionWorldC2EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration(%class.btCollisionWorld* returned, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btCollisionConfiguration*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btContactSolverInfo* @_ZN19btContactSolverInfoC2Ev(%struct.btContactSolverInfo* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btContactSolverInfo*, align 4
  store %struct.btContactSolverInfo* %this, %struct.btContactSolverInfo** %this.addr, align 4
  %this1 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %this.addr, align 4
  %0 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %1 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_tau = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %1, i32 0, i32 0
  store float 0x3FE3333340000000, float* %m_tau, align 4
  %2 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_damping = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %2, i32 0, i32 1
  store float 1.000000e+00, float* %m_damping, align 4
  %3 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_friction = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %3, i32 0, i32 2
  store float 0x3FD3333340000000, float* %m_friction, align 4
  %4 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %4, i32 0, i32 3
  store float 0x3F91111120000000, float* %m_timeStep, align 4
  %5 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_restitution = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %5, i32 0, i32 4
  store float 0.000000e+00, float* %m_restitution, align 4
  %6 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_maxErrorReduction = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %6, i32 0, i32 6
  store float 2.000000e+01, float* %m_maxErrorReduction, align 4
  %7 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_numIterations = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %7, i32 0, i32 5
  store i32 10, i32* %m_numIterations, align 4
  %8 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_erp = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %8, i32 0, i32 8
  store float 0x3FC99999A0000000, float* %m_erp, align 4
  %9 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_erp2 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %9, i32 0, i32 9
  store float 0x3FC99999A0000000, float* %m_erp2, align 4
  %10 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_globalCfm = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %10, i32 0, i32 10
  store float 0.000000e+00, float* %m_globalCfm, align 4
  %11 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_sor = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %11, i32 0, i32 7
  store float 1.000000e+00, float* %m_sor, align 4
  %12 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_splitImpulse = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %12, i32 0, i32 11
  store i32 1, i32* %m_splitImpulse, align 4
  %13 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_splitImpulsePenetrationThreshold = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %13, i32 0, i32 12
  store float 0xBFA47AE140000000, float* %m_splitImpulsePenetrationThreshold, align 4
  %14 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_splitImpulseTurnErp = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %14, i32 0, i32 13
  store float 0x3FB99999A0000000, float* %m_splitImpulseTurnErp, align 4
  %15 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_linearSlop = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %15, i32 0, i32 14
  store float 0.000000e+00, float* %m_linearSlop, align 4
  %16 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_warmstartingFactor = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %16, i32 0, i32 15
  store float 0x3FEB333340000000, float* %m_warmstartingFactor, align 4
  %17 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_solverMode = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %17, i32 0, i32 16
  store i32 260, i32* %m_solverMode, align 4
  %18 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_restingContactRestitutionThreshold = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %18, i32 0, i32 17
  store i32 2, i32* %m_restingContactRestitutionThreshold, align 4
  %19 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_minimumSolverBatchSize = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %19, i32 0, i32 18
  store i32 128, i32* %m_minimumSolverBatchSize, align 4
  %20 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_maxGyroscopicForce = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %20, i32 0, i32 19
  store float 1.000000e+02, float* %m_maxGyroscopicForce, align 4
  %21 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_singleAxisRollingFrictionThreshold = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %21, i32 0, i32 20
  store float 0x46293E5940000000, float* %m_singleAxisRollingFrictionThreshold, align 4
  %22 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_leastSquaresResidualThreshold = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %22, i32 0, i32 21
  store float 0.000000e+00, float* %m_leastSquaresResidualThreshold, align 4
  ret %struct.btContactSolverInfo* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btDynamicsWorld* @_ZN15btDynamicsWorldD2Ev(%class.btDynamicsWorld* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btDynamicsWorld* %this1 to %class.btCollisionWorld*
  %call = call %class.btCollisionWorld* @_ZN16btCollisionWorldD2Ev(%class.btCollisionWorld* %0) #10
  ret %class.btDynamicsWorld* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btDynamicsWorldD0Ev(%class.btDynamicsWorld* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

declare void @_ZN16btCollisionWorld9serializeEP12btSerializer(%class.btCollisionWorld*, %class.btSerializer*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btDynamicsWorld13addConstraintEP17btTypedConstraintb(%class.btDynamicsWorld* %this, %class.btTypedConstraint* %constraint, i1 zeroext %disableCollisionsBetweenLinkedBodies) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  %constraint.addr = alloca %class.btTypedConstraint*, align 4
  %disableCollisionsBetweenLinkedBodies.addr = alloca i8, align 1
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4
  store %class.btTypedConstraint* %constraint, %class.btTypedConstraint** %constraint.addr, align 4
  %frombool = zext i1 %disableCollisionsBetweenLinkedBodies to i8
  store i8 %frombool, i8* %disableCollisionsBetweenLinkedBodies.addr, align 1
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btDynamicsWorld16removeConstraintEP17btTypedConstraint(%class.btDynamicsWorld* %this, %class.btTypedConstraint* %constraint) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  %constraint.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4
  store %class.btTypedConstraint* %constraint, %class.btTypedConstraint** %constraint.addr, align 4
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK15btDynamicsWorld17getNumConstraintsEv(%class.btDynamicsWorld* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btTypedConstraint* @_ZN15btDynamicsWorld13getConstraintEi(%class.btDynamicsWorld* %this, i32 %index) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  %index.addr = alloca i32, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  ret %class.btTypedConstraint* null
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btTypedConstraint* @_ZNK15btDynamicsWorld13getConstraintEi(%class.btDynamicsWorld* %this, i32 %index) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  %index.addr = alloca i32, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  ret %class.btTypedConstraint* null
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btDynamicsWorld10addVehicleEP17btActionInterface(%class.btDynamicsWorld* %this, %class.btActionInterface* %vehicle) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  %vehicle.addr = alloca %class.btActionInterface*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4
  store %class.btActionInterface* %vehicle, %class.btActionInterface** %vehicle.addr, align 4
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btDynamicsWorld13removeVehicleEP17btActionInterface(%class.btDynamicsWorld* %this, %class.btActionInterface* %vehicle) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  %vehicle.addr = alloca %class.btActionInterface*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4
  store %class.btActionInterface* %vehicle, %class.btActionInterface** %vehicle.addr, align 4
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btDynamicsWorld12addCharacterEP17btActionInterface(%class.btDynamicsWorld* %this, %class.btActionInterface* %character) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  %character.addr = alloca %class.btActionInterface*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4
  store %class.btActionInterface* %character, %class.btActionInterface** %character.addr, align 4
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btDynamicsWorld15removeCharacterEP17btActionInterface(%class.btDynamicsWorld* %this, %class.btActionInterface* %character) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  %character.addr = alloca %class.btActionInterface*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4
  store %class.btActionInterface* %character, %class.btActionInterface** %character.addr, align 4
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  ret void
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btSimulationIslandManager::IslandCallback"* @_ZN25btSimulationIslandManager14IslandCallbackC2Ev(%"struct.btSimulationIslandManager::IslandCallback"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSimulationIslandManager::IslandCallback"*, align 4
  store %"struct.btSimulationIslandManager::IslandCallback"* %this, %"struct.btSimulationIslandManager::IslandCallback"** %this.addr, align 4
  %this1 = load %"struct.btSimulationIslandManager::IslandCallback"*, %"struct.btSimulationIslandManager::IslandCallback"** %this.addr, align 4
  %0 = bitcast %"struct.btSimulationIslandManager::IslandCallback"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN25btSimulationIslandManager14IslandCallbackE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %"struct.btSimulationIslandManager::IslandCallback"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.InplaceSolverIslandCallback* @_ZN27InplaceSolverIslandCallbackD2Ev(%struct.InplaceSolverIslandCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.InplaceSolverIslandCallback*, align 4
  store %struct.InplaceSolverIslandCallback* %this, %struct.InplaceSolverIslandCallback** %this.addr, align 4
  %this1 = load %struct.InplaceSolverIslandCallback*, %struct.InplaceSolverIslandCallback** %this.addr, align 4
  %0 = bitcast %struct.InplaceSolverIslandCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV27InplaceSolverIslandCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_constraints = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 9
  %call = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray.4* %m_constraints) #10
  %m_manifolds = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 8
  %call2 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.9* %m_manifolds) #10
  %m_bodies = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 7
  %call3 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev(%class.btAlignedObjectArray* %m_bodies) #10
  %1 = bitcast %struct.InplaceSolverIslandCallback* %this1 to %"struct.btSimulationIslandManager::IslandCallback"*
  %call4 = call %"struct.btSimulationIslandManager::IslandCallback"* @_ZN25btSimulationIslandManager14IslandCallbackD2Ev(%"struct.btSimulationIslandManager::IslandCallback"* %1) #10
  ret %struct.InplaceSolverIslandCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN27InplaceSolverIslandCallbackD0Ev(%struct.InplaceSolverIslandCallback* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.InplaceSolverIslandCallback*, align 4
  store %struct.InplaceSolverIslandCallback* %this, %struct.InplaceSolverIslandCallback** %this.addr, align 4
  %this1 = load %struct.InplaceSolverIslandCallback*, %struct.InplaceSolverIslandCallback** %this.addr, align 4
  %call = call %struct.InplaceSolverIslandCallback* @_ZN27InplaceSolverIslandCallbackD2Ev(%struct.InplaceSolverIslandCallback* %this1) #10
  %0 = bitcast %struct.InplaceSolverIslandCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #12
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN27InplaceSolverIslandCallback13processIslandEPP17btCollisionObjectiPP20btPersistentManifoldii(%struct.InplaceSolverIslandCallback* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %class.btPersistentManifold** %manifolds, i32 %numManifolds, i32 %islandId) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.InplaceSolverIslandCallback*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %manifolds.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %islandId.addr = alloca i32, align 4
  %startConstraint = alloca %class.btTypedConstraint**, align 4
  %numCurConstraints = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.InplaceSolverIslandCallback* %this, %struct.InplaceSolverIslandCallback** %this.addr, align 4
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4
  store i32 %numBodies, i32* %numBodies.addr, align 4
  store %class.btPersistentManifold** %manifolds, %class.btPersistentManifold*** %manifolds.addr, align 4
  store i32 %numManifolds, i32* %numManifolds.addr, align 4
  store i32 %islandId, i32* %islandId.addr, align 4
  %this1 = load %struct.InplaceSolverIslandCallback*, %struct.InplaceSolverIslandCallback** %this.addr, align 4
  %0 = load i32, i32* %islandId.addr, align 4
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_solver = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 2
  %1 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_solver, align 4
  %2 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %3 = load i32, i32* %numBodies.addr, align 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifolds.addr, align 4
  %5 = load i32, i32* %numManifolds.addr, align 4
  %m_sortedConstraints = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 3
  %6 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_sortedConstraints, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %6, i32 0
  %m_numConstraints = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 4
  %7 = load i32, i32* %m_numConstraints, align 4
  %m_solverInfo = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 1
  %8 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %m_solverInfo, align 4
  %m_debugDrawer = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 5
  %9 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4
  %m_dispatcher = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 6
  %10 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %11 = bitcast %class.btConstraintSolver* %1 to float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)***
  %vtable = load float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)**, float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*** %11, align 4
  %vfn = getelementptr inbounds float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vtable, i64 3
  %12 = load float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vfn, align 4
  %call = call float %12(%class.btConstraintSolver* %1, %class.btCollisionObject** %2, i32 %3, %class.btPersistentManifold** %4, i32 %5, %class.btTypedConstraint** %arrayidx, i32 %7, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %8, %class.btIDebugDraw* %9, %class.btDispatcher* %10)
  br label %if.end68

if.else:                                          ; preds = %entry
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %startConstraint, align 4
  store i32 0, i32* %numCurConstraints, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %13 = load i32, i32* %i, align 4
  %m_numConstraints2 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 4
  %14 = load i32, i32* %m_numConstraints2, align 4
  %cmp3 = icmp slt i32 %13, %14
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_sortedConstraints4 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 3
  %15 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_sortedConstraints4, align 4
  %16 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %15, i32 %16
  %17 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx5, align 4
  %call6 = call i32 @_Z23btGetConstraintIslandIdPK17btTypedConstraint(%class.btTypedConstraint* %17)
  %18 = load i32, i32* %islandId.addr, align 4
  %cmp7 = icmp eq i32 %call6, %18
  br i1 %cmp7, label %if.then8, label %if.end

if.then8:                                         ; preds = %for.body
  %m_sortedConstraints9 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 3
  %19 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_sortedConstraints9, align 4
  %20 = load i32, i32* %i, align 4
  %arrayidx10 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %19, i32 %20
  store %class.btTypedConstraint** %arrayidx10, %class.btTypedConstraint*** %startConstraint, align 4
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %21 = load i32, i32* %i, align 4
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then8, %for.cond
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc22, %for.end
  %22 = load i32, i32* %i, align 4
  %m_numConstraints12 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 4
  %23 = load i32, i32* %m_numConstraints12, align 4
  %cmp13 = icmp slt i32 %22, %23
  br i1 %cmp13, label %for.body14, label %for.end24

for.body14:                                       ; preds = %for.cond11
  %m_sortedConstraints15 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 3
  %24 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_sortedConstraints15, align 4
  %25 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %24, i32 %25
  %26 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx16, align 4
  %call17 = call i32 @_Z23btGetConstraintIslandIdPK17btTypedConstraint(%class.btTypedConstraint* %26)
  %27 = load i32, i32* %islandId.addr, align 4
  %cmp18 = icmp eq i32 %call17, %27
  br i1 %cmp18, label %if.then19, label %if.end21

if.then19:                                        ; preds = %for.body14
  %28 = load i32, i32* %numCurConstraints, align 4
  %inc20 = add nsw i32 %28, 1
  store i32 %inc20, i32* %numCurConstraints, align 4
  br label %if.end21

if.end21:                                         ; preds = %if.then19, %for.body14
  br label %for.inc22

for.inc22:                                        ; preds = %if.end21
  %29 = load i32, i32* %i, align 4
  %inc23 = add nsw i32 %29, 1
  store i32 %inc23, i32* %i, align 4
  br label %for.cond11

for.end24:                                        ; preds = %for.cond11
  %m_solverInfo25 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 1
  %30 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %m_solverInfo25, align 4
  %31 = bitcast %struct.btContactSolverInfo* %30 to %struct.btContactSolverInfoData*
  %m_minimumSolverBatchSize = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %31, i32 0, i32 18
  %32 = load i32, i32* %m_minimumSolverBatchSize, align 4
  %cmp26 = icmp sle i32 %32, 1
  br i1 %cmp26, label %if.then27, label %if.else35

if.then27:                                        ; preds = %for.end24
  %m_solver28 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 2
  %33 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_solver28, align 4
  %34 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %35 = load i32, i32* %numBodies.addr, align 4
  %36 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifolds.addr, align 4
  %37 = load i32, i32* %numManifolds.addr, align 4
  %38 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %startConstraint, align 4
  %39 = load i32, i32* %numCurConstraints, align 4
  %m_solverInfo29 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 1
  %40 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %m_solverInfo29, align 4
  %m_debugDrawer30 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 5
  %41 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer30, align 4
  %m_dispatcher31 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 6
  %42 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher31, align 4
  %43 = bitcast %class.btConstraintSolver* %33 to float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)***
  %vtable32 = load float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)**, float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*** %43, align 4
  %vfn33 = getelementptr inbounds float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vtable32, i64 3
  %44 = load float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vfn33, align 4
  %call34 = call float %44(%class.btConstraintSolver* %33, %class.btCollisionObject** %34, i32 %35, %class.btPersistentManifold** %36, i32 %37, %class.btTypedConstraint** %38, i32 %39, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %40, %class.btIDebugDraw* %41, %class.btDispatcher* %42)
  br label %if.end67

if.else35:                                        ; preds = %for.end24
  store i32 0, i32* %i, align 4
  br label %for.cond36

for.cond36:                                       ; preds = %for.inc40, %if.else35
  %45 = load i32, i32* %i, align 4
  %46 = load i32, i32* %numBodies.addr, align 4
  %cmp37 = icmp slt i32 %45, %46
  br i1 %cmp37, label %for.body38, label %for.end42

for.body38:                                       ; preds = %for.cond36
  %m_bodies = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 7
  %47 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %48 = load i32, i32* %i, align 4
  %arrayidx39 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %47, i32 %48
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_(%class.btAlignedObjectArray* %m_bodies, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %arrayidx39)
  br label %for.inc40

for.inc40:                                        ; preds = %for.body38
  %49 = load i32, i32* %i, align 4
  %inc41 = add nsw i32 %49, 1
  store i32 %inc41, i32* %i, align 4
  br label %for.cond36

for.end42:                                        ; preds = %for.cond36
  store i32 0, i32* %i, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc47, %for.end42
  %50 = load i32, i32* %i, align 4
  %51 = load i32, i32* %numManifolds.addr, align 4
  %cmp44 = icmp slt i32 %50, %51
  br i1 %cmp44, label %for.body45, label %for.end49

for.body45:                                       ; preds = %for.cond43
  %m_manifolds = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 8
  %52 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifolds.addr, align 4
  %53 = load i32, i32* %i, align 4
  %arrayidx46 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %52, i32 %53
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.9* %m_manifolds, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %arrayidx46)
  br label %for.inc47

for.inc47:                                        ; preds = %for.body45
  %54 = load i32, i32* %i, align 4
  %inc48 = add nsw i32 %54, 1
  store i32 %inc48, i32* %i, align 4
  br label %for.cond43

for.end49:                                        ; preds = %for.cond43
  store i32 0, i32* %i, align 4
  br label %for.cond50

for.cond50:                                       ; preds = %for.inc54, %for.end49
  %55 = load i32, i32* %i, align 4
  %56 = load i32, i32* %numCurConstraints, align 4
  %cmp51 = icmp slt i32 %55, %56
  br i1 %cmp51, label %for.body52, label %for.end56

for.body52:                                       ; preds = %for.cond50
  %m_constraints = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 9
  %57 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %startConstraint, align 4
  %58 = load i32, i32* %i, align 4
  %arrayidx53 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %57, i32 %58
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_(%class.btAlignedObjectArray.4* %m_constraints, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %arrayidx53)
  br label %for.inc54

for.inc54:                                        ; preds = %for.body52
  %59 = load i32, i32* %i, align 4
  %inc55 = add nsw i32 %59, 1
  store i32 %inc55, i32* %i, align 4
  br label %for.cond50

for.end56:                                        ; preds = %for.cond50
  %m_constraints57 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 9
  %call58 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %m_constraints57)
  %m_manifolds59 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 8
  %call60 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %m_manifolds59)
  %add = add nsw i32 %call58, %call60
  %m_solverInfo61 = getelementptr inbounds %struct.InplaceSolverIslandCallback, %struct.InplaceSolverIslandCallback* %this1, i32 0, i32 1
  %60 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %m_solverInfo61, align 4
  %61 = bitcast %struct.btContactSolverInfo* %60 to %struct.btContactSolverInfoData*
  %m_minimumSolverBatchSize62 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %61, i32 0, i32 18
  %62 = load i32, i32* %m_minimumSolverBatchSize62, align 4
  %cmp63 = icmp sgt i32 %add, %62
  br i1 %cmp63, label %if.then64, label %if.else65

if.then64:                                        ; preds = %for.end56
  call void @_ZN27InplaceSolverIslandCallback18processConstraintsEv(%struct.InplaceSolverIslandCallback* %this1)
  br label %if.end66

if.else65:                                        ; preds = %for.end56
  br label %if.end66

if.end66:                                         ; preds = %if.else65, %if.then64
  br label %if.end67

if.end67:                                         ; preds = %if.end66, %if.then27
  br label %if.end68

if.end68:                                         ; preds = %if.end67, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btSimulationIslandManager::IslandCallback"* @_ZN25btSimulationIslandManager14IslandCallbackD2Ev(%"struct.btSimulationIslandManager::IslandCallback"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSimulationIslandManager::IslandCallback"*, align 4
  store %"struct.btSimulationIslandManager::IslandCallback"* %this, %"struct.btSimulationIslandManager::IslandCallback"** %this.addr, align 4
  %this1 = load %"struct.btSimulationIslandManager::IslandCallback"*, %"struct.btSimulationIslandManager::IslandCallback"** %this.addr, align 4
  ret %"struct.btSimulationIslandManager::IslandCallback"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN25btSimulationIslandManager14IslandCallbackD0Ev(%"struct.btSimulationIslandManager::IslandCallback"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSimulationIslandManager::IslandCallback"*, align 4
  store %"struct.btSimulationIslandManager::IslandCallback"* %this, %"struct.btSimulationIslandManager::IslandCallback"** %this.addr, align 4
  %this1 = load %"struct.btSimulationIslandManager::IslandCallback"*, %"struct.btSimulationIslandManager::IslandCallback"** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btCollisionObject** null, %class.btCollisionObject*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %tobool = icmp ne %class.btCollisionObject** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %m_allocator, %class.btCollisionObject** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btCollisionObject** null, %class.btCollisionObject*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %this, %class.btCollisionObject** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btCollisionObject**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %class.btCollisionObject** %ptr, %class.btCollisionObject*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %ptr.addr, align 4
  %1 = bitcast %class.btCollisionObject** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #8

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_Z23btGetConstraintIslandIdPK17btTypedConstraint(%class.btTypedConstraint* %lhs) #2 comdat {
entry:
  %lhs.addr = alloca %class.btTypedConstraint*, align 4
  %islandId = alloca i32, align 4
  %rcolObj0 = alloca %class.btCollisionObject*, align 4
  %rcolObj1 = alloca %class.btCollisionObject*, align 4
  store %class.btTypedConstraint* %lhs, %class.btTypedConstraint** %lhs.addr, align 4
  %0 = load %class.btTypedConstraint*, %class.btTypedConstraint** %lhs.addr, align 4
  %call = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %0)
  %1 = bitcast %class.btRigidBody* %call to %class.btCollisionObject*
  store %class.btCollisionObject* %1, %class.btCollisionObject** %rcolObj0, align 4
  %2 = load %class.btTypedConstraint*, %class.btTypedConstraint** %lhs.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %2)
  %3 = bitcast %class.btRigidBody* %call1 to %class.btCollisionObject*
  store %class.btCollisionObject* %3, %class.btCollisionObject** %rcolObj1, align 4
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj0, align 4
  %call2 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %4)
  %cmp = icmp sge i32 %call2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj0, align 4
  %call3 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %5)
  br label %cond.end

cond.false:                                       ; preds = %entry
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj1, align 4
  %call4 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %6)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call3, %cond.true ], [ %call4, %cond.false ]
  store i32 %cond, i32* %islandId, align 4
  %7 = load i32, i32* %islandId, align 4
  ret i32 %7
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_(%class.btAlignedObjectArray* %this, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.btCollisionObject**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btCollisionObject** %_Val, %class.btCollisionObject*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %1, i32 %2
  %3 = bitcast %class.btCollisionObject** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btCollisionObject**
  %5 = load %class.btCollisionObject**, %class.btCollisionObject*** %_Val.addr, align 4
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %5, align 4
  store %class.btCollisionObject* %6, %class.btCollisionObject** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 8
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  ret %class.btRigidBody* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 9
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  ret %class.btRigidBody* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btCollisionObject**, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btCollisionObject**
  store %class.btCollisionObject** %2, %class.btCollisionObject*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btCollisionObject** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btCollisionObject**, %class.btCollisionObject*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btCollisionObject** %4, %class.btCollisionObject*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btCollisionObject** @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btCollisionObject*** null)
  %2 = bitcast %class.btCollisionObject** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btCollisionObject** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btCollisionObject**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btCollisionObject** %dest, %class.btCollisionObject*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %3, i32 %4
  %5 = bitcast %class.btCollisionObject** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btCollisionObject**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %7, i32 %8
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %arrayidx2, align 4
  store %class.btCollisionObject* %9, %class.btCollisionObject** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionObject** @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %this, i32 %n, %class.btCollisionObject*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btCollisionObject***, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btCollisionObject*** %hint, %class.btCollisionObject**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btCollisionObject**
  ret %class.btCollisionObject** %1
}

; Function Attrs: nounwind
declare %class.btCollisionWorld* @_ZN16btCollisionWorldD2Ev(%class.btCollisionWorld* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float*, float** %_z.addr, align 4
  %4 = load float*, float** %_w.addr, align 4
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %agg.result)
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %agg.result)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q2) #2 comdat {
entry:
  %q1.addr = alloca %class.btQuaternion*, align 4
  %q2.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  store %class.btQuaternion* %q1, %class.btQuaternion** %q1.addr, align 4
  store %class.btQuaternion* %q2, %class.btQuaternion** %q2.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %1 = bitcast %class.btQuaternion* %0 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %1)
  %2 = load float, float* %call, align 4
  %3 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %4 = bitcast %class.btQuaternion* %3 to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %4)
  %5 = load float, float* %call1, align 4
  %mul = fmul float %2, %5
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %7)
  %8 = load float, float* %call2, align 4
  %9 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %10 = bitcast %class.btQuaternion* %9 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %10)
  %11 = load float, float* %call3, align 4
  %mul4 = fmul float %8, %11
  %add = fadd float %mul, %mul4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %13)
  %14 = load float, float* %call5, align 4
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %16)
  %17 = load float, float* %call6, align 4
  %mul7 = fmul float %14, %17
  %add8 = fadd float %add, %mul7
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %22 = bitcast %class.btQuaternion* %21 to %class.btQuadWord*
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %22)
  %23 = load float, float* %call10, align 4
  %mul11 = fmul float %20, %23
  %sub = fsub float %add8, %mul11
  store float %sub, float* %ref.tmp, align 4
  %24 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %25 = bitcast %class.btQuaternion* %24 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %25)
  %26 = load float, float* %call13, align 4
  %27 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %28 = bitcast %class.btQuaternion* %27 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %28)
  %29 = load float, float* %call14, align 4
  %mul15 = fmul float %26, %29
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %31)
  %32 = load float, float* %call16, align 4
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %34)
  %35 = load float, float* %call17, align 4
  %mul18 = fmul float %32, %35
  %add19 = fadd float %mul15, %mul18
  %36 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %37 = bitcast %class.btQuaternion* %36 to %class.btQuadWord*
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %37)
  %38 = load float, float* %call20, align 4
  %39 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %40 = bitcast %class.btQuaternion* %39 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %40)
  %41 = load float, float* %call21, align 4
  %mul22 = fmul float %38, %41
  %add23 = fadd float %add19, %mul22
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %43)
  %44 = load float, float* %call24, align 4
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %46)
  %47 = load float, float* %call25, align 4
  %mul26 = fmul float %44, %47
  %sub27 = fsub float %add23, %mul26
  store float %sub27, float* %ref.tmp12, align 4
  %48 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %49 = bitcast %class.btQuaternion* %48 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %49)
  %50 = load float, float* %call29, align 4
  %51 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %52 = bitcast %class.btQuaternion* %51 to %class.btQuadWord*
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %52)
  %53 = load float, float* %call30, align 4
  %mul31 = fmul float %50, %53
  %54 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %55 = bitcast %class.btQuaternion* %54 to %class.btQuadWord*
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %55)
  %56 = load float, float* %call32, align 4
  %57 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %58 = bitcast %class.btQuaternion* %57 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %58)
  %59 = load float, float* %call33, align 4
  %mul34 = fmul float %56, %59
  %add35 = fadd float %mul31, %mul34
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %61)
  %62 = load float, float* %call36, align 4
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %64)
  %65 = load float, float* %call37, align 4
  %mul38 = fmul float %62, %65
  %add39 = fadd float %add35, %mul38
  %66 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %67 = bitcast %class.btQuaternion* %66 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %67)
  %68 = load float, float* %call40, align 4
  %69 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %70 = bitcast %class.btQuaternion* %69 to %class.btQuadWord*
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %70)
  %71 = load float, float* %call41, align 4
  %mul42 = fmul float %68, %71
  %sub43 = fsub float %add39, %mul42
  store float %sub43, float* %ref.tmp28, align 4
  %72 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %73 = bitcast %class.btQuaternion* %72 to %class.btQuadWord*
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %73)
  %74 = load float, float* %call45, align 4
  %75 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %76 = bitcast %class.btQuaternion* %75 to %class.btQuadWord*
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %76)
  %77 = load float, float* %call46, align 4
  %mul47 = fmul float %74, %77
  %78 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %79 = bitcast %class.btQuaternion* %78 to %class.btQuadWord*
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %79)
  %80 = load float, float* %call48, align 4
  %81 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %82 = bitcast %class.btQuaternion* %81 to %class.btQuadWord*
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %82)
  %83 = load float, float* %call49, align 4
  %mul50 = fmul float %80, %83
  %sub51 = fsub float %mul47, %mul50
  %84 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %85 = bitcast %class.btQuaternion* %84 to %class.btQuadWord*
  %call52 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %85)
  %86 = load float, float* %call52, align 4
  %87 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %88 = bitcast %class.btQuaternion* %87 to %class.btQuadWord*
  %call53 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %88)
  %89 = load float, float* %call53, align 4
  %mul54 = fmul float %86, %89
  %sub55 = fsub float %sub51, %mul54
  %90 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %91 = bitcast %class.btQuaternion* %90 to %class.btQuadWord*
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %91)
  %92 = load float, float* %call56, align 4
  %93 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %94 = bitcast %class.btQuaternion* %93 to %class.btQuadWord*
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %94)
  %95 = load float, float* %call57, align 4
  %mul58 = fmul float %92, %95
  %sub59 = fsub float %sub55, %mul58
  store float %sub59, float* %ref.tmp44, align 4
  %call60 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp44)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btQuaternion* %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #9

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %trace = alloca float, align 4
  %temp = alloca [4 x float], align 16
  %s = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %s64 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx3)
  %1 = load float, float* %call4, align 4
  %add = fadd float %0, %1
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx6)
  %2 = load float, float* %call7, align 4
  %add8 = fadd float %add, %2
  store float %add8, float* %trace, align 4
  %3 = load float, float* %trace, align 4
  %cmp = fcmp ogt float %3, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load float, float* %trace, align 4
  %add9 = fadd float %4, 1.000000e+00
  %call10 = call float @_Z6btSqrtf(float %add9)
  store float %call10, float* %s, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %5, 5.000000e-01
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul, float* %arrayidx11, align 4
  %6 = load float, float* %s, align 4
  %div = fdiv float 5.000000e-01, %6
  store float %div, float* %s, align 4
  %m_el12 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el12, i32 0, i32 2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx13)
  %7 = load float, float* %call14, align 4
  %m_el15 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el15, i32 0, i32 1
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx16)
  %8 = load float, float* %call17, align 4
  %sub = fsub float %7, %8
  %9 = load float, float* %s, align 4
  %mul18 = fmul float %sub, %9
  %arrayidx19 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  store float %mul18, float* %arrayidx19, align 16
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 0
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %10 = load float, float* %call22, align 4
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx24)
  %11 = load float, float* %call25, align 4
  %sub26 = fsub float %10, %11
  %12 = load float, float* %s, align 4
  %mul27 = fmul float %sub26, %12
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  store float %mul27, float* %arrayidx28, align 4
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 1
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %13 = load float, float* %call31, align 4
  %m_el32 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el32, i32 0, i32 0
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx33)
  %14 = load float, float* %call34, align 4
  %sub35 = fsub float %13, %14
  %15 = load float, float* %s, align 4
  %mul36 = fmul float %sub35, %15
  %arrayidx37 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  store float %mul36, float* %arrayidx37, align 8
  br label %if.end

if.else:                                          ; preds = %entry
  %m_el38 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el38, i32 0, i32 0
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx39)
  %16 = load float, float* %call40, align 4
  %m_el41 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx42 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el41, i32 0, i32 1
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx42)
  %17 = load float, float* %call43, align 4
  %cmp44 = fcmp olt float %16, %17
  br i1 %cmp44, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 1
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx46)
  %18 = load float, float* %call47, align 4
  %m_el48 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el48, i32 0, i32 2
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx49)
  %19 = load float, float* %call50, align 4
  %cmp51 = fcmp olt float %18, %19
  %20 = zext i1 %cmp51 to i64
  %cond = select i1 %cmp51, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 0
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx53)
  %21 = load float, float* %call54, align 4
  %m_el55 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el55, i32 0, i32 2
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx56)
  %22 = load float, float* %call57, align 4
  %cmp58 = fcmp olt float %21, %22
  %23 = zext i1 %cmp58 to i64
  %cond59 = select i1 %cmp58, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond60 = phi i32 [ %cond, %cond.true ], [ %cond59, %cond.false ]
  store i32 %cond60, i32* %i, align 4
  %24 = load i32, i32* %i, align 4
  %add61 = add nsw i32 %24, 1
  %rem = srem i32 %add61, 3
  store i32 %rem, i32* %j, align 4
  %25 = load i32, i32* %i, align 4
  %add62 = add nsw i32 %25, 2
  %rem63 = srem i32 %add62, 3
  store i32 %rem63, i32* %k, align 4
  %m_el65 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %26 = load i32, i32* %i, align 4
  %arrayidx66 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el65, i32 0, i32 %26
  %call67 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx66)
  %27 = load i32, i32* %i, align 4
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 %27
  %28 = load float, float* %arrayidx68, align 4
  %m_el69 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %29 = load i32, i32* %j, align 4
  %arrayidx70 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el69, i32 0, i32 %29
  %call71 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx70)
  %30 = load i32, i32* %j, align 4
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 %30
  %31 = load float, float* %arrayidx72, align 4
  %sub73 = fsub float %28, %31
  %m_el74 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %32 = load i32, i32* %k, align 4
  %arrayidx75 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el74, i32 0, i32 %32
  %call76 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx75)
  %33 = load i32, i32* %k, align 4
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 %33
  %34 = load float, float* %arrayidx77, align 4
  %sub78 = fsub float %sub73, %34
  %add79 = fadd float %sub78, 1.000000e+00
  %call80 = call float @_Z6btSqrtf(float %add79)
  store float %call80, float* %s64, align 4
  %35 = load float, float* %s64, align 4
  %mul81 = fmul float %35, 5.000000e-01
  %36 = load i32, i32* %i, align 4
  %arrayidx82 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %36
  store float %mul81, float* %arrayidx82, align 4
  %37 = load float, float* %s64, align 4
  %div83 = fdiv float 5.000000e-01, %37
  store float %div83, float* %s64, align 4
  %m_el84 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %38 = load i32, i32* %k, align 4
  %arrayidx85 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el84, i32 0, i32 %38
  %call86 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx85)
  %39 = load i32, i32* %j, align 4
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 %39
  %40 = load float, float* %arrayidx87, align 4
  %m_el88 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %41 = load i32, i32* %j, align 4
  %arrayidx89 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el88, i32 0, i32 %41
  %call90 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx89)
  %42 = load i32, i32* %k, align 4
  %arrayidx91 = getelementptr inbounds float, float* %call90, i32 %42
  %43 = load float, float* %arrayidx91, align 4
  %sub92 = fsub float %40, %43
  %44 = load float, float* %s64, align 4
  %mul93 = fmul float %sub92, %44
  %arrayidx94 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul93, float* %arrayidx94, align 4
  %m_el95 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %45 = load i32, i32* %j, align 4
  %arrayidx96 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el95, i32 0, i32 %45
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx96)
  %46 = load i32, i32* %i, align 4
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 %46
  %47 = load float, float* %arrayidx98, align 4
  %m_el99 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %48 = load i32, i32* %i, align 4
  %arrayidx100 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el99, i32 0, i32 %48
  %call101 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx100)
  %49 = load i32, i32* %j, align 4
  %arrayidx102 = getelementptr inbounds float, float* %call101, i32 %49
  %50 = load float, float* %arrayidx102, align 4
  %add103 = fadd float %47, %50
  %51 = load float, float* %s64, align 4
  %mul104 = fmul float %add103, %51
  %52 = load i32, i32* %j, align 4
  %arrayidx105 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %52
  store float %mul104, float* %arrayidx105, align 4
  %m_el106 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %53 = load i32, i32* %k, align 4
  %arrayidx107 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el106, i32 0, i32 %53
  %call108 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx107)
  %54 = load i32, i32* %i, align 4
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 %54
  %55 = load float, float* %arrayidx109, align 4
  %m_el110 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %56 = load i32, i32* %i, align 4
  %arrayidx111 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el110, i32 0, i32 %56
  %call112 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx111)
  %57 = load i32, i32* %k, align 4
  %arrayidx113 = getelementptr inbounds float, float* %call112, i32 %57
  %58 = load float, float* %arrayidx113, align 4
  %add114 = fadd float %55, %58
  %59 = load float, float* %s64, align 4
  %mul115 = fmul float %add114, %59
  %60 = load i32, i32* %k, align 4
  %arrayidx116 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %60
  store float %mul115, float* %arrayidx116, align 4
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.then
  %61 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %62 = bitcast %class.btQuaternion* %61 to %class.btQuadWord*
  %arrayidx117 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  %arrayidx118 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  %arrayidx119 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  %arrayidx120 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %62, float* nonnull align 4 dereferenceable(4) %arrayidx117, float* nonnull align 4 dereferenceable(4) %arrayidx118, float* nonnull align 4 dereferenceable(4) %arrayidx119, float* nonnull align 4 dereferenceable(4) %arrayidx120)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btQuaternion* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %3 = load float, float* %arrayidx, align 4
  %mul = fmul float %3, %1
  store float %mul, float* %arrayidx, align 4
  %4 = load float*, float** %s.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %7, %5
  store float %mul4, float* %arrayidx3, align 4
  %8 = load float*, float** %s.addr, align 4
  %9 = load float, float* %8, align 4
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats5 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %11 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %11, %9
  store float %mul7, float* %arrayidx6, align 4
  %12 = load float*, float** %s.addr, align 4
  %13 = load float, float* %12, align 4
  %14 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats8 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %14, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 3
  %15 = load float, float* %arrayidx9, align 4
  %mul10 = fmul float %15, %13
  store float %mul10, float* %arrayidx9, align 4
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %0)
  store float %call, float* %d, align 4
  %1 = load float, float* %d, align 4
  %div = fdiv float 2.000000e+00, %1
  store float %div, float* %s, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call2, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %4, %5
  store float %mul, float* %xs, align 4
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load float, float* %call3, align 4
  %9 = load float, float* %s, align 4
  %mul4 = fmul float %8, %9
  store float %mul4, float* %ys, align 4
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call5, align 4
  %13 = load float, float* %s, align 4
  %mul6 = fmul float %12, %13
  store float %mul6, float* %zs, align 4
  %14 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %15 = bitcast %class.btQuaternion* %14 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %15)
  %16 = load float, float* %call7, align 4
  %17 = load float, float* %xs, align 4
  %mul8 = fmul float %16, %17
  store float %mul8, float* %wx, align 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load float, float* %ys, align 4
  %mul10 = fmul float %20, %21
  store float %mul10, float* %wy, align 4
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %23)
  %24 = load float, float* %call11, align 4
  %25 = load float, float* %zs, align 4
  %mul12 = fmul float %24, %25
  store float %mul12, float* %wz, align 4
  %26 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %27 = bitcast %class.btQuaternion* %26 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %27)
  %28 = load float, float* %call13, align 4
  %29 = load float, float* %xs, align 4
  %mul14 = fmul float %28, %29
  store float %mul14, float* %xx, align 4
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %31)
  %32 = load float, float* %call15, align 4
  %33 = load float, float* %ys, align 4
  %mul16 = fmul float %32, %33
  store float %mul16, float* %xy, align 4
  %34 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %35 = bitcast %class.btQuaternion* %34 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %35)
  %36 = load float, float* %call17, align 4
  %37 = load float, float* %zs, align 4
  %mul18 = fmul float %36, %37
  store float %mul18, float* %xz, align 4
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %39)
  %40 = load float, float* %call19, align 4
  %41 = load float, float* %ys, align 4
  %mul20 = fmul float %40, %41
  store float %mul20, float* %yy, align 4
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %43)
  %44 = load float, float* %call21, align 4
  %45 = load float, float* %zs, align 4
  %mul22 = fmul float %44, %45
  store float %mul22, float* %yz, align 4
  %46 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %47 = bitcast %class.btQuaternion* %46 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %47)
  %48 = load float, float* %call23, align 4
  %49 = load float, float* %zs, align 4
  %mul24 = fmul float %48, %49
  store float %mul24, float* %zz, align 4
  %50 = load float, float* %yy, align 4
  %51 = load float, float* %zz, align 4
  %add = fadd float %50, %51
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4
  %52 = load float, float* %xy, align 4
  %53 = load float, float* %wz, align 4
  %sub26 = fsub float %52, %53
  store float %sub26, float* %ref.tmp25, align 4
  %54 = load float, float* %xz, align 4
  %55 = load float, float* %wy, align 4
  %add28 = fadd float %54, %55
  store float %add28, float* %ref.tmp27, align 4
  %56 = load float, float* %xy, align 4
  %57 = load float, float* %wz, align 4
  %add30 = fadd float %56, %57
  store float %add30, float* %ref.tmp29, align 4
  %58 = load float, float* %xx, align 4
  %59 = load float, float* %zz, align 4
  %add32 = fadd float %58, %59
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4
  %60 = load float, float* %yz, align 4
  %61 = load float, float* %wx, align 4
  %sub35 = fsub float %60, %61
  store float %sub35, float* %ref.tmp34, align 4
  %62 = load float, float* %xz, align 4
  %63 = load float, float* %wy, align 4
  %sub37 = fsub float %62, %63
  store float %sub37, float* %ref.tmp36, align 4
  %64 = load float, float* %yz, align 4
  %65 = load float, float* %wx, align 4
  %add39 = fadd float %64, %65
  store float %add39, float* %ref.tmp38, align 4
  %66 = load float, float* %xx, align 4
  %67 = load float, float* %yy, align 4
  %add41 = fadd float %66, %67
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #9

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  ret %class.btVector3* %m_linearVelocity
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  ret %class.btVector3* %m_angularVelocity
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_(%class.btAlignedObjectArray* %this, i32 %newsize, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btCollisionObject**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btCollisionObject** %fillData, %class.btCollisionObject*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %14 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %14, i32 %15
  %16 = bitcast %class.btCollisionObject** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btCollisionObject**
  %18 = load %class.btCollisionObject**, %class.btCollisionObject*** %fillData.addr, align 4
  %19 = load %class.btCollisionObject*, %class.btCollisionObject** %18, align 4
  store %class.btCollisionObject* %19, %class.btCollisionObject** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.9* %this, i32 %newsize, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btPersistentManifold**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btPersistentManifold** %fillData, %class.btPersistentManifold*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.9* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %14 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %14, i32 %15
  %16 = bitcast %class.btPersistentManifold** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btPersistentManifold**
  %18 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %fillData.addr, align 4
  %19 = load %class.btPersistentManifold*, %class.btPersistentManifold** %18, align 4
  store %class.btPersistentManifold* %19, %class.btPersistentManifold** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.9* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.9* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.9* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %2, %class.btPersistentManifold*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.9* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %4, %class.btPersistentManifold*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.9* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.10* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.9* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  %5 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %7 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %7, i32 %8
  %9 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4
  store %class.btPersistentManifold* %9, %class.btPersistentManifold** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.9* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.9* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.10* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.10* %this, i32 %n, %class.btPersistentManifold*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.10* %this, %class.btPersistentManifold** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %this, i32 %x) #2 comdat {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %x.addr = alloca i32, align 4
  %elementPtr = alloca %struct.btElement*, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4
  store i32 %x, i32* %x.addr, align 4
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %x.addr, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %1 = load i32, i32* %x.addr, align 4
  %call = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.15* %m_elements, i32 %1)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call, i32 0, i32 0
  %2 = load i32, i32* %m_id, align 4
  %cmp = icmp ne i32 %0, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %m_elements2 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %m_elements3 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %3 = load i32, i32* %x.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.15* %m_elements3, i32 %3)
  %m_id5 = getelementptr inbounds %struct.btElement, %struct.btElement* %call4, i32 0, i32 0
  %4 = load i32, i32* %m_id5, align 4
  %call6 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.15* %m_elements2, i32 %4)
  store %struct.btElement* %call6, %struct.btElement** %elementPtr, align 4
  %5 = load %struct.btElement*, %struct.btElement** %elementPtr, align 4
  %m_id7 = getelementptr inbounds %struct.btElement, %struct.btElement* %5, i32 0, i32 0
  %6 = load i32, i32* %m_id7, align 4
  %m_elements8 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %7 = load i32, i32* %x.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.15* %m_elements8, i32 %7)
  %m_id10 = getelementptr inbounds %struct.btElement, %struct.btElement* %call9, i32 0, i32 0
  store i32 %6, i32* %m_id10, align 4
  %8 = load %struct.btElement*, %struct.btElement** %elementPtr, align 4
  %m_id11 = getelementptr inbounds %struct.btElement, %struct.btElement* %8, i32 0, i32 0
  %9 = load i32, i32* %m_id11, align 4
  store i32 %9, i32* %x.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %10 = load i32, i32* %x.addr, align 4
  ret i32 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.15* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %0 = load %struct.btElement*, %struct.btElement** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btElement, %struct.btElement* %0, i32 %1
  ret %struct.btElement* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %proxyType) #1 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4
  %0 = load i32, i32* %proxyType.addr, align 4
  %cmp = icmp slt i32 %0, 20
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_shapeType, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btCollisionWorld::ClosestConvexResultCallback"* @_ZN16btCollisionWorld27ClosestConvexResultCallbackC2ERK9btVector3S3_(%"struct.btCollisionWorld::ClosestConvexResultCallback"* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %convexFromWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %convexToWorld) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ClosestConvexResultCallback"*, align 4
  %convexFromWorld.addr = alloca %class.btVector3*, align 4
  %convexToWorld.addr = alloca %class.btVector3*, align 4
  store %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4
  store %class.btVector3* %convexFromWorld, %class.btVector3** %convexFromWorld.addr, align 4
  store %class.btVector3* %convexToWorld, %class.btVector3** %convexToWorld.addr, align 4
  %this1 = load %"struct.btCollisionWorld::ClosestConvexResultCallback"*, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4
  %0 = bitcast %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call = call %"struct.btCollisionWorld::ConvexResultCallback"* @_ZN16btCollisionWorld20ConvexResultCallbackC2Ev(%"struct.btCollisionWorld::ConvexResultCallback"* %0)
  %1 = bitcast %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTVN16btCollisionWorld27ClosestConvexResultCallbackE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_convexFromWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 1
  %2 = load %class.btVector3*, %class.btVector3** %convexFromWorld.addr, align 4
  %3 = bitcast %class.btVector3* %m_convexFromWorld to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %m_convexToWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 2
  %5 = load %class.btVector3*, %class.btVector3** %convexToWorld.addr, align 4
  %6 = bitcast %class.btVector3* %m_convexToWorld to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_hitNormalWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 3
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hitNormalWorld)
  %m_hitPointWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 4
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hitPointWorld)
  %m_hitCollisionObject = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 5
  store %class.btCollisionObject* null, %class.btCollisionObject** %m_hitCollisionObject, align 4
  ret %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN34btClosestNotMeConvexResultCallbackD0Ev(%class.btClosestNotMeConvexResultCallback* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btClosestNotMeConvexResultCallback*, align 4
  store %class.btClosestNotMeConvexResultCallback* %this, %class.btClosestNotMeConvexResultCallback** %this.addr, align 4
  %this1 = load %class.btClosestNotMeConvexResultCallback*, %class.btClosestNotMeConvexResultCallback** %this.addr, align 4
  %call = call %class.btClosestNotMeConvexResultCallback* @_ZN34btClosestNotMeConvexResultCallbackD2Ev(%class.btClosestNotMeConvexResultCallback* %this1) #10
  %0 = bitcast %class.btClosestNotMeConvexResultCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #12
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK34btClosestNotMeConvexResultCallback14needsCollisionEP17btBroadphaseProxy(%class.btClosestNotMeConvexResultCallback* %this, %struct.btBroadphaseProxy* %proxy0) unnamed_addr #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btClosestNotMeConvexResultCallback*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %otherObj = alloca %class.btCollisionObject*, align 4
  store %class.btClosestNotMeConvexResultCallback* %this, %class.btClosestNotMeConvexResultCallback** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %this1 = load %class.btClosestNotMeConvexResultCallback*, %class.btClosestNotMeConvexResultCallback** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %0, i32 0, i32 0
  %1 = load i8*, i8** %m_clientObject, align 4
  %m_me = getelementptr inbounds %class.btClosestNotMeConvexResultCallback, %class.btClosestNotMeConvexResultCallback* %this1, i32 0, i32 1
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %m_me, align 4
  %3 = bitcast %class.btCollisionObject* %2 to i8*
  %cmp = icmp eq i8* %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %4 = bitcast %class.btClosestNotMeConvexResultCallback* %this1 to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %5 = bitcast %"struct.btCollisionWorld::ClosestConvexResultCallback"* %4 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %call = call zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy(%"struct.btCollisionWorld::ConvexResultCallback"* %5, %struct.btBroadphaseProxy* %6)
  br i1 %call, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  store i1 false, i1* %retval, align 1
  br label %return

if.end3:                                          ; preds = %if.end
  %7 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %m_clientObject4 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %7, i32 0, i32 0
  %8 = load i8*, i8** %m_clientObject4, align 4
  %9 = bitcast i8* %8 to %class.btCollisionObject*
  store %class.btCollisionObject* %9, %class.btCollisionObject** %otherObj, align 4
  %m_dispatcher = getelementptr inbounds %class.btClosestNotMeConvexResultCallback, %class.btClosestNotMeConvexResultCallback* %this1, i32 0, i32 4
  %10 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %m_me5 = getelementptr inbounds %class.btClosestNotMeConvexResultCallback, %class.btClosestNotMeConvexResultCallback* %this1, i32 0, i32 1
  %11 = load %class.btCollisionObject*, %class.btCollisionObject** %m_me5, align 4
  %12 = load %class.btCollisionObject*, %class.btCollisionObject** %otherObj, align 4
  %13 = bitcast %class.btDispatcher* %10 to i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable = load i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)**, i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*** %13, align 4
  %vfn = getelementptr inbounds i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vtable, i64 7
  %14 = load i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vfn, align 4
  %call6 = call zeroext i1 %14(%class.btDispatcher* %10, %class.btCollisionObject* %11, %class.btCollisionObject* %12)
  br i1 %call6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %if.end3
  store i1 true, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.end3
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end8, %if.then7, %if.then2, %if.then
  %15 = load i1, i1* %retval, align 1
  ret i1 %15
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZN34btClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb(%class.btClosestNotMeConvexResultCallback* %this, %"struct.btCollisionWorld::LocalConvexResult"* nonnull align 4 dereferenceable(44) %convexResult, i1 zeroext %normalInWorldSpace) unnamed_addr #2 comdat {
entry:
  %retval = alloca float, align 4
  %this.addr = alloca %class.btClosestNotMeConvexResultCallback*, align 4
  %convexResult.addr = alloca %"struct.btCollisionWorld::LocalConvexResult"*, align 4
  %normalInWorldSpace.addr = alloca i8, align 1
  %linVelA = alloca %class.btVector3, align 4
  %linVelB = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %relativeVelocity = alloca %class.btVector3, align 4
  store %class.btClosestNotMeConvexResultCallback* %this, %class.btClosestNotMeConvexResultCallback** %this.addr, align 4
  store %"struct.btCollisionWorld::LocalConvexResult"* %convexResult, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %frombool = zext i1 %normalInWorldSpace to i8
  store i8 %frombool, i8* %normalInWorldSpace.addr, align 1
  %this1 = load %class.btClosestNotMeConvexResultCallback*, %class.btClosestNotMeConvexResultCallback** %this.addr, align 4
  %0 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitCollisionObject = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %0, i32 0, i32 0
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject, align 4
  %m_me = getelementptr inbounds %class.btClosestNotMeConvexResultCallback, %class.btClosestNotMeConvexResultCallback* %this1, i32 0, i32 1
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %m_me, align 4
  %cmp = icmp eq %class.btCollisionObject* %1, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float 1.000000e+00, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitCollisionObject2 = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %3, i32 0, i32 0
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject2, align 4
  %call = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %4)
  br i1 %call, label %if.end4, label %if.then3

if.then3:                                         ; preds = %if.end
  store float 1.000000e+00, float* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %linVelA)
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %linVelB)
  %5 = bitcast %class.btClosestNotMeConvexResultCallback* %this1 to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %m_convexToWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %5, i32 0, i32 2
  %6 = bitcast %class.btClosestNotMeConvexResultCallback* %this1 to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %m_convexFromWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %6, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_convexToWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %m_convexFromWorld)
  %7 = bitcast %class.btVector3* %linVelA to i8*
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 0.000000e+00, float* %ref.tmp9, align 4
  store float 0.000000e+00, float* %ref.tmp10, align 4
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  %9 = bitcast %class.btVector3* %linVelB to i8*
  %10 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %relativeVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelB)
  %11 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitNormalLocal = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %11, i32 0, i32 2
  %call12 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_hitNormalLocal, %class.btVector3* nonnull align 4 dereferenceable(16) %relativeVelocity)
  %m_allowedPenetration = getelementptr inbounds %class.btClosestNotMeConvexResultCallback, %class.btClosestNotMeConvexResultCallback* %this1, i32 0, i32 2
  %12 = load float, float* %m_allowedPenetration, align 4
  %fneg = fneg float %12
  %cmp13 = fcmp oge float %call12, %fneg
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.end4
  store float 1.000000e+00, float* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.end4
  %13 = bitcast %class.btClosestNotMeConvexResultCallback* %this1 to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %14 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %15 = load i8, i8* %normalInWorldSpace.addr, align 1
  %tobool = trunc i8 %15 to i1
  %call16 = call float @_ZN16btCollisionWorld27ClosestConvexResultCallback15addSingleResultERNS_17LocalConvexResultEb(%"struct.btCollisionWorld::ClosestConvexResultCallback"* %13, %"struct.btCollisionWorld::LocalConvexResult"* nonnull align 4 dereferenceable(44) %14, i1 zeroext %tobool)
  store float %call16, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end15, %if.then14, %if.then3, %if.then
  %16 = load float, float* %retval, align 4
  ret float %16
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btCollisionWorld::ConvexResultCallback"* @_ZN16btCollisionWorld20ConvexResultCallbackC2Ev(%"struct.btCollisionWorld::ConvexResultCallback"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ConvexResultCallback"*, align 4
  store %"struct.btCollisionWorld::ConvexResultCallback"* %this, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  %this1 = load %"struct.btCollisionWorld::ConvexResultCallback"*, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  %0 = bitcast %"struct.btCollisionWorld::ConvexResultCallback"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTVN16btCollisionWorld20ConvexResultCallbackE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %this1, i32 0, i32 1
  store float 1.000000e+00, float* %m_closestHitFraction, align 4
  %m_collisionFilterGroup = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %this1, i32 0, i32 2
  store i32 1, i32* %m_collisionFilterGroup, align 4
  %m_collisionFilterMask = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %this1, i32 0, i32 3
  store i32 -1, i32* %m_collisionFilterMask, align 4
  ret %"struct.btCollisionWorld::ConvexResultCallback"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btCollisionWorld::ClosestConvexResultCallback"* @_ZN16btCollisionWorld27ClosestConvexResultCallbackD2Ev(%"struct.btCollisionWorld::ClosestConvexResultCallback"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ClosestConvexResultCallback"*, align 4
  store %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4
  %this1 = load %"struct.btCollisionWorld::ClosestConvexResultCallback"*, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4
  %0 = bitcast %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call = call %"struct.btCollisionWorld::ConvexResultCallback"* @_ZN16btCollisionWorld20ConvexResultCallbackD2Ev(%"struct.btCollisionWorld::ConvexResultCallback"* %0) #10
  ret %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btCollisionWorld27ClosestConvexResultCallbackD0Ev(%"struct.btCollisionWorld::ClosestConvexResultCallback"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ClosestConvexResultCallback"*, align 4
  store %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4
  %this1 = load %"struct.btCollisionWorld::ClosestConvexResultCallback"*, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4
  %call = call %"struct.btCollisionWorld::ClosestConvexResultCallback"* @_ZN16btCollisionWorld27ClosestConvexResultCallbackD2Ev(%"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1) #10
  %0 = bitcast %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #12
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy(%"struct.btCollisionWorld::ConvexResultCallback"* %this, %struct.btBroadphaseProxy* %proxy0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ConvexResultCallback"*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %collides = alloca i8, align 1
  store %"struct.btCollisionWorld::ConvexResultCallback"* %this, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %this1 = load %"struct.btCollisionWorld::ConvexResultCallback"*, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %0, i32 0, i32 1
  %1 = load i32, i32* %m_collisionFilterGroup, align 4
  %m_collisionFilterMask = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %this1, i32 0, i32 3
  %2 = load i32, i32* %m_collisionFilterMask, align 4
  %and = and i32 %1, %2
  %cmp = icmp ne i32 %and, 0
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %collides, align 1
  %3 = load i8, i8* %collides, align 1
  %tobool = trunc i8 %3 to i1
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %m_collisionFilterGroup2 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %this1, i32 0, i32 2
  %4 = load i32, i32* %m_collisionFilterGroup2, align 4
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %m_collisionFilterMask3 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %5, i32 0, i32 2
  %6 = load i32, i32* %m_collisionFilterMask3, align 4
  %and4 = and i32 %4, %6
  %tobool5 = icmp ne i32 %and4, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %7 = phi i1 [ false, %entry ], [ %tobool5, %land.rhs ]
  %frombool6 = zext i1 %7 to i8
  store i8 %frombool6, i8* %collides, align 1
  %8 = load i8, i8* %collides, align 1
  %tobool7 = trunc i8 %8 to i1
  ret i1 %tobool7
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZN16btCollisionWorld27ClosestConvexResultCallback15addSingleResultERNS_17LocalConvexResultEb(%"struct.btCollisionWorld::ClosestConvexResultCallback"* %this, %"struct.btCollisionWorld::LocalConvexResult"* nonnull align 4 dereferenceable(44) %convexResult, i1 zeroext %normalInWorldSpace) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ClosestConvexResultCallback"*, align 4
  %convexResult.addr = alloca %"struct.btCollisionWorld::LocalConvexResult"*, align 4
  %normalInWorldSpace.addr = alloca i8, align 1
  %ref.tmp = alloca %class.btVector3, align 4
  store %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4
  store %"struct.btCollisionWorld::LocalConvexResult"* %convexResult, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %frombool = zext i1 %normalInWorldSpace to i8
  store i8 %frombool, i8* %normalInWorldSpace.addr, align 1
  %this1 = load %"struct.btCollisionWorld::ClosestConvexResultCallback"*, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4
  %0 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitFraction = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %0, i32 0, i32 4
  %1 = load float, float* %m_hitFraction, align 4
  %2 = bitcast %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %2, i32 0, i32 1
  store float %1, float* %m_closestHitFraction, align 4
  %3 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitCollisionObject = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %3, i32 0, i32 0
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject, align 4
  %m_hitCollisionObject2 = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 5
  store %class.btCollisionObject* %4, %class.btCollisionObject** %m_hitCollisionObject2, align 4
  %5 = load i8, i8* %normalInWorldSpace.addr, align 1
  %tobool = trunc i8 %5 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitNormalLocal = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %6, i32 0, i32 2
  %m_hitNormalWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 3
  %7 = bitcast %class.btVector3* %m_hitNormalWorld to i8*
  %8 = bitcast %class.btVector3* %m_hitNormalLocal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  br label %if.end

if.else:                                          ; preds = %entry
  %m_hitCollisionObject3 = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 5
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject3, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %9)
  %call4 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call)
  %10 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitNormalLocal5 = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %10, i32 0, i32 2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_hitNormalLocal5)
  %m_hitNormalWorld6 = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 3
  %11 = bitcast %class.btVector3* %m_hitNormalWorld6 to i8*
  %12 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %13 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitPointLocal = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %13, i32 0, i32 3
  %m_hitPointWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 4
  %14 = bitcast %class.btVector3* %m_hitPointWorld to i8*
  %15 = bitcast %class.btVector3* %m_hitPointLocal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false)
  %16 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitFraction7 = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %16, i32 0, i32 4
  %17 = load float, float* %m_hitFraction7, align 4
  ret float %17
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btCollisionWorld::ConvexResultCallback"* @_ZN16btCollisionWorld20ConvexResultCallbackD2Ev(%"struct.btCollisionWorld::ConvexResultCallback"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ConvexResultCallback"*, align 4
  store %"struct.btCollisionWorld::ConvexResultCallback"* %this, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  %this1 = load %"struct.btCollisionWorld::ConvexResultCallback"*, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  ret %"struct.btCollisionWorld::ConvexResultCallback"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btCollisionWorld20ConvexResultCallbackD0Ev(%"struct.btCollisionWorld::ConvexResultCallback"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ConvexResultCallback"*, align 4
  store %"struct.btCollisionWorld::ConvexResultCallback"* %this, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  %this1 = load %"struct.btCollisionWorld::ConvexResultCallback"*, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4
  %and = and i32 %0, 4
  %cmp = icmp eq i32 %and, 0
  ret i1 %cmp
}

declare %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* returned) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector34setXEf(%class.btVector3* %this, float %_x) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float %_x, float* %_x.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_x.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %0, float* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btConvexInternalShape* @_ZN21btConvexInternalShapeD2Ev(%class.btConvexInternalShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = bitcast %class.btConvexInternalShape* %this1 to %class.btConvexShape*
  %call = call %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* %0) #10
  ret %class.btConvexInternalShape* %this1
}

; Function Attrs: nounwind
declare %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* returned) unnamed_addr #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btRigidBody19applyCentralImpulseERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %impulse.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %impulse, %class.btVector3** %impulse.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %impulse.addr, align 4
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %m_inverseMass)
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btRigidBody18applyTorqueImpulseERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %torque) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %torque.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %torque, %class.btVector3** %torque.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  %0 = load %class.btVector3*, %class.btVector3** %torque.addr, align 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_invInertiaTensorWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularFactor)
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 1.000000e+00, float* %ref.tmp9, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %10, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

declare float @_ZNK14btAngularLimit6getLowEv(%class.btAngularLimit*) #3

declare float @_ZNK14btAngularLimit7getHighEv(%class.btAngularLimit*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK14btAngularLimit12getHalfRangeEv(%class.btAngularLimit* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAngularLimit*, align 4
  store %class.btAngularLimit* %this, %class.btAngularLimit** %this.addr, align 4
  %this1 = load %class.btAngularLimit*, %class.btAngularLimit** %this.addr, align 4
  %m_halfRange = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 1
  %0 = load float, float* %m_halfRange, align 4
  ret float %0
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #9

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sin.f32(float) #9

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %tobool = icmp ne %class.btTypedConstraint** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.5* %m_allocator, %class.btTypedConstraint** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.5* %this, %class.btTypedConstraint** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca %class.btTypedConstraint**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store %class.btTypedConstraint** %ptr, %class.btTypedConstraint*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %ptr.addr, align 4
  %1 = bitcast %class.btTypedConstraint** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.10* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.10* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  ret %class.btAlignedAllocator.10* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EEC2Ev(%class.btAlignedAllocator.5* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  ret %class.btAlignedAllocator.5* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btTypedConstraint**, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP17btTypedConstraintE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btTypedConstraint**
  store %class.btTypedConstraint** %2, %class.btTypedConstraint*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, %class.btTypedConstraint** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btTypedConstraint** %4, %class.btTypedConstraint*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP17btTypedConstraintE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btTypedConstraint** @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.5* %m_allocator, i32 %1, %class.btTypedConstraint*** null)
  %2 = bitcast %class.btTypedConstraint** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, %class.btTypedConstraint** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btTypedConstraint**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btTypedConstraint** %dest, %class.btTypedConstraint*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %3, i32 %4
  %5 = bitcast %class.btTypedConstraint** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btTypedConstraint**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %7 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %7, i32 %8
  %9 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx2, align 4
  store %class.btTypedConstraint* %9, %class.btTypedConstraint** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTypedConstraint** @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.5* %this, i32 %n, %class.btTypedConstraint*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btTypedConstraint***, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btTypedConstraint*** %hint, %class.btTypedConstraint**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btTypedConstraint**
  ret %class.btTypedConstraint** %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.9* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9allocSizeEi(%class.btAlignedObjectArray.4* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.20* @_ZN18btAlignedAllocatorIP11btRigidBodyLj16EEC2Ev(%class.btAlignedAllocator.20* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.20*, align 4
  store %class.btAlignedAllocator.20* %this, %class.btAlignedAllocator.20** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.20*, %class.btAlignedAllocator.20** %this.addr, align 4
  ret %class.btAlignedAllocator.20* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btRigidBodyE4initEv(%class.btAlignedObjectArray.19* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.19*, align 4
  store %class.btAlignedObjectArray.19* %this, %class.btAlignedObjectArray.19** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.19*, %class.btAlignedObjectArray.19** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 4
  store %class.btRigidBody** null, %class.btRigidBody*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.24* @_ZN18btAlignedAllocatorIP17btActionInterfaceLj16EEC2Ev(%class.btAlignedAllocator.24* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.24*, align 4
  store %class.btAlignedAllocator.24* %this, %class.btAlignedAllocator.24** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.24*, %class.btAlignedAllocator.24** %this.addr, align 4
  ret %class.btAlignedAllocator.24* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE4initEv(%class.btAlignedObjectArray.23* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.23*, align 4
  store %class.btAlignedObjectArray.23* %this, %class.btAlignedObjectArray.23** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.23*, %class.btAlignedObjectArray.23** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 4
  store %class.btActionInterface** null, %class.btActionInterface*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btRigidBodyE5clearEv(%class.btAlignedObjectArray.19* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.19*, align 4
  store %class.btAlignedObjectArray.19* %this, %class.btAlignedObjectArray.19** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.19*, %class.btAlignedObjectArray.19** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %this1)
  call void @_ZN20btAlignedObjectArrayIP11btRigidBodyE7destroyEii(%class.btAlignedObjectArray.19* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP11btRigidBodyE10deallocateEv(%class.btAlignedObjectArray.19* %this1)
  call void @_ZN20btAlignedObjectArrayIP11btRigidBodyE4initEv(%class.btAlignedObjectArray.19* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btRigidBodyE7destroyEii(%class.btAlignedObjectArray.19* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.19*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.19* %this, %class.btAlignedObjectArray.19** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.19*, %class.btAlignedObjectArray.19** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 4
  %3 = load %class.btRigidBody**, %class.btRigidBody*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btRigidBody*, %class.btRigidBody** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btRigidBodyE10deallocateEv(%class.btAlignedObjectArray.19* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.19*, align 4
  store %class.btAlignedObjectArray.19* %this, %class.btAlignedObjectArray.19** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.19*, %class.btAlignedObjectArray.19** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 4
  %0 = load %class.btRigidBody**, %class.btRigidBody*** %m_data, align 4
  %tobool = icmp ne %class.btRigidBody** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 4
  %2 = load %class.btRigidBody**, %class.btRigidBody*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP11btRigidBodyLj16EE10deallocateEPS1_(%class.btAlignedAllocator.20* %m_allocator, %class.btRigidBody** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 4
  store %class.btRigidBody** null, %class.btRigidBody*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP11btRigidBodyLj16EE10deallocateEPS1_(%class.btAlignedAllocator.20* %this, %class.btRigidBody** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.20*, align 4
  %ptr.addr = alloca %class.btRigidBody**, align 4
  store %class.btAlignedAllocator.20* %this, %class.btAlignedAllocator.20** %this.addr, align 4
  store %class.btRigidBody** %ptr, %class.btRigidBody*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.20*, %class.btAlignedAllocator.20** %this.addr, align 4
  %0 = load %class.btRigidBody**, %class.btRigidBody*** %ptr.addr, align 4
  %1 = bitcast %class.btRigidBody** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE5clearEv(%class.btAlignedObjectArray.23* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.23*, align 4
  store %class.btAlignedObjectArray.23* %this, %class.btAlignedObjectArray.23** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.23*, %class.btAlignedObjectArray.23** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btActionInterfaceE4sizeEv(%class.btAlignedObjectArray.23* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE7destroyEii(%class.btAlignedObjectArray.23* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE10deallocateEv(%class.btAlignedObjectArray.23* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE4initEv(%class.btAlignedObjectArray.23* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE7destroyEii(%class.btAlignedObjectArray.23* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.23*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.23* %this, %class.btAlignedObjectArray.23** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.23*, %class.btAlignedObjectArray.23** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 4
  %3 = load %class.btActionInterface**, %class.btActionInterface*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btActionInterface*, %class.btActionInterface** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE10deallocateEv(%class.btAlignedObjectArray.23* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.23*, align 4
  store %class.btAlignedObjectArray.23* %this, %class.btAlignedObjectArray.23** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.23*, %class.btAlignedObjectArray.23** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 4
  %0 = load %class.btActionInterface**, %class.btActionInterface*** %m_data, align 4
  %tobool = icmp ne %class.btActionInterface** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 4
  %2 = load %class.btActionInterface**, %class.btActionInterface*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP17btActionInterfaceLj16EE10deallocateEPS1_(%class.btAlignedAllocator.24* %m_allocator, %class.btActionInterface** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 4
  store %class.btActionInterface** null, %class.btActionInterface*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP17btActionInterfaceLj16EE10deallocateEPS1_(%class.btAlignedAllocator.24* %this, %class.btActionInterface** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.24*, align 4
  %ptr.addr = alloca %class.btActionInterface**, align 4
  store %class.btAlignedAllocator.24* %this, %class.btAlignedAllocator.24** %this.addr, align 4
  store %class.btActionInterface** %ptr, %class.btActionInterface*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.24*, %class.btAlignedAllocator.24** %this.addr, align 4
  %0 = load %class.btActionInterface**, %class.btActionInterface*** %ptr.addr, align 4
  %1 = bitcast %class.btActionInterface** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE16findLinearSearchERKS1_(%class.btAlignedObjectArray.19* %this, %class.btRigidBody** nonnull align 4 dereferenceable(4) %key) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.19*, align 4
  %key.addr = alloca %class.btRigidBody**, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.19* %this, %class.btAlignedObjectArray.19** %this.addr, align 4
  store %class.btRigidBody** %key, %class.btRigidBody*** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray.19*, %class.btAlignedObjectArray.19** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %this1)
  store i32 %call, i32* %index, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %this1)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 4
  %1 = load %class.btRigidBody**, %class.btRigidBody*** %m_data, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btRigidBody*, %class.btRigidBody** %1, i32 %2
  %3 = load %class.btRigidBody*, %class.btRigidBody** %arrayidx, align 4
  %4 = load %class.btRigidBody**, %class.btRigidBody*** %key.addr, align 4
  %5 = load %class.btRigidBody*, %class.btRigidBody** %4, align 4
  %cmp3 = icmp eq %class.btRigidBody* %3, %5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  store i32 %6, i32* %index, align 4
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %8 = load i32, i32* %index, align 4
  ret i32 %8
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btRigidBodyE13removeAtIndexEi(%class.btAlignedObjectArray.19* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.19*, align 4
  %index.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.19* %this, %class.btAlignedObjectArray.19** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btAlignedObjectArray.19*, %class.btAlignedObjectArray.19** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %this1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %index.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayIP11btRigidBodyE4swapEii(%class.btAlignedObjectArray.19* %this1, i32 %1, i32 %sub)
  call void @_ZN20btAlignedObjectArrayIP11btRigidBodyE8pop_backEv(%class.btAlignedObjectArray.19* %this1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btRigidBodyE4swapEii(%class.btAlignedObjectArray.19* %this, i32 %index0, i32 %index1) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.19*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %class.btRigidBody*, align 4
  store %class.btAlignedObjectArray.19* %this, %class.btAlignedObjectArray.19** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray.19*, %class.btAlignedObjectArray.19** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 4
  %0 = load %class.btRigidBody**, %class.btRigidBody*** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %class.btRigidBody*, %class.btRigidBody** %0, i32 %1
  %2 = load %class.btRigidBody*, %class.btRigidBody** %arrayidx, align 4
  store %class.btRigidBody* %2, %class.btRigidBody** %temp, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 4
  %3 = load %class.btRigidBody**, %class.btRigidBody*** %m_data2, align 4
  %4 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %class.btRigidBody*, %class.btRigidBody** %3, i32 %4
  %5 = load %class.btRigidBody*, %class.btRigidBody** %arrayidx3, align 4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 4
  %6 = load %class.btRigidBody**, %class.btRigidBody*** %m_data4, align 4
  %7 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %class.btRigidBody*, %class.btRigidBody** %6, i32 %7
  store %class.btRigidBody* %5, %class.btRigidBody** %arrayidx5, align 4
  %8 = load %class.btRigidBody*, %class.btRigidBody** %temp, align 4
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 4
  %9 = load %class.btRigidBody**, %class.btRigidBody*** %m_data6, align 4
  %10 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %class.btRigidBody*, %class.btRigidBody** %9, i32 %10
  store %class.btRigidBody* %8, %class.btRigidBody** %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btRigidBodyE8pop_backEv(%class.btAlignedObjectArray.19* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.19*, align 4
  store %class.btAlignedObjectArray.19* %this, %class.btAlignedObjectArray.19** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.19*, %class.btAlignedObjectArray.19** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 4
  %1 = load %class.btRigidBody**, %class.btRigidBody*** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds %class.btRigidBody*, %class.btRigidBody** %1, i32 %2
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE8capacityEv(%class.btAlignedObjectArray.19* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.19*, align 4
  store %class.btAlignedObjectArray.19* %this, %class.btAlignedObjectArray.19** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.19*, %class.btAlignedObjectArray.19** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btRigidBodyE7reserveEi(%class.btAlignedObjectArray.19* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.19*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btRigidBody**, align 4
  store %class.btAlignedObjectArray.19* %this, %class.btAlignedObjectArray.19** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.19*, %class.btAlignedObjectArray.19** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE8capacityEv(%class.btAlignedObjectArray.19* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP11btRigidBodyE8allocateEi(%class.btAlignedObjectArray.19* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btRigidBody**
  store %class.btRigidBody** %2, %class.btRigidBody*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %this1)
  %3 = load %class.btRigidBody**, %class.btRigidBody*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4copyEiiPS1_(%class.btAlignedObjectArray.19* %this1, i32 0, i32 %call3, %class.btRigidBody** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4sizeEv(%class.btAlignedObjectArray.19* %this1)
  call void @_ZN20btAlignedObjectArrayIP11btRigidBodyE7destroyEii(%class.btAlignedObjectArray.19* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP11btRigidBodyE10deallocateEv(%class.btAlignedObjectArray.19* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btRigidBody**, %class.btRigidBody*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 4
  store %class.btRigidBody** %4, %class.btRigidBody*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP11btRigidBodyE9allocSizeEi(%class.btAlignedObjectArray.19* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.19*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.19* %this, %class.btAlignedObjectArray.19** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.19*, %class.btAlignedObjectArray.19** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP11btRigidBodyE8allocateEi(%class.btAlignedObjectArray.19* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.19*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.19* %this, %class.btAlignedObjectArray.19** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.19*, %class.btAlignedObjectArray.19** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btRigidBody** @_ZN18btAlignedAllocatorIP11btRigidBodyLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.20* %m_allocator, i32 %1, %class.btRigidBody*** null)
  %2 = bitcast %class.btRigidBody** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP11btRigidBodyE4copyEiiPS1_(%class.btAlignedObjectArray.19* %this, i32 %start, i32 %end, %class.btRigidBody** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.19*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btRigidBody**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.19* %this, %class.btAlignedObjectArray.19** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btRigidBody** %dest, %class.btRigidBody*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.19*, %class.btAlignedObjectArray.19** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btRigidBody**, %class.btRigidBody*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btRigidBody*, %class.btRigidBody** %3, i32 %4
  %5 = bitcast %class.btRigidBody** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btRigidBody**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.19, %class.btAlignedObjectArray.19* %this1, i32 0, i32 4
  %7 = load %class.btRigidBody**, %class.btRigidBody*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btRigidBody*, %class.btRigidBody** %7, i32 %8
  %9 = load %class.btRigidBody*, %class.btRigidBody** %arrayidx2, align 4
  store %class.btRigidBody* %9, %class.btRigidBody** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btRigidBody** @_ZN18btAlignedAllocatorIP11btRigidBodyLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.20* %this, i32 %n, %class.btRigidBody*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.20*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btRigidBody***, align 4
  store %class.btAlignedAllocator.20* %this, %class.btAlignedAllocator.20** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btRigidBody*** %hint, %class.btRigidBody**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.20*, %class.btAlignedAllocator.20** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btRigidBody**
  ret %class.btRigidBody** %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE16findLinearSearchERKS1_(%class.btAlignedObjectArray.4* %this, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %key) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %key.addr = alloca %class.btTypedConstraint**, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store %class.btTypedConstraint** %key, %class.btTypedConstraint*** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %index, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %1 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %1, i32 %2
  %3 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx, align 4
  %4 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %key.addr, align 4
  %5 = load %class.btTypedConstraint*, %class.btTypedConstraint** %4, align 4
  %cmp3 = icmp eq %class.btTypedConstraint* %3, %5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  store i32 %6, i32* %index, align 4
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %8 = load i32, i32* %index, align 4
  ret i32 %8
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE13removeAtIndexEi(%class.btAlignedObjectArray.4* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %index.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %index.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4swapEii(%class.btAlignedObjectArray.4* %this1, i32 %1, i32 %sub)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE8pop_backEv(%class.btAlignedObjectArray.4* %this1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4swapEii(%class.btAlignedObjectArray.4* %this, i32 %index0, i32 %index1) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %class.btTypedConstraint*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %0, i32 %1
  %2 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx, align 4
  store %class.btTypedConstraint* %2, %class.btTypedConstraint** %temp, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data2, align 4
  %4 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %3, i32 %4
  %5 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx3, align 4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %6 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data4, align 4
  %7 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %6, i32 %7
  store %class.btTypedConstraint* %5, %class.btTypedConstraint** %arrayidx5, align 4
  %8 = load %class.btTypedConstraint*, %class.btTypedConstraint** %temp, align 4
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %9 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data6, align 4
  %10 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %9, i32 %10
  store %class.btTypedConstraint* %8, %class.btTypedConstraint** %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE8pop_backEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %1 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %1, i32 %2
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btActionInterfaceE8capacityEv(%class.btAlignedObjectArray.23* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.23*, align 4
  store %class.btAlignedObjectArray.23* %this, %class.btAlignedObjectArray.23** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.23*, %class.btAlignedObjectArray.23** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE7reserveEi(%class.btAlignedObjectArray.23* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.23*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btActionInterface**, align 4
  store %class.btAlignedObjectArray.23* %this, %class.btAlignedObjectArray.23** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.23*, %class.btAlignedObjectArray.23** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btActionInterfaceE8capacityEv(%class.btAlignedObjectArray.23* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP17btActionInterfaceE8allocateEi(%class.btAlignedObjectArray.23* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btActionInterface**
  store %class.btActionInterface** %2, %class.btActionInterface*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btActionInterfaceE4sizeEv(%class.btAlignedObjectArray.23* %this1)
  %3 = load %class.btActionInterface**, %class.btActionInterface*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP17btActionInterfaceE4copyEiiPS1_(%class.btAlignedObjectArray.23* %this1, i32 0, i32 %call3, %class.btActionInterface** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP17btActionInterfaceE4sizeEv(%class.btAlignedObjectArray.23* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE7destroyEii(%class.btAlignedObjectArray.23* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE10deallocateEv(%class.btAlignedObjectArray.23* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btActionInterface**, %class.btActionInterface*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 4
  store %class.btActionInterface** %4, %class.btActionInterface*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP17btActionInterfaceE9allocSizeEi(%class.btAlignedObjectArray.23* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.23*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.23* %this, %class.btAlignedObjectArray.23** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.23*, %class.btAlignedObjectArray.23** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP17btActionInterfaceE8allocateEi(%class.btAlignedObjectArray.23* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.23*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.23* %this, %class.btAlignedObjectArray.23** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.23*, %class.btAlignedObjectArray.23** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btActionInterface** @_ZN18btAlignedAllocatorIP17btActionInterfaceLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.24* %m_allocator, i32 %1, %class.btActionInterface*** null)
  %2 = bitcast %class.btActionInterface** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP17btActionInterfaceE4copyEiiPS1_(%class.btAlignedObjectArray.23* %this, i32 %start, i32 %end, %class.btActionInterface** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.23*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btActionInterface**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.23* %this, %class.btAlignedObjectArray.23** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btActionInterface** %dest, %class.btActionInterface*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.23*, %class.btAlignedObjectArray.23** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btActionInterface**, %class.btActionInterface*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btActionInterface*, %class.btActionInterface** %3, i32 %4
  %5 = bitcast %class.btActionInterface** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btActionInterface**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 4
  %7 = load %class.btActionInterface**, %class.btActionInterface*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btActionInterface*, %class.btActionInterface** %7, i32 %8
  %9 = load %class.btActionInterface*, %class.btActionInterface** %arrayidx2, align 4
  store %class.btActionInterface* %9, %class.btActionInterface** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btActionInterface** @_ZN18btAlignedAllocatorIP17btActionInterfaceLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.24* %this, i32 %n, %class.btActionInterface*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.24*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btActionInterface***, align 4
  store %class.btAlignedAllocator.24* %this, %class.btAlignedAllocator.24** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btActionInterface*** %hint, %class.btActionInterface**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.24*, %class.btAlignedAllocator.24** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btActionInterface**
  ret %class.btActionInterface** %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btActionInterfaceE16findLinearSearchERKS1_(%class.btAlignedObjectArray.23* %this, %class.btActionInterface** nonnull align 4 dereferenceable(4) %key) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.23*, align 4
  %key.addr = alloca %class.btActionInterface**, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.23* %this, %class.btAlignedObjectArray.23** %this.addr, align 4
  store %class.btActionInterface** %key, %class.btActionInterface*** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray.23*, %class.btAlignedObjectArray.23** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btActionInterfaceE4sizeEv(%class.btAlignedObjectArray.23* %this1)
  store i32 %call, i32* %index, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btActionInterfaceE4sizeEv(%class.btAlignedObjectArray.23* %this1)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 4
  %1 = load %class.btActionInterface**, %class.btActionInterface*** %m_data, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btActionInterface*, %class.btActionInterface** %1, i32 %2
  %3 = load %class.btActionInterface*, %class.btActionInterface** %arrayidx, align 4
  %4 = load %class.btActionInterface**, %class.btActionInterface*** %key.addr, align 4
  %5 = load %class.btActionInterface*, %class.btActionInterface** %4, align 4
  %cmp3 = icmp eq %class.btActionInterface* %3, %5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  store i32 %6, i32* %index, align 4
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %8 = load i32, i32* %index, align 4
  ret i32 %8
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE13removeAtIndexEi(%class.btAlignedObjectArray.23* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.23*, align 4
  %index.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.23* %this, %class.btAlignedObjectArray.23** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btAlignedObjectArray.23*, %class.btAlignedObjectArray.23** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btActionInterfaceE4sizeEv(%class.btAlignedObjectArray.23* %this1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %index.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btActionInterfaceE4sizeEv(%class.btAlignedObjectArray.23* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE4swapEii(%class.btAlignedObjectArray.23* %this1, i32 %1, i32 %sub)
  call void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE8pop_backEv(%class.btAlignedObjectArray.23* %this1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE4swapEii(%class.btAlignedObjectArray.23* %this, i32 %index0, i32 %index1) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.23*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %class.btActionInterface*, align 4
  store %class.btAlignedObjectArray.23* %this, %class.btAlignedObjectArray.23** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray.23*, %class.btAlignedObjectArray.23** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 4
  %0 = load %class.btActionInterface**, %class.btActionInterface*** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %class.btActionInterface*, %class.btActionInterface** %0, i32 %1
  %2 = load %class.btActionInterface*, %class.btActionInterface** %arrayidx, align 4
  store %class.btActionInterface* %2, %class.btActionInterface** %temp, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 4
  %3 = load %class.btActionInterface**, %class.btActionInterface*** %m_data2, align 4
  %4 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %class.btActionInterface*, %class.btActionInterface** %3, i32 %4
  %5 = load %class.btActionInterface*, %class.btActionInterface** %arrayidx3, align 4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 4
  %6 = load %class.btActionInterface**, %class.btActionInterface*** %m_data4, align 4
  %7 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %class.btActionInterface*, %class.btActionInterface** %6, i32 %7
  store %class.btActionInterface* %5, %class.btActionInterface** %arrayidx5, align 4
  %8 = load %class.btActionInterface*, %class.btActionInterface** %temp, align 4
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 4
  %9 = load %class.btActionInterface**, %class.btActionInterface*** %m_data6, align 4
  %10 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %class.btActionInterface*, %class.btActionInterface** %9, i32 %10
  store %class.btActionInterface* %8, %class.btActionInterface** %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btActionInterfaceE8pop_backEv(%class.btAlignedObjectArray.23* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.23*, align 4
  store %class.btAlignedObjectArray.23* %this, %class.btAlignedObjectArray.23** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.23*, %class.btAlignedObjectArray.23** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 4
  %1 = load %class.btActionInterface**, %class.btActionInterface*** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.23* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds %class.btActionInterface*, %class.btActionInterface** %1, i32 %2
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI33btSortConstraintOnIslandPredicateEEvRKT_ii(%class.btAlignedObjectArray.4* %this, %class.btSortConstraintOnIslandPredicate* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %CompareFunc.addr = alloca %class.btSortConstraintOnIslandPredicate*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %class.btTypedConstraint*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store %class.btSortConstraintOnIslandPredicate* %CompareFunc, %class.btSortConstraintOnIslandPredicate** %CompareFunc.addr, align 4
  store i32 %lo, i32* %lo.addr, align 4
  store i32 %hi, i32* %hi.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %lo.addr, align 4
  store i32 %0, i32* %i, align 4
  %1 = load i32, i32* %hi.addr, align 4
  store i32 %1, i32* %j, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %3 = load i32, i32* %lo.addr, align 4
  %4 = load i32, i32* %hi.addr, align 4
  %add = add nsw i32 %3, %4
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %2, i32 %div
  %5 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx, align 4
  store %class.btTypedConstraint* %5, %class.btTypedConstraint** %x, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %6 = load %class.btSortConstraintOnIslandPredicate*, %class.btSortConstraintOnIslandPredicate** %CompareFunc.addr, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %7 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data2, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %7, i32 %8
  %9 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx3, align 4
  %10 = load %class.btTypedConstraint*, %class.btTypedConstraint** %x, align 4
  %call = call zeroext i1 @_ZNK33btSortConstraintOnIslandPredicateclEPK17btTypedConstraintS2_(%class.btSortConstraintOnIslandPredicate* %6, %class.btTypedConstraint* %9, %class.btTypedConstraint* %10)
  br i1 %call, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond4

while.cond4:                                      ; preds = %while.body8, %while.end
  %12 = load %class.btSortConstraintOnIslandPredicate*, %class.btSortConstraintOnIslandPredicate** %CompareFunc.addr, align 4
  %13 = load %class.btTypedConstraint*, %class.btTypedConstraint** %x, align 4
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %14 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data5, align 4
  %15 = load i32, i32* %j, align 4
  %arrayidx6 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %14, i32 %15
  %16 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx6, align 4
  %call7 = call zeroext i1 @_ZNK33btSortConstraintOnIslandPredicateclEPK17btTypedConstraintS2_(%class.btSortConstraintOnIslandPredicate* %12, %class.btTypedConstraint* %13, %class.btTypedConstraint* %16)
  br i1 %call7, label %while.body8, label %while.end9

while.body8:                                      ; preds = %while.cond4
  %17 = load i32, i32* %j, align 4
  %dec = add nsw i32 %17, -1
  store i32 %dec, i32* %j, align 4
  br label %while.cond4

while.end9:                                       ; preds = %while.cond4
  %18 = load i32, i32* %i, align 4
  %19 = load i32, i32* %j, align 4
  %cmp = icmp sle i32 %18, %19
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end9
  %20 = load i32, i32* %i, align 4
  %21 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4swapEii(%class.btAlignedObjectArray.4* %this1, i32 %20, i32 %21)
  %22 = load i32, i32* %i, align 4
  %inc10 = add nsw i32 %22, 1
  store i32 %inc10, i32* %i, align 4
  %23 = load i32, i32* %j, align 4
  %dec11 = add nsw i32 %23, -1
  store i32 %dec11, i32* %j, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end9
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %24 = load i32, i32* %i, align 4
  %25 = load i32, i32* %j, align 4
  %cmp12 = icmp sle i32 %24, %25
  br i1 %cmp12, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %26 = load i32, i32* %lo.addr, align 4
  %27 = load i32, i32* %j, align 4
  %cmp13 = icmp slt i32 %26, %27
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %do.end
  %28 = load %class.btSortConstraintOnIslandPredicate*, %class.btSortConstraintOnIslandPredicate** %CompareFunc.addr, align 4
  %29 = load i32, i32* %lo.addr, align 4
  %30 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI33btSortConstraintOnIslandPredicateEEvRKT_ii(%class.btAlignedObjectArray.4* %this1, %class.btSortConstraintOnIslandPredicate* nonnull align 1 dereferenceable(1) %28, i32 %29, i32 %30)
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %do.end
  %31 = load i32, i32* %i, align 4
  %32 = load i32, i32* %hi.addr, align 4
  %cmp16 = icmp slt i32 %31, %32
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end15
  %33 = load %class.btSortConstraintOnIslandPredicate*, %class.btSortConstraintOnIslandPredicate** %CompareFunc.addr, align 4
  %34 = load i32, i32* %i, align 4
  %35 = load i32, i32* %hi.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI33btSortConstraintOnIslandPredicateEEvRKT_ii(%class.btAlignedObjectArray.4* %this1, %class.btSortConstraintOnIslandPredicate* nonnull align 1 dereferenceable(1) %33, i32 %34, i32 %35)
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %if.end15
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK33btSortConstraintOnIslandPredicateclEPK17btTypedConstraintS2_(%class.btSortConstraintOnIslandPredicate* %this, %class.btTypedConstraint* %lhs, %class.btTypedConstraint* %rhs) #2 comdat {
entry:
  %this.addr = alloca %class.btSortConstraintOnIslandPredicate*, align 4
  %lhs.addr = alloca %class.btTypedConstraint*, align 4
  %rhs.addr = alloca %class.btTypedConstraint*, align 4
  %rIslandId0 = alloca i32, align 4
  %lIslandId0 = alloca i32, align 4
  store %class.btSortConstraintOnIslandPredicate* %this, %class.btSortConstraintOnIslandPredicate** %this.addr, align 4
  store %class.btTypedConstraint* %lhs, %class.btTypedConstraint** %lhs.addr, align 4
  store %class.btTypedConstraint* %rhs, %class.btTypedConstraint** %rhs.addr, align 4
  %this1 = load %class.btSortConstraintOnIslandPredicate*, %class.btSortConstraintOnIslandPredicate** %this.addr, align 4
  %0 = load %class.btTypedConstraint*, %class.btTypedConstraint** %rhs.addr, align 4
  %call = call i32 @_Z23btGetConstraintIslandIdPK17btTypedConstraint(%class.btTypedConstraint* %0)
  store i32 %call, i32* %rIslandId0, align 4
  %1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %lhs.addr, align 4
  %call2 = call i32 @_Z23btGetConstraintIslandIdPK17btTypedConstraint(%class.btTypedConstraint* %1)
  store i32 %call2, i32* %lIslandId0, align 4
  %2 = load i32, i32* %lIslandId0, align 4
  %3 = load i32, i32* %rIslandId0, align 4
  %cmp = icmp slt i32 %2, %3
  ret i1 %cmp
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btDiscreteDynamicsWorld.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { argmemonly nounwind willreturn writeonly }
attributes #7 = { cold noreturn nounwind }
attributes #8 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind readnone speculatable willreturn }
attributes #10 = { nounwind }
attributes #11 = { noreturn nounwind }
attributes #12 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
