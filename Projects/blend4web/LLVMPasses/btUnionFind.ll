; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btUnionFind.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btUnionFind.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btUnionFind = type { %class.btAlignedObjectArray }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btElement*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btElement = type { i32, i32 }
%class.btUnionFindElementSortPredicate = type { i8 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN20btAlignedObjectArrayI9btElementED2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btElementEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btElementE6resizeEiRKS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btElementE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btElementEixEi = comdat any

$_ZNK20btAlignedObjectArrayI9btElementE4sizeEv = comdat any

$_ZN11btUnionFind4findEi = comdat any

$_ZN20btAlignedObjectArrayI9btElementE9quickSortI31btUnionFindElementSortPredicateEEvRKT_ = comdat any

$_ZN18btAlignedAllocatorI9btElementLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btElementE4initEv = comdat any

$_ZN20btAlignedObjectArrayI9btElementE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayI9btElementE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btElementE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btElementE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btElementE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI9btElementE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btElementLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI9btElementLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvRKT_ii = comdat any

$_ZNK31btUnionFindElementSortPredicateclERK9btElementS2_ = comdat any

$_ZN20btAlignedObjectArrayI9btElementE4swapEii = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btUnionFind.cpp, i8* null }]

@_ZN11btUnionFindD1Ev = hidden unnamed_addr alias %class.btUnionFind* (%class.btUnionFind*), %class.btUnionFind* (%class.btUnionFind*)* @_ZN11btUnionFindD2Ev
@_ZN11btUnionFindC1Ev = hidden unnamed_addr alias %class.btUnionFind* (%class.btUnionFind*), %class.btUnionFind* (%class.btUnionFind*)* @_ZN11btUnionFindC2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btUnionFind* @_ZN11btUnionFindD2Ev(%class.btUnionFind* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  call void @_ZN11btUnionFind4FreeEv(%class.btUnionFind* %this1)
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btElementED2Ev(%class.btAlignedObjectArray* %m_elements) #6
  ret %class.btUnionFind* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN11btUnionFind4FreeEv(%class.btUnionFind* %this) #2 {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  call void @_ZN20btAlignedObjectArrayI9btElementE5clearEv(%class.btAlignedObjectArray* %m_elements)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btElementED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btElementE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btUnionFind* @_ZN11btUnionFindC2Ev(%class.btUnionFind* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btElementEC2Ev(%class.btAlignedObjectArray* %m_elements)
  ret %class.btUnionFind* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btElementEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btElementLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btElementE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN11btUnionFind8allocateEi(%class.btUnionFind* %this, i32 %N) #2 {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %N.addr = alloca i32, align 4
  %ref.tmp = alloca %struct.btElement, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4
  store i32 %N, i32* %N.addr, align 4
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %0 = load i32, i32* %N.addr, align 4
  %1 = bitcast %struct.btElement* %ref.tmp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %1, i8 0, i32 8, i1 false)
  call void @_ZN20btAlignedObjectArrayI9btElementE6resizeEiRKS0_(%class.btAlignedObjectArray* %m_elements, i32 %0, %struct.btElement* nonnull align 4 dereferenceable(8) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btElementE6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %struct.btElement* nonnull align 4 dereferenceable(8) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btElement*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %struct.btElement* %fillData, %struct.btElement** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %struct.btElement*, %struct.btElement** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btElement, %struct.btElement* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btElementE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %14 = load %struct.btElement*, %struct.btElement** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %struct.btElement, %struct.btElement* %14, i32 %15
  %16 = bitcast %struct.btElement* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %struct.btElement*
  %18 = load %struct.btElement*, %struct.btElement** %fillData.addr, align 4
  %19 = bitcast %struct.btElement* %17 to i8*
  %20 = bitcast %struct.btElement* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 8, i1 false)
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %21, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btElementE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btElementE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btElementE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btElementE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN11btUnionFind5resetEi(%class.btUnionFind* %this, i32 %N) #2 {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %N.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4
  store i32 %N, i32* %N.addr, align 4
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %0 = load i32, i32* %N.addr, align 4
  call void @_ZN11btUnionFind8allocateEi(%class.btUnionFind* %this1, i32 %0)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %N.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32, i32* %i, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements, i32 %4)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call, i32 0, i32 0
  store i32 %3, i32* %m_id, align 4
  %m_elements2 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %5 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements2, i32 %5)
  %m_sz = getelementptr inbounds %struct.btElement, %struct.btElement* %call3, i32 0, i32 1
  store i32 1, i32* %m_sz, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btElement*, %struct.btElement** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btElement, %struct.btElement* %0, i32 %1
  ret %struct.btElement* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN11btUnionFind11sortIslandsEv(%class.btUnionFind* %this) #2 {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %numElements = alloca i32, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btUnionFindElementSortPredicate, align 1
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %m_elements)
  store i32 %call, i32* %numElements, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %numElements, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %i, align 4
  %call2 = call i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %this1, i32 %2)
  %m_elements3 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements3, i32 %3)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call4, i32 0, i32 0
  store i32 %call2, i32* %m_id, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_elements5 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  call void @_ZN20btAlignedObjectArrayI9btElementE9quickSortI31btUnionFindElementSortPredicateEEvRKT_(%class.btAlignedObjectArray* %m_elements5, %class.btUnionFindElementSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %this, i32 %x) #2 comdat {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %x.addr = alloca i32, align 4
  %elementPtr = alloca %struct.btElement*, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4
  store i32 %x, i32* %x.addr, align 4
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %x.addr, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %1 = load i32, i32* %x.addr, align 4
  %call = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements, i32 %1)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call, i32 0, i32 0
  %2 = load i32, i32* %m_id, align 4
  %cmp = icmp ne i32 %0, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %m_elements2 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %m_elements3 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %3 = load i32, i32* %x.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements3, i32 %3)
  %m_id5 = getelementptr inbounds %struct.btElement, %struct.btElement* %call4, i32 0, i32 0
  %4 = load i32, i32* %m_id5, align 4
  %call6 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements2, i32 %4)
  store %struct.btElement* %call6, %struct.btElement** %elementPtr, align 4
  %5 = load %struct.btElement*, %struct.btElement** %elementPtr, align 4
  %m_id7 = getelementptr inbounds %struct.btElement, %struct.btElement* %5, i32 0, i32 0
  %6 = load i32, i32* %m_id7, align 4
  %m_elements8 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %7 = load i32, i32* %x.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements8, i32 %7)
  %m_id10 = getelementptr inbounds %struct.btElement, %struct.btElement* %call9, i32 0, i32 0
  store i32 %6, i32* %m_id10, align 4
  %8 = load %struct.btElement*, %struct.btElement** %elementPtr, align 4
  %m_id11 = getelementptr inbounds %struct.btElement, %struct.btElement* %8, i32 0, i32 0
  %9 = load i32, i32* %m_id11, align 4
  store i32 %9, i32* %x.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %10 = load i32, i32* %x.addr, align 4
  ret i32 %10
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btElementE9quickSortI31btUnionFindElementSortPredicateEEvRKT_(%class.btAlignedObjectArray* %this, %class.btUnionFindElementSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %CompareFunc.addr = alloca %class.btUnionFindElementSortPredicate*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btUnionFindElementSortPredicate* %CompareFunc, %class.btUnionFindElementSortPredicate** %CompareFunc.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.btUnionFindElementSortPredicate*, %class.btUnionFindElementSortPredicate** %CompareFunc.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this1, %class.btUnionFindElementSortPredicate* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btElementLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btElementE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btElement* null, %struct.btElement** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btElementE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btElement*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btElementE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btElementE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btElement*
  store %struct.btElement* %2, %struct.btElement** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %struct.btElement*, %struct.btElement** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI9btElementE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btElement* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btElementE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btElementE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btElement*, %struct.btElement** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btElement* %4, %struct.btElement** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btElementE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btElementE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btElement* @_ZN18btAlignedAllocatorI9btElementLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btElement** null)
  %2 = bitcast %struct.btElement* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btElementE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btElement* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btElement*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btElement* %dest, %struct.btElement** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btElement*, %struct.btElement** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btElement, %struct.btElement* %3, i32 %4
  %5 = bitcast %struct.btElement* %arrayidx to i8*
  %6 = bitcast i8* %5 to %struct.btElement*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %struct.btElement*, %struct.btElement** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btElement, %struct.btElement* %7, i32 %8
  %9 = bitcast %struct.btElement* %6 to i8*
  %10 = bitcast %struct.btElement* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 8, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btElementE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %struct.btElement*, %struct.btElement** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btElement, %struct.btElement* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btElementE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btElement*, %struct.btElement** %m_data, align 4
  %tobool = icmp ne %struct.btElement* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btElement*, %struct.btElement** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI9btElementLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.btElement* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btElement* null, %struct.btElement** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btElement* @_ZN18btAlignedAllocatorI9btElementLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %struct.btElement** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btElement**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btElement** %hint, %struct.btElement*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 8, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btElement*
  ret %struct.btElement* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btElementLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.btElement* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btElement*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %struct.btElement* %ptr, %struct.btElement** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btElement*, %struct.btElement** %ptr.addr, align 4
  %1 = bitcast %struct.btElement* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this, %class.btUnionFindElementSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %CompareFunc.addr = alloca %class.btUnionFindElementSortPredicate*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %struct.btElement, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btUnionFindElementSortPredicate* %CompareFunc, %class.btUnionFindElementSortPredicate** %CompareFunc.addr, align 4
  store i32 %lo, i32* %lo.addr, align 4
  store i32 %hi, i32* %hi.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %lo.addr, align 4
  store i32 %0, i32* %i, align 4
  %1 = load i32, i32* %hi.addr, align 4
  store i32 %1, i32* %j, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btElement*, %struct.btElement** %m_data, align 4
  %3 = load i32, i32* %lo.addr, align 4
  %4 = load i32, i32* %hi.addr, align 4
  %add = add nsw i32 %3, %4
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %struct.btElement, %struct.btElement* %2, i32 %div
  %5 = bitcast %struct.btElement* %x to i8*
  %6 = bitcast %struct.btElement* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 8, i1 false)
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %7 = load %class.btUnionFindElementSortPredicate*, %class.btUnionFindElementSortPredicate** %CompareFunc.addr, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %struct.btElement*, %struct.btElement** %m_data2, align 4
  %9 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds %struct.btElement, %struct.btElement* %8, i32 %9
  %call = call zeroext i1 @_ZNK31btUnionFindElementSortPredicateclERK9btElementS2_(%class.btUnionFindElementSortPredicate* %7, %struct.btElement* nonnull align 4 dereferenceable(8) %arrayidx3, %struct.btElement* nonnull align 4 dereferenceable(8) %x)
  br i1 %call, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond4

while.cond4:                                      ; preds = %while.body8, %while.end
  %11 = load %class.btUnionFindElementSortPredicate*, %class.btUnionFindElementSortPredicate** %CompareFunc.addr, align 4
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %12 = load %struct.btElement*, %struct.btElement** %m_data5, align 4
  %13 = load i32, i32* %j, align 4
  %arrayidx6 = getelementptr inbounds %struct.btElement, %struct.btElement* %12, i32 %13
  %call7 = call zeroext i1 @_ZNK31btUnionFindElementSortPredicateclERK9btElementS2_(%class.btUnionFindElementSortPredicate* %11, %struct.btElement* nonnull align 4 dereferenceable(8) %x, %struct.btElement* nonnull align 4 dereferenceable(8) %arrayidx6)
  br i1 %call7, label %while.body8, label %while.end9

while.body8:                                      ; preds = %while.cond4
  %14 = load i32, i32* %j, align 4
  %dec = add nsw i32 %14, -1
  store i32 %dec, i32* %j, align 4
  br label %while.cond4

while.end9:                                       ; preds = %while.cond4
  %15 = load i32, i32* %i, align 4
  %16 = load i32, i32* %j, align 4
  %cmp = icmp sle i32 %15, %16
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end9
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayI9btElementE4swapEii(%class.btAlignedObjectArray* %this1, i32 %17, i32 %18)
  %19 = load i32, i32* %i, align 4
  %inc10 = add nsw i32 %19, 1
  store i32 %inc10, i32* %i, align 4
  %20 = load i32, i32* %j, align 4
  %dec11 = add nsw i32 %20, -1
  store i32 %dec11, i32* %j, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end9
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %21 = load i32, i32* %i, align 4
  %22 = load i32, i32* %j, align 4
  %cmp12 = icmp sle i32 %21, %22
  br i1 %cmp12, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %23 = load i32, i32* %lo.addr, align 4
  %24 = load i32, i32* %j, align 4
  %cmp13 = icmp slt i32 %23, %24
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %do.end
  %25 = load %class.btUnionFindElementSortPredicate*, %class.btUnionFindElementSortPredicate** %CompareFunc.addr, align 4
  %26 = load i32, i32* %lo.addr, align 4
  %27 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this1, %class.btUnionFindElementSortPredicate* nonnull align 1 dereferenceable(1) %25, i32 %26, i32 %27)
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %do.end
  %28 = load i32, i32* %i, align 4
  %29 = load i32, i32* %hi.addr, align 4
  %cmp16 = icmp slt i32 %28, %29
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end15
  %30 = load %class.btUnionFindElementSortPredicate*, %class.btUnionFindElementSortPredicate** %CompareFunc.addr, align 4
  %31 = load i32, i32* %i, align 4
  %32 = load i32, i32* %hi.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this1, %class.btUnionFindElementSortPredicate* nonnull align 1 dereferenceable(1) %30, i32 %31, i32 %32)
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %if.end15
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK31btUnionFindElementSortPredicateclERK9btElementS2_(%class.btUnionFindElementSortPredicate* %this, %struct.btElement* nonnull align 4 dereferenceable(8) %lhs, %struct.btElement* nonnull align 4 dereferenceable(8) %rhs) #1 comdat {
entry:
  %this.addr = alloca %class.btUnionFindElementSortPredicate*, align 4
  %lhs.addr = alloca %struct.btElement*, align 4
  %rhs.addr = alloca %struct.btElement*, align 4
  store %class.btUnionFindElementSortPredicate* %this, %class.btUnionFindElementSortPredicate** %this.addr, align 4
  store %struct.btElement* %lhs, %struct.btElement** %lhs.addr, align 4
  store %struct.btElement* %rhs, %struct.btElement** %rhs.addr, align 4
  %this1 = load %class.btUnionFindElementSortPredicate*, %class.btUnionFindElementSortPredicate** %this.addr, align 4
  %0 = load %struct.btElement*, %struct.btElement** %lhs.addr, align 4
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %0, i32 0, i32 0
  %1 = load i32, i32* %m_id, align 4
  %2 = load %struct.btElement*, %struct.btElement** %rhs.addr, align 4
  %m_id2 = getelementptr inbounds %struct.btElement, %struct.btElement* %2, i32 0, i32 0
  %3 = load i32, i32* %m_id2, align 4
  %cmp = icmp slt i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btElementE4swapEii(%class.btAlignedObjectArray* %this, i32 %index0, i32 %index1) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %struct.btElement, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btElement*, %struct.btElement** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btElement, %struct.btElement* %0, i32 %1
  %2 = bitcast %struct.btElement* %temp to i8*
  %3 = bitcast %struct.btElement* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 8, i1 false)
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %struct.btElement*, %struct.btElement** %m_data2, align 4
  %5 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %struct.btElement, %struct.btElement* %4, i32 %5
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %6 = load %struct.btElement*, %struct.btElement** %m_data4, align 4
  %7 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %struct.btElement, %struct.btElement* %6, i32 %7
  %8 = bitcast %struct.btElement* %arrayidx5 to i8*
  %9 = bitcast %struct.btElement* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 8, i1 false)
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %10 = load %struct.btElement*, %struct.btElement** %m_data6, align 4
  %11 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %struct.btElement, %struct.btElement* %10, i32 %11
  %12 = bitcast %struct.btElement* %arrayidx7 to i8*
  %13 = bitcast %struct.btElement* %temp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 8, i1 false)
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btUnionFind.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
