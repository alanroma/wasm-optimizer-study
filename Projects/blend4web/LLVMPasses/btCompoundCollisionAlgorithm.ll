; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btCompoundCollisionAlgorithm.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btCompoundCollisionAlgorithm.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btCompoundCollisionAlgorithm = type { %class.btActivatingCollisionAlgorithm, %class.btAlignedObjectArray, %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.11, i8, %class.btPersistentManifold*, i8, i32 }
%class.btActivatingCollisionAlgorithm = type { %class.btCollisionAlgorithm }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btDbvtNode**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon.0 }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%union.anon.0 = type { [2 x %struct.btDbvtNode*] }
%class.btAlignedObjectArray.1 = type <{ %class.btAlignedAllocator.2, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.2 = type { i8 }
%class.btAlignedObjectArray.11 = type <{ %class.btAlignedAllocator.12, [3 x i8], i32, i32, %class.btCollisionAlgorithm**, i8, [3 x i8] }>
%class.btAlignedAllocator.12 = type { i8 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.4, %union.anon.5, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.4 = type { float }
%union.anon.5 = type { float }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray.6, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.6 = type <{ %class.btAlignedAllocator.7, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.7 = type { i8 }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%class.btCompoundShape = type { %class.btCollisionShape, %class.btAlignedObjectArray.15, %class.btVector3, %class.btVector3, %struct.btDbvt*, i32, float, %class.btVector3 }
%class.btAlignedObjectArray.15 = type <{ %class.btAlignedAllocator.16, [3 x i8], i32, i32, %struct.btCompoundShapeChild*, i8, [3 x i8] }>
%class.btAlignedAllocator.16 = type { i8 }
%struct.btCompoundShapeChild = type { %class.btTransform, %class.btCollisionShape*, i32, float, %struct.btDbvtNode* }
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray.19 }
%class.btAlignedObjectArray.19 = type <{ %class.btAlignedAllocator.20, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator.20 = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type { i32 (...)** }
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32, float }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%struct.btCompoundLeafCallback = type { %"struct.btDbvt::ICollide", %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btDispatcher*, %struct.btDispatcherInfo*, %class.btManifoldResult*, %class.btCollisionAlgorithm**, %class.btPersistentManifold* }
%"struct.btDbvt::ICollide" = type { i32 (...)** }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEC2Ev = comdat any

$_ZNK24btCollisionObjectWrapper17getCollisionShapeEv = comdat any

$_ZNK15btCompoundShape17getUpdateRevisionEv = comdat any

$_ZNK15btCompoundShape17getNumChildShapesEv = comdat any

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE6resizeEiRKS1_ = comdat any

$_ZNK15btCompoundShape18getDynamicAabbTreeEv = comdat any

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi = comdat any

$_ZNK15btCompoundShape13getChildShapeEi = comdat any

$_ZNK24btCollisionObjectWrapper18getCollisionObjectEv = comdat any

$_ZNK24btCollisionObjectWrapper17getWorldTransformEv = comdat any

$_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii = comdat any

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmED2Ev = comdat any

$_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev = comdat any

$_ZN22btCompoundLeafCallbackC2EPK24btCollisionObjectWrapperS2_P12btDispatcherRK16btDispatcherInfoP16btManifoldResultPP20btCollisionAlgorithmP20btPersistentManifold = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi = comdat any

$_ZNK20btPersistentManifold14getNumContactsEv = comdat any

$_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold = comdat any

$_ZN16btManifoldResult20refreshContactPointsEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZNK11btTransform7inverseEv = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_ = comdat any

$_ZNK6btDbvt21collideTVNoStackAllocEPK10btDbvtNodeRK12btDbvtAabbMmR20btAlignedObjectArrayIS2_ERNS_8ICollideE = comdat any

$_ZN22btCompoundLeafCallback17ProcessChildShapeEPK16btCollisionShapei = comdat any

$_ZNK15btCompoundShape17getChildTransformEi = comdat any

$_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_ = comdat any

$_ZN22btCompoundLeafCallbackD2Ev = comdat any

$_ZN17btCollisionObject17getCollisionShapeEv = comdat any

$_ZN17btCollisionObject17getWorldTransformEv = comdat any

$_ZN15btCompoundShape17getChildTransformEi = comdat any

$_ZN17btCollisionObject17setWorldTransformERK11btTransform = comdat any

$_ZN28btCompoundCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE = comdat any

$_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi = comdat any

$_ZN6btDbvt8ICollideC2Ev = comdat any

$_ZN22btCompoundLeafCallbackD0Ev = comdat any

$_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_ = comdat any

$_ZN22btCompoundLeafCallback7ProcessEPK10btDbvtNode = comdat any

$_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef = comdat any

$_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode = comdat any

$_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode = comdat any

$_ZN6btDbvt8ICollideD2Ev = comdat any

$_ZN6btDbvt8ICollideD0Ev = comdat any

$_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK17btCollisionObject17getWorldTransformEv = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZN12btDbvtAabbMmC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE6resizeEiRKS2_ = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_ = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeEixEi = comdat any

$_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8pop_backEv = comdat any

$_Z9IntersectRK12btDbvtAabbMmS1_ = comdat any

$_ZNK10btDbvtNode10isinternalEv = comdat any

$_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_ = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE8allocateEiPPKS2_ = comdat any

$_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE10deallocateEPS2_ = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9allocSizeEi = comdat any

$_ZNK10btDbvtNode6isleafEv = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZNK16btManifoldResult16getBody0InternalEv = comdat any

$_ZNK16btManifoldResult12getBody0WrapEv = comdat any

$_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper = comdat any

$_ZNK16btManifoldResult12getBody1WrapEv = comdat any

$_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi = comdat any

$_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE4initEv = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE5clearEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv = comdat any

$_ZN18btAlignedAllocatorIP20btCollisionAlgorithmLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btCollisionAlgorithmLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btCollisionAlgorithmLj16EE8allocateEiPPKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZTV22btCompoundLeafCallback = comdat any

$_ZTS22btCompoundLeafCallback = comdat any

$_ZTSN6btDbvt8ICollideE = comdat any

$_ZTIN6btDbvt8ICollideE = comdat any

$_ZTI22btCompoundLeafCallback = comdat any

$_ZTVN6btDbvt8ICollideE = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@gCompoundChildShapePairCallback = hidden global i1 (%class.btCollisionShape*, %class.btCollisionShape*)* null, align 4
@_ZTV28btCompoundCollisionAlgorithm = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI28btCompoundCollisionAlgorithm to i8*), i8* bitcast (%class.btCompoundCollisionAlgorithm* (%class.btCompoundCollisionAlgorithm*)* @_ZN28btCompoundCollisionAlgorithmD1Ev to i8*), i8* bitcast (void (%class.btCompoundCollisionAlgorithm*)* @_ZN28btCompoundCollisionAlgorithmD0Ev to i8*), i8* bitcast (void (%class.btCompoundCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN28btCompoundCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (float (%class.btCompoundCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN28btCompoundCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (void (%class.btCompoundCollisionAlgorithm*, %class.btAlignedObjectArray.1*)* @_ZN28btCompoundCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS28btCompoundCollisionAlgorithm = hidden constant [31 x i8] c"28btCompoundCollisionAlgorithm\00", align 1
@_ZTI30btActivatingCollisionAlgorithm = external constant i8*
@_ZTI28btCompoundCollisionAlgorithm = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([31 x i8], [31 x i8]* @_ZTS28btCompoundCollisionAlgorithm, i32 0, i32 0), i8* bitcast (i8** @_ZTI30btActivatingCollisionAlgorithm to i8*) }, align 4
@_ZTV22btCompoundLeafCallback = linkonce_odr hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22btCompoundLeafCallback to i8*), i8* bitcast (%struct.btCompoundLeafCallback* (%struct.btCompoundLeafCallback*)* @_ZN22btCompoundLeafCallbackD2Ev to i8*), i8* bitcast (void (%struct.btCompoundLeafCallback*)* @_ZN22btCompoundLeafCallbackD0Ev to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_ to i8*), i8* bitcast (void (%struct.btCompoundLeafCallback*, %struct.btDbvtNode*)* @_ZN22btCompoundLeafCallback7ProcessEPK10btDbvtNode to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, float)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode to i8*)] }, comdat, align 4
@_ZTS22btCompoundLeafCallback = linkonce_odr hidden constant [25 x i8] c"22btCompoundLeafCallback\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTSN6btDbvt8ICollideE = linkonce_odr hidden constant [19 x i8] c"N6btDbvt8ICollideE\00", comdat, align 1
@_ZTIN6btDbvt8ICollideE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTSN6btDbvt8ICollideE, i32 0, i32 0) }, comdat, align 4
@_ZTI22btCompoundLeafCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([25 x i8], [25 x i8]* @_ZTS22btCompoundLeafCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN6btDbvt8ICollideE to i8*) }, comdat, align 4
@_ZTVN6btDbvt8ICollideE = linkonce_odr hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN6btDbvt8ICollideE to i8*), i8* bitcast (%"struct.btDbvt::ICollide"* (%"struct.btDbvt::ICollide"*)* @_ZN6btDbvt8ICollideD2Ev to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*)* @_ZN6btDbvt8ICollideD0Ev to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_ to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, float)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode to i8*)] }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btCompoundCollisionAlgorithm.cpp, i8* null }]

@_ZN28btCompoundCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b = hidden unnamed_addr alias %class.btCompoundCollisionAlgorithm* (%class.btCompoundCollisionAlgorithm*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1), %class.btCompoundCollisionAlgorithm* (%class.btCompoundCollisionAlgorithm*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1)* @_ZN28btCompoundCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b
@_ZN28btCompoundCollisionAlgorithmD1Ev = hidden unnamed_addr alias %class.btCompoundCollisionAlgorithm* (%class.btCompoundCollisionAlgorithm*), %class.btCompoundCollisionAlgorithm* (%class.btCompoundCollisionAlgorithm*)* @_ZN28btCompoundCollisionAlgorithmD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btCompoundCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btCompoundCollisionAlgorithm* returned %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, i1 zeroext %isSwapped) unnamed_addr #2 {
entry:
  %retval = alloca %class.btCompoundCollisionAlgorithm*, align 4
  %this.addr = alloca %class.btCompoundCollisionAlgorithm*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %isSwapped.addr = alloca i8, align 1
  %colObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %compoundShape = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundCollisionAlgorithm* %this, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %frombool = zext i1 %isSwapped to i8
  store i8 %frombool, i8* %isSwapped.addr, align 1
  %this1 = load %class.btCompoundCollisionAlgorithm*, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  store %class.btCompoundCollisionAlgorithm* %this1, %class.btCompoundCollisionAlgorithm** %retval, align 4
  %0 = bitcast %class.btCompoundCollisionAlgorithm* %this1 to %class.btActivatingCollisionAlgorithm*
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call = call %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%class.btActivatingCollisionAlgorithm* %0, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %1, %struct.btCollisionObjectWrapper* %2, %struct.btCollisionObjectWrapper* %3)
  %4 = bitcast %class.btCompoundCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV28btCompoundCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %4, align 4
  %stack2 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEC2Ev(%class.btAlignedObjectArray* %stack2)
  %manifoldArray = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.1* %manifoldArray)
  %m_childCollisionAlgorithms = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %call4 = call %class.btAlignedObjectArray.11* @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEC2Ev(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms)
  %m_isSwapped = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 4
  %5 = load i8, i8* %isSwapped.addr, align 1
  %tobool = trunc i8 %5 to i1
  %frombool5 = zext i1 %tobool to i8
  store i8 %frombool5, i8* %m_isSwapped, align 4
  %m_sharedManifold = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 5
  %6 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %m_manifold = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %6, i32 0, i32 1
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifold, align 4
  store %class.btPersistentManifold* %7, %class.btPersistentManifold** %m_sharedManifold, align 4
  %m_ownsManifold = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 6
  store i8 0, i8* %m_ownsManifold, align 4
  %m_isSwapped6 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 4
  %8 = load i8, i8* %m_isSwapped6, align 4
  %tobool7 = trunc i8 %8 to i1
  br i1 %tobool7, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btCollisionObjectWrapper* [ %9, %cond.true ], [ %10, %cond.false ]
  store %struct.btCollisionObjectWrapper* %cond, %struct.btCollisionObjectWrapper** %colObjWrap, align 4
  %11 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObjWrap, align 4
  %call8 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %11)
  %12 = bitcast %class.btCollisionShape* %call8 to %class.btCompoundShape*
  store %class.btCompoundShape* %12, %class.btCompoundShape** %compoundShape, align 4
  %13 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4
  %call9 = call i32 @_ZNK15btCompoundShape17getUpdateRevisionEv(%class.btCompoundShape* %13)
  %m_compoundShapeRevision = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 7
  store i32 %call9, i32* %m_compoundShapeRevision, align 4
  %14 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %15 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  call void @_ZN28btCompoundCollisionAlgorithm26preallocateChildAlgorithmsEPK24btCollisionObjectWrapperS2_(%class.btCompoundCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %14, %struct.btCollisionObjectWrapper* %15)
  %16 = load %class.btCompoundCollisionAlgorithm*, %class.btCompoundCollisionAlgorithm** %retval, align 4
  ret %class.btCompoundCollisionAlgorithm* %16
}

declare %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%class.btActivatingCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.1* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.2* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.2* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.1* %this1)
  ret %class.btAlignedObjectArray.1* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.11* @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEC2Ev(%class.btAlignedObjectArray.11* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.11*, align 4
  store %class.btAlignedObjectArray.11* %this, %class.btAlignedObjectArray.11** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.11*, %class.btAlignedObjectArray.11** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.12* @_ZN18btAlignedAllocatorIP20btCollisionAlgorithmLj16EEC2Ev(%class.btAlignedAllocator.12* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE4initEv(%class.btAlignedObjectArray.11* %this1)
  ret %class.btAlignedObjectArray.11* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_shape, align 4
  ret %class.btCollisionShape* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK15btCompoundShape17getUpdateRevisionEv(%class.btCompoundShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_updateRevision, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define hidden void @_ZN28btCompoundCollisionAlgorithm26preallocateChildAlgorithmsEPK24btCollisionObjectWrapperS2_(%class.btCompoundCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) #2 {
entry:
  %this.addr = alloca %class.btCompoundCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %colObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %otherObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %compoundShape = alloca %class.btCompoundShape*, align 4
  %numChildren = alloca i32, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btCollisionAlgorithm*, align 4
  %childShape = alloca %class.btCollisionShape*, align 4
  %childWrap = alloca %struct.btCollisionObjectWrapper, align 4
  %m_childCollisionAlgorithmsContact = alloca %class.btAlignedObjectArray.11, align 4
  %m_childCollisionAlgorithmsClosestPoints = alloca %class.btAlignedObjectArray.11, align 4
  store %class.btCompoundCollisionAlgorithm* %this, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %this1 = load %class.btCompoundCollisionAlgorithm*, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  %m_isSwapped = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 4
  %0 = load i8, i8* %m_isSwapped, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btCollisionObjectWrapper* [ %1, %cond.true ], [ %2, %cond.false ]
  store %struct.btCollisionObjectWrapper* %cond, %struct.btCollisionObjectWrapper** %colObjWrap, align 4
  %m_isSwapped2 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 4
  %3 = load i8, i8* %m_isSwapped2, align 4
  %tobool3 = trunc i8 %3 to i1
  br i1 %tobool3, label %cond.true4, label %cond.false5

cond.true4:                                       ; preds = %cond.end
  %4 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  br label %cond.end6

cond.false5:                                      ; preds = %cond.end
  %5 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  br label %cond.end6

cond.end6:                                        ; preds = %cond.false5, %cond.true4
  %cond7 = phi %struct.btCollisionObjectWrapper* [ %4, %cond.true4 ], [ %5, %cond.false5 ]
  store %struct.btCollisionObjectWrapper* %cond7, %struct.btCollisionObjectWrapper** %otherObjWrap, align 4
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObjWrap, align 4
  %call = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %6)
  %7 = bitcast %class.btCollisionShape* %call to %class.btCompoundShape*
  store %class.btCompoundShape* %7, %class.btCompoundShape** %compoundShape, align 4
  %8 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4
  %call8 = call i32 @_ZNK15btCompoundShape17getNumChildShapesEv(%class.btCompoundShape* %8)
  store i32 %call8, i32* %numChildren, align 4
  %m_childCollisionAlgorithms = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %9 = load i32, i32* %numChildren, align 4
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE6resizeEiRKS1_(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms, i32 %9, %class.btCollisionAlgorithm** nonnull align 4 dereferenceable(4) %ref.tmp)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end6
  %10 = load i32, i32* %i, align 4
  %11 = load i32, i32* %numChildren, align 4
  %cmp = icmp slt i32 %10, %11
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %12 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4
  %call9 = call %struct.btDbvt* @_ZNK15btCompoundShape18getDynamicAabbTreeEv(%class.btCompoundShape* %12)
  %tobool10 = icmp ne %struct.btDbvt* %call9, null
  br i1 %tobool10, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %m_childCollisionAlgorithms11 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %13 = load i32, i32* %i, align 4
  %call12 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms11, i32 %13)
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %call12, align 4
  br label %if.end

if.else:                                          ; preds = %for.body
  %14 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4
  %15 = load i32, i32* %i, align 4
  %call13 = call %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %14, i32 %15)
  store %class.btCollisionShape* %call13, %class.btCollisionShape** %childShape, align 4
  %16 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObjWrap, align 4
  %17 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape, align 4
  %18 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObjWrap, align 4
  %call14 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %18)
  %19 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObjWrap, align 4
  %call15 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %19)
  %20 = load i32, i32* %i, align 4
  %call16 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %childWrap, %struct.btCollisionObjectWrapper* %16, %class.btCollisionShape* %17, %class.btCollisionObject* %call14, %class.btTransform* nonnull align 4 dereferenceable(64) %call15, i32 -1, i32 %20)
  %21 = bitcast %class.btCompoundCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %21, i32 0, i32 1
  %22 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %23 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %otherObjWrap, align 4
  %m_sharedManifold = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 5
  %24 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_sharedManifold, align 4
  %25 = bitcast %class.btDispatcher* %22 to %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)***
  %vtable = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)**, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)*** %25, align 4
  %vfn = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)** %vtable, i64 2
  %26 = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)** %vfn, align 4
  %call17 = call %class.btCollisionAlgorithm* %26(%class.btDispatcher* %22, %struct.btCollisionObjectWrapper* %childWrap, %struct.btCollisionObjectWrapper* %23, %class.btPersistentManifold* %24, i32 1)
  %m_childCollisionAlgorithms18 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %27 = load i32, i32* %i, align 4
  %call19 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms18, i32 %27)
  store %class.btCollisionAlgorithm* %call17, %class.btCollisionAlgorithm** %call19, align 4
  %call20 = call %class.btAlignedObjectArray.11* @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEC2Ev(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithmsContact)
  %call21 = call %class.btAlignedObjectArray.11* @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEC2Ev(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithmsClosestPoints)
  %call22 = call %class.btAlignedObjectArray.11* @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmED2Ev(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithmsClosestPoints) #7
  %call23 = call %class.btAlignedObjectArray.11* @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmED2Ev(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithmsContact) #7
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %28 = load i32, i32* %i, align 4
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK15btCompoundShape17getNumChildShapesEv(%class.btCompoundShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray.15* %m_children)
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE6resizeEiRKS1_(%class.btAlignedObjectArray.11* %this, i32 %newsize, %class.btCollisionAlgorithm** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.11*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btCollisionAlgorithm**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.11* %this, %class.btAlignedObjectArray.11** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btCollisionAlgorithm** %fillData, %class.btCollisionAlgorithm*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.11*, %class.btAlignedObjectArray.11** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray.11* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 4
  %5 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE7reserveEi(%class.btAlignedObjectArray.11* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 4
  %14 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %14, i32 %15
  %16 = bitcast %class.btCollisionAlgorithm** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btCollisionAlgorithm**
  %18 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %fillData.addr, align 4
  %19 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %18, align 4
  store %class.btCollisionAlgorithm* %19, %class.btCollisionAlgorithm** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btDbvt* @_ZNK15btCompoundShape18getDynamicAabbTreeEv(%class.btCompoundShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_dynamicAabbTree = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %0 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree, align 4
  ret %struct.btDbvt* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray.11* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.11*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.11* %this, %class.btAlignedObjectArray.11** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.11*, %class.btAlignedObjectArray.11** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 4
  %0 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %0, i32 %1
  ret %class.btCollisionAlgorithm** %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray.15* %m_children, i32 %0)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call, i32 0, i32 1
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4
  ret %class.btCollisionShape* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4
  ret %class.btCollisionObject* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %0 = load %class.btTransform*, %class.btTransform** %m_worldTransform, align 4
  ret %class.btTransform* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* returned %this, %struct.btCollisionObjectWrapper* %parent, %class.btCollisionShape* %shape, %class.btCollisionObject* %collisionObject, %class.btTransform* nonnull align 4 dereferenceable(64) %worldTransform, i32 %partId, i32 %index) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %parent.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %shape.addr = alloca %class.btCollisionShape*, align 4
  %collisionObject.addr = alloca %class.btCollisionObject*, align 4
  %worldTransform.addr = alloca %class.btTransform*, align 4
  %partId.addr = alloca i32, align 4
  %index.addr = alloca i32, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  store %struct.btCollisionObjectWrapper* %parent, %struct.btCollisionObjectWrapper** %parent.addr, align 4
  store %class.btCollisionShape* %shape, %class.btCollisionShape** %shape.addr, align 4
  store %class.btCollisionObject* %collisionObject, %class.btCollisionObject** %collisionObject.addr, align 4
  store %class.btTransform* %worldTransform, %class.btTransform** %worldTransform.addr, align 4
  store i32 %partId, i32* %partId.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_parent = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 0
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %parent.addr, align 4
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %m_parent, align 4
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %shape.addr, align 4
  store %class.btCollisionShape* %1, %class.btCollisionShape** %m_shape, align 4
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4
  store %class.btCollisionObject* %2, %class.btCollisionObject** %m_collisionObject, align 4
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %3 = load %class.btTransform*, %class.btTransform** %worldTransform.addr, align 4
  store %class.btTransform* %3, %class.btTransform** %m_worldTransform, align 4
  %m_partId = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 4
  %4 = load i32, i32* %partId.addr, align 4
  store i32 %4, i32* %m_partId, align 4
  %m_index = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 5
  %5 = load i32, i32* %index.addr, align 4
  store i32 %5, i32* %m_index, align 4
  ret %struct.btCollisionObjectWrapper* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.11* @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmED2Ev(%class.btAlignedObjectArray.11* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.11*, align 4
  store %class.btAlignedObjectArray.11* %this, %class.btAlignedObjectArray.11** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.11*, %class.btAlignedObjectArray.11** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE5clearEv(%class.btAlignedObjectArray.11* %this1)
  ret %class.btAlignedObjectArray.11* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN28btCompoundCollisionAlgorithm21removeChildAlgorithmsEv(%class.btCompoundCollisionAlgorithm* %this) #2 {
entry:
  %this.addr = alloca %class.btCompoundCollisionAlgorithm*, align 4
  %numChildren = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btCompoundCollisionAlgorithm* %this, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  %this1 = load %class.btCompoundCollisionAlgorithm*, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  %m_childCollisionAlgorithms = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms)
  store i32 %call, i32* %numChildren, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %numChildren, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_childCollisionAlgorithms2 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %2 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms2, i32 %2)
  %3 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call3, align 4
  %tobool = icmp ne %class.btCollisionAlgorithm* %3, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %m_childCollisionAlgorithms4 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %4 = load i32, i32* %i, align 4
  %call5 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms4, i32 %4)
  %5 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call5, align 4
  %6 = bitcast %class.btCollisionAlgorithm* %5 to %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)***
  %vtable = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)**, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*** %6, align 4
  %vfn = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vtable, i64 0
  %7 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vfn, align 4
  %call6 = call %class.btCollisionAlgorithm* %7(%class.btCollisionAlgorithm* %5) #7
  %8 = bitcast %class.btCompoundCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %8, i32 0, i32 1
  %9 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %m_childCollisionAlgorithms7 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %10 = load i32, i32* %i, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms7, i32 %10)
  %11 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call8, align 4
  %12 = bitcast %class.btCollisionAlgorithm* %11 to i8*
  %13 = bitcast %class.btDispatcher* %9 to void (%class.btDispatcher*, i8*)***
  %vtable9 = load void (%class.btDispatcher*, i8*)**, void (%class.btDispatcher*, i8*)*** %13, align 4
  %vfn10 = getelementptr inbounds void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vtable9, i64 15
  %14 = load void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vfn10, align 4
  call void %14(%class.btDispatcher* %9, i8* %12)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %15 = load i32, i32* %i, align 4
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray.11* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.11*, align 4
  store %class.btAlignedObjectArray.11* %this, %class.btAlignedObjectArray.11** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.11*, %class.btAlignedObjectArray.11** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btCompoundCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithmD2Ev(%class.btCompoundCollisionAlgorithm* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btCompoundCollisionAlgorithm*, align 4
  store %class.btCompoundCollisionAlgorithm* %this, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  %this1 = load %class.btCompoundCollisionAlgorithm*, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btCompoundCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV28btCompoundCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  call void @_ZN28btCompoundCollisionAlgorithm21removeChildAlgorithmsEv(%class.btCompoundCollisionAlgorithm* %this1)
  %m_childCollisionAlgorithms = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %call = call %class.btAlignedObjectArray.11* @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmED2Ev(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms) #7
  %manifoldArray = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.1* %manifoldArray) #7
  %stack2 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %call3 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev(%class.btAlignedObjectArray* %stack2) #7
  %1 = bitcast %class.btCompoundCollisionAlgorithm* %this1 to %class.btActivatingCollisionAlgorithm*
  %call4 = call %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmD2Ev(%class.btActivatingCollisionAlgorithm* %1) #7
  ret %class.btCompoundCollisionAlgorithm* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.1* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.1* %this1)
  ret %class.btAlignedObjectArray.1* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
declare %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmD2Ev(%class.btActivatingCollisionAlgorithm* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN28btCompoundCollisionAlgorithmD0Ev(%class.btCompoundCollisionAlgorithm* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btCompoundCollisionAlgorithm*, align 4
  store %class.btCompoundCollisionAlgorithm* %this, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  %this1 = load %class.btCompoundCollisionAlgorithm*, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  %call = call %class.btCompoundCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithmD1Ev(%class.btCompoundCollisionAlgorithm* %this1) #7
  %0 = bitcast %class.btCompoundCollisionAlgorithm* %this1 to i8*
  call void @_ZdlPv(i8* %0) #8
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

; Function Attrs: noinline optnone
define hidden void @_ZN28btCompoundCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult(%class.btCompoundCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCompoundCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %colObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %otherObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %compoundShape = alloca %class.btCompoundShape*, align 4
  %tree = alloca %struct.btDbvt*, align 4
  %callback = alloca %struct.btCompoundLeafCallback, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btPersistentManifold*, align 4
  %m = alloca i32, align 4
  %ref.tmp43 = alloca %class.btPersistentManifold*, align 4
  %localAabbMin = alloca %class.btVector3, align 4
  %localAabbMax = alloca %class.btVector3, align 4
  %otherInCompoundSpace = alloca %class.btTransform, align 4
  %ref.tmp53 = alloca %class.btTransform, align 4
  %ref.tmp54 = alloca %class.btTransform, align 4
  %extraExtends = alloca %class.btVector3, align 4
  %bounds = alloca %struct.btDbvtAabbMm, align 4
  %numChildren = alloca i32, align 4
  %i68 = alloca i32, align 4
  %numChildren77 = alloca i32, align 4
  %i80 = alloca i32, align 4
  %ref.tmp82 = alloca %class.btPersistentManifold*, align 4
  %childShape = alloca %class.btCollisionShape*, align 4
  %orgTrans = alloca %class.btTransform, align 4
  %newChildWorldTrans = alloca %class.btTransform, align 4
  %aabbMin0 = alloca %class.btVector3, align 4
  %aabbMax0 = alloca %class.btVector3, align 4
  %aabbMin1 = alloca %class.btVector3, align 4
  %aabbMax1 = alloca %class.btVector3, align 4
  %childTrans = alloca %class.btTransform*, align 4
  %ref.tmp100 = alloca %class.btTransform, align 4
  store %class.btCompoundCollisionAlgorithm* %this, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4
  %this1 = load %class.btCompoundCollisionAlgorithm*, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  %m_isSwapped = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 4
  %0 = load i8, i8* %m_isSwapped, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btCollisionObjectWrapper* [ %1, %cond.true ], [ %2, %cond.false ]
  store %struct.btCollisionObjectWrapper* %cond, %struct.btCollisionObjectWrapper** %colObjWrap, align 4
  %m_isSwapped2 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 4
  %3 = load i8, i8* %m_isSwapped2, align 4
  %tobool3 = trunc i8 %3 to i1
  br i1 %tobool3, label %cond.true4, label %cond.false5

cond.true4:                                       ; preds = %cond.end
  %4 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  br label %cond.end6

cond.false5:                                      ; preds = %cond.end
  %5 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  br label %cond.end6

cond.end6:                                        ; preds = %cond.false5, %cond.true4
  %cond7 = phi %struct.btCollisionObjectWrapper* [ %4, %cond.true4 ], [ %5, %cond.false5 ]
  store %struct.btCollisionObjectWrapper* %cond7, %struct.btCollisionObjectWrapper** %otherObjWrap, align 4
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObjWrap, align 4
  %call = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %6)
  %7 = bitcast %class.btCollisionShape* %call to %class.btCompoundShape*
  store %class.btCompoundShape* %7, %class.btCompoundShape** %compoundShape, align 4
  %8 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4
  %call8 = call i32 @_ZNK15btCompoundShape17getUpdateRevisionEv(%class.btCompoundShape* %8)
  %m_compoundShapeRevision = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 7
  %9 = load i32, i32* %m_compoundShapeRevision, align 4
  %cmp = icmp ne i32 %call8, %9
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end6
  call void @_ZN28btCompoundCollisionAlgorithm21removeChildAlgorithmsEv(%class.btCompoundCollisionAlgorithm* %this1)
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %11 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  call void @_ZN28btCompoundCollisionAlgorithm26preallocateChildAlgorithmsEPK24btCollisionObjectWrapperS2_(%class.btCompoundCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %10, %struct.btCollisionObjectWrapper* %11)
  %12 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4
  %call9 = call i32 @_ZNK15btCompoundShape17getUpdateRevisionEv(%class.btCompoundShape* %12)
  %m_compoundShapeRevision10 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 7
  store i32 %call9, i32* %m_compoundShapeRevision10, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end6
  %m_childCollisionAlgorithms = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %call11 = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms)
  %cmp12 = icmp eq i32 %call11, 0
  br i1 %cmp12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %if.end
  br label %return

if.end14:                                         ; preds = %if.end
  %13 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4
  %call15 = call %struct.btDbvt* @_ZNK15btCompoundShape18getDynamicAabbTreeEv(%class.btCompoundShape* %13)
  store %struct.btDbvt* %call15, %struct.btDbvt** %tree, align 4
  %14 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObjWrap, align 4
  %15 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %otherObjWrap, align 4
  %16 = bitcast %class.btCompoundCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %16, i32 0, i32 1
  %17 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %18 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  %19 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %m_childCollisionAlgorithms16 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %call17 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms16, i32 0)
  %m_sharedManifold = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 5
  %20 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_sharedManifold, align 4
  %call18 = call %struct.btCompoundLeafCallback* @_ZN22btCompoundLeafCallbackC2EPK24btCollisionObjectWrapperS2_P12btDispatcherRK16btDispatcherInfoP16btManifoldResultPP20btCollisionAlgorithmP20btPersistentManifold(%struct.btCompoundLeafCallback* %callback, %struct.btCollisionObjectWrapper* %14, %struct.btCollisionObjectWrapper* %15, %class.btDispatcher* %17, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %18, %class.btManifoldResult* %19, %class.btCollisionAlgorithm** %call17, %class.btPersistentManifold* %20)
  %manifoldArray = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.1* %manifoldArray, i32 0, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %ref.tmp)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc45, %if.end14
  %21 = load i32, i32* %i, align 4
  %m_childCollisionAlgorithms19 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %call20 = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms19)
  %cmp21 = icmp slt i32 %21, %call20
  br i1 %cmp21, label %for.body, label %for.end47

for.body:                                         ; preds = %for.cond
  %m_childCollisionAlgorithms22 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %22 = load i32, i32* %i, align 4
  %call23 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms22, i32 %22)
  %23 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call23, align 4
  %tobool24 = icmp ne %class.btCollisionAlgorithm* %23, null
  br i1 %tobool24, label %if.then25, label %if.end44

if.then25:                                        ; preds = %for.body
  %m_childCollisionAlgorithms26 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %24 = load i32, i32* %i, align 4
  %call27 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms26, i32 %24)
  %25 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call27, align 4
  %manifoldArray28 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %26 = bitcast %class.btCollisionAlgorithm* %25 to void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)***
  %vtable = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)**, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)*** %26, align 4
  %vfn = getelementptr inbounds void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)** %vtable, i64 4
  %27 = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)** %vfn, align 4
  call void %27(%class.btCollisionAlgorithm* %25, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %manifoldArray28)
  store i32 0, i32* %m, align 4
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc, %if.then25
  %28 = load i32, i32* %m, align 4
  %manifoldArray30 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %call31 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.1* %manifoldArray30)
  %cmp32 = icmp slt i32 %28, %call31
  br i1 %cmp32, label %for.body33, label %for.end

for.body33:                                       ; preds = %for.cond29
  %manifoldArray34 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %29 = load i32, i32* %m, align 4
  %call35 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.1* %manifoldArray34, i32 %29)
  %30 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call35, align 4
  %call36 = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %30)
  %tobool37 = icmp ne i32 %call36, 0
  br i1 %tobool37, label %if.then38, label %if.end41

if.then38:                                        ; preds = %for.body33
  %31 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %manifoldArray39 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %32 = load i32, i32* %m, align 4
  %call40 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.1* %manifoldArray39, i32 %32)
  %33 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call40, align 4
  call void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %31, %class.btPersistentManifold* %33)
  %34 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  call void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %34)
  %35 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  call void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %35, %class.btPersistentManifold* null)
  br label %if.end41

if.end41:                                         ; preds = %if.then38, %for.body33
  br label %for.inc

for.inc:                                          ; preds = %if.end41
  %36 = load i32, i32* %m, align 4
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %m, align 4
  br label %for.cond29

for.end:                                          ; preds = %for.cond29
  %manifoldArray42 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %ref.tmp43, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.1* %manifoldArray42, i32 0, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %ref.tmp43)
  br label %if.end44

if.end44:                                         ; preds = %for.end, %for.body
  br label %for.inc45

for.inc45:                                        ; preds = %if.end44
  %37 = load i32, i32* %i, align 4
  %inc46 = add nsw i32 %37, 1
  store i32 %inc46, i32* %i, align 4
  br label %for.cond

for.end47:                                        ; preds = %for.cond
  %38 = load %struct.btDbvt*, %struct.btDbvt** %tree, align 4
  %tobool48 = icmp ne %struct.btDbvt* %38, null
  br i1 %tobool48, label %if.then49, label %if.else

if.then49:                                        ; preds = %for.end47
  %call50 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMin)
  %call51 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMax)
  %call52 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %otherInCompoundSpace)
  %39 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObjWrap, align 4
  %call55 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %39)
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp54, %class.btTransform* %call55)
  %40 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %otherObjWrap, align 4
  %call56 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %40)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp53, %class.btTransform* %ref.tmp54, %class.btTransform* nonnull align 4 dereferenceable(64) %call56)
  %call57 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %otherInCompoundSpace, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp53)
  %41 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %otherObjWrap, align 4
  %call58 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %41)
  %42 = bitcast %class.btCollisionShape* %call58 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable59 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %42, align 4
  %vfn60 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable59, i64 2
  %43 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn60, align 4
  call void %43(%class.btCollisionShape* %call58, %class.btTransform* nonnull align 4 dereferenceable(64) %otherInCompoundSpace, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax)
  %44 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %m_closestPointDistanceThreshold = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %44, i32 0, i32 8
  %45 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %m_closestPointDistanceThreshold61 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %45, i32 0, i32 8
  %46 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %m_closestPointDistanceThreshold62 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %46, i32 0, i32 8
  %call63 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %extraExtends, float* nonnull align 4 dereferenceable(4) %m_closestPointDistanceThreshold, float* nonnull align 4 dereferenceable(4) %m_closestPointDistanceThreshold61, float* nonnull align 4 dereferenceable(4) %m_closestPointDistanceThreshold62)
  %call64 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %extraExtends)
  %call65 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %localAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %extraExtends)
  call void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* sret align 4 %bounds, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax)
  %47 = load %struct.btDbvt*, %struct.btDbvt** %tree, align 4
  %48 = load %struct.btDbvt*, %struct.btDbvt** %tree, align 4
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %48, i32 0, i32 0
  %49 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %stack2 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %50 = bitcast %struct.btCompoundLeafCallback* %callback to %"struct.btDbvt::ICollide"*
  call void @_ZNK6btDbvt21collideTVNoStackAllocEPK10btDbvtNodeRK12btDbvtAabbMmR20btAlignedObjectArrayIS2_ERNS_8ICollideE(%struct.btDbvt* %47, %struct.btDbvtNode* %49, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %bounds, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %stack2, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %50)
  br label %if.end76

if.else:                                          ; preds = %for.end47
  %m_childCollisionAlgorithms66 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %call67 = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms66)
  store i32 %call67, i32* %numChildren, align 4
  store i32 0, i32* %i68, align 4
  br label %for.cond69

for.cond69:                                       ; preds = %for.inc73, %if.else
  %51 = load i32, i32* %i68, align 4
  %52 = load i32, i32* %numChildren, align 4
  %cmp70 = icmp slt i32 %51, %52
  br i1 %cmp70, label %for.body71, label %for.end75

for.body71:                                       ; preds = %for.cond69
  %53 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4
  %54 = load i32, i32* %i68, align 4
  %call72 = call %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %53, i32 %54)
  %55 = load i32, i32* %i68, align 4
  call void @_ZN22btCompoundLeafCallback17ProcessChildShapeEPK16btCollisionShapei(%struct.btCompoundLeafCallback* %callback, %class.btCollisionShape* %call72, i32 %55)
  br label %for.inc73

for.inc73:                                        ; preds = %for.body71
  %56 = load i32, i32* %i68, align 4
  %inc74 = add nsw i32 %56, 1
  store i32 %inc74, i32* %i68, align 4
  br label %for.cond69

for.end75:                                        ; preds = %for.cond69
  br label %if.end76

if.end76:                                         ; preds = %for.end75, %if.then49
  %m_childCollisionAlgorithms78 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %call79 = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms78)
  store i32 %call79, i32* %numChildren77, align 4
  %manifoldArray81 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %ref.tmp82, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.1* %manifoldArray81, i32 0, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %ref.tmp82)
  store %class.btCollisionShape* null, %class.btCollisionShape** %childShape, align 4
  %call83 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %orgTrans)
  %call84 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %newChildWorldTrans)
  %call85 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin0)
  %call86 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax0)
  %call87 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin1)
  %call88 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax1)
  store i32 0, i32* %i80, align 4
  br label %for.cond89

for.cond89:                                       ; preds = %for.inc124, %if.end76
  %57 = load i32, i32* %i80, align 4
  %58 = load i32, i32* %numChildren77, align 4
  %cmp90 = icmp slt i32 %57, %58
  br i1 %cmp90, label %for.body91, label %for.end126

for.body91:                                       ; preds = %for.cond89
  %m_childCollisionAlgorithms92 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %59 = load i32, i32* %i80, align 4
  %call93 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms92, i32 %59)
  %60 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call93, align 4
  %tobool94 = icmp ne %class.btCollisionAlgorithm* %60, null
  br i1 %tobool94, label %if.then95, label %if.end123

if.then95:                                        ; preds = %for.body91
  %61 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4
  %62 = load i32, i32* %i80, align 4
  %call96 = call %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %61, i32 %62)
  store %class.btCollisionShape* %call96, %class.btCollisionShape** %childShape, align 4
  %63 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObjWrap, align 4
  %call97 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %63)
  %call98 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %orgTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %call97)
  %64 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4
  %65 = load i32, i32* %i80, align 4
  %call99 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %64, i32 %65)
  store %class.btTransform* %call99, %class.btTransform** %childTrans, align 4
  %66 = load %class.btTransform*, %class.btTransform** %childTrans, align 4
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp100, %class.btTransform* %orgTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %66)
  %call101 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %newChildWorldTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp100)
  %67 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape, align 4
  %68 = bitcast %class.btCollisionShape* %67 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable102 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %68, align 4
  %vfn103 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable102, i64 2
  %69 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn103, align 4
  call void %69(%class.btCollisionShape* %67, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildWorldTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax0)
  %70 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %otherObjWrap, align 4
  %call104 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %70)
  %71 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %otherObjWrap, align 4
  %call105 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %71)
  %72 = bitcast %class.btCollisionShape* %call104 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable106 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %72, align 4
  %vfn107 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable106, i64 2
  %73 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn107, align 4
  call void %73(%class.btCollisionShape* %call104, %class.btTransform* nonnull align 4 dereferenceable(64) %call105, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1)
  %call108 = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1)
  br i1 %call108, label %if.end122, label %if.then109

if.then109:                                       ; preds = %if.then95
  %m_childCollisionAlgorithms110 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %74 = load i32, i32* %i80, align 4
  %call111 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms110, i32 %74)
  %75 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call111, align 4
  %76 = bitcast %class.btCollisionAlgorithm* %75 to %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)***
  %vtable112 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)**, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*** %76, align 4
  %vfn113 = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vtable112, i64 0
  %77 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vfn113, align 4
  %call114 = call %class.btCollisionAlgorithm* %77(%class.btCollisionAlgorithm* %75) #7
  %78 = bitcast %class.btCompoundCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher115 = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %78, i32 0, i32 1
  %79 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher115, align 4
  %m_childCollisionAlgorithms116 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %80 = load i32, i32* %i80, align 4
  %call117 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms116, i32 %80)
  %81 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call117, align 4
  %82 = bitcast %class.btCollisionAlgorithm* %81 to i8*
  %83 = bitcast %class.btDispatcher* %79 to void (%class.btDispatcher*, i8*)***
  %vtable118 = load void (%class.btDispatcher*, i8*)**, void (%class.btDispatcher*, i8*)*** %83, align 4
  %vfn119 = getelementptr inbounds void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vtable118, i64 15
  %84 = load void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vfn119, align 4
  call void %84(%class.btDispatcher* %79, i8* %82)
  %m_childCollisionAlgorithms120 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %85 = load i32, i32* %i80, align 4
  %call121 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms120, i32 %85)
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %call121, align 4
  br label %if.end122

if.end122:                                        ; preds = %if.then109, %if.then95
  br label %if.end123

if.end123:                                        ; preds = %if.end122, %for.body91
  br label %for.inc124

for.inc124:                                       ; preds = %if.end123
  %86 = load i32, i32* %i80, align 4
  %inc125 = add nsw i32 %86, 1
  store i32 %inc125, i32* %i80, align 4
  br label %for.cond89

for.end126:                                       ; preds = %for.cond89
  %call127 = call %struct.btCompoundLeafCallback* @_ZN22btCompoundLeafCallbackD2Ev(%struct.btCompoundLeafCallback* %callback) #7
  br label %return

return:                                           ; preds = %for.end126, %if.then13
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btCompoundLeafCallback* @_ZN22btCompoundLeafCallbackC2EPK24btCollisionObjectWrapperS2_P12btDispatcherRK16btDispatcherInfoP16btManifoldResultPP20btCollisionAlgorithmP20btPersistentManifold(%struct.btCompoundLeafCallback* returned %this, %struct.btCollisionObjectWrapper* %compoundObjWrap, %struct.btCollisionObjectWrapper* %otherObjWrap, %class.btDispatcher* %dispatcher, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut, %class.btCollisionAlgorithm** %childCollisionAlgorithms, %class.btPersistentManifold* %sharedManifold) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCompoundLeafCallback*, align 4
  %compoundObjWrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %otherObjWrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %childCollisionAlgorithms.addr = alloca %class.btCollisionAlgorithm**, align 4
  %sharedManifold.addr = alloca %class.btPersistentManifold*, align 4
  store %struct.btCompoundLeafCallback* %this, %struct.btCompoundLeafCallback** %this.addr, align 4
  store %struct.btCollisionObjectWrapper* %compoundObjWrap, %struct.btCollisionObjectWrapper** %compoundObjWrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %otherObjWrap, %struct.btCollisionObjectWrapper** %otherObjWrap.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4
  store %class.btCollisionAlgorithm** %childCollisionAlgorithms, %class.btCollisionAlgorithm*** %childCollisionAlgorithms.addr, align 4
  store %class.btPersistentManifold* %sharedManifold, %class.btPersistentManifold** %sharedManifold.addr, align 4
  %this1 = load %struct.btCompoundLeafCallback*, %struct.btCompoundLeafCallback** %this.addr, align 4
  %0 = bitcast %struct.btCompoundLeafCallback* %this1 to %"struct.btDbvt::ICollide"*
  %call = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideC2Ev(%"struct.btDbvt::ICollide"* %0) #7
  %1 = bitcast %struct.btCompoundLeafCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV22btCompoundLeafCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_compoundColObjWrap = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 1
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %compoundObjWrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %2, %struct.btCollisionObjectWrapper** %m_compoundColObjWrap, align 4
  %m_otherObjWrap = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 2
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %otherObjWrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %3, %struct.btCollisionObjectWrapper** %m_otherObjWrap, align 4
  %m_dispatcher = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 3
  %4 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  store %class.btDispatcher* %4, %class.btDispatcher** %m_dispatcher, align 4
  %m_dispatchInfo = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 4
  %5 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  store %struct.btDispatcherInfo* %5, %struct.btDispatcherInfo** %m_dispatchInfo, align 4
  %m_resultOut = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %6 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  store %class.btManifoldResult* %6, %class.btManifoldResult** %m_resultOut, align 4
  %m_childCollisionAlgorithms = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 6
  %7 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %childCollisionAlgorithms.addr, align 4
  store %class.btCollisionAlgorithm** %7, %class.btCollisionAlgorithm*** %m_childCollisionAlgorithms, align 4
  %m_sharedManifold = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 7
  %8 = load %class.btPersistentManifold*, %class.btPersistentManifold** %sharedManifold.addr, align 4
  store %class.btPersistentManifold* %8, %class.btPersistentManifold** %m_sharedManifold, align 4
  ret %struct.btCompoundLeafCallback* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.1* %this, i32 %newsize, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btPersistentManifold**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btPersistentManifold** %fillData, %class.btPersistentManifold*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.1* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %14 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %14, i32 %15
  %16 = bitcast %class.btPersistentManifold** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btPersistentManifold**
  %18 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %fillData.addr, align 4
  %19 = load %class.btPersistentManifold*, %class.btPersistentManifold** %18, align 4
  store %class.btPersistentManifold* %19, %class.btPersistentManifold** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.1* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.1* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %0, i32 %1
  ret %class.btPersistentManifold** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_cachedPoints, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %this, %class.btPersistentManifold* %manifoldPtr) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  store %class.btPersistentManifold* %manifoldPtr, %class.btPersistentManifold** %manifoldPtr.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifoldPtr.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  store %class.btPersistentManifold* %0, %class.btPersistentManifold** %m_manifoldPtr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %isSwapped = alloca i8, align 1
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4
  %call = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %if.end20

if.end:                                           ; preds = %entry
  %m_manifoldPtr2 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr2, align 4
  %call3 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %1)
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4
  %call4 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %2)
  %cmp = icmp ne %class.btCollisionObject* %call3, %call4
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %isSwapped, align 1
  %3 = load i8, i8* %isSwapped, align 1
  %tobool5 = trunc i8 %3 to i1
  br i1 %tobool5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.end
  %m_manifoldPtr7 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %4 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr7, align 4
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %5 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4
  %call8 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %5)
  %call9 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call8)
  %m_body0Wrap10 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap10, align 4
  %call11 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %6)
  %call12 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call11)
  call void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold* %4, %class.btTransform* nonnull align 4 dereferenceable(64) %call9, %class.btTransform* nonnull align 4 dereferenceable(64) %call12)
  br label %if.end20

if.else:                                          ; preds = %if.end
  %m_manifoldPtr13 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr13, align 4
  %m_body0Wrap14 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap14, align 4
  %call15 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %8)
  %call16 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call15)
  %m_body1Wrap17 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap17, align 4
  %call18 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %9)
  %call19 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call18)
  call void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold* %7, %class.btTransform* nonnull align 4 dereferenceable(64) %call16, %class.btTransform* nonnull align 4 dereferenceable(64) %call19)
  br label %if.end20

if.end20:                                         ; preds = %if.then, %if.else, %if.then6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform7inverseEv(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %inv = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %inv, %class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %mi, %class.btVector3* nonnull align 4 dereferenceable(16) %mx) #2 comdat {
entry:
  %mi.addr = alloca %class.btVector3*, align 4
  %mx.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %mi, %class.btVector3** %mi.addr, align 4
  store %class.btVector3* %mx, %class.btVector3** %mx.addr, align 4
  %call = call %struct.btDbvtAabbMm* @_ZN12btDbvtAabbMmC2Ev(%struct.btDbvtAabbMm* %agg.result)
  %0 = load %class.btVector3*, %class.btVector3** %mi.addr, align 4
  %mi1 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %agg.result, i32 0, i32 0
  %1 = bitcast %class.btVector3* %mi1 to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btVector3*, %class.btVector3** %mx.addr, align 4
  %mx2 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %agg.result, i32 0, i32 1
  %4 = bitcast %class.btVector3* %mx2 to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK6btDbvt21collideTVNoStackAllocEPK10btDbvtNodeRK12btDbvtAabbMmR20btAlignedObjectArrayIS2_ERNS_8ICollideE(%struct.btDbvt* %this, %struct.btDbvtNode* %root, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %vol, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %stack, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %policy) #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvt*, align 4
  %root.addr = alloca %struct.btDbvtNode*, align 4
  %vol.addr = alloca %struct.btDbvtAabbMm*, align 4
  %stack.addr = alloca %class.btAlignedObjectArray*, align 4
  %policy.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %volume = alloca %struct.btDbvtAabbMm, align 4
  %ref.tmp = alloca %struct.btDbvtNode*, align 4
  %n = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  store %struct.btDbvtNode* %root, %struct.btDbvtNode** %root.addr, align 4
  store %struct.btDbvtAabbMm* %vol, %struct.btDbvtAabbMm** %vol.addr, align 4
  store %class.btAlignedObjectArray* %stack, %class.btAlignedObjectArray** %stack.addr, align 4
  store %"struct.btDbvt::ICollide"* %policy, %"struct.btDbvt::ICollide"** %policy.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %tobool = icmp ne %struct.btDbvtNode* %0, null
  br i1 %tobool, label %if.then, label %if.end12

if.then:                                          ; preds = %entry
  %1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %vol.addr, align 4
  %2 = bitcast %struct.btDbvtAabbMm* %volume to i8*
  %3 = bitcast %struct.btDbvtAabbMm* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 32, i1 false)
  %4 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %stack.addr, align 4
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE6resizeEiRKS2_(%class.btAlignedObjectArray* %4, i32 0, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %ref.tmp)
  %5 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %stack.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray* %5, i32 64)
  %6 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %stack.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_(%class.btAlignedObjectArray* %6, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %root.addr)
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then
  %7 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %stack.addr, align 4
  %8 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %stack.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray* %8)
  %sub = sub nsw i32 %call, 1
  %call2 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEixEi(%class.btAlignedObjectArray* %7, i32 %sub)
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %call2, align 4
  store %struct.btDbvtNode* %9, %struct.btDbvtNode** %n, align 4
  %10 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %stack.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8pop_backEv(%class.btAlignedObjectArray* %10)
  %11 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %volume3 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %11, i32 0, i32 0
  %call4 = call zeroext i1 @_Z9IntersectRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume3, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume)
  br i1 %call4, label %if.then5, label %if.end10

if.then5:                                         ; preds = %do.body
  %12 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %call6 = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %12)
  br i1 %call6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.then5
  %13 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %stack.addr, align 4
  %14 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %15 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %14, i32 0, i32 2
  %childs = bitcast %union.anon.0* %15 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 0
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_(%class.btAlignedObjectArray* %13, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %arrayidx)
  %16 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %stack.addr, align 4
  %17 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %18 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %17, i32 0, i32 2
  %childs8 = bitcast %union.anon.0* %18 to [2 x %struct.btDbvtNode*]*
  %arrayidx9 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs8, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_(%class.btAlignedObjectArray* %16, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %arrayidx9)
  br label %if.end

if.else:                                          ; preds = %if.then5
  %19 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %policy.addr, align 4
  %20 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %21 = bitcast %"struct.btDbvt::ICollide"* %19 to void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)***
  %vtable = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)**, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*** %21, align 4
  %vfn = getelementptr inbounds void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vtable, i64 3
  %22 = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vfn, align 4
  call void %22(%"struct.btDbvt::ICollide"* %19, %struct.btDbvtNode* %20)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then7
  br label %if.end10

if.end10:                                         ; preds = %if.end, %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end10
  %23 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %stack.addr, align 4
  %call11 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray* %23)
  %cmp = icmp sgt i32 %call11, 0
  br i1 %cmp, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  br label %if.end12

if.end12:                                         ; preds = %do.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN22btCompoundLeafCallback17ProcessChildShapeEPK16btCollisionShapei(%struct.btCompoundLeafCallback* %this, %class.btCollisionShape* %childShape, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %struct.btCompoundLeafCallback*, align 4
  %childShape.addr = alloca %class.btCollisionShape*, align 4
  %index.addr = alloca i32, align 4
  %compoundShape = alloca %class.btCompoundShape*, align 4
  %orgTrans = alloca %class.btTransform, align 4
  %childTrans = alloca %class.btTransform*, align 4
  %newChildWorldTrans = alloca %class.btTransform, align 4
  %aabbMin0 = alloca %class.btVector3, align 4
  %aabbMax0 = alloca %class.btVector3, align 4
  %extendAabb = alloca %class.btVector3, align 4
  %aabbMin1 = alloca %class.btVector3, align 4
  %aabbMax1 = alloca %class.btVector3, align 4
  %compoundWrap = alloca %struct.btCollisionObjectWrapper, align 4
  %algo = alloca %class.btCollisionAlgorithm*, align 4
  %tmpWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCompoundLeafCallback* %this, %struct.btCompoundLeafCallback** %this.addr, align 4
  store %class.btCollisionShape* %childShape, %class.btCollisionShape** %childShape.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %struct.btCompoundLeafCallback*, %struct.btCompoundLeafCallback** %this.addr, align 4
  %m_compoundColObjWrap = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 1
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compoundColObjWrap, align 4
  %call = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %0)
  %1 = bitcast %class.btCollisionShape* %call to %class.btCompoundShape*
  store %class.btCompoundShape* %1, %class.btCompoundShape** %compoundShape, align 4
  %m_compoundColObjWrap2 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 1
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compoundColObjWrap2, align 4
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %2)
  %call4 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %orgTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %call3)
  %3 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4
  %4 = load i32, i32* %index.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %3, i32 %4)
  store %class.btTransform* %call5, %class.btTransform** %childTrans, align 4
  %5 = load %class.btTransform*, %class.btTransform** %childTrans, align 4
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %newChildWorldTrans, %class.btTransform* %orgTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %5)
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin0)
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax0)
  %6 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape.addr, align 4
  %7 = bitcast %class.btCollisionShape* %6 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %7, align 4
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %8 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %8(%class.btCollisionShape* %6, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildWorldTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax0)
  %m_resultOut = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %9 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut, align 4
  %m_closestPointDistanceThreshold = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %9, i32 0, i32 8
  %m_resultOut8 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %10 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut8, align 4
  %m_closestPointDistanceThreshold9 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %10, i32 0, i32 8
  %m_resultOut10 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %11 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut10, align 4
  %m_closestPointDistanceThreshold11 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %11, i32 0, i32 8
  %call12 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %extendAabb, float* nonnull align 4 dereferenceable(4) %m_closestPointDistanceThreshold, float* nonnull align 4 dereferenceable(4) %m_closestPointDistanceThreshold9, float* nonnull align 4 dereferenceable(4) %m_closestPointDistanceThreshold11)
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %aabbMin0, %class.btVector3* nonnull align 4 dereferenceable(16) %extendAabb)
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %aabbMax0, %class.btVector3* nonnull align 4 dereferenceable(16) %extendAabb)
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin1)
  %call16 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax1)
  %m_otherObjWrap = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 2
  %12 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_otherObjWrap, align 4
  %call17 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %12)
  %m_otherObjWrap18 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 2
  %13 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_otherObjWrap18, align 4
  %call19 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %13)
  %14 = bitcast %class.btCollisionShape* %call17 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable20 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %14, align 4
  %vfn21 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable20, i64 2
  %15 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn21, align 4
  call void %15(%class.btCollisionShape* %call17, %class.btTransform* nonnull align 4 dereferenceable(64) %call19, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1)
  %16 = load i1 (%class.btCollisionShape*, %class.btCollisionShape*)*, i1 (%class.btCollisionShape*, %class.btCollisionShape*)** @gCompoundChildShapePairCallback, align 4
  %tobool = icmp ne i1 (%class.btCollisionShape*, %class.btCollisionShape*)* %16, null
  br i1 %tobool, label %if.then, label %if.end26

if.then:                                          ; preds = %entry
  %17 = load i1 (%class.btCollisionShape*, %class.btCollisionShape*)*, i1 (%class.btCollisionShape*, %class.btCollisionShape*)** @gCompoundChildShapePairCallback, align 4
  %m_otherObjWrap22 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 2
  %18 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_otherObjWrap22, align 4
  %call23 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %18)
  %19 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape.addr, align 4
  %call24 = call zeroext i1 %17(%class.btCollisionShape* %call23, %class.btCollisionShape* %19)
  br i1 %call24, label %if.end, label %if.then25

if.then25:                                        ; preds = %if.then
  br label %if.end87

if.end:                                           ; preds = %if.then
  br label %if.end26

if.end26:                                         ; preds = %if.end, %entry
  %call27 = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1)
  br i1 %call27, label %if.then28, label %if.end87

if.then28:                                        ; preds = %if.end26
  %m_compoundColObjWrap29 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 1
  %20 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compoundColObjWrap29, align 4
  %21 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape.addr, align 4
  %m_compoundColObjWrap30 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 1
  %22 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compoundColObjWrap30, align 4
  %call31 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %22)
  %23 = load i32, i32* %index.addr, align 4
  %call32 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %compoundWrap, %struct.btCollisionObjectWrapper* %20, %class.btCollisionShape* %21, %class.btCollisionObject* %call31, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildWorldTrans, i32 -1, i32 %23)
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %algo, align 4
  %m_resultOut33 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %24 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut33, align 4
  %m_closestPointDistanceThreshold34 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %24, i32 0, i32 8
  %25 = load float, float* %m_closestPointDistanceThreshold34, align 4
  %cmp = fcmp ogt float %25, 0.000000e+00
  br i1 %cmp, label %if.then35, label %if.else

if.then35:                                        ; preds = %if.then28
  %m_dispatcher = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 3
  %26 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %m_otherObjWrap36 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 2
  %27 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_otherObjWrap36, align 4
  %28 = bitcast %class.btDispatcher* %26 to %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)***
  %vtable37 = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)**, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)*** %28, align 4
  %vfn38 = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)** %vtable37, i64 2
  %29 = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)** %vfn38, align 4
  %call39 = call %class.btCollisionAlgorithm* %29(%class.btDispatcher* %26, %struct.btCollisionObjectWrapper* %compoundWrap, %struct.btCollisionObjectWrapper* %27, %class.btPersistentManifold* null, i32 2)
  store %class.btCollisionAlgorithm* %call39, %class.btCollisionAlgorithm** %algo, align 4
  br label %if.end52

if.else:                                          ; preds = %if.then28
  %m_childCollisionAlgorithms = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 6
  %30 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_childCollisionAlgorithms, align 4
  %31 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %30, i32 %31
  %32 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %arrayidx, align 4
  %tobool40 = icmp ne %class.btCollisionAlgorithm* %32, null
  br i1 %tobool40, label %if.end49, label %if.then41

if.then41:                                        ; preds = %if.else
  %m_dispatcher42 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 3
  %33 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher42, align 4
  %m_otherObjWrap43 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 2
  %34 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_otherObjWrap43, align 4
  %m_sharedManifold = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 7
  %35 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_sharedManifold, align 4
  %36 = bitcast %class.btDispatcher* %33 to %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)***
  %vtable44 = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)**, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)*** %36, align 4
  %vfn45 = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)** %vtable44, i64 2
  %37 = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)** %vfn45, align 4
  %call46 = call %class.btCollisionAlgorithm* %37(%class.btDispatcher* %33, %struct.btCollisionObjectWrapper* %compoundWrap, %struct.btCollisionObjectWrapper* %34, %class.btPersistentManifold* %35, i32 1)
  %m_childCollisionAlgorithms47 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 6
  %38 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_childCollisionAlgorithms47, align 4
  %39 = load i32, i32* %index.addr, align 4
  %arrayidx48 = getelementptr inbounds %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %38, i32 %39
  store %class.btCollisionAlgorithm* %call46, %class.btCollisionAlgorithm** %arrayidx48, align 4
  br label %if.end49

if.end49:                                         ; preds = %if.then41, %if.else
  %m_childCollisionAlgorithms50 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 6
  %40 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_childCollisionAlgorithms50, align 4
  %41 = load i32, i32* %index.addr, align 4
  %arrayidx51 = getelementptr inbounds %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %40, i32 %41
  %42 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %arrayidx51, align 4
  store %class.btCollisionAlgorithm* %42, %class.btCollisionAlgorithm** %algo, align 4
  br label %if.end52

if.end52:                                         ; preds = %if.end49, %if.then35
  store %struct.btCollisionObjectWrapper* null, %struct.btCollisionObjectWrapper** %tmpWrap, align 4
  %m_resultOut53 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %43 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut53, align 4
  %call54 = call %class.btCollisionObject* @_ZNK16btManifoldResult16getBody0InternalEv(%class.btManifoldResult* %43)
  %m_compoundColObjWrap55 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 1
  %44 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compoundColObjWrap55, align 4
  %call56 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %44)
  %cmp57 = icmp eq %class.btCollisionObject* %call54, %call56
  br i1 %cmp57, label %if.then58, label %if.else65

if.then58:                                        ; preds = %if.end52
  %m_resultOut59 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %45 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut59, align 4
  %call60 = call %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody0WrapEv(%class.btManifoldResult* %45)
  store %struct.btCollisionObjectWrapper* %call60, %struct.btCollisionObjectWrapper** %tmpWrap, align 4
  %m_resultOut61 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %46 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut61, align 4
  call void @_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %46, %struct.btCollisionObjectWrapper* %compoundWrap)
  %m_resultOut62 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %47 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut62, align 4
  %48 = load i32, i32* %index.addr, align 4
  %49 = bitcast %class.btManifoldResult* %47 to void (%class.btManifoldResult*, i32, i32)***
  %vtable63 = load void (%class.btManifoldResult*, i32, i32)**, void (%class.btManifoldResult*, i32, i32)*** %49, align 4
  %vfn64 = getelementptr inbounds void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vtable63, i64 2
  %50 = load void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vfn64, align 4
  call void %50(%class.btManifoldResult* %47, i32 -1, i32 %48)
  br label %if.end72

if.else65:                                        ; preds = %if.end52
  %m_resultOut66 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %51 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut66, align 4
  %call67 = call %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody1WrapEv(%class.btManifoldResult* %51)
  store %struct.btCollisionObjectWrapper* %call67, %struct.btCollisionObjectWrapper** %tmpWrap, align 4
  %m_resultOut68 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %52 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut68, align 4
  call void @_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %52, %struct.btCollisionObjectWrapper* %compoundWrap)
  %m_resultOut69 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %53 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut69, align 4
  %54 = load i32, i32* %index.addr, align 4
  %55 = bitcast %class.btManifoldResult* %53 to void (%class.btManifoldResult*, i32, i32)***
  %vtable70 = load void (%class.btManifoldResult*, i32, i32)**, void (%class.btManifoldResult*, i32, i32)*** %55, align 4
  %vfn71 = getelementptr inbounds void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vtable70, i64 3
  %56 = load void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vfn71, align 4
  call void %56(%class.btManifoldResult* %53, i32 -1, i32 %54)
  br label %if.end72

if.end72:                                         ; preds = %if.else65, %if.then58
  %57 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %algo, align 4
  %m_otherObjWrap73 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 2
  %58 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_otherObjWrap73, align 4
  %m_dispatchInfo = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 4
  %59 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %m_dispatchInfo, align 4
  %m_resultOut74 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %60 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut74, align 4
  %61 = bitcast %class.btCollisionAlgorithm* %57 to void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)***
  %vtable75 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)**, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*** %61, align 4
  %vfn76 = getelementptr inbounds void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vtable75, i64 2
  %62 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vfn76, align 4
  call void %62(%class.btCollisionAlgorithm* %57, %struct.btCollisionObjectWrapper* %compoundWrap, %struct.btCollisionObjectWrapper* %58, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %59, %class.btManifoldResult* %60)
  %m_resultOut77 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %63 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut77, align 4
  %call78 = call %class.btCollisionObject* @_ZNK16btManifoldResult16getBody0InternalEv(%class.btManifoldResult* %63)
  %m_compoundColObjWrap79 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 1
  %64 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compoundColObjWrap79, align 4
  %call80 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %64)
  %cmp81 = icmp eq %class.btCollisionObject* %call78, %call80
  br i1 %cmp81, label %if.then82, label %if.else84

if.then82:                                        ; preds = %if.end72
  %m_resultOut83 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %65 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut83, align 4
  %66 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %tmpWrap, align 4
  call void @_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %65, %struct.btCollisionObjectWrapper* %66)
  br label %if.end86

if.else84:                                        ; preds = %if.end72
  %m_resultOut85 = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 5
  %67 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut85, align 4
  %68 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %tmpWrap, align 4
  call void @_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %67, %struct.btCollisionObjectWrapper* %68)
  br label %if.end86

if.end86:                                         ; preds = %if.else84, %if.then82
  br label %if.end87

if.end87:                                         ; preds = %if.then25, %if.end86, %if.end26
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %this, i32 %index) #1 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray.15* %m_children, i32 %0)
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call, i32 0, i32 0
  ret %class.btTransform* %m_transform
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin2, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax2) #2 comdat {
entry:
  %aabbMin1.addr = alloca %class.btVector3*, align 4
  %aabbMax1.addr = alloca %class.btVector3*, align 4
  %aabbMin2.addr = alloca %class.btVector3*, align 4
  %aabbMax2.addr = alloca %class.btVector3*, align 4
  %overlap = alloca i8, align 1
  store %class.btVector3* %aabbMin1, %class.btVector3** %aabbMin1.addr, align 4
  store %class.btVector3* %aabbMax1, %class.btVector3** %aabbMax1.addr, align 4
  store %class.btVector3* %aabbMin2, %class.btVector3** %aabbMin2.addr, align 4
  store %class.btVector3* %aabbMax2, %class.btVector3** %aabbMax2.addr, align 4
  store i8 1, i8* %overlap, align 1
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %2)
  %3 = load float, float* %call1, align 4
  %cmp = fcmp ogt float %1, %3
  br i1 %cmp, label %cond.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %4)
  %5 = load float, float* %call2, align 4
  %6 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %6)
  %7 = load float, float* %call3, align 4
  %cmp4 = fcmp olt float %5, %7
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %lor.lhs.false, %entry
  br label %cond.end

cond.false:                                       ; preds = %lor.lhs.false
  %8 = load i8, i8* %overlap, align 1
  %tobool = trunc i8 %8 to i1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i1 [ false, %cond.true ], [ %tobool, %cond.false ]
  %frombool = zext i1 %cond to i8
  store i8 %frombool, i8* %overlap, align 1
  %9 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %9)
  %10 = load float, float* %call5, align 4
  %11 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %11)
  %12 = load float, float* %call6, align 4
  %cmp7 = fcmp ogt float %10, %12
  br i1 %cmp7, label %cond.true12, label %lor.lhs.false8

lor.lhs.false8:                                   ; preds = %cond.end
  %13 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %13)
  %14 = load float, float* %call9, align 4
  %15 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %15)
  %16 = load float, float* %call10, align 4
  %cmp11 = fcmp olt float %14, %16
  br i1 %cmp11, label %cond.true12, label %cond.false13

cond.true12:                                      ; preds = %lor.lhs.false8, %cond.end
  br label %cond.end15

cond.false13:                                     ; preds = %lor.lhs.false8
  %17 = load i8, i8* %overlap, align 1
  %tobool14 = trunc i8 %17 to i1
  br label %cond.end15

cond.end15:                                       ; preds = %cond.false13, %cond.true12
  %cond16 = phi i1 [ false, %cond.true12 ], [ %tobool14, %cond.false13 ]
  %frombool17 = zext i1 %cond16 to i8
  store i8 %frombool17, i8* %overlap, align 1
  %18 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %18)
  %19 = load float, float* %call18, align 4
  %20 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %20)
  %21 = load float, float* %call19, align 4
  %cmp20 = fcmp ogt float %19, %21
  br i1 %cmp20, label %cond.true25, label %lor.lhs.false21

lor.lhs.false21:                                  ; preds = %cond.end15
  %22 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %22)
  %23 = load float, float* %call22, align 4
  %24 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %24)
  %25 = load float, float* %call23, align 4
  %cmp24 = fcmp olt float %23, %25
  br i1 %cmp24, label %cond.true25, label %cond.false26

cond.true25:                                      ; preds = %lor.lhs.false21, %cond.end15
  br label %cond.end28

cond.false26:                                     ; preds = %lor.lhs.false21
  %26 = load i8, i8* %overlap, align 1
  %tobool27 = trunc i8 %26 to i1
  br label %cond.end28

cond.end28:                                       ; preds = %cond.false26, %cond.true25
  %cond29 = phi i1 [ false, %cond.true25 ], [ %tobool27, %cond.false26 ]
  %frombool30 = zext i1 %cond29 to i8
  store i8 %frombool30, i8* %overlap, align 1
  %27 = load i8, i8* %overlap, align 1
  %tobool31 = trunc i8 %27 to i1
  ret i1 %tobool31
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btCompoundLeafCallback* @_ZN22btCompoundLeafCallbackD2Ev(%struct.btCompoundLeafCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCompoundLeafCallback*, align 4
  store %struct.btCompoundLeafCallback* %this, %struct.btCompoundLeafCallback** %this.addr, align 4
  %this1 = load %struct.btCompoundLeafCallback*, %struct.btCompoundLeafCallback** %this.addr, align 4
  %0 = bitcast %struct.btCompoundLeafCallback* %this1 to %"struct.btDbvt::ICollide"*
  %call = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideD2Ev(%"struct.btDbvt::ICollide"* %0) #7
  ret %struct.btCompoundLeafCallback* %this1
}

; Function Attrs: noinline optnone
define hidden float @_ZN28btCompoundCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult(%class.btCompoundCollisionAlgorithm* %this, %class.btCollisionObject* %body0, %class.btCollisionObject* %body1, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCompoundCollisionAlgorithm*, align 4
  %body0.addr = alloca %class.btCollisionObject*, align 4
  %body1.addr = alloca %class.btCollisionObject*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %colObj = alloca %class.btCollisionObject*, align 4
  %otherObj = alloca %class.btCollisionObject*, align 4
  %compoundShape = alloca %class.btCompoundShape*, align 4
  %hitFraction = alloca float, align 4
  %numChildren = alloca i32, align 4
  %i = alloca i32, align 4
  %orgTrans = alloca %class.btTransform, align 4
  %frac = alloca float, align 4
  %childTrans = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btTransform, align 4
  store %class.btCompoundCollisionAlgorithm* %this, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  store %class.btCollisionObject* %body0, %class.btCollisionObject** %body0.addr, align 4
  store %class.btCollisionObject* %body1, %class.btCollisionObject** %body1.addr, align 4
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4
  %this1 = load %class.btCompoundCollisionAlgorithm*, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  %m_isSwapped = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 4
  %0 = load i8, i8* %m_isSwapped, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btCollisionObject* [ %1, %cond.true ], [ %2, %cond.false ]
  store %class.btCollisionObject* %cond, %class.btCollisionObject** %colObj, align 4
  %m_isSwapped2 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 4
  %3 = load i8, i8* %m_isSwapped2, align 4
  %tobool3 = trunc i8 %3 to i1
  br i1 %tobool3, label %cond.true4, label %cond.false5

cond.true4:                                       ; preds = %cond.end
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4
  br label %cond.end6

cond.false5:                                      ; preds = %cond.end
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4
  br label %cond.end6

cond.end6:                                        ; preds = %cond.false5, %cond.true4
  %cond7 = phi %class.btCollisionObject* [ %4, %cond.true4 ], [ %5, %cond.false5 ]
  store %class.btCollisionObject* %cond7, %class.btCollisionObject** %otherObj, align 4
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4
  %call = call %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %6)
  %7 = bitcast %class.btCollisionShape* %call to %class.btCompoundShape*
  store %class.btCompoundShape* %7, %class.btCompoundShape** %compoundShape, align 4
  store float 1.000000e+00, float* %hitFraction, align 4
  %m_childCollisionAlgorithms = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %call8 = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms)
  store i32 %call8, i32* %numChildren, align 4
  %call9 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %orgTrans)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end6
  %8 = load i32, i32* %i, align 4
  %9 = load i32, i32* %numChildren, align 4
  %cmp = icmp slt i32 %8, %9
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4
  %call10 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %10)
  %call11 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %orgTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %call10)
  %11 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4
  %12 = load i32, i32* %i, align 4
  %call12 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %11, i32 %12)
  store %class.btTransform* %call12, %class.btTransform** %childTrans, align 4
  %13 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4
  %14 = load %class.btTransform*, %class.btTransform** %childTrans, align 4
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp, %class.btTransform* %orgTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %14)
  call void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %13, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp)
  %m_childCollisionAlgorithms13 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %15 = load i32, i32* %i, align 4
  %call14 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms13, i32 %15)
  %16 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call14, align 4
  %17 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4
  %18 = load %class.btCollisionObject*, %class.btCollisionObject** %otherObj, align 4
  %19 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  %20 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %21 = bitcast %class.btCollisionAlgorithm* %16 to float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)***
  %vtable = load float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)**, float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*** %21, align 4
  %vfn = getelementptr inbounds float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vtable, i64 3
  %22 = load float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vfn, align 4
  %call15 = call float %22(%class.btCollisionAlgorithm* %16, %class.btCollisionObject* %17, %class.btCollisionObject* %18, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %19, %class.btManifoldResult* %20)
  store float %call15, float* %frac, align 4
  %23 = load float, float* %frac, align 4
  %24 = load float, float* %hitFraction, align 4
  %cmp16 = fcmp olt float %23, %24
  br i1 %cmp16, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %25 = load float, float* %frac, align 4
  store float %25, float* %hitFraction, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %26 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4
  call void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %26, %class.btTransform* nonnull align 4 dereferenceable(64) %orgTrans)
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %27 = load i32, i32* %i, align 4
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %28 = load float, float* %hitFraction, align 4
  ret float %28
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4
  ret %class.btCollisionShape* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray.15* %m_children, i32 %0)
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call, i32 0, i32 0
  ret %class.btTransform* %m_transform
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %worldTrans) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %worldTrans.addr = alloca %class.btTransform*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store %class.btTransform* %worldTrans, %class.btTransform** %worldTrans.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 33
  %0 = load i32, i32* %m_updateRevision, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_updateRevision, align 4
  %1 = load %class.btTransform*, %class.btTransform** %worldTrans.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_worldTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN28btCompoundCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE(%class.btCompoundCollisionAlgorithm* %this, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %manifoldArray) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCompoundCollisionAlgorithm*, align 4
  %manifoldArray.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %i = alloca i32, align 4
  store %class.btCompoundCollisionAlgorithm* %this, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  store %class.btAlignedObjectArray.1* %manifoldArray, %class.btAlignedObjectArray.1** %manifoldArray.addr, align 4
  %this1 = load %class.btCompoundCollisionAlgorithm*, %class.btCompoundCollisionAlgorithm** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_childCollisionAlgorithms = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_childCollisionAlgorithms2 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %1 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms2, i32 %1)
  %2 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call3, align 4
  %tobool = icmp ne %class.btCollisionAlgorithm* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %m_childCollisionAlgorithms4 = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %3 = load i32, i32* %i, align 4
  %call5 = call nonnull align 4 dereferenceable(4) %class.btCollisionAlgorithm** @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmEixEi(%class.btAlignedObjectArray.11* %m_childCollisionAlgorithms4, i32 %3)
  %4 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %call5, align 4
  %5 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %manifoldArray.addr, align 4
  %6 = bitcast %class.btCollisionAlgorithm* %4 to void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)***
  %vtable = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)**, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)*** %6, align 4
  %vfn = getelementptr inbounds void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)** %vtable, i64 4
  %7 = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)** %vfn, align 4
  call void %7(%class.btCollisionAlgorithm* %4, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %5)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray.15* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray.15* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %0 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %0, i32 %1
  ret %struct.btCompoundShapeChild* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideC2Ev(%"struct.btDbvt::ICollide"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %0 = bitcast %"struct.btDbvt::ICollide"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTVN6btDbvt8ICollideE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %"struct.btDbvt::ICollide"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN22btCompoundLeafCallbackD0Ev(%struct.btCompoundLeafCallback* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCompoundLeafCallback*, align 4
  store %struct.btCompoundLeafCallback* %this, %struct.btCompoundLeafCallback** %this.addr, align 4
  %this1 = load %struct.btCompoundLeafCallback*, %struct.btCompoundLeafCallback** %this.addr, align 4
  %call = call %struct.btCompoundLeafCallback* @_ZN22btCompoundLeafCallbackD2Ev(%struct.btCompoundLeafCallback* %this1) #7
  %0 = bitcast %struct.btCompoundLeafCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0, %struct.btDbvtNode* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  %.addr1 = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4
  store %struct.btDbvtNode* %1, %struct.btDbvtNode** %.addr1, align 4
  %this2 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN22btCompoundLeafCallback7ProcessEPK10btDbvtNode(%struct.btCompoundLeafCallback* %this, %struct.btDbvtNode* %leaf) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btCompoundLeafCallback*, align 4
  %leaf.addr = alloca %struct.btDbvtNode*, align 4
  %index = alloca i32, align 4
  %compoundShape = alloca %class.btCompoundShape*, align 4
  %childShape = alloca %class.btCollisionShape*, align 4
  store %struct.btCompoundLeafCallback* %this, %struct.btCompoundLeafCallback** %this.addr, align 4
  store %struct.btDbvtNode* %leaf, %struct.btDbvtNode** %leaf.addr, align 4
  %this1 = load %struct.btCompoundLeafCallback*, %struct.btCompoundLeafCallback** %this.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %1 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %0, i32 0, i32 2
  %dataAsInt = bitcast %union.anon.0* %1 to i32*
  %2 = load i32, i32* %dataAsInt, align 4
  store i32 %2, i32* %index, align 4
  %m_compoundColObjWrap = getelementptr inbounds %struct.btCompoundLeafCallback, %struct.btCompoundLeafCallback* %this1, i32 0, i32 1
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compoundColObjWrap, align 4
  %call = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %3)
  %4 = bitcast %class.btCollisionShape* %call to %class.btCompoundShape*
  store %class.btCompoundShape* %4, %class.btCompoundShape** %compoundShape, align 4
  %5 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape, align 4
  %6 = load i32, i32* %index, align 4
  %call2 = call %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %5, i32 %6)
  store %class.btCollisionShape* %call2, %class.btCollisionShape** %childShape, align 4
  %7 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape, align 4
  %8 = load i32, i32* %index, align 4
  call void @_ZN22btCompoundLeafCallback17ProcessChildShapeEPK16btCollisionShapei(%struct.btCompoundLeafCallback* %this1, %class.btCollisionShape* %7, i32 %8)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %n, float %0) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %n.addr = alloca %struct.btDbvtNode*, align 4
  %.addr = alloca float, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %n, %struct.btDbvtNode** %n.addr, align 4
  store float %0, float* %.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %2 = bitcast %"struct.btDbvt::ICollide"* %this1 to void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)***
  %vtable = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)**, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*** %2, align 4
  %vfn = getelementptr inbounds void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vtable, i64 3
  %3 = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vfn, align 4
  call void %3(%"struct.btDbvt::ICollide"* %this1, %struct.btDbvtNode* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideD2Ev(%"struct.btDbvt::ICollide"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret %"struct.btDbvt::ICollide"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN6btDbvt8ICollideD0Ev(%"struct.btDbvt::ICollide"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %call = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideD2Ev(%"struct.btDbvt::ICollide"* %this1) #7
  %0 = bitcast %"struct.btDbvt::ICollide"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4
  ret %class.btCollisionObject* %0
}

declare void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64)) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #6

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %10, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btDbvtAabbMm* @_ZN12btDbvtAabbMmC2Ev(%struct.btDbvtAabbMm* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mi)
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mx)
  ret %struct.btDbvtAabbMm* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE6resizeEiRKS2_(%class.btAlignedObjectArray* %this, i32 %newsize, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btDbvtNode**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %struct.btDbvtNode** %fillData, %struct.btDbvtNode*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %14 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %14, i32 %15
  %16 = bitcast %struct.btDbvtNode** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %struct.btDbvtNode**
  %18 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %fillData.addr, align 4
  %19 = load %struct.btDbvtNode*, %struct.btDbvtNode** %18, align 4
  store %struct.btDbvtNode* %19, %struct.btDbvtNode** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btDbvtNode**, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btDbvtNode**
  store %struct.btDbvtNode** %2, %struct.btDbvtNode*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btDbvtNode** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btDbvtNode** %4, %struct.btDbvtNode*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_(%class.btAlignedObjectArray* %this, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %struct.btDbvtNode**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %struct.btDbvtNode** %_Val, %struct.btDbvtNode*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %1, i32 %2
  %3 = bitcast %struct.btDbvtNode** %arrayidx to i8*
  %4 = bitcast i8* %3 to %struct.btDbvtNode**
  %5 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %_Val.addr, align 4
  %6 = load %struct.btDbvtNode*, %struct.btDbvtNode** %5, align 4
  store %struct.btDbvtNode* %6, %struct.btDbvtNode** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %0, i32 %1
  ret %struct.btDbvtNode** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8pop_backEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %1, i32 %2
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_Z9IntersectRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %a, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %b) #1 comdat {
entry:
  %a.addr = alloca %struct.btDbvtAabbMm*, align 4
  %b.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %a, %struct.btDbvtAabbMm** %a.addr, align 4
  store %struct.btDbvtAabbMm* %b, %struct.btDbvtAabbMm** %b.addr, align 4
  %0 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %0, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mi)
  %1 = load float, float* %call, align 4
  %2 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %2, i32 0, i32 1
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mx)
  %3 = load float, float* %call1, align 4
  %cmp = fcmp ole float %1, %3
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %4 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx2 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %4, i32 0, i32 1
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mx2)
  %5 = load float, float* %call3, align 4
  %6 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mi4 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %6, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mi4)
  %7 = load float, float* %call5, align 4
  %cmp6 = fcmp oge float %5, %7
  br i1 %cmp6, label %land.lhs.true7, label %land.end

land.lhs.true7:                                   ; preds = %land.lhs.true
  %8 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi8 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %8, i32 0, i32 0
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mi8)
  %9 = load float, float* %call9, align 4
  %10 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mx10 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %10, i32 0, i32 1
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mx10)
  %11 = load float, float* %call11, align 4
  %cmp12 = fcmp ole float %9, %11
  br i1 %cmp12, label %land.lhs.true13, label %land.end

land.lhs.true13:                                  ; preds = %land.lhs.true7
  %12 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx14 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %12, i32 0, i32 1
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mx14)
  %13 = load float, float* %call15, align 4
  %14 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mi16 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %14, i32 0, i32 0
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mi16)
  %15 = load float, float* %call17, align 4
  %cmp18 = fcmp oge float %13, %15
  br i1 %cmp18, label %land.lhs.true19, label %land.end

land.lhs.true19:                                  ; preds = %land.lhs.true13
  %16 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi20 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %16, i32 0, i32 0
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mi20)
  %17 = load float, float* %call21, align 4
  %18 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mx22 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %18, i32 0, i32 1
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mx22)
  %19 = load float, float* %call23, align 4
  %cmp24 = fcmp ole float %17, %19
  br i1 %cmp24, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true19
  %20 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx25 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %20, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mx25)
  %21 = load float, float* %call26, align 4
  %22 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mi27 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %22, i32 0, i32 0
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mi27)
  %23 = load float, float* %call28, align 4
  %cmp29 = fcmp oge float %21, %23
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true19, %land.lhs.true13, %land.lhs.true7, %land.lhs.true, %entry
  %24 = phi i1 [ false, %land.lhs.true19 ], [ false, %land.lhs.true13 ], [ false, %land.lhs.true7 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp29, %land.rhs ]
  ret i1 %24
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %this, %struct.btDbvtNode** %this.addr, align 4
  %this1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %this.addr, align 4
  %call = call zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %this1)
  %lnot = xor i1 %call, true
  ret i1 %lnot
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btDbvtNode** @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE8allocateEiPPKS2_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btDbvtNode*** null)
  %2 = bitcast %struct.btDbvtNode** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btDbvtNode** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btDbvtNode**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btDbvtNode** %dest, %struct.btDbvtNode*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %3, i32 %4
  %5 = bitcast %struct.btDbvtNode** %arrayidx to i8*
  %6 = bitcast i8* %5 to %struct.btDbvtNode**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %7, i32 %8
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx2, align 4
  store %struct.btDbvtNode* %9, %struct.btDbvtNode** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %tobool = icmp ne %struct.btDbvtNode** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE10deallocateEPS2_(%class.btAlignedAllocator* %m_allocator, %struct.btDbvtNode** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btDbvtNode** null, %struct.btDbvtNode*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btDbvtNode** @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE8allocateEiPPKS2_(%class.btAlignedAllocator* %this, i32 %n, %struct.btDbvtNode*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btDbvtNode***, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btDbvtNode*** %hint, %struct.btDbvtNode**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btDbvtNode**
  ret %struct.btDbvtNode** %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE10deallocateEPS2_(%class.btAlignedAllocator* %this, %struct.btDbvtNode** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btDbvtNode**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %struct.btDbvtNode** %ptr, %struct.btDbvtNode*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %ptr.addr, align 4
  %1 = bitcast %struct.btDbvtNode** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %this, %struct.btDbvtNode** %this.addr, align 4
  %this1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %this1, i32 0, i32 2
  %childs = bitcast %union.anon.0* %0 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 1
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  %cmp = icmp eq %struct.btDbvtNode* %1, null
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK16btManifoldResult16getBody0InternalEv(%class.btManifoldResult* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4
  %call = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %0)
  ret %class.btCollisionObject* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody0WrapEv(%class.btManifoldResult* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4
  ret %struct.btCollisionObjectWrapper* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %this, %struct.btCollisionObjectWrapper* %obj0Wrap) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %obj0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  store %struct.btCollisionObjectWrapper* %obj0Wrap, %struct.btCollisionObjectWrapper** %obj0Wrap.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %obj0Wrap.addr, align 4
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody1WrapEv(%class.btManifoldResult* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4
  ret %struct.btCollisionObjectWrapper* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %this, %struct.btCollisionObjectWrapper* %obj1Wrap) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %obj1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  store %struct.btCollisionObjectWrapper* %obj1Wrap, %struct.btCollisionObjectWrapper** %obj1Wrap.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %obj1Wrap.addr, align 4
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray.15* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %0 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %0, i32 %1
  ret %struct.btCompoundShapeChild* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btDbvtNode** null, %struct.btDbvtNode*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.2* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.2* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.2*, align 4
  store %class.btAlignedAllocator.2* %this, %class.btAlignedAllocator.2** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.2*, %class.btAlignedAllocator.2** %this.addr, align 4
  ret %class.btAlignedAllocator.2* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.1* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.12* @_ZN18btAlignedAllocatorIP20btCollisionAlgorithmLj16EEC2Ev(%class.btAlignedAllocator.12* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.12*, align 4
  store %class.btAlignedAllocator.12* %this, %class.btAlignedAllocator.12** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.12*, %class.btAlignedAllocator.12** %this.addr, align 4
  ret %class.btAlignedAllocator.12* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE4initEv(%class.btAlignedObjectArray.11* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.11*, align 4
  store %class.btAlignedObjectArray.11* %this, %class.btAlignedObjectArray.11** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.11*, %class.btAlignedObjectArray.11** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 4
  store %class.btCollisionAlgorithm** null, %class.btCollisionAlgorithm*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.1* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.1* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.1* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.1* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.1* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.2* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.2* %this, %class.btPersistentManifold** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.2*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator.2* %this, %class.btAlignedAllocator.2** %this.addr, align 4
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.2*, %class.btAlignedAllocator.2** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE5clearEv(%class.btAlignedObjectArray.11* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.11*, align 4
  store %class.btAlignedObjectArray.11* %this, %class.btAlignedObjectArray.11** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.11*, %class.btAlignedObjectArray.11** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray.11* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE7destroyEii(%class.btAlignedObjectArray.11* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE10deallocateEv(%class.btAlignedObjectArray.11* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE4initEv(%class.btAlignedObjectArray.11* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE7destroyEii(%class.btAlignedObjectArray.11* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.11*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.11* %this, %class.btAlignedObjectArray.11** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.11*, %class.btAlignedObjectArray.11** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 4
  %3 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE10deallocateEv(%class.btAlignedObjectArray.11* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.11*, align 4
  store %class.btAlignedObjectArray.11* %this, %class.btAlignedObjectArray.11** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.11*, %class.btAlignedObjectArray.11** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 4
  %0 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_data, align 4
  %tobool = icmp ne %class.btCollisionAlgorithm** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 4
  %2 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP20btCollisionAlgorithmLj16EE10deallocateEPS1_(%class.btAlignedAllocator.12* %m_allocator, %class.btCollisionAlgorithm** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 4
  store %class.btCollisionAlgorithm** null, %class.btCollisionAlgorithm*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btCollisionAlgorithmLj16EE10deallocateEPS1_(%class.btAlignedAllocator.12* %this, %class.btCollisionAlgorithm** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.12*, align 4
  %ptr.addr = alloca %class.btCollisionAlgorithm**, align 4
  store %class.btAlignedAllocator.12* %this, %class.btAlignedAllocator.12** %this.addr, align 4
  store %class.btCollisionAlgorithm** %ptr, %class.btCollisionAlgorithm*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.12*, %class.btAlignedAllocator.12** %this.addr, align 4
  %0 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %ptr.addr, align 4
  %1 = bitcast %class.btCollisionAlgorithm** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE7reserveEi(%class.btAlignedObjectArray.11* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.11*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btCollisionAlgorithm**, align 4
  store %class.btAlignedObjectArray.11* %this, %class.btAlignedObjectArray.11** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.11*, %class.btAlignedObjectArray.11** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE8capacityEv(%class.btAlignedObjectArray.11* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE8allocateEi(%class.btAlignedObjectArray.11* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btCollisionAlgorithm**
  store %class.btCollisionAlgorithm** %2, %class.btCollisionAlgorithm*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray.11* %this1)
  %3 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4copyEiiPS1_(%class.btAlignedObjectArray.11* %this1, i32 0, i32 %call3, %class.btCollisionAlgorithm** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4sizeEv(%class.btAlignedObjectArray.11* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE7destroyEii(%class.btAlignedObjectArray.11* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE10deallocateEv(%class.btAlignedObjectArray.11* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 4
  store %class.btCollisionAlgorithm** %4, %class.btCollisionAlgorithm*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE8capacityEv(%class.btAlignedObjectArray.11* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.11*, align 4
  store %class.btAlignedObjectArray.11* %this, %class.btAlignedObjectArray.11** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.11*, %class.btAlignedObjectArray.11** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE8allocateEi(%class.btAlignedObjectArray.11* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.11*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.11* %this, %class.btAlignedObjectArray.11** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.11*, %class.btAlignedObjectArray.11** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btCollisionAlgorithm** @_ZN18btAlignedAllocatorIP20btCollisionAlgorithmLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.12* %m_allocator, i32 %1, %class.btCollisionAlgorithm*** null)
  %2 = bitcast %class.btCollisionAlgorithm** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4copyEiiPS1_(%class.btAlignedObjectArray.11* %this, i32 %start, i32 %end, %class.btCollisionAlgorithm** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.11*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btCollisionAlgorithm**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.11* %this, %class.btAlignedObjectArray.11** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btCollisionAlgorithm** %dest, %class.btCollisionAlgorithm*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.11*, %class.btAlignedObjectArray.11** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %3, i32 %4
  %5 = bitcast %class.btCollisionAlgorithm** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btCollisionAlgorithm**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.11* %this1, i32 0, i32 4
  %7 = load %class.btCollisionAlgorithm**, %class.btCollisionAlgorithm*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %7, i32 %8
  %9 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %arrayidx2, align 4
  store %class.btCollisionAlgorithm* %9, %class.btCollisionAlgorithm** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionAlgorithm** @_ZN18btAlignedAllocatorIP20btCollisionAlgorithmLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.12* %this, i32 %n, %class.btCollisionAlgorithm*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.12*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btCollisionAlgorithm***, align 4
  store %class.btAlignedAllocator.12* %this, %class.btAlignedAllocator.12** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btCollisionAlgorithm*** %hint, %class.btCollisionAlgorithm**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.12*, %class.btAlignedAllocator.12** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btCollisionAlgorithm**
  ret %class.btCollisionAlgorithm** %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.1* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.1* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.1* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %2, %class.btPersistentManifold*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.1* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.1* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.1* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %4, %class.btPersistentManifold*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.1* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.1* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.2* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.1* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  %5 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %7 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %7, i32 %8
  %9 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4
  store %class.btPersistentManifold* %9, %class.btPersistentManifold** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.2* %this, i32 %n, %class.btPersistentManifold*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.2*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator.2* %this, %class.btAlignedAllocator.2** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.2*, %class.btAlignedAllocator.2** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btCompoundCollisionAlgorithm.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind willreturn }
attributes #7 = { nounwind }
attributes #8 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
