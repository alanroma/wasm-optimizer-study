; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/ConstraintSolver/btConeTwistConstraint.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/ConstraintSolver/btConeTwistConstraint.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btVector3 = type { [4 x float] }
%class.btConeTwistConstraint = type { %class.btTypedConstraint, [3 x %class.btJacobianEntry], %class.btTransform, %class.btTransform, float, float, float, float, float, float, float, float, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, i8, i8, i8, i8, float, float, %class.btVector3, i8, i8, %class.btQuaternion, float, %class.btVector3, i32, float, float, float }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.0, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.0 = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btJacobianEntry = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.1, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray, i32, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.1 = type <{ %class.btAlignedAllocator.2, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.2 = type { i8 }
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%"struct.btTypedConstraint::btConstraintInfo2" = type { float, float, float*, float*, float*, float*, i32, float*, float*, float*, float*, i32, float }
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btAlignedObjectArray.5 = type opaque
%class.btSerializer = type opaque
%struct.btConeTwistConstraintData = type { %struct.btTypedConstraintData, %struct.btTransformFloatData, %struct.btTransformFloatData, float, float, float, float, float, float, float, [4 x i8] }
%struct.btTypedConstraintData = type { %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData*, i8*, i32, i32, i32, i32, float, float, i32, i32, float, i32 }
%struct.btRigidBodyFloatData = type { %struct.btCollisionObjectFloatData, %struct.btMatrix3x3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, i32 }
%struct.btCollisionObjectFloatData = type { i8*, i8*, %struct.btCollisionShapeData*, i8*, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, i32, i32, [4 x i8] }
%struct.btCollisionShapeData = type opaque
%struct.btMatrix3x3FloatData = type { [3 x %struct.btVector3FloatData] }
%struct.btVector3FloatData = type { [4 x float] }
%struct.btTransformFloatData = type { %struct.btMatrix3x3FloatData, %struct.btVector3FloatData }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN15btJacobianEntryC2Ev = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN12btQuaternionC2Ev = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN21btConeTwistConstraint8setLimitEffffff = comdat any

$_ZNK11btRigidBody24getCenterOfMassTransformEv = comdat any

$_ZNK11btRigidBody24getInvInertiaTensorWorldEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_ = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x39getColumnEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK9btVector310normalizedEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_ = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZNK11btRigidBody23getCenterOfMassPositionEv = comdat any

$_ZNK11btRigidBody22getInvInertiaDiagLocalEv = comdat any

$_ZNK11btRigidBody10getInvMassEv = comdat any

$_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f = comdat any

$_ZNK12btSolverBody39internalGetVelocityInLocalPointObsoleteERK9btVector3RS0_ = comdat any

$_ZNK15btJacobianEntry11getDiagonalEv = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f = comdat any

$_ZNK12btSolverBody26internalGetAngularVelocityER9btVector3 = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_ = comdat any

$_ZN11btTransformC2ERK12btQuaternionRK9btVector3 = comdat any

$_ZNK11btTransform7inverseEv = comdat any

$_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_ = comdat any

$_ZNK21btConeTwistConstraint13getRigidBodyAEv = comdat any

$_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3 = comdat any

$_ZNK21btConeTwistConstraint13getRigidBodyBEv = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_Z5btMaxIfERKT_S2_S2_ = comdat any

$_Z11btAtan2Fastff = comdat any

$_Z6btFabsf = comdat any

$_Z15shortestArcQuatRK9btVector3S1_ = comdat any

$_Z10quatRotateRK12btQuaternionRK9btVector3 = comdat any

$_ZNK11btTransform11getRotationEv = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_Z11btFuzzyZerof = comdat any

$_ZNK12btQuaternion8getAngleEv = comdat any

$_ZmlRK12btQuaternionS1_ = comdat any

$_ZNK12btQuaternion7inverseEv = comdat any

$_ZN12btQuaternion9normalizeEv = comdat any

$_Z32computeAngularImpulseDenominatorRK9btVector3RK11btMatrix3x3 = comdat any

$_Z7btAtan2ff = comdat any

$_Z5btCosf = comdat any

$_Z5btSinf = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_Z4fabsf = comdat any

$_Z4sqrtf = comdat any

$_ZN12btQuaternionC2ERK9btVector3RKf = comdat any

$_ZNK12btQuaternionngEv = comdat any

$_ZN9btVector34setZEf = comdat any

$_ZN9btVector34setYEf = comdat any

$_ZN21btConeTwistConstraintD2Ev = comdat any

$_ZN21btConeTwistConstraintD0Ev = comdat any

$_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif = comdat any

$_ZNK21btConeTwistConstraint28calculateSerializeBufferSizeEv = comdat any

$_ZNK21btConeTwistConstraint9serializeEPvP12btSerializer = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZN11btTransform11setRotationERK12btQuaternion = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN11btMatrix3x3C2ERK12btQuaternion = comdat any

$_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf = comdat any

$_ZNK11btMatrix3x37inverseEv = comdat any

$_ZNK11btMatrix3x311getRotationER12btQuaternion = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZNK11btMatrix3x35cofacEiiii = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZmlRK12btQuaternionRK9btVector3 = comdat any

$_ZN12btQuaternionmLERKS_ = comdat any

$_ZNK10btQuadWord4getXEv = comdat any

$_ZNK10btQuadWord4getYEv = comdat any

$_ZNK10btQuadWord4getZEv = comdat any

$_Z6btAcosf = comdat any

$_ZNK12btQuaternion6lengthEv = comdat any

$_ZN12btQuaterniondVERKf = comdat any

$_ZN12btQuaternionmLERKf = comdat any

$_ZN12btQuaternion11setRotationERK9btVector3RKf = comdat any

$_ZN17btTypedConstraintD2Ev = comdat any

$_ZN21btConeTwistConstraintdlEPv = comdat any

$_ZNK11btTransform9serializeER20btTransformFloatData = comdat any

$_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData = comdat any

$_ZNK9btVector39serializeER18btVector3FloatData = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV21btConeTwistConstraint = hidden unnamed_addr constant { [14 x i8*] } { [14 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI21btConeTwistConstraint to i8*), i8* bitcast (%class.btConeTwistConstraint* (%class.btConeTwistConstraint*)* @_ZN21btConeTwistConstraintD2Ev to i8*), i8* bitcast (void (%class.btConeTwistConstraint*)* @_ZN21btConeTwistConstraintD0Ev to i8*), i8* bitcast (void (%class.btConeTwistConstraint*)* @_ZN21btConeTwistConstraint13buildJacobianEv to i8*), i8* bitcast (void (%class.btTypedConstraint*, %class.btAlignedObjectArray.5*, i32, i32, float)* @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif to i8*), i8* bitcast (void (%class.btConeTwistConstraint*, %"struct.btTypedConstraint::btConstraintInfo1"*)* @_ZN21btConeTwistConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E to i8*), i8* bitcast (void (%class.btConeTwistConstraint*, %"struct.btTypedConstraint::btConstraintInfo2"*)* @_ZN21btConeTwistConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E to i8*), i8* bitcast (void (%class.btConeTwistConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)* @_ZN21btConeTwistConstraint23solveConstraintObsoleteER12btSolverBodyS1_f to i8*), i8* bitcast (void (%class.btConeTwistConstraint*, i32, float, i32)* @_ZN21btConeTwistConstraint8setParamEifi to i8*), i8* bitcast (float (%class.btConeTwistConstraint*, i32, i32)* @_ZNK21btConeTwistConstraint8getParamEii to i8*), i8* bitcast (i32 (%class.btConeTwistConstraint*)* @_ZNK21btConeTwistConstraint28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConeTwistConstraint*, i8*, %class.btSerializer*)* @_ZNK21btConeTwistConstraint9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btConeTwistConstraint*, %class.btTransform*, %class.btTransform*)* @_ZN21btConeTwistConstraint9setFramesERK11btTransformS2_ to i8*)] }, align 4
@_ZZN21btConeTwistConstraint23solveConstraintObsoleteER12btSolverBodyS1_fE9bDoTorque = internal global i8 1, align 1
@_ZL6vTwist = internal global %class.btVector3 zeroinitializer, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS21btConeTwistConstraint = hidden constant [24 x i8] c"21btConeTwistConstraint\00", align 1
@_ZTI17btTypedConstraint = external constant i8*
@_ZTI21btConeTwistConstraint = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @_ZTS21btConeTwistConstraint, i32 0, i32 0), i8* bitcast (i8** @_ZTI17btTypedConstraint to i8*) }, align 4
@_ZTV17btTypedConstraint = external unnamed_addr constant { [13 x i8*] }, align 4
@.str = private unnamed_addr constant [26 x i8] c"btConeTwistConstraintData\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btConeTwistConstraint.cpp, i8* null }]

@_ZN21btConeTwistConstraintC1ER11btRigidBodyS1_RK11btTransformS4_ = hidden unnamed_addr alias %class.btConeTwistConstraint* (%class.btConeTwistConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btTransform*, %class.btTransform*), %class.btConeTwistConstraint* (%class.btConeTwistConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btTransform*, %class.btTransform*)* @_ZN21btConeTwistConstraintC2ER11btRigidBodyS1_RK11btTransformS4_
@_ZN21btConeTwistConstraintC1ER11btRigidBodyRK11btTransform = hidden unnamed_addr alias %class.btConeTwistConstraint* (%class.btConeTwistConstraint*, %class.btRigidBody*, %class.btTransform*), %class.btConeTwistConstraint* (%class.btConeTwistConstraint*, %class.btRigidBody*, %class.btTransform*)* @_ZN21btConeTwistConstraintC2ER11btRigidBodyRK11btTransform

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btConeTwistConstraint* @_ZN21btConeTwistConstraintC2ER11btRigidBodyS1_RK11btTransformS4_(%class.btConeTwistConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbA, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbB, %class.btTransform* nonnull align 4 dereferenceable(64) %rbAFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %rbBFrame) unnamed_addr #2 {
entry:
  %retval = alloca %class.btConeTwistConstraint*, align 4
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %rbB.addr = alloca %class.btRigidBody*, align 4
  %rbAFrame.addr = alloca %class.btTransform*, align 4
  %rbBFrame.addr = alloca %class.btTransform*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4
  store %class.btRigidBody* %rbB, %class.btRigidBody** %rbB.addr, align 4
  store %class.btTransform* %rbAFrame, %class.btTransform** %rbAFrame.addr, align 4
  store %class.btTransform* %rbBFrame, %class.btTransform** %rbBFrame.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  store %class.btConeTwistConstraint* %this1, %class.btConeTwistConstraint** %retval, align 4
  %0 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4
  %2 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* %0, i32 5, %class.btRigidBody* nonnull align 4 dereferenceable(676) %1, %class.btRigidBody* nonnull align 4 dereferenceable(676) %2)
  %3 = bitcast %class.btConeTwistConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [14 x i8*] }, { [14 x i8*] }* @_ZTV21btConeTwistConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %3, align 4
  %m_jac = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %4 = load %class.btTransform*, %class.btTransform** %rbAFrame.addr, align 4
  %call3 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_rbAFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %4)
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %5 = load %class.btTransform*, %class.btTransform** %rbBFrame.addr, align 4
  %call4 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_rbBFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %5)
  %m_swingAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_swingAxis)
  %m_twistAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_twistAxis)
  %m_angularOnly = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 22
  store i8 0, i8* %m_angularOnly, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 25
  store i8 0, i8* %m_useSolveConstraintObsolete, align 1
  %m_twistAxisA = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 28
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_twistAxisA)
  %m_qTarget = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 31
  %call8 = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %m_qTarget)
  %m_accMotorImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 33
  %call9 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_accMotorImpulse)
  call void @_ZN21btConeTwistConstraint4initEv(%class.btConeTwistConstraint* %this1)
  %6 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %retval, align 4
  ret %class.btConeTwistConstraint* %6
}

declare %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* returned, i32, %class.btRigidBody* nonnull align 4 dereferenceable(676), %class.btRigidBody* nonnull align 4 dereferenceable(676)) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_linearJointAxis)
  %m_aJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aJ)
  %m_bJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bJ)
  %m_0MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_0MinvJt)
  %m_1MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_1MinvJt)
  ret %class.btJacobianEntry* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btConeTwistConstraint4initEv(%class.btConeTwistConstraint* %this) #2 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %m_angularOnly = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 22
  store i8 0, i8* %m_angularOnly, align 4
  %m_solveTwistLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 23
  store i8 0, i8* %m_solveTwistLimit, align 1
  %m_solveSwingLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  store i8 0, i8* %m_solveSwingLimit, align 2
  %m_bMotorEnabled = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 29
  store i8 0, i8* %m_bMotorEnabled, align 4
  %m_maxMotorImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 32
  store float -1.000000e+00, float* %m_maxMotorImpulse, align 4
  call void @_ZN21btConeTwistConstraint8setLimitEffffff(%class.btConeTwistConstraint* %this1, float 0x43ABC16D60000000, float 0x43ABC16D60000000, float 0x43ABC16D60000000, float 1.000000e+00, float 0x3FD3333340000000, float 1.000000e+00)
  %m_damping = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 7
  store float 0x3F847AE140000000, float* %m_damping, align 4
  %m_fixThresh = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  store float 0x3FA99999A0000000, float* %m_fixThresh, align 4
  %m_flags = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 34
  store i32 0, i32* %m_flags, align 4
  %m_linCFM = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 35
  store float 0.000000e+00, float* %m_linCFM, align 4
  %m_linERP = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 36
  store float 0x3FE6666660000000, float* %m_linERP, align 4
  %m_angCFM = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 37
  store float 0.000000e+00, float* %m_angCFM, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden %class.btConeTwistConstraint* @_ZN21btConeTwistConstraintC2ER11btRigidBodyRK11btTransform(%class.btConeTwistConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbA, %class.btTransform* nonnull align 4 dereferenceable(64) %rbAFrame) unnamed_addr #2 {
entry:
  %retval = alloca %class.btConeTwistConstraint*, align 4
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %rbAFrame.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4
  store %class.btTransform* %rbAFrame, %class.btTransform** %rbAFrame.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  store %class.btConeTwistConstraint* %this1, %class.btConeTwistConstraint** %retval, align 4
  %0 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody(%class.btTypedConstraint* %0, i32 5, %class.btRigidBody* nonnull align 4 dereferenceable(676) %1)
  %2 = bitcast %class.btConeTwistConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [14 x i8*] }, { [14 x i8*] }* @_ZTV21btConeTwistConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4
  %m_jac = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %3 = load %class.btTransform*, %class.btTransform** %rbAFrame.addr, align 4
  %call3 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_rbAFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %3)
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %call4 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_rbBFrame)
  %m_swingAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_swingAxis)
  %m_twistAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_twistAxis)
  %m_angularOnly = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 22
  store i8 0, i8* %m_angularOnly, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 25
  store i8 0, i8* %m_useSolveConstraintObsolete, align 1
  %m_twistAxisA = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 28
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_twistAxisA)
  %m_qTarget = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 31
  %call8 = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %m_qTarget)
  %m_accMotorImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 33
  %call9 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_accMotorImpulse)
  %m_rbAFrame10 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %m_rbBFrame11 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %call12 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_rbBFrame11, %class.btTransform* nonnull align 4 dereferenceable(64) %m_rbAFrame10)
  %m_rbBFrame13 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  store float 0.000000e+00, float* %ref.tmp14, align 4
  store float 0.000000e+00, float* %ref.tmp15, align 4
  store float 0.000000e+00, float* %ref.tmp16, align 4
  %call17 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %m_rbBFrame13, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  call void @_ZN21btConeTwistConstraint4initEv(%class.btConeTwistConstraint* %this1)
  %4 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %retval, align 4
  ret %class.btConeTwistConstraint* %4
}

declare %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody(%class.btTypedConstraint* returned, i32, %class.btRigidBody* nonnull align 4 dereferenceable(676)) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btConeTwistConstraint8setLimitEffffff(%class.btConeTwistConstraint* %this, float %_swingSpan1, float %_swingSpan2, float %_twistSpan, float %_softness, float %_biasFactor, float %_relaxationFactor) #1 comdat {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %_swingSpan1.addr = alloca float, align 4
  %_swingSpan2.addr = alloca float, align 4
  %_twistSpan.addr = alloca float, align 4
  %_softness.addr = alloca float, align 4
  %_biasFactor.addr = alloca float, align 4
  %_relaxationFactor.addr = alloca float, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store float %_swingSpan1, float* %_swingSpan1.addr, align 4
  store float %_swingSpan2, float* %_swingSpan2.addr, align 4
  store float %_twistSpan, float* %_twistSpan.addr, align 4
  store float %_softness, float* %_softness.addr, align 4
  store float %_biasFactor, float* %_biasFactor.addr, align 4
  store float %_relaxationFactor, float* %_relaxationFactor.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load float, float* %_swingSpan1.addr, align 4
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  store float %0, float* %m_swingSpan1, align 4
  %1 = load float, float* %_swingSpan2.addr, align 4
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  store float %1, float* %m_swingSpan2, align 4
  %2 = load float, float* %_twistSpan.addr, align 4
  %m_twistSpan = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  store float %2, float* %m_twistSpan, align 4
  %3 = load float, float* %_softness.addr, align 4
  %m_limitSoftness = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  store float %3, float* %m_limitSoftness, align 4
  %4 = load float, float* %_biasFactor.addr, align 4
  %m_biasFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 5
  store float %4, float* %m_biasFactor, align 4
  %5 = load float, float* %_relaxationFactor.addr, align 4
  %m_relaxationFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 6
  store float %5, float* %m_relaxationFactor, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btConeTwistConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E(%class.btConeTwistConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo1"* %info) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo1"* %info, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 25
  %0 = load i8, i8* %m_useSolveConstraintObsolete, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %1, i32 0, i32 0
  store i32 0, i32* %m_numConstraintRows, align 4
  %2 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %nub = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %2, i32 0, i32 1
  store i32 0, i32* %nub, align 4
  br label %if.end28

if.else:                                          ; preds = %entry
  %3 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows2 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %3, i32 0, i32 0
  store i32 3, i32* %m_numConstraintRows2, align 4
  %4 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %nub3 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %4, i32 0, i32 1
  store i32 3, i32* %nub3, align 4
  %5 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %5, i32 0, i32 8
  %6 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %6)
  %7 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %7, i32 0, i32 9
  %8 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %call4 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %8)
  %9 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA5 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %9, i32 0, i32 8
  %10 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA5, align 4
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %10)
  %11 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB7 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %11, i32 0, i32 9
  %12 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB7, align 4
  %call8 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %12)
  call void @_ZN21btConeTwistConstraint14calcAngleInfo2ERK11btTransformS2_RK11btMatrix3x3S5_(%class.btConeTwistConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call4, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call6, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call8)
  %m_solveSwingLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  %13 = load i8, i8* %m_solveSwingLimit, align 2
  %tobool9 = trunc i8 %13 to i1
  br i1 %tobool9, label %if.then10, label %if.end20

if.then10:                                        ; preds = %if.else
  %14 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows11 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %14, i32 0, i32 0
  %15 = load i32, i32* %m_numConstraintRows11, align 4
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %m_numConstraintRows11, align 4
  %16 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %nub12 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %16, i32 0, i32 1
  %17 = load i32, i32* %nub12, align 4
  %dec = add nsw i32 %17, -1
  store i32 %dec, i32* %nub12, align 4
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %18 = load float, float* %m_swingSpan1, align 4
  %m_fixThresh = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %19 = load float, float* %m_fixThresh, align 4
  %cmp = fcmp olt float %18, %19
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then10
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %20 = load float, float* %m_swingSpan2, align 4
  %m_fixThresh13 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %21 = load float, float* %m_fixThresh13, align 4
  %cmp14 = fcmp olt float %20, %21
  br i1 %cmp14, label %if.then15, label %if.end

if.then15:                                        ; preds = %land.lhs.true
  %22 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows16 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %22, i32 0, i32 0
  %23 = load i32, i32* %m_numConstraintRows16, align 4
  %inc17 = add nsw i32 %23, 1
  store i32 %inc17, i32* %m_numConstraintRows16, align 4
  %24 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %nub18 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %24, i32 0, i32 1
  %25 = load i32, i32* %nub18, align 4
  %dec19 = add nsw i32 %25, -1
  store i32 %dec19, i32* %nub18, align 4
  br label %if.end

if.end:                                           ; preds = %if.then15, %land.lhs.true, %if.then10
  br label %if.end20

if.end20:                                         ; preds = %if.end, %if.else
  %m_solveTwistLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 23
  %26 = load i8, i8* %m_solveTwistLimit, align 1
  %tobool21 = trunc i8 %26 to i1
  br i1 %tobool21, label %if.then22, label %if.end27

if.then22:                                        ; preds = %if.end20
  %27 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows23 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %27, i32 0, i32 0
  %28 = load i32, i32* %m_numConstraintRows23, align 4
  %inc24 = add nsw i32 %28, 1
  store i32 %inc24, i32* %m_numConstraintRows23, align 4
  %29 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %nub25 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %29, i32 0, i32 1
  %30 = load i32, i32* %nub25, align 4
  %dec26 = add nsw i32 %30, -1
  store i32 %dec26, i32* %nub25, align 4
  br label %if.end27

if.end27:                                         ; preds = %if.then22, %if.end20
  br label %if.end28

if.end28:                                         ; preds = %if.end27, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btConeTwistConstraint14calcAngleInfo2ERK11btTransformS2_RK11btMatrix3x3S5_(%class.btConeTwistConstraint* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %invInertiaWorldA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %invInertiaWorldB) #2 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %invInertiaWorldA.addr = alloca %class.btMatrix3x3*, align 4
  %invInertiaWorldB.addr = alloca %class.btMatrix3x3*, align 4
  %trPose = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %trA = alloca %class.btTransform, align 4
  %trB = alloca %class.btTransform, align 4
  %trDeltaAB = alloca %class.btTransform, align 4
  %ref.tmp7 = alloca %class.btTransform, align 4
  %ref.tmp8 = alloca %class.btTransform, align 4
  %qDeltaAB = alloca %class.btQuaternion, align 4
  %swingAxis = alloca %class.btVector3, align 4
  %swingAxisLen2 = alloca float, align 4
  %qA = alloca %class.btQuaternion, align 4
  %ref.tmp26 = alloca %class.btQuaternion, align 4
  %ref.tmp27 = alloca %class.btQuaternion, align 4
  %qB = alloca %class.btQuaternion, align 4
  %ref.tmp29 = alloca %class.btQuaternion, align 4
  %ref.tmp30 = alloca %class.btQuaternion, align 4
  %qAB = alloca %class.btQuaternion, align 4
  %ref.tmp32 = alloca %class.btQuaternion, align 4
  %vConeNoTwist = alloca %class.btVector3, align 4
  %qABCone = alloca %class.btQuaternion, align 4
  %qABTwist = alloca %class.btQuaternion, align 4
  %ref.tmp35 = alloca %class.btQuaternion, align 4
  %swingAngle = alloca float, align 4
  %swingLimit = alloca float, align 4
  %swingAxis41 = alloca %class.btVector3, align 4
  %ref.tmp62 = alloca %class.btVector3, align 4
  %ref.tmp63 = alloca %class.btVector3, align 4
  %ref.tmp65 = alloca float, align 4
  %ref.tmp66 = alloca float, align 4
  %ref.tmp67 = alloca float, align 4
  %ivA = alloca %class.btVector3, align 4
  %ref.tmp75 = alloca %class.btVector3, align 4
  %jvA = alloca %class.btVector3, align 4
  %ref.tmp79 = alloca %class.btVector3, align 4
  %kvA = alloca %class.btVector3, align 4
  %ref.tmp83 = alloca %class.btVector3, align 4
  %ivB = alloca %class.btVector3, align 4
  %ref.tmp87 = alloca %class.btVector3, align 4
  %target = alloca %class.btVector3, align 4
  %x = alloca float, align 4
  %y = alloca float, align 4
  %z = alloca float, align 4
  %ref.tmp106 = alloca %class.btVector3, align 4
  %ref.tmp107 = alloca %class.btVector3, align 4
  %span2 = alloca float, align 4
  %span1 = alloca float, align 4
  %ref.tmp217 = alloca %class.btVector3, align 4
  %ref.tmp218 = alloca %class.btVector3, align 4
  %twistAxis = alloca %class.btVector3, align 4
  %ref.tmp266 = alloca %class.btVector3, align 4
  %ref.tmp267 = alloca %class.btVector3, align 4
  %ref.tmp278 = alloca %class.btVector3, align 4
  %ref.tmp279 = alloca %class.btVector3, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4
  store %class.btMatrix3x3* %invInertiaWorldA, %class.btMatrix3x3** %invInertiaWorldA.addr, align 4
  store %class.btMatrix3x3* %invInertiaWorldB, %class.btMatrix3x3** %invInertiaWorldB.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %m_swingCorrection = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 17
  store float 0.000000e+00, float* %m_swingCorrection, align 4
  %m_twistLimitSign = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 16
  store float 0.000000e+00, float* %m_twistLimitSign, align 4
  %m_solveTwistLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 23
  store i8 0, i8* %m_solveTwistLimit, align 1
  %m_solveSwingLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  store i8 0, i8* %m_solveSwingLimit, align 2
  %m_bMotorEnabled = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 29
  %0 = load i8, i8* %m_bMotorEnabled, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end25

land.lhs.true:                                    ; preds = %entry
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 25
  %1 = load i8, i8* %m_useSolveConstraintObsolete, align 1
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.end25, label %if.then

if.then:                                          ; preds = %land.lhs.true
  %m_qTarget = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 31
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 0.000000e+00, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %call6 = call %class.btTransform* @_ZN11btTransformC2ERK12btQuaternionRK9btVector3(%class.btTransform* %trPose, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_qTarget, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %2 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trA, %class.btTransform* %2, %class.btTransform* nonnull align 4 dereferenceable(64) %m_rbAFrame)
  %3 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trB, %class.btTransform* %3, %class.btTransform* nonnull align 4 dereferenceable(64) %m_rbBFrame)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp7, %class.btTransform* %trB, %class.btTransform* nonnull align 4 dereferenceable(64) %trPose)
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp8, %class.btTransform* %trA)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trDeltaAB, %class.btTransform* %ref.tmp7, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp8)
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %qDeltaAB, %class.btTransform* %trDeltaAB)
  %4 = bitcast %class.btQuaternion* %qDeltaAB to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %4)
  %5 = bitcast %class.btQuaternion* %qDeltaAB to %class.btQuadWord*
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %5)
  %6 = bitcast %class.btQuaternion* %qDeltaAB to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %6)
  %call12 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %swingAxis, float* nonnull align 4 dereferenceable(4) %call9, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call11)
  %call13 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %swingAxis)
  store float %call13, float* %swingAxisLen2, align 4
  %7 = load float, float* %swingAxisLen2, align 4
  %call14 = call zeroext i1 @_Z11btFuzzyZerof(float %7)
  br i1 %call14, label %if.then15, label %if.end

if.then15:                                        ; preds = %if.then
  br label %if.end284

if.end:                                           ; preds = %if.then
  %m_swingAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %8 = bitcast %class.btVector3* %m_swingAxis to i8*
  %9 = bitcast %class.btVector3* %swingAxis to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  %m_swingAxis16 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %m_swingAxis16)
  %call18 = call float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %qDeltaAB)
  %m_swingCorrection19 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 17
  store float %call18, float* %m_swingCorrection19, align 4
  %m_swingCorrection20 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 17
  %10 = load float, float* %m_swingCorrection20, align 4
  %call21 = call zeroext i1 @_Z11btFuzzyZerof(float %10)
  br i1 %call21, label %if.end24, label %if.then22

if.then22:                                        ; preds = %if.end
  %m_solveSwingLimit23 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  store i8 1, i8* %m_solveSwingLimit23, align 2
  br label %if.end24

if.end24:                                         ; preds = %if.then22, %if.end
  br label %if.end284

if.end25:                                         ; preds = %land.lhs.true, %entry
  %11 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp26, %class.btTransform* %11)
  %m_rbAFrame28 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp27, %class.btTransform* %m_rbAFrame28)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %qA, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp26, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp27)
  %12 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp29, %class.btTransform* %12)
  %m_rbBFrame31 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp30, %class.btTransform* %m_rbBFrame31)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %qB, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp29, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp30)
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp32, %class.btQuaternion* %qB)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %qAB, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp32, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qA)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %vConeNoTwist, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qAB, %class.btVector3* nonnull align 4 dereferenceable(16) @_ZL6vTwist)
  %call33 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %vConeNoTwist)
  call void @_Z15shortestArcQuatRK9btVector3S1_(%class.btQuaternion* sret align 4 %qABCone, %class.btVector3* nonnull align 4 dereferenceable(16) @_ZL6vTwist, %class.btVector3* nonnull align 4 dereferenceable(16) %vConeNoTwist)
  %call34 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %qABCone)
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp35, %class.btQuaternion* %qABCone)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %qABTwist, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp35, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qAB)
  %call36 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %qABTwist)
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %13 = load float, float* %m_swingSpan1, align 4
  %m_fixThresh = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %14 = load float, float* %m_fixThresh, align 4
  %cmp = fcmp oge float %13, %14
  br i1 %cmp, label %land.lhs.true37, label %if.else

land.lhs.true37:                                  ; preds = %if.end25
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %15 = load float, float* %m_swingSpan2, align 4
  %m_fixThresh38 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %16 = load float, float* %m_fixThresh38, align 4
  %cmp39 = fcmp oge float %15, %16
  br i1 %cmp39, label %if.then40, label %if.else

if.then40:                                        ; preds = %land.lhs.true37
  store float 0.000000e+00, float* %swingLimit, align 4
  %call42 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %swingAxis41)
  call void @_ZN21btConeTwistConstraint20computeConeLimitInfoERK12btQuaternionRfR9btVector3S3_(%class.btConeTwistConstraint* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qABCone, float* nonnull align 4 dereferenceable(4) %swingAngle, %class.btVector3* nonnull align 4 dereferenceable(16) %swingAxis41, float* nonnull align 4 dereferenceable(4) %swingLimit)
  %17 = load float, float* %swingAngle, align 4
  %18 = load float, float* %swingLimit, align 4
  %m_limitSoftness = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %19 = load float, float* %m_limitSoftness, align 4
  %mul = fmul float %18, %19
  %cmp43 = fcmp ogt float %17, %mul
  br i1 %cmp43, label %if.then44, label %if.end73

if.then44:                                        ; preds = %if.then40
  %m_solveSwingLimit45 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  store i8 1, i8* %m_solveSwingLimit45, align 2
  %m_swingLimitRatio = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 26
  store float 1.000000e+00, float* %m_swingLimitRatio, align 4
  %20 = load float, float* %swingAngle, align 4
  %21 = load float, float* %swingLimit, align 4
  %cmp46 = fcmp olt float %20, %21
  br i1 %cmp46, label %land.lhs.true47, label %if.end57

land.lhs.true47:                                  ; preds = %if.then44
  %m_limitSoftness48 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %22 = load float, float* %m_limitSoftness48, align 4
  %cmp49 = fcmp olt float %22, 0x3FEFFFFFC0000000
  br i1 %cmp49, label %if.then50, label %if.end57

if.then50:                                        ; preds = %land.lhs.true47
  %23 = load float, float* %swingAngle, align 4
  %24 = load float, float* %swingLimit, align 4
  %m_limitSoftness51 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %25 = load float, float* %m_limitSoftness51, align 4
  %mul52 = fmul float %24, %25
  %sub = fsub float %23, %mul52
  %26 = load float, float* %swingLimit, align 4
  %27 = load float, float* %swingLimit, align 4
  %m_limitSoftness53 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %28 = load float, float* %m_limitSoftness53, align 4
  %mul54 = fmul float %27, %28
  %sub55 = fsub float %26, %mul54
  %div = fdiv float %sub, %sub55
  %m_swingLimitRatio56 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 26
  store float %div, float* %m_swingLimitRatio56, align 4
  br label %if.end57

if.end57:                                         ; preds = %if.then50, %land.lhs.true47, %if.then44
  %29 = load float, float* %swingAngle, align 4
  %30 = load float, float* %swingLimit, align 4
  %m_limitSoftness58 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %31 = load float, float* %m_limitSoftness58, align 4
  %mul59 = fmul float %30, %31
  %sub60 = fsub float %29, %mul59
  %m_swingCorrection61 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 17
  store float %sub60, float* %m_swingCorrection61, align 4
  call void @_ZNK21btConeTwistConstraint33adjustSwingAxisToUseEllipseNormalER9btVector3(%class.btConeTwistConstraint* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %swingAxis41)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp63, %class.btVector3* nonnull align 4 dereferenceable(16) %swingAxis41)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp62, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp63)
  %m_swingAxis64 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %32 = bitcast %class.btVector3* %m_swingAxis64 to i8*
  %33 = bitcast %class.btVector3* %ref.tmp62 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %32, i8* align 4 %33, i32 16, i1 false)
  %m_twistAxisA = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 28
  store float 0.000000e+00, float* %ref.tmp65, align 4
  store float 0.000000e+00, float* %ref.tmp66, align 4
  store float 0.000000e+00, float* %ref.tmp67, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_twistAxisA, float* nonnull align 4 dereferenceable(4) %ref.tmp65, float* nonnull align 4 dereferenceable(4) %ref.tmp66, float* nonnull align 4 dereferenceable(4) %ref.tmp67)
  %m_swingAxis68 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %34 = load %class.btMatrix3x3*, %class.btMatrix3x3** %invInertiaWorldA.addr, align 4
  %call69 = call float @_Z32computeAngularImpulseDenominatorRK9btVector3RK11btMatrix3x3(%class.btVector3* nonnull align 4 dereferenceable(16) %m_swingAxis68, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %34)
  %m_swingAxis70 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %35 = load %class.btMatrix3x3*, %class.btMatrix3x3** %invInertiaWorldB.addr, align 4
  %call71 = call float @_Z32computeAngularImpulseDenominatorRK9btVector3RK11btMatrix3x3(%class.btVector3* nonnull align 4 dereferenceable(16) %m_swingAxis70, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %35)
  %add = fadd float %call69, %call71
  %div72 = fdiv float 1.000000e+00, %add
  %m_kSwing = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 14
  store float %div72, float* %m_kSwing, align 4
  br label %if.end73

if.end73:                                         ; preds = %if.end57, %if.then40
  br label %if.end230

if.else:                                          ; preds = %land.lhs.true37, %if.end25
  %36 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call74 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %36)
  %m_rbAFrame76 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call77 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame76)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp75, %class.btMatrix3x3* %call77, i32 0)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ivA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call74, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp75)
  %37 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call78 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %37)
  %m_rbAFrame80 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call81 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame80)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp79, %class.btMatrix3x3* %call81, i32 1)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %jvA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call78, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp79)
  %38 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call82 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %38)
  %m_rbAFrame84 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call85 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame84)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp83, %class.btMatrix3x3* %call85, i32 2)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %kvA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call82, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp83)
  %39 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call86 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %39)
  %m_rbBFrame88 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %call89 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbBFrame88)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp87, %class.btMatrix3x3* %call89, i32 0)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ivB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call86, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp87)
  %call90 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %target)
  %call91 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ivB, %class.btVector3* nonnull align 4 dereferenceable(16) %ivA)
  store float %call91, float* %x, align 4
  %call92 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ivB, %class.btVector3* nonnull align 4 dereferenceable(16) %jvA)
  store float %call92, float* %y, align 4
  %call93 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ivB, %class.btVector3* nonnull align 4 dereferenceable(16) %kvA)
  store float %call93, float* %z, align 4
  %m_swingSpan194 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %40 = load float, float* %m_swingSpan194, align 4
  %m_fixThresh95 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %41 = load float, float* %m_fixThresh95, align 4
  %cmp96 = fcmp olt float %40, %41
  br i1 %cmp96, label %land.lhs.true97, label %if.else110

land.lhs.true97:                                  ; preds = %if.else
  %m_swingSpan298 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %42 = load float, float* %m_swingSpan298, align 4
  %m_fixThresh99 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %43 = load float, float* %m_fixThresh99, align 4
  %cmp100 = fcmp olt float %42, %43
  br i1 %cmp100, label %if.then101, label %if.else110

if.then101:                                       ; preds = %land.lhs.true97
  %44 = load float, float* %y, align 4
  %call102 = call zeroext i1 @_Z11btFuzzyZerof(float %44)
  br i1 %call102, label %lor.lhs.false, label %if.then104

lor.lhs.false:                                    ; preds = %if.then101
  %45 = load float, float* %z, align 4
  %call103 = call zeroext i1 @_Z11btFuzzyZerof(float %45)
  br i1 %call103, label %if.end109, label %if.then104

if.then104:                                       ; preds = %lor.lhs.false, %if.then101
  %m_solveSwingLimit105 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  store i8 1, i8* %m_solveSwingLimit105, align 2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp107, %class.btVector3* %ivB, %class.btVector3* nonnull align 4 dereferenceable(16) %ivA)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp106, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp107)
  %m_swingAxis108 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %46 = bitcast %class.btVector3* %m_swingAxis108 to i8*
  %47 = bitcast %class.btVector3* %ref.tmp106 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %46, i8* align 4 %47, i32 16, i1 false)
  br label %if.end109

if.end109:                                        ; preds = %if.then104, %lor.lhs.false
  br label %if.end229

if.else110:                                       ; preds = %land.lhs.true97, %if.else
  %m_swingSpan1111 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %48 = load float, float* %m_swingSpan1111, align 4
  %m_fixThresh112 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %49 = load float, float* %m_fixThresh112, align 4
  %cmp113 = fcmp olt float %48, %49
  br i1 %cmp113, label %if.then114, label %if.else145

if.then114:                                       ; preds = %if.else110
  %50 = load float, float* %x, align 4
  %call115 = call zeroext i1 @_Z11btFuzzyZerof(float %50)
  br i1 %call115, label %lor.lhs.false116, label %if.then118

lor.lhs.false116:                                 ; preds = %if.then114
  %51 = load float, float* %z, align 4
  %call117 = call zeroext i1 @_Z11btFuzzyZerof(float %51)
  br i1 %call117, label %if.end144, label %if.then118

if.then118:                                       ; preds = %lor.lhs.false116, %if.then114
  %m_solveSwingLimit119 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  store i8 1, i8* %m_solveSwingLimit119, align 2
  %m_swingSpan2120 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %52 = load float, float* %m_swingSpan2120, align 4
  %m_fixThresh121 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %53 = load float, float* %m_fixThresh121, align 4
  %cmp122 = fcmp oge float %52, %53
  br i1 %cmp122, label %if.then123, label %if.end143

if.then123:                                       ; preds = %if.then118
  store float 0.000000e+00, float* %y, align 4
  %54 = load float, float* %z, align 4
  %55 = load float, float* %x, align 4
  %call124 = call float @_Z7btAtan2ff(float %54, float %55)
  store float %call124, float* %span2, align 4
  %56 = load float, float* %span2, align 4
  %m_swingSpan2125 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %57 = load float, float* %m_swingSpan2125, align 4
  %cmp126 = fcmp ogt float %56, %57
  br i1 %cmp126, label %if.then127, label %if.else132

if.then127:                                       ; preds = %if.then123
  %m_swingSpan2128 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %58 = load float, float* %m_swingSpan2128, align 4
  %call129 = call float @_Z5btCosf(float %58)
  store float %call129, float* %x, align 4
  %m_swingSpan2130 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %59 = load float, float* %m_swingSpan2130, align 4
  %call131 = call float @_Z5btSinf(float %59)
  store float %call131, float* %z, align 4
  br label %if.end142

if.else132:                                       ; preds = %if.then123
  %60 = load float, float* %span2, align 4
  %m_swingSpan2133 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %61 = load float, float* %m_swingSpan2133, align 4
  %fneg = fneg float %61
  %cmp134 = fcmp olt float %60, %fneg
  br i1 %cmp134, label %if.then135, label %if.end141

if.then135:                                       ; preds = %if.else132
  %m_swingSpan2136 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %62 = load float, float* %m_swingSpan2136, align 4
  %call137 = call float @_Z5btCosf(float %62)
  store float %call137, float* %x, align 4
  %m_swingSpan2138 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %63 = load float, float* %m_swingSpan2138, align 4
  %call139 = call float @_Z5btSinf(float %63)
  %fneg140 = fneg float %call139
  store float %fneg140, float* %z, align 4
  br label %if.end141

if.end141:                                        ; preds = %if.then135, %if.else132
  br label %if.end142

if.end142:                                        ; preds = %if.end141, %if.then127
  br label %if.end143

if.end143:                                        ; preds = %if.end142, %if.then118
  br label %if.end144

if.end144:                                        ; preds = %if.end143, %lor.lhs.false116
  br label %if.end177

if.else145:                                       ; preds = %if.else110
  %64 = load float, float* %x, align 4
  %call146 = call zeroext i1 @_Z11btFuzzyZerof(float %64)
  br i1 %call146, label %lor.lhs.false147, label %if.then149

lor.lhs.false147:                                 ; preds = %if.else145
  %65 = load float, float* %y, align 4
  %call148 = call zeroext i1 @_Z11btFuzzyZerof(float %65)
  br i1 %call148, label %if.end176, label %if.then149

if.then149:                                       ; preds = %lor.lhs.false147, %if.else145
  %m_solveSwingLimit150 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  store i8 1, i8* %m_solveSwingLimit150, align 2
  %m_swingSpan1151 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %66 = load float, float* %m_swingSpan1151, align 4
  %m_fixThresh152 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %67 = load float, float* %m_fixThresh152, align 4
  %cmp153 = fcmp oge float %66, %67
  br i1 %cmp153, label %if.then154, label %if.end175

if.then154:                                       ; preds = %if.then149
  store float 0.000000e+00, float* %z, align 4
  %68 = load float, float* %y, align 4
  %69 = load float, float* %x, align 4
  %call155 = call float @_Z7btAtan2ff(float %68, float %69)
  store float %call155, float* %span1, align 4
  %70 = load float, float* %span1, align 4
  %m_swingSpan1156 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %71 = load float, float* %m_swingSpan1156, align 4
  %cmp157 = fcmp ogt float %70, %71
  br i1 %cmp157, label %if.then158, label %if.else163

if.then158:                                       ; preds = %if.then154
  %m_swingSpan1159 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %72 = load float, float* %m_swingSpan1159, align 4
  %call160 = call float @_Z5btCosf(float %72)
  store float %call160, float* %x, align 4
  %m_swingSpan1161 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %73 = load float, float* %m_swingSpan1161, align 4
  %call162 = call float @_Z5btSinf(float %73)
  store float %call162, float* %y, align 4
  br label %if.end174

if.else163:                                       ; preds = %if.then154
  %74 = load float, float* %span1, align 4
  %m_swingSpan1164 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %75 = load float, float* %m_swingSpan1164, align 4
  %fneg165 = fneg float %75
  %cmp166 = fcmp olt float %74, %fneg165
  br i1 %cmp166, label %if.then167, label %if.end173

if.then167:                                       ; preds = %if.else163
  %m_swingSpan1168 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %76 = load float, float* %m_swingSpan1168, align 4
  %call169 = call float @_Z5btCosf(float %76)
  store float %call169, float* %x, align 4
  %m_swingSpan1170 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %77 = load float, float* %m_swingSpan1170, align 4
  %call171 = call float @_Z5btSinf(float %77)
  %fneg172 = fneg float %call171
  store float %fneg172, float* %y, align 4
  br label %if.end173

if.end173:                                        ; preds = %if.then167, %if.else163
  br label %if.end174

if.end174:                                        ; preds = %if.end173, %if.then158
  br label %if.end175

if.end175:                                        ; preds = %if.end174, %if.then149
  br label %if.end176

if.end176:                                        ; preds = %if.end175, %lor.lhs.false147
  br label %if.end177

if.end177:                                        ; preds = %if.end176, %if.end144
  %78 = load float, float* %x, align 4
  %call178 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ivA)
  %arrayidx = getelementptr inbounds float, float* %call178, i32 0
  %79 = load float, float* %arrayidx, align 4
  %mul179 = fmul float %78, %79
  %80 = load float, float* %y, align 4
  %call180 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %jvA)
  %arrayidx181 = getelementptr inbounds float, float* %call180, i32 0
  %81 = load float, float* %arrayidx181, align 4
  %mul182 = fmul float %80, %81
  %add183 = fadd float %mul179, %mul182
  %82 = load float, float* %z, align 4
  %call184 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %kvA)
  %arrayidx185 = getelementptr inbounds float, float* %call184, i32 0
  %83 = load float, float* %arrayidx185, align 4
  %mul186 = fmul float %82, %83
  %add187 = fadd float %add183, %mul186
  %call188 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %target)
  %arrayidx189 = getelementptr inbounds float, float* %call188, i32 0
  store float %add187, float* %arrayidx189, align 4
  %84 = load float, float* %x, align 4
  %call190 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ivA)
  %arrayidx191 = getelementptr inbounds float, float* %call190, i32 1
  %85 = load float, float* %arrayidx191, align 4
  %mul192 = fmul float %84, %85
  %86 = load float, float* %y, align 4
  %call193 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %jvA)
  %arrayidx194 = getelementptr inbounds float, float* %call193, i32 1
  %87 = load float, float* %arrayidx194, align 4
  %mul195 = fmul float %86, %87
  %add196 = fadd float %mul192, %mul195
  %88 = load float, float* %z, align 4
  %call197 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %kvA)
  %arrayidx198 = getelementptr inbounds float, float* %call197, i32 1
  %89 = load float, float* %arrayidx198, align 4
  %mul199 = fmul float %88, %89
  %add200 = fadd float %add196, %mul199
  %call201 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %target)
  %arrayidx202 = getelementptr inbounds float, float* %call201, i32 1
  store float %add200, float* %arrayidx202, align 4
  %90 = load float, float* %x, align 4
  %call203 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ivA)
  %arrayidx204 = getelementptr inbounds float, float* %call203, i32 2
  %91 = load float, float* %arrayidx204, align 4
  %mul205 = fmul float %90, %91
  %92 = load float, float* %y, align 4
  %call206 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %jvA)
  %arrayidx207 = getelementptr inbounds float, float* %call206, i32 2
  %93 = load float, float* %arrayidx207, align 4
  %mul208 = fmul float %92, %93
  %add209 = fadd float %mul205, %mul208
  %94 = load float, float* %z, align 4
  %call210 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %kvA)
  %arrayidx211 = getelementptr inbounds float, float* %call210, i32 2
  %95 = load float, float* %arrayidx211, align 4
  %mul212 = fmul float %94, %95
  %add213 = fadd float %add209, %mul212
  %call214 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %target)
  %arrayidx215 = getelementptr inbounds float, float* %call214, i32 2
  store float %add213, float* %arrayidx215, align 4
  %call216 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %target)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp218, %class.btVector3* %ivB, %class.btVector3* nonnull align 4 dereferenceable(16) %target)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp217, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp218)
  %m_swingAxis219 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %96 = bitcast %class.btVector3* %m_swingAxis219 to i8*
  %97 = bitcast %class.btVector3* %ref.tmp217 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %96, i8* align 4 %97, i32 16, i1 false)
  %m_swingAxis220 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call221 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %m_swingAxis220)
  %m_swingCorrection222 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 17
  store float %call221, float* %m_swingCorrection222, align 4
  %m_swingCorrection223 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 17
  %98 = load float, float* %m_swingCorrection223, align 4
  %call224 = call zeroext i1 @_Z11btFuzzyZerof(float %98)
  br i1 %call224, label %if.end228, label %if.then225

if.then225:                                       ; preds = %if.end177
  %m_swingAxis226 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call227 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %m_swingAxis226)
  br label %if.end228

if.end228:                                        ; preds = %if.then225, %if.end177
  br label %if.end229

if.end229:                                        ; preds = %if.end228, %if.end109
  br label %if.end230

if.end230:                                        ; preds = %if.end229, %if.end73
  %m_twistSpan = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %99 = load float, float* %m_twistSpan, align 4
  %cmp231 = fcmp oge float %99, 0.000000e+00
  br i1 %cmp231, label %if.then232, label %if.else282

if.then232:                                       ; preds = %if.end230
  %call233 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %twistAxis)
  %m_twistAngle = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 19
  call void @_ZN21btConeTwistConstraint21computeTwistLimitInfoERK12btQuaternionRfR9btVector3(%class.btConeTwistConstraint* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qABTwist, float* nonnull align 4 dereferenceable(4) %m_twistAngle, %class.btVector3* nonnull align 4 dereferenceable(16) %twistAxis)
  %m_twistAngle234 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 19
  %100 = load float, float* %m_twistAngle234, align 4
  %m_twistSpan235 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %101 = load float, float* %m_twistSpan235, align 4
  %m_limitSoftness236 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %102 = load float, float* %m_limitSoftness236, align 4
  %mul237 = fmul float %101, %102
  %cmp238 = fcmp ogt float %100, %mul237
  br i1 %cmp238, label %if.then239, label %if.end274

if.then239:                                       ; preds = %if.then232
  %m_solveTwistLimit240 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 23
  store i8 1, i8* %m_solveTwistLimit240, align 1
  %m_twistLimitRatio = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 27
  store float 1.000000e+00, float* %m_twistLimitRatio, align 4
  %m_twistAngle241 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 19
  %103 = load float, float* %m_twistAngle241, align 4
  %m_twistSpan242 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %104 = load float, float* %m_twistSpan242, align 4
  %cmp243 = fcmp olt float %103, %104
  br i1 %cmp243, label %land.lhs.true244, label %if.end260

land.lhs.true244:                                 ; preds = %if.then239
  %m_limitSoftness245 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %105 = load float, float* %m_limitSoftness245, align 4
  %cmp246 = fcmp olt float %105, 0x3FEFFFFFC0000000
  br i1 %cmp246, label %if.then247, label %if.end260

if.then247:                                       ; preds = %land.lhs.true244
  %m_twistAngle248 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 19
  %106 = load float, float* %m_twistAngle248, align 4
  %m_twistSpan249 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %107 = load float, float* %m_twistSpan249, align 4
  %m_limitSoftness250 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %108 = load float, float* %m_limitSoftness250, align 4
  %mul251 = fmul float %107, %108
  %sub252 = fsub float %106, %mul251
  %m_twistSpan253 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %109 = load float, float* %m_twistSpan253, align 4
  %m_twistSpan254 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %110 = load float, float* %m_twistSpan254, align 4
  %m_limitSoftness255 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %111 = load float, float* %m_limitSoftness255, align 4
  %mul256 = fmul float %110, %111
  %sub257 = fsub float %109, %mul256
  %div258 = fdiv float %sub252, %sub257
  %m_twistLimitRatio259 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 27
  store float %div258, float* %m_twistLimitRatio259, align 4
  br label %if.end260

if.end260:                                        ; preds = %if.then247, %land.lhs.true244, %if.then239
  %m_twistAngle261 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 19
  %112 = load float, float* %m_twistAngle261, align 4
  %m_twistSpan262 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %113 = load float, float* %m_twistSpan262, align 4
  %m_limitSoftness263 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %114 = load float, float* %m_limitSoftness263, align 4
  %mul264 = fmul float %113, %114
  %sub265 = fsub float %112, %mul264
  %m_twistCorrection = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 18
  store float %sub265, float* %m_twistCorrection, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp267, %class.btVector3* nonnull align 4 dereferenceable(16) %twistAxis)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp266, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp267)
  %m_twistAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %115 = bitcast %class.btVector3* %m_twistAxis to i8*
  %116 = bitcast %class.btVector3* %ref.tmp266 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %115, i8* align 4 %116, i32 16, i1 false)
  %m_twistAxis268 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %117 = load %class.btMatrix3x3*, %class.btMatrix3x3** %invInertiaWorldA.addr, align 4
  %call269 = call float @_Z32computeAngularImpulseDenominatorRK9btVector3RK11btMatrix3x3(%class.btVector3* nonnull align 4 dereferenceable(16) %m_twistAxis268, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %117)
  %m_twistAxis270 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %118 = load %class.btMatrix3x3*, %class.btMatrix3x3** %invInertiaWorldB.addr, align 4
  %call271 = call float @_Z32computeAngularImpulseDenominatorRK9btVector3RK11btMatrix3x3(%class.btVector3* nonnull align 4 dereferenceable(16) %m_twistAxis270, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %118)
  %add272 = fadd float %call269, %call271
  %div273 = fdiv float 1.000000e+00, %add272
  %m_kTwist = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 15
  store float %div273, float* %m_kTwist, align 4
  br label %if.end274

if.end274:                                        ; preds = %if.end260, %if.then232
  %m_solveSwingLimit275 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  %119 = load i8, i8* %m_solveSwingLimit275, align 2
  %tobool276 = trunc i8 %119 to i1
  br i1 %tobool276, label %if.then277, label %if.end281

if.then277:                                       ; preds = %if.end274
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp279, %class.btVector3* nonnull align 4 dereferenceable(16) %twistAxis)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp278, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp279)
  %m_twistAxisA280 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 28
  %120 = bitcast %class.btVector3* %m_twistAxisA280 to i8*
  %121 = bitcast %class.btVector3* %ref.tmp278 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %120, i8* align 4 %121, i32 16, i1 false)
  br label %if.end281

if.end281:                                        ; preds = %if.then277, %if.end274
  br label %if.end284

if.else282:                                       ; preds = %if.end230
  %m_twistAngle283 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 19
  store float 0.000000e+00, float* %m_twistAngle283, align 4
  br label %if.end284

if.end284:                                        ; preds = %if.then15, %if.end24, %if.else282, %if.end281
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  ret %class.btMatrix3x3* %m_invInertiaTensorWorld
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN21btConeTwistConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E(%class.btConeTwistConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo1"* %info) #1 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo1"* %info, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %0, i32 0, i32 0
  store i32 6, i32* %m_numConstraintRows, align 4
  %1 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %nub = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %1, i32 0, i32 1
  store i32 0, i32* %nub, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btConeTwistConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E(%class.btConeTwistConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %1 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %1, i32 0, i32 8
  %2 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %2)
  %3 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %3, i32 0, i32 9
  %4 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %4)
  %5 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA3 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %5, i32 0, i32 8
  %6 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA3, align 4
  %call4 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %6)
  %7 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB5 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %7, i32 0, i32 9
  %8 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB5, align 4
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %8)
  call void @_ZN21btConeTwistConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK11btMatrix3x3S8_(%class.btConeTwistConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call4, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call6)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btConeTwistConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK11btMatrix3x3S8_(%class.btConeTwistConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %invInertiaWorldA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %invInertiaWorldB) #2 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %invInertiaWorldA.addr = alloca %class.btMatrix3x3*, align 4
  %invInertiaWorldB.addr = alloca %class.btMatrix3x3*, align 4
  %a1 = alloca %class.btVector3, align 4
  %angular0 = alloca %class.btVector3*, align 4
  %angular1 = alloca %class.btVector3*, align 4
  %angular2 = alloca %class.btVector3*, align 4
  %a1neg = alloca %class.btVector3, align 4
  %a2 = alloca %class.btVector3, align 4
  %angular027 = alloca %class.btVector3*, align 4
  %angular128 = alloca %class.btVector3*, align 4
  %angular232 = alloca %class.btVector3*, align 4
  %linERP = alloca float, align 4
  %k = alloca float, align 4
  %j = alloca i32, align 4
  %row = alloca i32, align 4
  %srow = alloca i32, align 4
  %ax1 = alloca %class.btVector3, align 4
  %J1 = alloca float*, align 4
  %J2 = alloca float*, align 4
  %trA = alloca %class.btTransform, align 4
  %p = alloca %class.btVector3, align 4
  %q = alloca %class.btVector3, align 4
  %srow1 = alloca i32, align 4
  %fact = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp156 = alloca %class.btVector3, align 4
  %k187 = alloca float, align 4
  %ref.tmp218 = alloca %class.btVector3, align 4
  %ref.tmp219 = alloca %class.btVector3, align 4
  %J1222 = alloca float*, align 4
  %J2224 = alloca float*, align 4
  %k253 = alloca float, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4
  store %class.btMatrix3x3* %invInertiaWorldA, %class.btMatrix3x3** %invInertiaWorldA.addr, align 4
  store %class.btMatrix3x3* %invInertiaWorldB, %class.btMatrix3x3** %invInertiaWorldB.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %invInertiaWorldA.addr, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %invInertiaWorldB.addr, align 4
  call void @_ZN21btConeTwistConstraint14calcAngleInfo2ERK11btTransformS2_RK11btMatrix3x3S5_(%class.btConeTwistConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %3)
  %4 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %4, i32 0, i32 2
  %5 = load float*, float** %m_J1linearAxis, align 4
  %arrayidx = getelementptr inbounds float, float* %5, i32 0
  store float 1.000000e+00, float* %arrayidx, align 4
  %6 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1linearAxis2 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %6, i32 0, i32 2
  %7 = load float*, float** %m_J1linearAxis2, align 4
  %8 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %8, i32 0, i32 6
  %9 = load i32, i32* %rowskip, align 4
  %add = add nsw i32 %9, 1
  %arrayidx3 = getelementptr inbounds float, float* %7, i32 %add
  store float 1.000000e+00, float* %arrayidx3, align 4
  %10 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1linearAxis4 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %10, i32 0, i32 2
  %11 = load float*, float** %m_J1linearAxis4, align 4
  %12 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip5 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %12, i32 0, i32 6
  %13 = load i32, i32* %rowskip5, align 4
  %mul = mul nsw i32 2, %13
  %add6 = add nsw i32 %mul, 2
  %arrayidx7 = getelementptr inbounds float, float* %11, i32 %add6
  store float 1.000000e+00, float* %arrayidx7, align 4
  %14 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %14)
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbAFrame)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %a1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call8)
  %15 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %15, i32 0, i32 3
  %16 = load float*, float** %m_J1angularAxis, align 4
  %17 = bitcast float* %16 to %class.btVector3*
  store %class.btVector3* %17, %class.btVector3** %angular0, align 4
  %18 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis9 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %18, i32 0, i32 3
  %19 = load float*, float** %m_J1angularAxis9, align 4
  %20 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip10 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %20, i32 0, i32 6
  %21 = load i32, i32* %rowskip10, align 4
  %add.ptr = getelementptr inbounds float, float* %19, i32 %21
  %22 = bitcast float* %add.ptr to %class.btVector3*
  store %class.btVector3* %22, %class.btVector3** %angular1, align 4
  %23 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis11 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %23, i32 0, i32 3
  %24 = load float*, float** %m_J1angularAxis11, align 4
  %25 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip12 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %25, i32 0, i32 6
  %26 = load i32, i32* %rowskip12, align 4
  %mul13 = mul nsw i32 2, %26
  %add.ptr14 = getelementptr inbounds float, float* %24, i32 %mul13
  %27 = bitcast float* %add.ptr14 to %class.btVector3*
  store %class.btVector3* %27, %class.btVector3** %angular2, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %a1neg, %class.btVector3* nonnull align 4 dereferenceable(16) %a1)
  %28 = load %class.btVector3*, %class.btVector3** %angular0, align 4
  %29 = load %class.btVector3*, %class.btVector3** %angular1, align 4
  %30 = load %class.btVector3*, %class.btVector3** %angular2, align 4
  call void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %a1neg, %class.btVector3* %28, %class.btVector3* %29, %class.btVector3* %30)
  %31 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %31, i32 0, i32 4
  %32 = load float*, float** %m_J2linearAxis, align 4
  %arrayidx15 = getelementptr inbounds float, float* %32, i32 0
  store float -1.000000e+00, float* %arrayidx15, align 4
  %33 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2linearAxis16 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %33, i32 0, i32 4
  %34 = load float*, float** %m_J2linearAxis16, align 4
  %35 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip17 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %35, i32 0, i32 6
  %36 = load i32, i32* %rowskip17, align 4
  %add18 = add nsw i32 %36, 1
  %arrayidx19 = getelementptr inbounds float, float* %34, i32 %add18
  store float -1.000000e+00, float* %arrayidx19, align 4
  %37 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2linearAxis20 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %37, i32 0, i32 4
  %38 = load float*, float** %m_J2linearAxis20, align 4
  %39 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip21 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %39, i32 0, i32 6
  %40 = load i32, i32* %rowskip21, align 4
  %mul22 = mul nsw i32 2, %40
  %add23 = add nsw i32 %mul22, 2
  %arrayidx24 = getelementptr inbounds float, float* %38, i32 %add23
  store float -1.000000e+00, float* %arrayidx24, align 4
  %41 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %41)
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %call26 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbBFrame)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %a2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call25, %class.btVector3* nonnull align 4 dereferenceable(16) %call26)
  %42 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %42, i32 0, i32 5
  %43 = load float*, float** %m_J2angularAxis, align 4
  %44 = bitcast float* %43 to %class.btVector3*
  store %class.btVector3* %44, %class.btVector3** %angular027, align 4
  %45 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis29 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %45, i32 0, i32 5
  %46 = load float*, float** %m_J2angularAxis29, align 4
  %47 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip30 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %47, i32 0, i32 6
  %48 = load i32, i32* %rowskip30, align 4
  %add.ptr31 = getelementptr inbounds float, float* %46, i32 %48
  %49 = bitcast float* %add.ptr31 to %class.btVector3*
  store %class.btVector3* %49, %class.btVector3** %angular128, align 4
  %50 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis33 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %50, i32 0, i32 5
  %51 = load float*, float** %m_J2angularAxis33, align 4
  %52 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip34 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %52, i32 0, i32 6
  %53 = load i32, i32* %rowskip34, align 4
  %mul35 = mul nsw i32 2, %53
  %add.ptr36 = getelementptr inbounds float, float* %51, i32 %mul35
  %54 = bitcast float* %add.ptr36 to %class.btVector3*
  store %class.btVector3* %54, %class.btVector3** %angular232, align 4
  %55 = load %class.btVector3*, %class.btVector3** %angular027, align 4
  %56 = load %class.btVector3*, %class.btVector3** %angular128, align 4
  %57 = load %class.btVector3*, %class.btVector3** %angular232, align 4
  call void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %a2, %class.btVector3* %55, %class.btVector3* %56, %class.btVector3* %57)
  %m_flags = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 34
  %58 = load i32, i32* %m_flags, align 4
  %and = and i32 %58, 2
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_linERP = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 36
  %59 = load float, float* %m_linERP, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %60 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %erp = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %60, i32 0, i32 1
  %61 = load float, float* %erp, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %59, %cond.true ], [ %61, %cond.false ]
  store float %cond, float* %linERP, align 4
  %62 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %62, i32 0, i32 0
  %63 = load float, float* %fps, align 4
  %64 = load float, float* %linERP, align 4
  %mul37 = fmul float %63, %64
  store float %mul37, float* %k, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end
  %65 = load i32, i32* %j, align 4
  %cmp = icmp slt i32 %65, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %66 = load float, float* %k, align 4
  %call38 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %a2)
  %67 = load i32, i32* %j, align 4
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 %67
  %68 = load float, float* %arrayidx39, align 4
  %69 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call40 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %69)
  %call41 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call40)
  %70 = load i32, i32* %j, align 4
  %arrayidx42 = getelementptr inbounds float, float* %call41, i32 %70
  %71 = load float, float* %arrayidx42, align 4
  %add43 = fadd float %68, %71
  %call44 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %a1)
  %72 = load i32, i32* %j, align 4
  %arrayidx45 = getelementptr inbounds float, float* %call44, i32 %72
  %73 = load float, float* %arrayidx45, align 4
  %sub = fsub float %add43, %73
  %74 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call46 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %74)
  %call47 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call46)
  %75 = load i32, i32* %j, align 4
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 %75
  %76 = load float, float* %arrayidx48, align 4
  %sub49 = fsub float %sub, %76
  %mul50 = fmul float %66, %sub49
  %77 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %77, i32 0, i32 7
  %78 = load float*, float** %m_constraintError, align 4
  %79 = load i32, i32* %j, align 4
  %80 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip51 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %80, i32 0, i32 6
  %81 = load i32, i32* %rowskip51, align 4
  %mul52 = mul nsw i32 %79, %81
  %arrayidx53 = getelementptr inbounds float, float* %78, i32 %mul52
  store float %mul50, float* %arrayidx53, align 4
  %82 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %82, i32 0, i32 9
  %83 = load float*, float** %m_lowerLimit, align 4
  %84 = load i32, i32* %j, align 4
  %85 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip54 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %85, i32 0, i32 6
  %86 = load i32, i32* %rowskip54, align 4
  %mul55 = mul nsw i32 %84, %86
  %arrayidx56 = getelementptr inbounds float, float* %83, i32 %mul55
  store float 0xC7EFFFFFE0000000, float* %arrayidx56, align 4
  %87 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %87, i32 0, i32 10
  %88 = load float*, float** %m_upperLimit, align 4
  %89 = load i32, i32* %j, align 4
  %90 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip57 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %90, i32 0, i32 6
  %91 = load i32, i32* %rowskip57, align 4
  %mul58 = mul nsw i32 %89, %91
  %arrayidx59 = getelementptr inbounds float, float* %88, i32 %mul58
  store float 0x47EFFFFFE0000000, float* %arrayidx59, align 4
  %m_flags60 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 34
  %92 = load i32, i32* %m_flags60, align 4
  %and61 = and i32 %92, 1
  %tobool62 = icmp ne i32 %and61, 0
  br i1 %tobool62, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %m_linCFM = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 35
  %93 = load float, float* %m_linCFM, align 4
  %94 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %94, i32 0, i32 8
  %95 = load float*, float** %cfm, align 4
  %96 = load i32, i32* %j, align 4
  %97 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip63 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %97, i32 0, i32 6
  %98 = load i32, i32* %rowskip63, align 4
  %mul64 = mul nsw i32 %96, %98
  %arrayidx65 = getelementptr inbounds float, float* %95, i32 %mul64
  store float %93, float* %arrayidx65, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %99 = load i32, i32* %j, align 4
  %inc = add nsw i32 %99, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 3, i32* %row, align 4
  %100 = load i32, i32* %row, align 4
  %101 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip66 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %101, i32 0, i32 6
  %102 = load i32, i32* %rowskip66, align 4
  %mul67 = mul nsw i32 %100, %102
  store i32 %mul67, i32* %srow, align 4
  %call68 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ax1)
  %m_solveSwingLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  %103 = load i8, i8* %m_solveSwingLimit, align 2
  %tobool69 = trunc i8 %103 to i1
  br i1 %tobool69, label %if.then70, label %if.end215

if.then70:                                        ; preds = %for.end
  %104 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis71 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %104, i32 0, i32 3
  %105 = load float*, float** %m_J1angularAxis71, align 4
  store float* %105, float** %J1, align 4
  %106 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis72 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %106, i32 0, i32 5
  %107 = load float*, float** %m_J2angularAxis72, align 4
  store float* %107, float** %J2, align 4
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %108 = load float, float* %m_swingSpan1, align 4
  %m_fixThresh = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %109 = load float, float* %m_fixThresh, align 4
  %cmp73 = fcmp olt float %108, %109
  br i1 %cmp73, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.then70
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %110 = load float, float* %m_swingSpan2, align 4
  %m_fixThresh74 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 11
  %111 = load float, float* %m_fixThresh74, align 4
  %cmp75 = fcmp olt float %110, %111
  br i1 %cmp75, label %if.then76, label %if.else

if.then76:                                        ; preds = %land.lhs.true
  %112 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %m_rbAFrame77 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trA, %class.btTransform* %112, %class.btTransform* nonnull align 4 dereferenceable(64) %m_rbAFrame77)
  %call78 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %trA)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %p, %class.btMatrix3x3* %call78, i32 1)
  %call79 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %trA)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %q, %class.btMatrix3x3* %call79, i32 2)
  %113 = load i32, i32* %srow, align 4
  %114 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip80 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %114, i32 0, i32 6
  %115 = load i32, i32* %rowskip80, align 4
  %add81 = add nsw i32 %113, %115
  store i32 %add81, i32* %srow1, align 4
  %call82 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx83 = getelementptr inbounds float, float* %call82, i32 0
  %116 = load float, float* %arrayidx83, align 4
  %117 = load float*, float** %J1, align 4
  %118 = load i32, i32* %srow, align 4
  %add84 = add nsw i32 %118, 0
  %arrayidx85 = getelementptr inbounds float, float* %117, i32 %add84
  store float %116, float* %arrayidx85, align 4
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 1
  %119 = load float, float* %arrayidx87, align 4
  %120 = load float*, float** %J1, align 4
  %121 = load i32, i32* %srow, align 4
  %add88 = add nsw i32 %121, 1
  %arrayidx89 = getelementptr inbounds float, float* %120, i32 %add88
  store float %119, float* %arrayidx89, align 4
  %call90 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx91 = getelementptr inbounds float, float* %call90, i32 2
  %122 = load float, float* %arrayidx91, align 4
  %123 = load float*, float** %J1, align 4
  %124 = load i32, i32* %srow, align 4
  %add92 = add nsw i32 %124, 2
  %arrayidx93 = getelementptr inbounds float, float* %123, i32 %add92
  store float %122, float* %arrayidx93, align 4
  %call94 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx95 = getelementptr inbounds float, float* %call94, i32 0
  %125 = load float, float* %arrayidx95, align 4
  %126 = load float*, float** %J1, align 4
  %127 = load i32, i32* %srow1, align 4
  %add96 = add nsw i32 %127, 0
  %arrayidx97 = getelementptr inbounds float, float* %126, i32 %add96
  store float %125, float* %arrayidx97, align 4
  %call98 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx99 = getelementptr inbounds float, float* %call98, i32 1
  %128 = load float, float* %arrayidx99, align 4
  %129 = load float*, float** %J1, align 4
  %130 = load i32, i32* %srow1, align 4
  %add100 = add nsw i32 %130, 1
  %arrayidx101 = getelementptr inbounds float, float* %129, i32 %add100
  store float %128, float* %arrayidx101, align 4
  %call102 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx103 = getelementptr inbounds float, float* %call102, i32 2
  %131 = load float, float* %arrayidx103, align 4
  %132 = load float*, float** %J1, align 4
  %133 = load i32, i32* %srow1, align 4
  %add104 = add nsw i32 %133, 2
  %arrayidx105 = getelementptr inbounds float, float* %132, i32 %add104
  store float %131, float* %arrayidx105, align 4
  %call106 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx107 = getelementptr inbounds float, float* %call106, i32 0
  %134 = load float, float* %arrayidx107, align 4
  %fneg = fneg float %134
  %135 = load float*, float** %J2, align 4
  %136 = load i32, i32* %srow, align 4
  %add108 = add nsw i32 %136, 0
  %arrayidx109 = getelementptr inbounds float, float* %135, i32 %add108
  store float %fneg, float* %arrayidx109, align 4
  %call110 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx111 = getelementptr inbounds float, float* %call110, i32 1
  %137 = load float, float* %arrayidx111, align 4
  %fneg112 = fneg float %137
  %138 = load float*, float** %J2, align 4
  %139 = load i32, i32* %srow, align 4
  %add113 = add nsw i32 %139, 1
  %arrayidx114 = getelementptr inbounds float, float* %138, i32 %add113
  store float %fneg112, float* %arrayidx114, align 4
  %call115 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx116 = getelementptr inbounds float, float* %call115, i32 2
  %140 = load float, float* %arrayidx116, align 4
  %fneg117 = fneg float %140
  %141 = load float*, float** %J2, align 4
  %142 = load i32, i32* %srow, align 4
  %add118 = add nsw i32 %142, 2
  %arrayidx119 = getelementptr inbounds float, float* %141, i32 %add118
  store float %fneg117, float* %arrayidx119, align 4
  %call120 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx121 = getelementptr inbounds float, float* %call120, i32 0
  %143 = load float, float* %arrayidx121, align 4
  %fneg122 = fneg float %143
  %144 = load float*, float** %J2, align 4
  %145 = load i32, i32* %srow1, align 4
  %add123 = add nsw i32 %145, 0
  %arrayidx124 = getelementptr inbounds float, float* %144, i32 %add123
  store float %fneg122, float* %arrayidx124, align 4
  %call125 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx126 = getelementptr inbounds float, float* %call125, i32 1
  %146 = load float, float* %arrayidx126, align 4
  %fneg127 = fneg float %146
  %147 = load float*, float** %J2, align 4
  %148 = load i32, i32* %srow1, align 4
  %add128 = add nsw i32 %148, 1
  %arrayidx129 = getelementptr inbounds float, float* %147, i32 %add128
  store float %fneg127, float* %arrayidx129, align 4
  %call130 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx131 = getelementptr inbounds float, float* %call130, i32 2
  %149 = load float, float* %arrayidx131, align 4
  %fneg132 = fneg float %149
  %150 = load float*, float** %J2, align 4
  %151 = load i32, i32* %srow1, align 4
  %add133 = add nsw i32 %151, 2
  %arrayidx134 = getelementptr inbounds float, float* %150, i32 %add133
  store float %fneg132, float* %arrayidx134, align 4
  %152 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps135 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %152, i32 0, i32 0
  %153 = load float, float* %fps135, align 4
  %m_relaxationFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 6
  %154 = load float, float* %m_relaxationFactor, align 4
  %mul136 = fmul float %153, %154
  store float %mul136, float* %fact, align 4
  %155 = load float, float* %fact, align 4
  %m_swingAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call137 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_swingAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  %mul138 = fmul float %155, %call137
  %156 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError139 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %156, i32 0, i32 7
  %157 = load float*, float** %m_constraintError139, align 4
  %158 = load i32, i32* %srow, align 4
  %arrayidx140 = getelementptr inbounds float, float* %157, i32 %158
  store float %mul138, float* %arrayidx140, align 4
  %159 = load float, float* %fact, align 4
  %m_swingAxis141 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call142 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_swingAxis141, %class.btVector3* nonnull align 4 dereferenceable(16) %q)
  %mul143 = fmul float %159, %call142
  %160 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError144 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %160, i32 0, i32 7
  %161 = load float*, float** %m_constraintError144, align 4
  %162 = load i32, i32* %srow1, align 4
  %arrayidx145 = getelementptr inbounds float, float* %161, i32 %162
  store float %mul143, float* %arrayidx145, align 4
  %163 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit146 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %163, i32 0, i32 9
  %164 = load float*, float** %m_lowerLimit146, align 4
  %165 = load i32, i32* %srow, align 4
  %arrayidx147 = getelementptr inbounds float, float* %164, i32 %165
  store float 0xC7EFFFFFE0000000, float* %arrayidx147, align 4
  %166 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit148 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %166, i32 0, i32 10
  %167 = load float*, float** %m_upperLimit148, align 4
  %168 = load i32, i32* %srow, align 4
  %arrayidx149 = getelementptr inbounds float, float* %167, i32 %168
  store float 0x47EFFFFFE0000000, float* %arrayidx149, align 4
  %169 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit150 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %169, i32 0, i32 9
  %170 = load float*, float** %m_lowerLimit150, align 4
  %171 = load i32, i32* %srow1, align 4
  %arrayidx151 = getelementptr inbounds float, float* %170, i32 %171
  store float 0xC7EFFFFFE0000000, float* %arrayidx151, align 4
  %172 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit152 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %172, i32 0, i32 10
  %173 = load float*, float** %m_upperLimit152, align 4
  %174 = load i32, i32* %srow1, align 4
  %arrayidx153 = getelementptr inbounds float, float* %173, i32 %174
  store float 0x47EFFFFFE0000000, float* %arrayidx153, align 4
  %175 = load i32, i32* %srow1, align 4
  %176 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip154 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %176, i32 0, i32 6
  %177 = load i32, i32* %rowskip154, align 4
  %add155 = add nsw i32 %175, %177
  store i32 %add155, i32* %srow, align 4
  br label %if.end214

if.else:                                          ; preds = %land.lhs.true, %if.then70
  %m_swingAxis157 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %m_relaxationFactor158 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp156, %class.btVector3* nonnull align 4 dereferenceable(16) %m_swingAxis157, float* nonnull align 4 dereferenceable(4) %m_relaxationFactor158)
  %m_relaxationFactor159 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp156, float* nonnull align 4 dereferenceable(4) %m_relaxationFactor159)
  %178 = bitcast %class.btVector3* %ax1 to i8*
  %179 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %178, i8* align 4 %179, i32 16, i1 false)
  %call160 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx161 = getelementptr inbounds float, float* %call160, i32 0
  %180 = load float, float* %arrayidx161, align 4
  %181 = load float*, float** %J1, align 4
  %182 = load i32, i32* %srow, align 4
  %add162 = add nsw i32 %182, 0
  %arrayidx163 = getelementptr inbounds float, float* %181, i32 %add162
  store float %180, float* %arrayidx163, align 4
  %call164 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx165 = getelementptr inbounds float, float* %call164, i32 1
  %183 = load float, float* %arrayidx165, align 4
  %184 = load float*, float** %J1, align 4
  %185 = load i32, i32* %srow, align 4
  %add166 = add nsw i32 %185, 1
  %arrayidx167 = getelementptr inbounds float, float* %184, i32 %add166
  store float %183, float* %arrayidx167, align 4
  %call168 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx169 = getelementptr inbounds float, float* %call168, i32 2
  %186 = load float, float* %arrayidx169, align 4
  %187 = load float*, float** %J1, align 4
  %188 = load i32, i32* %srow, align 4
  %add170 = add nsw i32 %188, 2
  %arrayidx171 = getelementptr inbounds float, float* %187, i32 %add170
  store float %186, float* %arrayidx171, align 4
  %call172 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx173 = getelementptr inbounds float, float* %call172, i32 0
  %189 = load float, float* %arrayidx173, align 4
  %fneg174 = fneg float %189
  %190 = load float*, float** %J2, align 4
  %191 = load i32, i32* %srow, align 4
  %add175 = add nsw i32 %191, 0
  %arrayidx176 = getelementptr inbounds float, float* %190, i32 %add175
  store float %fneg174, float* %arrayidx176, align 4
  %call177 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx178 = getelementptr inbounds float, float* %call177, i32 1
  %192 = load float, float* %arrayidx178, align 4
  %fneg179 = fneg float %192
  %193 = load float*, float** %J2, align 4
  %194 = load i32, i32* %srow, align 4
  %add180 = add nsw i32 %194, 1
  %arrayidx181 = getelementptr inbounds float, float* %193, i32 %add180
  store float %fneg179, float* %arrayidx181, align 4
  %call182 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx183 = getelementptr inbounds float, float* %call182, i32 2
  %195 = load float, float* %arrayidx183, align 4
  %fneg184 = fneg float %195
  %196 = load float*, float** %J2, align 4
  %197 = load i32, i32* %srow, align 4
  %add185 = add nsw i32 %197, 2
  %arrayidx186 = getelementptr inbounds float, float* %196, i32 %add185
  store float %fneg184, float* %arrayidx186, align 4
  %198 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps188 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %198, i32 0, i32 0
  %199 = load float, float* %fps188, align 4
  %m_biasFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 5
  %200 = load float, float* %m_biasFactor, align 4
  %mul189 = fmul float %199, %200
  store float %mul189, float* %k187, align 4
  %201 = load float, float* %k187, align 4
  %m_swingCorrection = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 17
  %202 = load float, float* %m_swingCorrection, align 4
  %mul190 = fmul float %201, %202
  %203 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError191 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %203, i32 0, i32 7
  %204 = load float*, float** %m_constraintError191, align 4
  %205 = load i32, i32* %srow, align 4
  %arrayidx192 = getelementptr inbounds float, float* %204, i32 %205
  store float %mul190, float* %arrayidx192, align 4
  %m_flags193 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 34
  %206 = load i32, i32* %m_flags193, align 4
  %and194 = and i32 %206, 4
  %tobool195 = icmp ne i32 %and194, 0
  br i1 %tobool195, label %if.then196, label %if.end199

if.then196:                                       ; preds = %if.else
  %m_angCFM = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 37
  %207 = load float, float* %m_angCFM, align 4
  %208 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm197 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %208, i32 0, i32 8
  %209 = load float*, float** %cfm197, align 4
  %210 = load i32, i32* %srow, align 4
  %arrayidx198 = getelementptr inbounds float, float* %209, i32 %210
  store float %207, float* %arrayidx198, align 4
  br label %if.end199

if.end199:                                        ; preds = %if.then196, %if.else
  %211 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit200 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %211, i32 0, i32 9
  %212 = load float*, float** %m_lowerLimit200, align 4
  %213 = load i32, i32* %srow, align 4
  %arrayidx201 = getelementptr inbounds float, float* %212, i32 %213
  store float 0.000000e+00, float* %arrayidx201, align 4
  %m_bMotorEnabled = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 29
  %214 = load i8, i8* %m_bMotorEnabled, align 4
  %tobool202 = trunc i8 %214 to i1
  br i1 %tobool202, label %land.lhs.true203, label %cond.false207

land.lhs.true203:                                 ; preds = %if.end199
  %m_maxMotorImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 32
  %215 = load float, float* %m_maxMotorImpulse, align 4
  %cmp204 = fcmp oge float %215, 0.000000e+00
  br i1 %cmp204, label %cond.true205, label %cond.false207

cond.true205:                                     ; preds = %land.lhs.true203
  %m_maxMotorImpulse206 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 32
  %216 = load float, float* %m_maxMotorImpulse206, align 4
  br label %cond.end208

cond.false207:                                    ; preds = %land.lhs.true203, %if.end199
  br label %cond.end208

cond.end208:                                      ; preds = %cond.false207, %cond.true205
  %cond209 = phi float [ %216, %cond.true205 ], [ 0x47EFFFFFE0000000, %cond.false207 ]
  %217 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit210 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %217, i32 0, i32 10
  %218 = load float*, float** %m_upperLimit210, align 4
  %219 = load i32, i32* %srow, align 4
  %arrayidx211 = getelementptr inbounds float, float* %218, i32 %219
  store float %cond209, float* %arrayidx211, align 4
  %220 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip212 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %220, i32 0, i32 6
  %221 = load i32, i32* %rowskip212, align 4
  %222 = load i32, i32* %srow, align 4
  %add213 = add nsw i32 %222, %221
  store i32 %add213, i32* %srow, align 4
  br label %if.end214

if.end214:                                        ; preds = %cond.end208, %if.then76
  br label %if.end215

if.end215:                                        ; preds = %if.end214, %for.end
  %m_solveTwistLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 23
  %223 = load i8, i8* %m_solveTwistLimit, align 1
  %tobool216 = trunc i8 %223 to i1
  br i1 %tobool216, label %if.then217, label %if.end291

if.then217:                                       ; preds = %if.end215
  %m_twistAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %m_relaxationFactor220 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp219, %class.btVector3* nonnull align 4 dereferenceable(16) %m_twistAxis, float* nonnull align 4 dereferenceable(4) %m_relaxationFactor220)
  %m_relaxationFactor221 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp218, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp219, float* nonnull align 4 dereferenceable(4) %m_relaxationFactor221)
  %224 = bitcast %class.btVector3* %ax1 to i8*
  %225 = bitcast %class.btVector3* %ref.tmp218 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %224, i8* align 4 %225, i32 16, i1 false)
  %226 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis223 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %226, i32 0, i32 3
  %227 = load float*, float** %m_J1angularAxis223, align 4
  store float* %227, float** %J1222, align 4
  %228 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis225 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %228, i32 0, i32 5
  %229 = load float*, float** %m_J2angularAxis225, align 4
  store float* %229, float** %J2224, align 4
  %call226 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx227 = getelementptr inbounds float, float* %call226, i32 0
  %230 = load float, float* %arrayidx227, align 4
  %231 = load float*, float** %J1222, align 4
  %232 = load i32, i32* %srow, align 4
  %add228 = add nsw i32 %232, 0
  %arrayidx229 = getelementptr inbounds float, float* %231, i32 %add228
  store float %230, float* %arrayidx229, align 4
  %call230 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx231 = getelementptr inbounds float, float* %call230, i32 1
  %233 = load float, float* %arrayidx231, align 4
  %234 = load float*, float** %J1222, align 4
  %235 = load i32, i32* %srow, align 4
  %add232 = add nsw i32 %235, 1
  %arrayidx233 = getelementptr inbounds float, float* %234, i32 %add232
  store float %233, float* %arrayidx233, align 4
  %call234 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx235 = getelementptr inbounds float, float* %call234, i32 2
  %236 = load float, float* %arrayidx235, align 4
  %237 = load float*, float** %J1222, align 4
  %238 = load i32, i32* %srow, align 4
  %add236 = add nsw i32 %238, 2
  %arrayidx237 = getelementptr inbounds float, float* %237, i32 %add236
  store float %236, float* %arrayidx237, align 4
  %call238 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx239 = getelementptr inbounds float, float* %call238, i32 0
  %239 = load float, float* %arrayidx239, align 4
  %fneg240 = fneg float %239
  %240 = load float*, float** %J2224, align 4
  %241 = load i32, i32* %srow, align 4
  %add241 = add nsw i32 %241, 0
  %arrayidx242 = getelementptr inbounds float, float* %240, i32 %add241
  store float %fneg240, float* %arrayidx242, align 4
  %call243 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx244 = getelementptr inbounds float, float* %call243, i32 1
  %242 = load float, float* %arrayidx244, align 4
  %fneg245 = fneg float %242
  %243 = load float*, float** %J2224, align 4
  %244 = load i32, i32* %srow, align 4
  %add246 = add nsw i32 %244, 1
  %arrayidx247 = getelementptr inbounds float, float* %243, i32 %add246
  store float %fneg245, float* %arrayidx247, align 4
  %call248 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx249 = getelementptr inbounds float, float* %call248, i32 2
  %245 = load float, float* %arrayidx249, align 4
  %fneg250 = fneg float %245
  %246 = load float*, float** %J2224, align 4
  %247 = load i32, i32* %srow, align 4
  %add251 = add nsw i32 %247, 2
  %arrayidx252 = getelementptr inbounds float, float* %246, i32 %add251
  store float %fneg250, float* %arrayidx252, align 4
  %248 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps254 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %248, i32 0, i32 0
  %249 = load float, float* %fps254, align 4
  %m_biasFactor255 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 5
  %250 = load float, float* %m_biasFactor255, align 4
  %mul256 = fmul float %249, %250
  store float %mul256, float* %k253, align 4
  %251 = load float, float* %k253, align 4
  %m_twistCorrection = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 18
  %252 = load float, float* %m_twistCorrection, align 4
  %mul257 = fmul float %251, %252
  %253 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError258 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %253, i32 0, i32 7
  %254 = load float*, float** %m_constraintError258, align 4
  %255 = load i32, i32* %srow, align 4
  %arrayidx259 = getelementptr inbounds float, float* %254, i32 %255
  store float %mul257, float* %arrayidx259, align 4
  %m_flags260 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 34
  %256 = load i32, i32* %m_flags260, align 4
  %and261 = and i32 %256, 4
  %tobool262 = icmp ne i32 %and261, 0
  br i1 %tobool262, label %if.then263, label %if.end267

if.then263:                                       ; preds = %if.then217
  %m_angCFM264 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 37
  %257 = load float, float* %m_angCFM264, align 4
  %258 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm265 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %258, i32 0, i32 8
  %259 = load float*, float** %cfm265, align 4
  %260 = load i32, i32* %srow, align 4
  %arrayidx266 = getelementptr inbounds float, float* %259, i32 %260
  store float %257, float* %arrayidx266, align 4
  br label %if.end267

if.end267:                                        ; preds = %if.then263, %if.then217
  %m_twistSpan = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %261 = load float, float* %m_twistSpan, align 4
  %cmp268 = fcmp ogt float %261, 0.000000e+00
  br i1 %cmp268, label %if.then269, label %if.else283

if.then269:                                       ; preds = %if.end267
  %m_twistCorrection270 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 18
  %262 = load float, float* %m_twistCorrection270, align 4
  %cmp271 = fcmp ogt float %262, 0.000000e+00
  br i1 %cmp271, label %if.then272, label %if.else277

if.then272:                                       ; preds = %if.then269
  %263 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit273 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %263, i32 0, i32 9
  %264 = load float*, float** %m_lowerLimit273, align 4
  %265 = load i32, i32* %srow, align 4
  %arrayidx274 = getelementptr inbounds float, float* %264, i32 %265
  store float 0.000000e+00, float* %arrayidx274, align 4
  %266 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit275 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %266, i32 0, i32 10
  %267 = load float*, float** %m_upperLimit275, align 4
  %268 = load i32, i32* %srow, align 4
  %arrayidx276 = getelementptr inbounds float, float* %267, i32 %268
  store float 0x47EFFFFFE0000000, float* %arrayidx276, align 4
  br label %if.end282

if.else277:                                       ; preds = %if.then269
  %269 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit278 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %269, i32 0, i32 9
  %270 = load float*, float** %m_lowerLimit278, align 4
  %271 = load i32, i32* %srow, align 4
  %arrayidx279 = getelementptr inbounds float, float* %270, i32 %271
  store float 0xC7EFFFFFE0000000, float* %arrayidx279, align 4
  %272 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit280 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %272, i32 0, i32 10
  %273 = load float*, float** %m_upperLimit280, align 4
  %274 = load i32, i32* %srow, align 4
  %arrayidx281 = getelementptr inbounds float, float* %273, i32 %274
  store float 0.000000e+00, float* %arrayidx281, align 4
  br label %if.end282

if.end282:                                        ; preds = %if.else277, %if.then272
  br label %if.end288

if.else283:                                       ; preds = %if.end267
  %275 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit284 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %275, i32 0, i32 9
  %276 = load float*, float** %m_lowerLimit284, align 4
  %277 = load i32, i32* %srow, align 4
  %arrayidx285 = getelementptr inbounds float, float* %276, i32 %277
  store float 0xC7EFFFFFE0000000, float* %arrayidx285, align 4
  %278 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit286 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %278, i32 0, i32 10
  %279 = load float*, float** %m_upperLimit286, align 4
  %280 = load i32, i32* %srow, align 4
  %arrayidx287 = getelementptr inbounds float, float* %279, i32 %280
  store float 0x47EFFFFFE0000000, float* %arrayidx287, align 4
  br label %if.end288

if.end288:                                        ; preds = %if.else283, %if.end282
  %281 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip289 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %281, i32 0, i32 6
  %282 = load i32, i32* %rowskip289, align 4
  %283 = load i32, i32* %srow, align 4
  %add290 = add nsw i32 %283, %282
  store i32 %add290, i32* %srow, align 4
  br label %if.end291

if.end291:                                        ; preds = %if.end288, %if.end215
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %this, %class.btVector3* %v0, %class.btVector3* %v1, %class.btVector3* %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this1)
  %1 = load float, float* %call, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp2, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this1)
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %call3)
  %2 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this1)
  store float 0.000000e+00, float* %ref.tmp5, align 4
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this1)
  %3 = load float, float* %call7, align 4
  %fneg8 = fneg float %3
  store float %fneg8, float* %ref.tmp6, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %2, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %4 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this1)
  %5 = load float, float* %call10, align 4
  %fneg11 = fneg float %5
  store float %fneg11, float* %ref.tmp9, align 4
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this1)
  store float 0.000000e+00, float* %ref.tmp13, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %4, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %call12, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %1 = load i32, i32* %i.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 2
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %2 = load i32, i32* %i.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %2
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %arrayidx2, float* nonnull align 4 dereferenceable(4) %arrayidx6, float* nonnull align 4 dereferenceable(4) %arrayidx10)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline optnone
define hidden void @_ZN21btConeTwistConstraint13buildJacobianEv(%class.btConeTwistConstraint* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %pivotAInW = alloca %class.btVector3, align 4
  %pivotBInW = alloca %class.btVector3, align 4
  %relPos = alloca %class.btVector3, align 4
  %normal = alloca [3 x %class.btVector3], align 16
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %i = alloca i32, align 4
  %ref.tmp24 = alloca %class.btMatrix3x3, align 4
  %ref.tmp28 = alloca %class.btMatrix3x3, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp35 = alloca %class.btVector3, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 25
  %0 = load i8, i8* %m_useSolveConstraintObsolete, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end57

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_appliedImpulse = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %1, i32 0, i32 10
  store float 0.000000e+00, float* %m_appliedImpulse, align 4
  %m_accTwistLimitImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 21
  store float 0.000000e+00, float* %m_accTwistLimitImpulse, align 4
  %m_accSwingLimitImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 20
  store float 0.000000e+00, float* %m_accSwingLimitImpulse, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %m_accMotorImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 33
  %2 = bitcast %class.btVector3* %m_accMotorImpulse to i8*
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %m_angularOnly = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 22
  %4 = load i8, i8* %m_angularOnly, align 4
  %tobool5 = trunc i8 %4 to i1
  br i1 %tobool5, label %if.end48, label %if.then6

if.then6:                                         ; preds = %if.then
  %5 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %5, i32 0, i32 8
  %6 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call7 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %6)
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbAFrame)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %pivotAInW, %class.btTransform* %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %call8)
  %7 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %7, i32 0, i32 9
  %8 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %call9 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %8)
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbBFrame)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %pivotBInW, %class.btTransform* %call9, %class.btVector3* nonnull align 4 dereferenceable(16) %call10)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %relPos, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBInW, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAInW)
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %if.then6
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %if.then6 ], [ %arrayctor.next, %arrayctor.loop ]
  %call11 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %call12 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %relPos)
  %cmp = fcmp ogt float %call12, 0x3E80000000000000
  br i1 %cmp, label %if.then13, label %if.else

if.then13:                                        ; preds = %arrayctor.cont
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* %relPos)
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 0
  %9 = bitcast %class.btVector3* %arrayidx to i8*
  %10 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %9, i8* align 4 %10, i32 16, i1 false)
  br label %if.end

if.else:                                          ; preds = %arrayctor.cont
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 0
  store float 1.000000e+00, float* %ref.tmp16, align 4
  store float 0.000000e+00, float* %ref.tmp17, align 4
  store float 0.000000e+00, float* %ref.tmp18, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then13
  %arrayidx19 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 1
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 2
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx19, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx20, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx21)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %11 = load i32, i32* %i, align 4
  %cmp22 = icmp slt i32 %11, 3
  br i1 %cmp22, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_jac = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 1
  %12 = load i32, i32* %i, align 4
  %arrayidx23 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 %12
  %13 = bitcast %class.btJacobianEntry* %arrayidx23 to i8*
  %14 = bitcast i8* %13 to %class.btJacobianEntry*
  %15 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA25 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %15, i32 0, i32 8
  %16 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA25, align 4
  %call26 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %16)
  %call27 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call26)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp24, %class.btMatrix3x3* %call27)
  %17 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB29 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %17, i32 0, i32 9
  %18 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB29, align 4
  %call30 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %18)
  %call31 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call30)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp28, %class.btMatrix3x3* %call31)
  %19 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA33 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %19, i32 0, i32 8
  %20 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA33, align 4
  %call34 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %20)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp32, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAInW, %class.btVector3* nonnull align 4 dereferenceable(16) %call34)
  %21 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB36 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %21, i32 0, i32 9
  %22 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB36, align 4
  %call37 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %22)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp35, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBInW, %class.btVector3* nonnull align 4 dereferenceable(16) %call37)
  %23 = load i32, i32* %i, align 4
  %arrayidx38 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 %23
  %24 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA39 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %24, i32 0, i32 8
  %25 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA39, align 4
  %call40 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %25)
  %26 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA41 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %26, i32 0, i32 8
  %27 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA41, align 4
  %call42 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %27)
  %28 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB43 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %28, i32 0, i32 9
  %29 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB43, align 4
  %call44 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %29)
  %30 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB45 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %30, i32 0, i32 9
  %31 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB45, align 4
  %call46 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %31)
  %call47 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f(%class.btJacobianEntry* %14, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp24, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp28, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp32, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp35, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx38, %class.btVector3* nonnull align 4 dereferenceable(16) %call40, float %call42, %class.btVector3* nonnull align 4 dereferenceable(16) %call44, float %call46)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %32 = load i32, i32* %i, align 4
  %inc = add nsw i32 %32, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end48

if.end48:                                         ; preds = %for.end, %if.then
  %33 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA49 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %33, i32 0, i32 8
  %34 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA49, align 4
  %call50 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %34)
  %35 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB51 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %35, i32 0, i32 9
  %36 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB51, align 4
  %call52 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %36)
  %37 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA53 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %37, i32 0, i32 8
  %38 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA53, align 4
  %call54 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %38)
  %39 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB55 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %39, i32 0, i32 9
  %40 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB55, align 4
  %call56 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %40)
  call void @_ZN21btConeTwistConstraint14calcAngleInfo2ERK11btTransformS2_RK11btMatrix3x3S5_(%class.btConeTwistConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %call50, %class.btTransform* nonnull align 4 dereferenceable(64) %call52, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call54, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call56)
  br label %if.end57

if.end57:                                         ; preds = %if.end48, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector310normalizedEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %nrm = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast %class.btVector3* %nrm to i8*
  %1 = bitcast %class.btVector3* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %nrm)
  %2 = bitcast %class.btVector3* %agg.result to i8*
  %3 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %q) #1 comdat {
entry:
  %n.addr = alloca %class.btVector3*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %q.addr = alloca %class.btVector3*, align 4
  %a = alloca float, align 4
  %k = alloca float, align 4
  %a42 = alloca float, align 4
  %k54 = alloca float, align 4
  store %class.btVector3* %n, %class.btVector3** %n.addr, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4
  store %class.btVector3* %q, %class.btVector3** %q.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 2
  %1 = load float, float* %arrayidx, align 4
  %call1 = call float @_Z6btFabsf(float %1)
  %cmp = fcmp ogt float %call1, 0x3FE6A09E60000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %2)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %4 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %4)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %mul = fmul float %3, %5
  %6 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  %7 = load float, float* %arrayidx7, align 4
  %8 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 2
  %9 = load float, float* %arrayidx9, align 4
  %mul10 = fmul float %7, %9
  %add = fadd float %mul, %mul10
  store float %add, float* %a, align 4
  %10 = load float, float* %a, align 4
  %call11 = call float @_Z6btSqrtf(float %10)
  %div = fdiv float 1.000000e+00, %call11
  store float %div, float* %k, align 4
  %11 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %11)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 0
  store float 0.000000e+00, float* %arrayidx13, align 4
  %12 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %12)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %13 = load float, float* %arrayidx15, align 4
  %fneg = fneg float %13
  %14 = load float, float* %k, align 4
  %mul16 = fmul float %fneg, %14
  %15 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %15)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  store float %mul16, float* %arrayidx18, align 4
  %16 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %16)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  %17 = load float, float* %arrayidx20, align 4
  %18 = load float, float* %k, align 4
  %mul21 = fmul float %17, %18
  %19 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %19)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 2
  store float %mul21, float* %arrayidx23, align 4
  %20 = load float, float* %a, align 4
  %21 = load float, float* %k, align 4
  %mul24 = fmul float %20, %21
  %22 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %22)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 0
  store float %mul24, float* %arrayidx26, align 4
  %23 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call27 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %23)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 0
  %24 = load float, float* %arrayidx28, align 4
  %fneg29 = fneg float %24
  %25 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %25)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %26 = load float, float* %arrayidx31, align 4
  %mul32 = fmul float %fneg29, %26
  %27 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %27)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 1
  store float %mul32, float* %arrayidx34, align 4
  %28 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call35 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %28)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 0
  %29 = load float, float* %arrayidx36, align 4
  %30 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %30)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 1
  %31 = load float, float* %arrayidx38, align 4
  %mul39 = fmul float %29, %31
  %32 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %32)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 2
  store float %mul39, float* %arrayidx41, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %33 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %33)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 0
  %34 = load float, float* %arrayidx44, align 4
  %35 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call45 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %35)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 0
  %36 = load float, float* %arrayidx46, align 4
  %mul47 = fmul float %34, %36
  %37 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call48 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %37)
  %arrayidx49 = getelementptr inbounds float, float* %call48, i32 1
  %38 = load float, float* %arrayidx49, align 4
  %39 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %39)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 1
  %40 = load float, float* %arrayidx51, align 4
  %mul52 = fmul float %38, %40
  %add53 = fadd float %mul47, %mul52
  store float %add53, float* %a42, align 4
  %41 = load float, float* %a42, align 4
  %call55 = call float @_Z6btSqrtf(float %41)
  %div56 = fdiv float 1.000000e+00, %call55
  store float %div56, float* %k54, align 4
  %42 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %42)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 1
  %43 = load float, float* %arrayidx58, align 4
  %fneg59 = fneg float %43
  %44 = load float, float* %k54, align 4
  %mul60 = fmul float %fneg59, %44
  %45 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %45)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 0
  store float %mul60, float* %arrayidx62, align 4
  %46 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call63 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %46)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 0
  %47 = load float, float* %arrayidx64, align 4
  %48 = load float, float* %k54, align 4
  %mul65 = fmul float %47, %48
  %49 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %49)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 1
  store float %mul65, float* %arrayidx67, align 4
  %50 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call68 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %50)
  %arrayidx69 = getelementptr inbounds float, float* %call68, i32 2
  store float 0.000000e+00, float* %arrayidx69, align 4
  %51 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call70 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %51)
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 2
  %52 = load float, float* %arrayidx71, align 4
  %fneg72 = fneg float %52
  %53 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %53)
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 1
  %54 = load float, float* %arrayidx74, align 4
  %mul75 = fmul float %fneg72, %54
  %55 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %55)
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 0
  store float %mul75, float* %arrayidx77, align 4
  %56 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call78 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %56)
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 2
  %57 = load float, float* %arrayidx79, align 4
  %58 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %58)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 0
  %59 = load float, float* %arrayidx81, align 4
  %mul82 = fmul float %57, %59
  %60 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %60)
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 1
  store float %mul82, float* %arrayidx84, align 4
  %61 = load float, float* %a42, align 4
  %62 = load float, float* %k54, align 4
  %mul85 = fmul float %61, %62
  %63 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %63)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 2
  store float %mul85, float* %arrayidx87, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %m_worldTransform)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaLocal = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 8
  ret %class.btVector3* %m_invInertiaLocal
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4
  ret float %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f(%class.btJacobianEntry* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2A, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2B, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvA, float %massInvA, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvB, float %massInvB) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  %world2A.addr = alloca %class.btMatrix3x3*, align 4
  %world2B.addr = alloca %class.btMatrix3x3*, align 4
  %rel_pos1.addr = alloca %class.btVector3*, align 4
  %rel_pos2.addr = alloca %class.btVector3*, align 4
  %jointAxis.addr = alloca %class.btVector3*, align 4
  %inertiaInvA.addr = alloca %class.btVector3*, align 4
  %massInvA.addr = alloca float, align 4
  %inertiaInvB.addr = alloca %class.btVector3*, align 4
  %massInvB.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4
  store %class.btMatrix3x3* %world2A, %class.btMatrix3x3** %world2A.addr, align 4
  store %class.btMatrix3x3* %world2B, %class.btMatrix3x3** %world2B.addr, align 4
  store %class.btVector3* %rel_pos1, %class.btVector3** %rel_pos1.addr, align 4
  store %class.btVector3* %rel_pos2, %class.btVector3** %rel_pos2.addr, align 4
  store %class.btVector3* %jointAxis, %class.btVector3** %jointAxis.addr, align 4
  store %class.btVector3* %inertiaInvA, %class.btVector3** %inertiaInvA.addr, align 4
  store float %massInvA, float* %massInvA.addr, align 4
  store %class.btVector3* %inertiaInvB, %class.btVector3** %inertiaInvB.addr, align 4
  store float %massInvB, float* %massInvB.addr, align 4
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %jointAxis.addr, align 4
  %1 = bitcast %class.btVector3* %m_linearJointAxis to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %m_aJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aJ)
  %m_bJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bJ)
  %m_0MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_0MinvJt)
  %m_1MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_1MinvJt)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2A.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %rel_pos1.addr, align 4
  %m_linearJointAxis6 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearJointAxis6)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %m_aJ7 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %5 = bitcast %class.btVector3* %m_aJ7 to i8*
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2B.addr, align 4
  %8 = load %class.btVector3*, %class.btVector3** %rel_pos2.addr, align 4
  %m_linearJointAxis11 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearJointAxis11)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp8, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %m_bJ12 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %9 = bitcast %class.btVector3* %m_bJ12 to i8*
  %10 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  %11 = load %class.btVector3*, %class.btVector3** %inertiaInvA.addr, align 4
  %m_aJ14 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %11, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ14)
  %m_0MinvJt15 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %12 = bitcast %class.btVector3* %m_0MinvJt15 to i8*
  %13 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  %14 = load %class.btVector3*, %class.btVector3** %inertiaInvB.addr, align 4
  %m_bJ17 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %14, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ17)
  %m_1MinvJt18 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %15 = bitcast %class.btVector3* %m_1MinvJt18 to i8*
  %16 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false)
  %17 = load float, float* %massInvA.addr, align 4
  %m_0MinvJt19 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %m_aJ20 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call21 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_0MinvJt19, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ20)
  %add = fadd float %17, %call21
  %18 = load float, float* %massInvB.addr, align 4
  %add22 = fadd float %add, %18
  %m_1MinvJt23 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %m_bJ24 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call25 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_1MinvJt23, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ24)
  %add26 = fadd float %add22, %call25
  %m_Adiag = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 5
  store float %add26, float* %m_Adiag, align 4
  ret %class.btJacobianEntry* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btConeTwistConstraint23solveConstraintObsoleteER12btSolverBodyS1_f(%class.btConeTwistConstraint* %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %bodyA, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %bodyB, float %timeStep) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %bodyA.addr = alloca %struct.btSolverBody*, align 4
  %bodyB.addr = alloca %struct.btSolverBody*, align 4
  %timeStep.addr = alloca float, align 4
  %pivotAInW = alloca %class.btVector3, align 4
  %pivotBInW = alloca %class.btVector3, align 4
  %tau = alloca float, align 4
  %rel_pos1 = alloca %class.btVector3, align 4
  %rel_pos2 = alloca %class.btVector3, align 4
  %vel1 = alloca %class.btVector3, align 4
  %vel2 = alloca %class.btVector3, align 4
  %vel = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %normal = alloca %class.btVector3*, align 4
  %jacDiagABInv = alloca float, align 4
  %rel_vel = alloca float, align 4
  %depth = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %impulse = alloca float, align 4
  %ftorqueAxis1 = alloca %class.btVector3, align 4
  %ftorqueAxis2 = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp25 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca %class.btVector3, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %trACur = alloca %class.btTransform, align 4
  %trBCur = alloca %class.btTransform, align 4
  %omegaA = alloca %class.btVector3, align 4
  %omegaB = alloca %class.btVector3, align 4
  %trAPred = alloca %class.btTransform, align 4
  %zerovec = alloca %class.btVector3, align 4
  %ref.tmp47 = alloca float, align 4
  %ref.tmp48 = alloca float, align 4
  %ref.tmp49 = alloca float, align 4
  %trBPred = alloca %class.btTransform, align 4
  %trPose = alloca %class.btTransform, align 4
  %ref.tmp52 = alloca %class.btVector3, align 4
  %ref.tmp53 = alloca float, align 4
  %ref.tmp54 = alloca float, align 4
  %ref.tmp55 = alloca float, align 4
  %trABDes = alloca %class.btTransform, align 4
  %ref.tmp58 = alloca %class.btTransform, align 4
  %ref.tmp60 = alloca %class.btTransform, align 4
  %trADes = alloca %class.btTransform, align 4
  %trBDes = alloca %class.btTransform, align 4
  %ref.tmp62 = alloca %class.btTransform, align 4
  %omegaADes = alloca %class.btVector3, align 4
  %omegaBDes = alloca %class.btVector3, align 4
  %dOmegaA = alloca %class.btVector3, align 4
  %dOmegaB = alloca %class.btVector3, align 4
  %axisA = alloca %class.btVector3, align 4
  %axisB = alloca %class.btVector3, align 4
  %kAxisAInv = alloca float, align 4
  %kAxisBInv = alloca float, align 4
  %ref.tmp70 = alloca %class.btVector3, align 4
  %ref.tmp77 = alloca %class.btVector3, align 4
  %avgAxis = alloca %class.btVector3, align 4
  %ref.tmp81 = alloca %class.btVector3, align 4
  %ref.tmp82 = alloca %class.btVector3, align 4
  %kInvCombined = alloca float, align 4
  %impulse93 = alloca %class.btVector3, align 4
  %ref.tmp94 = alloca %class.btVector3, align 4
  %ref.tmp95 = alloca %class.btVector3, align 4
  %ref.tmp96 = alloca %class.btVector3, align 4
  %ref.tmp97 = alloca float, align 4
  %fMaxImpulse = alloca float, align 4
  %newUnclampedAccImpulse = alloca %class.btVector3, align 4
  %newUnclampedMag = alloca float, align 4
  %ref.tmp111 = alloca %class.btVector3, align 4
  %impulseMag = alloca float, align 4
  %impulseAxis = alloca %class.btVector3, align 4
  %ref.tmp118 = alloca %class.btVector3, align 4
  %ref.tmp119 = alloca float, align 4
  %ref.tmp120 = alloca float, align 4
  %ref.tmp121 = alloca float, align 4
  %ref.tmp123 = alloca %class.btVector3, align 4
  %ref.tmp126 = alloca %class.btVector3, align 4
  %ref.tmp127 = alloca float, align 4
  %ref.tmp128 = alloca float, align 4
  %ref.tmp129 = alloca float, align 4
  %ref.tmp131 = alloca %class.btVector3, align 4
  %angVelA = alloca %class.btVector3, align 4
  %angVelB = alloca %class.btVector3, align 4
  %relVel = alloca %class.btVector3, align 4
  %relVelAxis = alloca %class.btVector3, align 4
  %m_kDamping = alloca float, align 4
  %impulse149 = alloca %class.btVector3, align 4
  %ref.tmp150 = alloca float, align 4
  %impulseMag153 = alloca float, align 4
  %impulseAxis155 = alloca %class.btVector3, align 4
  %ref.tmp156 = alloca %class.btVector3, align 4
  %ref.tmp157 = alloca float, align 4
  %ref.tmp158 = alloca float, align 4
  %ref.tmp159 = alloca float, align 4
  %ref.tmp161 = alloca %class.btVector3, align 4
  %ref.tmp164 = alloca %class.btVector3, align 4
  %ref.tmp165 = alloca float, align 4
  %ref.tmp166 = alloca float, align 4
  %ref.tmp167 = alloca float, align 4
  %ref.tmp169 = alloca %class.btVector3, align 4
  %angVelA176 = alloca %class.btVector3, align 4
  %angVelB178 = alloca %class.btVector3, align 4
  %amplitude = alloca float, align 4
  %relSwingVel = alloca float, align 4
  %ref.tmp185 = alloca %class.btVector3, align 4
  %impulseMag194 = alloca float, align 4
  %temp = alloca float, align 4
  %ref.tmp196 = alloca float, align 4
  %ref.tmp199 = alloca float, align 4
  %impulse204 = alloca %class.btVector3, align 4
  %impulseTwistCouple = alloca %class.btVector3, align 4
  %ref.tmp206 = alloca float, align 4
  %impulseNoTwistCouple = alloca %class.btVector3, align 4
  %noTwistSwingAxis = alloca %class.btVector3, align 4
  %ref.tmp210 = alloca %class.btVector3, align 4
  %ref.tmp211 = alloca float, align 4
  %ref.tmp212 = alloca float, align 4
  %ref.tmp213 = alloca float, align 4
  %ref.tmp215 = alloca %class.btVector3, align 4
  %ref.tmp218 = alloca %class.btVector3, align 4
  %ref.tmp219 = alloca float, align 4
  %ref.tmp220 = alloca float, align 4
  %ref.tmp221 = alloca float, align 4
  %ref.tmp223 = alloca %class.btVector3, align 4
  %amplitude230 = alloca float, align 4
  %relTwistVel = alloca float, align 4
  %ref.tmp235 = alloca %class.btVector3, align 4
  %impulseMag245 = alloca float, align 4
  %temp247 = alloca float, align 4
  %ref.tmp248 = alloca float, align 4
  %ref.tmp251 = alloca float, align 4
  %ref.tmp256 = alloca %class.btVector3, align 4
  %ref.tmp257 = alloca float, align 4
  %ref.tmp258 = alloca float, align 4
  %ref.tmp259 = alloca float, align 4
  %ref.tmp261 = alloca %class.btVector3, align 4
  %ref.tmp265 = alloca %class.btVector3, align 4
  %ref.tmp266 = alloca float, align 4
  %ref.tmp267 = alloca float, align 4
  %ref.tmp268 = alloca float, align 4
  %ref.tmp270 = alloca %class.btVector3, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store %struct.btSolverBody* %bodyA, %struct.btSolverBody** %bodyA.addr, align 4
  store %struct.btSolverBody* %bodyB, %struct.btSolverBody** %bodyB.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 25
  %0 = load i8, i8* %m_useSolveConstraintObsolete, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end276

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %1, i32 0, i32 8
  %2 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %2)
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbAFrame)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %pivotAInW, %class.btTransform* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  %3 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %3, i32 0, i32 9
  %4 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %4)
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbBFrame)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %pivotBInW, %class.btTransform* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call4)
  store float 0x3FD3333340000000, float* %tau, align 4
  %m_angularOnly = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 22
  %5 = load i8, i8* %m_angularOnly, align 4
  %tobool5 = trunc i8 %5 to i1
  br i1 %tobool5, label %if.end, label %if.then6

if.then6:                                         ; preds = %if.then
  %6 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA7 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %6, i32 0, i32 8
  %7 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA7, align 4
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %7)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAInW, %class.btVector3* nonnull align 4 dereferenceable(16) %call8)
  %8 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB9 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %8, i32 0, i32 9
  %9 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB9, align 4
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %9)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBInW, %class.btVector3* nonnull align 4 dereferenceable(16) %call10)
  %call11 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel1)
  %10 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA.addr, align 4
  call void @_ZNK12btSolverBody39internalGetVelocityInLocalPointObsoleteERK9btVector3RS0_(%struct.btSolverBody* %10, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %vel1)
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel2)
  %11 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB.addr, align 4
  call void @_ZNK12btSolverBody39internalGetVelocityInLocalPointObsoleteERK9btVector3RS0_(%struct.btSolverBody* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %vel2)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %vel, %class.btVector3* nonnull align 4 dereferenceable(16) %vel1, %class.btVector3* nonnull align 4 dereferenceable(16) %vel2)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then6
  %12 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %12, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_jac = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 1
  %13 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 %13
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayidx, i32 0, i32 0
  store %class.btVector3* %m_linearJointAxis, %class.btVector3** %normal, align 4
  %m_jac13 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 1
  %14 = load i32, i32* %i, align 4
  %arrayidx14 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac13, i32 0, i32 %14
  %call15 = call float @_ZNK15btJacobianEntry11getDiagonalEv(%class.btJacobianEntry* %arrayidx14)
  %div = fdiv float 1.000000e+00, %call15
  store float %div, float* %jacDiagABInv, align 4
  %15 = load %class.btVector3*, %class.btVector3** %normal, align 4
  %call16 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %vel)
  store float %call16, float* %rel_vel, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAInW, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBInW)
  %16 = load %class.btVector3*, %class.btVector3** %normal, align 4
  %call17 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %16)
  %fneg = fneg float %call17
  store float %fneg, float* %depth, align 4
  %17 = load float, float* %depth, align 4
  %18 = load float, float* %tau, align 4
  %mul = fmul float %17, %18
  %19 = load float, float* %timeStep.addr, align 4
  %div18 = fdiv float %mul, %19
  %20 = load float, float* %jacDiagABInv, align 4
  %mul19 = fmul float %div18, %20
  %21 = load float, float* %rel_vel, align 4
  %22 = load float, float* %jacDiagABInv, align 4
  %mul20 = fmul float %21, %22
  %sub = fsub float %mul19, %mul20
  store float %sub, float* %impulse, align 4
  %23 = load float, float* %impulse, align 4
  %24 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_appliedImpulse = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %24, i32 0, i32 10
  %25 = load float, float* %m_appliedImpulse, align 4
  %add = fadd float %25, %23
  store float %add, float* %m_appliedImpulse, align 4
  %26 = load %class.btVector3*, %class.btVector3** %normal, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ftorqueAxis1, %class.btVector3* %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %26)
  %27 = load %class.btVector3*, %class.btVector3** %normal, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ftorqueAxis2, %class.btVector3* %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %27)
  %28 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA.addr, align 4
  %29 = load %class.btVector3*, %class.btVector3** %normal, align 4
  %30 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA23 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %30, i32 0, i32 8
  %31 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA23, align 4
  %call24 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %31)
  store float %call24, float* %ref.tmp22, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %29, float* nonnull align 4 dereferenceable(4) %ref.tmp22)
  %32 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA26 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %32, i32 0, i32 8
  %33 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA26, align 4
  %call27 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %33)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp25, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call27, %class.btVector3* nonnull align 4 dereferenceable(16) %ftorqueAxis1)
  %34 = load float, float* %impulse, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %28, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp25, float %34)
  %35 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB.addr, align 4
  %36 = load %class.btVector3*, %class.btVector3** %normal, align 4
  %37 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB30 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %37, i32 0, i32 9
  %38 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB30, align 4
  %call31 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %38)
  store float %call31, float* %ref.tmp29, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp28, %class.btVector3* nonnull align 4 dereferenceable(16) %36, float* nonnull align 4 dereferenceable(4) %ref.tmp29)
  %39 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB33 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %39, i32 0, i32 9
  %40 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB33, align 4
  %call34 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %40)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp32, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call34, %class.btVector3* nonnull align 4 dereferenceable(16) %ftorqueAxis2)
  %41 = load float, float* %impulse, align 4
  %fneg35 = fneg float %41
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %35, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp28, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp32, float %fneg35)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %42 = load i32, i32* %i, align 4
  %inc = add nsw i32 %42, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.end:                                           ; preds = %for.end, %if.then
  %m_bMotorEnabled = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 29
  %43 = load i8, i8* %m_bMotorEnabled, align 4
  %tobool36 = trunc i8 %43 to i1
  br i1 %tobool36, label %if.then37, label %if.else

if.then37:                                        ; preds = %if.end
  %44 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA38 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %44, i32 0, i32 8
  %45 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA38, align 4
  %call39 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %45)
  %call40 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %trACur, %class.btTransform* nonnull align 4 dereferenceable(64) %call39)
  %46 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB41 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %46, i32 0, i32 9
  %47 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB41, align 4
  %call42 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %47)
  %call43 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %trBCur, %class.btTransform* nonnull align 4 dereferenceable(64) %call42)
  %call44 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %omegaA)
  %48 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA.addr, align 4
  call void @_ZNK12btSolverBody26internalGetAngularVelocityER9btVector3(%struct.btSolverBody* %48, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaA)
  %call45 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %omegaB)
  %49 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB.addr, align 4
  call void @_ZNK12btSolverBody26internalGetAngularVelocityER9btVector3(%struct.btSolverBody* %49, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaB)
  %call46 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %trAPred)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %trAPred)
  store float 0.000000e+00, float* %ref.tmp47, align 4
  store float 0.000000e+00, float* %ref.tmp48, align 4
  store float 0.000000e+00, float* %ref.tmp49, align 4
  %call50 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %zerovec, float* nonnull align 4 dereferenceable(4) %ref.tmp47, float* nonnull align 4 dereferenceable(4) %ref.tmp48, float* nonnull align 4 dereferenceable(4) %ref.tmp49)
  %50 = load float, float* %timeStep.addr, align 4
  call void @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_(%class.btTransform* nonnull align 4 dereferenceable(64) %trACur, %class.btVector3* nonnull align 4 dereferenceable(16) %zerovec, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaA, float %50, %class.btTransform* nonnull align 4 dereferenceable(64) %trAPred)
  %call51 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %trBPred)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %trBPred)
  %51 = load float, float* %timeStep.addr, align 4
  call void @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_(%class.btTransform* nonnull align 4 dereferenceable(64) %trBCur, %class.btVector3* nonnull align 4 dereferenceable(16) %zerovec, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaB, float %51, %class.btTransform* nonnull align 4 dereferenceable(64) %trBPred)
  %m_qTarget = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 31
  store float 0.000000e+00, float* %ref.tmp53, align 4
  store float 0.000000e+00, float* %ref.tmp54, align 4
  store float 0.000000e+00, float* %ref.tmp55, align 4
  %call56 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp52, float* nonnull align 4 dereferenceable(4) %ref.tmp53, float* nonnull align 4 dereferenceable(4) %ref.tmp54, float* nonnull align 4 dereferenceable(4) %ref.tmp55)
  %call57 = call %class.btTransform* @_ZN11btTransformC2ERK12btQuaternionRK9btVector3(%class.btTransform* %trPose, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_qTarget, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp52)
  %m_rbBFrame59 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp58, %class.btTransform* %m_rbBFrame59, %class.btTransform* nonnull align 4 dereferenceable(64) %trPose)
  %m_rbAFrame61 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp60, %class.btTransform* %m_rbAFrame61)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trABDes, %class.btTransform* %ref.tmp58, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp60)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trADes, %class.btTransform* %trBPred, %class.btTransform* nonnull align 4 dereferenceable(64) %trABDes)
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp62, %class.btTransform* %trABDes)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trBDes, %class.btTransform* %trAPred, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp62)
  %call63 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %omegaADes)
  %call64 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %omegaBDes)
  %52 = load float, float* %timeStep.addr, align 4
  call void @_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_(%class.btTransform* nonnull align 4 dereferenceable(64) %trACur, %class.btTransform* nonnull align 4 dereferenceable(64) %trADes, float %52, %class.btVector3* nonnull align 4 dereferenceable(16) %zerovec, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaADes)
  %53 = load float, float* %timeStep.addr, align 4
  call void @_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_(%class.btTransform* nonnull align 4 dereferenceable(64) %trBCur, %class.btTransform* nonnull align 4 dereferenceable(64) %trBDes, float %53, %class.btVector3* nonnull align 4 dereferenceable(16) %zerovec, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaBDes)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %dOmegaA, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaADes, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaA)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %dOmegaB, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaBDes, %class.btVector3* nonnull align 4 dereferenceable(16) %omegaB)
  %call65 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axisA)
  %call66 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axisB)
  store float 0.000000e+00, float* %kAxisAInv, align 4
  store float 0.000000e+00, float* %kAxisBInv, align 4
  %call67 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %dOmegaA)
  %cmp68 = fcmp ogt float %call67, 0x3E80000000000000
  br i1 %cmp68, label %if.then69, label %if.end73

if.then69:                                        ; preds = %if.then37
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp70, %class.btVector3* %dOmegaA)
  %54 = bitcast %class.btVector3* %axisA to i8*
  %55 = bitcast %class.btVector3* %ref.tmp70 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %54, i8* align 4 %55, i32 16, i1 false)
  %call71 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyAEv(%class.btConeTwistConstraint* %this1)
  %call72 = call float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %call71, %class.btVector3* nonnull align 4 dereferenceable(16) %axisA)
  store float %call72, float* %kAxisAInv, align 4
  br label %if.end73

if.end73:                                         ; preds = %if.then69, %if.then37
  %call74 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %dOmegaB)
  %cmp75 = fcmp ogt float %call74, 0x3E80000000000000
  br i1 %cmp75, label %if.then76, label %if.end80

if.then76:                                        ; preds = %if.end73
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp77, %class.btVector3* %dOmegaB)
  %56 = bitcast %class.btVector3* %axisB to i8*
  %57 = bitcast %class.btVector3* %ref.tmp77 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %56, i8* align 4 %57, i32 16, i1 false)
  %call78 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyBEv(%class.btConeTwistConstraint* %this1)
  %call79 = call float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %call78, %class.btVector3* nonnull align 4 dereferenceable(16) %axisB)
  store float %call79, float* %kAxisBInv, align 4
  br label %if.end80

if.end80:                                         ; preds = %if.then76, %if.end73
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp81, float* nonnull align 4 dereferenceable(4) %kAxisAInv, %class.btVector3* nonnull align 4 dereferenceable(16) %axisA)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp82, float* nonnull align 4 dereferenceable(4) %kAxisBInv, %class.btVector3* nonnull align 4 dereferenceable(16) %axisB)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %avgAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp81, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp82)
  %58 = load i8, i8* @_ZZN21btConeTwistConstraint23solveConstraintObsoleteER12btSolverBodyS1_fE9bDoTorque, align 1
  %tobool83 = trunc i8 %58 to i1
  br i1 %tobool83, label %land.lhs.true, label %if.end135

land.lhs.true:                                    ; preds = %if.end80
  %call84 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %avgAxis)
  %cmp85 = fcmp ogt float %call84, 0x3E80000000000000
  br i1 %cmp85, label %if.then86, label %if.end135

if.then86:                                        ; preds = %land.lhs.true
  %call87 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %avgAxis)
  %call88 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyAEv(%class.btConeTwistConstraint* %this1)
  %call89 = call float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %call88, %class.btVector3* nonnull align 4 dereferenceable(16) %avgAxis)
  store float %call89, float* %kAxisAInv, align 4
  %call90 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyBEv(%class.btConeTwistConstraint* %this1)
  %call91 = call float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %call90, %class.btVector3* nonnull align 4 dereferenceable(16) %avgAxis)
  store float %call91, float* %kAxisBInv, align 4
  %59 = load float, float* %kAxisAInv, align 4
  %60 = load float, float* %kAxisBInv, align 4
  %add92 = fadd float %59, %60
  store float %add92, float* %kInvCombined, align 4
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp95, float* nonnull align 4 dereferenceable(4) %kAxisAInv, %class.btVector3* nonnull align 4 dereferenceable(16) %dOmegaA)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp96, float* nonnull align 4 dereferenceable(4) %kAxisBInv, %class.btVector3* nonnull align 4 dereferenceable(16) %dOmegaB)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp94, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp95, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp96)
  %61 = load float, float* %kInvCombined, align 4
  %62 = load float, float* %kInvCombined, align 4
  %mul98 = fmul float %61, %62
  store float %mul98, float* %ref.tmp97, align 4
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %impulse93, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp94, float* nonnull align 4 dereferenceable(4) %ref.tmp97)
  %m_maxMotorImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 32
  %63 = load float, float* %m_maxMotorImpulse, align 4
  %cmp99 = fcmp oge float %63, 0.000000e+00
  br i1 %cmp99, label %if.then100, label %if.end116

if.then100:                                       ; preds = %if.then86
  %m_maxMotorImpulse101 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 32
  %64 = load float, float* %m_maxMotorImpulse101, align 4
  store float %64, float* %fMaxImpulse, align 4
  %m_bNormalizedMotorStrength = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 30
  %65 = load i8, i8* %m_bNormalizedMotorStrength, align 1
  %tobool102 = trunc i8 %65 to i1
  br i1 %tobool102, label %if.then103, label %if.end105

if.then103:                                       ; preds = %if.then100
  %66 = load float, float* %fMaxImpulse, align 4
  %67 = load float, float* %kAxisAInv, align 4
  %div104 = fdiv float %66, %67
  store float %div104, float* %fMaxImpulse, align 4
  br label %if.end105

if.end105:                                        ; preds = %if.then103, %if.then100
  %m_accMotorImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 33
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %newUnclampedAccImpulse, %class.btVector3* nonnull align 4 dereferenceable(16) %m_accMotorImpulse, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse93)
  %call106 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %newUnclampedAccImpulse)
  store float %call106, float* %newUnclampedMag, align 4
  %68 = load float, float* %newUnclampedMag, align 4
  %69 = load float, float* %fMaxImpulse, align 4
  %cmp107 = fcmp ogt float %68, %69
  br i1 %cmp107, label %if.then108, label %if.end113

if.then108:                                       ; preds = %if.end105
  %call109 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %newUnclampedAccImpulse)
  %call110 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %newUnclampedAccImpulse, float* nonnull align 4 dereferenceable(4) %fMaxImpulse)
  %m_accMotorImpulse112 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 33
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp111, %class.btVector3* nonnull align 4 dereferenceable(16) %newUnclampedAccImpulse, %class.btVector3* nonnull align 4 dereferenceable(16) %m_accMotorImpulse112)
  %70 = bitcast %class.btVector3* %impulse93 to i8*
  %71 = bitcast %class.btVector3* %ref.tmp111 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %70, i8* align 4 %71, i32 16, i1 false)
  br label %if.end113

if.end113:                                        ; preds = %if.then108, %if.end105
  %m_accMotorImpulse114 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 33
  %call115 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_accMotorImpulse114, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse93)
  br label %if.end116

if.end116:                                        ; preds = %if.end113, %if.then86
  %call117 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %impulse93)
  store float %call117, float* %impulseMag, align 4
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %impulseAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse93, float* nonnull align 4 dereferenceable(4) %impulseMag)
  %72 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA.addr, align 4
  store float 0.000000e+00, float* %ref.tmp119, align 4
  store float 0.000000e+00, float* %ref.tmp120, align 4
  store float 0.000000e+00, float* %ref.tmp121, align 4
  %call122 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp118, float* nonnull align 4 dereferenceable(4) %ref.tmp119, float* nonnull align 4 dereferenceable(4) %ref.tmp120, float* nonnull align 4 dereferenceable(4) %ref.tmp121)
  %73 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA124 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %73, i32 0, i32 8
  %74 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA124, align 4
  %call125 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %74)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp123, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call125, %class.btVector3* nonnull align 4 dereferenceable(16) %impulseAxis)
  %75 = load float, float* %impulseMag, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %72, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp118, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp123, float %75)
  %76 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB.addr, align 4
  store float 0.000000e+00, float* %ref.tmp127, align 4
  store float 0.000000e+00, float* %ref.tmp128, align 4
  store float 0.000000e+00, float* %ref.tmp129, align 4
  %call130 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp126, float* nonnull align 4 dereferenceable(4) %ref.tmp127, float* nonnull align 4 dereferenceable(4) %ref.tmp128, float* nonnull align 4 dereferenceable(4) %ref.tmp129)
  %77 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB132 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %77, i32 0, i32 9
  %78 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB132, align 4
  %call133 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %78)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp131, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call133, %class.btVector3* nonnull align 4 dereferenceable(16) %impulseAxis)
  %79 = load float, float* %impulseMag, align 4
  %fneg134 = fneg float %79
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %76, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp126, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp131, float %fneg134)
  br label %if.end135

if.end135:                                        ; preds = %if.end116, %land.lhs.true, %if.end80
  br label %if.end175

if.else:                                          ; preds = %if.end
  %m_damping = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 7
  %80 = load float, float* %m_damping, align 4
  %cmp136 = fcmp ogt float %80, 0x3E80000000000000
  br i1 %cmp136, label %if.then137, label %if.end174

if.then137:                                       ; preds = %if.else
  %call138 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %angVelA)
  %81 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA.addr, align 4
  call void @_ZNK12btSolverBody26internalGetAngularVelocityER9btVector3(%struct.btSolverBody* %81, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA)
  %call139 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %angVelB)
  %82 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB.addr, align 4
  call void @_ZNK12btSolverBody26internalGetAngularVelocityER9btVector3(%struct.btSolverBody* %82, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %relVel, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA)
  %call140 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %relVel)
  %cmp141 = fcmp ogt float %call140, 0x3E80000000000000
  br i1 %cmp141, label %if.then142, label %if.end173

if.then142:                                       ; preds = %if.then137
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %relVelAxis, %class.btVector3* %relVel)
  %call143 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyAEv(%class.btConeTwistConstraint* %this1)
  %call144 = call float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %call143, %class.btVector3* nonnull align 4 dereferenceable(16) %relVelAxis)
  %call145 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyBEv(%class.btConeTwistConstraint* %this1)
  %call146 = call float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %call145, %class.btVector3* nonnull align 4 dereferenceable(16) %relVelAxis)
  %add147 = fadd float %call144, %call146
  %div148 = fdiv float 1.000000e+00, %add147
  store float %div148, float* %m_kDamping, align 4
  %m_damping151 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 7
  %83 = load float, float* %m_damping151, align 4
  %84 = load float, float* %m_kDamping, align 4
  %mul152 = fmul float %83, %84
  store float %mul152, float* %ref.tmp150, align 4
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %impulse149, float* nonnull align 4 dereferenceable(4) %ref.tmp150, %class.btVector3* nonnull align 4 dereferenceable(16) %relVel)
  %call154 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %impulse149)
  store float %call154, float* %impulseMag153, align 4
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %impulseAxis155, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse149, float* nonnull align 4 dereferenceable(4) %impulseMag153)
  %85 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA.addr, align 4
  store float 0.000000e+00, float* %ref.tmp157, align 4
  store float 0.000000e+00, float* %ref.tmp158, align 4
  store float 0.000000e+00, float* %ref.tmp159, align 4
  %call160 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp156, float* nonnull align 4 dereferenceable(4) %ref.tmp157, float* nonnull align 4 dereferenceable(4) %ref.tmp158, float* nonnull align 4 dereferenceable(4) %ref.tmp159)
  %86 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA162 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %86, i32 0, i32 8
  %87 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA162, align 4
  %call163 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %87)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp161, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call163, %class.btVector3* nonnull align 4 dereferenceable(16) %impulseAxis155)
  %88 = load float, float* %impulseMag153, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %85, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp156, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp161, float %88)
  %89 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB.addr, align 4
  store float 0.000000e+00, float* %ref.tmp165, align 4
  store float 0.000000e+00, float* %ref.tmp166, align 4
  store float 0.000000e+00, float* %ref.tmp167, align 4
  %call168 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp164, float* nonnull align 4 dereferenceable(4) %ref.tmp165, float* nonnull align 4 dereferenceable(4) %ref.tmp166, float* nonnull align 4 dereferenceable(4) %ref.tmp167)
  %90 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB170 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %90, i32 0, i32 9
  %91 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB170, align 4
  %call171 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %91)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp169, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call171, %class.btVector3* nonnull align 4 dereferenceable(16) %impulseAxis155)
  %92 = load float, float* %impulseMag153, align 4
  %fneg172 = fneg float %92
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %89, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp164, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp169, float %fneg172)
  br label %if.end173

if.end173:                                        ; preds = %if.then142, %if.then137
  br label %if.end174

if.end174:                                        ; preds = %if.end173, %if.else
  br label %if.end175

if.end175:                                        ; preds = %if.end174, %if.end135
  %call177 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %angVelA176)
  %93 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA.addr, align 4
  call void @_ZNK12btSolverBody26internalGetAngularVelocityER9btVector3(%struct.btSolverBody* %93, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA176)
  %call179 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %angVelB178)
  %94 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB.addr, align 4
  call void @_ZNK12btSolverBody26internalGetAngularVelocityER9btVector3(%struct.btSolverBody* %94, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB178)
  %m_solveSwingLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  %95 = load i8, i8* %m_solveSwingLimit, align 2
  %tobool180 = trunc i8 %95 to i1
  br i1 %tobool180, label %if.then181, label %if.end227

if.then181:                                       ; preds = %if.end175
  %m_swingLimitRatio = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 26
  %96 = load float, float* %m_swingLimitRatio, align 4
  %m_swingCorrection = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 17
  %97 = load float, float* %m_swingCorrection, align 4
  %mul182 = fmul float %96, %97
  %m_biasFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 5
  %98 = load float, float* %m_biasFactor, align 4
  %mul183 = fmul float %mul182, %98
  %99 = load float, float* %timeStep.addr, align 4
  %div184 = fdiv float %mul183, %99
  store float %div184, float* %amplitude, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp185, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB178, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA176)
  %m_swingAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call186 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp185, %class.btVector3* nonnull align 4 dereferenceable(16) %m_swingAxis)
  store float %call186, float* %relSwingVel, align 4
  %100 = load float, float* %relSwingVel, align 4
  %cmp187 = fcmp ogt float %100, 0.000000e+00
  br i1 %cmp187, label %if.then188, label %if.end193

if.then188:                                       ; preds = %if.then181
  %m_swingLimitRatio189 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 26
  %101 = load float, float* %m_swingLimitRatio189, align 4
  %102 = load float, float* %relSwingVel, align 4
  %mul190 = fmul float %101, %102
  %m_relaxationFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 6
  %103 = load float, float* %m_relaxationFactor, align 4
  %mul191 = fmul float %mul190, %103
  %104 = load float, float* %amplitude, align 4
  %add192 = fadd float %104, %mul191
  store float %add192, float* %amplitude, align 4
  br label %if.end193

if.end193:                                        ; preds = %if.then188, %if.then181
  %105 = load float, float* %amplitude, align 4
  %m_kSwing = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 14
  %106 = load float, float* %m_kSwing, align 4
  %mul195 = fmul float %105, %106
  store float %mul195, float* %impulseMag194, align 4
  %m_accSwingLimitImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 20
  %107 = load float, float* %m_accSwingLimitImpulse, align 4
  store float %107, float* %temp, align 4
  %m_accSwingLimitImpulse197 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 20
  %108 = load float, float* %m_accSwingLimitImpulse197, align 4
  %109 = load float, float* %impulseMag194, align 4
  %add198 = fadd float %108, %109
  store float %add198, float* %ref.tmp196, align 4
  store float 0.000000e+00, float* %ref.tmp199, align 4
  %call200 = call nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %ref.tmp196, float* nonnull align 4 dereferenceable(4) %ref.tmp199)
  %110 = load float, float* %call200, align 4
  %m_accSwingLimitImpulse201 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 20
  store float %110, float* %m_accSwingLimitImpulse201, align 4
  %m_accSwingLimitImpulse202 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 20
  %111 = load float, float* %m_accSwingLimitImpulse202, align 4
  %112 = load float, float* %temp, align 4
  %sub203 = fsub float %111, %112
  store float %sub203, float* %impulseMag194, align 4
  %m_swingAxis205 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %impulse204, %class.btVector3* nonnull align 4 dereferenceable(16) %m_swingAxis205, float* nonnull align 4 dereferenceable(4) %impulseMag194)
  %m_twistAxisA = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 28
  %call207 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %impulse204, %class.btVector3* nonnull align 4 dereferenceable(16) %m_twistAxisA)
  store float %call207, float* %ref.tmp206, align 4
  %m_twistAxisA208 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 28
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %impulseTwistCouple, float* nonnull align 4 dereferenceable(4) %ref.tmp206, %class.btVector3* nonnull align 4 dereferenceable(16) %m_twistAxisA208)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %impulseNoTwistCouple, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse204, %class.btVector3* nonnull align 4 dereferenceable(16) %impulseTwistCouple)
  %113 = bitcast %class.btVector3* %impulse204 to i8*
  %114 = bitcast %class.btVector3* %impulseNoTwistCouple to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %113, i8* align 4 %114, i32 16, i1 false)
  %call209 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %impulse204)
  store float %call209, float* %impulseMag194, align 4
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %noTwistSwingAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse204, float* nonnull align 4 dereferenceable(4) %impulseMag194)
  %115 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA.addr, align 4
  store float 0.000000e+00, float* %ref.tmp211, align 4
  store float 0.000000e+00, float* %ref.tmp212, align 4
  store float 0.000000e+00, float* %ref.tmp213, align 4
  %call214 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp210, float* nonnull align 4 dereferenceable(4) %ref.tmp211, float* nonnull align 4 dereferenceable(4) %ref.tmp212, float* nonnull align 4 dereferenceable(4) %ref.tmp213)
  %116 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA216 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %116, i32 0, i32 8
  %117 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA216, align 4
  %call217 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %117)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp215, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call217, %class.btVector3* nonnull align 4 dereferenceable(16) %noTwistSwingAxis)
  %118 = load float, float* %impulseMag194, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %115, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp210, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp215, float %118)
  %119 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB.addr, align 4
  store float 0.000000e+00, float* %ref.tmp219, align 4
  store float 0.000000e+00, float* %ref.tmp220, align 4
  store float 0.000000e+00, float* %ref.tmp221, align 4
  %call222 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp218, float* nonnull align 4 dereferenceable(4) %ref.tmp219, float* nonnull align 4 dereferenceable(4) %ref.tmp220, float* nonnull align 4 dereferenceable(4) %ref.tmp221)
  %120 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB224 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %120, i32 0, i32 9
  %121 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB224, align 4
  %call225 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %121)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp223, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call225, %class.btVector3* nonnull align 4 dereferenceable(16) %noTwistSwingAxis)
  %122 = load float, float* %impulseMag194, align 4
  %fneg226 = fneg float %122
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %119, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp218, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp223, float %fneg226)
  br label %if.end227

if.end227:                                        ; preds = %if.end193, %if.end175
  %m_solveTwistLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 23
  %123 = load i8, i8* %m_solveTwistLimit, align 1
  %tobool228 = trunc i8 %123 to i1
  br i1 %tobool228, label %if.then229, label %if.end275

if.then229:                                       ; preds = %if.end227
  %m_twistLimitRatio = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 27
  %124 = load float, float* %m_twistLimitRatio, align 4
  %m_twistCorrection = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 18
  %125 = load float, float* %m_twistCorrection, align 4
  %mul231 = fmul float %124, %125
  %m_biasFactor232 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 5
  %126 = load float, float* %m_biasFactor232, align 4
  %mul233 = fmul float %mul231, %126
  %127 = load float, float* %timeStep.addr, align 4
  %div234 = fdiv float %mul233, %127
  store float %div234, float* %amplitude230, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp235, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB178, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA176)
  %m_twistAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %call236 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp235, %class.btVector3* nonnull align 4 dereferenceable(16) %m_twistAxis)
  store float %call236, float* %relTwistVel, align 4
  %128 = load float, float* %relTwistVel, align 4
  %cmp237 = fcmp ogt float %128, 0.000000e+00
  br i1 %cmp237, label %if.then238, label %if.end244

if.then238:                                       ; preds = %if.then229
  %m_twistLimitRatio239 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 27
  %129 = load float, float* %m_twistLimitRatio239, align 4
  %130 = load float, float* %relTwistVel, align 4
  %mul240 = fmul float %129, %130
  %m_relaxationFactor241 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 6
  %131 = load float, float* %m_relaxationFactor241, align 4
  %mul242 = fmul float %mul240, %131
  %132 = load float, float* %amplitude230, align 4
  %add243 = fadd float %132, %mul242
  store float %add243, float* %amplitude230, align 4
  br label %if.end244

if.end244:                                        ; preds = %if.then238, %if.then229
  %133 = load float, float* %amplitude230, align 4
  %m_kTwist = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 15
  %134 = load float, float* %m_kTwist, align 4
  %mul246 = fmul float %133, %134
  store float %mul246, float* %impulseMag245, align 4
  %m_accTwistLimitImpulse = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 21
  %135 = load float, float* %m_accTwistLimitImpulse, align 4
  store float %135, float* %temp247, align 4
  %m_accTwistLimitImpulse249 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 21
  %136 = load float, float* %m_accTwistLimitImpulse249, align 4
  %137 = load float, float* %impulseMag245, align 4
  %add250 = fadd float %136, %137
  store float %add250, float* %ref.tmp248, align 4
  store float 0.000000e+00, float* %ref.tmp251, align 4
  %call252 = call nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %ref.tmp248, float* nonnull align 4 dereferenceable(4) %ref.tmp251)
  %138 = load float, float* %call252, align 4
  %m_accTwistLimitImpulse253 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 21
  store float %138, float* %m_accTwistLimitImpulse253, align 4
  %m_accTwistLimitImpulse254 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 21
  %139 = load float, float* %m_accTwistLimitImpulse254, align 4
  %140 = load float, float* %temp247, align 4
  %sub255 = fsub float %139, %140
  store float %sub255, float* %impulseMag245, align 4
  %141 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA.addr, align 4
  store float 0.000000e+00, float* %ref.tmp257, align 4
  store float 0.000000e+00, float* %ref.tmp258, align 4
  store float 0.000000e+00, float* %ref.tmp259, align 4
  %call260 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp256, float* nonnull align 4 dereferenceable(4) %ref.tmp257, float* nonnull align 4 dereferenceable(4) %ref.tmp258, float* nonnull align 4 dereferenceable(4) %ref.tmp259)
  %142 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA262 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %142, i32 0, i32 8
  %143 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA262, align 4
  %call263 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %143)
  %m_twistAxis264 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp261, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call263, %class.btVector3* nonnull align 4 dereferenceable(16) %m_twistAxis264)
  %144 = load float, float* %impulseMag245, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %141, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp256, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp261, float %144)
  %145 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB.addr, align 4
  store float 0.000000e+00, float* %ref.tmp266, align 4
  store float 0.000000e+00, float* %ref.tmp267, align 4
  store float 0.000000e+00, float* %ref.tmp268, align 4
  %call269 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp265, float* nonnull align 4 dereferenceable(4) %ref.tmp266, float* nonnull align 4 dereferenceable(4) %ref.tmp267, float* nonnull align 4 dereferenceable(4) %ref.tmp268)
  %146 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB271 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %146, i32 0, i32 9
  %147 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB271, align 4
  %call272 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %147)
  %m_twistAxis273 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp270, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call272, %class.btVector3* nonnull align 4 dereferenceable(16) %m_twistAxis273)
  %148 = load float, float* %impulseMag245, align 4
  %fneg274 = fneg float %148
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %145, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp265, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp270, float %fneg274)
  br label %if.end275

if.end275:                                        ; preds = %if.end244, %if.end227
  br label %if.end276

if.end276:                                        ; preds = %if.end275, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12btSolverBody39internalGetVelocityInLocalPointObsoleteERK9btVector3RS0_(%struct.btSolverBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos, %class.btVector3* nonnull align 4 dereferenceable(16) %velocity) #2 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  %rel_pos.addr = alloca %class.btVector3*, align 4
  %velocity.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  store %class.btVector3* %rel_pos, %class.btVector3** %rel_pos.addr, align 4
  store %class.btVector3* %velocity, %class.btVector3** %velocity.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_linearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 8
  %m_deltaLinearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_deltaLinearVelocity)
  %m_angularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 9
  %m_deltaAngularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_deltaAngularVelocity)
  %0 = load %class.btVector3*, %class.btVector3** %rel_pos.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %1 = load %class.btVector3*, %class.btVector3** %velocity.addr, align 4
  %2 = bitcast %class.btVector3* %1 to i8*
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK15btJacobianEntry11getDiagonalEv(%class.btJacobianEntry* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_Adiag = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 5
  %0 = load float, float* %m_Adiag, align 4
  ret float %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %linearComponent, %class.btVector3* nonnull align 4 dereferenceable(16) %angularComponent, float %impulseMagnitude) #2 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  %linearComponent.addr = alloca %class.btVector3*, align 4
  %angularComponent.addr = alloca %class.btVector3*, align 4
  %impulseMagnitude.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  store %class.btVector3* %linearComponent, %class.btVector3** %linearComponent.addr, align 4
  store %class.btVector3* %angularComponent, %class.btVector3** %angularComponent.addr, align 4
  store float %impulseMagnitude, float* %impulseMagnitude.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 12
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  %tobool = icmp ne %class.btRigidBody* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btVector3*, %class.btVector3** %linearComponent.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr)
  %m_linearFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 4
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  %m_deltaLinearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_deltaLinearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %2 = load %class.btVector3*, %class.btVector3** %angularComponent.addr, align 4
  %m_angularFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 3
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularFactor)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %m_deltaAngularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 2
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_deltaAngularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12btSolverBody26internalGetAngularVelocityER9btVector3(%struct.btSolverBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %angVel) #2 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  %angVel.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  store %class.btVector3* %angVel, %class.btVector3** %angVel.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_angularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 9
  %m_deltaAngularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_deltaAngularVelocity)
  %0 = load %class.btVector3*, %class.btVector3** %angVel.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_(%class.btTransform* nonnull align 4 dereferenceable(64) %curTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %linvel, %class.btVector3* nonnull align 4 dereferenceable(16) %angvel, float %timeStep, %class.btTransform* nonnull align 4 dereferenceable(64) %predictedTransform) #2 comdat {
entry:
  %curTrans.addr = alloca %class.btTransform*, align 4
  %linvel.addr = alloca %class.btVector3*, align 4
  %angvel.addr = alloca %class.btVector3*, align 4
  %timeStep.addr = alloca float, align 4
  %predictedTransform.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %axis = alloca %class.btVector3, align 4
  %fAngle = alloca float, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca float, align 4
  %dorn = alloca %class.btQuaternion, align 4
  %ref.tmp24 = alloca float, align 4
  %orn0 = alloca %class.btQuaternion, align 4
  %predictedOrn = alloca %class.btQuaternion, align 4
  store %class.btTransform* %curTrans, %class.btTransform** %curTrans.addr, align 4
  store %class.btVector3* %linvel, %class.btVector3** %linvel.addr, align 4
  store %class.btVector3* %angvel, %class.btVector3** %angvel.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  store %class.btTransform* %predictedTransform, %class.btTransform** %predictedTransform.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %predictedTransform.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %curTrans.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %1)
  %2 = load %class.btVector3*, %class.btVector3** %linvel.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis)
  %3 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4
  %call3 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %3)
  store float %call3, float* %fAngle, align 4
  %4 = load float, float* %fAngle, align 4
  %5 = load float, float* %timeStep.addr, align 4
  %mul = fmul float %4, %5
  %cmp = fcmp ogt float %mul, 0x3FE921FB60000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load float, float* %timeStep.addr, align 4
  %div = fdiv float 0x3FE921FB60000000, %6
  store float %div, float* %fAngle, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load float, float* %fAngle, align 4
  %cmp4 = fcmp olt float %7, 0x3F50624DE0000000
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %8 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4
  %9 = load float, float* %timeStep.addr, align 4
  %mul8 = fmul float 5.000000e-01, %9
  %10 = load float, float* %timeStep.addr, align 4
  %11 = load float, float* %timeStep.addr, align 4
  %mul9 = fmul float %10, %11
  %12 = load float, float* %timeStep.addr, align 4
  %mul10 = fmul float %mul9, %12
  %mul11 = fmul float %mul10, 0x3F95555560000000
  %13 = load float, float* %fAngle, align 4
  %mul12 = fmul float %mul11, %13
  %14 = load float, float* %fAngle, align 4
  %mul13 = fmul float %mul12, %14
  %sub = fsub float %mul8, %mul13
  store float %sub, float* %ref.tmp7, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %8, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %15 = bitcast %class.btVector3* %axis to i8*
  %16 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false)
  br label %if.end20

if.else:                                          ; preds = %if.end
  %17 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4
  %18 = load float, float* %fAngle, align 4
  %mul16 = fmul float 5.000000e-01, %18
  %19 = load float, float* %timeStep.addr, align 4
  %mul17 = fmul float %mul16, %19
  %call18 = call float @_Z5btSinf(float %mul17)
  %20 = load float, float* %fAngle, align 4
  %div19 = fdiv float %call18, %20
  store float %div19, float* %ref.tmp15, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %17, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %21 = bitcast %class.btVector3* %axis to i8*
  %22 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false)
  br label %if.end20

if.end20:                                         ; preds = %if.else, %if.then5
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %axis)
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %axis)
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %axis)
  %23 = load float, float* %fAngle, align 4
  %24 = load float, float* %timeStep.addr, align 4
  %mul25 = fmul float %23, %24
  %mul26 = fmul float %mul25, 5.000000e-01
  %call27 = call float @_Z5btCosf(float %mul26)
  store float %call27, float* %ref.tmp24, align 4
  %call28 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %dorn, float* nonnull align 4 dereferenceable(4) %call21, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call23, float* nonnull align 4 dereferenceable(4) %ref.tmp24)
  %25 = load %class.btTransform*, %class.btTransform** %curTrans.addr, align 4
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %orn0, %class.btTransform* %25)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %predictedOrn, %class.btQuaternion* nonnull align 4 dereferenceable(16) %dorn, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn0)
  %call29 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %predictedOrn)
  %26 = load %class.btTransform*, %class.btTransform** %predictedTransform.addr, align 4
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %26, %class.btQuaternion* nonnull align 4 dereferenceable(16) %predictedOrn)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK12btQuaternionRK9btVector3(%class.btTransform* returned %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform7inverseEv(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %inv = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %inv, %class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_(%class.btTransform* nonnull align 4 dereferenceable(64) %transform0, %class.btTransform* nonnull align 4 dereferenceable(64) %transform1, float %timeStep, %class.btVector3* nonnull align 4 dereferenceable(16) %linVel, %class.btVector3* nonnull align 4 dereferenceable(16) %angVel) #2 comdat {
entry:
  %transform0.addr = alloca %class.btTransform*, align 4
  %transform1.addr = alloca %class.btTransform*, align 4
  %timeStep.addr = alloca float, align 4
  %linVel.addr = alloca %class.btVector3*, align 4
  %angVel.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %axis = alloca %class.btVector3, align 4
  %angle = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  store %class.btTransform* %transform0, %class.btTransform** %transform0.addr, align 4
  store %class.btTransform* %transform1, %class.btTransform** %transform1.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  store %class.btVector3* %linVel, %class.btVector3** %linVel.addr, align 4
  store %class.btVector3* %angVel, %class.btVector3** %angVel.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transform1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %0)
  %1 = load %class.btTransform*, %class.btTransform** %transform0.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %1)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  %2 = load %class.btVector3*, %class.btVector3** %linVel.addr, align 4
  %3 = bitcast %class.btVector3* %2 to i8*
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis)
  %5 = load %class.btTransform*, %class.btTransform** %transform0.addr, align 4
  %6 = load %class.btTransform*, %class.btTransform** %transform1.addr, align 4
  call void @_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf(%class.btTransform* nonnull align 4 dereferenceable(64) %5, %class.btTransform* nonnull align 4 dereferenceable(64) %6, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %angle)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %angle)
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  %7 = load %class.btVector3*, %class.btVector3** %angVel.addr, align 4
  %8 = bitcast %class.btVector3* %7 to i8*
  %9 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyAEv(%class.btConeTwistConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 8
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  ret %class.btRigidBody* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %axis) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %vec = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %this1)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %vec, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call)
  %1 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call2 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  ret float %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyBEv(%class.btConeTwistConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 9
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  ret %class.btRigidBody* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  %2 = load float, float* %1, align 4
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %b.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp ogt float %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load float*, float** %a.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load float*, float** %b.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi float* [ %4, %cond.true ], [ %5, %cond.false ]
  ret float* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN21btConeTwistConstraint9updateRHSEf(%class.btConeTwistConstraint* %this, float %timeStep) #1 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btConeTwistConstraint13calcAngleInfoEv(%class.btConeTwistConstraint* %this) #2 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %b1Axis1 = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %b1Axis2 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %b1Axis3 = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %b2Axis1 = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %b2Axis2 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %ref.tmp24 = alloca %class.btVector3, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca %class.btVector3, align 4
  %swing1 = alloca float, align 4
  %swing2 = alloca float, align 4
  %swx = alloca float, align 4
  %swy = alloca float, align 4
  %thresh = alloca float, align 4
  %fact = alloca float, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp36 = alloca %class.btVector3, align 4
  %ref.tmp49 = alloca %class.btVector3, align 4
  %ref.tmp53 = alloca %class.btVector3, align 4
  %RMaxAngle1Sq = alloca float, align 4
  %RMaxAngle2Sq = alloca float, align 4
  %EllipseAngle = alloca float, align 4
  %ref.tmp87 = alloca %class.btVector3, align 4
  %ref.tmp88 = alloca %class.btVector3, align 4
  %ref.tmp89 = alloca %class.btVector3, align 4
  %ref.tmp90 = alloca float, align 4
  %ref.tmp92 = alloca %class.btVector3, align 4
  %ref.tmp93 = alloca float, align 4
  %swingAxisSign = alloca float, align 4
  %b2Axis2104 = alloca %class.btVector3, align 4
  %ref.tmp108 = alloca %class.btVector3, align 4
  %rotationArc = alloca %class.btQuaternion, align 4
  %TwistRef = alloca %class.btVector3, align 4
  %twist = alloca float, align 4
  %lockedFreeFactor = alloca float, align 4
  %ref.tmp125 = alloca %class.btVector3, align 4
  %ref.tmp126 = alloca %class.btVector3, align 4
  %ref.tmp127 = alloca float, align 4
  %ref.tmp130 = alloca float, align 4
  %ref.tmp141 = alloca %class.btVector3, align 4
  %ref.tmp142 = alloca %class.btVector3, align 4
  %ref.tmp143 = alloca float, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %m_swingCorrection = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 17
  store float 0.000000e+00, float* %m_swingCorrection, align 4
  %m_twistLimitSign = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 16
  store float 0.000000e+00, float* %m_twistLimitSign, align 4
  %m_solveTwistLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 23
  store i8 0, i8* %m_solveTwistLimit, align 1
  %m_solveSwingLimit = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  store i8 0, i8* %m_solveSwingLimit, align 2
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %b1Axis1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 0.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %b1Axis2, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 0.000000e+00, float* %ref.tmp9, align 4
  store float 0.000000e+00, float* %ref.tmp10, align 4
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %b1Axis3, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  store float 0.000000e+00, float* %ref.tmp12, align 4
  store float 0.000000e+00, float* %ref.tmp13, align 4
  store float 0.000000e+00, float* %ref.tmp14, align 4
  %call15 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %b2Axis1, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14)
  store float 0.000000e+00, float* %ref.tmp16, align 4
  store float 0.000000e+00, float* %ref.tmp17, align 4
  store float 0.000000e+00, float* %ref.tmp18, align 4
  %call19 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %b2Axis2, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18)
  %call21 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyAEv(%class.btConeTwistConstraint* %this1)
  %call22 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call21)
  %call23 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call22)
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp24, %class.btMatrix3x3* %call25, i32 0)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp20, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call23, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp24)
  %0 = bitcast %class.btVector3* %b1Axis1 to i8*
  %1 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %call27 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyBEv(%class.btConeTwistConstraint* %this1)
  %call28 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call27)
  %call29 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call28)
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %call31 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbBFrame)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp30, %class.btMatrix3x3* %call31, i32 0)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp26, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call29, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp30)
  %2 = bitcast %class.btVector3* %b2Axis1 to i8*
  %3 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  store float 0.000000e+00, float* %swing1, align 4
  store float 0.000000e+00, float* %swing2, align 4
  store float 0.000000e+00, float* %swx, align 4
  store float 0.000000e+00, float* %swy, align 4
  store float 1.000000e+01, float* %thresh, align 4
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %4 = load float, float* %m_swingSpan1, align 4
  %cmp = fcmp oge float %4, 0x3FA99999A0000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call33 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyAEv(%class.btConeTwistConstraint* %this1)
  %call34 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call33)
  %call35 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call34)
  %m_rbAFrame37 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call38 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame37)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp36, %class.btMatrix3x3* %call38, i32 1)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp32, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call35, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp36)
  %5 = bitcast %class.btVector3* %b1Axis2 to i8*
  %6 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  %call39 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis1)
  store float %call39, float* %swx, align 4
  %call40 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis2)
  store float %call40, float* %swy, align 4
  %7 = load float, float* %swy, align 4
  %8 = load float, float* %swx, align 4
  %call41 = call float @_Z11btAtan2Fastff(float %7, float %8)
  store float %call41, float* %swing1, align 4
  %9 = load float, float* %swy, align 4
  %10 = load float, float* %swy, align 4
  %mul = fmul float %9, %10
  %11 = load float, float* %swx, align 4
  %12 = load float, float* %swx, align 4
  %mul42 = fmul float %11, %12
  %add = fadd float %mul, %mul42
  %13 = load float, float* %thresh, align 4
  %mul43 = fmul float %add, %13
  %14 = load float, float* %thresh, align 4
  %mul44 = fmul float %mul43, %14
  store float %mul44, float* %fact, align 4
  %15 = load float, float* %fact, align 4
  %16 = load float, float* %fact, align 4
  %add45 = fadd float %16, 1.000000e+00
  %div = fdiv float %15, %add45
  store float %div, float* %fact, align 4
  %17 = load float, float* %fact, align 4
  %18 = load float, float* %swing1, align 4
  %mul46 = fmul float %18, %17
  store float %mul46, float* %swing1, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %19 = load float, float* %m_swingSpan2, align 4
  %cmp47 = fcmp oge float %19, 0x3FA99999A0000000
  br i1 %cmp47, label %if.then48, label %if.end67

if.then48:                                        ; preds = %if.end
  %call50 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyAEv(%class.btConeTwistConstraint* %this1)
  %call51 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call50)
  %call52 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call51)
  %m_rbAFrame54 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call55 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame54)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp53, %class.btMatrix3x3* %call55, i32 2)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp49, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call52, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp53)
  %20 = bitcast %class.btVector3* %b1Axis3 to i8*
  %21 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false)
  %call56 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis1)
  store float %call56, float* %swx, align 4
  %call57 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis3)
  store float %call57, float* %swy, align 4
  %22 = load float, float* %swy, align 4
  %23 = load float, float* %swx, align 4
  %call58 = call float @_Z11btAtan2Fastff(float %22, float %23)
  store float %call58, float* %swing2, align 4
  %24 = load float, float* %swy, align 4
  %25 = load float, float* %swy, align 4
  %mul59 = fmul float %24, %25
  %26 = load float, float* %swx, align 4
  %27 = load float, float* %swx, align 4
  %mul60 = fmul float %26, %27
  %add61 = fadd float %mul59, %mul60
  %28 = load float, float* %thresh, align 4
  %mul62 = fmul float %add61, %28
  %29 = load float, float* %thresh, align 4
  %mul63 = fmul float %mul62, %29
  store float %mul63, float* %fact, align 4
  %30 = load float, float* %fact, align 4
  %31 = load float, float* %fact, align 4
  %add64 = fadd float %31, 1.000000e+00
  %div65 = fdiv float %30, %add64
  store float %div65, float* %fact, align 4
  %32 = load float, float* %fact, align 4
  %33 = load float, float* %swing2, align 4
  %mul66 = fmul float %33, %32
  store float %mul66, float* %swing2, align 4
  br label %if.end67

if.end67:                                         ; preds = %if.then48, %if.end
  %m_swingSpan168 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %34 = load float, float* %m_swingSpan168, align 4
  %m_swingSpan169 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %35 = load float, float* %m_swingSpan169, align 4
  %mul70 = fmul float %34, %35
  %div71 = fdiv float 1.000000e+00, %mul70
  store float %div71, float* %RMaxAngle1Sq, align 4
  %m_swingSpan272 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %36 = load float, float* %m_swingSpan272, align 4
  %m_swingSpan273 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %37 = load float, float* %m_swingSpan273, align 4
  %mul74 = fmul float %36, %37
  %div75 = fdiv float 1.000000e+00, %mul74
  store float %div75, float* %RMaxAngle2Sq, align 4
  %38 = load float, float* %swing1, align 4
  %39 = load float, float* %swing1, align 4
  %mul76 = fmul float %38, %39
  %call77 = call float @_Z6btFabsf(float %mul76)
  %40 = load float, float* %RMaxAngle1Sq, align 4
  %mul78 = fmul float %call77, %40
  %41 = load float, float* %swing2, align 4
  %42 = load float, float* %swing2, align 4
  %mul79 = fmul float %41, %42
  %call80 = call float @_Z6btFabsf(float %mul79)
  %43 = load float, float* %RMaxAngle2Sq, align 4
  %mul81 = fmul float %call80, %43
  %add82 = fadd float %mul78, %mul81
  store float %add82, float* %EllipseAngle, align 4
  %44 = load float, float* %EllipseAngle, align 4
  %cmp83 = fcmp ogt float %44, 1.000000e+00
  br i1 %cmp83, label %if.then84, label %if.end101

if.then84:                                        ; preds = %if.end67
  %45 = load float, float* %EllipseAngle, align 4
  %sub = fsub float %45, 1.000000e+00
  %m_swingCorrection85 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 17
  store float %sub, float* %m_swingCorrection85, align 4
  %m_solveSwingLimit86 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 24
  store i8 1, i8* %m_solveSwingLimit86, align 2
  %call91 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis2)
  store float %call91, float* %ref.tmp90, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp89, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis2, float* nonnull align 4 dereferenceable(4) %ref.tmp90)
  %call94 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis3)
  store float %call94, float* %ref.tmp93, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp92, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis3, float* nonnull align 4 dereferenceable(4) %ref.tmp93)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp88, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp89, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp92)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp87, %class.btVector3* %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp88)
  %m_swingAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %46 = bitcast %class.btVector3* %m_swingAxis to i8*
  %47 = bitcast %class.btVector3* %ref.tmp87 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %46, i8* align 4 %47, i32 16, i1 false)
  %m_swingAxis95 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call96 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %m_swingAxis95)
  %call97 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis1)
  %cmp98 = fcmp oge float %call97, 0.000000e+00
  %48 = zext i1 %cmp98 to i64
  %cond = select i1 %cmp98, float 1.000000e+00, float -1.000000e+00
  store float %cond, float* %swingAxisSign, align 4
  %m_swingAxis99 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 12
  %call100 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_swingAxis99, float* nonnull align 4 dereferenceable(4) %swingAxisSign)
  br label %if.end101

if.end101:                                        ; preds = %if.then84, %if.end67
  %m_twistSpan = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %49 = load float, float* %m_twistSpan, align 4
  %cmp102 = fcmp oge float %49, 0.000000e+00
  br i1 %cmp102, label %if.then103, label %if.end149

if.then103:                                       ; preds = %if.end101
  %call105 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK21btConeTwistConstraint13getRigidBodyBEv(%class.btConeTwistConstraint* %this1)
  %call106 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call105)
  %call107 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call106)
  %m_rbBFrame109 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %call110 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbBFrame109)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp108, %class.btMatrix3x3* %call110, i32 1)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %b2Axis2104, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call107, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp108)
  call void @_Z15shortestArcQuatRK9btVector3S1_(%class.btQuaternion* sret align 4 %rotationArc, %class.btVector3* nonnull align 4 dereferenceable(16) %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis1)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %TwistRef, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotationArc, %class.btVector3* nonnull align 4 dereferenceable(16) %b2Axis2104)
  %call111 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %TwistRef, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis3)
  %call112 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %TwistRef, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis2)
  %call113 = call float @_Z11btAtan2Fastff(float %call111, float %call112)
  store float %call113, float* %twist, align 4
  %50 = load float, float* %twist, align 4
  %m_twistAngle = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 19
  store float %50, float* %m_twistAngle, align 4
  %m_twistSpan114 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %51 = load float, float* %m_twistSpan114, align 4
  %cmp115 = fcmp ogt float %51, 0x3FA99999A0000000
  %52 = zext i1 %cmp115 to i64
  %cond116 = select i1 %cmp115, float 1.000000e+00, float 0.000000e+00
  store float %cond116, float* %lockedFreeFactor, align 4
  %53 = load float, float* %twist, align 4
  %m_twistSpan117 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %54 = load float, float* %m_twistSpan117, align 4
  %fneg = fneg float %54
  %55 = load float, float* %lockedFreeFactor, align 4
  %mul118 = fmul float %fneg, %55
  %cmp119 = fcmp ole float %53, %mul118
  br i1 %cmp119, label %if.then120, label %if.else

if.then120:                                       ; preds = %if.then103
  %56 = load float, float* %twist, align 4
  %m_twistSpan121 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %57 = load float, float* %m_twistSpan121, align 4
  %add122 = fadd float %56, %57
  %fneg123 = fneg float %add122
  %m_twistCorrection = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 18
  store float %fneg123, float* %m_twistCorrection, align 4
  %m_solveTwistLimit124 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 23
  store i8 1, i8* %m_solveTwistLimit124, align 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp126, %class.btVector3* nonnull align 4 dereferenceable(16) %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis1)
  store float 5.000000e-01, float* %ref.tmp127, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp125, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp126, float* nonnull align 4 dereferenceable(4) %ref.tmp127)
  %m_twistAxis = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %58 = bitcast %class.btVector3* %m_twistAxis to i8*
  %59 = bitcast %class.btVector3* %ref.tmp125 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %58, i8* align 4 %59, i32 16, i1 false)
  %m_twistAxis128 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %call129 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %m_twistAxis128)
  store float -1.000000e+00, float* %ref.tmp130, align 4
  %m_twistAxis131 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %call132 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_twistAxis131, float* nonnull align 4 dereferenceable(4) %ref.tmp130)
  br label %if.end148

if.else:                                          ; preds = %if.then103
  %60 = load float, float* %twist, align 4
  %m_twistSpan133 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %61 = load float, float* %m_twistSpan133, align 4
  %62 = load float, float* %lockedFreeFactor, align 4
  %mul134 = fmul float %61, %62
  %cmp135 = fcmp ogt float %60, %mul134
  br i1 %cmp135, label %if.then136, label %if.end147

if.then136:                                       ; preds = %if.else
  %63 = load float, float* %twist, align 4
  %m_twistSpan137 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %64 = load float, float* %m_twistSpan137, align 4
  %sub138 = fsub float %63, %64
  %m_twistCorrection139 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 18
  store float %sub138, float* %m_twistCorrection139, align 4
  %m_solveTwistLimit140 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 23
  store i8 1, i8* %m_solveTwistLimit140, align 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp142, %class.btVector3* nonnull align 4 dereferenceable(16) %b2Axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1Axis1)
  store float 5.000000e-01, float* %ref.tmp143, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp141, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp142, float* nonnull align 4 dereferenceable(4) %ref.tmp143)
  %m_twistAxis144 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %65 = bitcast %class.btVector3* %m_twistAxis144 to i8*
  %66 = bitcast %class.btVector3* %ref.tmp141 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %65, i8* align 4 %66, i32 16, i1 false)
  %m_twistAxis145 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 13
  %call146 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %m_twistAxis145)
  br label %if.end147

if.end147:                                        ; preds = %if.then136, %if.else
  br label %if.end148

if.end148:                                        ; preds = %if.end147, %if.then120
  br label %if.end149

if.end149:                                        ; preds = %if.end148, %if.end101
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_Z11btAtan2Fastff(float %y, float %x) #2 comdat {
entry:
  %y.addr = alloca float, align 4
  %x.addr = alloca float, align 4
  %coeff_1 = alloca float, align 4
  %coeff_2 = alloca float, align 4
  %abs_y = alloca float, align 4
  %angle = alloca float, align 4
  %r = alloca float, align 4
  %r3 = alloca float, align 4
  store float %y, float* %y.addr, align 4
  store float %x, float* %x.addr, align 4
  store float 0x3FE921FB60000000, float* %coeff_1, align 4
  %0 = load float, float* %coeff_1, align 4
  %mul = fmul float 3.000000e+00, %0
  store float %mul, float* %coeff_2, align 4
  %1 = load float, float* %y.addr, align 4
  %call = call float @_Z6btFabsf(float %1)
  store float %call, float* %abs_y, align 4
  %2 = load float, float* %x.addr, align 4
  %cmp = fcmp oge float %2, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = load float, float* %x.addr, align 4
  %4 = load float, float* %abs_y, align 4
  %sub = fsub float %3, %4
  %5 = load float, float* %x.addr, align 4
  %6 = load float, float* %abs_y, align 4
  %add = fadd float %5, %6
  %div = fdiv float %sub, %add
  store float %div, float* %r, align 4
  %7 = load float, float* %coeff_1, align 4
  %8 = load float, float* %coeff_1, align 4
  %9 = load float, float* %r, align 4
  %mul1 = fmul float %8, %9
  %sub2 = fsub float %7, %mul1
  store float %sub2, float* %angle, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %10 = load float, float* %x.addr, align 4
  %11 = load float, float* %abs_y, align 4
  %add4 = fadd float %10, %11
  %12 = load float, float* %abs_y, align 4
  %13 = load float, float* %x.addr, align 4
  %sub5 = fsub float %12, %13
  %div6 = fdiv float %add4, %sub5
  store float %div6, float* %r3, align 4
  %14 = load float, float* %coeff_2, align 4
  %15 = load float, float* %coeff_1, align 4
  %16 = load float, float* %r3, align 4
  %mul7 = fmul float %15, %16
  %sub8 = fsub float %14, %mul7
  store float %sub8, float* %angle, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %17 = load float, float* %y.addr, align 4
  %cmp9 = fcmp olt float %17, 0.000000e+00
  br i1 %cmp9, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %18 = load float, float* %angle, align 4
  %fneg = fneg float %18
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %19 = load float, float* %angle, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %fneg, %cond.true ], [ %19, %cond.false ]
  ret float %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z15shortestArcQuatRK9btVector3S1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1) #2 comdat {
entry:
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %c = alloca %class.btVector3, align 4
  %d = alloca float, align 4
  %n = alloca %class.btVector3, align 4
  %unused = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %s = alloca float, align 4
  %rs = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %c, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call, float* %d, align 4
  %4 = load float, float* %d, align 4
  %conv = fpext float %4 to double
  %cmp = fcmp olt double %conv, 0xBFEFFFFFC0000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call1 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %n)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %unused)
  %5 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %unused)
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %n)
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %n)
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %n)
  store float 0.000000e+00, float* %ref.tmp, align 4
  %call6 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %call3, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call5, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  br label %return

if.end:                                           ; preds = %entry
  %6 = load float, float* %d, align 4
  %add = fadd float 1.000000e+00, %6
  %mul = fmul float %add, 2.000000e+00
  %call7 = call float @_Z6btSqrtf(float %mul)
  store float %call7, float* %s, align 4
  %7 = load float, float* %s, align 4
  %div = fdiv float 1.000000e+00, %7
  store float %div, float* %rs, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %c)
  %8 = load float, float* %call9, align 4
  %9 = load float, float* %rs, align 4
  %mul10 = fmul float %8, %9
  store float %mul10, float* %ref.tmp8, align 4
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %c)
  %10 = load float, float* %call12, align 4
  %11 = load float, float* %rs, align 4
  %mul13 = fmul float %10, %11
  store float %mul13, float* %ref.tmp11, align 4
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %c)
  %12 = load float, float* %call15, align 4
  %13 = load float, float* %rs, align 4
  %mul16 = fmul float %12, %13
  store float %mul16, float* %ref.tmp14, align 4
  %14 = load float, float* %s, align 4
  %mul18 = fmul float %14, 5.000000e-01
  store float %mul18, float* %ref.tmp17, align 4
  %call19 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotation, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %rotation.addr = alloca %class.btQuaternion*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %q = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  store %class.btQuaternion* %rotation, %class.btQuaternion** %rotation.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  call void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* sret align 4 %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* %2)
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp)
  %3 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %3)
  %4 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %4)
  %5 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %5)
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call1, float* nonnull align 4 dereferenceable(4) %call2, float* nonnull align 4 dereferenceable(4) %call3)
  ret void
}

; Function Attrs: noinline
define internal void @__cxx_global_var_init.1() #0 {
entry:
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp1, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* @_ZL6vTwist, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %agg.result)
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %agg.result)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_Z11btFuzzyZerof(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %call = call float @_Z6btFabsf(float %0)
  %cmp = fcmp olt float %call, 0x3E80000000000000
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %1 = load float, float* %arrayidx, align 4
  %call = call float @_Z6btAcosf(float %1)
  %mul = fmul float 2.000000e+00, %call
  store float %mul, float* %s, align 4
  %2 = load float, float* %s, align 4
  ret float %2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q2) #2 comdat {
entry:
  %q1.addr = alloca %class.btQuaternion*, align 4
  %q2.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  store %class.btQuaternion* %q1, %class.btQuaternion** %q1.addr, align 4
  store %class.btQuaternion* %q2, %class.btQuaternion** %q2.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %1 = bitcast %class.btQuaternion* %0 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %1)
  %2 = load float, float* %call, align 4
  %3 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %4 = bitcast %class.btQuaternion* %3 to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %4)
  %5 = load float, float* %call1, align 4
  %mul = fmul float %2, %5
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %7)
  %8 = load float, float* %call2, align 4
  %9 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %10 = bitcast %class.btQuaternion* %9 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %10)
  %11 = load float, float* %call3, align 4
  %mul4 = fmul float %8, %11
  %add = fadd float %mul, %mul4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %13)
  %14 = load float, float* %call5, align 4
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %16)
  %17 = load float, float* %call6, align 4
  %mul7 = fmul float %14, %17
  %add8 = fadd float %add, %mul7
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %22 = bitcast %class.btQuaternion* %21 to %class.btQuadWord*
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %22)
  %23 = load float, float* %call10, align 4
  %mul11 = fmul float %20, %23
  %sub = fsub float %add8, %mul11
  store float %sub, float* %ref.tmp, align 4
  %24 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %25 = bitcast %class.btQuaternion* %24 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %25)
  %26 = load float, float* %call13, align 4
  %27 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %28 = bitcast %class.btQuaternion* %27 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %28)
  %29 = load float, float* %call14, align 4
  %mul15 = fmul float %26, %29
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %31)
  %32 = load float, float* %call16, align 4
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %34)
  %35 = load float, float* %call17, align 4
  %mul18 = fmul float %32, %35
  %add19 = fadd float %mul15, %mul18
  %36 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %37 = bitcast %class.btQuaternion* %36 to %class.btQuadWord*
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %37)
  %38 = load float, float* %call20, align 4
  %39 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %40 = bitcast %class.btQuaternion* %39 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %40)
  %41 = load float, float* %call21, align 4
  %mul22 = fmul float %38, %41
  %add23 = fadd float %add19, %mul22
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %43)
  %44 = load float, float* %call24, align 4
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %46)
  %47 = load float, float* %call25, align 4
  %mul26 = fmul float %44, %47
  %sub27 = fsub float %add23, %mul26
  store float %sub27, float* %ref.tmp12, align 4
  %48 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %49 = bitcast %class.btQuaternion* %48 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %49)
  %50 = load float, float* %call29, align 4
  %51 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %52 = bitcast %class.btQuaternion* %51 to %class.btQuadWord*
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %52)
  %53 = load float, float* %call30, align 4
  %mul31 = fmul float %50, %53
  %54 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %55 = bitcast %class.btQuaternion* %54 to %class.btQuadWord*
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %55)
  %56 = load float, float* %call32, align 4
  %57 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %58 = bitcast %class.btQuaternion* %57 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %58)
  %59 = load float, float* %call33, align 4
  %mul34 = fmul float %56, %59
  %add35 = fadd float %mul31, %mul34
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %61)
  %62 = load float, float* %call36, align 4
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %64)
  %65 = load float, float* %call37, align 4
  %mul38 = fmul float %62, %65
  %add39 = fadd float %add35, %mul38
  %66 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %67 = bitcast %class.btQuaternion* %66 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %67)
  %68 = load float, float* %call40, align 4
  %69 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %70 = bitcast %class.btQuaternion* %69 to %class.btQuadWord*
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %70)
  %71 = load float, float* %call41, align 4
  %mul42 = fmul float %68, %71
  %sub43 = fsub float %add39, %mul42
  store float %sub43, float* %ref.tmp28, align 4
  %72 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %73 = bitcast %class.btQuaternion* %72 to %class.btQuadWord*
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %73)
  %74 = load float, float* %call45, align 4
  %75 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %76 = bitcast %class.btQuaternion* %75 to %class.btQuadWord*
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %76)
  %77 = load float, float* %call46, align 4
  %mul47 = fmul float %74, %77
  %78 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %79 = bitcast %class.btQuaternion* %78 to %class.btQuadWord*
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %79)
  %80 = load float, float* %call48, align 4
  %81 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %82 = bitcast %class.btQuaternion* %81 to %class.btQuadWord*
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %82)
  %83 = load float, float* %call49, align 4
  %mul50 = fmul float %80, %83
  %sub51 = fsub float %mul47, %mul50
  %84 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %85 = bitcast %class.btQuaternion* %84 to %class.btQuadWord*
  %call52 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %85)
  %86 = load float, float* %call52, align 4
  %87 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %88 = bitcast %class.btQuaternion* %87 to %class.btQuadWord*
  %call53 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %88)
  %89 = load float, float* %call53, align 4
  %mul54 = fmul float %86, %89
  %sub55 = fsub float %sub51, %mul54
  %90 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %91 = bitcast %class.btQuaternion* %90 to %class.btQuadWord*
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %91)
  %92 = load float, float* %call56, align 4
  %93 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %94 = bitcast %class.btQuaternion* %93 to %class.btQuadWord*
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %94)
  %95 = load float, float* %call57, align 4
  %mul58 = fmul float %92, %95
  %sub59 = fsub float %sub55, %mul58
  store float %sub59, float* %ref.tmp44, align 4
  %call60 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp44)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats3 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [4 x float], [4 x float]* %m_floats3, i32 0, i32 1
  %3 = load float, float* %arrayidx4, align 4
  %fneg5 = fneg float %3
  store float %fneg5, float* %ref.tmp2, align 4
  %4 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %4, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 2
  %5 = load float, float* %arrayidx8, align 4
  %fneg9 = fneg float %5
  store float %fneg9, float* %ref.tmp6, align 4
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats10 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 3
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btQuaternion* %call2
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btConeTwistConstraint20computeConeLimitInfoERK12btQuaternionRfR9btVector3S3_(%class.btConeTwistConstraint* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qCone, float* nonnull align 4 dereferenceable(4) %swingAngle, %class.btVector3* nonnull align 4 dereferenceable(16) %vSwingAxis, float* nonnull align 4 dereferenceable(4) %swingLimit) #2 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %qCone.addr = alloca %class.btQuaternion*, align 4
  %swingAngle.addr = alloca float*, align 4
  %vSwingAxis.addr = alloca %class.btVector3*, align 4
  %swingLimit.addr = alloca float*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %xEllipse = alloca float, align 4
  %yEllipse = alloca float, align 4
  %surfaceSlope2 = alloca float, align 4
  %norm = alloca float, align 4
  %swingLimit2 = alloca float, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store %class.btQuaternion* %qCone, %class.btQuaternion** %qCone.addr, align 4
  store float* %swingAngle, float** %swingAngle.addr, align 4
  store %class.btVector3* %vSwingAxis, %class.btVector3** %vSwingAxis.addr, align 4
  store float* %swingLimit, float** %swingLimit.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %qCone.addr, align 4
  %call = call float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %0)
  %1 = load float*, float** %swingAngle.addr, align 4
  store float %call, float* %1, align 4
  %2 = load float*, float** %swingAngle.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp ogt float %3, 0x3E80000000000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %class.btQuaternion*, %class.btQuaternion** %qCone.addr, align 4
  %5 = bitcast %class.btQuaternion* %4 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %5)
  %6 = load %class.btQuaternion*, %class.btQuaternion** %qCone.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load %class.btQuaternion*, %class.btQuaternion** %qCone.addr, align 4
  %9 = bitcast %class.btQuaternion* %8 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %9)
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %call2, float* nonnull align 4 dereferenceable(4) %call3, float* nonnull align 4 dereferenceable(4) %call4)
  %10 = load %class.btVector3*, %class.btVector3** %vSwingAxis.addr, align 4
  %11 = bitcast %class.btVector3* %10 to i8*
  %12 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false)
  %13 = load %class.btVector3*, %class.btVector3** %vSwingAxis.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %13)
  %14 = load %class.btVector3*, %class.btVector3** %vSwingAxis.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %14)
  %15 = load float, float* %call7, align 4
  store float %15, float* %xEllipse, align 4
  %16 = load %class.btVector3*, %class.btVector3** %vSwingAxis.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %16)
  %17 = load float, float* %call8, align 4
  %fneg = fneg float %17
  store float %fneg, float* %yEllipse, align 4
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %18 = load float, float* %m_swingSpan1, align 4
  %19 = load float*, float** %swingLimit.addr, align 4
  store float %18, float* %19, align 4
  %20 = load float, float* %xEllipse, align 4
  %call9 = call float @_Z4fabsf(float %20) #7
  %cmp10 = fcmp ogt float %call9, 0x3E80000000000000
  br i1 %cmp10, label %if.then11, label %if.end

if.then11:                                        ; preds = %if.then
  %21 = load float, float* %yEllipse, align 4
  %22 = load float, float* %yEllipse, align 4
  %mul = fmul float %21, %22
  %23 = load float, float* %xEllipse, align 4
  %24 = load float, float* %xEllipse, align 4
  %mul12 = fmul float %23, %24
  %div = fdiv float %mul, %mul12
  store float %div, float* %surfaceSlope2, align 4
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %25 = load float, float* %m_swingSpan2, align 4
  %m_swingSpan213 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %26 = load float, float* %m_swingSpan213, align 4
  %mul14 = fmul float %25, %26
  %div15 = fdiv float 1.000000e+00, %mul14
  store float %div15, float* %norm, align 4
  %27 = load float, float* %surfaceSlope2, align 4
  %m_swingSpan116 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %28 = load float, float* %m_swingSpan116, align 4
  %m_swingSpan117 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %29 = load float, float* %m_swingSpan117, align 4
  %mul18 = fmul float %28, %29
  %div19 = fdiv float %27, %mul18
  %30 = load float, float* %norm, align 4
  %add = fadd float %30, %div19
  store float %add, float* %norm, align 4
  %31 = load float, float* %surfaceSlope2, align 4
  %add20 = fadd float 1.000000e+00, %31
  %32 = load float, float* %norm, align 4
  %div21 = fdiv float %add20, %32
  store float %div21, float* %swingLimit2, align 4
  %33 = load float, float* %swingLimit2, align 4
  %call22 = call float @_Z4sqrtf(float %33) #7
  %34 = load float*, float** %swingLimit.addr, align 4
  store float %call22, float* %34, align 4
  br label %if.end

if.end:                                           ; preds = %if.then11, %if.then
  br label %if.end26

if.else:                                          ; preds = %entry
  %35 = load float*, float** %swingAngle.addr, align 4
  %36 = load float, float* %35, align 4
  %cmp23 = fcmp olt float %36, 0.000000e+00
  br i1 %cmp23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %if.else
  br label %if.end25

if.end25:                                         ; preds = %if.then24, %if.else
  br label %if.end26

if.end26:                                         ; preds = %if.end25, %if.end
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK21btConeTwistConstraint33adjustSwingAxisToUseEllipseNormalER9btVector3(%class.btConeTwistConstraint* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vSwingAxis) #2 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %vSwingAxis.addr = alloca %class.btVector3*, align 4
  %y = alloca float, align 4
  %z = alloca float, align 4
  %grad = alloca float, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store %class.btVector3* %vSwingAxis, %class.btVector3** %vSwingAxis.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %vSwingAxis.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4
  %fneg = fneg float %1
  store float %fneg, float* %y, align 4
  %2 = load %class.btVector3*, %class.btVector3** %vSwingAxis.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %2)
  %3 = load float, float* %call2, align 4
  store float %3, float* %z, align 4
  %4 = load float, float* %z, align 4
  %call3 = call float @_Z4fabsf(float %4) #7
  %cmp = fcmp ogt float %call3, 0x3E80000000000000
  br i1 %cmp, label %if.then, label %if.end14

if.then:                                          ; preds = %entry
  %5 = load float, float* %y, align 4
  %6 = load float, float* %z, align 4
  %div = fdiv float %5, %6
  store float %div, float* %grad, align 4
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %7 = load float, float* %m_swingSpan2, align 4
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %8 = load float, float* %m_swingSpan1, align 4
  %div4 = fdiv float %7, %8
  %9 = load float, float* %grad, align 4
  %mul = fmul float %9, %div4
  store float %mul, float* %grad, align 4
  %10 = load float, float* %y, align 4
  %cmp5 = fcmp ogt float %10, 0.000000e+00
  br i1 %cmp5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.then
  %11 = load float, float* %grad, align 4
  %12 = load float, float* %z, align 4
  %mul7 = fmul float %11, %12
  %call8 = call float @_Z4fabsf(float %mul7) #7
  store float %call8, float* %y, align 4
  br label %if.end

if.else:                                          ; preds = %if.then
  %13 = load float, float* %grad, align 4
  %14 = load float, float* %z, align 4
  %mul9 = fmul float %13, %14
  %call10 = call float @_Z4fabsf(float %mul9) #7
  %fneg11 = fneg float %call10
  store float %fneg11, float* %y, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then6
  %15 = load %class.btVector3*, %class.btVector3** %vSwingAxis.addr, align 4
  %16 = load float, float* %y, align 4
  %fneg12 = fneg float %16
  call void @_ZN9btVector34setZEf(%class.btVector3* %15, float %fneg12)
  %17 = load %class.btVector3*, %class.btVector3** %vSwingAxis.addr, align 4
  %18 = load float, float* %z, align 4
  call void @_ZN9btVector34setYEf(%class.btVector3* %17, float %18)
  %19 = load %class.btVector3*, %class.btVector3** %vSwingAxis.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %19)
  br label %if.end14

if.end14:                                         ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_Z32computeAngularImpulseDenominatorRK9btVector3RK11btMatrix3x3(%class.btVector3* nonnull align 4 dereferenceable(16) %axis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %invInertiaWorld) #2 comdat {
entry:
  %axis.addr = alloca %class.btVector3*, align 4
  %invInertiaWorld.addr = alloca %class.btMatrix3x3*, align 4
  %vec = alloca %class.btVector3, align 4
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4
  store %class.btMatrix3x3* %invInertiaWorld, %class.btMatrix3x3** %invInertiaWorld.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %invInertiaWorld.addr, align 4
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %vec, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %1)
  %2 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z7btAtan2ff(float %x, float %y) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = load float, float* %y.addr, align 4
  %call = call float @atan2f(float %0, float %1) #8
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btCosf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btSinf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.sin.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btConeTwistConstraint21computeTwistLimitInfoERK12btQuaternionRfR9btVector3(%class.btConeTwistConstraint* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qTwist, float* nonnull align 4 dereferenceable(4) %twistAngle, %class.btVector3* nonnull align 4 dereferenceable(16) %vTwistAxis) #2 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %qTwist.addr = alloca %class.btQuaternion*, align 4
  %twistAngle.addr = alloca float*, align 4
  %vTwistAxis.addr = alloca %class.btVector3*, align 4
  %qMinTwist = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store %class.btQuaternion* %qTwist, %class.btQuaternion** %qTwist.addr, align 4
  store float* %twistAngle, float** %twistAngle.addr, align 4
  store %class.btVector3* %vTwistAxis, %class.btVector3** %vTwistAxis.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %qTwist.addr, align 4
  %1 = bitcast %class.btQuaternion* %qMinTwist to i8*
  %2 = bitcast %class.btQuaternion* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btQuaternion*, %class.btQuaternion** %qTwist.addr, align 4
  %call = call float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %3)
  %4 = load float*, float** %twistAngle.addr, align 4
  store float %call, float* %4, align 4
  %5 = load float*, float** %twistAngle.addr, align 4
  %6 = load float, float* %5, align 4
  %cmp = fcmp ogt float %6, 0x400921FB60000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %class.btQuaternion*, %class.btQuaternion** %qTwist.addr, align 4
  call void @_ZNK12btQuaternionngEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* %7)
  %8 = bitcast %class.btQuaternion* %qMinTwist to i8*
  %9 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  %call2 = call float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %qMinTwist)
  %10 = load float*, float** %twistAngle.addr, align 4
  store float %call2, float* %10, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %11 = load float*, float** %twistAngle.addr, align 4
  %12 = load float, float* %11, align 4
  %cmp3 = fcmp olt float %12, 0.000000e+00
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  br label %if.end5

if.end5:                                          ; preds = %if.then4, %if.end
  %13 = bitcast %class.btQuaternion* %qMinTwist to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %13)
  %14 = bitcast %class.btQuaternion* %qMinTwist to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %14)
  %15 = bitcast %class.btQuaternion* %qMinTwist to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %15)
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp6, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call8, float* nonnull align 4 dereferenceable(4) %call9)
  %16 = load %class.btVector3*, %class.btVector3** %vTwistAxis.addr, align 4
  %17 = bitcast %class.btVector3* %16 to i8*
  %18 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 16, i1 false)
  %19 = load float*, float** %twistAngle.addr, align 4
  %20 = load float, float* %19, align 4
  %cmp11 = fcmp ogt float %20, 0x3E80000000000000
  br i1 %cmp11, label %if.then12, label %if.end14

if.then12:                                        ; preds = %if.end5
  %21 = load %class.btVector3*, %class.btVector3** %vTwistAxis.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %21)
  br label %if.end14

if.end14:                                         ; preds = %if.then12, %if.end5
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z4fabsf(float %__lcpp_x) #1 comdat {
entry:
  %__lcpp_x.addr = alloca float, align 4
  store float %__lcpp_x, float* %__lcpp_x.addr, align 4
  %0 = load float, float* %__lcpp_x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z4sqrtf(float %__lcpp_x) #1 comdat {
entry:
  %__lcpp_x.addr = alloca float, align 4
  store float %__lcpp_x, float* %__lcpp_x.addr, align 4
  %0 = load float, float* %__lcpp_x.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define hidden void @_ZNK21btConeTwistConstraint16GetPointForAngleEff(%class.btVector3* noalias sret align 4 %agg.result, %class.btConeTwistConstraint* %this, float %fAngleInRadians, float %fLength) #2 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %fAngleInRadians.addr = alloca float, align 4
  %fLength.addr = alloca float, align 4
  %xEllipse = alloca float, align 4
  %yEllipse = alloca float, align 4
  %swingLimit = alloca float, align 4
  %surfaceSlope2 = alloca float, align 4
  %norm = alloca float, align 4
  %swingLimit2 = alloca float, align 4
  %vSwingAxis = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %qSwing = alloca %class.btQuaternion, align 4
  %vPointInConstraintSpace = alloca %class.btVector3, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store float %fAngleInRadians, float* %fAngleInRadians.addr, align 4
  store float %fLength, float* %fLength.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load float, float* %fAngleInRadians.addr, align 4
  %call = call float @_Z5btCosf(float %0)
  store float %call, float* %xEllipse, align 4
  %1 = load float, float* %fAngleInRadians.addr, align 4
  %call2 = call float @_Z5btSinf(float %1)
  store float %call2, float* %yEllipse, align 4
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %2 = load float, float* %m_swingSpan1, align 4
  store float %2, float* %swingLimit, align 4
  %3 = load float, float* %xEllipse, align 4
  %call3 = call float @_Z4fabsf(float %3) #7
  %cmp = fcmp ogt float %call3, 0x3E80000000000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float, float* %yEllipse, align 4
  %5 = load float, float* %yEllipse, align 4
  %mul = fmul float %4, %5
  %6 = load float, float* %xEllipse, align 4
  %7 = load float, float* %xEllipse, align 4
  %mul4 = fmul float %6, %7
  %div = fdiv float %mul, %mul4
  store float %div, float* %surfaceSlope2, align 4
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %8 = load float, float* %m_swingSpan2, align 4
  %m_swingSpan25 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %9 = load float, float* %m_swingSpan25, align 4
  %mul6 = fmul float %8, %9
  %div7 = fdiv float 1.000000e+00, %mul6
  store float %div7, float* %norm, align 4
  %10 = load float, float* %surfaceSlope2, align 4
  %m_swingSpan18 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %11 = load float, float* %m_swingSpan18, align 4
  %m_swingSpan19 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %12 = load float, float* %m_swingSpan19, align 4
  %mul10 = fmul float %11, %12
  %div11 = fdiv float %10, %mul10
  %13 = load float, float* %norm, align 4
  %add = fadd float %13, %div11
  store float %add, float* %norm, align 4
  %14 = load float, float* %surfaceSlope2, align 4
  %add12 = fadd float 1.000000e+00, %14
  %15 = load float, float* %norm, align 4
  %div13 = fdiv float %add12, %15
  store float %div13, float* %swingLimit2, align 4
  %16 = load float, float* %swingLimit2, align 4
  %call14 = call float @_Z4sqrtf(float %16) #7
  store float %call14, float* %swingLimit, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  store float 0.000000e+00, float* %ref.tmp, align 4
  %17 = load float, float* %yEllipse, align 4
  %fneg = fneg float %17
  store float %fneg, float* %ref.tmp15, align 4
  %call16 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %vSwingAxis, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %xEllipse, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %call17 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %qSwing, %class.btVector3* nonnull align 4 dereferenceable(16) %vSwingAxis, float* nonnull align 4 dereferenceable(4) %swingLimit)
  store float 0.000000e+00, float* %ref.tmp18, align 4
  store float 0.000000e+00, float* %ref.tmp19, align 4
  %call20 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %vPointInConstraintSpace, float* nonnull align 4 dereferenceable(4) %fLength.addr, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp19)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qSwing, %class.btVector3* nonnull align 4 dereferenceable(16) %vPointInConstraintSpace)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_axis, float* nonnull align 4 dereferenceable(4) %_angle) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btVector3* %_axis, %class.btVector3** %_axis.addr, align 4
  store float* %_angle, float** %_angle.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  %1 = load %class.btVector3*, %class.btVector3** %_axis.addr, align 4
  %2 = load float*, float** %_angle.addr, align 4
  call void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %2)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12btQuaternionngEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q2 = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %this1, %class.btQuaternion** %q2, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q2, align 4
  %1 = bitcast %class.btQuaternion* %0 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %1)
  %2 = load float, float* %call, align 4
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4
  %3 = load %class.btQuaternion*, %class.btQuaternion** %q2, align 4
  %4 = bitcast %class.btQuaternion* %3 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %4)
  %5 = load float, float* %call3, align 4
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp2, align 4
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q2, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %7)
  %8 = load float, float* %call6, align 4
  %fneg7 = fneg float %8
  store float %fneg7, float* %ref.tmp5, align 4
  %9 = load %class.btQuaternion*, %class.btQuaternion** %q2, align 4
  %10 = bitcast %class.btQuaternion* %9 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %11 = load float, float* %arrayidx, align 4
  %fneg9 = fneg float %11
  store float %fneg9, float* %ref.tmp8, align 4
  %call10 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector34setZEf(%class.btVector3* %this, float %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_z.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float %_z, float* %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_z.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  store float %0, float* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector34setYEf(%class.btVector3* %this, float %_y) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_y.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float %_y, float* %_y.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_y.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  store float %0, float* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btConeTwistConstraint14setMotorTargetERK12btQuaternion(%class.btConeTwistConstraint* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %qConstraint = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  %ref.tmp2 = alloca %class.btQuaternion, align 4
  %ref.tmp3 = alloca %class.btQuaternion, align 4
  %ref.tmp4 = alloca %class.btQuaternion, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp3, %class.btTransform* %m_rbBFrame)
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp2, %class.btQuaternion* %ref.tmp3)
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp4, %class.btTransform* %m_rbAFrame)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %qConstraint, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp4)
  call void @_ZN21btConeTwistConstraint31setMotorTargetInConstraintSpaceERK12btQuaternion(%class.btConeTwistConstraint* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qConstraint)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btConeTwistConstraint31setMotorTargetInConstraintSpaceERK12btQuaternion(%class.btConeTwistConstraint* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %softness = alloca float, align 4
  %vTwisted = alloca %class.btVector3, align 4
  %qTargetCone = alloca %class.btQuaternion, align 4
  %qTargetTwist = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  %swingAngle = alloca float, align 4
  %swingLimit = alloca float, align 4
  %swingAxis = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca %class.btQuaternion, align 4
  %twistAngle = alloca float, align 4
  %twistAxis = alloca %class.btVector3, align 4
  %ref.tmp46 = alloca %class.btQuaternion, align 4
  %ref.tmp50 = alloca %class.btQuaternion, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %m_qTarget = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 31
  %1 = bitcast %class.btQuaternion* %m_qTarget to i8*
  %2 = bitcast %class.btQuaternion* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  store float 1.000000e+00, float* %softness, align 4
  %m_qTarget2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 31
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %vTwisted, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_qTarget2, %class.btVector3* nonnull align 4 dereferenceable(16) @_ZL6vTwist)
  call void @_Z15shortestArcQuatRK9btVector3S1_(%class.btQuaternion* sret align 4 %qTargetCone, %class.btVector3* nonnull align 4 dereferenceable(16) @_ZL6vTwist, %class.btVector3* nonnull align 4 dereferenceable(16) %vTwisted)
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %qTargetCone)
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* %qTargetCone)
  %m_qTarget3 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 31
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %qTargetTwist, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_qTarget3)
  %call4 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %qTargetTwist)
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %3 = load float, float* %m_swingSpan1, align 4
  %cmp = fcmp oge float %3, 0x3FA99999A0000000
  br i1 %cmp, label %land.lhs.true, label %if.end22

land.lhs.true:                                    ; preds = %entry
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %4 = load float, float* %m_swingSpan2, align 4
  %cmp5 = fcmp oge float %4, 0x3FA99999A0000000
  br i1 %cmp5, label %if.then, label %if.end22

if.then:                                          ; preds = %land.lhs.true
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %swingAxis)
  call void @_ZN21btConeTwistConstraint20computeConeLimitInfoERK12btQuaternionRfR9btVector3S3_(%class.btConeTwistConstraint* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qTargetCone, float* nonnull align 4 dereferenceable(4) %swingAngle, %class.btVector3* nonnull align 4 dereferenceable(16) %swingAxis, float* nonnull align 4 dereferenceable(4) %swingLimit)
  %5 = load float, float* %swingAngle, align 4
  %call7 = call float @_Z4fabsf(float %5) #7
  %cmp8 = fcmp ogt float %call7, 0x3E80000000000000
  br i1 %cmp8, label %if.then9, label %if.end21

if.then9:                                         ; preds = %if.then
  %6 = load float, float* %swingAngle, align 4
  %7 = load float, float* %swingLimit, align 4
  %8 = load float, float* %softness, align 4
  %mul = fmul float %7, %8
  %cmp10 = fcmp ogt float %6, %mul
  br i1 %cmp10, label %if.then11, label %if.else

if.then11:                                        ; preds = %if.then9
  %9 = load float, float* %swingLimit, align 4
  %10 = load float, float* %softness, align 4
  %mul12 = fmul float %9, %10
  store float %mul12, float* %swingAngle, align 4
  br label %if.end18

if.else:                                          ; preds = %if.then9
  %11 = load float, float* %swingAngle, align 4
  %12 = load float, float* %swingLimit, align 4
  %fneg = fneg float %12
  %13 = load float, float* %softness, align 4
  %mul13 = fmul float %fneg, %13
  %cmp14 = fcmp olt float %11, %mul13
  br i1 %cmp14, label %if.then15, label %if.end

if.then15:                                        ; preds = %if.else
  %14 = load float, float* %swingLimit, align 4
  %fneg16 = fneg float %14
  %15 = load float, float* %softness, align 4
  %mul17 = fmul float %fneg16, %15
  store float %mul17, float* %swingAngle, align 4
  br label %if.end

if.end:                                           ; preds = %if.then15, %if.else
  br label %if.end18

if.end18:                                         ; preds = %if.end, %if.then11
  %call20 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %ref.tmp19, %class.btVector3* nonnull align 4 dereferenceable(16) %swingAxis, float* nonnull align 4 dereferenceable(4) %swingAngle)
  %16 = bitcast %class.btQuaternion* %qTargetCone to i8*
  %17 = bitcast %class.btQuaternion* %ref.tmp19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false)
  br label %if.end21

if.end21:                                         ; preds = %if.end18, %if.then
  br label %if.end22

if.end22:                                         ; preds = %if.end21, %land.lhs.true, %entry
  %m_twistSpan = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %18 = load float, float* %m_twistSpan, align 4
  %cmp23 = fcmp oge float %18, 0x3FA99999A0000000
  br i1 %cmp23, label %if.then24, label %if.end49

if.then24:                                        ; preds = %if.end22
  %call25 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %twistAxis)
  call void @_ZN21btConeTwistConstraint21computeTwistLimitInfoERK12btQuaternionRfR9btVector3(%class.btConeTwistConstraint* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qTargetTwist, float* nonnull align 4 dereferenceable(4) %twistAngle, %class.btVector3* nonnull align 4 dereferenceable(16) %twistAxis)
  %19 = load float, float* %twistAngle, align 4
  %call26 = call float @_Z4fabsf(float %19) #7
  %cmp27 = fcmp ogt float %call26, 0x3E80000000000000
  br i1 %cmp27, label %if.then28, label %if.end48

if.then28:                                        ; preds = %if.then24
  %20 = load float, float* %twistAngle, align 4
  %m_twistSpan29 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %21 = load float, float* %m_twistSpan29, align 4
  %22 = load float, float* %softness, align 4
  %mul30 = fmul float %21, %22
  %cmp31 = fcmp ogt float %20, %mul30
  br i1 %cmp31, label %if.then32, label %if.else35

if.then32:                                        ; preds = %if.then28
  %m_twistSpan33 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %23 = load float, float* %m_twistSpan33, align 4
  %24 = load float, float* %softness, align 4
  %mul34 = fmul float %23, %24
  store float %mul34, float* %twistAngle, align 4
  br label %if.end45

if.else35:                                        ; preds = %if.then28
  %25 = load float, float* %twistAngle, align 4
  %m_twistSpan36 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %26 = load float, float* %m_twistSpan36, align 4
  %fneg37 = fneg float %26
  %27 = load float, float* %softness, align 4
  %mul38 = fmul float %fneg37, %27
  %cmp39 = fcmp olt float %25, %mul38
  br i1 %cmp39, label %if.then40, label %if.end44

if.then40:                                        ; preds = %if.else35
  %m_twistSpan41 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %28 = load float, float* %m_twistSpan41, align 4
  %fneg42 = fneg float %28
  %29 = load float, float* %softness, align 4
  %mul43 = fmul float %fneg42, %29
  store float %mul43, float* %twistAngle, align 4
  br label %if.end44

if.end44:                                         ; preds = %if.then40, %if.else35
  br label %if.end45

if.end45:                                         ; preds = %if.end44, %if.then32
  %call47 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %ref.tmp46, %class.btVector3* nonnull align 4 dereferenceable(16) %twistAxis, float* nonnull align 4 dereferenceable(4) %twistAngle)
  %30 = bitcast %class.btQuaternion* %qTargetTwist to i8*
  %31 = bitcast %class.btQuaternion* %ref.tmp46 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %30, i8* align 4 %31, i32 16, i1 false)
  br label %if.end48

if.end48:                                         ; preds = %if.end45, %if.then24
  br label %if.end49

if.end49:                                         ; preds = %if.end48, %if.end22
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %ref.tmp50, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qTargetCone, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qTargetTwist)
  %m_qTarget51 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 31
  %32 = bitcast %class.btQuaternion* %m_qTarget51 to i8*
  %33 = bitcast %class.btQuaternion* %ref.tmp50 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %32, i8* align 4 %33, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN21btConeTwistConstraint8setParamEifi(%class.btConeTwistConstraint* %this, i32 %num, float %value, i32 %axis) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %num.addr = alloca i32, align 4
  %value.addr = alloca float, align 4
  %axis.addr = alloca i32, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store i32 %num, i32* %num.addr, align 4
  store float %value, float* %value.addr, align 4
  store i32 %axis, i32* %axis.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load i32, i32* %num.addr, align 4
  switch i32 %0, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb
    i32 3, label %sw.bb3
    i32 4, label %sw.bb3
  ]

sw.bb:                                            ; preds = %entry, %entry
  %1 = load i32, i32* %axis.addr, align 4
  %cmp = icmp sge i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %sw.bb
  %2 = load i32, i32* %axis.addr, align 4
  %cmp2 = icmp slt i32 %2, 3
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %3 = load float, float* %value.addr, align 4
  %m_linERP = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 36
  store float %3, float* %m_linERP, align 4
  %m_flags = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 34
  %4 = load i32, i32* %m_flags, align 4
  %or = or i32 %4, 2
  store i32 %or, i32* %m_flags, align 4
  br label %if.end

if.else:                                          ; preds = %land.lhs.true, %sw.bb
  %5 = load float, float* %value.addr, align 4
  %m_biasFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 5
  store float %5, float* %m_biasFactor, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry, %entry
  %6 = load i32, i32* %axis.addr, align 4
  %cmp4 = icmp sge i32 %6, 0
  br i1 %cmp4, label %land.lhs.true5, label %if.else10

land.lhs.true5:                                   ; preds = %sw.bb3
  %7 = load i32, i32* %axis.addr, align 4
  %cmp6 = icmp slt i32 %7, 3
  br i1 %cmp6, label %if.then7, label %if.else10

if.then7:                                         ; preds = %land.lhs.true5
  %8 = load float, float* %value.addr, align 4
  %m_linCFM = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 35
  store float %8, float* %m_linCFM, align 4
  %m_flags8 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 34
  %9 = load i32, i32* %m_flags8, align 4
  %or9 = or i32 %9, 1
  store i32 %or9, i32* %m_flags8, align 4
  br label %if.end13

if.else10:                                        ; preds = %land.lhs.true5, %sw.bb3
  %10 = load float, float* %value.addr, align 4
  %m_angCFM = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 37
  store float %10, float* %m_angCFM, align 4
  %m_flags11 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 34
  %11 = load i32, i32* %m_flags11, align 4
  %or12 = or i32 %11, 4
  store i32 %or12, i32* %m_flags11, align 4
  br label %if.end13

if.end13:                                         ; preds = %if.else10, %if.then7
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %if.end13, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden float @_ZNK21btConeTwistConstraint8getParamEii(%class.btConeTwistConstraint* %this, i32 %num, i32 %axis) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %num.addr = alloca i32, align 4
  %axis.addr = alloca i32, align 4
  %retVal = alloca float, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store i32 %num, i32* %num.addr, align 4
  store i32 %axis, i32* %axis.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  store float 0.000000e+00, float* %retVal, align 4
  %0 = load i32, i32* %num.addr, align 4
  switch i32 %0, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb
    i32 3, label %sw.bb9
    i32 4, label %sw.bb9
  ]

sw.bb:                                            ; preds = %entry, %entry
  %1 = load i32, i32* %axis.addr, align 4
  %cmp = icmp sge i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %sw.bb
  %2 = load i32, i32* %axis.addr, align 4
  %cmp2 = icmp slt i32 %2, 3
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %m_linERP = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 36
  %3 = load float, float* %m_linERP, align 4
  store float %3, float* %retVal, align 4
  br label %if.end8

if.else:                                          ; preds = %land.lhs.true, %sw.bb
  %4 = load i32, i32* %axis.addr, align 4
  %cmp3 = icmp sge i32 %4, 3
  br i1 %cmp3, label %land.lhs.true4, label %if.else7

land.lhs.true4:                                   ; preds = %if.else
  %5 = load i32, i32* %axis.addr, align 4
  %cmp5 = icmp slt i32 %5, 6
  br i1 %cmp5, label %if.then6, label %if.else7

if.then6:                                         ; preds = %land.lhs.true4
  %m_biasFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 5
  %6 = load float, float* %m_biasFactor, align 4
  store float %6, float* %retVal, align 4
  br label %if.end

if.else7:                                         ; preds = %land.lhs.true4, %if.else
  br label %if.end

if.end:                                           ; preds = %if.else7, %if.then6
  br label %if.end8

if.end8:                                          ; preds = %if.end, %if.then
  br label %sw.epilog

sw.bb9:                                           ; preds = %entry, %entry
  %7 = load i32, i32* %axis.addr, align 4
  %cmp10 = icmp sge i32 %7, 0
  br i1 %cmp10, label %land.lhs.true11, label %if.else14

land.lhs.true11:                                  ; preds = %sw.bb9
  %8 = load i32, i32* %axis.addr, align 4
  %cmp12 = icmp slt i32 %8, 3
  br i1 %cmp12, label %if.then13, label %if.else14

if.then13:                                        ; preds = %land.lhs.true11
  %m_linCFM = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 35
  %9 = load float, float* %m_linCFM, align 4
  store float %9, float* %retVal, align 4
  br label %if.end21

if.else14:                                        ; preds = %land.lhs.true11, %sw.bb9
  %10 = load i32, i32* %axis.addr, align 4
  %cmp15 = icmp sge i32 %10, 3
  br i1 %cmp15, label %land.lhs.true16, label %if.else19

land.lhs.true16:                                  ; preds = %if.else14
  %11 = load i32, i32* %axis.addr, align 4
  %cmp17 = icmp slt i32 %11, 6
  br i1 %cmp17, label %if.then18, label %if.else19

if.then18:                                        ; preds = %land.lhs.true16
  %m_angCFM = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 37
  %12 = load float, float* %m_angCFM, align 4
  store float %12, float* %retVal, align 4
  br label %if.end20

if.else19:                                        ; preds = %land.lhs.true16, %if.else14
  br label %if.end20

if.end20:                                         ; preds = %if.else19, %if.then18
  br label %if.end21

if.end21:                                         ; preds = %if.end20, %if.then13
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %if.end21, %if.end8
  %13 = load float, float* %retVal, align 4
  ret float %13
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btConeTwistConstraint9setFramesERK11btTransformS2_(%class.btConeTwistConstraint* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %frameA, %class.btTransform* nonnull align 4 dereferenceable(64) %frameB) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %frameA.addr = alloca %class.btTransform*, align 4
  %frameB.addr = alloca %class.btTransform*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store %class.btTransform* %frameA, %class.btTransform** %frameA.addr, align 4
  store %class.btTransform* %frameB, %class.btTransform** %frameB.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %frameA.addr, align 4
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_rbAFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %0)
  %1 = load %class.btTransform*, %class.btTransform** %frameB.addr, align 4
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_rbBFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  %2 = bitcast %class.btConeTwistConstraint* %this1 to void (%class.btConeTwistConstraint*)***
  %vtable = load void (%class.btConeTwistConstraint*)**, void (%class.btConeTwistConstraint*)*** %2, align 4
  %vfn = getelementptr inbounds void (%class.btConeTwistConstraint*)*, void (%class.btConeTwistConstraint*)** %vtable, i64 2
  %3 = load void (%class.btConeTwistConstraint*)*, void (%class.btConeTwistConstraint*)** %vfn, align 4
  call void %3(%class.btConeTwistConstraint* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btConeTwistConstraint* @_ZN21btConeTwistConstraintD2Ev(%class.btConeTwistConstraint* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintD2Ev(%class.btTypedConstraint* %0) #7
  ret %class.btConeTwistConstraint* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btConeTwistConstraintD0Ev(%class.btConeTwistConstraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %call = call %class.btConeTwistConstraint* @_ZN21btConeTwistConstraintD2Ev(%class.btConeTwistConstraint* %this1) #7
  %0 = bitcast %class.btConeTwistConstraint* %this1 to i8*
  call void @_ZN21btConeTwistConstraintdlEPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif(%class.btTypedConstraint* %this, %class.btAlignedObjectArray.5* nonnull align 1 %ca, i32 %solverBodyA, i32 %solverBodyB, float %timeStep) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %ca.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %solverBodyA.addr = alloca i32, align 4
  %solverBodyB.addr = alloca i32, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  store %class.btAlignedObjectArray.5* %ca, %class.btAlignedObjectArray.5** %ca.addr, align 4
  store i32 %solverBodyA, i32* %solverBodyA.addr, align 4
  store i32 %solverBodyB, i32* %solverBodyB.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %ca.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK21btConeTwistConstraint28calculateSerializeBufferSizeEv(%class.btConeTwistConstraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  ret i32 212
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZNK21btConeTwistConstraint9serializeEPvP12btSerializer(%class.btConeTwistConstraint* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConeTwistConstraint*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %cone = alloca %struct.btConeTwistConstraintData*, align 4
  store %class.btConeTwistConstraint* %this, %class.btConeTwistConstraint** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btConeTwistConstraint*, %class.btConeTwistConstraint** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btConeTwistConstraintData*
  store %struct.btConeTwistConstraintData* %1, %struct.btConeTwistConstraintData** %cone, align 4
  %2 = bitcast %class.btConeTwistConstraint* %this1 to %class.btTypedConstraint*
  %3 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4
  %m_typeConstraintData = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %3, i32 0, i32 0
  %4 = bitcast %struct.btTypedConstraintData* %m_typeConstraintData to i8*
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %call = call i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint* %2, i8* %4, %class.btSerializer* %5)
  %m_rbAFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 2
  %6 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4
  %m_rbAFrame2 = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %6, i32 0, i32 1
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_rbAFrame, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_rbAFrame2)
  %m_rbBFrame = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 3
  %7 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4
  %m_rbBFrame3 = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %7, i32 0, i32 2
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_rbBFrame, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_rbBFrame3)
  %m_swingSpan1 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 8
  %8 = load float, float* %m_swingSpan1, align 4
  %9 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4
  %m_swingSpan14 = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %9, i32 0, i32 3
  store float %8, float* %m_swingSpan14, align 4
  %m_swingSpan2 = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 9
  %10 = load float, float* %m_swingSpan2, align 4
  %11 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4
  %m_swingSpan25 = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %11, i32 0, i32 4
  store float %10, float* %m_swingSpan25, align 4
  %m_twistSpan = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 10
  %12 = load float, float* %m_twistSpan, align 4
  %13 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4
  %m_twistSpan6 = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %13, i32 0, i32 5
  store float %12, float* %m_twistSpan6, align 4
  %m_limitSoftness = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 4
  %14 = load float, float* %m_limitSoftness, align 4
  %15 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4
  %m_limitSoftness7 = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %15, i32 0, i32 6
  store float %14, float* %m_limitSoftness7, align 4
  %m_biasFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 5
  %16 = load float, float* %m_biasFactor, align 4
  %17 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4
  %m_biasFactor8 = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %17, i32 0, i32 7
  store float %16, float* %m_biasFactor8, align 4
  %m_relaxationFactor = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 6
  %18 = load float, float* %m_relaxationFactor, align 4
  %19 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4
  %m_relaxationFactor9 = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %19, i32 0, i32 8
  store float %18, float* %m_relaxationFactor9, align 4
  %m_damping = getelementptr inbounds %class.btConeTwistConstraint, %class.btConeTwistConstraint* %this1, i32 0, i32 7
  %20 = load float, float* %m_damping, align 4
  %21 = load %struct.btConeTwistConstraintData*, %struct.btConeTwistConstraintData** %cone, align 4
  %m_damping10 = getelementptr inbounds %struct.btConeTwistConstraintData, %struct.btConeTwistConstraintData* %21, i32 0, i32 9
  store float %20, float* %m_damping10, align 4
  ret i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %10, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 1.000000e+00, float* %ref.tmp9, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float*, float** %_z.addr, align 4
  %4 = load float*, float** %_w.addr, align 4
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %0)
  store float %call, float* %d, align 4
  %1 = load float, float* %d, align 4
  %div = fdiv float 2.000000e+00, %1
  store float %div, float* %s, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call2, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %4, %5
  store float %mul, float* %xs, align 4
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load float, float* %call3, align 4
  %9 = load float, float* %s, align 4
  %mul4 = fmul float %8, %9
  store float %mul4, float* %ys, align 4
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call5, align 4
  %13 = load float, float* %s, align 4
  %mul6 = fmul float %12, %13
  store float %mul6, float* %zs, align 4
  %14 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %15 = bitcast %class.btQuaternion* %14 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %15)
  %16 = load float, float* %call7, align 4
  %17 = load float, float* %xs, align 4
  %mul8 = fmul float %16, %17
  store float %mul8, float* %wx, align 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load float, float* %ys, align 4
  %mul10 = fmul float %20, %21
  store float %mul10, float* %wy, align 4
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %23)
  %24 = load float, float* %call11, align 4
  %25 = load float, float* %zs, align 4
  %mul12 = fmul float %24, %25
  store float %mul12, float* %wz, align 4
  %26 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %27 = bitcast %class.btQuaternion* %26 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %27)
  %28 = load float, float* %call13, align 4
  %29 = load float, float* %xs, align 4
  %mul14 = fmul float %28, %29
  store float %mul14, float* %xx, align 4
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %31)
  %32 = load float, float* %call15, align 4
  %33 = load float, float* %ys, align 4
  %mul16 = fmul float %32, %33
  store float %mul16, float* %xy, align 4
  %34 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %35 = bitcast %class.btQuaternion* %34 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %35)
  %36 = load float, float* %call17, align 4
  %37 = load float, float* %zs, align 4
  %mul18 = fmul float %36, %37
  store float %mul18, float* %xz, align 4
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %39)
  %40 = load float, float* %call19, align 4
  %41 = load float, float* %ys, align 4
  %mul20 = fmul float %40, %41
  store float %mul20, float* %yy, align 4
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %43)
  %44 = load float, float* %call21, align 4
  %45 = load float, float* %zs, align 4
  %mul22 = fmul float %44, %45
  store float %mul22, float* %yz, align 4
  %46 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %47 = bitcast %class.btQuaternion* %46 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %47)
  %48 = load float, float* %call23, align 4
  %49 = load float, float* %zs, align 4
  %mul24 = fmul float %48, %49
  store float %mul24, float* %zz, align 4
  %50 = load float, float* %yy, align 4
  %51 = load float, float* %zz, align 4
  %add = fadd float %50, %51
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4
  %52 = load float, float* %xy, align 4
  %53 = load float, float* %wz, align 4
  %sub26 = fsub float %52, %53
  store float %sub26, float* %ref.tmp25, align 4
  %54 = load float, float* %xz, align 4
  %55 = load float, float* %wy, align 4
  %add28 = fadd float %54, %55
  store float %add28, float* %ref.tmp27, align 4
  %56 = load float, float* %xy, align 4
  %57 = load float, float* %wz, align 4
  %add30 = fadd float %56, %57
  store float %add30, float* %ref.tmp29, align 4
  %58 = load float, float* %xx, align 4
  %59 = load float, float* %zz, align 4
  %add32 = fadd float %58, %59
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4
  %60 = load float, float* %yz, align 4
  %61 = load float, float* %wx, align 4
  %sub35 = fsub float %60, %61
  store float %sub35, float* %ref.tmp34, align 4
  %62 = load float, float* %xz, align 4
  %63 = load float, float* %wy, align 4
  %sub37 = fsub float %62, %63
  store float %sub37, float* %ref.tmp36, align 4
  %64 = load float, float* %yz, align 4
  %65 = load float, float* %wx, align 4
  %add39 = fadd float %64, %65
  store float %add39, float* %ref.tmp38, align 4
  %66 = load float, float* %xx, align 4
  %67 = load float, float* %yy, align 4
  %add41 = fadd float %66, %67
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* returned %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf(%class.btTransform* nonnull align 4 dereferenceable(64) %transform0, %class.btTransform* nonnull align 4 dereferenceable(64) %transform1, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %angle) #2 comdat {
entry:
  %transform0.addr = alloca %class.btTransform*, align 4
  %transform1.addr = alloca %class.btTransform*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %angle.addr = alloca float*, align 4
  %dmat = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %dorn = alloca %class.btQuaternion, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %len = alloca float, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  store %class.btTransform* %transform0, %class.btTransform** %transform0.addr, align 4
  store %class.btTransform* %transform1, %class.btTransform** %transform1.addr, align 4
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4
  store float* %angle, float** %angle.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transform1.addr, align 4
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %0)
  %1 = load %class.btTransform*, %class.btTransform** %transform0.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %1)
  call void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %call1)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %dmat, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp)
  %call2 = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %dorn)
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %dmat, %class.btQuaternion* nonnull align 4 dereferenceable(16) %dorn)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %dorn)
  %call4 = call float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %dorn)
  %2 = load float*, float** %angle.addr, align 4
  store float %call4, float* %2, align 4
  %3 = bitcast %class.btQuaternion* %dorn to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = bitcast %class.btQuaternion* %dorn to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %4)
  %5 = bitcast %class.btQuaternion* %dorn to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %5)
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp5, float* nonnull align 4 dereferenceable(4) %call6, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call8)
  %6 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %7 = bitcast %class.btVector3* %6 to i8*
  %8 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %9)
  %arrayidx = getelementptr inbounds float, float* %call10, i32 3
  store float 0.000000e+00, float* %arrayidx, align 4
  %10 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call11 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %10)
  store float %call11, float* %len, align 4
  %11 = load float, float* %len, align 4
  %cmp = fcmp olt float %11, 0x3D10000000000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store float 1.000000e+00, float* %ref.tmp13, align 4
  store float 0.000000e+00, float* %ref.tmp14, align 4
  store float 0.000000e+00, float* %ref.tmp15, align 4
  %call16 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %12 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %13 = bitcast %class.btVector3* %12 to i8*
  %14 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false)
  br label %if.end

if.else:                                          ; preds = %entry
  %15 = load float, float* %len, align 4
  %call18 = call float @_Z6btSqrtf(float %15)
  store float %call18, float* %ref.tmp17, align 4
  %16 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %16, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %co = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %det = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %call = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 1, i32 2, i32 2)
  store float %call, float* %ref.tmp, align 4
  %call3 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 2, i32 2, i32 0)
  store float %call3, float* %ref.tmp2, align 4
  %call5 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 0, i32 2, i32 1)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %co, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this1, i32 0)
  %call8 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %co)
  store float %call8, float* %det, align 4
  %1 = load float, float* %det, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %s, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %co)
  %2 = load float, float* %call10, align 4
  %3 = load float, float* %s, align 4
  %mul = fmul float %2, %3
  store float %mul, float* %ref.tmp9, align 4
  %call12 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 2, i32 1)
  %4 = load float, float* %s, align 4
  %mul13 = fmul float %call12, %4
  store float %mul13, float* %ref.tmp11, align 4
  %call15 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 1, i32 2)
  %5 = load float, float* %s, align 4
  %mul16 = fmul float %call15, %5
  store float %mul16, float* %ref.tmp14, align 4
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %co)
  %6 = load float, float* %call18, align 4
  %7 = load float, float* %s, align 4
  %mul19 = fmul float %6, %7
  store float %mul19, float* %ref.tmp17, align 4
  %call21 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 2, i32 2)
  %8 = load float, float* %s, align 4
  %mul22 = fmul float %call21, %8
  store float %mul22, float* %ref.tmp20, align 4
  %call24 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 1, i32 0)
  %9 = load float, float* %s, align 4
  %mul25 = fmul float %call24, %9
  store float %mul25, float* %ref.tmp23, align 4
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %co)
  %10 = load float, float* %call27, align 4
  %11 = load float, float* %s, align 4
  %mul28 = fmul float %10, %11
  store float %mul28, float* %ref.tmp26, align 4
  %call30 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 2, i32 0)
  %12 = load float, float* %s, align 4
  %mul31 = fmul float %call30, %12
  store float %mul31, float* %ref.tmp29, align 4
  %call33 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 1, i32 1)
  %13 = load float, float* %s, align 4
  %mul34 = fmul float %call33, %13
  store float %mul34, float* %ref.tmp32, align 4
  %call35 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %trace = alloca float, align 4
  %temp = alloca [4 x float], align 16
  %s = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %s64 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx3)
  %1 = load float, float* %call4, align 4
  %add = fadd float %0, %1
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx6)
  %2 = load float, float* %call7, align 4
  %add8 = fadd float %add, %2
  store float %add8, float* %trace, align 4
  %3 = load float, float* %trace, align 4
  %cmp = fcmp ogt float %3, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load float, float* %trace, align 4
  %add9 = fadd float %4, 1.000000e+00
  %call10 = call float @_Z6btSqrtf(float %add9)
  store float %call10, float* %s, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %5, 5.000000e-01
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul, float* %arrayidx11, align 4
  %6 = load float, float* %s, align 4
  %div = fdiv float 5.000000e-01, %6
  store float %div, float* %s, align 4
  %m_el12 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el12, i32 0, i32 2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx13)
  %7 = load float, float* %call14, align 4
  %m_el15 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el15, i32 0, i32 1
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx16)
  %8 = load float, float* %call17, align 4
  %sub = fsub float %7, %8
  %9 = load float, float* %s, align 4
  %mul18 = fmul float %sub, %9
  %arrayidx19 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  store float %mul18, float* %arrayidx19, align 16
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 0
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %10 = load float, float* %call22, align 4
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx24)
  %11 = load float, float* %call25, align 4
  %sub26 = fsub float %10, %11
  %12 = load float, float* %s, align 4
  %mul27 = fmul float %sub26, %12
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  store float %mul27, float* %arrayidx28, align 4
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 1
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %13 = load float, float* %call31, align 4
  %m_el32 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el32, i32 0, i32 0
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx33)
  %14 = load float, float* %call34, align 4
  %sub35 = fsub float %13, %14
  %15 = load float, float* %s, align 4
  %mul36 = fmul float %sub35, %15
  %arrayidx37 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  store float %mul36, float* %arrayidx37, align 8
  br label %if.end

if.else:                                          ; preds = %entry
  %m_el38 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el38, i32 0, i32 0
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx39)
  %16 = load float, float* %call40, align 4
  %m_el41 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx42 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el41, i32 0, i32 1
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx42)
  %17 = load float, float* %call43, align 4
  %cmp44 = fcmp olt float %16, %17
  br i1 %cmp44, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 1
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx46)
  %18 = load float, float* %call47, align 4
  %m_el48 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el48, i32 0, i32 2
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx49)
  %19 = load float, float* %call50, align 4
  %cmp51 = fcmp olt float %18, %19
  %20 = zext i1 %cmp51 to i64
  %cond = select i1 %cmp51, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 0
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx53)
  %21 = load float, float* %call54, align 4
  %m_el55 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el55, i32 0, i32 2
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx56)
  %22 = load float, float* %call57, align 4
  %cmp58 = fcmp olt float %21, %22
  %23 = zext i1 %cmp58 to i64
  %cond59 = select i1 %cmp58, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond60 = phi i32 [ %cond, %cond.true ], [ %cond59, %cond.false ]
  store i32 %cond60, i32* %i, align 4
  %24 = load i32, i32* %i, align 4
  %add61 = add nsw i32 %24, 1
  %rem = srem i32 %add61, 3
  store i32 %rem, i32* %j, align 4
  %25 = load i32, i32* %i, align 4
  %add62 = add nsw i32 %25, 2
  %rem63 = srem i32 %add62, 3
  store i32 %rem63, i32* %k, align 4
  %m_el65 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %26 = load i32, i32* %i, align 4
  %arrayidx66 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el65, i32 0, i32 %26
  %call67 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx66)
  %27 = load i32, i32* %i, align 4
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 %27
  %28 = load float, float* %arrayidx68, align 4
  %m_el69 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %29 = load i32, i32* %j, align 4
  %arrayidx70 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el69, i32 0, i32 %29
  %call71 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx70)
  %30 = load i32, i32* %j, align 4
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 %30
  %31 = load float, float* %arrayidx72, align 4
  %sub73 = fsub float %28, %31
  %m_el74 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %32 = load i32, i32* %k, align 4
  %arrayidx75 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el74, i32 0, i32 %32
  %call76 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx75)
  %33 = load i32, i32* %k, align 4
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 %33
  %34 = load float, float* %arrayidx77, align 4
  %sub78 = fsub float %sub73, %34
  %add79 = fadd float %sub78, 1.000000e+00
  %call80 = call float @_Z6btSqrtf(float %add79)
  store float %call80, float* %s64, align 4
  %35 = load float, float* %s64, align 4
  %mul81 = fmul float %35, 5.000000e-01
  %36 = load i32, i32* %i, align 4
  %arrayidx82 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %36
  store float %mul81, float* %arrayidx82, align 4
  %37 = load float, float* %s64, align 4
  %div83 = fdiv float 5.000000e-01, %37
  store float %div83, float* %s64, align 4
  %m_el84 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %38 = load i32, i32* %k, align 4
  %arrayidx85 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el84, i32 0, i32 %38
  %call86 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx85)
  %39 = load i32, i32* %j, align 4
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 %39
  %40 = load float, float* %arrayidx87, align 4
  %m_el88 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %41 = load i32, i32* %j, align 4
  %arrayidx89 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el88, i32 0, i32 %41
  %call90 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx89)
  %42 = load i32, i32* %k, align 4
  %arrayidx91 = getelementptr inbounds float, float* %call90, i32 %42
  %43 = load float, float* %arrayidx91, align 4
  %sub92 = fsub float %40, %43
  %44 = load float, float* %s64, align 4
  %mul93 = fmul float %sub92, %44
  %arrayidx94 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul93, float* %arrayidx94, align 4
  %m_el95 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %45 = load i32, i32* %j, align 4
  %arrayidx96 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el95, i32 0, i32 %45
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx96)
  %46 = load i32, i32* %i, align 4
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 %46
  %47 = load float, float* %arrayidx98, align 4
  %m_el99 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %48 = load i32, i32* %i, align 4
  %arrayidx100 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el99, i32 0, i32 %48
  %call101 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx100)
  %49 = load i32, i32* %j, align 4
  %arrayidx102 = getelementptr inbounds float, float* %call101, i32 %49
  %50 = load float, float* %arrayidx102, align 4
  %add103 = fadd float %47, %50
  %51 = load float, float* %s64, align 4
  %mul104 = fmul float %add103, %51
  %52 = load i32, i32* %j, align 4
  %arrayidx105 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %52
  store float %mul104, float* %arrayidx105, align 4
  %m_el106 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %53 = load i32, i32* %k, align 4
  %arrayidx107 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el106, i32 0, i32 %53
  %call108 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx107)
  %54 = load i32, i32* %i, align 4
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 %54
  %55 = load float, float* %arrayidx109, align 4
  %m_el110 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %56 = load i32, i32* %i, align 4
  %arrayidx111 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el110, i32 0, i32 %56
  %call112 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx111)
  %57 = load i32, i32* %k, align 4
  %arrayidx113 = getelementptr inbounds float, float* %call112, i32 %57
  %58 = load float, float* %arrayidx113, align 4
  %add114 = fadd float %55, %58
  %59 = load float, float* %s64, align 4
  %mul115 = fmul float %add114, %59
  %60 = load i32, i32* %k, align 4
  %arrayidx116 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %60
  store float %mul115, float* %arrayidx116, align 4
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.then
  %61 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %62 = bitcast %class.btQuaternion* %61 to %class.btQuadWord*
  %arrayidx117 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  %arrayidx118 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  %arrayidx119 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  %arrayidx120 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %62, float* nonnull align 4 dereferenceable(4) %arrayidx117, float* nonnull align 4 dereferenceable(4) %arrayidx118, float* nonnull align 4 dereferenceable(4) %arrayidx119, float* nonnull align 4 dereferenceable(4) %arrayidx120)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this, i32 %r1, i32 %c1, i32 %r2, i32 %c2) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %r1.addr = alloca i32, align 4
  %c1.addr = alloca i32, align 4
  %r2.addr = alloca i32, align 4
  %c2.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %r1, i32* %r1.addr, align 4
  store i32 %c1, i32* %c1.addr, align 4
  store i32 %r2, i32* %r2.addr, align 4
  store i32 %c2, i32* %c2.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %r1.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %1 = load i32, i32* %c1.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %1
  %2 = load float, float* %arrayidx2, align 4
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %3 = load i32, i32* %r2.addr, align 4
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 %3
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %4 = load i32, i32* %c2.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %4
  %5 = load float, float* %arrayidx6, align 4
  %mul = fmul float %2, %5
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %6 = load i32, i32* %r1.addr, align 4
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 %6
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %7 = load i32, i32* %c2.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %7
  %8 = load float, float* %arrayidx10, align 4
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %9 = load i32, i32* %r2.addr, align 4
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 %9
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx12)
  %10 = load i32, i32* %c1.addr, align 4
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 %10
  %11 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %8, %11
  %sub = fsub float %mul, %mul15
  ret float %sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call2, float* %ref.tmp1, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp3, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %w) #2 comdat {
entry:
  %q.addr = alloca %class.btQuaternion*, align 4
  %w.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  store %class.btVector3* %w, %class.btVector3** %w.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %1 = bitcast %class.btQuaternion* %0 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %1)
  %2 = load float, float* %call, align 4
  %3 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %3)
  %4 = load float, float* %call1, align 4
  %mul = fmul float %2, %4
  %5 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %6 = bitcast %class.btQuaternion* %5 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %6)
  %7 = load float, float* %call2, align 4
  %8 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %8)
  %9 = load float, float* %call3, align 4
  %mul4 = fmul float %7, %9
  %add = fadd float %mul, %mul4
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call5, align 4
  %13 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %13)
  %14 = load float, float* %call6, align 4
  %mul7 = fmul float %12, %14
  %sub = fsub float %add, %mul7
  store float %sub, float* %ref.tmp, align 4
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %16)
  %17 = load float, float* %call9, align 4
  %18 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %18)
  %19 = load float, float* %call10, align 4
  %mul11 = fmul float %17, %19
  %20 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %21 = bitcast %class.btQuaternion* %20 to %class.btQuadWord*
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %21)
  %22 = load float, float* %call12, align 4
  %23 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %23)
  %24 = load float, float* %call13, align 4
  %mul14 = fmul float %22, %24
  %add15 = fadd float %mul11, %mul14
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %26)
  %27 = load float, float* %call16, align 4
  %28 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %28)
  %29 = load float, float* %call17, align 4
  %mul18 = fmul float %27, %29
  %sub19 = fsub float %add15, %mul18
  store float %sub19, float* %ref.tmp8, align 4
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %31)
  %32 = load float, float* %call21, align 4
  %33 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %33)
  %34 = load float, float* %call22, align 4
  %mul23 = fmul float %32, %34
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call24, align 4
  %38 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %38)
  %39 = load float, float* %call25, align 4
  %mul26 = fmul float %37, %39
  %add27 = fadd float %mul23, %mul26
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %41)
  %42 = load float, float* %call28, align 4
  %43 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %43)
  %44 = load float, float* %call29, align 4
  %mul30 = fmul float %42, %44
  %sub31 = fsub float %add27, %mul30
  store float %sub31, float* %ref.tmp20, align 4
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %46)
  %47 = load float, float* %call33, align 4
  %fneg = fneg float %47
  %48 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %48)
  %49 = load float, float* %call34, align 4
  %mul35 = fmul float %fneg, %49
  %50 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %51 = bitcast %class.btQuaternion* %50 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %51)
  %52 = load float, float* %call36, align 4
  %53 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %53)
  %54 = load float, float* %call37, align 4
  %mul38 = fmul float %52, %54
  %sub39 = fsub float %mul35, %mul38
  %55 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %56 = bitcast %class.btQuaternion* %55 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %56)
  %57 = load float, float* %call40, align 4
  %58 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %58)
  %59 = load float, float* %call41, align 4
  %mul42 = fmul float %57, %59
  %sub43 = fsub float %sub39, %mul42
  store float %sub43, float* %ref.tmp32, align 4
  %call44 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp58 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %2 = load float, float* %arrayidx, align 4
  %3 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %4 = bitcast %class.btQuaternion* %3 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %4)
  %5 = load float, float* %call, align 4
  %mul = fmul float %2, %5
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %7 = load float, float* %arrayidx3, align 4
  %8 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %9 = bitcast %class.btQuaternion* %8 to %class.btQuadWord*
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %9, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 3
  %10 = load float, float* %arrayidx5, align 4
  %mul6 = fmul float %7, %10
  %add = fadd float %mul, %mul6
  %11 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %11, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %12 = load float, float* %arrayidx8, align 4
  %13 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %14 = bitcast %class.btQuaternion* %13 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %14)
  %15 = load float, float* %call9, align 4
  %mul10 = fmul float %12, %15
  %add11 = fadd float %add, %mul10
  %16 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats12 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %16, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %17 = load float, float* %arrayidx13, align 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %19)
  %20 = load float, float* %call14, align 4
  %mul15 = fmul float %17, %20
  %sub = fsub float %add11, %mul15
  store float %sub, float* %ref.tmp, align 4
  %21 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats17 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %21, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 3
  %22 = load float, float* %arrayidx18, align 4
  %23 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %24 = bitcast %class.btQuaternion* %23 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %24)
  %25 = load float, float* %call19, align 4
  %mul20 = fmul float %22, %25
  %26 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats21 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %26, i32 0, i32 0
  %arrayidx22 = getelementptr inbounds [4 x float], [4 x float]* %m_floats21, i32 0, i32 1
  %27 = load float, float* %arrayidx22, align 4
  %28 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %29 = bitcast %class.btQuaternion* %28 to %class.btQuadWord*
  %m_floats23 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %29, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [4 x float], [4 x float]* %m_floats23, i32 0, i32 3
  %30 = load float, float* %arrayidx24, align 4
  %mul25 = fmul float %27, %30
  %add26 = fadd float %mul20, %mul25
  %31 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats27 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %31, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 2
  %32 = load float, float* %arrayidx28, align 4
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %34)
  %35 = load float, float* %call29, align 4
  %mul30 = fmul float %32, %35
  %add31 = fadd float %add26, %mul30
  %36 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats32 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %36, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [4 x float], [4 x float]* %m_floats32, i32 0, i32 0
  %37 = load float, float* %arrayidx33, align 4
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %39)
  %40 = load float, float* %call34, align 4
  %mul35 = fmul float %37, %40
  %sub36 = fsub float %add31, %mul35
  store float %sub36, float* %ref.tmp16, align 4
  %41 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats38 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %41, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [4 x float], [4 x float]* %m_floats38, i32 0, i32 3
  %42 = load float, float* %arrayidx39, align 4
  %43 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %44 = bitcast %class.btQuaternion* %43 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %44)
  %45 = load float, float* %call40, align 4
  %mul41 = fmul float %42, %45
  %46 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats42 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %46, i32 0, i32 0
  %arrayidx43 = getelementptr inbounds [4 x float], [4 x float]* %m_floats42, i32 0, i32 2
  %47 = load float, float* %arrayidx43, align 4
  %48 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %49 = bitcast %class.btQuaternion* %48 to %class.btQuadWord*
  %m_floats44 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %49, i32 0, i32 0
  %arrayidx45 = getelementptr inbounds [4 x float], [4 x float]* %m_floats44, i32 0, i32 3
  %50 = load float, float* %arrayidx45, align 4
  %mul46 = fmul float %47, %50
  %add47 = fadd float %mul41, %mul46
  %51 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats48 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %51, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [4 x float], [4 x float]* %m_floats48, i32 0, i32 0
  %52 = load float, float* %arrayidx49, align 4
  %53 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %54 = bitcast %class.btQuaternion* %53 to %class.btQuadWord*
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %54)
  %55 = load float, float* %call50, align 4
  %mul51 = fmul float %52, %55
  %add52 = fadd float %add47, %mul51
  %56 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats53 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %56, i32 0, i32 0
  %arrayidx54 = getelementptr inbounds [4 x float], [4 x float]* %m_floats53, i32 0, i32 1
  %57 = load float, float* %arrayidx54, align 4
  %58 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %59 = bitcast %class.btQuaternion* %58 to %class.btQuadWord*
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %59)
  %60 = load float, float* %call55, align 4
  %mul56 = fmul float %57, %60
  %sub57 = fsub float %add52, %mul56
  store float %sub57, float* %ref.tmp37, align 4
  %61 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats59 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %61, i32 0, i32 0
  %arrayidx60 = getelementptr inbounds [4 x float], [4 x float]* %m_floats59, i32 0, i32 3
  %62 = load float, float* %arrayidx60, align 4
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %m_floats61 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %64, i32 0, i32 0
  %arrayidx62 = getelementptr inbounds [4 x float], [4 x float]* %m_floats61, i32 0, i32 3
  %65 = load float, float* %arrayidx62, align 4
  %mul63 = fmul float %62, %65
  %66 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats64 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %66, i32 0, i32 0
  %arrayidx65 = getelementptr inbounds [4 x float], [4 x float]* %m_floats64, i32 0, i32 0
  %67 = load float, float* %arrayidx65, align 4
  %68 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %69 = bitcast %class.btQuaternion* %68 to %class.btQuadWord*
  %call66 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %69)
  %70 = load float, float* %call66, align 4
  %mul67 = fmul float %67, %70
  %sub68 = fsub float %mul63, %mul67
  %71 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats69 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %71, i32 0, i32 0
  %arrayidx70 = getelementptr inbounds [4 x float], [4 x float]* %m_floats69, i32 0, i32 1
  %72 = load float, float* %arrayidx70, align 4
  %73 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %74 = bitcast %class.btQuaternion* %73 to %class.btQuadWord*
  %call71 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %74)
  %75 = load float, float* %call71, align 4
  %mul72 = fmul float %72, %75
  %sub73 = fsub float %sub68, %mul72
  %76 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats74 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %76, i32 0, i32 0
  %arrayidx75 = getelementptr inbounds [4 x float], [4 x float]* %m_floats74, i32 0, i32 2
  %77 = load float, float* %arrayidx75, align 4
  %78 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %79 = bitcast %class.btQuaternion* %78 to %class.btQuadWord*
  %call76 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %79)
  %80 = load float, float* %call76, align 4
  %mul77 = fmul float %77, %80
  %sub78 = fsub float %sub73, %mul77
  store float %sub78, float* %ref.tmp58, align 4
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp58)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btAcosf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %cmp = fcmp olt float %0, -1.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float -1.000000e+00, float* %x.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load float, float* %x.addr, align 4
  %cmp1 = fcmp ogt float %1, 1.000000e+00
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store float 1.000000e+00, float* %x.addr, align 4
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %2 = load float, float* %x.addr, align 4
  %call = call float @acosf(float %2) #8
  ret float %call
}

; Function Attrs: nounwind readnone
declare float @acosf(float) #6

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btQuaternion* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %3 = load float, float* %arrayidx, align 4
  %mul = fmul float %3, %1
  store float %mul, float* %arrayidx, align 4
  %4 = load float*, float** %s.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %7, %5
  store float %mul4, float* %arrayidx3, align 4
  %8 = load float*, float** %s.addr, align 4
  %9 = load float, float* %8, align 4
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats5 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %11 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %11, %9
  store float %mul7, float* %arrayidx6, align 4
  %12 = load float*, float** %s.addr, align 4
  %13 = load float, float* %12, align 4
  %14 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats8 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %14, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 3
  %15 = load float, float* %arrayidx9, align 4
  %mul10 = fmul float %15, %13
  store float %mul10, float* %arrayidx9, align 4
  ret %class.btQuaternion* %this1
}

; Function Attrs: nounwind readnone
declare float @atan2f(float, float) #6

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #5

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sin.f32(float) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %_angle) #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4
  store float* %_angle, float** %_angle.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %0)
  store float %call, float* %d, align 4
  %1 = load float*, float** %_angle.addr, align 4
  %2 = load float, float* %1, align 4
  %mul = fmul float %2, 5.000000e-01
  %call2 = call float @_Z5btSinf(float %mul)
  %3 = load float, float* %d, align 4
  %div = fdiv float %call2, %3
  store float %div, float* %s, align 4
  %4 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %5 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %5)
  %6 = load float, float* %call3, align 4
  %7 = load float, float* %s, align 4
  %mul4 = fmul float %6, %7
  store float %mul4, float* %ref.tmp, align 4
  %8 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %8)
  %9 = load float, float* %call6, align 4
  %10 = load float, float* %s, align 4
  %mul7 = fmul float %9, %10
  store float %mul7, float* %ref.tmp5, align 4
  %11 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %11)
  %12 = load float, float* %call9, align 4
  %13 = load float, float* %s, align 4
  %mul10 = fmul float %12, %13
  store float %mul10, float* %ref.tmp8, align 4
  %14 = load float*, float** %_angle.addr, align 4
  %15 = load float, float* %14, align 4
  %mul12 = fmul float %15, 5.000000e-01
  %call13 = call float @_Z5btCosf(float %mul12)
  store float %call13, float* %ref.tmp11, align 4
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %4, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btTypedConstraint* @_ZN17btTypedConstraintD2Ev(%class.btTypedConstraint* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = bitcast %class.btTypedConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV17btTypedConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btTypedConstraint* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btConeTwistConstraintdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

declare i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint*, i8*, %class.btSerializer*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %this, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %dataOut.addr = alloca %struct.btTransformFloatData*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %struct.btTransformFloatData* %dataOut, %struct.btTransformFloatData** %dataOut.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4
  %m_basis2 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %0, i32 0, i32 0
  call void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %m_basis, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4
  %m_origin3 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %1, i32 0, i32 1
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_origin, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_origin3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %this, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %dataOut.addr = alloca %struct.btMatrix3x3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %struct.btMatrix3x3FloatData* %dataOut, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %1
  %2 = load %struct.btMatrix3x3FloatData*, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4
  %m_el2 = getelementptr inbounds %struct.btMatrix3x3FloatData, %struct.btMatrix3x3FloatData* %2, i32 0, i32 0
  %3 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [3 x %struct.btVector3FloatData], [3 x %struct.btVector3FloatData]* %m_el2, i32 0, i32 %3
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %arrayidx, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %2, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btConeTwistConstraint.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  call void @__cxx_global_var_init.1()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }
attributes #8 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
