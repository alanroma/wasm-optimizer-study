; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/NarrowPhaseCollision/btVoronoiSimplexSolver.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/NarrowPhaseCollision/btVoronoiSimplexSolver.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btVoronoiSimplexSolver = type <{ i32, [5 x %class.btVector3], [5 x %class.btVector3], [5 x %class.btVector3], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, i8, [3 x i8], %struct.btSubSimplexClosestResult, i8, [3 x i8] }>
%class.btVector3 = type { [4 x float] }
%struct.btSubSimplexClosestResult = type <{ %class.btVector3, %struct.btUsageBitfield, [2 x i8], [4 x float], i8, [3 x i8] }>
%struct.btUsageBitfield = type { i8, i8 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZNK22btVoronoiSimplexSolver11numVerticesEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN25btSubSimplexClosestResult5resetEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff = comdat any

$_ZN25btSubSimplexClosestResult7isValidEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK9btVector39distance2ERKS_ = comdat any

$_ZNK9btVector3eqERKS_ = comdat any

$_ZN15btUsageBitfield5resetEv = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN25btSubSimplexClosestResultC2Ev = comdat any

$_ZN15btUsageBitfieldC2Ev = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btVoronoiSimplexSolver.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN22btVoronoiSimplexSolver12removeVertexEi(%class.btVoronoiSimplexSolver* %this, i32 %index) #1 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %index.addr = alloca i32, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %m_numVertices = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_numVertices, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_numVertices, align 4
  %m_simplexVectorW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %m_numVertices2 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %1 = load i32, i32* %m_numVertices2, align 4
  %arrayidx = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW, i32 0, i32 %1
  %m_simplexVectorW3 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %2 = load i32, i32* %index.addr, align 4
  %arrayidx4 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW3, i32 0, i32 %2
  %3 = bitcast %class.btVector3* %arrayidx4 to i8*
  %4 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %m_simplexPointsP = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %m_numVertices5 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %5 = load i32, i32* %m_numVertices5, align 4
  %arrayidx6 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP, i32 0, i32 %5
  %m_simplexPointsP7 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %6 = load i32, i32* %index.addr, align 4
  %arrayidx8 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP7, i32 0, i32 %6
  %7 = bitcast %class.btVector3* %arrayidx8 to i8*
  %8 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %m_simplexPointsQ = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %m_numVertices9 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %9 = load i32, i32* %m_numVertices9, align 4
  %arrayidx10 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ, i32 0, i32 %9
  %m_simplexPointsQ11 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %10 = load i32, i32* %index.addr, align 4
  %arrayidx12 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ11, i32 0, i32 %10
  %11 = bitcast %class.btVector3* %arrayidx12 to i8*
  %12 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline optnone
define hidden void @_ZN22btVoronoiSimplexSolver14reduceVerticesERK15btUsageBitfield(%class.btVoronoiSimplexSolver* %this, %struct.btUsageBitfield* nonnull align 2 dereferenceable(1) %usedVerts) #3 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %usedVerts.addr = alloca %struct.btUsageBitfield*, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4
  store %struct.btUsageBitfield* %usedVerts, %struct.btUsageBitfield** %usedVerts.addr, align 4
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %call = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  %cmp = icmp sge i32 %call, 4
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %0 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %usedVerts.addr, align 4
  %1 = bitcast %struct.btUsageBitfield* %0 to i8*
  %bf.load = load i8, i8* %1, align 2
  %bf.lshr = lshr i8 %bf.load, 3
  %bf.clear = and i8 %bf.lshr, 1
  %bf.cast = zext i8 %bf.clear to i16
  %tobool = icmp ne i16 %bf.cast, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %land.lhs.true
  call void @_ZN22btVoronoiSimplexSolver12removeVertexEi(%class.btVoronoiSimplexSolver* %this1, i32 3)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %call2 = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  %cmp3 = icmp sge i32 %call2, 3
  br i1 %cmp3, label %land.lhs.true4, label %if.end11

land.lhs.true4:                                   ; preds = %if.end
  %2 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %usedVerts.addr, align 4
  %3 = bitcast %struct.btUsageBitfield* %2 to i8*
  %bf.load5 = load i8, i8* %3, align 2
  %bf.lshr6 = lshr i8 %bf.load5, 2
  %bf.clear7 = and i8 %bf.lshr6, 1
  %bf.cast8 = zext i8 %bf.clear7 to i16
  %tobool9 = icmp ne i16 %bf.cast8, 0
  br i1 %tobool9, label %if.end11, label %if.then10

if.then10:                                        ; preds = %land.lhs.true4
  call void @_ZN22btVoronoiSimplexSolver12removeVertexEi(%class.btVoronoiSimplexSolver* %this1, i32 2)
  br label %if.end11

if.end11:                                         ; preds = %if.then10, %land.lhs.true4, %if.end
  %call12 = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  %cmp13 = icmp sge i32 %call12, 2
  br i1 %cmp13, label %land.lhs.true14, label %if.end21

land.lhs.true14:                                  ; preds = %if.end11
  %4 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %usedVerts.addr, align 4
  %5 = bitcast %struct.btUsageBitfield* %4 to i8*
  %bf.load15 = load i8, i8* %5, align 2
  %bf.lshr16 = lshr i8 %bf.load15, 1
  %bf.clear17 = and i8 %bf.lshr16, 1
  %bf.cast18 = zext i8 %bf.clear17 to i16
  %tobool19 = icmp ne i16 %bf.cast18, 0
  br i1 %tobool19, label %if.end21, label %if.then20

if.then20:                                        ; preds = %land.lhs.true14
  call void @_ZN22btVoronoiSimplexSolver12removeVertexEi(%class.btVoronoiSimplexSolver* %this1, i32 1)
  br label %if.end21

if.end21:                                         ; preds = %if.then20, %land.lhs.true14, %if.end11
  %call22 = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  %cmp23 = icmp sge i32 %call22, 1
  br i1 %cmp23, label %land.lhs.true24, label %if.end30

land.lhs.true24:                                  ; preds = %if.end21
  %6 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %usedVerts.addr, align 4
  %7 = bitcast %struct.btUsageBitfield* %6 to i8*
  %bf.load25 = load i8, i8* %7, align 2
  %bf.clear26 = and i8 %bf.load25, 1
  %bf.cast27 = zext i8 %bf.clear26 to i16
  %tobool28 = icmp ne i16 %bf.cast27, 0
  br i1 %tobool28, label %if.end30, label %if.then29

if.then29:                                        ; preds = %land.lhs.true24
  call void @_ZN22btVoronoiSimplexSolver12removeVertexEi(%class.btVoronoiSimplexSolver* %this1, i32 0)
  br label %if.end30

if.end30:                                         ; preds = %if.then29, %land.lhs.true24, %if.end21
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %m_numVertices = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_numVertices, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define hidden void @_ZN22btVoronoiSimplexSolver5resetEv(%class.btVoronoiSimplexSolver* %this) #3 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %m_cachedValidClosest = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  store i8 0, i8* %m_cachedValidClosest, align 4
  %m_numVertices = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  store i32 0, i32* %m_numVertices, align 4
  %m_needsUpdate = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 12
  store i8 1, i8* %m_needsUpdate, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp2, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp3, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp4, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %m_lastW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 7
  %0 = bitcast %class.btVector3* %m_lastW to i8*
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %m_cachedBC = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  call void @_ZN25btSubSimplexClosestResult5resetEv(%struct.btSubSimplexClosestResult* %m_cachedBC)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN25btSubSimplexClosestResult5resetEv(%struct.btSubSimplexClosestResult* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btSubSimplexClosestResult*, align 4
  store %struct.btSubSimplexClosestResult* %this, %struct.btSubSimplexClosestResult** %this.addr, align 4
  %this1 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %this.addr, align 4
  %m_degenerate = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 4
  store i8 0, i8* %m_degenerate, align 4
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %this1, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00)
  %m_usedVertices = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 1
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %m_usedVertices)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN22btVoronoiSimplexSolver9addVertexERK9btVector3S2_S2_(%class.btVoronoiSimplexSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %w, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %q) #1 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %w.addr = alloca %class.btVector3*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %q.addr = alloca %class.btVector3*, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4
  store %class.btVector3* %w, %class.btVector3** %w.addr, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4
  store %class.btVector3* %q, %class.btVector3** %q.addr, align 4
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %m_lastW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 7
  %1 = bitcast %class.btVector3* %m_lastW to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %m_needsUpdate = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 12
  store i8 1, i8* %m_needsUpdate, align 4
  %3 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %m_simplexVectorW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %m_numVertices = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %4 = load i32, i32* %m_numVertices, align 4
  %arrayidx = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW, i32 0, i32 %4
  %5 = bitcast %class.btVector3* %arrayidx to i8*
  %6 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  %7 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %m_simplexPointsP = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %m_numVertices2 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %8 = load i32, i32* %m_numVertices2, align 4
  %arrayidx3 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP, i32 0, i32 %8
  %9 = bitcast %class.btVector3* %arrayidx3 to i8*
  %10 = bitcast %class.btVector3* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  %11 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %m_simplexPointsQ = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %m_numVertices4 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %12 = load i32, i32* %m_numVertices4, align 4
  %arrayidx5 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ, i32 0, i32 %12
  %13 = bitcast %class.btVector3* %arrayidx5 to i8*
  %14 = bitcast %class.btVector3* %11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false)
  %m_numVertices6 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %15 = load i32, i32* %m_numVertices6, align 4
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %m_numVertices6, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN22btVoronoiSimplexSolver28updateClosestVectorAndPointsEv(%class.btVoronoiSimplexSolver* %this) #3 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %from = alloca %class.btVector3*, align 4
  %to = alloca %class.btVector3*, align 4
  %nearest = alloca %class.btVector3, align 4
  %p = alloca %class.btVector3, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %diff = alloca %class.btVector3, align 4
  %v = alloca %class.btVector3, align 4
  %t = alloca float, align 4
  %dotVV = alloca float, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp48 = alloca %class.btVector3, align 4
  %ref.tmp49 = alloca %class.btVector3, align 4
  %ref.tmp50 = alloca %class.btVector3, align 4
  %ref.tmp53 = alloca %class.btVector3, align 4
  %ref.tmp54 = alloca %class.btVector3, align 4
  %ref.tmp60 = alloca %class.btVector3, align 4
  %ref.tmp63 = alloca %class.btVector3, align 4
  %ref.tmp64 = alloca %class.btVector3, align 4
  %ref.tmp70 = alloca %class.btVector3, align 4
  %p81 = alloca %class.btVector3, align 4
  %ref.tmp82 = alloca float, align 4
  %ref.tmp83 = alloca float, align 4
  %ref.tmp84 = alloca float, align 4
  %a = alloca %class.btVector3*, align 4
  %b = alloca %class.btVector3*, align 4
  %c = alloca %class.btVector3*, align 4
  %ref.tmp94 = alloca %class.btVector3, align 4
  %ref.tmp95 = alloca %class.btVector3, align 4
  %ref.tmp96 = alloca %class.btVector3, align 4
  %ref.tmp101 = alloca %class.btVector3, align 4
  %ref.tmp107 = alloca %class.btVector3, align 4
  %ref.tmp114 = alloca %class.btVector3, align 4
  %ref.tmp115 = alloca %class.btVector3, align 4
  %ref.tmp116 = alloca %class.btVector3, align 4
  %ref.tmp122 = alloca %class.btVector3, align 4
  %ref.tmp128 = alloca %class.btVector3, align 4
  %ref.tmp135 = alloca %class.btVector3, align 4
  %p146 = alloca %class.btVector3, align 4
  %ref.tmp147 = alloca float, align 4
  %ref.tmp148 = alloca float, align 4
  %ref.tmp149 = alloca float, align 4
  %a151 = alloca %class.btVector3*, align 4
  %b154 = alloca %class.btVector3*, align 4
  %c157 = alloca %class.btVector3*, align 4
  %d = alloca %class.btVector3*, align 4
  %hasSeperation = alloca i8, align 1
  %ref.tmp167 = alloca %class.btVector3, align 4
  %ref.tmp168 = alloca %class.btVector3, align 4
  %ref.tmp169 = alloca %class.btVector3, align 4
  %ref.tmp170 = alloca %class.btVector3, align 4
  %ref.tmp176 = alloca %class.btVector3, align 4
  %ref.tmp182 = alloca %class.btVector3, align 4
  %ref.tmp188 = alloca %class.btVector3, align 4
  %ref.tmp195 = alloca %class.btVector3, align 4
  %ref.tmp196 = alloca %class.btVector3, align 4
  %ref.tmp197 = alloca %class.btVector3, align 4
  %ref.tmp198 = alloca %class.btVector3, align 4
  %ref.tmp204 = alloca %class.btVector3, align 4
  %ref.tmp210 = alloca %class.btVector3, align 4
  %ref.tmp216 = alloca %class.btVector3, align 4
  %ref.tmp223 = alloca %class.btVector3, align 4
  %ref.tmp237 = alloca float, align 4
  %ref.tmp238 = alloca float, align 4
  %ref.tmp239 = alloca float, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %m_needsUpdate = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 12
  %0 = load i8, i8* %m_needsUpdate, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end247

if.then:                                          ; preds = %entry
  %m_cachedBC = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  call void @_ZN25btSubSimplexClosestResult5resetEv(%struct.btSubSimplexClosestResult* %m_cachedBC)
  %m_needsUpdate2 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 12
  store i8 0, i8* %m_needsUpdate2, align 4
  %call = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  switch i32 %call, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb3
    i32 2, label %sw.bb12
    i32 3, label %sw.bb80
    i32 4, label %sw.bb145
  ]

sw.bb:                                            ; preds = %if.then
  %m_cachedValidClosest = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  store i8 0, i8* %m_cachedValidClosest, align 4
  br label %sw.epilog

sw.bb3:                                           ; preds = %if.then
  %m_simplexPointsP = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP, i32 0, i32 0
  %m_cachedP1 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %1 = bitcast %class.btVector3* %m_cachedP1 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %m_simplexPointsQ = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx4 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ, i32 0, i32 0
  %m_cachedP2 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  %3 = bitcast %class.btVector3* %m_cachedP2 to i8*
  %4 = bitcast %class.btVector3* %arrayidx4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %m_cachedP15 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %m_cachedP26 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedP15, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedP26)
  %m_cachedV = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 6
  %5 = bitcast %class.btVector3* %m_cachedV to i8*
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  %m_cachedBC7 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  call void @_ZN25btSubSimplexClosestResult5resetEv(%struct.btSubSimplexClosestResult* %m_cachedBC7)
  %m_cachedBC8 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %m_cachedBC8, float 1.000000e+00, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00)
  %m_cachedBC9 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %call10 = call zeroext i1 @_ZN25btSubSimplexClosestResult7isValidEv(%struct.btSubSimplexClosestResult* %m_cachedBC9)
  %m_cachedValidClosest11 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  %frombool = zext i1 %call10 to i8
  store i8 %frombool, i8* %m_cachedValidClosest11, align 4
  br label %sw.epilog

sw.bb12:                                          ; preds = %if.then
  %m_simplexVectorW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %arrayidx13 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW, i32 0, i32 0
  store %class.btVector3* %arrayidx13, %class.btVector3** %from, align 4
  %m_simplexVectorW14 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %arrayidx15 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW14, i32 0, i32 1
  store %class.btVector3* %arrayidx15, %class.btVector3** %to, align 4
  %call16 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %nearest)
  store float 0.000000e+00, float* %ref.tmp17, align 4
  store float 0.000000e+00, float* %ref.tmp18, align 4
  store float 0.000000e+00, float* %ref.tmp19, align 4
  %call20 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %p, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp19)
  %7 = load %class.btVector3*, %class.btVector3** %from, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %diff, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %7)
  %8 = load %class.btVector3*, %class.btVector3** %to, align 4
  %9 = load %class.btVector3*, %class.btVector3** %from, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  %call21 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %v, %class.btVector3* nonnull align 4 dereferenceable(16) %diff)
  store float %call21, float* %t, align 4
  %10 = load float, float* %t, align 4
  %cmp = fcmp ogt float %10, 0.000000e+00
  br i1 %cmp, label %if.then22, label %if.else40

if.then22:                                        ; preds = %sw.bb12
  %call23 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %v, %class.btVector3* nonnull align 4 dereferenceable(16) %v)
  store float %call23, float* %dotVV, align 4
  %11 = load float, float* %t, align 4
  %12 = load float, float* %dotVV, align 4
  %cmp24 = fcmp olt float %11, %12
  br i1 %cmp24, label %if.then25, label %if.else

if.then25:                                        ; preds = %if.then22
  %13 = load float, float* %dotVV, align 4
  %14 = load float, float* %t, align 4
  %div = fdiv float %14, %13
  store float %div, float* %t, align 4
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp26, float* nonnull align 4 dereferenceable(4) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %v)
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %diff, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp26)
  %m_cachedBC28 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_usedVertices = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC28, i32 0, i32 1
  %15 = bitcast %struct.btUsageBitfield* %m_usedVertices to i8*
  %bf.load = load i8, i8* %15, align 4
  %bf.clear = and i8 %bf.load, -2
  %bf.set = or i8 %bf.clear, 1
  store i8 %bf.set, i8* %15, align 4
  %m_cachedBC29 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_usedVertices30 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC29, i32 0, i32 1
  %16 = bitcast %struct.btUsageBitfield* %m_usedVertices30 to i8*
  %bf.load31 = load i8, i8* %16, align 4
  %bf.clear32 = and i8 %bf.load31, -3
  %bf.set33 = or i8 %bf.clear32, 2
  store i8 %bf.set33, i8* %16, align 4
  br label %if.end

if.else:                                          ; preds = %if.then22
  store float 1.000000e+00, float* %t, align 4
  %call34 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %diff, %class.btVector3* nonnull align 4 dereferenceable(16) %v)
  %m_cachedBC35 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_usedVertices36 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC35, i32 0, i32 1
  %17 = bitcast %struct.btUsageBitfield* %m_usedVertices36 to i8*
  %bf.load37 = load i8, i8* %17, align 4
  %bf.clear38 = and i8 %bf.load37, -3
  %bf.set39 = or i8 %bf.clear38, 2
  store i8 %bf.set39, i8* %17, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then25
  br label %if.end46

if.else40:                                        ; preds = %sw.bb12
  store float 0.000000e+00, float* %t, align 4
  %m_cachedBC41 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_usedVertices42 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC41, i32 0, i32 1
  %18 = bitcast %struct.btUsageBitfield* %m_usedVertices42 to i8*
  %bf.load43 = load i8, i8* %18, align 4
  %bf.clear44 = and i8 %bf.load43, -2
  %bf.set45 = or i8 %bf.clear44, 1
  store i8 %bf.set45, i8* %18, align 4
  br label %if.end46

if.end46:                                         ; preds = %if.else40, %if.end
  %m_cachedBC47 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %19 = load float, float* %t, align 4
  %sub = fsub float 1.000000e+00, %19
  %20 = load float, float* %t, align 4
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %m_cachedBC47, float %sub, float %20, float 0.000000e+00, float 0.000000e+00)
  %21 = load %class.btVector3*, %class.btVector3** %from, align 4
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp49, float* nonnull align 4 dereferenceable(4) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %v)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp48, %class.btVector3* nonnull align 4 dereferenceable(16) %21, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp49)
  %22 = bitcast %class.btVector3* %nearest to i8*
  %23 = bitcast %class.btVector3* %ref.tmp48 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 16, i1 false)
  %m_simplexPointsP51 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx52 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP51, i32 0, i32 0
  %m_simplexPointsP55 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx56 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP55, i32 0, i32 1
  %m_simplexPointsP57 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx58 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP57, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp54, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx56, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx58)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp53, float* nonnull align 4 dereferenceable(4) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp54)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp50, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx52, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp53)
  %m_cachedP159 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %24 = bitcast %class.btVector3* %m_cachedP159 to i8*
  %25 = bitcast %class.btVector3* %ref.tmp50 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %24, i8* align 4 %25, i32 16, i1 false)
  %m_simplexPointsQ61 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx62 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ61, i32 0, i32 0
  %m_simplexPointsQ65 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx66 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ65, i32 0, i32 1
  %m_simplexPointsQ67 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx68 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ67, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp64, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx66, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx68)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp63, float* nonnull align 4 dereferenceable(4) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp64)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp60, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx62, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp63)
  %m_cachedP269 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  %26 = bitcast %class.btVector3* %m_cachedP269 to i8*
  %27 = bitcast %class.btVector3* %ref.tmp60 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 16, i1 false)
  %m_cachedP171 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %m_cachedP272 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp70, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedP171, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedP272)
  %m_cachedV73 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 6
  %28 = bitcast %class.btVector3* %m_cachedV73 to i8*
  %29 = bitcast %class.btVector3* %ref.tmp70 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false)
  %m_cachedBC74 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_usedVertices75 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC74, i32 0, i32 1
  call void @_ZN22btVoronoiSimplexSolver14reduceVerticesERK15btUsageBitfield(%class.btVoronoiSimplexSolver* %this1, %struct.btUsageBitfield* nonnull align 2 dereferenceable(1) %m_usedVertices75)
  %m_cachedBC76 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %call77 = call zeroext i1 @_ZN25btSubSimplexClosestResult7isValidEv(%struct.btSubSimplexClosestResult* %m_cachedBC76)
  %m_cachedValidClosest78 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  %frombool79 = zext i1 %call77 to i8
  store i8 %frombool79, i8* %m_cachedValidClosest78, align 4
  br label %sw.epilog

sw.bb80:                                          ; preds = %if.then
  store float 0.000000e+00, float* %ref.tmp82, align 4
  store float 0.000000e+00, float* %ref.tmp83, align 4
  store float 0.000000e+00, float* %ref.tmp84, align 4
  %call85 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %p81, float* nonnull align 4 dereferenceable(4) %ref.tmp82, float* nonnull align 4 dereferenceable(4) %ref.tmp83, float* nonnull align 4 dereferenceable(4) %ref.tmp84)
  %m_simplexVectorW86 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %arrayidx87 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW86, i32 0, i32 0
  store %class.btVector3* %arrayidx87, %class.btVector3** %a, align 4
  %m_simplexVectorW88 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %arrayidx89 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW88, i32 0, i32 1
  store %class.btVector3* %arrayidx89, %class.btVector3** %b, align 4
  %m_simplexVectorW90 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %arrayidx91 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW90, i32 0, i32 2
  store %class.btVector3* %arrayidx91, %class.btVector3** %c, align 4
  %30 = load %class.btVector3*, %class.btVector3** %a, align 4
  %31 = load %class.btVector3*, %class.btVector3** %b, align 4
  %32 = load %class.btVector3*, %class.btVector3** %c, align 4
  %m_cachedBC92 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %call93 = call zeroext i1 @_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %p81, %class.btVector3* nonnull align 4 dereferenceable(16) %30, %class.btVector3* nonnull align 4 dereferenceable(16) %31, %class.btVector3* nonnull align 4 dereferenceable(16) %32, %struct.btSubSimplexClosestResult* nonnull align 4 dereferenceable(37) %m_cachedBC92)
  %m_simplexPointsP97 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx98 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP97, i32 0, i32 0
  %m_cachedBC99 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC99, i32 0, i32 3
  %arrayidx100 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords, i32 0, i32 0
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp96, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx98, float* nonnull align 4 dereferenceable(4) %arrayidx100)
  %m_simplexPointsP102 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx103 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP102, i32 0, i32 1
  %m_cachedBC104 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords105 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC104, i32 0, i32 3
  %arrayidx106 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords105, i32 0, i32 1
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp101, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx103, float* nonnull align 4 dereferenceable(4) %arrayidx106)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp95, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp96, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp101)
  %m_simplexPointsP108 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx109 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP108, i32 0, i32 2
  %m_cachedBC110 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords111 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC110, i32 0, i32 3
  %arrayidx112 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords111, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp107, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx109, float* nonnull align 4 dereferenceable(4) %arrayidx112)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp94, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp95, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp107)
  %m_cachedP1113 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %33 = bitcast %class.btVector3* %m_cachedP1113 to i8*
  %34 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 4 %34, i32 16, i1 false)
  %m_simplexPointsQ117 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx118 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ117, i32 0, i32 0
  %m_cachedBC119 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords120 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC119, i32 0, i32 3
  %arrayidx121 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords120, i32 0, i32 0
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp116, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx118, float* nonnull align 4 dereferenceable(4) %arrayidx121)
  %m_simplexPointsQ123 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx124 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ123, i32 0, i32 1
  %m_cachedBC125 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords126 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC125, i32 0, i32 3
  %arrayidx127 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords126, i32 0, i32 1
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp122, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx124, float* nonnull align 4 dereferenceable(4) %arrayidx127)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp115, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp116, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp122)
  %m_simplexPointsQ129 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx130 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ129, i32 0, i32 2
  %m_cachedBC131 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords132 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC131, i32 0, i32 3
  %arrayidx133 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords132, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp128, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx130, float* nonnull align 4 dereferenceable(4) %arrayidx133)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp114, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp115, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp128)
  %m_cachedP2134 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  %35 = bitcast %class.btVector3* %m_cachedP2134 to i8*
  %36 = bitcast %class.btVector3* %ref.tmp114 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %35, i8* align 4 %36, i32 16, i1 false)
  %m_cachedP1136 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %m_cachedP2137 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp135, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedP1136, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedP2137)
  %m_cachedV138 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 6
  %37 = bitcast %class.btVector3* %m_cachedV138 to i8*
  %38 = bitcast %class.btVector3* %ref.tmp135 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %37, i8* align 4 %38, i32 16, i1 false)
  %m_cachedBC139 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_usedVertices140 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC139, i32 0, i32 1
  call void @_ZN22btVoronoiSimplexSolver14reduceVerticesERK15btUsageBitfield(%class.btVoronoiSimplexSolver* %this1, %struct.btUsageBitfield* nonnull align 2 dereferenceable(1) %m_usedVertices140)
  %m_cachedBC141 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %call142 = call zeroext i1 @_ZN25btSubSimplexClosestResult7isValidEv(%struct.btSubSimplexClosestResult* %m_cachedBC141)
  %m_cachedValidClosest143 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  %frombool144 = zext i1 %call142 to i8
  store i8 %frombool144, i8* %m_cachedValidClosest143, align 4
  br label %sw.epilog

sw.bb145:                                         ; preds = %if.then
  store float 0.000000e+00, float* %ref.tmp147, align 4
  store float 0.000000e+00, float* %ref.tmp148, align 4
  store float 0.000000e+00, float* %ref.tmp149, align 4
  %call150 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %p146, float* nonnull align 4 dereferenceable(4) %ref.tmp147, float* nonnull align 4 dereferenceable(4) %ref.tmp148, float* nonnull align 4 dereferenceable(4) %ref.tmp149)
  %m_simplexVectorW152 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %arrayidx153 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW152, i32 0, i32 0
  store %class.btVector3* %arrayidx153, %class.btVector3** %a151, align 4
  %m_simplexVectorW155 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %arrayidx156 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW155, i32 0, i32 1
  store %class.btVector3* %arrayidx156, %class.btVector3** %b154, align 4
  %m_simplexVectorW158 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %arrayidx159 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW158, i32 0, i32 2
  store %class.btVector3* %arrayidx159, %class.btVector3** %c157, align 4
  %m_simplexVectorW160 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %arrayidx161 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW160, i32 0, i32 3
  store %class.btVector3* %arrayidx161, %class.btVector3** %d, align 4
  %39 = load %class.btVector3*, %class.btVector3** %a151, align 4
  %40 = load %class.btVector3*, %class.btVector3** %b154, align 4
  %41 = load %class.btVector3*, %class.btVector3** %c157, align 4
  %42 = load %class.btVector3*, %class.btVector3** %d, align 4
  %m_cachedBC162 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %call163 = call zeroext i1 @_ZN22btVoronoiSimplexSolver25closestPtPointTetrahedronERK9btVector3S2_S2_S2_S2_R25btSubSimplexClosestResult(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %p146, %class.btVector3* nonnull align 4 dereferenceable(16) %39, %class.btVector3* nonnull align 4 dereferenceable(16) %40, %class.btVector3* nonnull align 4 dereferenceable(16) %41, %class.btVector3* nonnull align 4 dereferenceable(16) %42, %struct.btSubSimplexClosestResult* nonnull align 4 dereferenceable(37) %m_cachedBC162)
  %frombool164 = zext i1 %call163 to i8
  store i8 %frombool164, i8* %hasSeperation, align 1
  %43 = load i8, i8* %hasSeperation, align 1
  %tobool165 = trunc i8 %43 to i1
  br i1 %tobool165, label %if.then166, label %if.else229

if.then166:                                       ; preds = %sw.bb145
  %m_simplexPointsP171 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx172 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP171, i32 0, i32 0
  %m_cachedBC173 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords174 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC173, i32 0, i32 3
  %arrayidx175 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords174, i32 0, i32 0
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp170, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx172, float* nonnull align 4 dereferenceable(4) %arrayidx175)
  %m_simplexPointsP177 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx178 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP177, i32 0, i32 1
  %m_cachedBC179 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords180 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC179, i32 0, i32 3
  %arrayidx181 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords180, i32 0, i32 1
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp176, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx178, float* nonnull align 4 dereferenceable(4) %arrayidx181)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp169, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp170, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp176)
  %m_simplexPointsP183 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx184 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP183, i32 0, i32 2
  %m_cachedBC185 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords186 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC185, i32 0, i32 3
  %arrayidx187 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords186, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp182, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx184, float* nonnull align 4 dereferenceable(4) %arrayidx187)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp168, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp169, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp182)
  %m_simplexPointsP189 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %arrayidx190 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP189, i32 0, i32 3
  %m_cachedBC191 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords192 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC191, i32 0, i32 3
  %arrayidx193 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords192, i32 0, i32 3
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp188, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx190, float* nonnull align 4 dereferenceable(4) %arrayidx193)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp167, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp168, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp188)
  %m_cachedP1194 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %44 = bitcast %class.btVector3* %m_cachedP1194 to i8*
  %45 = bitcast %class.btVector3* %ref.tmp167 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %44, i8* align 4 %45, i32 16, i1 false)
  %m_simplexPointsQ199 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx200 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ199, i32 0, i32 0
  %m_cachedBC201 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords202 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC201, i32 0, i32 3
  %arrayidx203 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords202, i32 0, i32 0
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp198, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx200, float* nonnull align 4 dereferenceable(4) %arrayidx203)
  %m_simplexPointsQ205 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx206 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ205, i32 0, i32 1
  %m_cachedBC207 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords208 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC207, i32 0, i32 3
  %arrayidx209 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords208, i32 0, i32 1
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp204, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx206, float* nonnull align 4 dereferenceable(4) %arrayidx209)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp197, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp198, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp204)
  %m_simplexPointsQ211 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx212 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ211, i32 0, i32 2
  %m_cachedBC213 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords214 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC213, i32 0, i32 3
  %arrayidx215 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords214, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp210, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx212, float* nonnull align 4 dereferenceable(4) %arrayidx215)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp196, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp197, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp210)
  %m_simplexPointsQ217 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %arrayidx218 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ217, i32 0, i32 3
  %m_cachedBC219 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_barycentricCoords220 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC219, i32 0, i32 3
  %arrayidx221 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords220, i32 0, i32 3
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp216, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx218, float* nonnull align 4 dereferenceable(4) %arrayidx221)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp195, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp196, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp216)
  %m_cachedP2222 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  %46 = bitcast %class.btVector3* %m_cachedP2222 to i8*
  %47 = bitcast %class.btVector3* %ref.tmp195 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %46, i8* align 4 %47, i32 16, i1 false)
  %m_cachedP1224 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %m_cachedP2225 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp223, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedP1224, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedP2225)
  %m_cachedV226 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 6
  %48 = bitcast %class.btVector3* %m_cachedV226 to i8*
  %49 = bitcast %class.btVector3* %ref.tmp223 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %48, i8* align 4 %49, i32 16, i1 false)
  %m_cachedBC227 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_usedVertices228 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC227, i32 0, i32 1
  call void @_ZN22btVoronoiSimplexSolver14reduceVerticesERK15btUsageBitfield(%class.btVoronoiSimplexSolver* %this1, %struct.btUsageBitfield* nonnull align 2 dereferenceable(1) %m_usedVertices228)
  br label %if.end241

if.else229:                                       ; preds = %sw.bb145
  %m_cachedBC230 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %m_degenerate = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %m_cachedBC230, i32 0, i32 4
  %50 = load i8, i8* %m_degenerate, align 4
  %tobool231 = trunc i8 %50 to i1
  br i1 %tobool231, label %if.then232, label %if.else234

if.then232:                                       ; preds = %if.else229
  %m_cachedValidClosest233 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  store i8 0, i8* %m_cachedValidClosest233, align 4
  br label %if.end240

if.else234:                                       ; preds = %if.else229
  %m_cachedValidClosest235 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  store i8 1, i8* %m_cachedValidClosest235, align 4
  %m_cachedV236 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 6
  store float 0.000000e+00, float* %ref.tmp237, align 4
  store float 0.000000e+00, float* %ref.tmp238, align 4
  store float 0.000000e+00, float* %ref.tmp239, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_cachedV236, float* nonnull align 4 dereferenceable(4) %ref.tmp237, float* nonnull align 4 dereferenceable(4) %ref.tmp238, float* nonnull align 4 dereferenceable(4) %ref.tmp239)
  br label %if.end240

if.end240:                                        ; preds = %if.else234, %if.then232
  br label %sw.epilog

if.end241:                                        ; preds = %if.then166
  %m_cachedBC242 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %call243 = call zeroext i1 @_ZN25btSubSimplexClosestResult7isValidEv(%struct.btSubSimplexClosestResult* %m_cachedBC242)
  %m_cachedValidClosest244 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  %frombool245 = zext i1 %call243 to i8
  store i8 %frombool245, i8* %m_cachedValidClosest244, align 4
  br label %sw.epilog

sw.default:                                       ; preds = %if.then
  %m_cachedValidClosest246 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  store i8 0, i8* %m_cachedValidClosest246, align 4
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %if.end241, %if.end240, %sw.bb80, %if.end46, %sw.bb3, %sw.bb
  br label %if.end247

if.end247:                                        ; preds = %sw.epilog, %entry
  %m_cachedValidClosest248 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 9
  %51 = load i8, i8* %m_cachedValidClosest248, align 4
  %tobool249 = trunc i8 %51 to i1
  ret i1 %tobool249
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %this, float %a, float %b, float %c, float %d) #1 comdat {
entry:
  %this.addr = alloca %struct.btSubSimplexClosestResult*, align 4
  %a.addr = alloca float, align 4
  %b.addr = alloca float, align 4
  %c.addr = alloca float, align 4
  %d.addr = alloca float, align 4
  store %struct.btSubSimplexClosestResult* %this, %struct.btSubSimplexClosestResult** %this.addr, align 4
  store float %a, float* %a.addr, align 4
  store float %b, float* %b.addr, align 4
  store float %c, float* %c.addr, align 4
  store float %d, float* %d.addr, align 4
  %this1 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %this.addr, align 4
  %0 = load float, float* %a.addr, align 4
  %m_barycentricCoords = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 3
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords, i32 0, i32 0
  store float %0, float* %arrayidx, align 4
  %1 = load float, float* %b.addr, align 4
  %m_barycentricCoords2 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 3
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords2, i32 0, i32 1
  store float %1, float* %arrayidx3, align 4
  %2 = load float, float* %c.addr, align 4
  %m_barycentricCoords4 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 3
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords4, i32 0, i32 2
  store float %2, float* %arrayidx5, align 4
  %3 = load float, float* %d.addr, align 4
  %m_barycentricCoords6 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 3
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords6, i32 0, i32 3
  store float %3, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN25btSubSimplexClosestResult7isValidEv(%struct.btSubSimplexClosestResult* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btSubSimplexClosestResult*, align 4
  %valid = alloca i8, align 1
  store %struct.btSubSimplexClosestResult* %this, %struct.btSubSimplexClosestResult** %this.addr, align 4
  %this1 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %this.addr, align 4
  %m_barycentricCoords = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 3
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %cmp = fcmp oge float %0, 0.000000e+00
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %m_barycentricCoords2 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 3
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4
  %cmp4 = fcmp oge float %1, 0.000000e+00
  br i1 %cmp4, label %land.lhs.true5, label %land.end

land.lhs.true5:                                   ; preds = %land.lhs.true
  %m_barycentricCoords6 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 3
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords6, i32 0, i32 2
  %2 = load float, float* %arrayidx7, align 4
  %cmp8 = fcmp oge float %2, 0.000000e+00
  br i1 %cmp8, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true5
  %m_barycentricCoords9 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 3
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords9, i32 0, i32 3
  %3 = load float, float* %arrayidx10, align 4
  %cmp11 = fcmp oge float %3, 0.000000e+00
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true5, %land.lhs.true, %entry
  %4 = phi i1 [ false, %land.lhs.true5 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp11, %land.rhs ]
  %frombool = zext i1 %4 to i8
  store i8 %frombool, i8* %valid, align 1
  %5 = load i8, i8* %valid, align 1
  %tobool = trunc i8 %5 to i1
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult(%class.btVoronoiSimplexSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %a, %class.btVector3* nonnull align 4 dereferenceable(16) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c, %struct.btSubSimplexClosestResult* nonnull align 4 dereferenceable(37) %result) #3 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %a.addr = alloca %class.btVector3*, align 4
  %b.addr = alloca %class.btVector3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  %result.addr = alloca %struct.btSubSimplexClosestResult*, align 4
  %ab = alloca %class.btVector3, align 4
  %ac = alloca %class.btVector3, align 4
  %ap = alloca %class.btVector3, align 4
  %d1 = alloca float, align 4
  %d2 = alloca float, align 4
  %bp = alloca %class.btVector3, align 4
  %d3 = alloca float, align 4
  %d4 = alloca float, align 4
  %vc = alloca float, align 4
  %v = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp25 = alloca %class.btVector3, align 4
  %cp = alloca %class.btVector3, align 4
  %d5 = alloca float, align 4
  %d6 = alloca float, align 4
  %vb = alloca float, align 4
  %w = alloca float, align 4
  %ref.tmp60 = alloca %class.btVector3, align 4
  %ref.tmp61 = alloca %class.btVector3, align 4
  %va = alloca float, align 4
  %w84 = alloca float, align 4
  %ref.tmp89 = alloca %class.btVector3, align 4
  %ref.tmp90 = alloca %class.btVector3, align 4
  %ref.tmp91 = alloca %class.btVector3, align 4
  %denom = alloca float, align 4
  %v106 = alloca float, align 4
  %w108 = alloca float, align 4
  %ref.tmp110 = alloca %class.btVector3, align 4
  %ref.tmp111 = alloca %class.btVector3, align 4
  %ref.tmp112 = alloca %class.btVector3, align 4
  %ref.tmp113 = alloca %class.btVector3, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4
  store %class.btVector3* %a, %class.btVector3** %a.addr, align 4
  store %class.btVector3* %b, %class.btVector3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  store %struct.btSubSimplexClosestResult* %result, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %0 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_usedVertices = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %0, i32 0, i32 1
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %m_usedVertices)
  %1 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ab, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %3 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ac, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %5 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %6 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ap, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ab, %class.btVector3* nonnull align 4 dereferenceable(16) %ap)
  store float %call, float* %d1, align 4
  %call2 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ac, %class.btVector3* nonnull align 4 dereferenceable(16) %ap)
  store float %call2, float* %d2, align 4
  %7 = load float, float* %d1, align 4
  %cmp = fcmp ole float %7, 0.000000e+00
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %8 = load float, float* %d2, align 4
  %cmp3 = fcmp ole float %8, 0.000000e+00
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %9 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %10 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_closestPointOnSimplex = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %10, i32 0, i32 0
  %11 = bitcast %class.btVector3* %m_closestPointOnSimplex to i8*
  %12 = bitcast %class.btVector3* %9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false)
  %13 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_usedVertices4 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %13, i32 0, i32 1
  %14 = bitcast %struct.btUsageBitfield* %m_usedVertices4 to i8*
  %bf.load = load i8, i8* %14, align 4
  %bf.clear = and i8 %bf.load, -2
  %bf.set = or i8 %bf.clear, 1
  store i8 %bf.set, i8* %14, align 4
  %15 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %15, float 1.000000e+00, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00)
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %16 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %17 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %bp, %class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btVector3* nonnull align 4 dereferenceable(16) %17)
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ab, %class.btVector3* nonnull align 4 dereferenceable(16) %bp)
  store float %call5, float* %d3, align 4
  %call6 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ac, %class.btVector3* nonnull align 4 dereferenceable(16) %bp)
  store float %call6, float* %d4, align 4
  %18 = load float, float* %d3, align 4
  %cmp7 = fcmp oge float %18, 0.000000e+00
  br i1 %cmp7, label %land.lhs.true8, label %if.end16

land.lhs.true8:                                   ; preds = %if.end
  %19 = load float, float* %d4, align 4
  %20 = load float, float* %d3, align 4
  %cmp9 = fcmp ole float %19, %20
  br i1 %cmp9, label %if.then10, label %if.end16

if.then10:                                        ; preds = %land.lhs.true8
  %21 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %22 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_closestPointOnSimplex11 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %22, i32 0, i32 0
  %23 = bitcast %class.btVector3* %m_closestPointOnSimplex11 to i8*
  %24 = bitcast %class.btVector3* %21 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false)
  %25 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_usedVertices12 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %25, i32 0, i32 1
  %26 = bitcast %struct.btUsageBitfield* %m_usedVertices12 to i8*
  %bf.load13 = load i8, i8* %26, align 4
  %bf.clear14 = and i8 %bf.load13, -3
  %bf.set15 = or i8 %bf.clear14, 2
  store i8 %bf.set15, i8* %26, align 4
  %27 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %27, float 0.000000e+00, float 1.000000e+00, float 0.000000e+00, float 0.000000e+00)
  store i1 true, i1* %retval, align 1
  br label %return

if.end16:                                         ; preds = %land.lhs.true8, %if.end
  %28 = load float, float* %d1, align 4
  %29 = load float, float* %d4, align 4
  %mul = fmul float %28, %29
  %30 = load float, float* %d3, align 4
  %31 = load float, float* %d2, align 4
  %mul17 = fmul float %30, %31
  %sub = fsub float %mul, %mul17
  store float %sub, float* %vc, align 4
  %32 = load float, float* %vc, align 4
  %cmp18 = fcmp ole float %32, 0.000000e+00
  br i1 %cmp18, label %land.lhs.true19, label %if.end36

land.lhs.true19:                                  ; preds = %if.end16
  %33 = load float, float* %d1, align 4
  %cmp20 = fcmp oge float %33, 0.000000e+00
  br i1 %cmp20, label %land.lhs.true21, label %if.end36

land.lhs.true21:                                  ; preds = %land.lhs.true19
  %34 = load float, float* %d3, align 4
  %cmp22 = fcmp ole float %34, 0.000000e+00
  br i1 %cmp22, label %if.then23, label %if.end36

if.then23:                                        ; preds = %land.lhs.true21
  %35 = load float, float* %d1, align 4
  %36 = load float, float* %d1, align 4
  %37 = load float, float* %d3, align 4
  %sub24 = fsub float %36, %37
  %div = fdiv float %35, %sub24
  store float %div, float* %v, align 4
  %38 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp25, float* nonnull align 4 dereferenceable(4) %v, %class.btVector3* nonnull align 4 dereferenceable(16) %ab)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %38, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp25)
  %39 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_closestPointOnSimplex26 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %39, i32 0, i32 0
  %40 = bitcast %class.btVector3* %m_closestPointOnSimplex26 to i8*
  %41 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %40, i8* align 4 %41, i32 16, i1 false)
  %42 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_usedVertices27 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %42, i32 0, i32 1
  %43 = bitcast %struct.btUsageBitfield* %m_usedVertices27 to i8*
  %bf.load28 = load i8, i8* %43, align 4
  %bf.clear29 = and i8 %bf.load28, -2
  %bf.set30 = or i8 %bf.clear29, 1
  store i8 %bf.set30, i8* %43, align 4
  %44 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_usedVertices31 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %44, i32 0, i32 1
  %45 = bitcast %struct.btUsageBitfield* %m_usedVertices31 to i8*
  %bf.load32 = load i8, i8* %45, align 4
  %bf.clear33 = and i8 %bf.load32, -3
  %bf.set34 = or i8 %bf.clear33, 2
  store i8 %bf.set34, i8* %45, align 4
  %46 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %47 = load float, float* %v, align 4
  %sub35 = fsub float 1.000000e+00, %47
  %48 = load float, float* %v, align 4
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %46, float %sub35, float %48, float 0.000000e+00, float 0.000000e+00)
  store i1 true, i1* %retval, align 1
  br label %return

if.end36:                                         ; preds = %land.lhs.true21, %land.lhs.true19, %if.end16
  %49 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %50 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %cp, %class.btVector3* nonnull align 4 dereferenceable(16) %49, %class.btVector3* nonnull align 4 dereferenceable(16) %50)
  %call37 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ab, %class.btVector3* nonnull align 4 dereferenceable(16) %cp)
  store float %call37, float* %d5, align 4
  %call38 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ac, %class.btVector3* nonnull align 4 dereferenceable(16) %cp)
  store float %call38, float* %d6, align 4
  %51 = load float, float* %d6, align 4
  %cmp39 = fcmp oge float %51, 0.000000e+00
  br i1 %cmp39, label %land.lhs.true40, label %if.end48

land.lhs.true40:                                  ; preds = %if.end36
  %52 = load float, float* %d5, align 4
  %53 = load float, float* %d6, align 4
  %cmp41 = fcmp ole float %52, %53
  br i1 %cmp41, label %if.then42, label %if.end48

if.then42:                                        ; preds = %land.lhs.true40
  %54 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %55 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_closestPointOnSimplex43 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %55, i32 0, i32 0
  %56 = bitcast %class.btVector3* %m_closestPointOnSimplex43 to i8*
  %57 = bitcast %class.btVector3* %54 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %56, i8* align 4 %57, i32 16, i1 false)
  %58 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_usedVertices44 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %58, i32 0, i32 1
  %59 = bitcast %struct.btUsageBitfield* %m_usedVertices44 to i8*
  %bf.load45 = load i8, i8* %59, align 4
  %bf.clear46 = and i8 %bf.load45, -5
  %bf.set47 = or i8 %bf.clear46, 4
  store i8 %bf.set47, i8* %59, align 4
  %60 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %60, float 0.000000e+00, float 0.000000e+00, float 1.000000e+00, float 0.000000e+00)
  store i1 true, i1* %retval, align 1
  br label %return

if.end48:                                         ; preds = %land.lhs.true40, %if.end36
  %61 = load float, float* %d5, align 4
  %62 = load float, float* %d2, align 4
  %mul49 = fmul float %61, %62
  %63 = load float, float* %d1, align 4
  %64 = load float, float* %d6, align 4
  %mul50 = fmul float %63, %64
  %sub51 = fsub float %mul49, %mul50
  store float %sub51, float* %vb, align 4
  %65 = load float, float* %vb, align 4
  %cmp52 = fcmp ole float %65, 0.000000e+00
  br i1 %cmp52, label %land.lhs.true53, label %if.end72

land.lhs.true53:                                  ; preds = %if.end48
  %66 = load float, float* %d2, align 4
  %cmp54 = fcmp oge float %66, 0.000000e+00
  br i1 %cmp54, label %land.lhs.true55, label %if.end72

land.lhs.true55:                                  ; preds = %land.lhs.true53
  %67 = load float, float* %d6, align 4
  %cmp56 = fcmp ole float %67, 0.000000e+00
  br i1 %cmp56, label %if.then57, label %if.end72

if.then57:                                        ; preds = %land.lhs.true55
  %68 = load float, float* %d2, align 4
  %69 = load float, float* %d2, align 4
  %70 = load float, float* %d6, align 4
  %sub58 = fsub float %69, %70
  %div59 = fdiv float %68, %sub58
  store float %div59, float* %w, align 4
  %71 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp61, float* nonnull align 4 dereferenceable(4) %w, %class.btVector3* nonnull align 4 dereferenceable(16) %ac)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp60, %class.btVector3* nonnull align 4 dereferenceable(16) %71, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp61)
  %72 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_closestPointOnSimplex62 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %72, i32 0, i32 0
  %73 = bitcast %class.btVector3* %m_closestPointOnSimplex62 to i8*
  %74 = bitcast %class.btVector3* %ref.tmp60 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %73, i8* align 4 %74, i32 16, i1 false)
  %75 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_usedVertices63 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %75, i32 0, i32 1
  %76 = bitcast %struct.btUsageBitfield* %m_usedVertices63 to i8*
  %bf.load64 = load i8, i8* %76, align 4
  %bf.clear65 = and i8 %bf.load64, -2
  %bf.set66 = or i8 %bf.clear65, 1
  store i8 %bf.set66, i8* %76, align 4
  %77 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_usedVertices67 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %77, i32 0, i32 1
  %78 = bitcast %struct.btUsageBitfield* %m_usedVertices67 to i8*
  %bf.load68 = load i8, i8* %78, align 4
  %bf.clear69 = and i8 %bf.load68, -5
  %bf.set70 = or i8 %bf.clear69, 4
  store i8 %bf.set70, i8* %78, align 4
  %79 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %80 = load float, float* %w, align 4
  %sub71 = fsub float 1.000000e+00, %80
  %81 = load float, float* %w, align 4
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %79, float %sub71, float 0.000000e+00, float %81, float 0.000000e+00)
  store i1 true, i1* %retval, align 1
  br label %return

if.end72:                                         ; preds = %land.lhs.true55, %land.lhs.true53, %if.end48
  %82 = load float, float* %d3, align 4
  %83 = load float, float* %d6, align 4
  %mul73 = fmul float %82, %83
  %84 = load float, float* %d5, align 4
  %85 = load float, float* %d4, align 4
  %mul74 = fmul float %84, %85
  %sub75 = fsub float %mul73, %mul74
  store float %sub75, float* %va, align 4
  %86 = load float, float* %va, align 4
  %cmp76 = fcmp ole float %86, 0.000000e+00
  br i1 %cmp76, label %land.lhs.true77, label %if.end102

land.lhs.true77:                                  ; preds = %if.end72
  %87 = load float, float* %d4, align 4
  %88 = load float, float* %d3, align 4
  %sub78 = fsub float %87, %88
  %cmp79 = fcmp oge float %sub78, 0.000000e+00
  br i1 %cmp79, label %land.lhs.true80, label %if.end102

land.lhs.true80:                                  ; preds = %land.lhs.true77
  %89 = load float, float* %d5, align 4
  %90 = load float, float* %d6, align 4
  %sub81 = fsub float %89, %90
  %cmp82 = fcmp oge float %sub81, 0.000000e+00
  br i1 %cmp82, label %if.then83, label %if.end102

if.then83:                                        ; preds = %land.lhs.true80
  %91 = load float, float* %d4, align 4
  %92 = load float, float* %d3, align 4
  %sub85 = fsub float %91, %92
  %93 = load float, float* %d4, align 4
  %94 = load float, float* %d3, align 4
  %sub86 = fsub float %93, %94
  %95 = load float, float* %d5, align 4
  %96 = load float, float* %d6, align 4
  %sub87 = fsub float %95, %96
  %add = fadd float %sub86, %sub87
  %div88 = fdiv float %sub85, %add
  store float %div88, float* %w84, align 4
  %97 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %98 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %99 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp91, %class.btVector3* nonnull align 4 dereferenceable(16) %98, %class.btVector3* nonnull align 4 dereferenceable(16) %99)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp90, float* nonnull align 4 dereferenceable(4) %w84, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp91)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp89, %class.btVector3* nonnull align 4 dereferenceable(16) %97, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp90)
  %100 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_closestPointOnSimplex92 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %100, i32 0, i32 0
  %101 = bitcast %class.btVector3* %m_closestPointOnSimplex92 to i8*
  %102 = bitcast %class.btVector3* %ref.tmp89 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %101, i8* align 4 %102, i32 16, i1 false)
  %103 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_usedVertices93 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %103, i32 0, i32 1
  %104 = bitcast %struct.btUsageBitfield* %m_usedVertices93 to i8*
  %bf.load94 = load i8, i8* %104, align 4
  %bf.clear95 = and i8 %bf.load94, -3
  %bf.set96 = or i8 %bf.clear95, 2
  store i8 %bf.set96, i8* %104, align 4
  %105 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_usedVertices97 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %105, i32 0, i32 1
  %106 = bitcast %struct.btUsageBitfield* %m_usedVertices97 to i8*
  %bf.load98 = load i8, i8* %106, align 4
  %bf.clear99 = and i8 %bf.load98, -5
  %bf.set100 = or i8 %bf.clear99, 4
  store i8 %bf.set100, i8* %106, align 4
  %107 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %108 = load float, float* %w84, align 4
  %sub101 = fsub float 1.000000e+00, %108
  %109 = load float, float* %w84, align 4
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %107, float 0.000000e+00, float %sub101, float %109, float 0.000000e+00)
  store i1 true, i1* %retval, align 1
  br label %return

if.end102:                                        ; preds = %land.lhs.true80, %land.lhs.true77, %if.end72
  %110 = load float, float* %va, align 4
  %111 = load float, float* %vb, align 4
  %add103 = fadd float %110, %111
  %112 = load float, float* %vc, align 4
  %add104 = fadd float %add103, %112
  %div105 = fdiv float 1.000000e+00, %add104
  store float %div105, float* %denom, align 4
  %113 = load float, float* %vb, align 4
  %114 = load float, float* %denom, align 4
  %mul107 = fmul float %113, %114
  store float %mul107, float* %v106, align 4
  %115 = load float, float* %vc, align 4
  %116 = load float, float* %denom, align 4
  %mul109 = fmul float %115, %116
  store float %mul109, float* %w108, align 4
  %117 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp112, %class.btVector3* nonnull align 4 dereferenceable(16) %ab, float* nonnull align 4 dereferenceable(4) %v106)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp111, %class.btVector3* nonnull align 4 dereferenceable(16) %117, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp112)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp113, %class.btVector3* nonnull align 4 dereferenceable(16) %ac, float* nonnull align 4 dereferenceable(4) %w108)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp110, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp111, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp113)
  %118 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_closestPointOnSimplex114 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %118, i32 0, i32 0
  %119 = bitcast %class.btVector3* %m_closestPointOnSimplex114 to i8*
  %120 = bitcast %class.btVector3* %ref.tmp110 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %119, i8* align 4 %120, i32 16, i1 false)
  %121 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_usedVertices115 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %121, i32 0, i32 1
  %122 = bitcast %struct.btUsageBitfield* %m_usedVertices115 to i8*
  %bf.load116 = load i8, i8* %122, align 4
  %bf.clear117 = and i8 %bf.load116, -2
  %bf.set118 = or i8 %bf.clear117, 1
  store i8 %bf.set118, i8* %122, align 4
  %123 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_usedVertices119 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %123, i32 0, i32 1
  %124 = bitcast %struct.btUsageBitfield* %m_usedVertices119 to i8*
  %bf.load120 = load i8, i8* %124, align 4
  %bf.clear121 = and i8 %bf.load120, -3
  %bf.set122 = or i8 %bf.clear121, 2
  store i8 %bf.set122, i8* %124, align 4
  %125 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %m_usedVertices123 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %125, i32 0, i32 1
  %126 = bitcast %struct.btUsageBitfield* %m_usedVertices123 to i8*
  %bf.load124 = load i8, i8* %126, align 4
  %bf.clear125 = and i8 %bf.load124, -5
  %bf.set126 = or i8 %bf.clear125, 4
  store i8 %bf.set126, i8* %126, align 4
  %127 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %result.addr, align 4
  %128 = load float, float* %v106, align 4
  %sub127 = fsub float 1.000000e+00, %128
  %129 = load float, float* %w108, align 4
  %sub128 = fsub float %sub127, %129
  %130 = load float, float* %v106, align 4
  %131 = load float, float* %w108, align 4
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %127, float %sub128, float %130, float %131, float 0.000000e+00)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end102, %if.then83, %if.then57, %if.then42, %if.then23, %if.then10, %if.then
  %132 = load i1, i1* %retval, align 1
  ret i1 %132
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN22btVoronoiSimplexSolver25closestPtPointTetrahedronERK9btVector3S2_S2_S2_S2_R25btSubSimplexClosestResult(%class.btVoronoiSimplexSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %a, %class.btVector3* nonnull align 4 dereferenceable(16) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c, %class.btVector3* nonnull align 4 dereferenceable(16) %d, %struct.btSubSimplexClosestResult* nonnull align 4 dereferenceable(37) %finalResult) #3 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %a.addr = alloca %class.btVector3*, align 4
  %b.addr = alloca %class.btVector3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  %d.addr = alloca %class.btVector3*, align 4
  %finalResult.addr = alloca %struct.btSubSimplexClosestResult*, align 4
  %tempResult = alloca %struct.btSubSimplexClosestResult, align 4
  %pointOutsideABC = alloca i32, align 4
  %pointOutsideACD = alloca i32, align 4
  %pointOutsideADB = alloca i32, align 4
  %pointOutsideBDC = alloca i32, align 4
  %bestSqDist = alloca float, align 4
  %q = alloca %class.btVector3, align 4
  %sqDist = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp35 = alloca %class.btVector3, align 4
  %q77 = alloca %class.btVector3, align 4
  %sqDist79 = alloca float, align 4
  %ref.tmp80 = alloca %class.btVector3, align 4
  %ref.tmp81 = alloca %class.btVector3, align 4
  %q129 = alloca %class.btVector3, align 4
  %sqDist131 = alloca float, align 4
  %ref.tmp132 = alloca %class.btVector3, align 4
  %ref.tmp133 = alloca %class.btVector3, align 4
  %q181 = alloca %class.btVector3, align 4
  %sqDist183 = alloca float, align 4
  %ref.tmp184 = alloca %class.btVector3, align 4
  %ref.tmp185 = alloca %class.btVector3, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4
  store %class.btVector3* %a, %class.btVector3** %a.addr, align 4
  store %class.btVector3* %b, %class.btVector3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  store %class.btVector3* %d, %class.btVector3** %d.addr, align 4
  store %struct.btSubSimplexClosestResult* %finalResult, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %call = call %struct.btSubSimplexClosestResult* @_ZN25btSubSimplexClosestResultC2Ev(%struct.btSubSimplexClosestResult* %tempResult)
  %0 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %1 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_closestPointOnSimplex = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %1, i32 0, i32 0
  %2 = bitcast %class.btVector3* %m_closestPointOnSimplex to i8*
  %3 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %4 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %4, i32 0, i32 1
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %m_usedVertices)
  %5 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices2 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %5, i32 0, i32 1
  %6 = bitcast %struct.btUsageBitfield* %m_usedVertices2 to i8*
  %bf.load = load i8, i8* %6, align 4
  %bf.clear = and i8 %bf.load, -2
  %bf.set = or i8 %bf.clear, 1
  store i8 %bf.set, i8* %6, align 4
  %7 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices3 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %7, i32 0, i32 1
  %8 = bitcast %struct.btUsageBitfield* %m_usedVertices3 to i8*
  %bf.load4 = load i8, i8* %8, align 4
  %bf.clear5 = and i8 %bf.load4, -3
  %bf.set6 = or i8 %bf.clear5, 2
  store i8 %bf.set6, i8* %8, align 4
  %9 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices7 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %9, i32 0, i32 1
  %10 = bitcast %struct.btUsageBitfield* %m_usedVertices7 to i8*
  %bf.load8 = load i8, i8* %10, align 4
  %bf.clear9 = and i8 %bf.load8, -5
  %bf.set10 = or i8 %bf.clear9, 4
  store i8 %bf.set10, i8* %10, align 4
  %11 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices11 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %11, i32 0, i32 1
  %12 = bitcast %struct.btUsageBitfield* %m_usedVertices11 to i8*
  %bf.load12 = load i8, i8* %12, align 4
  %bf.clear13 = and i8 %bf.load12, -9
  %bf.set14 = or i8 %bf.clear13, 8
  store i8 %bf.set14, i8* %12, align 4
  %13 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %14 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %15 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %16 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %17 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  %call15 = call i32 @_ZN22btVoronoiSimplexSolver19pointOutsideOfPlaneERK9btVector3S2_S2_S2_S2_(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %14, %class.btVector3* nonnull align 4 dereferenceable(16) %15, %class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btVector3* nonnull align 4 dereferenceable(16) %17)
  store i32 %call15, i32* %pointOutsideABC, align 4
  %18 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %19 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %20 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %21 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  %22 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %call16 = call i32 @_ZN22btVoronoiSimplexSolver19pointOutsideOfPlaneERK9btVector3S2_S2_S2_S2_(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %18, %class.btVector3* nonnull align 4 dereferenceable(16) %19, %class.btVector3* nonnull align 4 dereferenceable(16) %20, %class.btVector3* nonnull align 4 dereferenceable(16) %21, %class.btVector3* nonnull align 4 dereferenceable(16) %22)
  store i32 %call16, i32* %pointOutsideACD, align 4
  %23 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %24 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %25 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  %26 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %27 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %call17 = call i32 @_ZN22btVoronoiSimplexSolver19pointOutsideOfPlaneERK9btVector3S2_S2_S2_S2_(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %23, %class.btVector3* nonnull align 4 dereferenceable(16) %24, %class.btVector3* nonnull align 4 dereferenceable(16) %25, %class.btVector3* nonnull align 4 dereferenceable(16) %26, %class.btVector3* nonnull align 4 dereferenceable(16) %27)
  store i32 %call17, i32* %pointOutsideADB, align 4
  %28 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %29 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %30 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  %31 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %32 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %call18 = call i32 @_ZN22btVoronoiSimplexSolver19pointOutsideOfPlaneERK9btVector3S2_S2_S2_S2_(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %28, %class.btVector3* nonnull align 4 dereferenceable(16) %29, %class.btVector3* nonnull align 4 dereferenceable(16) %30, %class.btVector3* nonnull align 4 dereferenceable(16) %31, %class.btVector3* nonnull align 4 dereferenceable(16) %32)
  store i32 %call18, i32* %pointOutsideBDC, align 4
  %33 = load i32, i32* %pointOutsideABC, align 4
  %cmp = icmp slt i32 %33, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %34 = load i32, i32* %pointOutsideACD, align 4
  %cmp19 = icmp slt i32 %34, 0
  br i1 %cmp19, label %if.then, label %lor.lhs.false20

lor.lhs.false20:                                  ; preds = %lor.lhs.false
  %35 = load i32, i32* %pointOutsideADB, align 4
  %cmp21 = icmp slt i32 %35, 0
  br i1 %cmp21, label %if.then, label %lor.lhs.false22

lor.lhs.false22:                                  ; preds = %lor.lhs.false20
  %36 = load i32, i32* %pointOutsideBDC, align 4
  %cmp23 = icmp slt i32 %36, 0
  br i1 %cmp23, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false22, %lor.lhs.false20, %lor.lhs.false, %entry
  %37 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_degenerate = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %37, i32 0, i32 4
  store i8 1, i8* %m_degenerate, align 4
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false22
  %38 = load i32, i32* %pointOutsideABC, align 4
  %tobool = icmp ne i32 %38, 0
  br i1 %tobool, label %if.end30, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.end
  %39 = load i32, i32* %pointOutsideACD, align 4
  %tobool24 = icmp ne i32 %39, 0
  br i1 %tobool24, label %if.end30, label %land.lhs.true25

land.lhs.true25:                                  ; preds = %land.lhs.true
  %40 = load i32, i32* %pointOutsideADB, align 4
  %tobool26 = icmp ne i32 %40, 0
  br i1 %tobool26, label %if.end30, label %land.lhs.true27

land.lhs.true27:                                  ; preds = %land.lhs.true25
  %41 = load i32, i32* %pointOutsideBDC, align 4
  %tobool28 = icmp ne i32 %41, 0
  br i1 %tobool28, label %if.end30, label %if.then29

if.then29:                                        ; preds = %land.lhs.true27
  store i1 false, i1* %retval, align 1
  br label %return

if.end30:                                         ; preds = %land.lhs.true27, %land.lhs.true25, %land.lhs.true, %if.end
  store float 0x47EFFFFFE0000000, float* %bestSqDist, align 4
  %42 = load i32, i32* %pointOutsideABC, align 4
  %tobool31 = icmp ne i32 %42, 0
  br i1 %tobool31, label %if.then32, label %if.end73

if.then32:                                        ; preds = %if.end30
  %43 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %44 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %45 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %46 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %call33 = call zeroext i1 @_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %43, %class.btVector3* nonnull align 4 dereferenceable(16) %44, %class.btVector3* nonnull align 4 dereferenceable(16) %45, %class.btVector3* nonnull align 4 dereferenceable(16) %46, %struct.btSubSimplexClosestResult* nonnull align 4 dereferenceable(37) %tempResult)
  %m_closestPointOnSimplex34 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 0
  %47 = bitcast %class.btVector3* %q to i8*
  %48 = bitcast %class.btVector3* %m_closestPointOnSimplex34 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %47, i8* align 4 %48, i32 16, i1 false)
  %49 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %49)
  %50 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp35, %class.btVector3* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %50)
  %call36 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp35)
  store float %call36, float* %sqDist, align 4
  %51 = load float, float* %sqDist, align 4
  %52 = load float, float* %bestSqDist, align 4
  %cmp37 = fcmp olt float %51, %52
  br i1 %cmp37, label %if.then38, label %if.end72

if.then38:                                        ; preds = %if.then32
  %53 = load float, float* %sqDist, align 4
  store float %53, float* %bestSqDist, align 4
  %54 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_closestPointOnSimplex39 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %54, i32 0, i32 0
  %55 = bitcast %class.btVector3* %m_closestPointOnSimplex39 to i8*
  %56 = bitcast %class.btVector3* %q to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %55, i8* align 4 %56, i32 16, i1 false)
  %57 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices40 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %57, i32 0, i32 1
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %m_usedVertices40)
  %m_usedVertices41 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %58 = bitcast %struct.btUsageBitfield* %m_usedVertices41 to i8*
  %bf.load42 = load i8, i8* %58, align 4
  %bf.clear43 = and i8 %bf.load42, 1
  %bf.cast = zext i8 %bf.clear43 to i16
  %59 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices44 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %59, i32 0, i32 1
  %60 = bitcast %struct.btUsageBitfield* %m_usedVertices44 to i8*
  %61 = trunc i16 %bf.cast to i8
  %bf.load45 = load i8, i8* %60, align 4
  %bf.value = and i8 %61, 1
  %bf.clear46 = and i8 %bf.load45, -2
  %bf.set47 = or i8 %bf.clear46, %bf.value
  store i8 %bf.set47, i8* %60, align 4
  %m_usedVertices48 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %62 = bitcast %struct.btUsageBitfield* %m_usedVertices48 to i8*
  %bf.load49 = load i8, i8* %62, align 4
  %bf.lshr = lshr i8 %bf.load49, 1
  %bf.clear50 = and i8 %bf.lshr, 1
  %bf.cast51 = zext i8 %bf.clear50 to i16
  %63 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices52 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %63, i32 0, i32 1
  %64 = bitcast %struct.btUsageBitfield* %m_usedVertices52 to i8*
  %65 = trunc i16 %bf.cast51 to i8
  %bf.load53 = load i8, i8* %64, align 4
  %bf.value54 = and i8 %65, 1
  %bf.shl = shl i8 %bf.value54, 1
  %bf.clear55 = and i8 %bf.load53, -3
  %bf.set56 = or i8 %bf.clear55, %bf.shl
  store i8 %bf.set56, i8* %64, align 4
  %m_usedVertices57 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %66 = bitcast %struct.btUsageBitfield* %m_usedVertices57 to i8*
  %bf.load58 = load i8, i8* %66, align 4
  %bf.lshr59 = lshr i8 %bf.load58, 2
  %bf.clear60 = and i8 %bf.lshr59, 1
  %bf.cast61 = zext i8 %bf.clear60 to i16
  %67 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices62 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %67, i32 0, i32 1
  %68 = bitcast %struct.btUsageBitfield* %m_usedVertices62 to i8*
  %69 = trunc i16 %bf.cast61 to i8
  %bf.load63 = load i8, i8* %68, align 4
  %bf.value64 = and i8 %69, 1
  %bf.shl65 = shl i8 %bf.value64, 2
  %bf.clear66 = and i8 %bf.load63, -5
  %bf.set67 = or i8 %bf.clear66, %bf.shl65
  store i8 %bf.set67, i8* %68, align 4
  %70 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_barycentricCoords = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords, i32 0, i32 0
  %71 = load float, float* %arrayidx, align 4
  %m_barycentricCoords68 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx69 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords68, i32 0, i32 1
  %72 = load float, float* %arrayidx69, align 4
  %m_barycentricCoords70 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx71 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords70, i32 0, i32 2
  %73 = load float, float* %arrayidx71, align 4
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %70, float %71, float %72, float %73, float 0.000000e+00)
  br label %if.end72

if.end72:                                         ; preds = %if.then38, %if.then32
  br label %if.end73

if.end73:                                         ; preds = %if.end72, %if.end30
  %74 = load i32, i32* %pointOutsideACD, align 4
  %tobool74 = icmp ne i32 %74, 0
  br i1 %tobool74, label %if.then75, label %if.end125

if.then75:                                        ; preds = %if.end73
  %75 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %76 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %77 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %78 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  %call76 = call zeroext i1 @_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %75, %class.btVector3* nonnull align 4 dereferenceable(16) %76, %class.btVector3* nonnull align 4 dereferenceable(16) %77, %class.btVector3* nonnull align 4 dereferenceable(16) %78, %struct.btSubSimplexClosestResult* nonnull align 4 dereferenceable(37) %tempResult)
  %m_closestPointOnSimplex78 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 0
  %79 = bitcast %class.btVector3* %q77 to i8*
  %80 = bitcast %class.btVector3* %m_closestPointOnSimplex78 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %79, i8* align 4 %80, i32 16, i1 false)
  %81 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp80, %class.btVector3* nonnull align 4 dereferenceable(16) %q77, %class.btVector3* nonnull align 4 dereferenceable(16) %81)
  %82 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp81, %class.btVector3* nonnull align 4 dereferenceable(16) %q77, %class.btVector3* nonnull align 4 dereferenceable(16) %82)
  %call82 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp80, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp81)
  store float %call82, float* %sqDist79, align 4
  %83 = load float, float* %sqDist79, align 4
  %84 = load float, float* %bestSqDist, align 4
  %cmp83 = fcmp olt float %83, %84
  br i1 %cmp83, label %if.then84, label %if.end124

if.then84:                                        ; preds = %if.then75
  %85 = load float, float* %sqDist79, align 4
  store float %85, float* %bestSqDist, align 4
  %86 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_closestPointOnSimplex85 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %86, i32 0, i32 0
  %87 = bitcast %class.btVector3* %m_closestPointOnSimplex85 to i8*
  %88 = bitcast %class.btVector3* %q77 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %87, i8* align 4 %88, i32 16, i1 false)
  %89 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices86 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %89, i32 0, i32 1
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %m_usedVertices86)
  %m_usedVertices87 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %90 = bitcast %struct.btUsageBitfield* %m_usedVertices87 to i8*
  %bf.load88 = load i8, i8* %90, align 4
  %bf.clear89 = and i8 %bf.load88, 1
  %bf.cast90 = zext i8 %bf.clear89 to i16
  %91 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices91 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %91, i32 0, i32 1
  %92 = bitcast %struct.btUsageBitfield* %m_usedVertices91 to i8*
  %93 = trunc i16 %bf.cast90 to i8
  %bf.load92 = load i8, i8* %92, align 4
  %bf.value93 = and i8 %93, 1
  %bf.clear94 = and i8 %bf.load92, -2
  %bf.set95 = or i8 %bf.clear94, %bf.value93
  store i8 %bf.set95, i8* %92, align 4
  %m_usedVertices96 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %94 = bitcast %struct.btUsageBitfield* %m_usedVertices96 to i8*
  %bf.load97 = load i8, i8* %94, align 4
  %bf.lshr98 = lshr i8 %bf.load97, 1
  %bf.clear99 = and i8 %bf.lshr98, 1
  %bf.cast100 = zext i8 %bf.clear99 to i16
  %95 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices101 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %95, i32 0, i32 1
  %96 = bitcast %struct.btUsageBitfield* %m_usedVertices101 to i8*
  %97 = trunc i16 %bf.cast100 to i8
  %bf.load102 = load i8, i8* %96, align 4
  %bf.value103 = and i8 %97, 1
  %bf.shl104 = shl i8 %bf.value103, 2
  %bf.clear105 = and i8 %bf.load102, -5
  %bf.set106 = or i8 %bf.clear105, %bf.shl104
  store i8 %bf.set106, i8* %96, align 4
  %m_usedVertices107 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %98 = bitcast %struct.btUsageBitfield* %m_usedVertices107 to i8*
  %bf.load108 = load i8, i8* %98, align 4
  %bf.lshr109 = lshr i8 %bf.load108, 2
  %bf.clear110 = and i8 %bf.lshr109, 1
  %bf.cast111 = zext i8 %bf.clear110 to i16
  %99 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices112 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %99, i32 0, i32 1
  %100 = bitcast %struct.btUsageBitfield* %m_usedVertices112 to i8*
  %101 = trunc i16 %bf.cast111 to i8
  %bf.load113 = load i8, i8* %100, align 4
  %bf.value114 = and i8 %101, 1
  %bf.shl115 = shl i8 %bf.value114, 3
  %bf.clear116 = and i8 %bf.load113, -9
  %bf.set117 = or i8 %bf.clear116, %bf.shl115
  store i8 %bf.set117, i8* %100, align 4
  %102 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_barycentricCoords118 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx119 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords118, i32 0, i32 0
  %103 = load float, float* %arrayidx119, align 4
  %m_barycentricCoords120 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx121 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords120, i32 0, i32 1
  %104 = load float, float* %arrayidx121, align 4
  %m_barycentricCoords122 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx123 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords122, i32 0, i32 2
  %105 = load float, float* %arrayidx123, align 4
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %102, float %103, float 0.000000e+00, float %104, float %105)
  br label %if.end124

if.end124:                                        ; preds = %if.then84, %if.then75
  br label %if.end125

if.end125:                                        ; preds = %if.end124, %if.end73
  %106 = load i32, i32* %pointOutsideADB, align 4
  %tobool126 = icmp ne i32 %106, 0
  br i1 %tobool126, label %if.then127, label %if.end177

if.then127:                                       ; preds = %if.end125
  %107 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %108 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %109 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  %110 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %call128 = call zeroext i1 @_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %107, %class.btVector3* nonnull align 4 dereferenceable(16) %108, %class.btVector3* nonnull align 4 dereferenceable(16) %109, %class.btVector3* nonnull align 4 dereferenceable(16) %110, %struct.btSubSimplexClosestResult* nonnull align 4 dereferenceable(37) %tempResult)
  %m_closestPointOnSimplex130 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 0
  %111 = bitcast %class.btVector3* %q129 to i8*
  %112 = bitcast %class.btVector3* %m_closestPointOnSimplex130 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %111, i8* align 4 %112, i32 16, i1 false)
  %113 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp132, %class.btVector3* nonnull align 4 dereferenceable(16) %q129, %class.btVector3* nonnull align 4 dereferenceable(16) %113)
  %114 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp133, %class.btVector3* nonnull align 4 dereferenceable(16) %q129, %class.btVector3* nonnull align 4 dereferenceable(16) %114)
  %call134 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp132, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp133)
  store float %call134, float* %sqDist131, align 4
  %115 = load float, float* %sqDist131, align 4
  %116 = load float, float* %bestSqDist, align 4
  %cmp135 = fcmp olt float %115, %116
  br i1 %cmp135, label %if.then136, label %if.end176

if.then136:                                       ; preds = %if.then127
  %117 = load float, float* %sqDist131, align 4
  store float %117, float* %bestSqDist, align 4
  %118 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_closestPointOnSimplex137 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %118, i32 0, i32 0
  %119 = bitcast %class.btVector3* %m_closestPointOnSimplex137 to i8*
  %120 = bitcast %class.btVector3* %q129 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %119, i8* align 4 %120, i32 16, i1 false)
  %121 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices138 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %121, i32 0, i32 1
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %m_usedVertices138)
  %m_usedVertices139 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %122 = bitcast %struct.btUsageBitfield* %m_usedVertices139 to i8*
  %bf.load140 = load i8, i8* %122, align 4
  %bf.clear141 = and i8 %bf.load140, 1
  %bf.cast142 = zext i8 %bf.clear141 to i16
  %123 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices143 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %123, i32 0, i32 1
  %124 = bitcast %struct.btUsageBitfield* %m_usedVertices143 to i8*
  %125 = trunc i16 %bf.cast142 to i8
  %bf.load144 = load i8, i8* %124, align 4
  %bf.value145 = and i8 %125, 1
  %bf.clear146 = and i8 %bf.load144, -2
  %bf.set147 = or i8 %bf.clear146, %bf.value145
  store i8 %bf.set147, i8* %124, align 4
  %m_usedVertices148 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %126 = bitcast %struct.btUsageBitfield* %m_usedVertices148 to i8*
  %bf.load149 = load i8, i8* %126, align 4
  %bf.lshr150 = lshr i8 %bf.load149, 2
  %bf.clear151 = and i8 %bf.lshr150, 1
  %bf.cast152 = zext i8 %bf.clear151 to i16
  %127 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices153 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %127, i32 0, i32 1
  %128 = bitcast %struct.btUsageBitfield* %m_usedVertices153 to i8*
  %129 = trunc i16 %bf.cast152 to i8
  %bf.load154 = load i8, i8* %128, align 4
  %bf.value155 = and i8 %129, 1
  %bf.shl156 = shl i8 %bf.value155, 1
  %bf.clear157 = and i8 %bf.load154, -3
  %bf.set158 = or i8 %bf.clear157, %bf.shl156
  store i8 %bf.set158, i8* %128, align 4
  %m_usedVertices159 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %130 = bitcast %struct.btUsageBitfield* %m_usedVertices159 to i8*
  %bf.load160 = load i8, i8* %130, align 4
  %bf.lshr161 = lshr i8 %bf.load160, 1
  %bf.clear162 = and i8 %bf.lshr161, 1
  %bf.cast163 = zext i8 %bf.clear162 to i16
  %131 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices164 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %131, i32 0, i32 1
  %132 = bitcast %struct.btUsageBitfield* %m_usedVertices164 to i8*
  %133 = trunc i16 %bf.cast163 to i8
  %bf.load165 = load i8, i8* %132, align 4
  %bf.value166 = and i8 %133, 1
  %bf.shl167 = shl i8 %bf.value166, 3
  %bf.clear168 = and i8 %bf.load165, -9
  %bf.set169 = or i8 %bf.clear168, %bf.shl167
  store i8 %bf.set169, i8* %132, align 4
  %134 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_barycentricCoords170 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx171 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords170, i32 0, i32 0
  %135 = load float, float* %arrayidx171, align 4
  %m_barycentricCoords172 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx173 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords172, i32 0, i32 2
  %136 = load float, float* %arrayidx173, align 4
  %m_barycentricCoords174 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx175 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords174, i32 0, i32 1
  %137 = load float, float* %arrayidx175, align 4
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %134, float %135, float %136, float 0.000000e+00, float %137)
  br label %if.end176

if.end176:                                        ; preds = %if.then136, %if.then127
  br label %if.end177

if.end177:                                        ; preds = %if.end176, %if.end125
  %138 = load i32, i32* %pointOutsideBDC, align 4
  %tobool178 = icmp ne i32 %138, 0
  br i1 %tobool178, label %if.then179, label %if.end230

if.then179:                                       ; preds = %if.end177
  %139 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %140 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %141 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  %142 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %call180 = call zeroext i1 @_ZN22btVoronoiSimplexSolver22closestPtPointTriangleERK9btVector3S2_S2_S2_R25btSubSimplexClosestResult(%class.btVoronoiSimplexSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %139, %class.btVector3* nonnull align 4 dereferenceable(16) %140, %class.btVector3* nonnull align 4 dereferenceable(16) %141, %class.btVector3* nonnull align 4 dereferenceable(16) %142, %struct.btSubSimplexClosestResult* nonnull align 4 dereferenceable(37) %tempResult)
  %m_closestPointOnSimplex182 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 0
  %143 = bitcast %class.btVector3* %q181 to i8*
  %144 = bitcast %class.btVector3* %m_closestPointOnSimplex182 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %143, i8* align 4 %144, i32 16, i1 false)
  %145 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp184, %class.btVector3* nonnull align 4 dereferenceable(16) %q181, %class.btVector3* nonnull align 4 dereferenceable(16) %145)
  %146 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp185, %class.btVector3* nonnull align 4 dereferenceable(16) %q181, %class.btVector3* nonnull align 4 dereferenceable(16) %146)
  %call186 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp184, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp185)
  store float %call186, float* %sqDist183, align 4
  %147 = load float, float* %sqDist183, align 4
  %148 = load float, float* %bestSqDist, align 4
  %cmp187 = fcmp olt float %147, %148
  br i1 %cmp187, label %if.then188, label %if.end229

if.then188:                                       ; preds = %if.then179
  %149 = load float, float* %sqDist183, align 4
  store float %149, float* %bestSqDist, align 4
  %150 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_closestPointOnSimplex189 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %150, i32 0, i32 0
  %151 = bitcast %class.btVector3* %m_closestPointOnSimplex189 to i8*
  %152 = bitcast %class.btVector3* %q181 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %151, i8* align 4 %152, i32 16, i1 false)
  %153 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices190 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %153, i32 0, i32 1
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %m_usedVertices190)
  %m_usedVertices191 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %154 = bitcast %struct.btUsageBitfield* %m_usedVertices191 to i8*
  %bf.load192 = load i8, i8* %154, align 4
  %bf.clear193 = and i8 %bf.load192, 1
  %bf.cast194 = zext i8 %bf.clear193 to i16
  %155 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices195 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %155, i32 0, i32 1
  %156 = bitcast %struct.btUsageBitfield* %m_usedVertices195 to i8*
  %157 = trunc i16 %bf.cast194 to i8
  %bf.load196 = load i8, i8* %156, align 4
  %bf.value197 = and i8 %157, 1
  %bf.shl198 = shl i8 %bf.value197, 1
  %bf.clear199 = and i8 %bf.load196, -3
  %bf.set200 = or i8 %bf.clear199, %bf.shl198
  store i8 %bf.set200, i8* %156, align 4
  %m_usedVertices201 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %158 = bitcast %struct.btUsageBitfield* %m_usedVertices201 to i8*
  %bf.load202 = load i8, i8* %158, align 4
  %bf.lshr203 = lshr i8 %bf.load202, 2
  %bf.clear204 = and i8 %bf.lshr203, 1
  %bf.cast205 = zext i8 %bf.clear204 to i16
  %159 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices206 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %159, i32 0, i32 1
  %160 = bitcast %struct.btUsageBitfield* %m_usedVertices206 to i8*
  %161 = trunc i16 %bf.cast205 to i8
  %bf.load207 = load i8, i8* %160, align 4
  %bf.value208 = and i8 %161, 1
  %bf.shl209 = shl i8 %bf.value208, 2
  %bf.clear210 = and i8 %bf.load207, -5
  %bf.set211 = or i8 %bf.clear210, %bf.shl209
  store i8 %bf.set211, i8* %160, align 4
  %m_usedVertices212 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 1
  %162 = bitcast %struct.btUsageBitfield* %m_usedVertices212 to i8*
  %bf.load213 = load i8, i8* %162, align 4
  %bf.lshr214 = lshr i8 %bf.load213, 1
  %bf.clear215 = and i8 %bf.lshr214, 1
  %bf.cast216 = zext i8 %bf.clear215 to i16
  %163 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices217 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %163, i32 0, i32 1
  %164 = bitcast %struct.btUsageBitfield* %m_usedVertices217 to i8*
  %165 = trunc i16 %bf.cast216 to i8
  %bf.load218 = load i8, i8* %164, align 4
  %bf.value219 = and i8 %165, 1
  %bf.shl220 = shl i8 %bf.value219, 3
  %bf.clear221 = and i8 %bf.load218, -9
  %bf.set222 = or i8 %bf.clear221, %bf.shl220
  store i8 %bf.set222, i8* %164, align 4
  %166 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_barycentricCoords223 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx224 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords223, i32 0, i32 0
  %167 = load float, float* %arrayidx224, align 4
  %m_barycentricCoords225 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx226 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords225, i32 0, i32 2
  %168 = load float, float* %arrayidx226, align 4
  %m_barycentricCoords227 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %tempResult, i32 0, i32 3
  %arrayidx228 = getelementptr inbounds [4 x float], [4 x float]* %m_barycentricCoords227, i32 0, i32 1
  %169 = load float, float* %arrayidx228, align 4
  call void @_ZN25btSubSimplexClosestResult25setBarycentricCoordinatesEffff(%struct.btSubSimplexClosestResult* %166, float 0.000000e+00, float %167, float %168, float %169)
  br label %if.end229

if.end229:                                        ; preds = %if.then188, %if.then179
  br label %if.end230

if.end230:                                        ; preds = %if.end229, %if.end177
  %170 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices231 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %170, i32 0, i32 1
  %171 = bitcast %struct.btUsageBitfield* %m_usedVertices231 to i8*
  %bf.load232 = load i8, i8* %171, align 4
  %bf.clear233 = and i8 %bf.load232, 1
  %bf.cast234 = zext i8 %bf.clear233 to i16
  %tobool235 = icmp ne i16 %bf.cast234, 0
  br i1 %tobool235, label %land.lhs.true236, label %if.end258

land.lhs.true236:                                 ; preds = %if.end230
  %172 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices237 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %172, i32 0, i32 1
  %173 = bitcast %struct.btUsageBitfield* %m_usedVertices237 to i8*
  %bf.load238 = load i8, i8* %173, align 4
  %bf.lshr239 = lshr i8 %bf.load238, 1
  %bf.clear240 = and i8 %bf.lshr239, 1
  %bf.cast241 = zext i8 %bf.clear240 to i16
  %tobool242 = icmp ne i16 %bf.cast241, 0
  br i1 %tobool242, label %land.lhs.true243, label %if.end258

land.lhs.true243:                                 ; preds = %land.lhs.true236
  %174 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices244 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %174, i32 0, i32 1
  %175 = bitcast %struct.btUsageBitfield* %m_usedVertices244 to i8*
  %bf.load245 = load i8, i8* %175, align 4
  %bf.lshr246 = lshr i8 %bf.load245, 2
  %bf.clear247 = and i8 %bf.lshr246, 1
  %bf.cast248 = zext i8 %bf.clear247 to i16
  %tobool249 = icmp ne i16 %bf.cast248, 0
  br i1 %tobool249, label %land.lhs.true250, label %if.end258

land.lhs.true250:                                 ; preds = %land.lhs.true243
  %176 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %finalResult.addr, align 4
  %m_usedVertices251 = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %176, i32 0, i32 1
  %177 = bitcast %struct.btUsageBitfield* %m_usedVertices251 to i8*
  %bf.load252 = load i8, i8* %177, align 4
  %bf.lshr253 = lshr i8 %bf.load252, 3
  %bf.clear254 = and i8 %bf.lshr253, 1
  %bf.cast255 = zext i8 %bf.clear254 to i16
  %tobool256 = icmp ne i16 %bf.cast255, 0
  br i1 %tobool256, label %if.then257, label %if.end258

if.then257:                                       ; preds = %land.lhs.true250
  store i1 true, i1* %retval, align 1
  br label %return

if.end258:                                        ; preds = %land.lhs.true250, %land.lhs.true243, %land.lhs.true236, %if.end230
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end258, %if.then257, %if.then29, %if.then
  %178 = load i1, i1* %retval, align 1
  ret i1 %178
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN22btVoronoiSimplexSolver7closestER9btVector3(%class.btVoronoiSimplexSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %succes = alloca i8, align 1
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %call = call zeroext i1 @_ZN22btVoronoiSimplexSolver28updateClosestVectorAndPointsEv(%class.btVoronoiSimplexSolver* %this1)
  %frombool = zext i1 %call to i8
  store i8 %frombool, i8* %succes, align 1
  %m_cachedV = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 6
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  %2 = bitcast %class.btVector3* %m_cachedV to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load i8, i8* %succes, align 1
  %tobool = trunc i8 %3 to i1
  ret i1 %tobool
}

; Function Attrs: noinline optnone
define hidden float @_ZN22btVoronoiSimplexSolver9maxVertexEv(%class.btVoronoiSimplexSolver* %this) #3 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %i = alloca i32, align 4
  %numverts = alloca i32, align 4
  %maxV = alloca float, align 4
  %curLen2 = alloca float, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %call = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  store i32 %call, i32* %numverts, align 4
  store float 0.000000e+00, float* %maxV, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %numverts, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_simplexVectorW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW, i32 0, i32 %2
  %call2 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %arrayidx)
  store float %call2, float* %curLen2, align 4
  %3 = load float, float* %maxV, align 4
  %4 = load float, float* %curLen2, align 4
  %cmp3 = fcmp olt float %3, %4
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %5 = load float, float* %curLen2, align 4
  store float %5, float* %maxV, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = load float, float* %maxV, align 4
  ret float %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline optnone
define hidden i32 @_ZNK22btVoronoiSimplexSolver10getSimplexEP9btVector3S1_S1_(%class.btVoronoiSimplexSolver* %this, %class.btVector3* %pBuf, %class.btVector3* %qBuf, %class.btVector3* %yBuf) #3 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %pBuf.addr = alloca %class.btVector3*, align 4
  %qBuf.addr = alloca %class.btVector3*, align 4
  %yBuf.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4
  store %class.btVector3* %pBuf, %class.btVector3** %pBuf.addr, align 4
  store %class.btVector3* %qBuf, %class.btVector3** %qBuf.addr, align 4
  store %class.btVector3* %yBuf, %class.btVector3** %yBuf.addr, align 4
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %call = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_simplexVectorW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW, i32 0, i32 %1
  %2 = load %class.btVector3*, %class.btVector3** %yBuf.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 %3
  %4 = bitcast %class.btVector3* %arrayidx2 to i8*
  %5 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %m_simplexPointsP = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %6 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP, i32 0, i32 %6
  %7 = load %class.btVector3*, %class.btVector3** %pBuf.addr, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  %9 = bitcast %class.btVector3* %arrayidx4 to i8*
  %10 = bitcast %class.btVector3* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  %m_simplexPointsQ = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %11 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ, i32 0, i32 %11
  %12 = load %class.btVector3*, %class.btVector3** %qBuf.addr, align 4
  %13 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 %13
  %14 = bitcast %class.btVector3* %arrayidx6 to i8*
  %15 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %16 = load i32, i32* %i, align 4
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  ret i32 %call7
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN22btVoronoiSimplexSolver9inSimplexERK9btVector3(%class.btVoronoiSimplexSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %w) #3 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %w.addr = alloca %class.btVector3*, align 4
  %found = alloca i8, align 1
  %i = alloca i32, align 4
  %numverts = alloca i32, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4
  store %class.btVector3* %w, %class.btVector3** %w.addr, align 4
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  store i8 0, i8* %found, align 1
  %call = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  store i32 %call, i32* %numverts, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %numverts, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_simplexVectorW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW, i32 0, i32 %2
  %3 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call2 = call float @_ZNK9btVector39distance2ERKS_(%class.btVector3* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %m_equalVertexThreshold = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 8
  %4 = load float, float* %m_equalVertexThreshold, align 4
  %cmp3 = fcmp ole float %call2, %4
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i8 1, i8* %found, align 1
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %6 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %m_lastW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 7
  %call4 = call zeroext i1 @_ZNK9btVector3eqERKS_(%class.btVector3* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lastW)
  br i1 %call4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %for.end
  store i1 true, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %for.end
  %7 = load i8, i8* %found, align 1
  %tobool = trunc i8 %7 to i1
  store i1 %tobool, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end6, %if.then5
  %8 = load i1, i1* %retval, align 1
  ret i1 %8
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector39distance2ERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK9btVector3eqERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 3
  %2 = load float, float* %arrayidx3, align 4
  %cmp = fcmp oeq float %0, %2
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %cmp8 = fcmp oeq float %3, %5
  br i1 %cmp8, label %land.lhs.true9, label %land.end

land.lhs.true9:                                   ; preds = %land.lhs.true
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 1
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 1
  %8 = load float, float* %arrayidx13, align 4
  %cmp14 = fcmp oeq float %6, %8
  br i1 %cmp14, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true9
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 0
  %11 = load float, float* %arrayidx18, align 4
  %cmp19 = fcmp oeq float %9, %11
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true9, %land.lhs.true, %entry
  %12 = phi i1 [ false, %land.lhs.true9 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp19, %land.rhs ]
  ret i1 %12
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN22btVoronoiSimplexSolver14backup_closestER9btVector3(%class.btVoronoiSimplexSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %m_cachedV = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 6
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  %2 = bitcast %class.btVector3* %m_cachedV to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZNK22btVoronoiSimplexSolver12emptySimplexEv(%class.btVoronoiSimplexSolver* %this) #3 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %call = call i32 @_ZNK22btVoronoiSimplexSolver11numVerticesEv(%class.btVoronoiSimplexSolver* %this1)
  %cmp = icmp eq i32 %call, 0
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define hidden void @_ZN22btVoronoiSimplexSolver14compute_pointsER9btVector3S1_(%class.btVoronoiSimplexSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %p1, %class.btVector3* nonnull align 4 dereferenceable(16) %p2) #3 {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %p1.addr = alloca %class.btVector3*, align 4
  %p2.addr = alloca %class.btVector3*, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4
  store %class.btVector3* %p1, %class.btVector3** %p1.addr, align 4
  store %class.btVector3* %p2, %class.btVector3** %p2.addr, align 4
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %call = call zeroext i1 @_ZN22btVoronoiSimplexSolver28updateClosestVectorAndPointsEv(%class.btVoronoiSimplexSolver* %this1)
  %m_cachedP1 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  %2 = bitcast %class.btVector3* %m_cachedP1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %m_cachedP2 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  %3 = load %class.btVector3*, %class.btVector3** %p2.addr, align 4
  %4 = bitcast %class.btVector3* %3 to i8*
  %5 = bitcast %class.btVector3* %m_cachedP2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btUsageBitfield*, align 4
  store %struct.btUsageBitfield* %this, %struct.btUsageBitfield** %this.addr, align 4
  %this1 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %this.addr, align 4
  %0 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load = load i8, i8* %0, align 2
  %bf.clear = and i8 %bf.load, -2
  store i8 %bf.clear, i8* %0, align 2
  %1 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load2 = load i8, i8* %1, align 2
  %bf.clear3 = and i8 %bf.load2, -3
  store i8 %bf.clear3, i8* %1, align 2
  %2 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load4 = load i8, i8* %2, align 2
  %bf.clear5 = and i8 %bf.load4, -5
  store i8 %bf.clear5, i8* %2, align 2
  %3 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load6 = load i8, i8* %3, align 2
  %bf.clear7 = and i8 %bf.load6, -9
  store i8 %bf.clear7, i8* %3, align 2
  ret void
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN22btVoronoiSimplexSolver19pointOutsideOfPlaneERK9btVector3S2_S2_S2_S2_(%class.btVoronoiSimplexSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %a, %class.btVector3* nonnull align 4 dereferenceable(16) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c, %class.btVector3* nonnull align 4 dereferenceable(16) %d) #3 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %a.addr = alloca %class.btVector3*, align 4
  %b.addr = alloca %class.btVector3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  %d.addr = alloca %class.btVector3*, align 4
  %normal = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %signp = alloca float, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %signd = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4
  store %class.btVector3* %a, %class.btVector3** %a.addr, align 4
  store %class.btVector3* %b, %class.btVector3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  store %class.btVector3* %d, %class.btVector3** %d.addr, align 4
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %normal, %class.btVector3* %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %4 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  store float %call, float* %signp, align 4
  %6 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  %7 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7)
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  store float %call5, float* %signd, align 4
  %8 = load float, float* %signd, align 4
  %9 = load float, float* %signd, align 4
  %mul = fmul float %8, %9
  %cmp = fcmp olt float %mul, 0x3E45798EC0000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %10 = load float, float* %signp, align 4
  %11 = load float, float* %signd, align 4
  %mul6 = fmul float %10, %11
  %cmp7 = fcmp olt float %mul6, 0.000000e+00
  %conv = zext i1 %cmp7 to i32
  store i32 %conv, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btSubSimplexClosestResult* @_ZN25btSubSimplexClosestResultC2Ev(%struct.btSubSimplexClosestResult* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btSubSimplexClosestResult*, align 4
  store %struct.btSubSimplexClosestResult* %this, %struct.btSubSimplexClosestResult** %this.addr, align 4
  %this1 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %this.addr, align 4
  %m_closestPointOnSimplex = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_closestPointOnSimplex)
  %m_usedVertices = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 1
  %call2 = call %struct.btUsageBitfield* @_ZN15btUsageBitfieldC2Ev(%struct.btUsageBitfield* %m_usedVertices)
  ret %struct.btSubSimplexClosestResult* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btUsageBitfield* @_ZN15btUsageBitfieldC2Ev(%struct.btUsageBitfield* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btUsageBitfield*, align 4
  store %struct.btUsageBitfield* %this, %struct.btUsageBitfield** %this.addr, align 4
  %this1 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %this.addr, align 4
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %this1)
  ret %struct.btUsageBitfield* %this1
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btVoronoiSimplexSolver.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
