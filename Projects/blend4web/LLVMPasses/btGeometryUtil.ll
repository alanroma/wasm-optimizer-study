; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/LinearMath/btGeometryUtil.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/LinearMath/btGeometryUtil.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btVector3 = type { [4 x float] }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_ = comdat any

$_Z6btFabsf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btSqrtf = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btGeometryUtil.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @btBulletMathProbe() #1 {
entry:
  ret void
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN14btGeometryUtil19isPointInsidePlanesERK20btAlignedObjectArrayI9btVector3ERKS1_f(%class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %planeEquations, %class.btVector3* nonnull align 4 dereferenceable(16) %point, float %margin) #2 {
entry:
  %retval = alloca i1, align 1
  %planeEquations.addr = alloca %class.btAlignedObjectArray*, align 4
  %point.addr = alloca %class.btVector3*, align 4
  %margin.addr = alloca float, align 4
  %numbrushes = alloca i32, align 4
  %i = alloca i32, align 4
  %N1 = alloca %class.btVector3*, align 4
  %dist = alloca float, align 4
  store %class.btAlignedObjectArray* %planeEquations, %class.btAlignedObjectArray** %planeEquations.addr, align 4
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4
  store float %margin, float* %margin.addr, align 4
  %0 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %planeEquations.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %0)
  store i32 %call, i32* %numbrushes, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %numbrushes, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %planeEquations.addr, align 4
  %4 = load i32, i32* %i, align 4
  %call1 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %3, i32 %4)
  store %class.btVector3* %call1, %class.btVector3** %N1, align 4
  %5 = load %class.btVector3*, %class.btVector3** %N1, align 4
  %6 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %call2 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  %7 = load %class.btVector3*, %class.btVector3** %N1, align 4
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %7)
  %arrayidx = getelementptr inbounds float, float* %call3, i32 3
  %8 = load float, float* %arrayidx, align 4
  %add = fadd float %call2, %8
  %9 = load float, float* %margin.addr, align 4
  %sub = fsub float %add, %9
  store float %sub, float* %dist, align 4
  %10 = load float, float* %dist, align 4
  %cmp4 = fcmp ogt float %10, 0.000000e+00
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %if.then
  %12 = load i1, i1* %retval, align 1
  ret i1 %12
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN14btGeometryUtil22areVerticesBehindPlaneERK9btVector3RK20btAlignedObjectArrayIS0_Ef(%class.btVector3* nonnull align 4 dereferenceable(16) %planeNormal, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %vertices, float %margin) #2 {
entry:
  %retval = alloca i1, align 1
  %planeNormal.addr = alloca %class.btVector3*, align 4
  %vertices.addr = alloca %class.btAlignedObjectArray*, align 4
  %margin.addr = alloca float, align 4
  %numvertices = alloca i32, align 4
  %i = alloca i32, align 4
  %N1 = alloca %class.btVector3*, align 4
  %dist = alloca float, align 4
  store %class.btVector3* %planeNormal, %class.btVector3** %planeNormal.addr, align 4
  store %class.btAlignedObjectArray* %vertices, %class.btAlignedObjectArray** %vertices.addr, align 4
  store float %margin, float* %margin.addr, align 4
  %0 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %vertices.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %0)
  store i32 %call, i32* %numvertices, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %numvertices, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %vertices.addr, align 4
  %4 = load i32, i32* %i, align 4
  %call1 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %3, i32 %4)
  store %class.btVector3* %call1, %class.btVector3** %N1, align 4
  %5 = load %class.btVector3*, %class.btVector3** %planeNormal.addr, align 4
  %6 = load %class.btVector3*, %class.btVector3** %N1, align 4
  %call2 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  %7 = load %class.btVector3*, %class.btVector3** %planeNormal.addr, align 4
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %7)
  %arrayidx = getelementptr inbounds float, float* %call3, i32 3
  %8 = load float, float* %arrayidx, align 4
  %add = fadd float %call2, %8
  %9 = load float, float* %margin.addr, align 4
  %sub = fsub float %add, %9
  store float %sub, float* %dist, align 4
  %10 = load float, float* %dist, align 4
  %cmp4 = fcmp ogt float %10, 0.000000e+00
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %if.then
  %12 = load i1, i1* %retval, align 1
  ret i1 %12
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_Z8notExistRK9btVector3RK20btAlignedObjectArrayIS_E(%class.btVector3* nonnull align 4 dereferenceable(16) %planeEquation, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %planeEquations) #2 {
entry:
  %retval = alloca i1, align 1
  %planeEquation.addr = alloca %class.btVector3*, align 4
  %planeEquations.addr = alloca %class.btAlignedObjectArray*, align 4
  %numbrushes = alloca i32, align 4
  %i = alloca i32, align 4
  %N1 = alloca %class.btVector3*, align 4
  store %class.btVector3* %planeEquation, %class.btVector3** %planeEquation.addr, align 4
  store %class.btAlignedObjectArray* %planeEquations, %class.btAlignedObjectArray** %planeEquations.addr, align 4
  %0 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %planeEquations.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %0)
  store i32 %call, i32* %numbrushes, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %numbrushes, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %planeEquations.addr, align 4
  %4 = load i32, i32* %i, align 4
  %call1 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %3, i32 %4)
  store %class.btVector3* %call1, %class.btVector3** %N1, align 4
  %5 = load %class.btVector3*, %class.btVector3** %planeEquation.addr, align 4
  %6 = load %class.btVector3*, %class.btVector3** %N1, align 4
  %call2 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  %cmp3 = fcmp ogt float %call2, 0x3FEFF7CEE0000000
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %if.then
  %8 = load i1, i1* %retval, align 1
  ret i1 %8
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btGeometryUtil29getPlaneEquationsFromVerticesER20btAlignedObjectArrayI9btVector3ES3_(%class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %vertices, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %planeEquationsOut) #2 {
entry:
  %vertices.addr = alloca %class.btAlignedObjectArray*, align 4
  %planeEquationsOut.addr = alloca %class.btAlignedObjectArray*, align 4
  %numvertices = alloca i32, align 4
  %i = alloca i32, align 4
  %N1 = alloca %class.btVector3*, align 4
  %j = alloca i32, align 4
  %N2 = alloca %class.btVector3*, align 4
  %k = alloca i32, align 4
  %N3 = alloca %class.btVector3*, align 4
  %planeEquation = alloca %class.btVector3, align 4
  %edge0 = alloca %class.btVector3, align 4
  %edge1 = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %normalSign = alloca float, align 4
  %ww = alloca i32, align 4
  %ref.tmp18 = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  store %class.btAlignedObjectArray* %vertices, %class.btAlignedObjectArray** %vertices.addr, align 4
  store %class.btAlignedObjectArray* %planeEquationsOut, %class.btAlignedObjectArray** %planeEquationsOut.addr, align 4
  %0 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %vertices.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %0)
  store i32 %call, i32* %numvertices, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc37, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %numvertices, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end39

for.body:                                         ; preds = %for.cond
  %3 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %vertices.addr, align 4
  %4 = load i32, i32* %i, align 4
  %call1 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %3, i32 %4)
  store %class.btVector3* %call1, %class.btVector3** %N1, align 4
  %5 = load i32, i32* %i, align 4
  %add = add nsw i32 %5, 1
  store i32 %add, i32* %j, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc34, %for.body
  %6 = load i32, i32* %j, align 4
  %7 = load i32, i32* %numvertices, align 4
  %cmp3 = icmp slt i32 %6, %7
  br i1 %cmp3, label %for.body4, label %for.end36

for.body4:                                        ; preds = %for.cond2
  %8 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %vertices.addr, align 4
  %9 = load i32, i32* %j, align 4
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %8, i32 %9)
  store %class.btVector3* %call5, %class.btVector3** %N2, align 4
  %10 = load i32, i32* %j, align 4
  %add6 = add nsw i32 %10, 1
  store i32 %add6, i32* %k, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc31, %for.body4
  %11 = load i32, i32* %k, align 4
  %12 = load i32, i32* %numvertices, align 4
  %cmp8 = icmp slt i32 %11, %12
  br i1 %cmp8, label %for.body9, label %for.end33

for.body9:                                        ; preds = %for.cond7
  %13 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %vertices.addr, align 4
  %14 = load i32, i32* %k, align 4
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %13, i32 %14)
  store %class.btVector3* %call10, %class.btVector3** %N3, align 4
  %call11 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %planeEquation)
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %edge0)
  %call13 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %edge1)
  %15 = load %class.btVector3*, %class.btVector3** %N2, align 4
  %16 = load %class.btVector3*, %class.btVector3** %N1, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %15, %class.btVector3* nonnull align 4 dereferenceable(16) %16)
  %17 = bitcast %class.btVector3* %edge0 to i8*
  %18 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 16, i1 false)
  %19 = load %class.btVector3*, %class.btVector3** %N3, align 4
  %20 = load %class.btVector3*, %class.btVector3** %N1, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %19, %class.btVector3* nonnull align 4 dereferenceable(16) %20)
  %21 = bitcast %class.btVector3* %edge1 to i8*
  %22 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false)
  store float 1.000000e+00, float* %normalSign, align 4
  store i32 0, i32* %ww, align 4
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc, %for.body9
  %23 = load i32, i32* %ww, align 4
  %cmp16 = icmp slt i32 %23, 2
  br i1 %cmp16, label %for.body17, label %for.end

for.body17:                                       ; preds = %for.cond15
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp19, %class.btVector3* %edge0, %class.btVector3* nonnull align 4 dereferenceable(16) %edge1)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp18, float* nonnull align 4 dereferenceable(4) %normalSign, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp19)
  %24 = bitcast %class.btVector3* %planeEquation to i8*
  %25 = bitcast %class.btVector3* %ref.tmp18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %24, i8* align 4 %25, i32 16, i1 false)
  %call20 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %planeEquation)
  %cmp21 = fcmp ogt float %call20, 0x3F1A36E2E0000000
  br i1 %cmp21, label %if.then, label %if.end30

if.then:                                          ; preds = %for.body17
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %planeEquation)
  %26 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %planeEquationsOut.addr, align 4
  %call23 = call zeroext i1 @_Z8notExistRK9btVector3RK20btAlignedObjectArrayIS_E(%class.btVector3* nonnull align 4 dereferenceable(16) %planeEquation, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %26)
  br i1 %call23, label %if.then24, label %if.end29

if.then24:                                        ; preds = %if.then
  %27 = load %class.btVector3*, %class.btVector3** %N1, align 4
  %call25 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %planeEquation, %class.btVector3* nonnull align 4 dereferenceable(16) %27)
  %fneg = fneg float %call25
  %call26 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %planeEquation)
  %arrayidx = getelementptr inbounds float, float* %call26, i32 3
  store float %fneg, float* %arrayidx, align 4
  %28 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %vertices.addr, align 4
  %call27 = call zeroext i1 @_ZN14btGeometryUtil22areVerticesBehindPlaneERK9btVector3RK20btAlignedObjectArrayIS0_Ef(%class.btVector3* nonnull align 4 dereferenceable(16) %planeEquation, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %28, float 0x3F847AE140000000)
  br i1 %call27, label %if.then28, label %if.end

if.then28:                                        ; preds = %if.then24
  %29 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %planeEquationsOut.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %29, %class.btVector3* nonnull align 4 dereferenceable(16) %planeEquation)
  br label %if.end

if.end:                                           ; preds = %if.then28, %if.then24
  br label %if.end29

if.end29:                                         ; preds = %if.end, %if.then
  br label %if.end30

if.end30:                                         ; preds = %if.end29, %for.body17
  store float -1.000000e+00, float* %normalSign, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end30
  %30 = load i32, i32* %ww, align 4
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %ww, align 4
  br label %for.cond15

for.end:                                          ; preds = %for.cond15
  br label %for.inc31

for.inc31:                                        ; preds = %for.end
  %31 = load i32, i32* %k, align 4
  %inc32 = add nsw i32 %31, 1
  store i32 %inc32, i32* %k, align 4
  br label %for.cond7

for.end33:                                        ; preds = %for.cond7
  br label %for.inc34

for.inc34:                                        ; preds = %for.end33
  %32 = load i32, i32* %j, align 4
  %inc35 = add nsw i32 %32, 1
  store i32 %inc35, i32* %j, align 4
  br label %for.cond2

for.end36:                                        ; preds = %for.cond2
  br label %for.inc37

for.inc37:                                        ; preds = %for.end36
  %33 = load i32, i32* %i, align 4
  %inc38 = add nsw i32 %33, 1
  store i32 %inc38, i32* %i, align 4
  br label %for.cond

for.end39:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.btVector3*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btVector3* %_Val, %class.btVector3** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 %2
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  %call5 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %3)
  %4 = bitcast i8* %call5 to %class.btVector3*
  %5 = load %class.btVector3*, %class.btVector3** %_Val.addr, align 4
  %6 = bitcast %class.btVector3* %4 to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size6, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size6, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btGeometryUtil29getVerticesFromPlaneEquationsERK20btAlignedObjectArrayI9btVector3ERS2_(%class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %planeEquations, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %verticesOut) #2 {
entry:
  %planeEquations.addr = alloca %class.btAlignedObjectArray*, align 4
  %verticesOut.addr = alloca %class.btAlignedObjectArray*, align 4
  %numbrushes = alloca i32, align 4
  %i = alloca i32, align 4
  %N1 = alloca %class.btVector3*, align 4
  %j = alloca i32, align 4
  %N2 = alloca %class.btVector3*, align 4
  %k = alloca i32, align 4
  %N3 = alloca %class.btVector3*, align 4
  %n2n3 = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %n3n1 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %n1n2 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  %quotient = alloca float, align 4
  %potentialVertex = alloca %class.btVector3, align 4
  store %class.btAlignedObjectArray* %planeEquations, %class.btAlignedObjectArray** %planeEquations.addr, align 4
  store %class.btAlignedObjectArray* %verticesOut, %class.btAlignedObjectArray** %verticesOut.addr, align 4
  %0 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %planeEquations.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %0)
  store i32 %call, i32* %numbrushes, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc45, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %numbrushes, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end47

for.body:                                         ; preds = %for.cond
  %3 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %planeEquations.addr, align 4
  %4 = load i32, i32* %i, align 4
  %call1 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %3, i32 %4)
  store %class.btVector3* %call1, %class.btVector3** %N1, align 4
  %5 = load i32, i32* %i, align 4
  %add = add nsw i32 %5, 1
  store i32 %add, i32* %j, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc42, %for.body
  %6 = load i32, i32* %j, align 4
  %7 = load i32, i32* %numbrushes, align 4
  %cmp3 = icmp slt i32 %6, %7
  br i1 %cmp3, label %for.body4, label %for.end44

for.body4:                                        ; preds = %for.cond2
  %8 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %planeEquations.addr, align 4
  %9 = load i32, i32* %j, align 4
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %8, i32 %9)
  store %class.btVector3* %call5, %class.btVector3** %N2, align 4
  %10 = load i32, i32* %j, align 4
  %add6 = add nsw i32 %10, 1
  store i32 %add6, i32* %k, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc, %for.body4
  %11 = load i32, i32* %k, align 4
  %12 = load i32, i32* %numbrushes, align 4
  %cmp8 = icmp slt i32 %11, %12
  br i1 %cmp8, label %for.body9, label %for.end

for.body9:                                        ; preds = %for.cond7
  %13 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %planeEquations.addr, align 4
  %14 = load i32, i32* %k, align 4
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %13, i32 %14)
  store %class.btVector3* %call10, %class.btVector3** %N3, align 4
  %call11 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %n2n3)
  %15 = load %class.btVector3*, %class.btVector3** %N2, align 4
  %16 = load %class.btVector3*, %class.btVector3** %N3, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %16)
  %17 = bitcast %class.btVector3* %n2n3 to i8*
  %18 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 16, i1 false)
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %n3n1)
  %19 = load %class.btVector3*, %class.btVector3** %N3, align 4
  %20 = load %class.btVector3*, %class.btVector3** %N1, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* %19, %class.btVector3* nonnull align 4 dereferenceable(16) %20)
  %21 = bitcast %class.btVector3* %n3n1 to i8*
  %22 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false)
  %call14 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %n1n2)
  %23 = load %class.btVector3*, %class.btVector3** %N1, align 4
  %24 = load %class.btVector3*, %class.btVector3** %N2, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp15, %class.btVector3* %23, %class.btVector3* nonnull align 4 dereferenceable(16) %24)
  %25 = bitcast %class.btVector3* %n1n2 to i8*
  %26 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 16, i1 false)
  %call16 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %n2n3)
  %cmp17 = fcmp ogt float %call16, 0x3F1A36E2E0000000
  br i1 %cmp17, label %land.lhs.true, label %if.end41

land.lhs.true:                                    ; preds = %for.body9
  %call18 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %n3n1)
  %cmp19 = fcmp ogt float %call18, 0x3F1A36E2E0000000
  br i1 %cmp19, label %land.lhs.true20, label %if.end41

land.lhs.true20:                                  ; preds = %land.lhs.true
  %call21 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %n1n2)
  %cmp22 = fcmp ogt float %call21, 0x3F1A36E2E0000000
  br i1 %cmp22, label %if.then, label %if.end41

if.then:                                          ; preds = %land.lhs.true20
  %27 = load %class.btVector3*, %class.btVector3** %N1, align 4
  %call23 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %27, %class.btVector3* nonnull align 4 dereferenceable(16) %n2n3)
  store float %call23, float* %quotient, align 4
  %28 = load float, float* %quotient, align 4
  %call24 = call float @_Z6btFabsf(float %28)
  %cmp25 = fcmp ogt float %call24, 0x3EB0C6F7A0000000
  br i1 %cmp25, label %if.then26, label %if.end40

if.then26:                                        ; preds = %if.then
  %29 = load float, float* %quotient, align 4
  %div = fdiv float -1.000000e+00, %29
  store float %div, float* %quotient, align 4
  %30 = load %class.btVector3*, %class.btVector3** %N1, align 4
  %call27 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %30)
  %arrayidx = getelementptr inbounds float, float* %call27, i32 3
  %call28 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %n2n3, float* nonnull align 4 dereferenceable(4) %arrayidx)
  %31 = load %class.btVector3*, %class.btVector3** %N2, align 4
  %call29 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %31)
  %arrayidx30 = getelementptr inbounds float, float* %call29, i32 3
  %call31 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %n3n1, float* nonnull align 4 dereferenceable(4) %arrayidx30)
  %32 = load %class.btVector3*, %class.btVector3** %N3, align 4
  %call32 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %32)
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 3
  %call34 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %n1n2, float* nonnull align 4 dereferenceable(4) %arrayidx33)
  %33 = bitcast %class.btVector3* %potentialVertex to i8*
  %34 = bitcast %class.btVector3* %n2n3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 4 %34, i32 16, i1 false)
  %call35 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %potentialVertex, %class.btVector3* nonnull align 4 dereferenceable(16) %n3n1)
  %call36 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %potentialVertex, %class.btVector3* nonnull align 4 dereferenceable(16) %n1n2)
  %call37 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %potentialVertex, float* nonnull align 4 dereferenceable(4) %quotient)
  %35 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %planeEquations.addr, align 4
  %call38 = call zeroext i1 @_ZN14btGeometryUtil19isPointInsidePlanesERK20btAlignedObjectArrayI9btVector3ERKS1_f(%class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %35, %class.btVector3* nonnull align 4 dereferenceable(16) %potentialVertex, float 0x3F847AE140000000)
  br i1 %call38, label %if.then39, label %if.end

if.then39:                                        ; preds = %if.then26
  %36 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %verticesOut.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %36, %class.btVector3* nonnull align 4 dereferenceable(16) %potentialVertex)
  br label %if.end

if.end:                                           ; preds = %if.then39, %if.then26
  br label %if.end40

if.end40:                                         ; preds = %if.end, %if.then
  br label %if.end41

if.end41:                                         ; preds = %if.end40, %land.lhs.true20, %land.lhs.true, %for.body9
  br label %for.inc

for.inc:                                          ; preds = %if.end41
  %37 = load i32, i32* %k, align 4
  %inc = add nsw i32 %37, 1
  store i32 %inc, i32* %k, align 4
  br label %for.cond7

for.end:                                          ; preds = %for.cond7
  br label %for.inc42

for.inc42:                                        ; preds = %for.end
  %38 = load i32, i32* %j, align 4
  %inc43 = add nsw i32 %38, 1
  store i32 %inc43, i32* %j, align 4
  br label %for.cond2

for.end44:                                        ; preds = %for.cond2
  br label %for.inc45

for.inc45:                                        ; preds = %for.end44
  %39 = load i32, i32* %i, align 4
  %inc46 = add nsw i32 %39, 1
  store i32 %inc46, i32* %i, align 4
  br label %for.cond

for.end47:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #4

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %2, %class.btVector3** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %class.btVector3*, %class.btVector3** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btVector3* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* %4, %class.btVector3** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btVector3* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  %5 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  %9 = bitcast %class.btVector3* %6 to i8*
  %10 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %class.btVector3** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %class.btVector3* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #5

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btGeometryUtil.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { nounwind readnone speculatable willreturn }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
