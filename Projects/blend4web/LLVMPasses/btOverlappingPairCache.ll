; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/BroadphaseCollision/btOverlappingPairCache.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/BroadphaseCollision/btOverlappingPairCache.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btHashedOverlappingPairCache = type { %class.btOverlappingPairCache, %class.btAlignedObjectArray, %struct.btOverlapFilterCallback*, %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1, %class.btOverlappingPairCallback* }
%class.btOverlappingPairCache = type { %class.btOverlappingPairCallback }
%class.btOverlappingPairCallback = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btBroadphasePair*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btBroadphasePair = type { %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btCollisionAlgorithm*, %union.anon.0 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%union.anon.0 = type { i8* }
%struct.btOverlapFilterCallback = type { i32 (...)** }
%class.btAlignedObjectArray.1 = type <{ %class.btAlignedAllocator.2, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.2 = type { i8 }
%class.btSortedOverlappingPairCache = type { %class.btOverlappingPairCache, %class.btAlignedObjectArray, i8, i8, %struct.btOverlapFilterCallback*, %class.btOverlappingPairCallback* }
%class.CleanPairCallback = type { %struct.btOverlapCallback, %struct.btBroadphaseProxy*, %class.btOverlappingPairCache*, %class.btDispatcher* }
%struct.btOverlapCallback = type { i32 (...)** }
%class.RemovePairCallback = type { %struct.btOverlapCallback, %struct.btBroadphaseProxy* }
%class.CProfileSample = type { i8 }
%class.btBroadphasePairSortPredicate = type { i8 }
%class.CleanPairCallback.5 = type { %struct.btOverlapCallback, %struct.btBroadphaseProxy*, %class.btOverlappingPairCache*, %class.btDispatcher* }
%class.RemovePairCallback.6 = type { %struct.btOverlapCallback, %struct.btBroadphaseProxy* }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN22btOverlappingPairCacheC2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIiED2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairED2Ev = comdat any

$_ZN28btHashedOverlappingPairCachedlEPv = comdat any

$_Z6btSwapIP17btBroadphaseProxyEvRT_S3_ = comdat any

$_ZNK17btBroadphaseProxy6getUidEv = comdat any

$_ZN28btHashedOverlappingPairCache7getHashEjj = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZN28btHashedOverlappingPairCache10equalsPairERK16btBroadphasePairii = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi = comdat any

$_ZN20btAlignedObjectArrayIiE6resizeEiRKi = comdat any

$_ZN28btHashedOverlappingPairCache16internalFindPairEP17btBroadphaseProxyS1_i = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE21expandNonInitializingEv = comdat any

$_ZN16btBroadphasePairnwEmPv = comdat any

$_ZN16btBroadphasePairC2ER17btBroadphaseProxyS1_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE8pop_backEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE9push_backERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_ = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE16findLinearSearchERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii = comdat any

$_ZNK28btSortedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_ = comdat any

$_ZN28btHashedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_ = comdat any

$_ZN28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv = comdat any

$_ZNK28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv = comdat any

$_ZN28btHashedOverlappingPairCache23getOverlappingPairArrayEv = comdat any

$_ZNK28btHashedOverlappingPairCache22getNumOverlappingPairsEv = comdat any

$_ZN28btHashedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback = comdat any

$_ZN28btHashedOverlappingPairCache18hasDeferredRemovalEv = comdat any

$_ZN28btHashedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback = comdat any

$_ZN28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv = comdat any

$_ZNK28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv = comdat any

$_ZN28btSortedOverlappingPairCache23getOverlappingPairArrayEv = comdat any

$_ZNK28btSortedOverlappingPairCache22getNumOverlappingPairsEv = comdat any

$_ZN28btSortedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback = comdat any

$_ZN28btSortedOverlappingPairCache18hasDeferredRemovalEv = comdat any

$_ZN28btSortedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback = comdat any

$_ZN25btOverlappingPairCallbackC2Ev = comdat any

$_ZN22btOverlappingPairCacheD2Ev = comdat any

$_ZN22btOverlappingPairCacheD0Ev = comdat any

$_ZN25btOverlappingPairCallbackD2Ev = comdat any

$_ZN25btOverlappingPairCallbackD0Ev = comdat any

$_ZN17btOverlapCallbackC2Ev = comdat any

$_ZN17btOverlapCallbackD2Ev = comdat any

$_ZN17btOverlapCallbackD0Ev = comdat any

$_ZNK28btHashedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_ = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairEixEi = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE4initEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_ = comdat any

$_ZN16btBroadphasePairC2ERKS_ = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii = comdat any

$_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_ = comdat any

$_ZeqRK16btBroadphasePairS1_ = comdat any

$_ZTS22btOverlappingPairCache = comdat any

$_ZTS25btOverlappingPairCallback = comdat any

$_ZTI25btOverlappingPairCallback = comdat any

$_ZTI22btOverlappingPairCache = comdat any

$_ZTV22btOverlappingPairCache = comdat any

$_ZTV25btOverlappingPairCallback = comdat any

$_ZTS17btOverlapCallback = comdat any

$_ZTI17btOverlapCallback = comdat any

$_ZTV17btOverlapCallback = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@gOverlappingPairs = hidden global i32 0, align 4
@gRemovePairs = hidden global i32 0, align 4
@gAddedPairs = hidden global i32 0, align 4
@gFindPairs = hidden global i32 0, align 4
@_ZTV28btHashedOverlappingPairCache = hidden unnamed_addr constant { [19 x i8*] } { [19 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI28btHashedOverlappingPairCache to i8*), i8* bitcast (%class.btHashedOverlappingPairCache* (%class.btHashedOverlappingPairCache*)* @_ZN28btHashedOverlappingPairCacheD1Ev to i8*), i8* bitcast (void (%class.btHashedOverlappingPairCache*)* @_ZN28btHashedOverlappingPairCacheD0Ev to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)* @_ZN28btHashedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_ to i8*), i8* bitcast (i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN28btHashedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher to i8*), i8* bitcast (void (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*)* @_ZN28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*)* @_ZNK28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv to i8*), i8* bitcast (%class.btAlignedObjectArray* (%class.btHashedOverlappingPairCache*)* @_ZN28btHashedOverlappingPairCache23getOverlappingPairArrayEv to i8*), i8* bitcast (void (%class.btHashedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)* @_ZN28btHashedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher to i8*), i8* bitcast (i32 (%class.btHashedOverlappingPairCache*)* @_ZNK28btHashedOverlappingPairCache22getNumOverlappingPairsEv to i8*), i8* bitcast (void (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (void (%class.btHashedOverlappingPairCache*, %struct.btOverlapFilterCallback*)* @_ZN28btHashedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback to i8*), i8* bitcast (void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)* @_ZN28btHashedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)* @_ZN28btHashedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_ to i8*), i8* bitcast (i1 (%class.btHashedOverlappingPairCache*)* @_ZN28btHashedOverlappingPairCache18hasDeferredRemovalEv to i8*), i8* bitcast (void (%class.btHashedOverlappingPairCache*, %class.btOverlappingPairCallback*)* @_ZN28btHashedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback to i8*), i8* bitcast (void (%class.btHashedOverlappingPairCache*, %class.btDispatcher*)* @_ZN28btHashedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher to i8*)] }, align 4
@.str = private unnamed_addr constant [57 x i8] c"btHashedOverlappingPairCache::processAllOverlappingPairs\00", align 1
@_ZTV28btSortedOverlappingPairCache = hidden unnamed_addr constant { [19 x i8*] } { [19 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI28btSortedOverlappingPairCache to i8*), i8* bitcast (%class.btSortedOverlappingPairCache* (%class.btSortedOverlappingPairCache*)* @_ZN28btSortedOverlappingPairCacheD1Ev to i8*), i8* bitcast (void (%class.btSortedOverlappingPairCache*)* @_ZN28btSortedOverlappingPairCacheD0Ev to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btSortedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)* @_ZN28btSortedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_ to i8*), i8* bitcast (i8* (%class.btSortedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN28btSortedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher to i8*), i8* bitcast (void (%class.btSortedOverlappingPairCache*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btSortedOverlappingPairCache*)* @_ZN28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btSortedOverlappingPairCache*)* @_ZNK28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv to i8*), i8* bitcast (%class.btAlignedObjectArray* (%class.btSortedOverlappingPairCache*)* @_ZN28btSortedOverlappingPairCache23getOverlappingPairArrayEv to i8*), i8* bitcast (void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)* @_ZN28btSortedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher to i8*), i8* bitcast (i32 (%class.btSortedOverlappingPairCache*)* @_ZNK28btSortedOverlappingPairCache22getNumOverlappingPairsEv to i8*), i8* bitcast (void (%class.btSortedOverlappingPairCache*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (void (%class.btSortedOverlappingPairCache*, %struct.btOverlapFilterCallback*)* @_ZN28btSortedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback to i8*), i8* bitcast (void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)* @_ZN28btSortedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btSortedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)* @_ZN28btSortedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_ to i8*), i8* bitcast (i1 (%class.btSortedOverlappingPairCache*)* @_ZN28btSortedOverlappingPairCache18hasDeferredRemovalEv to i8*), i8* bitcast (void (%class.btSortedOverlappingPairCache*, %class.btOverlappingPairCallback*)* @_ZN28btSortedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback to i8*), i8* bitcast (void (%class.btSortedOverlappingPairCache*, %class.btDispatcher*)* @_ZN28btSortedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS28btHashedOverlappingPairCache = hidden constant [31 x i8] c"28btHashedOverlappingPairCache\00", align 1
@_ZTS22btOverlappingPairCache = linkonce_odr hidden constant [25 x i8] c"22btOverlappingPairCache\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS25btOverlappingPairCallback = linkonce_odr hidden constant [28 x i8] c"25btOverlappingPairCallback\00", comdat, align 1
@_ZTI25btOverlappingPairCallback = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([28 x i8], [28 x i8]* @_ZTS25btOverlappingPairCallback, i32 0, i32 0) }, comdat, align 4
@_ZTI22btOverlappingPairCache = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([25 x i8], [25 x i8]* @_ZTS22btOverlappingPairCache, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI25btOverlappingPairCallback to i8*) }, comdat, align 4
@_ZTI28btHashedOverlappingPairCache = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([31 x i8], [31 x i8]* @_ZTS28btHashedOverlappingPairCache, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22btOverlappingPairCache to i8*) }, align 4
@_ZTS28btSortedOverlappingPairCache = hidden constant [31 x i8] c"28btSortedOverlappingPairCache\00", align 1
@_ZTI28btSortedOverlappingPairCache = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([31 x i8], [31 x i8]* @_ZTS28btSortedOverlappingPairCache, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22btOverlappingPairCache to i8*) }, align 4
@_ZTV22btOverlappingPairCache = linkonce_odr hidden unnamed_addr constant { [19 x i8*] } { [19 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22btOverlappingPairCache to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btOverlappingPairCache*)* @_ZN22btOverlappingPairCacheD2Ev to i8*), i8* bitcast (void (%class.btOverlappingPairCache*)* @_ZN22btOverlappingPairCacheD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTV25btOverlappingPairCallback = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI25btOverlappingPairCallback to i8*), i8* bitcast (%class.btOverlappingPairCallback* (%class.btOverlappingPairCallback*)* @_ZN25btOverlappingPairCallbackD2Ev to i8*), i8* bitcast (void (%class.btOverlappingPairCallback*)* @_ZN25btOverlappingPairCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTVZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback to i8*), i8* bitcast (%class.CleanPairCallback* (%class.CleanPairCallback*)* @_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD2Ev to i8*), i8* bitcast (void (%class.CleanPairCallback*)* @_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev to i8*), i8* bitcast (i1 (%class.CleanPairCallback*, %struct.btBroadphasePair*)* @_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair to i8*)] }, align 4
@_ZTSZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback = internal constant [110 x i8] c"ZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback\00", align 1
@_ZTS17btOverlapCallback = linkonce_odr hidden constant [20 x i8] c"17btOverlapCallback\00", comdat, align 1
@_ZTI17btOverlapCallback = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17btOverlapCallback, i32 0, i32 0) }, comdat, align 4
@_ZTIZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([110 x i8], [110 x i8]* @_ZTSZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI17btOverlapCallback to i8*) }, align 4
@_ZTV17btOverlapCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI17btOverlapCallback to i8*), i8* bitcast (%struct.btOverlapCallback* (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD2Ev to i8*), i8* bitcast (void (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTVZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback to i8*), i8* bitcast (%class.RemovePairCallback* (%class.RemovePairCallback*)* @_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD2Ev to i8*), i8* bitcast (void (%class.RemovePairCallback*)* @_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev to i8*), i8* bitcast (i1 (%class.RemovePairCallback*, %struct.btBroadphasePair*)* @_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair to i8*)] }, align 4
@_ZTSZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback = internal constant [129 x i8] c"ZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback\00", align 1
@_ZTIZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([129 x i8], [129 x i8]* @_ZTSZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI17btOverlapCallback to i8*) }, align 4
@_ZTVZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback to i8*), i8* bitcast (%class.CleanPairCallback.5* (%class.CleanPairCallback.5*)* @_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD2Ev to i8*), i8* bitcast (void (%class.CleanPairCallback.5*)* @_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev to i8*), i8* bitcast (i1 (%class.CleanPairCallback.5*, %struct.btBroadphasePair*)* @_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair to i8*)] }, align 4
@_ZTSZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback = internal constant [110 x i8] c"ZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback\00", align 1
@_ZTIZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([110 x i8], [110 x i8]* @_ZTSZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI17btOverlapCallback to i8*) }, align 4
@_ZTVZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback to i8*), i8* bitcast (%class.RemovePairCallback.6* (%class.RemovePairCallback.6*)* @_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD2Ev to i8*), i8* bitcast (void (%class.RemovePairCallback.6*)* @_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev to i8*), i8* bitcast (i1 (%class.RemovePairCallback.6*, %struct.btBroadphasePair*)* @_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair to i8*)] }, align 4
@_ZTSZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback = internal constant [129 x i8] c"ZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback\00", align 1
@_ZTIZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([129 x i8], [129 x i8]* @_ZTSZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI17btOverlapCallback to i8*) }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btOverlappingPairCache.cpp, i8* null }]

@_ZN28btHashedOverlappingPairCacheC1Ev = hidden unnamed_addr alias %class.btHashedOverlappingPairCache* (%class.btHashedOverlappingPairCache*), %class.btHashedOverlappingPairCache* (%class.btHashedOverlappingPairCache*)* @_ZN28btHashedOverlappingPairCacheC2Ev
@_ZN28btHashedOverlappingPairCacheD1Ev = hidden unnamed_addr alias %class.btHashedOverlappingPairCache* (%class.btHashedOverlappingPairCache*), %class.btHashedOverlappingPairCache* (%class.btHashedOverlappingPairCache*)* @_ZN28btHashedOverlappingPairCacheD2Ev
@_ZN28btSortedOverlappingPairCacheC1Ev = hidden unnamed_addr alias %class.btSortedOverlappingPairCache* (%class.btSortedOverlappingPairCache*), %class.btSortedOverlappingPairCache* (%class.btSortedOverlappingPairCache*)* @_ZN28btSortedOverlappingPairCacheC2Ev
@_ZN28btSortedOverlappingPairCacheD1Ev = hidden unnamed_addr alias %class.btSortedOverlappingPairCache* (%class.btSortedOverlappingPairCache*), %class.btSortedOverlappingPairCache* (%class.btSortedOverlappingPairCache*)* @_ZN28btSortedOverlappingPairCacheD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheC2Ev(%class.btHashedOverlappingPairCache* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %initialAllocatedSize = alloca i32, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.btHashedOverlappingPairCache* %this1 to %class.btOverlappingPairCache*
  %call = call %class.btOverlappingPairCache* @_ZN22btOverlappingPairCacheC2Ev(%class.btOverlappingPairCache* %0) #8
  %1 = bitcast %class.btHashedOverlappingPairCache* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV28btHashedOverlappingPairCache, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI16btBroadphasePairEC2Ev(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %m_overlapFilterCallback = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 2
  store %struct.btOverlapFilterCallback* null, %struct.btOverlapFilterCallback** %m_overlapFilterCallback, align 4
  %m_hashTable = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  %call3 = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.1* %m_hashTable)
  %m_next = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 4
  %call4 = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.1* %m_next)
  %m_ghostPairCallback = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  store %class.btOverlappingPairCallback* null, %class.btOverlappingPairCallback** %m_ghostPairCallback, align 4
  store i32 2, i32* %initialAllocatedSize, align 4
  %m_overlappingPairArray5 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %2 = load i32, i32* %initialAllocatedSize, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray* %m_overlappingPairArray5, i32 %2)
  call void @_ZN28btHashedOverlappingPairCache10growTablesEv(%class.btHashedOverlappingPairCache* %this1)
  ret %class.btHashedOverlappingPairCache* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZN22btOverlappingPairCacheC2Ev(%class.btOverlappingPairCache* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCache*, align 4
  store %class.btOverlappingPairCache* %this, %class.btOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.btOverlappingPairCache* %this1 to %class.btOverlappingPairCallback*
  %call = call %class.btOverlappingPairCallback* @_ZN25btOverlappingPairCallbackC2Ev(%class.btOverlappingPairCallback* %0) #8
  %1 = bitcast %class.btOverlappingPairCache* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV22btOverlappingPairCache, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %class.btOverlappingPairCache* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI16btBroadphasePairEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.1* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.2* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.2* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.1* %this1)
  ret %class.btAlignedObjectArray.1* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btBroadphasePair*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btBroadphasePair*
  store %struct.btBroadphasePair* %2, %struct.btBroadphasePair** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btBroadphasePair* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* %4, %struct.btBroadphasePair** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN28btHashedOverlappingPairCache10growTablesEv(%class.btHashedOverlappingPairCache* %this) #2 {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %newCapacity = alloca i32, align 4
  %curHashtableSize = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp6 = alloca i32, align 4
  %i = alloca i32, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  %proxyId1 = alloca i32, align 4
  %proxyId2 = alloca i32, align 4
  %hashValue = alloca i32, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  store i32 %call, i32* %newCapacity, align 4
  %m_hashTable = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.1* %m_hashTable)
  %0 = load i32, i32* %newCapacity, align 4
  %cmp = icmp slt i32 %call2, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_hashTable3 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.1* %m_hashTable3)
  store i32 %call4, i32* %curHashtableSize, align 4
  %m_hashTable5 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  %1 = load i32, i32* %newCapacity, align 4
  store i32 0, i32* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.1* %m_hashTable5, i32 %1, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %m_next = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 4
  %2 = load i32, i32* %newCapacity, align 4
  store i32 0, i32* %ref.tmp6, align 4
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.1* %m_next, i32 %2, i32* nonnull align 4 dereferenceable(4) %ref.tmp6)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %newCapacity, align 4
  %cmp7 = icmp slt i32 %3, %4
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_hashTable8 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  %5 = load i32, i32* %i, align 4
  %call9 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_hashTable8, i32 %5)
  store i32 -1, i32* %call9, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc15, %for.end
  %7 = load i32, i32* %i, align 4
  %8 = load i32, i32* %newCapacity, align 4
  %cmp11 = icmp slt i32 %7, %8
  br i1 %cmp11, label %for.body12, label %for.end17

for.body12:                                       ; preds = %for.cond10
  %m_next13 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 4
  %9 = load i32, i32* %i, align 4
  %call14 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_next13, i32 %9)
  store i32 -1, i32* %call14, align 4
  br label %for.inc15

for.inc15:                                        ; preds = %for.body12
  %10 = load i32, i32* %i, align 4
  %inc16 = add nsw i32 %10, 1
  store i32 %inc16, i32* %i, align 4
  br label %for.cond10

for.end17:                                        ; preds = %for.cond10
  store i32 0, i32* %i, align 4
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc34, %for.end17
  %11 = load i32, i32* %i, align 4
  %12 = load i32, i32* %curHashtableSize, align 4
  %cmp19 = icmp slt i32 %11, %12
  br i1 %cmp19, label %for.body20, label %for.end36

for.body20:                                       ; preds = %for.cond18
  %m_overlappingPairArray21 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %13 = load i32, i32* %i, align 4
  %call22 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray21, i32 %13)
  store %struct.btBroadphasePair* %call22, %struct.btBroadphasePair** %pair, align 4
  %14 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %14, i32 0, i32 0
  %15 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %call23 = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %15)
  store i32 %call23, i32* %proxyId1, align 4
  %16 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %16, i32 0, i32 1
  %17 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %call24 = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %17)
  store i32 %call24, i32* %proxyId2, align 4
  %18 = load i32, i32* %proxyId1, align 4
  %19 = load i32, i32* %proxyId2, align 4
  %call25 = call i32 @_ZN28btHashedOverlappingPairCache7getHashEjj(%class.btHashedOverlappingPairCache* %this1, i32 %18, i32 %19)
  %m_overlappingPairArray26 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call27 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray26)
  %sub = sub nsw i32 %call27, 1
  %and = and i32 %call25, %sub
  store i32 %and, i32* %hashValue, align 4
  %m_hashTable28 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  %20 = load i32, i32* %hashValue, align 4
  %call29 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_hashTable28, i32 %20)
  %21 = load i32, i32* %call29, align 4
  %m_next30 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 4
  %22 = load i32, i32* %i, align 4
  %call31 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_next30, i32 %22)
  store i32 %21, i32* %call31, align 4
  %23 = load i32, i32* %i, align 4
  %m_hashTable32 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  %24 = load i32, i32* %hashValue, align 4
  %call33 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_hashTable32, i32 %24)
  store i32 %23, i32* %call33, align 4
  br label %for.inc34

for.inc34:                                        ; preds = %for.body20
  %25 = load i32, i32* %i, align 4
  %inc35 = add nsw i32 %25, 1
  store i32 %inc35, i32* %i, align 4
  br label %for.cond18

for.end36:                                        ; preds = %for.cond18
  br label %if.end

if.end:                                           ; preds = %for.end36, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheD2Ev(%class.btHashedOverlappingPairCache* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.btHashedOverlappingPairCache* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV28btHashedOverlappingPairCache, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_next = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 4
  %call = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.1* %m_next) #8
  %m_hashTable = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  %call2 = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.1* %m_hashTable) #8
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call3 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI16btBroadphasePairED2Ev(%class.btAlignedObjectArray* %m_overlappingPairArray) #8
  %1 = bitcast %class.btHashedOverlappingPairCache* %this1 to %class.btOverlappingPairCache*
  %call4 = call %class.btOverlappingPairCache* @_ZN22btOverlappingPairCacheD2Ev(%class.btOverlappingPairCache* %1) #8
  ret %class.btHashedOverlappingPairCache* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.1* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.1* %this1)
  ret %class.btAlignedObjectArray.1* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI16btBroadphasePairED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN28btHashedOverlappingPairCacheD0Ev(%class.btHashedOverlappingPairCache* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %call = call %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheD1Ev(%class.btHashedOverlappingPairCache* %this1) #8
  %0 = bitcast %class.btHashedOverlappingPairCache* %this1 to i8*
  call void @_ZN28btHashedOverlappingPairCachedlEPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN28btHashedOverlappingPairCachedlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN28btHashedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %pair, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %pair.addr = alloca %struct.btBroadphasePair*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  store %struct.btBroadphasePair* %pair, %struct.btBroadphasePair** %pair.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 2
  %1 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm, align 4
  %tobool = icmp ne %class.btCollisionAlgorithm* %1, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %2 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %tobool2 = icmp ne %class.btDispatcher* %2, null
  br i1 %tobool2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_algorithm3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 0, i32 2
  %4 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm3, align 4
  %5 = bitcast %class.btCollisionAlgorithm* %4 to %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)***
  %vtable = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)**, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*** %5, align 4
  %vfn = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vtable, i64 0
  %6 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vfn, align 4
  %call = call %class.btCollisionAlgorithm* %6(%class.btCollisionAlgorithm* %4) #8
  %7 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %8 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_algorithm4 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %8, i32 0, i32 2
  %9 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm4, align 4
  %10 = bitcast %class.btCollisionAlgorithm* %9 to i8*
  %11 = bitcast %class.btDispatcher* %7 to void (%class.btDispatcher*, i8*)***
  %vtable5 = load void (%class.btDispatcher*, i8*)**, void (%class.btDispatcher*, i8*)*** %11, align 4
  %vfn6 = getelementptr inbounds void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vtable5, i64 15
  %12 = load void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vfn6, align 4
  call void %12(%class.btDispatcher* %7, i8* %10)
  %13 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_algorithm7 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %13, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm7, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %cleanPairs = alloca %class.CleanPairCallback, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4
  %1 = bitcast %class.btHashedOverlappingPairCache* %this1 to %class.btOverlappingPairCache*
  %2 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %call = call %class.CleanPairCallback* @_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackC2ES1_P22btOverlappingPairCacheS3_(%class.CleanPairCallback* %cleanPairs, %struct.btBroadphaseProxy* %0, %class.btOverlappingPairCache* %1, %class.btDispatcher* %2)
  %3 = bitcast %class.CleanPairCallback* %cleanPairs to %struct.btOverlapCallback*
  %4 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %5 = bitcast %class.btHashedOverlappingPairCache* %this1 to void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)***
  %vtable = load void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)**, void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*** %5, align 4
  %vfn = getelementptr inbounds void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vtable, i64 12
  %6 = load void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vfn, align 4
  call void %6(%class.btHashedOverlappingPairCache* %this1, %struct.btOverlapCallback* %3, %class.btDispatcher* %4)
  %call2 = call %class.CleanPairCallback* @_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD2Ev(%class.CleanPairCallback* %cleanPairs) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal %class.CleanPairCallback* @_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackC2ES1_P22btOverlappingPairCacheS3_(%class.CleanPairCallback* returned %this, %struct.btBroadphaseProxy* %cleanProxy, %class.btOverlappingPairCache* %pairCache, %class.btDispatcher* %dispatcher) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.CleanPairCallback*, align 4
  %cleanProxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %pairCache.addr = alloca %class.btOverlappingPairCache*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.CleanPairCallback* %this, %class.CleanPairCallback** %this.addr, align 4
  store %struct.btBroadphaseProxy* %cleanProxy, %struct.btBroadphaseProxy** %cleanProxy.addr, align 4
  store %class.btOverlappingPairCache* %pairCache, %class.btOverlappingPairCache** %pairCache.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.CleanPairCallback*, %class.CleanPairCallback** %this.addr, align 4
  %0 = bitcast %class.CleanPairCallback* %this1 to %struct.btOverlapCallback*
  %call = call %struct.btOverlapCallback* @_ZN17btOverlapCallbackC2Ev(%struct.btOverlapCallback* %0) #8
  %1 = bitcast %class.CleanPairCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_cleanProxy = getelementptr inbounds %class.CleanPairCallback, %class.CleanPairCallback* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %cleanProxy.addr, align 4
  store %struct.btBroadphaseProxy* %2, %struct.btBroadphaseProxy** %m_cleanProxy, align 4
  %m_pairCache = getelementptr inbounds %class.CleanPairCallback, %class.CleanPairCallback* %this1, i32 0, i32 2
  %3 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCache.addr, align 4
  store %class.btOverlappingPairCache* %3, %class.btOverlappingPairCache** %m_pairCache, align 4
  %m_dispatcher = getelementptr inbounds %class.CleanPairCallback, %class.CleanPairCallback* %this1, i32 0, i32 3
  %4 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  store %class.btDispatcher* %4, %class.btDispatcher** %m_dispatcher, align 4
  ret %class.CleanPairCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define internal %class.CleanPairCallback* @_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD2Ev(%class.CleanPairCallback* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.CleanPairCallback*, align 4
  store %class.CleanPairCallback* %this, %class.CleanPairCallback** %this.addr, align 4
  %this1 = load %class.CleanPairCallback*, %class.CleanPairCallback** %this.addr, align 4
  %0 = bitcast %class.CleanPairCallback* %this1 to %struct.btOverlapCallback*
  %call = call %struct.btOverlapCallback* @_ZN17btOverlapCallbackD2Ev(%struct.btOverlapCallback* %0) #8
  ret %class.CleanPairCallback* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %removeCallback = alloca %class.RemovePairCallback, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4
  %call = call %class.RemovePairCallback* @_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackC2ES1_(%class.RemovePairCallback* %removeCallback, %struct.btBroadphaseProxy* %0)
  %1 = bitcast %class.RemovePairCallback* %removeCallback to %struct.btOverlapCallback*
  %2 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %3 = bitcast %class.btHashedOverlappingPairCache* %this1 to void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)***
  %vtable = load void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)**, void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*** %3, align 4
  %vfn = getelementptr inbounds void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vtable, i64 12
  %4 = load void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btHashedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vfn, align 4
  call void %4(%class.btHashedOverlappingPairCache* %this1, %struct.btOverlapCallback* %1, %class.btDispatcher* %2)
  %call2 = call %class.RemovePairCallback* @_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD2Ev(%class.RemovePairCallback* %removeCallback) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal %class.RemovePairCallback* @_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackC2ES1_(%class.RemovePairCallback* returned %this, %struct.btBroadphaseProxy* %obsoleteProxy) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.RemovePairCallback*, align 4
  %obsoleteProxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  store %class.RemovePairCallback* %this, %class.RemovePairCallback** %this.addr, align 4
  store %struct.btBroadphaseProxy* %obsoleteProxy, %struct.btBroadphaseProxy** %obsoleteProxy.addr, align 4
  %this1 = load %class.RemovePairCallback*, %class.RemovePairCallback** %this.addr, align 4
  %0 = bitcast %class.RemovePairCallback* %this1 to %struct.btOverlapCallback*
  %call = call %struct.btOverlapCallback* @_ZN17btOverlapCallbackC2Ev(%struct.btOverlapCallback* %0) #8
  %1 = bitcast %class.RemovePairCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_obsoleteProxy = getelementptr inbounds %class.RemovePairCallback, %class.RemovePairCallback* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %obsoleteProxy.addr, align 4
  store %struct.btBroadphaseProxy* %2, %struct.btBroadphaseProxy** %m_obsoleteProxy, align 4
  ret %class.RemovePairCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define internal %class.RemovePairCallback* @_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD2Ev(%class.RemovePairCallback* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.RemovePairCallback*, align 4
  store %class.RemovePairCallback* %this, %class.RemovePairCallback** %this.addr, align 4
  %this1 = load %class.RemovePairCallback*, %class.RemovePairCallback** %this.addr, align 4
  %0 = bitcast %class.RemovePairCallback* %this1 to %struct.btOverlapCallback*
  %call = call %struct.btOverlapCallback* @_ZN17btOverlapCallbackD2Ev(%struct.btOverlapCallback* %0) #8
  ret %class.RemovePairCallback* %this1
}

; Function Attrs: noinline optnone
define hidden %struct.btBroadphasePair* @_ZN28btHashedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) unnamed_addr #2 {
entry:
  %retval = alloca %struct.btBroadphasePair*, align 4
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxyId1 = alloca i32, align 4
  %proxyId2 = alloca i32, align 4
  %hash = alloca i32, align 4
  %index = alloca i32, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load i32, i32* @gFindPairs, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* @gFindPairs, align 4
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %1, i32 0, i32 3
  %2 = load i32, i32* %m_uniqueId, align 4
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %m_uniqueId2 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %3, i32 0, i32 3
  %4 = load i32, i32* %m_uniqueId2, align 4
  %cmp = icmp sgt i32 %2, %4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_Z6btSwapIP17btBroadphaseProxyEvRT_S3_(%struct.btBroadphaseProxy** nonnull align 4 dereferenceable(4) %proxy0.addr, %struct.btBroadphaseProxy** nonnull align 4 dereferenceable(4) %proxy1.addr)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %call = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %5)
  store i32 %call, i32* %proxyId1, align 4
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %call3 = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %6)
  store i32 %call3, i32* %proxyId2, align 4
  %7 = load i32, i32* %proxyId1, align 4
  %8 = load i32, i32* %proxyId2, align 4
  %call4 = call i32 @_ZN28btHashedOverlappingPairCache7getHashEjj(%class.btHashedOverlappingPairCache* %this1, i32 %7, i32 %8)
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %sub = sub nsw i32 %call5, 1
  %and = and i32 %call4, %sub
  store i32 %and, i32* %hash, align 4
  %9 = load i32, i32* %hash, align 4
  %m_hashTable = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  %call6 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.1* %m_hashTable)
  %cmp7 = icmp sge i32 %9, %call6
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end
  %m_hashTable10 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  %10 = load i32, i32* %hash, align 4
  %call11 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_hashTable10, i32 %10)
  %11 = load i32, i32* %call11, align 4
  store i32 %11, i32* %index, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end9
  %12 = load i32, i32* %index, align 4
  %cmp12 = icmp ne i32 %12, -1
  br i1 %cmp12, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %m_overlappingPairArray13 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %13 = load i32, i32* %index, align 4
  %call14 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray13, i32 %13)
  %14 = load i32, i32* %proxyId1, align 4
  %15 = load i32, i32* %proxyId2, align 4
  %call15 = call zeroext i1 @_ZN28btHashedOverlappingPairCache10equalsPairERK16btBroadphasePairii(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %call14, i32 %14, i32 %15)
  %conv = zext i1 %call15 to i32
  %cmp16 = icmp eq i32 %conv, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %16 = phi i1 [ false, %while.cond ], [ %cmp16, %land.rhs ]
  br i1 %16, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %m_next = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 4
  %17 = load i32, i32* %index, align 4
  %call17 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_next, i32 %17)
  %18 = load i32, i32* %call17, align 4
  store i32 %18, i32* %index, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  %19 = load i32, i32* %index, align 4
  %cmp18 = icmp eq i32 %19, -1
  br i1 %cmp18, label %if.then19, label %if.end20

if.then19:                                        ; preds = %while.end
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %retval, align 4
  br label %return

if.end20:                                         ; preds = %while.end
  %m_overlappingPairArray21 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %20 = load i32, i32* %index, align 4
  %call22 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray21, i32 %20)
  store %struct.btBroadphasePair* %call22, %struct.btBroadphasePair** %retval, align 4
  br label %return

return:                                           ; preds = %if.end20, %if.then19, %if.then8
  %21 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %retval, align 4
  ret %struct.btBroadphasePair* %21
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z6btSwapIP17btBroadphaseProxyEvRT_S3_(%struct.btBroadphaseProxy** nonnull align 4 dereferenceable(4) %a, %struct.btBroadphaseProxy** nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca %struct.btBroadphaseProxy**, align 4
  %b.addr = alloca %struct.btBroadphaseProxy**, align 4
  %tmp = alloca %struct.btBroadphaseProxy*, align 4
  store %struct.btBroadphaseProxy** %a, %struct.btBroadphaseProxy*** %a.addr, align 4
  store %struct.btBroadphaseProxy** %b, %struct.btBroadphaseProxy*** %b.addr, align 4
  %0 = load %struct.btBroadphaseProxy**, %struct.btBroadphaseProxy*** %a.addr, align 4
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %0, align 4
  store %struct.btBroadphaseProxy* %1, %struct.btBroadphaseProxy** %tmp, align 4
  %2 = load %struct.btBroadphaseProxy**, %struct.btBroadphaseProxy*** %b.addr, align 4
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %2, align 4
  %4 = load %struct.btBroadphaseProxy**, %struct.btBroadphaseProxy*** %a.addr, align 4
  store %struct.btBroadphaseProxy* %3, %struct.btBroadphaseProxy** %4, align 4
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %tmp, align 4
  %6 = load %struct.btBroadphaseProxy**, %struct.btBroadphaseProxy*** %b.addr, align 4
  store %struct.btBroadphaseProxy* %5, %struct.btBroadphaseProxy** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseProxy*, align 4
  store %struct.btBroadphaseProxy* %this, %struct.btBroadphaseProxy** %this.addr, align 4
  %this1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %this.addr, align 4
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_uniqueId, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN28btHashedOverlappingPairCache7getHashEjj(%class.btHashedOverlappingPairCache* %this, i32 %proxyId1, i32 %proxyId2) #1 comdat {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %proxyId1.addr = alloca i32, align 4
  %proxyId2.addr = alloca i32, align 4
  %key = alloca i32, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  store i32 %proxyId1, i32* %proxyId1.addr, align 4
  store i32 %proxyId2, i32* %proxyId2.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load i32, i32* %proxyId1.addr, align 4
  %1 = load i32, i32* %proxyId2.addr, align 4
  %shl = shl i32 %1, 16
  %or = or i32 %0, %shl
  store i32 %or, i32* %key, align 4
  %2 = load i32, i32* %key, align 4
  %shl2 = shl i32 %2, 15
  %neg = xor i32 %shl2, -1
  %3 = load i32, i32* %key, align 4
  %add = add nsw i32 %3, %neg
  store i32 %add, i32* %key, align 4
  %4 = load i32, i32* %key, align 4
  %shr = ashr i32 %4, 10
  %5 = load i32, i32* %key, align 4
  %xor = xor i32 %5, %shr
  store i32 %xor, i32* %key, align 4
  %6 = load i32, i32* %key, align 4
  %shl3 = shl i32 %6, 3
  %7 = load i32, i32* %key, align 4
  %add4 = add nsw i32 %7, %shl3
  store i32 %add4, i32* %key, align 4
  %8 = load i32, i32* %key, align 4
  %shr5 = ashr i32 %8, 6
  %9 = load i32, i32* %key, align 4
  %xor6 = xor i32 %9, %shr5
  store i32 %xor6, i32* %key, align 4
  %10 = load i32, i32* %key, align 4
  %shl7 = shl i32 %10, 11
  %neg8 = xor i32 %shl7, -1
  %11 = load i32, i32* %key, align 4
  %add9 = add nsw i32 %11, %neg8
  store i32 %add9, i32* %key, align 4
  %12 = load i32, i32* %key, align 4
  %shr10 = ashr i32 %12, 16
  %13 = load i32, i32* %key, align 4
  %xor11 = xor i32 %13, %shr10
  store i32 %xor11, i32* %key, align 4
  %14 = load i32, i32* %key, align 4
  ret i32 %14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.1* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN28btHashedOverlappingPairCache10equalsPairERK16btBroadphasePairii(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %pair, i32 %proxyId1, i32 %proxyId2) #1 comdat {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %pair.addr = alloca %struct.btBroadphasePair*, align 4
  %proxyId1.addr = alloca i32, align 4
  %proxyId2.addr = alloca i32, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  store %struct.btBroadphasePair* %pair, %struct.btBroadphasePair** %pair.addr, align 4
  store i32 %proxyId1, i32* %proxyId1.addr, align 4
  store i32 %proxyId2, i32* %proxyId2.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %call = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %1)
  %2 = load i32, i32* %proxyId1.addr, align 4
  %cmp = icmp eq i32 %call, %2
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 0, i32 1
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %call2 = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %4)
  %5 = load i32, i32* %proxyId2.addr, align 4
  %cmp3 = icmp eq i32 %call2, %5
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %6 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 %1
  ret %struct.btBroadphasePair* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.1* %this, i32 %newsize, i32* nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i32*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store i32* %fillData, i32** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %5 = load i32*, i32** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.1* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %14 = load i32*, i32** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds i32, i32* %14, i32 %15
  %16 = bitcast i32* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to i32*
  %18 = load i32*, i32** %fillData.addr, align 4
  %19 = load i32, i32* %18, align 4
  store i32 %19, i32* %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden %struct.btBroadphasePair* @_ZN28btHashedOverlappingPairCache15internalAddPairEP17btBroadphaseProxyS1_(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) #2 {
entry:
  %retval = alloca %struct.btBroadphasePair*, align 4
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxyId1 = alloca i32, align 4
  %proxyId2 = alloca i32, align 4
  %hash = alloca i32, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  %count = alloca i32, align 4
  %oldCapacity = alloca i32, align 4
  %mem = alloca i8*, align 4
  %newCapacity = alloca i32, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %0, i32 0, i32 3
  %1 = load i32, i32* %m_uniqueId, align 4
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %m_uniqueId2 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %2, i32 0, i32 3
  %3 = load i32, i32* %m_uniqueId2, align 4
  %cmp = icmp sgt i32 %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_Z6btSwapIP17btBroadphaseProxyEvRT_S3_(%struct.btBroadphaseProxy** nonnull align 4 dereferenceable(4) %proxy0.addr, %struct.btBroadphaseProxy** nonnull align 4 dereferenceable(4) %proxy1.addr)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %call = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %4)
  store i32 %call, i32* %proxyId1, align 4
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %call3 = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %5)
  store i32 %call3, i32* %proxyId2, align 4
  %6 = load i32, i32* %proxyId1, align 4
  %7 = load i32, i32* %proxyId2, align 4
  %call4 = call i32 @_ZN28btHashedOverlappingPairCache7getHashEjj(%class.btHashedOverlappingPairCache* %this1, i32 %6, i32 %7)
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %sub = sub nsw i32 %call5, 1
  %and = and i32 %call4, %sub
  store i32 %and, i32* %hash, align 4
  %8 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %9 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %10 = load i32, i32* %hash, align 4
  %call6 = call %struct.btBroadphasePair* @_ZN28btHashedOverlappingPairCache16internalFindPairEP17btBroadphaseProxyS1_i(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphaseProxy* %8, %struct.btBroadphaseProxy* %9, i32 %10)
  store %struct.btBroadphasePair* %call6, %struct.btBroadphasePair** %pair, align 4
  %11 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %cmp7 = icmp ne %struct.btBroadphasePair* %11, null
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end
  %12 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  store %struct.btBroadphasePair* %12, %struct.btBroadphasePair** %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end
  %m_overlappingPairArray10 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call11 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray10)
  store i32 %call11, i32* %count, align 4
  %m_overlappingPairArray12 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call13 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray12)
  store i32 %call13, i32* %oldCapacity, align 4
  %m_overlappingPairArray14 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call15 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairE21expandNonInitializingEv(%class.btAlignedObjectArray* %m_overlappingPairArray14)
  %13 = bitcast %struct.btBroadphasePair* %call15 to i8*
  store i8* %13, i8** %mem, align 4
  %m_ghostPairCallback = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %14 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_ghostPairCallback, align 4
  %tobool = icmp ne %class.btOverlappingPairCallback* %14, null
  br i1 %tobool, label %if.then16, label %if.end19

if.then16:                                        ; preds = %if.end9
  %m_ghostPairCallback17 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %15 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_ghostPairCallback17, align 4
  %16 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %17 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %18 = bitcast %class.btOverlappingPairCallback* %15 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %18, align 4
  %vfn = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %19 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call18 = call %struct.btBroadphasePair* %19(%class.btOverlappingPairCallback* %15, %struct.btBroadphaseProxy* %16, %struct.btBroadphaseProxy* %17)
  br label %if.end19

if.end19:                                         ; preds = %if.then16, %if.end9
  %m_overlappingPairArray20 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call21 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray20)
  store i32 %call21, i32* %newCapacity, align 4
  %20 = load i32, i32* %oldCapacity, align 4
  %21 = load i32, i32* %newCapacity, align 4
  %cmp22 = icmp slt i32 %20, %21
  br i1 %cmp22, label %if.then23, label %if.end29

if.then23:                                        ; preds = %if.end19
  call void @_ZN28btHashedOverlappingPairCache10growTablesEv(%class.btHashedOverlappingPairCache* %this1)
  %22 = load i32, i32* %proxyId1, align 4
  %23 = load i32, i32* %proxyId2, align 4
  %call24 = call i32 @_ZN28btHashedOverlappingPairCache7getHashEjj(%class.btHashedOverlappingPairCache* %this1, i32 %22, i32 %23)
  %m_overlappingPairArray25 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call26 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray25)
  %sub27 = sub nsw i32 %call26, 1
  %and28 = and i32 %call24, %sub27
  store i32 %and28, i32* %hash, align 4
  br label %if.end29

if.end29:                                         ; preds = %if.then23, %if.end19
  %24 = load i8*, i8** %mem, align 4
  %call30 = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %24)
  %25 = bitcast i8* %call30 to %struct.btBroadphasePair*
  %26 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %27 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %call31 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ER17btBroadphaseProxyS1_(%struct.btBroadphasePair* %25, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %26, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %27)
  store %struct.btBroadphasePair* %25, %struct.btBroadphasePair** %pair, align 4
  %28 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %28, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm, align 4
  %29 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %30 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %29, i32 0, i32 3
  %m_internalTmpValue = bitcast %union.anon.0* %30 to i32*
  store i32 0, i32* %m_internalTmpValue, align 4
  %m_hashTable = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  %31 = load i32, i32* %hash, align 4
  %call32 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_hashTable, i32 %31)
  %32 = load i32, i32* %call32, align 4
  %m_next = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 4
  %33 = load i32, i32* %count, align 4
  %call33 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_next, i32 %33)
  store i32 %32, i32* %call33, align 4
  %34 = load i32, i32* %count, align 4
  %m_hashTable34 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  %35 = load i32, i32* %hash, align 4
  %call35 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_hashTable34, i32 %35)
  store i32 %34, i32* %call35, align 4
  %36 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  store %struct.btBroadphasePair* %36, %struct.btBroadphasePair** %retval, align 4
  br label %return

return:                                           ; preds = %if.end29, %if.then8
  %37 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %retval, align 4
  ret %struct.btBroadphasePair* %37
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN28btHashedOverlappingPairCache16internalFindPairEP17btBroadphaseProxyS1_i(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1, i32 %hash) #2 comdat {
entry:
  %retval = alloca %struct.btBroadphasePair*, align 4
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %hash.addr = alloca i32, align 4
  %proxyId1 = alloca i32, align 4
  %proxyId2 = alloca i32, align 4
  %index = alloca i32, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  store i32 %hash, i32* %hash.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %call = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %0)
  store i32 %call, i32* %proxyId1, align 4
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %call2 = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %1)
  store i32 %call2, i32* %proxyId2, align 4
  %m_hashTable = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  %2 = load i32, i32* %hash.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_hashTable, i32 %2)
  %3 = load i32, i32* %call3, align 4
  store i32 %3, i32* %index, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %4 = load i32, i32* %index, align 4
  %cmp = icmp ne i32 %4, -1
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %5 = load i32, i32* %index, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray, i32 %5)
  %6 = load i32, i32* %proxyId1, align 4
  %7 = load i32, i32* %proxyId2, align 4
  %call5 = call zeroext i1 @_ZN28btHashedOverlappingPairCache10equalsPairERK16btBroadphasePairii(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %call4, i32 %6, i32 %7)
  %conv = zext i1 %call5 to i32
  %cmp6 = icmp eq i32 %conv, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %8 = phi i1 [ false, %while.cond ], [ %cmp6, %land.rhs ]
  br i1 %8, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %m_next = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 4
  %9 = load i32, i32* %index, align 4
  %call7 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_next, i32 %9)
  %10 = load i32, i32* %call7, align 4
  store i32 %10, i32* %index, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  %11 = load i32, i32* %index, align 4
  %cmp8 = icmp eq i32 %11, -1
  br i1 %cmp8, label %if.then, label %if.end

if.then:                                          ; preds = %while.end
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %retval, align 4
  br label %return

if.end:                                           ; preds = %while.end
  %m_overlappingPairArray9 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %12 = load i32, i32* %index, align 4
  %call10 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray9, i32 %12)
  store %struct.btBroadphasePair* %call10, %struct.btBroadphasePair** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %13 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %retval, align 4
  ret %struct.btBroadphasePair* %13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairE21expandNonInitializingEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI16btBroadphasePairE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %1 = load i32, i32* %m_size, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %3 = load i32, i32* %sz, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 %3
  ret %struct.btBroadphasePair* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN16btBroadphasePairnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ER17btBroadphaseProxyS1_(%struct.btBroadphasePair* returned %this, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %proxy0, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %proxy1) unnamed_addr #1 comdat {
entry:
  %retval = alloca %struct.btBroadphasePair*, align 4
  %this.addr = alloca %struct.btBroadphasePair*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  store %struct.btBroadphasePair* %this, %struct.btBroadphasePair** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %this1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %this.addr, align 4
  store %struct.btBroadphasePair* %this1, %struct.btBroadphasePair** %retval, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %0, i32 0, i32 3
  %1 = load i32, i32* %m_uniqueId, align 4
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %m_uniqueId2 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %2, i32 0, i32 3
  %3 = load i32, i32* %m_uniqueId2, align 4
  %cmp = icmp slt i32 %1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 0
  store %struct.btBroadphaseProxy* %4, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 1
  store %struct.btBroadphaseProxy* %5, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %m_pProxy03 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 0
  store %struct.btBroadphaseProxy* %6, %struct.btBroadphaseProxy** %m_pProxy03, align 4
  %7 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %m_pProxy14 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 1
  store %struct.btBroadphaseProxy* %7, %struct.btBroadphaseProxy** %m_pProxy14, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm, align 4
  %8 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon.0* %8 to i8**
  store i8* null, i8** %m_internalInfo1, align 4
  %9 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %retval, align 4
  ret %struct.btBroadphasePair* %9
}

; Function Attrs: noinline optnone
define hidden i8* @_ZN28btHashedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %proxyId1 = alloca i32, align 4
  %proxyId2 = alloca i32, align 4
  %hash = alloca i32, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  %userData = alloca i8*, align 4
  %pairIndex = alloca i32, align 4
  %index = alloca i32, align 4
  %previous = alloca i32, align 4
  %lastPairIndex = alloca i32, align 4
  %last = alloca %struct.btBroadphasePair*, align 4
  %lastHash = alloca i32, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load i32, i32* @gRemovePairs, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* @gRemovePairs, align 4
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %1, i32 0, i32 3
  %2 = load i32, i32* %m_uniqueId, align 4
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %m_uniqueId2 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %3, i32 0, i32 3
  %4 = load i32, i32* %m_uniqueId2, align 4
  %cmp = icmp sgt i32 %2, %4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_Z6btSwapIP17btBroadphaseProxyEvRT_S3_(%struct.btBroadphaseProxy** nonnull align 4 dereferenceable(4) %proxy0.addr, %struct.btBroadphaseProxy** nonnull align 4 dereferenceable(4) %proxy1.addr)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %call = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %5)
  store i32 %call, i32* %proxyId1, align 4
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %call3 = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %6)
  store i32 %call3, i32* %proxyId2, align 4
  %7 = load i32, i32* %proxyId1, align 4
  %8 = load i32, i32* %proxyId2, align 4
  %call4 = call i32 @_ZN28btHashedOverlappingPairCache7getHashEjj(%class.btHashedOverlappingPairCache* %this1, i32 %7, i32 %8)
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %sub = sub nsw i32 %call5, 1
  %and = and i32 %call4, %sub
  store i32 %and, i32* %hash, align 4
  %9 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %10 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %11 = load i32, i32* %hash, align 4
  %call6 = call %struct.btBroadphasePair* @_ZN28btHashedOverlappingPairCache16internalFindPairEP17btBroadphaseProxyS1_i(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphaseProxy* %9, %struct.btBroadphaseProxy* %10, i32 %11)
  store %struct.btBroadphasePair* %call6, %struct.btBroadphasePair** %pair, align 4
  %12 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %cmp7 = icmp eq %struct.btBroadphasePair* %12, null
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end
  store i8* null, i8** %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end
  %13 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %14 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %15 = bitcast %class.btHashedOverlappingPairCache* %this1 to void (%class.btHashedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)***
  %vtable = load void (%class.btHashedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)**, void (%class.btHashedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*** %15, align 4
  %vfn = getelementptr inbounds void (%class.btHashedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btHashedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vtable, i64 8
  %16 = load void (%class.btHashedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btHashedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vfn, align 4
  call void %16(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %13, %class.btDispatcher* %14)
  %17 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %18 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %17, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon.0* %18 to i8**
  %19 = load i8*, i8** %m_internalInfo1, align 4
  store i8* %19, i8** %userData, align 4
  %20 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_overlappingPairArray10 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call11 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray10, i32 0)
  %sub.ptr.lhs.cast = ptrtoint %struct.btBroadphasePair* %20 to i32
  %sub.ptr.rhs.cast = ptrtoint %struct.btBroadphasePair* %call11 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 16
  store i32 %sub.ptr.div, i32* %pairIndex, align 4
  %m_hashTable = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  %21 = load i32, i32* %hash, align 4
  %call12 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_hashTable, i32 %21)
  %22 = load i32, i32* %call12, align 4
  store i32 %22, i32* %index, align 4
  store i32 -1, i32* %previous, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end9
  %23 = load i32, i32* %index, align 4
  %24 = load i32, i32* %pairIndex, align 4
  %cmp13 = icmp ne i32 %23, %24
  br i1 %cmp13, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %25 = load i32, i32* %index, align 4
  store i32 %25, i32* %previous, align 4
  %m_next = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 4
  %26 = load i32, i32* %index, align 4
  %call14 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_next, i32 %26)
  %27 = load i32, i32* %call14, align 4
  store i32 %27, i32* %index, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %28 = load i32, i32* %previous, align 4
  %cmp15 = icmp ne i32 %28, -1
  br i1 %cmp15, label %if.then16, label %if.else

if.then16:                                        ; preds = %while.end
  %m_next17 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 4
  %29 = load i32, i32* %pairIndex, align 4
  %call18 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_next17, i32 %29)
  %30 = load i32, i32* %call18, align 4
  %m_next19 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 4
  %31 = load i32, i32* %previous, align 4
  %call20 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_next19, i32 %31)
  store i32 %30, i32* %call20, align 4
  br label %if.end25

if.else:                                          ; preds = %while.end
  %m_next21 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 4
  %32 = load i32, i32* %pairIndex, align 4
  %call22 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_next21, i32 %32)
  %33 = load i32, i32* %call22, align 4
  %m_hashTable23 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  %34 = load i32, i32* %hash, align 4
  %call24 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_hashTable23, i32 %34)
  store i32 %33, i32* %call24, align 4
  br label %if.end25

if.end25:                                         ; preds = %if.else, %if.then16
  %m_overlappingPairArray26 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call27 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray26)
  %sub28 = sub nsw i32 %call27, 1
  store i32 %sub28, i32* %lastPairIndex, align 4
  %m_ghostPairCallback = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %35 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_ghostPairCallback, align 4
  %tobool = icmp ne %class.btOverlappingPairCallback* %35, null
  br i1 %tobool, label %if.then29, label %if.end34

if.then29:                                        ; preds = %if.end25
  %m_ghostPairCallback30 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  %36 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_ghostPairCallback30, align 4
  %37 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %38 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %39 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %40 = bitcast %class.btOverlappingPairCallback* %36 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable31 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %40, align 4
  %vfn32 = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable31, i64 3
  %41 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn32, align 4
  %call33 = call i8* %41(%class.btOverlappingPairCallback* %36, %struct.btBroadphaseProxy* %37, %struct.btBroadphaseProxy* %38, %class.btDispatcher* %39)
  br label %if.end34

if.end34:                                         ; preds = %if.then29, %if.end25
  %42 = load i32, i32* %lastPairIndex, align 4
  %43 = load i32, i32* %pairIndex, align 4
  %cmp35 = icmp eq i32 %42, %43
  br i1 %cmp35, label %if.then36, label %if.end38

if.then36:                                        ; preds = %if.end34
  %m_overlappingPairArray37 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE8pop_backEv(%class.btAlignedObjectArray* %m_overlappingPairArray37)
  %44 = load i8*, i8** %userData, align 4
  store i8* %44, i8** %retval, align 4
  br label %return

if.end38:                                         ; preds = %if.end34
  %m_overlappingPairArray39 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %45 = load i32, i32* %lastPairIndex, align 4
  %call40 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray39, i32 %45)
  store %struct.btBroadphasePair* %call40, %struct.btBroadphasePair** %last, align 4
  %46 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %last, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %46, i32 0, i32 0
  %47 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %call41 = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %47)
  %48 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %last, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %48, i32 0, i32 1
  %49 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %call42 = call i32 @_ZNK17btBroadphaseProxy6getUidEv(%struct.btBroadphaseProxy* %49)
  %call43 = call i32 @_ZN28btHashedOverlappingPairCache7getHashEjj(%class.btHashedOverlappingPairCache* %this1, i32 %call41, i32 %call42)
  %m_overlappingPairArray44 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call45 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray44)
  %sub46 = sub nsw i32 %call45, 1
  %and47 = and i32 %call43, %sub46
  store i32 %and47, i32* %lastHash, align 4
  %m_hashTable48 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  %50 = load i32, i32* %lastHash, align 4
  %call49 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_hashTable48, i32 %50)
  %51 = load i32, i32* %call49, align 4
  store i32 %51, i32* %index, align 4
  store i32 -1, i32* %previous, align 4
  br label %while.cond50

while.cond50:                                     ; preds = %while.body52, %if.end38
  %52 = load i32, i32* %index, align 4
  %53 = load i32, i32* %lastPairIndex, align 4
  %cmp51 = icmp ne i32 %52, %53
  br i1 %cmp51, label %while.body52, label %while.end55

while.body52:                                     ; preds = %while.cond50
  %54 = load i32, i32* %index, align 4
  store i32 %54, i32* %previous, align 4
  %m_next53 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 4
  %55 = load i32, i32* %index, align 4
  %call54 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_next53, i32 %55)
  %56 = load i32, i32* %call54, align 4
  store i32 %56, i32* %index, align 4
  br label %while.cond50

while.end55:                                      ; preds = %while.cond50
  %57 = load i32, i32* %previous, align 4
  %cmp56 = icmp ne i32 %57, -1
  br i1 %cmp56, label %if.then57, label %if.else62

if.then57:                                        ; preds = %while.end55
  %m_next58 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 4
  %58 = load i32, i32* %lastPairIndex, align 4
  %call59 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_next58, i32 %58)
  %59 = load i32, i32* %call59, align 4
  %m_next60 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 4
  %60 = load i32, i32* %previous, align 4
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_next60, i32 %60)
  store i32 %59, i32* %call61, align 4
  br label %if.end67

if.else62:                                        ; preds = %while.end55
  %m_next63 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 4
  %61 = load i32, i32* %lastPairIndex, align 4
  %call64 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_next63, i32 %61)
  %62 = load i32, i32* %call64, align 4
  %m_hashTable65 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  %63 = load i32, i32* %lastHash, align 4
  %call66 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_hashTable65, i32 %63)
  store i32 %62, i32* %call66, align 4
  br label %if.end67

if.end67:                                         ; preds = %if.else62, %if.then57
  %m_overlappingPairArray68 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %64 = load i32, i32* %lastPairIndex, align 4
  %call69 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray68, i32 %64)
  %m_overlappingPairArray70 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %65 = load i32, i32* %pairIndex, align 4
  %call71 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray70, i32 %65)
  %66 = bitcast %struct.btBroadphasePair* %call71 to i8*
  %67 = bitcast %struct.btBroadphasePair* %call69 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %66, i8* align 4 %67, i32 16, i1 false)
  %m_hashTable72 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  %68 = load i32, i32* %lastHash, align 4
  %call73 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_hashTable72, i32 %68)
  %69 = load i32, i32* %call73, align 4
  %m_next74 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 4
  %70 = load i32, i32* %pairIndex, align 4
  %call75 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_next74, i32 %70)
  store i32 %69, i32* %call75, align 4
  %71 = load i32, i32* %pairIndex, align 4
  %m_hashTable76 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 3
  %72 = load i32, i32* %lastHash, align 4
  %call77 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_hashTable76, i32 %72)
  store i32 %71, i32* %call77, align 4
  %m_overlappingPairArray78 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE8pop_backEv(%class.btAlignedObjectArray* %m_overlappingPairArray78)
  %73 = load i8*, i8** %userData, align 4
  store i8* %73, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end67, %if.then36, %if.then8
  %74 = load i8*, i8** %retval, align 4
  ret i8* %74
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE8pop_backEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %1, i32 %2
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline optnone
define hidden void @_ZN28btHashedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher(%class.btHashedOverlappingPairCache* %this, %struct.btOverlapCallback* %callback, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %callback.addr = alloca %struct.btOverlapCallback*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %i = alloca i32, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  store %struct.btOverlapCallback* %callback, %struct.btOverlapCallback** %callback.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([57 x i8], [57 x i8]* @.str, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %if.end, %entry
  %0 = load i32, i32* %i, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_overlappingPairArray3 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %1 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray3, i32 %1)
  store %struct.btBroadphasePair* %call4, %struct.btBroadphasePair** %pair, align 4
  %2 = load %struct.btOverlapCallback*, %struct.btOverlapCallback** %callback.addr, align 4
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %4 = bitcast %struct.btOverlapCallback* %2 to i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)***
  %vtable = load i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)**, i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)*** %4, align 4
  %vfn = getelementptr inbounds i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)*, i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)** %vtable, i64 2
  %5 = load i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)*, i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)** %vfn, align 4
  %call5 = call zeroext i1 %5(%struct.btOverlapCallback* %2, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %3)
  br i1 %call5, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %6 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %6, i32 0, i32 0
  %7 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %8 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %8, i32 0, i32 1
  %9 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %10 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %11 = bitcast %class.btHashedOverlappingPairCache* %this1 to i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable6 = load i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %11, align 4
  %vfn7 = getelementptr inbounds i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable6, i64 3
  %12 = load i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn7, align 4
  %call8 = call i8* %12(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphaseProxy* %7, %struct.btBroadphaseProxy* %9, %class.btDispatcher* %10)
  %13 = load i32, i32* @gOverlappingPairs, align 4
  %dec = add nsw i32 %13, -1
  store i32 %dec, i32* @gOverlappingPairs, align 4
  br label %if.end

if.else:                                          ; preds = %for.body
  %14 = load i32, i32* %i, align 4
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call9 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #8
  ret void
}

declare %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* returned, i8*) unnamed_addr #4

; Function Attrs: nounwind
declare %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* returned) unnamed_addr #5

; Function Attrs: noinline optnone
define hidden void @_ZN28btHashedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher(%class.btHashedOverlappingPairCache* %this, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %tmpPairs = alloca %class.btAlignedObjectArray, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btBroadphasePairSortPredicate, align 1
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI16btBroadphasePairEC2Ev(%class.btAlignedObjectArray* %tmpPairs)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_overlappingPairArray3 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %1 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray3, i32 %1)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9push_backERKS0_(%class.btAlignedObjectArray* %tmpPairs, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %2 = load i32, i32* %i, align 4
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc12, %for.end
  %3 = load i32, i32* %i, align 4
  %call6 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %tmpPairs)
  %cmp7 = icmp slt i32 %3, %call6
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond5
  %4 = load i32, i32* %i, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %tmpPairs, i32 %4)
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %call9, i32 0, i32 0
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %6 = load i32, i32* %i, align 4
  %call10 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %tmpPairs, i32 %6)
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %call10, i32 0, i32 1
  %7 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %8 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %9 = bitcast %class.btHashedOverlappingPairCache* %this1 to i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable = load i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %9, align 4
  %vfn = getelementptr inbounds i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable, i64 3
  %10 = load i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn, align 4
  %call11 = call i8* %10(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphaseProxy* %5, %struct.btBroadphaseProxy* %7, %class.btDispatcher* %8)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %11 = load i32, i32* %i, align 4
  %inc13 = add nsw i32 %11, 1
  store i32 %inc13, i32* %i, align 4
  br label %for.cond5

for.end14:                                        ; preds = %for.cond5
  store i32 0, i32* %i, align 4
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc21, %for.end14
  %12 = load i32, i32* %i, align 4
  %m_next = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 4
  %call16 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.1* %m_next)
  %cmp17 = icmp slt i32 %12, %call16
  br i1 %cmp17, label %for.body18, label %for.end23

for.body18:                                       ; preds = %for.cond15
  %m_next19 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 4
  %13 = load i32, i32* %i, align 4
  %call20 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.1* %m_next19, i32 %13)
  store i32 -1, i32* %call20, align 4
  br label %for.inc21

for.inc21:                                        ; preds = %for.body18
  %14 = load i32, i32* %i, align 4
  %inc22 = add nsw i32 %14, 1
  store i32 %inc22, i32* %i, align 4
  br label %for.cond15

for.end23:                                        ; preds = %for.cond15
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray* %tmpPairs, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp)
  store i32 0, i32* %i, align 4
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc35, %for.end23
  %15 = load i32, i32* %i, align 4
  %call25 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %tmpPairs)
  %cmp26 = icmp slt i32 %15, %call25
  br i1 %cmp26, label %for.body27, label %for.end37

for.body27:                                       ; preds = %for.cond24
  %16 = load i32, i32* %i, align 4
  %call28 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %tmpPairs, i32 %16)
  %m_pProxy029 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %call28, i32 0, i32 0
  %17 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy029, align 4
  %18 = load i32, i32* %i, align 4
  %call30 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %tmpPairs, i32 %18)
  %m_pProxy131 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %call30, i32 0, i32 1
  %19 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy131, align 4
  %20 = bitcast %class.btHashedOverlappingPairCache* %this1 to %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable32 = load %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %20, align 4
  %vfn33 = getelementptr inbounds %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable32, i64 2
  %21 = load %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn33, align 4
  %call34 = call %struct.btBroadphasePair* %21(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphaseProxy* %17, %struct.btBroadphaseProxy* %19)
  br label %for.inc35

for.inc35:                                        ; preds = %for.body27
  %22 = load i32, i32* %i, align 4
  %inc36 = add nsw i32 %22, 1
  store i32 %inc36, i32* %i, align 4
  br label %for.cond24

for.end37:                                        ; preds = %for.cond24
  %call38 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI16btBroadphasePairED2Ev(%class.btAlignedObjectArray* %tmpPairs) #8
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9push_backERKS0_(%class.btAlignedObjectArray* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %struct.btBroadphasePair*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %struct.btBroadphasePair* %_Val, %struct.btBroadphasePair** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI16btBroadphasePairE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %1, i32 %2
  %3 = bitcast %struct.btBroadphasePair* %arrayidx to i8*
  %call5 = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %3)
  %4 = bitcast i8* %call5 to %struct.btBroadphasePair*
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %_Val.addr, align 4
  %call6 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %4, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %5)
  %m_size7 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %6 = load i32, i32* %m_size7, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %m_size7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray* %this, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %CompareFunc.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btBroadphasePairSortPredicate* %CompareFunc, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden i8* @_ZN28btSortedOverlappingPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher(%class.btSortedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %findPair = alloca %struct.btBroadphasePair, align 4
  %findIndex = alloca i32, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  %userData = alloca i8*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.btSortedOverlappingPairCache* %this1 to i1 (%class.btSortedOverlappingPairCache*)***
  %vtable = load i1 (%class.btSortedOverlappingPairCache*)**, i1 (%class.btSortedOverlappingPairCache*)*** %0, align 4
  %vfn = getelementptr inbounds i1 (%class.btSortedOverlappingPairCache*)*, i1 (%class.btSortedOverlappingPairCache*)** %vtable, i64 14
  %1 = load i1 (%class.btSortedOverlappingPairCache*)*, i1 (%class.btSortedOverlappingPairCache*)** %vfn, align 4
  %call = call zeroext i1 %1(%class.btSortedOverlappingPairCache* %this1)
  br i1 %call, label %if.end21, label %if.then

if.then:                                          ; preds = %entry
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %call2 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ER17btBroadphaseProxyS1_(%struct.btBroadphasePair* %findPair, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %2, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %3)
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE16findLinearSearchERKS0_(%class.btAlignedObjectArray* %m_overlappingPairArray, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %findPair)
  store i32 %call3, i32* %findIndex, align 4
  %4 = load i32, i32* %findIndex, align 4
  %m_overlappingPairArray4 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray4)
  %cmp = icmp slt i32 %4, %call5
  br i1 %cmp, label %if.then6, label %if.end20

if.then6:                                         ; preds = %if.then
  %5 = load i32, i32* @gOverlappingPairs, align 4
  %dec = add nsw i32 %5, -1
  store i32 %dec, i32* @gOverlappingPairs, align 4
  %m_overlappingPairArray7 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %6 = load i32, i32* %findIndex, align 4
  %call8 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray7, i32 %6)
  store %struct.btBroadphasePair* %call8, %struct.btBroadphasePair** %pair, align 4
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %8 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon.0* %8 to i8**
  %9 = load i8*, i8** %m_internalInfo1, align 4
  store i8* %9, i8** %userData, align 4
  %10 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %11 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %12 = bitcast %class.btSortedOverlappingPairCache* %this1 to void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)***
  %vtable9 = load void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)**, void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*** %12, align 4
  %vfn10 = getelementptr inbounds void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vtable9, i64 8
  %13 = load void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vfn10, align 4
  call void %13(%class.btSortedOverlappingPairCache* %this1, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %10, %class.btDispatcher* %11)
  %m_ghostPairCallback = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 5
  %14 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_ghostPairCallback, align 4
  %tobool = icmp ne %class.btOverlappingPairCallback* %14, null
  br i1 %tobool, label %if.then11, label %if.end

if.then11:                                        ; preds = %if.then6
  %m_ghostPairCallback12 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 5
  %15 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_ghostPairCallback12, align 4
  %16 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %17 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %18 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %19 = bitcast %class.btOverlappingPairCallback* %15 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable13 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %19, align 4
  %vfn14 = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable13, i64 3
  %20 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn14, align 4
  %call15 = call i8* %20(%class.btOverlappingPairCallback* %15, %struct.btBroadphaseProxy* %16, %struct.btBroadphaseProxy* %17, %class.btDispatcher* %18)
  br label %if.end

if.end:                                           ; preds = %if.then11, %if.then6
  %m_overlappingPairArray16 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %21 = load i32, i32* %findIndex, align 4
  %m_overlappingPairArray17 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call18 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %m_overlappingPairArray17)
  %sub = sub nsw i32 %call18, 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray* %m_overlappingPairArray16, i32 %21, i32 %sub)
  %m_overlappingPairArray19 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE8pop_backEv(%class.btAlignedObjectArray* %m_overlappingPairArray19)
  %22 = load i8*, i8** %userData, align 4
  store i8* %22, i8** %retval, align 4
  br label %return

if.end20:                                         ; preds = %if.then
  br label %if.end21

if.end21:                                         ; preds = %if.end20, %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end21, %if.end
  %23 = load i8*, i8** %retval, align 4
  ret i8* %23
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE16findLinearSearchERKS0_(%class.btAlignedObjectArray* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %key) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %key.addr = alloca %struct.btBroadphasePair*, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %struct.btBroadphasePair* %key, %struct.btBroadphasePair** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %index, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %1, i32 %2
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %key.addr, align 4
  %call3 = call zeroext i1 @_ZeqRK16btBroadphasePairS1_(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %3)
  br i1 %call3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  store i32 %4, i32* %index, align 4
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %6 = load i32, i32* %index, align 4
  ret i32 %6
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray* %this, i32 %index0, i32 %index1) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %struct.btBroadphasePair, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 %1
  %call = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %temp, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx)
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data2, align 4
  %3 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 %3
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data4, align 4
  %5 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 %5
  %6 = bitcast %struct.btBroadphasePair* %arrayidx5 to i8*
  %7 = bitcast %struct.btBroadphasePair* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data6, align 4
  %9 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %8, i32 %9
  %10 = bitcast %struct.btBroadphasePair* %arrayidx7 to i8*
  %11 = bitcast %struct.btBroadphasePair* %temp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define hidden %struct.btBroadphasePair* @_ZN28btSortedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_(%class.btSortedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) unnamed_addr #2 {
entry:
  %retval = alloca %struct.btBroadphasePair*, align 4
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %mem = alloca i8*, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %call = call zeroext i1 @_ZNK28btSortedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_(%class.btSortedOverlappingPairCache* %this1, %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy* %1)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call2 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairE21expandNonInitializingEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %2 = bitcast %struct.btBroadphasePair* %call2 to i8*
  store i8* %2, i8** %mem, align 4
  %3 = load i8*, i8** %mem, align 4
  %call3 = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %3)
  %4 = bitcast i8* %call3 to %struct.btBroadphasePair*
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %call4 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ER17btBroadphaseProxyS1_(%struct.btBroadphasePair* %4, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %5, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %6)
  store %struct.btBroadphasePair* %4, %struct.btBroadphasePair** %pair, align 4
  %7 = load i32, i32* @gOverlappingPairs, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* @gOverlappingPairs, align 4
  %8 = load i32, i32* @gAddedPairs, align 4
  %inc5 = add nsw i32 %8, 1
  store i32 %inc5, i32* @gAddedPairs, align 4
  %m_ghostPairCallback = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 5
  %9 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_ghostPairCallback, align 4
  %tobool = icmp ne %class.btOverlappingPairCallback* %9, null
  br i1 %tobool, label %if.then6, label %if.end9

if.then6:                                         ; preds = %if.end
  %m_ghostPairCallback7 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 5
  %10 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_ghostPairCallback7, align 4
  %11 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %12 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %13 = bitcast %class.btOverlappingPairCallback* %10 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %13, align 4
  %vfn = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %14 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call8 = call %struct.btBroadphasePair* %14(%class.btOverlappingPairCallback* %10, %struct.btBroadphaseProxy* %11, %struct.btBroadphaseProxy* %12)
  br label %if.end9

if.end9:                                          ; preds = %if.then6, %if.end
  %15 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  store %struct.btBroadphasePair* %15, %struct.btBroadphasePair** %retval, align 4
  br label %return

return:                                           ; preds = %if.end9, %if.then
  %16 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %retval, align 4
  ret %struct.btBroadphasePair* %16
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK28btSortedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_(%class.btSortedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %collides = alloca i8, align 1
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %m_overlapFilterCallback = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 4
  %0 = load %struct.btOverlapFilterCallback*, %struct.btOverlapFilterCallback** %m_overlapFilterCallback, align 4
  %tobool = icmp ne %struct.btOverlapFilterCallback* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_overlapFilterCallback2 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 4
  %1 = load %struct.btOverlapFilterCallback*, %struct.btOverlapFilterCallback** %m_overlapFilterCallback2, align 4
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %4 = bitcast %struct.btOverlapFilterCallback* %1 to i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %4, align 4
  %vfn = getelementptr inbounds i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %5 = load i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call = call zeroext i1 %5(%struct.btOverlapFilterCallback* %1, %struct.btBroadphaseProxy* %2, %struct.btBroadphaseProxy* %3)
  store i1 %call, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %6, i32 0, i32 1
  %7 = load i32, i32* %m_collisionFilterGroup, align 4
  %8 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %8, i32 0, i32 2
  %9 = load i32, i32* %m_collisionFilterMask, align 4
  %and = and i32 %7, %9
  %cmp = icmp ne i32 %and, 0
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %collides, align 1
  %10 = load i8, i8* %collides, align 1
  %tobool3 = trunc i8 %10 to i1
  br i1 %tobool3, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end
  %11 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %m_collisionFilterGroup4 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %11, i32 0, i32 1
  %12 = load i32, i32* %m_collisionFilterGroup4, align 4
  %13 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %m_collisionFilterMask5 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %13, i32 0, i32 2
  %14 = load i32, i32* %m_collisionFilterMask5, align 4
  %and6 = and i32 %12, %14
  %tobool7 = icmp ne i32 %and6, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end
  %15 = phi i1 [ false, %if.end ], [ %tobool7, %land.rhs ]
  %frombool8 = zext i1 %15 to i8
  store i8 %frombool8, i8* %collides, align 1
  %16 = load i8, i8* %collides, align 1
  %tobool9 = trunc i8 %16 to i1
  store i1 %tobool9, i1* %retval, align 1
  br label %return

return:                                           ; preds = %land.end, %if.then
  %17 = load i1, i1* %retval, align 1
  ret i1 %17
}

; Function Attrs: noinline optnone
define hidden %struct.btBroadphasePair* @_ZN28btSortedOverlappingPairCache8findPairEP17btBroadphaseProxyS1_(%class.btSortedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) unnamed_addr #2 {
entry:
  %retval = alloca %struct.btBroadphasePair*, align 4
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %tmpPair = alloca %struct.btBroadphasePair, align 4
  %findIndex = alloca i32, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %call = call zeroext i1 @_ZNK28btSortedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_(%class.btSortedOverlappingPairCache* %this1, %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy* %1)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %call2 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ER17btBroadphaseProxyS1_(%struct.btBroadphasePair* %tmpPair, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %2, %struct.btBroadphaseProxy* nonnull align 4 dereferenceable(48) %3)
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE16findLinearSearchERKS0_(%class.btAlignedObjectArray* %m_overlappingPairArray, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %tmpPair)
  store i32 %call3, i32* %findIndex, align 4
  %4 = load i32, i32* %findIndex, align 4
  %m_overlappingPairArray4 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray4)
  %cmp = icmp slt i32 %4, %call5
  br i1 %cmp, label %if.then6, label %if.end9

if.then6:                                         ; preds = %if.end
  %m_overlappingPairArray7 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %5 = load i32, i32* %findIndex, align 4
  %call8 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray7, i32 %5)
  store %struct.btBroadphasePair* %call8, %struct.btBroadphasePair** %pair, align 4
  %6 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  store %struct.btBroadphasePair* %6, %struct.btBroadphasePair** %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %retval, align 4
  br label %return

return:                                           ; preds = %if.end9, %if.then6, %if.then
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %retval, align 4
  ret %struct.btBroadphasePair* %7
}

; Function Attrs: noinline optnone
define hidden void @_ZN28btSortedOverlappingPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher(%class.btSortedOverlappingPairCache* %this, %struct.btOverlapCallback* %callback, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %callback.addr = alloca %struct.btOverlapCallback*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %i = alloca i32, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4
  store %struct.btOverlapCallback* %callback, %struct.btOverlapCallback** %callback.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %if.end, %entry
  %0 = load i32, i32* %i, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_overlappingPairArray2 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %1 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray2, i32 %1)
  store %struct.btBroadphasePair* %call3, %struct.btBroadphasePair** %pair, align 4
  %2 = load %struct.btOverlapCallback*, %struct.btOverlapCallback** %callback.addr, align 4
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %4 = bitcast %struct.btOverlapCallback* %2 to i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)***
  %vtable = load i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)**, i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)*** %4, align 4
  %vfn = getelementptr inbounds i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)*, i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)** %vtable, i64 2
  %5 = load i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)*, i1 (%struct.btOverlapCallback*, %struct.btBroadphasePair*)** %vfn, align 4
  %call4 = call zeroext i1 %5(%struct.btOverlapCallback* %2, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %3)
  br i1 %call4, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %6 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %7 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %8 = bitcast %class.btSortedOverlappingPairCache* %this1 to void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)***
  %vtable5 = load void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)**, void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*** %8, align 4
  %vfn6 = getelementptr inbounds void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vtable5, i64 8
  %9 = load void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btSortedOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vfn6, align 4
  call void %9(%class.btSortedOverlappingPairCache* %this1, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %6, %class.btDispatcher* %7)
  %10 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %10, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %11 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %11, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %m_overlappingPairArray7 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %12 = load i32, i32* %i, align 4
  %m_overlappingPairArray8 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call9 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray8)
  %sub = sub nsw i32 %call9, 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray* %m_overlappingPairArray7, i32 %12, i32 %sub)
  %m_overlappingPairArray10 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE8pop_backEv(%class.btAlignedObjectArray* %m_overlappingPairArray10)
  %13 = load i32, i32* @gOverlappingPairs, align 4
  %dec = add nsw i32 %13, -1
  store i32 %dec, i32* @gOverlappingPairs, align 4
  br label %if.end

if.else:                                          ; preds = %for.body
  %14 = load i32, i32* %i, align 4
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define hidden %class.btSortedOverlappingPairCache* @_ZN28btSortedOverlappingPairCacheC2Ev(%class.btSortedOverlappingPairCache* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %initialAllocatedSize = alloca i32, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.btSortedOverlappingPairCache* %this1 to %class.btOverlappingPairCache*
  %call = call %class.btOverlappingPairCache* @_ZN22btOverlappingPairCacheC2Ev(%class.btOverlappingPairCache* %0) #8
  %1 = bitcast %class.btSortedOverlappingPairCache* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV28btSortedOverlappingPairCache, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI16btBroadphasePairEC2Ev(%class.btAlignedObjectArray* %m_overlappingPairArray)
  %m_blockedForChanges = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 2
  store i8 0, i8* %m_blockedForChanges, align 4
  %m_hasDeferredRemoval = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 3
  store i8 1, i8* %m_hasDeferredRemoval, align 1
  %m_overlapFilterCallback = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 4
  store %struct.btOverlapFilterCallback* null, %struct.btOverlapFilterCallback** %m_overlapFilterCallback, align 4
  %m_ghostPairCallback = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 5
  store %class.btOverlappingPairCallback* null, %class.btOverlappingPairCallback** %m_ghostPairCallback, align 4
  store i32 2, i32* %initialAllocatedSize, align 4
  %m_overlappingPairArray3 = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %2 = load i32, i32* %initialAllocatedSize, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray* %m_overlappingPairArray3, i32 %2)
  ret %class.btSortedOverlappingPairCache* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btSortedOverlappingPairCache* @_ZN28btSortedOverlappingPairCacheD2Ev(%class.btSortedOverlappingPairCache* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.btSortedOverlappingPairCache* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV28btSortedOverlappingPairCache, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI16btBroadphasePairED2Ev(%class.btAlignedObjectArray* %m_overlappingPairArray) #8
  %1 = bitcast %class.btSortedOverlappingPairCache* %this1 to %class.btOverlappingPairCache*
  %call2 = call %class.btOverlappingPairCache* @_ZN22btOverlappingPairCacheD2Ev(%class.btOverlappingPairCache* %1) #8
  ret %class.btSortedOverlappingPairCache* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN28btSortedOverlappingPairCacheD0Ev(%class.btSortedOverlappingPairCache* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %call = call %class.btSortedOverlappingPairCache* @_ZN28btSortedOverlappingPairCacheD1Ev(%class.btSortedOverlappingPairCache* %this1) #8
  %0 = bitcast %class.btSortedOverlappingPairCache* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

; Function Attrs: noinline optnone
define hidden void @_ZN28btSortedOverlappingPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher(%class.btSortedOverlappingPairCache* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %pair, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %pair.addr = alloca %struct.btBroadphasePair*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4
  store %struct.btBroadphasePair* %pair, %struct.btBroadphasePair** %pair.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 2
  %1 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm, align 4
  %tobool = icmp ne %class.btCollisionAlgorithm* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_algorithm2 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 2
  %3 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm2, align 4
  %4 = bitcast %class.btCollisionAlgorithm* %3 to %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)***
  %vtable = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)**, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*** %4, align 4
  %vfn = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vtable, i64 0
  %5 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vfn, align 4
  %call = call %class.btCollisionAlgorithm* %5(%class.btCollisionAlgorithm* %3) #8
  %6 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_algorithm3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 0, i32 2
  %8 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm3, align 4
  %9 = bitcast %class.btCollisionAlgorithm* %8 to i8*
  %10 = bitcast %class.btDispatcher* %6 to void (%class.btDispatcher*, i8*)***
  %vtable4 = load void (%class.btDispatcher*, i8*)**, void (%class.btDispatcher*, i8*)*** %10, align 4
  %vfn5 = getelementptr inbounds void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vtable4, i64 15
  %11 = load void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vfn5, align 4
  call void %11(%class.btDispatcher* %6, i8* %9)
  %12 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_algorithm6 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %12, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm6, align 4
  %13 = load i32, i32* @gRemovePairs, align 4
  %dec = add nsw i32 %13, -1
  store i32 %dec, i32* @gRemovePairs, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher(%class.btSortedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %cleanPairs = alloca %class.CleanPairCallback.5, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4
  %1 = bitcast %class.btSortedOverlappingPairCache* %this1 to %class.btOverlappingPairCache*
  %2 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %call = call %class.CleanPairCallback.5* @_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackC2ES1_P22btOverlappingPairCacheS3_(%class.CleanPairCallback.5* %cleanPairs, %struct.btBroadphaseProxy* %0, %class.btOverlappingPairCache* %1, %class.btDispatcher* %2)
  %3 = bitcast %class.CleanPairCallback.5* %cleanPairs to %struct.btOverlapCallback*
  %4 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %5 = bitcast %class.btSortedOverlappingPairCache* %this1 to void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)***
  %vtable = load void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)**, void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*** %5, align 4
  %vfn = getelementptr inbounds void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vtable, i64 12
  %6 = load void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vfn, align 4
  call void %6(%class.btSortedOverlappingPairCache* %this1, %struct.btOverlapCallback* %3, %class.btDispatcher* %4)
  %call2 = call %class.CleanPairCallback.5* @_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD2Ev(%class.CleanPairCallback.5* %cleanPairs) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal %class.CleanPairCallback.5* @_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackC2ES1_P22btOverlappingPairCacheS3_(%class.CleanPairCallback.5* returned %this, %struct.btBroadphaseProxy* %cleanProxy, %class.btOverlappingPairCache* %pairCache, %class.btDispatcher* %dispatcher) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.CleanPairCallback.5*, align 4
  %cleanProxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %pairCache.addr = alloca %class.btOverlappingPairCache*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.CleanPairCallback.5* %this, %class.CleanPairCallback.5** %this.addr, align 4
  store %struct.btBroadphaseProxy* %cleanProxy, %struct.btBroadphaseProxy** %cleanProxy.addr, align 4
  store %class.btOverlappingPairCache* %pairCache, %class.btOverlappingPairCache** %pairCache.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.CleanPairCallback.5*, %class.CleanPairCallback.5** %this.addr, align 4
  %0 = bitcast %class.CleanPairCallback.5* %this1 to %struct.btOverlapCallback*
  %call = call %struct.btOverlapCallback* @_ZN17btOverlapCallbackC2Ev(%struct.btOverlapCallback* %0) #8
  %1 = bitcast %class.CleanPairCallback.5* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherE17CleanPairCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_cleanProxy = getelementptr inbounds %class.CleanPairCallback.5, %class.CleanPairCallback.5* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %cleanProxy.addr, align 4
  store %struct.btBroadphaseProxy* %2, %struct.btBroadphaseProxy** %m_cleanProxy, align 4
  %m_pairCache = getelementptr inbounds %class.CleanPairCallback.5, %class.CleanPairCallback.5* %this1, i32 0, i32 2
  %3 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCache.addr, align 4
  store %class.btOverlappingPairCache* %3, %class.btOverlappingPairCache** %m_pairCache, align 4
  %m_dispatcher = getelementptr inbounds %class.CleanPairCallback.5, %class.CleanPairCallback.5* %this1, i32 0, i32 3
  %4 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  store %class.btDispatcher* %4, %class.btDispatcher** %m_dispatcher, align 4
  ret %class.CleanPairCallback.5* %this1
}

; Function Attrs: noinline nounwind optnone
define internal %class.CleanPairCallback.5* @_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD2Ev(%class.CleanPairCallback.5* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.CleanPairCallback.5*, align 4
  store %class.CleanPairCallback.5* %this, %class.CleanPairCallback.5** %this.addr, align 4
  %this1 = load %class.CleanPairCallback.5*, %class.CleanPairCallback.5** %this.addr, align 4
  %0 = bitcast %class.CleanPairCallback.5* %this1 to %struct.btOverlapCallback*
  %call = call %struct.btOverlapCallback* @_ZN17btOverlapCallbackD2Ev(%struct.btOverlapCallback* %0) #8
  ret %class.CleanPairCallback.5* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher(%class.btSortedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %removeCallback = alloca %class.RemovePairCallback.6, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4
  %call = call %class.RemovePairCallback.6* @_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackC2ES1_(%class.RemovePairCallback.6* %removeCallback, %struct.btBroadphaseProxy* %0)
  %1 = bitcast %class.RemovePairCallback.6* %removeCallback to %struct.btOverlapCallback*
  %2 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %3 = bitcast %class.btSortedOverlappingPairCache* %this1 to void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)***
  %vtable = load void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)**, void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*** %3, align 4
  %vfn = getelementptr inbounds void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vtable, i64 12
  %4 = load void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btSortedOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vfn, align 4
  call void %4(%class.btSortedOverlappingPairCache* %this1, %struct.btOverlapCallback* %1, %class.btDispatcher* %2)
  %call2 = call %class.RemovePairCallback.6* @_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD2Ev(%class.RemovePairCallback.6* %removeCallback) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal %class.RemovePairCallback.6* @_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackC2ES1_(%class.RemovePairCallback.6* returned %this, %struct.btBroadphaseProxy* %obsoleteProxy) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.RemovePairCallback.6*, align 4
  %obsoleteProxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  store %class.RemovePairCallback.6* %this, %class.RemovePairCallback.6** %this.addr, align 4
  store %struct.btBroadphaseProxy* %obsoleteProxy, %struct.btBroadphaseProxy** %obsoleteProxy.addr, align 4
  %this1 = load %class.RemovePairCallback.6*, %class.RemovePairCallback.6** %this.addr, align 4
  %0 = bitcast %class.RemovePairCallback.6* %this1 to %struct.btOverlapCallback*
  %call = call %struct.btOverlapCallback* @_ZN17btOverlapCallbackC2Ev(%struct.btOverlapCallback* %0) #8
  %1 = bitcast %class.RemovePairCallback.6* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherE18RemovePairCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_obsoleteProxy = getelementptr inbounds %class.RemovePairCallback.6, %class.RemovePairCallback.6* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %obsoleteProxy.addr, align 4
  store %struct.btBroadphaseProxy* %2, %struct.btBroadphaseProxy** %m_obsoleteProxy, align 4
  ret %class.RemovePairCallback.6* %this1
}

; Function Attrs: noinline nounwind optnone
define internal %class.RemovePairCallback.6* @_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD2Ev(%class.RemovePairCallback.6* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.RemovePairCallback.6*, align 4
  store %class.RemovePairCallback.6* %this, %class.RemovePairCallback.6** %this.addr, align 4
  %this1 = load %class.RemovePairCallback.6*, %class.RemovePairCallback.6** %this.addr, align 4
  %0 = bitcast %class.RemovePairCallback.6* %this1 to %struct.btOverlapCallback*
  %call = call %struct.btOverlapCallback* @_ZN17btOverlapCallbackD2Ev(%struct.btOverlapCallback* %0) #8
  ret %class.RemovePairCallback.6* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN28btSortedOverlappingPairCache20sortOverlappingPairsEP12btDispatcher(%class.btSortedOverlappingPairCache* %this, %class.btDispatcher* %dispatcher) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN28btHashedOverlappingPairCache18addOverlappingPairEP17btBroadphaseProxyS1_(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) unnamed_addr #2 comdat {
entry:
  %retval = alloca %struct.btBroadphasePair*, align 4
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load i32, i32* @gAddedPairs, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* @gAddedPairs, align 4
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %call = call zeroext i1 @_ZNK28btHashedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphaseProxy* %1, %struct.btBroadphaseProxy* %2)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %call2 = call %struct.btBroadphasePair* @_ZN28btHashedOverlappingPairCache15internalAddPairEP17btBroadphaseProxyS1_(%class.btHashedOverlappingPairCache* %this1, %struct.btBroadphaseProxy* %3, %struct.btBroadphaseProxy* %4)
  store %struct.btBroadphasePair* %call2, %struct.btBroadphasePair** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %retval, align 4
  ret %struct.btBroadphasePair* %5
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv(%class.btHashedOverlappingPairCache* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray, i32 0)
  ret %struct.btBroadphasePair* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZNK28btHashedOverlappingPairCache26getOverlappingPairArrayPtrEv(%class.btHashedOverlappingPairCache* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZNK20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray, i32 0)
  ret %struct.btBroadphasePair* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray* @_ZN28btHashedOverlappingPairCache23getOverlappingPairArrayEv(%class.btHashedOverlappingPairCache* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  ret %class.btAlignedObjectArray* %m_overlappingPairArray
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK28btHashedOverlappingPairCache22getNumOverlappingPairsEv(%class.btHashedOverlappingPairCache* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN28btHashedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback(%class.btHashedOverlappingPairCache* %this, %struct.btOverlapFilterCallback* %callback) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %callback.addr = alloca %struct.btOverlapFilterCallback*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  store %struct.btOverlapFilterCallback* %callback, %struct.btOverlapFilterCallback** %callback.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btOverlapFilterCallback*, %struct.btOverlapFilterCallback** %callback.addr, align 4
  %m_overlapFilterCallback = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 2
  store %struct.btOverlapFilterCallback* %0, %struct.btOverlapFilterCallback** %m_overlapFilterCallback, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN28btHashedOverlappingPairCache18hasDeferredRemovalEv(%class.btHashedOverlappingPairCache* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  ret i1 false
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN28btHashedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback(%class.btHashedOverlappingPairCache* %this, %class.btOverlappingPairCallback* %ghostPairCallback) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %ghostPairCallback.addr = alloca %class.btOverlappingPairCallback*, align 4
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  store %class.btOverlappingPairCallback* %ghostPairCallback, %class.btOverlappingPairCallback** %ghostPairCallback.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %0 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %ghostPairCallback.addr, align 4
  %m_ghostPairCallback = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 5
  store %class.btOverlappingPairCallback* %0, %class.btOverlappingPairCallback** %m_ghostPairCallback, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv(%class.btSortedOverlappingPairCache* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray, i32 0)
  ret %struct.btBroadphasePair* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZNK28btSortedOverlappingPairCache26getOverlappingPairArrayPtrEv(%class.btSortedOverlappingPairCache* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZNK20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %m_overlappingPairArray, i32 0)
  ret %struct.btBroadphasePair* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray* @_ZN28btSortedOverlappingPairCache23getOverlappingPairArrayEv(%class.btSortedOverlappingPairCache* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  ret %class.btAlignedObjectArray* %m_overlappingPairArray
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK28btSortedOverlappingPairCache22getNumOverlappingPairsEv(%class.btSortedOverlappingPairCache* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %m_overlappingPairArray)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN28btSortedOverlappingPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback(%class.btSortedOverlappingPairCache* %this, %struct.btOverlapFilterCallback* %callback) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %callback.addr = alloca %struct.btOverlapFilterCallback*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4
  store %struct.btOverlapFilterCallback* %callback, %struct.btOverlapFilterCallback** %callback.addr, align 4
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = load %struct.btOverlapFilterCallback*, %struct.btOverlapFilterCallback** %callback.addr, align 4
  %m_overlapFilterCallback = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 4
  store %struct.btOverlapFilterCallback* %0, %struct.btOverlapFilterCallback** %m_overlapFilterCallback, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN28btSortedOverlappingPairCache18hasDeferredRemovalEv(%class.btSortedOverlappingPairCache* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %m_hasDeferredRemoval = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 3
  %0 = load i8, i8* %m_hasDeferredRemoval, align 1
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN28btSortedOverlappingPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback(%class.btSortedOverlappingPairCache* %this, %class.btOverlappingPairCallback* %ghostPairCallback) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSortedOverlappingPairCache*, align 4
  %ghostPairCallback.addr = alloca %class.btOverlappingPairCallback*, align 4
  store %class.btSortedOverlappingPairCache* %this, %class.btSortedOverlappingPairCache** %this.addr, align 4
  store %class.btOverlappingPairCallback* %ghostPairCallback, %class.btOverlappingPairCallback** %ghostPairCallback.addr, align 4
  %this1 = load %class.btSortedOverlappingPairCache*, %class.btSortedOverlappingPairCache** %this.addr, align 4
  %0 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %ghostPairCallback.addr, align 4
  %m_ghostPairCallback = getelementptr inbounds %class.btSortedOverlappingPairCache, %class.btSortedOverlappingPairCache* %this1, i32 0, i32 5
  store %class.btOverlappingPairCallback* %0, %class.btOverlappingPairCallback** %m_ghostPairCallback, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btOverlappingPairCallback* @_ZN25btOverlappingPairCallbackC2Ev(%class.btOverlappingPairCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCallback*, align 4
  store %class.btOverlappingPairCallback* %this, %class.btOverlappingPairCallback** %this.addr, align 4
  %this1 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %this.addr, align 4
  %0 = bitcast %class.btOverlappingPairCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV25btOverlappingPairCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btOverlappingPairCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZN22btOverlappingPairCacheD2Ev(%class.btOverlappingPairCache* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCache*, align 4
  store %class.btOverlappingPairCache* %this, %class.btOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.btOverlappingPairCache* %this1 to %class.btOverlappingPairCallback*
  %call = call %class.btOverlappingPairCallback* @_ZN25btOverlappingPairCallbackD2Ev(%class.btOverlappingPairCallback* %0) #8
  ret %class.btOverlappingPairCache* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN22btOverlappingPairCacheD0Ev(%class.btOverlappingPairCache* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCache*, align 4
  store %class.btOverlappingPairCache* %this, %class.btOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btOverlappingPairCallback* @_ZN25btOverlappingPairCallbackD2Ev(%class.btOverlappingPairCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCallback*, align 4
  store %class.btOverlappingPairCallback* %this, %class.btOverlappingPairCallback** %this.addr, align 4
  %this1 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %this.addr, align 4
  ret %class.btOverlappingPairCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN25btOverlappingPairCallbackD0Ev(%class.btOverlappingPairCallback* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCallback*, align 4
  store %class.btOverlappingPairCallback* %this, %class.btOverlappingPairCallback** %this.addr, align 4
  %this1 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #7

declare void @_Z21btAlignedFreeInternalPv(i8*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btOverlapCallback* @_ZN17btOverlapCallbackC2Ev(%struct.btOverlapCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btOverlapCallback*, align 4
  store %struct.btOverlapCallback* %this, %struct.btOverlapCallback** %this.addr, align 4
  %this1 = load %struct.btOverlapCallback*, %struct.btOverlapCallback** %this.addr, align 4
  %0 = bitcast %struct.btOverlapCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV17btOverlapCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %struct.btOverlapCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev(%class.CleanPairCallback* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.CleanPairCallback*, align 4
  store %class.CleanPairCallback* %this, %class.CleanPairCallback** %this.addr, align 4
  %this1 = load %class.CleanPairCallback*, %class.CleanPairCallback** %this.addr, align 4
  %call = call %class.CleanPairCallback* @_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD2Ev(%class.CleanPairCallback* %this1) #8
  %0 = bitcast %class.CleanPairCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define internal zeroext i1 @_ZZN28btHashedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair(%class.CleanPairCallback* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %pair) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.CleanPairCallback*, align 4
  %pair.addr = alloca %struct.btBroadphasePair*, align 4
  store %class.CleanPairCallback* %this, %class.CleanPairCallback** %this.addr, align 4
  store %struct.btBroadphasePair* %pair, %struct.btBroadphasePair** %pair.addr, align 4
  %this1 = load %class.CleanPairCallback*, %class.CleanPairCallback** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %m_cleanProxy = getelementptr inbounds %class.CleanPairCallback, %class.CleanPairCallback* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_cleanProxy, align 4
  %cmp = icmp eq %struct.btBroadphaseProxy* %1, %2
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 0, i32 1
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %m_cleanProxy2 = getelementptr inbounds %class.CleanPairCallback, %class.CleanPairCallback* %this1, i32 0, i32 1
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_cleanProxy2, align 4
  %cmp3 = icmp eq %struct.btBroadphaseProxy* %4, %5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %m_pairCache = getelementptr inbounds %class.CleanPairCallback, %class.CleanPairCallback* %this1, i32 0, i32 2
  %6 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_dispatcher = getelementptr inbounds %class.CleanPairCallback, %class.CleanPairCallback* %this1, i32 0, i32 3
  %8 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %9 = bitcast %class.btOverlappingPairCache* %6 to void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)***
  %vtable = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)**, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*** %9, align 4
  %vfn = getelementptr inbounds void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vtable, i64 8
  %10 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vfn, align 4
  call void %10(%class.btOverlappingPairCache* %6, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %7, %class.btDispatcher* %8)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  ret i1 false
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btOverlapCallback* @_ZN17btOverlapCallbackD2Ev(%struct.btOverlapCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btOverlapCallback*, align 4
  store %struct.btOverlapCallback* %this, %struct.btOverlapCallback** %this.addr, align 4
  %this1 = load %struct.btOverlapCallback*, %struct.btOverlapCallback** %this.addr, align 4
  ret %struct.btOverlapCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btOverlapCallbackD0Ev(%struct.btOverlapCallback* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btOverlapCallback*, align 4
  store %struct.btOverlapCallback* %this, %struct.btOverlapCallback** %this.addr, align 4
  %this1 = load %struct.btOverlapCallback*, %struct.btOverlapCallback** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev(%class.RemovePairCallback* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.RemovePairCallback*, align 4
  store %class.RemovePairCallback* %this, %class.RemovePairCallback** %this.addr, align 4
  %this1 = load %class.RemovePairCallback*, %class.RemovePairCallback** %this.addr, align 4
  %call = call %class.RemovePairCallback* @_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD2Ev(%class.RemovePairCallback* %this1) #8
  %0 = bitcast %class.RemovePairCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal zeroext i1 @_ZZN28btHashedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair(%class.RemovePairCallback* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %pair) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.RemovePairCallback*, align 4
  %pair.addr = alloca %struct.btBroadphasePair*, align 4
  store %class.RemovePairCallback* %this, %class.RemovePairCallback** %this.addr, align 4
  store %struct.btBroadphasePair* %pair, %struct.btBroadphasePair** %pair.addr, align 4
  %this1 = load %class.RemovePairCallback*, %class.RemovePairCallback** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %m_obsoleteProxy = getelementptr inbounds %class.RemovePairCallback, %class.RemovePairCallback* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_obsoleteProxy, align 4
  %cmp = icmp eq %struct.btBroadphaseProxy* %1, %2
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 0, i32 1
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %m_obsoleteProxy2 = getelementptr inbounds %class.RemovePairCallback, %class.RemovePairCallback* %this1, i32 0, i32 1
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_obsoleteProxy2, align 4
  %cmp3 = icmp eq %struct.btBroadphaseProxy* %4, %5
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %6 = phi i1 [ true, %entry ], [ %cmp3, %lor.rhs ]
  ret i1 %6
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD0Ev(%class.CleanPairCallback.5* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.CleanPairCallback.5*, align 4
  store %class.CleanPairCallback.5* %this, %class.CleanPairCallback.5** %this.addr, align 4
  %this1 = load %class.CleanPairCallback.5*, %class.CleanPairCallback.5** %this.addr, align 4
  %call = call %class.CleanPairCallback.5* @_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallbackD2Ev(%class.CleanPairCallback.5* %this1) #8
  %0 = bitcast %class.CleanPairCallback.5* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define internal zeroext i1 @_ZZN28btSortedOverlappingPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcherEN17CleanPairCallback14processOverlapER16btBroadphasePair(%class.CleanPairCallback.5* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %pair) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.CleanPairCallback.5*, align 4
  %pair.addr = alloca %struct.btBroadphasePair*, align 4
  store %class.CleanPairCallback.5* %this, %class.CleanPairCallback.5** %this.addr, align 4
  store %struct.btBroadphasePair* %pair, %struct.btBroadphasePair** %pair.addr, align 4
  %this1 = load %class.CleanPairCallback.5*, %class.CleanPairCallback.5** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %m_cleanProxy = getelementptr inbounds %class.CleanPairCallback.5, %class.CleanPairCallback.5* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_cleanProxy, align 4
  %cmp = icmp eq %struct.btBroadphaseProxy* %1, %2
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 0, i32 1
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %m_cleanProxy2 = getelementptr inbounds %class.CleanPairCallback.5, %class.CleanPairCallback.5* %this1, i32 0, i32 1
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_cleanProxy2, align 4
  %cmp3 = icmp eq %struct.btBroadphaseProxy* %4, %5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %m_pairCache = getelementptr inbounds %class.CleanPairCallback.5, %class.CleanPairCallback.5* %this1, i32 0, i32 2
  %6 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_dispatcher = getelementptr inbounds %class.CleanPairCallback.5, %class.CleanPairCallback.5* %this1, i32 0, i32 3
  %8 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %9 = bitcast %class.btOverlappingPairCache* %6 to void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)***
  %vtable = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)**, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*** %9, align 4
  %vfn = getelementptr inbounds void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vtable, i64 8
  %10 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vfn, align 4
  call void %10(%class.btOverlappingPairCache* %6, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %7, %class.btDispatcher* %8)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  ret i1 false
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD0Ev(%class.RemovePairCallback.6* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.RemovePairCallback.6*, align 4
  store %class.RemovePairCallback.6* %this, %class.RemovePairCallback.6** %this.addr, align 4
  %this1 = load %class.RemovePairCallback.6*, %class.RemovePairCallback.6** %this.addr, align 4
  %call = call %class.RemovePairCallback.6* @_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallbackD2Ev(%class.RemovePairCallback.6* %this1) #8
  %0 = bitcast %class.RemovePairCallback.6* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal zeroext i1 @_ZZN28btSortedOverlappingPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcherEN18RemovePairCallback14processOverlapER16btBroadphasePair(%class.RemovePairCallback.6* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %pair) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.RemovePairCallback.6*, align 4
  %pair.addr = alloca %struct.btBroadphasePair*, align 4
  store %class.RemovePairCallback.6* %this, %class.RemovePairCallback.6** %this.addr, align 4
  store %struct.btBroadphasePair* %pair, %struct.btBroadphasePair** %pair.addr, align 4
  %this1 = load %class.RemovePairCallback.6*, %class.RemovePairCallback.6** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %m_obsoleteProxy = getelementptr inbounds %class.RemovePairCallback.6, %class.RemovePairCallback.6* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_obsoleteProxy, align 4
  %cmp = icmp eq %struct.btBroadphaseProxy* %1, %2
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 0, i32 1
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %m_obsoleteProxy2 = getelementptr inbounds %class.RemovePairCallback.6, %class.RemovePairCallback.6* %this1, i32 0, i32 1
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_obsoleteProxy2, align 4
  %cmp3 = icmp eq %struct.btBroadphaseProxy* %4, %5
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %6 = phi i1 [ true, %entry ], [ %cmp3, %lor.rhs ]
  ret i1 %6
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK28btHashedOverlappingPairCache24needsBroadphaseCollisionEP17btBroadphaseProxyS1_(%class.btHashedOverlappingPairCache* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btHashedOverlappingPairCache*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %collides = alloca i8, align 1
  store %class.btHashedOverlappingPairCache* %this, %class.btHashedOverlappingPairCache** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %this1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %this.addr, align 4
  %m_overlapFilterCallback = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 2
  %0 = load %struct.btOverlapFilterCallback*, %struct.btOverlapFilterCallback** %m_overlapFilterCallback, align 4
  %tobool = icmp ne %struct.btOverlapFilterCallback* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_overlapFilterCallback2 = getelementptr inbounds %class.btHashedOverlappingPairCache, %class.btHashedOverlappingPairCache* %this1, i32 0, i32 2
  %1 = load %struct.btOverlapFilterCallback*, %struct.btOverlapFilterCallback** %m_overlapFilterCallback2, align 4
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %4 = bitcast %struct.btOverlapFilterCallback* %1 to i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %4, align 4
  %vfn = getelementptr inbounds i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %5 = load i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, i1 (%struct.btOverlapFilterCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call = call zeroext i1 %5(%struct.btOverlapFilterCallback* %1, %struct.btBroadphaseProxy* %2, %struct.btBroadphaseProxy* %3)
  store i1 %call, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %6, i32 0, i32 1
  %7 = load i32, i32* %m_collisionFilterGroup, align 4
  %8 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %8, i32 0, i32 2
  %9 = load i32, i32* %m_collisionFilterMask, align 4
  %and = and i32 %7, %9
  %cmp = icmp ne i32 %and, 0
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %collides, align 1
  %10 = load i8, i8* %collides, align 1
  %tobool3 = trunc i8 %10 to i1
  br i1 %tobool3, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end
  %11 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %m_collisionFilterGroup4 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %11, i32 0, i32 1
  %12 = load i32, i32* %m_collisionFilterGroup4, align 4
  %13 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %m_collisionFilterMask5 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %13, i32 0, i32 2
  %14 = load i32, i32* %m_collisionFilterMask5, align 4
  %and6 = and i32 %12, %14
  %tobool7 = icmp ne i32 %and6, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end
  %15 = phi i1 [ false, %if.end ], [ %tobool7, %land.rhs ]
  %frombool8 = zext i1 %15 to i8
  store i8 %frombool8, i8* %collides, align 1
  %16 = load i8, i8* %collides, align 1
  %tobool9 = trunc i8 %16 to i1
  store i1 %tobool9, i1* %retval, align 1
  br label %return

return:                                           ; preds = %land.end, %if.then
  %17 = load i1, i1* %retval, align 1
  ret i1 %17
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZNK20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 %1
  ret %struct.btBroadphasePair* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.2* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.2* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.2*, align 4
  store %class.btAlignedAllocator.2* %this, %class.btAlignedAllocator.2** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.2*, %class.btAlignedAllocator.2** %this.addr, align 4
  ret %class.btAlignedAllocator.2* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.1* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %tobool = icmp ne %struct.btBroadphasePair* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.btBroadphasePair* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.btBroadphasePair* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btBroadphasePair*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %struct.btBroadphasePair* %ptr, %struct.btBroadphasePair** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %ptr.addr, align 4
  %1 = bitcast %struct.btBroadphasePair* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.1* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.1* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.1* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.1* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.1* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.2* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.2* %this, i32* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.2*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.2* %this, %class.btAlignedAllocator.2** %this.addr, align 4
  store i32* %ptr, i32** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.2*, %class.btAlignedAllocator.2** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btBroadphasePair* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btBroadphasePair** null)
  %2 = bitcast %struct.btBroadphasePair* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btBroadphasePair* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btBroadphasePair*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btBroadphasePair* %dest, %struct.btBroadphasePair** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 %4
  %5 = bitcast %struct.btBroadphasePair* %arrayidx to i8*
  %call = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call to %struct.btBroadphasePair*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 %8
  %call3 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %6, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %struct.btBroadphasePair** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btBroadphasePair**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btBroadphasePair** %hint, %struct.btBroadphasePair*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btBroadphasePair*
  ret %struct.btBroadphasePair* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* returned %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %other) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btBroadphasePair*, align 4
  %other.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %this, %struct.btBroadphasePair** %this.addr, align 4
  store %struct.btBroadphasePair* %other, %struct.btBroadphasePair** %other.addr, align 4
  %this1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %this.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 0
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4
  %m_pProxy02 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy02, align 4
  store %struct.btBroadphaseProxy* %1, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4
  %m_pProxy13 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 1
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy13, align 4
  store %struct.btBroadphaseProxy* %3, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 2
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4
  %m_algorithm4 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 0, i32 2
  %5 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm4, align 4
  store %class.btCollisionAlgorithm* %5, %class.btCollisionAlgorithm** %m_algorithm, align 4
  %6 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon.0* %6 to i8**
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4
  %8 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 0, i32 3
  %m_internalInfo15 = bitcast %union.anon.0* %8 to i8**
  %9 = load i8*, i8** %m_internalInfo15, align 4
  store i8* %9, i8** %m_internalInfo1, align 4
  ret %struct.btBroadphasePair* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.1* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.1* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.1* %this1, i32 %1)
  %2 = bitcast i8* %call2 to i32*
  store i32* %2, i32** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  %3 = load i32*, i32** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.1* %this1, i32 0, i32 %call3, i32* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.1* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.1* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load i32*, i32** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store i32* %4, i32** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.1* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.1* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.2* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.1* %this, i32 %start, i32 %end, i32* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store i32* %dest, i32** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = bitcast i32* %arrayidx to i8*
  %6 = bitcast i8* %5 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %7 = load i32*, i32** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %7, i32 %8
  %9 = load i32, i32* %arrayidx2, align 4
  store i32 %9, i32* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.2* %this, i32 %n, i32** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.2*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.2* %this, %class.btAlignedAllocator.2** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32** %hint, i32*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.2*, %class.btAlignedAllocator.2** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI16btBroadphasePairE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %CompareFunc.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %struct.btBroadphasePair, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btBroadphasePairSortPredicate* %CompareFunc, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  store i32 %lo, i32* %lo.addr, align 4
  store i32 %hi, i32* %hi.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %lo.addr, align 4
  store i32 %0, i32* %i, align 4
  %1 = load i32, i32* %hi.addr, align 4
  store i32 %1, i32* %j, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %3 = load i32, i32* %lo.addr, align 4
  %4 = load i32, i32* %hi.addr, align 4
  %add = add nsw i32 %3, %4
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 %div
  %call = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %x, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx)
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %5 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %6 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data2, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %6, i32 %7
  %call4 = call zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %5, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx3, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %x)
  br i1 %call4, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond5

while.cond5:                                      ; preds = %while.body9, %while.end
  %9 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %10 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data6, align 4
  %11 = load i32, i32* %j, align 4
  %arrayidx7 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %10, i32 %11
  %call8 = call zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %9, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %x, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx7)
  br i1 %call8, label %while.body9, label %while.end10

while.body9:                                      ; preds = %while.cond5
  %12 = load i32, i32* %j, align 4
  %dec = add nsw i32 %12, -1
  store i32 %dec, i32* %j, align 4
  br label %while.cond5

while.end10:                                      ; preds = %while.cond5
  %13 = load i32, i32* %i, align 4
  %14 = load i32, i32* %j, align 4
  %cmp = icmp sle i32 %13, %14
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end10
  %15 = load i32, i32* %i, align 4
  %16 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray* %this1, i32 %15, i32 %16)
  %17 = load i32, i32* %i, align 4
  %inc11 = add nsw i32 %17, 1
  store i32 %inc11, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %dec12 = add nsw i32 %18, -1
  store i32 %dec12, i32* %j, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end10
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %19 = load i32, i32* %i, align 4
  %20 = load i32, i32* %j, align 4
  %cmp13 = icmp sle i32 %19, %20
  br i1 %cmp13, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %21 = load i32, i32* %lo.addr, align 4
  %22 = load i32, i32* %j, align 4
  %cmp14 = icmp slt i32 %21, %22
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %do.end
  %23 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %24 = load i32, i32* %lo.addr, align 4
  %25 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %23, i32 %24, i32 %25)
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %do.end
  %26 = load i32, i32* %i, align 4
  %27 = load i32, i32* %hi.addr, align 4
  %cmp17 = icmp slt i32 %26, %27
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end16
  %28 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %29 = load i32, i32* %i, align 4
  %30 = load i32, i32* %hi.addr, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %28, i32 %29, i32 %30)
  br label %if.end19

if.end19:                                         ; preds = %if.then18, %if.end16
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %a, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %b) #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  %a.addr = alloca %struct.btBroadphasePair*, align 4
  %b.addr = alloca %struct.btBroadphasePair*, align 4
  %uidA0 = alloca i32, align 4
  %uidB0 = alloca i32, align 4
  %uidA1 = alloca i32, align 4
  %uidB1 = alloca i32, align 4
  store %class.btBroadphasePairSortPredicate* %this, %class.btBroadphasePairSortPredicate** %this.addr, align 4
  store %struct.btBroadphasePair* %a, %struct.btBroadphasePair** %a.addr, align 4
  store %struct.btBroadphasePair* %b, %struct.btBroadphasePair** %b.addr, align 4
  %this1 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %tobool = icmp ne %struct.btBroadphaseProxy* %1, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy02 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 0
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy02, align 4
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %3, i32 0, i32 3
  %4 = load i32, i32* %m_uniqueId, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %4, %cond.true ], [ -1, %cond.false ]
  store i32 %cond, i32* %uidA0, align 4
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy03 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %5, i32 0, i32 0
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy03, align 4
  %tobool4 = icmp ne %struct.btBroadphaseProxy* %6, null
  br i1 %tobool4, label %cond.true5, label %cond.false8

cond.true5:                                       ; preds = %cond.end
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy06 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 0, i32 0
  %8 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy06, align 4
  %m_uniqueId7 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %8, i32 0, i32 3
  %9 = load i32, i32* %m_uniqueId7, align 4
  br label %cond.end9

cond.false8:                                      ; preds = %cond.end
  br label %cond.end9

cond.end9:                                        ; preds = %cond.false8, %cond.true5
  %cond10 = phi i32 [ %9, %cond.true5 ], [ -1, %cond.false8 ]
  store i32 %cond10, i32* %uidB0, align 4
  %10 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %10, i32 0, i32 1
  %11 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %tobool11 = icmp ne %struct.btBroadphaseProxy* %11, null
  br i1 %tobool11, label %cond.true12, label %cond.false15

cond.true12:                                      ; preds = %cond.end9
  %12 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy113 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %12, i32 0, i32 1
  %13 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy113, align 4
  %m_uniqueId14 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %13, i32 0, i32 3
  %14 = load i32, i32* %m_uniqueId14, align 4
  br label %cond.end16

cond.false15:                                     ; preds = %cond.end9
  br label %cond.end16

cond.end16:                                       ; preds = %cond.false15, %cond.true12
  %cond17 = phi i32 [ %14, %cond.true12 ], [ -1, %cond.false15 ]
  store i32 %cond17, i32* %uidA1, align 4
  %15 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy118 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %15, i32 0, i32 1
  %16 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy118, align 4
  %tobool19 = icmp ne %struct.btBroadphaseProxy* %16, null
  br i1 %tobool19, label %cond.true20, label %cond.false23

cond.true20:                                      ; preds = %cond.end16
  %17 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy121 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %17, i32 0, i32 1
  %18 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy121, align 4
  %m_uniqueId22 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %18, i32 0, i32 3
  %19 = load i32, i32* %m_uniqueId22, align 4
  br label %cond.end24

cond.false23:                                     ; preds = %cond.end16
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false23, %cond.true20
  %cond25 = phi i32 [ %19, %cond.true20 ], [ -1, %cond.false23 ]
  store i32 %cond25, i32* %uidB1, align 4
  %20 = load i32, i32* %uidA0, align 4
  %21 = load i32, i32* %uidB0, align 4
  %cmp = icmp sgt i32 %20, %21
  br i1 %cmp, label %lor.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %cond.end24
  %22 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy026 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %22, i32 0, i32 0
  %23 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy026, align 4
  %24 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy027 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %24, i32 0, i32 0
  %25 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy027, align 4
  %cmp28 = icmp eq %struct.btBroadphaseProxy* %23, %25
  br i1 %cmp28, label %land.lhs.true, label %lor.rhs

land.lhs.true:                                    ; preds = %lor.lhs.false
  %26 = load i32, i32* %uidA1, align 4
  %27 = load i32, i32* %uidB1, align 4
  %cmp29 = icmp sgt i32 %26, %27
  br i1 %cmp29, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %land.lhs.true, %lor.lhs.false
  %28 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy030 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %28, i32 0, i32 0
  %29 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy030, align 4
  %30 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy031 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %30, i32 0, i32 0
  %31 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy031, align 4
  %cmp32 = icmp eq %struct.btBroadphaseProxy* %29, %31
  br i1 %cmp32, label %land.lhs.true33, label %land.end

land.lhs.true33:                                  ; preds = %lor.rhs
  %32 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy134 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %32, i32 0, i32 1
  %33 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy134, align 4
  %34 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy135 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %34, i32 0, i32 1
  %35 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy135, align 4
  %cmp36 = icmp eq %struct.btBroadphaseProxy* %33, %35
  br i1 %cmp36, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true33
  %36 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %36, i32 0, i32 2
  %37 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm, align 4
  %38 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_algorithm37 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %38, i32 0, i32 2
  %39 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm37, align 4
  %cmp38 = icmp ugt %class.btCollisionAlgorithm* %37, %39
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true33, %lor.rhs
  %40 = phi i1 [ false, %land.lhs.true33 ], [ false, %lor.rhs ], [ %cmp38, %land.rhs ]
  br label %lor.end

lor.end:                                          ; preds = %land.end, %land.lhs.true, %cond.end24
  %41 = phi i1 [ true, %land.lhs.true ], [ true, %cond.end24 ], [ %40, %land.end ]
  ret i1 %41
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZeqRK16btBroadphasePairS1_(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %a, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %b) #1 comdat {
entry:
  %a.addr = alloca %struct.btBroadphasePair*, align 4
  %b.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %a, %struct.btBroadphasePair** %a.addr, align 4
  store %struct.btBroadphasePair* %b, %struct.btBroadphasePair** %b.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy01 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 0
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy01, align 4
  %cmp = icmp eq %struct.btBroadphaseProxy* %1, %3
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 0, i32 1
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %6 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy12 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %6, i32 0, i32 1
  %7 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy12, align 4
  %cmp3 = icmp eq %struct.btBroadphaseProxy* %5, %7
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %8 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %8
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btOverlappingPairCache.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { cold noreturn nounwind }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }
attributes #10 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
