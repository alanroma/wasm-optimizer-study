; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/Gimpact/btTriangleShapeEx.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/Gimpact/btTriangleShapeEx.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%struct.GIM_TRIANGLE_CONTACT = type { float, i32, %class.btVector4, [16 x %class.btVector3] }
%class.btVector4 = type { %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btPrimitiveTriangle = type { [3 x %class.btVector3], %class.btVector4, float, float }
%class.btTriangleShapeEx = type { %class.btTriangleShape }
%class.btTriangleShape = type { %class.btPolyhedralConvexShape, [3 x %class.btVector3] }
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btConvexPolyhedron = type opaque

$_ZN18btInfMaskConverterC2Ei = comdat any

$_Z23bt_distance_point_planeRK9btVector4RK9btVector3 = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN9btVector4C2Ev = comdat any

$_ZNK19btPrimitiveTriangle14get_edge_planeEiR9btVector4 = comdat any

$_Z22bt_plane_clip_triangleRK9btVector4RK9btVector3S4_S4_PS2_ = comdat any

$_Z21bt_plane_clip_polygonRK9btVector4PK9btVector3iPS2_ = comdat any

$_ZN20GIM_TRIANGLE_CONTACTC2Ev = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN20GIM_TRIANGLE_CONTACT9copy_fromERKS_ = comdat any

$_ZNK17btTriangleShapeEx13buildTriPlaneER9btVector4 = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_Z13bt_edge_planeRK9btVector3S1_S1_R9btVector4 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZN9btVector48setValueERKfS1_S1_S1_ = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_Z29bt_plane_clip_polygon_collectRK9btVector3S1_ffPS_Ri = comdat any

$_Z12bt_vec_blendR9btVector3RKS_S2_f = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZmlRK9btVector3RKf = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btTriangleShapeEx.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN20GIM_TRIANGLE_CONTACT12merge_pointsERK9btVector4fPK9btVector3i(%struct.GIM_TRIANGLE_CONTACT* %this, %class.btVector4* nonnull align 4 dereferenceable(16) %plane, float %margin, %class.btVector3* %points, i32 %point_count) #2 {
entry:
  %this.addr = alloca %struct.GIM_TRIANGLE_CONTACT*, align 4
  %plane.addr = alloca %class.btVector4*, align 4
  %margin.addr = alloca float, align 4
  %points.addr = alloca %class.btVector3*, align 4
  %point_count.addr = alloca i32, align 4
  %point_indices = alloca [16 x i32], align 16
  %_k = alloca i32, align 4
  %_dist = alloca float, align 4
  store %struct.GIM_TRIANGLE_CONTACT* %this, %struct.GIM_TRIANGLE_CONTACT** %this.addr, align 4
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4
  store float %margin, float* %margin.addr, align 4
  store %class.btVector3* %points, %class.btVector3** %points.addr, align 4
  store i32 %point_count, i32* %point_count.addr, align 4
  %this1 = load %struct.GIM_TRIANGLE_CONTACT*, %struct.GIM_TRIANGLE_CONTACT** %this.addr, align 4
  %m_point_count = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %this1, i32 0, i32 1
  store i32 0, i32* %m_point_count, align 4
  %m_penetration_depth = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %this1, i32 0, i32 0
  store float -1.000000e+03, float* %m_penetration_depth, align 4
  store i32 0, i32* %_k, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %_k, align 4
  %1 = load i32, i32* %point_count.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %points.addr, align 4
  %4 = load i32, i32* %_k, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  %call = call float @_Z23bt_distance_point_planeRK9btVector4RK9btVector3(%class.btVector4* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  %fneg = fneg float %call
  %5 = load float, float* %margin.addr, align 4
  %add = fadd float %fneg, %5
  store float %add, float* %_dist, align 4
  %6 = load float, float* %_dist, align 4
  %cmp2 = fcmp oge float %6, 0.000000e+00
  br i1 %cmp2, label %if.then, label %if.end17

if.then:                                          ; preds = %for.body
  %7 = load float, float* %_dist, align 4
  %m_penetration_depth3 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %this1, i32 0, i32 0
  %8 = load float, float* %m_penetration_depth3, align 4
  %cmp4 = fcmp ogt float %7, %8
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.then
  %9 = load float, float* %_dist, align 4
  %m_penetration_depth6 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %this1, i32 0, i32 0
  store float %9, float* %m_penetration_depth6, align 4
  %10 = load i32, i32* %_k, align 4
  %arrayidx7 = getelementptr inbounds [16 x i32], [16 x i32]* %point_indices, i32 0, i32 0
  store i32 %10, i32* %arrayidx7, align 16
  %m_point_count8 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %this1, i32 0, i32 1
  store i32 1, i32* %m_point_count8, align 4
  br label %if.end16

if.else:                                          ; preds = %if.then
  %11 = load float, float* %_dist, align 4
  %add9 = fadd float %11, 0x3E80000000000000
  %m_penetration_depth10 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %this1, i32 0, i32 0
  %12 = load float, float* %m_penetration_depth10, align 4
  %cmp11 = fcmp oge float %add9, %12
  br i1 %cmp11, label %if.then12, label %if.end

if.then12:                                        ; preds = %if.else
  %13 = load i32, i32* %_k, align 4
  %m_point_count13 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %this1, i32 0, i32 1
  %14 = load i32, i32* %m_point_count13, align 4
  %arrayidx14 = getelementptr inbounds [16 x i32], [16 x i32]* %point_indices, i32 0, i32 %14
  store i32 %13, i32* %arrayidx14, align 4
  %m_point_count15 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %this1, i32 0, i32 1
  %15 = load i32, i32* %m_point_count15, align 4
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %m_point_count15, align 4
  br label %if.end

if.end:                                           ; preds = %if.then12, %if.else
  br label %if.end16

if.end16:                                         ; preds = %if.end, %if.then5
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end17
  %16 = load i32, i32* %_k, align 4
  %inc18 = add nsw i32 %16, 1
  store i32 %inc18, i32* %_k, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %_k, align 4
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc26, %for.end
  %17 = load i32, i32* %_k, align 4
  %m_point_count20 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %this1, i32 0, i32 1
  %18 = load i32, i32* %m_point_count20, align 4
  %cmp21 = icmp slt i32 %17, %18
  br i1 %cmp21, label %for.body22, label %for.end28

for.body22:                                       ; preds = %for.cond19
  %19 = load %class.btVector3*, %class.btVector3** %points.addr, align 4
  %20 = load i32, i32* %_k, align 4
  %arrayidx23 = getelementptr inbounds [16 x i32], [16 x i32]* %point_indices, i32 0, i32 %20
  %21 = load i32, i32* %arrayidx23, align 4
  %arrayidx24 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 %21
  %m_points = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %this1, i32 0, i32 3
  %22 = load i32, i32* %_k, align 4
  %arrayidx25 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %m_points, i32 0, i32 %22
  %23 = bitcast %class.btVector3* %arrayidx25 to i8*
  %24 = bitcast %class.btVector3* %arrayidx24 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false)
  br label %for.inc26

for.inc26:                                        ; preds = %for.body22
  %25 = load i32, i32* %_k, align 4
  %inc27 = add nsw i32 %25, 1
  store i32 %inc27, i32* %_k, align 4
  br label %for.cond19

for.end28:                                        ; preds = %for.cond19
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_Z23bt_distance_point_planeRK9btVector4RK9btVector3(%class.btVector4* nonnull align 4 dereferenceable(16) %plane, %class.btVector3* nonnull align 4 dereferenceable(16) %point) #2 comdat {
entry:
  %plane.addr = alloca %class.btVector4*, align 4
  %point.addr = alloca %class.btVector3*, align 4
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %1 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %2 = bitcast %class.btVector4* %1 to %class.btVector3*
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %3 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %4 = bitcast %class.btVector4* %3 to %class.btVector3*
  %call1 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %4)
  %arrayidx = getelementptr inbounds float, float* %call1, i32 3
  %5 = load float, float* %arrayidx, align 4
  %sub = fsub float %call, %5
  ret float %sub
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN19btPrimitiveTriangle25overlap_test_conservativeERKS_(%class.btPrimitiveTriangle* %this, %class.btPrimitiveTriangle* nonnull align 4 dereferenceable(72) %other) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btPrimitiveTriangle*, align 4
  %other.addr = alloca %class.btPrimitiveTriangle*, align 4
  %total_margin = alloca float, align 4
  %dis0 = alloca float, align 4
  %dis1 = alloca float, align 4
  %dis2 = alloca float, align 4
  store %class.btPrimitiveTriangle* %this, %class.btPrimitiveTriangle** %this.addr, align 4
  store %class.btPrimitiveTriangle* %other, %class.btPrimitiveTriangle** %other.addr, align 4
  %this1 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %this.addr, align 4
  %m_margin = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 2
  %0 = load float, float* %m_margin, align 4
  %1 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %other.addr, align 4
  %m_margin2 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %1, i32 0, i32 2
  %2 = load float, float* %m_margin2, align 4
  %add = fadd float %0, %2
  store float %add, float* %total_margin, align 4
  %m_plane = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 1
  %3 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %other.addr, align 4
  %m_vertices = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %3, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices, i32 0, i32 0
  %call = call float @_Z23bt_distance_point_planeRK9btVector4RK9btVector3(%class.btVector4* nonnull align 4 dereferenceable(16) %m_plane, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  %4 = load float, float* %total_margin, align 4
  %sub = fsub float %call, %4
  store float %sub, float* %dis0, align 4
  %m_plane3 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 1
  %5 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %other.addr, align 4
  %m_vertices4 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %5, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices4, i32 0, i32 1
  %call6 = call float @_Z23bt_distance_point_planeRK9btVector4RK9btVector3(%class.btVector4* nonnull align 4 dereferenceable(16) %m_plane3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx5)
  %6 = load float, float* %total_margin, align 4
  %sub7 = fsub float %call6, %6
  store float %sub7, float* %dis1, align 4
  %m_plane8 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 1
  %7 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %other.addr, align 4
  %m_vertices9 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %7, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices9, i32 0, i32 2
  %call11 = call float @_Z23bt_distance_point_planeRK9btVector4RK9btVector3(%class.btVector4* nonnull align 4 dereferenceable(16) %m_plane8, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx10)
  %8 = load float, float* %total_margin, align 4
  %sub12 = fsub float %call11, %8
  store float %sub12, float* %dis2, align 4
  %9 = load float, float* %dis0, align 4
  %cmp = fcmp ogt float %9, 0.000000e+00
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %10 = load float, float* %dis1, align 4
  %cmp13 = fcmp ogt float %10, 0.000000e+00
  br i1 %cmp13, label %land.lhs.true14, label %if.end

land.lhs.true14:                                  ; preds = %land.lhs.true
  %11 = load float, float* %dis2, align 4
  %cmp15 = fcmp ogt float %11, 0.000000e+00
  br i1 %cmp15, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true14
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %land.lhs.true14, %land.lhs.true, %entry
  %12 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %other.addr, align 4
  %m_plane16 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %12, i32 0, i32 1
  %m_vertices17 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices17, i32 0, i32 0
  %call19 = call float @_Z23bt_distance_point_planeRK9btVector4RK9btVector3(%class.btVector4* nonnull align 4 dereferenceable(16) %m_plane16, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx18)
  %13 = load float, float* %total_margin, align 4
  %sub20 = fsub float %call19, %13
  store float %sub20, float* %dis0, align 4
  %14 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %other.addr, align 4
  %m_plane21 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %14, i32 0, i32 1
  %m_vertices22 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices22, i32 0, i32 1
  %call24 = call float @_Z23bt_distance_point_planeRK9btVector4RK9btVector3(%class.btVector4* nonnull align 4 dereferenceable(16) %m_plane21, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx23)
  %15 = load float, float* %total_margin, align 4
  %sub25 = fsub float %call24, %15
  store float %sub25, float* %dis1, align 4
  %16 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %other.addr, align 4
  %m_plane26 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %16, i32 0, i32 1
  %m_vertices27 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices27, i32 0, i32 2
  %call29 = call float @_Z23bt_distance_point_planeRK9btVector4RK9btVector3(%class.btVector4* nonnull align 4 dereferenceable(16) %m_plane26, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx28)
  %17 = load float, float* %total_margin, align 4
  %sub30 = fsub float %call29, %17
  store float %sub30, float* %dis2, align 4
  %18 = load float, float* %dis0, align 4
  %cmp31 = fcmp ogt float %18, 0.000000e+00
  br i1 %cmp31, label %land.lhs.true32, label %if.end37

land.lhs.true32:                                  ; preds = %if.end
  %19 = load float, float* %dis1, align 4
  %cmp33 = fcmp ogt float %19, 0.000000e+00
  br i1 %cmp33, label %land.lhs.true34, label %if.end37

land.lhs.true34:                                  ; preds = %land.lhs.true32
  %20 = load float, float* %dis2, align 4
  %cmp35 = fcmp ogt float %20, 0.000000e+00
  br i1 %cmp35, label %if.then36, label %if.end37

if.then36:                                        ; preds = %land.lhs.true34
  store i1 false, i1* %retval, align 1
  br label %return

if.end37:                                         ; preds = %land.lhs.true34, %land.lhs.true32, %if.end
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end37, %if.then36, %if.then
  %21 = load i1, i1* %retval, align 1
  ret i1 %21
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN19btPrimitiveTriangle13clip_triangleERS_P9btVector3(%class.btPrimitiveTriangle* %this, %class.btPrimitiveTriangle* nonnull align 4 dereferenceable(72) %other, %class.btVector3* %clipped_points) #2 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btPrimitiveTriangle*, align 4
  %other.addr = alloca %class.btPrimitiveTriangle*, align 4
  %clipped_points.addr = alloca %class.btVector3*, align 4
  %temp_points = alloca [16 x %class.btVector3], align 16
  %edgeplane = alloca %class.btVector4, align 4
  %clipped_count = alloca i32, align 4
  %temp_points1 = alloca [16 x %class.btVector3], align 16
  store %class.btPrimitiveTriangle* %this, %class.btPrimitiveTriangle** %this.addr, align 4
  store %class.btPrimitiveTriangle* %other, %class.btPrimitiveTriangle** %other.addr, align 4
  store %class.btVector3* %clipped_points, %class.btVector3** %clipped_points.addr, align 4
  %this1 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %this.addr, align 4
  %array.begin = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %temp_points, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 16
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %call2 = call %class.btVector4* @_ZN9btVector4C2Ev(%class.btVector4* %edgeplane)
  call void @_ZNK19btPrimitiveTriangle14get_edge_planeEiR9btVector4(%class.btPrimitiveTriangle* %this1, i32 0, %class.btVector4* nonnull align 4 dereferenceable(16) %edgeplane)
  %0 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %other.addr, align 4
  %m_vertices = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices, i32 0, i32 0
  %1 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %other.addr, align 4
  %m_vertices3 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices3, i32 0, i32 1
  %2 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %other.addr, align 4
  %m_vertices5 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %2, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices5, i32 0, i32 2
  %arraydecay = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %temp_points, i32 0, i32 0
  %call7 = call i32 @_Z22bt_plane_clip_triangleRK9btVector4RK9btVector3S4_S4_PS2_(%class.btVector4* nonnull align 4 dereferenceable(16) %edgeplane, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6, %class.btVector3* %arraydecay)
  store i32 %call7, i32* %clipped_count, align 4
  %3 = load i32, i32* %clipped_count, align 4
  %cmp = icmp eq i32 %3, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %arrayctor.cont
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %arrayctor.cont
  %array.begin8 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %temp_points1, i32 0, i32 0
  %arrayctor.end9 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin8, i32 16
  br label %arrayctor.loop10

arrayctor.loop10:                                 ; preds = %arrayctor.loop10, %if.end
  %arrayctor.cur11 = phi %class.btVector3* [ %array.begin8, %if.end ], [ %arrayctor.next13, %arrayctor.loop10 ]
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur11)
  %arrayctor.next13 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur11, i32 1
  %arrayctor.done14 = icmp eq %class.btVector3* %arrayctor.next13, %arrayctor.end9
  br i1 %arrayctor.done14, label %arrayctor.cont15, label %arrayctor.loop10

arrayctor.cont15:                                 ; preds = %arrayctor.loop10
  call void @_ZNK19btPrimitiveTriangle14get_edge_planeEiR9btVector4(%class.btPrimitiveTriangle* %this1, i32 1, %class.btVector4* nonnull align 4 dereferenceable(16) %edgeplane)
  %arraydecay16 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %temp_points, i32 0, i32 0
  %4 = load i32, i32* %clipped_count, align 4
  %arraydecay17 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %temp_points1, i32 0, i32 0
  %call18 = call i32 @_Z21bt_plane_clip_polygonRK9btVector4PK9btVector3iPS2_(%class.btVector4* nonnull align 4 dereferenceable(16) %edgeplane, %class.btVector3* %arraydecay16, i32 %4, %class.btVector3* %arraydecay17)
  store i32 %call18, i32* %clipped_count, align 4
  %5 = load i32, i32* %clipped_count, align 4
  %cmp19 = icmp eq i32 %5, 0
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %arrayctor.cont15
  store i32 0, i32* %retval, align 4
  br label %return

if.end21:                                         ; preds = %arrayctor.cont15
  call void @_ZNK19btPrimitiveTriangle14get_edge_planeEiR9btVector4(%class.btPrimitiveTriangle* %this1, i32 2, %class.btVector4* nonnull align 4 dereferenceable(16) %edgeplane)
  %arraydecay22 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %temp_points1, i32 0, i32 0
  %6 = load i32, i32* %clipped_count, align 4
  %7 = load %class.btVector3*, %class.btVector3** %clipped_points.addr, align 4
  %call23 = call i32 @_Z21bt_plane_clip_polygonRK9btVector4PK9btVector3iPS2_(%class.btVector4* nonnull align 4 dereferenceable(16) %edgeplane, %class.btVector3* %arraydecay22, i32 %6, %class.btVector3* %7)
  store i32 %call23, i32* %clipped_count, align 4
  %8 = load i32, i32* %clipped_count, align 4
  store i32 %8, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end21, %if.then20, %if.then
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector4* @_ZN9btVector4C2Ev(%class.btVector4* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector4*, align 4
  store %class.btVector4* %this, %class.btVector4** %this.addr, align 4
  %this1 = load %class.btVector4*, %class.btVector4** %this.addr, align 4
  %0 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %0)
  ret %class.btVector4* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK19btPrimitiveTriangle14get_edge_planeEiR9btVector4(%class.btPrimitiveTriangle* %this, i32 %edge_index, %class.btVector4* nonnull align 4 dereferenceable(16) %plane) #2 comdat {
entry:
  %this.addr = alloca %class.btPrimitiveTriangle*, align 4
  %edge_index.addr = alloca i32, align 4
  %plane.addr = alloca %class.btVector4*, align 4
  %e0 = alloca %class.btVector3*, align 4
  %e1 = alloca %class.btVector3*, align 4
  store %class.btPrimitiveTriangle* %this, %class.btPrimitiveTriangle** %this.addr, align 4
  store i32 %edge_index, i32* %edge_index.addr, align 4
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4
  %this1 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %this.addr, align 4
  %m_vertices = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 0
  %0 = load i32, i32* %edge_index.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices, i32 0, i32 %0
  store %class.btVector3* %arrayidx, %class.btVector3** %e0, align 4
  %m_vertices2 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 0
  %1 = load i32, i32* %edge_index.addr, align 4
  %add = add nsw i32 %1, 1
  %rem = srem i32 %add, 3
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices2, i32 0, i32 %rem
  store %class.btVector3* %arrayidx3, %class.btVector3** %e1, align 4
  %2 = load %class.btVector3*, %class.btVector3** %e0, align 4
  %3 = load %class.btVector3*, %class.btVector3** %e1, align 4
  %m_plane = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 1
  %4 = bitcast %class.btVector4* %m_plane to %class.btVector3*
  %5 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  call void @_Z13bt_edge_planeRK9btVector3S1_S1_R9btVector4(%class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector4* nonnull align 4 dereferenceable(16) %5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_Z22bt_plane_clip_triangleRK9btVector4RK9btVector3S4_S4_PS2_(%class.btVector4* nonnull align 4 dereferenceable(16) %plane, %class.btVector3* nonnull align 4 dereferenceable(16) %point0, %class.btVector3* nonnull align 4 dereferenceable(16) %point1, %class.btVector3* nonnull align 4 dereferenceable(16) %point2, %class.btVector3* %clipped) #2 comdat {
entry:
  %plane.addr = alloca %class.btVector4*, align 4
  %point0.addr = alloca %class.btVector3*, align 4
  %point1.addr = alloca %class.btVector3*, align 4
  %point2.addr = alloca %class.btVector3*, align 4
  %clipped.addr = alloca %class.btVector3*, align 4
  %clipped_count = alloca i32, align 4
  %firstdist = alloca float, align 4
  %olddist = alloca float, align 4
  %dist = alloca float, align 4
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4
  store %class.btVector3* %point0, %class.btVector3** %point0.addr, align 4
  store %class.btVector3* %point1, %class.btVector3** %point1.addr, align 4
  store %class.btVector3* %point2, %class.btVector3** %point2.addr, align 4
  store %class.btVector3* %clipped, %class.btVector3** %clipped.addr, align 4
  store i32 0, i32* %clipped_count, align 4
  %0 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4
  %call = call float @_Z23bt_distance_point_planeRK9btVector4RK9btVector3(%class.btVector4* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %firstdist, align 4
  %2 = load float, float* %firstdist, align 4
  %cmp = fcmp ogt float %2, 0x3E80000000000000
  br i1 %cmp, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %3 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  %5 = load i32, i32* %clipped_count, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  %6 = bitcast %class.btVector3* %arrayidx to i8*
  %7 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %8 = load i32, i32* %clipped_count, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %clipped_count, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load float, float* %firstdist, align 4
  store float %9, float* %olddist, align 4
  %10 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %11 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4
  %call1 = call float @_Z23bt_distance_point_planeRK9btVector4RK9btVector3(%class.btVector4* nonnull align 4 dereferenceable(16) %10, %class.btVector3* nonnull align 4 dereferenceable(16) %11)
  store float %call1, float* %dist, align 4
  %12 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4
  %13 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4
  %14 = load float, float* %olddist, align 4
  %15 = load float, float* %dist, align 4
  %16 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  call void @_Z29bt_plane_clip_polygon_collectRK9btVector3S1_ffPS_Ri(%class.btVector3* nonnull align 4 dereferenceable(16) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13, float %14, float %15, %class.btVector3* %16, i32* nonnull align 4 dereferenceable(4) %clipped_count)
  %17 = load float, float* %dist, align 4
  store float %17, float* %olddist, align 4
  %18 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %19 = load %class.btVector3*, %class.btVector3** %point2.addr, align 4
  %call2 = call float @_Z23bt_distance_point_planeRK9btVector4RK9btVector3(%class.btVector4* nonnull align 4 dereferenceable(16) %18, %class.btVector3* nonnull align 4 dereferenceable(16) %19)
  store float %call2, float* %dist, align 4
  %20 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4
  %21 = load %class.btVector3*, %class.btVector3** %point2.addr, align 4
  %22 = load float, float* %olddist, align 4
  %23 = load float, float* %dist, align 4
  %24 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  call void @_Z29bt_plane_clip_polygon_collectRK9btVector3S1_ffPS_Ri(%class.btVector3* nonnull align 4 dereferenceable(16) %20, %class.btVector3* nonnull align 4 dereferenceable(16) %21, float %22, float %23, %class.btVector3* %24, i32* nonnull align 4 dereferenceable(4) %clipped_count)
  %25 = load float, float* %dist, align 4
  store float %25, float* %olddist, align 4
  %26 = load %class.btVector3*, %class.btVector3** %point2.addr, align 4
  %27 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4
  %28 = load float, float* %olddist, align 4
  %29 = load float, float* %firstdist, align 4
  %30 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  call void @_Z29bt_plane_clip_polygon_collectRK9btVector3S1_ffPS_Ri(%class.btVector3* nonnull align 4 dereferenceable(16) %26, %class.btVector3* nonnull align 4 dereferenceable(16) %27, float %28, float %29, %class.btVector3* %30, i32* nonnull align 4 dereferenceable(4) %clipped_count)
  %31 = load i32, i32* %clipped_count, align 4
  ret i32 %31
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_Z21bt_plane_clip_polygonRK9btVector4PK9btVector3iPS2_(%class.btVector4* nonnull align 4 dereferenceable(16) %plane, %class.btVector3* %polygon_points, i32 %polygon_point_count, %class.btVector3* %clipped) #2 comdat {
entry:
  %plane.addr = alloca %class.btVector4*, align 4
  %polygon_points.addr = alloca %class.btVector3*, align 4
  %polygon_point_count.addr = alloca i32, align 4
  %clipped.addr = alloca %class.btVector3*, align 4
  %clipped_count = alloca i32, align 4
  %firstdist = alloca float, align 4
  %olddist = alloca float, align 4
  %i = alloca i32, align 4
  %dist = alloca float, align 4
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4
  store %class.btVector3* %polygon_points, %class.btVector3** %polygon_points.addr, align 4
  store i32 %polygon_point_count, i32* %polygon_point_count.addr, align 4
  store %class.btVector3* %clipped, %class.btVector3** %clipped.addr, align 4
  store i32 0, i32* %clipped_count, align 4
  %0 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0
  %call = call float @_Z23bt_distance_point_planeRK9btVector4RK9btVector3(%class.btVector4* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  store float %call, float* %firstdist, align 4
  %2 = load float, float* %firstdist, align 4
  %cmp = fcmp ogt float %2, 0x3E80000000000000
  br i1 %cmp, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %3 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4
  %arrayidx1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0
  %4 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  %5 = load i32, i32* %clipped_count, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  %6 = bitcast %class.btVector3* %arrayidx2 to i8*
  %7 = bitcast %class.btVector3* %arrayidx1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %8 = load i32, i32* %clipped_count, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %clipped_count, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load float, float* %firstdist, align 4
  store float %9, float* %olddist, align 4
  store i32 1, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %10 = load i32, i32* %i, align 4
  %11 = load i32, i32* %polygon_point_count.addr, align 4
  %cmp3 = icmp slt i32 %10, %11
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %12 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %13 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4
  %14 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 %14
  %call5 = call float @_Z23bt_distance_point_planeRK9btVector4RK9btVector3(%class.btVector4* nonnull align 4 dereferenceable(16) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4)
  store float %call5, float* %dist, align 4
  %15 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4
  %16 = load i32, i32* %i, align 4
  %sub = sub nsw i32 %16, 1
  %arrayidx6 = getelementptr inbounds %class.btVector3, %class.btVector3* %15, i32 %sub
  %17 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4
  %18 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr inbounds %class.btVector3, %class.btVector3* %17, i32 %18
  %19 = load float, float* %olddist, align 4
  %20 = load float, float* %dist, align 4
  %21 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  call void @_Z29bt_plane_clip_polygon_collectRK9btVector3S1_ffPS_Ri(%class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx7, float %19, float %20, %class.btVector3* %21, i32* nonnull align 4 dereferenceable(4) %clipped_count)
  %22 = load float, float* %dist, align 4
  store float %22, float* %olddist, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %23 = load i32, i32* %i, align 4
  %inc8 = add nsw i32 %23, 1
  store i32 %inc8, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %24 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4
  %25 = load i32, i32* %polygon_point_count.addr, align 4
  %sub9 = sub nsw i32 %25, 1
  %arrayidx10 = getelementptr inbounds %class.btVector3, %class.btVector3* %24, i32 %sub9
  %26 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4
  %arrayidx11 = getelementptr inbounds %class.btVector3, %class.btVector3* %26, i32 0
  %27 = load float, float* %olddist, align 4
  %28 = load float, float* %firstdist, align 4
  %29 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  call void @_Z29bt_plane_clip_polygon_collectRK9btVector3S1_ffPS_Ri(%class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx10, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx11, float %27, float %28, %class.btVector3* %29, i32* nonnull align 4 dereferenceable(4) %clipped_count)
  %30 = load i32, i32* %clipped_count, align 4
  ret i32 %30
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN19btPrimitiveTriangle35find_triangle_collision_clip_methodERS_R20GIM_TRIANGLE_CONTACT(%class.btPrimitiveTriangle* %this, %class.btPrimitiveTriangle* nonnull align 4 dereferenceable(72) %other, %struct.GIM_TRIANGLE_CONTACT* nonnull align 4 dereferenceable(280) %contacts) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btPrimitiveTriangle*, align 4
  %other.addr = alloca %class.btPrimitiveTriangle*, align 4
  %contacts.addr = alloca %struct.GIM_TRIANGLE_CONTACT*, align 4
  %margin = alloca float, align 4
  %clipped_points = alloca [16 x %class.btVector3], align 16
  %clipped_count = alloca i32, align 4
  %contacts1 = alloca %struct.GIM_TRIANGLE_CONTACT, align 4
  %ref.tmp = alloca float, align 4
  %contacts2 = alloca %struct.GIM_TRIANGLE_CONTACT, align 4
  store %class.btPrimitiveTriangle* %this, %class.btPrimitiveTriangle** %this.addr, align 4
  store %class.btPrimitiveTriangle* %other, %class.btPrimitiveTriangle** %other.addr, align 4
  store %struct.GIM_TRIANGLE_CONTACT* %contacts, %struct.GIM_TRIANGLE_CONTACT** %contacts.addr, align 4
  %this1 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %this.addr, align 4
  %m_margin = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 2
  %0 = load float, float* %m_margin, align 4
  %1 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %other.addr, align 4
  %m_margin2 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %1, i32 0, i32 2
  %2 = load float, float* %m_margin2, align 4
  %add = fadd float %0, %2
  store float %add, float* %margin, align 4
  %array.begin = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %clipped_points, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 16
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %call3 = call %struct.GIM_TRIANGLE_CONTACT* @_ZN20GIM_TRIANGLE_CONTACTC2Ev(%struct.GIM_TRIANGLE_CONTACT* %contacts1)
  %m_plane = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 1
  %m_separating_normal = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %contacts1, i32 0, i32 2
  %3 = bitcast %class.btVector4* %m_separating_normal to i8*
  %4 = bitcast %class.btVector4* %m_plane to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %5 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %other.addr, align 4
  %arraydecay = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %clipped_points, i32 0, i32 0
  %call4 = call i32 @_ZN19btPrimitiveTriangle13clip_triangleERS_P9btVector3(%class.btPrimitiveTriangle* %this1, %class.btPrimitiveTriangle* nonnull align 4 dereferenceable(72) %5, %class.btVector3* %arraydecay)
  store i32 %call4, i32* %clipped_count, align 4
  %6 = load i32, i32* %clipped_count, align 4
  %cmp = icmp eq i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %arrayctor.cont
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %arrayctor.cont
  %m_separating_normal5 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %contacts1, i32 0, i32 2
  %7 = load float, float* %margin, align 4
  %arraydecay6 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %clipped_points, i32 0, i32 0
  %8 = load i32, i32* %clipped_count, align 4
  call void @_ZN20GIM_TRIANGLE_CONTACT12merge_pointsERK9btVector4fPK9btVector3i(%struct.GIM_TRIANGLE_CONTACT* %contacts1, %class.btVector4* nonnull align 4 dereferenceable(16) %m_separating_normal5, float %7, %class.btVector3* %arraydecay6, i32 %8)
  %m_point_count = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %contacts1, i32 0, i32 1
  %9 = load i32, i32* %m_point_count, align 4
  %cmp7 = icmp eq i32 %9, 0
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end
  store i1 false, i1* %retval, align 1
  br label %return

if.end9:                                          ; preds = %if.end
  store float -1.000000e+00, float* %ref.tmp, align 4
  %m_separating_normal10 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %contacts1, i32 0, i32 2
  %10 = bitcast %class.btVector4* %m_separating_normal10 to %class.btVector3*
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %10, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %call12 = call %struct.GIM_TRIANGLE_CONTACT* @_ZN20GIM_TRIANGLE_CONTACTC2Ev(%struct.GIM_TRIANGLE_CONTACT* %contacts2)
  %11 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %other.addr, align 4
  %m_plane13 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %11, i32 0, i32 1
  %m_separating_normal14 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %contacts2, i32 0, i32 2
  %12 = bitcast %class.btVector4* %m_separating_normal14 to i8*
  %13 = bitcast %class.btVector4* %m_plane13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  %14 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %other.addr, align 4
  %arraydecay15 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %clipped_points, i32 0, i32 0
  %call16 = call i32 @_ZN19btPrimitiveTriangle13clip_triangleERS_P9btVector3(%class.btPrimitiveTriangle* %14, %class.btPrimitiveTriangle* nonnull align 4 dereferenceable(72) %this1, %class.btVector3* %arraydecay15)
  store i32 %call16, i32* %clipped_count, align 4
  %15 = load i32, i32* %clipped_count, align 4
  %cmp17 = icmp eq i32 %15, 0
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end9
  store i1 false, i1* %retval, align 1
  br label %return

if.end19:                                         ; preds = %if.end9
  %m_separating_normal20 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %contacts2, i32 0, i32 2
  %16 = load float, float* %margin, align 4
  %arraydecay21 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %clipped_points, i32 0, i32 0
  %17 = load i32, i32* %clipped_count, align 4
  call void @_ZN20GIM_TRIANGLE_CONTACT12merge_pointsERK9btVector4fPK9btVector3i(%struct.GIM_TRIANGLE_CONTACT* %contacts2, %class.btVector4* nonnull align 4 dereferenceable(16) %m_separating_normal20, float %16, %class.btVector3* %arraydecay21, i32 %17)
  %m_point_count22 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %contacts2, i32 0, i32 1
  %18 = load i32, i32* %m_point_count22, align 4
  %cmp23 = icmp eq i32 %18, 0
  br i1 %cmp23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %if.end19
  store i1 false, i1* %retval, align 1
  br label %return

if.end25:                                         ; preds = %if.end19
  %m_penetration_depth = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %contacts2, i32 0, i32 0
  %19 = load float, float* %m_penetration_depth, align 4
  %m_penetration_depth26 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %contacts1, i32 0, i32 0
  %20 = load float, float* %m_penetration_depth26, align 4
  %cmp27 = fcmp olt float %19, %20
  br i1 %cmp27, label %if.then28, label %if.else

if.then28:                                        ; preds = %if.end25
  %21 = load %struct.GIM_TRIANGLE_CONTACT*, %struct.GIM_TRIANGLE_CONTACT** %contacts.addr, align 4
  call void @_ZN20GIM_TRIANGLE_CONTACT9copy_fromERKS_(%struct.GIM_TRIANGLE_CONTACT* %21, %struct.GIM_TRIANGLE_CONTACT* nonnull align 4 dereferenceable(280) %contacts2)
  br label %if.end29

if.else:                                          ; preds = %if.end25
  %22 = load %struct.GIM_TRIANGLE_CONTACT*, %struct.GIM_TRIANGLE_CONTACT** %contacts.addr, align 4
  call void @_ZN20GIM_TRIANGLE_CONTACT9copy_fromERKS_(%struct.GIM_TRIANGLE_CONTACT* %22, %struct.GIM_TRIANGLE_CONTACT* nonnull align 4 dereferenceable(280) %contacts1)
  br label %if.end29

if.end29:                                         ; preds = %if.else, %if.then28
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end29, %if.then24, %if.then18, %if.then8, %if.then
  %23 = load i1, i1* %retval, align 1
  ret i1 %23
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.GIM_TRIANGLE_CONTACT* @_ZN20GIM_TRIANGLE_CONTACTC2Ev(%struct.GIM_TRIANGLE_CONTACT* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %struct.GIM_TRIANGLE_CONTACT*, align 4
  %this.addr = alloca %struct.GIM_TRIANGLE_CONTACT*, align 4
  store %struct.GIM_TRIANGLE_CONTACT* %this, %struct.GIM_TRIANGLE_CONTACT** %this.addr, align 4
  %this1 = load %struct.GIM_TRIANGLE_CONTACT*, %struct.GIM_TRIANGLE_CONTACT** %this.addr, align 4
  store %struct.GIM_TRIANGLE_CONTACT* %this1, %struct.GIM_TRIANGLE_CONTACT** %retval, align 4
  %m_separating_normal = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %this1, i32 0, i32 2
  %call = call %class.btVector4* @_ZN9btVector4C2Ev(%class.btVector4* %m_separating_normal)
  %m_points = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %this1, i32 0, i32 3
  %array.begin = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %m_points, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 16
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %struct.GIM_TRIANGLE_CONTACT*, %struct.GIM_TRIANGLE_CONTACT** %retval, align 4
  ret %struct.GIM_TRIANGLE_CONTACT* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20GIM_TRIANGLE_CONTACT9copy_fromERKS_(%struct.GIM_TRIANGLE_CONTACT* %this, %struct.GIM_TRIANGLE_CONTACT* nonnull align 4 dereferenceable(280) %other) #1 comdat {
entry:
  %this.addr = alloca %struct.GIM_TRIANGLE_CONTACT*, align 4
  %other.addr = alloca %struct.GIM_TRIANGLE_CONTACT*, align 4
  %i = alloca i32, align 4
  store %struct.GIM_TRIANGLE_CONTACT* %this, %struct.GIM_TRIANGLE_CONTACT** %this.addr, align 4
  store %struct.GIM_TRIANGLE_CONTACT* %other, %struct.GIM_TRIANGLE_CONTACT** %other.addr, align 4
  %this1 = load %struct.GIM_TRIANGLE_CONTACT*, %struct.GIM_TRIANGLE_CONTACT** %this.addr, align 4
  %0 = load %struct.GIM_TRIANGLE_CONTACT*, %struct.GIM_TRIANGLE_CONTACT** %other.addr, align 4
  %m_penetration_depth = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %0, i32 0, i32 0
  %1 = load float, float* %m_penetration_depth, align 4
  %m_penetration_depth2 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %this1, i32 0, i32 0
  store float %1, float* %m_penetration_depth2, align 4
  %2 = load %struct.GIM_TRIANGLE_CONTACT*, %struct.GIM_TRIANGLE_CONTACT** %other.addr, align 4
  %m_separating_normal = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %2, i32 0, i32 2
  %m_separating_normal3 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %this1, i32 0, i32 2
  %3 = bitcast %class.btVector4* %m_separating_normal3 to i8*
  %4 = bitcast %class.btVector4* %m_separating_normal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %5 = load %struct.GIM_TRIANGLE_CONTACT*, %struct.GIM_TRIANGLE_CONTACT** %other.addr, align 4
  %m_point_count = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %5, i32 0, i32 1
  %6 = load i32, i32* %m_point_count, align 4
  %m_point_count4 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %this1, i32 0, i32 1
  store i32 %6, i32* %m_point_count4, align 4
  %m_point_count5 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %this1, i32 0, i32 1
  %7 = load i32, i32* %m_point_count5, align 4
  store i32 %7, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %8 = load i32, i32* %i, align 4
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %i, align 4
  %tobool = icmp ne i32 %8, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load %struct.GIM_TRIANGLE_CONTACT*, %struct.GIM_TRIANGLE_CONTACT** %other.addr, align 4
  %m_points = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %9, i32 0, i32 3
  %10 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %m_points, i32 0, i32 %10
  %m_points6 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %this1, i32 0, i32 3
  %11 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %m_points6, i32 0, i32 %11
  %12 = bitcast %class.btVector3* %arrayidx7 to i8*
  %13 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN17btTriangleShapeEx25overlap_test_conservativeERKS_(%class.btTriangleShapeEx* %this, %class.btTriangleShapeEx* nonnull align 4 dereferenceable(108) %other) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btTriangleShapeEx*, align 4
  %other.addr = alloca %class.btTriangleShapeEx*, align 4
  %total_margin = alloca float, align 4
  %plane0 = alloca %class.btVector4, align 4
  %plane1 = alloca %class.btVector4, align 4
  %dis0 = alloca float, align 4
  %dis1 = alloca float, align 4
  %dis2 = alloca float, align 4
  store %class.btTriangleShapeEx* %this, %class.btTriangleShapeEx** %this.addr, align 4
  store %class.btTriangleShapeEx* %other, %class.btTriangleShapeEx** %other.addr, align 4
  %this1 = load %class.btTriangleShapeEx*, %class.btTriangleShapeEx** %this.addr, align 4
  %0 = bitcast %class.btTriangleShapeEx* %this1 to %class.btConvexInternalShape*
  %1 = bitcast %class.btConvexInternalShape* %0 to float (%class.btConvexInternalShape*)***
  %vtable = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %1, align 4
  %vfn = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable, i64 12
  %2 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn, align 4
  %call = call float %2(%class.btConvexInternalShape* %0)
  %3 = load %class.btTriangleShapeEx*, %class.btTriangleShapeEx** %other.addr, align 4
  %4 = bitcast %class.btTriangleShapeEx* %3 to %class.btConvexInternalShape*
  %5 = bitcast %class.btConvexInternalShape* %4 to float (%class.btConvexInternalShape*)***
  %vtable2 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %5, align 4
  %vfn3 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable2, i64 12
  %6 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn3, align 4
  %call4 = call float %6(%class.btConvexInternalShape* %4)
  %add = fadd float %call, %call4
  store float %add, float* %total_margin, align 4
  %call5 = call %class.btVector4* @_ZN9btVector4C2Ev(%class.btVector4* %plane0)
  call void @_ZNK17btTriangleShapeEx13buildTriPlaneER9btVector4(%class.btTriangleShapeEx* %this1, %class.btVector4* nonnull align 4 dereferenceable(16) %plane0)
  %call6 = call %class.btVector4* @_ZN9btVector4C2Ev(%class.btVector4* %plane1)
  %7 = load %class.btTriangleShapeEx*, %class.btTriangleShapeEx** %other.addr, align 4
  call void @_ZNK17btTriangleShapeEx13buildTriPlaneER9btVector4(%class.btTriangleShapeEx* %7, %class.btVector4* nonnull align 4 dereferenceable(16) %plane1)
  %8 = load %class.btTriangleShapeEx*, %class.btTriangleShapeEx** %other.addr, align 4
  %9 = bitcast %class.btTriangleShapeEx* %8 to %class.btTriangleShape*
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %9, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %call7 = call float @_Z23bt_distance_point_planeRK9btVector4RK9btVector3(%class.btVector4* nonnull align 4 dereferenceable(16) %plane0, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  %10 = load float, float* %total_margin, align 4
  %sub = fsub float %call7, %10
  store float %sub, float* %dis0, align 4
  %11 = load %class.btTriangleShapeEx*, %class.btTriangleShapeEx** %other.addr, align 4
  %12 = bitcast %class.btTriangleShapeEx* %11 to %class.btTriangleShape*
  %m_vertices18 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %12, i32 0, i32 1
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices18, i32 0, i32 1
  %call10 = call float @_Z23bt_distance_point_planeRK9btVector4RK9btVector3(%class.btVector4* nonnull align 4 dereferenceable(16) %plane0, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx9)
  %13 = load float, float* %total_margin, align 4
  %sub11 = fsub float %call10, %13
  store float %sub11, float* %dis1, align 4
  %14 = load %class.btTriangleShapeEx*, %class.btTriangleShapeEx** %other.addr, align 4
  %15 = bitcast %class.btTriangleShapeEx* %14 to %class.btTriangleShape*
  %m_vertices112 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %15, i32 0, i32 1
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices112, i32 0, i32 2
  %call14 = call float @_Z23bt_distance_point_planeRK9btVector4RK9btVector3(%class.btVector4* nonnull align 4 dereferenceable(16) %plane0, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx13)
  %16 = load float, float* %total_margin, align 4
  %sub15 = fsub float %call14, %16
  store float %sub15, float* %dis2, align 4
  %17 = load float, float* %dis0, align 4
  %cmp = fcmp ogt float %17, 0.000000e+00
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %18 = load float, float* %dis1, align 4
  %cmp16 = fcmp ogt float %18, 0.000000e+00
  br i1 %cmp16, label %land.lhs.true17, label %if.end

land.lhs.true17:                                  ; preds = %land.lhs.true
  %19 = load float, float* %dis2, align 4
  %cmp18 = fcmp ogt float %19, 0.000000e+00
  br i1 %cmp18, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true17
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %land.lhs.true17, %land.lhs.true, %entry
  %20 = bitcast %class.btTriangleShapeEx* %this1 to %class.btTriangleShape*
  %m_vertices119 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %20, i32 0, i32 1
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices119, i32 0, i32 0
  %call21 = call float @_Z23bt_distance_point_planeRK9btVector4RK9btVector3(%class.btVector4* nonnull align 4 dereferenceable(16) %plane1, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx20)
  %21 = load float, float* %total_margin, align 4
  %sub22 = fsub float %call21, %21
  store float %sub22, float* %dis0, align 4
  %22 = bitcast %class.btTriangleShapeEx* %this1 to %class.btTriangleShape*
  %m_vertices123 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %22, i32 0, i32 1
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices123, i32 0, i32 1
  %call25 = call float @_Z23bt_distance_point_planeRK9btVector4RK9btVector3(%class.btVector4* nonnull align 4 dereferenceable(16) %plane1, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx24)
  %23 = load float, float* %total_margin, align 4
  %sub26 = fsub float %call25, %23
  store float %sub26, float* %dis1, align 4
  %24 = bitcast %class.btTriangleShapeEx* %this1 to %class.btTriangleShape*
  %m_vertices127 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %24, i32 0, i32 1
  %arrayidx28 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices127, i32 0, i32 2
  %call29 = call float @_Z23bt_distance_point_planeRK9btVector4RK9btVector3(%class.btVector4* nonnull align 4 dereferenceable(16) %plane1, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx28)
  %25 = load float, float* %total_margin, align 4
  %sub30 = fsub float %call29, %25
  store float %sub30, float* %dis2, align 4
  %26 = load float, float* %dis0, align 4
  %cmp31 = fcmp ogt float %26, 0.000000e+00
  br i1 %cmp31, label %land.lhs.true32, label %if.end37

land.lhs.true32:                                  ; preds = %if.end
  %27 = load float, float* %dis1, align 4
  %cmp33 = fcmp ogt float %27, 0.000000e+00
  br i1 %cmp33, label %land.lhs.true34, label %if.end37

land.lhs.true34:                                  ; preds = %land.lhs.true32
  %28 = load float, float* %dis2, align 4
  %cmp35 = fcmp ogt float %28, 0.000000e+00
  br i1 %cmp35, label %if.then36, label %if.end37

if.then36:                                        ; preds = %land.lhs.true34
  store i1 false, i1* %retval, align 1
  br label %return

if.end37:                                         ; preds = %land.lhs.true34, %land.lhs.true32, %if.end
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end37, %if.then36, %if.then
  %29 = load i1, i1* %retval, align 1
  ret i1 %29
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK17btTriangleShapeEx13buildTriPlaneER9btVector4(%class.btTriangleShapeEx* %this, %class.btVector4* nonnull align 4 dereferenceable(16) %plane) #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShapeEx*, align 4
  %plane.addr = alloca %class.btVector4*, align 4
  %normal = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca float, align 4
  store %class.btTriangleShapeEx* %this, %class.btTriangleShapeEx** %this.addr, align 4
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4
  %this1 = load %class.btTriangleShapeEx*, %class.btTriangleShapeEx** %this.addr, align 4
  %0 = bitcast %class.btTriangleShapeEx* %this1 to %class.btTriangleShape*
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %0, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 1
  %1 = bitcast %class.btTriangleShapeEx* %this1 to %class.btTriangleShape*
  %m_vertices12 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %1, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices12, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx3)
  %2 = bitcast %class.btTriangleShapeEx* %this1 to %class.btTriangleShape*
  %m_vertices15 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %2, i32 0, i32 1
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices15, i32 0, i32 2
  %3 = bitcast %class.btTriangleShapeEx* %this1 to %class.btTriangleShape*
  %m_vertices17 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %3, i32 0, i32 1
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices17, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx8)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %normal, %class.btVector3* %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %normal)
  %4 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 0
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal)
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 1
  %call13 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal)
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 2
  %5 = bitcast %class.btTriangleShapeEx* %this1 to %class.btTriangleShape*
  %m_vertices116 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %5, i32 0, i32 1
  %arrayidx17 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices116, i32 0, i32 0
  %call18 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %arrayidx17, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  store float %call18, float* %ref.tmp15, align 4
  call void @_ZN9btVector48setValueERKfS1_S1_S1_(%class.btVector4* %4, float* nonnull align 4 dereferenceable(4) %arrayidx10, float* nonnull align 4 dereferenceable(4) %arrayidx12, float* nonnull align 4 dereferenceable(4) %arrayidx14, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z13bt_edge_planeRK9btVector3S1_S1_R9btVector4(%class.btVector3* nonnull align 4 dereferenceable(16) %e1, %class.btVector3* nonnull align 4 dereferenceable(16) %e2, %class.btVector3* nonnull align 4 dereferenceable(16) %normal, %class.btVector4* nonnull align 4 dereferenceable(16) %plane) #2 comdat {
entry:
  %e1.addr = alloca %class.btVector3*, align 4
  %e2.addr = alloca %class.btVector3*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %plane.addr = alloca %class.btVector4*, align 4
  %planenormal = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btVector3* %e1, %class.btVector3** %e1.addr, align 4
  store %class.btVector3* %e2, %class.btVector3** %e2.addr, align 4
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %e2.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %e1.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %planenormal, %class.btVector3* %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %planenormal)
  %3 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %call1 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %planenormal)
  %arrayidx = getelementptr inbounds float, float* %call1, i32 0
  %call2 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %planenormal)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 1
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %planenormal)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 2
  %4 = load %class.btVector3*, %class.btVector3** %e2.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %planenormal)
  store float %call7, float* %ref.tmp6, align 4
  call void @_ZN9btVector48setValueERKfS1_S1_S1_(%class.btVector4* %3, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3, float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector48setValueERKfS1_S1_S1_(%class.btVector4* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #1 comdat {
entry:
  %this.addr = alloca %class.btVector4*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btVector4* %this, %class.btVector4** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btVector4*, %class.btVector4** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %3 = load float*, float** %_y.addr, align 4
  %4 = load float, float* %3, align 4
  %5 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %4, float* %arrayidx3, align 4
  %6 = load float*, float** %_z.addr, align 4
  %7 = load float, float* %6, align 4
  %8 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %7, float* %arrayidx5, align 4
  %9 = load float*, float** %_w.addr, align 4
  %10 = load float, float* %9, align 4
  %11 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %10, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z29bt_plane_clip_polygon_collectRK9btVector3S1_ffPS_Ri(%class.btVector3* nonnull align 4 dereferenceable(16) %point0, %class.btVector3* nonnull align 4 dereferenceable(16) %point1, float %dist0, float %dist1, %class.btVector3* %clipped, i32* nonnull align 4 dereferenceable(4) %clipped_count) #2 comdat {
entry:
  %point0.addr = alloca %class.btVector3*, align 4
  %point1.addr = alloca %class.btVector3*, align 4
  %dist0.addr = alloca float, align 4
  %dist1.addr = alloca float, align 4
  %clipped.addr = alloca %class.btVector3*, align 4
  %clipped_count.addr = alloca i32*, align 4
  %_prevclassif = alloca i8, align 1
  %_classif = alloca i8, align 1
  %blendfactor = alloca float, align 4
  store %class.btVector3* %point0, %class.btVector3** %point0.addr, align 4
  store %class.btVector3* %point1, %class.btVector3** %point1.addr, align 4
  store float %dist0, float* %dist0.addr, align 4
  store float %dist1, float* %dist1.addr, align 4
  store %class.btVector3* %clipped, %class.btVector3** %clipped.addr, align 4
  store i32* %clipped_count, i32** %clipped_count.addr, align 4
  %0 = load float, float* %dist0.addr, align 4
  %cmp = fcmp ogt float %0, 0x3E80000000000000
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %_prevclassif, align 1
  %1 = load float, float* %dist1.addr, align 4
  %cmp1 = fcmp ogt float %1, 0x3E80000000000000
  %frombool2 = zext i1 %cmp1 to i8
  store i8 %frombool2, i8* %_classif, align 1
  %2 = load i8, i8* %_classif, align 1
  %tobool = trunc i8 %2 to i1
  %conv = zext i1 %tobool to i32
  %3 = load i8, i8* %_prevclassif, align 1
  %tobool3 = trunc i8 %3 to i1
  %conv4 = zext i1 %tobool3 to i32
  %cmp5 = icmp ne i32 %conv, %conv4
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float, float* %dist0.addr, align 4
  %fneg = fneg float %4
  %5 = load float, float* %dist1.addr, align 4
  %6 = load float, float* %dist0.addr, align 4
  %sub = fsub float %5, %6
  %div = fdiv float %fneg, %sub
  store float %div, float* %blendfactor, align 4
  %7 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  %8 = load i32*, i32** %clipped_count.addr, align 4
  %9 = load i32, i32* %8, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %9
  %10 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4
  %11 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4
  %12 = load float, float* %blendfactor, align 4
  call void @_Z12bt_vec_blendR9btVector3RKS_S2_f(%class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %10, %class.btVector3* nonnull align 4 dereferenceable(16) %11, float %12)
  %13 = load i32*, i32** %clipped_count.addr, align 4
  %14 = load i32, i32* %13, align 4
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %13, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %15 = load i8, i8* %_classif, align 1
  %tobool6 = trunc i8 %15 to i1
  br i1 %tobool6, label %if.end10, label %if.then7

if.then7:                                         ; preds = %if.end
  %16 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4
  %17 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  %18 = load i32*, i32** %clipped_count.addr, align 4
  %19 = load i32, i32* %18, align 4
  %arrayidx8 = getelementptr inbounds %class.btVector3, %class.btVector3* %17, i32 %19
  %20 = bitcast %class.btVector3* %arrayidx8 to i8*
  %21 = bitcast %class.btVector3* %16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false)
  %22 = load i32*, i32** %clipped_count.addr, align 4
  %23 = load i32, i32* %22, align 4
  %inc9 = add nsw i32 %23, 1
  store i32 %inc9, i32* %22, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.then7, %if.end
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z12bt_vec_blendR9btVector3RKS_S2_f(%class.btVector3* nonnull align 4 dereferenceable(16) %vr, %class.btVector3* nonnull align 4 dereferenceable(16) %va, %class.btVector3* nonnull align 4 dereferenceable(16) %vb, float %blend_factor) #2 comdat {
entry:
  %vr.addr = alloca %class.btVector3*, align 4
  %va.addr = alloca %class.btVector3*, align 4
  %vb.addr = alloca %class.btVector3*, align 4
  %blend_factor.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  store %class.btVector3* %vr, %class.btVector3** %vr.addr, align 4
  store %class.btVector3* %va, %class.btVector3** %va.addr, align 4
  store %class.btVector3* %vb, %class.btVector3** %vb.addr, align 4
  store float %blend_factor, float* %blend_factor.addr, align 4
  %0 = load float, float* %blend_factor.addr, align 4
  %sub = fsub float 1.000000e+00, %0
  store float %sub, float* %ref.tmp2, align 4
  %1 = load %class.btVector3*, %class.btVector3** %va.addr, align 4
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = load %class.btVector3*, %class.btVector3** %vb.addr, align 4
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, float* nonnull align 4 dereferenceable(4) %blend_factor.addr, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %3 = load %class.btVector3*, %class.btVector3** %vr.addr, align 4
  %4 = bitcast %class.btVector3* %3 to i8*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btTriangleShapeEx.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { nounwind readnone speculatable willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
