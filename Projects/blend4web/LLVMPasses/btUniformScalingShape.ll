; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btUniformScalingShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btUniformScalingShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btUniformScalingShape = type { %class.btConvexShape, %class.btConvexShape*, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btVector3 = type { [4 x float] }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btSerializer = type opaque

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN21btUniformScalingShapedlEPv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK21btUniformScalingShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZNK16btCollisionShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV21btUniformScalingShape = hidden unnamed_addr constant { [25 x i8*] } { [25 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI21btUniformScalingShape to i8*), i8* bitcast (%class.btUniformScalingShape* (%class.btUniformScalingShape*)* @_ZN21btUniformScalingShapeD1Ev to i8*), i8* bitcast (void (%class.btUniformScalingShape*)* @_ZN21btUniformScalingShapeD0Ev to i8*), i8* bitcast (void (%class.btUniformScalingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btUniformScalingShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btUniformScalingShape*, %class.btVector3*)* @_ZN21btUniformScalingShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btUniformScalingShape*)* @_ZNK21btUniformScalingShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btUniformScalingShape*, float, %class.btVector3*)* @_ZNK21btUniformScalingShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btUniformScalingShape*)* @_ZNK21btUniformScalingShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btUniformScalingShape*, float)* @_ZN21btUniformScalingShape9setMarginEf to i8*), i8* bitcast (float (%class.btUniformScalingShape*)* @_ZNK21btUniformScalingShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btCollisionShape*)* @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)* @_ZNK16btCollisionShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btUniformScalingShape*, %class.btVector3*)* @_ZNK21btUniformScalingShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btUniformScalingShape*, %class.btVector3*)* @_ZNK21btUniformScalingShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*, %class.btVector3*, %class.btVector3*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_RS3_S7_ to i8*), i8* bitcast (void (%class.btUniformScalingShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK21btUniformScalingShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btUniformScalingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btUniformScalingShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btUniformScalingShape*)* @_ZNK21btUniformScalingShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btUniformScalingShape*, i32, %class.btVector3*)* @_ZNK21btUniformScalingShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS21btUniformScalingShape = hidden constant [24 x i8] c"21btUniformScalingShape\00", align 1
@_ZTI13btConvexShape = external constant i8*
@_ZTI21btUniformScalingShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @_ZTS21btUniformScalingShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI13btConvexShape to i8*) }, align 4
@.str = private unnamed_addr constant [20 x i8] c"UniformScalingShape\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btUniformScalingShape.cpp, i8* null }]

@_ZN21btUniformScalingShapeC1EP13btConvexShapef = hidden unnamed_addr alias %class.btUniformScalingShape* (%class.btUniformScalingShape*, %class.btConvexShape*, float), %class.btUniformScalingShape* (%class.btUniformScalingShape*, %class.btConvexShape*, float)* @_ZN21btUniformScalingShapeC2EP13btConvexShapef
@_ZN21btUniformScalingShapeD1Ev = hidden unnamed_addr alias %class.btUniformScalingShape* (%class.btUniformScalingShape*), %class.btUniformScalingShape* (%class.btUniformScalingShape*)* @_ZN21btUniformScalingShapeD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btUniformScalingShape* @_ZN21btUniformScalingShapeC2EP13btConvexShapef(%class.btUniformScalingShape* returned %this, %class.btConvexShape* %convexChildShape, float %uniformScalingFactor) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %convexChildShape.addr = alloca %class.btConvexShape*, align 4
  %uniformScalingFactor.addr = alloca float, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4
  store %class.btConvexShape* %convexChildShape, %class.btConvexShape** %convexChildShape.addr, align 4
  store float %uniformScalingFactor, float* %uniformScalingFactor.addr, align 4
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %0 = bitcast %class.btUniformScalingShape* %this1 to %class.btConvexShape*
  %call = call %class.btConvexShape* @_ZN13btConvexShapeC2Ev(%class.btConvexShape* %0)
  %1 = bitcast %class.btUniformScalingShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [25 x i8*] }, { [25 x i8*] }* @_ZTV21btUniformScalingShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %2 = load %class.btConvexShape*, %class.btConvexShape** %convexChildShape.addr, align 4
  store %class.btConvexShape* %2, %class.btConvexShape** %m_childConvexShape, align 4
  %m_uniformScalingFactor = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 2
  %3 = load float, float* %uniformScalingFactor.addr, align 4
  store float %3, float* %m_uniformScalingFactor, align 4
  %4 = bitcast %class.btUniformScalingShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %4, i32 0, i32 1
  store i32 14, i32* %m_shapeType, align 4
  ret %class.btUniformScalingShape* %this1
}

declare %class.btConvexShape* @_ZN13btConvexShapeC2Ev(%class.btConvexShape* returned) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define hidden %class.btUniformScalingShape* @_ZN21btUniformScalingShapeD2Ev(%class.btUniformScalingShape* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %0 = bitcast %class.btUniformScalingShape* %this1 to %class.btConvexShape*
  %call = call %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* %0) #6
  ret %class.btUniformScalingShape* %this1
}

; Function Attrs: nounwind
declare %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN21btUniformScalingShapeD0Ev(%class.btUniformScalingShape* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %call = call %class.btUniformScalingShape* @_ZN21btUniformScalingShapeD1Ev(%class.btUniformScalingShape* %this1) #6
  %0 = bitcast %class.btUniformScalingShape* %this1 to i8*
  call void @_ZN21btUniformScalingShapedlEPv(i8* %0) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btUniformScalingShapedlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK21btUniformScalingShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btUniformScalingShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %vec.addr = alloca %class.btVector3*, align 4
  %tmpVertex = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4
  store %class.btVector3* %vec, %class.btVector3** %vec.addr, align 4
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmpVertex)
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4
  %2 = bitcast %class.btConvexShape* %0 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)***
  %vtable = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*** %2, align 4
  %vfn = getelementptr inbounds void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vtable, i64 17
  %3 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vfn, align 4
  call void %3(%class.btVector3* sret align 4 %ref.tmp, %class.btConvexShape* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %4 = bitcast %class.btVector3* %tmpVertex to i8*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %m_uniformScalingFactor = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpVertex, float* nonnull align 4 dereferenceable(4) %m_uniformScalingFactor)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK21btUniformScalingShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btUniformScalingShape* %this, %class.btVector3* %vectors, %class.btVector3* %supportVerticesOut, i32 %numVectors) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %vectors.addr = alloca %class.btVector3*, align 4
  %supportVerticesOut.addr = alloca %class.btVector3*, align 4
  %numVectors.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4
  store %class.btVector3* %vectors, %class.btVector3** %vectors.addr, align 4
  store %class.btVector3* %supportVerticesOut, %class.btVector3** %supportVerticesOut.addr, align 4
  store i32 %numVectors, i32* %numVectors.addr, align 4
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = load %class.btVector3*, %class.btVector3** %vectors.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4
  %3 = load i32, i32* %numVectors.addr, align 4
  %4 = bitcast %class.btConvexShape* %0 to void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)***
  %vtable = load void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)**, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*** %4, align 4
  %vfn = getelementptr inbounds void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)** %vtable, i64 19
  %5 = load void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)** %vfn, align 4
  call void %5(%class.btConvexShape* %0, %class.btVector3* %1, %class.btVector3* %2, i32 %3)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4
  %7 = load i32, i32* %numVectors.addr, align 4
  %cmp = icmp slt i32 %6, %7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4
  %9 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  %m_uniformScalingFactor = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, float* nonnull align 4 dereferenceable(4) %m_uniformScalingFactor)
  %10 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4
  %11 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 %11
  %12 = bitcast %class.btVector3* %arrayidx2 to i8*
  %13 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %i, align 4
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK21btUniformScalingShape24localGetSupportingVertexERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btUniformScalingShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %vec.addr = alloca %class.btVector3*, align 4
  %tmpVertex = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4
  store %class.btVector3* %vec, %class.btVector3** %vec.addr, align 4
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmpVertex)
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4
  %2 = bitcast %class.btConvexShape* %0 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)***
  %vtable = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*** %2, align 4
  %vfn = getelementptr inbounds void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vtable, i64 16
  %3 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vfn, align 4
  call void %3(%class.btVector3* sret align 4 %ref.tmp, %class.btConvexShape* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %4 = bitcast %class.btVector3* %tmpVertex to i8*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %m_uniformScalingFactor = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpVertex, float* nonnull align 4 dereferenceable(4) %m_uniformScalingFactor)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK21btUniformScalingShape21calculateLocalInertiaEfR9btVector3(%class.btUniformScalingShape* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %tmpInertia = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4
  store float %mass, float* %mass.addr, align 4
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmpInertia)
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = bitcast %class.btConvexShape* %0 to %class.btCollisionShape*
  %2 = load float, float* %mass.addr, align 4
  %3 = bitcast %class.btCollisionShape* %1 to void (%class.btCollisionShape*, float, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, float, %class.btVector3*)**, void (%class.btCollisionShape*, float, %class.btVector3*)*** %3, align 4
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, float, %class.btVector3*)*, void (%class.btCollisionShape*, float, %class.btVector3*)** %vtable, i64 8
  %4 = load void (%class.btCollisionShape*, float, %class.btVector3*)*, void (%class.btCollisionShape*, float, %class.btVector3*)** %vfn, align 4
  call void %4(%class.btCollisionShape* %1, float %2, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpInertia)
  %m_uniformScalingFactor = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpInertia, float* nonnull align 4 dereferenceable(4) %m_uniformScalingFactor)
  %5 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %6 = bitcast %class.btVector3* %5 to i8*
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK21btUniformScalingShape7getAabbERK11btTransformR9btVector3S4_(%class.btUniformScalingShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %3 = bitcast %class.btUniformScalingShape* %this1 to void (%class.btUniformScalingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btUniformScalingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btUniformScalingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %3, align 4
  %vfn = getelementptr inbounds void (%class.btUniformScalingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btUniformScalingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 20
  %4 = load void (%class.btUniformScalingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btUniformScalingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %4(%class.btUniformScalingShape* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK21btUniformScalingShape11getAabbSlowERK11btTransformR9btVector3S4_(%class.btUniformScalingShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %_directions = alloca [6 x %class.btVector3], align 16
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %_supporting = alloca [6 x %class.btVector3], align 16
  %ref.tmp29 = alloca float, align 4
  %ref.tmp30 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp35 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  %ref.tmp41 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp46 = alloca float, align 4
  %ref.tmp49 = alloca float, align 4
  %ref.tmp50 = alloca float, align 4
  %ref.tmp51 = alloca float, align 4
  %ref.tmp54 = alloca float, align 4
  %ref.tmp55 = alloca float, align 4
  %ref.tmp56 = alloca float, align 4
  %i = alloca i32, align 4
  %ref.tmp58 = alloca %class.btVector3, align 4
  %aabbMin1 = alloca %class.btVector3, align 4
  %ref.tmp62 = alloca float, align 4
  %ref.tmp63 = alloca float, align 4
  %ref.tmp64 = alloca float, align 4
  %aabbMax1 = alloca %class.btVector3, align 4
  %ref.tmp66 = alloca float, align 4
  %ref.tmp67 = alloca float, align 4
  %ref.tmp68 = alloca float, align 4
  %i70 = alloca i32, align 4
  %ref.tmp74 = alloca %class.btVector3, align 4
  %ref.tmp80 = alloca %class.btVector3, align 4
  %marginVec = alloca %class.btVector3, align 4
  %ref.tmp89 = alloca float, align 4
  %ref.tmp93 = alloca float, align 4
  %ref.tmp97 = alloca float, align 4
  %ref.tmp102 = alloca %class.btVector3, align 4
  %ref.tmp103 = alloca %class.btVector3, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %arrayinit.begin = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_directions, i32 0, i32 0
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.begin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %arrayinit.element = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.begin, i32 1
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %arrayinit.element8 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element, i32 1
  store float 0.000000e+00, float* %ref.tmp9, align 4
  store float 0.000000e+00, float* %ref.tmp10, align 4
  store float 1.000000e+00, float* %ref.tmp11, align 4
  %call12 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element8, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  %arrayinit.element13 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element8, i32 1
  store float -1.000000e+00, float* %ref.tmp14, align 4
  store float 0.000000e+00, float* %ref.tmp15, align 4
  store float 0.000000e+00, float* %ref.tmp16, align 4
  %call17 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16)
  %arrayinit.element18 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element13, i32 1
  store float 0.000000e+00, float* %ref.tmp19, align 4
  store float -1.000000e+00, float* %ref.tmp20, align 4
  store float 0.000000e+00, float* %ref.tmp21, align 4
  %call22 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element18, float* nonnull align 4 dereferenceable(4) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %arrayinit.element23 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element18, i32 1
  store float 0.000000e+00, float* %ref.tmp24, align 4
  store float 0.000000e+00, float* %ref.tmp25, align 4
  store float -1.000000e+00, float* %ref.tmp26, align 4
  %call27 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element23, float* nonnull align 4 dereferenceable(4) %ref.tmp24, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp26)
  %arrayinit.begin28 = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_supporting, i32 0, i32 0
  store float 0.000000e+00, float* %ref.tmp29, align 4
  store float 0.000000e+00, float* %ref.tmp30, align 4
  store float 0.000000e+00, float* %ref.tmp31, align 4
  %call32 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.begin28, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp30, float* nonnull align 4 dereferenceable(4) %ref.tmp31)
  %arrayinit.element33 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.begin28, i32 1
  store float 0.000000e+00, float* %ref.tmp34, align 4
  store float 0.000000e+00, float* %ref.tmp35, align 4
  store float 0.000000e+00, float* %ref.tmp36, align 4
  %call37 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element33, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp35, float* nonnull align 4 dereferenceable(4) %ref.tmp36)
  %arrayinit.element38 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element33, i32 1
  store float 0.000000e+00, float* %ref.tmp39, align 4
  store float 0.000000e+00, float* %ref.tmp40, align 4
  store float 0.000000e+00, float* %ref.tmp41, align 4
  %call42 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element38, float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp40, float* nonnull align 4 dereferenceable(4) %ref.tmp41)
  %arrayinit.element43 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element38, i32 1
  store float 0.000000e+00, float* %ref.tmp44, align 4
  store float 0.000000e+00, float* %ref.tmp45, align 4
  store float 0.000000e+00, float* %ref.tmp46, align 4
  %call47 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element43, float* nonnull align 4 dereferenceable(4) %ref.tmp44, float* nonnull align 4 dereferenceable(4) %ref.tmp45, float* nonnull align 4 dereferenceable(4) %ref.tmp46)
  %arrayinit.element48 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element43, i32 1
  store float 0.000000e+00, float* %ref.tmp49, align 4
  store float 0.000000e+00, float* %ref.tmp50, align 4
  store float 0.000000e+00, float* %ref.tmp51, align 4
  %call52 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element48, float* nonnull align 4 dereferenceable(4) %ref.tmp49, float* nonnull align 4 dereferenceable(4) %ref.tmp50, float* nonnull align 4 dereferenceable(4) %ref.tmp51)
  %arrayinit.element53 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element48, i32 1
  store float 0.000000e+00, float* %ref.tmp54, align 4
  store float 0.000000e+00, float* %ref.tmp55, align 4
  store float 0.000000e+00, float* %ref.tmp56, align 4
  %call57 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element53, float* nonnull align 4 dereferenceable(4) %ref.tmp54, float* nonnull align 4 dereferenceable(4) %ref.tmp55, float* nonnull align 4 dereferenceable(4) %ref.tmp56)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_directions, i32 0, i32 %1
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %call59 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %2)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp58, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call59)
  %3 = load i32, i32* %i, align 4
  %arrayidx60 = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_directions, i32 0, i32 %3
  %4 = bitcast %class.btVector3* %arrayidx60 to i8*
  %5 = bitcast %class.btVector3* %ref.tmp58 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %4, i8* align 4 %5, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_directions, i32 0, i32 0
  %arraydecay61 = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_supporting, i32 0, i32 0
  %7 = bitcast %class.btUniformScalingShape* %this1 to void (%class.btUniformScalingShape*, %class.btVector3*, %class.btVector3*, i32)***
  %vtable = load void (%class.btUniformScalingShape*, %class.btVector3*, %class.btVector3*, i32)**, void (%class.btUniformScalingShape*, %class.btVector3*, %class.btVector3*, i32)*** %7, align 4
  %vfn = getelementptr inbounds void (%class.btUniformScalingShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btUniformScalingShape*, %class.btVector3*, %class.btVector3*, i32)** %vtable, i64 19
  %8 = load void (%class.btUniformScalingShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btUniformScalingShape*, %class.btVector3*, %class.btVector3*, i32)** %vfn, align 4
  call void %8(%class.btUniformScalingShape* %this1, %class.btVector3* %arraydecay, %class.btVector3* %arraydecay61, i32 6)
  store float 0.000000e+00, float* %ref.tmp62, align 4
  store float 0.000000e+00, float* %ref.tmp63, align 4
  store float 0.000000e+00, float* %ref.tmp64, align 4
  %call65 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %aabbMin1, float* nonnull align 4 dereferenceable(4) %ref.tmp62, float* nonnull align 4 dereferenceable(4) %ref.tmp63, float* nonnull align 4 dereferenceable(4) %ref.tmp64)
  store float 0.000000e+00, float* %ref.tmp66, align 4
  store float 0.000000e+00, float* %ref.tmp67, align 4
  store float 0.000000e+00, float* %ref.tmp68, align 4
  %call69 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %aabbMax1, float* nonnull align 4 dereferenceable(4) %ref.tmp66, float* nonnull align 4 dereferenceable(4) %ref.tmp67, float* nonnull align 4 dereferenceable(4) %ref.tmp68)
  store i32 0, i32* %i70, align 4
  br label %for.cond71

for.cond71:                                       ; preds = %for.inc86, %for.end
  %9 = load i32, i32* %i70, align 4
  %cmp72 = icmp slt i32 %9, 3
  br i1 %cmp72, label %for.body73, label %for.end88

for.body73:                                       ; preds = %for.cond71
  %10 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %11 = load i32, i32* %i70, align 4
  %arrayidx75 = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_supporting, i32 0, i32 %11
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp74, %class.btTransform* %10, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx75)
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ref.tmp74)
  %12 = load i32, i32* %i70, align 4
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 %12
  %13 = load float, float* %arrayidx77, align 4
  %call78 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %aabbMax1)
  %14 = load i32, i32* %i70, align 4
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 %14
  store float %13, float* %arrayidx79, align 4
  %15 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %16 = load i32, i32* %i70, align 4
  %add = add nsw i32 %16, 3
  %arrayidx81 = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_supporting, i32 0, i32 %add
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp80, %class.btTransform* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx81)
  %call82 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ref.tmp80)
  %17 = load i32, i32* %i70, align 4
  %arrayidx83 = getelementptr inbounds float, float* %call82, i32 %17
  %18 = load float, float* %arrayidx83, align 4
  %call84 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %aabbMin1)
  %19 = load i32, i32* %i70, align 4
  %arrayidx85 = getelementptr inbounds float, float* %call84, i32 %19
  store float %18, float* %arrayidx85, align 4
  br label %for.inc86

for.inc86:                                        ; preds = %for.body73
  %20 = load i32, i32* %i70, align 4
  %inc87 = add nsw i32 %20, 1
  store i32 %inc87, i32* %i70, align 4
  br label %for.cond71

for.end88:                                        ; preds = %for.cond71
  %21 = bitcast %class.btUniformScalingShape* %this1 to float (%class.btUniformScalingShape*)***
  %vtable90 = load float (%class.btUniformScalingShape*)**, float (%class.btUniformScalingShape*)*** %21, align 4
  %vfn91 = getelementptr inbounds float (%class.btUniformScalingShape*)*, float (%class.btUniformScalingShape*)** %vtable90, i64 12
  %22 = load float (%class.btUniformScalingShape*)*, float (%class.btUniformScalingShape*)** %vfn91, align 4
  %call92 = call float %22(%class.btUniformScalingShape* %this1)
  store float %call92, float* %ref.tmp89, align 4
  %23 = bitcast %class.btUniformScalingShape* %this1 to float (%class.btUniformScalingShape*)***
  %vtable94 = load float (%class.btUniformScalingShape*)**, float (%class.btUniformScalingShape*)*** %23, align 4
  %vfn95 = getelementptr inbounds float (%class.btUniformScalingShape*)*, float (%class.btUniformScalingShape*)** %vtable94, i64 12
  %24 = load float (%class.btUniformScalingShape*)*, float (%class.btUniformScalingShape*)** %vfn95, align 4
  %call96 = call float %24(%class.btUniformScalingShape* %this1)
  store float %call96, float* %ref.tmp93, align 4
  %25 = bitcast %class.btUniformScalingShape* %this1 to float (%class.btUniformScalingShape*)***
  %vtable98 = load float (%class.btUniformScalingShape*)**, float (%class.btUniformScalingShape*)*** %25, align 4
  %vfn99 = getelementptr inbounds float (%class.btUniformScalingShape*)*, float (%class.btUniformScalingShape*)** %vtable98, i64 12
  %26 = load float (%class.btUniformScalingShape*)*, float (%class.btUniformScalingShape*)** %vfn99, align 4
  %call100 = call float %26(%class.btUniformScalingShape* %this1)
  store float %call100, float* %ref.tmp97, align 4
  %call101 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %marginVec, float* nonnull align 4 dereferenceable(4) %ref.tmp89, float* nonnull align 4 dereferenceable(4) %ref.tmp93, float* nonnull align 4 dereferenceable(4) %ref.tmp97)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp102, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %marginVec)
  %27 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %28 = bitcast %class.btVector3* %27 to i8*
  %29 = bitcast %class.btVector3* %ref.tmp102 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp103, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1, %class.btVector3* nonnull align 4 dereferenceable(16) %marginVec)
  %30 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %31 = bitcast %class.btVector3* %30 to i8*
  %32 = bitcast %class.btVector3* %ref.tmp103 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %31, i8* align 4 %32, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call2, float* %ref.tmp1, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp3, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btUniformScalingShape15setLocalScalingERK9btVector3(%class.btUniformScalingShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4
  %2 = bitcast %class.btConvexShape* %0 to void (%class.btConvexShape*, %class.btVector3*)***
  %vtable = load void (%class.btConvexShape*, %class.btVector3*)**, void (%class.btConvexShape*, %class.btVector3*)*** %2, align 4
  %vfn = getelementptr inbounds void (%class.btConvexShape*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btVector3*)** %vtable, i64 6
  %3 = load void (%class.btConvexShape*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btVector3*)** %vfn, align 4
  call void %3(%class.btConvexShape* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret void
}

; Function Attrs: noinline optnone
define hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btUniformScalingShape15getLocalScalingEv(%class.btUniformScalingShape* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = bitcast %class.btConvexShape* %0 to %class.btVector3* (%class.btConvexShape*)***
  %vtable = load %class.btVector3* (%class.btConvexShape*)**, %class.btVector3* (%class.btConvexShape*)*** %1, align 4
  %vfn = getelementptr inbounds %class.btVector3* (%class.btConvexShape*)*, %class.btVector3* (%class.btConvexShape*)** %vtable, i64 7
  %2 = load %class.btVector3* (%class.btConvexShape*)*, %class.btVector3* (%class.btConvexShape*)** %vfn, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* %2(%class.btConvexShape* %0)
  ret %class.btVector3* %call
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btUniformScalingShape9setMarginEf(%class.btUniformScalingShape* %this, float %margin) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %margin.addr = alloca float, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4
  store float %margin, float* %margin.addr, align 4
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = load float, float* %margin.addr, align 4
  %2 = bitcast %class.btConvexShape* %0 to void (%class.btConvexShape*, float)***
  %vtable = load void (%class.btConvexShape*, float)**, void (%class.btConvexShape*, float)*** %2, align 4
  %vfn = getelementptr inbounds void (%class.btConvexShape*, float)*, void (%class.btConvexShape*, float)** %vtable, i64 11
  %3 = load void (%class.btConvexShape*, float)*, void (%class.btConvexShape*, float)** %vfn, align 4
  call void %3(%class.btConvexShape* %0, float %1)
  ret void
}

; Function Attrs: noinline optnone
define hidden float @_ZNK21btUniformScalingShape9getMarginEv(%class.btUniformScalingShape* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = bitcast %class.btConvexShape* %0 to float (%class.btConvexShape*)***
  %vtable = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %1, align 4
  %vfn = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable, i64 12
  %2 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn, align 4
  %call = call float %2(%class.btConvexShape* %0)
  %m_uniformScalingFactor = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 2
  %3 = load float, float* %m_uniformScalingFactor, align 4
  %mul = fmul float %call, %3
  ret float %mul
}

; Function Attrs: noinline optnone
define hidden i32 @_ZNK21btUniformScalingShape36getNumPreferredPenetrationDirectionsEv(%class.btUniformScalingShape* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = bitcast %class.btConvexShape* %0 to i32 (%class.btConvexShape*)***
  %vtable = load i32 (%class.btConvexShape*)**, i32 (%class.btConvexShape*)*** %1, align 4
  %vfn = getelementptr inbounds i32 (%class.btConvexShape*)*, i32 (%class.btConvexShape*)** %vtable, i64 21
  %2 = load i32 (%class.btConvexShape*)*, i32 (%class.btConvexShape*)** %vfn, align 4
  %call = call i32 %2(%class.btConvexShape* %0)
  ret i32 %call
}

; Function Attrs: noinline optnone
define hidden void @_ZNK21btUniformScalingShape32getPreferredPenetrationDirectionEiR9btVector3(%class.btUniformScalingShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %penetrationVector) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  %index.addr = alloca i32, align 4
  %penetrationVector.addr = alloca %class.btVector3*, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store %class.btVector3* %penetrationVector, %class.btVector3** %penetrationVector.addr, align 4
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btUniformScalingShape, %class.btUniformScalingShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = load i32, i32* %index.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4
  %3 = bitcast %class.btConvexShape* %0 to void (%class.btConvexShape*, i32, %class.btVector3*)***
  %vtable = load void (%class.btConvexShape*, i32, %class.btVector3*)**, void (%class.btConvexShape*, i32, %class.btVector3*)*** %3, align 4
  %vfn = getelementptr inbounds void (%class.btConvexShape*, i32, %class.btVector3*)*, void (%class.btConvexShape*, i32, %class.btVector3*)** %vtable, i64 22
  %4 = load void (%class.btConvexShape*, i32, %class.btVector3*)*, void (%class.btConvexShape*, i32, %class.btVector3*)** %vfn, align 4
  call void %4(%class.btConvexShape* %0, i32 %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #3

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #3

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK21btUniformScalingShape7getNameEv(%class.btUniformScalingShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btUniformScalingShape*, align 4
  store %class.btUniformScalingShape* %this, %class.btUniformScalingShape** %this.addr, align 4
  %this1 = load %class.btUniformScalingShape*, %class.btUniformScalingShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  store float 1.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv(%class.btCollisionShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  ret i32 12
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #3

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #3

declare void @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_RS3_S7_(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float* nonnull align 4 dereferenceable(4), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btUniformScalingShape.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
