; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btOptimizedBvh.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btOptimizedBvh.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btOptimizedBvh = type { %class.btQuantizedBvh }
%class.btQuantizedBvh = type { i32 (...)**, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32, i8, [3 x i8], %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0, i32, %class.btAlignedObjectArray.4, i32 }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btOptimizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btOptimizedBvhNode = type { %class.btVector3, %class.btVector3, i32, i32, i32, [20 x i8] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btQuantizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btQuantizedBvhNode = type { [3 x i16], [3 x i16], i32 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btBvhSubtreeInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btBvhSubtreeInfo = type { [3 x i16], [3 x i16], i32, i32, [3 x i32] }
%class.btStridingMeshInterface = type { i32 (...)**, %class.btVector3 }
%struct.QuantizedNodeTriangleCallback = type { %class.btInternalTriangleIndexCallback, %class.btAlignedObjectArray.0*, %class.btQuantizedBvh* }
%class.btInternalTriangleIndexCallback = type { i32 (...)** }
%struct.NodeTriangleCallback = type { %class.btInternalTriangleIndexCallback, %class.btAlignedObjectArray* }
%class.btSerializer = type opaque
%struct.btQuantizedBvhFloatData = type { %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, i32, i32, i32, i32, %struct.btOptimizedBvhNodeFloatData*, %struct.btQuantizedBvhNodeData*, %struct.btBvhSubtreeInfoData*, i32, i32 }
%struct.btVector3FloatData = type { [4 x float] }
%struct.btOptimizedBvhNodeFloatData = type { %struct.btVector3FloatData, %struct.btVector3FloatData, i32, i32, i32, [4 x i8] }
%struct.btQuantizedBvhNodeData = type { [3 x i16], [3 x i16], i32 }
%struct.btBvhSubtreeInfoData = type { i32, i32, [3 x i16], [3 x i16] }
%struct.btQuantizedBvhDoubleData = type { %struct.btVector3DoubleData, %struct.btVector3DoubleData, %struct.btVector3DoubleData, i32, i32, i32, i32, %struct.btOptimizedBvhNodeDoubleData*, %struct.btQuantizedBvhNodeData*, i32, i32, %struct.btBvhSubtreeInfoData* }
%struct.btVector3DoubleData = type { [4 x double] }
%struct.btOptimizedBvhNodeDoubleData = type { %struct.btVector3DoubleData, %struct.btVector3DoubleData, i32, i32, i32, [4 x i8] }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN14btOptimizedBvhdlEPv = comdat any

$_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE6resizeEiRKS0_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE6resizeEiRKS0_ = comdat any

$_ZN18btOptimizedBvhNodeC2Ev = comdat any

$_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_ = comdat any

$_ZN16btBvhSubtreeInfoC2Ev = comdat any

$_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi = comdat any

$_ZNK18btQuantizedBvhNode10isLeafNodeEv = comdat any

$_ZNK18btQuantizedBvhNode14getEscapeIndexEv = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi = comdat any

$_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i = comdat any

$_Z37testQuantizedAabbAgainstQuantizedAabbPKtS0_S0_S0_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK23btStridingMeshInterface10getScalingEv = comdat any

$_ZNK18btQuantizedBvhNode9getPartIdEv = comdat any

$_ZNK18btQuantizedBvhNode16getTriangleIndexEv = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN9btVector36setMinERKS_ = comdat any

$_ZN9btVector36setMaxERKS_ = comdat any

$_ZNK14btQuantizedBvh31calculateSerializeBufferSizeNewEv = comdat any

$_ZNK14btOptimizedBvh16serializeInPlaceEPvjb = comdat any

$_ZN31btInternalTriangleIndexCallbackC2Ev = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZN9btVector34setXEf = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZN9btVector34setYEf = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN9btVector34setZEf = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9push_backERKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9allocSizeEi = comdat any

$_ZN18btQuantizedBvhNodenwEmPv = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE9push_backERKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE9allocSizeEi = comdat any

$_ZN18btOptimizedBvhNodenwEmPv = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE10deallocateEPS0_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_Z8btSelectjii = comdat any

$_Z8btSetMinIfEvRT_RKS0_ = comdat any

$_ZNK9btVector31wEv = comdat any

$_Z8btSetMaxIfEvRT_RKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE9allocSizeEi = comdat any

$_ZN16btBvhSubtreeInfonwEmPv = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE4initEv = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE4initEv = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV14btOptimizedBvh = hidden unnamed_addr constant { [10 x i8*] } { [10 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI14btOptimizedBvh to i8*), i8* bitcast (%class.btOptimizedBvh* (%class.btOptimizedBvh*)* @_ZN14btOptimizedBvhD1Ev to i8*), i8* bitcast (void (%class.btOptimizedBvh*)* @_ZN14btOptimizedBvhD0Ev to i8*), i8* bitcast (i1 (%class.btQuantizedBvh*, i8*, i32, i1)* @_ZNK14btQuantizedBvh9serializeEPvjb to i8*), i8* bitcast (i32 (%class.btQuantizedBvh*)* @_ZNK14btQuantizedBvh31calculateSerializeBufferSizeNewEv to i8*), i8* bitcast (i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)* @_ZNK14btQuantizedBvh9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btQuantizedBvh*, %struct.btQuantizedBvhFloatData*)* @_ZN14btQuantizedBvh16deSerializeFloatER23btQuantizedBvhFloatData to i8*), i8* bitcast (void (%class.btQuantizedBvh*, %struct.btQuantizedBvhDoubleData*)* @_ZN14btQuantizedBvh17deSerializeDoubleER24btQuantizedBvhDoubleData to i8*), i8* bitcast (i1 (%class.btOptimizedBvh*, i8*, i32, i1)* @_ZNK14btOptimizedBvh16serializeInPlaceEPvjb to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS14btOptimizedBvh = hidden constant [17 x i8] c"14btOptimizedBvh\00", align 1
@_ZTI14btQuantizedBvh = external constant i8*
@_ZTI14btOptimizedBvh = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([17 x i8], [17 x i8]* @_ZTS14btOptimizedBvh, i32 0, i32 0), i8* bitcast (i8** @_ZTI14btQuantizedBvh to i8*) }, align 4
@_ZTVZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback to i8*), i8* bitcast (%struct.QuantizedNodeTriangleCallback* (%struct.QuantizedNodeTriangleCallback*)* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallbackD2Ev to i8*), i8* bitcast (void (%struct.QuantizedNodeTriangleCallback*)* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallbackD0Ev to i8*), i8* bitcast (void (%struct.QuantizedNodeTriangleCallback*, %class.btVector3*, i32, i32)* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallback28internalProcessTriangleIndexEPS2_ii to i8*)] }, align 4
@_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback = internal constant [100 x i8] c"ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback\00", align 1
@_ZTI31btInternalTriangleIndexCallback = external constant i8*
@_ZTIZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([100 x i8], [100 x i8]* @_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback, i32 0, i32 0), i8* bitcast (i8** @_ZTI31btInternalTriangleIndexCallback to i8*) }, align 4
@_ZTV31btInternalTriangleIndexCallback = external unnamed_addr constant { [5 x i8*] }, align 4
@_ZTVZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback to i8*), i8* bitcast (%struct.NodeTriangleCallback* (%struct.NodeTriangleCallback*)* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallbackD2Ev to i8*), i8* bitcast (void (%struct.NodeTriangleCallback*)* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallbackD0Ev to i8*), i8* bitcast (void (%struct.NodeTriangleCallback*, %class.btVector3*, i32, i32)* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallback28internalProcessTriangleIndexEPS2_ii to i8*)] }, align 4
@_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback = internal constant [91 x i8] c"ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback\00", align 1
@_ZTIZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([91 x i8], [91 x i8]* @_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback, i32 0, i32 0), i8* bitcast (i8** @_ZTI31btInternalTriangleIndexCallback to i8*) }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btOptimizedBvh.cpp, i8* null }]

@_ZN14btOptimizedBvhC1Ev = hidden unnamed_addr alias %class.btOptimizedBvh* (%class.btOptimizedBvh*), %class.btOptimizedBvh* (%class.btOptimizedBvh*)* @_ZN14btOptimizedBvhC2Ev
@_ZN14btOptimizedBvhD1Ev = hidden unnamed_addr alias %class.btOptimizedBvh* (%class.btOptimizedBvh*), %class.btOptimizedBvh* (%class.btOptimizedBvh*)* @_ZN14btOptimizedBvhD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btOptimizedBvh* @_ZN14btOptimizedBvhC2Ev(%class.btOptimizedBvh* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btOptimizedBvh*, align 4
  store %class.btOptimizedBvh* %this, %class.btOptimizedBvh** %this.addr, align 4
  %this1 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %this.addr, align 4
  %0 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %call = call %class.btQuantizedBvh* @_ZN14btQuantizedBvhC2Ev(%class.btQuantizedBvh* %0)
  %1 = bitcast %class.btOptimizedBvh* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [10 x i8*] }, { [10 x i8*] }* @_ZTV14btOptimizedBvh, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %class.btOptimizedBvh* %this1
}

declare %class.btQuantizedBvh* @_ZN14btQuantizedBvhC2Ev(%class.btQuantizedBvh* returned) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define hidden %class.btOptimizedBvh* @_ZN14btOptimizedBvhD2Ev(%class.btOptimizedBvh* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btOptimizedBvh*, align 4
  store %class.btOptimizedBvh* %this, %class.btOptimizedBvh** %this.addr, align 4
  %this1 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %this.addr, align 4
  %0 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %call = call %class.btQuantizedBvh* @_ZN14btQuantizedBvhD2Ev(%class.btQuantizedBvh* %0) #8
  ret %class.btOptimizedBvh* %this1
}

; Function Attrs: nounwind
declare %class.btQuantizedBvh* @_ZN14btQuantizedBvhD2Ev(%class.btQuantizedBvh* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN14btOptimizedBvhD0Ev(%class.btOptimizedBvh* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btOptimizedBvh*, align 4
  store %class.btOptimizedBvh* %this, %class.btOptimizedBvh** %this.addr, align 4
  %this1 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %this.addr, align 4
  %call = call %class.btOptimizedBvh* @_ZN14btOptimizedBvhD1Ev(%class.btOptimizedBvh* %this1) #8
  %0 = bitcast %class.btOptimizedBvh* %this1 to i8*
  call void @_ZN14btOptimizedBvhdlEPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN14btOptimizedBvhdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_(%class.btOptimizedBvh* %this, %class.btStridingMeshInterface* %triangles, i1 zeroext %useQuantizedAabbCompression, %class.btVector3* nonnull align 4 dereferenceable(16) %bvhAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %bvhAabbMax) #2 {
entry:
  %this.addr = alloca %class.btOptimizedBvh*, align 4
  %triangles.addr = alloca %class.btStridingMeshInterface*, align 4
  %useQuantizedAabbCompression.addr = alloca i8, align 1
  %bvhAabbMin.addr = alloca %class.btVector3*, align 4
  %bvhAabbMax.addr = alloca %class.btVector3*, align 4
  %numLeafNodes = alloca i32, align 4
  %callback = alloca %struct.QuantizedNodeTriangleCallback, align 4
  %ref.tmp = alloca %struct.btQuantizedBvhNode, align 4
  %callback8 = alloca %struct.NodeTriangleCallback, align 4
  %aabbMin = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp23 = alloca %struct.btOptimizedBvhNode, align 4
  %subtree = alloca %class.btBvhSubtreeInfo*, align 4
  %ref.tmp32 = alloca %class.btBvhSubtreeInfo, align 4
  store %class.btOptimizedBvh* %this, %class.btOptimizedBvh** %this.addr, align 4
  store %class.btStridingMeshInterface* %triangles, %class.btStridingMeshInterface** %triangles.addr, align 4
  %frombool = zext i1 %useQuantizedAabbCompression to i8
  store i8 %frombool, i8* %useQuantizedAabbCompression.addr, align 1
  store %class.btVector3* %bvhAabbMin, %class.btVector3** %bvhAabbMin.addr, align 4
  store %class.btVector3* %bvhAabbMax, %class.btVector3** %bvhAabbMax.addr, align 4
  %this1 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %this.addr, align 4
  %0 = load i8, i8* %useQuantizedAabbCompression.addr, align 1
  %tobool = trunc i8 %0 to i1
  %1 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %1, i32 0, i32 6
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %m_useQuantization, align 4
  store i32 0, i32* %numLeafNodes, align 4
  %2 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_useQuantization3 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %2, i32 0, i32 6
  %3 = load i8, i8* %m_useQuantization3, align 4
  %tobool4 = trunc i8 %3 to i1
  br i1 %tobool4, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %5 = load %class.btVector3*, %class.btVector3** %bvhAabbMin.addr, align 4
  %6 = load %class.btVector3*, %class.btVector3** %bvhAabbMax.addr, align 4
  call void @_ZN14btQuantizedBvh21setQuantizationValuesERK9btVector3S2_f(%class.btQuantizedBvh* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6, float 1.000000e+00)
  %7 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedLeafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %7, i32 0, i32 10
  %8 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %call = call %struct.QuantizedNodeTriangleCallback* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallbackC2ER20btAlignedObjectArrayI18btQuantizedBvhNodeEPK14btQuantizedBvh(%struct.QuantizedNodeTriangleCallback* %callback, %class.btAlignedObjectArray.0* nonnull align 4 dereferenceable(17) %m_quantizedLeafNodes, %class.btQuantizedBvh* %8)
  %9 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %triangles.addr, align 4
  %10 = bitcast %struct.QuantizedNodeTriangleCallback* %callback to %class.btInternalTriangleIndexCallback*
  %11 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %11, i32 0, i32 1
  %12 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %12, i32 0, i32 2
  %13 = bitcast %class.btStridingMeshInterface* %9 to void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*** %13, align 4
  %vfn = getelementptr inbounds void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %14 = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %14(%class.btStridingMeshInterface* %9, %class.btInternalTriangleIndexCallback* %10, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMax)
  %15 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedLeafNodes5 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %15, i32 0, i32 10
  %call6 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes5)
  store i32 %call6, i32* %numLeafNodes, align 4
  %16 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %16, i32 0, i32 11
  %17 = load i32, i32* %numLeafNodes, align 4
  %mul = mul nsw i32 2, %17
  %18 = bitcast %struct.btQuantizedBvhNode* %ref.tmp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %18, i8 0, i32 16, i1 false)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %mul, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %ref.tmp)
  %call7 = call %struct.QuantizedNodeTriangleCallback* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallbackD2Ev(%struct.QuantizedNodeTriangleCallback* %callback) #8
  br label %if.end

if.else:                                          ; preds = %entry
  %19 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_leafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %19, i32 0, i32 8
  %call9 = call %struct.NodeTriangleCallback* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallbackC2ER20btAlignedObjectArrayI18btOptimizedBvhNodeE(%struct.NodeTriangleCallback* %callback8, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %m_leafNodes)
  store float 0xC3ABC16D60000000, float* %ref.tmp10, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp11, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp12, align 4
  %call13 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %aabbMin, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12)
  store float 0x43ABC16D60000000, float* %ref.tmp14, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp15, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp16, align 4
  %call17 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %aabbMax, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16)
  %20 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %triangles.addr, align 4
  %21 = bitcast %struct.NodeTriangleCallback* %callback8 to %class.btInternalTriangleIndexCallback*
  %22 = bitcast %class.btStridingMeshInterface* %20 to void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable18 = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*** %22, align 4
  %vfn19 = getelementptr inbounds void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vtable18, i64 2
  %23 = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vfn19, align 4
  call void %23(%class.btStridingMeshInterface* %20, %class.btInternalTriangleIndexCallback* %21, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  %24 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_leafNodes20 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %24, i32 0, i32 8
  %call21 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %m_leafNodes20)
  store i32 %call21, i32* %numLeafNodes, align 4
  %25 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %25, i32 0, i32 9
  %26 = load i32, i32* %numLeafNodes, align 4
  %mul22 = mul nsw i32 2, %26
  %27 = bitcast %struct.btOptimizedBvhNode* %ref.tmp23 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %27, i8 0, i32 64, i1 false)
  %call24 = call %struct.btOptimizedBvhNode* @_ZN18btOptimizedBvhNodeC2Ev(%struct.btOptimizedBvhNode* %ref.tmp23)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray* %m_contiguousNodes, i32 %mul22, %struct.btOptimizedBvhNode* nonnull align 4 dereferenceable(64) %ref.tmp23)
  %call25 = call %struct.NodeTriangleCallback* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallbackD2Ev(%struct.NodeTriangleCallback* %callback8) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %28 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %28, i32 0, i32 5
  store i32 0, i32* %m_curNodeIndex, align 4
  %29 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %30 = load i32, i32* %numLeafNodes, align 4
  call void @_ZN14btQuantizedBvh9buildTreeEii(%class.btQuantizedBvh* %29, i32 0, i32 %30)
  %31 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_useQuantization26 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %31, i32 0, i32 6
  %32 = load i8, i8* %m_useQuantization26, align 4
  %tobool27 = trunc i8 %32 to i1
  br i1 %tobool27, label %land.lhs.true, label %if.end43

land.lhs.true:                                    ; preds = %if.end
  %33 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %33, i32 0, i32 13
  %call28 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders)
  %tobool29 = icmp ne i32 %call28, 0
  br i1 %tobool29, label %if.end43, label %if.then30

if.then30:                                        ; preds = %land.lhs.true
  %34 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_SubtreeHeaders31 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %34, i32 0, i32 13
  %call33 = call %class.btBvhSubtreeInfo* @_ZN16btBvhSubtreeInfoC2Ev(%class.btBvhSubtreeInfo* %ref.tmp32)
  %call34 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_(%class.btAlignedObjectArray.4* %m_SubtreeHeaders31, %class.btBvhSubtreeInfo* nonnull align 4 dereferenceable(32) %ref.tmp32)
  store %class.btBvhSubtreeInfo* %call34, %class.btBvhSubtreeInfo** %subtree, align 4
  %35 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %36 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes35 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %36, i32 0, i32 11
  %call36 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes35, i32 0)
  call void @_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode(%class.btBvhSubtreeInfo* %35, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %call36)
  %37 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %m_rootNodeIndex = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %37, i32 0, i32 2
  store i32 0, i32* %m_rootNodeIndex, align 4
  %38 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes37 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %38, i32 0, i32 11
  %call38 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes37, i32 0)
  %call39 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %call38)
  br i1 %call39, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then30
  br label %cond.end

cond.false:                                       ; preds = %if.then30
  %39 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes40 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %39, i32 0, i32 11
  %call41 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes40, i32 0)
  %call42 = call i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %call41)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 1, %cond.true ], [ %call42, %cond.false ]
  %40 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %m_subtreeSize = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %40, i32 0, i32 3
  store i32 %cond, i32* %m_subtreeSize, align 4
  br label %if.end43

if.end43:                                         ; preds = %cond.end, %land.lhs.true, %if.end
  %41 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_SubtreeHeaders44 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %41, i32 0, i32 13
  %call45 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders44)
  %42 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_subtreeHeaderCount = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %42, i32 0, i32 14
  store i32 %call45, i32* %m_subtreeHeaderCount, align 4
  %43 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedLeafNodes46 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %43, i32 0, i32 10
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE5clearEv(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes46)
  %44 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_leafNodes47 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %44, i32 0, i32 8
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE5clearEv(%class.btAlignedObjectArray* %m_leafNodes47)
  ret void
}

declare void @_ZN14btQuantizedBvh21setQuantizationValuesERK9btVector3S2_f(%class.btQuantizedBvh*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), float) #3

; Function Attrs: noinline nounwind optnone
define internal %struct.QuantizedNodeTriangleCallback* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallbackC2ER20btAlignedObjectArrayI18btQuantizedBvhNodeEPK14btQuantizedBvh(%struct.QuantizedNodeTriangleCallback* returned %this, %class.btAlignedObjectArray.0* nonnull align 4 dereferenceable(17) %triangleNodes, %class.btQuantizedBvh* %tree) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.QuantizedNodeTriangleCallback*, align 4
  %triangleNodes.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %tree.addr = alloca %class.btQuantizedBvh*, align 4
  store %struct.QuantizedNodeTriangleCallback* %this, %struct.QuantizedNodeTriangleCallback** %this.addr, align 4
  store %class.btAlignedObjectArray.0* %triangleNodes, %class.btAlignedObjectArray.0** %triangleNodes.addr, align 4
  store %class.btQuantizedBvh* %tree, %class.btQuantizedBvh** %tree.addr, align 4
  %this1 = load %struct.QuantizedNodeTriangleCallback*, %struct.QuantizedNodeTriangleCallback** %this.addr, align 4
  %0 = bitcast %struct.QuantizedNodeTriangleCallback* %this1 to %class.btInternalTriangleIndexCallback*
  %call = call %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackC2Ev(%class.btInternalTriangleIndexCallback* %0) #8
  %1 = bitcast %struct.QuantizedNodeTriangleCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_triangleNodes = getelementptr inbounds %struct.QuantizedNodeTriangleCallback, %struct.QuantizedNodeTriangleCallback* %this1, i32 0, i32 1
  %2 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %triangleNodes.addr, align 4
  store %class.btAlignedObjectArray.0* %2, %class.btAlignedObjectArray.0** %m_triangleNodes, align 4
  %m_optimizedTree = getelementptr inbounds %struct.QuantizedNodeTriangleCallback, %struct.QuantizedNodeTriangleCallback* %this1, i32 0, i32 2
  %3 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %tree.addr, align 4
  store %class.btQuantizedBvh* %3, %class.btQuantizedBvh** %m_optimizedTree, align 4
  ret %struct.QuantizedNodeTriangleCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %this, i32 %newsize, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btQuantizedBvhNode*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %struct.btQuantizedBvhNode* %fillData, %struct.btQuantizedBvhNode** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %5 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end15

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %14 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %14, i32 %15
  %16 = bitcast %struct.btQuantizedBvhNode* %arrayidx10 to i8*
  %call11 = call i8* @_ZN18btQuantizedBvhNodenwEmPv(i32 16, i8* %16)
  %17 = bitcast i8* %call11 to %struct.btQuantizedBvhNode*
  %18 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %fillData.addr, align 4
  %19 = bitcast %struct.btQuantizedBvhNode* %17 to i8*
  %20 = bitcast %struct.btQuantizedBvhNode* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc13 = add nsw i32 %21, 1
  store i32 %inc13, i32* %i5, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  br label %if.end15

if.end15:                                         ; preds = %for.end14, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #5

; Function Attrs: noinline nounwind optnone
define internal %struct.QuantizedNodeTriangleCallback* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallbackD2Ev(%struct.QuantizedNodeTriangleCallback* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.QuantizedNodeTriangleCallback*, align 4
  store %struct.QuantizedNodeTriangleCallback* %this, %struct.QuantizedNodeTriangleCallback** %this.addr, align 4
  %this1 = load %struct.QuantizedNodeTriangleCallback*, %struct.QuantizedNodeTriangleCallback** %this.addr, align 4
  %0 = bitcast %struct.QuantizedNodeTriangleCallback* %this1 to %class.btInternalTriangleIndexCallback*
  %call = call %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackD2Ev(%class.btInternalTriangleIndexCallback* %0) #8
  ret %struct.QuantizedNodeTriangleCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define internal %struct.NodeTriangleCallback* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallbackC2ER20btAlignedObjectArrayI18btOptimizedBvhNodeE(%struct.NodeTriangleCallback* returned %this, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %triangleNodes) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.NodeTriangleCallback*, align 4
  %triangleNodes.addr = alloca %class.btAlignedObjectArray*, align 4
  store %struct.NodeTriangleCallback* %this, %struct.NodeTriangleCallback** %this.addr, align 4
  store %class.btAlignedObjectArray* %triangleNodes, %class.btAlignedObjectArray** %triangleNodes.addr, align 4
  %this1 = load %struct.NodeTriangleCallback*, %struct.NodeTriangleCallback** %this.addr, align 4
  %0 = bitcast %struct.NodeTriangleCallback* %this1 to %class.btInternalTriangleIndexCallback*
  %call = call %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackC2Ev(%class.btInternalTriangleIndexCallback* %0) #8
  %1 = bitcast %struct.NodeTriangleCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_triangleNodes = getelementptr inbounds %struct.NodeTriangleCallback, %struct.NodeTriangleCallback* %this1, i32 0, i32 1
  %2 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %triangleNodes.addr, align 4
  store %class.btAlignedObjectArray* %2, %class.btAlignedObjectArray** %m_triangleNodes, align 4
  ret %struct.NodeTriangleCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %struct.btOptimizedBvhNode* nonnull align 4 dereferenceable(64) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btOptimizedBvhNode*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %struct.btOptimizedBvhNode* %fillData, %struct.btOptimizedBvhNode** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end15

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %14 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %14, i32 %15
  %16 = bitcast %struct.btOptimizedBvhNode* %arrayidx10 to i8*
  %call11 = call i8* @_ZN18btOptimizedBvhNodenwEmPv(i32 64, i8* %16)
  %17 = bitcast i8* %call11 to %struct.btOptimizedBvhNode*
  %18 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %fillData.addr, align 4
  %19 = bitcast %struct.btOptimizedBvhNode* %17 to i8*
  %20 = bitcast %struct.btOptimizedBvhNode* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 64, i1 false)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc13 = add nsw i32 %21, 1
  store i32 %inc13, i32* %i5, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  br label %if.end15

if.end15:                                         ; preds = %for.end14, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btOptimizedBvhNode* @_ZN18btOptimizedBvhNodeC2Ev(%struct.btOptimizedBvhNode* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btOptimizedBvhNode*, align 4
  store %struct.btOptimizedBvhNode* %this, %struct.btOptimizedBvhNode** %this.addr, align 4
  %this1 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %this.addr, align 4
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMinOrg)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMaxOrg)
  ret %struct.btOptimizedBvhNode* %this1
}

; Function Attrs: noinline nounwind optnone
define internal %struct.NodeTriangleCallback* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallbackD2Ev(%struct.NodeTriangleCallback* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.NodeTriangleCallback*, align 4
  store %struct.NodeTriangleCallback* %this, %struct.NodeTriangleCallback** %this.addr, align 4
  %this1 = load %struct.NodeTriangleCallback*, %struct.NodeTriangleCallback** %this.addr, align 4
  %0 = bitcast %struct.NodeTriangleCallback* %this1 to %class.btInternalTriangleIndexCallback*
  %call = call %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackD2Ev(%class.btInternalTriangleIndexCallback* %0) #8
  ret %struct.NodeTriangleCallback* %this1
}

declare void @_ZN14btQuantizedBvh9buildTreeEii(%class.btQuantizedBvh*, i32, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_(%class.btAlignedObjectArray.4* %this, %class.btBvhSubtreeInfo* nonnull align 4 dereferenceable(32) %fillValue) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %fillValue.addr = alloca %class.btBvhSubtreeInfo*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store %class.btBvhSubtreeInfo* %fillValue, %class.btBvhSubtreeInfo** %fillValue.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE9allocSizeEi(%class.btAlignedObjectArray.4* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %1 = load i32, i32* %m_size, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4
  %3 = load i32, i32* %sz, align 4
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %2, i32 %3
  %4 = bitcast %class.btBvhSubtreeInfo* %arrayidx to i8*
  %call5 = call i8* @_ZN16btBvhSubtreeInfonwEmPv(i32 32, i8* %4)
  %5 = bitcast i8* %call5 to %class.btBvhSubtreeInfo*
  %6 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %fillValue.addr, align 4
  %7 = bitcast %class.btBvhSubtreeInfo* %5 to i8*
  %8 = bitcast %class.btBvhSubtreeInfo* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 32, i1 false)
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %9 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data6, align 4
  %10 = load i32, i32* %sz, align 4
  %arrayidx7 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %9, i32 %10
  ret %class.btBvhSubtreeInfo* %arrayidx7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btBvhSubtreeInfo* @_ZN16btBvhSubtreeInfoC2Ev(%class.btBvhSubtreeInfo* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBvhSubtreeInfo*, align 4
  store %class.btBvhSubtreeInfo* %this, %class.btBvhSubtreeInfo** %this.addr, align 4
  %this1 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %this.addr, align 4
  ret %class.btBvhSubtreeInfo* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode(%class.btBvhSubtreeInfo* %this, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %quantizedNode) #1 comdat {
entry:
  %this.addr = alloca %class.btBvhSubtreeInfo*, align 4
  %quantizedNode.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %class.btBvhSubtreeInfo* %this, %class.btBvhSubtreeInfo** %this.addr, align 4
  store %struct.btQuantizedBvhNode* %quantizedNode, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4
  %this1 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %this.addr, align 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %1 = load i16, i16* %arrayidx, align 4
  %m_quantizedAabbMin2 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin2, i32 0, i32 0
  store i16 %1, i16* %arrayidx3, align 4
  %2 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4
  %m_quantizedAabbMin4 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %2, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin4, i32 0, i32 1
  %3 = load i16, i16* %arrayidx5, align 2
  %m_quantizedAabbMin6 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin6, i32 0, i32 1
  store i16 %3, i16* %arrayidx7, align 2
  %4 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4
  %m_quantizedAabbMin8 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %4, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin8, i32 0, i32 2
  %5 = load i16, i16* %arrayidx9, align 4
  %m_quantizedAabbMin10 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin10, i32 0, i32 2
  store i16 %5, i16* %arrayidx11, align 4
  %6 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %6, i32 0, i32 1
  %arrayidx12 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %7 = load i16, i16* %arrayidx12, align 2
  %m_quantizedAabbMax13 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 1
  %arrayidx14 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax13, i32 0, i32 0
  store i16 %7, i16* %arrayidx14, align 2
  %8 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4
  %m_quantizedAabbMax15 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %8, i32 0, i32 1
  %arrayidx16 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax15, i32 0, i32 1
  %9 = load i16, i16* %arrayidx16, align 2
  %m_quantizedAabbMax17 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 1
  %arrayidx18 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax17, i32 0, i32 1
  store i16 %9, i16* %arrayidx18, align 2
  %10 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4
  %m_quantizedAabbMax19 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %10, i32 0, i32 1
  %arrayidx20 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax19, i32 0, i32 2
  %11 = load i16, i16* %arrayidx20, align 2
  %m_quantizedAabbMax21 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 1
  %arrayidx22 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax21, i32 0, i32 2
  store i16 %11, i16* %arrayidx22, align 2
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %0, i32 %1
  ret %struct.btQuantizedBvhNode* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %struct.btQuantizedBvhNode* %this, %struct.btQuantizedBvhNode** %this.addr, align 4
  %this1 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %this.addr, align 4
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4
  %cmp = icmp sge i32 %0, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %struct.btQuantizedBvhNode* %this, %struct.btQuantizedBvhNode** %this.addr, align 4
  %this1 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %this.addr, align 4
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4
  %sub = sub nsw i32 0, %0
  ret i32 %sub
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE5clearEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btOptimizedBvh5refitEP23btStridingMeshInterfaceRK9btVector3S4_(%class.btOptimizedBvh* %this, %class.btStridingMeshInterface* %meshInterface, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #2 {
entry:
  %this.addr = alloca %class.btOptimizedBvh*, align 4
  %meshInterface.addr = alloca %class.btStridingMeshInterface*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  %subtree = alloca %class.btBvhSubtreeInfo*, align 4
  store %class.btOptimizedBvh* %this, %class.btOptimizedBvh** %this.addr, align 4
  store %class.btStridingMeshInterface* %meshInterface, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %this.addr, align 4
  %0 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %0, i32 0, i32 6
  %1 = load i8, i8* %m_useQuantization, align 4
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_ZN14btQuantizedBvh21setQuantizationValuesERK9btVector3S2_f(%class.btQuantizedBvh* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4, float 1.000000e+00)
  %5 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  %6 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %6, i32 0, i32 5
  %7 = load i32, i32* %m_curNodeIndex, align 4
  call void @_ZN14btOptimizedBvh14updateBvhNodesEP23btStridingMeshInterfaceiii(%class.btOptimizedBvh* %this1, %class.btStridingMeshInterface* %5, i32 0, i32 %7, i32 0)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %8 = load i32, i32* %i, align 4
  %9 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %9, i32 0, i32 13
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders)
  %cmp = icmp slt i32 %8, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_SubtreeHeaders2 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %10, i32 0, i32 13
  %11 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders2, i32 %11)
  store %class.btBvhSubtreeInfo* %call3, %class.btBvhSubtreeInfo** %subtree, align 4
  %12 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %13 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %13, i32 0, i32 11
  %14 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %m_rootNodeIndex = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %14, i32 0, i32 2
  %15 = load i32, i32* %m_rootNodeIndex, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %15)
  call void @_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode(%class.btBvhSubtreeInfo* %12, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %16 = load i32, i32* %i, align 4
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.else:                                          ; preds = %entry
  br label %if.end

if.end:                                           ; preds = %if.else, %for.end
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btOptimizedBvh14updateBvhNodesEP23btStridingMeshInterfaceiii(%class.btOptimizedBvh* %this, %class.btStridingMeshInterface* %meshInterface, i32 %firstNode, i32 %endNode, i32 %index) #2 {
entry:
  %this.addr = alloca %class.btOptimizedBvh*, align 4
  %meshInterface.addr = alloca %class.btStridingMeshInterface*, align 4
  %firstNode.addr = alloca i32, align 4
  %endNode.addr = alloca i32, align 4
  %index.addr = alloca i32, align 4
  %curNodeSubPart = alloca i32, align 4
  %vertexbase = alloca i8*, align 4
  %numverts = alloca i32, align 4
  %type = alloca i32, align 4
  %stride = alloca i32, align 4
  %indexbase = alloca i8*, align 4
  %indexstride = alloca i32, align 4
  %numfaces = alloca i32, align 4
  %indicestype = alloca i32, align 4
  %triangleVerts = alloca [3 x %class.btVector3], align 16
  %aabbMin = alloca %class.btVector3, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %meshScaling = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  %curNode = alloca %struct.btQuantizedBvhNode*, align 4
  %nodeSubPart = alloca i32, align 4
  %nodeTriangleIndex = alloca i32, align 4
  %gfxbase = alloca i32*, align 4
  %j = alloca i32, align 4
  %graphicsindex = alloca i32, align 4
  %graphicsbase = alloca float*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %graphicsbase39 = alloca double*, align 4
  %ref.tmp42 = alloca %class.btVector3, align 4
  %ref.tmp43 = alloca float, align 4
  %ref.tmp49 = alloca float, align 4
  %ref.tmp55 = alloca float, align 4
  %ref.tmp64 = alloca float, align 4
  %ref.tmp65 = alloca float, align 4
  %ref.tmp66 = alloca float, align 4
  %ref.tmp67 = alloca float, align 4
  %ref.tmp68 = alloca float, align 4
  %ref.tmp69 = alloca float, align 4
  %leftChildNode = alloca %struct.btQuantizedBvhNode*, align 4
  %rightChildNode = alloca %struct.btQuantizedBvhNode*, align 4
  %i94 = alloca i32, align 4
  store %class.btOptimizedBvh* %this, %class.btOptimizedBvh** %this.addr, align 4
  store %class.btStridingMeshInterface* %meshInterface, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  store i32 %firstNode, i32* %firstNode.addr, align 4
  store i32 %endNode, i32* %endNode.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %this.addr, align 4
  store i32 -1, i32* %curNodeSubPart, align 4
  store i8* null, i8** %vertexbase, align 4
  store i32 0, i32* %numverts, align 4
  store i32 2, i32* %type, align 4
  store i32 0, i32* %stride, align 4
  store i8* null, i8** %indexbase, align 4
  store i32 0, i32* %indexstride, align 4
  store i32 0, i32* %numfaces, align 4
  store i32 2, i32* %indicestype, align 4
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin)
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax)
  %0 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %0)
  store %class.btVector3* %call4, %class.btVector3** %meshScaling, align 4
  %1 = load i32, i32* %endNode.addr, align 4
  %sub = sub nsw i32 %1, 1
  store i32 %sub, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc135, %arrayctor.cont
  %2 = load i32, i32* %i, align 4
  %3 = load i32, i32* %firstNode.addr, align 4
  %cmp = icmp sge i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end137

for.body:                                         ; preds = %for.cond
  %4 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %4, i32 0, i32 11
  %5 = load i32, i32* %i, align 4
  %call5 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %5)
  store %struct.btQuantizedBvhNode* %call5, %struct.btQuantizedBvhNode** %curNode, align 4
  %6 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4
  %call6 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %6)
  br i1 %call6, label %if.then, label %if.else78

if.then:                                          ; preds = %for.body
  %7 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4
  %call7 = call i32 @_ZNK18btQuantizedBvhNode9getPartIdEv(%struct.btQuantizedBvhNode* %7)
  store i32 %call7, i32* %nodeSubPart, align 4
  %8 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4
  %call8 = call i32 @_ZNK18btQuantizedBvhNode16getTriangleIndexEv(%struct.btQuantizedBvhNode* %8)
  store i32 %call8, i32* %nodeTriangleIndex, align 4
  %9 = load i32, i32* %nodeSubPart, align 4
  %10 = load i32, i32* %curNodeSubPart, align 4
  %cmp9 = icmp ne i32 %9, %10
  br i1 %cmp9, label %if.then10, label %if.end15

if.then10:                                        ; preds = %if.then
  %11 = load i32, i32* %curNodeSubPart, align 4
  %cmp11 = icmp sge i32 %11, 0
  br i1 %cmp11, label %if.then12, label %if.end

if.then12:                                        ; preds = %if.then10
  %12 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  %13 = load i32, i32* %curNodeSubPart, align 4
  %14 = bitcast %class.btStridingMeshInterface* %12 to void (%class.btStridingMeshInterface*, i32)***
  %vtable = load void (%class.btStridingMeshInterface*, i32)**, void (%class.btStridingMeshInterface*, i32)*** %14, align 4
  %vfn = getelementptr inbounds void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vtable, i64 6
  %15 = load void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vfn, align 4
  call void %15(%class.btStridingMeshInterface* %12, i32 %13)
  br label %if.end

if.end:                                           ; preds = %if.then12, %if.then10
  %16 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  %17 = load i32, i32* %nodeSubPart, align 4
  %18 = bitcast %class.btStridingMeshInterface* %16 to void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)***
  %vtable13 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)**, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*** %18, align 4
  %vfn14 = getelementptr inbounds void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vtable13, i64 4
  %19 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vfn14, align 4
  call void %19(%class.btStridingMeshInterface* %16, i8** %vertexbase, i32* nonnull align 4 dereferenceable(4) %numverts, i32* nonnull align 4 dereferenceable(4) %type, i32* nonnull align 4 dereferenceable(4) %stride, i8** %indexbase, i32* nonnull align 4 dereferenceable(4) %indexstride, i32* nonnull align 4 dereferenceable(4) %numfaces, i32* nonnull align 4 dereferenceable(4) %indicestype, i32 %17)
  %20 = load i32, i32* %nodeSubPart, align 4
  store i32 %20, i32* %curNodeSubPart, align 4
  br label %if.end15

if.end15:                                         ; preds = %if.end, %if.then
  %21 = load i8*, i8** %indexbase, align 4
  %22 = load i32, i32* %nodeTriangleIndex, align 4
  %23 = load i32, i32* %indexstride, align 4
  %mul = mul nsw i32 %22, %23
  %add.ptr = getelementptr inbounds i8, i8* %21, i32 %mul
  %24 = bitcast i8* %add.ptr to i32*
  store i32* %24, i32** %gfxbase, align 4
  store i32 2, i32* %j, align 4
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc, %if.end15
  %25 = load i32, i32* %j, align 4
  %cmp17 = icmp sge i32 %25, 0
  br i1 %cmp17, label %for.body18, label %for.end

for.body18:                                       ; preds = %for.cond16
  %26 = load i32, i32* %indicestype, align 4
  %cmp19 = icmp eq i32 %26, 3
  br i1 %cmp19, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body18
  %27 = load i32*, i32** %gfxbase, align 4
  %28 = bitcast i32* %27 to i16*
  %29 = load i32, i32* %j, align 4
  %arrayidx = getelementptr inbounds i16, i16* %28, i32 %29
  %30 = load i16, i16* %arrayidx, align 2
  %conv = zext i16 %30 to i32
  br label %cond.end

cond.false:                                       ; preds = %for.body18
  %31 = load i32*, i32** %gfxbase, align 4
  %32 = load i32, i32* %j, align 4
  %arrayidx20 = getelementptr inbounds i32, i32* %31, i32 %32
  %33 = load i32, i32* %arrayidx20, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ %33, %cond.false ]
  store i32 %cond, i32* %graphicsindex, align 4
  %34 = load i32, i32* %type, align 4
  %cmp21 = icmp eq i32 %34, 0
  br i1 %cmp21, label %if.then22, label %if.else

if.then22:                                        ; preds = %cond.end
  %35 = load i8*, i8** %vertexbase, align 4
  %36 = load i32, i32* %graphicsindex, align 4
  %37 = load i32, i32* %stride, align 4
  %mul23 = mul nsw i32 %36, %37
  %add.ptr24 = getelementptr inbounds i8, i8* %35, i32 %mul23
  %38 = bitcast i8* %add.ptr24 to float*
  store float* %38, float** %graphicsbase, align 4
  %39 = load float*, float** %graphicsbase, align 4
  %arrayidx26 = getelementptr inbounds float, float* %39, i32 0
  %40 = load float, float* %arrayidx26, align 4
  %41 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %41)
  %42 = load float, float* %call27, align 4
  %mul28 = fmul float %40, %42
  store float %mul28, float* %ref.tmp25, align 4
  %43 = load float*, float** %graphicsbase, align 4
  %arrayidx30 = getelementptr inbounds float, float* %43, i32 1
  %44 = load float, float* %arrayidx30, align 4
  %45 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %45)
  %46 = load float, float* %call31, align 4
  %mul32 = fmul float %44, %46
  store float %mul32, float* %ref.tmp29, align 4
  %47 = load float*, float** %graphicsbase, align 4
  %arrayidx34 = getelementptr inbounds float, float* %47, i32 2
  %48 = load float, float* %arrayidx34, align 4
  %49 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %49)
  %50 = load float, float* %call35, align 4
  %mul36 = fmul float %48, %50
  store float %mul36, float* %ref.tmp33, align 4
  %call37 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp33)
  %51 = load i32, i32* %j, align 4
  %arrayidx38 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 %51
  %52 = bitcast %class.btVector3* %arrayidx38 to i8*
  %53 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %52, i8* align 4 %53, i32 16, i1 false)
  br label %if.end63

if.else:                                          ; preds = %cond.end
  %54 = load i8*, i8** %vertexbase, align 4
  %55 = load i32, i32* %graphicsindex, align 4
  %56 = load i32, i32* %stride, align 4
  %mul40 = mul nsw i32 %55, %56
  %add.ptr41 = getelementptr inbounds i8, i8* %54, i32 %mul40
  %57 = bitcast i8* %add.ptr41 to double*
  store double* %57, double** %graphicsbase39, align 4
  %58 = load double*, double** %graphicsbase39, align 4
  %arrayidx44 = getelementptr inbounds double, double* %58, i32 0
  %59 = load double, double* %arrayidx44, align 8
  %60 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %60)
  %61 = load float, float* %call45, align 4
  %conv46 = fpext float %61 to double
  %mul47 = fmul double %59, %conv46
  %conv48 = fptrunc double %mul47 to float
  store float %conv48, float* %ref.tmp43, align 4
  %62 = load double*, double** %graphicsbase39, align 4
  %arrayidx50 = getelementptr inbounds double, double* %62, i32 1
  %63 = load double, double* %arrayidx50, align 8
  %64 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call51 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %64)
  %65 = load float, float* %call51, align 4
  %conv52 = fpext float %65 to double
  %mul53 = fmul double %63, %conv52
  %conv54 = fptrunc double %mul53 to float
  store float %conv54, float* %ref.tmp49, align 4
  %66 = load double*, double** %graphicsbase39, align 4
  %arrayidx56 = getelementptr inbounds double, double* %66, i32 2
  %67 = load double, double* %arrayidx56, align 8
  %68 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %68)
  %69 = load float, float* %call57, align 4
  %conv58 = fpext float %69 to double
  %mul59 = fmul double %67, %conv58
  %conv60 = fptrunc double %mul59 to float
  store float %conv60, float* %ref.tmp55, align 4
  %call61 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp42, float* nonnull align 4 dereferenceable(4) %ref.tmp43, float* nonnull align 4 dereferenceable(4) %ref.tmp49, float* nonnull align 4 dereferenceable(4) %ref.tmp55)
  %70 = load i32, i32* %j, align 4
  %arrayidx62 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 %70
  %71 = bitcast %class.btVector3* %arrayidx62 to i8*
  %72 = bitcast %class.btVector3* %ref.tmp42 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %71, i8* align 4 %72, i32 16, i1 false)
  br label %if.end63

if.end63:                                         ; preds = %if.else, %if.then22
  br label %for.inc

for.inc:                                          ; preds = %if.end63
  %73 = load i32, i32* %j, align 4
  %dec = add nsw i32 %73, -1
  store i32 %dec, i32* %j, align 4
  br label %for.cond16

for.end:                                          ; preds = %for.cond16
  store float 0x43ABC16D60000000, float* %ref.tmp64, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp65, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp66, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %aabbMin, float* nonnull align 4 dereferenceable(4) %ref.tmp64, float* nonnull align 4 dereferenceable(4) %ref.tmp65, float* nonnull align 4 dereferenceable(4) %ref.tmp66)
  store float 0xC3ABC16D60000000, float* %ref.tmp67, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp68, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp69, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %aabbMax, float* nonnull align 4 dereferenceable(4) %ref.tmp67, float* nonnull align 4 dereferenceable(4) %ref.tmp68, float* nonnull align 4 dereferenceable(4) %ref.tmp69)
  %arrayidx70 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 0
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx70)
  %arrayidx71 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 0
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx71)
  %arrayidx72 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 1
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx72)
  %arrayidx73 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 1
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx73)
  %arrayidx74 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 2
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx74)
  %arrayidx75 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 2
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx75)
  %74 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %75 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %75, i32 0, i32 0
  %arrayidx76 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %74, i16* %arrayidx76, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, i32 0)
  %76 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %77 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %77, i32 0, i32 1
  %arrayidx77 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %76, i16* %arrayidx77, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i32 1)
  br label %if.end134

if.else78:                                        ; preds = %for.body
  %78 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes79 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %78, i32 0, i32 11
  %79 = load i32, i32* %i, align 4
  %add = add nsw i32 %79, 1
  %call80 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes79, i32 %add)
  store %struct.btQuantizedBvhNode* %call80, %struct.btQuantizedBvhNode** %leftChildNode, align 4
  %80 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4
  %call81 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %80)
  br i1 %call81, label %cond.true82, label %cond.false86

cond.true82:                                      ; preds = %if.else78
  %81 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes83 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %81, i32 0, i32 11
  %82 = load i32, i32* %i, align 4
  %add84 = add nsw i32 %82, 2
  %call85 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes83, i32 %add84)
  br label %cond.end92

cond.false86:                                     ; preds = %if.else78
  %83 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes87 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %83, i32 0, i32 11
  %84 = load i32, i32* %i, align 4
  %add88 = add nsw i32 %84, 1
  %85 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4
  %call89 = call i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %85)
  %add90 = add nsw i32 %add88, %call89
  %call91 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes87, i32 %add90)
  br label %cond.end92

cond.end92:                                       ; preds = %cond.false86, %cond.true82
  %cond93 = phi %struct.btQuantizedBvhNode* [ %call85, %cond.true82 ], [ %call91, %cond.false86 ]
  store %struct.btQuantizedBvhNode* %cond93, %struct.btQuantizedBvhNode** %rightChildNode, align 4
  store i32 0, i32* %i94, align 4
  br label %for.cond95

for.cond95:                                       ; preds = %for.inc132, %cond.end92
  %86 = load i32, i32* %i94, align 4
  %cmp96 = icmp slt i32 %86, 3
  br i1 %cmp96, label %for.body97, label %for.end133

for.body97:                                       ; preds = %for.cond95
  %87 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4
  %m_quantizedAabbMin98 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %87, i32 0, i32 0
  %88 = load i32, i32* %i94, align 4
  %arrayidx99 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin98, i32 0, i32 %88
  %89 = load i16, i16* %arrayidx99, align 2
  %90 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4
  %m_quantizedAabbMin100 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %90, i32 0, i32 0
  %91 = load i32, i32* %i94, align 4
  %arrayidx101 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin100, i32 0, i32 %91
  store i16 %89, i16* %arrayidx101, align 2
  %92 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4
  %m_quantizedAabbMin102 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %92, i32 0, i32 0
  %93 = load i32, i32* %i94, align 4
  %arrayidx103 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin102, i32 0, i32 %93
  %94 = load i16, i16* %arrayidx103, align 2
  %conv104 = zext i16 %94 to i32
  %95 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rightChildNode, align 4
  %m_quantizedAabbMin105 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %95, i32 0, i32 0
  %96 = load i32, i32* %i94, align 4
  %arrayidx106 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin105, i32 0, i32 %96
  %97 = load i16, i16* %arrayidx106, align 2
  %conv107 = zext i16 %97 to i32
  %cmp108 = icmp sgt i32 %conv104, %conv107
  br i1 %cmp108, label %if.then109, label %if.end114

if.then109:                                       ; preds = %for.body97
  %98 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rightChildNode, align 4
  %m_quantizedAabbMin110 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %98, i32 0, i32 0
  %99 = load i32, i32* %i94, align 4
  %arrayidx111 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin110, i32 0, i32 %99
  %100 = load i16, i16* %arrayidx111, align 2
  %101 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4
  %m_quantizedAabbMin112 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %101, i32 0, i32 0
  %102 = load i32, i32* %i94, align 4
  %arrayidx113 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin112, i32 0, i32 %102
  store i16 %100, i16* %arrayidx113, align 2
  br label %if.end114

if.end114:                                        ; preds = %if.then109, %for.body97
  %103 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4
  %m_quantizedAabbMax115 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %103, i32 0, i32 1
  %104 = load i32, i32* %i94, align 4
  %arrayidx116 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax115, i32 0, i32 %104
  %105 = load i16, i16* %arrayidx116, align 2
  %106 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4
  %m_quantizedAabbMax117 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %106, i32 0, i32 1
  %107 = load i32, i32* %i94, align 4
  %arrayidx118 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax117, i32 0, i32 %107
  store i16 %105, i16* %arrayidx118, align 2
  %108 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4
  %m_quantizedAabbMax119 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %108, i32 0, i32 1
  %109 = load i32, i32* %i94, align 4
  %arrayidx120 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax119, i32 0, i32 %109
  %110 = load i16, i16* %arrayidx120, align 2
  %conv121 = zext i16 %110 to i32
  %111 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rightChildNode, align 4
  %m_quantizedAabbMax122 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %111, i32 0, i32 1
  %112 = load i32, i32* %i94, align 4
  %arrayidx123 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax122, i32 0, i32 %112
  %113 = load i16, i16* %arrayidx123, align 2
  %conv124 = zext i16 %113 to i32
  %cmp125 = icmp slt i32 %conv121, %conv124
  br i1 %cmp125, label %if.then126, label %if.end131

if.then126:                                       ; preds = %if.end114
  %114 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rightChildNode, align 4
  %m_quantizedAabbMax127 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %114, i32 0, i32 1
  %115 = load i32, i32* %i94, align 4
  %arrayidx128 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax127, i32 0, i32 %115
  %116 = load i16, i16* %arrayidx128, align 2
  %117 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4
  %m_quantizedAabbMax129 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %117, i32 0, i32 1
  %118 = load i32, i32* %i94, align 4
  %arrayidx130 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax129, i32 0, i32 %118
  store i16 %116, i16* %arrayidx130, align 2
  br label %if.end131

if.end131:                                        ; preds = %if.then126, %if.end114
  br label %for.inc132

for.inc132:                                       ; preds = %if.end131
  %119 = load i32, i32* %i94, align 4
  %inc = add nsw i32 %119, 1
  store i32 %inc, i32* %i94, align 4
  br label %for.cond95

for.end133:                                       ; preds = %for.cond95
  br label %if.end134

if.end134:                                        ; preds = %for.end133, %for.end
  br label %for.inc135

for.inc135:                                       ; preds = %if.end134
  %120 = load i32, i32* %i, align 4
  %dec136 = add nsw i32 %120, -1
  store i32 %dec136, i32* %i, align 4
  br label %for.cond

for.end137:                                       ; preds = %for.cond
  %121 = load i32, i32* %curNodeSubPart, align 4
  %cmp138 = icmp sge i32 %121, 0
  br i1 %cmp138, label %if.then139, label %if.end142

if.then139:                                       ; preds = %for.end137
  %122 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  %123 = load i32, i32* %curNodeSubPart, align 4
  %124 = bitcast %class.btStridingMeshInterface* %122 to void (%class.btStridingMeshInterface*, i32)***
  %vtable140 = load void (%class.btStridingMeshInterface*, i32)**, void (%class.btStridingMeshInterface*, i32)*** %124, align 4
  %vfn141 = getelementptr inbounds void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vtable140, i64 6
  %125 = load void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vfn141, align 4
  call void %125(%class.btStridingMeshInterface* %122, i32 %123)
  br label %if.end142

if.end142:                                        ; preds = %if.then139, %for.end137
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %0, i32 %1
  ret %class.btBvhSubtreeInfo* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btOptimizedBvh12refitPartialEP23btStridingMeshInterfaceRK9btVector3S4_(%class.btOptimizedBvh* %this, %class.btStridingMeshInterface* %meshInterface, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #2 {
entry:
  %this.addr = alloca %class.btOptimizedBvh*, align 4
  %meshInterface.addr = alloca %class.btStridingMeshInterface*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %quantizedQueryAabbMin = alloca [3 x i16], align 2
  %quantizedQueryAabbMax = alloca [3 x i16], align 2
  %i = alloca i32, align 4
  %subtree = alloca %class.btBvhSubtreeInfo*, align 4
  %overlap = alloca i32, align 4
  store %class.btOptimizedBvh* %this, %class.btOptimizedBvh** %this.addr, align 4
  store %class.btStridingMeshInterface* %meshInterface, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %this.addr, align 4
  %0 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMin, i32 0, i32 0
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %0, i16* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %1, i32 0)
  %2 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %arrayidx2 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMax, i32 0, i32 0
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %2, i16* %arrayidx2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, i32 1)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %i, align 4
  %5 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %5, i32 0, i32 13
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders)
  %cmp = icmp slt i32 %4, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %6 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_SubtreeHeaders3 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %6, i32 0, i32 13
  %7 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders3, i32 %7)
  store %class.btBvhSubtreeInfo* %call4, %class.btBvhSubtreeInfo** %subtree, align 4
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMin, i32 0, i32 0
  %arraydecay5 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMax, i32 0, i32 0
  %8 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %m_quantizedAabbMin = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %8, i32 0, i32 0
  %arraydecay6 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %9 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %m_quantizedAabbMax = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %9, i32 0, i32 1
  %arraydecay7 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %call8 = call i32 @_Z37testQuantizedAabbAgainstQuantizedAabbPKtS0_S0_S0_(i16* %arraydecay, i16* %arraydecay5, i16* %arraydecay6, i16* %arraydecay7)
  store i32 %call8, i32* %overlap, align 4
  %10 = load i32, i32* %overlap, align 4
  %cmp9 = icmp ne i32 %10, 0
  br i1 %cmp9, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %11 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  %12 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %m_rootNodeIndex = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %12, i32 0, i32 2
  %13 = load i32, i32* %m_rootNodeIndex, align 4
  %14 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %m_rootNodeIndex10 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %14, i32 0, i32 2
  %15 = load i32, i32* %m_rootNodeIndex10, align 4
  %16 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %m_subtreeSize = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %16, i32 0, i32 3
  %17 = load i32, i32* %m_subtreeSize, align 4
  %add = add nsw i32 %15, %17
  %18 = load i32, i32* %i, align 4
  call void @_ZN14btOptimizedBvh14updateBvhNodesEP23btStridingMeshInterfaceiii(%class.btOptimizedBvh* %this1, %class.btStridingMeshInterface* %11, i32 %13, i32 %add, i32 %18)
  %19 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %20 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %20, i32 0, i32 11
  %21 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %m_rootNodeIndex11 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %21, i32 0, i32 2
  %22 = load i32, i32* %m_rootNodeIndex11, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %22)
  call void @_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode(%class.btBvhSubtreeInfo* %19, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %call12)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %23 = load i32, i32* %i, align 4
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %this, i16* %out, %class.btVector3* nonnull align 4 dereferenceable(16) %point, i32 %isMax) #2 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %out.addr = alloca i16*, align 4
  %point.addr = alloca %class.btVector3*, align 4
  %isMax.addr = alloca i32, align 4
  %v = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store i16* %out, i16** %out.addr, align 4
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4
  store i32 %isMax, i32* %isMax.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin)
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhQuantization)
  %1 = load i32, i32* %isMax.addr, align 4
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %v)
  %2 = load float, float* %call, align 4
  %add = fadd float %2, 1.000000e+00
  %conv = fptoui float %add to i16
  %conv2 = zext i16 %conv to i32
  %or = or i32 %conv2, 1
  %conv3 = trunc i32 %or to i16
  %3 = load i16*, i16** %out.addr, align 4
  %arrayidx = getelementptr inbounds i16, i16* %3, i32 0
  store i16 %conv3, i16* %arrayidx, align 2
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %v)
  %4 = load float, float* %call4, align 4
  %add5 = fadd float %4, 1.000000e+00
  %conv6 = fptoui float %add5 to i16
  %conv7 = zext i16 %conv6 to i32
  %or8 = or i32 %conv7, 1
  %conv9 = trunc i32 %or8 to i16
  %5 = load i16*, i16** %out.addr, align 4
  %arrayidx10 = getelementptr inbounds i16, i16* %5, i32 1
  store i16 %conv9, i16* %arrayidx10, align 2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %v)
  %6 = load float, float* %call11, align 4
  %add12 = fadd float %6, 1.000000e+00
  %conv13 = fptoui float %add12 to i16
  %conv14 = zext i16 %conv13 to i32
  %or15 = or i32 %conv14, 1
  %conv16 = trunc i32 %or15 to i16
  %7 = load i16*, i16** %out.addr, align 4
  %arrayidx17 = getelementptr inbounds i16, i16* %7, i32 2
  store i16 %conv16, i16* %arrayidx17, align 2
  br label %if.end

if.else:                                          ; preds = %entry
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %v)
  %8 = load float, float* %call18, align 4
  %conv19 = fptoui float %8 to i16
  %conv20 = zext i16 %conv19 to i32
  %and = and i32 %conv20, 65534
  %conv21 = trunc i32 %and to i16
  %9 = load i16*, i16** %out.addr, align 4
  %arrayidx22 = getelementptr inbounds i16, i16* %9, i32 0
  store i16 %conv21, i16* %arrayidx22, align 2
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %v)
  %10 = load float, float* %call23, align 4
  %conv24 = fptoui float %10 to i16
  %conv25 = zext i16 %conv24 to i32
  %and26 = and i32 %conv25, 65534
  %conv27 = trunc i32 %and26 to i16
  %11 = load i16*, i16** %out.addr, align 4
  %arrayidx28 = getelementptr inbounds i16, i16* %11, i32 1
  store i16 %conv27, i16* %arrayidx28, align 2
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %v)
  %12 = load float, float* %call29, align 4
  %conv30 = fptoui float %12 to i16
  %conv31 = zext i16 %conv30 to i32
  %and32 = and i32 %conv31, 65534
  %conv33 = trunc i32 %and32 to i16
  %13 = load i16*, i16** %out.addr, align 4
  %arrayidx34 = getelementptr inbounds i16, i16* %13, i32 2
  store i16 %conv33, i16* %arrayidx34, align 2
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_Z37testQuantizedAabbAgainstQuantizedAabbPKtS0_S0_S0_(i16* %aabbMin1, i16* %aabbMax1, i16* %aabbMin2, i16* %aabbMax2) #2 comdat {
entry:
  %aabbMin1.addr = alloca i16*, align 4
  %aabbMax1.addr = alloca i16*, align 4
  %aabbMin2.addr = alloca i16*, align 4
  %aabbMax2.addr = alloca i16*, align 4
  store i16* %aabbMin1, i16** %aabbMin1.addr, align 4
  store i16* %aabbMax1, i16** %aabbMax1.addr, align 4
  store i16* %aabbMin2, i16** %aabbMin2.addr, align 4
  store i16* %aabbMax2, i16** %aabbMax2.addr, align 4
  %0 = load i16*, i16** %aabbMin1.addr, align 4
  %arrayidx = getelementptr inbounds i16, i16* %0, i32 0
  %1 = load i16, i16* %arrayidx, align 2
  %conv = zext i16 %1 to i32
  %2 = load i16*, i16** %aabbMax2.addr, align 4
  %arrayidx1 = getelementptr inbounds i16, i16* %2, i32 0
  %3 = load i16, i16* %arrayidx1, align 2
  %conv2 = zext i16 %3 to i32
  %cmp = icmp sle i32 %conv, %conv2
  %conv3 = zext i1 %cmp to i32
  %4 = load i16*, i16** %aabbMax1.addr, align 4
  %arrayidx4 = getelementptr inbounds i16, i16* %4, i32 0
  %5 = load i16, i16* %arrayidx4, align 2
  %conv5 = zext i16 %5 to i32
  %6 = load i16*, i16** %aabbMin2.addr, align 4
  %arrayidx6 = getelementptr inbounds i16, i16* %6, i32 0
  %7 = load i16, i16* %arrayidx6, align 2
  %conv7 = zext i16 %7 to i32
  %cmp8 = icmp sge i32 %conv5, %conv7
  %conv9 = zext i1 %cmp8 to i32
  %and = and i32 %conv3, %conv9
  %8 = load i16*, i16** %aabbMin1.addr, align 4
  %arrayidx10 = getelementptr inbounds i16, i16* %8, i32 2
  %9 = load i16, i16* %arrayidx10, align 2
  %conv11 = zext i16 %9 to i32
  %10 = load i16*, i16** %aabbMax2.addr, align 4
  %arrayidx12 = getelementptr inbounds i16, i16* %10, i32 2
  %11 = load i16, i16* %arrayidx12, align 2
  %conv13 = zext i16 %11 to i32
  %cmp14 = icmp sle i32 %conv11, %conv13
  %conv15 = zext i1 %cmp14 to i32
  %and16 = and i32 %and, %conv15
  %12 = load i16*, i16** %aabbMax1.addr, align 4
  %arrayidx17 = getelementptr inbounds i16, i16* %12, i32 2
  %13 = load i16, i16* %arrayidx17, align 2
  %conv18 = zext i16 %13 to i32
  %14 = load i16*, i16** %aabbMin2.addr, align 4
  %arrayidx19 = getelementptr inbounds i16, i16* %14, i32 2
  %15 = load i16, i16* %arrayidx19, align 2
  %conv20 = zext i16 %15 to i32
  %cmp21 = icmp sge i32 %conv18, %conv20
  %conv22 = zext i1 %cmp21 to i32
  %and23 = and i32 %and16, %conv22
  %16 = load i16*, i16** %aabbMin1.addr, align 4
  %arrayidx24 = getelementptr inbounds i16, i16* %16, i32 1
  %17 = load i16, i16* %arrayidx24, align 2
  %conv25 = zext i16 %17 to i32
  %18 = load i16*, i16** %aabbMax2.addr, align 4
  %arrayidx26 = getelementptr inbounds i16, i16* %18, i32 1
  %19 = load i16, i16* %arrayidx26, align 2
  %conv27 = zext i16 %19 to i32
  %cmp28 = icmp sle i32 %conv25, %conv27
  %conv29 = zext i1 %cmp28 to i32
  %and30 = and i32 %and23, %conv29
  %20 = load i16*, i16** %aabbMax1.addr, align 4
  %arrayidx31 = getelementptr inbounds i16, i16* %20, i32 1
  %21 = load i16, i16* %arrayidx31, align 2
  %conv32 = zext i16 %21 to i32
  %22 = load i16*, i16** %aabbMin2.addr, align 4
  %arrayidx33 = getelementptr inbounds i16, i16* %22, i32 1
  %23 = load i16, i16* %arrayidx33, align 2
  %conv34 = zext i16 %23 to i32
  %cmp35 = icmp sge i32 %conv32, %conv34
  %conv36 = zext i1 %cmp35 to i32
  %and37 = and i32 %and30, %conv36
  %call = call i32 @_Z8btSelectjii(i32 %and37, i32 1, i32 0)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %m_scaling = getelementptr inbounds %class.btStridingMeshInterface, %class.btStridingMeshInterface* %this1, i32 0, i32 1
  ret %class.btVector3* %m_scaling
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK18btQuantizedBvhNode9getPartIdEv(%struct.btQuantizedBvhNode* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %struct.btQuantizedBvhNode* %this, %struct.btQuantizedBvhNode** %this.addr, align 4
  %this1 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %this.addr, align 4
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4
  %shr = ashr i32 %0, 21
  ret i32 %shr
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK18btQuantizedBvhNode16getTriangleIndexEv(%struct.btQuantizedBvhNode* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btQuantizedBvhNode*, align 4
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  store %struct.btQuantizedBvhNode* %this, %struct.btQuantizedBvhNode** %this.addr, align 4
  %this1 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %this.addr, align 4
  store i32 0, i32* %x, align 4
  %0 = load i32, i32* %x, align 4
  %and = and i32 %0, 0
  %neg = xor i32 %and, -1
  %shl = shl i32 %neg, 21
  store i32 %shl, i32* %y, align 4
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %this1, i32 0, i32 2
  %1 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4
  %2 = load i32, i32* %y, align 4
  %neg2 = xor i32 %2, -1
  %and3 = and i32 %1, %neg2
  ret i32 %and3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btVector36setMinERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btVector36setMaxERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline optnone
define hidden %class.btOptimizedBvh* @_ZN14btOptimizedBvh18deSerializeInPlaceEPvjb(i8* %i_alignedDataBuffer, i32 %i_dataBufferSize, i1 zeroext %i_swapEndian) #2 {
entry:
  %i_alignedDataBuffer.addr = alloca i8*, align 4
  %i_dataBufferSize.addr = alloca i32, align 4
  %i_swapEndian.addr = alloca i8, align 1
  %bvh = alloca %class.btQuantizedBvh*, align 4
  store i8* %i_alignedDataBuffer, i8** %i_alignedDataBuffer.addr, align 4
  store i32 %i_dataBufferSize, i32* %i_dataBufferSize.addr, align 4
  %frombool = zext i1 %i_swapEndian to i8
  store i8 %frombool, i8* %i_swapEndian.addr, align 1
  %0 = load i8*, i8** %i_alignedDataBuffer.addr, align 4
  %1 = load i32, i32* %i_dataBufferSize.addr, align 4
  %2 = load i8, i8* %i_swapEndian.addr, align 1
  %tobool = trunc i8 %2 to i1
  %call = call %class.btQuantizedBvh* @_ZN14btQuantizedBvh18deSerializeInPlaceEPvjb(i8* %0, i32 %1, i1 zeroext %tobool)
  store %class.btQuantizedBvh* %call, %class.btQuantizedBvh** %bvh, align 4
  %3 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %4 = bitcast %class.btQuantizedBvh* %3 to %class.btOptimizedBvh*
  ret %class.btOptimizedBvh* %4
}

declare %class.btQuantizedBvh* @_ZN14btQuantizedBvh18deSerializeInPlaceEPvjb(i8*, i32, i1 zeroext) #3

declare zeroext i1 @_ZNK14btQuantizedBvh9serializeEPvjb(%class.btQuantizedBvh*, i8*, i32, i1 zeroext) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK14btQuantizedBvh31calculateSerializeBufferSizeNewEv(%class.btQuantizedBvh* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  ret i32 84
}

declare i8* @_ZNK14btQuantizedBvh9serializeEPvP12btSerializer(%class.btQuantizedBvh*, i8*, %class.btSerializer*) unnamed_addr #3

declare void @_ZN14btQuantizedBvh16deSerializeFloatER23btQuantizedBvhFloatData(%class.btQuantizedBvh*, %struct.btQuantizedBvhFloatData* nonnull align 4 dereferenceable(84)) unnamed_addr #3

declare void @_ZN14btQuantizedBvh17deSerializeDoubleER24btQuantizedBvhDoubleData(%class.btQuantizedBvh*, %struct.btQuantizedBvhDoubleData* nonnull align 8 dereferenceable(136)) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK14btOptimizedBvh16serializeInPlaceEPvjb(%class.btOptimizedBvh* %this, i8* %o_alignedDataBuffer, i32 %i_dataBufferSize, i1 zeroext %i_swapEndian) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btOptimizedBvh*, align 4
  %o_alignedDataBuffer.addr = alloca i8*, align 4
  %i_dataBufferSize.addr = alloca i32, align 4
  %i_swapEndian.addr = alloca i8, align 1
  store %class.btOptimizedBvh* %this, %class.btOptimizedBvh** %this.addr, align 4
  store i8* %o_alignedDataBuffer, i8** %o_alignedDataBuffer.addr, align 4
  store i32 %i_dataBufferSize, i32* %i_dataBufferSize.addr, align 4
  %frombool = zext i1 %i_swapEndian to i8
  store i8 %frombool, i8* %i_swapEndian.addr, align 1
  %this1 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %this.addr, align 4
  %0 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %1 = load i8*, i8** %o_alignedDataBuffer.addr, align 4
  %2 = load i32, i32* %i_dataBufferSize.addr, align 4
  %3 = load i8, i8* %i_swapEndian.addr, align 1
  %tobool = trunc i8 %3 to i1
  %call = call zeroext i1 @_ZNK14btQuantizedBvh9serializeEPvjb(%class.btQuantizedBvh* %0, i8* %1, i32 %2, i1 zeroext %tobool)
  ret i1 %call
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackC2Ev(%class.btInternalTriangleIndexCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btInternalTriangleIndexCallback*, align 4
  store %class.btInternalTriangleIndexCallback* %this, %class.btInternalTriangleIndexCallback** %this.addr, align 4
  %this1 = load %class.btInternalTriangleIndexCallback*, %class.btInternalTriangleIndexCallback** %this.addr, align 4
  %0 = bitcast %class.btInternalTriangleIndexCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV31btInternalTriangleIndexCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btInternalTriangleIndexCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallbackD0Ev(%struct.QuantizedNodeTriangleCallback* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.QuantizedNodeTriangleCallback*, align 4
  store %struct.QuantizedNodeTriangleCallback* %this, %struct.QuantizedNodeTriangleCallback** %this.addr, align 4
  %this1 = load %struct.QuantizedNodeTriangleCallback*, %struct.QuantizedNodeTriangleCallback** %this.addr, align 4
  %call = call %struct.QuantizedNodeTriangleCallback* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallbackD2Ev(%struct.QuantizedNodeTriangleCallback* %this1) #8
  %0 = bitcast %struct.QuantizedNodeTriangleCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define internal void @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallback28internalProcessTriangleIndexEPS2_ii(%struct.QuantizedNodeTriangleCallback* %this, %class.btVector3* %triangle, i32 %partId, i32 %triangleIndex) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.QuantizedNodeTriangleCallback*, align 4
  %triangle.addr = alloca %class.btVector3*, align 4
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  %node = alloca %struct.btQuantizedBvhNode, align 4
  %aabbMin = alloca %class.btVector3, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %MIN_AABB_DIMENSION = alloca float, align 4
  %MIN_AABB_HALF_DIMENSION = alloca float, align 4
  store %struct.QuantizedNodeTriangleCallback* %this, %struct.QuantizedNodeTriangleCallback** %this.addr, align 4
  store %class.btVector3* %triangle, %class.btVector3** %triangle.addr, align 4
  store i32 %partId, i32* %partId.addr, align 4
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4
  %this1 = load %struct.QuantizedNodeTriangleCallback*, %struct.QuantizedNodeTriangleCallback** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax)
  store float 0x43ABC16D60000000, float* %ref.tmp, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp3, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp4, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %aabbMin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  store float 0xC3ABC16D60000000, float* %ref.tmp5, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp6, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp7, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %aabbMax, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %0 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  %1 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx8 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx8)
  %2 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx9 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 1
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx9)
  %3 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx10 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 1
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx10)
  %4 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx11 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 2
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx11)
  %5 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx12 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 2
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx12)
  store float 0x3F60624DE0000000, float* %MIN_AABB_DIMENSION, align 4
  store float 0x3F50624DE0000000, float* %MIN_AABB_HALF_DIMENSION, align 4
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %aabbMax)
  %6 = load float, float* %call13, align 4
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %aabbMin)
  %7 = load float, float* %call14, align 4
  %sub = fsub float %6, %7
  %cmp = fcmp olt float %sub, 0x3F60624DE0000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %aabbMax)
  %8 = load float, float* %call15, align 4
  %add = fadd float %8, 0x3F50624DE0000000
  call void @_ZN9btVector34setXEf(%class.btVector3* %aabbMax, float %add)
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %aabbMin)
  %9 = load float, float* %call16, align 4
  %sub17 = fsub float %9, 0x3F50624DE0000000
  call void @_ZN9btVector34setXEf(%class.btVector3* %aabbMin, float %sub17)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %aabbMax)
  %10 = load float, float* %call18, align 4
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %aabbMin)
  %11 = load float, float* %call19, align 4
  %sub20 = fsub float %10, %11
  %cmp21 = fcmp olt float %sub20, 0x3F60624DE0000000
  br i1 %cmp21, label %if.then22, label %if.end27

if.then22:                                        ; preds = %if.end
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %aabbMax)
  %12 = load float, float* %call23, align 4
  %add24 = fadd float %12, 0x3F50624DE0000000
  call void @_ZN9btVector34setYEf(%class.btVector3* %aabbMax, float %add24)
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %aabbMin)
  %13 = load float, float* %call25, align 4
  %sub26 = fsub float %13, 0x3F50624DE0000000
  call void @_ZN9btVector34setYEf(%class.btVector3* %aabbMin, float %sub26)
  br label %if.end27

if.end27:                                         ; preds = %if.then22, %if.end
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %aabbMax)
  %14 = load float, float* %call28, align 4
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %aabbMin)
  %15 = load float, float* %call29, align 4
  %sub30 = fsub float %14, %15
  %cmp31 = fcmp olt float %sub30, 0x3F60624DE0000000
  br i1 %cmp31, label %if.then32, label %if.end37

if.then32:                                        ; preds = %if.end27
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %aabbMax)
  %16 = load float, float* %call33, align 4
  %add34 = fadd float %16, 0x3F50624DE0000000
  call void @_ZN9btVector34setZEf(%class.btVector3* %aabbMax, float %add34)
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %aabbMin)
  %17 = load float, float* %call35, align 4
  %sub36 = fsub float %17, 0x3F50624DE0000000
  call void @_ZN9btVector34setZEf(%class.btVector3* %aabbMin, float %sub36)
  br label %if.end37

if.end37:                                         ; preds = %if.then32, %if.end27
  %m_optimizedTree = getelementptr inbounds %struct.QuantizedNodeTriangleCallback, %struct.QuantizedNodeTriangleCallback* %this1, i32 0, i32 2
  %18 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %m_optimizedTree, align 4
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %node, i32 0, i32 0
  %arrayidx38 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %18, i16* %arrayidx38, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, i32 0)
  %m_optimizedTree39 = getelementptr inbounds %struct.QuantizedNodeTriangleCallback, %struct.QuantizedNodeTriangleCallback* %this1, i32 0, i32 2
  %19 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %m_optimizedTree39, align 4
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %node, i32 0, i32 1
  %arrayidx40 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %19, i16* %arrayidx40, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i32 1)
  %20 = load i32, i32* %partId.addr, align 4
  %shl = shl i32 %20, 21
  %21 = load i32, i32* %triangleIndex.addr, align 4
  %or = or i32 %shl, %21
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %node, i32 0, i32 2
  store i32 %or, i32* %m_escapeIndexOrTriangleIndex, align 4
  %m_triangleNodes = getelementptr inbounds %struct.QuantizedNodeTriangleCallback, %struct.QuantizedNodeTriangleCallback* %this1, i32 0, i32 1
  %22 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %m_triangleNodes, align 4
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9push_backERKS0_(%class.btAlignedObjectArray.0* %22, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %node)
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector34setXEf(%class.btVector3* %this, float %_x) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float %_x, float* %_x.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_x.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %0, float* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector34setYEf(%class.btVector3* %this, float %_y) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_y.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float %_y, float* %_y.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_y.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  store float %0, float* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector34setZEf(%class.btVector3* %this, float %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_z.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float %_z, float* %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_z.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  store float %0, float* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9push_backERKS0_(%class.btAlignedObjectArray.0* %this, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Val.addr = alloca %struct.btQuantizedBvhNode*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store %struct.btQuantizedBvhNode* %_Val, %struct.btQuantizedBvhNode** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9allocSizeEi(%class.btAlignedObjectArray.0* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %1 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %1, i32 %2
  %3 = bitcast %struct.btQuantizedBvhNode* %arrayidx to i8*
  %call5 = call i8* @_ZN18btQuantizedBvhNodenwEmPv(i32 16, i8* %3)
  %4 = bitcast i8* %call5 to %struct.btQuantizedBvhNode*
  %5 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %_Val.addr, align 4
  %6 = bitcast %struct.btQuantizedBvhNode* %4 to i8*
  %7 = bitcast %struct.btQuantizedBvhNode* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size6, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE8capacityEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btQuantizedBvhNode*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btQuantizedBvhNode*
  store %struct.btQuantizedBvhNode* %2, %struct.btQuantizedBvhNode** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %3 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %struct.btQuantizedBvhNode* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btQuantizedBvhNode* %4, %struct.btQuantizedBvhNode** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9allocSizeEi(%class.btAlignedObjectArray.0* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN18btQuantizedBvhNodenwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btQuantizedBvhNode* @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %struct.btQuantizedBvhNode** null)
  %2 = bitcast %struct.btQuantizedBvhNode* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %struct.btQuantizedBvhNode* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btQuantizedBvhNode*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btQuantizedBvhNode* %dest, %struct.btQuantizedBvhNode** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %3, i32 %4
  %5 = bitcast %struct.btQuantizedBvhNode* %arrayidx to i8*
  %call = call i8* @_ZN18btQuantizedBvhNodenwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call to %struct.btQuantizedBvhNode*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %7 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %7, i32 %8
  %9 = bitcast %struct.btQuantizedBvhNode* %6 to i8*
  %10 = bitcast %struct.btQuantizedBvhNode* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4
  %tobool = icmp ne %struct.btQuantizedBvhNode* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %m_allocator, %struct.btQuantizedBvhNode* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btQuantizedBvhNode* null, %struct.btQuantizedBvhNode** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btQuantizedBvhNode* @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %this, i32 %n, %struct.btQuantizedBvhNode** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btQuantizedBvhNode**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btQuantizedBvhNode** %hint, %struct.btQuantizedBvhNode*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btQuantizedBvhNode*
  ret %struct.btQuantizedBvhNode* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %this, %struct.btQuantizedBvhNode* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store %struct.btQuantizedBvhNode* %ptr, %struct.btQuantizedBvhNode** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %ptr.addr, align 4
  %1 = bitcast %struct.btQuantizedBvhNode* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: nounwind
declare %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackD2Ev(%class.btInternalTriangleIndexCallback* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallbackD0Ev(%struct.NodeTriangleCallback* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.NodeTriangleCallback*, align 4
  store %struct.NodeTriangleCallback* %this, %struct.NodeTriangleCallback** %this.addr, align 4
  %this1 = load %struct.NodeTriangleCallback*, %struct.NodeTriangleCallback** %this.addr, align 4
  %call = call %struct.NodeTriangleCallback* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallbackD2Ev(%struct.NodeTriangleCallback* %this1) #8
  %0 = bitcast %struct.NodeTriangleCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define internal void @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallback28internalProcessTriangleIndexEPS2_ii(%struct.NodeTriangleCallback* %this, %class.btVector3* %triangle, i32 %partId, i32 %triangleIndex) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.NodeTriangleCallback*, align 4
  %triangle.addr = alloca %class.btVector3*, align 4
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  %node = alloca %struct.btOptimizedBvhNode, align 4
  %aabbMin = alloca %class.btVector3, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  store %struct.NodeTriangleCallback* %this, %struct.NodeTriangleCallback** %this.addr, align 4
  store %class.btVector3* %triangle, %class.btVector3** %triangle.addr, align 4
  store i32 %partId, i32* %partId.addr, align 4
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4
  %this1 = load %struct.NodeTriangleCallback*, %struct.NodeTriangleCallback** %this.addr, align 4
  %call = call %struct.btOptimizedBvhNode* @_ZN18btOptimizedBvhNodeC2Ev(%struct.btOptimizedBvhNode* %node)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin)
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax)
  store float 0x43ABC16D60000000, float* %ref.tmp, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp4, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp5, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %aabbMin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  store float 0xC3ABC16D60000000, float* %ref.tmp6, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp7, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp8, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %aabbMax, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %0 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  %1 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx9 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx9)
  %2 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 1
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx10)
  %3 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx11 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 1
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx11)
  %4 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx12 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 2
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx12)
  %5 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx13 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 2
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx13)
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %node, i32 0, i32 0
  %6 = bitcast %class.btVector3* %m_aabbMinOrg to i8*
  %7 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %node, i32 0, i32 1
  %8 = bitcast %class.btVector3* %m_aabbMaxOrg to i8*
  %9 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  %m_escapeIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %node, i32 0, i32 2
  store i32 -1, i32* %m_escapeIndex, align 4
  %10 = load i32, i32* %partId.addr, align 4
  %m_subPart = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %node, i32 0, i32 3
  store i32 %10, i32* %m_subPart, align 4
  %11 = load i32, i32* %triangleIndex.addr, align 4
  %m_triangleIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %node, i32 0, i32 4
  store i32 %11, i32* %m_triangleIndex, align 4
  %m_triangleNodes = getelementptr inbounds %struct.NodeTriangleCallback, %struct.NodeTriangleCallback* %this1, i32 0, i32 1
  %12 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %m_triangleNodes, align 4
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE9push_backERKS0_(%class.btAlignedObjectArray* %12, %struct.btOptimizedBvhNode* nonnull align 4 dereferenceable(64) %node)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE9push_backERKS0_(%class.btAlignedObjectArray* %this, %struct.btOptimizedBvhNode* nonnull align 4 dereferenceable(64) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %struct.btOptimizedBvhNode*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %struct.btOptimizedBvhNode* %_Val, %struct.btOptimizedBvhNode** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %1, i32 %2
  %3 = bitcast %struct.btOptimizedBvhNode* %arrayidx to i8*
  %call5 = call i8* @_ZN18btOptimizedBvhNodenwEmPv(i32 64, i8* %3)
  %4 = bitcast i8* %call5 to %struct.btOptimizedBvhNode*
  %5 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %_Val.addr, align 4
  %6 = bitcast %struct.btOptimizedBvhNode* %4 to i8*
  %7 = bitcast %struct.btOptimizedBvhNode* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 64, i1 false)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size6, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btOptimizedBvhNode*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btOptimizedBvhNode*
  store %struct.btOptimizedBvhNode* %2, %struct.btOptimizedBvhNode** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btOptimizedBvhNode* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btOptimizedBvhNode* %4, %struct.btOptimizedBvhNode** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN18btOptimizedBvhNodenwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btOptimizedBvhNode* @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btOptimizedBvhNode** null)
  %2 = bitcast %struct.btOptimizedBvhNode* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btOptimizedBvhNode* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btOptimizedBvhNode*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btOptimizedBvhNode* %dest, %struct.btOptimizedBvhNode** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %3, i32 %4
  %5 = bitcast %struct.btOptimizedBvhNode* %arrayidx to i8*
  %call = call i8* @_ZN18btOptimizedBvhNodenwEmPv(i32 64, i8* %5)
  %6 = bitcast i8* %call to %struct.btOptimizedBvhNode*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %7, i32 %8
  %9 = bitcast %struct.btOptimizedBvhNode* %6 to i8*
  %10 = bitcast %struct.btOptimizedBvhNode* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 64, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4
  %tobool = icmp ne %struct.btOptimizedBvhNode* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.btOptimizedBvhNode* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btOptimizedBvhNode* null, %struct.btOptimizedBvhNode** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btOptimizedBvhNode* @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %struct.btOptimizedBvhNode** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btOptimizedBvhNode**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btOptimizedBvhNode** %hint, %struct.btOptimizedBvhNode*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 64, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btOptimizedBvhNode*
  ret %struct.btOptimizedBvhNode* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.btOptimizedBvhNode* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btOptimizedBvhNode*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %struct.btOptimizedBvhNode* %ptr, %struct.btOptimizedBvhNode** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %ptr.addr, align 4
  %1 = bitcast %struct.btOptimizedBvhNode* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_Z8btSelectjii(i32 %condition, i32 %valueIfConditionNonZero, i32 %valueIfConditionZero) #1 comdat {
entry:
  %condition.addr = alloca i32, align 4
  %valueIfConditionNonZero.addr = alloca i32, align 4
  %valueIfConditionZero.addr = alloca i32, align 4
  %testNz = alloca i32, align 4
  %testEqz = alloca i32, align 4
  store i32 %condition, i32* %condition.addr, align 4
  store i32 %valueIfConditionNonZero, i32* %valueIfConditionNonZero.addr, align 4
  store i32 %valueIfConditionZero, i32* %valueIfConditionZero.addr, align 4
  %0 = load i32, i32* %condition.addr, align 4
  %1 = load i32, i32* %condition.addr, align 4
  %sub = sub nsw i32 0, %1
  %or = or i32 %0, %sub
  %shr = ashr i32 %or, 31
  store i32 %shr, i32* %testNz, align 4
  %2 = load i32, i32* %testNz, align 4
  %neg = xor i32 %2, -1
  store i32 %neg, i32* %testEqz, align 4
  %3 = load i32, i32* %valueIfConditionNonZero.addr, align 4
  %4 = load i32, i32* %testNz, align 4
  %and = and i32 %3, %4
  %5 = load i32, i32* %valueIfConditionZero.addr, align 4
  %6 = load i32, i32* %testEqz, align 4
  %and1 = and i32 %5, %6
  %or2 = or i32 %and, %and1
  ret i32 %or2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %b.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %a.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = load float*, float** %a.addr, align 4
  store float %5, float* %6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %b.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = load float*, float** %a.addr, align 4
  store float %5, float* %6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE8capacityEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btBvhSubtreeInfo*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btBvhSubtreeInfo*
  store %class.btBvhSubtreeInfo* %2, %class.btBvhSubtreeInfo** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %3 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4copyEiiPS0_(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, %class.btBvhSubtreeInfo* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btBvhSubtreeInfo* %4, %class.btBvhSubtreeInfo** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE9allocSizeEi(%class.btAlignedObjectArray.4* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN16btBvhSubtreeInfonwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btBvhSubtreeInfo* @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.5* %m_allocator, i32 %1, %class.btBvhSubtreeInfo** null)
  %2 = bitcast %class.btBvhSubtreeInfo* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4copyEiiPS0_(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, %class.btBvhSubtreeInfo* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btBvhSubtreeInfo*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btBvhSubtreeInfo* %dest, %class.btBvhSubtreeInfo** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %3, i32 %4
  %5 = bitcast %class.btBvhSubtreeInfo* %arrayidx to i8*
  %call = call i8* @_ZN16btBvhSubtreeInfonwEmPv(i32 32, i8* %5)
  %6 = bitcast i8* %call to %class.btBvhSubtreeInfo*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %7 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %7, i32 %8
  %9 = bitcast %class.btBvhSubtreeInfo* %6 to i8*
  %10 = bitcast %class.btBvhSubtreeInfo* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 32, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %3 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE10deallocateEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4
  %tobool = icmp ne %class.btBvhSubtreeInfo* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE10deallocateEPS0_(%class.btAlignedAllocator.5* %m_allocator, %class.btBvhSubtreeInfo* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btBvhSubtreeInfo* null, %class.btBvhSubtreeInfo** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btBvhSubtreeInfo* @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.5* %this, i32 %n, %class.btBvhSubtreeInfo** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btBvhSubtreeInfo**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btBvhSubtreeInfo** %hint, %class.btBvhSubtreeInfo*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 32, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btBvhSubtreeInfo*
  ret %class.btBvhSubtreeInfo* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE10deallocateEPS0_(%class.btAlignedAllocator.5* %this, %class.btBvhSubtreeInfo* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca %class.btBvhSubtreeInfo*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store %class.btBvhSubtreeInfo* %ptr, %class.btBvhSubtreeInfo** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %ptr.addr, align 4
  %1 = bitcast %class.btBvhSubtreeInfo* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btQuantizedBvhNode* null, %struct.btQuantizedBvhNode** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btOptimizedBvhNode* null, %struct.btOptimizedBvhNode** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btOptimizedBvh.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn writeonly }
attributes #6 = { argmemonly nounwind willreturn }
attributes #7 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
