; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/MLCPSolvers/btMLCPSolver.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/MLCPSolvers/btMLCPSolver.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btMLCPSolver = type { %class.btSequentialImpulseConstraintSolver, %struct.btMatrixX, %struct.btVectorX, %struct.btVectorX, %struct.btVectorX, %struct.btVectorX, %struct.btVectorX, %struct.btVectorX, %struct.btVectorX, %struct.btVectorX, %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.30, %class.btMLCPSolverInterface*, i32, %struct.btMatrixX, %struct.btMatrixX, %class.btAlignedObjectArray.14, %struct.btMatrixX, %struct.btMatrixX, %struct.btMatrixX, %struct.btMatrixX }
%class.btSequentialImpulseConstraintSolver = type { %class.btConstraintSolver, %class.btAlignedObjectArray, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.18, i32, i32, %class.btAlignedObjectArray.14, float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)*, float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)*, float, i32 }
%class.btConstraintSolver = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btSolverBody*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.3, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray.0, i32, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.3 = type <{ %class.btAlignedAllocator.4, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.4 = type { i8 }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.6, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.6 = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.9 = type <{ %class.btAlignedAllocator.10, [3 x i8], i32, i32, %struct.btSolverConstraint*, i8, [3 x i8] }>
%class.btAlignedAllocator.10 = type { i8 }
%struct.btSolverConstraint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.12, i32, i32, i32, i32 }
%union.anon.12 = type { i8* }
%class.btAlignedObjectArray.18 = type <{ %class.btAlignedAllocator.19, [3 x i8], i32, i32, %"struct.btTypedConstraint::btConstraintInfo1"*, i8, [3 x i8] }>
%class.btAlignedAllocator.19 = type { i8 }
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%struct.btVectorX = type { %class.btAlignedObjectArray.22 }
%class.btAlignedObjectArray.22 = type <{ %class.btAlignedAllocator.23, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.23 = type { i8 }
%class.btAlignedObjectArray.30 = type <{ %class.btAlignedAllocator.31, [3 x i8], i32, i32, %struct.btSolverConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.31 = type { i8 }
%class.btAlignedObjectArray.14 = type <{ %class.btAlignedAllocator.15, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.15 = type { i8 }
%struct.btMatrixX = type { i32, i32, i32, i32, i32, %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.26 }
%class.btAlignedObjectArray.26 = type <{ %class.btAlignedAllocator.27, [3 x i8], i32, i32, %class.btAlignedObjectArray.14*, i8, [3 x i8] }>
%class.btAlignedAllocator.27 = type { i8 }
%class.btMLCPSolverInterface = type { i32 (...)** }
%class.btPersistentManifold = type opaque
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float, float }
%class.btIDebugDraw = type opaque
%class.CProfileSample = type { i8 }
%class.btAlignedObjectArray.34 = type <{ %class.btAlignedAllocator.35, [3 x i8], i32, i32, %struct.btJointNode*, i8, [3 x i8] }>
%class.btAlignedAllocator.35 = type { i8 }
%struct.btJointNode = type { i32, i32, i32, i32 }
%class.btDispatcher = type opaque

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN9btMatrixXIfEC2Ev = comdat any

$_ZN9btVectorXIfEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP18btSolverConstraintEC2Ev = comdat any

$_ZN9btMatrixXIfED2Ev = comdat any

$_ZN20btAlignedObjectArrayIiED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP18btSolverConstraintED2Ev = comdat any

$_ZN9btVectorXIfED2Ev = comdat any

$_ZN35btSequentialImpulseConstraintSolverdlEPv = comdat any

$_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP18btSolverConstraintE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayIiE6resizeEiRKi = comdat any

$_ZN20btAlignedObjectArrayIP18btSolverConstraintE9push_backERKS1_ = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv = comdat any

$_ZN9btMatrixXIfE6resizeEii = comdat any

$_ZN9btVectorXIfE6resizeEi = comdat any

$_ZNK9btMatrixXIfE4rowsEv = comdat any

$_ZN9btMatrixXIfEC2ERKS0_ = comdat any

$_ZN20btAlignedObjectArrayIiEC2ERKS0_ = comdat any

$_ZN9btVectorXIfE7setZeroEv = comdat any

$_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi = comdat any

$_Z11btFuzzyZerof = comdat any

$_ZN9btVectorXIfEixEi = comdat any

$_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeE7reserveEi = comdat any

$_ZN9btMatrixXIfE7setZeroEv = comdat any

$_ZN20btAlignedObjectArrayIiE18resizeNoInitializeEi = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyEixEi = comdat any

$_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEixEi = comdat any

$_ZNK20btAlignedObjectArrayI11btJointNodeE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeE6expandERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeEixEi = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK11btRigidBody10getInvMassEv = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZNK11btRigidBody24getInvInertiaTensorWorldEv = comdat any

$_ZN9btMatrixXIfE7setElemEiif = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK9btMatrixXIfE16getBufferPointerEv = comdat any

$_ZN9btMatrixXIfE16multiplyAdd2_p8rEPKfS2_iiii = comdat any

$_ZN9btMatrixXIfE13multiply2_p8rEPKfS2_iiii = comdat any

$_ZNK9btMatrixXIfEclEii = comdat any

$_ZN9btMatrixXIfE24copyLowerToUpperTriangleEv = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeED2Ev = comdat any

$_Z7setElemR9btMatrixXIfEiif = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btMatrixXIfE9transposeEv = comdat any

$_ZN9btMatrixXIfEaSEOS0_ = comdat any

$_ZN9btMatrixXIfEmlERKS0_ = comdat any

$_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZNK12btSolverBody18internalGetInvMassEv = comdat any

$_ZN12btSolverBody24internalApplyPushImpulseERK9btVector3S2_f = comdat any

$_ZN18btConstraintSolver12prepareSolveEii = comdat any

$_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDraw = comdat any

$_ZNK12btMLCPSolver13getSolverTypeEv = comdat any

$_ZN20btAlignedObjectArrayIS_IiEED2Ev = comdat any

$_ZN20btAlignedObjectArrayIfED2Ev = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE4initEv = comdat any

$_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIfE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIfE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIfE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIfE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIfE4initEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf = comdat any

$_ZN20btAlignedObjectArrayIfEC2ERKS0_ = comdat any

$_ZN20btAlignedObjectArrayIS_IiEEC2ERKS1_ = comdat any

$_ZN18btAlignedAllocatorIfLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIfE6resizeEiRKf = comdat any

$_ZNK20btAlignedObjectArrayIfE4copyEiiPf = comdat any

$_ZN20btAlignedObjectArrayIfE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIfE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIfE8allocateEi = comdat any

$_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf = comdat any

$_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE6resizeEiRKS0_ = comdat any

$_ZNK20btAlignedObjectArrayIS_IiEE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIS_IiEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE8allocateEi = comdat any

$_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EE8allocateEiPPKS1_ = comdat any

$_Z6btFabsf = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN20btAlignedObjectArrayIfEaSERKS0_ = comdat any

$_ZN20btAlignedObjectArrayIS_IiEEaSERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIfE13copyFromArrayERKS0_ = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE13copyFromArrayERKS1_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN20btAlignedObjectArrayIfEixEi = comdat any

$_ZNK20btAlignedObjectArrayIfEixEi = comdat any

$_ZN20btAlignedObjectArrayIfEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIS_IiEEC2Ev = comdat any

$_ZN18btAlignedAllocatorIiLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZN18btAlignedAllocatorIP18btSolverConstraintLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP18btSolverConstraintE4initEv = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZN20btAlignedObjectArrayIP18btSolverConstraintE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP18btSolverConstraintE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP18btSolverConstraintE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP18btSolverConstraintLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP18btSolverConstraintE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIP18btSolverConstraintE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP18btSolverConstraintE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP18btSolverConstraintLj16EE8allocateEiPPKS1_ = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

$_ZN20btAlignedObjectArrayIP18btSolverConstraintE9allocSizeEi = comdat any

$_Z9btSetZeroIfEvPT_i = comdat any

$_ZN18btAlignedAllocatorI11btJointNodeLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeE4initEv = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI11btJointNodeLj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayI11btJointNodeE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI11btJointNodeE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI11btJointNodeLj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayI11btJointNodeE9allocSizeEi = comdat any

$_ZN9btMatrixXIfE7addElemEiif = comdat any

$_ZN9btMatrixXIfEC2Eii = comdat any

$_ZNK9btMatrixXIfE4colsEv = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV12btMLCPSolver = hidden unnamed_addr constant { [18 x i8*] } { [18 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI12btMLCPSolver to i8*), i8* bitcast (%class.btMLCPSolver* (%class.btMLCPSolver*)* @_ZN12btMLCPSolverD1Ev to i8*), i8* bitcast (void (%class.btMLCPSolver*)* @_ZN12btMLCPSolverD0Ev to i8*), i8* bitcast (void (%class.btConstraintSolver*, i32, i32)* @_ZN18btConstraintSolver12prepareSolveEii to i8*), i8* bitcast (float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)* @_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher to i8*), i8* bitcast (void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (void (%class.btSequentialImpulseConstraintSolver*)* @_ZN35btSequentialImpulseConstraintSolver5resetEv to i8*), i8* bitcast (i32 (%class.btMLCPSolver*)* @_ZNK12btMLCPSolver13getSolverTypeEv to i8*), i8* bitcast (void (%class.btSequentialImpulseConstraintSolver*, %class.btPersistentManifold**, i32, %struct.btContactSolverInfo*)* @_ZN35btSequentialImpulseConstraintSolver15convertContactsEPP20btPersistentManifoldiRK19btContactSolverInfo to i8*), i8* bitcast (void (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN35btSequentialImpulseConstraintSolver45solveGroupCacheFriendlySplitImpulseIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %struct.btContactSolverInfo*)* @_ZN35btSequentialImpulseConstraintSolver29solveGroupCacheFriendlyFinishEPP17btCollisionObjectiRK19btContactSolverInfo to i8*), i8* bitcast (float (%class.btSequentialImpulseConstraintSolver*, i32, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN35btSequentialImpulseConstraintSolver20solveSingleIterationEiPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (float (%class.btMLCPSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN12btMLCPSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (float (%class.btMLCPSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN12btMLCPSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)* @_ZN12btMLCPSolver10createMLCPERK19btContactSolverInfo to i8*), i8* bitcast (void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)* @_ZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfo to i8*), i8* bitcast (i1 (%class.btMLCPSolver*, %struct.btContactSolverInfo*)* @_ZN12btMLCPSolver9solveMLCPERK19btContactSolverInfo to i8*)] }, align 4
@gUseMatrixMultiply = hidden global i8 0, align 1
@interleaveContactAndFriction = hidden global i8 0, align 1
@.str = private unnamed_addr constant [23 x i8] c"gather constraint data\00", align 1
@.str.1 = private unnamed_addr constant [11 x i8] c"createMLCP\00", align 1
@.str.2 = private unnamed_addr constant [15 x i8] c"createMLCPFast\00", align 1
@.str.3 = private unnamed_addr constant [13 x i8] c"init b (rhs)\00", align 1
@.str.4 = private unnamed_addr constant [11 x i8] c"init lo/ho\00", align 1
@.str.5 = private unnamed_addr constant [26 x i8] c"bodyJointNodeArray.resize\00", align 1
@.str.6 = private unnamed_addr constant [23 x i8] c"jointNodeArray.reserve\00", align 1
@.str.7 = private unnamed_addr constant [10 x i8] c"J3.resize\00", align 1
@.str.8 = private unnamed_addr constant [22 x i8] c"JinvM3.resize/setZero\00", align 1
@.str.9 = private unnamed_addr constant [11 x i8] c"ofs resize\00", align 1
@.str.10 = private unnamed_addr constant [20 x i8] c"Compute J and JinvM\00", align 1
@.str.11 = private unnamed_addr constant [11 x i8] c"m_A.resize\00", align 1
@.str.12 = private unnamed_addr constant [12 x i8] c"m_A.setZero\00", align 1
@.str.13 = private unnamed_addr constant [10 x i8] c"Compute A\00", align 1
@.str.14 = private unnamed_addr constant [17 x i8] c"compute diagonal\00", align 1
@.str.15 = private unnamed_addr constant [25 x i8] c"fill the upper triangle \00", align 1
@.str.16 = private unnamed_addr constant [14 x i8] c"resize/init x\00", align 1
@.str.17 = private unnamed_addr constant [7 x i8] c"J*Minv\00", align 1
@.str.18 = private unnamed_addr constant [6 x i8] c"J*tmp\00", align 1
@.str.19 = private unnamed_addr constant [10 x i8] c"solveMLCP\00", align 1
@.str.20 = private unnamed_addr constant [21 x i8] c"process MLCP results\00", align 1
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS12btMLCPSolver = hidden constant [15 x i8] c"12btMLCPSolver\00", align 1
@_ZTI35btSequentialImpulseConstraintSolver = external constant i8*
@_ZTI12btMLCPSolver = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([15 x i8], [15 x i8]* @_ZTS12btMLCPSolver, i32 0, i32 0), i8* bitcast (i8** @_ZTI35btSequentialImpulseConstraintSolver to i8*) }, align 4
@.str.21 = private unnamed_addr constant [17 x i8] c"m_storage.resize\00", align 1
@.str.22 = private unnamed_addr constant [10 x i8] c"storage=0\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btMLCPSolver.cpp, i8* null }]

@_ZN12btMLCPSolverC1EP21btMLCPSolverInterface = hidden unnamed_addr alias %class.btMLCPSolver* (%class.btMLCPSolver*, %class.btMLCPSolverInterface*), %class.btMLCPSolver* (%class.btMLCPSolver*, %class.btMLCPSolverInterface*)* @_ZN12btMLCPSolverC2EP21btMLCPSolverInterface
@_ZN12btMLCPSolverD1Ev = hidden unnamed_addr alias %class.btMLCPSolver* (%class.btMLCPSolver*), %class.btMLCPSolver* (%class.btMLCPSolver*)* @_ZN12btMLCPSolverD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btMLCPSolver* @_ZN12btMLCPSolverC2EP21btMLCPSolverInterface(%class.btMLCPSolver* returned %this, %class.btMLCPSolverInterface* %solver) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMLCPSolver*, align 4
  %solver.addr = alloca %class.btMLCPSolverInterface*, align 4
  store %class.btMLCPSolver* %this, %class.btMLCPSolver** %this.addr, align 4
  store %class.btMLCPSolverInterface* %solver, %class.btMLCPSolverInterface** %solver.addr, align 4
  %this1 = load %class.btMLCPSolver*, %class.btMLCPSolver** %this.addr, align 4
  %0 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %call = call %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverC2Ev(%class.btSequentialImpulseConstraintSolver* %0)
  %1 = bitcast %class.btMLCPSolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [18 x i8*] }, { [18 x i8*] }* @_ZTV12btMLCPSolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_A = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %call2 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Ev(%struct.btMatrixX* %m_A)
  %m_b = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  %call3 = call %struct.btVectorX* @_ZN9btVectorXIfEC2Ev(%struct.btVectorX* %m_b)
  %m_x = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %call4 = call %struct.btVectorX* @_ZN9btVectorXIfEC2Ev(%struct.btVectorX* %m_x)
  %m_lo = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  %call5 = call %struct.btVectorX* @_ZN9btVectorXIfEC2Ev(%struct.btVectorX* %m_lo)
  %m_hi = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  %call6 = call %struct.btVectorX* @_ZN9btVectorXIfEC2Ev(%struct.btVectorX* %m_hi)
  %m_bSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 6
  %call7 = call %struct.btVectorX* @_ZN9btVectorXIfEC2Ev(%struct.btVectorX* %m_bSplit)
  %m_xSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  %call8 = call %struct.btVectorX* @_ZN9btVectorXIfEC2Ev(%struct.btVectorX* %m_xSplit)
  %m_bSplit1 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 8
  %call9 = call %struct.btVectorX* @_ZN9btVectorXIfEC2Ev(%struct.btVectorX* %m_bSplit1)
  %m_xSplit2 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 9
  %call10 = call %struct.btVectorX* @_ZN9btVectorXIfEC2Ev(%struct.btVectorX* %m_xSplit2)
  %m_limitDependencies = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %call11 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.14* %m_limitDependencies)
  %m_allConstraintPtrArray = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call12 = call %class.btAlignedObjectArray.30* @_ZN20btAlignedObjectArrayIP18btSolverConstraintEC2Ev(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray)
  %m_solver = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 12
  %2 = load %class.btMLCPSolverInterface*, %class.btMLCPSolverInterface** %solver.addr, align 4
  store %class.btMLCPSolverInterface* %2, %class.btMLCPSolverInterface** %m_solver, align 4
  %m_fallback = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 13
  store i32 0, i32* %m_fallback, align 4
  %m_scratchJ3 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 14
  %call13 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Ev(%struct.btMatrixX* %m_scratchJ3)
  %m_scratchJInvM3 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 15
  %call14 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Ev(%struct.btMatrixX* %m_scratchJInvM3)
  %m_scratchOfs = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 16
  %call15 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.14* %m_scratchOfs)
  %m_scratchMInv = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 17
  %call16 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Ev(%struct.btMatrixX* %m_scratchMInv)
  %m_scratchJ = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 18
  %call17 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Ev(%struct.btMatrixX* %m_scratchJ)
  %m_scratchJTranspose = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 19
  %call18 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Ev(%struct.btMatrixX* %m_scratchJTranspose)
  %m_scratchTmp = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 20
  %call19 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Ev(%struct.btMatrixX* %m_scratchTmp)
  ret %class.btMLCPSolver* %this1
}

declare %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverC2Ev(%class.btSequentialImpulseConstraintSolver* returned) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btMatrixX* @_ZN9btMatrixXIfEC2Ev(%struct.btMatrixX* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_rows = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  store i32 0, i32* %m_rows, align 4
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  store i32 0, i32* %m_cols, align 4
  %m_operations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 2
  store i32 0, i32* %m_operations, align 4
  %m_resizeOperations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 3
  store i32 0, i32* %m_resizeOperations, align 4
  %m_setElemOperations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 4
  store i32 0, i32* %m_setElemOperations, align 4
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %call = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.22* %m_storage)
  %m_rowNonZeroElements1 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %call2 = call %class.btAlignedObjectArray.26* @_ZN20btAlignedObjectArrayIS_IiEEC2Ev(%class.btAlignedObjectArray.26* %m_rowNonZeroElements1)
  ret %struct.btMatrixX* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btVectorX* @_ZN9btVectorXIfEC2Ev(%struct.btVectorX* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.22* %m_storage)
  ret %struct.btVectorX* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.14* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.15* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.15* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.14* %this1)
  ret %class.btAlignedObjectArray.14* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.30* @_ZN20btAlignedObjectArrayIP18btSolverConstraintEC2Ev(%class.btAlignedObjectArray.30* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.31* @_ZN18btAlignedAllocatorIP18btSolverConstraintLj16EEC2Ev(%class.btAlignedAllocator.31* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE4initEv(%class.btAlignedObjectArray.30* %this1)
  ret %class.btAlignedObjectArray.30* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btMLCPSolver* @_ZN12btMLCPSolverD2Ev(%class.btMLCPSolver* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btMLCPSolver*, align 4
  store %class.btMLCPSolver* %this, %class.btMLCPSolver** %this.addr, align 4
  %this1 = load %class.btMLCPSolver*, %class.btMLCPSolver** %this.addr, align 4
  %0 = bitcast %class.btMLCPSolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [18 x i8*] }, { [18 x i8*] }* @_ZTV12btMLCPSolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_scratchTmp = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 20
  %call = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %m_scratchTmp) #8
  %m_scratchJTranspose = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 19
  %call2 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %m_scratchJTranspose) #8
  %m_scratchJ = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 18
  %call3 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %m_scratchJ) #8
  %m_scratchMInv = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 17
  %call4 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %m_scratchMInv) #8
  %m_scratchOfs = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 16
  %call5 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.14* %m_scratchOfs) #8
  %m_scratchJInvM3 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 15
  %call6 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %m_scratchJInvM3) #8
  %m_scratchJ3 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 14
  %call7 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %m_scratchJ3) #8
  %m_allConstraintPtrArray = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call8 = call %class.btAlignedObjectArray.30* @_ZN20btAlignedObjectArrayIP18btSolverConstraintED2Ev(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray) #8
  %m_limitDependencies = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %call9 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.14* %m_limitDependencies) #8
  %m_xSplit2 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 9
  %call10 = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %m_xSplit2) #8
  %m_bSplit1 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 8
  %call11 = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %m_bSplit1) #8
  %m_xSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  %call12 = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %m_xSplit) #8
  %m_bSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 6
  %call13 = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %m_bSplit) #8
  %m_hi = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  %call14 = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %m_hi) #8
  %m_lo = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  %call15 = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %m_lo) #8
  %m_x = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %call16 = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %m_x) #8
  %m_b = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  %call17 = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %m_b) #8
  %m_A = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %call18 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %m_A) #8
  %1 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %call19 = call %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverD2Ev(%class.btSequentialImpulseConstraintSolver* %1) #8
  ret %class.btMLCPSolver* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_rowNonZeroElements1 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %call = call %class.btAlignedObjectArray.26* @_ZN20btAlignedObjectArrayIS_IiEED2Ev(%class.btAlignedObjectArray.26* %m_rowNonZeroElements1) #8
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %call2 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.22* %m_storage) #8
  ret %struct.btMatrixX* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.14* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.14* %this1)
  ret %class.btAlignedObjectArray.14* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.30* @_ZN20btAlignedObjectArrayIP18btSolverConstraintED2Ev(%class.btAlignedObjectArray.30* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE5clearEv(%class.btAlignedObjectArray.30* %this1)
  ret %class.btAlignedObjectArray.30* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.22* %m_storage) #8
  ret %struct.btVectorX* %this1
}

; Function Attrs: nounwind
declare %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverD2Ev(%class.btSequentialImpulseConstraintSolver* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN12btMLCPSolverD0Ev(%class.btMLCPSolver* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btMLCPSolver*, align 4
  store %class.btMLCPSolver* %this, %class.btMLCPSolver** %this.addr, align 4
  %this1 = load %class.btMLCPSolver*, %class.btMLCPSolver** %this.addr, align 4
  %call = call %class.btMLCPSolver* @_ZN12btMLCPSolverD1Ev(%class.btMLCPSolver* %this1) #8
  %0 = bitcast %class.btMLCPSolver* %this1 to i8*
  call void @_ZN35btSequentialImpulseConstraintSolverdlEPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN35btSequentialImpulseConstraintSolverdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden float @_ZN12btMLCPSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btMLCPSolver* %this, %class.btCollisionObject** %bodies, i32 %numBodiesUnUsed, %class.btPersistentManifold** %manifoldPtr, i32 %numManifolds, %class.btTypedConstraint** %constraints, i32 %numConstraints, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal, %class.btIDebugDraw* %debugDrawer) unnamed_addr #2 {
entry:
  %retval = alloca float, align 4
  %this.addr = alloca %class.btMLCPSolver*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodiesUnUsed.addr = alloca i32, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %numFrictionPerContact = alloca i32, align 4
  %ref.tmp = alloca %struct.btSolverConstraint*, align 4
  %ref.tmp11 = alloca i32, align 4
  %dindex = alloca i32, align 4
  %i = alloca i32, align 4
  %ref.tmp16 = alloca %struct.btSolverConstraint*, align 4
  %firstContactConstraintOffset = alloca i32, align 4
  %i22 = alloca i32, align 4
  %ref.tmp29 = alloca %struct.btSolverConstraint*, align 4
  %ref.tmp36 = alloca %struct.btSolverConstraint*, align 4
  %findex = alloca i32, align 4
  %ref.tmp51 = alloca %struct.btSolverConstraint*, align 4
  %i63 = alloca i32, align 4
  %ref.tmp70 = alloca %struct.btSolverConstraint*, align 4
  %i79 = alloca i32, align 4
  %ref.tmp86 = alloca %struct.btSolverConstraint*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %__profile108 = alloca %class.CProfileSample, align 1
  %__profile112 = alloca %class.CProfileSample, align 1
  store %class.btMLCPSolver* %this, %class.btMLCPSolver** %this.addr, align 4
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4
  store i32 %numBodiesUnUsed, i32* %numBodiesUnUsed.addr, align 4
  store %class.btPersistentManifold** %manifoldPtr, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  store i32 %numManifolds, i32* %numManifolds.addr, align 4
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4
  store i32 %numConstraints, i32* %numConstraints.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %this1 = load %class.btMLCPSolver*, %class.btMLCPSolver** %this.addr, align 4
  %0 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %1 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %2 = load i32, i32* %numBodiesUnUsed.addr, align 4
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  %4 = load i32, i32* %numManifolds.addr, align 4
  %5 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %6 = load i32, i32* %numConstraints.addr, align 4
  %7 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %8 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %call = call float @_ZN35btSequentialImpulseConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver* %0, %class.btCollisionObject** %1, i32 %2, %class.btPersistentManifold** %3, i32 %4, %class.btTypedConstraint** %5, i32 %6, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %7, %class.btIDebugDraw* %8)
  %call2 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str, i32 0, i32 0))
  %9 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %9, i32 0, i32 2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool)
  %10 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %10, i32 0, i32 4
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool)
  %cmp = icmp eq i32 %call3, %call4
  %11 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 1, i32 2
  store i32 %cond, i32* %numFrictionPerContact, align 4
  %m_allConstraintPtrArray = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  store %struct.btSolverConstraint* null, %struct.btSolverConstraint** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray, i32 0, %struct.btSolverConstraint** nonnull align 4 dereferenceable(4) %ref.tmp)
  %m_limitDependencies = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %12 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %12, i32 0, i32 3
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool)
  %13 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool6 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %13, i32 0, i32 2
  %call7 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool6)
  %add = add nsw i32 %call5, %call7
  %14 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool8 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %14, i32 0, i32 4
  %call9 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool8)
  %add10 = add nsw i32 %add, %call9
  store i32 0, i32* %ref.tmp11, align 4
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.14* %m_limitDependencies, i32 %add10, i32* nonnull align 4 dereferenceable(4) %ref.tmp11)
  store i32 0, i32* %dindex, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %15 = load i32, i32* %i, align 4
  %16 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool12 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %16, i32 0, i32 3
  %call13 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool12)
  %cmp14 = icmp slt i32 %15, %call13
  br i1 %cmp14, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_allConstraintPtrArray15 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %17 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool17 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %17, i32 0, i32 3
  %18 = load i32, i32* %i, align 4
  %call18 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool17, i32 %18)
  store %struct.btSolverConstraint* %call18, %struct.btSolverConstraint** %ref.tmp16, align 4
  call void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE9push_backERKS1_(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray15, %struct.btSolverConstraint** nonnull align 4 dereferenceable(4) %ref.tmp16)
  %m_limitDependencies19 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %19 = load i32, i32* %dindex, align 4
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %dindex, align 4
  %call20 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_limitDependencies19, i32 %19)
  store i32 -1, i32* %call20, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %20 = load i32, i32* %i, align 4
  %inc21 = add nsw i32 %20, 1
  store i32 %inc21, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %21 = load i32, i32* %dindex, align 4
  store i32 %21, i32* %firstContactConstraintOffset, align 4
  %22 = load i8, i8* @interleaveContactAndFriction, align 1
  %tobool = trunc i8 %22 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.end
  store i32 0, i32* %i22, align 4
  br label %for.cond23

for.cond23:                                       ; preds = %for.inc60, %if.then
  %23 = load i32, i32* %i22, align 4
  %24 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool24 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %24, i32 0, i32 2
  %call25 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool24)
  %cmp26 = icmp slt i32 %23, %call25
  br i1 %cmp26, label %for.body27, label %for.end62

for.body27:                                       ; preds = %for.cond23
  %m_allConstraintPtrArray28 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %25 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool30 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %25, i32 0, i32 2
  %26 = load i32, i32* %i22, align 4
  %call31 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool30, i32 %26)
  store %struct.btSolverConstraint* %call31, %struct.btSolverConstraint** %ref.tmp29, align 4
  call void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE9push_backERKS1_(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray28, %struct.btSolverConstraint** nonnull align 4 dereferenceable(4) %ref.tmp29)
  %m_limitDependencies32 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %27 = load i32, i32* %dindex, align 4
  %inc33 = add nsw i32 %27, 1
  store i32 %inc33, i32* %dindex, align 4
  %call34 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_limitDependencies32, i32 %27)
  store i32 -1, i32* %call34, align 4
  %m_allConstraintPtrArray35 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %28 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool37 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %28, i32 0, i32 4
  %29 = load i32, i32* %i22, align 4
  %30 = load i32, i32* %numFrictionPerContact, align 4
  %mul = mul nsw i32 %29, %30
  %call38 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool37, i32 %mul)
  store %struct.btSolverConstraint* %call38, %struct.btSolverConstraint** %ref.tmp36, align 4
  call void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE9push_backERKS1_(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray35, %struct.btSolverConstraint** nonnull align 4 dereferenceable(4) %ref.tmp36)
  %31 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool39 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %31, i32 0, i32 4
  %32 = load i32, i32* %i22, align 4
  %33 = load i32, i32* %numFrictionPerContact, align 4
  %mul40 = mul nsw i32 %32, %33
  %call41 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool39, i32 %mul40)
  %m_frictionIndex = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call41, i32 0, i32 17
  %34 = load i32, i32* %m_frictionIndex, align 4
  %35 = load i32, i32* %numFrictionPerContact, align 4
  %add42 = add nsw i32 1, %35
  %mul43 = mul nsw i32 %34, %add42
  store i32 %mul43, i32* %findex, align 4
  %36 = load i32, i32* %findex, align 4
  %37 = load i32, i32* %firstContactConstraintOffset, align 4
  %add44 = add nsw i32 %36, %37
  %m_limitDependencies45 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %38 = load i32, i32* %dindex, align 4
  %inc46 = add nsw i32 %38, 1
  store i32 %inc46, i32* %dindex, align 4
  %call47 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_limitDependencies45, i32 %38)
  store i32 %add44, i32* %call47, align 4
  %39 = load i32, i32* %numFrictionPerContact, align 4
  %cmp48 = icmp eq i32 %39, 2
  br i1 %cmp48, label %if.then49, label %if.end

if.then49:                                        ; preds = %for.body27
  %m_allConstraintPtrArray50 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %40 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool52 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %40, i32 0, i32 4
  %41 = load i32, i32* %i22, align 4
  %42 = load i32, i32* %numFrictionPerContact, align 4
  %mul53 = mul nsw i32 %41, %42
  %add54 = add nsw i32 %mul53, 1
  %call55 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool52, i32 %add54)
  store %struct.btSolverConstraint* %call55, %struct.btSolverConstraint** %ref.tmp51, align 4
  call void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE9push_backERKS1_(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray50, %struct.btSolverConstraint** nonnull align 4 dereferenceable(4) %ref.tmp51)
  %43 = load i32, i32* %findex, align 4
  %44 = load i32, i32* %firstContactConstraintOffset, align 4
  %add56 = add nsw i32 %43, %44
  %m_limitDependencies57 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %45 = load i32, i32* %dindex, align 4
  %inc58 = add nsw i32 %45, 1
  store i32 %inc58, i32* %dindex, align 4
  %call59 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_limitDependencies57, i32 %45)
  store i32 %add56, i32* %call59, align 4
  br label %if.end

if.end:                                           ; preds = %if.then49, %for.body27
  br label %for.inc60

for.inc60:                                        ; preds = %if.end
  %46 = load i32, i32* %i22, align 4
  %inc61 = add nsw i32 %46, 1
  store i32 %inc61, i32* %i22, align 4
  br label %for.cond23

for.end62:                                        ; preds = %for.cond23
  br label %if.end99

if.else:                                          ; preds = %for.end
  store i32 0, i32* %i63, align 4
  br label %for.cond64

for.cond64:                                       ; preds = %for.inc76, %if.else
  %47 = load i32, i32* %i63, align 4
  %48 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool65 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %48, i32 0, i32 2
  %call66 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool65)
  %cmp67 = icmp slt i32 %47, %call66
  br i1 %cmp67, label %for.body68, label %for.end78

for.body68:                                       ; preds = %for.cond64
  %m_allConstraintPtrArray69 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %49 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool71 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %49, i32 0, i32 2
  %50 = load i32, i32* %i63, align 4
  %call72 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool71, i32 %50)
  store %struct.btSolverConstraint* %call72, %struct.btSolverConstraint** %ref.tmp70, align 4
  call void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE9push_backERKS1_(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray69, %struct.btSolverConstraint** nonnull align 4 dereferenceable(4) %ref.tmp70)
  %m_limitDependencies73 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %51 = load i32, i32* %dindex, align 4
  %inc74 = add nsw i32 %51, 1
  store i32 %inc74, i32* %dindex, align 4
  %call75 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_limitDependencies73, i32 %51)
  store i32 -1, i32* %call75, align 4
  br label %for.inc76

for.inc76:                                        ; preds = %for.body68
  %52 = load i32, i32* %i63, align 4
  %inc77 = add nsw i32 %52, 1
  store i32 %inc77, i32* %i63, align 4
  br label %for.cond64

for.end78:                                        ; preds = %for.cond64
  store i32 0, i32* %i79, align 4
  br label %for.cond80

for.cond80:                                       ; preds = %for.inc96, %for.end78
  %53 = load i32, i32* %i79, align 4
  %54 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool81 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %54, i32 0, i32 4
  %call82 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool81)
  %cmp83 = icmp slt i32 %53, %call82
  br i1 %cmp83, label %for.body84, label %for.end98

for.body84:                                       ; preds = %for.cond80
  %m_allConstraintPtrArray85 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %55 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool87 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %55, i32 0, i32 4
  %56 = load i32, i32* %i79, align 4
  %call88 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool87, i32 %56)
  store %struct.btSolverConstraint* %call88, %struct.btSolverConstraint** %ref.tmp86, align 4
  call void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE9push_backERKS1_(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray85, %struct.btSolverConstraint** nonnull align 4 dereferenceable(4) %ref.tmp86)
  %57 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool89 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %57, i32 0, i32 4
  %58 = load i32, i32* %i79, align 4
  %call90 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool89, i32 %58)
  %m_frictionIndex91 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call90, i32 0, i32 17
  %59 = load i32, i32* %m_frictionIndex91, align 4
  %60 = load i32, i32* %firstContactConstraintOffset, align 4
  %add92 = add nsw i32 %59, %60
  %m_limitDependencies93 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %61 = load i32, i32* %dindex, align 4
  %inc94 = add nsw i32 %61, 1
  store i32 %inc94, i32* %dindex, align 4
  %call95 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_limitDependencies93, i32 %61)
  store i32 %add92, i32* %call95, align 4
  br label %for.inc96

for.inc96:                                        ; preds = %for.body84
  %62 = load i32, i32* %i79, align 4
  %inc97 = add nsw i32 %62, 1
  store i32 %inc97, i32* %i79, align 4
  br label %for.cond80

for.end98:                                        ; preds = %for.cond80
  br label %if.end99

if.end99:                                         ; preds = %for.end98, %for.end62
  %m_allConstraintPtrArray100 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call101 = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray100)
  %tobool102 = icmp ne i32 %call101, 0
  br i1 %tobool102, label %if.end104, label %if.then103

if.then103:                                       ; preds = %if.end99
  %m_A = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  call void @_ZN9btMatrixXIfE6resizeEii(%struct.btMatrixX* %m_A, i32 0, i32 0)
  %m_b = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_b, i32 0)
  %m_x = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_x, i32 0)
  %m_lo = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_lo, i32 0)
  %m_hi = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_hi, i32 0)
  store float 0.000000e+00, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end104:                                        ; preds = %if.end99
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end104, %if.then103
  %call105 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
  ]

cleanup.cont:                                     ; preds = %cleanup
  %63 = load i8, i8* @gUseMatrixMultiply, align 1
  %tobool106 = trunc i8 %63 to i1
  br i1 %tobool106, label %if.then107, label %if.else111

if.then107:                                       ; preds = %cleanup.cont
  %call109 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile108, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.1, i32 0, i32 0))
  %64 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %65 = bitcast %class.btMLCPSolver* %this1 to void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)***
  %vtable = load void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)**, void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)*** %65, align 4
  %vfn = getelementptr inbounds void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)*, void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)** %vtable, i64 13
  %66 = load void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)*, void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)** %vfn, align 4
  call void %66(%class.btMLCPSolver* %this1, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %64)
  %call110 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile108) #8
  br label %if.end117

if.else111:                                       ; preds = %cleanup.cont
  %call113 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile112, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.2, i32 0, i32 0))
  %67 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %68 = bitcast %class.btMLCPSolver* %this1 to void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)***
  %vtable114 = load void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)**, void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)*** %68, align 4
  %vfn115 = getelementptr inbounds void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)*, void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)** %vtable114, i64 14
  %69 = load void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)*, void (%class.btMLCPSolver*, %struct.btContactSolverInfo*)** %vfn115, align 4
  call void %69(%class.btMLCPSolver* %this1, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %67)
  %call116 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile112) #8
  br label %if.end117

if.end117:                                        ; preds = %if.else111, %if.then107
  store float 0.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end117, %cleanup
  %70 = load float, float* %retval, align 4
  ret float %70

unreachable:                                      ; preds = %cleanup
  unreachable
}

declare float @_ZN35btSequentialImpulseConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88), %class.btIDebugDraw*) unnamed_addr #3

declare %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* returned, i8*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.30* %this, i32 %newsize, %struct.btSolverConstraint** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btSolverConstraint**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %struct.btSolverConstraint** %fillData, %struct.btSolverConstraint*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.30* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  %5 = load %struct.btSolverConstraint**, %struct.btSolverConstraint*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverConstraint*, %struct.btSolverConstraint** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE7reserveEi(%class.btAlignedObjectArray.30* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  %14 = load %struct.btSolverConstraint**, %struct.btSolverConstraint*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %struct.btSolverConstraint*, %struct.btSolverConstraint** %14, i32 %15
  %16 = bitcast %struct.btSolverConstraint** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %struct.btSolverConstraint**
  %18 = load %struct.btSolverConstraint**, %struct.btSolverConstraint*** %fillData.addr, align 4
  %19 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %18, align 4
  store %struct.btSolverConstraint* %19, %struct.btSolverConstraint** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.14* %this, i32 %newsize, i32* nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i32*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store i32* %fillData, i32** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.14* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %5 = load i32*, i32** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.14* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %14 = load i32*, i32** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds i32, i32* %14, i32 %15
  %16 = bitcast i32* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to i32*
  %18 = load i32*, i32** %fillData.addr, align 4
  %19 = load i32, i32* %18, align 4
  store i32 %19, i32* %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE9push_backERKS1_(%class.btAlignedObjectArray.30* %this, %struct.btSolverConstraint** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  %_Val.addr = alloca %struct.btSolverConstraint**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4
  store %struct.btSolverConstraint** %_Val, %struct.btSolverConstraint*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.30* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE8capacityEv(%class.btAlignedObjectArray.30* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.30* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP18btSolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.30* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE7reserveEi(%class.btAlignedObjectArray.30* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  %1 = load %struct.btSolverConstraint**, %struct.btSolverConstraint*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverConstraint*, %struct.btSolverConstraint** %1, i32 %2
  %3 = bitcast %struct.btSolverConstraint** %arrayidx to i8*
  %4 = bitcast i8* %3 to %struct.btSolverConstraint**
  %5 = load %struct.btSolverConstraint**, %struct.btSolverConstraint*** %_Val.addr, align 4
  %6 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %5, align 4
  store %struct.btSolverConstraint* %6, %struct.btSolverConstraint** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %0, i32 %1
  ret %struct.btSolverConstraint* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.30* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btMatrixXIfE6resizeEii(%struct.btMatrixX* %this, i32 %rows, i32 %cols) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %rows.addr = alloca i32, align 4
  %cols.addr = alloca i32, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %ref.tmp = alloca float, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  store i32 %rows, i32* %rows.addr, align 4
  store i32 %cols, i32* %cols.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_resizeOperations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_resizeOperations, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_resizeOperations, align 4
  %1 = load i32, i32* %rows.addr, align 4
  %m_rows = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  store i32 %1, i32* %m_rows, align 4
  %2 = load i32, i32* %cols.addr, align 4
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  store i32 %2, i32* %m_cols, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.21, i32 0, i32 0))
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %3 = load i32, i32* %rows.addr, align 4
  %4 = load i32, i32* %cols.addr, align 4
  %mul = mul nsw i32 %3, %4
  store float 0.000000e+00, float* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.22* %m_storage, i32 %mul, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %call2 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #8
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %this, i32 %rows) #2 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  %rows.addr = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4
  store i32 %rows, i32* %rows.addr, align 4
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %0 = load i32, i32* %rows.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.22* %m_storage, i32 %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret void
}

; Function Attrs: nounwind
declare %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* returned) unnamed_addr #4

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN12btMLCPSolver9solveMLCPERK19btContactSolverInfo(%class.btMLCPSolver* %this, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal) unnamed_addr #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btMLCPSolver*, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %result = alloca i8, align 1
  %Acopy = alloca %struct.btMatrixX, align 4
  %limitDependenciesCopy = alloca %class.btAlignedObjectArray.14, align 4
  store %class.btMLCPSolver* %this, %class.btMLCPSolver** %this.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %this1 = load %class.btMLCPSolver*, %class.btMLCPSolver** %this.addr, align 4
  store i8 1, i8* %result, align 1
  %m_A = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %call = call i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %m_A)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %0 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %1 = bitcast %struct.btContactSolverInfo* %0 to %struct.btContactSolverInfoData*
  %m_splitImpulse = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %1, i32 0, i32 11
  %2 = load i32, i32* %m_splitImpulse, align 4
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.end
  %m_A3 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %call4 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2ERKS0_(%struct.btMatrixX* %Acopy, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %m_A3)
  %m_limitDependencies = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %call5 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiEC2ERKS0_(%class.btAlignedObjectArray.14* %limitDependenciesCopy, %class.btAlignedObjectArray.14* nonnull align 4 dereferenceable(17) %m_limitDependencies)
  %m_solver = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 12
  %3 = load %class.btMLCPSolverInterface*, %class.btMLCPSolverInterface** %m_solver, align 4
  %m_A6 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %m_b = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  %m_x = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %m_lo = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  %m_hi = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  %m_limitDependencies7 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %4 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %5 = bitcast %struct.btContactSolverInfo* %4 to %struct.btContactSolverInfoData*
  %m_numIterations = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %5, i32 0, i32 5
  %6 = load i32, i32* %m_numIterations, align 4
  %7 = bitcast %class.btMLCPSolverInterface* %3 to i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)***
  %vtable = load i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)**, i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)*** %7, align 4
  %vfn = getelementptr inbounds i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)*, i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)** %vtable, i64 2
  %8 = load i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)*, i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)** %vfn, align 4
  %call8 = call zeroext i1 %8(%class.btMLCPSolverInterface* %3, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %m_A6, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_b, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_x, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_lo, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_hi, %class.btAlignedObjectArray.14* nonnull align 4 dereferenceable(17) %m_limitDependencies7, i32 %6, i1 zeroext true)
  %frombool = zext i1 %call8 to i8
  store i8 %frombool, i8* %result, align 1
  %9 = load i8, i8* %result, align 1
  %tobool9 = trunc i8 %9 to i1
  br i1 %tobool9, label %if.then10, label %if.end19

if.then10:                                        ; preds = %if.then2
  %m_solver11 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 12
  %10 = load %class.btMLCPSolverInterface*, %class.btMLCPSolverInterface** %m_solver11, align 4
  %m_bSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 6
  %m_xSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  %m_lo12 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  %m_hi13 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  %11 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %12 = bitcast %struct.btContactSolverInfo* %11 to %struct.btContactSolverInfoData*
  %m_numIterations14 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %12, i32 0, i32 5
  %13 = load i32, i32* %m_numIterations14, align 4
  %14 = bitcast %class.btMLCPSolverInterface* %10 to i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)***
  %vtable15 = load i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)**, i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)*** %14, align 4
  %vfn16 = getelementptr inbounds i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)*, i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)** %vtable15, i64 2
  %15 = load i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)*, i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)** %vfn16, align 4
  %call17 = call zeroext i1 %15(%class.btMLCPSolverInterface* %10, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %Acopy, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_bSplit, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_xSplit, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_lo12, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_hi13, %class.btAlignedObjectArray.14* nonnull align 4 dereferenceable(17) %limitDependenciesCopy, i32 %13, i1 zeroext true)
  %frombool18 = zext i1 %call17 to i8
  store i8 %frombool18, i8* %result, align 1
  br label %if.end19

if.end19:                                         ; preds = %if.then10, %if.then2
  %call20 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.14* %limitDependenciesCopy) #8
  %call21 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %Acopy) #8
  br label %if.end34

if.else:                                          ; preds = %if.end
  %m_solver22 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 12
  %16 = load %class.btMLCPSolverInterface*, %class.btMLCPSolverInterface** %m_solver22, align 4
  %m_A23 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %m_b24 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  %m_x25 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %m_lo26 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  %m_hi27 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  %m_limitDependencies28 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 10
  %17 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %18 = bitcast %struct.btContactSolverInfo* %17 to %struct.btContactSolverInfoData*
  %m_numIterations29 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %18, i32 0, i32 5
  %19 = load i32, i32* %m_numIterations29, align 4
  %20 = bitcast %class.btMLCPSolverInterface* %16 to i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)***
  %vtable30 = load i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)**, i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)*** %20, align 4
  %vfn31 = getelementptr inbounds i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)*, i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)** %vtable30, i64 2
  %21 = load i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)*, i1 (%class.btMLCPSolverInterface*, %struct.btMatrixX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %struct.btVectorX*, %class.btAlignedObjectArray.14*, i32, i1)** %vfn31, align 4
  %call32 = call zeroext i1 %21(%class.btMLCPSolverInterface* %16, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %m_A23, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_b24, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_x25, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_lo26, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_hi27, %class.btAlignedObjectArray.14* nonnull align 4 dereferenceable(17) %m_limitDependencies28, i32 %19, i1 zeroext true)
  %frombool33 = zext i1 %call32 to i8
  store i8 %frombool33, i8* %result, align 1
  br label %if.end34

if.end34:                                         ; preds = %if.else, %if.end19
  %22 = load i8, i8* %result, align 1
  %tobool35 = trunc i8 %22 to i1
  store i1 %tobool35, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end34, %if.then
  %23 = load i1, i1* %retval, align 1
  ret i1 %23
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_rows = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_rows, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btMatrixX* @_ZN9btMatrixXIfEC2ERKS0_(%struct.btMatrixX* returned %this, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %0) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %.addr = alloca %struct.btMatrixX*, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  store %struct.btMatrixX* %0, %struct.btMatrixX** %.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_rows = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  %1 = load %struct.btMatrixX*, %struct.btMatrixX** %.addr, align 4
  %m_rows2 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %1, i32 0, i32 0
  %2 = bitcast i32* %m_rows to i8*
  %3 = bitcast i32* %m_rows2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 4 %2, i8* align 4 %3, i64 20, i1 false)
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %4 = load %struct.btMatrixX*, %struct.btMatrixX** %.addr, align 4
  %m_storage3 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %4, i32 0, i32 5
  %call = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfEC2ERKS0_(%class.btAlignedObjectArray.22* %m_storage, %class.btAlignedObjectArray.22* nonnull align 4 dereferenceable(17) %m_storage3)
  %m_rowNonZeroElements1 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %5 = load %struct.btMatrixX*, %struct.btMatrixX** %.addr, align 4
  %m_rowNonZeroElements14 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %5, i32 0, i32 6
  %call5 = call %class.btAlignedObjectArray.26* @_ZN20btAlignedObjectArrayIS_IiEEC2ERKS1_(%class.btAlignedObjectArray.26* %m_rowNonZeroElements1, %class.btAlignedObjectArray.26* nonnull align 4 dereferenceable(17) %m_rowNonZeroElements14)
  ret %struct.btMatrixX* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiEC2ERKS0_(%class.btAlignedObjectArray.14* returned %this, %class.btAlignedObjectArray.14* nonnull align 4 dereferenceable(17) %otherArray) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %otherArray.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %otherSize = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store %class.btAlignedObjectArray.14* %otherArray, %class.btAlignedObjectArray.14** %otherArray.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.15* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.15* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.14* %this1)
  %0 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %otherArray.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.14* %0)
  store i32 %call2, i32* %otherSize, align 4
  %1 = load i32, i32* %otherSize, align 4
  store i32 0, i32* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.14* %this1, i32 %1, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %2 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %otherArray.addr, align 4
  %3 = load i32, i32* %otherSize, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %4 = load i32*, i32** %m_data, align 4
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.14* %2, i32 0, i32 %3, i32* %4)
  ret %class.btAlignedObjectArray.14* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN12btMLCPSolver14createMLCPFastERK19btContactSolverInfo(%class.btMLCPSolver* %this, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMLCPSolver*, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %numContactRows = alloca i32, align 4
  %numConstraintRows = alloca i32, align 4
  %n = alloca i32, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %i = alloca i32, align 4
  %jacDiag = alloca float, align 4
  %rhs = alloca float, align 4
  %rhsPenetration = alloca float, align 4
  %__profile18 = alloca %class.CProfileSample, align 1
  %i20 = alloca i32, align 4
  %m = alloca i32, align 4
  %numBodies = alloca i32, align 4
  %bodyJointNodeArray = alloca %class.btAlignedObjectArray.14, align 4
  %__profile40 = alloca %class.CProfileSample, align 1
  %ref.tmp = alloca i32, align 4
  %jointNodeArray = alloca %class.btAlignedObjectArray.34, align 4
  %__profile44 = alloca %class.CProfileSample, align 1
  %J3 = alloca %struct.btMatrixX*, align 4
  %__profile49 = alloca %class.CProfileSample, align 1
  %JinvM3 = alloca %struct.btMatrixX*, align 4
  %__profile53 = alloca %class.CProfileSample, align 1
  %cur = alloca i32, align 4
  %rowOffset = alloca i32, align 4
  %ofs = alloca %class.btAlignedObjectArray.14*, align 4
  %__profile57 = alloca %class.CProfileSample, align 1
  %ref.tmp59 = alloca i32, align 4
  %__profile63 = alloca %class.CProfileSample, align 1
  %c = alloca i32, align 4
  %numRows = alloca i32, align 4
  %i65 = alloca i32, align 4
  %sbA = alloca i32, align 4
  %sbB = alloca i32, align 4
  %orgBodyA = alloca %class.btRigidBody*, align 4
  %orgBodyB = alloca %class.btRigidBody*, align 4
  %slotA = alloca i32, align 4
  %ref.tmp88 = alloca %struct.btJointNode, align 4
  %prevSlot = alloca i32, align 4
  %row = alloca i32, align 4
  %normalInvMass = alloca %class.btVector3, align 4
  %ref.tmp106 = alloca float, align 4
  %relPosCrossNormalInvInertia = alloca %class.btVector3, align 4
  %r = alloca i32, align 4
  %slotB = alloca i32, align 4
  %ref.tmp144 = alloca %struct.btJointNode, align 4
  %prevSlot146 = alloca i32, align 4
  %row162 = alloca i32, align 4
  %normalInvMassB = alloca %class.btVector3, align 4
  %ref.tmp169 = alloca float, align 4
  %relPosInvInertiaB = alloca %class.btVector3, align 4
  %r175 = alloca i32, align 4
  %JinvM = alloca float*, align 4
  %Jptr = alloca float*, align 4
  %__profile215 = alloca %class.CProfileSample, align 1
  %__profile218 = alloca %class.CProfileSample, align 1
  %c222 = alloca i32, align 4
  %numRows223 = alloca i32, align 4
  %__profile224 = alloca %class.CProfileSample, align 1
  %i226 = alloca i32, align 4
  %row__ = alloca i32, align 4
  %sbA233 = alloca i32, align 4
  %sbB237 = alloca i32, align 4
  %JinvMrow = alloca float*, align 4
  %startJointNodeA = alloca i32, align 4
  %j0 = alloca i32, align 4
  %cr0 = alloca i32, align 4
  %numRowsOther = alloca i32, align 4
  %ofsother = alloca i32, align 4
  %startJointNodeB = alloca i32, align 4
  %j1 = alloca i32, align 4
  %cj1 = alloca i32, align 4
  %numRowsOther298 = alloca i32, align 4
  %ofsother309 = alloca i32, align 4
  %__profile335 = alloca %class.CProfileSample, align 1
  %row__337 = alloca i32, align 4
  %numJointRows = alloca i32, align 4
  %jj = alloca i32, align 4
  %sbB343 = alloca i32, align 4
  %orgBodyB347 = alloca %class.btRigidBody*, align 4
  %infom = alloca i32, align 4
  %JinvMrow361 = alloca float*, align 4
  %Jrow = alloca float*, align 4
  %i380 = alloca i32, align 4
  %__profile394 = alloca %class.CProfileSample, align 1
  %__profile398 = alloca %class.CProfileSample, align 1
  %i402 = alloca i32, align 4
  %c408 = alloca %struct.btSolverConstraint*, align 4
  store %class.btMLCPSolver* %this, %class.btMLCPSolver** %this.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %this1 = load %class.btMLCPSolver*, %class.btMLCPSolver** %this.addr, align 4
  %0 = load i8, i8* @interleaveContactAndFriction, align 1
  %tobool = trunc i8 %0 to i1
  %1 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 3, i32 1
  store i32 %cond, i32* %numContactRows, align 4
  %m_allConstraintPtrArray = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray)
  store i32 %call, i32* %numConstraintRows, align 4
  %2 = load i32, i32* %numConstraintRows, align 4
  store i32 %2, i32* %n, align 4
  %call2 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.3, i32 0, i32 0))
  %m_b = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  %3 = load i32, i32* %numConstraintRows, align 4
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_b, i32 %3)
  %m_bSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 6
  %4 = load i32, i32* %numConstraintRows, align 4
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_bSplit, i32 %4)
  %m_b3 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  call void @_ZN9btVectorXIfE7setZeroEv(%struct.btVectorX* %m_b3)
  %m_bSplit4 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 6
  call void @_ZN9btVectorXIfE7setZeroEv(%struct.btVectorX* %m_bSplit4)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4
  %6 = load i32, i32* %numConstraintRows, align 4
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_allConstraintPtrArray5 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %7 = load i32, i32* %i, align 4
  %call6 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray5, i32 %7)
  %8 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call6, align 4
  %m_jacDiagABInv = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %8, i32 0, i32 9
  %9 = load float, float* %m_jacDiagABInv, align 4
  store float %9, float* %jacDiag, align 4
  %10 = load float, float* %jacDiag, align 4
  %call7 = call zeroext i1 @_Z11btFuzzyZerof(float %10)
  br i1 %call7, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  %m_allConstraintPtrArray8 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %11 = load i32, i32* %i, align 4
  %call9 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray8, i32 %11)
  %12 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call9, align 4
  %m_rhs = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %12, i32 0, i32 10
  %13 = load float, float* %m_rhs, align 4
  store float %13, float* %rhs, align 4
  %m_allConstraintPtrArray10 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %14 = load i32, i32* %i, align 4
  %call11 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray10, i32 %14)
  %15 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call11, align 4
  %m_rhsPenetration = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %15, i32 0, i32 14
  %16 = load float, float* %m_rhsPenetration, align 4
  store float %16, float* %rhsPenetration, align 4
  %17 = load float, float* %rhs, align 4
  %18 = load float, float* %jacDiag, align 4
  %div = fdiv float %17, %18
  %m_b12 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  %19 = load i32, i32* %i, align 4
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_b12, i32 %19)
  store float %div, float* %call13, align 4
  %20 = load float, float* %rhsPenetration, align 4
  %21 = load float, float* %jacDiag, align 4
  %div14 = fdiv float %20, %21
  %m_bSplit15 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 6
  %22 = load i32, i32* %i, align 4
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_bSplit15, i32 %22)
  store float %div14, float* %call16, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %23 = load i32, i32* %i, align 4
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call17 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #8
  %m_lo = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  %24 = load i32, i32* %numConstraintRows, align 4
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_lo, i32 %24)
  %m_hi = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  %25 = load i32, i32* %numConstraintRows, align 4
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_hi, i32 %25)
  %call19 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile18, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.4, i32 0, i32 0))
  store i32 0, i32* %i20, align 4
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc32, %for.end
  %26 = load i32, i32* %i20, align 4
  %27 = load i32, i32* %numConstraintRows, align 4
  %cmp22 = icmp slt i32 %26, %27
  br i1 %cmp22, label %for.body23, label %for.end34

for.body23:                                       ; preds = %for.cond21
  %m_allConstraintPtrArray24 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %28 = load i32, i32* %i20, align 4
  %call25 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray24, i32 %28)
  %29 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call25, align 4
  %m_lowerLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %29, i32 0, i32 12
  %30 = load float, float* %m_lowerLimit, align 4
  %m_lo26 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  %31 = load i32, i32* %i20, align 4
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_lo26, i32 %31)
  store float %30, float* %call27, align 4
  %m_allConstraintPtrArray28 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %32 = load i32, i32* %i20, align 4
  %call29 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray28, i32 %32)
  %33 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call29, align 4
  %m_upperLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %33, i32 0, i32 13
  %34 = load float, float* %m_upperLimit, align 4
  %m_hi30 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  %35 = load i32, i32* %i20, align 4
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_hi30, i32 %35)
  store float %34, float* %call31, align 4
  br label %for.inc32

for.inc32:                                        ; preds = %for.body23
  %36 = load i32, i32* %i20, align 4
  %inc33 = add nsw i32 %36, 1
  store i32 %inc33, i32* %i20, align 4
  br label %for.cond21

for.end34:                                        ; preds = %for.cond21
  %call35 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile18) #8
  %m_allConstraintPtrArray36 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call37 = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray36)
  store i32 %call37, i32* %m, align 4
  %37 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %37, i32 0, i32 1
  %call38 = call i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv(%class.btAlignedObjectArray* %m_tmpSolverBodyPool)
  store i32 %call38, i32* %numBodies, align 4
  %call39 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.14* %bodyJointNodeArray)
  %call41 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile40, i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.5, i32 0, i32 0))
  %38 = load i32, i32* %numBodies, align 4
  store i32 -1, i32* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.14* %bodyJointNodeArray, i32 %38, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %call42 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile40) #8
  %call43 = call %class.btAlignedObjectArray.34* @_ZN20btAlignedObjectArrayI11btJointNodeEC2Ev(%class.btAlignedObjectArray.34* %jointNodeArray)
  %call45 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile44, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.6, i32 0, i32 0))
  %m_allConstraintPtrArray46 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call47 = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray46)
  %mul = mul nsw i32 2, %call47
  call void @_ZN20btAlignedObjectArrayI11btJointNodeE7reserveEi(%class.btAlignedObjectArray.34* %jointNodeArray, i32 %mul)
  %call48 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile44) #8
  %m_scratchJ3 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 14
  store %struct.btMatrixX* %m_scratchJ3, %struct.btMatrixX** %J3, align 4
  %call50 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile49, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.7, i32 0, i32 0))
  %39 = load %struct.btMatrixX*, %struct.btMatrixX** %J3, align 4
  %40 = load i32, i32* %m, align 4
  %mul51 = mul nsw i32 2, %40
  call void @_ZN9btMatrixXIfE6resizeEii(%struct.btMatrixX* %39, i32 %mul51, i32 8)
  %call52 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile49) #8
  %m_scratchJInvM3 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 15
  store %struct.btMatrixX* %m_scratchJInvM3, %struct.btMatrixX** %JinvM3, align 4
  %call54 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile53, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.8, i32 0, i32 0))
  %41 = load %struct.btMatrixX*, %struct.btMatrixX** %JinvM3, align 4
  %42 = load i32, i32* %m, align 4
  %mul55 = mul nsw i32 2, %42
  call void @_ZN9btMatrixXIfE6resizeEii(%struct.btMatrixX* %41, i32 %mul55, i32 8)
  %43 = load %struct.btMatrixX*, %struct.btMatrixX** %JinvM3, align 4
  call void @_ZN9btMatrixXIfE7setZeroEv(%struct.btMatrixX* %43)
  %44 = load %struct.btMatrixX*, %struct.btMatrixX** %J3, align 4
  call void @_ZN9btMatrixXIfE7setZeroEv(%struct.btMatrixX* %44)
  %call56 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile53) #8
  store i32 0, i32* %cur, align 4
  store i32 0, i32* %rowOffset, align 4
  %m_scratchOfs = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 16
  store %class.btAlignedObjectArray.14* %m_scratchOfs, %class.btAlignedObjectArray.14** %ofs, align 4
  %call58 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile57, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.9, i32 0, i32 0))
  %45 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %ofs, align 4
  store i32 0, i32* %ref.tmp59, align 4
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.14* %45, i32 0, i32* nonnull align 4 dereferenceable(4) %ref.tmp59)
  %46 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %ofs, align 4
  %m_allConstraintPtrArray60 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call61 = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray60)
  call void @_ZN20btAlignedObjectArrayIiE18resizeNoInitializeEi(%class.btAlignedObjectArray.14* %46, i32 %call61)
  %call62 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile57) #8
  %call64 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile63, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.10, i32 0, i32 0))
  store i32 0, i32* %c, align 4
  store i32 0, i32* %numRows, align 4
  store i32 0, i32* %i65, align 4
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc208, %for.end34
  %47 = load i32, i32* %i65, align 4
  %m_allConstraintPtrArray67 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call68 = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray67)
  %cmp69 = icmp slt i32 %47, %call68
  br i1 %cmp69, label %for.body70, label %for.end211

for.body70:                                       ; preds = %for.cond66
  %48 = load i32, i32* %rowOffset, align 4
  %49 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %ofs, align 4
  %50 = load i32, i32* %c, align 4
  %call71 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %49, i32 %50)
  store i32 %48, i32* %call71, align 4
  %m_allConstraintPtrArray72 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %51 = load i32, i32* %i65, align 4
  %call73 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray72, i32 %51)
  %52 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call73, align 4
  %m_solverBodyIdA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %52, i32 0, i32 18
  %53 = load i32, i32* %m_solverBodyIdA, align 4
  store i32 %53, i32* %sbA, align 4
  %m_allConstraintPtrArray74 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %54 = load i32, i32* %i65, align 4
  %call75 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray74, i32 %54)
  %55 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call75, align 4
  %m_solverBodyIdB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %55, i32 0, i32 19
  %56 = load i32, i32* %m_solverBodyIdB, align 4
  store i32 %56, i32* %sbB, align 4
  %57 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool76 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %57, i32 0, i32 1
  %58 = load i32, i32* %sbA, align 4
  %call77 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool76, i32 %58)
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call77, i32 0, i32 12
  %59 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  store %class.btRigidBody* %59, %class.btRigidBody** %orgBodyA, align 4
  %60 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool78 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %60, i32 0, i32 1
  %61 = load i32, i32* %sbB, align 4
  %call79 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool78, i32 %61)
  %m_originalBody80 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call79, i32 0, i32 12
  %62 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody80, align 4
  store %class.btRigidBody* %62, %class.btRigidBody** %orgBodyB, align 4
  %63 = load i32, i32* %i65, align 4
  %64 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %64, i32 0, i32 3
  %call81 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool)
  %cmp82 = icmp slt i32 %63, %call81
  br i1 %cmp82, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body70
  %65 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpConstraintSizesPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %65, i32 0, i32 9
  %66 = load i32, i32* %c, align 4
  %call83 = call nonnull align 4 dereferenceable(8) %"struct.btTypedConstraint::btConstraintInfo1"* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEixEi(%class.btAlignedObjectArray.18* %m_tmpConstraintSizesPool, i32 %66)
  %m_numConstraintRows = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %call83, i32 0, i32 0
  %67 = load i32, i32* %m_numConstraintRows, align 4
  br label %cond.end

cond.false:                                       ; preds = %for.body70
  %68 = load i32, i32* %numContactRows, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond84 = phi i32 [ %67, %cond.true ], [ %68, %cond.false ]
  store i32 %cond84, i32* %numRows, align 4
  %69 = load %class.btRigidBody*, %class.btRigidBody** %orgBodyA, align 4
  %tobool85 = icmp ne %class.btRigidBody* %69, null
  br i1 %tobool85, label %if.then86, label %if.else

if.then86:                                        ; preds = %cond.end
  store i32 -1, i32* %slotA, align 4
  %call87 = call i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE4sizeEv(%class.btAlignedObjectArray.34* %jointNodeArray)
  store i32 %call87, i32* %slotA, align 4
  %70 = bitcast %struct.btJointNode* %ref.tmp88 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %70, i8 0, i32 16, i1 false)
  %call89 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeE6expandERKS0_(%class.btAlignedObjectArray.34* %jointNodeArray, %struct.btJointNode* nonnull align 4 dereferenceable(16) %ref.tmp88)
  %71 = load i32, i32* %sbA, align 4
  %call90 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %bodyJointNodeArray, i32 %71)
  %72 = load i32, i32* %call90, align 4
  store i32 %72, i32* %prevSlot, align 4
  %73 = load i32, i32* %slotA, align 4
  %74 = load i32, i32* %sbA, align 4
  %call91 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %bodyJointNodeArray, i32 %74)
  store i32 %73, i32* %call91, align 4
  %75 = load i32, i32* %prevSlot, align 4
  %76 = load i32, i32* %slotA, align 4
  %call92 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.34* %jointNodeArray, i32 %76)
  %nextJointNodeIndex = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call92, i32 0, i32 2
  store i32 %75, i32* %nextJointNodeIndex, align 4
  %77 = load i32, i32* %c, align 4
  %78 = load i32, i32* %slotA, align 4
  %call93 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.34* %jointNodeArray, i32 %78)
  %jointIndex = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call93, i32 0, i32 0
  store i32 %77, i32* %jointIndex, align 4
  %79 = load i32, i32* %i65, align 4
  %80 = load i32, i32* %slotA, align 4
  %call94 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.34* %jointNodeArray, i32 %80)
  %constraintRowIndex = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call94, i32 0, i32 3
  store i32 %79, i32* %constraintRowIndex, align 4
  %81 = load %class.btRigidBody*, %class.btRigidBody** %orgBodyB, align 4
  %tobool95 = icmp ne %class.btRigidBody* %81, null
  br i1 %tobool95, label %cond.true96, label %cond.false97

cond.true96:                                      ; preds = %if.then86
  %82 = load i32, i32* %sbB, align 4
  br label %cond.end98

cond.false97:                                     ; preds = %if.then86
  br label %cond.end98

cond.end98:                                       ; preds = %cond.false97, %cond.true96
  %cond99 = phi i32 [ %82, %cond.true96 ], [ -1, %cond.false97 ]
  %83 = load i32, i32* %slotA, align 4
  %call100 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.34* %jointNodeArray, i32 %83)
  %otherBodyIndex = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call100, i32 0, i32 1
  store i32 %cond99, i32* %otherBodyIndex, align 4
  store i32 0, i32* %row, align 4
  br label %for.cond101

for.cond101:                                      ; preds = %for.inc135, %cond.end98
  %84 = load i32, i32* %row, align 4
  %85 = load i32, i32* %numRows, align 4
  %cmp102 = icmp slt i32 %84, %85
  br i1 %cmp102, label %for.body103, label %for.end138

for.body103:                                      ; preds = %for.cond101
  %m_allConstraintPtrArray104 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %86 = load i32, i32* %i65, align 4
  %87 = load i32, i32* %row, align 4
  %add = add nsw i32 %86, %87
  %call105 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray104, i32 %add)
  %88 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call105, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %88, i32 0, i32 1
  %89 = load %class.btRigidBody*, %class.btRigidBody** %orgBodyA, align 4
  %call107 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %89)
  store float %call107, float* %ref.tmp106, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %normalInvMass, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1, float* nonnull align 4 dereferenceable(4) %ref.tmp106)
  %m_allConstraintPtrArray108 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %90 = load i32, i32* %i65, align 4
  %91 = load i32, i32* %row, align 4
  %add109 = add nsw i32 %90, %91
  %call110 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray108, i32 %add109)
  %92 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call110, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %92, i32 0, i32 0
  %93 = load %class.btRigidBody*, %class.btRigidBody** %orgBodyA, align 4
  %call111 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %93)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %relPosCrossNormalInvInertia, %class.btVector3* nonnull align 4 dereferenceable(16) %m_relpos1CrossNormal, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call111)
  store i32 0, i32* %r, align 4
  br label %for.cond112

for.cond112:                                      ; preds = %for.inc132, %for.body103
  %94 = load i32, i32* %r, align 4
  %cmp113 = icmp slt i32 %94, 3
  br i1 %cmp113, label %for.body114, label %for.end134

for.body114:                                      ; preds = %for.cond112
  %95 = load %struct.btMatrixX*, %struct.btMatrixX** %J3, align 4
  %96 = load i32, i32* %cur, align 4
  %97 = load i32, i32* %r, align 4
  %m_allConstraintPtrArray115 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %98 = load i32, i32* %i65, align 4
  %99 = load i32, i32* %row, align 4
  %add116 = add nsw i32 %98, %99
  %call117 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray115, i32 %add116)
  %100 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call117, align 4
  %m_contactNormal1118 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %100, i32 0, i32 1
  %call119 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_contactNormal1118)
  %101 = load i32, i32* %r, align 4
  %arrayidx = getelementptr inbounds float, float* %call119, i32 %101
  %102 = load float, float* %arrayidx, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %95, i32 %96, i32 %97, float %102)
  %103 = load %struct.btMatrixX*, %struct.btMatrixX** %J3, align 4
  %104 = load i32, i32* %cur, align 4
  %105 = load i32, i32* %r, align 4
  %add120 = add nsw i32 %105, 4
  %m_allConstraintPtrArray121 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %106 = load i32, i32* %i65, align 4
  %107 = load i32, i32* %row, align 4
  %add122 = add nsw i32 %106, %107
  %call123 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray121, i32 %add122)
  %108 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call123, align 4
  %m_relpos1CrossNormal124 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %108, i32 0, i32 0
  %call125 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_relpos1CrossNormal124)
  %109 = load i32, i32* %r, align 4
  %arrayidx126 = getelementptr inbounds float, float* %call125, i32 %109
  %110 = load float, float* %arrayidx126, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %103, i32 %104, i32 %add120, float %110)
  %111 = load %struct.btMatrixX*, %struct.btMatrixX** %JinvM3, align 4
  %112 = load i32, i32* %cur, align 4
  %113 = load i32, i32* %r, align 4
  %call127 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalInvMass)
  %114 = load i32, i32* %r, align 4
  %arrayidx128 = getelementptr inbounds float, float* %call127, i32 %114
  %115 = load float, float* %arrayidx128, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %111, i32 %112, i32 %113, float %115)
  %116 = load %struct.btMatrixX*, %struct.btMatrixX** %JinvM3, align 4
  %117 = load i32, i32* %cur, align 4
  %118 = load i32, i32* %r, align 4
  %add129 = add nsw i32 %118, 4
  %call130 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %relPosCrossNormalInvInertia)
  %119 = load i32, i32* %r, align 4
  %arrayidx131 = getelementptr inbounds float, float* %call130, i32 %119
  %120 = load float, float* %arrayidx131, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %116, i32 %117, i32 %add129, float %120)
  br label %for.inc132

for.inc132:                                       ; preds = %for.body114
  %121 = load i32, i32* %r, align 4
  %inc133 = add nsw i32 %121, 1
  store i32 %inc133, i32* %r, align 4
  br label %for.cond112

for.end134:                                       ; preds = %for.cond112
  %122 = load %struct.btMatrixX*, %struct.btMatrixX** %J3, align 4
  %123 = load i32, i32* %cur, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %122, i32 %123, i32 3, float 0.000000e+00)
  %124 = load %struct.btMatrixX*, %struct.btMatrixX** %JinvM3, align 4
  %125 = load i32, i32* %cur, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %124, i32 %125, i32 3, float 0.000000e+00)
  %126 = load %struct.btMatrixX*, %struct.btMatrixX** %J3, align 4
  %127 = load i32, i32* %cur, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %126, i32 %127, i32 7, float 0.000000e+00)
  %128 = load %struct.btMatrixX*, %struct.btMatrixX** %JinvM3, align 4
  %129 = load i32, i32* %cur, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %128, i32 %129, i32 7, float 0.000000e+00)
  br label %for.inc135

for.inc135:                                       ; preds = %for.end134
  %130 = load i32, i32* %row, align 4
  %inc136 = add nsw i32 %130, 1
  store i32 %inc136, i32* %row, align 4
  %131 = load i32, i32* %cur, align 4
  %inc137 = add nsw i32 %131, 1
  store i32 %inc137, i32* %cur, align 4
  br label %for.cond101

for.end138:                                       ; preds = %for.cond101
  br label %if.end140

if.else:                                          ; preds = %cond.end
  %132 = load i32, i32* %numRows, align 4
  %133 = load i32, i32* %cur, align 4
  %add139 = add nsw i32 %133, %132
  store i32 %add139, i32* %cur, align 4
  br label %if.end140

if.end140:                                        ; preds = %if.else, %for.end138
  %134 = load %class.btRigidBody*, %class.btRigidBody** %orgBodyB, align 4
  %tobool141 = icmp ne %class.btRigidBody* %134, null
  br i1 %tobool141, label %if.then142, label %if.else204

if.then142:                                       ; preds = %if.end140
  store i32 -1, i32* %slotB, align 4
  %call143 = call i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE4sizeEv(%class.btAlignedObjectArray.34* %jointNodeArray)
  store i32 %call143, i32* %slotB, align 4
  %135 = bitcast %struct.btJointNode* %ref.tmp144 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %135, i8 0, i32 16, i1 false)
  %call145 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeE6expandERKS0_(%class.btAlignedObjectArray.34* %jointNodeArray, %struct.btJointNode* nonnull align 4 dereferenceable(16) %ref.tmp144)
  %136 = load i32, i32* %sbB, align 4
  %call147 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %bodyJointNodeArray, i32 %136)
  %137 = load i32, i32* %call147, align 4
  store i32 %137, i32* %prevSlot146, align 4
  %138 = load i32, i32* %slotB, align 4
  %139 = load i32, i32* %sbB, align 4
  %call148 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %bodyJointNodeArray, i32 %139)
  store i32 %138, i32* %call148, align 4
  %140 = load i32, i32* %prevSlot146, align 4
  %141 = load i32, i32* %slotB, align 4
  %call149 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.34* %jointNodeArray, i32 %141)
  %nextJointNodeIndex150 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call149, i32 0, i32 2
  store i32 %140, i32* %nextJointNodeIndex150, align 4
  %142 = load i32, i32* %c, align 4
  %143 = load i32, i32* %slotB, align 4
  %call151 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.34* %jointNodeArray, i32 %143)
  %jointIndex152 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call151, i32 0, i32 0
  store i32 %142, i32* %jointIndex152, align 4
  %144 = load %class.btRigidBody*, %class.btRigidBody** %orgBodyA, align 4
  %tobool153 = icmp ne %class.btRigidBody* %144, null
  br i1 %tobool153, label %cond.true154, label %cond.false155

cond.true154:                                     ; preds = %if.then142
  %145 = load i32, i32* %sbA, align 4
  br label %cond.end156

cond.false155:                                    ; preds = %if.then142
  br label %cond.end156

cond.end156:                                      ; preds = %cond.false155, %cond.true154
  %cond157 = phi i32 [ %145, %cond.true154 ], [ -1, %cond.false155 ]
  %146 = load i32, i32* %slotB, align 4
  %call158 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.34* %jointNodeArray, i32 %146)
  %otherBodyIndex159 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call158, i32 0, i32 1
  store i32 %cond157, i32* %otherBodyIndex159, align 4
  %147 = load i32, i32* %i65, align 4
  %148 = load i32, i32* %slotB, align 4
  %call160 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.34* %jointNodeArray, i32 %148)
  %constraintRowIndex161 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call160, i32 0, i32 3
  store i32 %147, i32* %constraintRowIndex161, align 4
  store i32 0, i32* %row162, align 4
  br label %for.cond163

for.cond163:                                      ; preds = %for.inc200, %cond.end156
  %149 = load i32, i32* %row162, align 4
  %150 = load i32, i32* %numRows, align 4
  %cmp164 = icmp slt i32 %149, %150
  br i1 %cmp164, label %for.body165, label %for.end203

for.body165:                                      ; preds = %for.cond163
  %m_allConstraintPtrArray166 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %151 = load i32, i32* %i65, align 4
  %152 = load i32, i32* %row162, align 4
  %add167 = add nsw i32 %151, %152
  %call168 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray166, i32 %add167)
  %153 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call168, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %153, i32 0, i32 3
  %154 = load %class.btRigidBody*, %class.btRigidBody** %orgBodyB, align 4
  %call170 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %154)
  store float %call170, float* %ref.tmp169, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %normalInvMassB, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2, float* nonnull align 4 dereferenceable(4) %ref.tmp169)
  %m_allConstraintPtrArray171 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %155 = load i32, i32* %i65, align 4
  %156 = load i32, i32* %row162, align 4
  %add172 = add nsw i32 %155, %156
  %call173 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray171, i32 %add172)
  %157 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call173, align 4
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %157, i32 0, i32 2
  %158 = load %class.btRigidBody*, %class.btRigidBody** %orgBodyB, align 4
  %call174 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %158)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %relPosInvInertiaB, %class.btVector3* nonnull align 4 dereferenceable(16) %m_relpos2CrossNormal, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call174)
  store i32 0, i32* %r175, align 4
  br label %for.cond176

for.cond176:                                      ; preds = %for.inc197, %for.body165
  %159 = load i32, i32* %r175, align 4
  %cmp177 = icmp slt i32 %159, 3
  br i1 %cmp177, label %for.body178, label %for.end199

for.body178:                                      ; preds = %for.cond176
  %160 = load %struct.btMatrixX*, %struct.btMatrixX** %J3, align 4
  %161 = load i32, i32* %cur, align 4
  %162 = load i32, i32* %r175, align 4
  %m_allConstraintPtrArray179 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %163 = load i32, i32* %i65, align 4
  %164 = load i32, i32* %row162, align 4
  %add180 = add nsw i32 %163, %164
  %call181 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray179, i32 %add180)
  %165 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call181, align 4
  %m_contactNormal2182 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %165, i32 0, i32 3
  %call183 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_contactNormal2182)
  %166 = load i32, i32* %r175, align 4
  %arrayidx184 = getelementptr inbounds float, float* %call183, i32 %166
  %167 = load float, float* %arrayidx184, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %160, i32 %161, i32 %162, float %167)
  %168 = load %struct.btMatrixX*, %struct.btMatrixX** %J3, align 4
  %169 = load i32, i32* %cur, align 4
  %170 = load i32, i32* %r175, align 4
  %add185 = add nsw i32 %170, 4
  %m_allConstraintPtrArray186 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %171 = load i32, i32* %i65, align 4
  %172 = load i32, i32* %row162, align 4
  %add187 = add nsw i32 %171, %172
  %call188 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray186, i32 %add187)
  %173 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call188, align 4
  %m_relpos2CrossNormal189 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %173, i32 0, i32 2
  %call190 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_relpos2CrossNormal189)
  %174 = load i32, i32* %r175, align 4
  %arrayidx191 = getelementptr inbounds float, float* %call190, i32 %174
  %175 = load float, float* %arrayidx191, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %168, i32 %169, i32 %add185, float %175)
  %176 = load %struct.btMatrixX*, %struct.btMatrixX** %JinvM3, align 4
  %177 = load i32, i32* %cur, align 4
  %178 = load i32, i32* %r175, align 4
  %call192 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalInvMassB)
  %179 = load i32, i32* %r175, align 4
  %arrayidx193 = getelementptr inbounds float, float* %call192, i32 %179
  %180 = load float, float* %arrayidx193, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %176, i32 %177, i32 %178, float %180)
  %181 = load %struct.btMatrixX*, %struct.btMatrixX** %JinvM3, align 4
  %182 = load i32, i32* %cur, align 4
  %183 = load i32, i32* %r175, align 4
  %add194 = add nsw i32 %183, 4
  %call195 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %relPosInvInertiaB)
  %184 = load i32, i32* %r175, align 4
  %arrayidx196 = getelementptr inbounds float, float* %call195, i32 %184
  %185 = load float, float* %arrayidx196, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %181, i32 %182, i32 %add194, float %185)
  br label %for.inc197

for.inc197:                                       ; preds = %for.body178
  %186 = load i32, i32* %r175, align 4
  %inc198 = add nsw i32 %186, 1
  store i32 %inc198, i32* %r175, align 4
  br label %for.cond176

for.end199:                                       ; preds = %for.cond176
  %187 = load %struct.btMatrixX*, %struct.btMatrixX** %J3, align 4
  %188 = load i32, i32* %cur, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %187, i32 %188, i32 3, float 0.000000e+00)
  %189 = load %struct.btMatrixX*, %struct.btMatrixX** %JinvM3, align 4
  %190 = load i32, i32* %cur, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %189, i32 %190, i32 3, float 0.000000e+00)
  %191 = load %struct.btMatrixX*, %struct.btMatrixX** %J3, align 4
  %192 = load i32, i32* %cur, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %191, i32 %192, i32 7, float 0.000000e+00)
  %193 = load %struct.btMatrixX*, %struct.btMatrixX** %JinvM3, align 4
  %194 = load i32, i32* %cur, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %193, i32 %194, i32 7, float 0.000000e+00)
  br label %for.inc200

for.inc200:                                       ; preds = %for.end199
  %195 = load i32, i32* %row162, align 4
  %inc201 = add nsw i32 %195, 1
  store i32 %inc201, i32* %row162, align 4
  %196 = load i32, i32* %cur, align 4
  %inc202 = add nsw i32 %196, 1
  store i32 %inc202, i32* %cur, align 4
  br label %for.cond163

for.end203:                                       ; preds = %for.cond163
  br label %if.end206

if.else204:                                       ; preds = %if.end140
  %197 = load i32, i32* %numRows, align 4
  %198 = load i32, i32* %cur, align 4
  %add205 = add nsw i32 %198, %197
  store i32 %add205, i32* %cur, align 4
  br label %if.end206

if.end206:                                        ; preds = %if.else204, %for.end203
  %199 = load i32, i32* %numRows, align 4
  %200 = load i32, i32* %rowOffset, align 4
  %add207 = add nsw i32 %200, %199
  store i32 %add207, i32* %rowOffset, align 4
  br label %for.inc208

for.inc208:                                       ; preds = %if.end206
  %201 = load i32, i32* %numRows, align 4
  %202 = load i32, i32* %i65, align 4
  %add209 = add nsw i32 %202, %201
  store i32 %add209, i32* %i65, align 4
  %203 = load i32, i32* %c, align 4
  %inc210 = add nsw i32 %203, 1
  store i32 %inc210, i32* %c, align 4
  br label %for.cond66

for.end211:                                       ; preds = %for.cond66
  %call212 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile63) #8
  %204 = load %struct.btMatrixX*, %struct.btMatrixX** %JinvM3, align 4
  %call213 = call float* @_ZNK9btMatrixXIfE16getBufferPointerEv(%struct.btMatrixX* %204)
  store float* %call213, float** %JinvM, align 4
  %205 = load %struct.btMatrixX*, %struct.btMatrixX** %J3, align 4
  %call214 = call float* @_ZNK9btMatrixXIfE16getBufferPointerEv(%struct.btMatrixX* %205)
  store float* %call214, float** %Jptr, align 4
  %call216 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile215, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.11, i32 0, i32 0))
  %m_A = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %206 = load i32, i32* %n, align 4
  %207 = load i32, i32* %n, align 4
  call void @_ZN9btMatrixXIfE6resizeEii(%struct.btMatrixX* %m_A, i32 %206, i32 %207)
  %call217 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile215) #8
  %call219 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile218, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.12, i32 0, i32 0))
  %m_A220 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  call void @_ZN9btMatrixXIfE7setZeroEv(%struct.btMatrixX* %m_A220)
  %call221 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile218) #8
  store i32 0, i32* %c222, align 4
  store i32 0, i32* %numRows223, align 4
  %call225 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile224, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.13, i32 0, i32 0))
  store i32 0, i32* %i226, align 4
  br label %for.cond227

for.cond227:                                      ; preds = %for.inc331, %for.end211
  %208 = load i32, i32* %i226, align 4
  %m_allConstraintPtrArray228 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call229 = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray228)
  %cmp230 = icmp slt i32 %208, %call229
  br i1 %cmp230, label %for.body231, label %for.end334

for.body231:                                      ; preds = %for.cond227
  %209 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %ofs, align 4
  %210 = load i32, i32* %c222, align 4
  %call232 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %209, i32 %210)
  %211 = load i32, i32* %call232, align 4
  store i32 %211, i32* %row__, align 4
  %m_allConstraintPtrArray234 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %212 = load i32, i32* %i226, align 4
  %call235 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray234, i32 %212)
  %213 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call235, align 4
  %m_solverBodyIdA236 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %213, i32 0, i32 18
  %214 = load i32, i32* %m_solverBodyIdA236, align 4
  store i32 %214, i32* %sbA233, align 4
  %m_allConstraintPtrArray238 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %215 = load i32, i32* %i226, align 4
  %call239 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray238, i32 %215)
  %216 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call239, align 4
  %m_solverBodyIdB240 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %216, i32 0, i32 19
  %217 = load i32, i32* %m_solverBodyIdB240, align 4
  store i32 %217, i32* %sbB237, align 4
  %218 = load i32, i32* %i226, align 4
  %219 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool241 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %219, i32 0, i32 3
  %call242 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool241)
  %cmp243 = icmp slt i32 %218, %call242
  br i1 %cmp243, label %cond.true244, label %cond.false248

cond.true244:                                     ; preds = %for.body231
  %220 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpConstraintSizesPool245 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %220, i32 0, i32 9
  %221 = load i32, i32* %c222, align 4
  %call246 = call nonnull align 4 dereferenceable(8) %"struct.btTypedConstraint::btConstraintInfo1"* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEixEi(%class.btAlignedObjectArray.18* %m_tmpConstraintSizesPool245, i32 %221)
  %m_numConstraintRows247 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %call246, i32 0, i32 0
  %222 = load i32, i32* %m_numConstraintRows247, align 4
  br label %cond.end249

cond.false248:                                    ; preds = %for.body231
  %223 = load i32, i32* %numContactRows, align 4
  br label %cond.end249

cond.end249:                                      ; preds = %cond.false248, %cond.true244
  %cond250 = phi i32 [ %222, %cond.true244 ], [ %223, %cond.false248 ]
  store i32 %cond250, i32* %numRows223, align 4
  %224 = load float*, float** %JinvM, align 4
  %225 = load i32, i32* %row__, align 4
  %mul251 = mul i32 16, %225
  %add.ptr = getelementptr inbounds float, float* %224, i32 %mul251
  store float* %add.ptr, float** %JinvMrow, align 4
  %226 = load i32, i32* %sbA233, align 4
  %call252 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %bodyJointNodeArray, i32 %226)
  %227 = load i32, i32* %call252, align 4
  store i32 %227, i32* %startJointNodeA, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end285, %cond.end249
  %228 = load i32, i32* %startJointNodeA, align 4
  %cmp253 = icmp sge i32 %228, 0
  br i1 %cmp253, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %229 = load i32, i32* %startJointNodeA, align 4
  %call254 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.34* %jointNodeArray, i32 %229)
  %jointIndex255 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call254, i32 0, i32 0
  %230 = load i32, i32* %jointIndex255, align 4
  store i32 %230, i32* %j0, align 4
  %231 = load i32, i32* %startJointNodeA, align 4
  %call256 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.34* %jointNodeArray, i32 %231)
  %constraintRowIndex257 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call256, i32 0, i32 3
  %232 = load i32, i32* %constraintRowIndex257, align 4
  store i32 %232, i32* %cr0, align 4
  %233 = load i32, i32* %j0, align 4
  %234 = load i32, i32* %c222, align 4
  %cmp258 = icmp slt i32 %233, %234
  br i1 %cmp258, label %if.then259, label %if.end285

if.then259:                                       ; preds = %while.body
  %235 = load i32, i32* %cr0, align 4
  %236 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool260 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %236, i32 0, i32 3
  %call261 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool260)
  %cmp262 = icmp slt i32 %235, %call261
  br i1 %cmp262, label %cond.true263, label %cond.false267

cond.true263:                                     ; preds = %if.then259
  %237 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpConstraintSizesPool264 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %237, i32 0, i32 9
  %238 = load i32, i32* %j0, align 4
  %call265 = call nonnull align 4 dereferenceable(8) %"struct.btTypedConstraint::btConstraintInfo1"* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEixEi(%class.btAlignedObjectArray.18* %m_tmpConstraintSizesPool264, i32 %238)
  %m_numConstraintRows266 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %call265, i32 0, i32 0
  %239 = load i32, i32* %m_numConstraintRows266, align 4
  br label %cond.end268

cond.false267:                                    ; preds = %if.then259
  %240 = load i32, i32* %numContactRows, align 4
  br label %cond.end268

cond.end268:                                      ; preds = %cond.false267, %cond.true263
  %cond269 = phi i32 [ %239, %cond.true263 ], [ %240, %cond.false267 ]
  store i32 %cond269, i32* %numRowsOther, align 4
  %m_allConstraintPtrArray270 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %241 = load i32, i32* %cr0, align 4
  %call271 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray270, i32 %241)
  %242 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call271, align 4
  %m_solverBodyIdB272 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %242, i32 0, i32 19
  %243 = load i32, i32* %m_solverBodyIdB272, align 4
  %244 = load i32, i32* %sbA233, align 4
  %cmp273 = icmp eq i32 %243, %244
  br i1 %cmp273, label %cond.true274, label %cond.false276

cond.true274:                                     ; preds = %cond.end268
  %245 = load i32, i32* %numRowsOther, align 4
  %mul275 = mul nsw i32 8, %245
  br label %cond.end277

cond.false276:                                    ; preds = %cond.end268
  br label %cond.end277

cond.end277:                                      ; preds = %cond.false276, %cond.true274
  %cond278 = phi i32 [ %mul275, %cond.true274 ], [ 0, %cond.false276 ]
  store i32 %cond278, i32* %ofsother, align 4
  %m_A279 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %246 = load float*, float** %JinvMrow, align 4
  %247 = load float*, float** %Jptr, align 4
  %248 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %ofs, align 4
  %249 = load i32, i32* %j0, align 4
  %call280 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %248, i32 %249)
  %250 = load i32, i32* %call280, align 4
  %mul281 = mul i32 16, %250
  %add.ptr282 = getelementptr inbounds float, float* %247, i32 %mul281
  %251 = load i32, i32* %ofsother, align 4
  %add.ptr283 = getelementptr inbounds float, float* %add.ptr282, i32 %251
  %252 = load i32, i32* %numRows223, align 4
  %253 = load i32, i32* %numRowsOther, align 4
  %254 = load i32, i32* %row__, align 4
  %255 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %ofs, align 4
  %256 = load i32, i32* %j0, align 4
  %call284 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %255, i32 %256)
  %257 = load i32, i32* %call284, align 4
  call void @_ZN9btMatrixXIfE16multiplyAdd2_p8rEPKfS2_iiii(%struct.btMatrixX* %m_A279, float* %246, float* %add.ptr283, i32 %252, i32 %253, i32 %254, i32 %257)
  br label %if.end285

if.end285:                                        ; preds = %cond.end277, %while.body
  %258 = load i32, i32* %startJointNodeA, align 4
  %call286 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.34* %jointNodeArray, i32 %258)
  %nextJointNodeIndex287 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call286, i32 0, i32 2
  %259 = load i32, i32* %nextJointNodeIndex287, align 4
  store i32 %259, i32* %startJointNodeA, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %260 = load i32, i32* %sbB237, align 4
  %call288 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %bodyJointNodeArray, i32 %260)
  %261 = load i32, i32* %call288, align 4
  store i32 %261, i32* %startJointNodeB, align 4
  br label %while.cond289

while.cond289:                                    ; preds = %if.end327, %while.end
  %262 = load i32, i32* %startJointNodeB, align 4
  %cmp290 = icmp sge i32 %262, 0
  br i1 %cmp290, label %while.body291, label %while.end330

while.body291:                                    ; preds = %while.cond289
  %263 = load i32, i32* %startJointNodeB, align 4
  %call292 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.34* %jointNodeArray, i32 %263)
  %jointIndex293 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call292, i32 0, i32 0
  %264 = load i32, i32* %jointIndex293, align 4
  store i32 %264, i32* %j1, align 4
  %265 = load i32, i32* %startJointNodeB, align 4
  %call294 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.34* %jointNodeArray, i32 %265)
  %constraintRowIndex295 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call294, i32 0, i32 3
  %266 = load i32, i32* %constraintRowIndex295, align 4
  store i32 %266, i32* %cj1, align 4
  %267 = load i32, i32* %j1, align 4
  %268 = load i32, i32* %c222, align 4
  %cmp296 = icmp slt i32 %267, %268
  br i1 %cmp296, label %if.then297, label %if.end327

if.then297:                                       ; preds = %while.body291
  %269 = load i32, i32* %cj1, align 4
  %270 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool299 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %270, i32 0, i32 3
  %call300 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool299)
  %cmp301 = icmp slt i32 %269, %call300
  br i1 %cmp301, label %cond.true302, label %cond.false306

cond.true302:                                     ; preds = %if.then297
  %271 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpConstraintSizesPool303 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %271, i32 0, i32 9
  %272 = load i32, i32* %j1, align 4
  %call304 = call nonnull align 4 dereferenceable(8) %"struct.btTypedConstraint::btConstraintInfo1"* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEixEi(%class.btAlignedObjectArray.18* %m_tmpConstraintSizesPool303, i32 %272)
  %m_numConstraintRows305 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %call304, i32 0, i32 0
  %273 = load i32, i32* %m_numConstraintRows305, align 4
  br label %cond.end307

cond.false306:                                    ; preds = %if.then297
  %274 = load i32, i32* %numContactRows, align 4
  br label %cond.end307

cond.end307:                                      ; preds = %cond.false306, %cond.true302
  %cond308 = phi i32 [ %273, %cond.true302 ], [ %274, %cond.false306 ]
  store i32 %cond308, i32* %numRowsOther298, align 4
  %m_allConstraintPtrArray310 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %275 = load i32, i32* %cj1, align 4
  %call311 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray310, i32 %275)
  %276 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call311, align 4
  %m_solverBodyIdB312 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %276, i32 0, i32 19
  %277 = load i32, i32* %m_solverBodyIdB312, align 4
  %278 = load i32, i32* %sbB237, align 4
  %cmp313 = icmp eq i32 %277, %278
  br i1 %cmp313, label %cond.true314, label %cond.false316

cond.true314:                                     ; preds = %cond.end307
  %279 = load i32, i32* %numRowsOther298, align 4
  %mul315 = mul nsw i32 8, %279
  br label %cond.end317

cond.false316:                                    ; preds = %cond.end307
  br label %cond.end317

cond.end317:                                      ; preds = %cond.false316, %cond.true314
  %cond318 = phi i32 [ %mul315, %cond.true314 ], [ 0, %cond.false316 ]
  store i32 %cond318, i32* %ofsother309, align 4
  %m_A319 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %280 = load float*, float** %JinvMrow, align 4
  %281 = load i32, i32* %numRows223, align 4
  %mul320 = mul i32 8, %281
  %add.ptr321 = getelementptr inbounds float, float* %280, i32 %mul320
  %282 = load float*, float** %Jptr, align 4
  %283 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %ofs, align 4
  %284 = load i32, i32* %j1, align 4
  %call322 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %283, i32 %284)
  %285 = load i32, i32* %call322, align 4
  %mul323 = mul i32 16, %285
  %add.ptr324 = getelementptr inbounds float, float* %282, i32 %mul323
  %286 = load i32, i32* %ofsother309, align 4
  %add.ptr325 = getelementptr inbounds float, float* %add.ptr324, i32 %286
  %287 = load i32, i32* %numRows223, align 4
  %288 = load i32, i32* %numRowsOther298, align 4
  %289 = load i32, i32* %row__, align 4
  %290 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %ofs, align 4
  %291 = load i32, i32* %j1, align 4
  %call326 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %290, i32 %291)
  %292 = load i32, i32* %call326, align 4
  call void @_ZN9btMatrixXIfE16multiplyAdd2_p8rEPKfS2_iiii(%struct.btMatrixX* %m_A319, float* %add.ptr321, float* %add.ptr325, i32 %287, i32 %288, i32 %289, i32 %292)
  br label %if.end327

if.end327:                                        ; preds = %cond.end317, %while.body291
  %293 = load i32, i32* %startJointNodeB, align 4
  %call328 = call nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.34* %jointNodeArray, i32 %293)
  %nextJointNodeIndex329 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %call328, i32 0, i32 2
  %294 = load i32, i32* %nextJointNodeIndex329, align 4
  store i32 %294, i32* %startJointNodeB, align 4
  br label %while.cond289

while.end330:                                     ; preds = %while.cond289
  br label %for.inc331

for.inc331:                                       ; preds = %while.end330
  %295 = load i32, i32* %numRows223, align 4
  %296 = load i32, i32* %i226, align 4
  %add332 = add nsw i32 %296, %295
  store i32 %add332, i32* %i226, align 4
  %297 = load i32, i32* %c222, align 4
  %inc333 = add nsw i32 %297, 1
  store i32 %inc333, i32* %c222, align 4
  br label %for.cond227

for.end334:                                       ; preds = %for.cond227
  %call336 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile335, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.14, i32 0, i32 0))
  store i32 0, i32* %row__337, align 4
  %m_allConstraintPtrArray338 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call339 = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray338)
  store i32 %call339, i32* %numJointRows, align 4
  store i32 0, i32* %jj, align 4
  br label %for.cond340

for.cond340:                                      ; preds = %if.end374, %for.end334
  %298 = load i32, i32* %row__337, align 4
  %299 = load i32, i32* %numJointRows, align 4
  %cmp341 = icmp slt i32 %298, %299
  br i1 %cmp341, label %for.body342, label %for.end377

for.body342:                                      ; preds = %for.cond340
  %m_allConstraintPtrArray344 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %300 = load i32, i32* %row__337, align 4
  %call345 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray344, i32 %300)
  %301 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call345, align 4
  %m_solverBodyIdB346 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %301, i32 0, i32 19
  %302 = load i32, i32* %m_solverBodyIdB346, align 4
  store i32 %302, i32* %sbB343, align 4
  %303 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool348 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %303, i32 0, i32 1
  %304 = load i32, i32* %sbB343, align 4
  %call349 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool348, i32 %304)
  %m_originalBody350 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call349, i32 0, i32 12
  %305 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody350, align 4
  store %class.btRigidBody* %305, %class.btRigidBody** %orgBodyB347, align 4
  %306 = load i32, i32* %row__337, align 4
  %307 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool351 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %307, i32 0, i32 3
  %call352 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool351)
  %cmp353 = icmp slt i32 %306, %call352
  br i1 %cmp353, label %cond.true354, label %cond.false358

cond.true354:                                     ; preds = %for.body342
  %308 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpConstraintSizesPool355 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %308, i32 0, i32 9
  %309 = load i32, i32* %jj, align 4
  %call356 = call nonnull align 4 dereferenceable(8) %"struct.btTypedConstraint::btConstraintInfo1"* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEixEi(%class.btAlignedObjectArray.18* %m_tmpConstraintSizesPool355, i32 %309)
  %m_numConstraintRows357 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %call356, i32 0, i32 0
  %310 = load i32, i32* %m_numConstraintRows357, align 4
  br label %cond.end359

cond.false358:                                    ; preds = %for.body342
  %311 = load i32, i32* %numContactRows, align 4
  br label %cond.end359

cond.end359:                                      ; preds = %cond.false358, %cond.true354
  %cond360 = phi i32 [ %310, %cond.true354 ], [ %311, %cond.false358 ]
  store i32 %cond360, i32* %infom, align 4
  %312 = load float*, float** %JinvM, align 4
  %313 = load i32, i32* %row__337, align 4
  %mul362 = mul i32 16, %313
  %add.ptr363 = getelementptr inbounds float, float* %312, i32 %mul362
  store float* %add.ptr363, float** %JinvMrow361, align 4
  %314 = load float*, float** %Jptr, align 4
  %315 = load i32, i32* %row__337, align 4
  %mul364 = mul i32 16, %315
  %add.ptr365 = getelementptr inbounds float, float* %314, i32 %mul364
  store float* %add.ptr365, float** %Jrow, align 4
  %m_A366 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %316 = load float*, float** %JinvMrow361, align 4
  %317 = load float*, float** %Jrow, align 4
  %318 = load i32, i32* %infom, align 4
  %319 = load i32, i32* %infom, align 4
  %320 = load i32, i32* %row__337, align 4
  %321 = load i32, i32* %row__337, align 4
  call void @_ZN9btMatrixXIfE13multiply2_p8rEPKfS2_iiii(%struct.btMatrixX* %m_A366, float* %316, float* %317, i32 %318, i32 %319, i32 %320, i32 %321)
  %322 = load %class.btRigidBody*, %class.btRigidBody** %orgBodyB347, align 4
  %tobool367 = icmp ne %class.btRigidBody* %322, null
  br i1 %tobool367, label %if.then368, label %if.end374

if.then368:                                       ; preds = %cond.end359
  %m_A369 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %323 = load float*, float** %JinvMrow361, align 4
  %324 = load i32, i32* %infom, align 4
  %mul370 = mul i32 8, %324
  %add.ptr371 = getelementptr inbounds float, float* %323, i32 %mul370
  %325 = load float*, float** %Jrow, align 4
  %326 = load i32, i32* %infom, align 4
  %mul372 = mul i32 8, %326
  %add.ptr373 = getelementptr inbounds float, float* %325, i32 %mul372
  %327 = load i32, i32* %infom, align 4
  %328 = load i32, i32* %infom, align 4
  %329 = load i32, i32* %row__337, align 4
  %330 = load i32, i32* %row__337, align 4
  call void @_ZN9btMatrixXIfE16multiplyAdd2_p8rEPKfS2_iiii(%struct.btMatrixX* %m_A369, float* %add.ptr371, float* %add.ptr373, i32 %327, i32 %328, i32 %329, i32 %330)
  br label %if.end374

if.end374:                                        ; preds = %if.then368, %cond.end359
  %331 = load i32, i32* %infom, align 4
  %332 = load i32, i32* %row__337, align 4
  %add375 = add i32 %332, %331
  store i32 %add375, i32* %row__337, align 4
  %333 = load i32, i32* %jj, align 4
  %inc376 = add nsw i32 %333, 1
  store i32 %inc376, i32* %jj, align 4
  br label %for.cond340

for.end377:                                       ; preds = %for.cond340
  %call378 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile335) #8
  %call379 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile224) #8
  store i32 0, i32* %i380, align 4
  br label %for.cond381

for.cond381:                                      ; preds = %for.inc391, %for.end377
  %334 = load i32, i32* %i380, align 4
  %m_A382 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %call383 = call i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %m_A382)
  %cmp384 = icmp slt i32 %334, %call383
  br i1 %cmp384, label %for.body385, label %for.end393

for.body385:                                      ; preds = %for.cond381
  %m_A386 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %335 = load i32, i32* %i380, align 4
  %336 = load i32, i32* %i380, align 4
  %m_A387 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %337 = load i32, i32* %i380, align 4
  %338 = load i32, i32* %i380, align 4
  %call388 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %m_A387, i32 %337, i32 %338)
  %339 = load float, float* %call388, align 4
  %340 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %341 = bitcast %struct.btContactSolverInfo* %340 to %struct.btContactSolverInfoData*
  %m_globalCfm = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %341, i32 0, i32 10
  %342 = load float, float* %m_globalCfm, align 4
  %343 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %344 = bitcast %struct.btContactSolverInfo* %343 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %344, i32 0, i32 3
  %345 = load float, float* %m_timeStep, align 4
  %div389 = fdiv float %342, %345
  %add390 = fadd float %339, %div389
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %m_A386, i32 %335, i32 %336, float %add390)
  br label %for.inc391

for.inc391:                                       ; preds = %for.body385
  %346 = load i32, i32* %i380, align 4
  %inc392 = add nsw i32 %346, 1
  store i32 %inc392, i32* %i380, align 4
  br label %for.cond381

for.end393:                                       ; preds = %for.cond381
  %call395 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile394, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.15, i32 0, i32 0))
  %m_A396 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  call void @_ZN9btMatrixXIfE24copyLowerToUpperTriangleEv(%struct.btMatrixX* %m_A396)
  %call397 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile394) #8
  %call399 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile398, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.16, i32 0, i32 0))
  %m_x = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %347 = load i32, i32* %numConstraintRows, align 4
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_x, i32 %347)
  %m_xSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  %348 = load i32, i32* %numConstraintRows, align 4
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_xSplit, i32 %348)
  %349 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %350 = bitcast %struct.btContactSolverInfo* %349 to %struct.btContactSolverInfoData*
  %m_solverMode = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %350, i32 0, i32 16
  %351 = load i32, i32* %m_solverMode, align 4
  %and = and i32 %351, 4
  %tobool400 = icmp ne i32 %and, 0
  br i1 %tobool400, label %if.then401, label %if.else418

if.then401:                                       ; preds = %for.end393
  store i32 0, i32* %i402, align 4
  br label %for.cond403

for.cond403:                                      ; preds = %for.inc415, %if.then401
  %352 = load i32, i32* %i402, align 4
  %m_allConstraintPtrArray404 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call405 = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray404)
  %cmp406 = icmp slt i32 %352, %call405
  br i1 %cmp406, label %for.body407, label %for.end417

for.body407:                                      ; preds = %for.cond403
  %m_allConstraintPtrArray409 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %353 = load i32, i32* %i402, align 4
  %call410 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray409, i32 %353)
  %354 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call410, align 4
  store %struct.btSolverConstraint* %354, %struct.btSolverConstraint** %c408, align 4
  %355 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c408, align 4
  %m_appliedImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %355, i32 0, i32 7
  %356 = load float, float* %m_appliedImpulse, align 4
  %m_x411 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %357 = load i32, i32* %i402, align 4
  %call412 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_x411, i32 %357)
  store float %356, float* %call412, align 4
  %358 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c408, align 4
  %m_appliedPushImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %358, i32 0, i32 6
  %359 = load float, float* %m_appliedPushImpulse, align 4
  %m_xSplit413 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  %360 = load i32, i32* %i402, align 4
  %call414 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_xSplit413, i32 %360)
  store float %359, float* %call414, align 4
  br label %for.inc415

for.inc415:                                       ; preds = %for.body407
  %361 = load i32, i32* %i402, align 4
  %inc416 = add nsw i32 %361, 1
  store i32 %inc416, i32* %i402, align 4
  br label %for.cond403

for.end417:                                       ; preds = %for.cond403
  br label %if.end421

if.else418:                                       ; preds = %for.end393
  %m_x419 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  call void @_ZN9btVectorXIfE7setZeroEv(%struct.btVectorX* %m_x419)
  %m_xSplit420 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  call void @_ZN9btVectorXIfE7setZeroEv(%struct.btVectorX* %m_xSplit420)
  br label %if.end421

if.end421:                                        ; preds = %if.else418, %for.end417
  %call422 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile398) #8
  %call423 = call %class.btAlignedObjectArray.34* @_ZN20btAlignedObjectArrayI11btJointNodeED2Ev(%class.btAlignedObjectArray.34* %jointNodeArray) #8
  %call424 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.14* %bodyJointNodeArray) #8
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btVectorXIfE7setZeroEv(%struct.btVectorX* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.22* %m_storage)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_storage2 = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_storage2, i32 0)
  %m_storage4 = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %call5 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.22* %m_storage4)
  call void @_Z9btSetZeroIfEvPT_i(float* %call3, i32 %call5)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  %0 = load %struct.btSolverConstraint**, %struct.btSolverConstraint*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverConstraint*, %struct.btSolverConstraint** %0, i32 %1
  ret %struct.btSolverConstraint** %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_Z11btFuzzyZerof(float %x) #2 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %call = call float @_Z6btFabsf(float %0)
  %cmp = fcmp olt float %call, 0x3E80000000000000
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %this, i32 %index) #1 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  %index.addr = alloca i32, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_storage, i32 %0)
  ret float* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.34* @_ZN20btAlignedObjectArrayI11btJointNodeEC2Ev(%class.btAlignedObjectArray.34* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.35* @_ZN18btAlignedAllocatorI11btJointNodeLj16EEC2Ev(%class.btAlignedAllocator.35* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI11btJointNodeE4initEv(%class.btAlignedObjectArray.34* %this1)
  ret %class.btAlignedObjectArray.34* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btJointNodeE7reserveEi(%class.btAlignedObjectArray.34* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btJointNode*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE8capacityEv(%class.btAlignedObjectArray.34* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI11btJointNodeE8allocateEi(%class.btAlignedObjectArray.34* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btJointNode*
  store %struct.btJointNode* %2, %struct.btJointNode** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE4sizeEv(%class.btAlignedObjectArray.34* %this1)
  %3 = load %struct.btJointNode*, %struct.btJointNode** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI11btJointNodeE4copyEiiPS0_(%class.btAlignedObjectArray.34* %this1, i32 0, i32 %call3, %struct.btJointNode* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE4sizeEv(%class.btAlignedObjectArray.34* %this1)
  call void @_ZN20btAlignedObjectArrayI11btJointNodeE7destroyEii(%class.btAlignedObjectArray.34* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI11btJointNodeE10deallocateEv(%class.btAlignedObjectArray.34* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btJointNode*, %struct.btJointNode** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  store %struct.btJointNode* %4, %struct.btJointNode** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btMatrixXIfE7setZeroEv(%struct.btMatrixX* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.22, i32 0, i32 0))
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_storage, i32 0)
  %m_storage3 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.22* %m_storage3)
  call void @_Z9btSetZeroIfEvPT_i(float* %call2, i32 %call4)
  %call5 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #8
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE18resizeNoInitializeEi(%class.btAlignedObjectArray.14* %this, i32 %newsize) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %newsize.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.14* %this1)
  %cmp = icmp sgt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.14* %this1, i32 %1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 2
  store i32 %2, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btSolverBody*, %struct.btSolverBody** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %0, i32 %1
  ret %struct.btSolverBody* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %"struct.btTypedConstraint::btConstraintInfo1"* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEixEi(%class.btAlignedObjectArray.18* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %0 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %0, i32 %1
  ret %"struct.btTypedConstraint::btConstraintInfo1"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE4sizeEv(%class.btAlignedObjectArray.34* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeE6expandERKS0_(%class.btAlignedObjectArray.34* %this, %struct.btJointNode* nonnull align 4 dereferenceable(16) %fillValue) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  %fillValue.addr = alloca %struct.btJointNode*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  store %struct.btJointNode* %fillValue, %struct.btJointNode** %fillValue.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE4sizeEv(%class.btAlignedObjectArray.34* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE8capacityEv(%class.btAlignedObjectArray.34* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE4sizeEv(%class.btAlignedObjectArray.34* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI11btJointNodeE9allocSizeEi(%class.btAlignedObjectArray.34* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI11btJointNodeE7reserveEi(%class.btAlignedObjectArray.34* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 2
  %1 = load i32, i32* %m_size, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  %2 = load %struct.btJointNode*, %struct.btJointNode** %m_data, align 4
  %3 = load i32, i32* %sz, align 4
  %arrayidx = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %2, i32 %3
  %4 = bitcast %struct.btJointNode* %arrayidx to i8*
  %5 = bitcast i8* %4 to %struct.btJointNode*
  %6 = load %struct.btJointNode*, %struct.btJointNode** %fillValue.addr, align 4
  %7 = bitcast %struct.btJointNode* %5 to i8*
  %8 = bitcast %struct.btJointNode* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  %9 = load %struct.btJointNode*, %struct.btJointNode** %m_data5, align 4
  %10 = load i32, i32* %sz, align 4
  %arrayidx6 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %9, i32 %10
  ret %struct.btJointNode* %arrayidx6
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btJointNode* @_ZN20btAlignedObjectArrayI11btJointNodeEixEi(%class.btAlignedObjectArray.34* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  %0 = load %struct.btJointNode*, %struct.btJointNode** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %0, i32 %1
  ret %struct.btJointNode* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4
  ret float %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call2, float* %ref.tmp1, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp3, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  ret %class.btMatrix3x3* %m_invInertiaTensorWorld
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %this, i32 %row, i32 %col, float %val) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %row.addr = alloca i32, align 4
  %col.addr = alloca i32, align 4
  %val.addr = alloca float, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  store i32 %row, i32* %row.addr, align 4
  store i32 %col, i32* %col.addr, align 4
  store float %val, float* %val.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_setElemOperations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_setElemOperations, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_setElemOperations, align 4
  %1 = load float, float* %val.addr, align 4
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %2 = load i32, i32* %row.addr, align 4
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %3 = load i32, i32* %m_cols, align 4
  %mul = mul nsw i32 %2, %3
  %4 = load i32, i32* %col.addr, align 4
  %add = add nsw i32 %mul, %4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_storage, i32 %add)
  store float %1, float* %call, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btMatrixXIfE16getBufferPointerEv(%struct.btMatrixX* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.22* %m_storage)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_storage2 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_storage2, i32 0)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float* [ %call3, %cond.true ], [ null, %cond.false ]
  ret float* %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btMatrixXIfE16multiplyAdd2_p8rEPKfS2_iiii(%struct.btMatrixX* %this, float* %B, float* %C, i32 %numRows, i32 %numRowsOther, i32 %row, i32 %col) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %B.addr = alloca float*, align 4
  %C.addr = alloca float*, align 4
  %numRows.addr = alloca i32, align 4
  %numRowsOther.addr = alloca i32, align 4
  %row.addr = alloca i32, align 4
  %col.addr = alloca i32, align 4
  %bb = alloca float*, align 4
  %i = alloca i32, align 4
  %cc = alloca float*, align 4
  %j = alloca i32, align 4
  %sum = alloca float, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  store float* %B, float** %B.addr, align 4
  store float* %C, float** %C.addr, align 4
  store i32 %numRows, i32* %numRows.addr, align 4
  store i32 %numRowsOther, i32* %numRowsOther.addr, align 4
  store i32 %row, i32* %row.addr, align 4
  store i32 %col, i32* %col.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %0 = load float*, float** %B.addr, align 4
  store float* %0, float** %bb, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc28, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %numRows.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end30

for.body:                                         ; preds = %for.cond
  %3 = load float*, float** %C.addr, align 4
  store float* %3, float** %cc, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %4 = load i32, i32* %j, align 4
  %5 = load i32, i32* %numRowsOther.addr, align 4
  %cmp3 = icmp slt i32 %4, %5
  br i1 %cmp3, label %for.body4, label %for.end

for.body4:                                        ; preds = %for.cond2
  %6 = load float*, float** %bb, align 4
  %arrayidx = getelementptr inbounds float, float* %6, i32 0
  %7 = load float, float* %arrayidx, align 4
  %8 = load float*, float** %cc, align 4
  %arrayidx5 = getelementptr inbounds float, float* %8, i32 0
  %9 = load float, float* %arrayidx5, align 4
  %mul = fmul float %7, %9
  store float %mul, float* %sum, align 4
  %10 = load float*, float** %bb, align 4
  %arrayidx6 = getelementptr inbounds float, float* %10, i32 1
  %11 = load float, float* %arrayidx6, align 4
  %12 = load float*, float** %cc, align 4
  %arrayidx7 = getelementptr inbounds float, float* %12, i32 1
  %13 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %11, %13
  %14 = load float, float* %sum, align 4
  %add = fadd float %14, %mul8
  store float %add, float* %sum, align 4
  %15 = load float*, float** %bb, align 4
  %arrayidx9 = getelementptr inbounds float, float* %15, i32 2
  %16 = load float, float* %arrayidx9, align 4
  %17 = load float*, float** %cc, align 4
  %arrayidx10 = getelementptr inbounds float, float* %17, i32 2
  %18 = load float, float* %arrayidx10, align 4
  %mul11 = fmul float %16, %18
  %19 = load float, float* %sum, align 4
  %add12 = fadd float %19, %mul11
  store float %add12, float* %sum, align 4
  %20 = load float*, float** %bb, align 4
  %arrayidx13 = getelementptr inbounds float, float* %20, i32 4
  %21 = load float, float* %arrayidx13, align 4
  %22 = load float*, float** %cc, align 4
  %arrayidx14 = getelementptr inbounds float, float* %22, i32 4
  %23 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %21, %23
  %24 = load float, float* %sum, align 4
  %add16 = fadd float %24, %mul15
  store float %add16, float* %sum, align 4
  %25 = load float*, float** %bb, align 4
  %arrayidx17 = getelementptr inbounds float, float* %25, i32 5
  %26 = load float, float* %arrayidx17, align 4
  %27 = load float*, float** %cc, align 4
  %arrayidx18 = getelementptr inbounds float, float* %27, i32 5
  %28 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %26, %28
  %29 = load float, float* %sum, align 4
  %add20 = fadd float %29, %mul19
  store float %add20, float* %sum, align 4
  %30 = load float*, float** %bb, align 4
  %arrayidx21 = getelementptr inbounds float, float* %30, i32 6
  %31 = load float, float* %arrayidx21, align 4
  %32 = load float*, float** %cc, align 4
  %arrayidx22 = getelementptr inbounds float, float* %32, i32 6
  %33 = load float, float* %arrayidx22, align 4
  %mul23 = fmul float %31, %33
  %34 = load float, float* %sum, align 4
  %add24 = fadd float %34, %mul23
  store float %add24, float* %sum, align 4
  %35 = load i32, i32* %row.addr, align 4
  %36 = load i32, i32* %i, align 4
  %add25 = add nsw i32 %35, %36
  %37 = load i32, i32* %col.addr, align 4
  %38 = load i32, i32* %j, align 4
  %add26 = add nsw i32 %37, %38
  %39 = load float, float* %sum, align 4
  call void @_ZN9btMatrixXIfE7addElemEiif(%struct.btMatrixX* %this1, i32 %add25, i32 %add26, float %39)
  %40 = load float*, float** %cc, align 4
  %add.ptr = getelementptr inbounds float, float* %40, i32 8
  store float* %add.ptr, float** %cc, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %41 = load i32, i32* %j, align 4
  %inc = add nsw i32 %41, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  %42 = load float*, float** %bb, align 4
  %add.ptr27 = getelementptr inbounds float, float* %42, i32 8
  store float* %add.ptr27, float** %bb, align 4
  br label %for.inc28

for.inc28:                                        ; preds = %for.end
  %43 = load i32, i32* %i, align 4
  %inc29 = add nsw i32 %43, 1
  store i32 %inc29, i32* %i, align 4
  br label %for.cond

for.end30:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btMatrixXIfE13multiply2_p8rEPKfS2_iiii(%struct.btMatrixX* %this, float* %B, float* %C, i32 %numRows, i32 %numRowsOther, i32 %row, i32 %col) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %B.addr = alloca float*, align 4
  %C.addr = alloca float*, align 4
  %numRows.addr = alloca i32, align 4
  %numRowsOther.addr = alloca i32, align 4
  %row.addr = alloca i32, align 4
  %col.addr = alloca i32, align 4
  %bb = alloca float*, align 4
  %i = alloca i32, align 4
  %cc = alloca float*, align 4
  %j = alloca i32, align 4
  %sum = alloca float, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  store float* %B, float** %B.addr, align 4
  store float* %C, float** %C.addr, align 4
  store i32 %numRows, i32* %numRows.addr, align 4
  store i32 %numRowsOther, i32* %numRowsOther.addr, align 4
  store i32 %row, i32* %row.addr, align 4
  store i32 %col, i32* %col.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %0 = load float*, float** %B.addr, align 4
  store float* %0, float** %bb, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc28, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %numRows.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end30

for.body:                                         ; preds = %for.cond
  %3 = load float*, float** %C.addr, align 4
  store float* %3, float** %cc, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %4 = load i32, i32* %j, align 4
  %5 = load i32, i32* %numRowsOther.addr, align 4
  %cmp3 = icmp slt i32 %4, %5
  br i1 %cmp3, label %for.body4, label %for.end

for.body4:                                        ; preds = %for.cond2
  %6 = load float*, float** %bb, align 4
  %arrayidx = getelementptr inbounds float, float* %6, i32 0
  %7 = load float, float* %arrayidx, align 4
  %8 = load float*, float** %cc, align 4
  %arrayidx5 = getelementptr inbounds float, float* %8, i32 0
  %9 = load float, float* %arrayidx5, align 4
  %mul = fmul float %7, %9
  store float %mul, float* %sum, align 4
  %10 = load float*, float** %bb, align 4
  %arrayidx6 = getelementptr inbounds float, float* %10, i32 1
  %11 = load float, float* %arrayidx6, align 4
  %12 = load float*, float** %cc, align 4
  %arrayidx7 = getelementptr inbounds float, float* %12, i32 1
  %13 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %11, %13
  %14 = load float, float* %sum, align 4
  %add = fadd float %14, %mul8
  store float %add, float* %sum, align 4
  %15 = load float*, float** %bb, align 4
  %arrayidx9 = getelementptr inbounds float, float* %15, i32 2
  %16 = load float, float* %arrayidx9, align 4
  %17 = load float*, float** %cc, align 4
  %arrayidx10 = getelementptr inbounds float, float* %17, i32 2
  %18 = load float, float* %arrayidx10, align 4
  %mul11 = fmul float %16, %18
  %19 = load float, float* %sum, align 4
  %add12 = fadd float %19, %mul11
  store float %add12, float* %sum, align 4
  %20 = load float*, float** %bb, align 4
  %arrayidx13 = getelementptr inbounds float, float* %20, i32 4
  %21 = load float, float* %arrayidx13, align 4
  %22 = load float*, float** %cc, align 4
  %arrayidx14 = getelementptr inbounds float, float* %22, i32 4
  %23 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %21, %23
  %24 = load float, float* %sum, align 4
  %add16 = fadd float %24, %mul15
  store float %add16, float* %sum, align 4
  %25 = load float*, float** %bb, align 4
  %arrayidx17 = getelementptr inbounds float, float* %25, i32 5
  %26 = load float, float* %arrayidx17, align 4
  %27 = load float*, float** %cc, align 4
  %arrayidx18 = getelementptr inbounds float, float* %27, i32 5
  %28 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %26, %28
  %29 = load float, float* %sum, align 4
  %add20 = fadd float %29, %mul19
  store float %add20, float* %sum, align 4
  %30 = load float*, float** %bb, align 4
  %arrayidx21 = getelementptr inbounds float, float* %30, i32 6
  %31 = load float, float* %arrayidx21, align 4
  %32 = load float*, float** %cc, align 4
  %arrayidx22 = getelementptr inbounds float, float* %32, i32 6
  %33 = load float, float* %arrayidx22, align 4
  %mul23 = fmul float %31, %33
  %34 = load float, float* %sum, align 4
  %add24 = fadd float %34, %mul23
  store float %add24, float* %sum, align 4
  %35 = load i32, i32* %row.addr, align 4
  %36 = load i32, i32* %i, align 4
  %add25 = add nsw i32 %35, %36
  %37 = load i32, i32* %col.addr, align 4
  %38 = load i32, i32* %j, align 4
  %add26 = add nsw i32 %37, %38
  %39 = load float, float* %sum, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %this1, i32 %add25, i32 %add26, float %39)
  %40 = load float*, float** %cc, align 4
  %add.ptr = getelementptr inbounds float, float* %40, i32 8
  store float* %add.ptr, float** %cc, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %41 = load i32, i32* %j, align 4
  %inc = add nsw i32 %41, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  %42 = load float*, float** %bb, align 4
  %add.ptr27 = getelementptr inbounds float, float* %42, i32 8
  store float* %add.ptr27, float** %bb, align 4
  br label %for.inc28

for.inc28:                                        ; preds = %for.end
  %43 = load i32, i32* %i, align 4
  %inc29 = add nsw i32 %43, 1
  store i32 %inc29, i32* %i, align 4
  br label %for.cond

for.end30:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %this, i32 %row, i32 %col) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %row.addr = alloca i32, align 4
  %col.addr = alloca i32, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  store i32 %row, i32* %row.addr, align 4
  store i32 %col, i32* %col.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %0 = load i32, i32* %col.addr, align 4
  %1 = load i32, i32* %row.addr, align 4
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %2 = load i32, i32* %m_cols, align 4
  %mul = mul nsw i32 %1, %2
  %add = add nsw i32 %0, %mul
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_storage, i32 %add)
  ret float* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btMatrixXIfE24copyLowerToUpperTriangleEv(%struct.btMatrixX* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %count = alloca i32, align 4
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  store i32 0, i32* %count, align 4
  store i32 0, i32* %row, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc7, %entry
  %0 = load i32, i32* %row, align 4
  %call = call i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %this1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end9

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %col, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %1 = load i32, i32* %col, align 4
  %2 = load i32, i32* %row, align 4
  %cmp3 = icmp slt i32 %1, %2
  br i1 %cmp3, label %for.body4, label %for.end

for.body4:                                        ; preds = %for.cond2
  %3 = load i32, i32* %col, align 4
  %4 = load i32, i32* %row, align 4
  %5 = load i32, i32* %row, align 4
  %6 = load i32, i32* %col, align 4
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %this1, i32 %5, i32 %6)
  %7 = load float, float* %call5, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %this1, i32 %3, i32 %4, float %7)
  %8 = load i32, i32* %count, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %count, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %9 = load i32, i32* %col, align 4
  %inc6 = add nsw i32 %9, 1
  store i32 %inc6, i32* %col, align 4
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  br label %for.inc7

for.inc7:                                         ; preds = %for.end
  %10 = load i32, i32* %row, align 4
  %inc8 = add nsw i32 %10, 1
  store i32 %inc8, i32* %row, align 4
  br label %for.cond

for.end9:                                         ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.34* @_ZN20btAlignedObjectArrayI11btJointNodeED2Ev(%class.btAlignedObjectArray.34* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI11btJointNodeE5clearEv(%class.btAlignedObjectArray.34* %this1)
  ret %class.btAlignedObjectArray.34* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN12btMLCPSolver10createMLCPERK19btContactSolverInfo(%class.btMLCPSolver* %this, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMLCPSolver*, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %numBodies = alloca i32, align 4
  %numConstraintRows = alloca i32, align 4
  %i = alloca i32, align 4
  %Minv = alloca %struct.btMatrixX*, align 4
  %i30 = alloca i32, align 4
  %rb = alloca %struct.btSolverBody*, align 4
  %invMass = alloca %class.btVector3*, align 4
  %orgBody = alloca %class.btRigidBody*, align 4
  %r = alloca i32, align 4
  %c = alloca i32, align 4
  %J = alloca %struct.btMatrixX*, align 4
  %i81 = alloca i32, align 4
  %bodyIndex0 = alloca i32, align 4
  %bodyIndex1 = alloca i32, align 4
  %J_transpose = alloca %struct.btMatrixX*, align 4
  %ref.tmp = alloca %struct.btMatrixX, align 4
  %tmp = alloca %struct.btMatrixX*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %ref.tmp195 = alloca %struct.btMatrixX, align 4
  %__profile199 = alloca %class.CProfileSample, align 1
  %ref.tmp201 = alloca %struct.btMatrixX, align 4
  %i205 = alloca i32, align 4
  %i223 = alloca i32, align 4
  %c229 = alloca %struct.btSolverConstraint*, align 4
  store %class.btMLCPSolver* %this, %class.btMLCPSolver** %this.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %this1 = load %class.btMLCPSolver*, %class.btMLCPSolver** %this.addr, align 4
  %0 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %0, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv(%class.btAlignedObjectArray* %m_tmpSolverBodyPool)
  store i32 %call, i32* %numBodies, align 4
  %m_allConstraintPtrArray = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray)
  store i32 %call2, i32* %numConstraintRows, align 4
  %m_b = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  %1 = load i32, i32* %numConstraintRows, align 4
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_b, i32 %1)
  %2 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %3 = bitcast %struct.btContactSolverInfo* %2 to %struct.btContactSolverInfoData*
  %m_splitImpulse = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %3, i32 0, i32 11
  %4 = load i32, i32* %m_splitImpulse, align 4
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_bSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 6
  %5 = load i32, i32* %numConstraintRows, align 4
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_bSplit, i32 %5)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_bSplit3 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 6
  call void @_ZN9btVectorXIfE7setZeroEv(%struct.btVectorX* %m_bSplit3)
  %m_b4 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  call void @_ZN9btVectorXIfE7setZeroEv(%struct.btVectorX* %m_b4)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %6 = load i32, i32* %i, align 4
  %7 = load i32, i32* %numConstraintRows, align 4
  %cmp = icmp slt i32 %6, %7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_allConstraintPtrArray5 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %8 = load i32, i32* %i, align 4
  %call6 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray5, i32 %8)
  %9 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call6, align 4
  %m_jacDiagABInv = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %9, i32 0, i32 9
  %10 = load float, float* %m_jacDiagABInv, align 4
  %tobool7 = fcmp une float %10, 0.000000e+00
  br i1 %tobool7, label %if.then8, label %if.end28

if.then8:                                         ; preds = %for.body
  %m_allConstraintPtrArray9 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %11 = load i32, i32* %i, align 4
  %call10 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray9, i32 %11)
  %12 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call10, align 4
  %m_rhs = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %12, i32 0, i32 10
  %13 = load float, float* %m_rhs, align 4
  %m_allConstraintPtrArray11 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %14 = load i32, i32* %i, align 4
  %call12 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray11, i32 %14)
  %15 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call12, align 4
  %m_jacDiagABInv13 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %15, i32 0, i32 9
  %16 = load float, float* %m_jacDiagABInv13, align 4
  %div = fdiv float %13, %16
  %m_b14 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 2
  %17 = load i32, i32* %i, align 4
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_b14, i32 %17)
  store float %div, float* %call15, align 4
  %18 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %19 = bitcast %struct.btContactSolverInfo* %18 to %struct.btContactSolverInfoData*
  %m_splitImpulse16 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %19, i32 0, i32 11
  %20 = load i32, i32* %m_splitImpulse16, align 4
  %tobool17 = icmp ne i32 %20, 0
  br i1 %tobool17, label %if.then18, label %if.end27

if.then18:                                        ; preds = %if.then8
  %m_allConstraintPtrArray19 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %21 = load i32, i32* %i, align 4
  %call20 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray19, i32 %21)
  %22 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call20, align 4
  %m_rhsPenetration = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %22, i32 0, i32 14
  %23 = load float, float* %m_rhsPenetration, align 4
  %m_allConstraintPtrArray21 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %24 = load i32, i32* %i, align 4
  %call22 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray21, i32 %24)
  %25 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call22, align 4
  %m_jacDiagABInv23 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %25, i32 0, i32 9
  %26 = load float, float* %m_jacDiagABInv23, align 4
  %div24 = fdiv float %23, %26
  %m_bSplit25 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 6
  %27 = load i32, i32* %i, align 4
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_bSplit25, i32 %27)
  store float %div24, float* %call26, align 4
  br label %if.end27

if.end27:                                         ; preds = %if.then18, %if.then8
  br label %if.end28

if.end28:                                         ; preds = %if.end27, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end28
  %28 = load i32, i32* %i, align 4
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_scratchMInv = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 17
  store %struct.btMatrixX* %m_scratchMInv, %struct.btMatrixX** %Minv, align 4
  %29 = load %struct.btMatrixX*, %struct.btMatrixX** %Minv, align 4
  %30 = load i32, i32* %numBodies, align 4
  %mul = mul nsw i32 6, %30
  %31 = load i32, i32* %numBodies, align 4
  %mul29 = mul nsw i32 6, %31
  call void @_ZN9btMatrixXIfE6resizeEii(%struct.btMatrixX* %29, i32 %mul, i32 %mul29)
  %32 = load %struct.btMatrixX*, %struct.btMatrixX** %Minv, align 4
  call void @_ZN9btMatrixXIfE7setZeroEv(%struct.btMatrixX* %32)
  store i32 0, i32* %i30, align 4
  br label %for.cond31

for.cond31:                                       ; preds = %for.inc77, %for.end
  %33 = load i32, i32* %i30, align 4
  %34 = load i32, i32* %numBodies, align 4
  %cmp32 = icmp slt i32 %33, %34
  br i1 %cmp32, label %for.body33, label %for.end79

for.body33:                                       ; preds = %for.cond31
  %35 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool34 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %35, i32 0, i32 1
  %36 = load i32, i32* %i30, align 4
  %call35 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool34, i32 %36)
  store %struct.btSolverBody* %call35, %struct.btSolverBody** %rb, align 4
  %37 = load %struct.btSolverBody*, %struct.btSolverBody** %rb, align 4
  %m_invMass = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %37, i32 0, i32 5
  store %class.btVector3* %m_invMass, %class.btVector3** %invMass, align 4
  %38 = load %struct.btMatrixX*, %struct.btMatrixX** %Minv, align 4
  %39 = load i32, i32* %i30, align 4
  %mul36 = mul nsw i32 %39, 6
  %add = add nsw i32 %mul36, 0
  %40 = load i32, i32* %i30, align 4
  %mul37 = mul nsw i32 %40, 6
  %add38 = add nsw i32 %mul37, 0
  %41 = load %class.btVector3*, %class.btVector3** %invMass, align 4
  %call39 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %41)
  %arrayidx = getelementptr inbounds float, float* %call39, i32 0
  %42 = load float, float* %arrayidx, align 4
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(60) %38, i32 %add, i32 %add38, float %42)
  %43 = load %struct.btMatrixX*, %struct.btMatrixX** %Minv, align 4
  %44 = load i32, i32* %i30, align 4
  %mul40 = mul nsw i32 %44, 6
  %add41 = add nsw i32 %mul40, 1
  %45 = load i32, i32* %i30, align 4
  %mul42 = mul nsw i32 %45, 6
  %add43 = add nsw i32 %mul42, 1
  %46 = load %class.btVector3*, %class.btVector3** %invMass, align 4
  %call44 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %46)
  %arrayidx45 = getelementptr inbounds float, float* %call44, i32 1
  %47 = load float, float* %arrayidx45, align 4
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(60) %43, i32 %add41, i32 %add43, float %47)
  %48 = load %struct.btMatrixX*, %struct.btMatrixX** %Minv, align 4
  %49 = load i32, i32* %i30, align 4
  %mul46 = mul nsw i32 %49, 6
  %add47 = add nsw i32 %mul46, 2
  %50 = load i32, i32* %i30, align 4
  %mul48 = mul nsw i32 %50, 6
  %add49 = add nsw i32 %mul48, 2
  %51 = load %class.btVector3*, %class.btVector3** %invMass, align 4
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %51)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 2
  %52 = load float, float* %arrayidx51, align 4
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(60) %48, i32 %add47, i32 %add49, float %52)
  %53 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool52 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %53, i32 0, i32 1
  %54 = load i32, i32* %i30, align 4
  %call53 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool52, i32 %54)
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call53, i32 0, i32 12
  %55 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  store %class.btRigidBody* %55, %class.btRigidBody** %orgBody, align 4
  store i32 0, i32* %r, align 4
  br label %for.cond54

for.cond54:                                       ; preds = %for.inc74, %for.body33
  %56 = load i32, i32* %r, align 4
  %cmp55 = icmp slt i32 %56, 3
  br i1 %cmp55, label %for.body56, label %for.end76

for.body56:                                       ; preds = %for.cond54
  store i32 0, i32* %c, align 4
  br label %for.cond57

for.cond57:                                       ; preds = %for.inc71, %for.body56
  %57 = load i32, i32* %c, align 4
  %cmp58 = icmp slt i32 %57, 3
  br i1 %cmp58, label %for.body59, label %for.end73

for.body59:                                       ; preds = %for.cond57
  %58 = load %struct.btMatrixX*, %struct.btMatrixX** %Minv, align 4
  %59 = load i32, i32* %i30, align 4
  %mul60 = mul nsw i32 %59, 6
  %add61 = add nsw i32 %mul60, 3
  %60 = load i32, i32* %r, align 4
  %add62 = add nsw i32 %add61, %60
  %61 = load i32, i32* %i30, align 4
  %mul63 = mul nsw i32 %61, 6
  %add64 = add nsw i32 %mul63, 3
  %62 = load i32, i32* %c, align 4
  %add65 = add nsw i32 %add64, %62
  %63 = load %class.btRigidBody*, %class.btRigidBody** %orgBody, align 4
  %tobool66 = icmp ne %class.btRigidBody* %63, null
  br i1 %tobool66, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body59
  %64 = load %class.btRigidBody*, %class.btRigidBody** %orgBody, align 4
  %call67 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %64)
  %65 = load i32, i32* %r, align 4
  %call68 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call67, i32 %65)
  %call69 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call68)
  %66 = load i32, i32* %c, align 4
  %arrayidx70 = getelementptr inbounds float, float* %call69, i32 %66
  %67 = load float, float* %arrayidx70, align 4
  br label %cond.end

cond.false:                                       ; preds = %for.body59
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %67, %cond.true ], [ 0.000000e+00, %cond.false ]
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(60) %58, i32 %add62, i32 %add65, float %cond)
  br label %for.inc71

for.inc71:                                        ; preds = %cond.end
  %68 = load i32, i32* %c, align 4
  %inc72 = add nsw i32 %68, 1
  store i32 %inc72, i32* %c, align 4
  br label %for.cond57

for.end73:                                        ; preds = %for.cond57
  br label %for.inc74

for.inc74:                                        ; preds = %for.end73
  %69 = load i32, i32* %r, align 4
  %inc75 = add nsw i32 %69, 1
  store i32 %inc75, i32* %r, align 4
  br label %for.cond54

for.end76:                                        ; preds = %for.cond54
  br label %for.inc77

for.inc77:                                        ; preds = %for.end76
  %70 = load i32, i32* %i30, align 4
  %inc78 = add nsw i32 %70, 1
  store i32 %inc78, i32* %i30, align 4
  br label %for.cond31

for.end79:                                        ; preds = %for.cond31
  %m_scratchJ = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 18
  store %struct.btMatrixX* %m_scratchJ, %struct.btMatrixX** %J, align 4
  %71 = load %struct.btMatrixX*, %struct.btMatrixX** %J, align 4
  %72 = load i32, i32* %numConstraintRows, align 4
  %73 = load i32, i32* %numBodies, align 4
  %mul80 = mul nsw i32 6, %73
  call void @_ZN9btMatrixXIfE6resizeEii(%struct.btMatrixX* %71, i32 %72, i32 %mul80)
  %74 = load %struct.btMatrixX*, %struct.btMatrixX** %J, align 4
  call void @_ZN9btMatrixXIfE7setZeroEv(%struct.btMatrixX* %74)
  %m_lo = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  %75 = load i32, i32* %numConstraintRows, align 4
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_lo, i32 %75)
  %m_hi = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  %76 = load i32, i32* %numConstraintRows, align 4
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_hi, i32 %76)
  store i32 0, i32* %i81, align 4
  br label %for.cond82

for.cond82:                                       ; preds = %for.inc189, %for.end79
  %77 = load i32, i32* %i81, align 4
  %78 = load i32, i32* %numConstraintRows, align 4
  %cmp83 = icmp slt i32 %77, %78
  br i1 %cmp83, label %for.body84, label %for.end191

for.body84:                                       ; preds = %for.cond82
  %m_allConstraintPtrArray85 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %79 = load i32, i32* %i81, align 4
  %call86 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray85, i32 %79)
  %80 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call86, align 4
  %m_lowerLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %80, i32 0, i32 12
  %81 = load float, float* %m_lowerLimit, align 4
  %m_lo87 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 4
  %82 = load i32, i32* %i81, align 4
  %call88 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_lo87, i32 %82)
  store float %81, float* %call88, align 4
  %m_allConstraintPtrArray89 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %83 = load i32, i32* %i81, align 4
  %call90 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray89, i32 %83)
  %84 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call90, align 4
  %m_upperLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %84, i32 0, i32 13
  %85 = load float, float* %m_upperLimit, align 4
  %m_hi91 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 5
  %86 = load i32, i32* %i81, align 4
  %call92 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_hi91, i32 %86)
  store float %85, float* %call92, align 4
  %m_allConstraintPtrArray93 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %87 = load i32, i32* %i81, align 4
  %call94 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray93, i32 %87)
  %88 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call94, align 4
  %m_solverBodyIdA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %88, i32 0, i32 18
  %89 = load i32, i32* %m_solverBodyIdA, align 4
  store i32 %89, i32* %bodyIndex0, align 4
  %m_allConstraintPtrArray95 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %90 = load i32, i32* %i81, align 4
  %call96 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray95, i32 %90)
  %91 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call96, align 4
  %m_solverBodyIdB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %91, i32 0, i32 19
  %92 = load i32, i32* %m_solverBodyIdB, align 4
  store i32 %92, i32* %bodyIndex1, align 4
  %93 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool97 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %93, i32 0, i32 1
  %94 = load i32, i32* %bodyIndex0, align 4
  %call98 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool97, i32 %94)
  %m_originalBody99 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call98, i32 0, i32 12
  %95 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody99, align 4
  %tobool100 = icmp ne %class.btRigidBody* %95, null
  br i1 %tobool100, label %if.then101, label %if.end142

if.then101:                                       ; preds = %for.body84
  %96 = load %struct.btMatrixX*, %struct.btMatrixX** %J, align 4
  %97 = load i32, i32* %i81, align 4
  %98 = load i32, i32* %bodyIndex0, align 4
  %mul102 = mul nsw i32 6, %98
  %add103 = add nsw i32 %mul102, 0
  %m_allConstraintPtrArray104 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %99 = load i32, i32* %i81, align 4
  %call105 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray104, i32 %99)
  %100 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call105, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %100, i32 0, i32 1
  %call106 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_contactNormal1)
  %arrayidx107 = getelementptr inbounds float, float* %call106, i32 0
  %101 = load float, float* %arrayidx107, align 4
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(60) %96, i32 %97, i32 %add103, float %101)
  %102 = load %struct.btMatrixX*, %struct.btMatrixX** %J, align 4
  %103 = load i32, i32* %i81, align 4
  %104 = load i32, i32* %bodyIndex0, align 4
  %mul108 = mul nsw i32 6, %104
  %add109 = add nsw i32 %mul108, 1
  %m_allConstraintPtrArray110 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %105 = load i32, i32* %i81, align 4
  %call111 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray110, i32 %105)
  %106 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call111, align 4
  %m_contactNormal1112 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %106, i32 0, i32 1
  %call113 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_contactNormal1112)
  %arrayidx114 = getelementptr inbounds float, float* %call113, i32 1
  %107 = load float, float* %arrayidx114, align 4
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(60) %102, i32 %103, i32 %add109, float %107)
  %108 = load %struct.btMatrixX*, %struct.btMatrixX** %J, align 4
  %109 = load i32, i32* %i81, align 4
  %110 = load i32, i32* %bodyIndex0, align 4
  %mul115 = mul nsw i32 6, %110
  %add116 = add nsw i32 %mul115, 2
  %m_allConstraintPtrArray117 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %111 = load i32, i32* %i81, align 4
  %call118 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray117, i32 %111)
  %112 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call118, align 4
  %m_contactNormal1119 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %112, i32 0, i32 1
  %call120 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_contactNormal1119)
  %arrayidx121 = getelementptr inbounds float, float* %call120, i32 2
  %113 = load float, float* %arrayidx121, align 4
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(60) %108, i32 %109, i32 %add116, float %113)
  %114 = load %struct.btMatrixX*, %struct.btMatrixX** %J, align 4
  %115 = load i32, i32* %i81, align 4
  %116 = load i32, i32* %bodyIndex0, align 4
  %mul122 = mul nsw i32 6, %116
  %add123 = add nsw i32 %mul122, 3
  %m_allConstraintPtrArray124 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %117 = load i32, i32* %i81, align 4
  %call125 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray124, i32 %117)
  %118 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call125, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %118, i32 0, i32 0
  %call126 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_relpos1CrossNormal)
  %arrayidx127 = getelementptr inbounds float, float* %call126, i32 0
  %119 = load float, float* %arrayidx127, align 4
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(60) %114, i32 %115, i32 %add123, float %119)
  %120 = load %struct.btMatrixX*, %struct.btMatrixX** %J, align 4
  %121 = load i32, i32* %i81, align 4
  %122 = load i32, i32* %bodyIndex0, align 4
  %mul128 = mul nsw i32 6, %122
  %add129 = add nsw i32 %mul128, 4
  %m_allConstraintPtrArray130 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %123 = load i32, i32* %i81, align 4
  %call131 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray130, i32 %123)
  %124 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call131, align 4
  %m_relpos1CrossNormal132 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %124, i32 0, i32 0
  %call133 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_relpos1CrossNormal132)
  %arrayidx134 = getelementptr inbounds float, float* %call133, i32 1
  %125 = load float, float* %arrayidx134, align 4
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(60) %120, i32 %121, i32 %add129, float %125)
  %126 = load %struct.btMatrixX*, %struct.btMatrixX** %J, align 4
  %127 = load i32, i32* %i81, align 4
  %128 = load i32, i32* %bodyIndex0, align 4
  %mul135 = mul nsw i32 6, %128
  %add136 = add nsw i32 %mul135, 5
  %m_allConstraintPtrArray137 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %129 = load i32, i32* %i81, align 4
  %call138 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray137, i32 %129)
  %130 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call138, align 4
  %m_relpos1CrossNormal139 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %130, i32 0, i32 0
  %call140 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_relpos1CrossNormal139)
  %arrayidx141 = getelementptr inbounds float, float* %call140, i32 2
  %131 = load float, float* %arrayidx141, align 4
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(60) %126, i32 %127, i32 %add136, float %131)
  br label %if.end142

if.end142:                                        ; preds = %if.then101, %for.body84
  %132 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool143 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %132, i32 0, i32 1
  %133 = load i32, i32* %bodyIndex1, align 4
  %call144 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool143, i32 %133)
  %m_originalBody145 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call144, i32 0, i32 12
  %134 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody145, align 4
  %tobool146 = icmp ne %class.btRigidBody* %134, null
  br i1 %tobool146, label %if.then147, label %if.end188

if.then147:                                       ; preds = %if.end142
  %135 = load %struct.btMatrixX*, %struct.btMatrixX** %J, align 4
  %136 = load i32, i32* %i81, align 4
  %137 = load i32, i32* %bodyIndex1, align 4
  %mul148 = mul nsw i32 6, %137
  %add149 = add nsw i32 %mul148, 0
  %m_allConstraintPtrArray150 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %138 = load i32, i32* %i81, align 4
  %call151 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray150, i32 %138)
  %139 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call151, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %139, i32 0, i32 3
  %call152 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_contactNormal2)
  %arrayidx153 = getelementptr inbounds float, float* %call152, i32 0
  %140 = load float, float* %arrayidx153, align 4
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(60) %135, i32 %136, i32 %add149, float %140)
  %141 = load %struct.btMatrixX*, %struct.btMatrixX** %J, align 4
  %142 = load i32, i32* %i81, align 4
  %143 = load i32, i32* %bodyIndex1, align 4
  %mul154 = mul nsw i32 6, %143
  %add155 = add nsw i32 %mul154, 1
  %m_allConstraintPtrArray156 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %144 = load i32, i32* %i81, align 4
  %call157 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray156, i32 %144)
  %145 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call157, align 4
  %m_contactNormal2158 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %145, i32 0, i32 3
  %call159 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_contactNormal2158)
  %arrayidx160 = getelementptr inbounds float, float* %call159, i32 1
  %146 = load float, float* %arrayidx160, align 4
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(60) %141, i32 %142, i32 %add155, float %146)
  %147 = load %struct.btMatrixX*, %struct.btMatrixX** %J, align 4
  %148 = load i32, i32* %i81, align 4
  %149 = load i32, i32* %bodyIndex1, align 4
  %mul161 = mul nsw i32 6, %149
  %add162 = add nsw i32 %mul161, 2
  %m_allConstraintPtrArray163 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %150 = load i32, i32* %i81, align 4
  %call164 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray163, i32 %150)
  %151 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call164, align 4
  %m_contactNormal2165 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %151, i32 0, i32 3
  %call166 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_contactNormal2165)
  %arrayidx167 = getelementptr inbounds float, float* %call166, i32 2
  %152 = load float, float* %arrayidx167, align 4
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(60) %147, i32 %148, i32 %add162, float %152)
  %153 = load %struct.btMatrixX*, %struct.btMatrixX** %J, align 4
  %154 = load i32, i32* %i81, align 4
  %155 = load i32, i32* %bodyIndex1, align 4
  %mul168 = mul nsw i32 6, %155
  %add169 = add nsw i32 %mul168, 3
  %m_allConstraintPtrArray170 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %156 = load i32, i32* %i81, align 4
  %call171 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray170, i32 %156)
  %157 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call171, align 4
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %157, i32 0, i32 2
  %call172 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_relpos2CrossNormal)
  %arrayidx173 = getelementptr inbounds float, float* %call172, i32 0
  %158 = load float, float* %arrayidx173, align 4
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(60) %153, i32 %154, i32 %add169, float %158)
  %159 = load %struct.btMatrixX*, %struct.btMatrixX** %J, align 4
  %160 = load i32, i32* %i81, align 4
  %161 = load i32, i32* %bodyIndex1, align 4
  %mul174 = mul nsw i32 6, %161
  %add175 = add nsw i32 %mul174, 4
  %m_allConstraintPtrArray176 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %162 = load i32, i32* %i81, align 4
  %call177 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray176, i32 %162)
  %163 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call177, align 4
  %m_relpos2CrossNormal178 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %163, i32 0, i32 2
  %call179 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_relpos2CrossNormal178)
  %arrayidx180 = getelementptr inbounds float, float* %call179, i32 1
  %164 = load float, float* %arrayidx180, align 4
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(60) %159, i32 %160, i32 %add175, float %164)
  %165 = load %struct.btMatrixX*, %struct.btMatrixX** %J, align 4
  %166 = load i32, i32* %i81, align 4
  %167 = load i32, i32* %bodyIndex1, align 4
  %mul181 = mul nsw i32 6, %167
  %add182 = add nsw i32 %mul181, 5
  %m_allConstraintPtrArray183 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %168 = load i32, i32* %i81, align 4
  %call184 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray183, i32 %168)
  %169 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call184, align 4
  %m_relpos2CrossNormal185 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %169, i32 0, i32 2
  %call186 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_relpos2CrossNormal185)
  %arrayidx187 = getelementptr inbounds float, float* %call186, i32 2
  %170 = load float, float* %arrayidx187, align 4
  call void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(60) %165, i32 %166, i32 %add182, float %170)
  br label %if.end188

if.end188:                                        ; preds = %if.then147, %if.end142
  br label %for.inc189

for.inc189:                                       ; preds = %if.end188
  %171 = load i32, i32* %i81, align 4
  %inc190 = add nsw i32 %171, 1
  store i32 %inc190, i32* %i81, align 4
  br label %for.cond82

for.end191:                                       ; preds = %for.cond82
  %m_scratchJTranspose = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 19
  store %struct.btMatrixX* %m_scratchJTranspose, %struct.btMatrixX** %J_transpose, align 4
  %172 = load %struct.btMatrixX*, %struct.btMatrixX** %J, align 4
  call void @_ZNK9btMatrixXIfE9transposeEv(%struct.btMatrixX* sret align 4 %ref.tmp, %struct.btMatrixX* %172)
  %173 = load %struct.btMatrixX*, %struct.btMatrixX** %J_transpose, align 4
  %call192 = call nonnull align 4 dereferenceable(60) %struct.btMatrixX* @_ZN9btMatrixXIfEaSEOS0_(%struct.btMatrixX* %173, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %ref.tmp)
  %call193 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %ref.tmp) #8
  %m_scratchTmp = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 20
  store %struct.btMatrixX* %m_scratchTmp, %struct.btMatrixX** %tmp, align 4
  %call194 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.17, i32 0, i32 0))
  %174 = load %struct.btMatrixX*, %struct.btMatrixX** %J, align 4
  %175 = load %struct.btMatrixX*, %struct.btMatrixX** %Minv, align 4
  call void @_ZN9btMatrixXIfEmlERKS0_(%struct.btMatrixX* sret align 4 %ref.tmp195, %struct.btMatrixX* %174, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %175)
  %176 = load %struct.btMatrixX*, %struct.btMatrixX** %tmp, align 4
  %call196 = call nonnull align 4 dereferenceable(60) %struct.btMatrixX* @_ZN9btMatrixXIfEaSEOS0_(%struct.btMatrixX* %176, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %ref.tmp195)
  %call197 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %ref.tmp195) #8
  %call198 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #8
  %call200 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile199, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.18, i32 0, i32 0))
  %177 = load %struct.btMatrixX*, %struct.btMatrixX** %tmp, align 4
  %178 = load %struct.btMatrixX*, %struct.btMatrixX** %J_transpose, align 4
  call void @_ZN9btMatrixXIfEmlERKS0_(%struct.btMatrixX* sret align 4 %ref.tmp201, %struct.btMatrixX* %177, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %178)
  %m_A = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %call202 = call nonnull align 4 dereferenceable(60) %struct.btMatrixX* @_ZN9btMatrixXIfEaSEOS0_(%struct.btMatrixX* %m_A, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %ref.tmp201)
  %call203 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %ref.tmp201) #8
  %call204 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile199) #8
  store i32 0, i32* %i205, align 4
  br label %for.cond206

for.cond206:                                      ; preds = %for.inc216, %for.end191
  %179 = load i32, i32* %i205, align 4
  %m_A207 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %call208 = call i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %m_A207)
  %cmp209 = icmp slt i32 %179, %call208
  br i1 %cmp209, label %for.body210, label %for.end218

for.body210:                                      ; preds = %for.cond206
  %m_A211 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %180 = load i32, i32* %i205, align 4
  %181 = load i32, i32* %i205, align 4
  %m_A212 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 1
  %182 = load i32, i32* %i205, align 4
  %183 = load i32, i32* %i205, align 4
  %call213 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %m_A212, i32 %182, i32 %183)
  %184 = load float, float* %call213, align 4
  %185 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %186 = bitcast %struct.btContactSolverInfo* %185 to %struct.btContactSolverInfoData*
  %m_globalCfm = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %186, i32 0, i32 10
  %187 = load float, float* %m_globalCfm, align 4
  %188 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %189 = bitcast %struct.btContactSolverInfo* %188 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %189, i32 0, i32 3
  %190 = load float, float* %m_timeStep, align 4
  %div214 = fdiv float %187, %190
  %add215 = fadd float %184, %div214
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %m_A211, i32 %180, i32 %181, float %add215)
  br label %for.inc216

for.inc216:                                       ; preds = %for.body210
  %191 = load i32, i32* %i205, align 4
  %inc217 = add nsw i32 %191, 1
  store i32 %inc217, i32* %i205, align 4
  br label %for.cond206

for.end218:                                       ; preds = %for.cond206
  %m_x = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %192 = load i32, i32* %numConstraintRows, align 4
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_x, i32 %192)
  %193 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %194 = bitcast %struct.btContactSolverInfo* %193 to %struct.btContactSolverInfoData*
  %m_splitImpulse219 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %194, i32 0, i32 11
  %195 = load i32, i32* %m_splitImpulse219, align 4
  %tobool220 = icmp ne i32 %195, 0
  br i1 %tobool220, label %if.then221, label %if.end222

if.then221:                                       ; preds = %for.end218
  %m_xSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  %196 = load i32, i32* %numConstraintRows, align 4
  call void @_ZN9btVectorXIfE6resizeEi(%struct.btVectorX* %m_xSplit, i32 %196)
  br label %if.end222

if.end222:                                        ; preds = %if.then221, %for.end218
  store i32 0, i32* %i223, align 4
  br label %for.cond224

for.cond224:                                      ; preds = %for.inc240, %if.end222
  %197 = load i32, i32* %i223, align 4
  %m_allConstraintPtrArray225 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call226 = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray225)
  %cmp227 = icmp slt i32 %197, %call226
  br i1 %cmp227, label %for.body228, label %for.end242

for.body228:                                      ; preds = %for.cond224
  %m_allConstraintPtrArray230 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %198 = load i32, i32* %i223, align 4
  %call231 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray230, i32 %198)
  %199 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call231, align 4
  store %struct.btSolverConstraint* %199, %struct.btSolverConstraint** %c229, align 4
  %200 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c229, align 4
  %m_appliedImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %200, i32 0, i32 7
  %201 = load float, float* %m_appliedImpulse, align 4
  %m_x232 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %202 = load i32, i32* %i223, align 4
  %call233 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_x232, i32 %202)
  store float %201, float* %call233, align 4
  %203 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %204 = bitcast %struct.btContactSolverInfo* %203 to %struct.btContactSolverInfoData*
  %m_splitImpulse234 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %204, i32 0, i32 11
  %205 = load i32, i32* %m_splitImpulse234, align 4
  %tobool235 = icmp ne i32 %205, 0
  br i1 %tobool235, label %if.then236, label %if.end239

if.then236:                                       ; preds = %for.body228
  %206 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c229, align 4
  %m_appliedPushImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %206, i32 0, i32 6
  %207 = load float, float* %m_appliedPushImpulse, align 4
  %m_xSplit237 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  %208 = load i32, i32* %i223, align 4
  %call238 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_xSplit237, i32 %208)
  store float %207, float* %call238, align 4
  br label %if.end239

if.end239:                                        ; preds = %if.then236, %for.body228
  br label %for.inc240

for.inc240:                                       ; preds = %if.end239
  %209 = load i32, i32* %i223, align 4
  %inc241 = add nsw i32 %209, 1
  store i32 %inc241, i32* %i223, align 4
  br label %for.cond224

for.end242:                                       ; preds = %for.cond224
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z7setElemR9btMatrixXIfEiif(%struct.btMatrixX* nonnull align 4 dereferenceable(60) %mat, i32 %row, i32 %col, float %val) #2 comdat {
entry:
  %mat.addr = alloca %struct.btMatrixX*, align 4
  %row.addr = alloca i32, align 4
  %col.addr = alloca i32, align 4
  %val.addr = alloca float, align 4
  store %struct.btMatrixX* %mat, %struct.btMatrixX** %mat.addr, align 4
  store i32 %row, i32* %row.addr, align 4
  store i32 %col, i32* %col.addr, align 4
  store float %val, float* %val.addr, align 4
  %0 = load %struct.btMatrixX*, %struct.btMatrixX** %mat.addr, align 4
  %1 = load i32, i32* %row.addr, align 4
  %2 = load i32, i32* %col.addr, align 4
  %3 = load float, float* %val.addr, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %0, i32 %1, i32 %2, float %3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btMatrixXIfE9transposeEv(%struct.btMatrixX* noalias sret align 4 %agg.result, %struct.btMatrixX* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %struct.btMatrixX*, align 4
  %nrvo = alloca i1, align 1
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %v = alloca float, align 4
  %0 = bitcast %struct.btMatrixX* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  store i1 false, i1* %nrvo, align 1
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_cols, align 4
  %m_rows = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  %2 = load i32, i32* %m_rows, align 4
  %call = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Eii(%struct.btMatrixX* %agg.result, i32 %1, i32 %2)
  call void @_ZN9btMatrixXIfE7setZeroEv(%struct.btMatrixX* %agg.result)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc8, %entry
  %3 = load i32, i32* %i, align 4
  %m_cols2 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %4 = load i32, i32* %m_cols2, align 4
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end10

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %j, align 4
  %m_rows4 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  %6 = load i32, i32* %m_rows4, align 4
  %cmp5 = icmp slt i32 %5, %6
  br i1 %cmp5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond3
  %7 = load i32, i32* %j, align 4
  %8 = load i32, i32* %i, align 4
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %this1, i32 %7, i32 %8)
  %9 = load float, float* %call7, align 4
  store float %9, float* %v, align 4
  %10 = load float, float* %v, align 4
  %tobool = fcmp une float %10, 0.000000e+00
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body6
  %11 = load i32, i32* %i, align 4
  %12 = load i32, i32* %j, align 4
  %13 = load float, float* %v, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %agg.result, i32 %11, i32 %12, float %13)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body6
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %14 = load i32, i32* %j, align 4
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond3

for.end:                                          ; preds = %for.cond3
  br label %for.inc8

for.inc8:                                         ; preds = %for.end
  %15 = load i32, i32* %i, align 4
  %inc9 = add nsw i32 %15, 1
  store i32 %inc9, i32* %i, align 4
  br label %for.cond

for.end10:                                        ; preds = %for.cond
  store i1 true, i1* %nrvo, align 1
  %nrvo.val = load i1, i1* %nrvo, align 1
  br i1 %nrvo.val, label %nrvo.skipdtor, label %nrvo.unused

nrvo.unused:                                      ; preds = %for.end10
  %call11 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %agg.result) #8
  br label %nrvo.skipdtor

nrvo.skipdtor:                                    ; preds = %nrvo.unused, %for.end10
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(60) %struct.btMatrixX* @_ZN9btMatrixXIfEaSEOS0_(%struct.btMatrixX* %this, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %0) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %.addr = alloca %struct.btMatrixX*, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  store %struct.btMatrixX* %0, %struct.btMatrixX** %.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_rows = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  %1 = load %struct.btMatrixX*, %struct.btMatrixX** %.addr, align 4
  %m_rows2 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %1, i32 0, i32 0
  %2 = bitcast i32* %m_rows to i8*
  %3 = bitcast i32* %m_rows2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 4 %2, i8* align 4 %3, i64 20, i1 false)
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %4 = load %struct.btMatrixX*, %struct.btMatrixX** %.addr, align 4
  %m_storage3 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %4, i32 0, i32 5
  %call = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfEaSERKS0_(%class.btAlignedObjectArray.22* %m_storage, %class.btAlignedObjectArray.22* nonnull align 4 dereferenceable(17) %m_storage3)
  %m_rowNonZeroElements1 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %5 = load %struct.btMatrixX*, %struct.btMatrixX** %.addr, align 4
  %m_rowNonZeroElements14 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %5, i32 0, i32 6
  %call5 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.26* @_ZN20btAlignedObjectArrayIS_IiEEaSERKS1_(%class.btAlignedObjectArray.26* %m_rowNonZeroElements1, %class.btAlignedObjectArray.26* nonnull align 4 dereferenceable(17) %m_rowNonZeroElements14)
  ret %struct.btMatrixX* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btMatrixXIfEmlERKS0_(%struct.btMatrixX* noalias sret align 4 %agg.result, %struct.btMatrixX* %this, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %other) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %struct.btMatrixX*, align 4
  %other.addr = alloca %struct.btMatrixX*, align 4
  %nrvo = alloca i1, align 1
  %j = alloca i32, align 4
  %i = alloca i32, align 4
  %dotProd = alloca float, align 4
  %v = alloca i32, align 4
  %w = alloca float, align 4
  %0 = bitcast %struct.btMatrixX* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  store %struct.btMatrixX* %other, %struct.btMatrixX** %other.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  store i1 false, i1* %nrvo, align 1
  %call = call i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %this1)
  %1 = load %struct.btMatrixX*, %struct.btMatrixX** %other.addr, align 4
  %call2 = call i32 @_ZNK9btMatrixXIfE4colsEv(%struct.btMatrixX* %1)
  %call3 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Eii(%struct.btMatrixX* %agg.result, i32 %call, i32 %call2)
  call void @_ZN9btMatrixXIfE7setZeroEv(%struct.btMatrixX* %agg.result)
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc22, %entry
  %2 = load i32, i32* %j, align 4
  %call4 = call i32 @_ZNK9btMatrixXIfE4colsEv(%struct.btMatrixX* %agg.result)
  %cmp = icmp slt i32 %2, %call4
  br i1 %cmp, label %for.body, label %for.end24

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc19, %for.body
  %3 = load i32, i32* %i, align 4
  %call6 = call i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %agg.result)
  %cmp7 = icmp slt i32 %3, %call6
  br i1 %cmp7, label %for.body8, label %for.end21

for.body8:                                        ; preds = %for.cond5
  store float 0.000000e+00, float* %dotProd, align 4
  store i32 0, i32* %v, align 4
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc, %for.body8
  %4 = load i32, i32* %v, align 4
  %call10 = call i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %this1)
  %cmp11 = icmp slt i32 %4, %call10
  br i1 %cmp11, label %for.body12, label %for.end

for.body12:                                       ; preds = %for.cond9
  %5 = load i32, i32* %i, align 4
  %6 = load i32, i32* %v, align 4
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %this1, i32 %5, i32 %6)
  %7 = load float, float* %call13, align 4
  store float %7, float* %w, align 4
  %8 = load %struct.btMatrixX*, %struct.btMatrixX** %other.addr, align 4
  %9 = load i32, i32* %v, align 4
  %10 = load i32, i32* %j, align 4
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %8, i32 %9, i32 %10)
  %11 = load float, float* %call14, align 4
  %cmp15 = fcmp une float %11, 0.000000e+00
  br i1 %cmp15, label %if.then, label %if.end

if.then:                                          ; preds = %for.body12
  %12 = load float, float* %w, align 4
  %13 = load %struct.btMatrixX*, %struct.btMatrixX** %other.addr, align 4
  %14 = load i32, i32* %v, align 4
  %15 = load i32, i32* %j, align 4
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %13, i32 %14, i32 %15)
  %16 = load float, float* %call16, align 4
  %mul = fmul float %12, %16
  %17 = load float, float* %dotProd, align 4
  %add = fadd float %17, %mul
  store float %add, float* %dotProd, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body12
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %18 = load i32, i32* %v, align 4
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %v, align 4
  br label %for.cond9

for.end:                                          ; preds = %for.cond9
  %19 = load float, float* %dotProd, align 4
  %tobool = fcmp une float %19, 0.000000e+00
  br i1 %tobool, label %if.then17, label %if.end18

if.then17:                                        ; preds = %for.end
  %20 = load i32, i32* %i, align 4
  %21 = load i32, i32* %j, align 4
  %22 = load float, float* %dotProd, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %agg.result, i32 %20, i32 %21, float %22)
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %for.end
  br label %for.inc19

for.inc19:                                        ; preds = %if.end18
  %23 = load i32, i32* %i, align 4
  %inc20 = add nsw i32 %23, 1
  store i32 %inc20, i32* %i, align 4
  br label %for.cond5

for.end21:                                        ; preds = %for.cond5
  br label %for.inc22

for.inc22:                                        ; preds = %for.end21
  %24 = load i32, i32* %j, align 4
  %inc23 = add nsw i32 %24, 1
  store i32 %inc23, i32* %j, align 4
  br label %for.cond

for.end24:                                        ; preds = %for.cond
  store i1 true, i1* %nrvo, align 1
  %nrvo.val = load i1, i1* %nrvo, align 1
  br i1 %nrvo.val, label %nrvo.skipdtor, label %nrvo.unused

nrvo.unused:                                      ; preds = %for.end24
  %call25 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %agg.result) #8
  br label %nrvo.skipdtor

nrvo.skipdtor:                                    ; preds = %nrvo.unused, %for.end24
  ret void
}

; Function Attrs: noinline optnone
define hidden float @_ZN12btMLCPSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btMLCPSolver* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %class.btPersistentManifold** %manifoldPtr, i32 %numManifolds, %class.btTypedConstraint** %constraints, i32 %numConstraints, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal, %class.btIDebugDraw* %debugDrawer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMLCPSolver*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %result = alloca i8, align 1
  %__profile = alloca %class.CProfileSample, align 1
  %__profile4 = alloca %class.CProfileSample, align 1
  %i = alloca i32, align 4
  %c = alloca %struct.btSolverConstraint*, align 4
  %sbA = alloca i32, align 4
  %sbB = alloca i32, align 4
  %solverBodyA = alloca %struct.btSolverBody*, align 4
  %solverBodyB = alloca %struct.btSolverBody*, align 4
  %deltaImpulse = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp17 = alloca %class.btVector3, align 4
  %deltaImpulse21 = alloca float, align 4
  %ref.tmp24 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca %class.btVector3, align 4
  store %class.btMLCPSolver* %this, %class.btMLCPSolver** %this.addr, align 4
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4
  store i32 %numBodies, i32* %numBodies.addr, align 4
  store %class.btPersistentManifold** %manifoldPtr, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  store i32 %numManifolds, i32* %numManifolds.addr, align 4
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4
  store i32 %numConstraints, i32* %numConstraints.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %this1 = load %class.btMLCPSolver*, %class.btMLCPSolver** %this.addr, align 4
  store i8 1, i8* %result, align 1
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.19, i32 0, i32 0))
  %0 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %1 = bitcast %class.btMLCPSolver* %this1 to i1 (%class.btMLCPSolver*, %struct.btContactSolverInfo*)***
  %vtable = load i1 (%class.btMLCPSolver*, %struct.btContactSolverInfo*)**, i1 (%class.btMLCPSolver*, %struct.btContactSolverInfo*)*** %1, align 4
  %vfn = getelementptr inbounds i1 (%class.btMLCPSolver*, %struct.btContactSolverInfo*)*, i1 (%class.btMLCPSolver*, %struct.btContactSolverInfo*)** %vtable, i64 15
  %2 = load i1 (%class.btMLCPSolver*, %struct.btContactSolverInfo*)*, i1 (%class.btMLCPSolver*, %struct.btContactSolverInfo*)** %vfn, align 4
  %call2 = call zeroext i1 %2(%class.btMLCPSolver* %this1, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %0)
  %frombool = zext i1 %call2 to i8
  store i8 %frombool, i8* %result, align 1
  %call3 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #8
  %3 = load i8, i8* %result, align 1
  %tobool = trunc i8 %3 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call5 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile4, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.20, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %4 = load i32, i32* %i, align 4
  %m_allConstraintPtrArray = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %call6 = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray)
  %cmp = icmp slt i32 %4, %call6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_allConstraintPtrArray7 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 11
  %5 = load i32, i32* %i, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %struct.btSolverConstraint** @_ZN20btAlignedObjectArrayIP18btSolverConstraintEixEi(%class.btAlignedObjectArray.30* %m_allConstraintPtrArray7, i32 %5)
  %6 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %call8, align 4
  store %struct.btSolverConstraint* %6, %struct.btSolverConstraint** %c, align 4
  %7 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4
  %m_solverBodyIdA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %7, i32 0, i32 18
  %8 = load i32, i32* %m_solverBodyIdA, align 4
  store i32 %8, i32* %sbA, align 4
  %9 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4
  %m_solverBodyIdB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %9, i32 0, i32 19
  %10 = load i32, i32* %m_solverBodyIdB, align 4
  store i32 %10, i32* %sbB, align 4
  %11 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %11, i32 0, i32 1
  %12 = load i32, i32* %sbA, align 4
  %call9 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool, i32 %12)
  store %struct.btSolverBody* %call9, %struct.btSolverBody** %solverBodyA, align 4
  %13 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool10 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %13, i32 0, i32 1
  %14 = load i32, i32* %sbB, align 4
  %call11 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool10, i32 %14)
  store %struct.btSolverBody* %call11, %struct.btSolverBody** %solverBodyB, align 4
  %m_x = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %15 = load i32, i32* %i, align 4
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_x, i32 %15)
  %16 = load float, float* %call12, align 4
  %17 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4
  %m_appliedImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %17, i32 0, i32 7
  %18 = load float, float* %m_appliedImpulse, align 4
  %sub = fsub float %16, %18
  store float %sub, float* %deltaImpulse, align 4
  %m_x13 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 3
  %19 = load i32, i32* %i, align 4
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_x13, i32 %19)
  %20 = load float, float* %call14, align 4
  %21 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4
  %m_appliedImpulse15 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %21, i32 0, i32 7
  store float %20, float* %m_appliedImpulse15, align 4
  %22 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyA, align 4
  %23 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %23, i32 0, i32 1
  %24 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyA, align 4
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %24)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1, %class.btVector3* nonnull align 4 dereferenceable(16) %call16)
  %25 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4
  %m_angularComponentA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %25, i32 0, i32 4
  %26 = load float, float* %deltaImpulse, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %22, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentA, float %26)
  %27 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyB, align 4
  %28 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %28, i32 0, i32 3
  %29 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyB, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %29)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp17, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  %30 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4
  %m_angularComponentB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %30, i32 0, i32 5
  %31 = load float, float* %deltaImpulse, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %27, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp17, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB, float %31)
  %32 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %33 = bitcast %struct.btContactSolverInfo* %32 to %struct.btContactSolverInfoData*
  %m_splitImpulse = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %33, i32 0, i32 11
  %34 = load i32, i32* %m_splitImpulse, align 4
  %tobool19 = icmp ne i32 %34, 0
  br i1 %tobool19, label %if.then20, label %if.end

if.then20:                                        ; preds = %for.body
  %m_xSplit = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  %35 = load i32, i32* %i, align 4
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_xSplit, i32 %35)
  %36 = load float, float* %call22, align 4
  %37 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4
  %m_appliedPushImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %37, i32 0, i32 6
  %38 = load float, float* %m_appliedPushImpulse, align 4
  %sub23 = fsub float %36, %38
  store float %sub23, float* %deltaImpulse21, align 4
  %39 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyA, align 4
  %40 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4
  %m_contactNormal125 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %40, i32 0, i32 1
  %41 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyA, align 4
  %call26 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %41)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp24, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal125, %class.btVector3* nonnull align 4 dereferenceable(16) %call26)
  %42 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4
  %m_angularComponentA27 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %42, i32 0, i32 4
  %43 = load float, float* %deltaImpulse21, align 4
  call void @_ZN12btSolverBody24internalApplyPushImpulseERK9btVector3S2_f(%struct.btSolverBody* %39, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp24, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentA27, float %43)
  %44 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyB, align 4
  %45 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4
  %m_contactNormal229 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %45, i32 0, i32 3
  %46 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyB, align 4
  %call30 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %46)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp28, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal229, %class.btVector3* nonnull align 4 dereferenceable(16) %call30)
  %47 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4
  %m_angularComponentB31 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %47, i32 0, i32 5
  %48 = load float, float* %deltaImpulse21, align 4
  call void @_ZN12btSolverBody24internalApplyPushImpulseERK9btVector3S2_f(%struct.btSolverBody* %44, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp28, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB31, float %48)
  %m_xSplit32 = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 7
  %49 = load i32, i32* %i, align 4
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %m_xSplit32, i32 %49)
  %50 = load float, float* %call33, align 4
  %51 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4
  %m_appliedPushImpulse34 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %51, i32 0, i32 6
  store float %50, float* %m_appliedPushImpulse34, align 4
  br label %if.end

if.end:                                           ; preds = %if.then20, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %52 = load i32, i32* %i, align 4
  %inc = add nsw i32 %52, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call35 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile4) #8
  br label %if.end38

if.else:                                          ; preds = %entry
  %m_fallback = getelementptr inbounds %class.btMLCPSolver, %class.btMLCPSolver* %this1, i32 0, i32 13
  %53 = load i32, i32* %m_fallback, align 4
  %inc36 = add nsw i32 %53, 1
  store i32 %inc36, i32* %m_fallback, align 4
  %54 = bitcast %class.btMLCPSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %55 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %56 = load i32, i32* %numBodies.addr, align 4
  %57 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  %58 = load i32, i32* %numManifolds.addr, align 4
  %59 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %60 = load i32, i32* %numConstraints.addr, align 4
  %61 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %62 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %call37 = call float @_ZN35btSequentialImpulseConstraintSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver* %54, %class.btCollisionObject** %55, i32 %56, %class.btPersistentManifold** %57, i32 %58, %class.btTypedConstraint** %59, i32 %60, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %61, %class.btIDebugDraw* %62)
  br label %if.end38

if.end38:                                         ; preds = %if.else, %for.end
  ret float 0.000000e+00
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %linearComponent, %class.btVector3* nonnull align 4 dereferenceable(16) %angularComponent, float %impulseMagnitude) #2 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  %linearComponent.addr = alloca %class.btVector3*, align 4
  %angularComponent.addr = alloca %class.btVector3*, align 4
  %impulseMagnitude.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  store %class.btVector3* %linearComponent, %class.btVector3** %linearComponent.addr, align 4
  store %class.btVector3* %angularComponent, %class.btVector3** %angularComponent.addr, align 4
  store float %impulseMagnitude, float* %impulseMagnitude.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 12
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  %tobool = icmp ne %class.btRigidBody* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btVector3*, %class.btVector3** %linearComponent.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr)
  %m_linearFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 4
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  %m_deltaLinearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_deltaLinearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %2 = load %class.btVector3*, %class.btVector3** %angularComponent.addr, align 4
  %m_angularFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 3
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularFactor)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %m_deltaAngularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 2
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_deltaAngularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_invMass = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 5
  ret %class.btVector3* %m_invMass
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btSolverBody24internalApplyPushImpulseERK9btVector3S2_f(%struct.btSolverBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %linearComponent, %class.btVector3* nonnull align 4 dereferenceable(16) %angularComponent, float %impulseMagnitude) #2 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  %linearComponent.addr = alloca %class.btVector3*, align 4
  %angularComponent.addr = alloca %class.btVector3*, align 4
  %impulseMagnitude.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  store %class.btVector3* %linearComponent, %class.btVector3** %linearComponent.addr, align 4
  store %class.btVector3* %angularComponent, %class.btVector3** %angularComponent.addr, align 4
  store float %impulseMagnitude, float* %impulseMagnitude.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 12
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  %tobool = icmp ne %class.btRigidBody* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btVector3*, %class.btVector3** %linearComponent.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr)
  %m_linearFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 4
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  %m_pushVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 6
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_pushVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %2 = load %class.btVector3*, %class.btVector3** %angularComponent.addr, align 4
  %m_angularFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 3
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularFactor)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %m_turnVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 7
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_turnVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare float @_ZN35btSequentialImpulseConstraintSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88), %class.btIDebugDraw*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN18btConstraintSolver12prepareSolveEii(%class.btConstraintSolver* %this, i32 %0, i32 %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConstraintSolver*, align 4
  %.addr = alloca i32, align 4
  %.addr1 = alloca i32, align 4
  store %class.btConstraintSolver* %this, %class.btConstraintSolver** %this.addr, align 4
  store i32 %0, i32* %.addr, align 4
  store i32 %1, i32* %.addr1, align 4
  %this2 = load %class.btConstraintSolver*, %class.btConstraintSolver** %this.addr, align 4
  ret void
}

declare float @_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88), %class.btIDebugDraw*, %class.btDispatcher*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDraw(%class.btConstraintSolver* %this, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %0, %class.btIDebugDraw* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConstraintSolver*, align 4
  %.addr = alloca %struct.btContactSolverInfo*, align 4
  %.addr1 = alloca %class.btIDebugDraw*, align 4
  store %class.btConstraintSolver* %this, %class.btConstraintSolver** %this.addr, align 4
  store %struct.btContactSolverInfo* %0, %struct.btContactSolverInfo** %.addr, align 4
  store %class.btIDebugDraw* %1, %class.btIDebugDraw** %.addr1, align 4
  %this2 = load %class.btConstraintSolver*, %class.btConstraintSolver** %this.addr, align 4
  ret void
}

declare void @_ZN35btSequentialImpulseConstraintSolver5resetEv(%class.btSequentialImpulseConstraintSolver*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK12btMLCPSolver13getSolverTypeEv(%class.btMLCPSolver* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btMLCPSolver*, align 4
  store %class.btMLCPSolver* %this, %class.btMLCPSolver** %this.addr, align 4
  %this1 = load %class.btMLCPSolver*, %class.btMLCPSolver** %this.addr, align 4
  ret i32 2
}

declare void @_ZN35btSequentialImpulseConstraintSolver15convertContactsEPP20btPersistentManifoldiRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver*, %class.btPersistentManifold**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88)) unnamed_addr #3

declare void @_ZN35btSequentialImpulseConstraintSolver45solveGroupCacheFriendlySplitImpulseIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88), %class.btIDebugDraw*) unnamed_addr #3

declare float @_ZN35btSequentialImpulseConstraintSolver29solveGroupCacheFriendlyFinishEPP17btCollisionObjectiRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88)) unnamed_addr #3

declare float @_ZN35btSequentialImpulseConstraintSolver20solveSingleIterationEiPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver*, i32, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88), %class.btIDebugDraw*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.26* @_ZN20btAlignedObjectArrayIS_IiEED2Ev(%class.btAlignedObjectArray.26* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIS_IiEE5clearEv(%class.btAlignedObjectArray.26* %this1)
  ret %class.btAlignedObjectArray.26* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.22* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.22* %this1)
  ret %class.btAlignedObjectArray.22* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IiEE5clearEv(%class.btAlignedObjectArray.26* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv(%class.btAlignedObjectArray.26* %this1)
  call void @_ZN20btAlignedObjectArrayIS_IiEE7destroyEii(%class.btAlignedObjectArray.26* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIS_IiEE10deallocateEv(%class.btAlignedObjectArray.26* %this1)
  call void @_ZN20btAlignedObjectArrayIS_IiEE4initEv(%class.btAlignedObjectArray.26* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IiEE7destroyEii(%class.btAlignedObjectArray.26* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  %3 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %3, i32 %4
  %call = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.14* %arrayidx) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv(%class.btAlignedObjectArray.26* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IiEE10deallocateEv(%class.btAlignedObjectArray.26* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  %0 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %m_data, align 4
  %tobool = icmp ne %class.btAlignedObjectArray.14* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  %2 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EE10deallocateEPS1_(%class.btAlignedAllocator.27* %m_allocator, %class.btAlignedObjectArray.14* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  store %class.btAlignedObjectArray.14* null, %class.btAlignedObjectArray.14** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IiEE4initEv(%class.btAlignedObjectArray.26* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  store %class.btAlignedObjectArray.14* null, %class.btAlignedObjectArray.14** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EE10deallocateEPS1_(%class.btAlignedAllocator.27* %this, %class.btAlignedObjectArray.14* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.27*, align 4
  %ptr.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedAllocator.27* %this, %class.btAlignedAllocator.27** %this.addr, align 4
  store %class.btAlignedObjectArray.14* %ptr, %class.btAlignedObjectArray.14** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.27*, %class.btAlignedAllocator.27** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %ptr.addr, align 4
  %1 = bitcast %class.btAlignedObjectArray.14* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.22* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.22* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.22* %this1)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.22* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.22* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %3 = load float*, float** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.22* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.22* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %tobool = icmp ne float* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %2 = load float*, float** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.23* %m_allocator, float* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  store float* null, float** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.22* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  store float* null, float** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.23* %this, float* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.23*, align 4
  %ptr.addr = alloca float*, align 4
  store %class.btAlignedAllocator.23* %this, %class.btAlignedAllocator.23** %this.addr, align 4
  store float* %ptr, float** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.23*, %class.btAlignedAllocator.23** %this.addr, align 4
  %0 = load float*, float** %ptr.addr, align 4
  %1 = bitcast float* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i64, i1 immarg) #6

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfEC2ERKS0_(%class.btAlignedObjectArray.22* returned %this, %class.btAlignedObjectArray.22* nonnull align 4 dereferenceable(17) %otherArray) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %otherArray.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %otherSize = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store %class.btAlignedObjectArray.22* %otherArray, %class.btAlignedObjectArray.22** %otherArray.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.23* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.23* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.22* %this1)
  %0 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %otherArray.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.22* %0)
  store i32 %call2, i32* %otherSize, align 4
  %1 = load i32, i32* %otherSize, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.22* %this1, i32 %1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %2 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %otherArray.addr, align 4
  %3 = load i32, i32* %otherSize, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %4 = load float*, float** %m_data, align 4
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.22* %2, i32 0, i32 %3, float* %4)
  ret %class.btAlignedObjectArray.22* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.26* @_ZN20btAlignedObjectArrayIS_IiEEC2ERKS1_(%class.btAlignedObjectArray.26* returned %this, %class.btAlignedObjectArray.26* nonnull align 4 dereferenceable(17) %otherArray) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  %otherArray.addr = alloca %class.btAlignedObjectArray.26*, align 4
  %otherSize = alloca i32, align 4
  %ref.tmp = alloca %class.btAlignedObjectArray.14, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4
  store %class.btAlignedObjectArray.26* %otherArray, %class.btAlignedObjectArray.26** %otherArray.addr, align 4
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.27* @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EEC2Ev(%class.btAlignedAllocator.27* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIS_IiEE4initEv(%class.btAlignedObjectArray.26* %this1)
  %0 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %otherArray.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv(%class.btAlignedObjectArray.26* %0)
  store i32 %call2, i32* %otherSize, align 4
  %1 = load i32, i32* %otherSize, align 4
  %call3 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.14* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayIS_IiEE6resizeEiRKS0_(%class.btAlignedObjectArray.26* %this1, i32 %1, %class.btAlignedObjectArray.14* nonnull align 4 dereferenceable(17) %ref.tmp)
  %call4 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.14* %ref.tmp) #8
  %2 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %otherArray.addr, align 4
  %3 = load i32, i32* %otherSize, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  %4 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %m_data, align 4
  call void @_ZNK20btAlignedObjectArrayIS_IiEE4copyEiiPS0_(%class.btAlignedObjectArray.26* %2, i32 0, i32 %3, %class.btAlignedObjectArray.14* %4)
  ret %class.btAlignedObjectArray.26* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.23* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.23* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.23*, align 4
  store %class.btAlignedAllocator.23* %this, %class.btAlignedAllocator.23** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.23*, %class.btAlignedAllocator.23** %this.addr, align 4
  ret %class.btAlignedAllocator.23* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.22* %this, i32 %newsize, float* nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca float*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store float* %fillData, float** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %5 = load float*, float** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.22* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %14 = load float*, float** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds float, float* %14, i32 %15
  %16 = bitcast float* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to float*
  %18 = load float*, float** %fillData.addr, align 4
  %19 = load float, float* %18, align 4
  store float %19, float* %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.22* %this, i32 %start, i32 %end, float* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca float*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store float* %dest, float** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load float*, float** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  %5 = bitcast float* %arrayidx to i8*
  %6 = bitcast i8* %5 to float*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %7 = load float*, float** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds float, float* %7, i32 %8
  %9 = load float, float* %arrayidx2, align 4
  store float %9, float* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.22* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca float*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.22* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.22* %this1, i32 %1)
  %2 = bitcast i8* %call2 to float*
  store float* %2, float** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  %3 = load float*, float** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.22* %this1, i32 0, i32 %call3, float* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.22* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.22* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load float*, float** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  store float* %4, float** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.22* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.22* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.23* %m_allocator, i32 %1, float** null)
  %2 = bitcast float* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.23* %this, i32 %n, float** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.23*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca float**, align 4
  store %class.btAlignedAllocator.23* %this, %class.btAlignedAllocator.23** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store float** %hint, float*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.23*, %class.btAlignedAllocator.23** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to float*
  ret float* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.27* @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EEC2Ev(%class.btAlignedAllocator.27* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.27*, align 4
  store %class.btAlignedAllocator.27* %this, %class.btAlignedAllocator.27** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.27*, %class.btAlignedAllocator.27** %this.addr, align 4
  ret %class.btAlignedAllocator.27* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IiEE6resizeEiRKS0_(%class.btAlignedObjectArray.26* %this, i32 %newsize, %class.btAlignedObjectArray.14* nonnull align 4 dereferenceable(17) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btAlignedObjectArray.14* %fillData, %class.btAlignedObjectArray.14** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv(%class.btAlignedObjectArray.26* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  %5 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %5, i32 %6
  %call3 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.14* %arrayidx) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end16

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp4 = icmp sgt i32 %8, %9
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIS_IiEE7reserveEi(%class.btAlignedObjectArray.26* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i6, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %12 = load i32, i32* %i6, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp8 = icmp slt i32 %12, %13
  br i1 %cmp8, label %for.body9, label %for.end15

for.body9:                                        ; preds = %for.cond7
  %m_data10 = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  %14 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %m_data10, align 4
  %15 = load i32, i32* %i6, align 4
  %arrayidx11 = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %14, i32 %15
  %16 = bitcast %class.btAlignedObjectArray.14* %arrayidx11 to i8*
  %17 = bitcast i8* %16 to %class.btAlignedObjectArray.14*
  %18 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %fillData.addr, align 4
  %call12 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiEC2ERKS0_(%class.btAlignedObjectArray.14* %17, %class.btAlignedObjectArray.14* nonnull align 4 dereferenceable(17) %18)
  br label %for.inc13

for.inc13:                                        ; preds = %for.body9
  %19 = load i32, i32* %i6, align 4
  %inc14 = add nsw i32 %19, 1
  store i32 %inc14, i32* %i6, align 4
  br label %for.cond7

for.end15:                                        ; preds = %for.cond7
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %20 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 2
  store i32 %20, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIS_IiEE4copyEiiPS0_(%class.btAlignedObjectArray.26* %this, i32 %start, i32 %end, %class.btAlignedObjectArray.14* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btAlignedObjectArray.14* %dest, %class.btAlignedObjectArray.14** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %3, i32 %4
  %5 = bitcast %class.btAlignedObjectArray.14* %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btAlignedObjectArray.14*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  %7 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %7, i32 %8
  %call = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiEC2ERKS0_(%class.btAlignedObjectArray.14* %6, %class.btAlignedObjectArray.14* nonnull align 4 dereferenceable(17) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IiEE7reserveEi(%class.btAlignedObjectArray.26* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIS_IiEE8capacityEv(%class.btAlignedObjectArray.26* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIS_IiEE8allocateEi(%class.btAlignedObjectArray.26* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btAlignedObjectArray.14*
  store %class.btAlignedObjectArray.14* %2, %class.btAlignedObjectArray.14** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv(%class.btAlignedObjectArray.26* %this1)
  %3 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIS_IiEE4copyEiiPS0_(%class.btAlignedObjectArray.26* %this1, i32 0, i32 %call3, %class.btAlignedObjectArray.14* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv(%class.btAlignedObjectArray.26* %this1)
  call void @_ZN20btAlignedObjectArrayIS_IiEE7destroyEii(%class.btAlignedObjectArray.26* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIS_IiEE10deallocateEv(%class.btAlignedObjectArray.26* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  store %class.btAlignedObjectArray.14* %4, %class.btAlignedObjectArray.14** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIS_IiEE8capacityEv(%class.btAlignedObjectArray.26* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIS_IiEE8allocateEi(%class.btAlignedObjectArray.26* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btAlignedObjectArray.14* @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.27* %m_allocator, i32 %1, %class.btAlignedObjectArray.14** null)
  %2 = bitcast %class.btAlignedObjectArray.14* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.14* @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.27* %this, i32 %n, %class.btAlignedObjectArray.14** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.27*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btAlignedObjectArray.14**, align 4
  store %class.btAlignedAllocator.27* %this, %class.btAlignedAllocator.27** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btAlignedObjectArray.14** %hint, %class.btAlignedObjectArray.14*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.27*, %class.btAlignedAllocator.27** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 20, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btAlignedObjectArray.14*
  ret %class.btAlignedObjectArray.14* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfEaSERKS0_(%class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22* nonnull align 4 dereferenceable(17) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %other.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store %class.btAlignedObjectArray.22* %other, %class.btAlignedObjectArray.22** %other.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %other.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE13copyFromArrayERKS0_(%class.btAlignedObjectArray.22* %this1, %class.btAlignedObjectArray.22* nonnull align 4 dereferenceable(17) %0)
  ret %class.btAlignedObjectArray.22* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.26* @_ZN20btAlignedObjectArrayIS_IiEEaSERKS1_(%class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26* nonnull align 4 dereferenceable(17) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  %other.addr = alloca %class.btAlignedObjectArray.26*, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4
  store %class.btAlignedObjectArray.26* %other, %class.btAlignedObjectArray.26** %other.addr, align 4
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %other.addr, align 4
  call void @_ZN20btAlignedObjectArrayIS_IiEE13copyFromArrayERKS1_(%class.btAlignedObjectArray.26* %this1, %class.btAlignedObjectArray.26* nonnull align 4 dereferenceable(17) %0)
  ret %class.btAlignedObjectArray.26* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE13copyFromArrayERKS0_(%class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22* nonnull align 4 dereferenceable(17) %otherArray) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %otherArray.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %otherSize = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store %class.btAlignedObjectArray.22* %otherArray, %class.btAlignedObjectArray.22** %otherArray.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %otherArray.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.22* %0)
  store i32 %call, i32* %otherSize, align 4
  %1 = load i32, i32* %otherSize, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.22* %this1, i32 %1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %2 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %otherArray.addr, align 4
  %3 = load i32, i32* %otherSize, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %4 = load float*, float** %m_data, align 4
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.22* %2, i32 0, i32 %3, float* %4)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IiEE13copyFromArrayERKS1_(%class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26* nonnull align 4 dereferenceable(17) %otherArray) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  %otherArray.addr = alloca %class.btAlignedObjectArray.26*, align 4
  %otherSize = alloca i32, align 4
  %ref.tmp = alloca %class.btAlignedObjectArray.14, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4
  store %class.btAlignedObjectArray.26* %otherArray, %class.btAlignedObjectArray.26** %otherArray.addr, align 4
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %otherArray.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv(%class.btAlignedObjectArray.26* %0)
  store i32 %call, i32* %otherSize, align 4
  %1 = load i32, i32* %otherSize, align 4
  %call2 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.14* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayIS_IiEE6resizeEiRKS0_(%class.btAlignedObjectArray.26* %this1, i32 %1, %class.btAlignedObjectArray.14* nonnull align 4 dereferenceable(17) %ref.tmp)
  %call3 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.14* %ref.tmp) #8
  %2 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %otherArray.addr, align 4
  %3 = load i32, i32* %otherSize, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  %4 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %m_data, align 4
  call void @_ZNK20btAlignedObjectArrayIS_IiEE4copyEiiPS0_(%class.btAlignedObjectArray.26* %2, i32 0, i32 %3, %class.btAlignedObjectArray.14* %4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.22* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.23* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.23* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.22* %this1)
  ret %class.btAlignedObjectArray.22* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.26* @_ZN20btAlignedObjectArrayIS_IiEEC2Ev(%class.btAlignedObjectArray.26* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.27* @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EEC2Ev(%class.btAlignedAllocator.27* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIS_IiEE4initEv(%class.btAlignedObjectArray.26* %this1)
  ret %class.btAlignedObjectArray.26* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.15* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.15* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.15*, align 4
  store %class.btAlignedAllocator.15* %this, %class.btAlignedAllocator.15** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.15*, %class.btAlignedAllocator.15** %this.addr, align 4
  ret %class.btAlignedAllocator.15* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.14* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.31* @_ZN18btAlignedAllocatorIP18btSolverConstraintLj16EEC2Ev(%class.btAlignedAllocator.31* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.31*, align 4
  store %class.btAlignedAllocator.31* %this, %class.btAlignedAllocator.31** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.31*, %class.btAlignedAllocator.31** %this.addr, align 4
  ret %class.btAlignedAllocator.31* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE4initEv(%class.btAlignedObjectArray.30* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  store %struct.btSolverConstraint** null, %struct.btSolverConstraint*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.14* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.14* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.14* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.14* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.14* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.14* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.14* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.14* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.15* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.15* %this, i32* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.15*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.15* %this, %class.btAlignedAllocator.15** %this.addr, align 4
  store i32* %ptr, i32** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.15*, %class.btAlignedAllocator.15** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE5clearEv(%class.btAlignedObjectArray.30* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.30* %this1)
  call void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE7destroyEii(%class.btAlignedObjectArray.30* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE10deallocateEv(%class.btAlignedObjectArray.30* %this1)
  call void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE4initEv(%class.btAlignedObjectArray.30* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE7destroyEii(%class.btAlignedObjectArray.30* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  %3 = load %struct.btSolverConstraint**, %struct.btSolverConstraint*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverConstraint*, %struct.btSolverConstraint** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE10deallocateEv(%class.btAlignedObjectArray.30* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  %0 = load %struct.btSolverConstraint**, %struct.btSolverConstraint*** %m_data, align 4
  %tobool = icmp ne %struct.btSolverConstraint** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  %2 = load %struct.btSolverConstraint**, %struct.btSolverConstraint*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP18btSolverConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.31* %m_allocator, %struct.btSolverConstraint** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  store %struct.btSolverConstraint** null, %struct.btSolverConstraint*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP18btSolverConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.31* %this, %struct.btSolverConstraint** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.31*, align 4
  %ptr.addr = alloca %struct.btSolverConstraint**, align 4
  store %class.btAlignedAllocator.31* %this, %class.btAlignedAllocator.31** %this.addr, align 4
  store %struct.btSolverConstraint** %ptr, %struct.btSolverConstraint*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.31*, %class.btAlignedAllocator.31** %this.addr, align 4
  %0 = load %struct.btSolverConstraint**, %struct.btSolverConstraint*** %ptr.addr, align 4
  %1 = bitcast %struct.btSolverConstraint** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE7reserveEi(%class.btAlignedObjectArray.30* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btSolverConstraint**, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE8capacityEv(%class.btAlignedObjectArray.30* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP18btSolverConstraintE8allocateEi(%class.btAlignedObjectArray.30* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btSolverConstraint**
  store %struct.btSolverConstraint** %2, %struct.btSolverConstraint*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.30* %this1)
  %3 = load %struct.btSolverConstraint**, %struct.btSolverConstraint*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4copyEiiPS1_(%class.btAlignedObjectArray.30* %this1, i32 0, i32 %call3, %struct.btSolverConstraint** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.30* %this1)
  call void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE7destroyEii(%class.btAlignedObjectArray.30* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP18btSolverConstraintE10deallocateEv(%class.btAlignedObjectArray.30* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btSolverConstraint**, %struct.btSolverConstraint*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  store %struct.btSolverConstraint** %4, %struct.btSolverConstraint*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE8capacityEv(%class.btAlignedObjectArray.30* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP18btSolverConstraintE8allocateEi(%class.btAlignedObjectArray.30* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btSolverConstraint** @_ZN18btAlignedAllocatorIP18btSolverConstraintLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.31* %m_allocator, i32 %1, %struct.btSolverConstraint*** null)
  %2 = bitcast %struct.btSolverConstraint** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP18btSolverConstraintE4copyEiiPS1_(%class.btAlignedObjectArray.30* %this, i32 %start, i32 %end, %struct.btSolverConstraint** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btSolverConstraint**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btSolverConstraint** %dest, %struct.btSolverConstraint*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btSolverConstraint**, %struct.btSolverConstraint*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverConstraint*, %struct.btSolverConstraint** %3, i32 %4
  %5 = bitcast %struct.btSolverConstraint** %arrayidx to i8*
  %6 = bitcast i8* %5 to %struct.btSolverConstraint**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  %7 = load %struct.btSolverConstraint**, %struct.btSolverConstraint*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btSolverConstraint*, %struct.btSolverConstraint** %7, i32 %8
  %9 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %arrayidx2, align 4
  store %struct.btSolverConstraint* %9, %struct.btSolverConstraint** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btSolverConstraint** @_ZN18btAlignedAllocatorIP18btSolverConstraintLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.31* %this, i32 %n, %struct.btSolverConstraint*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.31*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btSolverConstraint***, align 4
  store %class.btAlignedAllocator.31* %this, %class.btAlignedAllocator.31** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btSolverConstraint*** %hint, %struct.btSolverConstraint**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.31*, %class.btAlignedAllocator.31** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btSolverConstraint**
  ret %struct.btSolverConstraint** %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.14* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.14* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.14* %this1, i32 %1)
  %2 = bitcast i8* %call2 to i32*
  store i32* %2, i32** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.14* %this1)
  %3 = load i32*, i32** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.14* %this1, i32 0, i32 %call3, i32* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.14* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.14* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.14* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load i32*, i32** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  store i32* %4, i32** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.14* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.14* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.15* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.14* %this, i32 %start, i32 %end, i32* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store i32* %dest, i32** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = bitcast i32* %arrayidx to i8*
  %6 = bitcast i8* %5 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %7 = load i32*, i32** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %7, i32 %8
  %9 = load i32, i32* %arrayidx2, align 4
  store i32 %9, i32* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.15* %this, i32 %n, i32** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.15*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.15* %this, %class.btAlignedAllocator.15** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32** %hint, i32*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.15*, %class.btAlignedAllocator.15** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP18btSolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.30* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z9btSetZeroIfEvPT_i(float* %a, i32 %n) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %acurr = alloca float*, align 4
  %ncurr = alloca i32, align 4
  store float* %a, float** %a.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  store float* %0, float** %acurr, align 4
  %1 = load i32, i32* %n.addr, align 4
  store i32 %1, i32* %ncurr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %2 = load i32, i32* %ncurr, align 4
  %cmp = icmp ugt i32 %2, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %3 = load float*, float** %acurr, align 4
  %incdec.ptr = getelementptr inbounds float, float* %3, i32 1
  store float* %incdec.ptr, float** %acurr, align 4
  store float 0.000000e+00, float* %3, align 4
  %4 = load i32, i32* %ncurr, align 4
  %dec = add i32 %4, -1
  store i32 %dec, i32* %ncurr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.35* @_ZN18btAlignedAllocatorI11btJointNodeLj16EEC2Ev(%class.btAlignedAllocator.35* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.35*, align 4
  store %class.btAlignedAllocator.35* %this, %class.btAlignedAllocator.35** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.35*, %class.btAlignedAllocator.35** %this.addr, align 4
  ret %class.btAlignedAllocator.35* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btJointNodeE4initEv(%class.btAlignedObjectArray.34* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  store %struct.btJointNode* null, %struct.btJointNode** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btJointNodeE5clearEv(%class.btAlignedObjectArray.34* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE4sizeEv(%class.btAlignedObjectArray.34* %this1)
  call void @_ZN20btAlignedObjectArrayI11btJointNodeE7destroyEii(%class.btAlignedObjectArray.34* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI11btJointNodeE10deallocateEv(%class.btAlignedObjectArray.34* %this1)
  call void @_ZN20btAlignedObjectArrayI11btJointNodeE4initEv(%class.btAlignedObjectArray.34* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btJointNodeE7destroyEii(%class.btAlignedObjectArray.34* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  %3 = load %struct.btJointNode*, %struct.btJointNode** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btJointNodeE10deallocateEv(%class.btAlignedObjectArray.34* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  %0 = load %struct.btJointNode*, %struct.btJointNode** %m_data, align 4
  %tobool = icmp ne %struct.btJointNode* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  %2 = load %struct.btJointNode*, %struct.btJointNode** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI11btJointNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator.35* %m_allocator, %struct.btJointNode* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  store %struct.btJointNode* null, %struct.btJointNode** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI11btJointNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator.35* %this, %struct.btJointNode* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.35*, align 4
  %ptr.addr = alloca %struct.btJointNode*, align 4
  store %class.btAlignedAllocator.35* %this, %class.btAlignedAllocator.35** %this.addr, align 4
  store %struct.btJointNode* %ptr, %struct.btJointNode** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.35*, %class.btAlignedAllocator.35** %this.addr, align 4
  %0 = load %struct.btJointNode*, %struct.btJointNode** %ptr.addr, align 4
  %1 = bitcast %struct.btJointNode* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11btJointNodeE8capacityEv(%class.btAlignedObjectArray.34* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI11btJointNodeE8allocateEi(%class.btAlignedObjectArray.34* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btJointNode* @_ZN18btAlignedAllocatorI11btJointNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.35* %m_allocator, i32 %1, %struct.btJointNode** null)
  %2 = bitcast %struct.btJointNode* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI11btJointNodeE4copyEiiPS0_(%class.btAlignedObjectArray.34* %this, i32 %start, i32 %end, %struct.btJointNode* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btJointNode*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btJointNode* %dest, %struct.btJointNode** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btJointNode*, %struct.btJointNode** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %3, i32 %4
  %5 = bitcast %struct.btJointNode* %arrayidx to i8*
  %6 = bitcast i8* %5 to %struct.btJointNode*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  %7 = load %struct.btJointNode*, %struct.btJointNode** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btJointNode, %struct.btJointNode* %7, i32 %8
  %9 = bitcast %struct.btJointNode* %6 to i8*
  %10 = bitcast %struct.btJointNode* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btJointNode* @_ZN18btAlignedAllocatorI11btJointNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.35* %this, i32 %n, %struct.btJointNode** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.35*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btJointNode**, align 4
  store %class.btAlignedAllocator.35* %this, %class.btAlignedAllocator.35** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btJointNode** %hint, %struct.btJointNode*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.35*, %class.btAlignedAllocator.35** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btJointNode*
  ret %struct.btJointNode* %1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI11btJointNodeE9allocSizeEi(%class.btAlignedObjectArray.34* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btMatrixXIfE7addElemEiif(%struct.btMatrixX* %this, i32 %row, i32 %col, float %val) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %row.addr = alloca i32, align 4
  %col.addr = alloca i32, align 4
  %val.addr = alloca float, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  store i32 %row, i32* %row.addr, align 4
  store i32 %col, i32* %col.addr, align 4
  store float %val, float* %val.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %0 = load float, float* %val.addr, align 4
  %tobool = fcmp une float %0, 0.000000e+00
  br i1 %tobool, label %if.then, label %if.end9

if.then:                                          ; preds = %entry
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %1 = load i32, i32* %col.addr, align 4
  %2 = load i32, i32* %row.addr, align 4
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %3 = load i32, i32* %m_cols, align 4
  %mul = mul nsw i32 %2, %3
  %add = add nsw i32 %1, %mul
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_storage, i32 %add)
  %4 = load float, float* %call, align 4
  %cmp = fcmp oeq float %4, 0.000000e+00
  br i1 %cmp, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %5 = load i32, i32* %row.addr, align 4
  %6 = load i32, i32* %col.addr, align 4
  %7 = load float, float* %val.addr, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %this1, i32 %5, i32 %6, float %7)
  br label %if.end

if.else:                                          ; preds = %if.then
  %8 = load float, float* %val.addr, align 4
  %m_storage3 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %9 = load i32, i32* %row.addr, align 4
  %m_cols4 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %10 = load i32, i32* %m_cols4, align 4
  %mul5 = mul nsw i32 %9, %10
  %11 = load i32, i32* %col.addr, align 4
  %add6 = add nsw i32 %mul5, %11
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_storage3, i32 %add6)
  %12 = load float, float* %call7, align 4
  %add8 = fadd float %12, %8
  store float %add8, float* %call7, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then2
  br label %if.end9

if.end9:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btMatrixX* @_ZN9btMatrixXIfEC2Eii(%struct.btMatrixX* returned %this, i32 %rows, i32 %cols) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %rows.addr = alloca i32, align 4
  %cols.addr = alloca i32, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  store i32 %rows, i32* %rows.addr, align 4
  store i32 %cols, i32* %cols.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_rows = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  %0 = load i32, i32* %rows.addr, align 4
  store i32 %0, i32* %m_rows, align 4
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %1 = load i32, i32* %cols.addr, align 4
  store i32 %1, i32* %m_cols, align 4
  %m_operations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 2
  store i32 0, i32* %m_operations, align 4
  %m_resizeOperations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 3
  store i32 0, i32* %m_resizeOperations, align 4
  %m_setElemOperations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 4
  store i32 0, i32* %m_setElemOperations, align 4
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %call = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.22* %m_storage)
  %m_rowNonZeroElements1 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %call2 = call %class.btAlignedObjectArray.26* @_ZN20btAlignedObjectArrayIS_IiEEC2Ev(%class.btAlignedObjectArray.26* %m_rowNonZeroElements1)
  %2 = load i32, i32* %rows.addr, align 4
  %3 = load i32, i32* %cols.addr, align 4
  call void @_ZN9btMatrixXIfE6resizeEii(%struct.btMatrixX* %this1, i32 %2, i32 %3)
  ret %struct.btMatrixX* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btMatrixXIfE4colsEv(%struct.btMatrixX* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_cols, align 4
  ret i32 %0
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btMLCPSolver.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn writeonly }
attributes #6 = { argmemonly nounwind willreturn }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
