; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Featherstone/btMultiBodyConstraintSolver.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Featherstone/btMultiBodyConstraintSolver.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btMultiBodyConstraintSolver = type { %class.btSequentialImpulseConstraintSolver, %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22, %struct.btMultiBodyJacobianData, %class.btMultiBodyConstraint**, i32 }
%class.btSequentialImpulseConstraintSolver = type { %class.btConstraintSolver, %class.btAlignedObjectArray, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.18, i32, i32, %class.btAlignedObjectArray.14, float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)*, float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)*, float, i32 }
%class.btConstraintSolver = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btSolverBody*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.3, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray.0, i32, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.3 = type <{ %class.btAlignedAllocator.4, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.4 = type { i8 }
%class.btAlignedObjectArray.9 = type <{ %class.btAlignedAllocator.10, [3 x i8], i32, i32, %struct.btSolverConstraint*, i8, [3 x i8] }>
%class.btAlignedAllocator.10 = type { i8 }
%struct.btSolverConstraint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.12, i32, i32, i32, i32 }
%union.anon.12 = type { i8* }
%class.btAlignedObjectArray.18 = type <{ %class.btAlignedAllocator.19, [3 x i8], i32, i32, %"struct.btTypedConstraint::btConstraintInfo1"*, i8, [3 x i8] }>
%class.btAlignedAllocator.19 = type { i8 }
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%class.btAlignedObjectArray.14 = type <{ %class.btAlignedAllocator.15, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.15 = type { i8 }
%class.btAlignedObjectArray.22 = type <{ %class.btAlignedAllocator.23, [3 x i8], i32, i32, %struct.btMultiBodySolverConstraint*, i8, [3 x i8] }>
%class.btAlignedAllocator.23 = type { i8 }
%struct.btMultiBodySolverConstraint = type { i32, i32, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.25, i32, i32, i32, %class.btMultiBody*, i32, i32, %class.btMultiBody*, i32, %class.btMultiBodyConstraint*, i32 }
%union.anon.25 = type { i8* }
%class.btMultiBody = type <{ i32 (...)**, %class.btMultiBodyLinkCollider*, i8*, %class.btVector3, %class.btQuaternion, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.42, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, i8, i8, i8, i8, float, i8*, i32, i32, i32, float, float, i8, [3 x i8], float, float, i8, i8, [2 x i8], i32, i32, i8, i8, i8, i8 }>
%class.btMultiBodyLinkCollider = type { %class.btCollisionObject, %class.btMultiBody*, i32 }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btAlignedObjectArray.26 = type <{ %class.btAlignedAllocator.27, [3 x i8], i32, i32, %struct.btMultibodyLink*, i8, [3 x i8] }>
%class.btAlignedAllocator.27 = type { i8 }
%struct.btMultibodyLink = type { float, %class.btVector3, i32, %class.btQuaternion, %class.btVector3, %class.btVector3, %struct.btSpatialMotionVector, %struct.btSpatialMotionVector, [6 x %struct.btSpatialMotionVector], i32, i32, %class.btQuaternion, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, [7 x float], [6 x float], %class.btMultiBodyLinkCollider*, i32, i32, i32, i32, %struct.btMultiBodyJointFeedback*, %class.btTransform, i8*, i8*, i8*, float, float }
%struct.btSpatialMotionVector = type { %class.btVector3, %class.btVector3 }
%struct.btMultiBodyJointFeedback = type opaque
%class.btAlignedObjectArray.30 = type <{ %class.btAlignedAllocator.31, [3 x i8], i32, i32, %class.btMultiBodyLinkCollider**, i8, [3 x i8] }>
%class.btAlignedAllocator.31 = type { i8 }
%class.btAlignedObjectArray.34 = type <{ %class.btAlignedAllocator.35, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.35 = type { i8 }
%class.btAlignedObjectArray.38 = type <{ %class.btAlignedAllocator.39, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.39 = type { i8 }
%class.btAlignedObjectArray.42 = type <{ %class.btAlignedAllocator.43, [3 x i8], i32, i32, %class.btMatrix3x3*, i8, [3 x i8] }>
%class.btAlignedAllocator.43 = type { i8 }
%class.btMultiBodyConstraint = type { i32 (...)**, %class.btMultiBody*, %class.btMultiBody*, i32, i32, i32, i32, i32, i32, i8, i32, float, %class.btAlignedObjectArray.34 }
%struct.btMultiBodyJacobianData = type { %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.42, %class.btAlignedObjectArray*, i32 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.47, %union.anon.48, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.47 = type { float }
%union.anon.48 = type { float }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.6, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%union.anon.6 = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float, float }
%class.btIDebugDraw = type opaque
%class.CProfileSample = type { i8 }
%class.btDispatcher = type opaque

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintEixEi = comdat any

$_ZN11btMultiBody13setPosUpdatedEb = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE6resizeEiRKS0_ = comdat any

$_ZN27btMultiBodySolverConstraintC2Ev = comdat any

$_ZN20btAlignedObjectArrayIfE6resizeEiRKf = comdat any

$_ZN23btMultiBodyLinkCollider6upcastEP17btCollisionObject = comdat any

$_ZN11btMultiBody14setCompanionIdEi = comdat any

$_ZN20btAlignedObjectArrayIfEixEi = comdat any

$_ZNK11btMultiBody10getNumDofsEv = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyEixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN12btSolverBody30internalGetDeltaLinearVelocityEv = comdat any

$_ZN12btSolverBody31internalGetDeltaAngularVelocityEv = comdat any

$_ZN11btMultiBody22applyDeltaVeeMultiDof2EPKff = comdat any

$_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZNK12btSolverBody18internalGetInvMassEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK15btManifoldPoint19getPositionWorldOnAEv = comdat any

$_ZNK15btManifoldPoint19getPositionWorldOnBEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK12btSolverBody17getWorldTransformEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK11btMultiBody10getBasePosEv = comdat any

$_ZN11btMultiBody7getLinkEi = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZNK11btMultiBody14getCompanionIdEv = comdat any

$_ZNK20btAlignedObjectArrayIfE4sizeEv = comdat any

$_ZNK11btMultiBody27fillContactJacobianMultiDofEiRK9btVector3S2_PfR20btAlignedObjectArrayIfERS4_IS0_ERS4_I11btMatrix3x3E = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btRigidBody24getInvInertiaTensorWorldEv = comdat any

$_ZNK11btRigidBody16getAngularFactorEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK11btRigidBody10getInvMassEv = comdat any

$_ZNK15btManifoldPoint11getDistanceEv = comdat any

$_ZNK11btMultiBody17getVelocityVectorEv = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3 = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZNK11btRigidBody14getTotalTorqueEv = comdat any

$_ZNK11btRigidBody13getTotalForceEv = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv = comdat any

$_ZN23btMultiBodyLinkCollider6upcastEPK17btCollisionObject = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK20btPersistentManifold8getBody1Ev = comdat any

$_ZNK20btPersistentManifold14getNumContactsEv = comdat any

$_ZN20btPersistentManifold15getContactPointEi = comdat any

$_ZNK20btPersistentManifold29getContactProcessingThresholdEv = comdat any

$_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_ = comdat any

$_ZN21btMultiBodyConstraint25internalSetAppliedImpulseEif = comdat any

$_ZN11btMultiBody22addBaseConstraintForceERK9btVector3 = comdat any

$_ZN11btMultiBody23addBaseConstraintTorqueERK9btVector3 = comdat any

$_ZN27btMultiBodyConstraintSolverD2Ev = comdat any

$_ZN27btMultiBodyConstraintSolverD0Ev = comdat any

$_ZN18btConstraintSolver12prepareSolveEii = comdat any

$_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDraw = comdat any

$_ZNK35btSequentialImpulseConstraintSolver13getSolverTypeEv = comdat any

$_ZNK17btCollisionObject15getInternalTypeEv = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK20btAlignedObjectArrayIfEixEi = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN23btMultiBodyJacobianDataD2Ev = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintED2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3ED2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZN20btAlignedObjectArrayIfED2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E4initEv = comdat any

$_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIfE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIfE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIfE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIfE4initEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE4initEv = comdat any

$_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_ = comdat any

$_ZN27btMultiBodyConstraintSolverdlEPv = comdat any

$_Z6btFabsf = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi = comdat any

$_ZN27btMultiBodySolverConstraintnwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayIfE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIfE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIfE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIfE4copyEiiPf = comdat any

$_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@.str = private unnamed_addr constant [32 x i8] c"setupMultiBodyContactConstraint\00", align 1
@.str.1 = private unnamed_addr constant [40 x i8] c"setupMultiBodyRollingFrictionConstraint\00", align 1
@.str.2 = private unnamed_addr constant [31 x i8] c"addMultiBodyFrictionConstraint\00", align 1
@.str.3 = private unnamed_addr constant [38 x i8] c"addMultiBodyRollingFrictionConstraint\00", align 1
@.str.4 = private unnamed_addr constant [59 x i8] c"btMultiBodyConstraintSolver::solveGroupCacheFriendlyFinish\00", align 1
@.str.5 = private unnamed_addr constant [25 x i8] c"warm starting write back\00", align 1
@_ZTV27btMultiBodyConstraintSolver = hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI27btMultiBodyConstraintSolver to i8*), i8* bitcast (%class.btMultiBodyConstraintSolver* (%class.btMultiBodyConstraintSolver*)* @_ZN27btMultiBodyConstraintSolverD2Ev to i8*), i8* bitcast (void (%class.btMultiBodyConstraintSolver*)* @_ZN27btMultiBodyConstraintSolverD0Ev to i8*), i8* bitcast (void (%class.btConstraintSolver*, i32, i32)* @_ZN18btConstraintSolver12prepareSolveEii to i8*), i8* bitcast (float (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)* @_ZN27btMultiBodyConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher to i8*), i8* bitcast (void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (void (%class.btSequentialImpulseConstraintSolver*)* @_ZN35btSequentialImpulseConstraintSolver5resetEv to i8*), i8* bitcast (i32 (%class.btSequentialImpulseConstraintSolver*)* @_ZNK35btSequentialImpulseConstraintSolver13getSolverTypeEv to i8*), i8* bitcast (void (%class.btMultiBodyConstraintSolver*, %class.btPersistentManifold**, i32, %struct.btContactSolverInfo*)* @_ZN27btMultiBodyConstraintSolver15convertContactsEPP20btPersistentManifoldiRK19btContactSolverInfo to i8*), i8* bitcast (void (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN35btSequentialImpulseConstraintSolver45solveGroupCacheFriendlySplitImpulseIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (float (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %struct.btContactSolverInfo*)* @_ZN27btMultiBodyConstraintSolver29solveGroupCacheFriendlyFinishEPP17btCollisionObjectiRK19btContactSolverInfo to i8*), i8* bitcast (float (%class.btMultiBodyConstraintSolver*, i32, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN27btMultiBodyConstraintSolver20solveSingleIterationEiPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (float (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN27btMultiBodyConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN35btSequentialImpulseConstraintSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)* @_ZN27btMultiBodyConstraintSolver19solveMultiBodyGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiPP21btMultiBodyConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS27btMultiBodyConstraintSolver = hidden constant [30 x i8] c"27btMultiBodyConstraintSolver\00", align 1
@_ZTI35btSequentialImpulseConstraintSolver = external constant i8*
@_ZTI27btMultiBodyConstraintSolver = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([30 x i8], [30 x i8]* @_ZTS27btMultiBodyConstraintSolver, i32 0, i32 0), i8* bitcast (i8** @_ZTI35btSequentialImpulseConstraintSolver to i8*) }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btMultiBodyConstraintSolver.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden float @_ZN27btMultiBodyConstraintSolver20solveSingleIterationEiPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btMultiBodyConstraintSolver* %this, i32 %iteration, %class.btCollisionObject** %bodies, i32 %numBodies, %class.btPersistentManifold** %manifoldPtr, i32 %numManifolds, %class.btTypedConstraint** %constraints, i32 %numConstraints, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal, %class.btIDebugDraw* %debugDrawer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %iteration.addr = alloca i32, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %leastSquaredResidual = alloca float, align 4
  %j = alloca i32, align 4
  %index = alloca i32, align 4
  %constraint = alloca %struct.btMultiBodySolverConstraint*, align 4
  %residual = alloca float, align 4
  %j15 = alloca i32, align 4
  %constraint20 = alloca %struct.btMultiBodySolverConstraint*, align 4
  %residual23 = alloca float, align 4
  %j43 = alloca i32, align 4
  %frictionConstraint = alloca %struct.btMultiBodySolverConstraint*, align 4
  %totalImpulse = alloca float, align 4
  %residual60 = alloca float, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  store i32 %iteration, i32* %iteration.addr, align 4
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4
  store i32 %numBodies, i32* %numBodies.addr, align 4
  store %class.btPersistentManifold** %manifoldPtr, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  store i32 %numManifolds, i32* %numManifolds.addr, align 4
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4
  store i32 %numConstraints, i32* %numConstraints.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %1 = load i32, i32* %iteration.addr, align 4
  %2 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %3 = load i32, i32* %numBodies.addr, align 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  %5 = load i32, i32* %numManifolds.addr, align 4
  %6 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %7 = load i32, i32* %numConstraints.addr, align 4
  %8 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %9 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %call = call float @_ZN35btSequentialImpulseConstraintSolver20solveSingleIterationEiPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver* %0, i32 %1, %class.btCollisionObject** %2, i32 %3, %class.btPersistentManifold** %4, i32 %5, %class.btTypedConstraint** %6, i32 %7, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %8, %class.btIDebugDraw* %9)
  store float %call, float* %leastSquaredResidual, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %10 = load i32, i32* %j, align 4
  %m_multiBodyNonContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.22* %m_multiBodyNonContactConstraints)
  %cmp = icmp slt i32 %10, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load i32, i32* %iteration.addr, align 4
  %and = and i32 %11, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %12 = load i32, i32* %j, align 4
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %m_multiBodyNonContactConstraints3 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 1
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.22* %m_multiBodyNonContactConstraints3)
  %sub = sub nsw i32 %call4, 1
  %13 = load i32, i32* %j, align 4
  %sub5 = sub nsw i32 %sub, %13
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %12, %cond.true ], [ %sub5, %cond.false ]
  store i32 %cond, i32* %index, align 4
  %m_multiBodyNonContactConstraints6 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 1
  %14 = load i32, i32* %index, align 4
  %call7 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintEixEi(%class.btAlignedObjectArray.22* %m_multiBodyNonContactConstraints6, i32 %14)
  store %struct.btMultiBodySolverConstraint* %call7, %struct.btMultiBodySolverConstraint** %constraint, align 4
  %15 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraint, align 4
  %call8 = call float @_ZN27btMultiBodyConstraintSolver33resolveSingleConstraintRowGenericERK27btMultiBodySolverConstraint(%class.btMultiBodyConstraintSolver* %this1, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %15)
  store float %call8, float* %residual, align 4
  %16 = load float, float* %residual, align 4
  %17 = load float, float* %residual, align 4
  %mul = fmul float %16, %17
  %18 = load float, float* %leastSquaredResidual, align 4
  %add = fadd float %18, %mul
  store float %add, float* %leastSquaredResidual, align 4
  %19 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraint, align 4
  %m_multiBodyA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %19, i32 0, i32 23
  %20 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA, align 4
  %tobool9 = icmp ne %class.btMultiBody* %20, null
  br i1 %tobool9, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %21 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraint, align 4
  %m_multiBodyA10 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %21, i32 0, i32 23
  %22 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA10, align 4
  call void @_ZN11btMultiBody13setPosUpdatedEb(%class.btMultiBody* %22, i1 zeroext false)
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end
  %23 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraint, align 4
  %m_multiBodyB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %23, i32 0, i32 26
  %24 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB, align 4
  %tobool11 = icmp ne %class.btMultiBody* %24, null
  br i1 %tobool11, label %if.then12, label %if.end14

if.then12:                                        ; preds = %if.end
  %25 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraint, align 4
  %m_multiBodyB13 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %25, i32 0, i32 26
  %26 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB13, align 4
  call void @_ZN11btMultiBody13setPosUpdatedEb(%class.btMultiBody* %26, i1 zeroext false)
  br label %if.end14

if.end14:                                         ; preds = %if.then12, %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end14
  %27 = load i32, i32* %j, align 4
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %j15, align 4
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc40, %for.end
  %28 = load i32, i32* %j15, align 4
  %m_multiBodyNormalContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 2
  %call17 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.22* %m_multiBodyNormalContactConstraints)
  %cmp18 = icmp slt i32 %28, %call17
  br i1 %cmp18, label %for.body19, label %for.end42

for.body19:                                       ; preds = %for.cond16
  %m_multiBodyNormalContactConstraints21 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 2
  %29 = load i32, i32* %j15, align 4
  %call22 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintEixEi(%class.btAlignedObjectArray.22* %m_multiBodyNormalContactConstraints21, i32 %29)
  store %struct.btMultiBodySolverConstraint* %call22, %struct.btMultiBodySolverConstraint** %constraint20, align 4
  store float 0.000000e+00, float* %residual23, align 4
  %30 = load i32, i32* %iteration.addr, align 4
  %31 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %32 = bitcast %struct.btContactSolverInfo* %31 to %struct.btContactSolverInfoData*
  %m_numIterations = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %32, i32 0, i32 5
  %33 = load i32, i32* %m_numIterations, align 4
  %cmp24 = icmp slt i32 %30, %33
  br i1 %cmp24, label %if.then25, label %if.end27

if.then25:                                        ; preds = %for.body19
  %34 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraint20, align 4
  %call26 = call float @_ZN27btMultiBodyConstraintSolver33resolveSingleConstraintRowGenericERK27btMultiBodySolverConstraint(%class.btMultiBodyConstraintSolver* %this1, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %34)
  store float %call26, float* %residual23, align 4
  br label %if.end27

if.end27:                                         ; preds = %if.then25, %for.body19
  %35 = load float, float* %residual23, align 4
  %36 = load float, float* %residual23, align 4
  %mul28 = fmul float %35, %36
  %37 = load float, float* %leastSquaredResidual, align 4
  %add29 = fadd float %37, %mul28
  store float %add29, float* %leastSquaredResidual, align 4
  %38 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraint20, align 4
  %m_multiBodyA30 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %38, i32 0, i32 23
  %39 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA30, align 4
  %tobool31 = icmp ne %class.btMultiBody* %39, null
  br i1 %tobool31, label %if.then32, label %if.end34

if.then32:                                        ; preds = %if.end27
  %40 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraint20, align 4
  %m_multiBodyA33 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %40, i32 0, i32 23
  %41 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA33, align 4
  call void @_ZN11btMultiBody13setPosUpdatedEb(%class.btMultiBody* %41, i1 zeroext false)
  br label %if.end34

if.end34:                                         ; preds = %if.then32, %if.end27
  %42 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraint20, align 4
  %m_multiBodyB35 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %42, i32 0, i32 26
  %43 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB35, align 4
  %tobool36 = icmp ne %class.btMultiBody* %43, null
  br i1 %tobool36, label %if.then37, label %if.end39

if.then37:                                        ; preds = %if.end34
  %44 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraint20, align 4
  %m_multiBodyB38 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %44, i32 0, i32 26
  %45 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB38, align 4
  call void @_ZN11btMultiBody13setPosUpdatedEb(%class.btMultiBody* %45, i1 zeroext false)
  br label %if.end39

if.end39:                                         ; preds = %if.then37, %if.end34
  br label %for.inc40

for.inc40:                                        ; preds = %if.end39
  %46 = load i32, i32* %j15, align 4
  %inc41 = add nsw i32 %46, 1
  store i32 %inc41, i32* %j15, align 4
  br label %for.cond16

for.end42:                                        ; preds = %for.cond16
  store i32 0, i32* %j43, align 4
  br label %for.cond44

for.cond44:                                       ; preds = %for.inc76, %for.end42
  %47 = load i32, i32* %j43, align 4
  %m_multiBodyFrictionContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 3
  %call45 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.22* %m_multiBodyFrictionContactConstraints)
  %cmp46 = icmp slt i32 %47, %call45
  br i1 %cmp46, label %for.body47, label %for.end78

for.body47:                                       ; preds = %for.cond44
  %48 = load i32, i32* %iteration.addr, align 4
  %49 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %50 = bitcast %struct.btContactSolverInfo* %49 to %struct.btContactSolverInfoData*
  %m_numIterations48 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %50, i32 0, i32 5
  %51 = load i32, i32* %m_numIterations48, align 4
  %cmp49 = icmp slt i32 %48, %51
  br i1 %cmp49, label %if.then50, label %if.end75

if.then50:                                        ; preds = %for.body47
  %m_multiBodyFrictionContactConstraints51 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 3
  %52 = load i32, i32* %j43, align 4
  %call52 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintEixEi(%class.btAlignedObjectArray.22* %m_multiBodyFrictionContactConstraints51, i32 %52)
  store %struct.btMultiBodySolverConstraint* %call52, %struct.btMultiBodySolverConstraint** %frictionConstraint, align 4
  %m_multiBodyNormalContactConstraints53 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 2
  %53 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %frictionConstraint, align 4
  %m_frictionIndex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %53, i32 0, i32 21
  %54 = load i32, i32* %m_frictionIndex, align 4
  %call54 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintEixEi(%class.btAlignedObjectArray.22* %m_multiBodyNormalContactConstraints53, i32 %54)
  %m_appliedImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %call54, i32 0, i32 11
  %55 = load float, float* %m_appliedImpulse, align 4
  store float %55, float* %totalImpulse, align 4
  %56 = load float, float* %totalImpulse, align 4
  %cmp55 = fcmp ogt float %56, 0.000000e+00
  br i1 %cmp55, label %if.then56, label %if.end74

if.then56:                                        ; preds = %if.then50
  %57 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %frictionConstraint, align 4
  %m_friction = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %57, i32 0, i32 12
  %58 = load float, float* %m_friction, align 4
  %59 = load float, float* %totalImpulse, align 4
  %mul57 = fmul float %58, %59
  %fneg = fneg float %mul57
  %60 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %frictionConstraint, align 4
  %m_lowerLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %60, i32 0, i32 16
  store float %fneg, float* %m_lowerLimit, align 4
  %61 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %frictionConstraint, align 4
  %m_friction58 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %61, i32 0, i32 12
  %62 = load float, float* %m_friction58, align 4
  %63 = load float, float* %totalImpulse, align 4
  %mul59 = fmul float %62, %63
  %64 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %frictionConstraint, align 4
  %m_upperLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %64, i32 0, i32 17
  store float %mul59, float* %m_upperLimit, align 4
  %65 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %frictionConstraint, align 4
  %call61 = call float @_ZN27btMultiBodyConstraintSolver33resolveSingleConstraintRowGenericERK27btMultiBodySolverConstraint(%class.btMultiBodyConstraintSolver* %this1, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %65)
  store float %call61, float* %residual60, align 4
  %66 = load float, float* %residual60, align 4
  %67 = load float, float* %residual60, align 4
  %mul62 = fmul float %66, %67
  %68 = load float, float* %leastSquaredResidual, align 4
  %add63 = fadd float %68, %mul62
  store float %add63, float* %leastSquaredResidual, align 4
  %69 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %frictionConstraint, align 4
  %m_multiBodyA64 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %69, i32 0, i32 23
  %70 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA64, align 4
  %tobool65 = icmp ne %class.btMultiBody* %70, null
  br i1 %tobool65, label %if.then66, label %if.end68

if.then66:                                        ; preds = %if.then56
  %71 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %frictionConstraint, align 4
  %m_multiBodyA67 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %71, i32 0, i32 23
  %72 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA67, align 4
  call void @_ZN11btMultiBody13setPosUpdatedEb(%class.btMultiBody* %72, i1 zeroext false)
  br label %if.end68

if.end68:                                         ; preds = %if.then66, %if.then56
  %73 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %frictionConstraint, align 4
  %m_multiBodyB69 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %73, i32 0, i32 26
  %74 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB69, align 4
  %tobool70 = icmp ne %class.btMultiBody* %74, null
  br i1 %tobool70, label %if.then71, label %if.end73

if.then71:                                        ; preds = %if.end68
  %75 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %frictionConstraint, align 4
  %m_multiBodyB72 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %75, i32 0, i32 26
  %76 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB72, align 4
  call void @_ZN11btMultiBody13setPosUpdatedEb(%class.btMultiBody* %76, i1 zeroext false)
  br label %if.end73

if.end73:                                         ; preds = %if.then71, %if.end68
  br label %if.end74

if.end74:                                         ; preds = %if.end73, %if.then50
  br label %if.end75

if.end75:                                         ; preds = %if.end74, %for.body47
  br label %for.inc76

for.inc76:                                        ; preds = %if.end75
  %77 = load i32, i32* %j43, align 4
  %inc77 = add nsw i32 %77, 1
  store i32 %inc77, i32* %j43, align 4
  br label %for.cond44

for.end78:                                        ; preds = %for.cond44
  %78 = load float, float* %leastSquaredResidual, align 4
  ret float %78
}

declare float @_ZN35btSequentialImpulseConstraintSolver20solveSingleIterationEiPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver*, i32, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88), %class.btIDebugDraw*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.22* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintEixEi(%class.btAlignedObjectArray.22* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %0, i32 %1
  ret %struct.btMultiBodySolverConstraint* %arrayidx
}

; Function Attrs: noinline optnone
define hidden float @_ZN27btMultiBodyConstraintSolver33resolveSingleConstraintRowGenericERK27btMultiBodySolverConstraint(%class.btMultiBodyConstraintSolver* %this, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %c) #2 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %c.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  %deltaImpulse = alloca float, align 4
  %deltaVelADotn = alloca float, align 4
  %deltaVelBDotn = alloca float, align 4
  %bodyA = alloca %struct.btSolverBody*, align 4
  %bodyB = alloca %struct.btSolverBody*, align 4
  %ndofA = alloca i32, align 4
  %ndofB = alloca i32, align 4
  %i = alloca i32, align 4
  %i26 = alloca i32, align 4
  %sum = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp120 = alloca %class.btVector3, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  store %struct.btMultiBodySolverConstraint* %c, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_rhs = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %0, i32 0, i32 14
  %1 = load float, float* %m_rhs, align 4
  %2 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_appliedImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %2, i32 0, i32 11
  %3 = load float, float* %m_appliedImpulse, align 4
  %4 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_cfm = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %4, i32 0, i32 15
  %5 = load float, float* %m_cfm, align 4
  %mul = fmul float %3, %5
  %sub = fsub float %1, %mul
  store float %sub, float* %deltaImpulse, align 4
  store float 0.000000e+00, float* %deltaVelADotn, align 4
  store float 0.000000e+00, float* %deltaVelBDotn, align 4
  store %struct.btSolverBody* null, %struct.btSolverBody** %bodyA, align 4
  store %struct.btSolverBody* null, %struct.btSolverBody** %bodyB, align 4
  store i32 0, i32* %ndofA, align 4
  store i32 0, i32* %ndofB, align 4
  %6 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %6, i32 0, i32 23
  %7 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA, align 4
  %tobool = icmp ne %class.btMultiBody* %7, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %8 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyA2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %8, i32 0, i32 23
  %9 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA2, align 4
  %call = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %9)
  %add = add nsw i32 %call, 6
  store i32 %add, i32* %ndofA, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %10 = load i32, i32* %i, align 4
  %11 = load i32, i32* %ndofA, align 4
  %cmp = icmp slt i32 %10, %11
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data, i32 0, i32 0
  %12 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_jacAindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %12, i32 0, i32 1
  %13 = load i32, i32* %m_jacAindex, align 4
  %14 = load i32, i32* %i, align 4
  %add3 = add nsw i32 %13, %14
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_jacobians, i32 %add3)
  %15 = load float, float* %call4, align 4
  %m_data5 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data5, i32 0, i32 2
  %16 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_deltaVelAindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %16, i32 0, i32 0
  %17 = load i32, i32* %m_deltaVelAindex, align 4
  %18 = load i32, i32* %i, align 4
  %add6 = add nsw i32 %17, %18
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_deltaVelocities, i32 %add6)
  %19 = load float, float* %call7, align 4
  %mul8 = fmul float %15, %19
  %20 = load float, float* %deltaVelADotn, align 4
  %add9 = fadd float %20, %mul8
  store float %add9, float* %deltaVelADotn, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %i, align 4
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end20

if.else:                                          ; preds = %entry
  %22 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_solverBodyIdA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %22, i32 0, i32 22
  %23 = load i32, i32* %m_solverBodyIdA, align 4
  %cmp10 = icmp sge i32 %23, 0
  br i1 %cmp10, label %if.then11, label %if.end

if.then11:                                        ; preds = %if.else
  %24 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %24, i32 0, i32 1
  %25 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_solverBodyIdA12 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %25, i32 0, i32 22
  %26 = load i32, i32* %m_solverBodyIdA12, align 4
  %call13 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool, i32 %26)
  store %struct.btSolverBody* %call13, %struct.btSolverBody** %bodyA, align 4
  %27 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %27, i32 0, i32 5
  %28 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody30internalGetDeltaLinearVelocityEv(%struct.btSolverBody* %28)
  %call15 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormal1, %class.btVector3* nonnull align 4 dereferenceable(16) %call14)
  %29 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %29, i32 0, i32 4
  %30 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody31internalGetDeltaAngularVelocityEv(%struct.btSolverBody* %30)
  %call17 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_relpos1CrossNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %call16)
  %add18 = fadd float %call15, %call17
  %31 = load float, float* %deltaVelADotn, align 4
  %add19 = fadd float %31, %add18
  store float %add19, float* %deltaVelADotn, align 4
  br label %if.end

if.end:                                           ; preds = %if.then11, %if.else
  br label %if.end20

if.end20:                                         ; preds = %if.end, %for.end
  %32 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %32, i32 0, i32 26
  %33 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB, align 4
  %tobool21 = icmp ne %class.btMultiBody* %33, null
  br i1 %tobool21, label %if.then22, label %if.else43

if.then22:                                        ; preds = %if.end20
  %34 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyB23 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %34, i32 0, i32 26
  %35 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB23, align 4
  %call24 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %35)
  %add25 = add nsw i32 %call24, 6
  store i32 %add25, i32* %ndofB, align 4
  store i32 0, i32* %i26, align 4
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc40, %if.then22
  %36 = load i32, i32* %i26, align 4
  %37 = load i32, i32* %ndofB, align 4
  %cmp28 = icmp slt i32 %36, %37
  br i1 %cmp28, label %for.body29, label %for.end42

for.body29:                                       ; preds = %for.cond27
  %m_data30 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians31 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data30, i32 0, i32 0
  %38 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_jacBindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %38, i32 0, i32 3
  %39 = load i32, i32* %m_jacBindex, align 4
  %40 = load i32, i32* %i26, align 4
  %add32 = add nsw i32 %39, %40
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_jacobians31, i32 %add32)
  %41 = load float, float* %call33, align 4
  %m_data34 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities35 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data34, i32 0, i32 2
  %42 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_deltaVelBindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %42, i32 0, i32 2
  %43 = load i32, i32* %m_deltaVelBindex, align 4
  %44 = load i32, i32* %i26, align 4
  %add36 = add nsw i32 %43, %44
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_deltaVelocities35, i32 %add36)
  %45 = load float, float* %call37, align 4
  %mul38 = fmul float %41, %45
  %46 = load float, float* %deltaVelBDotn, align 4
  %add39 = fadd float %46, %mul38
  store float %add39, float* %deltaVelBDotn, align 4
  br label %for.inc40

for.inc40:                                        ; preds = %for.body29
  %47 = load i32, i32* %i26, align 4
  %inc41 = add nsw i32 %47, 1
  store i32 %inc41, i32* %i26, align 4
  br label %for.cond27

for.end42:                                        ; preds = %for.cond27
  br label %if.end56

if.else43:                                        ; preds = %if.end20
  %48 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_solverBodyIdB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %48, i32 0, i32 25
  %49 = load i32, i32* %m_solverBodyIdB, align 4
  %cmp44 = icmp sge i32 %49, 0
  br i1 %cmp44, label %if.then45, label %if.end55

if.then45:                                        ; preds = %if.else43
  %50 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool46 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %50, i32 0, i32 1
  %51 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_solverBodyIdB47 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %51, i32 0, i32 25
  %52 = load i32, i32* %m_solverBodyIdB47, align 4
  %call48 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool46, i32 %52)
  store %struct.btSolverBody* %call48, %struct.btSolverBody** %bodyB, align 4
  %53 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %53, i32 0, i32 7
  %54 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %call49 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody30internalGetDeltaLinearVelocityEv(%struct.btSolverBody* %54)
  %call50 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormal2, %class.btVector3* nonnull align 4 dereferenceable(16) %call49)
  %55 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %55, i32 0, i32 6
  %56 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %call51 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody31internalGetDeltaAngularVelocityEv(%struct.btSolverBody* %56)
  %call52 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_relpos2CrossNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %call51)
  %add53 = fadd float %call50, %call52
  %57 = load float, float* %deltaVelBDotn, align 4
  %add54 = fadd float %57, %add53
  store float %add54, float* %deltaVelBDotn, align 4
  br label %if.end55

if.end55:                                         ; preds = %if.then45, %if.else43
  br label %if.end56

if.end56:                                         ; preds = %if.end55, %for.end42
  %58 = load float, float* %deltaVelADotn, align 4
  %59 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_jacDiagABInv = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %59, i32 0, i32 13
  %60 = load float, float* %m_jacDiagABInv, align 4
  %mul57 = fmul float %58, %60
  %61 = load float, float* %deltaImpulse, align 4
  %sub58 = fsub float %61, %mul57
  store float %sub58, float* %deltaImpulse, align 4
  %62 = load float, float* %deltaVelBDotn, align 4
  %63 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_jacDiagABInv59 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %63, i32 0, i32 13
  %64 = load float, float* %m_jacDiagABInv59, align 4
  %mul60 = fmul float %62, %64
  %65 = load float, float* %deltaImpulse, align 4
  %sub61 = fsub float %65, %mul60
  store float %sub61, float* %deltaImpulse, align 4
  %66 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_appliedImpulse62 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %66, i32 0, i32 11
  %67 = load float, float* %m_appliedImpulse62, align 4
  %68 = load float, float* %deltaImpulse, align 4
  %add63 = fadd float %67, %68
  store float %add63, float* %sum, align 4
  %69 = load float, float* %sum, align 4
  %70 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_lowerLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %70, i32 0, i32 16
  %71 = load float, float* %m_lowerLimit, align 4
  %cmp64 = fcmp olt float %69, %71
  br i1 %cmp64, label %if.then65, label %if.else71

if.then65:                                        ; preds = %if.end56
  %72 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_lowerLimit66 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %72, i32 0, i32 16
  %73 = load float, float* %m_lowerLimit66, align 4
  %74 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_appliedImpulse67 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %74, i32 0, i32 11
  %75 = load float, float* %m_appliedImpulse67, align 4
  %sub68 = fsub float %73, %75
  store float %sub68, float* %deltaImpulse, align 4
  %76 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_lowerLimit69 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %76, i32 0, i32 16
  %77 = load float, float* %m_lowerLimit69, align 4
  %78 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_appliedImpulse70 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %78, i32 0, i32 11
  store float %77, float* %m_appliedImpulse70, align 4
  br label %if.end82

if.else71:                                        ; preds = %if.end56
  %79 = load float, float* %sum, align 4
  %80 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_upperLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %80, i32 0, i32 17
  %81 = load float, float* %m_upperLimit, align 4
  %cmp72 = fcmp ogt float %79, %81
  br i1 %cmp72, label %if.then73, label %if.else79

if.then73:                                        ; preds = %if.else71
  %82 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_upperLimit74 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %82, i32 0, i32 17
  %83 = load float, float* %m_upperLimit74, align 4
  %84 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_appliedImpulse75 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %84, i32 0, i32 11
  %85 = load float, float* %m_appliedImpulse75, align 4
  %sub76 = fsub float %83, %85
  store float %sub76, float* %deltaImpulse, align 4
  %86 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_upperLimit77 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %86, i32 0, i32 17
  %87 = load float, float* %m_upperLimit77, align 4
  %88 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_appliedImpulse78 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %88, i32 0, i32 11
  store float %87, float* %m_appliedImpulse78, align 4
  br label %if.end81

if.else79:                                        ; preds = %if.else71
  %89 = load float, float* %sum, align 4
  %90 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_appliedImpulse80 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %90, i32 0, i32 11
  store float %89, float* %m_appliedImpulse80, align 4
  br label %if.end81

if.end81:                                         ; preds = %if.else79, %if.then73
  br label %if.end82

if.end82:                                         ; preds = %if.end81, %if.then65
  %91 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyA83 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %91, i32 0, i32 23
  %92 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA83, align 4
  %tobool84 = icmp ne %class.btMultiBody* %92, null
  br i1 %tobool84, label %if.then85, label %if.else95

if.then85:                                        ; preds = %if.end82
  %m_data86 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data86, i32 0, i32 1
  %93 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_jacAindex87 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %93, i32 0, i32 1
  %94 = load i32, i32* %m_jacAindex87, align 4
  %call88 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse, i32 %94)
  %95 = load float, float* %deltaImpulse, align 4
  %96 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_deltaVelAindex89 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %96, i32 0, i32 0
  %97 = load i32, i32* %m_deltaVelAindex89, align 4
  %98 = load i32, i32* %ndofA, align 4
  call void @_ZN27btMultiBodyConstraintSolver13applyDeltaVeeEPffii(%class.btMultiBodyConstraintSolver* %this1, float* %call88, float %95, i32 %97, i32 %98)
  %99 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyA90 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %99, i32 0, i32 23
  %100 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA90, align 4
  %m_data91 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse92 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data91, i32 0, i32 1
  %101 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_jacAindex93 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %101, i32 0, i32 1
  %102 = load i32, i32* %m_jacAindex93, align 4
  %call94 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse92, i32 %102)
  %103 = load float, float* %deltaImpulse, align 4
  call void @_ZN11btMultiBody22applyDeltaVeeMultiDof2EPKff(%class.btMultiBody* %100, float* %call94, float %103)
  br label %if.end102

if.else95:                                        ; preds = %if.end82
  %104 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_solverBodyIdA96 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %104, i32 0, i32 22
  %105 = load i32, i32* %m_solverBodyIdA96, align 4
  %cmp97 = icmp sge i32 %105, 0
  br i1 %cmp97, label %if.then98, label %if.end101

if.then98:                                        ; preds = %if.else95
  %106 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %107 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_contactNormal199 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %107, i32 0, i32 5
  %108 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %call100 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %108)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal199, %class.btVector3* nonnull align 4 dereferenceable(16) %call100)
  %109 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_angularComponentA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %109, i32 0, i32 8
  %110 = load float, float* %deltaImpulse, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %106, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentA, float %110)
  br label %if.end101

if.end101:                                        ; preds = %if.then98, %if.else95
  br label %if.end102

if.end102:                                        ; preds = %if.end101, %if.then85
  %111 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyB103 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %111, i32 0, i32 26
  %112 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB103, align 4
  %tobool104 = icmp ne %class.btMultiBody* %112, null
  br i1 %tobool104, label %if.then105, label %if.else116

if.then105:                                       ; preds = %if.end102
  %m_data106 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse107 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data106, i32 0, i32 1
  %113 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_jacBindex108 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %113, i32 0, i32 3
  %114 = load i32, i32* %m_jacBindex108, align 4
  %call109 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse107, i32 %114)
  %115 = load float, float* %deltaImpulse, align 4
  %116 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_deltaVelBindex110 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %116, i32 0, i32 2
  %117 = load i32, i32* %m_deltaVelBindex110, align 4
  %118 = load i32, i32* %ndofB, align 4
  call void @_ZN27btMultiBodyConstraintSolver13applyDeltaVeeEPffii(%class.btMultiBodyConstraintSolver* %this1, float* %call109, float %115, i32 %117, i32 %118)
  %119 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyB111 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %119, i32 0, i32 26
  %120 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB111, align 4
  %m_data112 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse113 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data112, i32 0, i32 1
  %121 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_jacBindex114 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %121, i32 0, i32 3
  %122 = load i32, i32* %m_jacBindex114, align 4
  %call115 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse113, i32 %122)
  %123 = load float, float* %deltaImpulse, align 4
  call void @_ZN11btMultiBody22applyDeltaVeeMultiDof2EPKff(%class.btMultiBody* %120, float* %call115, float %123)
  br label %if.end124

if.else116:                                       ; preds = %if.end102
  %124 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_solverBodyIdB117 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %124, i32 0, i32 25
  %125 = load i32, i32* %m_solverBodyIdB117, align 4
  %cmp118 = icmp sge i32 %125, 0
  br i1 %cmp118, label %if.then119, label %if.end123

if.then119:                                       ; preds = %if.else116
  %126 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %127 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_contactNormal2121 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %127, i32 0, i32 7
  %128 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %call122 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %128)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp120, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2121, %class.btVector3* nonnull align 4 dereferenceable(16) %call122)
  %129 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_angularComponentB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %129, i32 0, i32 9
  %130 = load float, float* %deltaImpulse, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %126, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp120, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB, float %130)
  br label %if.end123

if.end123:                                        ; preds = %if.then119, %if.else116
  br label %if.end124

if.end124:                                        ; preds = %if.end123, %if.then105
  %131 = load float, float* %deltaImpulse, align 4
  ret float %131
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMultiBody13setPosUpdatedEb(%class.btMultiBody* %this, i1 zeroext %updated) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %updated.addr = alloca i8, align 1
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %frombool = zext i1 %updated to i8
  store i8 %frombool, i8* %updated.addr, align 1
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load i8, i8* %updated.addr, align 1
  %tobool = trunc i8 %0 to i1
  %__posUpdated = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 37
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %__posUpdated, align 1
  ret void
}

; Function Attrs: noinline optnone
define hidden float @_ZN27btMultiBodyConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btMultiBodyConstraintSolver* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %class.btPersistentManifold** %manifoldPtr, i32 %numManifolds, %class.btTypedConstraint** %constraints, i32 %numConstraints, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal, %class.btIDebugDraw* %debugDrawer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %ref.tmp = alloca %struct.btMultiBodySolverConstraint, align 4
  %ref.tmp2 = alloca %struct.btMultiBodySolverConstraint, align 4
  %ref.tmp4 = alloca %struct.btMultiBodySolverConstraint, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %i = alloca i32, align 4
  %fcA = alloca %class.btMultiBodyLinkCollider*, align 4
  %val = alloca float, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4
  store i32 %numBodies, i32* %numBodies.addr, align 4
  store %class.btPersistentManifold** %manifoldPtr, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  store i32 %numManifolds, i32* %numManifolds.addr, align 4
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4
  store i32 %numConstraints, i32* %numConstraints.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %m_multiBodyNonContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 1
  %call = call %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodySolverConstraintC2Ev(%struct.btMultiBodySolverConstraint* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE6resizeEiRKS0_(%class.btAlignedObjectArray.22* %m_multiBodyNonContactConstraints, i32 0, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %ref.tmp)
  %m_multiBodyNormalContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 2
  %call3 = call %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodySolverConstraintC2Ev(%struct.btMultiBodySolverConstraint* %ref.tmp2)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE6resizeEiRKS0_(%class.btAlignedObjectArray.22* %m_multiBodyNormalContactConstraints, i32 0, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %ref.tmp2)
  %m_multiBodyFrictionContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 3
  %call5 = call %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodySolverConstraintC2Ev(%struct.btMultiBodySolverConstraint* %ref.tmp4)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE6resizeEiRKS0_(%class.btAlignedObjectArray.22* %m_multiBodyFrictionContactConstraints, i32 0, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %ref.tmp4)
  %m_data = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data, i32 0, i32 0
  store float 0.000000e+00, float* %ref.tmp6, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.34* %m_jacobians, i32 0, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %m_data7 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data7, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp8, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse, i32 0, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %m_data9 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data9, i32 0, i32 2
  store float 0.000000e+00, float* %ref.tmp10, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.34* %m_deltaVelocities, i32 0, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %numBodies.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %2, i32 %3
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %arrayidx, align 4
  %call11 = call %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEP17btCollisionObject(%class.btCollisionObject* %4)
  store %class.btMultiBodyLinkCollider* %call11, %class.btMultiBodyLinkCollider** %fcA, align 4
  %5 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4
  %tobool = icmp ne %class.btMultiBodyLinkCollider* %5, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4
  %m_multiBody = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %6, i32 0, i32 1
  %7 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBody, align 4
  call void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %7, i32 -1)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %10 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %11 = load i32, i32* %numBodies.addr, align 4
  %12 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  %13 = load i32, i32* %numManifolds.addr, align 4
  %14 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %15 = load i32, i32* %numConstraints.addr, align 4
  %16 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %17 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %call12 = call float @_ZN35btSequentialImpulseConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver* %9, %class.btCollisionObject** %10, i32 %11, %class.btPersistentManifold** %12, i32 %13, %class.btTypedConstraint** %14, i32 %15, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %16, %class.btIDebugDraw* %17)
  store float %call12, float* %val, align 4
  %18 = load float, float* %val, align 4
  ret float %18
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE6resizeEiRKS0_(%class.btAlignedObjectArray.22* %this, i32 %newsize, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %struct.btMultiBodySolverConstraint* %fillData, %struct.btMultiBodySolverConstraint** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %5 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end15

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi(%class.btAlignedObjectArray.22* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %14 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %14, i32 %15
  %16 = bitcast %struct.btMultiBodySolverConstraint* %arrayidx10 to i8*
  %call11 = call i8* @_ZN27btMultiBodySolverConstraintnwEmPv(i32 192, i8* %16)
  %17 = bitcast i8* %call11 to %struct.btMultiBodySolverConstraint*
  %18 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %fillData.addr, align 4
  %19 = bitcast %struct.btMultiBodySolverConstraint* %17 to i8*
  %20 = bitcast %struct.btMultiBodySolverConstraint* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 192, i1 false)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc13 = add nsw i32 %21, 1
  store i32 %inc13, i32* %i5, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  br label %if.end15

if.end15:                                         ; preds = %for.end14, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodySolverConstraintC2Ev(%struct.btMultiBodySolverConstraint* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  store %struct.btMultiBodySolverConstraint* %this, %struct.btMultiBodySolverConstraint** %this.addr, align 4
  %this1 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %this.addr, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_relpos1CrossNormal)
  %m_contactNormal1 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 5
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_contactNormal1)
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 6
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_relpos2CrossNormal)
  %m_contactNormal2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 7
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_contactNormal2)
  %m_angularComponentA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 8
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_angularComponentA)
  %m_angularComponentB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 9
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_angularComponentB)
  %m_solverBodyIdA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 22
  store i32 -1, i32* %m_solverBodyIdA, align 4
  %m_multiBodyA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 23
  store %class.btMultiBody* null, %class.btMultiBody** %m_multiBodyA, align 4
  %m_linkA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 24
  store i32 -1, i32* %m_linkA, align 4
  %m_solverBodyIdB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 25
  store i32 -1, i32* %m_solverBodyIdB, align 4
  %m_multiBodyB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 26
  store %class.btMultiBody* null, %class.btMultiBody** %m_multiBodyB, align 4
  %m_linkB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 27
  store i32 -1, i32* %m_linkB, align 4
  %m_orgConstraint = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 28
  store %class.btMultiBodyConstraint* null, %class.btMultiBodyConstraint** %m_orgConstraint, align 4
  %m_orgDofIndex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %this1, i32 0, i32 29
  store i32 -1, i32* %m_orgDofIndex, align 4
  ret %struct.btMultiBodySolverConstraint* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.34* %this, i32 %newsize, float* nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca float*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store float* %fillData, float** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  %5 = load float*, float** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.34* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  %14 = load float*, float** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds float, float* %14, i32 %15
  %16 = bitcast float* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to float*
  %18 = load float*, float** %fillData.addr, align 4
  %19 = load float, float* %18, align 4
  store float %19, float* %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEP17btCollisionObject(%class.btCollisionObject* %colObj) #2 comdat {
entry:
  %retval = alloca %class.btMultiBodyLinkCollider*, align 4
  %colObj.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %colObj, %class.btCollisionObject** %colObj.addr, align 4
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4
  %call = call i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %0)
  %and = and i32 %call, 64
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4
  %2 = bitcast %class.btCollisionObject* %1 to %class.btMultiBodyLinkCollider*
  store %class.btMultiBodyLinkCollider* %2, %class.btMultiBodyLinkCollider** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store %class.btMultiBodyLinkCollider* null, %class.btMultiBodyLinkCollider** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %retval, align 4
  ret %class.btMultiBodyLinkCollider* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %this, i32 %id) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %id.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  store i32 %id, i32* %id.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load i32, i32* %id.addr, align 4
  %m_companionId = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 29
  store i32 %0, i32* %m_companionId, align 4
  ret void
}

declare float @_ZN35btSequentialImpulseConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88), %class.btIDebugDraw*) unnamed_addr #3

; Function Attrs: noinline optnone
define hidden void @_ZN27btMultiBodyConstraintSolver13applyDeltaVeeEPffii(%class.btMultiBodyConstraintSolver* %this, float* %delta_vee, float %impulse, i32 %velocityIndex, i32 %ndof) #2 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %delta_vee.addr = alloca float*, align 4
  %impulse.addr = alloca float, align 4
  %velocityIndex.addr = alloca i32, align 4
  %ndof.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  store float* %delta_vee, float** %delta_vee.addr, align 4
  store float %impulse, float* %impulse.addr, align 4
  store i32 %velocityIndex, i32* %velocityIndex.addr, align 4
  store i32 %ndof, i32* %ndof.addr, align 4
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %ndof.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load float*, float** %delta_vee.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %2, i32 %3
  %4 = load float, float* %arrayidx, align 4
  %5 = load float, float* %impulse.addr, align 4
  %mul = fmul float %4, %5
  %m_data = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data, i32 0, i32 2
  %6 = load i32, i32* %velocityIndex.addr, align 4
  %7 = load i32, i32* %i, align 4
  %add = add nsw i32 %6, %7
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_deltaVelocities, i32 %add)
  %8 = load float, float* %call, align 4
  %add2 = fadd float %8, %mul
  store float %add2, float* %call, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_dofCount = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 39
  %0 = load i32, i32* %m_dofCount, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btSolverBody*, %struct.btSolverBody** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %0, i32 %1
  ret %struct.btSolverBody* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody30internalGetDeltaLinearVelocityEv(%struct.btSolverBody* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_deltaLinearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 1
  ret %class.btVector3* %m_deltaLinearVelocity
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody31internalGetDeltaAngularVelocityEv(%struct.btSolverBody* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_deltaAngularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 2
  ret %class.btVector3* %m_deltaAngularVelocity
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMultiBody22applyDeltaVeeMultiDof2EPKff(%class.btMultiBody* %this, float* %delta_vee, float %multiplier) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %delta_vee.addr = alloca float*, align 4
  %multiplier.addr = alloca float, align 4
  %dof = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  store float* %delta_vee, float** %delta_vee.addr, align 4
  store float %multiplier, float* %multiplier.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  store i32 0, i32* %dof, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %dof, align 4
  %call = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %this1)
  %add = add nsw i32 6, %call
  %cmp = icmp slt i32 %0, %add
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load float*, float** %delta_vee.addr, align 4
  %2 = load i32, i32* %dof, align 4
  %arrayidx = getelementptr inbounds float, float* %1, i32 %2
  %3 = load float, float* %arrayidx, align 4
  %4 = load float, float* %multiplier.addr, align 4
  %mul = fmul float %3, %4
  %m_deltaV = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 13
  %5 = load i32, i32* %dof, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_deltaV, i32 %5)
  %6 = load float, float* %call2, align 4
  %add3 = fadd float %6, %mul
  store float %add3, float* %call2, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %dof, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %dof, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %linearComponent, %class.btVector3* nonnull align 4 dereferenceable(16) %angularComponent, float %impulseMagnitude) #2 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  %linearComponent.addr = alloca %class.btVector3*, align 4
  %angularComponent.addr = alloca %class.btVector3*, align 4
  %impulseMagnitude.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  store %class.btVector3* %linearComponent, %class.btVector3** %linearComponent.addr, align 4
  store %class.btVector3* %angularComponent, %class.btVector3** %angularComponent.addr, align 4
  store float %impulseMagnitude, float* %impulseMagnitude.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 12
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  %tobool = icmp ne %class.btRigidBody* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btVector3*, %class.btVector3** %linearComponent.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr)
  %m_linearFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 4
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  %m_deltaLinearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_deltaLinearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %2 = load %class.btVector3*, %class.btVector3** %angularComponent.addr, align 4
  %m_angularFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 3
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularFactor)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %m_deltaAngularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 2
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_deltaAngularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_invMass = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 5
  ret %class.btVector3* %m_invMass
}

; Function Attrs: noinline optnone
define hidden void @_ZN27btMultiBodyConstraintSolver31setupMultiBodyContactConstraintER27btMultiBodySolverConstraintRK9btVector3R15btManifoldPointRK19btContactSolverInfoRfbff(%class.btMultiBodyConstraintSolver* %this, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %solverConstraint, %class.btVector3* nonnull align 4 dereferenceable(16) %contactNormal, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %cp, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal, float* nonnull align 4 dereferenceable(4) %relaxation, i1 zeroext %isFriction, float %desiredVelocity, float %cfmSlip) #2 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %solverConstraint.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  %contactNormal.addr = alloca %class.btVector3*, align 4
  %cp.addr = alloca %class.btManifoldPoint*, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %relaxation.addr = alloca float*, align 4
  %isFriction.addr = alloca i8, align 1
  %desiredVelocity.addr = alloca float, align 4
  %cfmSlip.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %rel_pos1 = alloca %class.btVector3, align 4
  %rel_pos2 = alloca %class.btVector3, align 4
  %multiBodyA = alloca %class.btMultiBody*, align 4
  %multiBodyB = alloca %class.btMultiBody*, align 4
  %pos1 = alloca %class.btVector3*, align 4
  %pos2 = alloca %class.btVector3*, align 4
  %bodyA = alloca %struct.btSolverBody*, align 4
  %bodyB = alloca %struct.btSolverBody*, align 4
  %rb0 = alloca %class.btRigidBody*, align 4
  %rb1 = alloca %class.btRigidBody*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca %class.btVector3, align 4
  %invTimeStep = alloca float, align 4
  %cfm = alloca float, align 4
  %erp = alloca float, align 4
  %denom = alloca float, align 4
  %ref.tmp68 = alloca %class.btVector3, align 4
  %ref.tmp71 = alloca %class.btVector3, align 4
  %ndofA = alloca i32, align 4
  %ref.tmp91 = alloca float, align 4
  %ref.tmp102 = alloca float, align 4
  %ref.tmp108 = alloca float, align 4
  %jac1 = alloca float*, align 4
  %delta = alloca float*, align 4
  %torqueAxis0 = alloca %class.btVector3, align 4
  %torqueAxis0131 = alloca %class.btVector3, align 4
  %ref.tmp134 = alloca %class.btVector3, align 4
  %ref.tmp137 = alloca %class.btVector3, align 4
  %ref.tmp141 = alloca float, align 4
  %ref.tmp142 = alloca float, align 4
  %ref.tmp143 = alloca float, align 4
  %ref.tmp151 = alloca %class.btVector3, align 4
  %ref.tmp154 = alloca %class.btVector3, align 4
  %ndofB = alloca i32, align 4
  %ref.tmp177 = alloca float, align 4
  %ref.tmp188 = alloca float, align 4
  %ref.tmp195 = alloca float, align 4
  %ref.tmp198 = alloca %class.btVector3, align 4
  %torqueAxis1 = alloca %class.btVector3, align 4
  %ref.tmp221 = alloca %class.btVector3, align 4
  %ref.tmp222 = alloca %class.btVector3, align 4
  %torqueAxis1224 = alloca %class.btVector3, align 4
  %ref.tmp225 = alloca %class.btVector3, align 4
  %ref.tmp227 = alloca %class.btVector3, align 4
  %ref.tmp229 = alloca %class.btVector3, align 4
  %ref.tmp232 = alloca %class.btVector3, align 4
  %ref.tmp234 = alloca %class.btVector3, align 4
  %ref.tmp237 = alloca float, align 4
  %ref.tmp238 = alloca float, align 4
  %ref.tmp239 = alloca float, align 4
  %vec = alloca %class.btVector3, align 4
  %denom0 = alloca float, align 4
  %denom1 = alloca float, align 4
  %jacB = alloca float*, align 4
  %jacA = alloca float*, align 4
  %lambdaA = alloca float*, align 4
  %lambdaB = alloca float*, align 4
  %ndofA244 = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca float, align 4
  %l = alloca float, align 4
  %ref.tmp264 = alloca %class.btVector3, align 4
  %ndofB273 = alloca i32, align 4
  %i284 = alloca i32, align 4
  %j288 = alloca float, align 4
  %l290 = alloca float, align 4
  %ref.tmp300 = alloca %class.btVector3, align 4
  %ref.tmp301 = alloca %class.btVector3, align 4
  %d = alloca float, align 4
  %restitution = alloca float, align 4
  %penetration = alloca float, align 4
  %rel_vel = alloca float, align 4
  %ndofA323 = alloca i32, align 4
  %ndofB324 = alloca i32, align 4
  %vel1 = alloca %class.btVector3, align 4
  %vel2 = alloca %class.btVector3, align 4
  %jacA331 = alloca float*, align 4
  %i336 = alloca i32, align 4
  %ref.tmp351 = alloca %class.btVector3, align 4
  %ref.tmp352 = alloca %class.btVector3, align 4
  %ref.tmp353 = alloca %class.btVector3, align 4
  %ref.tmp354 = alloca %class.btVector3, align 4
  %ref.tmp355 = alloca %class.btVector3, align 4
  %ref.tmp356 = alloca %class.btVector3, align 4
  %ref.tmp360 = alloca %class.btVector3, align 4
  %ref.tmp361 = alloca %class.btVector3, align 4
  %ref.tmp363 = alloca float, align 4
  %jacB375 = alloca float*, align 4
  %i380 = alloca i32, align 4
  %ref.tmp395 = alloca %class.btVector3, align 4
  %ref.tmp396 = alloca %class.btVector3, align 4
  %ref.tmp397 = alloca %class.btVector3, align 4
  %ref.tmp398 = alloca %class.btVector3, align 4
  %ref.tmp399 = alloca %class.btVector3, align 4
  %ref.tmp400 = alloca %class.btVector3, align 4
  %ref.tmp404 = alloca %class.btVector3, align 4
  %ref.tmp405 = alloca %class.btVector3, align 4
  %ref.tmp407 = alloca float, align 4
  %positionalError = alloca float, align 4
  %velocityError = alloca float, align 4
  %penetrationImpulse = alloca float, align 4
  %velocityImpulse = alloca float, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  store %struct.btMultiBodySolverConstraint* %solverConstraint, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  store %class.btVector3* %contactNormal, %class.btVector3** %contactNormal.addr, align 4
  store %class.btManifoldPoint* %cp, %class.btManifoldPoint** %cp.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  store float* %relaxation, float** %relaxation.addr, align 4
  %frombool = zext i1 %isFriction to i8
  store i8 %frombool, i8* %isFriction.addr, align 1
  store float %desiredVelocity, float* %desiredVelocity.addr, align 4
  store float %cfmSlip, float* %cfmSlip.addr, align 4
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str, i32 0, i32 0))
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %rel_pos1)
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %rel_pos2)
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_multiBodyA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %0, i32 0, i32 23
  %1 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA, align 4
  store %class.btMultiBody* %1, %class.btMultiBody** %multiBodyA, align 4
  %2 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_multiBodyB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %2, i32 0, i32 26
  %3 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB, align 4
  store %class.btMultiBody* %3, %class.btMultiBody** %multiBodyB, align 4
  %4 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnAEv(%class.btManifoldPoint* %4)
  store %class.btVector3* %call4, %class.btVector3** %pos1, align 4
  %5 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnBEv(%class.btManifoldPoint* %5)
  store %class.btVector3* %call5, %class.btVector3** %pos2, align 4
  %6 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %tobool = icmp ne %class.btMultiBody* %6, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %7 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %7, i32 0, i32 1
  %8 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_solverBodyIdA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %8, i32 0, i32 22
  %9 = load i32, i32* %m_solverBodyIdA, align 4
  %call6 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool, i32 %9)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btSolverBody* [ null, %cond.true ], [ %call6, %cond.false ]
  store %struct.btSolverBody* %cond, %struct.btSolverBody** %bodyA, align 4
  %10 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %tobool7 = icmp ne %class.btMultiBody* %10, null
  br i1 %tobool7, label %cond.true8, label %cond.false9

cond.true8:                                       ; preds = %cond.end
  br label %cond.end12

cond.false9:                                      ; preds = %cond.end
  %11 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool10 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %11, i32 0, i32 1
  %12 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_solverBodyIdB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %12, i32 0, i32 25
  %13 = load i32, i32* %m_solverBodyIdB, align 4
  %call11 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool10, i32 %13)
  br label %cond.end12

cond.end12:                                       ; preds = %cond.false9, %cond.true8
  %cond13 = phi %struct.btSolverBody* [ null, %cond.true8 ], [ %call11, %cond.false9 ]
  store %struct.btSolverBody* %cond13, %struct.btSolverBody** %bodyB, align 4
  %14 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %tobool14 = icmp ne %class.btMultiBody* %14, null
  br i1 %tobool14, label %cond.true15, label %cond.false16

cond.true15:                                      ; preds = %cond.end12
  br label %cond.end17

cond.false16:                                     ; preds = %cond.end12
  %15 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %15, i32 0, i32 12
  %16 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  br label %cond.end17

cond.end17:                                       ; preds = %cond.false16, %cond.true15
  %cond18 = phi %class.btRigidBody* [ null, %cond.true15 ], [ %16, %cond.false16 ]
  store %class.btRigidBody* %cond18, %class.btRigidBody** %rb0, align 4
  %17 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %tobool19 = icmp ne %class.btMultiBody* %17, null
  br i1 %tobool19, label %cond.true20, label %cond.false21

cond.true20:                                      ; preds = %cond.end17
  br label %cond.end23

cond.false21:                                     ; preds = %cond.end17
  %18 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %m_originalBody22 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %18, i32 0, i32 12
  %19 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody22, align 4
  br label %cond.end23

cond.end23:                                       ; preds = %cond.false21, %cond.true20
  %cond24 = phi %class.btRigidBody* [ null, %cond.true20 ], [ %19, %cond.false21 ]
  store %class.btRigidBody* %cond24, %class.btRigidBody** %rb1, align 4
  %20 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %tobool25 = icmp ne %struct.btSolverBody* %20, null
  br i1 %tobool25, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end23
  %21 = load %class.btVector3*, %class.btVector3** %pos1, align 4
  %22 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %call26 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK12btSolverBody17getWorldTransformEv(%struct.btSolverBody* %22)
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call26)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %21, %class.btVector3* nonnull align 4 dereferenceable(16) %call27)
  %23 = bitcast %class.btVector3* %rel_pos1 to i8*
  %24 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end23
  %25 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %tobool28 = icmp ne %struct.btSolverBody* %25, null
  br i1 %tobool28, label %if.then29, label %if.end33

if.then29:                                        ; preds = %if.end
  %26 = load %class.btVector3*, %class.btVector3** %pos2, align 4
  %27 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %call31 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK12btSolverBody17getWorldTransformEv(%struct.btSolverBody* %27)
  %call32 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call31)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp30, %class.btVector3* nonnull align 4 dereferenceable(16) %26, %class.btVector3* nonnull align 4 dereferenceable(16) %call32)
  %28 = bitcast %class.btVector3* %rel_pos2 to i8*
  %29 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false)
  br label %if.end33

if.end33:                                         ; preds = %if.then29, %if.end
  %30 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %31 = bitcast %struct.btContactSolverInfo* %30 to %struct.btContactSolverInfoData*
  %m_sor = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %31, i32 0, i32 7
  %32 = load float, float* %m_sor, align 4
  %33 = load float*, float** %relaxation.addr, align 4
  store float %32, float* %33, align 4
  %34 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %35 = bitcast %struct.btContactSolverInfo* %34 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %35, i32 0, i32 3
  %36 = load float, float* %m_timeStep, align 4
  %div = fdiv float 1.000000e+00, %36
  store float %div, float* %invTimeStep, align 4
  %37 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %38 = bitcast %struct.btContactSolverInfo* %37 to %struct.btContactSolverInfoData*
  %m_globalCfm = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %38, i32 0, i32 10
  %39 = load float, float* %m_globalCfm, align 4
  store float %39, float* %cfm, align 4
  %40 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %41 = bitcast %struct.btContactSolverInfo* %40 to %struct.btContactSolverInfoData*
  %m_erp2 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %41, i32 0, i32 9
  %42 = load float, float* %m_erp2, align 4
  store float %42, float* %erp, align 4
  %43 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_contactPointFlags = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %43, i32 0, i32 15
  %44 = load i32, i32* %m_contactPointFlags, align 4
  %and = and i32 %44, 2
  %tobool34 = icmp ne i32 %and, 0
  br i1 %tobool34, label %if.then38, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end33
  %45 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_contactPointFlags35 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %45, i32 0, i32 15
  %46 = load i32, i32* %m_contactPointFlags35, align 4
  %and36 = and i32 %46, 4
  %tobool37 = icmp ne i32 %and36, 0
  br i1 %tobool37, label %if.then38, label %if.else

if.then38:                                        ; preds = %lor.lhs.false, %if.end33
  %47 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_contactPointFlags39 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %47, i32 0, i32 15
  %48 = load i32, i32* %m_contactPointFlags39, align 4
  %and40 = and i32 %48, 2
  %tobool41 = icmp ne i32 %and40, 0
  br i1 %tobool41, label %if.then42, label %if.end43

if.then42:                                        ; preds = %if.then38
  %49 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %50 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %49, i32 0, i32 21
  %m_contactCFM = bitcast %union.anon.47* %50 to float*
  %51 = load float, float* %m_contactCFM, align 4
  store float %51, float* %cfm, align 4
  br label %if.end43

if.end43:                                         ; preds = %if.then42, %if.then38
  %52 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_contactPointFlags44 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %52, i32 0, i32 15
  %53 = load i32, i32* %m_contactPointFlags44, align 4
  %and45 = and i32 %53, 4
  %tobool46 = icmp ne i32 %and45, 0
  br i1 %tobool46, label %if.then47, label %if.end48

if.then47:                                        ; preds = %if.end43
  %54 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %55 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %54, i32 0, i32 22
  %m_contactERP = bitcast %union.anon.48* %55 to float*
  %56 = load float, float* %m_contactERP, align 4
  store float %56, float* %erp, align 4
  br label %if.end48

if.end48:                                         ; preds = %if.then47, %if.end43
  br label %if.end62

if.else:                                          ; preds = %lor.lhs.false
  %57 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_contactPointFlags49 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %57, i32 0, i32 15
  %58 = load i32, i32* %m_contactPointFlags49, align 4
  %and50 = and i32 %58, 8
  %tobool51 = icmp ne i32 %and50, 0
  br i1 %tobool51, label %if.then52, label %if.end61

if.then52:                                        ; preds = %if.else
  %59 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %60 = bitcast %struct.btContactSolverInfo* %59 to %struct.btContactSolverInfoData*
  %m_timeStep53 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %60, i32 0, i32 3
  %61 = load float, float* %m_timeStep53, align 4
  %62 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %63 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %62, i32 0, i32 21
  %m_combinedContactStiffness1 = bitcast %union.anon.47* %63 to float*
  %64 = load float, float* %m_combinedContactStiffness1, align 4
  %mul = fmul float %61, %64
  %65 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %66 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %65, i32 0, i32 22
  %m_combinedContactDamping1 = bitcast %union.anon.48* %66 to float*
  %67 = load float, float* %m_combinedContactDamping1, align 4
  %add = fadd float %mul, %67
  store float %add, float* %denom, align 4
  %68 = load float, float* %denom, align 4
  %cmp = fcmp olt float %68, 0x3E80000000000000
  br i1 %cmp, label %if.then54, label %if.end55

if.then54:                                        ; preds = %if.then52
  store float 0x3E80000000000000, float* %denom, align 4
  br label %if.end55

if.end55:                                         ; preds = %if.then54, %if.then52
  %69 = load float, float* %denom, align 4
  %div56 = fdiv float 1.000000e+00, %69
  store float %div56, float* %cfm, align 4
  %70 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %71 = bitcast %struct.btContactSolverInfo* %70 to %struct.btContactSolverInfoData*
  %m_timeStep57 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %71, i32 0, i32 3
  %72 = load float, float* %m_timeStep57, align 4
  %73 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %74 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %73, i32 0, i32 21
  %m_combinedContactStiffness158 = bitcast %union.anon.47* %74 to float*
  %75 = load float, float* %m_combinedContactStiffness158, align 4
  %mul59 = fmul float %72, %75
  %76 = load float, float* %denom, align 4
  %div60 = fdiv float %mul59, %76
  store float %div60, float* %erp, align 4
  br label %if.end61

if.end61:                                         ; preds = %if.end55, %if.else
  br label %if.end62

if.end62:                                         ; preds = %if.end61, %if.end48
  %77 = load float, float* %invTimeStep, align 4
  %78 = load float, float* %cfm, align 4
  %mul63 = fmul float %78, %77
  store float %mul63, float* %cfm, align 4
  %79 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %tobool64 = icmp ne %class.btMultiBody* %79, null
  br i1 %tobool64, label %if.then65, label %if.else130

if.then65:                                        ; preds = %if.end62
  %80 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %80, i32 0, i32 24
  %81 = load i32, i32* %m_linkA, align 4
  %cmp66 = icmp slt i32 %81, 0
  br i1 %cmp66, label %if.then67, label %if.else70

if.then67:                                        ; preds = %if.then65
  %82 = load %class.btVector3*, %class.btVector3** %pos1, align 4
  %83 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %call69 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getBasePosEv(%class.btMultiBody* %83)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp68, %class.btVector3* nonnull align 4 dereferenceable(16) %82, %class.btVector3* nonnull align 4 dereferenceable(16) %call69)
  %84 = bitcast %class.btVector3* %rel_pos1 to i8*
  %85 = bitcast %class.btVector3* %ref.tmp68 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %84, i8* align 4 %85, i32 16, i1 false)
  br label %if.end75

if.else70:                                        ; preds = %if.then65
  %86 = load %class.btVector3*, %class.btVector3** %pos1, align 4
  %87 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %88 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkA72 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %88, i32 0, i32 24
  %89 = load i32, i32* %m_linkA72, align 4
  %call73 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %87, i32 %89)
  %m_cachedWorldTransform = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call73, i32 0, i32 25
  %call74 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_cachedWorldTransform)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp71, %class.btVector3* nonnull align 4 dereferenceable(16) %86, %class.btVector3* nonnull align 4 dereferenceable(16) %call74)
  %90 = bitcast %class.btVector3* %rel_pos1 to i8*
  %91 = bitcast %class.btVector3* %ref.tmp71 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %90, i8* align 4 %91, i32 16, i1 false)
  br label %if.end75

if.end75:                                         ; preds = %if.else70, %if.then67
  %92 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %call76 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %92)
  %add77 = add nsw i32 %call76, 6
  store i32 %add77, i32* %ndofA, align 4
  %93 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %call78 = call i32 @_ZNK11btMultiBody14getCompanionIdEv(%class.btMultiBody* %93)
  %94 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelAindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %94, i32 0, i32 0
  store i32 %call78, i32* %m_deltaVelAindex, align 4
  %95 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelAindex79 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %95, i32 0, i32 0
  %96 = load i32, i32* %m_deltaVelAindex79, align 4
  %cmp80 = icmp slt i32 %96, 0
  br i1 %cmp80, label %if.then81, label %if.else92

if.then81:                                        ; preds = %if.end75
  %m_data = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data, i32 0, i32 2
  %call82 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_deltaVelocities)
  %97 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelAindex83 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %97, i32 0, i32 0
  store i32 %call82, i32* %m_deltaVelAindex83, align 4
  %98 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %99 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelAindex84 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %99, i32 0, i32 0
  %100 = load i32, i32* %m_deltaVelAindex84, align 4
  call void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %98, i32 %100)
  %m_data85 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities86 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data85, i32 0, i32 2
  %m_data87 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities88 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data87, i32 0, i32 2
  %call89 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_deltaVelocities88)
  %101 = load i32, i32* %ndofA, align 4
  %add90 = add nsw i32 %call89, %101
  store float 0.000000e+00, float* %ref.tmp91, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.34* %m_deltaVelocities86, i32 %add90, float* nonnull align 4 dereferenceable(4) %ref.tmp91)
  br label %if.end93

if.else92:                                        ; preds = %if.end75
  br label %if.end93

if.end93:                                         ; preds = %if.else92, %if.then81
  %m_data94 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data94, i32 0, i32 0
  %call95 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_jacobians)
  %102 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %102, i32 0, i32 1
  store i32 %call95, i32* %m_jacAindex, align 4
  %m_data96 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians97 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data96, i32 0, i32 0
  %m_data98 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians99 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data98, i32 0, i32 0
  %call100 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_jacobians99)
  %103 = load i32, i32* %ndofA, align 4
  %add101 = add nsw i32 %call100, %103
  store float 0.000000e+00, float* %ref.tmp102, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.34* %m_jacobians97, i32 %add101, float* nonnull align 4 dereferenceable(4) %ref.tmp102)
  %m_data103 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data103, i32 0, i32 1
  %m_data104 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse105 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data104, i32 0, i32 1
  %call106 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse105)
  %104 = load i32, i32* %ndofA, align 4
  %add107 = add nsw i32 %call106, %104
  store float 0.000000e+00, float* %ref.tmp108, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse, i32 %add107, float* nonnull align 4 dereferenceable(4) %ref.tmp108)
  %m_data109 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians110 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data109, i32 0, i32 0
  %105 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex111 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %105, i32 0, i32 1
  %106 = load i32, i32* %m_jacAindex111, align 4
  %call112 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_jacobians110, i32 %106)
  store float* %call112, float** %jac1, align 4
  %107 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %108 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkA113 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %108, i32 0, i32 24
  %109 = load i32, i32* %m_linkA113, align 4
  %110 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %call114 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnAEv(%class.btManifoldPoint* %110)
  %111 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4
  %112 = load float*, float** %jac1, align 4
  %m_data115 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_r = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data115, i32 0, i32 3
  %m_data116 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_v = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data116, i32 0, i32 4
  %m_data117 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_m = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data117, i32 0, i32 5
  call void @_ZNK11btMultiBody27fillContactJacobianMultiDofEiRK9btVector3S2_PfR20btAlignedObjectArrayIfERS4_IS0_ERS4_I11btMatrix3x3E(%class.btMultiBody* %107, i32 %109, %class.btVector3* nonnull align 4 dereferenceable(16) %call114, %class.btVector3* nonnull align 4 dereferenceable(16) %111, float* %112, %class.btAlignedObjectArray.34* nonnull align 4 dereferenceable(17) %scratch_r, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17) %scratch_v, %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17) %scratch_m)
  %m_data118 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse119 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data118, i32 0, i32 1
  %113 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex120 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %113, i32 0, i32 1
  %114 = load i32, i32* %m_jacAindex120, align 4
  %call121 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse119, i32 %114)
  store float* %call121, float** %delta, align 4
  %115 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %m_data122 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians123 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data122, i32 0, i32 0
  %116 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex124 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %116, i32 0, i32 1
  %117 = load i32, i32* %m_jacAindex124, align 4
  %call125 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_jacobians123, i32 %117)
  %118 = load float*, float** %delta, align 4
  %m_data126 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_r127 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data126, i32 0, i32 3
  %m_data128 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_v129 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data128, i32 0, i32 4
  call void @_ZNK11btMultiBody30calcAccelerationDeltasMultiDofEPKfPfR20btAlignedObjectArrayIfERS3_I9btVector3E(%class.btMultiBody* %115, float* %call125, float* %118, %class.btAlignedObjectArray.34* nonnull align 4 dereferenceable(17) %scratch_r127, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17) %scratch_v129)
  %119 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %torqueAxis0, %class.btVector3* %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %119)
  %120 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %120, i32 0, i32 4
  %121 = bitcast %class.btVector3* %m_relpos1CrossNormal to i8*
  %122 = bitcast %class.btVector3* %torqueAxis0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %121, i8* align 4 %122, i32 16, i1 false)
  %123 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4
  %124 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %124, i32 0, i32 5
  %125 = bitcast %class.btVector3* %m_contactNormal1 to i8*
  %126 = bitcast %class.btVector3* %123 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %125, i8* align 4 %126, i32 16, i1 false)
  br label %if.end146

if.else130:                                       ; preds = %if.end62
  %127 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %torqueAxis0131, %class.btVector3* %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %127)
  %128 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_relpos1CrossNormal132 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %128, i32 0, i32 4
  %129 = bitcast %class.btVector3* %m_relpos1CrossNormal132 to i8*
  %130 = bitcast %class.btVector3* %torqueAxis0131 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %129, i8* align 4 %130, i32 16, i1 false)
  %131 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4
  %132 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal1133 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %132, i32 0, i32 5
  %133 = bitcast %class.btVector3* %m_contactNormal1133 to i8*
  %134 = bitcast %class.btVector3* %131 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %133, i8* align 4 %134, i32 16, i1 false)
  %135 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %tobool135 = icmp ne %class.btRigidBody* %135, null
  br i1 %tobool135, label %cond.true136, label %cond.false140

cond.true136:                                     ; preds = %if.else130
  %136 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call138 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %136)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp137, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call138, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis0131)
  %137 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call139 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %137)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp134, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp137, %class.btVector3* nonnull align 4 dereferenceable(16) %call139)
  br label %cond.end145

cond.false140:                                    ; preds = %if.else130
  store float 0.000000e+00, float* %ref.tmp141, align 4
  store float 0.000000e+00, float* %ref.tmp142, align 4
  store float 0.000000e+00, float* %ref.tmp143, align 4
  %call144 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp134, float* nonnull align 4 dereferenceable(4) %ref.tmp141, float* nonnull align 4 dereferenceable(4) %ref.tmp142, float* nonnull align 4 dereferenceable(4) %ref.tmp143)
  br label %cond.end145

cond.end145:                                      ; preds = %cond.false140, %cond.true136
  %138 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %138, i32 0, i32 8
  %139 = bitcast %class.btVector3* %m_angularComponentA to i8*
  %140 = bitcast %class.btVector3* %ref.tmp134 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %139, i8* align 4 %140, i32 16, i1 false)
  br label %if.end146

if.end146:                                        ; preds = %cond.end145, %if.end93
  %141 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %tobool147 = icmp ne %class.btMultiBody* %141, null
  br i1 %tobool147, label %if.then148, label %if.else223

if.then148:                                       ; preds = %if.end146
  %142 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %142, i32 0, i32 27
  %143 = load i32, i32* %m_linkB, align 4
  %cmp149 = icmp slt i32 %143, 0
  br i1 %cmp149, label %if.then150, label %if.else153

if.then150:                                       ; preds = %if.then148
  %144 = load %class.btVector3*, %class.btVector3** %pos2, align 4
  %145 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %call152 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getBasePosEv(%class.btMultiBody* %145)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp151, %class.btVector3* nonnull align 4 dereferenceable(16) %144, %class.btVector3* nonnull align 4 dereferenceable(16) %call152)
  %146 = bitcast %class.btVector3* %rel_pos2 to i8*
  %147 = bitcast %class.btVector3* %ref.tmp151 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %146, i8* align 4 %147, i32 16, i1 false)
  br label %if.end159

if.else153:                                       ; preds = %if.then148
  %148 = load %class.btVector3*, %class.btVector3** %pos2, align 4
  %149 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %150 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkB155 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %150, i32 0, i32 27
  %151 = load i32, i32* %m_linkB155, align 4
  %call156 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %149, i32 %151)
  %m_cachedWorldTransform157 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call156, i32 0, i32 25
  %call158 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_cachedWorldTransform157)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp154, %class.btVector3* nonnull align 4 dereferenceable(16) %148, %class.btVector3* nonnull align 4 dereferenceable(16) %call158)
  %152 = bitcast %class.btVector3* %rel_pos2 to i8*
  %153 = bitcast %class.btVector3* %ref.tmp154 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %152, i8* align 4 %153, i32 16, i1 false)
  br label %if.end159

if.end159:                                        ; preds = %if.else153, %if.then150
  %154 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %call160 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %154)
  %add161 = add nsw i32 %call160, 6
  store i32 %add161, i32* %ndofB, align 4
  %155 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %call162 = call i32 @_ZNK11btMultiBody14getCompanionIdEv(%class.btMultiBody* %155)
  %156 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelBindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %156, i32 0, i32 2
  store i32 %call162, i32* %m_deltaVelBindex, align 4
  %157 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelBindex163 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %157, i32 0, i32 2
  %158 = load i32, i32* %m_deltaVelBindex163, align 4
  %cmp164 = icmp slt i32 %158, 0
  br i1 %cmp164, label %if.then165, label %if.end178

if.then165:                                       ; preds = %if.end159
  %m_data166 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities167 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data166, i32 0, i32 2
  %call168 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_deltaVelocities167)
  %159 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelBindex169 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %159, i32 0, i32 2
  store i32 %call168, i32* %m_deltaVelBindex169, align 4
  %160 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %161 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelBindex170 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %161, i32 0, i32 2
  %162 = load i32, i32* %m_deltaVelBindex170, align 4
  call void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %160, i32 %162)
  %m_data171 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities172 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data171, i32 0, i32 2
  %m_data173 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities174 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data173, i32 0, i32 2
  %call175 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_deltaVelocities174)
  %163 = load i32, i32* %ndofB, align 4
  %add176 = add nsw i32 %call175, %163
  store float 0.000000e+00, float* %ref.tmp177, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.34* %m_deltaVelocities172, i32 %add176, float* nonnull align 4 dereferenceable(4) %ref.tmp177)
  br label %if.end178

if.end178:                                        ; preds = %if.then165, %if.end159
  %m_data179 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians180 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data179, i32 0, i32 0
  %call181 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_jacobians180)
  %164 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %164, i32 0, i32 3
  store i32 %call181, i32* %m_jacBindex, align 4
  %m_data182 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians183 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data182, i32 0, i32 0
  %m_data184 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians185 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data184, i32 0, i32 0
  %call186 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_jacobians185)
  %165 = load i32, i32* %ndofB, align 4
  %add187 = add nsw i32 %call186, %165
  store float 0.000000e+00, float* %ref.tmp188, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.34* %m_jacobians183, i32 %add187, float* nonnull align 4 dereferenceable(4) %ref.tmp188)
  %m_data189 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse190 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data189, i32 0, i32 1
  %m_data191 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse192 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data191, i32 0, i32 1
  %call193 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse192)
  %166 = load i32, i32* %ndofB, align 4
  %add194 = add nsw i32 %call193, %166
  store float 0.000000e+00, float* %ref.tmp195, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse190, i32 %add194, float* nonnull align 4 dereferenceable(4) %ref.tmp195)
  %167 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %168 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkB196 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %168, i32 0, i32 27
  %169 = load i32, i32* %m_linkB196, align 4
  %170 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %call197 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnBEv(%class.btManifoldPoint* %170)
  %171 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp198, %class.btVector3* nonnull align 4 dereferenceable(16) %171)
  %m_data199 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians200 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data199, i32 0, i32 0
  %172 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex201 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %172, i32 0, i32 3
  %173 = load i32, i32* %m_jacBindex201, align 4
  %call202 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_jacobians200, i32 %173)
  %m_data203 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_r204 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data203, i32 0, i32 3
  %m_data205 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_v206 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data205, i32 0, i32 4
  %m_data207 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_m208 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data207, i32 0, i32 5
  call void @_ZNK11btMultiBody27fillContactJacobianMultiDofEiRK9btVector3S2_PfR20btAlignedObjectArrayIfERS4_IS0_ERS4_I11btMatrix3x3E(%class.btMultiBody* %167, i32 %169, %class.btVector3* nonnull align 4 dereferenceable(16) %call197, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp198, float* %call202, %class.btAlignedObjectArray.34* nonnull align 4 dereferenceable(17) %scratch_r204, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17) %scratch_v206, %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17) %scratch_m208)
  %174 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %m_data209 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians210 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data209, i32 0, i32 0
  %175 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex211 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %175, i32 0, i32 3
  %176 = load i32, i32* %m_jacBindex211, align 4
  %call212 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_jacobians210, i32 %176)
  %m_data213 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse214 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data213, i32 0, i32 1
  %177 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex215 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %177, i32 0, i32 3
  %178 = load i32, i32* %m_jacBindex215, align 4
  %call216 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse214, i32 %178)
  %m_data217 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_r218 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data217, i32 0, i32 3
  %m_data219 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_v220 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data219, i32 0, i32 4
  call void @_ZNK11btMultiBody30calcAccelerationDeltasMultiDofEPKfPfR20btAlignedObjectArrayIfERS3_I9btVector3E(%class.btMultiBody* %174, float* %call212, float* %call216, %class.btAlignedObjectArray.34* nonnull align 4 dereferenceable(17) %scratch_r218, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17) %scratch_v220)
  %179 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %torqueAxis1, %class.btVector3* %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %179)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp221, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis1)
  %180 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %180, i32 0, i32 6
  %181 = bitcast %class.btVector3* %m_relpos2CrossNormal to i8*
  %182 = bitcast %class.btVector3* %ref.tmp221 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %181, i8* align 4 %182, i32 16, i1 false)
  %183 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp222, %class.btVector3* nonnull align 4 dereferenceable(16) %183)
  %184 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %184, i32 0, i32 7
  %185 = bitcast %class.btVector3* %m_contactNormal2 to i8*
  %186 = bitcast %class.btVector3* %ref.tmp222 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %185, i8* align 4 %186, i32 16, i1 false)
  br label %if.end242

if.else223:                                       ; preds = %if.end146
  %187 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %torqueAxis1224, %class.btVector3* %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %187)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp225, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis1224)
  %188 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_relpos2CrossNormal226 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %188, i32 0, i32 6
  %189 = bitcast %class.btVector3* %m_relpos2CrossNormal226 to i8*
  %190 = bitcast %class.btVector3* %ref.tmp225 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %189, i8* align 4 %190, i32 16, i1 false)
  %191 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp227, %class.btVector3* nonnull align 4 dereferenceable(16) %191)
  %192 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal2228 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %192, i32 0, i32 7
  %193 = bitcast %class.btVector3* %m_contactNormal2228 to i8*
  %194 = bitcast %class.btVector3* %ref.tmp227 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %193, i8* align 4 %194, i32 16, i1 false)
  %195 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %tobool230 = icmp ne %class.btRigidBody* %195, null
  br i1 %tobool230, label %cond.true231, label %cond.false236

cond.true231:                                     ; preds = %if.else223
  %196 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call233 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %196)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp234, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis1224)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp232, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call233, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp234)
  %197 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call235 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %197)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp229, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp232, %class.btVector3* nonnull align 4 dereferenceable(16) %call235)
  br label %cond.end241

cond.false236:                                    ; preds = %if.else223
  store float 0.000000e+00, float* %ref.tmp237, align 4
  store float 0.000000e+00, float* %ref.tmp238, align 4
  store float 0.000000e+00, float* %ref.tmp239, align 4
  %call240 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp229, float* nonnull align 4 dereferenceable(4) %ref.tmp237, float* nonnull align 4 dereferenceable(4) %ref.tmp238, float* nonnull align 4 dereferenceable(4) %ref.tmp239)
  br label %cond.end241

cond.end241:                                      ; preds = %cond.false236, %cond.true231
  %198 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %198, i32 0, i32 9
  %199 = bitcast %class.btVector3* %m_angularComponentB to i8*
  %200 = bitcast %class.btVector3* %ref.tmp229 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %199, i8* align 4 %200, i32 16, i1 false)
  br label %if.end242

if.end242:                                        ; preds = %cond.end241, %if.end178
  %call243 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vec)
  store float 0.000000e+00, float* %denom0, align 4
  store float 0.000000e+00, float* %denom1, align 4
  store float* null, float** %jacB, align 4
  store float* null, float** %jacA, align 4
  store float* null, float** %lambdaA, align 4
  store float* null, float** %lambdaB, align 4
  store i32 0, i32* %ndofA244, align 4
  %201 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %tobool245 = icmp ne %class.btMultiBody* %201, null
  br i1 %tobool245, label %if.then246, label %if.else261

if.then246:                                       ; preds = %if.end242
  %202 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %call247 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %202)
  %add248 = add nsw i32 %call247, 6
  store i32 %add248, i32* %ndofA244, align 4
  %m_data249 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians250 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data249, i32 0, i32 0
  %203 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex251 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %203, i32 0, i32 1
  %204 = load i32, i32* %m_jacAindex251, align 4
  %call252 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_jacobians250, i32 %204)
  store float* %call252, float** %jacA, align 4
  %m_data253 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse254 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data253, i32 0, i32 1
  %205 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex255 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %205, i32 0, i32 1
  %206 = load i32, i32* %m_jacAindex255, align 4
  %call256 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse254, i32 %206)
  store float* %call256, float** %lambdaA, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then246
  %207 = load i32, i32* %i, align 4
  %208 = load i32, i32* %ndofA244, align 4
  %cmp257 = icmp slt i32 %207, %208
  br i1 %cmp257, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %209 = load float*, float** %jacA, align 4
  %210 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %209, i32 %210
  %211 = load float, float* %arrayidx, align 4
  store float %211, float* %j, align 4
  %212 = load float*, float** %lambdaA, align 4
  %213 = load i32, i32* %i, align 4
  %arrayidx258 = getelementptr inbounds float, float* %212, i32 %213
  %214 = load float, float* %arrayidx258, align 4
  store float %214, float* %l, align 4
  %215 = load float, float* %j, align 4
  %216 = load float, float* %l, align 4
  %mul259 = fmul float %215, %216
  %217 = load float, float* %denom0, align 4
  %add260 = fadd float %217, %mul259
  store float %add260, float* %denom0, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %218 = load i32, i32* %i, align 4
  %inc = add nsw i32 %218, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end270

if.else261:                                       ; preds = %if.end242
  %219 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %tobool262 = icmp ne %class.btRigidBody* %219, null
  br i1 %tobool262, label %if.then263, label %if.end269

if.then263:                                       ; preds = %if.else261
  %220 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentA265 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %220, i32 0, i32 8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp264, %class.btVector3* %m_angularComponentA265, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1)
  %221 = bitcast %class.btVector3* %vec to i8*
  %222 = bitcast %class.btVector3* %ref.tmp264 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %221, i8* align 4 %222, i32 16, i1 false)
  %223 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call266 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %223)
  %224 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4
  %call267 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %224, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %add268 = fadd float %call266, %call267
  store float %add268, float* %denom0, align 4
  br label %if.end269

if.end269:                                        ; preds = %if.then263, %if.else261
  br label %if.end270

if.end270:                                        ; preds = %if.end269, %for.end
  %225 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %tobool271 = icmp ne %class.btMultiBody* %225, null
  br i1 %tobool271, label %if.then272, label %if.else297

if.then272:                                       ; preds = %if.end270
  %226 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %call274 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %226)
  %add275 = add nsw i32 %call274, 6
  store i32 %add275, i32* %ndofB273, align 4
  %m_data276 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians277 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data276, i32 0, i32 0
  %227 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex278 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %227, i32 0, i32 3
  %228 = load i32, i32* %m_jacBindex278, align 4
  %call279 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_jacobians277, i32 %228)
  store float* %call279, float** %jacB, align 4
  %m_data280 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse281 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data280, i32 0, i32 1
  %229 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex282 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %229, i32 0, i32 3
  %230 = load i32, i32* %m_jacBindex282, align 4
  %call283 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse281, i32 %230)
  store float* %call283, float** %lambdaB, align 4
  store i32 0, i32* %i284, align 4
  br label %for.cond285

for.cond285:                                      ; preds = %for.inc294, %if.then272
  %231 = load i32, i32* %i284, align 4
  %232 = load i32, i32* %ndofB273, align 4
  %cmp286 = icmp slt i32 %231, %232
  br i1 %cmp286, label %for.body287, label %for.end296

for.body287:                                      ; preds = %for.cond285
  %233 = load float*, float** %jacB, align 4
  %234 = load i32, i32* %i284, align 4
  %arrayidx289 = getelementptr inbounds float, float* %233, i32 %234
  %235 = load float, float* %arrayidx289, align 4
  store float %235, float* %j288, align 4
  %236 = load float*, float** %lambdaB, align 4
  %237 = load i32, i32* %i284, align 4
  %arrayidx291 = getelementptr inbounds float, float* %236, i32 %237
  %238 = load float, float* %arrayidx291, align 4
  store float %238, float* %l290, align 4
  %239 = load float, float* %j288, align 4
  %240 = load float, float* %l290, align 4
  %mul292 = fmul float %239, %240
  %241 = load float, float* %denom1, align 4
  %add293 = fadd float %241, %mul292
  store float %add293, float* %denom1, align 4
  br label %for.inc294

for.inc294:                                       ; preds = %for.body287
  %242 = load i32, i32* %i284, align 4
  %inc295 = add nsw i32 %242, 1
  store i32 %inc295, i32* %i284, align 4
  br label %for.cond285

for.end296:                                       ; preds = %for.cond285
  br label %if.end307

if.else297:                                       ; preds = %if.end270
  %243 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %tobool298 = icmp ne %class.btRigidBody* %243, null
  br i1 %tobool298, label %if.then299, label %if.end306

if.then299:                                       ; preds = %if.else297
  %244 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentB302 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %244, i32 0, i32 9
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp301, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB302)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp300, %class.btVector3* %ref.tmp301, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2)
  %245 = bitcast %class.btVector3* %vec to i8*
  %246 = bitcast %class.btVector3* %ref.tmp300 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %245, i8* align 4 %246, i32 16, i1 false)
  %247 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call303 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %247)
  %248 = load %class.btVector3*, %class.btVector3** %contactNormal.addr, align 4
  %call304 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %248, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %add305 = fadd float %call303, %call304
  store float %add305, float* %denom1, align 4
  br label %if.end306

if.end306:                                        ; preds = %if.then299, %if.else297
  br label %if.end307

if.end307:                                        ; preds = %if.end306, %for.end296
  %249 = load float, float* %denom0, align 4
  %250 = load float, float* %denom1, align 4
  %add308 = fadd float %249, %250
  %251 = load float, float* %cfm, align 4
  %add309 = fadd float %add308, %251
  store float %add309, float* %d, align 4
  %252 = load float, float* %d, align 4
  %cmp310 = fcmp ogt float %252, 0x3E80000000000000
  br i1 %cmp310, label %if.then311, label %if.else313

if.then311:                                       ; preds = %if.end307
  %253 = load float*, float** %relaxation.addr, align 4
  %254 = load float, float* %253, align 4
  %255 = load float, float* %d, align 4
  %div312 = fdiv float %254, %255
  %256 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %256, i32 0, i32 13
  store float %div312, float* %m_jacDiagABInv, align 4
  br label %if.end315

if.else313:                                       ; preds = %if.end307
  %257 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv314 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %257, i32 0, i32 13
  store float 0.000000e+00, float* %m_jacDiagABInv314, align 4
  br label %if.end315

if.end315:                                        ; preds = %if.else313, %if.then311
  store float 0.000000e+00, float* %restitution, align 4
  %258 = load i8, i8* %isFriction.addr, align 1
  %tobool316 = trunc i8 %258 to i1
  br i1 %tobool316, label %cond.true317, label %cond.false318

cond.true317:                                     ; preds = %if.end315
  br label %cond.end321

cond.false318:                                    ; preds = %if.end315
  %259 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %call319 = call float @_ZNK15btManifoldPoint11getDistanceEv(%class.btManifoldPoint* %259)
  %260 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %261 = bitcast %struct.btContactSolverInfo* %260 to %struct.btContactSolverInfoData*
  %m_linearSlop = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %261, i32 0, i32 14
  %262 = load float, float* %m_linearSlop, align 4
  %add320 = fadd float %call319, %262
  br label %cond.end321

cond.end321:                                      ; preds = %cond.false318, %cond.true317
  %cond322 = phi float [ 0.000000e+00, %cond.true317 ], [ %add320, %cond.false318 ]
  store float %cond322, float* %penetration, align 4
  store float 0.000000e+00, float* %rel_vel, align 4
  store i32 0, i32* %ndofA323, align 4
  store i32 0, i32* %ndofB324, align 4
  %call325 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel1)
  %call326 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel2)
  %263 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %tobool327 = icmp ne %class.btMultiBody* %263, null
  br i1 %tobool327, label %if.then328, label %if.else348

if.then328:                                       ; preds = %cond.end321
  %264 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %call329 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %264)
  %add330 = add nsw i32 %call329, 6
  store i32 %add330, i32* %ndofA323, align 4
  %m_data332 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians333 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data332, i32 0, i32 0
  %265 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex334 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %265, i32 0, i32 1
  %266 = load i32, i32* %m_jacAindex334, align 4
  %call335 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_jacobians333, i32 %266)
  store float* %call335, float** %jacA331, align 4
  store i32 0, i32* %i336, align 4
  br label %for.cond337

for.cond337:                                      ; preds = %for.inc345, %if.then328
  %267 = load i32, i32* %i336, align 4
  %268 = load i32, i32* %ndofA323, align 4
  %cmp338 = icmp slt i32 %267, %268
  br i1 %cmp338, label %for.body339, label %for.end347

for.body339:                                      ; preds = %for.cond337
  %269 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %call340 = call float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %269)
  %270 = load i32, i32* %i336, align 4
  %arrayidx341 = getelementptr inbounds float, float* %call340, i32 %270
  %271 = load float, float* %arrayidx341, align 4
  %272 = load float*, float** %jacA331, align 4
  %273 = load i32, i32* %i336, align 4
  %arrayidx342 = getelementptr inbounds float, float* %272, i32 %273
  %274 = load float, float* %arrayidx342, align 4
  %mul343 = fmul float %271, %274
  %275 = load float, float* %rel_vel, align 4
  %add344 = fadd float %275, %mul343
  store float %add344, float* %rel_vel, align 4
  br label %for.inc345

for.inc345:                                       ; preds = %for.body339
  %276 = load i32, i32* %i336, align 4
  %inc346 = add nsw i32 %276, 1
  store i32 %inc346, i32* %i336, align 4
  br label %for.cond337

for.end347:                                       ; preds = %for.cond337
  br label %if.end370

if.else348:                                       ; preds = %cond.end321
  %277 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %tobool349 = icmp ne %class.btRigidBody* %277, null
  br i1 %tobool349, label %if.then350, label %if.end369

if.then350:                                       ; preds = %if.else348
  %278 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp353, %class.btRigidBody* %278, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1)
  %279 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call357 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody14getTotalTorqueEv(%class.btRigidBody* %279)
  %280 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call358 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %280)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp356, %class.btVector3* nonnull align 4 dereferenceable(16) %call357, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call358)
  %281 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %282 = bitcast %struct.btContactSolverInfo* %281 to %struct.btContactSolverInfoData*
  %m_timeStep359 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %282, i32 0, i32 3
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp355, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp356, float* nonnull align 4 dereferenceable(4) %m_timeStep359)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp354, %class.btVector3* %ref.tmp355, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp352, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp353, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp354)
  %283 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call362 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody13getTotalForceEv(%class.btRigidBody* %283)
  %284 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call364 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %284)
  store float %call364, float* %ref.tmp363, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp361, %class.btVector3* nonnull align 4 dereferenceable(16) %call362, float* nonnull align 4 dereferenceable(4) %ref.tmp363)
  %285 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %286 = bitcast %struct.btContactSolverInfo* %285 to %struct.btContactSolverInfoData*
  %m_timeStep365 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %286, i32 0, i32 3
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp360, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp361, float* nonnull align 4 dereferenceable(4) %m_timeStep365)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp351, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp352, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp360)
  %287 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal1366 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %287, i32 0, i32 5
  %call367 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp351, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1366)
  %288 = load float, float* %rel_vel, align 4
  %add368 = fadd float %288, %call367
  store float %add368, float* %rel_vel, align 4
  br label %if.end369

if.end369:                                        ; preds = %if.then350, %if.else348
  br label %if.end370

if.end370:                                        ; preds = %if.end369, %for.end347
  %289 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %tobool371 = icmp ne %class.btMultiBody* %289, null
  br i1 %tobool371, label %if.then372, label %if.else392

if.then372:                                       ; preds = %if.end370
  %290 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %call373 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %290)
  %add374 = add nsw i32 %call373, 6
  store i32 %add374, i32* %ndofB324, align 4
  %m_data376 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians377 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data376, i32 0, i32 0
  %291 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex378 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %291, i32 0, i32 3
  %292 = load i32, i32* %m_jacBindex378, align 4
  %call379 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_jacobians377, i32 %292)
  store float* %call379, float** %jacB375, align 4
  store i32 0, i32* %i380, align 4
  br label %for.cond381

for.cond381:                                      ; preds = %for.inc389, %if.then372
  %293 = load i32, i32* %i380, align 4
  %294 = load i32, i32* %ndofB324, align 4
  %cmp382 = icmp slt i32 %293, %294
  br i1 %cmp382, label %for.body383, label %for.end391

for.body383:                                      ; preds = %for.cond381
  %295 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %call384 = call float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %295)
  %296 = load i32, i32* %i380, align 4
  %arrayidx385 = getelementptr inbounds float, float* %call384, i32 %296
  %297 = load float, float* %arrayidx385, align 4
  %298 = load float*, float** %jacB375, align 4
  %299 = load i32, i32* %i380, align 4
  %arrayidx386 = getelementptr inbounds float, float* %298, i32 %299
  %300 = load float, float* %arrayidx386, align 4
  %mul387 = fmul float %297, %300
  %301 = load float, float* %rel_vel, align 4
  %add388 = fadd float %301, %mul387
  store float %add388, float* %rel_vel, align 4
  br label %for.inc389

for.inc389:                                       ; preds = %for.body383
  %302 = load i32, i32* %i380, align 4
  %inc390 = add nsw i32 %302, 1
  store i32 %inc390, i32* %i380, align 4
  br label %for.cond381

for.end391:                                       ; preds = %for.cond381
  br label %if.end414

if.else392:                                       ; preds = %if.end370
  %303 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %tobool393 = icmp ne %class.btRigidBody* %303, null
  br i1 %tobool393, label %if.then394, label %if.end413

if.then394:                                       ; preds = %if.else392
  %304 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp397, %class.btRigidBody* %304, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2)
  %305 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call401 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody14getTotalTorqueEv(%class.btRigidBody* %305)
  %306 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call402 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %306)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp400, %class.btVector3* nonnull align 4 dereferenceable(16) %call401, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call402)
  %307 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %308 = bitcast %struct.btContactSolverInfo* %307 to %struct.btContactSolverInfoData*
  %m_timeStep403 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %308, i32 0, i32 3
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp399, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp400, float* nonnull align 4 dereferenceable(4) %m_timeStep403)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp398, %class.btVector3* %ref.tmp399, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp396, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp397, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp398)
  %309 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call406 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody13getTotalForceEv(%class.btRigidBody* %309)
  %310 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call408 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %310)
  store float %call408, float* %ref.tmp407, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp405, %class.btVector3* nonnull align 4 dereferenceable(16) %call406, float* nonnull align 4 dereferenceable(4) %ref.tmp407)
  %311 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %312 = bitcast %struct.btContactSolverInfo* %311 to %struct.btContactSolverInfoData*
  %m_timeStep409 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %312, i32 0, i32 3
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp404, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp405, float* nonnull align 4 dereferenceable(4) %m_timeStep409)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp395, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp396, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp404)
  %313 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal2410 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %313, i32 0, i32 7
  %call411 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp395, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2410)
  %314 = load float, float* %rel_vel, align 4
  %add412 = fadd float %314, %call411
  store float %add412, float* %rel_vel, align 4
  br label %if.end413

if.end413:                                        ; preds = %if.then394, %if.else392
  br label %if.end414

if.end414:                                        ; preds = %if.end413, %for.end391
  %315 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_combinedFriction = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %315, i32 0, i32 6
  %316 = load float, float* %m_combinedFriction, align 4
  %317 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_friction = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %317, i32 0, i32 12
  store float %316, float* %m_friction, align 4
  %318 = load i8, i8* %isFriction.addr, align 1
  %tobool415 = trunc i8 %318 to i1
  br i1 %tobool415, label %if.end421, label %if.then416

if.then416:                                       ; preds = %if.end414
  %319 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %320 = load float, float* %rel_vel, align 4
  %321 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_combinedRestitution = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %321, i32 0, i32 9
  %322 = load float, float* %m_combinedRestitution, align 4
  %call417 = call float @_ZN35btSequentialImpulseConstraintSolver16restitutionCurveEff(%class.btSequentialImpulseConstraintSolver* %319, float %320, float %322)
  store float %call417, float* %restitution, align 4
  %323 = load float, float* %restitution, align 4
  %cmp418 = fcmp ole float %323, 0.000000e+00
  br i1 %cmp418, label %if.then419, label %if.end420

if.then419:                                       ; preds = %if.then416
  store float 0.000000e+00, float* %restitution, align 4
  br label %if.end420

if.end420:                                        ; preds = %if.then419, %if.then416
  br label %if.end421

if.end421:                                        ; preds = %if.end420, %if.end414
  %324 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_appliedImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %324, i32 0, i32 11
  store float 0.000000e+00, float* %m_appliedImpulse, align 4
  %325 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_appliedPushImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %325, i32 0, i32 10
  store float 0.000000e+00, float* %m_appliedPushImpulse, align 4
  store float 0.000000e+00, float* %positionalError, align 4
  %326 = load float, float* %restitution, align 4
  %327 = load float, float* %rel_vel, align 4
  %sub = fsub float %326, %327
  store float %sub, float* %velocityError, align 4
  %328 = load float, float* %penetration, align 4
  %cmp422 = fcmp ogt float %328, 0.000000e+00
  br i1 %cmp422, label %if.then423, label %if.else427

if.then423:                                       ; preds = %if.end421
  store float 0.000000e+00, float* %positionalError, align 4
  %329 = load float, float* %penetration, align 4
  %330 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %331 = bitcast %struct.btContactSolverInfo* %330 to %struct.btContactSolverInfoData*
  %m_timeStep424 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %331, i32 0, i32 3
  %332 = load float, float* %m_timeStep424, align 4
  %div425 = fdiv float %329, %332
  %333 = load float, float* %velocityError, align 4
  %sub426 = fsub float %333, %div425
  store float %sub426, float* %velocityError, align 4
  br label %if.end431

if.else427:                                       ; preds = %if.end421
  %334 = load float, float* %penetration, align 4
  %fneg = fneg float %334
  %335 = load float, float* %erp, align 4
  %mul428 = fmul float %fneg, %335
  %336 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %337 = bitcast %struct.btContactSolverInfo* %336 to %struct.btContactSolverInfoData*
  %m_timeStep429 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %337, i32 0, i32 3
  %338 = load float, float* %m_timeStep429, align 4
  %div430 = fdiv float %mul428, %338
  store float %div430, float* %positionalError, align 4
  br label %if.end431

if.end431:                                        ; preds = %if.else427, %if.then423
  %339 = load float, float* %positionalError, align 4
  %340 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv432 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %340, i32 0, i32 13
  %341 = load float, float* %m_jacDiagABInv432, align 4
  %mul433 = fmul float %339, %341
  store float %mul433, float* %penetrationImpulse, align 4
  %342 = load float, float* %velocityError, align 4
  %343 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv434 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %343, i32 0, i32 13
  %344 = load float, float* %m_jacDiagABInv434, align 4
  %mul435 = fmul float %342, %344
  store float %mul435, float* %velocityImpulse, align 4
  %345 = load i8, i8* %isFriction.addr, align 1
  %tobool436 = trunc i8 %345 to i1
  br i1 %tobool436, label %if.else439, label %if.then437

if.then437:                                       ; preds = %if.end431
  %346 = load float, float* %penetrationImpulse, align 4
  %347 = load float, float* %velocityImpulse, align 4
  %add438 = fadd float %346, %347
  %348 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_rhs = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %348, i32 0, i32 14
  store float %add438, float* %m_rhs, align 4
  %349 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_rhsPenetration = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %349, i32 0, i32 18
  store float 0.000000e+00, float* %m_rhsPenetration, align 4
  %350 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_lowerLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %350, i32 0, i32 16
  store float 0.000000e+00, float* %m_lowerLimit, align 4
  %351 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_upperLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %351, i32 0, i32 17
  store float 1.000000e+10, float* %m_upperLimit, align 4
  br label %if.end447

if.else439:                                       ; preds = %if.end431
  %352 = load float, float* %velocityImpulse, align 4
  %353 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_rhs440 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %353, i32 0, i32 14
  store float %352, float* %m_rhs440, align 4
  %354 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_rhsPenetration441 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %354, i32 0, i32 18
  store float 0.000000e+00, float* %m_rhsPenetration441, align 4
  %355 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_friction442 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %355, i32 0, i32 12
  %356 = load float, float* %m_friction442, align 4
  %fneg443 = fneg float %356
  %357 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_lowerLimit444 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %357, i32 0, i32 16
  store float %fneg443, float* %m_lowerLimit444, align 4
  %358 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_friction445 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %358, i32 0, i32 12
  %359 = load float, float* %m_friction445, align 4
  %360 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_upperLimit446 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %360, i32 0, i32 17
  store float %359, float* %m_upperLimit446, align 4
  br label %if.end447

if.end447:                                        ; preds = %if.else439, %if.then437
  %361 = load float, float* %cfm, align 4
  %362 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv448 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %362, i32 0, i32 13
  %363 = load float, float* %m_jacDiagABInv448, align 4
  %mul449 = fmul float %361, %363
  %364 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_cfm = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %364, i32 0, i32 15
  store float %mul449, float* %m_cfm, align 4
  %call450 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #7
  ret void
}

declare %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* returned, i8*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnAEv(%class.btManifoldPoint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_positionWorldOnA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 3
  ret %class.btVector3* %m_positionWorldOnA
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnBEv(%class.btManifoldPoint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_positionWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 2
  ret %class.btVector3* %m_positionWorldOnB
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK12btSolverBody17getWorldTransformEv(%struct.btSolverBody* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 0
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getBasePosEv(%class.btMultiBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_basePos = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 3
  ret %class.btVector3* %m_basePos
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %index.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 11
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray.26* %m_links, i32 %0)
  ret %struct.btMultibodyLink* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK11btMultiBody14getCompanionIdEv(%class.btMultiBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_companionId = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 29
  %0 = load i32, i32* %m_companionId, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMultiBody27fillContactJacobianMultiDofEiRK9btVector3S2_PfR20btAlignedObjectArrayIfERS4_IS0_ERS4_I11btMatrix3x3E(%class.btMultiBody* %this, i32 %link, %class.btVector3* nonnull align 4 dereferenceable(16) %contact_point, %class.btVector3* nonnull align 4 dereferenceable(16) %normal, float* %jac, %class.btAlignedObjectArray.34* nonnull align 4 dereferenceable(17) %scratch_r, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17) %scratch_v, %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17) %scratch_m) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %link.addr = alloca i32, align 4
  %contact_point.addr = alloca %class.btVector3*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %jac.addr = alloca float*, align 4
  %scratch_r.addr = alloca %class.btAlignedObjectArray.34*, align 4
  %scratch_v.addr = alloca %class.btAlignedObjectArray.38*, align 4
  %scratch_m.addr = alloca %class.btAlignedObjectArray.42*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  store i32 %link, i32* %link.addr, align 4
  store %class.btVector3* %contact_point, %class.btVector3** %contact_point.addr, align 4
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4
  store float* %jac, float** %jac.addr, align 4
  store %class.btAlignedObjectArray.34* %scratch_r, %class.btAlignedObjectArray.34** %scratch_r.addr, align 4
  store %class.btAlignedObjectArray.38* %scratch_v, %class.btAlignedObjectArray.38** %scratch_v.addr, align 4
  store %class.btAlignedObjectArray.42* %scratch_m, %class.btAlignedObjectArray.42** %scratch_m.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load i32, i32* %link.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %contact_point.addr, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %2 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %3 = load float*, float** %jac.addr, align 4
  %4 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %scratch_r.addr, align 4
  %5 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %scratch_v.addr, align 4
  %6 = load %class.btAlignedObjectArray.42*, %class.btAlignedObjectArray.42** %scratch_m.addr, align 4
  call void @_ZNK11btMultiBody30fillConstraintJacobianMultiDofEiRK9btVector3S2_S2_PfR20btAlignedObjectArrayIfERS4_IS0_ERS4_I11btMatrix3x3E(%class.btMultiBody* %this1, i32 %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %2, float* %3, %class.btAlignedObjectArray.34* nonnull align 4 dereferenceable(17) %4, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17) %5, %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17) %6)
  ret void
}

declare void @_ZNK11btMultiBody30calcAccelerationDeltasMultiDofEPKfPfR20btAlignedObjectArrayIfERS3_I9btVector3E(%class.btMultiBody*, float*, float*, %class.btAlignedObjectArray.34* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17)) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  ret %class.btMatrix3x3* %m_invInertiaTensorWorld
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  ret %class.btVector3* %m_angularFactor
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK15btManifoldPoint11getDistanceEv(%class.btManifoldPoint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_distance1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 5
  %0 = load float, float* %m_distance1, align 4
  ret float %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_realBuf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 14
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_realBuf, i32 0)
  ret float* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %rel_pos.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %rel_pos, %class.btVector3** %rel_pos.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %0 = load %class.btVector3*, %class.btVector3** %rel_pos.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call2, float* %ref.tmp1, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp3, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody14getTotalTorqueEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_totalTorque = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 10
  ret %class.btVector3* %m_totalTorque
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody13getTotalForceEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_totalForce = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 9
  ret %class.btVector3* %m_totalForce
}

declare float @_ZN35btSequentialImpulseConstraintSolver16restitutionCurveEff(%class.btSequentialImpulseConstraintSolver*, float, float) #3

; Function Attrs: nounwind
declare %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* returned) unnamed_addr #5

; Function Attrs: noinline optnone
define hidden void @_ZN27btMultiBodyConstraintSolver41setupMultiBodyTorsionalFrictionConstraintER27btMultiBodySolverConstraintRK9btVector3R15btManifoldPointfRK19btContactSolverInfoRfbff(%class.btMultiBodyConstraintSolver* %this, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %solverConstraint, %class.btVector3* nonnull align 4 dereferenceable(16) %constraintNormal, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %cp, float %combinedTorsionalFriction, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal, float* nonnull align 4 dereferenceable(4) %relaxation, i1 zeroext %isFriction, float %desiredVelocity, float %cfmSlip) #2 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %solverConstraint.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  %constraintNormal.addr = alloca %class.btVector3*, align 4
  %cp.addr = alloca %class.btManifoldPoint*, align 4
  %combinedTorsionalFriction.addr = alloca float, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %relaxation.addr = alloca float*, align 4
  %isFriction.addr = alloca i8, align 1
  %desiredVelocity.addr = alloca float, align 4
  %cfmSlip.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %rel_pos1 = alloca %class.btVector3, align 4
  %rel_pos2 = alloca %class.btVector3, align 4
  %multiBodyA = alloca %class.btMultiBody*, align 4
  %multiBodyB = alloca %class.btMultiBody*, align 4
  %pos1 = alloca %class.btVector3*, align 4
  %pos2 = alloca %class.btVector3*, align 4
  %bodyA = alloca %struct.btSolverBody*, align 4
  %bodyB = alloca %struct.btSolverBody*, align 4
  %rb0 = alloca %class.btRigidBody*, align 4
  %rb1 = alloca %class.btRigidBody*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca %class.btVector3, align 4
  %ref.tmp37 = alloca %class.btVector3, align 4
  %ref.tmp39 = alloca %class.btVector3, align 4
  %ndofA = alloca i32, align 4
  %ref.tmp58 = alloca float, align 4
  %ref.tmp69 = alloca float, align 4
  %ref.tmp75 = alloca float, align 4
  %jac1 = alloca float*, align 4
  %ref.tmp82 = alloca %class.btVector3, align 4
  %ref.tmp83 = alloca float, align 4
  %ref.tmp84 = alloca float, align 4
  %ref.tmp85 = alloca float, align 4
  %delta = alloca float*, align 4
  %torqueAxis0 = alloca %class.btVector3, align 4
  %ref.tmp102 = alloca %class.btVector3, align 4
  %ref.tmp103 = alloca float, align 4
  %ref.tmp104 = alloca float, align 4
  %ref.tmp105 = alloca float, align 4
  %torqueAxis0108 = alloca %class.btVector3, align 4
  %ref.tmp110 = alloca %class.btVector3, align 4
  %ref.tmp111 = alloca float, align 4
  %ref.tmp112 = alloca float, align 4
  %ref.tmp113 = alloca float, align 4
  %ref.tmp116 = alloca %class.btVector3, align 4
  %ref.tmp119 = alloca %class.btVector3, align 4
  %ref.tmp123 = alloca float, align 4
  %ref.tmp124 = alloca float, align 4
  %ref.tmp125 = alloca float, align 4
  %ref.tmp133 = alloca %class.btVector3, align 4
  %ref.tmp136 = alloca %class.btVector3, align 4
  %ndofB = alloca i32, align 4
  %ref.tmp159 = alloca float, align 4
  %ref.tmp170 = alloca float, align 4
  %ref.tmp177 = alloca float, align 4
  %ref.tmp180 = alloca %class.btVector3, align 4
  %ref.tmp181 = alloca %class.btVector3, align 4
  %ref.tmp182 = alloca float, align 4
  %ref.tmp183 = alloca float, align 4
  %ref.tmp184 = alloca float, align 4
  %torqueAxis1 = alloca %class.btVector3, align 4
  %ref.tmp208 = alloca %class.btVector3, align 4
  %ref.tmp209 = alloca %class.btVector3, align 4
  %ref.tmp210 = alloca %class.btVector3, align 4
  %ref.tmp211 = alloca float, align 4
  %ref.tmp212 = alloca float, align 4
  %ref.tmp213 = alloca float, align 4
  %torqueAxis1216 = alloca %class.btVector3, align 4
  %ref.tmp217 = alloca %class.btVector3, align 4
  %ref.tmp219 = alloca %class.btVector3, align 4
  %ref.tmp220 = alloca %class.btVector3, align 4
  %ref.tmp221 = alloca float, align 4
  %ref.tmp222 = alloca float, align 4
  %ref.tmp223 = alloca float, align 4
  %ref.tmp226 = alloca %class.btVector3, align 4
  %ref.tmp229 = alloca %class.btVector3, align 4
  %ref.tmp231 = alloca %class.btVector3, align 4
  %ref.tmp234 = alloca float, align 4
  %ref.tmp235 = alloca float, align 4
  %ref.tmp236 = alloca float, align 4
  %vec = alloca %class.btVector3, align 4
  %denom0 = alloca float, align 4
  %denom1 = alloca float, align 4
  %jacB = alloca float*, align 4
  %jacA = alloca float*, align 4
  %lambdaA = alloca float*, align 4
  %lambdaB = alloca float*, align 4
  %ndofA241 = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca float, align 4
  %l = alloca float, align 4
  %ref.tmp260 = alloca %class.btVector3, align 4
  %ndofB269 = alloca i32, align 4
  %i280 = alloca i32, align 4
  %j284 = alloca float, align 4
  %l286 = alloca float, align 4
  %ref.tmp296 = alloca %class.btVector3, align 4
  %ref.tmp297 = alloca %class.btVector3, align 4
  %d = alloca float, align 4
  %restitution = alloca float, align 4
  %penetration = alloca float, align 4
  %rel_vel = alloca float, align 4
  %ndofA317 = alloca i32, align 4
  %ndofB318 = alloca i32, align 4
  %vel1 = alloca %class.btVector3, align 4
  %vel2 = alloca %class.btVector3, align 4
  %jacA325 = alloca float*, align 4
  %i330 = alloca i32, align 4
  %ref.tmp345 = alloca %class.btVector3, align 4
  %jacB355 = alloca float*, align 4
  %i360 = alloca i32, align 4
  %ref.tmp375 = alloca %class.btVector3, align 4
  %velocityError = alloca float, align 4
  %velocityImpulse = alloca float, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  store %struct.btMultiBodySolverConstraint* %solverConstraint, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  store %class.btVector3* %constraintNormal, %class.btVector3** %constraintNormal.addr, align 4
  store %class.btManifoldPoint* %cp, %class.btManifoldPoint** %cp.addr, align 4
  store float %combinedTorsionalFriction, float* %combinedTorsionalFriction.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  store float* %relaxation, float** %relaxation.addr, align 4
  %frombool = zext i1 %isFriction to i8
  store i8 %frombool, i8* %isFriction.addr, align 1
  store float %desiredVelocity, float* %desiredVelocity.addr, align 4
  store float %cfmSlip, float* %cfmSlip.addr, align 4
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.1, i32 0, i32 0))
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %rel_pos1)
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %rel_pos2)
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_multiBodyA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %0, i32 0, i32 23
  %1 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA, align 4
  store %class.btMultiBody* %1, %class.btMultiBody** %multiBodyA, align 4
  %2 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_multiBodyB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %2, i32 0, i32 26
  %3 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB, align 4
  store %class.btMultiBody* %3, %class.btMultiBody** %multiBodyB, align 4
  %4 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnAEv(%class.btManifoldPoint* %4)
  store %class.btVector3* %call4, %class.btVector3** %pos1, align 4
  %5 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnBEv(%class.btManifoldPoint* %5)
  store %class.btVector3* %call5, %class.btVector3** %pos2, align 4
  %6 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %tobool = icmp ne %class.btMultiBody* %6, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %7 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %7, i32 0, i32 1
  %8 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_solverBodyIdA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %8, i32 0, i32 22
  %9 = load i32, i32* %m_solverBodyIdA, align 4
  %call6 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool, i32 %9)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btSolverBody* [ null, %cond.true ], [ %call6, %cond.false ]
  store %struct.btSolverBody* %cond, %struct.btSolverBody** %bodyA, align 4
  %10 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %tobool7 = icmp ne %class.btMultiBody* %10, null
  br i1 %tobool7, label %cond.true8, label %cond.false9

cond.true8:                                       ; preds = %cond.end
  br label %cond.end12

cond.false9:                                      ; preds = %cond.end
  %11 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool10 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %11, i32 0, i32 1
  %12 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_solverBodyIdB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %12, i32 0, i32 25
  %13 = load i32, i32* %m_solverBodyIdB, align 4
  %call11 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool10, i32 %13)
  br label %cond.end12

cond.end12:                                       ; preds = %cond.false9, %cond.true8
  %cond13 = phi %struct.btSolverBody* [ null, %cond.true8 ], [ %call11, %cond.false9 ]
  store %struct.btSolverBody* %cond13, %struct.btSolverBody** %bodyB, align 4
  %14 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %tobool14 = icmp ne %class.btMultiBody* %14, null
  br i1 %tobool14, label %cond.true15, label %cond.false16

cond.true15:                                      ; preds = %cond.end12
  br label %cond.end17

cond.false16:                                     ; preds = %cond.end12
  %15 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %15, i32 0, i32 12
  %16 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  br label %cond.end17

cond.end17:                                       ; preds = %cond.false16, %cond.true15
  %cond18 = phi %class.btRigidBody* [ null, %cond.true15 ], [ %16, %cond.false16 ]
  store %class.btRigidBody* %cond18, %class.btRigidBody** %rb0, align 4
  %17 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %tobool19 = icmp ne %class.btMultiBody* %17, null
  br i1 %tobool19, label %cond.true20, label %cond.false21

cond.true20:                                      ; preds = %cond.end17
  br label %cond.end23

cond.false21:                                     ; preds = %cond.end17
  %18 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %m_originalBody22 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %18, i32 0, i32 12
  %19 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody22, align 4
  br label %cond.end23

cond.end23:                                       ; preds = %cond.false21, %cond.true20
  %cond24 = phi %class.btRigidBody* [ null, %cond.true20 ], [ %19, %cond.false21 ]
  store %class.btRigidBody* %cond24, %class.btRigidBody** %rb1, align 4
  %20 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %tobool25 = icmp ne %struct.btSolverBody* %20, null
  br i1 %tobool25, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end23
  %21 = load %class.btVector3*, %class.btVector3** %pos1, align 4
  %22 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %call26 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK12btSolverBody17getWorldTransformEv(%struct.btSolverBody* %22)
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call26)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %21, %class.btVector3* nonnull align 4 dereferenceable(16) %call27)
  %23 = bitcast %class.btVector3* %rel_pos1 to i8*
  %24 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end23
  %25 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %tobool28 = icmp ne %struct.btSolverBody* %25, null
  br i1 %tobool28, label %if.then29, label %if.end33

if.then29:                                        ; preds = %if.end
  %26 = load %class.btVector3*, %class.btVector3** %pos2, align 4
  %27 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %call31 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK12btSolverBody17getWorldTransformEv(%struct.btSolverBody* %27)
  %call32 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call31)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp30, %class.btVector3* nonnull align 4 dereferenceable(16) %26, %class.btVector3* nonnull align 4 dereferenceable(16) %call32)
  %28 = bitcast %class.btVector3* %rel_pos2 to i8*
  %29 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false)
  br label %if.end33

if.end33:                                         ; preds = %if.then29, %if.end
  %30 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %31 = bitcast %struct.btContactSolverInfo* %30 to %struct.btContactSolverInfoData*
  %m_sor = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %31, i32 0, i32 7
  %32 = load float, float* %m_sor, align 4
  %33 = load float*, float** %relaxation.addr, align 4
  store float %32, float* %33, align 4
  %34 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %tobool34 = icmp ne %class.btMultiBody* %34, null
  br i1 %tobool34, label %if.then35, label %if.else107

if.then35:                                        ; preds = %if.end33
  %35 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %35, i32 0, i32 24
  %36 = load i32, i32* %m_linkA, align 4
  %cmp = icmp slt i32 %36, 0
  br i1 %cmp, label %if.then36, label %if.else

if.then36:                                        ; preds = %if.then35
  %37 = load %class.btVector3*, %class.btVector3** %pos1, align 4
  %38 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %call38 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getBasePosEv(%class.btMultiBody* %38)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp37, %class.btVector3* nonnull align 4 dereferenceable(16) %37, %class.btVector3* nonnull align 4 dereferenceable(16) %call38)
  %39 = bitcast %class.btVector3* %rel_pos1 to i8*
  %40 = bitcast %class.btVector3* %ref.tmp37 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %39, i8* align 4 %40, i32 16, i1 false)
  br label %if.end43

if.else:                                          ; preds = %if.then35
  %41 = load %class.btVector3*, %class.btVector3** %pos1, align 4
  %42 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %43 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkA40 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %43, i32 0, i32 24
  %44 = load i32, i32* %m_linkA40, align 4
  %call41 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %42, i32 %44)
  %m_cachedWorldTransform = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call41, i32 0, i32 25
  %call42 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_cachedWorldTransform)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp39, %class.btVector3* nonnull align 4 dereferenceable(16) %41, %class.btVector3* nonnull align 4 dereferenceable(16) %call42)
  %45 = bitcast %class.btVector3* %rel_pos1 to i8*
  %46 = bitcast %class.btVector3* %ref.tmp39 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %45, i8* align 4 %46, i32 16, i1 false)
  br label %if.end43

if.end43:                                         ; preds = %if.else, %if.then36
  %47 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %call44 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %47)
  %add = add nsw i32 %call44, 6
  store i32 %add, i32* %ndofA, align 4
  %48 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %call45 = call i32 @_ZNK11btMultiBody14getCompanionIdEv(%class.btMultiBody* %48)
  %49 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelAindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %49, i32 0, i32 0
  store i32 %call45, i32* %m_deltaVelAindex, align 4
  %50 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelAindex46 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %50, i32 0, i32 0
  %51 = load i32, i32* %m_deltaVelAindex46, align 4
  %cmp47 = icmp slt i32 %51, 0
  br i1 %cmp47, label %if.then48, label %if.else59

if.then48:                                        ; preds = %if.end43
  %m_data = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data, i32 0, i32 2
  %call49 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_deltaVelocities)
  %52 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelAindex50 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %52, i32 0, i32 0
  store i32 %call49, i32* %m_deltaVelAindex50, align 4
  %53 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %54 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelAindex51 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %54, i32 0, i32 0
  %55 = load i32, i32* %m_deltaVelAindex51, align 4
  call void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %53, i32 %55)
  %m_data52 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities53 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data52, i32 0, i32 2
  %m_data54 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities55 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data54, i32 0, i32 2
  %call56 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_deltaVelocities55)
  %56 = load i32, i32* %ndofA, align 4
  %add57 = add nsw i32 %call56, %56
  store float 0.000000e+00, float* %ref.tmp58, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.34* %m_deltaVelocities53, i32 %add57, float* nonnull align 4 dereferenceable(4) %ref.tmp58)
  br label %if.end60

if.else59:                                        ; preds = %if.end43
  br label %if.end60

if.end60:                                         ; preds = %if.else59, %if.then48
  %m_data61 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data61, i32 0, i32 0
  %call62 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_jacobians)
  %57 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %57, i32 0, i32 1
  store i32 %call62, i32* %m_jacAindex, align 4
  %m_data63 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians64 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data63, i32 0, i32 0
  %m_data65 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians66 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data65, i32 0, i32 0
  %call67 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_jacobians66)
  %58 = load i32, i32* %ndofA, align 4
  %add68 = add nsw i32 %call67, %58
  store float 0.000000e+00, float* %ref.tmp69, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.34* %m_jacobians64, i32 %add68, float* nonnull align 4 dereferenceable(4) %ref.tmp69)
  %m_data70 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data70, i32 0, i32 1
  %m_data71 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse72 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data71, i32 0, i32 1
  %call73 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse72)
  %59 = load i32, i32* %ndofA, align 4
  %add74 = add nsw i32 %call73, %59
  store float 0.000000e+00, float* %ref.tmp75, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse, i32 %add74, float* nonnull align 4 dereferenceable(4) %ref.tmp75)
  %m_data76 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians77 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data76, i32 0, i32 0
  %60 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex78 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %60, i32 0, i32 1
  %61 = load i32, i32* %m_jacAindex78, align 4
  %call79 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_jacobians77, i32 %61)
  store float* %call79, float** %jac1, align 4
  %62 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %63 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkA80 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %63, i32 0, i32 24
  %64 = load i32, i32* %m_linkA80, align 4
  %65 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %call81 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnAEv(%class.btManifoldPoint* %65)
  %66 = load %class.btVector3*, %class.btVector3** %constraintNormal.addr, align 4
  store float 0.000000e+00, float* %ref.tmp83, align 4
  store float 0.000000e+00, float* %ref.tmp84, align 4
  store float 0.000000e+00, float* %ref.tmp85, align 4
  %call86 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp82, float* nonnull align 4 dereferenceable(4) %ref.tmp83, float* nonnull align 4 dereferenceable(4) %ref.tmp84, float* nonnull align 4 dereferenceable(4) %ref.tmp85)
  %67 = load float*, float** %jac1, align 4
  %m_data87 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_r = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data87, i32 0, i32 3
  %m_data88 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_v = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data88, i32 0, i32 4
  %m_data89 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_m = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data89, i32 0, i32 5
  call void @_ZNK11btMultiBody30fillConstraintJacobianMultiDofEiRK9btVector3S2_S2_PfR20btAlignedObjectArrayIfERS4_IS0_ERS4_I11btMatrix3x3E(%class.btMultiBody* %62, i32 %64, %class.btVector3* nonnull align 4 dereferenceable(16) %call81, %class.btVector3* nonnull align 4 dereferenceable(16) %66, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp82, float* %67, %class.btAlignedObjectArray.34* nonnull align 4 dereferenceable(17) %scratch_r, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17) %scratch_v, %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17) %scratch_m)
  %m_data90 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse91 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data90, i32 0, i32 1
  %68 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex92 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %68, i32 0, i32 1
  %69 = load i32, i32* %m_jacAindex92, align 4
  %call93 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse91, i32 %69)
  store float* %call93, float** %delta, align 4
  %70 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %m_data94 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians95 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data94, i32 0, i32 0
  %71 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex96 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %71, i32 0, i32 1
  %72 = load i32, i32* %m_jacAindex96, align 4
  %call97 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_jacobians95, i32 %72)
  %73 = load float*, float** %delta, align 4
  %m_data98 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_r99 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data98, i32 0, i32 3
  %m_data100 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_v101 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data100, i32 0, i32 4
  call void @_ZNK11btMultiBody30calcAccelerationDeltasMultiDofEPKfPfR20btAlignedObjectArrayIfERS3_I9btVector3E(%class.btMultiBody* %70, float* %call97, float* %73, %class.btAlignedObjectArray.34* nonnull align 4 dereferenceable(17) %scratch_r99, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17) %scratch_v101)
  %74 = load %class.btVector3*, %class.btVector3** %constraintNormal.addr, align 4
  %75 = bitcast %class.btVector3* %torqueAxis0 to i8*
  %76 = bitcast %class.btVector3* %74 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %75, i8* align 4 %76, i32 16, i1 false)
  %77 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %77, i32 0, i32 4
  %78 = bitcast %class.btVector3* %m_relpos1CrossNormal to i8*
  %79 = bitcast %class.btVector3* %torqueAxis0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %78, i8* align 4 %79, i32 16, i1 false)
  store float 0.000000e+00, float* %ref.tmp103, align 4
  store float 0.000000e+00, float* %ref.tmp104, align 4
  store float 0.000000e+00, float* %ref.tmp105, align 4
  %call106 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp102, float* nonnull align 4 dereferenceable(4) %ref.tmp103, float* nonnull align 4 dereferenceable(4) %ref.tmp104, float* nonnull align 4 dereferenceable(4) %ref.tmp105)
  %80 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %80, i32 0, i32 5
  %81 = bitcast %class.btVector3* %m_contactNormal1 to i8*
  %82 = bitcast %class.btVector3* %ref.tmp102 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %81, i8* align 4 %82, i32 16, i1 false)
  br label %if.end128

if.else107:                                       ; preds = %if.end33
  %83 = load %class.btVector3*, %class.btVector3** %constraintNormal.addr, align 4
  %84 = bitcast %class.btVector3* %torqueAxis0108 to i8*
  %85 = bitcast %class.btVector3* %83 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %84, i8* align 4 %85, i32 16, i1 false)
  %86 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_relpos1CrossNormal109 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %86, i32 0, i32 4
  %87 = bitcast %class.btVector3* %m_relpos1CrossNormal109 to i8*
  %88 = bitcast %class.btVector3* %torqueAxis0108 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %87, i8* align 4 %88, i32 16, i1 false)
  store float 0.000000e+00, float* %ref.tmp111, align 4
  store float 0.000000e+00, float* %ref.tmp112, align 4
  store float 0.000000e+00, float* %ref.tmp113, align 4
  %call114 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp110, float* nonnull align 4 dereferenceable(4) %ref.tmp111, float* nonnull align 4 dereferenceable(4) %ref.tmp112, float* nonnull align 4 dereferenceable(4) %ref.tmp113)
  %89 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal1115 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %89, i32 0, i32 5
  %90 = bitcast %class.btVector3* %m_contactNormal1115 to i8*
  %91 = bitcast %class.btVector3* %ref.tmp110 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %90, i8* align 4 %91, i32 16, i1 false)
  %92 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %tobool117 = icmp ne %class.btRigidBody* %92, null
  br i1 %tobool117, label %cond.true118, label %cond.false122

cond.true118:                                     ; preds = %if.else107
  %93 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call120 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %93)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp119, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call120, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis0108)
  %94 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call121 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %94)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp116, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp119, %class.btVector3* nonnull align 4 dereferenceable(16) %call121)
  br label %cond.end127

cond.false122:                                    ; preds = %if.else107
  store float 0.000000e+00, float* %ref.tmp123, align 4
  store float 0.000000e+00, float* %ref.tmp124, align 4
  store float 0.000000e+00, float* %ref.tmp125, align 4
  %call126 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp116, float* nonnull align 4 dereferenceable(4) %ref.tmp123, float* nonnull align 4 dereferenceable(4) %ref.tmp124, float* nonnull align 4 dereferenceable(4) %ref.tmp125)
  br label %cond.end127

cond.end127:                                      ; preds = %cond.false122, %cond.true118
  %95 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %95, i32 0, i32 8
  %96 = bitcast %class.btVector3* %m_angularComponentA to i8*
  %97 = bitcast %class.btVector3* %ref.tmp116 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %96, i8* align 4 %97, i32 16, i1 false)
  br label %if.end128

if.end128:                                        ; preds = %cond.end127, %if.end60
  %98 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %tobool129 = icmp ne %class.btMultiBody* %98, null
  br i1 %tobool129, label %if.then130, label %if.else215

if.then130:                                       ; preds = %if.end128
  %99 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %99, i32 0, i32 27
  %100 = load i32, i32* %m_linkB, align 4
  %cmp131 = icmp slt i32 %100, 0
  br i1 %cmp131, label %if.then132, label %if.else135

if.then132:                                       ; preds = %if.then130
  %101 = load %class.btVector3*, %class.btVector3** %pos2, align 4
  %102 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %call134 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getBasePosEv(%class.btMultiBody* %102)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp133, %class.btVector3* nonnull align 4 dereferenceable(16) %101, %class.btVector3* nonnull align 4 dereferenceable(16) %call134)
  %103 = bitcast %class.btVector3* %rel_pos2 to i8*
  %104 = bitcast %class.btVector3* %ref.tmp133 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %103, i8* align 4 %104, i32 16, i1 false)
  br label %if.end141

if.else135:                                       ; preds = %if.then130
  %105 = load %class.btVector3*, %class.btVector3** %pos2, align 4
  %106 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %107 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkB137 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %107, i32 0, i32 27
  %108 = load i32, i32* %m_linkB137, align 4
  %call138 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %106, i32 %108)
  %m_cachedWorldTransform139 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call138, i32 0, i32 25
  %call140 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_cachedWorldTransform139)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp136, %class.btVector3* nonnull align 4 dereferenceable(16) %105, %class.btVector3* nonnull align 4 dereferenceable(16) %call140)
  %109 = bitcast %class.btVector3* %rel_pos2 to i8*
  %110 = bitcast %class.btVector3* %ref.tmp136 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %109, i8* align 4 %110, i32 16, i1 false)
  br label %if.end141

if.end141:                                        ; preds = %if.else135, %if.then132
  %111 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %call142 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %111)
  %add143 = add nsw i32 %call142, 6
  store i32 %add143, i32* %ndofB, align 4
  %112 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %call144 = call i32 @_ZNK11btMultiBody14getCompanionIdEv(%class.btMultiBody* %112)
  %113 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelBindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %113, i32 0, i32 2
  store i32 %call144, i32* %m_deltaVelBindex, align 4
  %114 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelBindex145 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %114, i32 0, i32 2
  %115 = load i32, i32* %m_deltaVelBindex145, align 4
  %cmp146 = icmp slt i32 %115, 0
  br i1 %cmp146, label %if.then147, label %if.end160

if.then147:                                       ; preds = %if.end141
  %m_data148 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities149 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data148, i32 0, i32 2
  %call150 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_deltaVelocities149)
  %116 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelBindex151 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %116, i32 0, i32 2
  store i32 %call150, i32* %m_deltaVelBindex151, align 4
  %117 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %118 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelBindex152 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %118, i32 0, i32 2
  %119 = load i32, i32* %m_deltaVelBindex152, align 4
  call void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %117, i32 %119)
  %m_data153 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities154 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data153, i32 0, i32 2
  %m_data155 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocities156 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data155, i32 0, i32 2
  %call157 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_deltaVelocities156)
  %120 = load i32, i32* %ndofB, align 4
  %add158 = add nsw i32 %call157, %120
  store float 0.000000e+00, float* %ref.tmp159, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.34* %m_deltaVelocities154, i32 %add158, float* nonnull align 4 dereferenceable(4) %ref.tmp159)
  br label %if.end160

if.end160:                                        ; preds = %if.then147, %if.end141
  %m_data161 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians162 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data161, i32 0, i32 0
  %call163 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_jacobians162)
  %121 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %121, i32 0, i32 3
  store i32 %call163, i32* %m_jacBindex, align 4
  %m_data164 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians165 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data164, i32 0, i32 0
  %m_data166 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians167 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data166, i32 0, i32 0
  %call168 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_jacobians167)
  %122 = load i32, i32* %ndofB, align 4
  %add169 = add nsw i32 %call168, %122
  store float 0.000000e+00, float* %ref.tmp170, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.34* %m_jacobians165, i32 %add169, float* nonnull align 4 dereferenceable(4) %ref.tmp170)
  %m_data171 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse172 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data171, i32 0, i32 1
  %m_data173 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse174 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data173, i32 0, i32 1
  %call175 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse174)
  %123 = load i32, i32* %ndofB, align 4
  %add176 = add nsw i32 %call175, %123
  store float 0.000000e+00, float* %ref.tmp177, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse172, i32 %add176, float* nonnull align 4 dereferenceable(4) %ref.tmp177)
  %124 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %125 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkB178 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %125, i32 0, i32 27
  %126 = load i32, i32* %m_linkB178, align 4
  %127 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %call179 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnBEv(%class.btManifoldPoint* %127)
  %128 = load %class.btVector3*, %class.btVector3** %constraintNormal.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp180, %class.btVector3* nonnull align 4 dereferenceable(16) %128)
  store float 0.000000e+00, float* %ref.tmp182, align 4
  store float 0.000000e+00, float* %ref.tmp183, align 4
  store float 0.000000e+00, float* %ref.tmp184, align 4
  %call185 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp181, float* nonnull align 4 dereferenceable(4) %ref.tmp182, float* nonnull align 4 dereferenceable(4) %ref.tmp183, float* nonnull align 4 dereferenceable(4) %ref.tmp184)
  %m_data186 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians187 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data186, i32 0, i32 0
  %129 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex188 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %129, i32 0, i32 3
  %130 = load i32, i32* %m_jacBindex188, align 4
  %call189 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_jacobians187, i32 %130)
  %m_data190 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_r191 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data190, i32 0, i32 3
  %m_data192 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_v193 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data192, i32 0, i32 4
  %m_data194 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_m195 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data194, i32 0, i32 5
  call void @_ZNK11btMultiBody30fillConstraintJacobianMultiDofEiRK9btVector3S2_S2_PfR20btAlignedObjectArrayIfERS4_IS0_ERS4_I11btMatrix3x3E(%class.btMultiBody* %124, i32 %126, %class.btVector3* nonnull align 4 dereferenceable(16) %call179, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp180, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp181, float* %call189, %class.btAlignedObjectArray.34* nonnull align 4 dereferenceable(17) %scratch_r191, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17) %scratch_v193, %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17) %scratch_m195)
  %131 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %m_data196 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians197 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data196, i32 0, i32 0
  %132 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex198 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %132, i32 0, i32 3
  %133 = load i32, i32* %m_jacBindex198, align 4
  %call199 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_jacobians197, i32 %133)
  %m_data200 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse201 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data200, i32 0, i32 1
  %134 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex202 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %134, i32 0, i32 3
  %135 = load i32, i32* %m_jacBindex202, align 4
  %call203 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse201, i32 %135)
  %m_data204 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_r205 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data204, i32 0, i32 3
  %m_data206 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %scratch_v207 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data206, i32 0, i32 4
  call void @_ZNK11btMultiBody30calcAccelerationDeltasMultiDofEPKfPfR20btAlignedObjectArrayIfERS3_I9btVector3E(%class.btMultiBody* %131, float* %call199, float* %call203, %class.btAlignedObjectArray.34* nonnull align 4 dereferenceable(17) %scratch_r205, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17) %scratch_v207)
  %136 = load %class.btVector3*, %class.btVector3** %constraintNormal.addr, align 4
  %137 = bitcast %class.btVector3* %torqueAxis1 to i8*
  %138 = bitcast %class.btVector3* %136 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %137, i8* align 4 %138, i32 16, i1 false)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp208, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis1)
  %139 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %139, i32 0, i32 6
  %140 = bitcast %class.btVector3* %m_relpos2CrossNormal to i8*
  %141 = bitcast %class.btVector3* %ref.tmp208 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %140, i8* align 4 %141, i32 16, i1 false)
  store float 0.000000e+00, float* %ref.tmp211, align 4
  store float 0.000000e+00, float* %ref.tmp212, align 4
  store float 0.000000e+00, float* %ref.tmp213, align 4
  %call214 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp210, float* nonnull align 4 dereferenceable(4) %ref.tmp211, float* nonnull align 4 dereferenceable(4) %ref.tmp212, float* nonnull align 4 dereferenceable(4) %ref.tmp213)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp209, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp210)
  %142 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %142, i32 0, i32 7
  %143 = bitcast %class.btVector3* %m_contactNormal2 to i8*
  %144 = bitcast %class.btVector3* %ref.tmp209 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %143, i8* align 4 %144, i32 16, i1 false)
  br label %if.end239

if.else215:                                       ; preds = %if.end128
  %145 = load %class.btVector3*, %class.btVector3** %constraintNormal.addr, align 4
  %146 = bitcast %class.btVector3* %torqueAxis1216 to i8*
  %147 = bitcast %class.btVector3* %145 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %146, i8* align 4 %147, i32 16, i1 false)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp217, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis1216)
  %148 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_relpos2CrossNormal218 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %148, i32 0, i32 6
  %149 = bitcast %class.btVector3* %m_relpos2CrossNormal218 to i8*
  %150 = bitcast %class.btVector3* %ref.tmp217 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %149, i8* align 4 %150, i32 16, i1 false)
  store float 0.000000e+00, float* %ref.tmp221, align 4
  store float 0.000000e+00, float* %ref.tmp222, align 4
  store float 0.000000e+00, float* %ref.tmp223, align 4
  %call224 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp220, float* nonnull align 4 dereferenceable(4) %ref.tmp221, float* nonnull align 4 dereferenceable(4) %ref.tmp222, float* nonnull align 4 dereferenceable(4) %ref.tmp223)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp219, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp220)
  %151 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal2225 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %151, i32 0, i32 7
  %152 = bitcast %class.btVector3* %m_contactNormal2225 to i8*
  %153 = bitcast %class.btVector3* %ref.tmp219 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %152, i8* align 4 %153, i32 16, i1 false)
  %154 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %tobool227 = icmp ne %class.btRigidBody* %154, null
  br i1 %tobool227, label %cond.true228, label %cond.false233

cond.true228:                                     ; preds = %if.else215
  %155 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call230 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %155)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp231, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis1216)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp229, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call230, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp231)
  %156 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call232 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %156)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp226, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp229, %class.btVector3* nonnull align 4 dereferenceable(16) %call232)
  br label %cond.end238

cond.false233:                                    ; preds = %if.else215
  store float 0.000000e+00, float* %ref.tmp234, align 4
  store float 0.000000e+00, float* %ref.tmp235, align 4
  store float 0.000000e+00, float* %ref.tmp236, align 4
  %call237 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp226, float* nonnull align 4 dereferenceable(4) %ref.tmp234, float* nonnull align 4 dereferenceable(4) %ref.tmp235, float* nonnull align 4 dereferenceable(4) %ref.tmp236)
  br label %cond.end238

cond.end238:                                      ; preds = %cond.false233, %cond.true228
  %157 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %157, i32 0, i32 9
  %158 = bitcast %class.btVector3* %m_angularComponentB to i8*
  %159 = bitcast %class.btVector3* %ref.tmp226 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %158, i8* align 4 %159, i32 16, i1 false)
  br label %if.end239

if.end239:                                        ; preds = %cond.end238, %if.end160
  %call240 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vec)
  store float 0.000000e+00, float* %denom0, align 4
  store float 0.000000e+00, float* %denom1, align 4
  store float* null, float** %jacB, align 4
  store float* null, float** %jacA, align 4
  store float* null, float** %lambdaA, align 4
  store float* null, float** %lambdaB, align 4
  store i32 0, i32* %ndofA241, align 4
  %160 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %tobool242 = icmp ne %class.btMultiBody* %160, null
  br i1 %tobool242, label %if.then243, label %if.else257

if.then243:                                       ; preds = %if.end239
  %161 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %call244 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %161)
  %add245 = add nsw i32 %call244, 6
  store i32 %add245, i32* %ndofA241, align 4
  %m_data246 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians247 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data246, i32 0, i32 0
  %162 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex248 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %162, i32 0, i32 1
  %163 = load i32, i32* %m_jacAindex248, align 4
  %call249 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_jacobians247, i32 %163)
  store float* %call249, float** %jacA, align 4
  %m_data250 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse251 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data250, i32 0, i32 1
  %164 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex252 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %164, i32 0, i32 1
  %165 = load i32, i32* %m_jacAindex252, align 4
  %call253 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse251, i32 %165)
  store float* %call253, float** %lambdaA, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then243
  %166 = load i32, i32* %i, align 4
  %167 = load i32, i32* %ndofA241, align 4
  %cmp254 = icmp slt i32 %166, %167
  br i1 %cmp254, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %168 = load float*, float** %jacA, align 4
  %169 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %168, i32 %169
  %170 = load float, float* %arrayidx, align 4
  store float %170, float* %j, align 4
  %171 = load float*, float** %lambdaA, align 4
  %172 = load i32, i32* %i, align 4
  %arrayidx255 = getelementptr inbounds float, float* %171, i32 %172
  %173 = load float, float* %arrayidx255, align 4
  store float %173, float* %l, align 4
  %174 = load float, float* %j, align 4
  %175 = load float, float* %l, align 4
  %mul = fmul float %174, %175
  %176 = load float, float* %denom0, align 4
  %add256 = fadd float %176, %mul
  store float %add256, float* %denom0, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %177 = load i32, i32* %i, align 4
  %inc = add nsw i32 %177, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end266

if.else257:                                       ; preds = %if.end239
  %178 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %tobool258 = icmp ne %class.btRigidBody* %178, null
  br i1 %tobool258, label %if.then259, label %if.end265

if.then259:                                       ; preds = %if.else257
  %179 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentA261 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %179, i32 0, i32 8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp260, %class.btVector3* %m_angularComponentA261, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1)
  %180 = bitcast %class.btVector3* %vec to i8*
  %181 = bitcast %class.btVector3* %ref.tmp260 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %180, i8* align 4 %181, i32 16, i1 false)
  %182 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call262 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %182)
  %183 = load %class.btVector3*, %class.btVector3** %constraintNormal.addr, align 4
  %call263 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %183, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %add264 = fadd float %call262, %call263
  store float %add264, float* %denom0, align 4
  br label %if.end265

if.end265:                                        ; preds = %if.then259, %if.else257
  br label %if.end266

if.end266:                                        ; preds = %if.end265, %for.end
  %184 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %tobool267 = icmp ne %class.btMultiBody* %184, null
  br i1 %tobool267, label %if.then268, label %if.else293

if.then268:                                       ; preds = %if.end266
  %185 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %call270 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %185)
  %add271 = add nsw i32 %call270, 6
  store i32 %add271, i32* %ndofB269, align 4
  %m_data272 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians273 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data272, i32 0, i32 0
  %186 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex274 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %186, i32 0, i32 3
  %187 = load i32, i32* %m_jacBindex274, align 4
  %call275 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_jacobians273, i32 %187)
  store float* %call275, float** %jacB, align 4
  %m_data276 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_deltaVelocitiesUnitImpulse277 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data276, i32 0, i32 1
  %188 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex278 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %188, i32 0, i32 3
  %189 = load i32, i32* %m_jacBindex278, align 4
  %call279 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse277, i32 %189)
  store float* %call279, float** %lambdaB, align 4
  store i32 0, i32* %i280, align 4
  br label %for.cond281

for.cond281:                                      ; preds = %for.inc290, %if.then268
  %190 = load i32, i32* %i280, align 4
  %191 = load i32, i32* %ndofB269, align 4
  %cmp282 = icmp slt i32 %190, %191
  br i1 %cmp282, label %for.body283, label %for.end292

for.body283:                                      ; preds = %for.cond281
  %192 = load float*, float** %jacB, align 4
  %193 = load i32, i32* %i280, align 4
  %arrayidx285 = getelementptr inbounds float, float* %192, i32 %193
  %194 = load float, float* %arrayidx285, align 4
  store float %194, float* %j284, align 4
  %195 = load float*, float** %lambdaB, align 4
  %196 = load i32, i32* %i280, align 4
  %arrayidx287 = getelementptr inbounds float, float* %195, i32 %196
  %197 = load float, float* %arrayidx287, align 4
  store float %197, float* %l286, align 4
  %198 = load float, float* %j284, align 4
  %199 = load float, float* %l286, align 4
  %mul288 = fmul float %198, %199
  %200 = load float, float* %denom1, align 4
  %add289 = fadd float %200, %mul288
  store float %add289, float* %denom1, align 4
  br label %for.inc290

for.inc290:                                       ; preds = %for.body283
  %201 = load i32, i32* %i280, align 4
  %inc291 = add nsw i32 %201, 1
  store i32 %inc291, i32* %i280, align 4
  br label %for.cond281

for.end292:                                       ; preds = %for.cond281
  br label %if.end303

if.else293:                                       ; preds = %if.end266
  %202 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %tobool294 = icmp ne %class.btRigidBody* %202, null
  br i1 %tobool294, label %if.then295, label %if.end302

if.then295:                                       ; preds = %if.else293
  %203 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentB298 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %203, i32 0, i32 9
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp297, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB298)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp296, %class.btVector3* %ref.tmp297, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2)
  %204 = bitcast %class.btVector3* %vec to i8*
  %205 = bitcast %class.btVector3* %ref.tmp296 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %204, i8* align 4 %205, i32 16, i1 false)
  %206 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call299 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %206)
  %207 = load %class.btVector3*, %class.btVector3** %constraintNormal.addr, align 4
  %call300 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %207, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %add301 = fadd float %call299, %call300
  store float %add301, float* %denom1, align 4
  br label %if.end302

if.end302:                                        ; preds = %if.then295, %if.else293
  br label %if.end303

if.end303:                                        ; preds = %if.end302, %for.end292
  %208 = load float, float* %denom0, align 4
  %209 = load float, float* %denom1, align 4
  %add304 = fadd float %208, %209
  %210 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %211 = bitcast %struct.btContactSolverInfo* %210 to %struct.btContactSolverInfoData*
  %m_globalCfm = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %211, i32 0, i32 10
  %212 = load float, float* %m_globalCfm, align 4
  %add305 = fadd float %add304, %212
  store float %add305, float* %d, align 4
  %213 = load float, float* %d, align 4
  %cmp306 = fcmp ogt float %213, 0x3E80000000000000
  br i1 %cmp306, label %if.then307, label %if.else308

if.then307:                                       ; preds = %if.end303
  %214 = load float*, float** %relaxation.addr, align 4
  %215 = load float, float* %214, align 4
  %216 = load float, float* %d, align 4
  %div = fdiv float %215, %216
  %217 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %217, i32 0, i32 13
  store float %div, float* %m_jacDiagABInv, align 4
  br label %if.end310

if.else308:                                       ; preds = %if.end303
  %218 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv309 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %218, i32 0, i32 13
  store float 0.000000e+00, float* %m_jacDiagABInv309, align 4
  br label %if.end310

if.end310:                                        ; preds = %if.else308, %if.then307
  store float 0.000000e+00, float* %restitution, align 4
  %219 = load i8, i8* %isFriction.addr, align 1
  %tobool311 = trunc i8 %219 to i1
  br i1 %tobool311, label %cond.true312, label %cond.false313

cond.true312:                                     ; preds = %if.end310
  br label %cond.end315

cond.false313:                                    ; preds = %if.end310
  %220 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %call314 = call float @_ZNK15btManifoldPoint11getDistanceEv(%class.btManifoldPoint* %220)
  br label %cond.end315

cond.end315:                                      ; preds = %cond.false313, %cond.true312
  %cond316 = phi float [ 0.000000e+00, %cond.true312 ], [ %call314, %cond.false313 ]
  store float %cond316, float* %penetration, align 4
  store float 0.000000e+00, float* %rel_vel, align 4
  store i32 0, i32* %ndofA317, align 4
  store i32 0, i32* %ndofB318, align 4
  %call319 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel1)
  %call320 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel2)
  %221 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %tobool321 = icmp ne %class.btMultiBody* %221, null
  br i1 %tobool321, label %if.then322, label %if.else342

if.then322:                                       ; preds = %cond.end315
  %222 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %call323 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %222)
  %add324 = add nsw i32 %call323, 6
  store i32 %add324, i32* %ndofA317, align 4
  %m_data326 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians327 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data326, i32 0, i32 0
  %223 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex328 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %223, i32 0, i32 1
  %224 = load i32, i32* %m_jacAindex328, align 4
  %call329 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_jacobians327, i32 %224)
  store float* %call329, float** %jacA325, align 4
  store i32 0, i32* %i330, align 4
  br label %for.cond331

for.cond331:                                      ; preds = %for.inc339, %if.then322
  %225 = load i32, i32* %i330, align 4
  %226 = load i32, i32* %ndofA317, align 4
  %cmp332 = icmp slt i32 %225, %226
  br i1 %cmp332, label %for.body333, label %for.end341

for.body333:                                      ; preds = %for.cond331
  %227 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %call334 = call float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %227)
  %228 = load i32, i32* %i330, align 4
  %arrayidx335 = getelementptr inbounds float, float* %call334, i32 %228
  %229 = load float, float* %arrayidx335, align 4
  %230 = load float*, float** %jacA325, align 4
  %231 = load i32, i32* %i330, align 4
  %arrayidx336 = getelementptr inbounds float, float* %230, i32 %231
  %232 = load float, float* %arrayidx336, align 4
  %mul337 = fmul float %229, %232
  %233 = load float, float* %rel_vel, align 4
  %add338 = fadd float %233, %mul337
  store float %add338, float* %rel_vel, align 4
  br label %for.inc339

for.inc339:                                       ; preds = %for.body333
  %234 = load i32, i32* %i330, align 4
  %inc340 = add nsw i32 %234, 1
  store i32 %inc340, i32* %i330, align 4
  br label %for.cond331

for.end341:                                       ; preds = %for.cond331
  br label %if.end350

if.else342:                                       ; preds = %cond.end315
  %235 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %tobool343 = icmp ne %class.btRigidBody* %235, null
  br i1 %tobool343, label %if.then344, label %if.end349

if.then344:                                       ; preds = %if.else342
  %236 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp345, %class.btRigidBody* %236, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1)
  %237 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal1346 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %237, i32 0, i32 5
  %call347 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp345, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1346)
  %238 = load float, float* %rel_vel, align 4
  %add348 = fadd float %238, %call347
  store float %add348, float* %rel_vel, align 4
  br label %if.end349

if.end349:                                        ; preds = %if.then344, %if.else342
  br label %if.end350

if.end350:                                        ; preds = %if.end349, %for.end341
  %239 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %tobool351 = icmp ne %class.btMultiBody* %239, null
  br i1 %tobool351, label %if.then352, label %if.else372

if.then352:                                       ; preds = %if.end350
  %240 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %call353 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %240)
  %add354 = add nsw i32 %call353, 6
  store i32 %add354, i32* %ndofB318, align 4
  %m_data356 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_jacobians357 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data356, i32 0, i32 0
  %241 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex358 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %241, i32 0, i32 3
  %242 = load i32, i32* %m_jacBindex358, align 4
  %call359 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_jacobians357, i32 %242)
  store float* %call359, float** %jacB355, align 4
  store i32 0, i32* %i360, align 4
  br label %for.cond361

for.cond361:                                      ; preds = %for.inc369, %if.then352
  %243 = load i32, i32* %i360, align 4
  %244 = load i32, i32* %ndofB318, align 4
  %cmp362 = icmp slt i32 %243, %244
  br i1 %cmp362, label %for.body363, label %for.end371

for.body363:                                      ; preds = %for.cond361
  %245 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %call364 = call float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %245)
  %246 = load i32, i32* %i360, align 4
  %arrayidx365 = getelementptr inbounds float, float* %call364, i32 %246
  %247 = load float, float* %arrayidx365, align 4
  %248 = load float*, float** %jacB355, align 4
  %249 = load i32, i32* %i360, align 4
  %arrayidx366 = getelementptr inbounds float, float* %248, i32 %249
  %250 = load float, float* %arrayidx366, align 4
  %mul367 = fmul float %247, %250
  %251 = load float, float* %rel_vel, align 4
  %add368 = fadd float %251, %mul367
  store float %add368, float* %rel_vel, align 4
  br label %for.inc369

for.inc369:                                       ; preds = %for.body363
  %252 = load i32, i32* %i360, align 4
  %inc370 = add nsw i32 %252, 1
  store i32 %inc370, i32* %i360, align 4
  br label %for.cond361

for.end371:                                       ; preds = %for.cond361
  br label %if.end380

if.else372:                                       ; preds = %if.end350
  %253 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %tobool373 = icmp ne %class.btRigidBody* %253, null
  br i1 %tobool373, label %if.then374, label %if.end379

if.then374:                                       ; preds = %if.else372
  %254 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp375, %class.btRigidBody* %254, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2)
  %255 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal2376 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %255, i32 0, i32 7
  %call377 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp375, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2376)
  %256 = load float, float* %rel_vel, align 4
  %add378 = fadd float %256, %call377
  store float %add378, float* %rel_vel, align 4
  br label %if.end379

if.end379:                                        ; preds = %if.then374, %if.else372
  br label %if.end380

if.end380:                                        ; preds = %if.end379, %for.end371
  %257 = load float, float* %combinedTorsionalFriction.addr, align 4
  %258 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_friction = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %258, i32 0, i32 12
  store float %257, float* %m_friction, align 4
  %259 = load i8, i8* %isFriction.addr, align 1
  %tobool381 = trunc i8 %259 to i1
  br i1 %tobool381, label %if.end387, label %if.then382

if.then382:                                       ; preds = %if.end380
  %260 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %261 = load float, float* %rel_vel, align 4
  %262 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_combinedRestitution = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %262, i32 0, i32 9
  %263 = load float, float* %m_combinedRestitution, align 4
  %call383 = call float @_ZN35btSequentialImpulseConstraintSolver16restitutionCurveEff(%class.btSequentialImpulseConstraintSolver* %260, float %261, float %263)
  store float %call383, float* %restitution, align 4
  %264 = load float, float* %restitution, align 4
  %cmp384 = fcmp ole float %264, 0.000000e+00
  br i1 %cmp384, label %if.then385, label %if.end386

if.then385:                                       ; preds = %if.then382
  store float 0.000000e+00, float* %restitution, align 4
  br label %if.end386

if.end386:                                        ; preds = %if.then385, %if.then382
  br label %if.end387

if.end387:                                        ; preds = %if.end386, %if.end380
  %265 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_appliedImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %265, i32 0, i32 11
  store float 0.000000e+00, float* %m_appliedImpulse, align 4
  %266 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_appliedPushImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %266, i32 0, i32 10
  store float 0.000000e+00, float* %m_appliedPushImpulse, align 4
  %267 = load float, float* %restitution, align 4
  %268 = load float, float* %rel_vel, align 4
  %sub = fsub float %267, %268
  store float %sub, float* %velocityError, align 4
  %269 = load float, float* %penetration, align 4
  %cmp388 = fcmp ogt float %269, 0.000000e+00
  br i1 %cmp388, label %if.then389, label %if.end392

if.then389:                                       ; preds = %if.end387
  %270 = load float, float* %penetration, align 4
  %271 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %272 = bitcast %struct.btContactSolverInfo* %271 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %272, i32 0, i32 3
  %273 = load float, float* %m_timeStep, align 4
  %div390 = fdiv float %270, %273
  %274 = load float, float* %velocityError, align 4
  %sub391 = fsub float %274, %div390
  store float %sub391, float* %velocityError, align 4
  br label %if.end392

if.end392:                                        ; preds = %if.then389, %if.end387
  %275 = load float, float* %velocityError, align 4
  %276 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv393 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %276, i32 0, i32 13
  %277 = load float, float* %m_jacDiagABInv393, align 4
  %mul394 = fmul float %275, %277
  store float %mul394, float* %velocityImpulse, align 4
  %278 = load float, float* %velocityImpulse, align 4
  %279 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_rhs = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %279, i32 0, i32 14
  store float %278, float* %m_rhs, align 4
  %280 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_rhsPenetration = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %280, i32 0, i32 18
  store float 0.000000e+00, float* %m_rhsPenetration, align 4
  %281 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_friction395 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %281, i32 0, i32 12
  %282 = load float, float* %m_friction395, align 4
  %fneg = fneg float %282
  %283 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_lowerLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %283, i32 0, i32 16
  store float %fneg, float* %m_lowerLimit, align 4
  %284 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_friction396 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %284, i32 0, i32 12
  %285 = load float, float* %m_friction396, align 4
  %286 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_upperLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %286, i32 0, i32 17
  store float %285, float* %m_upperLimit, align 4
  %287 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %288 = bitcast %struct.btContactSolverInfo* %287 to %struct.btContactSolverInfoData*
  %m_globalCfm397 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %288, i32 0, i32 10
  %289 = load float, float* %m_globalCfm397, align 4
  %290 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv398 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %290, i32 0, i32 13
  %291 = load float, float* %m_jacDiagABInv398, align 4
  %mul399 = fmul float %289, %291
  %292 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_cfm = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %292, i32 0, i32 15
  store float %mul399, float* %m_cfm, align 4
  %call400 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #7
  ret void
}

declare void @_ZNK11btMultiBody30fillConstraintJacobianMultiDofEiRK9btVector3S2_S2_PfR20btAlignedObjectArrayIfERS4_IS0_ERS4_I11btMatrix3x3E(%class.btMultiBody*, i32, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), float*, %class.btAlignedObjectArray.34* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17)) #3

; Function Attrs: noinline optnone
define hidden nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodyConstraintSolver30addMultiBodyFrictionConstraintERK9btVector3P20btPersistentManifoldiR15btManifoldPointP17btCollisionObjectS8_fRK19btContactSolverInfoff(%class.btMultiBodyConstraintSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %normalAxis, %class.btPersistentManifold* %manifold, i32 %frictionIndex, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %cp, %class.btCollisionObject* %colObj0, %class.btCollisionObject* %colObj1, float %relaxation, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal, float %desiredVelocity, float %cfmSlip) #2 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %normalAxis.addr = alloca %class.btVector3*, align 4
  %manifold.addr = alloca %class.btPersistentManifold*, align 4
  %frictionIndex.addr = alloca i32, align 4
  %cp.addr = alloca %class.btManifoldPoint*, align 4
  %colObj0.addr = alloca %class.btCollisionObject*, align 4
  %colObj1.addr = alloca %class.btCollisionObject*, align 4
  %relaxation.addr = alloca float, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %desiredVelocity.addr = alloca float, align 4
  %cfmSlip.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %solverConstraint = alloca %struct.btMultiBodySolverConstraint*, align 4
  %isFriction = alloca i8, align 1
  %fcA = alloca %class.btMultiBodyLinkCollider*, align 4
  %fcB = alloca %class.btMultiBodyLinkCollider*, align 4
  %mbA = alloca %class.btMultiBody*, align 4
  %mbB = alloca %class.btMultiBody*, align 4
  %solverBodyIdA = alloca i32, align 4
  %solverBodyIdB = alloca i32, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  store %class.btVector3* %normalAxis, %class.btVector3** %normalAxis.addr, align 4
  store %class.btPersistentManifold* %manifold, %class.btPersistentManifold** %manifold.addr, align 4
  store i32 %frictionIndex, i32* %frictionIndex.addr, align 4
  store %class.btManifoldPoint* %cp, %class.btManifoldPoint** %cp.addr, align 4
  store %class.btCollisionObject* %colObj0, %class.btCollisionObject** %colObj0.addr, align 4
  store %class.btCollisionObject* %colObj1, %class.btCollisionObject** %colObj1.addr, align 4
  store float %relaxation, float* %relaxation.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  store float %desiredVelocity, float* %desiredVelocity.addr, align 4
  store float %cfmSlip, float* %cfmSlip.addr, align 4
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.2, i32 0, i32 0))
  %m_multiBodyFrictionContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 3
  %call2 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.22* %m_multiBodyFrictionContactConstraints)
  store %struct.btMultiBodySolverConstraint* %call2, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_orgConstraint = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %0, i32 0, i32 28
  store %class.btMultiBodyConstraint* null, %class.btMultiBodyConstraint** %m_orgConstraint, align 4
  %1 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_orgDofIndex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %1, i32 0, i32 29
  store i32 -1, i32* %m_orgDofIndex, align 4
  %2 = load i32, i32* %frictionIndex.addr, align 4
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_frictionIndex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %3, i32 0, i32 21
  store i32 %2, i32* %m_frictionIndex, align 4
  store i8 1, i8* %isFriction, align 1
  %4 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %call3 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %4)
  %call4 = call %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEPK17btCollisionObject(%class.btCollisionObject* %call3)
  store %class.btMultiBodyLinkCollider* %call4, %class.btMultiBodyLinkCollider** %fcA, align 4
  %5 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %call5 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %5)
  %call6 = call %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEPK17btCollisionObject(%class.btCollisionObject* %call5)
  store %class.btMultiBodyLinkCollider* %call6, %class.btMultiBodyLinkCollider** %fcB, align 4
  %6 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4
  %tobool = icmp ne %class.btMultiBodyLinkCollider* %6, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %7 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4
  %m_multiBody = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %7, i32 0, i32 1
  %8 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBody, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btMultiBody* [ %8, %cond.true ], [ null, %cond.false ]
  store %class.btMultiBody* %cond, %class.btMultiBody** %mbA, align 4
  %9 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcB, align 4
  %tobool7 = icmp ne %class.btMultiBodyLinkCollider* %9, null
  br i1 %tobool7, label %cond.true8, label %cond.false10

cond.true8:                                       ; preds = %cond.end
  %10 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcB, align 4
  %m_multiBody9 = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %10, i32 0, i32 1
  %11 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBody9, align 4
  br label %cond.end11

cond.false10:                                     ; preds = %cond.end
  br label %cond.end11

cond.end11:                                       ; preds = %cond.false10, %cond.true8
  %cond12 = phi %class.btMultiBody* [ %11, %cond.true8 ], [ null, %cond.false10 ]
  store %class.btMultiBody* %cond12, %class.btMultiBody** %mbB, align 4
  %12 = load %class.btMultiBody*, %class.btMultiBody** %mbA, align 4
  %tobool13 = icmp ne %class.btMultiBody* %12, null
  br i1 %tobool13, label %cond.true14, label %cond.false15

cond.true14:                                      ; preds = %cond.end11
  br label %cond.end17

cond.false15:                                     ; preds = %cond.end11
  %13 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %14 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0.addr, align 4
  %15 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %16 = bitcast %struct.btContactSolverInfo* %15 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %16, i32 0, i32 3
  %17 = load float, float* %m_timeStep, align 4
  %call16 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %13, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %14, float %17)
  br label %cond.end17

cond.end17:                                       ; preds = %cond.false15, %cond.true14
  %cond18 = phi i32 [ -1, %cond.true14 ], [ %call16, %cond.false15 ]
  store i32 %cond18, i32* %solverBodyIdA, align 4
  %18 = load %class.btMultiBody*, %class.btMultiBody** %mbB, align 4
  %tobool19 = icmp ne %class.btMultiBody* %18, null
  br i1 %tobool19, label %cond.true20, label %cond.false21

cond.true20:                                      ; preds = %cond.end17
  br label %cond.end24

cond.false21:                                     ; preds = %cond.end17
  %19 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %20 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1.addr, align 4
  %21 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %22 = bitcast %struct.btContactSolverInfo* %21 to %struct.btContactSolverInfoData*
  %m_timeStep22 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %22, i32 0, i32 3
  %23 = load float, float* %m_timeStep22, align 4
  %call23 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %19, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %20, float %23)
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false21, %cond.true20
  %cond25 = phi i32 [ -1, %cond.true20 ], [ %call23, %cond.false21 ]
  store i32 %cond25, i32* %solverBodyIdB, align 4
  %24 = load i32, i32* %solverBodyIdA, align 4
  %25 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_solverBodyIdA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %25, i32 0, i32 22
  store i32 %24, i32* %m_solverBodyIdA, align 4
  %26 = load i32, i32* %solverBodyIdB, align 4
  %27 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_solverBodyIdB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %27, i32 0, i32 25
  store i32 %26, i32* %m_solverBodyIdB, align 4
  %28 = load %class.btMultiBody*, %class.btMultiBody** %mbA, align 4
  %29 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_multiBodyA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %29, i32 0, i32 23
  store %class.btMultiBody* %28, %class.btMultiBody** %m_multiBodyA, align 4
  %30 = load %class.btMultiBody*, %class.btMultiBody** %mbA, align 4
  %tobool26 = icmp ne %class.btMultiBody* %30, null
  br i1 %tobool26, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end24
  %31 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4
  %m_link = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %31, i32 0, i32 2
  %32 = load i32, i32* %m_link, align 4
  %33 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_linkA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %33, i32 0, i32 24
  store i32 %32, i32* %m_linkA, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end24
  %34 = load %class.btMultiBody*, %class.btMultiBody** %mbB, align 4
  %35 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_multiBodyB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %35, i32 0, i32 26
  store %class.btMultiBody* %34, %class.btMultiBody** %m_multiBodyB, align 4
  %36 = load %class.btMultiBody*, %class.btMultiBody** %mbB, align 4
  %tobool27 = icmp ne %class.btMultiBody* %36, null
  br i1 %tobool27, label %if.then28, label %if.end30

if.then28:                                        ; preds = %if.end
  %37 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcB, align 4
  %m_link29 = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %37, i32 0, i32 2
  %38 = load i32, i32* %m_link29, align 4
  %39 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_linkB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %39, i32 0, i32 27
  store i32 %38, i32* %m_linkB, align 4
  br label %if.end30

if.end30:                                         ; preds = %if.then28, %if.end
  %40 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %41 = bitcast %class.btManifoldPoint* %40 to i8*
  %42 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %43 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %42, i32 0, i32 19
  %m_originalContactPoint = bitcast %union.anon.25* %43 to i8**
  store i8* %41, i8** %m_originalContactPoint, align 4
  %44 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %45 = load %class.btVector3*, %class.btVector3** %normalAxis.addr, align 4
  %46 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %47 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %48 = load i8, i8* %isFriction, align 1
  %tobool31 = trunc i8 %48 to i1
  %49 = load float, float* %desiredVelocity.addr, align 4
  %50 = load float, float* %cfmSlip.addr, align 4
  call void @_ZN27btMultiBodyConstraintSolver31setupMultiBodyContactConstraintER27btMultiBodySolverConstraintRK9btVector3R15btManifoldPointRK19btContactSolverInfoRfbff(%class.btMultiBodyConstraintSolver* %this1, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %44, %class.btVector3* nonnull align 4 dereferenceable(16) %45, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %46, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %47, float* nonnull align 4 dereferenceable(4) %relaxation.addr, i1 zeroext %tobool31, float %49, float %50)
  %51 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %call32 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #7
  ret %struct.btMultiBodySolverConstraint* %51
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.22* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.22* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.22* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi(%class.btAlignedObjectArray.22* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  %1 = load i32, i32* %m_size, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %2 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4
  %3 = load i32, i32* %sz, align 4
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %2, i32 %3
  ret %struct.btMultiBodySolverConstraint* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEPK17btCollisionObject(%class.btCollisionObject* %colObj) #1 comdat {
entry:
  %retval = alloca %class.btMultiBodyLinkCollider*, align 4
  %colObj.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %colObj, %class.btCollisionObject** %colObj.addr, align 4
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4
  %call = call i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %0)
  %and = and i32 %call, 64
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4
  %2 = bitcast %class.btCollisionObject* %1 to %class.btMultiBodyLinkCollider*
  store %class.btMultiBodyLinkCollider* %2, %class.btMultiBodyLinkCollider** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store %class.btMultiBodyLinkCollider* null, %class.btMultiBodyLinkCollider** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %retval, align 4
  ret %class.btMultiBodyLinkCollider* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4
  ret %class.btCollisionObject* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body1 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 3
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body1, align 4
  ret %class.btCollisionObject* %0
}

declare i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject* nonnull align 4 dereferenceable(324), float) #3

; Function Attrs: noinline optnone
define hidden nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodyConstraintSolver39addMultiBodyTorsionalFrictionConstraintERK9btVector3P20btPersistentManifoldiR15btManifoldPointfP17btCollisionObjectS8_fRK19btContactSolverInfoff(%class.btMultiBodyConstraintSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %normalAxis, %class.btPersistentManifold* %manifold, i32 %frictionIndex, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %cp, float %combinedTorsionalFriction, %class.btCollisionObject* %colObj0, %class.btCollisionObject* %colObj1, float %relaxation, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal, float %desiredVelocity, float %cfmSlip) #2 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %normalAxis.addr = alloca %class.btVector3*, align 4
  %manifold.addr = alloca %class.btPersistentManifold*, align 4
  %frictionIndex.addr = alloca i32, align 4
  %cp.addr = alloca %class.btManifoldPoint*, align 4
  %combinedTorsionalFriction.addr = alloca float, align 4
  %colObj0.addr = alloca %class.btCollisionObject*, align 4
  %colObj1.addr = alloca %class.btCollisionObject*, align 4
  %relaxation.addr = alloca float, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %desiredVelocity.addr = alloca float, align 4
  %cfmSlip.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %solverConstraint = alloca %struct.btMultiBodySolverConstraint*, align 4
  %isFriction = alloca i8, align 1
  %fcA = alloca %class.btMultiBodyLinkCollider*, align 4
  %fcB = alloca %class.btMultiBodyLinkCollider*, align 4
  %mbA = alloca %class.btMultiBody*, align 4
  %mbB = alloca %class.btMultiBody*, align 4
  %solverBodyIdA = alloca i32, align 4
  %solverBodyIdB = alloca i32, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  store %class.btVector3* %normalAxis, %class.btVector3** %normalAxis.addr, align 4
  store %class.btPersistentManifold* %manifold, %class.btPersistentManifold** %manifold.addr, align 4
  store i32 %frictionIndex, i32* %frictionIndex.addr, align 4
  store %class.btManifoldPoint* %cp, %class.btManifoldPoint** %cp.addr, align 4
  store float %combinedTorsionalFriction, float* %combinedTorsionalFriction.addr, align 4
  store %class.btCollisionObject* %colObj0, %class.btCollisionObject** %colObj0.addr, align 4
  store %class.btCollisionObject* %colObj1, %class.btCollisionObject** %colObj1.addr, align 4
  store float %relaxation, float* %relaxation.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  store float %desiredVelocity, float* %desiredVelocity.addr, align 4
  store float %cfmSlip, float* %cfmSlip.addr, align 4
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.3, i32 0, i32 0))
  %m_multiBodyFrictionContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 3
  %call2 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.22* %m_multiBodyFrictionContactConstraints)
  store %struct.btMultiBodySolverConstraint* %call2, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_orgConstraint = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %0, i32 0, i32 28
  store %class.btMultiBodyConstraint* null, %class.btMultiBodyConstraint** %m_orgConstraint, align 4
  %1 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_orgDofIndex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %1, i32 0, i32 29
  store i32 -1, i32* %m_orgDofIndex, align 4
  %2 = load i32, i32* %frictionIndex.addr, align 4
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_frictionIndex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %3, i32 0, i32 21
  store i32 %2, i32* %m_frictionIndex, align 4
  store i8 1, i8* %isFriction, align 1
  %4 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %call3 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %4)
  %call4 = call %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEPK17btCollisionObject(%class.btCollisionObject* %call3)
  store %class.btMultiBodyLinkCollider* %call4, %class.btMultiBodyLinkCollider** %fcA, align 4
  %5 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %call5 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %5)
  %call6 = call %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEPK17btCollisionObject(%class.btCollisionObject* %call5)
  store %class.btMultiBodyLinkCollider* %call6, %class.btMultiBodyLinkCollider** %fcB, align 4
  %6 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4
  %tobool = icmp ne %class.btMultiBodyLinkCollider* %6, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %7 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4
  %m_multiBody = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %7, i32 0, i32 1
  %8 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBody, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btMultiBody* [ %8, %cond.true ], [ null, %cond.false ]
  store %class.btMultiBody* %cond, %class.btMultiBody** %mbA, align 4
  %9 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcB, align 4
  %tobool7 = icmp ne %class.btMultiBodyLinkCollider* %9, null
  br i1 %tobool7, label %cond.true8, label %cond.false10

cond.true8:                                       ; preds = %cond.end
  %10 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcB, align 4
  %m_multiBody9 = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %10, i32 0, i32 1
  %11 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBody9, align 4
  br label %cond.end11

cond.false10:                                     ; preds = %cond.end
  br label %cond.end11

cond.end11:                                       ; preds = %cond.false10, %cond.true8
  %cond12 = phi %class.btMultiBody* [ %11, %cond.true8 ], [ null, %cond.false10 ]
  store %class.btMultiBody* %cond12, %class.btMultiBody** %mbB, align 4
  %12 = load %class.btMultiBody*, %class.btMultiBody** %mbA, align 4
  %tobool13 = icmp ne %class.btMultiBody* %12, null
  br i1 %tobool13, label %cond.true14, label %cond.false15

cond.true14:                                      ; preds = %cond.end11
  br label %cond.end17

cond.false15:                                     ; preds = %cond.end11
  %13 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %14 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0.addr, align 4
  %15 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %16 = bitcast %struct.btContactSolverInfo* %15 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %16, i32 0, i32 3
  %17 = load float, float* %m_timeStep, align 4
  %call16 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %13, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %14, float %17)
  br label %cond.end17

cond.end17:                                       ; preds = %cond.false15, %cond.true14
  %cond18 = phi i32 [ -1, %cond.true14 ], [ %call16, %cond.false15 ]
  store i32 %cond18, i32* %solverBodyIdA, align 4
  %18 = load %class.btMultiBody*, %class.btMultiBody** %mbB, align 4
  %tobool19 = icmp ne %class.btMultiBody* %18, null
  br i1 %tobool19, label %cond.true20, label %cond.false21

cond.true20:                                      ; preds = %cond.end17
  br label %cond.end24

cond.false21:                                     ; preds = %cond.end17
  %19 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %20 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1.addr, align 4
  %21 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %22 = bitcast %struct.btContactSolverInfo* %21 to %struct.btContactSolverInfoData*
  %m_timeStep22 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %22, i32 0, i32 3
  %23 = load float, float* %m_timeStep22, align 4
  %call23 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %19, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %20, float %23)
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false21, %cond.true20
  %cond25 = phi i32 [ -1, %cond.true20 ], [ %call23, %cond.false21 ]
  store i32 %cond25, i32* %solverBodyIdB, align 4
  %24 = load i32, i32* %solverBodyIdA, align 4
  %25 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_solverBodyIdA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %25, i32 0, i32 22
  store i32 %24, i32* %m_solverBodyIdA, align 4
  %26 = load i32, i32* %solverBodyIdB, align 4
  %27 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_solverBodyIdB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %27, i32 0, i32 25
  store i32 %26, i32* %m_solverBodyIdB, align 4
  %28 = load %class.btMultiBody*, %class.btMultiBody** %mbA, align 4
  %29 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_multiBodyA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %29, i32 0, i32 23
  store %class.btMultiBody* %28, %class.btMultiBody** %m_multiBodyA, align 4
  %30 = load %class.btMultiBody*, %class.btMultiBody** %mbA, align 4
  %tobool26 = icmp ne %class.btMultiBody* %30, null
  br i1 %tobool26, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end24
  %31 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4
  %m_link = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %31, i32 0, i32 2
  %32 = load i32, i32* %m_link, align 4
  %33 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_linkA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %33, i32 0, i32 24
  store i32 %32, i32* %m_linkA, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end24
  %34 = load %class.btMultiBody*, %class.btMultiBody** %mbB, align 4
  %35 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_multiBodyB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %35, i32 0, i32 26
  store %class.btMultiBody* %34, %class.btMultiBody** %m_multiBodyB, align 4
  %36 = load %class.btMultiBody*, %class.btMultiBody** %mbB, align 4
  %tobool27 = icmp ne %class.btMultiBody* %36, null
  br i1 %tobool27, label %if.then28, label %if.end30

if.then28:                                        ; preds = %if.end
  %37 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcB, align 4
  %m_link29 = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %37, i32 0, i32 2
  %38 = load i32, i32* %m_link29, align 4
  %39 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_linkB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %39, i32 0, i32 27
  store i32 %38, i32* %m_linkB, align 4
  br label %if.end30

if.end30:                                         ; preds = %if.then28, %if.end
  %40 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %41 = bitcast %class.btManifoldPoint* %40 to i8*
  %42 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %43 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %42, i32 0, i32 19
  %m_originalContactPoint = bitcast %union.anon.25* %43 to i8**
  store i8* %41, i8** %m_originalContactPoint, align 4
  %44 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %45 = load %class.btVector3*, %class.btVector3** %normalAxis.addr, align 4
  %46 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %47 = load float, float* %combinedTorsionalFriction.addr, align 4
  %48 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %49 = load i8, i8* %isFriction, align 1
  %tobool31 = trunc i8 %49 to i1
  %50 = load float, float* %desiredVelocity.addr, align 4
  %51 = load float, float* %cfmSlip.addr, align 4
  call void @_ZN27btMultiBodyConstraintSolver41setupMultiBodyTorsionalFrictionConstraintER27btMultiBodySolverConstraintRK9btVector3R15btManifoldPointfRK19btContactSolverInfoRfbff(%class.btMultiBodyConstraintSolver* %this1, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %44, %class.btVector3* nonnull align 4 dereferenceable(16) %45, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %46, float %47, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %48, float* nonnull align 4 dereferenceable(4) %relaxation.addr, i1 zeroext %tobool31, float %50, float %51)
  %52 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %call32 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #7
  ret %struct.btMultiBodySolverConstraint* %52
}

; Function Attrs: noinline optnone
define hidden void @_ZN27btMultiBodyConstraintSolver23convertMultiBodyContactEP20btPersistentManifoldRK19btContactSolverInfo(%class.btMultiBodyConstraintSolver* %this, %class.btPersistentManifold* %manifold, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal) #2 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %manifold.addr = alloca %class.btPersistentManifold*, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %fcA = alloca %class.btMultiBodyLinkCollider*, align 4
  %fcB = alloca %class.btMultiBodyLinkCollider*, align 4
  %mbA = alloca %class.btMultiBody*, align 4
  %mbB = alloca %class.btMultiBody*, align 4
  %colObj0 = alloca %class.btCollisionObject*, align 4
  %colObj1 = alloca %class.btCollisionObject*, align 4
  %solverBodyIdA = alloca i32, align 4
  %solverBodyIdB = alloca i32, align 4
  %rollingFriction = alloca i32, align 4
  %j = alloca i32, align 4
  %cp = alloca %class.btManifoldPoint*, align 4
  %relaxation = alloca float, align 4
  %frictionIndex = alloca i32, align 4
  %solverConstraint = alloca %struct.btMultiBodySolverConstraint*, align 4
  %isFriction = alloca i8, align 1
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  store %class.btPersistentManifold* %manifold, %class.btPersistentManifold** %manifold.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %call = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %0)
  %call2 = call %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEPK17btCollisionObject(%class.btCollisionObject* %call)
  store %class.btMultiBodyLinkCollider* %call2, %class.btMultiBodyLinkCollider** %fcA, align 4
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %call3 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %1)
  %call4 = call %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEPK17btCollisionObject(%class.btCollisionObject* %call3)
  store %class.btMultiBodyLinkCollider* %call4, %class.btMultiBodyLinkCollider** %fcB, align 4
  %2 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4
  %tobool = icmp ne %class.btMultiBodyLinkCollider* %2, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %3 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4
  %m_multiBody = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %3, i32 0, i32 1
  %4 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBody, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btMultiBody* [ %4, %cond.true ], [ null, %cond.false ]
  store %class.btMultiBody* %cond, %class.btMultiBody** %mbA, align 4
  %5 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcB, align 4
  %tobool5 = icmp ne %class.btMultiBodyLinkCollider* %5, null
  br i1 %tobool5, label %cond.true6, label %cond.false8

cond.true6:                                       ; preds = %cond.end
  %6 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcB, align 4
  %m_multiBody7 = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %6, i32 0, i32 1
  %7 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBody7, align 4
  br label %cond.end9

cond.false8:                                      ; preds = %cond.end
  br label %cond.end9

cond.end9:                                        ; preds = %cond.false8, %cond.true6
  %cond10 = phi %class.btMultiBody* [ %7, %cond.true6 ], [ null, %cond.false8 ]
  store %class.btMultiBody* %cond10, %class.btMultiBody** %mbB, align 4
  store %class.btCollisionObject* null, %class.btCollisionObject** %colObj0, align 4
  store %class.btCollisionObject* null, %class.btCollisionObject** %colObj1, align 4
  %8 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %call11 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %8)
  store %class.btCollisionObject* %call11, %class.btCollisionObject** %colObj0, align 4
  %9 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %call12 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %9)
  store %class.btCollisionObject* %call12, %class.btCollisionObject** %colObj1, align 4
  %10 = load %class.btMultiBody*, %class.btMultiBody** %mbA, align 4
  %tobool13 = icmp ne %class.btMultiBody* %10, null
  br i1 %tobool13, label %cond.true14, label %cond.false15

cond.true14:                                      ; preds = %cond.end9
  br label %cond.end17

cond.false15:                                     ; preds = %cond.end9
  %11 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %12 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %13 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %14 = bitcast %struct.btContactSolverInfo* %13 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %14, i32 0, i32 3
  %15 = load float, float* %m_timeStep, align 4
  %call16 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %11, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %12, float %15)
  br label %cond.end17

cond.end17:                                       ; preds = %cond.false15, %cond.true14
  %cond18 = phi i32 [ -1, %cond.true14 ], [ %call16, %cond.false15 ]
  store i32 %cond18, i32* %solverBodyIdA, align 4
  %16 = load %class.btMultiBody*, %class.btMultiBody** %mbB, align 4
  %tobool19 = icmp ne %class.btMultiBody* %16, null
  br i1 %tobool19, label %cond.true20, label %cond.false21

cond.true20:                                      ; preds = %cond.end17
  br label %cond.end24

cond.false21:                                     ; preds = %cond.end17
  %17 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %18 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %19 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %20 = bitcast %struct.btContactSolverInfo* %19 to %struct.btContactSolverInfoData*
  %m_timeStep22 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %20, i32 0, i32 3
  %21 = load float, float* %m_timeStep22, align 4
  %call23 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %17, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %18, float %21)
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false21, %cond.true20
  %cond25 = phi i32 [ -1, %cond.true20 ], [ %call23, %cond.false21 ]
  store i32 %cond25, i32* %solverBodyIdB, align 4
  store i32 1, i32* %rollingFriction, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end24
  %22 = load i32, i32* %j, align 4
  %23 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %call26 = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %23)
  %cmp = icmp slt i32 %22, %call26
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %24 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %25 = load i32, i32* %j, align 4
  %call27 = call nonnull align 4 dereferenceable(192) %class.btManifoldPoint* @_ZN20btPersistentManifold15getContactPointEi(%class.btPersistentManifold* %24, i32 %25)
  store %class.btManifoldPoint* %call27, %class.btManifoldPoint** %cp, align 4
  %26 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %call28 = call float @_ZNK15btManifoldPoint11getDistanceEv(%class.btManifoldPoint* %26)
  %27 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %call29 = call float @_ZNK20btPersistentManifold29getContactProcessingThresholdEv(%class.btPersistentManifold* %27)
  %cmp30 = fcmp ole float %call28, %call29
  br i1 %cmp30, label %if.then, label %if.end89

if.then:                                          ; preds = %for.body
  %m_multiBodyNormalContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 2
  %call31 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.22* %m_multiBodyNormalContactConstraints)
  store i32 %call31, i32* %frictionIndex, align 4
  %m_multiBodyNormalContactConstraints32 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 2
  %call33 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.22* %m_multiBodyNormalContactConstraints32)
  store %struct.btMultiBodySolverConstraint* %call33, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %28 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_orgConstraint = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %28, i32 0, i32 28
  store %class.btMultiBodyConstraint* null, %class.btMultiBodyConstraint** %m_orgConstraint, align 4
  %29 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_orgDofIndex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %29, i32 0, i32 29
  store i32 -1, i32* %m_orgDofIndex, align 4
  %30 = load i32, i32* %solverBodyIdA, align 4
  %31 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_solverBodyIdA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %31, i32 0, i32 22
  store i32 %30, i32* %m_solverBodyIdA, align 4
  %32 = load i32, i32* %solverBodyIdB, align 4
  %33 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_solverBodyIdB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %33, i32 0, i32 25
  store i32 %32, i32* %m_solverBodyIdB, align 4
  %34 = load %class.btMultiBody*, %class.btMultiBody** %mbA, align 4
  %35 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_multiBodyA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %35, i32 0, i32 23
  store %class.btMultiBody* %34, %class.btMultiBody** %m_multiBodyA, align 4
  %36 = load %class.btMultiBody*, %class.btMultiBody** %mbA, align 4
  %tobool34 = icmp ne %class.btMultiBody* %36, null
  br i1 %tobool34, label %if.then35, label %if.end

if.then35:                                        ; preds = %if.then
  %37 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4
  %m_link = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %37, i32 0, i32 2
  %38 = load i32, i32* %m_link, align 4
  %39 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_linkA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %39, i32 0, i32 24
  store i32 %38, i32* %m_linkA, align 4
  br label %if.end

if.end:                                           ; preds = %if.then35, %if.then
  %40 = load %class.btMultiBody*, %class.btMultiBody** %mbB, align 4
  %41 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_multiBodyB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %41, i32 0, i32 26
  store %class.btMultiBody* %40, %class.btMultiBody** %m_multiBodyB, align 4
  %42 = load %class.btMultiBody*, %class.btMultiBody** %mbB, align 4
  %tobool36 = icmp ne %class.btMultiBody* %42, null
  br i1 %tobool36, label %if.then37, label %if.end39

if.then37:                                        ; preds = %if.end
  %43 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcB, align 4
  %m_link38 = getelementptr inbounds %class.btMultiBodyLinkCollider, %class.btMultiBodyLinkCollider* %43, i32 0, i32 2
  %44 = load i32, i32* %m_link38, align 4
  %45 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_linkB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %45, i32 0, i32 27
  store i32 %44, i32* %m_linkB, align 4
  br label %if.end39

if.end39:                                         ; preds = %if.then37, %if.end
  %46 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %47 = bitcast %class.btManifoldPoint* %46 to i8*
  %48 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %49 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %48, i32 0, i32 19
  %m_originalContactPoint = bitcast %union.anon.25* %49 to i8**
  store i8* %47, i8** %m_originalContactPoint, align 4
  store i8 0, i8* %isFriction, align 1
  %50 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %51 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_normalWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %51, i32 0, i32 4
  %52 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %53 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %54 = load i8, i8* %isFriction, align 1
  %tobool40 = trunc i8 %54 to i1
  call void @_ZN27btMultiBodyConstraintSolver31setupMultiBodyContactConstraintER27btMultiBodySolverConstraintRK9btVector3R15btManifoldPointRK19btContactSolverInfoRfbff(%class.btMultiBodyConstraintSolver* %this1, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %50, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %52, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %53, float* nonnull align 4 dereferenceable(4) %relaxation, i1 zeroext %tobool40, float 0.000000e+00, float 0.000000e+00)
  %55 = load i32, i32* %frictionIndex, align 4
  %56 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_frictionIndex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %56, i32 0, i32 21
  store i32 %55, i32* %m_frictionIndex, align 4
  %57 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %58 = bitcast %struct.btContactSolverInfo* %57 to %struct.btContactSolverInfoData*
  %m_solverMode = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %58, i32 0, i32 16
  %59 = load i32, i32* %m_solverMode, align 4
  %and = and i32 %59, 32
  %tobool41 = icmp ne i32 %and, 0
  br i1 %tobool41, label %lor.lhs.false, label %if.then44

lor.lhs.false:                                    ; preds = %if.end39
  %60 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_contactPointFlags = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %60, i32 0, i32 15
  %61 = load i32, i32* %m_contactPointFlags, align 4
  %and42 = and i32 %61, 1
  %tobool43 = icmp ne i32 %and42, 0
  br i1 %tobool43, label %if.else, label %if.then44

if.then44:                                        ; preds = %lor.lhs.false, %if.end39
  %62 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_normalWorldOnB45 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %62, i32 0, i32 4
  %63 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %63, i32 0, i32 25
  %64 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %64, i32 0, i32 26
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB45, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir2)
  %65 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %66 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir146 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %66, i32 0, i32 25
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %65, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir146, i32 1)
  %67 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %68 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir147 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %68, i32 0, i32 25
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %67, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir147, i32 1)
  %69 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir148 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %69, i32 0, i32 25
  %70 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %71 = load i32, i32* %frictionIndex, align 4
  %72 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %73 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %74 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %75 = load float, float* %relaxation, align 4
  %76 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %call49 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodyConstraintSolver30addMultiBodyFrictionConstraintERK9btVector3P20btPersistentManifoldiR15btManifoldPointP17btCollisionObjectS8_fRK19btContactSolverInfoff(%class.btMultiBodyConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir148, %class.btPersistentManifold* %70, i32 %71, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %72, %class.btCollisionObject* %73, %class.btCollisionObject* %74, float %75, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %76, float 0.000000e+00, float 0.000000e+00)
  %77 = load i32, i32* %rollingFriction, align 4
  %cmp50 = icmp sgt i32 %77, 0
  br i1 %cmp50, label %if.then51, label %if.end59

if.then51:                                        ; preds = %if.then44
  %78 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_normalWorldOnB52 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %78, i32 0, i32 4
  %79 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %80 = load i32, i32* %frictionIndex, align 4
  %81 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %82 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_combinedSpinningFriction = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %82, i32 0, i32 8
  %83 = load float, float* %m_combinedSpinningFriction, align 4
  %84 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %85 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %86 = load float, float* %relaxation, align 4
  %87 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %call53 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodyConstraintSolver39addMultiBodyTorsionalFrictionConstraintERK9btVector3P20btPersistentManifoldiR15btManifoldPointfP17btCollisionObjectS8_fRK19btContactSolverInfoff(%class.btMultiBodyConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB52, %class.btPersistentManifold* %79, i32 %80, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %81, float %83, %class.btCollisionObject* %84, %class.btCollisionObject* %85, float %86, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %87, float 0.000000e+00, float 0.000000e+00)
  %88 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir154 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %88, i32 0, i32 25
  %89 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %90 = load i32, i32* %frictionIndex, align 4
  %91 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %92 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_combinedRollingFriction = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %92, i32 0, i32 7
  %93 = load float, float* %m_combinedRollingFriction, align 4
  %94 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %95 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %96 = load float, float* %relaxation, align 4
  %97 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %call55 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodyConstraintSolver39addMultiBodyTorsionalFrictionConstraintERK9btVector3P20btPersistentManifoldiR15btManifoldPointfP17btCollisionObjectS8_fRK19btContactSolverInfoff(%class.btMultiBodyConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir154, %class.btPersistentManifold* %89, i32 %90, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %91, float %93, %class.btCollisionObject* %94, %class.btCollisionObject* %95, float %96, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %97, float 0.000000e+00, float 0.000000e+00)
  %98 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir256 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %98, i32 0, i32 26
  %99 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %100 = load i32, i32* %frictionIndex, align 4
  %101 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %102 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_combinedRollingFriction57 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %102, i32 0, i32 7
  %103 = load float, float* %m_combinedRollingFriction57, align 4
  %104 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %105 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %106 = load float, float* %relaxation, align 4
  %107 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %call58 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodyConstraintSolver39addMultiBodyTorsionalFrictionConstraintERK9btVector3P20btPersistentManifoldiR15btManifoldPointfP17btCollisionObjectS8_fRK19btContactSolverInfoff(%class.btMultiBodyConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir256, %class.btPersistentManifold* %99, i32 %100, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %101, float %103, %class.btCollisionObject* %104, %class.btCollisionObject* %105, float %106, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %107, float 0.000000e+00, float 0.000000e+00)
  %108 = load i32, i32* %rollingFriction, align 4
  %dec = add nsw i32 %108, -1
  store i32 %dec, i32* %rollingFriction, align 4
  br label %if.end59

if.end59:                                         ; preds = %if.then51, %if.then44
  %109 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %110 = bitcast %struct.btContactSolverInfo* %109 to %struct.btContactSolverInfoData*
  %m_solverMode60 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %110, i32 0, i32 16
  %111 = load i32, i32* %m_solverMode60, align 4
  %and61 = and i32 %111, 16
  %tobool62 = icmp ne i32 %and61, 0
  br i1 %tobool62, label %if.then63, label %if.end68

if.then63:                                        ; preds = %if.end59
  %112 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %113 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir264 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %113, i32 0, i32 26
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %112, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir264, i32 1)
  %114 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %115 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir265 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %115, i32 0, i32 26
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %114, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir265, i32 1)
  %116 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir266 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %116, i32 0, i32 26
  %117 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %118 = load i32, i32* %frictionIndex, align 4
  %119 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %120 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %121 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %122 = load float, float* %relaxation, align 4
  %123 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %call67 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodyConstraintSolver30addMultiBodyFrictionConstraintERK9btVector3P20btPersistentManifoldiR15btManifoldPointP17btCollisionObjectS8_fRK19btContactSolverInfoff(%class.btMultiBodyConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir266, %class.btPersistentManifold* %117, i32 %118, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %119, %class.btCollisionObject* %120, %class.btCollisionObject* %121, float %122, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %123, float 0.000000e+00, float 0.000000e+00)
  br label %if.end68

if.end68:                                         ; preds = %if.then63, %if.end59
  %124 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %125 = bitcast %struct.btContactSolverInfo* %124 to %struct.btContactSolverInfoData*
  %m_solverMode69 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %125, i32 0, i32 16
  %126 = load i32, i32* %m_solverMode69, align 4
  %and70 = and i32 %126, 16
  %tobool71 = icmp ne i32 %and70, 0
  br i1 %tobool71, label %land.lhs.true, label %if.end77

land.lhs.true:                                    ; preds = %if.end68
  %127 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %128 = bitcast %struct.btContactSolverInfo* %127 to %struct.btContactSolverInfoData*
  %m_solverMode72 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %128, i32 0, i32 16
  %129 = load i32, i32* %m_solverMode72, align 4
  %and73 = and i32 %129, 64
  %tobool74 = icmp ne i32 %and73, 0
  br i1 %tobool74, label %if.then75, label %if.end77

if.then75:                                        ; preds = %land.lhs.true
  %130 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_contactPointFlags76 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %130, i32 0, i32 15
  %131 = load i32, i32* %m_contactPointFlags76, align 4
  %or = or i32 %131, 1
  store i32 %or, i32* %m_contactPointFlags76, align 4
  br label %if.end77

if.end77:                                         ; preds = %if.then75, %land.lhs.true, %if.end68
  br label %if.end88

if.else:                                          ; preds = %lor.lhs.false
  %132 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir178 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %132, i32 0, i32 25
  %133 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %134 = load i32, i32* %frictionIndex, align 4
  %135 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %136 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %137 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %138 = load float, float* %relaxation, align 4
  %139 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %140 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_contactMotion1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %140, i32 0, i32 19
  %141 = load float, float* %m_contactMotion1, align 4
  %142 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_frictionCFM = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %142, i32 0, i32 23
  %143 = load float, float* %m_frictionCFM, align 4
  %call79 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodyConstraintSolver30addMultiBodyFrictionConstraintERK9btVector3P20btPersistentManifoldiR15btManifoldPointP17btCollisionObjectS8_fRK19btContactSolverInfoff(%class.btMultiBodyConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir178, %class.btPersistentManifold* %133, i32 %134, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %135, %class.btCollisionObject* %136, %class.btCollisionObject* %137, float %138, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %139, float %141, float %143)
  %144 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %145 = bitcast %struct.btContactSolverInfo* %144 to %struct.btContactSolverInfoData*
  %m_solverMode80 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %145, i32 0, i32 16
  %146 = load i32, i32* %m_solverMode80, align 4
  %and81 = and i32 %146, 16
  %tobool82 = icmp ne i32 %and81, 0
  br i1 %tobool82, label %if.then83, label %if.end87

if.then83:                                        ; preds = %if.else
  %147 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir284 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %147, i32 0, i32 26
  %148 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %149 = load i32, i32* %frictionIndex, align 4
  %150 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %151 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %152 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %153 = load float, float* %relaxation, align 4
  %154 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %155 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_contactMotion2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %155, i32 0, i32 20
  %156 = load float, float* %m_contactMotion2, align 4
  %157 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_frictionCFM85 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %157, i32 0, i32 23
  %158 = load float, float* %m_frictionCFM85, align 4
  %call86 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN27btMultiBodyConstraintSolver30addMultiBodyFrictionConstraintERK9btVector3P20btPersistentManifoldiR15btManifoldPointP17btCollisionObjectS8_fRK19btContactSolverInfoff(%class.btMultiBodyConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir284, %class.btPersistentManifold* %148, i32 %149, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %150, %class.btCollisionObject* %151, %class.btCollisionObject* %152, float %153, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %154, float %156, float %158)
  br label %if.end87

if.end87:                                         ; preds = %if.then83, %if.else
  %159 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_appliedImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %159, i32 0, i32 11
  store float 0.000000e+00, float* %m_appliedImpulse, align 4
  %160 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_appliedPushImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %160, i32 0, i32 10
  store float 0.000000e+00, float* %m_appliedPushImpulse, align 4
  br label %if.end88

if.end88:                                         ; preds = %if.end87, %if.end77
  br label %if.end89

if.end89:                                         ; preds = %if.end88, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end89
  %161 = load i32, i32* %j, align 4
  %inc = add nsw i32 %161, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_cachedPoints, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(192) %class.btManifoldPoint* @_ZN20btPersistentManifold15getContactPointEi(%class.btPersistentManifold* %this, i32 %index) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  %index.addr = alloca i32, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_pointCache = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache, i32 0, i32 %0
  ret %class.btManifoldPoint* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK20btPersistentManifold29getContactProcessingThresholdEv(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_contactProcessingThreshold = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 6
  %0 = load float, float* %m_contactProcessingThreshold, align 4
  ret float %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %n.addr = alloca %class.btVector3*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %q.addr = alloca %class.btVector3*, align 4
  %a = alloca float, align 4
  %k = alloca float, align 4
  %a42 = alloca float, align 4
  %k54 = alloca float, align 4
  store %class.btVector3* %n, %class.btVector3** %n.addr, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4
  store %class.btVector3* %q, %class.btVector3** %q.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 2
  %1 = load float, float* %arrayidx, align 4
  %call1 = call float @_Z6btFabsf(float %1)
  %cmp = fcmp ogt float %call1, 0x3FE6A09E60000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %2)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %4 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %4)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %mul = fmul float %3, %5
  %6 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  %7 = load float, float* %arrayidx7, align 4
  %8 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 2
  %9 = load float, float* %arrayidx9, align 4
  %mul10 = fmul float %7, %9
  %add = fadd float %mul, %mul10
  store float %add, float* %a, align 4
  %10 = load float, float* %a, align 4
  %call11 = call float @_Z6btSqrtf(float %10)
  %div = fdiv float 1.000000e+00, %call11
  store float %div, float* %k, align 4
  %11 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %11)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 0
  store float 0.000000e+00, float* %arrayidx13, align 4
  %12 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %12)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %13 = load float, float* %arrayidx15, align 4
  %fneg = fneg float %13
  %14 = load float, float* %k, align 4
  %mul16 = fmul float %fneg, %14
  %15 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %15)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  store float %mul16, float* %arrayidx18, align 4
  %16 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %16)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  %17 = load float, float* %arrayidx20, align 4
  %18 = load float, float* %k, align 4
  %mul21 = fmul float %17, %18
  %19 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %19)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 2
  store float %mul21, float* %arrayidx23, align 4
  %20 = load float, float* %a, align 4
  %21 = load float, float* %k, align 4
  %mul24 = fmul float %20, %21
  %22 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %22)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 0
  store float %mul24, float* %arrayidx26, align 4
  %23 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call27 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %23)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 0
  %24 = load float, float* %arrayidx28, align 4
  %fneg29 = fneg float %24
  %25 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %25)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %26 = load float, float* %arrayidx31, align 4
  %mul32 = fmul float %fneg29, %26
  %27 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %27)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 1
  store float %mul32, float* %arrayidx34, align 4
  %28 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call35 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %28)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 0
  %29 = load float, float* %arrayidx36, align 4
  %30 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %30)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 1
  %31 = load float, float* %arrayidx38, align 4
  %mul39 = fmul float %29, %31
  %32 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %32)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 2
  store float %mul39, float* %arrayidx41, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %33 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %33)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 0
  %34 = load float, float* %arrayidx44, align 4
  %35 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call45 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %35)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 0
  %36 = load float, float* %arrayidx46, align 4
  %mul47 = fmul float %34, %36
  %37 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call48 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %37)
  %arrayidx49 = getelementptr inbounds float, float* %call48, i32 1
  %38 = load float, float* %arrayidx49, align 4
  %39 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %39)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 1
  %40 = load float, float* %arrayidx51, align 4
  %mul52 = fmul float %38, %40
  %add53 = fadd float %mul47, %mul52
  store float %add53, float* %a42, align 4
  %41 = load float, float* %a42, align 4
  %call55 = call float @_Z6btSqrtf(float %41)
  %div56 = fdiv float 1.000000e+00, %call55
  store float %div56, float* %k54, align 4
  %42 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %42)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 1
  %43 = load float, float* %arrayidx58, align 4
  %fneg59 = fneg float %43
  %44 = load float, float* %k54, align 4
  %mul60 = fmul float %fneg59, %44
  %45 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %45)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 0
  store float %mul60, float* %arrayidx62, align 4
  %46 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call63 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %46)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 0
  %47 = load float, float* %arrayidx64, align 4
  %48 = load float, float* %k54, align 4
  %mul65 = fmul float %47, %48
  %49 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %49)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 1
  store float %mul65, float* %arrayidx67, align 4
  %50 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call68 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %50)
  %arrayidx69 = getelementptr inbounds float, float* %call68, i32 2
  store float 0.000000e+00, float* %arrayidx69, align 4
  %51 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call70 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %51)
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 2
  %52 = load float, float* %arrayidx71, align 4
  %fneg72 = fneg float %52
  %53 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %53)
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 1
  %54 = load float, float* %arrayidx74, align 4
  %mul75 = fmul float %fneg72, %54
  %55 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %55)
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 0
  store float %mul75, float* %arrayidx77, align 4
  %56 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call78 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %56)
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 2
  %57 = load float, float* %arrayidx79, align 4
  %58 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %58)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 0
  %59 = load float, float* %arrayidx81, align 4
  %mul82 = fmul float %57, %59
  %60 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %60)
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 1
  store float %mul82, float* %arrayidx84, align 4
  %61 = load float, float* %a42, align 4
  %62 = load float, float* %k54, align 4
  %mul85 = fmul float %61, %62
  %63 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %63)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 2
  store float %mul85, float* %arrayidx87, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

declare void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject*, %class.btVector3* nonnull align 4 dereferenceable(16), i32) #3

; Function Attrs: noinline optnone
define hidden void @_ZN27btMultiBodyConstraintSolver15convertContactsEPP20btPersistentManifoldiRK19btContactSolverInfo(%class.btMultiBodyConstraintSolver* %this, %class.btPersistentManifold** %manifoldPtr, i32 %numManifolds, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %i = alloca i32, align 4
  %manifold = alloca %class.btPersistentManifold*, align 4
  %fcA = alloca %class.btMultiBodyLinkCollider*, align 4
  %fcB = alloca %class.btMultiBodyLinkCollider*, align 4
  %i6 = alloca i32, align 4
  %c = alloca %class.btMultiBodyConstraint*, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  store %class.btPersistentManifold** %manifoldPtr, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  store i32 %numManifolds, i32* %numManifolds.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %numManifolds.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %2, i32 %3
  %4 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx, align 4
  store %class.btPersistentManifold* %4, %class.btPersistentManifold** %manifold, align 4
  %5 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %call = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %5)
  %call2 = call %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEPK17btCollisionObject(%class.btCollisionObject* %call)
  store %class.btMultiBodyLinkCollider* %call2, %class.btMultiBodyLinkCollider** %fcA, align 4
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %call3 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %6)
  %call4 = call %class.btMultiBodyLinkCollider* @_ZN23btMultiBodyLinkCollider6upcastEPK17btCollisionObject(%class.btCollisionObject* %call3)
  store %class.btMultiBodyLinkCollider* %call4, %class.btMultiBodyLinkCollider** %fcB, align 4
  %7 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcA, align 4
  %tobool = icmp ne %class.btMultiBodyLinkCollider* %7, null
  br i1 %tobool, label %if.else, label %land.lhs.true

land.lhs.true:                                    ; preds = %for.body
  %8 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %fcB, align 4
  %tobool5 = icmp ne %class.btMultiBodyLinkCollider* %8, null
  br i1 %tobool5, label %if.else, label %if.then

if.then:                                          ; preds = %land.lhs.true
  %9 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %10 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %11 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  call void @_ZN35btSequentialImpulseConstraintSolver14convertContactEP20btPersistentManifoldRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver* %9, %class.btPersistentManifold* %10, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %11)
  br label %if.end

if.else:                                          ; preds = %land.lhs.true, %for.body
  %12 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %13 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  call void @_ZN27btMultiBodyConstraintSolver23convertMultiBodyContactEP20btPersistentManifoldRK19btContactSolverInfo(%class.btMultiBodyConstraintSolver* %this1, %class.btPersistentManifold* %12, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %13)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %14 = load i32, i32* %i, align 4
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i6, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %for.end
  %15 = load i32, i32* %i6, align 4
  %m_tmpNumMultiBodyConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 6
  %16 = load i32, i32* %m_tmpNumMultiBodyConstraints, align 4
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body9, label %for.end16

for.body9:                                        ; preds = %for.cond7
  %m_tmpMultiBodyConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 5
  %17 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_tmpMultiBodyConstraints, align 4
  %18 = load i32, i32* %i6, align 4
  %arrayidx10 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %17, i32 %18
  %19 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx10, align 4
  store %class.btMultiBodyConstraint* %19, %class.btMultiBodyConstraint** %c, align 4
  %20 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %20, i32 0, i32 1
  %m_data = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_solverBodyPool = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data, i32 0, i32 6
  store %class.btAlignedObjectArray* %m_tmpSolverBodyPool, %class.btAlignedObjectArray** %m_solverBodyPool, align 4
  %21 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_fixedBodyId = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %21, i32 0, i32 11
  %22 = load i32, i32* %m_fixedBodyId, align 4
  %m_data11 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %m_fixedBodyId12 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %m_data11, i32 0, i32 7
  store i32 %22, i32* %m_fixedBodyId12, align 4
  %23 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %c, align 4
  %m_multiBodyNonContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 1
  %m_data13 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %24 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %25 = bitcast %class.btMultiBodyConstraint* %23 to void (%class.btMultiBodyConstraint*, %class.btAlignedObjectArray.22*, %struct.btMultiBodyJacobianData*, %struct.btContactSolverInfo*)***
  %vtable = load void (%class.btMultiBodyConstraint*, %class.btAlignedObjectArray.22*, %struct.btMultiBodyJacobianData*, %struct.btContactSolverInfo*)**, void (%class.btMultiBodyConstraint*, %class.btAlignedObjectArray.22*, %struct.btMultiBodyJacobianData*, %struct.btContactSolverInfo*)*** %25, align 4
  %vfn = getelementptr inbounds void (%class.btMultiBodyConstraint*, %class.btAlignedObjectArray.22*, %struct.btMultiBodyJacobianData*, %struct.btContactSolverInfo*)*, void (%class.btMultiBodyConstraint*, %class.btAlignedObjectArray.22*, %struct.btMultiBodyJacobianData*, %struct.btContactSolverInfo*)** %vtable, i64 7
  %26 = load void (%class.btMultiBodyConstraint*, %class.btAlignedObjectArray.22*, %struct.btMultiBodyJacobianData*, %struct.btContactSolverInfo*)*, void (%class.btMultiBodyConstraint*, %class.btAlignedObjectArray.22*, %struct.btMultiBodyJacobianData*, %struct.btContactSolverInfo*)** %vfn, align 4
  call void %26(%class.btMultiBodyConstraint* %23, %class.btAlignedObjectArray.22* nonnull align 4 dereferenceable(17) %m_multiBodyNonContactConstraints, %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128) %m_data13, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %24)
  br label %for.inc14

for.inc14:                                        ; preds = %for.body9
  %27 = load i32, i32* %i6, align 4
  %inc15 = add nsw i32 %27, 1
  store i32 %inc15, i32* %i6, align 4
  br label %for.cond7

for.end16:                                        ; preds = %for.cond7
  ret void
}

declare void @_ZN35btSequentialImpulseConstraintSolver14convertContactEP20btPersistentManifoldRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver*, %class.btPersistentManifold*, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88)) #3

; Function Attrs: noinline optnone
define hidden float @_ZN27btMultiBodyConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher(%class.btMultiBodyConstraintSolver* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %class.btPersistentManifold** %manifold, i32 %numManifolds, %class.btTypedConstraint** %constraints, i32 %numConstraints, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %info, %class.btIDebugDraw* %debugDrawer, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %manifold.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %info.addr = alloca %struct.btContactSolverInfo*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4
  store i32 %numBodies, i32* %numBodies.addr, align 4
  store %class.btPersistentManifold** %manifold, %class.btPersistentManifold*** %manifold.addr, align 4
  store i32 %numManifolds, i32* %numManifolds.addr, align 4
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4
  store i32 %numConstraints, i32* %numConstraints.addr, align 4
  store %struct.btContactSolverInfo* %info, %struct.btContactSolverInfo** %info.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %1 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %2 = load i32, i32* %numBodies.addr, align 4
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifold.addr, align 4
  %4 = load i32, i32* %numManifolds.addr, align 4
  %5 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %6 = load i32, i32* %numConstraints.addr, align 4
  %7 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %info.addr, align 4
  %8 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %9 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %call = call float @_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher(%class.btSequentialImpulseConstraintSolver* %0, %class.btCollisionObject** %1, i32 %2, %class.btPersistentManifold** %3, i32 %4, %class.btTypedConstraint** %5, i32 %6, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %7, %class.btIDebugDraw* %8, %class.btDispatcher* %9)
  ret float %call
}

declare float @_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88), %class.btIDebugDraw*, %class.btDispatcher*) unnamed_addr #3

; Function Attrs: noinline optnone
define hidden void @_ZN27btMultiBodyConstraintSolver30writeBackSolverBodyToMultiBodyER27btMultiBodySolverConstraintf(%class.btMultiBodyConstraintSolver* %this, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %c, float %deltaTime) #2 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %c.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  %deltaTime.addr = alloca float, align 4
  %force = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %torque = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  %force22 = alloca %class.btVector3, align 4
  %ref.tmp23 = alloca float, align 4
  %torque26 = alloca %class.btVector3, align 4
  %ref.tmp27 = alloca float, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  store %struct.btMultiBodySolverConstraint* %c, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  store float %deltaTime, float* %deltaTime.addr, align 4
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_orgConstraint = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %0, i32 0, i32 28
  %1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %m_orgConstraint, align 4
  %tobool = icmp ne %class.btMultiBodyConstraint* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_orgConstraint2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %2, i32 0, i32 28
  %3 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %m_orgConstraint2, align 4
  %4 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_orgDofIndex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %4, i32 0, i32 29
  %5 = load i32, i32* %m_orgDofIndex, align 4
  %6 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_appliedImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %6, i32 0, i32 11
  %7 = load float, float* %m_appliedImpulse, align 4
  call void @_ZN21btMultiBodyConstraint25internalSetAppliedImpulseEif(%class.btMultiBodyConstraint* %3, i32 %5, float %7)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %8, i32 0, i32 23
  %9 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA, align 4
  %tobool3 = icmp ne %class.btMultiBody* %9, null
  br i1 %tobool3, label %if.then4, label %if.end18

if.then4:                                         ; preds = %if.end
  %10 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyA5 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %10, i32 0, i32 23
  %11 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA5, align 4
  call void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %11, i32 -1)
  %12 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %12, i32 0, i32 5
  %13 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_appliedImpulse6 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %13, i32 0, i32 11
  %14 = load float, float* %m_appliedImpulse6, align 4
  %15 = load float, float* %deltaTime.addr, align 4
  %div = fdiv float %14, %15
  store float %div, float* %ref.tmp, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %force, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %16 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %16, i32 0, i32 4
  %17 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_appliedImpulse8 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %17, i32 0, i32 11
  %18 = load float, float* %m_appliedImpulse8, align 4
  %19 = load float, float* %deltaTime.addr, align 4
  %div9 = fdiv float %18, %19
  store float %div9, float* %ref.tmp7, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %torque, %class.btVector3* nonnull align 4 dereferenceable(16) %m_relpos1CrossNormal, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %20 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_linkA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %20, i32 0, i32 24
  %21 = load i32, i32* %m_linkA, align 4
  %cmp = icmp slt i32 %21, 0
  br i1 %cmp, label %if.then10, label %if.else

if.then10:                                        ; preds = %if.then4
  %22 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyA11 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %22, i32 0, i32 23
  %23 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA11, align 4
  call void @_ZN11btMultiBody22addBaseConstraintForceERK9btVector3(%class.btMultiBody* %23, %class.btVector3* nonnull align 4 dereferenceable(16) %force)
  %24 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyA12 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %24, i32 0, i32 23
  %25 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA12, align 4
  call void @_ZN11btMultiBody23addBaseConstraintTorqueERK9btVector3(%class.btMultiBody* %25, %class.btVector3* nonnull align 4 dereferenceable(16) %torque)
  br label %if.end17

if.else:                                          ; preds = %if.then4
  %26 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyA13 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %26, i32 0, i32 23
  %27 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA13, align 4
  %28 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_linkA14 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %28, i32 0, i32 24
  %29 = load i32, i32* %m_linkA14, align 4
  call void @_ZN11btMultiBody22addLinkConstraintForceEiRK9btVector3(%class.btMultiBody* %27, i32 %29, %class.btVector3* nonnull align 4 dereferenceable(16) %force)
  %30 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyA15 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %30, i32 0, i32 23
  %31 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA15, align 4
  %32 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_linkA16 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %32, i32 0, i32 24
  %33 = load i32, i32* %m_linkA16, align 4
  call void @_ZN11btMultiBody23addLinkConstraintTorqueEiRK9btVector3(%class.btMultiBody* %31, i32 %33, %class.btVector3* nonnull align 4 dereferenceable(16) %torque)
  br label %if.end17

if.end17:                                         ; preds = %if.else, %if.then10
  br label %if.end18

if.end18:                                         ; preds = %if.end17, %if.end
  %34 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %34, i32 0, i32 26
  %35 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB, align 4
  %tobool19 = icmp ne %class.btMultiBody* %35, null
  br i1 %tobool19, label %if.then20, label %if.end40

if.then20:                                        ; preds = %if.end18
  %36 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyB21 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %36, i32 0, i32 26
  %37 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB21, align 4
  call void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %37, i32 -1)
  %38 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %38, i32 0, i32 7
  %39 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_appliedImpulse24 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %39, i32 0, i32 11
  %40 = load float, float* %m_appliedImpulse24, align 4
  %41 = load float, float* %deltaTime.addr, align 4
  %div25 = fdiv float %40, %41
  store float %div25, float* %ref.tmp23, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %force22, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  %42 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %42, i32 0, i32 6
  %43 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_appliedImpulse28 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %43, i32 0, i32 11
  %44 = load float, float* %m_appliedImpulse28, align 4
  %45 = load float, float* %deltaTime.addr, align 4
  %div29 = fdiv float %44, %45
  store float %div29, float* %ref.tmp27, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %torque26, %class.btVector3* nonnull align 4 dereferenceable(16) %m_relpos2CrossNormal, float* nonnull align 4 dereferenceable(4) %ref.tmp27)
  %46 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_linkB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %46, i32 0, i32 27
  %47 = load i32, i32* %m_linkB, align 4
  %cmp30 = icmp slt i32 %47, 0
  br i1 %cmp30, label %if.then31, label %if.else34

if.then31:                                        ; preds = %if.then20
  %48 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyB32 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %48, i32 0, i32 26
  %49 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB32, align 4
  call void @_ZN11btMultiBody22addBaseConstraintForceERK9btVector3(%class.btMultiBody* %49, %class.btVector3* nonnull align 4 dereferenceable(16) %force22)
  %50 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyB33 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %50, i32 0, i32 26
  %51 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB33, align 4
  call void @_ZN11btMultiBody23addBaseConstraintTorqueERK9btVector3(%class.btMultiBody* %51, %class.btVector3* nonnull align 4 dereferenceable(16) %torque26)
  br label %if.end39

if.else34:                                        ; preds = %if.then20
  %52 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyB35 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %52, i32 0, i32 26
  %53 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB35, align 4
  %54 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_linkB36 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %54, i32 0, i32 27
  %55 = load i32, i32* %m_linkB36, align 4
  call void @_ZN11btMultiBody22addLinkConstraintForceEiRK9btVector3(%class.btMultiBody* %53, i32 %55, %class.btVector3* nonnull align 4 dereferenceable(16) %force22)
  %56 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_multiBodyB37 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %56, i32 0, i32 26
  %57 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB37, align 4
  %58 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %c.addr, align 4
  %m_linkB38 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %58, i32 0, i32 27
  %59 = load i32, i32* %m_linkB38, align 4
  call void @_ZN11btMultiBody23addLinkConstraintTorqueEiRK9btVector3(%class.btMultiBody* %57, i32 %59, %class.btVector3* nonnull align 4 dereferenceable(16) %torque26)
  br label %if.end39

if.end39:                                         ; preds = %if.else34, %if.then31
  br label %if.end40

if.end40:                                         ; preds = %if.end39, %if.end18
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN21btMultiBodyConstraint25internalSetAppliedImpulseEif(%class.btMultiBodyConstraint* %this, i32 %dof, float %appliedImpulse) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %dof.addr = alloca i32, align 4
  %appliedImpulse.addr = alloca float, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4
  store i32 %dof, i32* %dof.addr, align 4
  store float %appliedImpulse, float* %appliedImpulse.addr, align 4
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %0 = load float, float* %appliedImpulse.addr, align 4
  %m_data = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 12
  %1 = load i32, i32* %dof.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %m_data, i32 %1)
  store float %0, float* %call, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMultiBody22addBaseConstraintForceERK9btVector3(%class.btMultiBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %f) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %f.addr = alloca %class.btVector3*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  store %class.btVector3* %f, %class.btVector3** %f.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %f.addr, align 4
  %m_baseConstraintForce = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 9
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_baseConstraintForce, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMultiBody23addBaseConstraintTorqueERK9btVector3(%class.btMultiBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %t) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %t.addr = alloca %class.btVector3*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  store %class.btVector3* %t, %class.btVector3** %t.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %t.addr, align 4
  %m_baseConstraintTorque = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 10
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_baseConstraintTorque, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

declare void @_ZN11btMultiBody22addLinkConstraintForceEiRK9btVector3(%class.btMultiBody*, i32, %class.btVector3* nonnull align 4 dereferenceable(16)) #3

declare void @_ZN11btMultiBody23addLinkConstraintTorqueEiRK9btVector3(%class.btMultiBody*, i32, %class.btVector3* nonnull align 4 dereferenceable(16)) #3

; Function Attrs: noinline optnone
define hidden float @_ZN27btMultiBodyConstraintSolver29solveGroupCacheFriendlyFinishEPP17btCollisionObjectiRK19btContactSolverInfo(%class.btMultiBodyConstraintSolver* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %numPoolConstraints = alloca i32, align 4
  %i = alloca i32, align 4
  %solverConstraint = alloca %struct.btMultiBodySolverConstraint*, align 4
  %i11 = alloca i32, align 4
  %solverConstraint16 = alloca %struct.btMultiBodySolverConstraint*, align 4
  %__profile27 = alloca %class.CProfileSample, align 1
  %j = alloca i32, align 4
  %solverConstraint32 = alloca %struct.btMultiBodySolverConstraint*, align 4
  %pt = alloca %class.btManifoldPoint*, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4
  store i32 %numBodies, i32* %numBodies.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([59 x i8], [59 x i8]* @.str.4, i32 0, i32 0))
  %m_multiBodyNormalContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.22* %m_multiBodyNormalContactConstraints)
  store i32 %call2, i32* %numPoolConstraints, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %numPoolConstraints, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_multiBodyNormalContactConstraints3 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 2
  %2 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintEixEi(%class.btAlignedObjectArray.22* %m_multiBodyNormalContactConstraints3, i32 %2)
  store %struct.btMultiBodySolverConstraint* %call4, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %4 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %5 = bitcast %struct.btContactSolverInfo* %4 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %5, i32 0, i32 3
  %6 = load float, float* %m_timeStep, align 4
  call void @_ZN27btMultiBodyConstraintSolver30writeBackSolverBodyToMultiBodyER27btMultiBodySolverConstraintf(%class.btMultiBodyConstraintSolver* %this1, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %3, float %6)
  %m_multiBodyFrictionContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 3
  %7 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_frictionIndex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %7, i32 0, i32 21
  %8 = load i32, i32* %m_frictionIndex, align 4
  %call5 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintEixEi(%class.btAlignedObjectArray.22* %m_multiBodyFrictionContactConstraints, i32 %8)
  %9 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %10 = bitcast %struct.btContactSolverInfo* %9 to %struct.btContactSolverInfoData*
  %m_timeStep6 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %10, i32 0, i32 3
  %11 = load float, float* %m_timeStep6, align 4
  call void @_ZN27btMultiBodyConstraintSolver30writeBackSolverBodyToMultiBodyER27btMultiBodySolverConstraintf(%class.btMultiBodyConstraintSolver* %this1, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %call5, float %11)
  %12 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %13 = bitcast %struct.btContactSolverInfo* %12 to %struct.btContactSolverInfoData*
  %m_solverMode = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %13, i32 0, i32 16
  %14 = load i32, i32* %m_solverMode, align 4
  %and = and i32 %14, 16
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %m_multiBodyFrictionContactConstraints7 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 3
  %15 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint, align 4
  %m_frictionIndex8 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %15, i32 0, i32 21
  %16 = load i32, i32* %m_frictionIndex8, align 4
  %add = add nsw i32 %16, 1
  %call9 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintEixEi(%class.btAlignedObjectArray.22* %m_multiBodyFrictionContactConstraints7, i32 %add)
  %17 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %18 = bitcast %struct.btContactSolverInfo* %17 to %struct.btContactSolverInfoData*
  %m_timeStep10 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %18, i32 0, i32 3
  %19 = load float, float* %m_timeStep10, align 4
  call void @_ZN27btMultiBodyConstraintSolver30writeBackSolverBodyToMultiBodyER27btMultiBodySolverConstraintf(%class.btMultiBodyConstraintSolver* %this1, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %call9, float %19)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %20 = load i32, i32* %i, align 4
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i11, align 4
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc20, %for.end
  %21 = load i32, i32* %i11, align 4
  %m_multiBodyNonContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 1
  %call13 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.22* %m_multiBodyNonContactConstraints)
  %cmp14 = icmp slt i32 %21, %call13
  br i1 %cmp14, label %for.body15, label %for.end22

for.body15:                                       ; preds = %for.cond12
  %m_multiBodyNonContactConstraints17 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 1
  %22 = load i32, i32* %i11, align 4
  %call18 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintEixEi(%class.btAlignedObjectArray.22* %m_multiBodyNonContactConstraints17, i32 %22)
  store %struct.btMultiBodySolverConstraint* %call18, %struct.btMultiBodySolverConstraint** %solverConstraint16, align 4
  %23 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint16, align 4
  %24 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %25 = bitcast %struct.btContactSolverInfo* %24 to %struct.btContactSolverInfoData*
  %m_timeStep19 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %25, i32 0, i32 3
  %26 = load float, float* %m_timeStep19, align 4
  call void @_ZN27btMultiBodyConstraintSolver30writeBackSolverBodyToMultiBodyER27btMultiBodySolverConstraintf(%class.btMultiBodyConstraintSolver* %this1, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %23, float %26)
  br label %for.inc20

for.inc20:                                        ; preds = %for.body15
  %27 = load i32, i32* %i11, align 4
  %inc21 = add nsw i32 %27, 1
  store i32 %inc21, i32* %i11, align 4
  br label %for.cond12

for.end22:                                        ; preds = %for.cond12
  %28 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %29 = bitcast %struct.btContactSolverInfo* %28 to %struct.btContactSolverInfoData*
  %m_solverMode23 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %29, i32 0, i32 16
  %30 = load i32, i32* %m_solverMode23, align 4
  %and24 = and i32 %30, 4
  %tobool25 = icmp ne i32 %and24, 0
  br i1 %tobool25, label %if.then26, label %if.end54

if.then26:                                        ; preds = %for.end22
  %call28 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile27, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.5, i32 0, i32 0))
  store i32 0, i32* %j, align 4
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc50, %if.then26
  %31 = load i32, i32* %j, align 4
  %32 = load i32, i32* %numPoolConstraints, align 4
  %cmp30 = icmp slt i32 %31, %32
  br i1 %cmp30, label %for.body31, label %for.end52

for.body31:                                       ; preds = %for.cond29
  %m_multiBodyNormalContactConstraints33 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 2
  %33 = load i32, i32* %j, align 4
  %call34 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintEixEi(%class.btAlignedObjectArray.22* %m_multiBodyNormalContactConstraints33, i32 %33)
  store %struct.btMultiBodySolverConstraint* %call34, %struct.btMultiBodySolverConstraint** %solverConstraint32, align 4
  %34 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint32, align 4
  %35 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %34, i32 0, i32 19
  %m_originalContactPoint = bitcast %union.anon.25* %35 to i8**
  %36 = load i8*, i8** %m_originalContactPoint, align 4
  %37 = bitcast i8* %36 to %class.btManifoldPoint*
  store %class.btManifoldPoint* %37, %class.btManifoldPoint** %pt, align 4
  %38 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint32, align 4
  %m_appliedImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %38, i32 0, i32 11
  %39 = load float, float* %m_appliedImpulse, align 4
  %40 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4
  %m_appliedImpulse35 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %40, i32 0, i32 16
  store float %39, float* %m_appliedImpulse35, align 4
  %m_multiBodyFrictionContactConstraints36 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 3
  %41 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint32, align 4
  %m_frictionIndex37 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %41, i32 0, i32 21
  %42 = load i32, i32* %m_frictionIndex37, align 4
  %call38 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintEixEi(%class.btAlignedObjectArray.22* %m_multiBodyFrictionContactConstraints36, i32 %42)
  %m_appliedImpulse39 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %call38, i32 0, i32 11
  %43 = load float, float* %m_appliedImpulse39, align 4
  %44 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4
  %m_appliedImpulseLateral1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %44, i32 0, i32 17
  store float %43, float* %m_appliedImpulseLateral1, align 4
  %45 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %46 = bitcast %struct.btContactSolverInfo* %45 to %struct.btContactSolverInfoData*
  %m_solverMode40 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %46, i32 0, i32 16
  %47 = load i32, i32* %m_solverMode40, align 4
  %and41 = and i32 %47, 16
  %tobool42 = icmp ne i32 %and41, 0
  br i1 %tobool42, label %if.then43, label %if.end49

if.then43:                                        ; preds = %for.body31
  %m_multiBodyFrictionContactConstraints44 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 3
  %48 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint32, align 4
  %m_frictionIndex45 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %48, i32 0, i32 21
  %49 = load i32, i32* %m_frictionIndex45, align 4
  %add46 = add nsw i32 %49, 1
  %call47 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintEixEi(%class.btAlignedObjectArray.22* %m_multiBodyFrictionContactConstraints44, i32 %add46)
  %m_appliedImpulse48 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %call47, i32 0, i32 11
  %50 = load float, float* %m_appliedImpulse48, align 4
  %51 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4
  %m_appliedImpulseLateral2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %51, i32 0, i32 18
  store float %50, float* %m_appliedImpulseLateral2, align 4
  br label %if.end49

if.end49:                                         ; preds = %if.then43, %for.body31
  br label %for.inc50

for.inc50:                                        ; preds = %if.end49
  %52 = load i32, i32* %j, align 4
  %inc51 = add nsw i32 %52, 1
  store i32 %inc51, i32* %j, align 4
  br label %for.cond29

for.end52:                                        ; preds = %for.cond29
  %call53 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile27) #7
  br label %if.end54

if.end54:                                         ; preds = %for.end52, %for.end22
  %53 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %54 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %55 = load i32, i32* %numBodies.addr, align 4
  %56 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %call55 = call float @_ZN35btSequentialImpulseConstraintSolver29solveGroupCacheFriendlyFinishEPP17btCollisionObjectiRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver* %53, %class.btCollisionObject** %54, i32 %55, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %56)
  %call56 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #7
  ret float %call55
}

declare float @_ZN35btSequentialImpulseConstraintSolver29solveGroupCacheFriendlyFinishEPP17btCollisionObjectiRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88)) unnamed_addr #3

; Function Attrs: noinline optnone
define hidden void @_ZN27btMultiBodyConstraintSolver19solveMultiBodyGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiPP21btMultiBodyConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher(%class.btMultiBodyConstraintSolver* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %class.btPersistentManifold** %manifold, i32 %numManifolds, %class.btTypedConstraint** %constraints, i32 %numConstraints, %class.btMultiBodyConstraint** %multiBodyConstraints, i32 %numMultiBodyConstraints, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %info, %class.btIDebugDraw* %debugDrawer, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %manifold.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %multiBodyConstraints.addr = alloca %class.btMultiBodyConstraint**, align 4
  %numMultiBodyConstraints.addr = alloca i32, align 4
  %info.addr = alloca %struct.btContactSolverInfo*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4
  store i32 %numBodies, i32* %numBodies.addr, align 4
  store %class.btPersistentManifold** %manifold, %class.btPersistentManifold*** %manifold.addr, align 4
  store i32 %numManifolds, i32* %numManifolds.addr, align 4
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4
  store i32 %numConstraints, i32* %numConstraints.addr, align 4
  store %class.btMultiBodyConstraint** %multiBodyConstraints, %class.btMultiBodyConstraint*** %multiBodyConstraints.addr, align 4
  store i32 %numMultiBodyConstraints, i32* %numMultiBodyConstraints.addr, align 4
  store %struct.btContactSolverInfo* %info, %struct.btContactSolverInfo** %info.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %0 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %multiBodyConstraints.addr, align 4
  %m_tmpMultiBodyConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 5
  store %class.btMultiBodyConstraint** %0, %class.btMultiBodyConstraint*** %m_tmpMultiBodyConstraints, align 4
  %1 = load i32, i32* %numMultiBodyConstraints.addr, align 4
  %m_tmpNumMultiBodyConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 6
  store i32 %1, i32* %m_tmpNumMultiBodyConstraints, align 4
  %2 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %4 = load i32, i32* %numBodies.addr, align 4
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifold.addr, align 4
  %6 = load i32, i32* %numManifolds.addr, align 4
  %7 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %8 = load i32, i32* %numConstraints.addr, align 4
  %9 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %info.addr, align 4
  %10 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %11 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %call = call float @_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher(%class.btSequentialImpulseConstraintSolver* %2, %class.btCollisionObject** %3, i32 %4, %class.btPersistentManifold** %5, i32 %6, %class.btTypedConstraint** %7, i32 %8, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %9, %class.btIDebugDraw* %10, %class.btDispatcher* %11)
  %m_tmpMultiBodyConstraints2 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 5
  store %class.btMultiBodyConstraint** null, %class.btMultiBodyConstraint*** %m_tmpMultiBodyConstraints2, align 4
  %m_tmpNumMultiBodyConstraints3 = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 6
  store i32 0, i32* %m_tmpNumMultiBodyConstraints3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btMultiBodyConstraintSolver* @_ZN27btMultiBodyConstraintSolverD2Ev(%class.btMultiBodyConstraintSolver* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyConstraintSolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV27btMultiBodyConstraintSolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_data = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 4
  %call = call %struct.btMultiBodyJacobianData* @_ZN23btMultiBodyJacobianDataD2Ev(%struct.btMultiBodyJacobianData* %m_data) #7
  %m_multiBodyFrictionContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 3
  %call2 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintED2Ev(%class.btAlignedObjectArray.22* %m_multiBodyFrictionContactConstraints) #7
  %m_multiBodyNormalContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintED2Ev(%class.btAlignedObjectArray.22* %m_multiBodyNormalContactConstraints) #7
  %m_multiBodyNonContactConstraints = getelementptr inbounds %class.btMultiBodyConstraintSolver, %class.btMultiBodyConstraintSolver* %this1, i32 0, i32 1
  %call4 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintED2Ev(%class.btAlignedObjectArray.22* %m_multiBodyNonContactConstraints) #7
  %1 = bitcast %class.btMultiBodyConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %call5 = call %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverD2Ev(%class.btSequentialImpulseConstraintSolver* %1) #7
  ret %class.btMultiBodyConstraintSolver* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN27btMultiBodyConstraintSolverD0Ev(%class.btMultiBodyConstraintSolver* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  store %class.btMultiBodyConstraintSolver* %this, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %this1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %this.addr, align 4
  %call = call %class.btMultiBodyConstraintSolver* @_ZN27btMultiBodyConstraintSolverD2Ev(%class.btMultiBodyConstraintSolver* %this1) #7
  %0 = bitcast %class.btMultiBodyConstraintSolver* %this1 to i8*
  call void @_ZN27btMultiBodyConstraintSolverdlEPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN18btConstraintSolver12prepareSolveEii(%class.btConstraintSolver* %this, i32 %0, i32 %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConstraintSolver*, align 4
  %.addr = alloca i32, align 4
  %.addr1 = alloca i32, align 4
  store %class.btConstraintSolver* %this, %class.btConstraintSolver** %this.addr, align 4
  store i32 %0, i32* %.addr, align 4
  store i32 %1, i32* %.addr1, align 4
  %this2 = load %class.btConstraintSolver*, %class.btConstraintSolver** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDraw(%class.btConstraintSolver* %this, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %0, %class.btIDebugDraw* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConstraintSolver*, align 4
  %.addr = alloca %struct.btContactSolverInfo*, align 4
  %.addr1 = alloca %class.btIDebugDraw*, align 4
  store %class.btConstraintSolver* %this, %class.btConstraintSolver** %this.addr, align 4
  store %struct.btContactSolverInfo* %0, %struct.btContactSolverInfo** %.addr, align 4
  store %class.btIDebugDraw* %1, %class.btIDebugDraw** %.addr1, align 4
  %this2 = load %class.btConstraintSolver*, %class.btConstraintSolver** %this.addr, align 4
  ret void
}

declare void @_ZN35btSequentialImpulseConstraintSolver5resetEv(%class.btSequentialImpulseConstraintSolver*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK35btSequentialImpulseConstraintSolver13getSolverTypeEv(%class.btSequentialImpulseConstraintSolver* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  ret i32 1
}

declare void @_ZN35btSequentialImpulseConstraintSolver45solveGroupCacheFriendlySplitImpulseIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88), %class.btIDebugDraw*) unnamed_addr #3

declare float @_ZN35btSequentialImpulseConstraintSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88), %class.btIDebugDraw*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_internalType = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 24
  %0 = load i32, i32* %m_internalType, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray.26* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.26*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.26* %this, %class.btAlignedObjectArray.26** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.26*, %class.btAlignedObjectArray.26** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.26, %class.btAlignedObjectArray.26* %this1, i32 0, i32 4
  %0 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %0, i32 %1
  ret %struct.btMultibodyLink* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.34* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btMultiBodyJacobianData* @_ZN23btMultiBodyJacobianDataD2Ev(%struct.btMultiBodyJacobianData* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btMultiBodyJacobianData*, align 4
  store %struct.btMultiBodyJacobianData* %this, %struct.btMultiBodyJacobianData** %this.addr, align 4
  %this1 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %this.addr, align 4
  %scratch_m = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %this1, i32 0, i32 5
  %call = call %class.btAlignedObjectArray.42* @_ZN20btAlignedObjectArrayI11btMatrix3x3ED2Ev(%class.btAlignedObjectArray.42* %scratch_m) #7
  %scratch_v = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %this1, i32 0, i32 4
  %call2 = call %class.btAlignedObjectArray.38* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.38* %scratch_v) #7
  %scratch_r = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %this1, i32 0, i32 3
  %call3 = call %class.btAlignedObjectArray.34* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.34* %scratch_r) #7
  %m_deltaVelocities = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %this1, i32 0, i32 2
  %call4 = call %class.btAlignedObjectArray.34* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.34* %m_deltaVelocities) #7
  %m_deltaVelocitiesUnitImpulse = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %this1, i32 0, i32 1
  %call5 = call %class.btAlignedObjectArray.34* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.34* %m_deltaVelocitiesUnitImpulse) #7
  %m_jacobians = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %this1, i32 0, i32 0
  %call6 = call %class.btAlignedObjectArray.34* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.34* %m_jacobians) #7
  ret %struct.btMultiBodyJacobianData* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintED2Ev(%class.btAlignedObjectArray.22* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE5clearEv(%class.btAlignedObjectArray.22* %this1)
  ret %class.btAlignedObjectArray.22* %this1
}

; Function Attrs: nounwind
declare %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverD2Ev(%class.btSequentialImpulseConstraintSolver* returned) unnamed_addr #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.42* @_ZN20btAlignedObjectArrayI11btMatrix3x3ED2Ev(%class.btAlignedObjectArray.42* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.42*, align 4
  store %class.btAlignedObjectArray.42* %this, %class.btAlignedObjectArray.42** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.42*, %class.btAlignedObjectArray.42** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E5clearEv(%class.btAlignedObjectArray.42* %this1)
  ret %class.btAlignedObjectArray.42* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.38* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.38* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.38* %this1)
  ret %class.btAlignedObjectArray.38* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.34* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.34* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.34* %this1)
  ret %class.btAlignedObjectArray.34* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E5clearEv(%class.btAlignedObjectArray.42* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.42*, align 4
  store %class.btAlignedObjectArray.42* %this, %class.btAlignedObjectArray.42** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.42*, %class.btAlignedObjectArray.42** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.42* %this1)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E7destroyEii(%class.btAlignedObjectArray.42* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E10deallocateEv(%class.btAlignedObjectArray.42* %this1)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E4initEv(%class.btAlignedObjectArray.42* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E7destroyEii(%class.btAlignedObjectArray.42* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.42*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.42* %this, %class.btAlignedObjectArray.42** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.42*, %class.btAlignedObjectArray.42** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.42* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.42*, align 4
  store %class.btAlignedObjectArray.42* %this, %class.btAlignedObjectArray.42** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.42*, %class.btAlignedObjectArray.42** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E10deallocateEv(%class.btAlignedObjectArray.42* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.42*, align 4
  store %class.btAlignedObjectArray.42* %this, %class.btAlignedObjectArray.42** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.42*, %class.btAlignedObjectArray.42** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data, align 4
  %tobool = icmp ne %class.btMatrix3x3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.43* %m_allocator, %class.btMatrix3x3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 4
  store %class.btMatrix3x3* null, %class.btMatrix3x3** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E4initEv(%class.btAlignedObjectArray.42* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.42*, align 4
  store %class.btAlignedObjectArray.42* %this, %class.btAlignedObjectArray.42** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.42*, %class.btAlignedObjectArray.42** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 4
  store %class.btMatrix3x3* null, %class.btMatrix3x3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.43* %this, %class.btMatrix3x3* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.43*, align 4
  %ptr.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btAlignedAllocator.43* %this, %class.btAlignedAllocator.43** %this.addr, align 4
  store %class.btMatrix3x3* %ptr, %class.btMatrix3x3** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.43*, %class.btAlignedAllocator.43** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %ptr.addr, align 4
  %1 = bitcast %class.btMatrix3x3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.38* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.38* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.38* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.38* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.38* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.38* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.38* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.38* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.39* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.38* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.39* %this, %class.btVector3* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.39*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator.39* %this, %class.btAlignedAllocator.39** %this.addr, align 4
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.39*, %class.btAlignedAllocator.39** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.34* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.34* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.34* %this1)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.34* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.34* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  %3 = load float*, float** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.34* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %tobool = icmp ne float* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  %2 = load float*, float** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.35* %m_allocator, float* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  store float* null, float** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.34* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  store float* null, float** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.35* %this, float* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.35*, align 4
  %ptr.addr = alloca float*, align 4
  store %class.btAlignedAllocator.35* %this, %class.btAlignedAllocator.35** %this.addr, align 4
  store float* %ptr, float** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.35*, %class.btAlignedAllocator.35** %this.addr, align 4
  %0 = load float*, float** %ptr.addr, align 4
  %1 = bitcast float* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE5clearEv(%class.btAlignedObjectArray.22* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii(%class.btAlignedObjectArray.22* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv(%class.btAlignedObjectArray.22* %this1)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE4initEv(%class.btAlignedObjectArray.22* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii(%class.btAlignedObjectArray.22* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv(%class.btAlignedObjectArray.22* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4
  %tobool = icmp ne %struct.btMultiBodySolverConstraint* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %2 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_(%class.btAlignedAllocator.23* %m_allocator, %struct.btMultiBodySolverConstraint* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  store %struct.btMultiBodySolverConstraint* null, %struct.btMultiBodySolverConstraint** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE4initEv(%class.btAlignedObjectArray.22* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  store %struct.btMultiBodySolverConstraint* null, %struct.btMultiBodySolverConstraint** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_(%class.btAlignedAllocator.23* %this, %struct.btMultiBodySolverConstraint* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.23*, align 4
  %ptr.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  store %class.btAlignedAllocator.23* %this, %class.btAlignedAllocator.23** %this.addr, align 4
  store %struct.btMultiBodySolverConstraint* %ptr, %struct.btMultiBodySolverConstraint** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.23*, %class.btAlignedAllocator.23** %this.addr, align 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %ptr.addr, align 4
  %1 = bitcast %struct.btMultiBodySolverConstraint* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN27btMultiBodyConstraintSolverdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #6

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #6

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi(%class.btAlignedObjectArray.22* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btMultiBodySolverConstraint*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.22* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi(%class.btAlignedObjectArray.22* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btMultiBodySolverConstraint*
  store %struct.btMultiBodySolverConstraint* %2, %struct.btMultiBodySolverConstraint** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_(%class.btAlignedObjectArray.22* %this1, i32 0, i32 %call3, %struct.btMultiBodySolverConstraint* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii(%class.btAlignedObjectArray.22* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv(%class.btAlignedObjectArray.22* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  store %struct.btMultiBodySolverConstraint* %4, %struct.btMultiBodySolverConstraint** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN27btMultiBodySolverConstraintnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.22* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi(%class.btAlignedObjectArray.22* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btMultiBodySolverConstraint* @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.23* %m_allocator, i32 %1, %struct.btMultiBodySolverConstraint** null)
  %2 = bitcast %struct.btMultiBodySolverConstraint* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_(%class.btAlignedObjectArray.22* %this, i32 %start, i32 %end, %struct.btMultiBodySolverConstraint* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btMultiBodySolverConstraint* %dest, %struct.btMultiBodySolverConstraint** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %3, i32 %4
  %5 = bitcast %struct.btMultiBodySolverConstraint* %arrayidx to i8*
  %call = call i8* @_ZN27btMultiBodySolverConstraintnwEmPv(i32 192, i8* %5)
  %6 = bitcast i8* %call to %struct.btMultiBodySolverConstraint*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %7 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %7, i32 %8
  %9 = bitcast %struct.btMultiBodySolverConstraint* %6 to i8*
  %10 = bitcast %struct.btMultiBodySolverConstraint* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 192, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btMultiBodySolverConstraint* @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.23* %this, i32 %n, %struct.btMultiBodySolverConstraint** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.23*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btMultiBodySolverConstraint**, align 4
  store %class.btAlignedAllocator.23* %this, %class.btAlignedAllocator.23** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btMultiBodySolverConstraint** %hint, %struct.btMultiBodySolverConstraint*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.23*, %class.btAlignedAllocator.23** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 192, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btMultiBodySolverConstraint*
  ret %struct.btMultiBodySolverConstraint* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.34* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca float*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.34* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.34* %this1, i32 %1)
  %2 = bitcast i8* %call2 to float*
  store float* %2, float** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %this1)
  %3 = load float*, float** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.34* %this1, i32 0, i32 %call3, float* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.34* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.34* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.34* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load float*, float** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  store float* %4, float** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.34* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.34* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.35* %m_allocator, i32 %1, float** null)
  %2 = bitcast float* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.34* %this, i32 %start, i32 %end, float* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.34*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca float*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.34* %this, %class.btAlignedObjectArray.34** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store float* %dest, float** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.34*, %class.btAlignedObjectArray.34** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load float*, float** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  %5 = bitcast float* %arrayidx to i8*
  %6 = bitcast i8* %5 to float*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.34* %this1, i32 0, i32 4
  %7 = load float*, float** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds float, float* %7, i32 %8
  %9 = load float, float* %arrayidx2, align 4
  store float %9, float* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.35* %this, i32 %n, float** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.35*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca float**, align 4
  store %class.btAlignedAllocator.35* %this, %class.btAlignedAllocator.35** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store float** %hint, float*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.35*, %class.btAlignedAllocator.35** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to float*
  ret float* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.22* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btMultiBodyConstraintSolver.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
