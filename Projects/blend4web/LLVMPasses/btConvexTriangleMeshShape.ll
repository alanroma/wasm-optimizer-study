; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btConvexTriangleMeshShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btConvexTriangleMeshShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btConvexTriangleMeshShape = type { %class.btPolyhedralConvexAabbCachingShape.base, %class.btStridingMeshInterface* }
%class.btPolyhedralConvexAabbCachingShape.base = type <{ %class.btPolyhedralConvexShape, %class.btVector3, %class.btVector3, i8 }>
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btConvexPolyhedron = type opaque
%class.btVector3 = type { [4 x float] }
%class.btStridingMeshInterface = type { i32 (...)**, %class.btVector3 }
%class.btPolyhedralConvexAabbCachingShape = type <{ %class.btPolyhedralConvexShape, %class.btVector3, %class.btVector3, i8, [3 x i8] }>
%class.LocalSupportVertexCallback = type { %class.btInternalTriangleIndexCallback, %class.btVector3, float, %class.btVector3 }
%class.btInternalTriangleIndexCallback = type { i32 (...)** }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.CenterCallback = type { %class.btInternalTriangleIndexCallback, i8, %class.btVector3, %class.btVector3, float }
%class.InertiaCallback = type { %class.btInternalTriangleIndexCallback, %class.btMatrix3x3, %class.btVector3 }
%class.btSerializer = type opaque
%struct.btConvexInternalShapeData = type { %struct.btCollisionShapeData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, i32 }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN26LocalSupportVertexCallbackC2ERK9btVector3 = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN26LocalSupportVertexCallback21GetSupportVertexLocalEv = comdat any

$_ZN26LocalSupportVertexCallbackD2Ev = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN23btStridingMeshInterface10setScalingERK9btVector3 = comdat any

$_ZNK23btStridingMeshInterface10getScalingEv = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZN11btMatrix3x311diagonalizeERS_fi = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZN25btConvexTriangleMeshShapeD2Ev = comdat any

$_ZN25btConvexTriangleMeshShapeD0Ev = comdat any

$_ZNK25btConvexTriangleMeshShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN21btConvexInternalShape9setMarginEf = comdat any

$_ZNK21btConvexInternalShape9getMarginEv = comdat any

$_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK21btConvexInternalShape9serializeEPvP12btSerializer = comdat any

$_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv = comdat any

$_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN31btInternalTriangleIndexCallbackC2Ev = comdat any

$_ZN26LocalSupportVertexCallbackD0Ev = comdat any

$_ZN26LocalSupportVertexCallback28internalProcessTriangleIndexEP9btVector3ii = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_Z6btFabsf = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector36tripleERKS_S1_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN34btPolyhedralConvexAabbCachingShapeD2Ev = comdat any

$_ZN25btConvexTriangleMeshShapedlEPv = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

$_ZTV26LocalSupportVertexCallback = comdat any

$_ZTS26LocalSupportVertexCallback = comdat any

$_ZTI26LocalSupportVertexCallback = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV25btConvexTriangleMeshShape = hidden unnamed_addr constant { [33 x i8*] } { [33 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI25btConvexTriangleMeshShape to i8*), i8* bitcast (%class.btConvexTriangleMeshShape* (%class.btConvexTriangleMeshShape*)* @_ZN25btConvexTriangleMeshShapeD2Ev to i8*), i8* bitcast (void (%class.btConvexTriangleMeshShape*)* @_ZN25btConvexTriangleMeshShapeD0Ev to i8*), i8* bitcast (void (%class.btPolyhedralConvexAabbCachingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btConvexTriangleMeshShape*, %class.btVector3*)* @_ZN25btConvexTriangleMeshShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexTriangleMeshShape*)* @_ZNK25btConvexTriangleMeshShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btPolyhedralConvexShape*, float, %class.btVector3*)* @_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btConvexTriangleMeshShape*)* @_ZNK25btConvexTriangleMeshShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, float)* @_ZN21btConvexInternalShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConvexInternalShape*, i8*, %class.btSerializer*)* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexTriangleMeshShape*, %class.btVector3*)* @_ZNK25btConvexTriangleMeshShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexTriangleMeshShape*, %class.btVector3*)* @_ZNK25btConvexTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*, %class.btVector3*, %class.btVector3*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_RS3_S7_ to i8*), i8* bitcast (void (%class.btConvexTriangleMeshShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK25btConvexTriangleMeshShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, i32, %class.btVector3*)* @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*), i8* bitcast (i1 (%class.btPolyhedralConvexShape*, i32)* @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi to i8*), i8* bitcast (i32 (%class.btConvexTriangleMeshShape*)* @_ZNK25btConvexTriangleMeshShape14getNumVerticesEv to i8*), i8* bitcast (i32 (%class.btConvexTriangleMeshShape*)* @_ZNK25btConvexTriangleMeshShape11getNumEdgesEv to i8*), i8* bitcast (void (%class.btConvexTriangleMeshShape*, i32, %class.btVector3*, %class.btVector3*)* @_ZNK25btConvexTriangleMeshShape7getEdgeEiR9btVector3S1_ to i8*), i8* bitcast (void (%class.btConvexTriangleMeshShape*, i32, %class.btVector3*)* @_ZNK25btConvexTriangleMeshShape9getVertexEiR9btVector3 to i8*), i8* bitcast (i32 (%class.btConvexTriangleMeshShape*)* @_ZNK25btConvexTriangleMeshShape12getNumPlanesEv to i8*), i8* bitcast (void (%class.btConvexTriangleMeshShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK25btConvexTriangleMeshShape8getPlaneER9btVector3S1_i to i8*), i8* bitcast (i1 (%class.btConvexTriangleMeshShape*, %class.btVector3*, float)* @_ZNK25btConvexTriangleMeshShape8isInsideERK9btVector3f to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS25btConvexTriangleMeshShape = hidden constant [28 x i8] c"25btConvexTriangleMeshShape\00", align 1
@_ZTI34btPolyhedralConvexAabbCachingShape = external constant i8*
@_ZTI25btConvexTriangleMeshShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([28 x i8], [28 x i8]* @_ZTS25btConvexTriangleMeshShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI34btPolyhedralConvexAabbCachingShape to i8*) }, align 4
@_ZTV26LocalSupportVertexCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI26LocalSupportVertexCallback to i8*), i8* bitcast (%class.LocalSupportVertexCallback* (%class.LocalSupportVertexCallback*)* @_ZN26LocalSupportVertexCallbackD2Ev to i8*), i8* bitcast (void (%class.LocalSupportVertexCallback*)* @_ZN26LocalSupportVertexCallbackD0Ev to i8*), i8* bitcast (void (%class.LocalSupportVertexCallback*, %class.btVector3*, i32, i32)* @_ZN26LocalSupportVertexCallback28internalProcessTriangleIndexEP9btVector3ii to i8*)] }, comdat, align 4
@_ZTS26LocalSupportVertexCallback = linkonce_odr hidden constant [29 x i8] c"26LocalSupportVertexCallback\00", comdat, align 1
@_ZTI31btInternalTriangleIndexCallback = external constant i8*
@_ZTI26LocalSupportVertexCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([29 x i8], [29 x i8]* @_ZTS26LocalSupportVertexCallback, i32 0, i32 0), i8* bitcast (i8** @_ZTI31btInternalTriangleIndexCallback to i8*) }, comdat, align 4
@_ZTV31btInternalTriangleIndexCallback = external unnamed_addr constant { [5 x i8*] }, align 4
@_ZTVZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback to i8*), i8* bitcast (%class.CenterCallback* (%class.CenterCallback*)* @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallbackD2Ev to i8*), i8* bitcast (void (%class.CenterCallback*)* @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallbackD0Ev to i8*), i8* bitcast (void (%class.CenterCallback*, %class.btVector3*, i32, i32)* @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallback28internalProcessTriangleIndexEPS2_ii to i8*)] }, align 4
@_ZTSZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback = internal constant [109 x i8] c"ZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback\00", align 1
@_ZTIZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([109 x i8], [109 x i8]* @_ZTSZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback, i32 0, i32 0), i8* bitcast (i8** @_ZTI31btInternalTriangleIndexCallback to i8*) }, align 4
@_ZTVZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback to i8*), i8* bitcast (%class.InertiaCallback* (%class.InertiaCallback*)* @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallbackD2Ev to i8*), i8* bitcast (void (%class.InertiaCallback*)* @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallbackD0Ev to i8*), i8* bitcast (void (%class.InertiaCallback*, %class.btVector3*, i32, i32)* @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallback28internalProcessTriangleIndexEPS2_ii to i8*)] }, align 4
@_ZTSZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback = internal constant [110 x i8] c"ZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback\00", align 1
@_ZTIZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([110 x i8], [110 x i8]* @_ZTSZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback, i32 0, i32 0), i8* bitcast (i8** @_ZTI31btInternalTriangleIndexCallback to i8*) }, align 4
@.str = private unnamed_addr constant [14 x i8] c"ConvexTrimesh\00", align 1
@.str.1 = private unnamed_addr constant [26 x i8] c"btConvexInternalShapeData\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btConvexTriangleMeshShape.cpp, i8* null }]

@_ZN25btConvexTriangleMeshShapeC1EP23btStridingMeshInterfaceb = hidden unnamed_addr alias %class.btConvexTriangleMeshShape* (%class.btConvexTriangleMeshShape*, %class.btStridingMeshInterface*, i1), %class.btConvexTriangleMeshShape* (%class.btConvexTriangleMeshShape*, %class.btStridingMeshInterface*, i1)* @_ZN25btConvexTriangleMeshShapeC2EP23btStridingMeshInterfaceb

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btConvexTriangleMeshShape* @_ZN25btConvexTriangleMeshShapeC2EP23btStridingMeshInterfaceb(%class.btConvexTriangleMeshShape* returned %this, %class.btStridingMeshInterface* %meshInterface, i1 zeroext %calcAabb) unnamed_addr #2 {
entry:
  %retval = alloca %class.btConvexTriangleMeshShape*, align 4
  %this.addr = alloca %class.btConvexTriangleMeshShape*, align 4
  %meshInterface.addr = alloca %class.btStridingMeshInterface*, align 4
  %calcAabb.addr = alloca i8, align 1
  store %class.btConvexTriangleMeshShape* %this, %class.btConvexTriangleMeshShape** %this.addr, align 4
  store %class.btStridingMeshInterface* %meshInterface, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  %frombool = zext i1 %calcAabb to i8
  store i8 %frombool, i8* %calcAabb.addr, align 1
  %this1 = load %class.btConvexTriangleMeshShape*, %class.btConvexTriangleMeshShape** %this.addr, align 4
  store %class.btConvexTriangleMeshShape* %this1, %class.btConvexTriangleMeshShape** %retval, align 4
  %0 = bitcast %class.btConvexTriangleMeshShape* %this1 to %class.btPolyhedralConvexAabbCachingShape*
  %call = call %class.btPolyhedralConvexAabbCachingShape* @_ZN34btPolyhedralConvexAabbCachingShapeC2Ev(%class.btPolyhedralConvexAabbCachingShape* %0)
  %1 = bitcast %class.btConvexTriangleMeshShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [33 x i8*] }, { [33 x i8*] }* @_ZTV25btConvexTriangleMeshShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_stridingMesh = getelementptr inbounds %class.btConvexTriangleMeshShape, %class.btConvexTriangleMeshShape* %this1, i32 0, i32 1
  %2 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  store %class.btStridingMeshInterface* %2, %class.btStridingMeshInterface** %m_stridingMesh, align 4
  %3 = bitcast %class.btConvexTriangleMeshShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %3, i32 0, i32 1
  store i32 3, i32* %m_shapeType, align 4
  %4 = load i8, i8* %calcAabb.addr, align 1
  %tobool = trunc i8 %4 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = bitcast %class.btConvexTriangleMeshShape* %this1 to %class.btPolyhedralConvexAabbCachingShape*
  call void @_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv(%class.btPolyhedralConvexAabbCachingShape* %5)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = load %class.btConvexTriangleMeshShape*, %class.btConvexTriangleMeshShape** %retval, align 4
  ret %class.btConvexTriangleMeshShape* %6
}

declare %class.btPolyhedralConvexAabbCachingShape* @_ZN34btPolyhedralConvexAabbCachingShapeC2Ev(%class.btPolyhedralConvexAabbCachingShape* returned) unnamed_addr #3

declare void @_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv(%class.btPolyhedralConvexAabbCachingShape*) #3

; Function Attrs: noinline optnone
define hidden void @_ZNK25btConvexTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btConvexTriangleMeshShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec0) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvexTriangleMeshShape*, align 4
  %vec0.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %vec = alloca %class.btVector3, align 4
  %lenSqr = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %rlen = alloca float, align 4
  %supportCallback = alloca %class.LocalSupportVertexCallback, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  store %class.btConvexTriangleMeshShape* %this, %class.btConvexTriangleMeshShape** %this.addr, align 4
  store %class.btVector3* %vec0, %class.btVector3** %vec0.addr, align 4
  %this1 = load %class.btConvexTriangleMeshShape*, %class.btConvexTriangleMeshShape** %this.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %0 = load %class.btVector3*, %class.btVector3** %vec0.addr, align 4
  %1 = bitcast %class.btVector3* %vec to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %call4 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %vec)
  store float %call4, float* %lenSqr, align 4
  %3 = load float, float* %lenSqr, align 4
  %cmp = fcmp olt float %3, 0x3F1A36E2E0000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %vec, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  br label %if.end

if.else:                                          ; preds = %entry
  %4 = load float, float* %lenSqr, align 4
  %call8 = call float @_Z6btSqrtf(float %4)
  %div = fdiv float 1.000000e+00, %call8
  store float %div, float* %rlen, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %vec, float* nonnull align 4 dereferenceable(4) %rlen)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %call10 = call %class.LocalSupportVertexCallback* @_ZN26LocalSupportVertexCallbackC2ERK9btVector3(%class.LocalSupportVertexCallback* %supportCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  store float 0x43ABC16D60000000, float* %ref.tmp11, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp12, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp13, align 4
  %call14 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %aabbMax, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  %m_stridingMesh = getelementptr inbounds %class.btConvexTriangleMeshShape, %class.btConvexTriangleMeshShape* %this1, i32 0, i32 1
  %5 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_stridingMesh, align 4
  %6 = bitcast %class.LocalSupportVertexCallback* %supportCallback to %class.btInternalTriangleIndexCallback*
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  %7 = bitcast %class.btStridingMeshInterface* %5 to void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*** %7, align 4
  %vfn = getelementptr inbounds void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %8 = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %8(%class.btStridingMeshInterface* %5, %class.btInternalTriangleIndexCallback* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  call void @_ZN26LocalSupportVertexCallback21GetSupportVertexLocalEv(%class.btVector3* sret align 4 %ref.tmp16, %class.LocalSupportVertexCallback* %supportCallback)
  %9 = bitcast %class.btVector3* %agg.result to i8*
  %10 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  %call17 = call %class.LocalSupportVertexCallback* @_ZN26LocalSupportVertexCallbackD2Ev(%class.LocalSupportVertexCallback* %supportCallback) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.LocalSupportVertexCallback* @_ZN26LocalSupportVertexCallbackC2ERK9btVector3(%class.LocalSupportVertexCallback* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %supportVecLocal) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.LocalSupportVertexCallback*, align 4
  %supportVecLocal.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.LocalSupportVertexCallback* %this, %class.LocalSupportVertexCallback** %this.addr, align 4
  store %class.btVector3* %supportVecLocal, %class.btVector3** %supportVecLocal.addr, align 4
  %this1 = load %class.LocalSupportVertexCallback*, %class.LocalSupportVertexCallback** %this.addr, align 4
  %0 = bitcast %class.LocalSupportVertexCallback* %this1 to %class.btInternalTriangleIndexCallback*
  %call = call %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackC2Ev(%class.btInternalTriangleIndexCallback* %0) #8
  %1 = bitcast %class.LocalSupportVertexCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV26LocalSupportVertexCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_supportVertexLocal = getelementptr inbounds %class.LocalSupportVertexCallback, %class.LocalSupportVertexCallback* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_supportVertexLocal, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %m_maxDot = getelementptr inbounds %class.LocalSupportVertexCallback, %class.LocalSupportVertexCallback* %this1, i32 0, i32 2
  store float 0xC3ABC16D60000000, float* %m_maxDot, align 4
  %m_supportVecLocal = getelementptr inbounds %class.LocalSupportVertexCallback, %class.LocalSupportVertexCallback* %this1, i32 0, i32 3
  %2 = load %class.btVector3*, %class.btVector3** %supportVecLocal.addr, align 4
  %3 = bitcast %class.btVector3* %m_supportVecLocal to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  ret %class.LocalSupportVertexCallback* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN26LocalSupportVertexCallback21GetSupportVertexLocalEv(%class.btVector3* noalias sret align 4 %agg.result, %class.LocalSupportVertexCallback* %this) #1 comdat {
entry:
  %this.addr = alloca %class.LocalSupportVertexCallback*, align 4
  store %class.LocalSupportVertexCallback* %this, %class.LocalSupportVertexCallback** %this.addr, align 4
  %this1 = load %class.LocalSupportVertexCallback*, %class.LocalSupportVertexCallback** %this.addr, align 4
  %m_supportVertexLocal = getelementptr inbounds %class.LocalSupportVertexCallback, %class.LocalSupportVertexCallback* %this1, i32 0, i32 1
  %0 = bitcast %class.btVector3* %agg.result to i8*
  %1 = bitcast %class.btVector3* %m_supportVertexLocal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.LocalSupportVertexCallback* @_ZN26LocalSupportVertexCallbackD2Ev(%class.LocalSupportVertexCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.LocalSupportVertexCallback*, align 4
  store %class.LocalSupportVertexCallback* %this, %class.LocalSupportVertexCallback** %this.addr, align 4
  %this1 = load %class.LocalSupportVertexCallback*, %class.LocalSupportVertexCallback** %this.addr, align 4
  %0 = bitcast %class.LocalSupportVertexCallback* %this1 to %class.btInternalTriangleIndexCallback*
  %call = call %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackD2Ev(%class.btInternalTriangleIndexCallback* %0) #8
  ret %class.LocalSupportVertexCallback* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZNK25btConvexTriangleMeshShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btConvexTriangleMeshShape* %this, %class.btVector3* %vectors, %class.btVector3* %supportVerticesOut, i32 %numVectors) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvexTriangleMeshShape*, align 4
  %vectors.addr = alloca %class.btVector3*, align 4
  %supportVerticesOut.addr = alloca %class.btVector3*, align 4
  %numVectors.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %vec = alloca %class.btVector3*, align 4
  %supportCallback = alloca %class.LocalSupportVertexCallback, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  store %class.btConvexTriangleMeshShape* %this, %class.btConvexTriangleMeshShape** %this.addr, align 4
  store %class.btVector3* %vectors, %class.btVector3** %vectors.addr, align 4
  store %class.btVector3* %supportVerticesOut, %class.btVector3** %supportVerticesOut.addr, align 4
  store i32 %numVectors, i32* %numVectors.addr, align 4
  %this1 = load %class.btConvexTriangleMeshShape*, %class.btConvexTriangleMeshShape** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %numVectors.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 %3
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx)
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 3
  store float 0xC3ABC16D60000000, float* %arrayidx2, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %j, align 4
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc15, %for.end
  %5 = load i32, i32* %j, align 4
  %6 = load i32, i32* %numVectors.addr, align 4
  %cmp4 = icmp slt i32 %5, %6
  br i1 %cmp4, label %for.body5, label %for.end17

for.body5:                                        ; preds = %for.cond3
  %7 = load %class.btVector3*, %class.btVector3** %vectors.addr, align 4
  %8 = load i32, i32* %j, align 4
  %arrayidx6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  store %class.btVector3* %arrayidx6, %class.btVector3** %vec, align 4
  %9 = load %class.btVector3*, %class.btVector3** %vec, align 4
  %call7 = call %class.LocalSupportVertexCallback* @_ZN26LocalSupportVertexCallbackC2ERK9btVector3(%class.LocalSupportVertexCallback* %supportCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  store float 0x43ABC16D60000000, float* %ref.tmp, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp8, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp9, align 4
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %aabbMax, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %m_stridingMesh = getelementptr inbounds %class.btConvexTriangleMeshShape, %class.btConvexTriangleMeshShape* %this1, i32 0, i32 1
  %10 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_stridingMesh, align 4
  %11 = bitcast %class.LocalSupportVertexCallback* %supportCallback to %class.btInternalTriangleIndexCallback*
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  %12 = bitcast %class.btStridingMeshInterface* %10 to void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*** %12, align 4
  %vfn = getelementptr inbounds void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %13 = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %13(%class.btStridingMeshInterface* %10, %class.btInternalTriangleIndexCallback* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  call void @_ZN26LocalSupportVertexCallback21GetSupportVertexLocalEv(%class.btVector3* sret align 4 %ref.tmp12, %class.LocalSupportVertexCallback* %supportCallback)
  %14 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4
  %15 = load i32, i32* %j, align 4
  %arrayidx13 = getelementptr inbounds %class.btVector3, %class.btVector3* %14, i32 %15
  %16 = bitcast %class.btVector3* %arrayidx13 to i8*
  %17 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false)
  %call14 = call %class.LocalSupportVertexCallback* @_ZN26LocalSupportVertexCallbackD2Ev(%class.LocalSupportVertexCallback* %supportCallback) #8
  br label %for.inc15

for.inc15:                                        ; preds = %for.body5
  %18 = load i32, i32* %j, align 4
  %inc16 = add nsw i32 %18, 1
  store i32 %inc16, i32* %j, align 4
  br label %for.cond3

for.end17:                                        ; preds = %for.cond3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZNK25btConvexTriangleMeshShape24localGetSupportingVertexERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btConvexTriangleMeshShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvexTriangleMeshShape*, align 4
  %vec.addr = alloca %class.btVector3*, align 4
  %vecnorm = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca float, align 4
  store %class.btConvexTriangleMeshShape* %this, %class.btConvexTriangleMeshShape** %this.addr, align 4
  store %class.btVector3* %vec, %class.btVector3** %vec.addr, align 4
  %this1 = load %class.btConvexTriangleMeshShape*, %class.btConvexTriangleMeshShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4
  %1 = bitcast %class.btConvexTriangleMeshShape* %this1 to void (%class.btVector3*, %class.btConvexTriangleMeshShape*, %class.btVector3*)***
  %vtable = load void (%class.btVector3*, %class.btConvexTriangleMeshShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexTriangleMeshShape*, %class.btVector3*)*** %1, align 4
  %vfn = getelementptr inbounds void (%class.btVector3*, %class.btConvexTriangleMeshShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexTriangleMeshShape*, %class.btVector3*)** %vtable, i64 17
  %2 = load void (%class.btVector3*, %class.btConvexTriangleMeshShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexTriangleMeshShape*, %class.btVector3*)** %vfn, align 4
  call void %2(%class.btVector3* sret align 4 %agg.result, %class.btConvexTriangleMeshShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %3 = bitcast %class.btConvexTriangleMeshShape* %this1 to %class.btConvexInternalShape*
  %4 = bitcast %class.btConvexInternalShape* %3 to float (%class.btConvexInternalShape*)***
  %vtable2 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %4, align 4
  %vfn3 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable2, i64 12
  %5 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn3, align 4
  %call = call float %5(%class.btConvexInternalShape* %3)
  %cmp = fcmp une float %call, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end16

if.then:                                          ; preds = %entry
  %6 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4
  %7 = bitcast %class.btVector3* %vecnorm to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %call4 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %vecnorm)
  %cmp5 = fcmp olt float %call4, 0x3D10000000000000
  br i1 %cmp5, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.then
  store float -1.000000e+00, float* %ref.tmp, align 4
  store float -1.000000e+00, float* %ref.tmp7, align 4
  store float -1.000000e+00, float* %ref.tmp8, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %vecnorm, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  br label %if.end

if.end:                                           ; preds = %if.then6, %if.then
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %vecnorm)
  %9 = bitcast %class.btConvexTriangleMeshShape* %this1 to %class.btConvexInternalShape*
  %10 = bitcast %class.btConvexInternalShape* %9 to float (%class.btConvexInternalShape*)***
  %vtable12 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %10, align 4
  %vfn13 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable12, i64 12
  %11 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn13, align 4
  %call14 = call float %11(%class.btConvexInternalShape* %9)
  store float %call14, float* %ref.tmp11, align 4
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %vecnorm)
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10)
  br label %if.end16

if.end16:                                         ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZNK25btConvexTriangleMeshShape14getNumVerticesEv(%class.btConvexTriangleMeshShape* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConvexTriangleMeshShape*, align 4
  store %class.btConvexTriangleMeshShape* %this, %class.btConvexTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btConvexTriangleMeshShape*, %class.btConvexTriangleMeshShape** %this.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZNK25btConvexTriangleMeshShape11getNumEdgesEv(%class.btConvexTriangleMeshShape* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConvexTriangleMeshShape*, align 4
  store %class.btConvexTriangleMeshShape* %this, %class.btConvexTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btConvexTriangleMeshShape*, %class.btConvexTriangleMeshShape** %this.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK25btConvexTriangleMeshShape7getEdgeEiR9btVector3S1_(%class.btConvexTriangleMeshShape* %this, i32 %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConvexTriangleMeshShape*, align 4
  %.addr = alloca i32, align 4
  %.addr1 = alloca %class.btVector3*, align 4
  %.addr2 = alloca %class.btVector3*, align 4
  store %class.btConvexTriangleMeshShape* %this, %class.btConvexTriangleMeshShape** %this.addr, align 4
  store i32 %0, i32* %.addr, align 4
  store %class.btVector3* %1, %class.btVector3** %.addr1, align 4
  store %class.btVector3* %2, %class.btVector3** %.addr2, align 4
  %this3 = load %class.btConvexTriangleMeshShape*, %class.btConvexTriangleMeshShape** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK25btConvexTriangleMeshShape9getVertexEiR9btVector3(%class.btConvexTriangleMeshShape* %this, i32 %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConvexTriangleMeshShape*, align 4
  %.addr = alloca i32, align 4
  %.addr1 = alloca %class.btVector3*, align 4
  store %class.btConvexTriangleMeshShape* %this, %class.btConvexTriangleMeshShape** %this.addr, align 4
  store i32 %0, i32* %.addr, align 4
  store %class.btVector3* %1, %class.btVector3** %.addr1, align 4
  %this2 = load %class.btConvexTriangleMeshShape*, %class.btConvexTriangleMeshShape** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZNK25btConvexTriangleMeshShape12getNumPlanesEv(%class.btConvexTriangleMeshShape* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConvexTriangleMeshShape*, align 4
  store %class.btConvexTriangleMeshShape* %this, %class.btConvexTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btConvexTriangleMeshShape*, %class.btConvexTriangleMeshShape** %this.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK25btConvexTriangleMeshShape8getPlaneER9btVector3S1_i(%class.btConvexTriangleMeshShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, i32 %2) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConvexTriangleMeshShape*, align 4
  %.addr = alloca %class.btVector3*, align 4
  %.addr1 = alloca %class.btVector3*, align 4
  %.addr2 = alloca i32, align 4
  store %class.btConvexTriangleMeshShape* %this, %class.btConvexTriangleMeshShape** %this.addr, align 4
  store %class.btVector3* %0, %class.btVector3** %.addr, align 4
  store %class.btVector3* %1, %class.btVector3** %.addr1, align 4
  store i32 %2, i32* %.addr2, align 4
  %this3 = load %class.btConvexTriangleMeshShape*, %class.btConvexTriangleMeshShape** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZNK25btConvexTriangleMeshShape8isInsideERK9btVector3f(%class.btConvexTriangleMeshShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float %1) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConvexTriangleMeshShape*, align 4
  %.addr = alloca %class.btVector3*, align 4
  %.addr1 = alloca float, align 4
  store %class.btConvexTriangleMeshShape* %this, %class.btConvexTriangleMeshShape** %this.addr, align 4
  store %class.btVector3* %0, %class.btVector3** %.addr, align 4
  store float %1, float* %.addr1, align 4
  %this2 = load %class.btConvexTriangleMeshShape*, %class.btConvexTriangleMeshShape** %this.addr, align 4
  ret i1 false
}

; Function Attrs: noinline optnone
define hidden void @_ZN25btConvexTriangleMeshShape15setLocalScalingERK9btVector3(%class.btConvexTriangleMeshShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvexTriangleMeshShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  store %class.btConvexTriangleMeshShape* %this, %class.btConvexTriangleMeshShape** %this.addr, align 4
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4
  %this1 = load %class.btConvexTriangleMeshShape*, %class.btConvexTriangleMeshShape** %this.addr, align 4
  %m_stridingMesh = getelementptr inbounds %class.btConvexTriangleMeshShape, %class.btConvexTriangleMeshShape* %this1, i32 0, i32 1
  %0 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_stridingMesh, align 4
  %1 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4
  call void @_ZN23btStridingMeshInterface10setScalingERK9btVector3(%class.btStridingMeshInterface* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = bitcast %class.btConvexTriangleMeshShape* %this1 to %class.btPolyhedralConvexAabbCachingShape*
  call void @_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv(%class.btPolyhedralConvexAabbCachingShape* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN23btStridingMeshInterface10setScalingERK9btVector3(%class.btStridingMeshInterface* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) #1 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4
  %m_scaling = getelementptr inbounds %class.btStridingMeshInterface, %class.btStridingMeshInterface* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_scaling to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK25btConvexTriangleMeshShape15getLocalScalingEv(%class.btConvexTriangleMeshShape* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvexTriangleMeshShape*, align 4
  store %class.btConvexTriangleMeshShape* %this, %class.btConvexTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btConvexTriangleMeshShape*, %class.btConvexTriangleMeshShape** %this.addr, align 4
  %m_stridingMesh = getelementptr inbounds %class.btConvexTriangleMeshShape, %class.btConvexTriangleMeshShape* %this1, i32 0, i32 1
  %0 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_stridingMesh, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %0)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %m_scaling = getelementptr inbounds %class.btStridingMeshInterface, %class.btStridingMeshInterface* %this1, i32 0, i32 1
  ret %class.btVector3* %m_scaling
}

; Function Attrs: noinline optnone
define hidden void @_ZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3Rf(%class.btConvexTriangleMeshShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %principal, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia, float* nonnull align 4 dereferenceable(4) %volume) #2 {
entry:
  %this.addr = alloca %class.btConvexTriangleMeshShape*, align 4
  %principal.addr = alloca %class.btTransform*, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %volume.addr = alloca float*, align 4
  %centerCallback = alloca %class.CenterCallback, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %center = alloca %class.btVector3, align 4
  %inertiaCallback = alloca %class.InertiaCallback, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %i = alloca %class.btMatrix3x3*, align 4
  store %class.btConvexTriangleMeshShape* %this, %class.btConvexTriangleMeshShape** %this.addr, align 4
  store %class.btTransform* %principal, %class.btTransform** %principal.addr, align 4
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4
  store float* %volume, float** %volume.addr, align 4
  %this1 = load %class.btConvexTriangleMeshShape*, %class.btConvexTriangleMeshShape** %this.addr, align 4
  %call = call %class.CenterCallback* @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallbackC2Ev(%class.CenterCallback* %centerCallback)
  store float 0x43ABC16D60000000, float* %ref.tmp, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp2, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp3, align 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %aabbMax, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %m_stridingMesh = getelementptr inbounds %class.btConvexTriangleMeshShape, %class.btConvexTriangleMeshShape* %this1, i32 0, i32 1
  %0 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_stridingMesh, align 4
  %1 = bitcast %class.CenterCallback* %centerCallback to %class.btInternalTriangleIndexCallback*
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  %2 = bitcast %class.btStridingMeshInterface* %0 to void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*** %2, align 4
  %vfn = getelementptr inbounds void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %3 = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %3(%class.btStridingMeshInterface* %0, %class.btInternalTriangleIndexCallback* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  call void @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallback9getCenterEv(%class.btVector3* sret align 4 %center, %class.CenterCallback* %centerCallback)
  %4 = load %class.btTransform*, %class.btTransform** %principal.addr, align 4
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  %call6 = call float @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallback9getVolumeEv(%class.CenterCallback* %centerCallback)
  %5 = load float*, float** %volume.addr, align 4
  store float %call6, float* %5, align 4
  %call7 = call %class.InertiaCallback* @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallbackC2ES3_(%class.InertiaCallback* %inertiaCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  %m_stridingMesh8 = getelementptr inbounds %class.btConvexTriangleMeshShape, %class.btConvexTriangleMeshShape* %this1, i32 0, i32 1
  %6 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_stridingMesh8, align 4
  %7 = bitcast %class.InertiaCallback* %inertiaCallback to %class.btInternalTriangleIndexCallback*
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  %8 = bitcast %class.btStridingMeshInterface* %6 to void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable10 = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*** %8, align 4
  %vfn11 = getelementptr inbounds void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vtable10, i64 2
  %9 = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vfn11, align 4
  call void %9(%class.btStridingMeshInterface* %6, %class.btInternalTriangleIndexCallback* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  %call12 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallback10getInertiaEv(%class.InertiaCallback* %inertiaCallback)
  store %class.btMatrix3x3* %call12, %class.btMatrix3x3** %i, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %i, align 4
  %11 = load %class.btTransform*, %class.btTransform** %principal.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %11)
  call void @_ZN11btMatrix3x311diagonalizeERS_fi(%class.btMatrix3x3* %10, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call13, float 0x3EE4F8B580000000, i32 20)
  %12 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %i, align 4
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %13, i32 0)
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call14)
  %arrayidx = getelementptr inbounds float, float* %call15, i32 0
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %i, align 4
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 1)
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call16)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %i, align 4
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %15, i32 2)
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call19)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %12, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx18, float* nonnull align 4 dereferenceable(4) %arrayidx21)
  %16 = load float*, float** %volume.addr, align 4
  %17 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %17, float* nonnull align 4 dereferenceable(4) %16)
  %call23 = call %class.InertiaCallback* @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallbackD2Ev(%class.InertiaCallback* %inertiaCallback) #8
  %call24 = call %class.CenterCallback* @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallbackD2Ev(%class.CenterCallback* %centerCallback) #8
  ret void
}

; Function Attrs: noinline optnone
define internal %class.CenterCallback* @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallbackC2Ev(%class.CenterCallback* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.CenterCallback*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  store %class.CenterCallback* %this, %class.CenterCallback** %this.addr, align 4
  %this1 = load %class.CenterCallback*, %class.CenterCallback** %this.addr, align 4
  %0 = bitcast %class.CenterCallback* %this1 to %class.btInternalTriangleIndexCallback*
  %call = call %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackC2Ev(%class.btInternalTriangleIndexCallback* %0) #8
  %1 = bitcast %class.CenterCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %first = getelementptr inbounds %class.CenterCallback, %class.CenterCallback* %this1, i32 0, i32 1
  store i8 1, i8* %first, align 4
  %ref = getelementptr inbounds %class.CenterCallback, %class.CenterCallback* %this1, i32 0, i32 2
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %sum = getelementptr inbounds %class.CenterCallback, %class.CenterCallback* %this1, i32 0, i32 3
  store float 0.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %sum, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %volume = getelementptr inbounds %class.CenterCallback, %class.CenterCallback* %this1, i32 0, i32 4
  store float 0.000000e+00, float* %volume, align 4
  ret %class.CenterCallback* %this1
}

; Function Attrs: noinline optnone
define internal void @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallback9getCenterEv(%class.btVector3* noalias sret align 4 %agg.result, %class.CenterCallback* %this) #2 {
entry:
  %this.addr = alloca %class.CenterCallback*, align 4
  store %class.CenterCallback* %this, %class.CenterCallback** %this.addr, align 4
  %this1 = load %class.CenterCallback*, %class.CenterCallback** %this.addr, align 4
  %volume = getelementptr inbounds %class.CenterCallback, %class.CenterCallback* %this1, i32 0, i32 4
  %0 = load float, float* %volume, align 4
  %cmp = fcmp ogt float %0, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %sum = getelementptr inbounds %class.CenterCallback, %class.CenterCallback* %this1, i32 0, i32 3
  %volume2 = getelementptr inbounds %class.CenterCallback, %class.CenterCallback* %this1, i32 0, i32 4
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %sum, float* nonnull align 4 dereferenceable(4) %volume2)
  br label %cond.end

cond.false:                                       ; preds = %entry
  %ref = getelementptr inbounds %class.CenterCallback, %class.CenterCallback* %this1, i32 0, i32 2
  %1 = bitcast %class.btVector3* %agg.result to i8*
  %2 = bitcast %class.btVector3* %ref to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal float @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallback9getVolumeEv(%class.CenterCallback* %this) #1 {
entry:
  %this.addr = alloca %class.CenterCallback*, align 4
  store %class.CenterCallback* %this, %class.CenterCallback** %this.addr, align 4
  %this1 = load %class.CenterCallback*, %class.CenterCallback** %this.addr, align 4
  %volume = getelementptr inbounds %class.CenterCallback, %class.CenterCallback* %this1, i32 0, i32 4
  %0 = load float, float* %volume, align 4
  %mul = fmul float %0, 0x3FC5555560000000
  ret float %mul
}

; Function Attrs: noinline optnone
define internal %class.InertiaCallback* @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallbackC2ES3_(%class.InertiaCallback* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %center) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.InertiaCallback*, align 4
  %center.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.InertiaCallback* %this, %class.InertiaCallback** %this.addr, align 4
  store %class.btVector3* %center, %class.btVector3** %center.addr, align 4
  %this1 = load %class.InertiaCallback*, %class.InertiaCallback** %this.addr, align 4
  %0 = bitcast %class.InertiaCallback* %this1 to %class.btInternalTriangleIndexCallback*
  %call = call %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackC2Ev(%class.btInternalTriangleIndexCallback* %0) #8
  %1 = bitcast %class.InertiaCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %sum = getelementptr inbounds %class.InertiaCallback, %class.InertiaCallback* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 0.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 0.000000e+00, float* %ref.tmp9, align 4
  %call10 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %sum, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %center11 = getelementptr inbounds %class.InertiaCallback, %class.InertiaCallback* %this1, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %center.addr, align 4
  %3 = bitcast %class.btVector3* %center11 to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  ret %class.InertiaCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define internal nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallback10getInertiaEv(%class.InertiaCallback* %this) #1 {
entry:
  %this.addr = alloca %class.InertiaCallback*, align 4
  store %class.InertiaCallback* %this, %class.InertiaCallback** %this.addr, align 4
  %this1 = load %class.InertiaCallback*, %class.InertiaCallback** %this.addr, align 4
  %sum = getelementptr inbounds %class.InertiaCallback, %class.InertiaCallback* %this1, i32 0, i32 1
  ret %class.btMatrix3x3* %sum
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311diagonalizeERS_fi(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %rot, float %threshold, i32 %maxSteps) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %rot.addr = alloca %class.btMatrix3x3*, align 4
  %threshold.addr = alloca float, align 4
  %maxSteps.addr = alloca i32, align 4
  %step = alloca i32, align 4
  %p = alloca i32, align 4
  %q = alloca i32, align 4
  %r = alloca i32, align 4
  %max = alloca float, align 4
  %v = alloca float, align 4
  %t = alloca float, align 4
  %mpq = alloca float, align 4
  %theta = alloca float, align 4
  %theta2 = alloca float, align 4
  %cos = alloca float, align 4
  %sin = alloca float, align 4
  %mrp = alloca float, align 4
  %mrq = alloca float, align 4
  %i = alloca i32, align 4
  %row = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %rot, %class.btMatrix3x3** %rot.addr, align 4
  store float %threshold, float* %threshold.addr, align 4
  store i32 %maxSteps, i32* %maxSteps.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot.addr, align 4
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %0)
  %1 = load i32, i32* %maxSteps.addr, align 4
  store i32 %1, i32* %step, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc149, %entry
  %2 = load i32, i32* %step, align 4
  %cmp = icmp sgt i32 %2, 0
  br i1 %cmp, label %for.body, label %for.end150

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %p, align 4
  store i32 1, i32* %q, align 4
  store i32 2, i32* %r, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx)
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 1
  %3 = load float, float* %arrayidx2, align 4
  %call3 = call float @_Z6btFabsf(float %3)
  store float %call3, float* %max, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx5)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  %4 = load float, float* %arrayidx7, align 4
  %call8 = call float @_Z6btFabsf(float %4)
  store float %call8, float* %v, align 4
  %5 = load float, float* %v, align 4
  %6 = load float, float* %max, align 4
  %cmp9 = fcmp ogt float %5, %6
  br i1 %cmp9, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i32 2, i32* %q, align 4
  store i32 1, i32* %r, align 4
  %7 = load float, float* %v, align 4
  store float %7, float* %max, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 1
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx11)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 2
  %8 = load float, float* %arrayidx13, align 4
  %call14 = call float @_Z6btFabsf(float %8)
  store float %call14, float* %v, align 4
  %9 = load float, float* %v, align 4
  %10 = load float, float* %max, align 4
  %cmp15 = fcmp ogt float %9, %10
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end
  store i32 1, i32* %p, align 4
  store i32 2, i32* %q, align 4
  store i32 0, i32* %r, align 4
  %11 = load float, float* %v, align 4
  store float %11, float* %max, align 4
  br label %if.end17

if.end17:                                         ; preds = %if.then16, %if.end
  %12 = load float, float* %threshold.addr, align 4
  %m_el18 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx19 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el18, i32 0, i32 0
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx19)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 0
  %13 = load float, float* %arrayidx21, align 4
  %call22 = call float @_Z6btFabsf(float %13)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 1
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx24)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 1
  %14 = load float, float* %arrayidx26, align 4
  %call27 = call float @_Z6btFabsf(float %14)
  %add = fadd float %call22, %call27
  %m_el28 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx29 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el28, i32 0, i32 2
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx29)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %15 = load float, float* %arrayidx31, align 4
  %call32 = call float @_Z6btFabsf(float %15)
  %add33 = fadd float %add, %call32
  %mul = fmul float %12, %add33
  store float %mul, float* %t, align 4
  %16 = load float, float* %max, align 4
  %17 = load float, float* %t, align 4
  %cmp34 = fcmp ole float %16, %17
  br i1 %cmp34, label %if.then35, label %if.end40

if.then35:                                        ; preds = %if.end17
  %18 = load float, float* %max, align 4
  %19 = load float, float* %t, align 4
  %mul36 = fmul float 0x3E80000000000000, %19
  %cmp37 = fcmp ole float %18, %mul36
  br i1 %cmp37, label %if.then38, label %if.end39

if.then38:                                        ; preds = %if.then35
  br label %for.end150

if.end39:                                         ; preds = %if.then35
  store i32 1, i32* %step, align 4
  br label %if.end40

if.end40:                                         ; preds = %if.end39, %if.end17
  %m_el41 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %20 = load i32, i32* %p, align 4
  %arrayidx42 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el41, i32 0, i32 %20
  %call43 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx42)
  %21 = load i32, i32* %q, align 4
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 %21
  %22 = load float, float* %arrayidx44, align 4
  store float %22, float* %mpq, align 4
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %23 = load i32, i32* %q, align 4
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 %23
  %call47 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx46)
  %24 = load i32, i32* %q, align 4
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 %24
  %25 = load float, float* %arrayidx48, align 4
  %m_el49 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %26 = load i32, i32* %p, align 4
  %arrayidx50 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el49, i32 0, i32 %26
  %call51 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx50)
  %27 = load i32, i32* %p, align 4
  %arrayidx52 = getelementptr inbounds float, float* %call51, i32 %27
  %28 = load float, float* %arrayidx52, align 4
  %sub = fsub float %25, %28
  %29 = load float, float* %mpq, align 4
  %mul53 = fmul float 2.000000e+00, %29
  %div = fdiv float %sub, %mul53
  store float %div, float* %theta, align 4
  %30 = load float, float* %theta, align 4
  %31 = load float, float* %theta, align 4
  %mul54 = fmul float %30, %31
  store float %mul54, float* %theta2, align 4
  %32 = load float, float* %theta2, align 4
  %33 = load float, float* %theta2, align 4
  %mul55 = fmul float %32, %33
  %cmp56 = fcmp olt float %mul55, 0x4194000000000000
  br i1 %cmp56, label %if.then57, label %if.else

if.then57:                                        ; preds = %if.end40
  %34 = load float, float* %theta, align 4
  %cmp58 = fcmp oge float %34, 0.000000e+00
  br i1 %cmp58, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then57
  %35 = load float, float* %theta, align 4
  %36 = load float, float* %theta2, align 4
  %add59 = fadd float 1.000000e+00, %36
  %call60 = call float @_Z6btSqrtf(float %add59)
  %add61 = fadd float %35, %call60
  %div62 = fdiv float 1.000000e+00, %add61
  br label %cond.end

cond.false:                                       ; preds = %if.then57
  %37 = load float, float* %theta, align 4
  %38 = load float, float* %theta2, align 4
  %add63 = fadd float 1.000000e+00, %38
  %call64 = call float @_Z6btSqrtf(float %add63)
  %sub65 = fsub float %37, %call64
  %div66 = fdiv float 1.000000e+00, %sub65
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %div62, %cond.true ], [ %div66, %cond.false ]
  store float %cond, float* %t, align 4
  %39 = load float, float* %t, align 4
  %40 = load float, float* %t, align 4
  %mul67 = fmul float %39, %40
  %add68 = fadd float 1.000000e+00, %mul67
  %call69 = call float @_Z6btSqrtf(float %add68)
  %div70 = fdiv float 1.000000e+00, %call69
  store float %div70, float* %cos, align 4
  %41 = load float, float* %cos, align 4
  %42 = load float, float* %t, align 4
  %mul71 = fmul float %41, %42
  store float %mul71, float* %sin, align 4
  br label %if.end80

if.else:                                          ; preds = %if.end40
  %43 = load float, float* %theta, align 4
  %44 = load float, float* %theta2, align 4
  %div72 = fdiv float 5.000000e-01, %44
  %add73 = fadd float 2.000000e+00, %div72
  %mul74 = fmul float %43, %add73
  %div75 = fdiv float 1.000000e+00, %mul74
  store float %div75, float* %t, align 4
  %45 = load float, float* %t, align 4
  %mul76 = fmul float 5.000000e-01, %45
  %46 = load float, float* %t, align 4
  %mul77 = fmul float %mul76, %46
  %sub78 = fsub float 1.000000e+00, %mul77
  store float %sub78, float* %cos, align 4
  %47 = load float, float* %cos, align 4
  %48 = load float, float* %t, align 4
  %mul79 = fmul float %47, %48
  store float %mul79, float* %sin, align 4
  br label %if.end80

if.end80:                                         ; preds = %if.else, %cond.end
  %m_el81 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %49 = load i32, i32* %q, align 4
  %arrayidx82 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el81, i32 0, i32 %49
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx82)
  %50 = load i32, i32* %p, align 4
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 %50
  store float 0.000000e+00, float* %arrayidx84, align 4
  %m_el85 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %51 = load i32, i32* %p, align 4
  %arrayidx86 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el85, i32 0, i32 %51
  %call87 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx86)
  %52 = load i32, i32* %q, align 4
  %arrayidx88 = getelementptr inbounds float, float* %call87, i32 %52
  store float 0.000000e+00, float* %arrayidx88, align 4
  %53 = load float, float* %t, align 4
  %54 = load float, float* %mpq, align 4
  %mul89 = fmul float %53, %54
  %m_el90 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %55 = load i32, i32* %p, align 4
  %arrayidx91 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el90, i32 0, i32 %55
  %call92 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx91)
  %56 = load i32, i32* %p, align 4
  %arrayidx93 = getelementptr inbounds float, float* %call92, i32 %56
  %57 = load float, float* %arrayidx93, align 4
  %sub94 = fsub float %57, %mul89
  store float %sub94, float* %arrayidx93, align 4
  %58 = load float, float* %t, align 4
  %59 = load float, float* %mpq, align 4
  %mul95 = fmul float %58, %59
  %m_el96 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %60 = load i32, i32* %q, align 4
  %arrayidx97 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el96, i32 0, i32 %60
  %call98 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx97)
  %61 = load i32, i32* %q, align 4
  %arrayidx99 = getelementptr inbounds float, float* %call98, i32 %61
  %62 = load float, float* %arrayidx99, align 4
  %add100 = fadd float %62, %mul95
  store float %add100, float* %arrayidx99, align 4
  %m_el101 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %63 = load i32, i32* %r, align 4
  %arrayidx102 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el101, i32 0, i32 %63
  %call103 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx102)
  %64 = load i32, i32* %p, align 4
  %arrayidx104 = getelementptr inbounds float, float* %call103, i32 %64
  %65 = load float, float* %arrayidx104, align 4
  store float %65, float* %mrp, align 4
  %m_el105 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %66 = load i32, i32* %r, align 4
  %arrayidx106 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el105, i32 0, i32 %66
  %call107 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx106)
  %67 = load i32, i32* %q, align 4
  %arrayidx108 = getelementptr inbounds float, float* %call107, i32 %67
  %68 = load float, float* %arrayidx108, align 4
  store float %68, float* %mrq, align 4
  %69 = load float, float* %cos, align 4
  %70 = load float, float* %mrp, align 4
  %mul109 = fmul float %69, %70
  %71 = load float, float* %sin, align 4
  %72 = load float, float* %mrq, align 4
  %mul110 = fmul float %71, %72
  %sub111 = fsub float %mul109, %mul110
  %m_el112 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %73 = load i32, i32* %p, align 4
  %arrayidx113 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el112, i32 0, i32 %73
  %call114 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx113)
  %74 = load i32, i32* %r, align 4
  %arrayidx115 = getelementptr inbounds float, float* %call114, i32 %74
  store float %sub111, float* %arrayidx115, align 4
  %m_el116 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %75 = load i32, i32* %r, align 4
  %arrayidx117 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el116, i32 0, i32 %75
  %call118 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx117)
  %76 = load i32, i32* %p, align 4
  %arrayidx119 = getelementptr inbounds float, float* %call118, i32 %76
  store float %sub111, float* %arrayidx119, align 4
  %77 = load float, float* %cos, align 4
  %78 = load float, float* %mrq, align 4
  %mul120 = fmul float %77, %78
  %79 = load float, float* %sin, align 4
  %80 = load float, float* %mrp, align 4
  %mul121 = fmul float %79, %80
  %add122 = fadd float %mul120, %mul121
  %m_el123 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %81 = load i32, i32* %q, align 4
  %arrayidx124 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el123, i32 0, i32 %81
  %call125 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx124)
  %82 = load i32, i32* %r, align 4
  %arrayidx126 = getelementptr inbounds float, float* %call125, i32 %82
  store float %add122, float* %arrayidx126, align 4
  %m_el127 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %83 = load i32, i32* %r, align 4
  %arrayidx128 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el127, i32 0, i32 %83
  %call129 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx128)
  %84 = load i32, i32* %q, align 4
  %arrayidx130 = getelementptr inbounds float, float* %call129, i32 %84
  store float %add122, float* %arrayidx130, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond131

for.cond131:                                      ; preds = %for.inc, %if.end80
  %85 = load i32, i32* %i, align 4
  %cmp132 = icmp slt i32 %85, 3
  br i1 %cmp132, label %for.body133, label %for.end

for.body133:                                      ; preds = %for.cond131
  %86 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot.addr, align 4
  %87 = load i32, i32* %i, align 4
  %call134 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %86, i32 %87)
  store %class.btVector3* %call134, %class.btVector3** %row, align 4
  %88 = load %class.btVector3*, %class.btVector3** %row, align 4
  %call135 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %88)
  %89 = load i32, i32* %p, align 4
  %arrayidx136 = getelementptr inbounds float, float* %call135, i32 %89
  %90 = load float, float* %arrayidx136, align 4
  store float %90, float* %mrp, align 4
  %91 = load %class.btVector3*, %class.btVector3** %row, align 4
  %call137 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %91)
  %92 = load i32, i32* %q, align 4
  %arrayidx138 = getelementptr inbounds float, float* %call137, i32 %92
  %93 = load float, float* %arrayidx138, align 4
  store float %93, float* %mrq, align 4
  %94 = load float, float* %cos, align 4
  %95 = load float, float* %mrp, align 4
  %mul139 = fmul float %94, %95
  %96 = load float, float* %sin, align 4
  %97 = load float, float* %mrq, align 4
  %mul140 = fmul float %96, %97
  %sub141 = fsub float %mul139, %mul140
  %98 = load %class.btVector3*, %class.btVector3** %row, align 4
  %call142 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %98)
  %99 = load i32, i32* %p, align 4
  %arrayidx143 = getelementptr inbounds float, float* %call142, i32 %99
  store float %sub141, float* %arrayidx143, align 4
  %100 = load float, float* %cos, align 4
  %101 = load float, float* %mrq, align 4
  %mul144 = fmul float %100, %101
  %102 = load float, float* %sin, align 4
  %103 = load float, float* %mrp, align 4
  %mul145 = fmul float %102, %103
  %add146 = fadd float %mul144, %mul145
  %104 = load %class.btVector3*, %class.btVector3** %row, align 4
  %call147 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %104)
  %105 = load i32, i32* %q, align 4
  %arrayidx148 = getelementptr inbounds float, float* %call147, i32 %105
  store float %add146, float* %arrayidx148, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body133
  %106 = load i32, i32* %i, align 4
  %inc = add nsw i32 %106, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond131

for.end:                                          ; preds = %for.cond131
  br label %for.inc149

for.inc149:                                       ; preds = %for.end
  %107 = load i32, i32* %step, align 4
  %dec = add nsw i32 %107, -1
  store i32 %dec, i32* %step, align 4
  br label %for.cond

for.end150:                                       ; preds = %if.then38, %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define internal %class.InertiaCallback* @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallbackD2Ev(%class.InertiaCallback* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.InertiaCallback*, align 4
  store %class.InertiaCallback* %this, %class.InertiaCallback** %this.addr, align 4
  %this1 = load %class.InertiaCallback*, %class.InertiaCallback** %this.addr, align 4
  %0 = bitcast %class.InertiaCallback* %this1 to %class.btInternalTriangleIndexCallback*
  %call = call %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackD2Ev(%class.btInternalTriangleIndexCallback* %0) #8
  ret %class.InertiaCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define internal %class.CenterCallback* @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallbackD2Ev(%class.CenterCallback* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.CenterCallback*, align 4
  store %class.CenterCallback* %this, %class.CenterCallback** %this.addr, align 4
  %this1 = load %class.CenterCallback*, %class.CenterCallback** %this.addr, align 4
  %0 = bitcast %class.CenterCallback* %this1 to %class.btInternalTriangleIndexCallback*
  %call = call %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackD2Ev(%class.btInternalTriangleIndexCallback* %0) #8
  ret %class.CenterCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btConvexTriangleMeshShape* @_ZN25btConvexTriangleMeshShapeD2Ev(%class.btConvexTriangleMeshShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexTriangleMeshShape*, align 4
  store %class.btConvexTriangleMeshShape* %this, %class.btConvexTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btConvexTriangleMeshShape*, %class.btConvexTriangleMeshShape** %this.addr, align 4
  %0 = bitcast %class.btConvexTriangleMeshShape* %this1 to %class.btPolyhedralConvexAabbCachingShape*
  %call = call %class.btPolyhedralConvexAabbCachingShape* @_ZN34btPolyhedralConvexAabbCachingShapeD2Ev(%class.btPolyhedralConvexAabbCachingShape* %0) #8
  ret %class.btConvexTriangleMeshShape* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN25btConvexTriangleMeshShapeD0Ev(%class.btConvexTriangleMeshShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexTriangleMeshShape*, align 4
  store %class.btConvexTriangleMeshShape* %this, %class.btConvexTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btConvexTriangleMeshShape*, %class.btConvexTriangleMeshShape** %this.addr, align 4
  %call = call %class.btConvexTriangleMeshShape* @_ZN25btConvexTriangleMeshShapeD2Ev(%class.btConvexTriangleMeshShape* %this1) #8
  %0 = bitcast %class.btConvexTriangleMeshShape* %this1 to i8*
  call void @_ZN25btConvexTriangleMeshShapedlEPv(i8* %0) #8
  ret void
}

declare void @_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_(%class.btPolyhedralConvexAabbCachingShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #3

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #3

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #3

declare void @_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3(%class.btPolyhedralConvexShape*, float, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK25btConvexTriangleMeshShape7getNameEv(%class.btConvexTriangleMeshShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexTriangleMeshShape*, align 4
  store %class.btConvexTriangleMeshShape* %this, %class.btConvexTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btConvexTriangleMeshShape*, %class.btConvexTriangleMeshShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  store float 1.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btConvexInternalShape9setMarginEf(%class.btConvexInternalShape* %this, float %margin) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %margin.addr = alloca float, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  store float %margin, float* %margin.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  store float %0, float* %m_collisionMargin, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK21btConvexInternalShape9getMarginEv(%class.btConvexInternalShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %0 = load float, float* %m_collisionMargin, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv(%class.btConvexInternalShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 52
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer(%class.btConvexInternalShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btConvexInternalShapeData*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btConvexInternalShapeData*
  store %struct.btConvexInternalShapeData* %1, %struct.btConvexInternalShapeData** %shapeData, align 4
  %2 = bitcast %class.btConvexInternalShape* %this1 to %class.btCollisionShape*
  %3 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4
  %m_collisionShapeData = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %3, i32 0, i32 0
  %4 = bitcast %struct.btCollisionShapeData* %m_collisionShapeData to i8*
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %call = call i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape* %2, i8* %4, %class.btSerializer* %5)
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 2
  %6 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4
  %m_implicitShapeDimensions2 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %6, i32 0, i32 2
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_implicitShapeDimensions, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_implicitShapeDimensions2)
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  %7 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4
  %m_localScaling3 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %7, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_localScaling, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_localScaling3)
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %8 = load float, float* %m_collisionMargin, align 4
  %9 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4
  %m_collisionMargin4 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %9, i32 0, i32 3
  store float %8, float* %m_collisionMargin4, align 4
  ret i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.1, i32 0, i32 0)
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #3

declare void @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_RS3_S7_(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float* nonnull align 4 dereferenceable(4), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

declare void @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_(%class.btConvexInternalShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv(%class.btConvexInternalShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3(%class.btConvexInternalShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %penetrationVector) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %index.addr = alloca i32, align 4
  %penetrationVector.addr = alloca %class.btVector3*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store %class.btVector3* %penetrationVector, %class.btVector3** %penetrationVector.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4
  ret void
}

declare zeroext i1 @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi(%class.btPolyhedralConvexShape*, i32) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackC2Ev(%class.btInternalTriangleIndexCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btInternalTriangleIndexCallback*, align 4
  store %class.btInternalTriangleIndexCallback* %this, %class.btInternalTriangleIndexCallback** %this.addr, align 4
  %this1 = load %class.btInternalTriangleIndexCallback*, %class.btInternalTriangleIndexCallback** %this.addr, align 4
  %0 = bitcast %class.btInternalTriangleIndexCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV31btInternalTriangleIndexCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btInternalTriangleIndexCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN26LocalSupportVertexCallbackD0Ev(%class.LocalSupportVertexCallback* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.LocalSupportVertexCallback*, align 4
  store %class.LocalSupportVertexCallback* %this, %class.LocalSupportVertexCallback** %this.addr, align 4
  %this1 = load %class.LocalSupportVertexCallback*, %class.LocalSupportVertexCallback** %this.addr, align 4
  %call = call %class.LocalSupportVertexCallback* @_ZN26LocalSupportVertexCallbackD2Ev(%class.LocalSupportVertexCallback* %this1) #8
  %0 = bitcast %class.LocalSupportVertexCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN26LocalSupportVertexCallback28internalProcessTriangleIndexEP9btVector3ii(%class.LocalSupportVertexCallback* %this, %class.btVector3* %triangle, i32 %partId, i32 %triangleIndex) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.LocalSupportVertexCallback*, align 4
  %triangle.addr = alloca %class.btVector3*, align 4
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %dot = alloca float, align 4
  store %class.LocalSupportVertexCallback* %this, %class.LocalSupportVertexCallback** %this.addr, align 4
  store %class.btVector3* %triangle, %class.btVector3** %triangle.addr, align 4
  store i32 %partId, i32* %partId.addr, align 4
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4
  %this1 = load %class.LocalSupportVertexCallback*, %class.LocalSupportVertexCallback** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_supportVecLocal = getelementptr inbounds %class.LocalSupportVertexCallback, %class.LocalSupportVertexCallback* %this1, i32 0, i32 3
  %1 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 %2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_supportVecLocal, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  store float %call, float* %dot, align 4
  %3 = load float, float* %dot, align 4
  %m_maxDot = getelementptr inbounds %class.LocalSupportVertexCallback, %class.LocalSupportVertexCallback* %this1, i32 0, i32 2
  %4 = load float, float* %m_maxDot, align 4
  %cmp2 = fcmp ogt float %3, %4
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %5 = load float, float* %dot, align 4
  %m_maxDot3 = getelementptr inbounds %class.LocalSupportVertexCallback, %class.LocalSupportVertexCallback* %this1, i32 0, i32 2
  store float %5, float* %m_maxDot3, align 4
  %6 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 %7
  %m_supportVertexLocal = getelementptr inbounds %class.LocalSupportVertexCallback, %class.LocalSupportVertexCallback* %this1, i32 0, i32 1
  %8 = bitcast %class.btVector3* %m_supportVertexLocal to i8*
  %9 = bitcast %class.btVector3* %arrayidx4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

; Function Attrs: nounwind
declare %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackD2Ev(%class.btInternalTriangleIndexCallback* returned) unnamed_addr #7

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallbackD0Ev(%class.CenterCallback* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.CenterCallback*, align 4
  store %class.CenterCallback* %this, %class.CenterCallback** %this.addr, align 4
  %this1 = load %class.CenterCallback*, %class.CenterCallback** %this.addr, align 4
  %call = call %class.CenterCallback* @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallbackD2Ev(%class.CenterCallback* %this1) #8
  %0 = bitcast %class.CenterCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define internal void @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallback28internalProcessTriangleIndexEPS2_ii(%class.CenterCallback* %this, %class.btVector3* %triangle, i32 %partId, i32 %triangleIndex) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.CenterCallback*, align 4
  %triangle.addr = alloca %class.btVector3*, align 4
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  %vol = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  store %class.CenterCallback* %this, %class.CenterCallback** %this.addr, align 4
  store %class.btVector3* %triangle, %class.btVector3** %triangle.addr, align 4
  store i32 %partId, i32* %partId.addr, align 4
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4
  %this1 = load %class.CenterCallback*, %class.CenterCallback** %this.addr, align 4
  %first = getelementptr inbounds %class.CenterCallback, %class.CenterCallback* %this1, i32 0, i32 1
  %0 = load i8, i8* %first, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0
  %ref = getelementptr inbounds %class.CenterCallback, %class.CenterCallback* %this1, i32 0, i32 2
  %2 = bitcast %class.btVector3* %ref to i8*
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %first2 = getelementptr inbounds %class.CenterCallback, %class.CenterCallback* %this1, i32 0, i32 1
  store i8 0, i8* %first2, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %4 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx3 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0
  %ref4 = getelementptr inbounds %class.CenterCallback, %class.CenterCallback* %this1, i32 0, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx3, %class.btVector3* nonnull align 4 dereferenceable(16) %ref4)
  %5 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 1
  %ref7 = getelementptr inbounds %class.CenterCallback, %class.CenterCallback* %this1, i32 0, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6, %class.btVector3* nonnull align 4 dereferenceable(16) %ref7)
  %6 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 2
  %ref10 = getelementptr inbounds %class.CenterCallback, %class.CenterCallback* %this1, i32 0, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx9, %class.btVector3* nonnull align 4 dereferenceable(16) %ref10)
  %call = call float @_ZNK9btVector36tripleERKS_S1_(%class.btVector3* %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp8)
  %call11 = call float @_Z6btFabsf(float %call)
  store float %call11, float* %vol, align 4
  %7 = load float, float* %vol, align 4
  %mul = fmul float 2.500000e-01, %7
  store float %mul, float* %ref.tmp13, align 4
  %8 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx17 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0
  %9 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx18 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx17, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx18)
  %10 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx19 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx19)
  %ref20 = getelementptr inbounds %class.CenterCallback, %class.CenterCallback* %this1, i32 0, i32 2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %ref20)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp14)
  %sum = getelementptr inbounds %class.CenterCallback, %class.CenterCallback* %this1, i32 0, i32 3
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %sum, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp12)
  %11 = load float, float* %vol, align 4
  %volume = getelementptr inbounds %class.CenterCallback, %class.CenterCallback* %this1, i32 0, i32 4
  %12 = load float, float* %volume, align 4
  %add = fadd float %12, %11
  store float %add, float* %volume, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector36tripleERKS_S1_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %2 = load float, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4
  %mul = fmul float %2, %4
  %5 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %6 = load float, float* %arrayidx7, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 1
  %8 = load float, float* %arrayidx9, align 4
  %mul10 = fmul float %6, %8
  %sub = fsub float %mul, %mul10
  %mul11 = fmul float %0, %sub
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 1
  %9 = load float, float* %arrayidx13, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats14 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [4 x float], [4 x float]* %m_floats14, i32 0, i32 2
  %11 = load float, float* %arrayidx15, align 4
  %12 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats16 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx17 = getelementptr inbounds [4 x float], [4 x float]* %m_floats16, i32 0, i32 0
  %13 = load float, float* %arrayidx17, align 4
  %mul18 = fmul float %11, %13
  %14 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats19 = getelementptr inbounds %class.btVector3, %class.btVector3* %14, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [4 x float], [4 x float]* %m_floats19, i32 0, i32 0
  %15 = load float, float* %arrayidx20, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats21 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx22 = getelementptr inbounds [4 x float], [4 x float]* %m_floats21, i32 0, i32 2
  %17 = load float, float* %arrayidx22, align 4
  %mul23 = fmul float %15, %17
  %sub24 = fsub float %mul18, %mul23
  %mul25 = fmul float %9, %sub24
  %add = fadd float %mul11, %mul25
  %m_floats26 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx27 = getelementptr inbounds [4 x float], [4 x float]* %m_floats26, i32 0, i32 2
  %18 = load float, float* %arrayidx27, align 4
  %19 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats28 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx29 = getelementptr inbounds [4 x float], [4 x float]* %m_floats28, i32 0, i32 0
  %20 = load float, float* %arrayidx29, align 4
  %21 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats30 = getelementptr inbounds %class.btVector3, %class.btVector3* %21, i32 0, i32 0
  %arrayidx31 = getelementptr inbounds [4 x float], [4 x float]* %m_floats30, i32 0, i32 1
  %22 = load float, float* %arrayidx31, align 4
  %mul32 = fmul float %20, %22
  %23 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats33 = getelementptr inbounds %class.btVector3, %class.btVector3* %23, i32 0, i32 0
  %arrayidx34 = getelementptr inbounds [4 x float], [4 x float]* %m_floats33, i32 0, i32 1
  %24 = load float, float* %arrayidx34, align 4
  %25 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats35 = getelementptr inbounds %class.btVector3, %class.btVector3* %25, i32 0, i32 0
  %arrayidx36 = getelementptr inbounds [4 x float], [4 x float]* %m_floats35, i32 0, i32 0
  %26 = load float, float* %arrayidx36, align 4
  %mul37 = fmul float %24, %26
  %sub38 = fsub float %mul32, %mul37
  %mul39 = fmul float %18, %sub38
  %add40 = fadd float %add, %mul39
  ret float %add40
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  %2 = load float, float* %1, align 4
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallbackD0Ev(%class.InertiaCallback* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.InertiaCallback*, align 4
  store %class.InertiaCallback* %this, %class.InertiaCallback** %this.addr, align 4
  %this1 = load %class.InertiaCallback*, %class.InertiaCallback** %this.addr, align 4
  %call = call %class.InertiaCallback* @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallbackD2Ev(%class.InertiaCallback* %this1) #8
  %0 = bitcast %class.InertiaCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define internal void @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallback28internalProcessTriangleIndexEPS2_ii(%class.InertiaCallback* %this, %class.btVector3* %triangle, i32 %partId, i32 %triangleIndex) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.InertiaCallback*, align 4
  %triangle.addr = alloca %class.btVector3*, align 4
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  %i = alloca %class.btMatrix3x3, align 4
  %a = alloca %class.btVector3, align 4
  %b = alloca %class.btVector3, align 4
  %c = alloca %class.btVector3, align 4
  %volNeg = alloca float, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %i00 = alloca float, align 4
  %i11 = alloca float, align 4
  %i22 = alloca float, align 4
  store %class.InertiaCallback* %this, %class.InertiaCallback** %this.addr, align 4
  store %class.btVector3* %triangle, %class.btVector3** %triangle.addr, align 4
  store i32 %partId, i32* %partId.addr, align 4
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4
  %this1 = load %class.InertiaCallback*, %class.InertiaCallback** %this.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %i)
  %0 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0
  %center = getelementptr inbounds %class.InertiaCallback, %class.InertiaCallback* %this1, i32 0, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %a, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  %1 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 1
  %center3 = getelementptr inbounds %class.InertiaCallback, %class.InertiaCallback* %this1, i32 0, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %b, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx2, %class.btVector3* nonnull align 4 dereferenceable(16) %center3)
  %2 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx4 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 2
  %center5 = getelementptr inbounds %class.InertiaCallback, %class.InertiaCallback* %this1, i32 0, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %c, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4, %class.btVector3* nonnull align 4 dereferenceable(16) %center5)
  %call6 = call float @_ZNK9btVector36tripleERKS_S1_(%class.btVector3* %a, %class.btVector3* nonnull align 4 dereferenceable(16) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c)
  %call7 = call float @_Z6btFabsf(float %call6)
  %fneg = fneg float %call7
  %mul = fmul float %fneg, 0x3FC5555560000000
  store float %mul, float* %volNeg, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc72, %entry
  %3 = load i32, i32* %j, align 4
  %cmp = icmp slt i32 %3, 3
  br i1 %cmp, label %for.body, label %for.end74

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %k, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body
  %4 = load i32, i32* %k, align 4
  %5 = load i32, i32* %j, align 4
  %cmp9 = icmp sle i32 %4, %5
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %6 = load float, float* %volNeg, align 4
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %a)
  %7 = load i32, i32* %j, align 4
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 %7
  %8 = load float, float* %arrayidx12, align 4
  %call13 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %a)
  %9 = load i32, i32* %k, align 4
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 %9
  %10 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %8, %10
  %call16 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %b)
  %11 = load i32, i32* %j, align 4
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 %11
  %12 = load float, float* %arrayidx17, align 4
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %b)
  %13 = load i32, i32* %k, align 4
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 %13
  %14 = load float, float* %arrayidx19, align 4
  %mul20 = fmul float %12, %14
  %add = fadd float %mul15, %mul20
  %call21 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %c)
  %15 = load i32, i32* %j, align 4
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 %15
  %16 = load float, float* %arrayidx22, align 4
  %call23 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %c)
  %17 = load i32, i32* %k, align 4
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 %17
  %18 = load float, float* %arrayidx24, align 4
  %mul25 = fmul float %16, %18
  %add26 = fadd float %add, %mul25
  %mul27 = fmul float 0x3FB99999A0000000, %add26
  %call28 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %a)
  %19 = load i32, i32* %j, align 4
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 %19
  %20 = load float, float* %arrayidx29, align 4
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %b)
  %21 = load i32, i32* %k, align 4
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 %21
  %22 = load float, float* %arrayidx31, align 4
  %mul32 = fmul float %20, %22
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %a)
  %23 = load i32, i32* %k, align 4
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 %23
  %24 = load float, float* %arrayidx34, align 4
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %b)
  %25 = load i32, i32* %j, align 4
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 %25
  %26 = load float, float* %arrayidx36, align 4
  %mul37 = fmul float %24, %26
  %add38 = fadd float %mul32, %mul37
  %call39 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %a)
  %27 = load i32, i32* %j, align 4
  %arrayidx40 = getelementptr inbounds float, float* %call39, i32 %27
  %28 = load float, float* %arrayidx40, align 4
  %call41 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %c)
  %29 = load i32, i32* %k, align 4
  %arrayidx42 = getelementptr inbounds float, float* %call41, i32 %29
  %30 = load float, float* %arrayidx42, align 4
  %mul43 = fmul float %28, %30
  %add44 = fadd float %add38, %mul43
  %call45 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %a)
  %31 = load i32, i32* %k, align 4
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 %31
  %32 = load float, float* %arrayidx46, align 4
  %call47 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %c)
  %33 = load i32, i32* %j, align 4
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 %33
  %34 = load float, float* %arrayidx48, align 4
  %mul49 = fmul float %32, %34
  %add50 = fadd float %add44, %mul49
  %call51 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %b)
  %35 = load i32, i32* %j, align 4
  %arrayidx52 = getelementptr inbounds float, float* %call51, i32 %35
  %36 = load float, float* %arrayidx52, align 4
  %call53 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %c)
  %37 = load i32, i32* %k, align 4
  %arrayidx54 = getelementptr inbounds float, float* %call53, i32 %37
  %38 = load float, float* %arrayidx54, align 4
  %mul55 = fmul float %36, %38
  %add56 = fadd float %add50, %mul55
  %call57 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %b)
  %39 = load i32, i32* %k, align 4
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 %39
  %40 = load float, float* %arrayidx58, align 4
  %call59 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %c)
  %41 = load i32, i32* %j, align 4
  %arrayidx60 = getelementptr inbounds float, float* %call59, i32 %41
  %42 = load float, float* %arrayidx60, align 4
  %mul61 = fmul float %40, %42
  %add62 = fadd float %add56, %mul61
  %mul63 = fmul float 0x3FA99999A0000000, %add62
  %add64 = fadd float %mul27, %mul63
  %mul65 = fmul float %6, %add64
  %43 = load i32, i32* %k, align 4
  %call66 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %i, i32 %43)
  %call67 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call66)
  %44 = load i32, i32* %j, align 4
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 %44
  store float %mul65, float* %arrayidx68, align 4
  %45 = load i32, i32* %j, align 4
  %call69 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %i, i32 %45)
  %call70 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call69)
  %46 = load i32, i32* %k, align 4
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 %46
  store float %mul65, float* %arrayidx71, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %47 = load i32, i32* %k, align 4
  %inc = add nsw i32 %47, 1
  store i32 %inc, i32* %k, align 4
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  br label %for.inc72

for.inc72:                                        ; preds = %for.end
  %48 = load i32, i32* %j, align 4
  %inc73 = add nsw i32 %48, 1
  store i32 %inc73, i32* %j, align 4
  br label %for.cond

for.end74:                                        ; preds = %for.cond
  %call75 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %i, i32 0)
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call75)
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 0
  %49 = load float, float* %arrayidx77, align 4
  %fneg78 = fneg float %49
  store float %fneg78, float* %i00, align 4
  %call79 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %i, i32 1)
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call79)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 1
  %50 = load float, float* %arrayidx81, align 4
  %fneg82 = fneg float %50
  store float %fneg82, float* %i11, align 4
  %call83 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %i, i32 2)
  %call84 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call83)
  %arrayidx85 = getelementptr inbounds float, float* %call84, i32 2
  %51 = load float, float* %arrayidx85, align 4
  %fneg86 = fneg float %51
  store float %fneg86, float* %i22, align 4
  %52 = load float, float* %i11, align 4
  %53 = load float, float* %i22, align 4
  %add87 = fadd float %52, %53
  %call88 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %i, i32 0)
  %call89 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call88)
  %arrayidx90 = getelementptr inbounds float, float* %call89, i32 0
  store float %add87, float* %arrayidx90, align 4
  %54 = load float, float* %i22, align 4
  %55 = load float, float* %i00, align 4
  %add91 = fadd float %54, %55
  %call92 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %i, i32 1)
  %call93 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call92)
  %arrayidx94 = getelementptr inbounds float, float* %call93, i32 1
  store float %add91, float* %arrayidx94, align 4
  %56 = load float, float* %i00, align 4
  %57 = load float, float* %i11, align 4
  %add95 = fadd float %56, %57
  %call96 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %i, i32 2)
  %call97 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call96)
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 2
  store float %add95, float* %arrayidx98, align 4
  %call99 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %i, i32 0)
  %sum = getelementptr inbounds %class.InertiaCallback, %class.InertiaCallback* %this1, i32 0, i32 1
  %call100 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %sum, i32 0)
  %call101 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call100, %class.btVector3* nonnull align 4 dereferenceable(16) %call99)
  %call102 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %i, i32 1)
  %sum103 = getelementptr inbounds %class.InertiaCallback, %class.InertiaCallback* %this1, i32 0, i32 1
  %call104 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %sum103, i32 1)
  %call105 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call104, %class.btVector3* nonnull align 4 dereferenceable(16) %call102)
  %call106 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %i, i32 2)
  %sum107 = getelementptr inbounds %class.InertiaCallback, %class.InertiaCallback* %this1, i32 0, i32 1
  %call108 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %sum107, i32 2)
  %call109 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call108, %class.btVector3* nonnull align 4 dereferenceable(16) %call106)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 1.000000e+00, float* %ref.tmp9, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btPolyhedralConvexAabbCachingShape* @_ZN34btPolyhedralConvexAabbCachingShapeD2Ev(%class.btPolyhedralConvexAabbCachingShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btPolyhedralConvexAabbCachingShape*, align 4
  store %class.btPolyhedralConvexAabbCachingShape* %this, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  %this1 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  %0 = bitcast %class.btPolyhedralConvexAabbCachingShape* %this1 to %class.btPolyhedralConvexShape*
  %call = call %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeD2Ev(%class.btPolyhedralConvexShape* %0) #8
  ret %class.btPolyhedralConvexAabbCachingShape* %this1
}

; Function Attrs: nounwind
declare %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeD2Ev(%class.btPolyhedralConvexShape* returned) unnamed_addr #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN25btConvexTriangleMeshShapedlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %2, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btConvexTriangleMeshShape.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
