; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/Gimpact/gim_tri_collision.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/Gimpact/gim_tri_collision.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.GIM_TRIANGLE = type { float, [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%struct.GIM_TRIANGLE_CONTACT_DATA = type { float, i32, %class.btVector4, [16 x %class.btVector3] }
%class.btVector4 = type { %class.btVector3 }
%class.GIM_TRIANGLE_CALCULATION_CACHE = type { float, [3 x %class.btVector3], [3 x %class.btVector3], %class.btVector4, %class.btVector4, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, [4 x float], float, float, [4 x float], float, float, [16 x %class.btVector3], [16 x %class.btVector3], [16 x %class.btVector3] }
%class.DISTANCE_PLANE_3D_FUNC = type { i8 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN30GIM_TRIANGLE_CALCULATION_CACHEC2Ev = comdat any

$_ZN30GIM_TRIANGLE_CALCULATION_CACHE18triangle_collisionERK9btVector3S2_S2_fS2_S2_S2_fR25GIM_TRIANGLE_CONTACT_DATA = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN9btVector4C2Ev = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN30GIM_TRIANGLE_CALCULATION_CACHE13clip_triangleERK9btVector4PK9btVector3S5_PS3_ = comdat any

$_ZN25GIM_TRIANGLE_CONTACT_DATA12merge_pointsERK9btVector4fPK9btVector3j = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_Z21PLANE_CLIP_TRIANGLE3DI9btVector39btVector4EjRKT0_RKT_S7_S7_PS5_ = comdat any

$_Z20PLANE_CLIP_POLYGON3DI9btVector39btVector4EjRKT0_PKT_jPS5_ = comdat any

$_Z27PLANE_CLIP_TRIANGLE_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_RKT_S8_S8_PS6_T1_ = comdat any

$_ZN22DISTANCE_PLANE_3D_FUNCclI9btVector39btVector4EEfRKT0_RKT_ = comdat any

$_Z26PLANE_CLIP_POLYGON_COLLECTI9btVector3EvRKT_S3_ffPS1_Rj = comdat any

$_Z26PLANE_CLIP_POLYGON_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_PKT_jPS6_T1_ = comdat any

$_ZN25GIM_TRIANGLE_CONTACT_DATA19mergepoints_genericI22DISTANCE_PLANE_3D_FUNC9btVector4EEvRKT0_fPK9btVector3jT_ = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_gim_tri_collision.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZNK12GIM_TRIANGLE26collide_triangle_hard_testERKS_R25GIM_TRIANGLE_CONTACT_DATA(%class.GIM_TRIANGLE* %this, %class.GIM_TRIANGLE* nonnull align 4 dereferenceable(52) %other, %struct.GIM_TRIANGLE_CONTACT_DATA* nonnull align 4 dereferenceable(280) %contact_data) #2 {
entry:
  %this.addr = alloca %class.GIM_TRIANGLE*, align 4
  %other.addr = alloca %class.GIM_TRIANGLE*, align 4
  %contact_data.addr = alloca %struct.GIM_TRIANGLE_CONTACT_DATA*, align 4
  %calc_cache = alloca %class.GIM_TRIANGLE_CALCULATION_CACHE, align 4
  store %class.GIM_TRIANGLE* %this, %class.GIM_TRIANGLE** %this.addr, align 4
  store %class.GIM_TRIANGLE* %other, %class.GIM_TRIANGLE** %other.addr, align 4
  store %struct.GIM_TRIANGLE_CONTACT_DATA* %contact_data, %struct.GIM_TRIANGLE_CONTACT_DATA** %contact_data.addr, align 4
  %this1 = load %class.GIM_TRIANGLE*, %class.GIM_TRIANGLE** %this.addr, align 4
  %call = call %class.GIM_TRIANGLE_CALCULATION_CACHE* @_ZN30GIM_TRIANGLE_CALCULATION_CACHEC2Ev(%class.GIM_TRIANGLE_CALCULATION_CACHE* %calc_cache)
  %m_vertices = getelementptr inbounds %class.GIM_TRIANGLE, %class.GIM_TRIANGLE* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices, i32 0, i32 0
  %m_vertices2 = getelementptr inbounds %class.GIM_TRIANGLE, %class.GIM_TRIANGLE* %this1, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices2, i32 0, i32 1
  %m_vertices4 = getelementptr inbounds %class.GIM_TRIANGLE, %class.GIM_TRIANGLE* %this1, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices4, i32 0, i32 2
  %m_margin = getelementptr inbounds %class.GIM_TRIANGLE, %class.GIM_TRIANGLE* %this1, i32 0, i32 0
  %0 = load float, float* %m_margin, align 4
  %1 = load %class.GIM_TRIANGLE*, %class.GIM_TRIANGLE** %other.addr, align 4
  %m_vertices6 = getelementptr inbounds %class.GIM_TRIANGLE, %class.GIM_TRIANGLE* %1, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices6, i32 0, i32 0
  %2 = load %class.GIM_TRIANGLE*, %class.GIM_TRIANGLE** %other.addr, align 4
  %m_vertices8 = getelementptr inbounds %class.GIM_TRIANGLE, %class.GIM_TRIANGLE* %2, i32 0, i32 1
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices8, i32 0, i32 1
  %3 = load %class.GIM_TRIANGLE*, %class.GIM_TRIANGLE** %other.addr, align 4
  %m_vertices10 = getelementptr inbounds %class.GIM_TRIANGLE, %class.GIM_TRIANGLE* %3, i32 0, i32 1
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices10, i32 0, i32 2
  %4 = load %class.GIM_TRIANGLE*, %class.GIM_TRIANGLE** %other.addr, align 4
  %m_margin12 = getelementptr inbounds %class.GIM_TRIANGLE, %class.GIM_TRIANGLE* %4, i32 0, i32 0
  %5 = load float, float* %m_margin12, align 4
  %6 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contact_data.addr, align 4
  %call13 = call zeroext i1 @_ZN30GIM_TRIANGLE_CALCULATION_CACHE18triangle_collisionERK9btVector3S2_S2_fS2_S2_S2_fR25GIM_TRIANGLE_CONTACT_DATA(%class.GIM_TRIANGLE_CALCULATION_CACHE* %calc_cache, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx5, float %0, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx7, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx9, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx11, float %5, %struct.GIM_TRIANGLE_CONTACT_DATA* nonnull align 4 dereferenceable(280) %6)
  ret i1 %call13
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.GIM_TRIANGLE_CALCULATION_CACHE* @_ZN30GIM_TRIANGLE_CALCULATION_CACHEC2Ev(%class.GIM_TRIANGLE_CALCULATION_CACHE* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.GIM_TRIANGLE_CALCULATION_CACHE*, align 4
  %this.addr = alloca %class.GIM_TRIANGLE_CALCULATION_CACHE*, align 4
  store %class.GIM_TRIANGLE_CALCULATION_CACHE* %this, %class.GIM_TRIANGLE_CALCULATION_CACHE** %this.addr, align 4
  %this1 = load %class.GIM_TRIANGLE_CALCULATION_CACHE*, %class.GIM_TRIANGLE_CALCULATION_CACHE** %this.addr, align 4
  store %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, %class.GIM_TRIANGLE_CALCULATION_CACHE** %retval, align 4
  %tu_vertices = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %tv_vertices = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %array.begin2 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices, i32 0, i32 0
  %arrayctor.end3 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin2, i32 3
  br label %arrayctor.loop4

arrayctor.loop4:                                  ; preds = %arrayctor.loop4, %arrayctor.cont
  %arrayctor.cur5 = phi %class.btVector3* [ %array.begin2, %arrayctor.cont ], [ %arrayctor.next7, %arrayctor.loop4 ]
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur5)
  %arrayctor.next7 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur5, i32 1
  %arrayctor.done8 = icmp eq %class.btVector3* %arrayctor.next7, %arrayctor.end3
  br i1 %arrayctor.done8, label %arrayctor.cont9, label %arrayctor.loop4

arrayctor.cont9:                                  ; preds = %arrayctor.loop4
  %tu_plane = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %call10 = call %class.btVector4* @_ZN9btVector4C2Ev(%class.btVector4* %tu_plane)
  %tv_plane = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %call11 = call %class.btVector4* @_ZN9btVector4C2Ev(%class.btVector4* %tv_plane)
  %closest_point_u = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 5
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %closest_point_u)
  %closest_point_v = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 6
  %call13 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %closest_point_v)
  %edge_edge_dir = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 7
  %call14 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %edge_edge_dir)
  %distances = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %distances)
  %temp_points = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 15
  %array.begin16 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %temp_points, i32 0, i32 0
  %arrayctor.end17 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin16, i32 16
  br label %arrayctor.loop18

arrayctor.loop18:                                 ; preds = %arrayctor.loop18, %arrayctor.cont9
  %arrayctor.cur19 = phi %class.btVector3* [ %array.begin16, %arrayctor.cont9 ], [ %arrayctor.next21, %arrayctor.loop18 ]
  %call20 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur19)
  %arrayctor.next21 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur19, i32 1
  %arrayctor.done22 = icmp eq %class.btVector3* %arrayctor.next21, %arrayctor.end17
  br i1 %arrayctor.done22, label %arrayctor.cont23, label %arrayctor.loop18

arrayctor.cont23:                                 ; preds = %arrayctor.loop18
  %temp_points1 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 16
  %array.begin24 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %temp_points1, i32 0, i32 0
  %arrayctor.end25 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin24, i32 16
  br label %arrayctor.loop26

arrayctor.loop26:                                 ; preds = %arrayctor.loop26, %arrayctor.cont23
  %arrayctor.cur27 = phi %class.btVector3* [ %array.begin24, %arrayctor.cont23 ], [ %arrayctor.next29, %arrayctor.loop26 ]
  %call28 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur27)
  %arrayctor.next29 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur27, i32 1
  %arrayctor.done30 = icmp eq %class.btVector3* %arrayctor.next29, %arrayctor.end25
  br i1 %arrayctor.done30, label %arrayctor.cont31, label %arrayctor.loop26

arrayctor.cont31:                                 ; preds = %arrayctor.loop26
  %contact_points = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 17
  %array.begin32 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %contact_points, i32 0, i32 0
  %arrayctor.end33 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin32, i32 16
  br label %arrayctor.loop34

arrayctor.loop34:                                 ; preds = %arrayctor.loop34, %arrayctor.cont31
  %arrayctor.cur35 = phi %class.btVector3* [ %array.begin32, %arrayctor.cont31 ], [ %arrayctor.next37, %arrayctor.loop34 ]
  %call36 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur35)
  %arrayctor.next37 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur35, i32 1
  %arrayctor.done38 = icmp eq %class.btVector3* %arrayctor.next37, %arrayctor.end33
  br i1 %arrayctor.done38, label %arrayctor.cont39, label %arrayctor.loop34

arrayctor.cont39:                                 ; preds = %arrayctor.loop34
  %0 = load %class.GIM_TRIANGLE_CALCULATION_CACHE*, %class.GIM_TRIANGLE_CALCULATION_CACHE** %retval, align 4
  ret %class.GIM_TRIANGLE_CALCULATION_CACHE* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZN30GIM_TRIANGLE_CALCULATION_CACHE18triangle_collisionERK9btVector3S2_S2_fS2_S2_S2_fR25GIM_TRIANGLE_CONTACT_DATA(%class.GIM_TRIANGLE_CALCULATION_CACHE* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %u0, %class.btVector3* nonnull align 4 dereferenceable(16) %u1, %class.btVector3* nonnull align 4 dereferenceable(16) %u2, float %margin_u, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2, float %margin_v, %struct.GIM_TRIANGLE_CONTACT_DATA* nonnull align 4 dereferenceable(280) %contacts) #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.GIM_TRIANGLE_CALCULATION_CACHE*, align 4
  %u0.addr = alloca %class.btVector3*, align 4
  %u1.addr = alloca %class.btVector3*, align 4
  %u2.addr = alloca %class.btVector3*, align 4
  %margin_u.addr = alloca float, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %margin_v.addr = alloca float, align 4
  %contacts.addr = alloca %struct.GIM_TRIANGLE_CONTACT_DATA*, align 4
  %_dif1 = alloca [3 x float], align 4
  %_dif2 = alloca [3 x float], align 4
  %len = alloca float, align 4
  %_pp = alloca float, align 4
  %_x = alloca float, align 4
  %_y = alloca i32, align 4
  %_dif1770 = alloca [3 x float], align 4
  %_dif2771 = alloca [3 x float], align 4
  %len861 = alloca float, align 4
  %_pp862 = alloca float, align 4
  %_x889 = alloca float, align 4
  %_y891 = alloca i32, align 4
  %bl = alloca i32, align 4
  %point_count = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  store %class.GIM_TRIANGLE_CALCULATION_CACHE* %this, %class.GIM_TRIANGLE_CALCULATION_CACHE** %this.addr, align 4
  store %class.btVector3* %u0, %class.btVector3** %u0.addr, align 4
  store %class.btVector3* %u1, %class.btVector3** %u1.addr, align 4
  store %class.btVector3* %u2, %class.btVector3** %u2.addr, align 4
  store float %margin_u, float* %margin_u.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  store float %margin_v, float* %margin_v.addr, align 4
  store %struct.GIM_TRIANGLE_CONTACT_DATA* %contacts, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4
  %this1 = load %class.GIM_TRIANGLE_CALCULATION_CACHE*, %class.GIM_TRIANGLE_CALCULATION_CACHE** %this.addr, align 4
  %0 = load float, float* %margin_u.addr, align 4
  %1 = load float, float* %margin_v.addr, align 4
  %add = fadd float %0, %1
  %margin = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 0
  store float %add, float* %margin, align 4
  %2 = load %class.btVector3*, %class.btVector3** %u0.addr, align 4
  %tu_vertices = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices, i32 0, i32 0
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %5 = load %class.btVector3*, %class.btVector3** %u1.addr, align 4
  %tu_vertices2 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices2, i32 0, i32 1
  %6 = bitcast %class.btVector3* %arrayidx3 to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %8 = load %class.btVector3*, %class.btVector3** %u2.addr, align 4
  %tu_vertices4 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices4, i32 0, i32 2
  %9 = bitcast %class.btVector3* %arrayidx5 to i8*
  %10 = bitcast %class.btVector3* %8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  %11 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %tv_vertices = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices, i32 0, i32 0
  %12 = bitcast %class.btVector3* %arrayidx6 to i8*
  %13 = bitcast %class.btVector3* %11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  %14 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %tv_vertices7 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices7, i32 0, i32 1
  %15 = bitcast %class.btVector3* %arrayidx8 to i8*
  %16 = bitcast %class.btVector3* %14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false)
  %17 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %tv_vertices9 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices9, i32 0, i32 2
  %18 = bitcast %class.btVector3* %arrayidx10 to i8*
  %19 = bitcast %class.btVector3* %17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false)
  %tv_vertices11 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices11, i32 0, i32 1
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx12)
  %arrayidx13 = getelementptr inbounds float, float* %call, i32 0
  %20 = load float, float* %arrayidx13, align 4
  %tv_vertices14 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices14, i32 0, i32 0
  %call16 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx15)
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 0
  %21 = load float, float* %arrayidx17, align 4
  %sub = fsub float %20, %21
  %arrayidx18 = getelementptr inbounds [3 x float], [3 x float]* %_dif1, i32 0, i32 0
  store float %sub, float* %arrayidx18, align 4
  %tv_vertices19 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices19, i32 0, i32 1
  %call21 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx20)
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 1
  %22 = load float, float* %arrayidx22, align 4
  %tv_vertices23 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices23, i32 0, i32 0
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx24)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 1
  %23 = load float, float* %arrayidx26, align 4
  %sub27 = fsub float %22, %23
  %arrayidx28 = getelementptr inbounds [3 x float], [3 x float]* %_dif1, i32 0, i32 1
  store float %sub27, float* %arrayidx28, align 4
  %tv_vertices29 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices29, i32 0, i32 1
  %call31 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx30)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 2
  %24 = load float, float* %arrayidx32, align 4
  %tv_vertices33 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx34 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices33, i32 0, i32 0
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx34)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 2
  %25 = load float, float* %arrayidx36, align 4
  %sub37 = fsub float %24, %25
  %arrayidx38 = getelementptr inbounds [3 x float], [3 x float]* %_dif1, i32 0, i32 2
  store float %sub37, float* %arrayidx38, align 4
  %tv_vertices39 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices39, i32 0, i32 2
  %call41 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx40)
  %arrayidx42 = getelementptr inbounds float, float* %call41, i32 0
  %26 = load float, float* %arrayidx42, align 4
  %tv_vertices43 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx44 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices43, i32 0, i32 0
  %call45 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx44)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 0
  %27 = load float, float* %arrayidx46, align 4
  %sub47 = fsub float %26, %27
  %arrayidx48 = getelementptr inbounds [3 x float], [3 x float]* %_dif2, i32 0, i32 0
  store float %sub47, float* %arrayidx48, align 4
  %tv_vertices49 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx50 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices49, i32 0, i32 2
  %call51 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx50)
  %arrayidx52 = getelementptr inbounds float, float* %call51, i32 1
  %28 = load float, float* %arrayidx52, align 4
  %tv_vertices53 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx54 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices53, i32 0, i32 0
  %call55 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx54)
  %arrayidx56 = getelementptr inbounds float, float* %call55, i32 1
  %29 = load float, float* %arrayidx56, align 4
  %sub57 = fsub float %28, %29
  %arrayidx58 = getelementptr inbounds [3 x float], [3 x float]* %_dif2, i32 0, i32 1
  store float %sub57, float* %arrayidx58, align 4
  %tv_vertices59 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx60 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices59, i32 0, i32 2
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx60)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 2
  %30 = load float, float* %arrayidx62, align 4
  %tv_vertices63 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx64 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices63, i32 0, i32 0
  %call65 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx64)
  %arrayidx66 = getelementptr inbounds float, float* %call65, i32 2
  %31 = load float, float* %arrayidx66, align 4
  %sub67 = fsub float %30, %31
  %arrayidx68 = getelementptr inbounds [3 x float], [3 x float]* %_dif2, i32 0, i32 2
  store float %sub67, float* %arrayidx68, align 4
  %arrayidx69 = getelementptr inbounds [3 x float], [3 x float]* %_dif1, i32 0, i32 1
  %32 = load float, float* %arrayidx69, align 4
  %arrayidx70 = getelementptr inbounds [3 x float], [3 x float]* %_dif2, i32 0, i32 2
  %33 = load float, float* %arrayidx70, align 4
  %mul = fmul float %32, %33
  %arrayidx71 = getelementptr inbounds [3 x float], [3 x float]* %_dif1, i32 0, i32 2
  %34 = load float, float* %arrayidx71, align 4
  %arrayidx72 = getelementptr inbounds [3 x float], [3 x float]* %_dif2, i32 0, i32 1
  %35 = load float, float* %arrayidx72, align 4
  %mul73 = fmul float %34, %35
  %sub74 = fsub float %mul, %mul73
  %tv_plane = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %36 = bitcast %class.btVector4* %tv_plane to %class.btVector3*
  %call75 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %36)
  %arrayidx76 = getelementptr inbounds float, float* %call75, i32 0
  store float %sub74, float* %arrayidx76, align 4
  %arrayidx77 = getelementptr inbounds [3 x float], [3 x float]* %_dif1, i32 0, i32 2
  %37 = load float, float* %arrayidx77, align 4
  %arrayidx78 = getelementptr inbounds [3 x float], [3 x float]* %_dif2, i32 0, i32 0
  %38 = load float, float* %arrayidx78, align 4
  %mul79 = fmul float %37, %38
  %arrayidx80 = getelementptr inbounds [3 x float], [3 x float]* %_dif1, i32 0, i32 0
  %39 = load float, float* %arrayidx80, align 4
  %arrayidx81 = getelementptr inbounds [3 x float], [3 x float]* %_dif2, i32 0, i32 2
  %40 = load float, float* %arrayidx81, align 4
  %mul82 = fmul float %39, %40
  %sub83 = fsub float %mul79, %mul82
  %tv_plane84 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %41 = bitcast %class.btVector4* %tv_plane84 to %class.btVector3*
  %call85 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %41)
  %arrayidx86 = getelementptr inbounds float, float* %call85, i32 1
  store float %sub83, float* %arrayidx86, align 4
  %arrayidx87 = getelementptr inbounds [3 x float], [3 x float]* %_dif1, i32 0, i32 0
  %42 = load float, float* %arrayidx87, align 4
  %arrayidx88 = getelementptr inbounds [3 x float], [3 x float]* %_dif2, i32 0, i32 1
  %43 = load float, float* %arrayidx88, align 4
  %mul89 = fmul float %42, %43
  %arrayidx90 = getelementptr inbounds [3 x float], [3 x float]* %_dif1, i32 0, i32 1
  %44 = load float, float* %arrayidx90, align 4
  %arrayidx91 = getelementptr inbounds [3 x float], [3 x float]* %_dif2, i32 0, i32 0
  %45 = load float, float* %arrayidx91, align 4
  %mul92 = fmul float %44, %45
  %sub93 = fsub float %mul89, %mul92
  %tv_plane94 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %46 = bitcast %class.btVector4* %tv_plane94 to %class.btVector3*
  %call95 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %46)
  %arrayidx96 = getelementptr inbounds float, float* %call95, i32 2
  store float %sub93, float* %arrayidx96, align 4
  %tv_plane97 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %47 = bitcast %class.btVector4* %tv_plane97 to %class.btVector3*
  %call98 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %47)
  %arrayidx99 = getelementptr inbounds float, float* %call98, i32 0
  %48 = load float, float* %arrayidx99, align 4
  %tv_plane100 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %49 = bitcast %class.btVector4* %tv_plane100 to %class.btVector3*
  %call101 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %49)
  %arrayidx102 = getelementptr inbounds float, float* %call101, i32 0
  %50 = load float, float* %arrayidx102, align 4
  %mul103 = fmul float %48, %50
  %tv_plane104 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %51 = bitcast %class.btVector4* %tv_plane104 to %class.btVector3*
  %call105 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %51)
  %arrayidx106 = getelementptr inbounds float, float* %call105, i32 1
  %52 = load float, float* %arrayidx106, align 4
  %tv_plane107 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %53 = bitcast %class.btVector4* %tv_plane107 to %class.btVector3*
  %call108 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %53)
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 1
  %54 = load float, float* %arrayidx109, align 4
  %mul110 = fmul float %52, %54
  %add111 = fadd float %mul103, %mul110
  %tv_plane112 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %55 = bitcast %class.btVector4* %tv_plane112 to %class.btVector3*
  %call113 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %55)
  %arrayidx114 = getelementptr inbounds float, float* %call113, i32 2
  %56 = load float, float* %arrayidx114, align 4
  %tv_plane115 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %57 = bitcast %class.btVector4* %tv_plane115 to %class.btVector3*
  %call116 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %57)
  %arrayidx117 = getelementptr inbounds float, float* %call116, i32 2
  %58 = load float, float* %arrayidx117, align 4
  %mul118 = fmul float %56, %58
  %add119 = fadd float %add111, %mul118
  store float %add119, float* %_pp, align 4
  %59 = load float, float* %_pp, align 4
  %cmp = fcmp ole float %59, 0x3E7AD7F2A0000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store float 0x47EFFFFFE0000000, float* %len, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %60 = load float, float* %_pp, align 4
  %mul120 = fmul float %60, 5.000000e-01
  store float %mul120, float* %_x, align 4
  %61 = bitcast float* %_pp to i32*
  %62 = load i32, i32* %61, align 4
  %shr = lshr i32 %62, 1
  %sub121 = sub i32 1597463007, %shr
  store i32 %sub121, i32* %_y, align 4
  %63 = bitcast i32* %_y to float*
  %64 = load float, float* %63, align 4
  store float %64, float* %len, align 4
  %65 = load float, float* %len, align 4
  %66 = load float, float* %_x, align 4
  %67 = load float, float* %len, align 4
  %mul122 = fmul float %66, %67
  %68 = load float, float* %len, align 4
  %mul123 = fmul float %mul122, %68
  %sub124 = fsub float 1.500000e+00, %mul123
  %mul125 = fmul float %65, %sub124
  store float %mul125, float* %len, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %69 = load float, float* %len, align 4
  %cmp126 = fcmp olt float %69, 0x47EFFFFFE0000000
  br i1 %cmp126, label %if.then127, label %if.end140

if.then127:                                       ; preds = %if.end
  %70 = load float, float* %len, align 4
  %tv_plane128 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %71 = bitcast %class.btVector4* %tv_plane128 to %class.btVector3*
  %call129 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %71)
  %arrayidx130 = getelementptr inbounds float, float* %call129, i32 0
  %72 = load float, float* %arrayidx130, align 4
  %mul131 = fmul float %72, %70
  store float %mul131, float* %arrayidx130, align 4
  %73 = load float, float* %len, align 4
  %tv_plane132 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %74 = bitcast %class.btVector4* %tv_plane132 to %class.btVector3*
  %call133 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %74)
  %arrayidx134 = getelementptr inbounds float, float* %call133, i32 1
  %75 = load float, float* %arrayidx134, align 4
  %mul135 = fmul float %75, %73
  store float %mul135, float* %arrayidx134, align 4
  %76 = load float, float* %len, align 4
  %tv_plane136 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %77 = bitcast %class.btVector4* %tv_plane136 to %class.btVector3*
  %call137 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %77)
  %arrayidx138 = getelementptr inbounds float, float* %call137, i32 2
  %78 = load float, float* %arrayidx138, align 4
  %mul139 = fmul float %78, %76
  store float %mul139, float* %arrayidx138, align 4
  br label %if.end140

if.end140:                                        ; preds = %if.then127, %if.end
  %tv_vertices141 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx142 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices141, i32 0, i32 0
  %call143 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx142)
  %arrayidx144 = getelementptr inbounds float, float* %call143, i32 0
  %79 = load float, float* %arrayidx144, align 4
  %tv_plane145 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %80 = bitcast %class.btVector4* %tv_plane145 to %class.btVector3*
  %call146 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %80)
  %arrayidx147 = getelementptr inbounds float, float* %call146, i32 0
  %81 = load float, float* %arrayidx147, align 4
  %mul148 = fmul float %79, %81
  %tv_vertices149 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx150 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices149, i32 0, i32 0
  %call151 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx150)
  %arrayidx152 = getelementptr inbounds float, float* %call151, i32 1
  %82 = load float, float* %arrayidx152, align 4
  %tv_plane153 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %83 = bitcast %class.btVector4* %tv_plane153 to %class.btVector3*
  %call154 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %83)
  %arrayidx155 = getelementptr inbounds float, float* %call154, i32 1
  %84 = load float, float* %arrayidx155, align 4
  %mul156 = fmul float %82, %84
  %add157 = fadd float %mul148, %mul156
  %tv_vertices158 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx159 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices158, i32 0, i32 0
  %call160 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx159)
  %arrayidx161 = getelementptr inbounds float, float* %call160, i32 2
  %85 = load float, float* %arrayidx161, align 4
  %tv_plane162 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %86 = bitcast %class.btVector4* %tv_plane162 to %class.btVector3*
  %call163 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %86)
  %arrayidx164 = getelementptr inbounds float, float* %call163, i32 2
  %87 = load float, float* %arrayidx164, align 4
  %mul165 = fmul float %85, %87
  %add166 = fadd float %add157, %mul165
  %tv_plane167 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %88 = bitcast %class.btVector4* %tv_plane167 to %class.btVector3*
  %call168 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %88)
  %arrayidx169 = getelementptr inbounds float, float* %call168, i32 3
  store float %add166, float* %arrayidx169, align 4
  %tv_plane170 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %89 = bitcast %class.btVector4* %tv_plane170 to %class.btVector3*
  %call171 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %89)
  %arrayidx172 = getelementptr inbounds float, float* %call171, i32 0
  %90 = load float, float* %arrayidx172, align 4
  %tu_vertices173 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx174 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices173, i32 0, i32 0
  %call175 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx174)
  %arrayidx176 = getelementptr inbounds float, float* %call175, i32 0
  %91 = load float, float* %arrayidx176, align 4
  %mul177 = fmul float %90, %91
  %tv_plane178 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %92 = bitcast %class.btVector4* %tv_plane178 to %class.btVector3*
  %call179 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %92)
  %arrayidx180 = getelementptr inbounds float, float* %call179, i32 1
  %93 = load float, float* %arrayidx180, align 4
  %tu_vertices181 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx182 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices181, i32 0, i32 0
  %call183 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx182)
  %arrayidx184 = getelementptr inbounds float, float* %call183, i32 1
  %94 = load float, float* %arrayidx184, align 4
  %mul185 = fmul float %93, %94
  %add186 = fadd float %mul177, %mul185
  %tv_plane187 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %95 = bitcast %class.btVector4* %tv_plane187 to %class.btVector3*
  %call188 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %95)
  %arrayidx189 = getelementptr inbounds float, float* %call188, i32 2
  %96 = load float, float* %arrayidx189, align 4
  %tu_vertices190 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx191 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices190, i32 0, i32 0
  %call192 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx191)
  %arrayidx193 = getelementptr inbounds float, float* %call192, i32 2
  %97 = load float, float* %arrayidx193, align 4
  %mul194 = fmul float %96, %97
  %add195 = fadd float %add186, %mul194
  %tv_plane196 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %98 = bitcast %class.btVector4* %tv_plane196 to %class.btVector3*
  %call197 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %98)
  %arrayidx198 = getelementptr inbounds float, float* %call197, i32 3
  %99 = load float, float* %arrayidx198, align 4
  %sub199 = fsub float %add195, %99
  %du = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx200 = getelementptr inbounds [4 x float], [4 x float]* %du, i32 0, i32 0
  store float %sub199, float* %arrayidx200, align 4
  %tv_plane201 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %100 = bitcast %class.btVector4* %tv_plane201 to %class.btVector3*
  %call202 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %100)
  %arrayidx203 = getelementptr inbounds float, float* %call202, i32 0
  %101 = load float, float* %arrayidx203, align 4
  %tu_vertices204 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx205 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices204, i32 0, i32 1
  %call206 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx205)
  %arrayidx207 = getelementptr inbounds float, float* %call206, i32 0
  %102 = load float, float* %arrayidx207, align 4
  %mul208 = fmul float %101, %102
  %tv_plane209 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %103 = bitcast %class.btVector4* %tv_plane209 to %class.btVector3*
  %call210 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %103)
  %arrayidx211 = getelementptr inbounds float, float* %call210, i32 1
  %104 = load float, float* %arrayidx211, align 4
  %tu_vertices212 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx213 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices212, i32 0, i32 1
  %call214 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx213)
  %arrayidx215 = getelementptr inbounds float, float* %call214, i32 1
  %105 = load float, float* %arrayidx215, align 4
  %mul216 = fmul float %104, %105
  %add217 = fadd float %mul208, %mul216
  %tv_plane218 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %106 = bitcast %class.btVector4* %tv_plane218 to %class.btVector3*
  %call219 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %106)
  %arrayidx220 = getelementptr inbounds float, float* %call219, i32 2
  %107 = load float, float* %arrayidx220, align 4
  %tu_vertices221 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx222 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices221, i32 0, i32 1
  %call223 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx222)
  %arrayidx224 = getelementptr inbounds float, float* %call223, i32 2
  %108 = load float, float* %arrayidx224, align 4
  %mul225 = fmul float %107, %108
  %add226 = fadd float %add217, %mul225
  %tv_plane227 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %109 = bitcast %class.btVector4* %tv_plane227 to %class.btVector3*
  %call228 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %109)
  %arrayidx229 = getelementptr inbounds float, float* %call228, i32 3
  %110 = load float, float* %arrayidx229, align 4
  %sub230 = fsub float %add226, %110
  %du231 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx232 = getelementptr inbounds [4 x float], [4 x float]* %du231, i32 0, i32 1
  store float %sub230, float* %arrayidx232, align 4
  %tv_plane233 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %111 = bitcast %class.btVector4* %tv_plane233 to %class.btVector3*
  %call234 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %111)
  %arrayidx235 = getelementptr inbounds float, float* %call234, i32 0
  %112 = load float, float* %arrayidx235, align 4
  %tu_vertices236 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx237 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices236, i32 0, i32 2
  %call238 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx237)
  %arrayidx239 = getelementptr inbounds float, float* %call238, i32 0
  %113 = load float, float* %arrayidx239, align 4
  %mul240 = fmul float %112, %113
  %tv_plane241 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %114 = bitcast %class.btVector4* %tv_plane241 to %class.btVector3*
  %call242 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %114)
  %arrayidx243 = getelementptr inbounds float, float* %call242, i32 1
  %115 = load float, float* %arrayidx243, align 4
  %tu_vertices244 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx245 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices244, i32 0, i32 2
  %call246 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx245)
  %arrayidx247 = getelementptr inbounds float, float* %call246, i32 1
  %116 = load float, float* %arrayidx247, align 4
  %mul248 = fmul float %115, %116
  %add249 = fadd float %mul240, %mul248
  %tv_plane250 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %117 = bitcast %class.btVector4* %tv_plane250 to %class.btVector3*
  %call251 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %117)
  %arrayidx252 = getelementptr inbounds float, float* %call251, i32 2
  %118 = load float, float* %arrayidx252, align 4
  %tu_vertices253 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx254 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices253, i32 0, i32 2
  %call255 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx254)
  %arrayidx256 = getelementptr inbounds float, float* %call255, i32 2
  %119 = load float, float* %arrayidx256, align 4
  %mul257 = fmul float %118, %119
  %add258 = fadd float %add249, %mul257
  %tv_plane259 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %120 = bitcast %class.btVector4* %tv_plane259 to %class.btVector3*
  %call260 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %120)
  %arrayidx261 = getelementptr inbounds float, float* %call260, i32 3
  %121 = load float, float* %arrayidx261, align 4
  %sub262 = fsub float %add258, %121
  %du263 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx264 = getelementptr inbounds [4 x float], [4 x float]* %du263, i32 0, i32 2
  store float %sub262, float* %arrayidx264, align 4
  %du265 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx266 = getelementptr inbounds [4 x float], [4 x float]* %du265, i32 0, i32 0
  %122 = load float, float* %arrayidx266, align 4
  %du267 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx268 = getelementptr inbounds [4 x float], [4 x float]* %du267, i32 0, i32 1
  %123 = load float, float* %arrayidx268, align 4
  %mul269 = fmul float %122, %123
  %du0du1 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 10
  store float %mul269, float* %du0du1, align 4
  %du270 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx271 = getelementptr inbounds [4 x float], [4 x float]* %du270, i32 0, i32 0
  %124 = load float, float* %arrayidx271, align 4
  %du272 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx273 = getelementptr inbounds [4 x float], [4 x float]* %du272, i32 0, i32 2
  %125 = load float, float* %arrayidx273, align 4
  %mul274 = fmul float %124, %125
  %du0du2 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 11
  store float %mul274, float* %du0du2, align 4
  %du0du1275 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 10
  %126 = load float, float* %du0du1275, align 4
  %cmp276 = fcmp ogt float %126, 0.000000e+00
  br i1 %cmp276, label %land.lhs.true, label %if.else522

land.lhs.true:                                    ; preds = %if.end140
  %du0du2277 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 11
  %127 = load float, float* %du0du2277, align 4
  %cmp278 = fcmp ogt float %127, 0.000000e+00
  br i1 %cmp278, label %if.then279, label %if.else522

if.then279:                                       ; preds = %land.lhs.true
  %du280 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx281 = getelementptr inbounds [4 x float], [4 x float]* %du280, i32 0, i32 0
  %128 = load float, float* %arrayidx281, align 4
  %cmp282 = fcmp olt float %128, 0.000000e+00
  br i1 %cmp282, label %if.then283, label %if.else475

if.then283:                                       ; preds = %if.then279
  %du284 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx285 = getelementptr inbounds [4 x float], [4 x float]* %du284, i32 0, i32 0
  %129 = load float, float* %arrayidx285, align 4
  %du286 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx287 = getelementptr inbounds [4 x float], [4 x float]* %du286, i32 0, i32 1
  %130 = load float, float* %arrayidx287, align 4
  %du288 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx289 = getelementptr inbounds [4 x float], [4 x float]* %du288, i32 0, i32 2
  %131 = load float, float* %arrayidx289, align 4
  %cmp290 = fcmp olt float %130, %131
  br i1 %cmp290, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then283
  %du291 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx292 = getelementptr inbounds [4 x float], [4 x float]* %du291, i32 0, i32 2
  %132 = load float, float* %arrayidx292, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.then283
  %du293 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx294 = getelementptr inbounds [4 x float], [4 x float]* %du293, i32 0, i32 1
  %133 = load float, float* %arrayidx294, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %132, %cond.true ], [ %133, %cond.false ]
  %cmp295 = fcmp olt float %129, %cond
  br i1 %cmp295, label %cond.true296, label %cond.false310

cond.true296:                                     ; preds = %cond.end
  %du297 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx298 = getelementptr inbounds [4 x float], [4 x float]* %du297, i32 0, i32 1
  %134 = load float, float* %arrayidx298, align 4
  %du299 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx300 = getelementptr inbounds [4 x float], [4 x float]* %du299, i32 0, i32 2
  %135 = load float, float* %arrayidx300, align 4
  %cmp301 = fcmp olt float %134, %135
  br i1 %cmp301, label %cond.true302, label %cond.false305

cond.true302:                                     ; preds = %cond.true296
  %du303 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx304 = getelementptr inbounds [4 x float], [4 x float]* %du303, i32 0, i32 2
  %136 = load float, float* %arrayidx304, align 4
  br label %cond.end308

cond.false305:                                    ; preds = %cond.true296
  %du306 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx307 = getelementptr inbounds [4 x float], [4 x float]* %du306, i32 0, i32 1
  %137 = load float, float* %arrayidx307, align 4
  br label %cond.end308

cond.end308:                                      ; preds = %cond.false305, %cond.true302
  %cond309 = phi float [ %136, %cond.true302 ], [ %137, %cond.false305 ]
  br label %cond.end313

cond.false310:                                    ; preds = %cond.end
  %du311 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx312 = getelementptr inbounds [4 x float], [4 x float]* %du311, i32 0, i32 0
  %138 = load float, float* %arrayidx312, align 4
  br label %cond.end313

cond.end313:                                      ; preds = %cond.false310, %cond.end308
  %cond314 = phi float [ %cond309, %cond.end308 ], [ %138, %cond.false310 ]
  %distances = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call315 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances)
  %arrayidx316 = getelementptr inbounds float, float* %call315, i32 0
  store float %cond314, float* %arrayidx316, align 4
  %distances317 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call318 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances317)
  %arrayidx319 = getelementptr inbounds float, float* %call318, i32 0
  %139 = load float, float* %arrayidx319, align 4
  %fneg = fneg float %139
  %distances320 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call321 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances320)
  %arrayidx322 = getelementptr inbounds float, float* %call321, i32 0
  store float %fneg, float* %arrayidx322, align 4
  %distances323 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call324 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances323)
  %arrayidx325 = getelementptr inbounds float, float* %call324, i32 0
  %140 = load float, float* %arrayidx325, align 4
  %margin326 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 0
  %141 = load float, float* %margin326, align 4
  %cmp327 = fcmp ogt float %140, %141
  br i1 %cmp327, label %if.then328, label %if.end329

if.then328:                                       ; preds = %cond.end313
  store i1 false, i1* %retval, align 1
  br label %return

if.end329:                                        ; preds = %cond.end313
  %tv_vertices330 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx331 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices330, i32 0, i32 0
  %call332 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx331)
  %arrayidx333 = getelementptr inbounds float, float* %call332, i32 0
  %142 = load float, float* %arrayidx333, align 4
  %tv_vertices334 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx335 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices334, i32 0, i32 1
  %call336 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx335)
  %arrayidx337 = getelementptr inbounds float, float* %call336, i32 0
  %143 = load float, float* %arrayidx337, align 4
  %add338 = fadd float %142, %143
  %tv_vertices339 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx340 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices339, i32 0, i32 0
  %call341 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx340)
  %arrayidx342 = getelementptr inbounds float, float* %call341, i32 0
  store float %add338, float* %arrayidx342, align 4
  %tv_vertices343 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx344 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices343, i32 0, i32 0
  %call345 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx344)
  %arrayidx346 = getelementptr inbounds float, float* %call345, i32 0
  %144 = load float, float* %arrayidx346, align 4
  %tv_vertices347 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx348 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices347, i32 0, i32 1
  %call349 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx348)
  %arrayidx350 = getelementptr inbounds float, float* %call349, i32 0
  %145 = load float, float* %arrayidx350, align 4
  %sub351 = fsub float %144, %145
  %tv_vertices352 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx353 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices352, i32 0, i32 1
  %call354 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx353)
  %arrayidx355 = getelementptr inbounds float, float* %call354, i32 0
  store float %sub351, float* %arrayidx355, align 4
  %tv_vertices356 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx357 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices356, i32 0, i32 0
  %call358 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx357)
  %arrayidx359 = getelementptr inbounds float, float* %call358, i32 0
  %146 = load float, float* %arrayidx359, align 4
  %tv_vertices360 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx361 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices360, i32 0, i32 1
  %call362 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx361)
  %arrayidx363 = getelementptr inbounds float, float* %call362, i32 0
  %147 = load float, float* %arrayidx363, align 4
  %sub364 = fsub float %146, %147
  %tv_vertices365 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx366 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices365, i32 0, i32 0
  %call367 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx366)
  %arrayidx368 = getelementptr inbounds float, float* %call367, i32 0
  store float %sub364, float* %arrayidx368, align 4
  %tv_vertices369 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx370 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices369, i32 0, i32 0
  %call371 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx370)
  %arrayidx372 = getelementptr inbounds float, float* %call371, i32 1
  %148 = load float, float* %arrayidx372, align 4
  %tv_vertices373 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx374 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices373, i32 0, i32 1
  %call375 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx374)
  %arrayidx376 = getelementptr inbounds float, float* %call375, i32 1
  %149 = load float, float* %arrayidx376, align 4
  %add377 = fadd float %148, %149
  %tv_vertices378 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx379 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices378, i32 0, i32 0
  %call380 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx379)
  %arrayidx381 = getelementptr inbounds float, float* %call380, i32 1
  store float %add377, float* %arrayidx381, align 4
  %tv_vertices382 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx383 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices382, i32 0, i32 0
  %call384 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx383)
  %arrayidx385 = getelementptr inbounds float, float* %call384, i32 1
  %150 = load float, float* %arrayidx385, align 4
  %tv_vertices386 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx387 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices386, i32 0, i32 1
  %call388 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx387)
  %arrayidx389 = getelementptr inbounds float, float* %call388, i32 1
  %151 = load float, float* %arrayidx389, align 4
  %sub390 = fsub float %150, %151
  %tv_vertices391 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx392 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices391, i32 0, i32 1
  %call393 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx392)
  %arrayidx394 = getelementptr inbounds float, float* %call393, i32 1
  store float %sub390, float* %arrayidx394, align 4
  %tv_vertices395 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx396 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices395, i32 0, i32 0
  %call397 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx396)
  %arrayidx398 = getelementptr inbounds float, float* %call397, i32 1
  %152 = load float, float* %arrayidx398, align 4
  %tv_vertices399 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx400 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices399, i32 0, i32 1
  %call401 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx400)
  %arrayidx402 = getelementptr inbounds float, float* %call401, i32 1
  %153 = load float, float* %arrayidx402, align 4
  %sub403 = fsub float %152, %153
  %tv_vertices404 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx405 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices404, i32 0, i32 0
  %call406 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx405)
  %arrayidx407 = getelementptr inbounds float, float* %call406, i32 1
  store float %sub403, float* %arrayidx407, align 4
  %tv_vertices408 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx409 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices408, i32 0, i32 0
  %call410 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx409)
  %arrayidx411 = getelementptr inbounds float, float* %call410, i32 2
  %154 = load float, float* %arrayidx411, align 4
  %tv_vertices412 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx413 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices412, i32 0, i32 1
  %call414 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx413)
  %arrayidx415 = getelementptr inbounds float, float* %call414, i32 2
  %155 = load float, float* %arrayidx415, align 4
  %add416 = fadd float %154, %155
  %tv_vertices417 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx418 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices417, i32 0, i32 0
  %call419 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx418)
  %arrayidx420 = getelementptr inbounds float, float* %call419, i32 2
  store float %add416, float* %arrayidx420, align 4
  %tv_vertices421 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx422 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices421, i32 0, i32 0
  %call423 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx422)
  %arrayidx424 = getelementptr inbounds float, float* %call423, i32 2
  %156 = load float, float* %arrayidx424, align 4
  %tv_vertices425 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx426 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices425, i32 0, i32 1
  %call427 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx426)
  %arrayidx428 = getelementptr inbounds float, float* %call427, i32 2
  %157 = load float, float* %arrayidx428, align 4
  %sub429 = fsub float %156, %157
  %tv_vertices430 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx431 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices430, i32 0, i32 1
  %call432 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx431)
  %arrayidx433 = getelementptr inbounds float, float* %call432, i32 2
  store float %sub429, float* %arrayidx433, align 4
  %tv_vertices434 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx435 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices434, i32 0, i32 0
  %call436 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx435)
  %arrayidx437 = getelementptr inbounds float, float* %call436, i32 2
  %158 = load float, float* %arrayidx437, align 4
  %tv_vertices438 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx439 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices438, i32 0, i32 1
  %call440 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx439)
  %arrayidx441 = getelementptr inbounds float, float* %call440, i32 2
  %159 = load float, float* %arrayidx441, align 4
  %sub442 = fsub float %158, %159
  %tv_vertices443 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx444 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices443, i32 0, i32 0
  %call445 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx444)
  %arrayidx446 = getelementptr inbounds float, float* %call445, i32 2
  store float %sub442, float* %arrayidx446, align 4
  %tv_plane447 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %160 = bitcast %class.btVector4* %tv_plane447 to %class.btVector3*
  %call448 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %160)
  %arrayidx449 = getelementptr inbounds float, float* %call448, i32 0
  %161 = load float, float* %arrayidx449, align 4
  %mul450 = fmul float -1.000000e+00, %161
  %tv_plane451 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %162 = bitcast %class.btVector4* %tv_plane451 to %class.btVector3*
  %call452 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %162)
  %arrayidx453 = getelementptr inbounds float, float* %call452, i32 0
  store float %mul450, float* %arrayidx453, align 4
  %tv_plane454 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %163 = bitcast %class.btVector4* %tv_plane454 to %class.btVector3*
  %call455 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %163)
  %arrayidx456 = getelementptr inbounds float, float* %call455, i32 1
  %164 = load float, float* %arrayidx456, align 4
  %mul457 = fmul float -1.000000e+00, %164
  %tv_plane458 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %165 = bitcast %class.btVector4* %tv_plane458 to %class.btVector3*
  %call459 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %165)
  %arrayidx460 = getelementptr inbounds float, float* %call459, i32 1
  store float %mul457, float* %arrayidx460, align 4
  %tv_plane461 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %166 = bitcast %class.btVector4* %tv_plane461 to %class.btVector3*
  %call462 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %166)
  %arrayidx463 = getelementptr inbounds float, float* %call462, i32 2
  %167 = load float, float* %arrayidx463, align 4
  %mul464 = fmul float -1.000000e+00, %167
  %tv_plane465 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %168 = bitcast %class.btVector4* %tv_plane465 to %class.btVector3*
  %call466 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %168)
  %arrayidx467 = getelementptr inbounds float, float* %call466, i32 2
  store float %mul464, float* %arrayidx467, align 4
  %tv_plane468 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %169 = bitcast %class.btVector4* %tv_plane468 to %class.btVector3*
  %call469 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %169)
  %arrayidx470 = getelementptr inbounds float, float* %call469, i32 3
  %170 = load float, float* %arrayidx470, align 4
  %mul471 = fmul float -1.000000e+00, %170
  %tv_plane472 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %171 = bitcast %class.btVector4* %tv_plane472 to %class.btVector3*
  %call473 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %171)
  %arrayidx474 = getelementptr inbounds float, float* %call473, i32 3
  store float %mul471, float* %arrayidx474, align 4
  br label %if.end521

if.else475:                                       ; preds = %if.then279
  %du476 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx477 = getelementptr inbounds [4 x float], [4 x float]* %du476, i32 0, i32 0
  %172 = load float, float* %arrayidx477, align 4
  %du478 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx479 = getelementptr inbounds [4 x float], [4 x float]* %du478, i32 0, i32 1
  %173 = load float, float* %arrayidx479, align 4
  %du480 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx481 = getelementptr inbounds [4 x float], [4 x float]* %du480, i32 0, i32 2
  %174 = load float, float* %arrayidx481, align 4
  %cmp482 = fcmp ogt float %173, %174
  br i1 %cmp482, label %cond.true483, label %cond.false486

cond.true483:                                     ; preds = %if.else475
  %du484 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx485 = getelementptr inbounds [4 x float], [4 x float]* %du484, i32 0, i32 2
  %175 = load float, float* %arrayidx485, align 4
  br label %cond.end489

cond.false486:                                    ; preds = %if.else475
  %du487 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx488 = getelementptr inbounds [4 x float], [4 x float]* %du487, i32 0, i32 1
  %176 = load float, float* %arrayidx488, align 4
  br label %cond.end489

cond.end489:                                      ; preds = %cond.false486, %cond.true483
  %cond490 = phi float [ %175, %cond.true483 ], [ %176, %cond.false486 ]
  %cmp491 = fcmp ogt float %172, %cond490
  br i1 %cmp491, label %cond.true492, label %cond.false506

cond.true492:                                     ; preds = %cond.end489
  %du493 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx494 = getelementptr inbounds [4 x float], [4 x float]* %du493, i32 0, i32 1
  %177 = load float, float* %arrayidx494, align 4
  %du495 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx496 = getelementptr inbounds [4 x float], [4 x float]* %du495, i32 0, i32 2
  %178 = load float, float* %arrayidx496, align 4
  %cmp497 = fcmp ogt float %177, %178
  br i1 %cmp497, label %cond.true498, label %cond.false501

cond.true498:                                     ; preds = %cond.true492
  %du499 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx500 = getelementptr inbounds [4 x float], [4 x float]* %du499, i32 0, i32 2
  %179 = load float, float* %arrayidx500, align 4
  br label %cond.end504

cond.false501:                                    ; preds = %cond.true492
  %du502 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx503 = getelementptr inbounds [4 x float], [4 x float]* %du502, i32 0, i32 1
  %180 = load float, float* %arrayidx503, align 4
  br label %cond.end504

cond.end504:                                      ; preds = %cond.false501, %cond.true498
  %cond505 = phi float [ %179, %cond.true498 ], [ %180, %cond.false501 ]
  br label %cond.end509

cond.false506:                                    ; preds = %cond.end489
  %du507 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx508 = getelementptr inbounds [4 x float], [4 x float]* %du507, i32 0, i32 0
  %181 = load float, float* %arrayidx508, align 4
  br label %cond.end509

cond.end509:                                      ; preds = %cond.false506, %cond.end504
  %cond510 = phi float [ %cond505, %cond.end504 ], [ %181, %cond.false506 ]
  %distances511 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call512 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances511)
  %arrayidx513 = getelementptr inbounds float, float* %call512, i32 0
  store float %cond510, float* %arrayidx513, align 4
  %distances514 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call515 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances514)
  %arrayidx516 = getelementptr inbounds float, float* %call515, i32 0
  %182 = load float, float* %arrayidx516, align 4
  %margin517 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 0
  %183 = load float, float* %margin517, align 4
  %cmp518 = fcmp ogt float %182, %183
  br i1 %cmp518, label %if.then519, label %if.end520

if.then519:                                       ; preds = %cond.end509
  store i1 false, i1* %retval, align 1
  br label %return

if.end520:                                        ; preds = %cond.end509
  br label %if.end521

if.end521:                                        ; preds = %if.end520, %if.end329
  br label %if.end769

if.else522:                                       ; preds = %land.lhs.true, %if.end140
  %du523 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx524 = getelementptr inbounds [4 x float], [4 x float]* %du523, i32 0, i32 0
  %184 = load float, float* %arrayidx524, align 4
  %du525 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx526 = getelementptr inbounds [4 x float], [4 x float]* %du525, i32 0, i32 1
  %185 = load float, float* %arrayidx526, align 4
  %add527 = fadd float %184, %185
  %du528 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx529 = getelementptr inbounds [4 x float], [4 x float]* %du528, i32 0, i32 2
  %186 = load float, float* %arrayidx529, align 4
  %add530 = fadd float %add527, %186
  %div = fdiv float %add530, 3.000000e+00
  %distances531 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call532 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances531)
  %arrayidx533 = getelementptr inbounds float, float* %call532, i32 0
  store float %div, float* %arrayidx533, align 4
  %distances534 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call535 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances534)
  %arrayidx536 = getelementptr inbounds float, float* %call535, i32 0
  %187 = load float, float* %arrayidx536, align 4
  %cmp537 = fcmp olt float %187, 0.000000e+00
  br i1 %cmp537, label %if.then538, label %if.else729

if.then538:                                       ; preds = %if.else522
  %tv_vertices539 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx540 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices539, i32 0, i32 0
  %call541 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx540)
  %arrayidx542 = getelementptr inbounds float, float* %call541, i32 0
  %188 = load float, float* %arrayidx542, align 4
  %tv_vertices543 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx544 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices543, i32 0, i32 1
  %call545 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx544)
  %arrayidx546 = getelementptr inbounds float, float* %call545, i32 0
  %189 = load float, float* %arrayidx546, align 4
  %add547 = fadd float %188, %189
  %tv_vertices548 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx549 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices548, i32 0, i32 0
  %call550 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx549)
  %arrayidx551 = getelementptr inbounds float, float* %call550, i32 0
  store float %add547, float* %arrayidx551, align 4
  %tv_vertices552 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx553 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices552, i32 0, i32 0
  %call554 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx553)
  %arrayidx555 = getelementptr inbounds float, float* %call554, i32 0
  %190 = load float, float* %arrayidx555, align 4
  %tv_vertices556 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx557 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices556, i32 0, i32 1
  %call558 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx557)
  %arrayidx559 = getelementptr inbounds float, float* %call558, i32 0
  %191 = load float, float* %arrayidx559, align 4
  %sub560 = fsub float %190, %191
  %tv_vertices561 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx562 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices561, i32 0, i32 1
  %call563 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx562)
  %arrayidx564 = getelementptr inbounds float, float* %call563, i32 0
  store float %sub560, float* %arrayidx564, align 4
  %tv_vertices565 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx566 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices565, i32 0, i32 0
  %call567 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx566)
  %arrayidx568 = getelementptr inbounds float, float* %call567, i32 0
  %192 = load float, float* %arrayidx568, align 4
  %tv_vertices569 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx570 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices569, i32 0, i32 1
  %call571 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx570)
  %arrayidx572 = getelementptr inbounds float, float* %call571, i32 0
  %193 = load float, float* %arrayidx572, align 4
  %sub573 = fsub float %192, %193
  %tv_vertices574 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx575 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices574, i32 0, i32 0
  %call576 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx575)
  %arrayidx577 = getelementptr inbounds float, float* %call576, i32 0
  store float %sub573, float* %arrayidx577, align 4
  %tv_vertices578 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx579 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices578, i32 0, i32 0
  %call580 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx579)
  %arrayidx581 = getelementptr inbounds float, float* %call580, i32 1
  %194 = load float, float* %arrayidx581, align 4
  %tv_vertices582 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx583 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices582, i32 0, i32 1
  %call584 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx583)
  %arrayidx585 = getelementptr inbounds float, float* %call584, i32 1
  %195 = load float, float* %arrayidx585, align 4
  %add586 = fadd float %194, %195
  %tv_vertices587 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx588 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices587, i32 0, i32 0
  %call589 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx588)
  %arrayidx590 = getelementptr inbounds float, float* %call589, i32 1
  store float %add586, float* %arrayidx590, align 4
  %tv_vertices591 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx592 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices591, i32 0, i32 0
  %call593 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx592)
  %arrayidx594 = getelementptr inbounds float, float* %call593, i32 1
  %196 = load float, float* %arrayidx594, align 4
  %tv_vertices595 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx596 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices595, i32 0, i32 1
  %call597 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx596)
  %arrayidx598 = getelementptr inbounds float, float* %call597, i32 1
  %197 = load float, float* %arrayidx598, align 4
  %sub599 = fsub float %196, %197
  %tv_vertices600 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx601 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices600, i32 0, i32 1
  %call602 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx601)
  %arrayidx603 = getelementptr inbounds float, float* %call602, i32 1
  store float %sub599, float* %arrayidx603, align 4
  %tv_vertices604 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx605 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices604, i32 0, i32 0
  %call606 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx605)
  %arrayidx607 = getelementptr inbounds float, float* %call606, i32 1
  %198 = load float, float* %arrayidx607, align 4
  %tv_vertices608 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx609 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices608, i32 0, i32 1
  %call610 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx609)
  %arrayidx611 = getelementptr inbounds float, float* %call610, i32 1
  %199 = load float, float* %arrayidx611, align 4
  %sub612 = fsub float %198, %199
  %tv_vertices613 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx614 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices613, i32 0, i32 0
  %call615 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx614)
  %arrayidx616 = getelementptr inbounds float, float* %call615, i32 1
  store float %sub612, float* %arrayidx616, align 4
  %tv_vertices617 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx618 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices617, i32 0, i32 0
  %call619 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx618)
  %arrayidx620 = getelementptr inbounds float, float* %call619, i32 2
  %200 = load float, float* %arrayidx620, align 4
  %tv_vertices621 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx622 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices621, i32 0, i32 1
  %call623 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx622)
  %arrayidx624 = getelementptr inbounds float, float* %call623, i32 2
  %201 = load float, float* %arrayidx624, align 4
  %add625 = fadd float %200, %201
  %tv_vertices626 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx627 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices626, i32 0, i32 0
  %call628 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx627)
  %arrayidx629 = getelementptr inbounds float, float* %call628, i32 2
  store float %add625, float* %arrayidx629, align 4
  %tv_vertices630 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx631 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices630, i32 0, i32 0
  %call632 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx631)
  %arrayidx633 = getelementptr inbounds float, float* %call632, i32 2
  %202 = load float, float* %arrayidx633, align 4
  %tv_vertices634 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx635 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices634, i32 0, i32 1
  %call636 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx635)
  %arrayidx637 = getelementptr inbounds float, float* %call636, i32 2
  %203 = load float, float* %arrayidx637, align 4
  %sub638 = fsub float %202, %203
  %tv_vertices639 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx640 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices639, i32 0, i32 1
  %call641 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx640)
  %arrayidx642 = getelementptr inbounds float, float* %call641, i32 2
  store float %sub638, float* %arrayidx642, align 4
  %tv_vertices643 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx644 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices643, i32 0, i32 0
  %call645 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx644)
  %arrayidx646 = getelementptr inbounds float, float* %call645, i32 2
  %204 = load float, float* %arrayidx646, align 4
  %tv_vertices647 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx648 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices647, i32 0, i32 1
  %call649 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx648)
  %arrayidx650 = getelementptr inbounds float, float* %call649, i32 2
  %205 = load float, float* %arrayidx650, align 4
  %sub651 = fsub float %204, %205
  %tv_vertices652 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx653 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices652, i32 0, i32 0
  %call654 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx653)
  %arrayidx655 = getelementptr inbounds float, float* %call654, i32 2
  store float %sub651, float* %arrayidx655, align 4
  %tv_plane656 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %206 = bitcast %class.btVector4* %tv_plane656 to %class.btVector3*
  %call657 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %206)
  %arrayidx658 = getelementptr inbounds float, float* %call657, i32 0
  %207 = load float, float* %arrayidx658, align 4
  %mul659 = fmul float -1.000000e+00, %207
  %tv_plane660 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %208 = bitcast %class.btVector4* %tv_plane660 to %class.btVector3*
  %call661 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %208)
  %arrayidx662 = getelementptr inbounds float, float* %call661, i32 0
  store float %mul659, float* %arrayidx662, align 4
  %tv_plane663 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %209 = bitcast %class.btVector4* %tv_plane663 to %class.btVector3*
  %call664 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %209)
  %arrayidx665 = getelementptr inbounds float, float* %call664, i32 1
  %210 = load float, float* %arrayidx665, align 4
  %mul666 = fmul float -1.000000e+00, %210
  %tv_plane667 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %211 = bitcast %class.btVector4* %tv_plane667 to %class.btVector3*
  %call668 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %211)
  %arrayidx669 = getelementptr inbounds float, float* %call668, i32 1
  store float %mul666, float* %arrayidx669, align 4
  %tv_plane670 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %212 = bitcast %class.btVector4* %tv_plane670 to %class.btVector3*
  %call671 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %212)
  %arrayidx672 = getelementptr inbounds float, float* %call671, i32 2
  %213 = load float, float* %arrayidx672, align 4
  %mul673 = fmul float -1.000000e+00, %213
  %tv_plane674 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %214 = bitcast %class.btVector4* %tv_plane674 to %class.btVector3*
  %call675 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %214)
  %arrayidx676 = getelementptr inbounds float, float* %call675, i32 2
  store float %mul673, float* %arrayidx676, align 4
  %tv_plane677 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %215 = bitcast %class.btVector4* %tv_plane677 to %class.btVector3*
  %call678 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %215)
  %arrayidx679 = getelementptr inbounds float, float* %call678, i32 3
  %216 = load float, float* %arrayidx679, align 4
  %mul680 = fmul float -1.000000e+00, %216
  %tv_plane681 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %217 = bitcast %class.btVector4* %tv_plane681 to %class.btVector3*
  %call682 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %217)
  %arrayidx683 = getelementptr inbounds float, float* %call682, i32 3
  store float %mul680, float* %arrayidx683, align 4
  %du684 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx685 = getelementptr inbounds [4 x float], [4 x float]* %du684, i32 0, i32 0
  %218 = load float, float* %arrayidx685, align 4
  %du686 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx687 = getelementptr inbounds [4 x float], [4 x float]* %du686, i32 0, i32 1
  %219 = load float, float* %arrayidx687, align 4
  %du688 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx689 = getelementptr inbounds [4 x float], [4 x float]* %du688, i32 0, i32 2
  %220 = load float, float* %arrayidx689, align 4
  %cmp690 = fcmp olt float %219, %220
  br i1 %cmp690, label %cond.true691, label %cond.false694

cond.true691:                                     ; preds = %if.then538
  %du692 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx693 = getelementptr inbounds [4 x float], [4 x float]* %du692, i32 0, i32 2
  %221 = load float, float* %arrayidx693, align 4
  br label %cond.end697

cond.false694:                                    ; preds = %if.then538
  %du695 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx696 = getelementptr inbounds [4 x float], [4 x float]* %du695, i32 0, i32 1
  %222 = load float, float* %arrayidx696, align 4
  br label %cond.end697

cond.end697:                                      ; preds = %cond.false694, %cond.true691
  %cond698 = phi float [ %221, %cond.true691 ], [ %222, %cond.false694 ]
  %cmp699 = fcmp olt float %218, %cond698
  br i1 %cmp699, label %cond.true700, label %cond.false714

cond.true700:                                     ; preds = %cond.end697
  %du701 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx702 = getelementptr inbounds [4 x float], [4 x float]* %du701, i32 0, i32 1
  %223 = load float, float* %arrayidx702, align 4
  %du703 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx704 = getelementptr inbounds [4 x float], [4 x float]* %du703, i32 0, i32 2
  %224 = load float, float* %arrayidx704, align 4
  %cmp705 = fcmp olt float %223, %224
  br i1 %cmp705, label %cond.true706, label %cond.false709

cond.true706:                                     ; preds = %cond.true700
  %du707 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx708 = getelementptr inbounds [4 x float], [4 x float]* %du707, i32 0, i32 2
  %225 = load float, float* %arrayidx708, align 4
  br label %cond.end712

cond.false709:                                    ; preds = %cond.true700
  %du710 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx711 = getelementptr inbounds [4 x float], [4 x float]* %du710, i32 0, i32 1
  %226 = load float, float* %arrayidx711, align 4
  br label %cond.end712

cond.end712:                                      ; preds = %cond.false709, %cond.true706
  %cond713 = phi float [ %225, %cond.true706 ], [ %226, %cond.false709 ]
  br label %cond.end717

cond.false714:                                    ; preds = %cond.end697
  %du715 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx716 = getelementptr inbounds [4 x float], [4 x float]* %du715, i32 0, i32 0
  %227 = load float, float* %arrayidx716, align 4
  br label %cond.end717

cond.end717:                                      ; preds = %cond.false714, %cond.end712
  %cond718 = phi float [ %cond713, %cond.end712 ], [ %227, %cond.false714 ]
  %distances719 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call720 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances719)
  %arrayidx721 = getelementptr inbounds float, float* %call720, i32 0
  store float %cond718, float* %arrayidx721, align 4
  %distances722 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call723 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances722)
  %arrayidx724 = getelementptr inbounds float, float* %call723, i32 0
  %228 = load float, float* %arrayidx724, align 4
  %fneg725 = fneg float %228
  %distances726 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call727 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances726)
  %arrayidx728 = getelementptr inbounds float, float* %call727, i32 0
  store float %fneg725, float* %arrayidx728, align 4
  br label %if.end768

if.else729:                                       ; preds = %if.else522
  %du730 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx731 = getelementptr inbounds [4 x float], [4 x float]* %du730, i32 0, i32 0
  %229 = load float, float* %arrayidx731, align 4
  %du732 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx733 = getelementptr inbounds [4 x float], [4 x float]* %du732, i32 0, i32 1
  %230 = load float, float* %arrayidx733, align 4
  %du734 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx735 = getelementptr inbounds [4 x float], [4 x float]* %du734, i32 0, i32 2
  %231 = load float, float* %arrayidx735, align 4
  %cmp736 = fcmp ogt float %230, %231
  br i1 %cmp736, label %cond.true737, label %cond.false740

cond.true737:                                     ; preds = %if.else729
  %du738 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx739 = getelementptr inbounds [4 x float], [4 x float]* %du738, i32 0, i32 2
  %232 = load float, float* %arrayidx739, align 4
  br label %cond.end743

cond.false740:                                    ; preds = %if.else729
  %du741 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx742 = getelementptr inbounds [4 x float], [4 x float]* %du741, i32 0, i32 1
  %233 = load float, float* %arrayidx742, align 4
  br label %cond.end743

cond.end743:                                      ; preds = %cond.false740, %cond.true737
  %cond744 = phi float [ %232, %cond.true737 ], [ %233, %cond.false740 ]
  %cmp745 = fcmp ogt float %229, %cond744
  br i1 %cmp745, label %cond.true746, label %cond.false760

cond.true746:                                     ; preds = %cond.end743
  %du747 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx748 = getelementptr inbounds [4 x float], [4 x float]* %du747, i32 0, i32 1
  %234 = load float, float* %arrayidx748, align 4
  %du749 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx750 = getelementptr inbounds [4 x float], [4 x float]* %du749, i32 0, i32 2
  %235 = load float, float* %arrayidx750, align 4
  %cmp751 = fcmp ogt float %234, %235
  br i1 %cmp751, label %cond.true752, label %cond.false755

cond.true752:                                     ; preds = %cond.true746
  %du753 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx754 = getelementptr inbounds [4 x float], [4 x float]* %du753, i32 0, i32 2
  %236 = load float, float* %arrayidx754, align 4
  br label %cond.end758

cond.false755:                                    ; preds = %cond.true746
  %du756 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx757 = getelementptr inbounds [4 x float], [4 x float]* %du756, i32 0, i32 1
  %237 = load float, float* %arrayidx757, align 4
  br label %cond.end758

cond.end758:                                      ; preds = %cond.false755, %cond.true752
  %cond759 = phi float [ %236, %cond.true752 ], [ %237, %cond.false755 ]
  br label %cond.end763

cond.false760:                                    ; preds = %cond.end743
  %du761 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 9
  %arrayidx762 = getelementptr inbounds [4 x float], [4 x float]* %du761, i32 0, i32 0
  %238 = load float, float* %arrayidx762, align 4
  br label %cond.end763

cond.end763:                                      ; preds = %cond.false760, %cond.end758
  %cond764 = phi float [ %cond759, %cond.end758 ], [ %238, %cond.false760 ]
  %distances765 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call766 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances765)
  %arrayidx767 = getelementptr inbounds float, float* %call766, i32 0
  store float %cond764, float* %arrayidx767, align 4
  br label %if.end768

if.end768:                                        ; preds = %cond.end763, %cond.end717
  br label %if.end769

if.end769:                                        ; preds = %if.end768, %if.end521
  %tu_vertices772 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx773 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices772, i32 0, i32 1
  %call774 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx773)
  %arrayidx775 = getelementptr inbounds float, float* %call774, i32 0
  %239 = load float, float* %arrayidx775, align 4
  %tu_vertices776 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx777 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices776, i32 0, i32 0
  %call778 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx777)
  %arrayidx779 = getelementptr inbounds float, float* %call778, i32 0
  %240 = load float, float* %arrayidx779, align 4
  %sub780 = fsub float %239, %240
  %arrayidx781 = getelementptr inbounds [3 x float], [3 x float]* %_dif1770, i32 0, i32 0
  store float %sub780, float* %arrayidx781, align 4
  %tu_vertices782 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx783 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices782, i32 0, i32 1
  %call784 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx783)
  %arrayidx785 = getelementptr inbounds float, float* %call784, i32 1
  %241 = load float, float* %arrayidx785, align 4
  %tu_vertices786 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx787 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices786, i32 0, i32 0
  %call788 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx787)
  %arrayidx789 = getelementptr inbounds float, float* %call788, i32 1
  %242 = load float, float* %arrayidx789, align 4
  %sub790 = fsub float %241, %242
  %arrayidx791 = getelementptr inbounds [3 x float], [3 x float]* %_dif1770, i32 0, i32 1
  store float %sub790, float* %arrayidx791, align 4
  %tu_vertices792 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx793 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices792, i32 0, i32 1
  %call794 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx793)
  %arrayidx795 = getelementptr inbounds float, float* %call794, i32 2
  %243 = load float, float* %arrayidx795, align 4
  %tu_vertices796 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx797 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices796, i32 0, i32 0
  %call798 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx797)
  %arrayidx799 = getelementptr inbounds float, float* %call798, i32 2
  %244 = load float, float* %arrayidx799, align 4
  %sub800 = fsub float %243, %244
  %arrayidx801 = getelementptr inbounds [3 x float], [3 x float]* %_dif1770, i32 0, i32 2
  store float %sub800, float* %arrayidx801, align 4
  %tu_vertices802 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx803 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices802, i32 0, i32 2
  %call804 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx803)
  %arrayidx805 = getelementptr inbounds float, float* %call804, i32 0
  %245 = load float, float* %arrayidx805, align 4
  %tu_vertices806 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx807 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices806, i32 0, i32 0
  %call808 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx807)
  %arrayidx809 = getelementptr inbounds float, float* %call808, i32 0
  %246 = load float, float* %arrayidx809, align 4
  %sub810 = fsub float %245, %246
  %arrayidx811 = getelementptr inbounds [3 x float], [3 x float]* %_dif2771, i32 0, i32 0
  store float %sub810, float* %arrayidx811, align 4
  %tu_vertices812 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx813 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices812, i32 0, i32 2
  %call814 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx813)
  %arrayidx815 = getelementptr inbounds float, float* %call814, i32 1
  %247 = load float, float* %arrayidx815, align 4
  %tu_vertices816 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx817 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices816, i32 0, i32 0
  %call818 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx817)
  %arrayidx819 = getelementptr inbounds float, float* %call818, i32 1
  %248 = load float, float* %arrayidx819, align 4
  %sub820 = fsub float %247, %248
  %arrayidx821 = getelementptr inbounds [3 x float], [3 x float]* %_dif2771, i32 0, i32 1
  store float %sub820, float* %arrayidx821, align 4
  %tu_vertices822 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx823 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices822, i32 0, i32 2
  %call824 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx823)
  %arrayidx825 = getelementptr inbounds float, float* %call824, i32 2
  %249 = load float, float* %arrayidx825, align 4
  %tu_vertices826 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx827 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices826, i32 0, i32 0
  %call828 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx827)
  %arrayidx829 = getelementptr inbounds float, float* %call828, i32 2
  %250 = load float, float* %arrayidx829, align 4
  %sub830 = fsub float %249, %250
  %arrayidx831 = getelementptr inbounds [3 x float], [3 x float]* %_dif2771, i32 0, i32 2
  store float %sub830, float* %arrayidx831, align 4
  %arrayidx832 = getelementptr inbounds [3 x float], [3 x float]* %_dif1770, i32 0, i32 1
  %251 = load float, float* %arrayidx832, align 4
  %arrayidx833 = getelementptr inbounds [3 x float], [3 x float]* %_dif2771, i32 0, i32 2
  %252 = load float, float* %arrayidx833, align 4
  %mul834 = fmul float %251, %252
  %arrayidx835 = getelementptr inbounds [3 x float], [3 x float]* %_dif1770, i32 0, i32 2
  %253 = load float, float* %arrayidx835, align 4
  %arrayidx836 = getelementptr inbounds [3 x float], [3 x float]* %_dif2771, i32 0, i32 1
  %254 = load float, float* %arrayidx836, align 4
  %mul837 = fmul float %253, %254
  %sub838 = fsub float %mul834, %mul837
  %tu_plane = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %255 = bitcast %class.btVector4* %tu_plane to %class.btVector3*
  %call839 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %255)
  %arrayidx840 = getelementptr inbounds float, float* %call839, i32 0
  store float %sub838, float* %arrayidx840, align 4
  %arrayidx841 = getelementptr inbounds [3 x float], [3 x float]* %_dif1770, i32 0, i32 2
  %256 = load float, float* %arrayidx841, align 4
  %arrayidx842 = getelementptr inbounds [3 x float], [3 x float]* %_dif2771, i32 0, i32 0
  %257 = load float, float* %arrayidx842, align 4
  %mul843 = fmul float %256, %257
  %arrayidx844 = getelementptr inbounds [3 x float], [3 x float]* %_dif1770, i32 0, i32 0
  %258 = load float, float* %arrayidx844, align 4
  %arrayidx845 = getelementptr inbounds [3 x float], [3 x float]* %_dif2771, i32 0, i32 2
  %259 = load float, float* %arrayidx845, align 4
  %mul846 = fmul float %258, %259
  %sub847 = fsub float %mul843, %mul846
  %tu_plane848 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %260 = bitcast %class.btVector4* %tu_plane848 to %class.btVector3*
  %call849 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %260)
  %arrayidx850 = getelementptr inbounds float, float* %call849, i32 1
  store float %sub847, float* %arrayidx850, align 4
  %arrayidx851 = getelementptr inbounds [3 x float], [3 x float]* %_dif1770, i32 0, i32 0
  %261 = load float, float* %arrayidx851, align 4
  %arrayidx852 = getelementptr inbounds [3 x float], [3 x float]* %_dif2771, i32 0, i32 1
  %262 = load float, float* %arrayidx852, align 4
  %mul853 = fmul float %261, %262
  %arrayidx854 = getelementptr inbounds [3 x float], [3 x float]* %_dif1770, i32 0, i32 1
  %263 = load float, float* %arrayidx854, align 4
  %arrayidx855 = getelementptr inbounds [3 x float], [3 x float]* %_dif2771, i32 0, i32 0
  %264 = load float, float* %arrayidx855, align 4
  %mul856 = fmul float %263, %264
  %sub857 = fsub float %mul853, %mul856
  %tu_plane858 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %265 = bitcast %class.btVector4* %tu_plane858 to %class.btVector3*
  %call859 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %265)
  %arrayidx860 = getelementptr inbounds float, float* %call859, i32 2
  store float %sub857, float* %arrayidx860, align 4
  %tu_plane863 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %266 = bitcast %class.btVector4* %tu_plane863 to %class.btVector3*
  %call864 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %266)
  %arrayidx865 = getelementptr inbounds float, float* %call864, i32 0
  %267 = load float, float* %arrayidx865, align 4
  %tu_plane866 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %268 = bitcast %class.btVector4* %tu_plane866 to %class.btVector3*
  %call867 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %268)
  %arrayidx868 = getelementptr inbounds float, float* %call867, i32 0
  %269 = load float, float* %arrayidx868, align 4
  %mul869 = fmul float %267, %269
  %tu_plane870 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %270 = bitcast %class.btVector4* %tu_plane870 to %class.btVector3*
  %call871 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %270)
  %arrayidx872 = getelementptr inbounds float, float* %call871, i32 1
  %271 = load float, float* %arrayidx872, align 4
  %tu_plane873 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %272 = bitcast %class.btVector4* %tu_plane873 to %class.btVector3*
  %call874 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %272)
  %arrayidx875 = getelementptr inbounds float, float* %call874, i32 1
  %273 = load float, float* %arrayidx875, align 4
  %mul876 = fmul float %271, %273
  %add877 = fadd float %mul869, %mul876
  %tu_plane878 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %274 = bitcast %class.btVector4* %tu_plane878 to %class.btVector3*
  %call879 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %274)
  %arrayidx880 = getelementptr inbounds float, float* %call879, i32 2
  %275 = load float, float* %arrayidx880, align 4
  %tu_plane881 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %276 = bitcast %class.btVector4* %tu_plane881 to %class.btVector3*
  %call882 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %276)
  %arrayidx883 = getelementptr inbounds float, float* %call882, i32 2
  %277 = load float, float* %arrayidx883, align 4
  %mul884 = fmul float %275, %277
  %add885 = fadd float %add877, %mul884
  store float %add885, float* %_pp862, align 4
  %278 = load float, float* %_pp862, align 4
  %cmp886 = fcmp ole float %278, 0x3E7AD7F2A0000000
  br i1 %cmp886, label %if.then887, label %if.else888

if.then887:                                       ; preds = %if.end769
  store float 0x47EFFFFFE0000000, float* %len861, align 4
  br label %if.end898

if.else888:                                       ; preds = %if.end769
  %279 = load float, float* %_pp862, align 4
  %mul890 = fmul float %279, 5.000000e-01
  store float %mul890, float* %_x889, align 4
  %280 = bitcast float* %_pp862 to i32*
  %281 = load i32, i32* %280, align 4
  %shr892 = lshr i32 %281, 1
  %sub893 = sub i32 1597463007, %shr892
  store i32 %sub893, i32* %_y891, align 4
  %282 = bitcast i32* %_y891 to float*
  %283 = load float, float* %282, align 4
  store float %283, float* %len861, align 4
  %284 = load float, float* %len861, align 4
  %285 = load float, float* %_x889, align 4
  %286 = load float, float* %len861, align 4
  %mul894 = fmul float %285, %286
  %287 = load float, float* %len861, align 4
  %mul895 = fmul float %mul894, %287
  %sub896 = fsub float 1.500000e+00, %mul895
  %mul897 = fmul float %284, %sub896
  store float %mul897, float* %len861, align 4
  br label %if.end898

if.end898:                                        ; preds = %if.else888, %if.then887
  %288 = load float, float* %len861, align 4
  %cmp899 = fcmp olt float %288, 0x47EFFFFFE0000000
  br i1 %cmp899, label %if.then900, label %if.end913

if.then900:                                       ; preds = %if.end898
  %289 = load float, float* %len861, align 4
  %tu_plane901 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %290 = bitcast %class.btVector4* %tu_plane901 to %class.btVector3*
  %call902 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %290)
  %arrayidx903 = getelementptr inbounds float, float* %call902, i32 0
  %291 = load float, float* %arrayidx903, align 4
  %mul904 = fmul float %291, %289
  store float %mul904, float* %arrayidx903, align 4
  %292 = load float, float* %len861, align 4
  %tu_plane905 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %293 = bitcast %class.btVector4* %tu_plane905 to %class.btVector3*
  %call906 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %293)
  %arrayidx907 = getelementptr inbounds float, float* %call906, i32 1
  %294 = load float, float* %arrayidx907, align 4
  %mul908 = fmul float %294, %292
  store float %mul908, float* %arrayidx907, align 4
  %295 = load float, float* %len861, align 4
  %tu_plane909 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %296 = bitcast %class.btVector4* %tu_plane909 to %class.btVector3*
  %call910 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %296)
  %arrayidx911 = getelementptr inbounds float, float* %call910, i32 2
  %297 = load float, float* %arrayidx911, align 4
  %mul912 = fmul float %297, %295
  store float %mul912, float* %arrayidx911, align 4
  br label %if.end913

if.end913:                                        ; preds = %if.then900, %if.end898
  %tu_vertices914 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx915 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices914, i32 0, i32 0
  %call916 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx915)
  %arrayidx917 = getelementptr inbounds float, float* %call916, i32 0
  %298 = load float, float* %arrayidx917, align 4
  %tu_plane918 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %299 = bitcast %class.btVector4* %tu_plane918 to %class.btVector3*
  %call919 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %299)
  %arrayidx920 = getelementptr inbounds float, float* %call919, i32 0
  %300 = load float, float* %arrayidx920, align 4
  %mul921 = fmul float %298, %300
  %tu_vertices922 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx923 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices922, i32 0, i32 0
  %call924 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx923)
  %arrayidx925 = getelementptr inbounds float, float* %call924, i32 1
  %301 = load float, float* %arrayidx925, align 4
  %tu_plane926 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %302 = bitcast %class.btVector4* %tu_plane926 to %class.btVector3*
  %call927 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %302)
  %arrayidx928 = getelementptr inbounds float, float* %call927, i32 1
  %303 = load float, float* %arrayidx928, align 4
  %mul929 = fmul float %301, %303
  %add930 = fadd float %mul921, %mul929
  %tu_vertices931 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx932 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices931, i32 0, i32 0
  %call933 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx932)
  %arrayidx934 = getelementptr inbounds float, float* %call933, i32 2
  %304 = load float, float* %arrayidx934, align 4
  %tu_plane935 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %305 = bitcast %class.btVector4* %tu_plane935 to %class.btVector3*
  %call936 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %305)
  %arrayidx937 = getelementptr inbounds float, float* %call936, i32 2
  %306 = load float, float* %arrayidx937, align 4
  %mul938 = fmul float %304, %306
  %add939 = fadd float %add930, %mul938
  %tu_plane940 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %307 = bitcast %class.btVector4* %tu_plane940 to %class.btVector3*
  %call941 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %307)
  %arrayidx942 = getelementptr inbounds float, float* %call941, i32 3
  store float %add939, float* %arrayidx942, align 4
  %tu_plane943 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %308 = bitcast %class.btVector4* %tu_plane943 to %class.btVector3*
  %call944 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %308)
  %arrayidx945 = getelementptr inbounds float, float* %call944, i32 0
  %309 = load float, float* %arrayidx945, align 4
  %tv_vertices946 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx947 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices946, i32 0, i32 0
  %call948 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx947)
  %arrayidx949 = getelementptr inbounds float, float* %call948, i32 0
  %310 = load float, float* %arrayidx949, align 4
  %mul950 = fmul float %309, %310
  %tu_plane951 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %311 = bitcast %class.btVector4* %tu_plane951 to %class.btVector3*
  %call952 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %311)
  %arrayidx953 = getelementptr inbounds float, float* %call952, i32 1
  %312 = load float, float* %arrayidx953, align 4
  %tv_vertices954 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx955 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices954, i32 0, i32 0
  %call956 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx955)
  %arrayidx957 = getelementptr inbounds float, float* %call956, i32 1
  %313 = load float, float* %arrayidx957, align 4
  %mul958 = fmul float %312, %313
  %add959 = fadd float %mul950, %mul958
  %tu_plane960 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %314 = bitcast %class.btVector4* %tu_plane960 to %class.btVector3*
  %call961 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %314)
  %arrayidx962 = getelementptr inbounds float, float* %call961, i32 2
  %315 = load float, float* %arrayidx962, align 4
  %tv_vertices963 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx964 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices963, i32 0, i32 0
  %call965 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx964)
  %arrayidx966 = getelementptr inbounds float, float* %call965, i32 2
  %316 = load float, float* %arrayidx966, align 4
  %mul967 = fmul float %315, %316
  %add968 = fadd float %add959, %mul967
  %tu_plane969 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %317 = bitcast %class.btVector4* %tu_plane969 to %class.btVector3*
  %call970 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %317)
  %arrayidx971 = getelementptr inbounds float, float* %call970, i32 3
  %318 = load float, float* %arrayidx971, align 4
  %sub972 = fsub float %add968, %318
  %dv = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx973 = getelementptr inbounds [4 x float], [4 x float]* %dv, i32 0, i32 0
  store float %sub972, float* %arrayidx973, align 4
  %tu_plane974 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %319 = bitcast %class.btVector4* %tu_plane974 to %class.btVector3*
  %call975 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %319)
  %arrayidx976 = getelementptr inbounds float, float* %call975, i32 0
  %320 = load float, float* %arrayidx976, align 4
  %tv_vertices977 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx978 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices977, i32 0, i32 1
  %call979 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx978)
  %arrayidx980 = getelementptr inbounds float, float* %call979, i32 0
  %321 = load float, float* %arrayidx980, align 4
  %mul981 = fmul float %320, %321
  %tu_plane982 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %322 = bitcast %class.btVector4* %tu_plane982 to %class.btVector3*
  %call983 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %322)
  %arrayidx984 = getelementptr inbounds float, float* %call983, i32 1
  %323 = load float, float* %arrayidx984, align 4
  %tv_vertices985 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx986 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices985, i32 0, i32 1
  %call987 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx986)
  %arrayidx988 = getelementptr inbounds float, float* %call987, i32 1
  %324 = load float, float* %arrayidx988, align 4
  %mul989 = fmul float %323, %324
  %add990 = fadd float %mul981, %mul989
  %tu_plane991 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %325 = bitcast %class.btVector4* %tu_plane991 to %class.btVector3*
  %call992 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %325)
  %arrayidx993 = getelementptr inbounds float, float* %call992, i32 2
  %326 = load float, float* %arrayidx993, align 4
  %tv_vertices994 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx995 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices994, i32 0, i32 1
  %call996 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx995)
  %arrayidx997 = getelementptr inbounds float, float* %call996, i32 2
  %327 = load float, float* %arrayidx997, align 4
  %mul998 = fmul float %326, %327
  %add999 = fadd float %add990, %mul998
  %tu_plane1000 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %328 = bitcast %class.btVector4* %tu_plane1000 to %class.btVector3*
  %call1001 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %328)
  %arrayidx1002 = getelementptr inbounds float, float* %call1001, i32 3
  %329 = load float, float* %arrayidx1002, align 4
  %sub1003 = fsub float %add999, %329
  %dv1004 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1005 = getelementptr inbounds [4 x float], [4 x float]* %dv1004, i32 0, i32 1
  store float %sub1003, float* %arrayidx1005, align 4
  %tu_plane1006 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %330 = bitcast %class.btVector4* %tu_plane1006 to %class.btVector3*
  %call1007 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %330)
  %arrayidx1008 = getelementptr inbounds float, float* %call1007, i32 0
  %331 = load float, float* %arrayidx1008, align 4
  %tv_vertices1009 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx1010 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices1009, i32 0, i32 2
  %call1011 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1010)
  %arrayidx1012 = getelementptr inbounds float, float* %call1011, i32 0
  %332 = load float, float* %arrayidx1012, align 4
  %mul1013 = fmul float %331, %332
  %tu_plane1014 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %333 = bitcast %class.btVector4* %tu_plane1014 to %class.btVector3*
  %call1015 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %333)
  %arrayidx1016 = getelementptr inbounds float, float* %call1015, i32 1
  %334 = load float, float* %arrayidx1016, align 4
  %tv_vertices1017 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx1018 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices1017, i32 0, i32 2
  %call1019 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1018)
  %arrayidx1020 = getelementptr inbounds float, float* %call1019, i32 1
  %335 = load float, float* %arrayidx1020, align 4
  %mul1021 = fmul float %334, %335
  %add1022 = fadd float %mul1013, %mul1021
  %tu_plane1023 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %336 = bitcast %class.btVector4* %tu_plane1023 to %class.btVector3*
  %call1024 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %336)
  %arrayidx1025 = getelementptr inbounds float, float* %call1024, i32 2
  %337 = load float, float* %arrayidx1025, align 4
  %tv_vertices1026 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arrayidx1027 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices1026, i32 0, i32 2
  %call1028 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1027)
  %arrayidx1029 = getelementptr inbounds float, float* %call1028, i32 2
  %338 = load float, float* %arrayidx1029, align 4
  %mul1030 = fmul float %337, %338
  %add1031 = fadd float %add1022, %mul1030
  %tu_plane1032 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %339 = bitcast %class.btVector4* %tu_plane1032 to %class.btVector3*
  %call1033 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %339)
  %arrayidx1034 = getelementptr inbounds float, float* %call1033, i32 3
  %340 = load float, float* %arrayidx1034, align 4
  %sub1035 = fsub float %add1031, %340
  %dv1036 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1037 = getelementptr inbounds [4 x float], [4 x float]* %dv1036, i32 0, i32 2
  store float %sub1035, float* %arrayidx1037, align 4
  %dv1038 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1039 = getelementptr inbounds [4 x float], [4 x float]* %dv1038, i32 0, i32 0
  %341 = load float, float* %arrayidx1039, align 4
  %dv1040 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1041 = getelementptr inbounds [4 x float], [4 x float]* %dv1040, i32 0, i32 1
  %342 = load float, float* %arrayidx1041, align 4
  %mul1042 = fmul float %341, %342
  %dv0dv1 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 13
  store float %mul1042, float* %dv0dv1, align 4
  %dv1043 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1044 = getelementptr inbounds [4 x float], [4 x float]* %dv1043, i32 0, i32 0
  %343 = load float, float* %arrayidx1044, align 4
  %dv1045 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1046 = getelementptr inbounds [4 x float], [4 x float]* %dv1045, i32 0, i32 2
  %344 = load float, float* %arrayidx1046, align 4
  %mul1047 = fmul float %343, %344
  %dv0dv2 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 14
  store float %mul1047, float* %dv0dv2, align 4
  %dv0dv11048 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 13
  %345 = load float, float* %dv0dv11048, align 4
  %cmp1049 = fcmp ogt float %345, 0.000000e+00
  br i1 %cmp1049, label %land.lhs.true1050, label %if.else1302

land.lhs.true1050:                                ; preds = %if.end913
  %dv0dv21051 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 14
  %346 = load float, float* %dv0dv21051, align 4
  %cmp1052 = fcmp ogt float %346, 0.000000e+00
  br i1 %cmp1052, label %if.then1053, label %if.else1302

if.then1053:                                      ; preds = %land.lhs.true1050
  %dv1054 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1055 = getelementptr inbounds [4 x float], [4 x float]* %dv1054, i32 0, i32 0
  %347 = load float, float* %arrayidx1055, align 4
  %cmp1056 = fcmp olt float %347, 0.000000e+00
  br i1 %cmp1056, label %if.then1057, label %if.else1255

if.then1057:                                      ; preds = %if.then1053
  %dv1058 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1059 = getelementptr inbounds [4 x float], [4 x float]* %dv1058, i32 0, i32 0
  %348 = load float, float* %arrayidx1059, align 4
  %dv1060 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1061 = getelementptr inbounds [4 x float], [4 x float]* %dv1060, i32 0, i32 1
  %349 = load float, float* %arrayidx1061, align 4
  %dv1062 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1063 = getelementptr inbounds [4 x float], [4 x float]* %dv1062, i32 0, i32 2
  %350 = load float, float* %arrayidx1063, align 4
  %cmp1064 = fcmp olt float %349, %350
  br i1 %cmp1064, label %cond.true1065, label %cond.false1068

cond.true1065:                                    ; preds = %if.then1057
  %dv1066 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1067 = getelementptr inbounds [4 x float], [4 x float]* %dv1066, i32 0, i32 2
  %351 = load float, float* %arrayidx1067, align 4
  br label %cond.end1071

cond.false1068:                                   ; preds = %if.then1057
  %dv1069 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1070 = getelementptr inbounds [4 x float], [4 x float]* %dv1069, i32 0, i32 1
  %352 = load float, float* %arrayidx1070, align 4
  br label %cond.end1071

cond.end1071:                                     ; preds = %cond.false1068, %cond.true1065
  %cond1072 = phi float [ %351, %cond.true1065 ], [ %352, %cond.false1068 ]
  %cmp1073 = fcmp olt float %348, %cond1072
  br i1 %cmp1073, label %cond.true1074, label %cond.false1088

cond.true1074:                                    ; preds = %cond.end1071
  %dv1075 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1076 = getelementptr inbounds [4 x float], [4 x float]* %dv1075, i32 0, i32 1
  %353 = load float, float* %arrayidx1076, align 4
  %dv1077 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1078 = getelementptr inbounds [4 x float], [4 x float]* %dv1077, i32 0, i32 2
  %354 = load float, float* %arrayidx1078, align 4
  %cmp1079 = fcmp olt float %353, %354
  br i1 %cmp1079, label %cond.true1080, label %cond.false1083

cond.true1080:                                    ; preds = %cond.true1074
  %dv1081 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1082 = getelementptr inbounds [4 x float], [4 x float]* %dv1081, i32 0, i32 2
  %355 = load float, float* %arrayidx1082, align 4
  br label %cond.end1086

cond.false1083:                                   ; preds = %cond.true1074
  %dv1084 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1085 = getelementptr inbounds [4 x float], [4 x float]* %dv1084, i32 0, i32 1
  %356 = load float, float* %arrayidx1085, align 4
  br label %cond.end1086

cond.end1086:                                     ; preds = %cond.false1083, %cond.true1080
  %cond1087 = phi float [ %355, %cond.true1080 ], [ %356, %cond.false1083 ]
  br label %cond.end1091

cond.false1088:                                   ; preds = %cond.end1071
  %dv1089 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1090 = getelementptr inbounds [4 x float], [4 x float]* %dv1089, i32 0, i32 0
  %357 = load float, float* %arrayidx1090, align 4
  br label %cond.end1091

cond.end1091:                                     ; preds = %cond.false1088, %cond.end1086
  %cond1092 = phi float [ %cond1087, %cond.end1086 ], [ %357, %cond.false1088 ]
  %distances1093 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1094 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1093)
  %arrayidx1095 = getelementptr inbounds float, float* %call1094, i32 1
  store float %cond1092, float* %arrayidx1095, align 4
  %distances1096 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1097 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1096)
  %arrayidx1098 = getelementptr inbounds float, float* %call1097, i32 1
  %358 = load float, float* %arrayidx1098, align 4
  %fneg1099 = fneg float %358
  %distances1100 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1101 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1100)
  %arrayidx1102 = getelementptr inbounds float, float* %call1101, i32 1
  store float %fneg1099, float* %arrayidx1102, align 4
  %distances1103 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1104 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1103)
  %arrayidx1105 = getelementptr inbounds float, float* %call1104, i32 1
  %359 = load float, float* %arrayidx1105, align 4
  %margin1106 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 0
  %360 = load float, float* %margin1106, align 4
  %cmp1107 = fcmp ogt float %359, %360
  br i1 %cmp1107, label %if.then1108, label %if.end1109

if.then1108:                                      ; preds = %cond.end1091
  store i1 false, i1* %retval, align 1
  br label %return

if.end1109:                                       ; preds = %cond.end1091
  %tu_vertices1110 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1111 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1110, i32 0, i32 0
  %call1112 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1111)
  %arrayidx1113 = getelementptr inbounds float, float* %call1112, i32 0
  %361 = load float, float* %arrayidx1113, align 4
  %tu_vertices1114 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1115 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1114, i32 0, i32 1
  %call1116 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1115)
  %arrayidx1117 = getelementptr inbounds float, float* %call1116, i32 0
  %362 = load float, float* %arrayidx1117, align 4
  %add1118 = fadd float %361, %362
  %tu_vertices1119 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1120 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1119, i32 0, i32 0
  %call1121 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1120)
  %arrayidx1122 = getelementptr inbounds float, float* %call1121, i32 0
  store float %add1118, float* %arrayidx1122, align 4
  %tu_vertices1123 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1124 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1123, i32 0, i32 0
  %call1125 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1124)
  %arrayidx1126 = getelementptr inbounds float, float* %call1125, i32 0
  %363 = load float, float* %arrayidx1126, align 4
  %tu_vertices1127 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1128 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1127, i32 0, i32 1
  %call1129 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1128)
  %arrayidx1130 = getelementptr inbounds float, float* %call1129, i32 0
  %364 = load float, float* %arrayidx1130, align 4
  %sub1131 = fsub float %363, %364
  %tu_vertices1132 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1133 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1132, i32 0, i32 1
  %call1134 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1133)
  %arrayidx1135 = getelementptr inbounds float, float* %call1134, i32 0
  store float %sub1131, float* %arrayidx1135, align 4
  %tu_vertices1136 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1137 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1136, i32 0, i32 0
  %call1138 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1137)
  %arrayidx1139 = getelementptr inbounds float, float* %call1138, i32 0
  %365 = load float, float* %arrayidx1139, align 4
  %tu_vertices1140 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1141 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1140, i32 0, i32 1
  %call1142 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1141)
  %arrayidx1143 = getelementptr inbounds float, float* %call1142, i32 0
  %366 = load float, float* %arrayidx1143, align 4
  %sub1144 = fsub float %365, %366
  %tu_vertices1145 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1146 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1145, i32 0, i32 0
  %call1147 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1146)
  %arrayidx1148 = getelementptr inbounds float, float* %call1147, i32 0
  store float %sub1144, float* %arrayidx1148, align 4
  %tu_vertices1149 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1150 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1149, i32 0, i32 0
  %call1151 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1150)
  %arrayidx1152 = getelementptr inbounds float, float* %call1151, i32 1
  %367 = load float, float* %arrayidx1152, align 4
  %tu_vertices1153 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1154 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1153, i32 0, i32 1
  %call1155 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1154)
  %arrayidx1156 = getelementptr inbounds float, float* %call1155, i32 1
  %368 = load float, float* %arrayidx1156, align 4
  %add1157 = fadd float %367, %368
  %tu_vertices1158 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1159 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1158, i32 0, i32 0
  %call1160 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1159)
  %arrayidx1161 = getelementptr inbounds float, float* %call1160, i32 1
  store float %add1157, float* %arrayidx1161, align 4
  %tu_vertices1162 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1163 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1162, i32 0, i32 0
  %call1164 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1163)
  %arrayidx1165 = getelementptr inbounds float, float* %call1164, i32 1
  %369 = load float, float* %arrayidx1165, align 4
  %tu_vertices1166 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1167 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1166, i32 0, i32 1
  %call1168 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1167)
  %arrayidx1169 = getelementptr inbounds float, float* %call1168, i32 1
  %370 = load float, float* %arrayidx1169, align 4
  %sub1170 = fsub float %369, %370
  %tu_vertices1171 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1172 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1171, i32 0, i32 1
  %call1173 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1172)
  %arrayidx1174 = getelementptr inbounds float, float* %call1173, i32 1
  store float %sub1170, float* %arrayidx1174, align 4
  %tu_vertices1175 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1176 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1175, i32 0, i32 0
  %call1177 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1176)
  %arrayidx1178 = getelementptr inbounds float, float* %call1177, i32 1
  %371 = load float, float* %arrayidx1178, align 4
  %tu_vertices1179 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1180 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1179, i32 0, i32 1
  %call1181 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1180)
  %arrayidx1182 = getelementptr inbounds float, float* %call1181, i32 1
  %372 = load float, float* %arrayidx1182, align 4
  %sub1183 = fsub float %371, %372
  %tu_vertices1184 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1185 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1184, i32 0, i32 0
  %call1186 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1185)
  %arrayidx1187 = getelementptr inbounds float, float* %call1186, i32 1
  store float %sub1183, float* %arrayidx1187, align 4
  %tu_vertices1188 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1189 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1188, i32 0, i32 0
  %call1190 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1189)
  %arrayidx1191 = getelementptr inbounds float, float* %call1190, i32 2
  %373 = load float, float* %arrayidx1191, align 4
  %tu_vertices1192 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1193 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1192, i32 0, i32 1
  %call1194 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1193)
  %arrayidx1195 = getelementptr inbounds float, float* %call1194, i32 2
  %374 = load float, float* %arrayidx1195, align 4
  %add1196 = fadd float %373, %374
  %tu_vertices1197 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1198 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1197, i32 0, i32 0
  %call1199 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1198)
  %arrayidx1200 = getelementptr inbounds float, float* %call1199, i32 2
  store float %add1196, float* %arrayidx1200, align 4
  %tu_vertices1201 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1202 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1201, i32 0, i32 0
  %call1203 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1202)
  %arrayidx1204 = getelementptr inbounds float, float* %call1203, i32 2
  %375 = load float, float* %arrayidx1204, align 4
  %tu_vertices1205 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1206 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1205, i32 0, i32 1
  %call1207 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1206)
  %arrayidx1208 = getelementptr inbounds float, float* %call1207, i32 2
  %376 = load float, float* %arrayidx1208, align 4
  %sub1209 = fsub float %375, %376
  %tu_vertices1210 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1211 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1210, i32 0, i32 1
  %call1212 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1211)
  %arrayidx1213 = getelementptr inbounds float, float* %call1212, i32 2
  store float %sub1209, float* %arrayidx1213, align 4
  %tu_vertices1214 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1215 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1214, i32 0, i32 0
  %call1216 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1215)
  %arrayidx1217 = getelementptr inbounds float, float* %call1216, i32 2
  %377 = load float, float* %arrayidx1217, align 4
  %tu_vertices1218 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1219 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1218, i32 0, i32 1
  %call1220 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1219)
  %arrayidx1221 = getelementptr inbounds float, float* %call1220, i32 2
  %378 = load float, float* %arrayidx1221, align 4
  %sub1222 = fsub float %377, %378
  %tu_vertices1223 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1224 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1223, i32 0, i32 0
  %call1225 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1224)
  %arrayidx1226 = getelementptr inbounds float, float* %call1225, i32 2
  store float %sub1222, float* %arrayidx1226, align 4
  %tu_plane1227 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %379 = bitcast %class.btVector4* %tu_plane1227 to %class.btVector3*
  %call1228 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %379)
  %arrayidx1229 = getelementptr inbounds float, float* %call1228, i32 0
  %380 = load float, float* %arrayidx1229, align 4
  %mul1230 = fmul float -1.000000e+00, %380
  %tu_plane1231 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %381 = bitcast %class.btVector4* %tu_plane1231 to %class.btVector3*
  %call1232 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %381)
  %arrayidx1233 = getelementptr inbounds float, float* %call1232, i32 0
  store float %mul1230, float* %arrayidx1233, align 4
  %tu_plane1234 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %382 = bitcast %class.btVector4* %tu_plane1234 to %class.btVector3*
  %call1235 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %382)
  %arrayidx1236 = getelementptr inbounds float, float* %call1235, i32 1
  %383 = load float, float* %arrayidx1236, align 4
  %mul1237 = fmul float -1.000000e+00, %383
  %tu_plane1238 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %384 = bitcast %class.btVector4* %tu_plane1238 to %class.btVector3*
  %call1239 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %384)
  %arrayidx1240 = getelementptr inbounds float, float* %call1239, i32 1
  store float %mul1237, float* %arrayidx1240, align 4
  %tu_plane1241 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %385 = bitcast %class.btVector4* %tu_plane1241 to %class.btVector3*
  %call1242 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %385)
  %arrayidx1243 = getelementptr inbounds float, float* %call1242, i32 2
  %386 = load float, float* %arrayidx1243, align 4
  %mul1244 = fmul float -1.000000e+00, %386
  %tu_plane1245 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %387 = bitcast %class.btVector4* %tu_plane1245 to %class.btVector3*
  %call1246 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %387)
  %arrayidx1247 = getelementptr inbounds float, float* %call1246, i32 2
  store float %mul1244, float* %arrayidx1247, align 4
  %tu_plane1248 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %388 = bitcast %class.btVector4* %tu_plane1248 to %class.btVector3*
  %call1249 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %388)
  %arrayidx1250 = getelementptr inbounds float, float* %call1249, i32 3
  %389 = load float, float* %arrayidx1250, align 4
  %mul1251 = fmul float -1.000000e+00, %389
  %tu_plane1252 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %390 = bitcast %class.btVector4* %tu_plane1252 to %class.btVector3*
  %call1253 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %390)
  %arrayidx1254 = getelementptr inbounds float, float* %call1253, i32 3
  store float %mul1251, float* %arrayidx1254, align 4
  br label %if.end1301

if.else1255:                                      ; preds = %if.then1053
  %dv1256 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1257 = getelementptr inbounds [4 x float], [4 x float]* %dv1256, i32 0, i32 0
  %391 = load float, float* %arrayidx1257, align 4
  %dv1258 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1259 = getelementptr inbounds [4 x float], [4 x float]* %dv1258, i32 0, i32 1
  %392 = load float, float* %arrayidx1259, align 4
  %dv1260 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1261 = getelementptr inbounds [4 x float], [4 x float]* %dv1260, i32 0, i32 2
  %393 = load float, float* %arrayidx1261, align 4
  %cmp1262 = fcmp ogt float %392, %393
  br i1 %cmp1262, label %cond.true1263, label %cond.false1266

cond.true1263:                                    ; preds = %if.else1255
  %dv1264 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1265 = getelementptr inbounds [4 x float], [4 x float]* %dv1264, i32 0, i32 2
  %394 = load float, float* %arrayidx1265, align 4
  br label %cond.end1269

cond.false1266:                                   ; preds = %if.else1255
  %dv1267 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1268 = getelementptr inbounds [4 x float], [4 x float]* %dv1267, i32 0, i32 1
  %395 = load float, float* %arrayidx1268, align 4
  br label %cond.end1269

cond.end1269:                                     ; preds = %cond.false1266, %cond.true1263
  %cond1270 = phi float [ %394, %cond.true1263 ], [ %395, %cond.false1266 ]
  %cmp1271 = fcmp ogt float %391, %cond1270
  br i1 %cmp1271, label %cond.true1272, label %cond.false1286

cond.true1272:                                    ; preds = %cond.end1269
  %dv1273 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1274 = getelementptr inbounds [4 x float], [4 x float]* %dv1273, i32 0, i32 1
  %396 = load float, float* %arrayidx1274, align 4
  %dv1275 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1276 = getelementptr inbounds [4 x float], [4 x float]* %dv1275, i32 0, i32 2
  %397 = load float, float* %arrayidx1276, align 4
  %cmp1277 = fcmp ogt float %396, %397
  br i1 %cmp1277, label %cond.true1278, label %cond.false1281

cond.true1278:                                    ; preds = %cond.true1272
  %dv1279 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1280 = getelementptr inbounds [4 x float], [4 x float]* %dv1279, i32 0, i32 2
  %398 = load float, float* %arrayidx1280, align 4
  br label %cond.end1284

cond.false1281:                                   ; preds = %cond.true1272
  %dv1282 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1283 = getelementptr inbounds [4 x float], [4 x float]* %dv1282, i32 0, i32 1
  %399 = load float, float* %arrayidx1283, align 4
  br label %cond.end1284

cond.end1284:                                     ; preds = %cond.false1281, %cond.true1278
  %cond1285 = phi float [ %398, %cond.true1278 ], [ %399, %cond.false1281 ]
  br label %cond.end1289

cond.false1286:                                   ; preds = %cond.end1269
  %dv1287 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1288 = getelementptr inbounds [4 x float], [4 x float]* %dv1287, i32 0, i32 0
  %400 = load float, float* %arrayidx1288, align 4
  br label %cond.end1289

cond.end1289:                                     ; preds = %cond.false1286, %cond.end1284
  %cond1290 = phi float [ %cond1285, %cond.end1284 ], [ %400, %cond.false1286 ]
  %distances1291 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1292 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1291)
  %arrayidx1293 = getelementptr inbounds float, float* %call1292, i32 1
  store float %cond1290, float* %arrayidx1293, align 4
  %distances1294 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1295 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1294)
  %arrayidx1296 = getelementptr inbounds float, float* %call1295, i32 1
  %401 = load float, float* %arrayidx1296, align 4
  %margin1297 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 0
  %402 = load float, float* %margin1297, align 4
  %cmp1298 = fcmp ogt float %401, %402
  br i1 %cmp1298, label %if.then1299, label %if.end1300

if.then1299:                                      ; preds = %cond.end1289
  store i1 false, i1* %retval, align 1
  br label %return

if.end1300:                                       ; preds = %cond.end1289
  br label %if.end1301

if.end1301:                                       ; preds = %if.end1300, %if.end1109
  br label %if.end1550

if.else1302:                                      ; preds = %land.lhs.true1050, %if.end913
  %dv1303 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1304 = getelementptr inbounds [4 x float], [4 x float]* %dv1303, i32 0, i32 0
  %403 = load float, float* %arrayidx1304, align 4
  %dv1305 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1306 = getelementptr inbounds [4 x float], [4 x float]* %dv1305, i32 0, i32 1
  %404 = load float, float* %arrayidx1306, align 4
  %add1307 = fadd float %403, %404
  %dv1308 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1309 = getelementptr inbounds [4 x float], [4 x float]* %dv1308, i32 0, i32 2
  %405 = load float, float* %arrayidx1309, align 4
  %add1310 = fadd float %add1307, %405
  %div1311 = fdiv float %add1310, 3.000000e+00
  %distances1312 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1313 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1312)
  %arrayidx1314 = getelementptr inbounds float, float* %call1313, i32 1
  store float %div1311, float* %arrayidx1314, align 4
  %distances1315 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1316 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1315)
  %arrayidx1317 = getelementptr inbounds float, float* %call1316, i32 1
  %406 = load float, float* %arrayidx1317, align 4
  %cmp1318 = fcmp olt float %406, 0.000000e+00
  br i1 %cmp1318, label %if.then1319, label %if.else1510

if.then1319:                                      ; preds = %if.else1302
  %tu_vertices1320 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1321 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1320, i32 0, i32 0
  %call1322 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1321)
  %arrayidx1323 = getelementptr inbounds float, float* %call1322, i32 0
  %407 = load float, float* %arrayidx1323, align 4
  %tu_vertices1324 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1325 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1324, i32 0, i32 1
  %call1326 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1325)
  %arrayidx1327 = getelementptr inbounds float, float* %call1326, i32 0
  %408 = load float, float* %arrayidx1327, align 4
  %add1328 = fadd float %407, %408
  %tu_vertices1329 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1330 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1329, i32 0, i32 0
  %call1331 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1330)
  %arrayidx1332 = getelementptr inbounds float, float* %call1331, i32 0
  store float %add1328, float* %arrayidx1332, align 4
  %tu_vertices1333 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1334 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1333, i32 0, i32 0
  %call1335 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1334)
  %arrayidx1336 = getelementptr inbounds float, float* %call1335, i32 0
  %409 = load float, float* %arrayidx1336, align 4
  %tu_vertices1337 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1338 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1337, i32 0, i32 1
  %call1339 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1338)
  %arrayidx1340 = getelementptr inbounds float, float* %call1339, i32 0
  %410 = load float, float* %arrayidx1340, align 4
  %sub1341 = fsub float %409, %410
  %tu_vertices1342 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1343 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1342, i32 0, i32 1
  %call1344 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1343)
  %arrayidx1345 = getelementptr inbounds float, float* %call1344, i32 0
  store float %sub1341, float* %arrayidx1345, align 4
  %tu_vertices1346 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1347 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1346, i32 0, i32 0
  %call1348 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1347)
  %arrayidx1349 = getelementptr inbounds float, float* %call1348, i32 0
  %411 = load float, float* %arrayidx1349, align 4
  %tu_vertices1350 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1351 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1350, i32 0, i32 1
  %call1352 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1351)
  %arrayidx1353 = getelementptr inbounds float, float* %call1352, i32 0
  %412 = load float, float* %arrayidx1353, align 4
  %sub1354 = fsub float %411, %412
  %tu_vertices1355 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1356 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1355, i32 0, i32 0
  %call1357 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1356)
  %arrayidx1358 = getelementptr inbounds float, float* %call1357, i32 0
  store float %sub1354, float* %arrayidx1358, align 4
  %tu_vertices1359 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1360 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1359, i32 0, i32 0
  %call1361 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1360)
  %arrayidx1362 = getelementptr inbounds float, float* %call1361, i32 1
  %413 = load float, float* %arrayidx1362, align 4
  %tu_vertices1363 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1364 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1363, i32 0, i32 1
  %call1365 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1364)
  %arrayidx1366 = getelementptr inbounds float, float* %call1365, i32 1
  %414 = load float, float* %arrayidx1366, align 4
  %add1367 = fadd float %413, %414
  %tu_vertices1368 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1369 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1368, i32 0, i32 0
  %call1370 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1369)
  %arrayidx1371 = getelementptr inbounds float, float* %call1370, i32 1
  store float %add1367, float* %arrayidx1371, align 4
  %tu_vertices1372 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1373 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1372, i32 0, i32 0
  %call1374 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1373)
  %arrayidx1375 = getelementptr inbounds float, float* %call1374, i32 1
  %415 = load float, float* %arrayidx1375, align 4
  %tu_vertices1376 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1377 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1376, i32 0, i32 1
  %call1378 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1377)
  %arrayidx1379 = getelementptr inbounds float, float* %call1378, i32 1
  %416 = load float, float* %arrayidx1379, align 4
  %sub1380 = fsub float %415, %416
  %tu_vertices1381 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1382 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1381, i32 0, i32 1
  %call1383 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1382)
  %arrayidx1384 = getelementptr inbounds float, float* %call1383, i32 1
  store float %sub1380, float* %arrayidx1384, align 4
  %tu_vertices1385 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1386 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1385, i32 0, i32 0
  %call1387 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1386)
  %arrayidx1388 = getelementptr inbounds float, float* %call1387, i32 1
  %417 = load float, float* %arrayidx1388, align 4
  %tu_vertices1389 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1390 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1389, i32 0, i32 1
  %call1391 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1390)
  %arrayidx1392 = getelementptr inbounds float, float* %call1391, i32 1
  %418 = load float, float* %arrayidx1392, align 4
  %sub1393 = fsub float %417, %418
  %tu_vertices1394 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1395 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1394, i32 0, i32 0
  %call1396 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1395)
  %arrayidx1397 = getelementptr inbounds float, float* %call1396, i32 1
  store float %sub1393, float* %arrayidx1397, align 4
  %tu_vertices1398 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1399 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1398, i32 0, i32 0
  %call1400 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1399)
  %arrayidx1401 = getelementptr inbounds float, float* %call1400, i32 2
  %419 = load float, float* %arrayidx1401, align 4
  %tu_vertices1402 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1403 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1402, i32 0, i32 1
  %call1404 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1403)
  %arrayidx1405 = getelementptr inbounds float, float* %call1404, i32 2
  %420 = load float, float* %arrayidx1405, align 4
  %add1406 = fadd float %419, %420
  %tu_vertices1407 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1408 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1407, i32 0, i32 0
  %call1409 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1408)
  %arrayidx1410 = getelementptr inbounds float, float* %call1409, i32 2
  store float %add1406, float* %arrayidx1410, align 4
  %tu_vertices1411 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1412 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1411, i32 0, i32 0
  %call1413 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1412)
  %arrayidx1414 = getelementptr inbounds float, float* %call1413, i32 2
  %421 = load float, float* %arrayidx1414, align 4
  %tu_vertices1415 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1416 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1415, i32 0, i32 1
  %call1417 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1416)
  %arrayidx1418 = getelementptr inbounds float, float* %call1417, i32 2
  %422 = load float, float* %arrayidx1418, align 4
  %sub1419 = fsub float %421, %422
  %tu_vertices1420 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1421 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1420, i32 0, i32 1
  %call1422 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1421)
  %arrayidx1423 = getelementptr inbounds float, float* %call1422, i32 2
  store float %sub1419, float* %arrayidx1423, align 4
  %tu_vertices1424 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1425 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1424, i32 0, i32 0
  %call1426 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1425)
  %arrayidx1427 = getelementptr inbounds float, float* %call1426, i32 2
  %423 = load float, float* %arrayidx1427, align 4
  %tu_vertices1428 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1429 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1428, i32 0, i32 1
  %call1430 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1429)
  %arrayidx1431 = getelementptr inbounds float, float* %call1430, i32 2
  %424 = load float, float* %arrayidx1431, align 4
  %sub1432 = fsub float %423, %424
  %tu_vertices1433 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arrayidx1434 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1433, i32 0, i32 0
  %call1435 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx1434)
  %arrayidx1436 = getelementptr inbounds float, float* %call1435, i32 2
  store float %sub1432, float* %arrayidx1436, align 4
  %tu_plane1437 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %425 = bitcast %class.btVector4* %tu_plane1437 to %class.btVector3*
  %call1438 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %425)
  %arrayidx1439 = getelementptr inbounds float, float* %call1438, i32 0
  %426 = load float, float* %arrayidx1439, align 4
  %mul1440 = fmul float -1.000000e+00, %426
  %tu_plane1441 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %427 = bitcast %class.btVector4* %tu_plane1441 to %class.btVector3*
  %call1442 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %427)
  %arrayidx1443 = getelementptr inbounds float, float* %call1442, i32 0
  store float %mul1440, float* %arrayidx1443, align 4
  %tu_plane1444 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %428 = bitcast %class.btVector4* %tu_plane1444 to %class.btVector3*
  %call1445 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %428)
  %arrayidx1446 = getelementptr inbounds float, float* %call1445, i32 1
  %429 = load float, float* %arrayidx1446, align 4
  %mul1447 = fmul float -1.000000e+00, %429
  %tu_plane1448 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %430 = bitcast %class.btVector4* %tu_plane1448 to %class.btVector3*
  %call1449 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %430)
  %arrayidx1450 = getelementptr inbounds float, float* %call1449, i32 1
  store float %mul1447, float* %arrayidx1450, align 4
  %tu_plane1451 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %431 = bitcast %class.btVector4* %tu_plane1451 to %class.btVector3*
  %call1452 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %431)
  %arrayidx1453 = getelementptr inbounds float, float* %call1452, i32 2
  %432 = load float, float* %arrayidx1453, align 4
  %mul1454 = fmul float -1.000000e+00, %432
  %tu_plane1455 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %433 = bitcast %class.btVector4* %tu_plane1455 to %class.btVector3*
  %call1456 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %433)
  %arrayidx1457 = getelementptr inbounds float, float* %call1456, i32 2
  store float %mul1454, float* %arrayidx1457, align 4
  %tu_plane1458 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %434 = bitcast %class.btVector4* %tu_plane1458 to %class.btVector3*
  %call1459 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %434)
  %arrayidx1460 = getelementptr inbounds float, float* %call1459, i32 3
  %435 = load float, float* %arrayidx1460, align 4
  %mul1461 = fmul float -1.000000e+00, %435
  %tu_plane1462 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %436 = bitcast %class.btVector4* %tu_plane1462 to %class.btVector3*
  %call1463 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %436)
  %arrayidx1464 = getelementptr inbounds float, float* %call1463, i32 3
  store float %mul1461, float* %arrayidx1464, align 4
  %dv1465 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1466 = getelementptr inbounds [4 x float], [4 x float]* %dv1465, i32 0, i32 0
  %437 = load float, float* %arrayidx1466, align 4
  %dv1467 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1468 = getelementptr inbounds [4 x float], [4 x float]* %dv1467, i32 0, i32 1
  %438 = load float, float* %arrayidx1468, align 4
  %dv1469 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1470 = getelementptr inbounds [4 x float], [4 x float]* %dv1469, i32 0, i32 2
  %439 = load float, float* %arrayidx1470, align 4
  %cmp1471 = fcmp olt float %438, %439
  br i1 %cmp1471, label %cond.true1472, label %cond.false1475

cond.true1472:                                    ; preds = %if.then1319
  %dv1473 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1474 = getelementptr inbounds [4 x float], [4 x float]* %dv1473, i32 0, i32 2
  %440 = load float, float* %arrayidx1474, align 4
  br label %cond.end1478

cond.false1475:                                   ; preds = %if.then1319
  %dv1476 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1477 = getelementptr inbounds [4 x float], [4 x float]* %dv1476, i32 0, i32 1
  %441 = load float, float* %arrayidx1477, align 4
  br label %cond.end1478

cond.end1478:                                     ; preds = %cond.false1475, %cond.true1472
  %cond1479 = phi float [ %440, %cond.true1472 ], [ %441, %cond.false1475 ]
  %cmp1480 = fcmp olt float %437, %cond1479
  br i1 %cmp1480, label %cond.true1481, label %cond.false1495

cond.true1481:                                    ; preds = %cond.end1478
  %dv1482 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1483 = getelementptr inbounds [4 x float], [4 x float]* %dv1482, i32 0, i32 1
  %442 = load float, float* %arrayidx1483, align 4
  %dv1484 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1485 = getelementptr inbounds [4 x float], [4 x float]* %dv1484, i32 0, i32 2
  %443 = load float, float* %arrayidx1485, align 4
  %cmp1486 = fcmp olt float %442, %443
  br i1 %cmp1486, label %cond.true1487, label %cond.false1490

cond.true1487:                                    ; preds = %cond.true1481
  %dv1488 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1489 = getelementptr inbounds [4 x float], [4 x float]* %dv1488, i32 0, i32 2
  %444 = load float, float* %arrayidx1489, align 4
  br label %cond.end1493

cond.false1490:                                   ; preds = %cond.true1481
  %dv1491 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1492 = getelementptr inbounds [4 x float], [4 x float]* %dv1491, i32 0, i32 1
  %445 = load float, float* %arrayidx1492, align 4
  br label %cond.end1493

cond.end1493:                                     ; preds = %cond.false1490, %cond.true1487
  %cond1494 = phi float [ %444, %cond.true1487 ], [ %445, %cond.false1490 ]
  br label %cond.end1498

cond.false1495:                                   ; preds = %cond.end1478
  %dv1496 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1497 = getelementptr inbounds [4 x float], [4 x float]* %dv1496, i32 0, i32 0
  %446 = load float, float* %arrayidx1497, align 4
  br label %cond.end1498

cond.end1498:                                     ; preds = %cond.false1495, %cond.end1493
  %cond1499 = phi float [ %cond1494, %cond.end1493 ], [ %446, %cond.false1495 ]
  %distances1500 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1501 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1500)
  %arrayidx1502 = getelementptr inbounds float, float* %call1501, i32 1
  store float %cond1499, float* %arrayidx1502, align 4
  %distances1503 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1504 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1503)
  %arrayidx1505 = getelementptr inbounds float, float* %call1504, i32 1
  %447 = load float, float* %arrayidx1505, align 4
  %fneg1506 = fneg float %447
  %distances1507 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1508 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1507)
  %arrayidx1509 = getelementptr inbounds float, float* %call1508, i32 1
  store float %fneg1506, float* %arrayidx1509, align 4
  br label %if.end1549

if.else1510:                                      ; preds = %if.else1302
  %dv1511 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1512 = getelementptr inbounds [4 x float], [4 x float]* %dv1511, i32 0, i32 0
  %448 = load float, float* %arrayidx1512, align 4
  %dv1513 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1514 = getelementptr inbounds [4 x float], [4 x float]* %dv1513, i32 0, i32 1
  %449 = load float, float* %arrayidx1514, align 4
  %dv1515 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1516 = getelementptr inbounds [4 x float], [4 x float]* %dv1515, i32 0, i32 2
  %450 = load float, float* %arrayidx1516, align 4
  %cmp1517 = fcmp ogt float %449, %450
  br i1 %cmp1517, label %cond.true1518, label %cond.false1521

cond.true1518:                                    ; preds = %if.else1510
  %dv1519 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1520 = getelementptr inbounds [4 x float], [4 x float]* %dv1519, i32 0, i32 2
  %451 = load float, float* %arrayidx1520, align 4
  br label %cond.end1524

cond.false1521:                                   ; preds = %if.else1510
  %dv1522 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1523 = getelementptr inbounds [4 x float], [4 x float]* %dv1522, i32 0, i32 1
  %452 = load float, float* %arrayidx1523, align 4
  br label %cond.end1524

cond.end1524:                                     ; preds = %cond.false1521, %cond.true1518
  %cond1525 = phi float [ %451, %cond.true1518 ], [ %452, %cond.false1521 ]
  %cmp1526 = fcmp ogt float %448, %cond1525
  br i1 %cmp1526, label %cond.true1527, label %cond.false1541

cond.true1527:                                    ; preds = %cond.end1524
  %dv1528 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1529 = getelementptr inbounds [4 x float], [4 x float]* %dv1528, i32 0, i32 1
  %453 = load float, float* %arrayidx1529, align 4
  %dv1530 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1531 = getelementptr inbounds [4 x float], [4 x float]* %dv1530, i32 0, i32 2
  %454 = load float, float* %arrayidx1531, align 4
  %cmp1532 = fcmp ogt float %453, %454
  br i1 %cmp1532, label %cond.true1533, label %cond.false1536

cond.true1533:                                    ; preds = %cond.true1527
  %dv1534 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1535 = getelementptr inbounds [4 x float], [4 x float]* %dv1534, i32 0, i32 2
  %455 = load float, float* %arrayidx1535, align 4
  br label %cond.end1539

cond.false1536:                                   ; preds = %cond.true1527
  %dv1537 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1538 = getelementptr inbounds [4 x float], [4 x float]* %dv1537, i32 0, i32 1
  %456 = load float, float* %arrayidx1538, align 4
  br label %cond.end1539

cond.end1539:                                     ; preds = %cond.false1536, %cond.true1533
  %cond1540 = phi float [ %455, %cond.true1533 ], [ %456, %cond.false1536 ]
  br label %cond.end1544

cond.false1541:                                   ; preds = %cond.end1524
  %dv1542 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 12
  %arrayidx1543 = getelementptr inbounds [4 x float], [4 x float]* %dv1542, i32 0, i32 0
  %457 = load float, float* %arrayidx1543, align 4
  br label %cond.end1544

cond.end1544:                                     ; preds = %cond.false1541, %cond.end1539
  %cond1545 = phi float [ %cond1540, %cond.end1539 ], [ %457, %cond.false1541 ]
  %distances1546 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1547 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1546)
  %arrayidx1548 = getelementptr inbounds float, float* %call1547, i32 1
  store float %cond1545, float* %arrayidx1548, align 4
  br label %if.end1549

if.end1549:                                       ; preds = %cond.end1544, %cond.end1498
  br label %if.end1550

if.end1550:                                       ; preds = %if.end1549, %if.end1301
  store i32 0, i32* %bl, align 4
  %distances1551 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1552 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1551)
  %arrayidx1553 = getelementptr inbounds float, float* %call1552, i32 0
  %458 = load float, float* %arrayidx1553, align 4
  %distances1554 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1555 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1554)
  %arrayidx1556 = getelementptr inbounds float, float* %call1555, i32 1
  %459 = load float, float* %arrayidx1556, align 4
  %cmp1557 = fcmp olt float %458, %459
  br i1 %cmp1557, label %if.then1558, label %if.end1559

if.then1558:                                      ; preds = %if.end1550
  store i32 1, i32* %bl, align 4
  br label %if.end1559

if.end1559:                                       ; preds = %if.then1558, %if.end1550
  %460 = load i32, i32* %bl, align 4
  %cmp1560 = icmp eq i32 %460, 2
  br i1 %cmp1560, label %if.then1561, label %if.end1592

if.then1561:                                      ; preds = %if.end1559
  %distances1562 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1563 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1562)
  %arrayidx1564 = getelementptr inbounds float, float* %call1563, i32 2
  %461 = load float, float* %arrayidx1564, align 4
  %margin1565 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 0
  %462 = load float, float* %margin1565, align 4
  %cmp1566 = fcmp ogt float %461, %462
  br i1 %cmp1566, label %if.then1567, label %if.end1568

if.then1567:                                      ; preds = %if.then1561
  store i1 false, i1* %retval, align 1
  br label %return

if.end1568:                                       ; preds = %if.then1561
  %distances1569 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 8
  %call1570 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %distances1569)
  %arrayidx1571 = getelementptr inbounds float, float* %call1570, i32 2
  %463 = load float, float* %arrayidx1571, align 4
  %fneg1572 = fneg float %463
  %margin1573 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 0
  %464 = load float, float* %margin1573, align 4
  %add1574 = fadd float %fneg1572, %464
  %465 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4
  %m_penetration_depth = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %465, i32 0, i32 0
  store float %add1574, float* %m_penetration_depth, align 4
  %closest_point_v = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 6
  %466 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4
  %m_points = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %466, i32 0, i32 3
  %arrayidx1575 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %m_points, i32 0, i32 0
  %467 = bitcast %class.btVector3* %arrayidx1575 to i8*
  %468 = bitcast %class.btVector3* %closest_point_v to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %467, i8* align 4 %468, i32 16, i1 false)
  %469 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4
  %m_point_count = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %469, i32 0, i32 1
  store i32 1, i32* %m_point_count, align 4
  %edge_edge_dir = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 7
  %call1576 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %edge_edge_dir)
  %arrayidx1577 = getelementptr inbounds float, float* %call1576, i32 0
  %470 = load float, float* %arrayidx1577, align 4
  %471 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4
  %m_separating_normal = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %471, i32 0, i32 2
  %472 = bitcast %class.btVector4* %m_separating_normal to %class.btVector3*
  %call1578 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %472)
  %arrayidx1579 = getelementptr inbounds float, float* %call1578, i32 0
  store float %470, float* %arrayidx1579, align 4
  %edge_edge_dir1580 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 7
  %call1581 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %edge_edge_dir1580)
  %arrayidx1582 = getelementptr inbounds float, float* %call1581, i32 1
  %473 = load float, float* %arrayidx1582, align 4
  %474 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4
  %m_separating_normal1583 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %474, i32 0, i32 2
  %475 = bitcast %class.btVector4* %m_separating_normal1583 to %class.btVector3*
  %call1584 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %475)
  %arrayidx1585 = getelementptr inbounds float, float* %call1584, i32 1
  store float %473, float* %arrayidx1585, align 4
  %edge_edge_dir1586 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 7
  %call1587 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %edge_edge_dir1586)
  %arrayidx1588 = getelementptr inbounds float, float* %call1587, i32 2
  %476 = load float, float* %arrayidx1588, align 4
  %477 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4
  %m_separating_normal1589 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %477, i32 0, i32 2
  %478 = bitcast %class.btVector4* %m_separating_normal1589 to %class.btVector3*
  %call1590 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %478)
  %arrayidx1591 = getelementptr inbounds float, float* %call1590, i32 2
  store float %476, float* %arrayidx1591, align 4
  store i1 true, i1* %retval, align 1
  br label %return

if.end1592:                                       ; preds = %if.end1559
  %479 = load i32, i32* %bl, align 4
  %cmp1593 = icmp eq i32 %479, 0
  br i1 %cmp1593, label %if.then1594, label %if.else1608

if.then1594:                                      ; preds = %if.end1592
  %tv_plane1595 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %tv_vertices1596 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arraydecay = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices1596, i32 0, i32 0
  %tu_vertices1597 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arraydecay1598 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1597, i32 0, i32 0
  %contact_points = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 17
  %arraydecay1599 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %contact_points, i32 0, i32 0
  %call1600 = call i32 @_ZN30GIM_TRIANGLE_CALCULATION_CACHE13clip_triangleERK9btVector4PK9btVector3S5_PS3_(%class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, %class.btVector4* nonnull align 4 dereferenceable(16) %tv_plane1595, %class.btVector3* %arraydecay, %class.btVector3* %arraydecay1598, %class.btVector3* %arraydecay1599)
  store i32 %call1600, i32* %point_count, align 4
  %480 = load i32, i32* %point_count, align 4
  %cmp1601 = icmp eq i32 %480, 0
  br i1 %cmp1601, label %if.then1602, label %if.end1603

if.then1602:                                      ; preds = %if.then1594
  store i1 false, i1* %retval, align 1
  br label %return

if.end1603:                                       ; preds = %if.then1594
  %481 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4
  %tv_plane1604 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 4
  %margin1605 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 0
  %482 = load float, float* %margin1605, align 4
  %contact_points1606 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 17
  %arraydecay1607 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %contact_points1606, i32 0, i32 0
  %483 = load i32, i32* %point_count, align 4
  call void @_ZN25GIM_TRIANGLE_CONTACT_DATA12merge_pointsERK9btVector4fPK9btVector3j(%struct.GIM_TRIANGLE_CONTACT_DATA* %481, %class.btVector4* nonnull align 4 dereferenceable(16) %tv_plane1604, float %482, %class.btVector3* %arraydecay1607, i32 %483)
  br label %if.end1626

if.else1608:                                      ; preds = %if.end1592
  %tu_plane1609 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %tu_vertices1610 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 1
  %arraydecay1611 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tu_vertices1610, i32 0, i32 0
  %tv_vertices1612 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 2
  %arraydecay1613 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %tv_vertices1612, i32 0, i32 0
  %contact_points1614 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 17
  %arraydecay1615 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %contact_points1614, i32 0, i32 0
  %call1616 = call i32 @_ZN30GIM_TRIANGLE_CALCULATION_CACHE13clip_triangleERK9btVector4PK9btVector3S5_PS3_(%class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, %class.btVector4* nonnull align 4 dereferenceable(16) %tu_plane1609, %class.btVector3* %arraydecay1611, %class.btVector3* %arraydecay1613, %class.btVector3* %arraydecay1615)
  store i32 %call1616, i32* %point_count, align 4
  %484 = load i32, i32* %point_count, align 4
  %cmp1617 = icmp eq i32 %484, 0
  br i1 %cmp1617, label %if.then1618, label %if.end1619

if.then1618:                                      ; preds = %if.else1608
  store i1 false, i1* %retval, align 1
  br label %return

if.end1619:                                       ; preds = %if.else1608
  %485 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4
  %tu_plane1620 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 3
  %margin1621 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 0
  %486 = load float, float* %margin1621, align 4
  %contact_points1622 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 17
  %arraydecay1623 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %contact_points1622, i32 0, i32 0
  %487 = load i32, i32* %point_count, align 4
  call void @_ZN25GIM_TRIANGLE_CONTACT_DATA12merge_pointsERK9btVector4fPK9btVector3j(%struct.GIM_TRIANGLE_CONTACT_DATA* %485, %class.btVector4* nonnull align 4 dereferenceable(16) %tu_plane1620, float %486, %class.btVector3* %arraydecay1623, i32 %487)
  store float -1.000000e+00, float* %ref.tmp, align 4
  %488 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4
  %m_separating_normal1624 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %488, i32 0, i32 2
  %489 = bitcast %class.btVector4* %m_separating_normal1624 to %class.btVector3*
  %call1625 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %489, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  br label %if.end1626

if.end1626:                                       ; preds = %if.end1619, %if.end1603
  %490 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %contacts.addr, align 4
  %m_point_count1627 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %490, i32 0, i32 1
  %491 = load i32, i32* %m_point_count1627, align 4
  %cmp1628 = icmp eq i32 %491, 0
  br i1 %cmp1628, label %if.then1629, label %if.end1630

if.then1629:                                      ; preds = %if.end1626
  store i1 false, i1* %retval, align 1
  br label %return

if.end1630:                                       ; preds = %if.end1626
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end1630, %if.then1629, %if.then1618, %if.then1602, %if.end1568, %if.then1567, %if.then1299, %if.then1108, %if.then519, %if.then328
  %492 = load i1, i1* %retval, align 1
  ret i1 %492
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector4* @_ZN9btVector4C2Ev(%class.btVector4* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector4*, align 4
  store %class.btVector4* %this, %class.btVector4** %this.addr, align 4
  %this1 = load %class.btVector4*, %class.btVector4** %this.addr, align 4
  %0 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %0)
  ret %class.btVector4* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZN30GIM_TRIANGLE_CALCULATION_CACHE13clip_triangleERK9btVector4PK9btVector3S5_PS3_(%class.GIM_TRIANGLE_CALCULATION_CACHE* %this, %class.btVector4* nonnull align 4 dereferenceable(16) %tri_plane, %class.btVector3* %tripoints, %class.btVector3* %srcpoints, %class.btVector3* %clip_points) #2 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.GIM_TRIANGLE_CALCULATION_CACHE*, align 4
  %tri_plane.addr = alloca %class.btVector4*, align 4
  %tripoints.addr = alloca %class.btVector3*, align 4
  %srcpoints.addr = alloca %class.btVector3*, align 4
  %clip_points.addr = alloca %class.btVector3*, align 4
  %edgeplane = alloca %class.btVector4, align 4
  %_dif = alloca [3 x float], align 4
  %len = alloca float, align 4
  %_pp = alloca float, align 4
  %_x = alloca float, align 4
  %_y = alloca i32, align 4
  %clipped_count = alloca i32, align 4
  %_dif119 = alloca [3 x float], align 4
  %len177 = alloca float, align 4
  %_pp178 = alloca float, align 4
  %_x199 = alloca float, align 4
  %_y201 = alloca i32, align 4
  %_dif250 = alloca [3 x float], align 4
  %len308 = alloca float, align 4
  %_pp309 = alloca float, align 4
  %_x330 = alloca float, align 4
  %_y332 = alloca i32, align 4
  store %class.GIM_TRIANGLE_CALCULATION_CACHE* %this, %class.GIM_TRIANGLE_CALCULATION_CACHE** %this.addr, align 4
  store %class.btVector4* %tri_plane, %class.btVector4** %tri_plane.addr, align 4
  store %class.btVector3* %tripoints, %class.btVector3** %tripoints.addr, align 4
  store %class.btVector3* %srcpoints, %class.btVector3** %srcpoints.addr, align 4
  store %class.btVector3* %clip_points, %class.btVector3** %clip_points.addr, align 4
  %this1 = load %class.GIM_TRIANGLE_CALCULATION_CACHE*, %class.GIM_TRIANGLE_CALCULATION_CACHE** %this.addr, align 4
  %call = call %class.btVector4* @_ZN9btVector4C2Ev(%class.btVector4* %edgeplane)
  %0 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 1
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 0
  %1 = load float, float* %arrayidx3, align 4
  %2 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx4 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 0
  %3 = load float, float* %arrayidx6, align 4
  %sub = fsub float %1, %3
  %arrayidx7 = getelementptr inbounds [3 x float], [3 x float]* %_dif, i32 0, i32 0
  store float %sub, float* %arrayidx7, align 4
  %4 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx8 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 1
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 1
  %5 = load float, float* %arrayidx10, align 4
  %6 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx11 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0
  %call12 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx11)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 1
  %7 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %5, %7
  %arrayidx15 = getelementptr inbounds [3 x float], [3 x float]* %_dif, i32 0, i32 1
  store float %sub14, float* %arrayidx15, align 4
  %8 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx16 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 1
  %call17 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx16)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 2
  %9 = load float, float* %arrayidx18, align 4
  %10 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx19 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0
  %call20 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx19)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 2
  %11 = load float, float* %arrayidx21, align 4
  %sub22 = fsub float %9, %11
  %arrayidx23 = getelementptr inbounds [3 x float], [3 x float]* %_dif, i32 0, i32 2
  store float %sub22, float* %arrayidx23, align 4
  %arrayidx24 = getelementptr inbounds [3 x float], [3 x float]* %_dif, i32 0, i32 1
  %12 = load float, float* %arrayidx24, align 4
  %13 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4
  %14 = bitcast %class.btVector4* %13 to %class.btVector3*
  %call25 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %14)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 2
  %15 = load float, float* %arrayidx26, align 4
  %mul = fmul float %12, %15
  %arrayidx27 = getelementptr inbounds [3 x float], [3 x float]* %_dif, i32 0, i32 2
  %16 = load float, float* %arrayidx27, align 4
  %17 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4
  %18 = bitcast %class.btVector4* %17 to %class.btVector3*
  %call28 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %18)
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 1
  %19 = load float, float* %arrayidx29, align 4
  %mul30 = fmul float %16, %19
  %sub31 = fsub float %mul, %mul30
  %20 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call32 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %20)
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 0
  store float %sub31, float* %arrayidx33, align 4
  %arrayidx34 = getelementptr inbounds [3 x float], [3 x float]* %_dif, i32 0, i32 2
  %21 = load float, float* %arrayidx34, align 4
  %22 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4
  %23 = bitcast %class.btVector4* %22 to %class.btVector3*
  %call35 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %23)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 0
  %24 = load float, float* %arrayidx36, align 4
  %mul37 = fmul float %21, %24
  %arrayidx38 = getelementptr inbounds [3 x float], [3 x float]* %_dif, i32 0, i32 0
  %25 = load float, float* %arrayidx38, align 4
  %26 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4
  %27 = bitcast %class.btVector4* %26 to %class.btVector3*
  %call39 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %27)
  %arrayidx40 = getelementptr inbounds float, float* %call39, i32 2
  %28 = load float, float* %arrayidx40, align 4
  %mul41 = fmul float %25, %28
  %sub42 = fsub float %mul37, %mul41
  %29 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call43 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %29)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 1
  store float %sub42, float* %arrayidx44, align 4
  %arrayidx45 = getelementptr inbounds [3 x float], [3 x float]* %_dif, i32 0, i32 0
  %30 = load float, float* %arrayidx45, align 4
  %31 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4
  %32 = bitcast %class.btVector4* %31 to %class.btVector3*
  %call46 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %32)
  %arrayidx47 = getelementptr inbounds float, float* %call46, i32 1
  %33 = load float, float* %arrayidx47, align 4
  %mul48 = fmul float %30, %33
  %arrayidx49 = getelementptr inbounds [3 x float], [3 x float]* %_dif, i32 0, i32 1
  %34 = load float, float* %arrayidx49, align 4
  %35 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4
  %36 = bitcast %class.btVector4* %35 to %class.btVector3*
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %36)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 0
  %37 = load float, float* %arrayidx51, align 4
  %mul52 = fmul float %34, %37
  %sub53 = fsub float %mul48, %mul52
  %38 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call54 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %38)
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 2
  store float %sub53, float* %arrayidx55, align 4
  %39 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call56 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %39)
  %arrayidx57 = getelementptr inbounds float, float* %call56, i32 0
  %40 = load float, float* %arrayidx57, align 4
  %41 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call58 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %41)
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 0
  %42 = load float, float* %arrayidx59, align 4
  %mul60 = fmul float %40, %42
  %43 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %43)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 1
  %44 = load float, float* %arrayidx62, align 4
  %45 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call63 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %45)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 1
  %46 = load float, float* %arrayidx64, align 4
  %mul65 = fmul float %44, %46
  %add = fadd float %mul60, %mul65
  %47 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %47)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 2
  %48 = load float, float* %arrayidx67, align 4
  %49 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call68 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %49)
  %arrayidx69 = getelementptr inbounds float, float* %call68, i32 2
  %50 = load float, float* %arrayidx69, align 4
  %mul70 = fmul float %48, %50
  %add71 = fadd float %add, %mul70
  store float %add71, float* %_pp, align 4
  %51 = load float, float* %_pp, align 4
  %cmp = fcmp ole float %51, 0x3E7AD7F2A0000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store float 0x47EFFFFFE0000000, float* %len, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %52 = load float, float* %_pp, align 4
  %mul72 = fmul float %52, 5.000000e-01
  store float %mul72, float* %_x, align 4
  %53 = bitcast float* %_pp to i32*
  %54 = load i32, i32* %53, align 4
  %shr = lshr i32 %54, 1
  %sub73 = sub i32 1597463007, %shr
  store i32 %sub73, i32* %_y, align 4
  %55 = bitcast i32* %_y to float*
  %56 = load float, float* %55, align 4
  store float %56, float* %len, align 4
  %57 = load float, float* %len, align 4
  %58 = load float, float* %_x, align 4
  %59 = load float, float* %len, align 4
  %mul74 = fmul float %58, %59
  %60 = load float, float* %len, align 4
  %mul75 = fmul float %mul74, %60
  %sub76 = fsub float 1.500000e+00, %mul75
  %mul77 = fmul float %57, %sub76
  store float %mul77, float* %len, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %61 = load float, float* %len, align 4
  %cmp78 = fcmp olt float %61, 0x47EFFFFFE0000000
  br i1 %cmp78, label %if.then79, label %if.end89

if.then79:                                        ; preds = %if.end
  %62 = load float, float* %len, align 4
  %63 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %63)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 0
  %64 = load float, float* %arrayidx81, align 4
  %mul82 = fmul float %64, %62
  store float %mul82, float* %arrayidx81, align 4
  %65 = load float, float* %len, align 4
  %66 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %66)
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 1
  %67 = load float, float* %arrayidx84, align 4
  %mul85 = fmul float %67, %65
  store float %mul85, float* %arrayidx84, align 4
  %68 = load float, float* %len, align 4
  %69 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %69)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 2
  %70 = load float, float* %arrayidx87, align 4
  %mul88 = fmul float %70, %68
  store float %mul88, float* %arrayidx87, align 4
  br label %if.end89

if.end89:                                         ; preds = %if.then79, %if.end
  %71 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx90 = getelementptr inbounds %class.btVector3, %class.btVector3* %71, i32 0
  %call91 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx90)
  %arrayidx92 = getelementptr inbounds float, float* %call91, i32 0
  %72 = load float, float* %arrayidx92, align 4
  %73 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call93 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %73)
  %arrayidx94 = getelementptr inbounds float, float* %call93, i32 0
  %74 = load float, float* %arrayidx94, align 4
  %mul95 = fmul float %72, %74
  %75 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx96 = getelementptr inbounds %class.btVector3, %class.btVector3* %75, i32 0
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx96)
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 1
  %76 = load float, float* %arrayidx98, align 4
  %77 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call99 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %77)
  %arrayidx100 = getelementptr inbounds float, float* %call99, i32 1
  %78 = load float, float* %arrayidx100, align 4
  %mul101 = fmul float %76, %78
  %add102 = fadd float %mul95, %mul101
  %79 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx103 = getelementptr inbounds %class.btVector3, %class.btVector3* %79, i32 0
  %call104 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx103)
  %arrayidx105 = getelementptr inbounds float, float* %call104, i32 2
  %80 = load float, float* %arrayidx105, align 4
  %81 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call106 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %81)
  %arrayidx107 = getelementptr inbounds float, float* %call106, i32 2
  %82 = load float, float* %arrayidx107, align 4
  %mul108 = fmul float %80, %82
  %add109 = fadd float %add102, %mul108
  %83 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call110 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %83)
  %arrayidx111 = getelementptr inbounds float, float* %call110, i32 3
  store float %add109, float* %arrayidx111, align 4
  %84 = load %class.btVector3*, %class.btVector3** %srcpoints.addr, align 4
  %arrayidx112 = getelementptr inbounds %class.btVector3, %class.btVector3* %84, i32 0
  %85 = load %class.btVector3*, %class.btVector3** %srcpoints.addr, align 4
  %arrayidx113 = getelementptr inbounds %class.btVector3, %class.btVector3* %85, i32 1
  %86 = load %class.btVector3*, %class.btVector3** %srcpoints.addr, align 4
  %arrayidx114 = getelementptr inbounds %class.btVector3, %class.btVector3* %86, i32 2
  %temp_points = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 15
  %arraydecay = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %temp_points, i32 0, i32 0
  %call115 = call i32 @_Z21PLANE_CLIP_TRIANGLE3DI9btVector39btVector4EjRKT0_RKT_S7_S7_PS5_(%class.btVector4* nonnull align 4 dereferenceable(16) %edgeplane, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx112, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx113, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx114, %class.btVector3* %arraydecay)
  store i32 %call115, i32* %clipped_count, align 4
  %87 = load i32, i32* %clipped_count, align 4
  %cmp116 = icmp eq i32 %87, 0
  br i1 %cmp116, label %if.then117, label %if.end118

if.then117:                                       ; preds = %if.end89
  store i32 0, i32* %retval, align 4
  br label %return

if.end118:                                        ; preds = %if.end89
  %88 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx120 = getelementptr inbounds %class.btVector3, %class.btVector3* %88, i32 2
  %call121 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx120)
  %arrayidx122 = getelementptr inbounds float, float* %call121, i32 0
  %89 = load float, float* %arrayidx122, align 4
  %90 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx123 = getelementptr inbounds %class.btVector3, %class.btVector3* %90, i32 1
  %call124 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx123)
  %arrayidx125 = getelementptr inbounds float, float* %call124, i32 0
  %91 = load float, float* %arrayidx125, align 4
  %sub126 = fsub float %89, %91
  %arrayidx127 = getelementptr inbounds [3 x float], [3 x float]* %_dif119, i32 0, i32 0
  store float %sub126, float* %arrayidx127, align 4
  %92 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx128 = getelementptr inbounds %class.btVector3, %class.btVector3* %92, i32 2
  %call129 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx128)
  %arrayidx130 = getelementptr inbounds float, float* %call129, i32 1
  %93 = load float, float* %arrayidx130, align 4
  %94 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx131 = getelementptr inbounds %class.btVector3, %class.btVector3* %94, i32 1
  %call132 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx131)
  %arrayidx133 = getelementptr inbounds float, float* %call132, i32 1
  %95 = load float, float* %arrayidx133, align 4
  %sub134 = fsub float %93, %95
  %arrayidx135 = getelementptr inbounds [3 x float], [3 x float]* %_dif119, i32 0, i32 1
  store float %sub134, float* %arrayidx135, align 4
  %96 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx136 = getelementptr inbounds %class.btVector3, %class.btVector3* %96, i32 2
  %call137 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx136)
  %arrayidx138 = getelementptr inbounds float, float* %call137, i32 2
  %97 = load float, float* %arrayidx138, align 4
  %98 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx139 = getelementptr inbounds %class.btVector3, %class.btVector3* %98, i32 1
  %call140 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx139)
  %arrayidx141 = getelementptr inbounds float, float* %call140, i32 2
  %99 = load float, float* %arrayidx141, align 4
  %sub142 = fsub float %97, %99
  %arrayidx143 = getelementptr inbounds [3 x float], [3 x float]* %_dif119, i32 0, i32 2
  store float %sub142, float* %arrayidx143, align 4
  %arrayidx144 = getelementptr inbounds [3 x float], [3 x float]* %_dif119, i32 0, i32 1
  %100 = load float, float* %arrayidx144, align 4
  %101 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4
  %102 = bitcast %class.btVector4* %101 to %class.btVector3*
  %call145 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %102)
  %arrayidx146 = getelementptr inbounds float, float* %call145, i32 2
  %103 = load float, float* %arrayidx146, align 4
  %mul147 = fmul float %100, %103
  %arrayidx148 = getelementptr inbounds [3 x float], [3 x float]* %_dif119, i32 0, i32 2
  %104 = load float, float* %arrayidx148, align 4
  %105 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4
  %106 = bitcast %class.btVector4* %105 to %class.btVector3*
  %call149 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %106)
  %arrayidx150 = getelementptr inbounds float, float* %call149, i32 1
  %107 = load float, float* %arrayidx150, align 4
  %mul151 = fmul float %104, %107
  %sub152 = fsub float %mul147, %mul151
  %108 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call153 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %108)
  %arrayidx154 = getelementptr inbounds float, float* %call153, i32 0
  store float %sub152, float* %arrayidx154, align 4
  %arrayidx155 = getelementptr inbounds [3 x float], [3 x float]* %_dif119, i32 0, i32 2
  %109 = load float, float* %arrayidx155, align 4
  %110 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4
  %111 = bitcast %class.btVector4* %110 to %class.btVector3*
  %call156 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %111)
  %arrayidx157 = getelementptr inbounds float, float* %call156, i32 0
  %112 = load float, float* %arrayidx157, align 4
  %mul158 = fmul float %109, %112
  %arrayidx159 = getelementptr inbounds [3 x float], [3 x float]* %_dif119, i32 0, i32 0
  %113 = load float, float* %arrayidx159, align 4
  %114 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4
  %115 = bitcast %class.btVector4* %114 to %class.btVector3*
  %call160 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %115)
  %arrayidx161 = getelementptr inbounds float, float* %call160, i32 2
  %116 = load float, float* %arrayidx161, align 4
  %mul162 = fmul float %113, %116
  %sub163 = fsub float %mul158, %mul162
  %117 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call164 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %117)
  %arrayidx165 = getelementptr inbounds float, float* %call164, i32 1
  store float %sub163, float* %arrayidx165, align 4
  %arrayidx166 = getelementptr inbounds [3 x float], [3 x float]* %_dif119, i32 0, i32 0
  %118 = load float, float* %arrayidx166, align 4
  %119 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4
  %120 = bitcast %class.btVector4* %119 to %class.btVector3*
  %call167 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %120)
  %arrayidx168 = getelementptr inbounds float, float* %call167, i32 1
  %121 = load float, float* %arrayidx168, align 4
  %mul169 = fmul float %118, %121
  %arrayidx170 = getelementptr inbounds [3 x float], [3 x float]* %_dif119, i32 0, i32 1
  %122 = load float, float* %arrayidx170, align 4
  %123 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4
  %124 = bitcast %class.btVector4* %123 to %class.btVector3*
  %call171 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %124)
  %arrayidx172 = getelementptr inbounds float, float* %call171, i32 0
  %125 = load float, float* %arrayidx172, align 4
  %mul173 = fmul float %122, %125
  %sub174 = fsub float %mul169, %mul173
  %126 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call175 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %126)
  %arrayidx176 = getelementptr inbounds float, float* %call175, i32 2
  store float %sub174, float* %arrayidx176, align 4
  %127 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call179 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %127)
  %arrayidx180 = getelementptr inbounds float, float* %call179, i32 0
  %128 = load float, float* %arrayidx180, align 4
  %129 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call181 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %129)
  %arrayidx182 = getelementptr inbounds float, float* %call181, i32 0
  %130 = load float, float* %arrayidx182, align 4
  %mul183 = fmul float %128, %130
  %131 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call184 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %131)
  %arrayidx185 = getelementptr inbounds float, float* %call184, i32 1
  %132 = load float, float* %arrayidx185, align 4
  %133 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call186 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %133)
  %arrayidx187 = getelementptr inbounds float, float* %call186, i32 1
  %134 = load float, float* %arrayidx187, align 4
  %mul188 = fmul float %132, %134
  %add189 = fadd float %mul183, %mul188
  %135 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call190 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %135)
  %arrayidx191 = getelementptr inbounds float, float* %call190, i32 2
  %136 = load float, float* %arrayidx191, align 4
  %137 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call192 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %137)
  %arrayidx193 = getelementptr inbounds float, float* %call192, i32 2
  %138 = load float, float* %arrayidx193, align 4
  %mul194 = fmul float %136, %138
  %add195 = fadd float %add189, %mul194
  store float %add195, float* %_pp178, align 4
  %139 = load float, float* %_pp178, align 4
  %cmp196 = fcmp ole float %139, 0x3E7AD7F2A0000000
  br i1 %cmp196, label %if.then197, label %if.else198

if.then197:                                       ; preds = %if.end118
  store float 0x47EFFFFFE0000000, float* %len177, align 4
  br label %if.end208

if.else198:                                       ; preds = %if.end118
  %140 = load float, float* %_pp178, align 4
  %mul200 = fmul float %140, 5.000000e-01
  store float %mul200, float* %_x199, align 4
  %141 = bitcast float* %_pp178 to i32*
  %142 = load i32, i32* %141, align 4
  %shr202 = lshr i32 %142, 1
  %sub203 = sub i32 1597463007, %shr202
  store i32 %sub203, i32* %_y201, align 4
  %143 = bitcast i32* %_y201 to float*
  %144 = load float, float* %143, align 4
  store float %144, float* %len177, align 4
  %145 = load float, float* %len177, align 4
  %146 = load float, float* %_x199, align 4
  %147 = load float, float* %len177, align 4
  %mul204 = fmul float %146, %147
  %148 = load float, float* %len177, align 4
  %mul205 = fmul float %mul204, %148
  %sub206 = fsub float 1.500000e+00, %mul205
  %mul207 = fmul float %145, %sub206
  store float %mul207, float* %len177, align 4
  br label %if.end208

if.end208:                                        ; preds = %if.else198, %if.then197
  %149 = load float, float* %len177, align 4
  %cmp209 = fcmp olt float %149, 0x47EFFFFFE0000000
  br i1 %cmp209, label %if.then210, label %if.end220

if.then210:                                       ; preds = %if.end208
  %150 = load float, float* %len177, align 4
  %151 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call211 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %151)
  %arrayidx212 = getelementptr inbounds float, float* %call211, i32 0
  %152 = load float, float* %arrayidx212, align 4
  %mul213 = fmul float %152, %150
  store float %mul213, float* %arrayidx212, align 4
  %153 = load float, float* %len177, align 4
  %154 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call214 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %154)
  %arrayidx215 = getelementptr inbounds float, float* %call214, i32 1
  %155 = load float, float* %arrayidx215, align 4
  %mul216 = fmul float %155, %153
  store float %mul216, float* %arrayidx215, align 4
  %156 = load float, float* %len177, align 4
  %157 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call217 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %157)
  %arrayidx218 = getelementptr inbounds float, float* %call217, i32 2
  %158 = load float, float* %arrayidx218, align 4
  %mul219 = fmul float %158, %156
  store float %mul219, float* %arrayidx218, align 4
  br label %if.end220

if.end220:                                        ; preds = %if.then210, %if.end208
  %159 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx221 = getelementptr inbounds %class.btVector3, %class.btVector3* %159, i32 1
  %call222 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx221)
  %arrayidx223 = getelementptr inbounds float, float* %call222, i32 0
  %160 = load float, float* %arrayidx223, align 4
  %161 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call224 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %161)
  %arrayidx225 = getelementptr inbounds float, float* %call224, i32 0
  %162 = load float, float* %arrayidx225, align 4
  %mul226 = fmul float %160, %162
  %163 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx227 = getelementptr inbounds %class.btVector3, %class.btVector3* %163, i32 1
  %call228 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx227)
  %arrayidx229 = getelementptr inbounds float, float* %call228, i32 1
  %164 = load float, float* %arrayidx229, align 4
  %165 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call230 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %165)
  %arrayidx231 = getelementptr inbounds float, float* %call230, i32 1
  %166 = load float, float* %arrayidx231, align 4
  %mul232 = fmul float %164, %166
  %add233 = fadd float %mul226, %mul232
  %167 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx234 = getelementptr inbounds %class.btVector3, %class.btVector3* %167, i32 1
  %call235 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx234)
  %arrayidx236 = getelementptr inbounds float, float* %call235, i32 2
  %168 = load float, float* %arrayidx236, align 4
  %169 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call237 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %169)
  %arrayidx238 = getelementptr inbounds float, float* %call237, i32 2
  %170 = load float, float* %arrayidx238, align 4
  %mul239 = fmul float %168, %170
  %add240 = fadd float %add233, %mul239
  %171 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call241 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %171)
  %arrayidx242 = getelementptr inbounds float, float* %call241, i32 3
  store float %add240, float* %arrayidx242, align 4
  %temp_points243 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 15
  %arraydecay244 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %temp_points243, i32 0, i32 0
  %172 = load i32, i32* %clipped_count, align 4
  %temp_points1 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 16
  %arraydecay245 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %temp_points1, i32 0, i32 0
  %call246 = call i32 @_Z20PLANE_CLIP_POLYGON3DI9btVector39btVector4EjRKT0_PKT_jPS5_(%class.btVector4* nonnull align 4 dereferenceable(16) %edgeplane, %class.btVector3* %arraydecay244, i32 %172, %class.btVector3* %arraydecay245)
  store i32 %call246, i32* %clipped_count, align 4
  %173 = load i32, i32* %clipped_count, align 4
  %cmp247 = icmp eq i32 %173, 0
  br i1 %cmp247, label %if.then248, label %if.end249

if.then248:                                       ; preds = %if.end220
  store i32 0, i32* %retval, align 4
  br label %return

if.end249:                                        ; preds = %if.end220
  %174 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx251 = getelementptr inbounds %class.btVector3, %class.btVector3* %174, i32 0
  %call252 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx251)
  %arrayidx253 = getelementptr inbounds float, float* %call252, i32 0
  %175 = load float, float* %arrayidx253, align 4
  %176 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx254 = getelementptr inbounds %class.btVector3, %class.btVector3* %176, i32 2
  %call255 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx254)
  %arrayidx256 = getelementptr inbounds float, float* %call255, i32 0
  %177 = load float, float* %arrayidx256, align 4
  %sub257 = fsub float %175, %177
  %arrayidx258 = getelementptr inbounds [3 x float], [3 x float]* %_dif250, i32 0, i32 0
  store float %sub257, float* %arrayidx258, align 4
  %178 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx259 = getelementptr inbounds %class.btVector3, %class.btVector3* %178, i32 0
  %call260 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx259)
  %arrayidx261 = getelementptr inbounds float, float* %call260, i32 1
  %179 = load float, float* %arrayidx261, align 4
  %180 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx262 = getelementptr inbounds %class.btVector3, %class.btVector3* %180, i32 2
  %call263 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx262)
  %arrayidx264 = getelementptr inbounds float, float* %call263, i32 1
  %181 = load float, float* %arrayidx264, align 4
  %sub265 = fsub float %179, %181
  %arrayidx266 = getelementptr inbounds [3 x float], [3 x float]* %_dif250, i32 0, i32 1
  store float %sub265, float* %arrayidx266, align 4
  %182 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx267 = getelementptr inbounds %class.btVector3, %class.btVector3* %182, i32 0
  %call268 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx267)
  %arrayidx269 = getelementptr inbounds float, float* %call268, i32 2
  %183 = load float, float* %arrayidx269, align 4
  %184 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx270 = getelementptr inbounds %class.btVector3, %class.btVector3* %184, i32 2
  %call271 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx270)
  %arrayidx272 = getelementptr inbounds float, float* %call271, i32 2
  %185 = load float, float* %arrayidx272, align 4
  %sub273 = fsub float %183, %185
  %arrayidx274 = getelementptr inbounds [3 x float], [3 x float]* %_dif250, i32 0, i32 2
  store float %sub273, float* %arrayidx274, align 4
  %arrayidx275 = getelementptr inbounds [3 x float], [3 x float]* %_dif250, i32 0, i32 1
  %186 = load float, float* %arrayidx275, align 4
  %187 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4
  %188 = bitcast %class.btVector4* %187 to %class.btVector3*
  %call276 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %188)
  %arrayidx277 = getelementptr inbounds float, float* %call276, i32 2
  %189 = load float, float* %arrayidx277, align 4
  %mul278 = fmul float %186, %189
  %arrayidx279 = getelementptr inbounds [3 x float], [3 x float]* %_dif250, i32 0, i32 2
  %190 = load float, float* %arrayidx279, align 4
  %191 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4
  %192 = bitcast %class.btVector4* %191 to %class.btVector3*
  %call280 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %192)
  %arrayidx281 = getelementptr inbounds float, float* %call280, i32 1
  %193 = load float, float* %arrayidx281, align 4
  %mul282 = fmul float %190, %193
  %sub283 = fsub float %mul278, %mul282
  %194 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call284 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %194)
  %arrayidx285 = getelementptr inbounds float, float* %call284, i32 0
  store float %sub283, float* %arrayidx285, align 4
  %arrayidx286 = getelementptr inbounds [3 x float], [3 x float]* %_dif250, i32 0, i32 2
  %195 = load float, float* %arrayidx286, align 4
  %196 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4
  %197 = bitcast %class.btVector4* %196 to %class.btVector3*
  %call287 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %197)
  %arrayidx288 = getelementptr inbounds float, float* %call287, i32 0
  %198 = load float, float* %arrayidx288, align 4
  %mul289 = fmul float %195, %198
  %arrayidx290 = getelementptr inbounds [3 x float], [3 x float]* %_dif250, i32 0, i32 0
  %199 = load float, float* %arrayidx290, align 4
  %200 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4
  %201 = bitcast %class.btVector4* %200 to %class.btVector3*
  %call291 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %201)
  %arrayidx292 = getelementptr inbounds float, float* %call291, i32 2
  %202 = load float, float* %arrayidx292, align 4
  %mul293 = fmul float %199, %202
  %sub294 = fsub float %mul289, %mul293
  %203 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call295 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %203)
  %arrayidx296 = getelementptr inbounds float, float* %call295, i32 1
  store float %sub294, float* %arrayidx296, align 4
  %arrayidx297 = getelementptr inbounds [3 x float], [3 x float]* %_dif250, i32 0, i32 0
  %204 = load float, float* %arrayidx297, align 4
  %205 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4
  %206 = bitcast %class.btVector4* %205 to %class.btVector3*
  %call298 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %206)
  %arrayidx299 = getelementptr inbounds float, float* %call298, i32 1
  %207 = load float, float* %arrayidx299, align 4
  %mul300 = fmul float %204, %207
  %arrayidx301 = getelementptr inbounds [3 x float], [3 x float]* %_dif250, i32 0, i32 1
  %208 = load float, float* %arrayidx301, align 4
  %209 = load %class.btVector4*, %class.btVector4** %tri_plane.addr, align 4
  %210 = bitcast %class.btVector4* %209 to %class.btVector3*
  %call302 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %210)
  %arrayidx303 = getelementptr inbounds float, float* %call302, i32 0
  %211 = load float, float* %arrayidx303, align 4
  %mul304 = fmul float %208, %211
  %sub305 = fsub float %mul300, %mul304
  %212 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call306 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %212)
  %arrayidx307 = getelementptr inbounds float, float* %call306, i32 2
  store float %sub305, float* %arrayidx307, align 4
  %213 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call310 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %213)
  %arrayidx311 = getelementptr inbounds float, float* %call310, i32 0
  %214 = load float, float* %arrayidx311, align 4
  %215 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call312 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %215)
  %arrayidx313 = getelementptr inbounds float, float* %call312, i32 0
  %216 = load float, float* %arrayidx313, align 4
  %mul314 = fmul float %214, %216
  %217 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call315 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %217)
  %arrayidx316 = getelementptr inbounds float, float* %call315, i32 1
  %218 = load float, float* %arrayidx316, align 4
  %219 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call317 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %219)
  %arrayidx318 = getelementptr inbounds float, float* %call317, i32 1
  %220 = load float, float* %arrayidx318, align 4
  %mul319 = fmul float %218, %220
  %add320 = fadd float %mul314, %mul319
  %221 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call321 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %221)
  %arrayidx322 = getelementptr inbounds float, float* %call321, i32 2
  %222 = load float, float* %arrayidx322, align 4
  %223 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call323 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %223)
  %arrayidx324 = getelementptr inbounds float, float* %call323, i32 2
  %224 = load float, float* %arrayidx324, align 4
  %mul325 = fmul float %222, %224
  %add326 = fadd float %add320, %mul325
  store float %add326, float* %_pp309, align 4
  %225 = load float, float* %_pp309, align 4
  %cmp327 = fcmp ole float %225, 0x3E7AD7F2A0000000
  br i1 %cmp327, label %if.then328, label %if.else329

if.then328:                                       ; preds = %if.end249
  store float 0x47EFFFFFE0000000, float* %len308, align 4
  br label %if.end339

if.else329:                                       ; preds = %if.end249
  %226 = load float, float* %_pp309, align 4
  %mul331 = fmul float %226, 5.000000e-01
  store float %mul331, float* %_x330, align 4
  %227 = bitcast float* %_pp309 to i32*
  %228 = load i32, i32* %227, align 4
  %shr333 = lshr i32 %228, 1
  %sub334 = sub i32 1597463007, %shr333
  store i32 %sub334, i32* %_y332, align 4
  %229 = bitcast i32* %_y332 to float*
  %230 = load float, float* %229, align 4
  store float %230, float* %len308, align 4
  %231 = load float, float* %len308, align 4
  %232 = load float, float* %_x330, align 4
  %233 = load float, float* %len308, align 4
  %mul335 = fmul float %232, %233
  %234 = load float, float* %len308, align 4
  %mul336 = fmul float %mul335, %234
  %sub337 = fsub float 1.500000e+00, %mul336
  %mul338 = fmul float %231, %sub337
  store float %mul338, float* %len308, align 4
  br label %if.end339

if.end339:                                        ; preds = %if.else329, %if.then328
  %235 = load float, float* %len308, align 4
  %cmp340 = fcmp olt float %235, 0x47EFFFFFE0000000
  br i1 %cmp340, label %if.then341, label %if.end351

if.then341:                                       ; preds = %if.end339
  %236 = load float, float* %len308, align 4
  %237 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call342 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %237)
  %arrayidx343 = getelementptr inbounds float, float* %call342, i32 0
  %238 = load float, float* %arrayidx343, align 4
  %mul344 = fmul float %238, %236
  store float %mul344, float* %arrayidx343, align 4
  %239 = load float, float* %len308, align 4
  %240 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call345 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %240)
  %arrayidx346 = getelementptr inbounds float, float* %call345, i32 1
  %241 = load float, float* %arrayidx346, align 4
  %mul347 = fmul float %241, %239
  store float %mul347, float* %arrayidx346, align 4
  %242 = load float, float* %len308, align 4
  %243 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call348 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %243)
  %arrayidx349 = getelementptr inbounds float, float* %call348, i32 2
  %244 = load float, float* %arrayidx349, align 4
  %mul350 = fmul float %244, %242
  store float %mul350, float* %arrayidx349, align 4
  br label %if.end351

if.end351:                                        ; preds = %if.then341, %if.end339
  %245 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx352 = getelementptr inbounds %class.btVector3, %class.btVector3* %245, i32 2
  %call353 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx352)
  %arrayidx354 = getelementptr inbounds float, float* %call353, i32 0
  %246 = load float, float* %arrayidx354, align 4
  %247 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call355 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %247)
  %arrayidx356 = getelementptr inbounds float, float* %call355, i32 0
  %248 = load float, float* %arrayidx356, align 4
  %mul357 = fmul float %246, %248
  %249 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx358 = getelementptr inbounds %class.btVector3, %class.btVector3* %249, i32 2
  %call359 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx358)
  %arrayidx360 = getelementptr inbounds float, float* %call359, i32 1
  %250 = load float, float* %arrayidx360, align 4
  %251 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call361 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %251)
  %arrayidx362 = getelementptr inbounds float, float* %call361, i32 1
  %252 = load float, float* %arrayidx362, align 4
  %mul363 = fmul float %250, %252
  %add364 = fadd float %mul357, %mul363
  %253 = load %class.btVector3*, %class.btVector3** %tripoints.addr, align 4
  %arrayidx365 = getelementptr inbounds %class.btVector3, %class.btVector3* %253, i32 2
  %call366 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx365)
  %arrayidx367 = getelementptr inbounds float, float* %call366, i32 2
  %254 = load float, float* %arrayidx367, align 4
  %255 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call368 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %255)
  %arrayidx369 = getelementptr inbounds float, float* %call368, i32 2
  %256 = load float, float* %arrayidx369, align 4
  %mul370 = fmul float %254, %256
  %add371 = fadd float %add364, %mul370
  %257 = bitcast %class.btVector4* %edgeplane to %class.btVector3*
  %call372 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %257)
  %arrayidx373 = getelementptr inbounds float, float* %call372, i32 3
  store float %add371, float* %arrayidx373, align 4
  %temp_points1374 = getelementptr inbounds %class.GIM_TRIANGLE_CALCULATION_CACHE, %class.GIM_TRIANGLE_CALCULATION_CACHE* %this1, i32 0, i32 16
  %arraydecay375 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %temp_points1374, i32 0, i32 0
  %258 = load i32, i32* %clipped_count, align 4
  %259 = load %class.btVector3*, %class.btVector3** %clip_points.addr, align 4
  %call376 = call i32 @_Z20PLANE_CLIP_POLYGON3DI9btVector39btVector4EjRKT0_PKT_jPS5_(%class.btVector4* nonnull align 4 dereferenceable(16) %edgeplane, %class.btVector3* %arraydecay375, i32 %258, %class.btVector3* %259)
  store i32 %call376, i32* %clipped_count, align 4
  %260 = load i32, i32* %clipped_count, align 4
  store i32 %260, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end351, %if.then248, %if.then117
  %261 = load i32, i32* %retval, align 4
  ret i32 %261
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN25GIM_TRIANGLE_CONTACT_DATA12merge_pointsERK9btVector4fPK9btVector3j(%struct.GIM_TRIANGLE_CONTACT_DATA* %this, %class.btVector4* nonnull align 4 dereferenceable(16) %plane, float %margin, %class.btVector3* %points, i32 %point_count) #2 comdat {
entry:
  %this.addr = alloca %struct.GIM_TRIANGLE_CONTACT_DATA*, align 4
  %plane.addr = alloca %class.btVector4*, align 4
  %margin.addr = alloca float, align 4
  %points.addr = alloca %class.btVector3*, align 4
  %point_count.addr = alloca i32, align 4
  %agg.tmp = alloca %class.DISTANCE_PLANE_3D_FUNC, align 1
  store %struct.GIM_TRIANGLE_CONTACT_DATA* %this, %struct.GIM_TRIANGLE_CONTACT_DATA** %this.addr, align 4
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4
  store float %margin, float* %margin.addr, align 4
  store %class.btVector3* %points, %class.btVector3** %points.addr, align 4
  store i32 %point_count, i32* %point_count.addr, align 4
  %this1 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %this.addr, align 4
  %0 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %m_separating_normal = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 2
  %1 = bitcast %class.btVector4* %m_separating_normal to i8*
  %2 = bitcast %class.btVector4* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %4 = load float, float* %margin.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %points.addr, align 4
  %6 = load i32, i32* %point_count.addr, align 4
  call void @_ZN25GIM_TRIANGLE_CONTACT_DATA19mergepoints_genericI22DISTANCE_PLANE_3D_FUNC9btVector4EEvRKT0_fPK9btVector3jT_(%struct.GIM_TRIANGLE_CONTACT_DATA* %this1, %class.btVector4* nonnull align 4 dereferenceable(16) %3, float %4, %class.btVector3* %5, i32 %6)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_Z21PLANE_CLIP_TRIANGLE3DI9btVector39btVector4EjRKT0_RKT_S7_S7_PS5_(%class.btVector4* nonnull align 4 dereferenceable(16) %plane, %class.btVector3* nonnull align 4 dereferenceable(16) %point0, %class.btVector3* nonnull align 4 dereferenceable(16) %point1, %class.btVector3* nonnull align 4 dereferenceable(16) %point2, %class.btVector3* %clipped) #2 comdat {
entry:
  %plane.addr = alloca %class.btVector4*, align 4
  %point0.addr = alloca %class.btVector3*, align 4
  %point1.addr = alloca %class.btVector3*, align 4
  %point2.addr = alloca %class.btVector3*, align 4
  %clipped.addr = alloca %class.btVector3*, align 4
  %agg.tmp = alloca %class.DISTANCE_PLANE_3D_FUNC, align 1
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4
  store %class.btVector3* %point0, %class.btVector3** %point0.addr, align 4
  store %class.btVector3* %point1, %class.btVector3** %point1.addr, align 4
  store %class.btVector3* %point2, %class.btVector3** %point2.addr, align 4
  store %class.btVector3* %clipped, %class.btVector3** %clipped.addr, align 4
  %0 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %point2.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  %call = call i32 @_Z27PLANE_CLIP_TRIANGLE_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_RKT_S8_S8_PS6_T1_(%class.btVector4* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* %4)
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_Z20PLANE_CLIP_POLYGON3DI9btVector39btVector4EjRKT0_PKT_jPS5_(%class.btVector4* nonnull align 4 dereferenceable(16) %plane, %class.btVector3* %polygon_points, i32 %polygon_point_count, %class.btVector3* %clipped) #2 comdat {
entry:
  %plane.addr = alloca %class.btVector4*, align 4
  %polygon_points.addr = alloca %class.btVector3*, align 4
  %polygon_point_count.addr = alloca i32, align 4
  %clipped.addr = alloca %class.btVector3*, align 4
  %agg.tmp = alloca %class.DISTANCE_PLANE_3D_FUNC, align 1
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4
  store %class.btVector3* %polygon_points, %class.btVector3** %polygon_points.addr, align 4
  store i32 %polygon_point_count, i32* %polygon_point_count.addr, align 4
  store %class.btVector3* %clipped, %class.btVector3** %clipped.addr, align 4
  %0 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4
  %2 = load i32, i32* %polygon_point_count.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  %call = call i32 @_Z26PLANE_CLIP_POLYGON_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_PKT_jPS6_T1_(%class.btVector4* nonnull align 4 dereferenceable(16) %0, %class.btVector3* %1, i32 %2, %class.btVector3* %3)
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_Z27PLANE_CLIP_TRIANGLE_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_RKT_S8_S8_PS6_T1_(%class.btVector4* nonnull align 4 dereferenceable(16) %plane, %class.btVector3* nonnull align 4 dereferenceable(16) %point0, %class.btVector3* nonnull align 4 dereferenceable(16) %point1, %class.btVector3* nonnull align 4 dereferenceable(16) %point2, %class.btVector3* %clipped) #2 comdat {
entry:
  %distance_func = alloca %class.DISTANCE_PLANE_3D_FUNC, align 1
  %plane.addr = alloca %class.btVector4*, align 4
  %point0.addr = alloca %class.btVector3*, align 4
  %point1.addr = alloca %class.btVector3*, align 4
  %point2.addr = alloca %class.btVector3*, align 4
  %clipped.addr = alloca %class.btVector3*, align 4
  %clipped_count = alloca i32, align 4
  %firstdist = alloca float, align 4
  %olddist = alloca float, align 4
  %dist = alloca float, align 4
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4
  store %class.btVector3* %point0, %class.btVector3** %point0.addr, align 4
  store %class.btVector3* %point1, %class.btVector3** %point1.addr, align 4
  store %class.btVector3* %point2, %class.btVector3** %point2.addr, align 4
  store %class.btVector3* %clipped, %class.btVector3** %clipped.addr, align 4
  store i32 0, i32* %clipped_count, align 4
  %0 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4
  %call = call float @_ZN22DISTANCE_PLANE_3D_FUNCclI9btVector39btVector4EEfRKT0_RKT_(%class.DISTANCE_PLANE_3D_FUNC* %distance_func, %class.btVector4* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %firstdist, align 4
  %2 = load float, float* %firstdist, align 4
  %cmp = fcmp ogt float %2, 0x3E80000000000000
  br i1 %cmp, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %3 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4
  %call1 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %3)
  %arrayidx = getelementptr inbounds float, float* %call1, i32 0
  %4 = load float, float* %arrayidx, align 4
  %5 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  %6 = load i32, i32* %clipped_count, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  %call3 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 0
  store float %4, float* %arrayidx4, align 4
  %7 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %7)
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 1
  %8 = load float, float* %arrayidx6, align 4
  %9 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  %10 = load i32, i32* %clipped_count, align 4
  %arrayidx7 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 %10
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx7)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 1
  store float %8, float* %arrayidx9, align 4
  %11 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4
  %call10 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %11)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 2
  %12 = load float, float* %arrayidx11, align 4
  %13 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  %14 = load i32, i32* %clipped_count, align 4
  %arrayidx12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 %14
  %call13 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx12)
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 2
  store float %12, float* %arrayidx14, align 4
  %15 = load i32, i32* %clipped_count, align 4
  %inc = add i32 %15, 1
  store i32 %inc, i32* %clipped_count, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %16 = load float, float* %firstdist, align 4
  store float %16, float* %olddist, align 4
  %17 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %18 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4
  %call15 = call float @_ZN22DISTANCE_PLANE_3D_FUNCclI9btVector39btVector4EEfRKT0_RKT_(%class.DISTANCE_PLANE_3D_FUNC* %distance_func, %class.btVector4* nonnull align 4 dereferenceable(16) %17, %class.btVector3* nonnull align 4 dereferenceable(16) %18)
  store float %call15, float* %dist, align 4
  %19 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4
  %20 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4
  %21 = load float, float* %olddist, align 4
  %22 = load float, float* %dist, align 4
  %23 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  call void @_Z26PLANE_CLIP_POLYGON_COLLECTI9btVector3EvRKT_S3_ffPS1_Rj(%class.btVector3* nonnull align 4 dereferenceable(16) %19, %class.btVector3* nonnull align 4 dereferenceable(16) %20, float %21, float %22, %class.btVector3* %23, i32* nonnull align 4 dereferenceable(4) %clipped_count)
  %24 = load float, float* %dist, align 4
  store float %24, float* %olddist, align 4
  %25 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %26 = load %class.btVector3*, %class.btVector3** %point2.addr, align 4
  %call16 = call float @_ZN22DISTANCE_PLANE_3D_FUNCclI9btVector39btVector4EEfRKT0_RKT_(%class.DISTANCE_PLANE_3D_FUNC* %distance_func, %class.btVector4* nonnull align 4 dereferenceable(16) %25, %class.btVector3* nonnull align 4 dereferenceable(16) %26)
  store float %call16, float* %dist, align 4
  %27 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4
  %28 = load %class.btVector3*, %class.btVector3** %point2.addr, align 4
  %29 = load float, float* %olddist, align 4
  %30 = load float, float* %dist, align 4
  %31 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  call void @_Z26PLANE_CLIP_POLYGON_COLLECTI9btVector3EvRKT_S3_ffPS1_Rj(%class.btVector3* nonnull align 4 dereferenceable(16) %27, %class.btVector3* nonnull align 4 dereferenceable(16) %28, float %29, float %30, %class.btVector3* %31, i32* nonnull align 4 dereferenceable(4) %clipped_count)
  %32 = load float, float* %dist, align 4
  store float %32, float* %olddist, align 4
  %33 = load %class.btVector3*, %class.btVector3** %point2.addr, align 4
  %34 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4
  %35 = load float, float* %olddist, align 4
  %36 = load float, float* %firstdist, align 4
  %37 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  call void @_Z26PLANE_CLIP_POLYGON_COLLECTI9btVector3EvRKT_S3_ffPS1_Rj(%class.btVector3* nonnull align 4 dereferenceable(16) %33, %class.btVector3* nonnull align 4 dereferenceable(16) %34, float %35, float %36, %class.btVector3* %37, i32* nonnull align 4 dereferenceable(4) %clipped_count)
  %38 = load i32, i32* %clipped_count, align 4
  ret i32 %38
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN22DISTANCE_PLANE_3D_FUNCclI9btVector39btVector4EEfRKT0_RKT_(%class.DISTANCE_PLANE_3D_FUNC* %this, %class.btVector4* nonnull align 4 dereferenceable(16) %plane, %class.btVector3* nonnull align 4 dereferenceable(16) %point) #1 comdat {
entry:
  %this.addr = alloca %class.DISTANCE_PLANE_3D_FUNC*, align 4
  %plane.addr = alloca %class.btVector4*, align 4
  %point.addr = alloca %class.btVector3*, align 4
  store %class.DISTANCE_PLANE_3D_FUNC* %this, %class.DISTANCE_PLANE_3D_FUNC** %this.addr, align 4
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4
  %this1 = load %class.DISTANCE_PLANE_3D_FUNC*, %class.DISTANCE_PLANE_3D_FUNC** %this.addr, align 4
  %0 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %1 = bitcast %class.btVector4* %0 to %class.btVector3*
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %1)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %2 = load float, float* %arrayidx, align 4
  %3 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %3)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 0
  %4 = load float, float* %arrayidx3, align 4
  %mul = fmul float %2, %4
  %5 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %6 = bitcast %class.btVector4* %5 to %class.btVector3*
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %7 = load float, float* %arrayidx5, align 4
  %8 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 1
  %9 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %7, %9
  %add = fadd float %mul, %mul8
  %10 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %11 = bitcast %class.btVector4* %10 to %class.btVector3*
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %11)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 2
  %12 = load float, float* %arrayidx10, align 4
  %13 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %call11 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %13)
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 2
  %14 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %12, %14
  %add14 = fadd float %add, %mul13
  %15 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %16 = bitcast %class.btVector4* %15 to %class.btVector3*
  %call15 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %16)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 3
  %17 = load float, float* %arrayidx16, align 4
  %sub = fsub float %add14, %17
  ret float %sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z26PLANE_CLIP_POLYGON_COLLECTI9btVector3EvRKT_S3_ffPS1_Rj(%class.btVector3* nonnull align 4 dereferenceable(16) %point0, %class.btVector3* nonnull align 4 dereferenceable(16) %point1, float %dist0, float %dist1, %class.btVector3* %clipped, i32* nonnull align 4 dereferenceable(4) %clipped_count) #1 comdat {
entry:
  %point0.addr = alloca %class.btVector3*, align 4
  %point1.addr = alloca %class.btVector3*, align 4
  %dist0.addr = alloca float, align 4
  %dist1.addr = alloca float, align 4
  %clipped.addr = alloca %class.btVector3*, align 4
  %clipped_count.addr = alloca i32*, align 4
  %_prevclassif = alloca i32, align 4
  %_classif = alloca i32, align 4
  %blendfactor = alloca float, align 4
  store %class.btVector3* %point0, %class.btVector3** %point0.addr, align 4
  store %class.btVector3* %point1, %class.btVector3** %point1.addr, align 4
  store float %dist0, float* %dist0.addr, align 4
  store float %dist1, float* %dist1.addr, align 4
  store %class.btVector3* %clipped, %class.btVector3** %clipped.addr, align 4
  store i32* %clipped_count, i32** %clipped_count.addr, align 4
  %0 = load float, float* %dist0.addr, align 4
  %cmp = fcmp ogt float %0, 0x3E80000000000000
  %conv = zext i1 %cmp to i32
  store i32 %conv, i32* %_prevclassif, align 4
  %1 = load float, float* %dist1.addr, align 4
  %cmp1 = fcmp ogt float %1, 0x3E80000000000000
  %conv2 = zext i1 %cmp1 to i32
  store i32 %conv2, i32* %_classif, align 4
  %2 = load i32, i32* %_classif, align 4
  %3 = load i32, i32* %_prevclassif, align 4
  %cmp3 = icmp ne i32 %2, %3
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float, float* %dist0.addr, align 4
  %fneg = fneg float %4
  %5 = load float, float* %dist1.addr, align 4
  %6 = load float, float* %dist0.addr, align 4
  %sub = fsub float %5, %6
  %div = fdiv float %fneg, %sub
  store float %div, float* %blendfactor, align 4
  %7 = load float, float* %blendfactor, align 4
  %sub4 = fsub float 1.000000e+00, %7
  %8 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %9 = load float, float* %arrayidx, align 4
  %mul = fmul float %sub4, %9
  %10 = load float, float* %blendfactor, align 4
  %11 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %11)
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 0
  %12 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %10, %12
  %add = fadd float %mul, %mul7
  %13 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  %14 = load i32*, i32** %clipped_count.addr, align 4
  %15 = load i32, i32* %14, align 4
  %arrayidx8 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 %15
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx8)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 0
  store float %add, float* %arrayidx10, align 4
  %16 = load float, float* %blendfactor, align 4
  %sub11 = fsub float 1.000000e+00, %16
  %17 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4
  %call12 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %17)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 1
  %18 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %sub11, %18
  %19 = load float, float* %blendfactor, align 4
  %20 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4
  %call15 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %20)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 1
  %21 = load float, float* %arrayidx16, align 4
  %mul17 = fmul float %19, %21
  %add18 = fadd float %mul14, %mul17
  %22 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  %23 = load i32*, i32** %clipped_count.addr, align 4
  %24 = load i32, i32* %23, align 4
  %arrayidx19 = getelementptr inbounds %class.btVector3, %class.btVector3* %22, i32 %24
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx19)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 1
  store float %add18, float* %arrayidx21, align 4
  %25 = load float, float* %blendfactor, align 4
  %sub22 = fsub float 1.000000e+00, %25
  %26 = load %class.btVector3*, %class.btVector3** %point0.addr, align 4
  %call23 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %26)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 2
  %27 = load float, float* %arrayidx24, align 4
  %mul25 = fmul float %sub22, %27
  %28 = load float, float* %blendfactor, align 4
  %29 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4
  %call26 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %29)
  %arrayidx27 = getelementptr inbounds float, float* %call26, i32 2
  %30 = load float, float* %arrayidx27, align 4
  %mul28 = fmul float %28, %30
  %add29 = fadd float %mul25, %mul28
  %31 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  %32 = load i32*, i32** %clipped_count.addr, align 4
  %33 = load i32, i32* %32, align 4
  %arrayidx30 = getelementptr inbounds %class.btVector3, %class.btVector3* %31, i32 %33
  %call31 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx30)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 2
  store float %add29, float* %arrayidx32, align 4
  %34 = load i32*, i32** %clipped_count.addr, align 4
  %35 = load i32, i32* %34, align 4
  %inc = add i32 %35, 1
  store i32 %inc, i32* %34, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %36 = load i32, i32* %_classif, align 4
  %tobool = icmp ne i32 %36, 0
  br i1 %tobool, label %if.end50, label %if.then33

if.then33:                                        ; preds = %if.end
  %37 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4
  %call34 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %37)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 0
  %38 = load float, float* %arrayidx35, align 4
  %39 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  %40 = load i32*, i32** %clipped_count.addr, align 4
  %41 = load i32, i32* %40, align 4
  %arrayidx36 = getelementptr inbounds %class.btVector3, %class.btVector3* %39, i32 %41
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx36)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 0
  store float %38, float* %arrayidx38, align 4
  %42 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4
  %call39 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %42)
  %arrayidx40 = getelementptr inbounds float, float* %call39, i32 1
  %43 = load float, float* %arrayidx40, align 4
  %44 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  %45 = load i32*, i32** %clipped_count.addr, align 4
  %46 = load i32, i32* %45, align 4
  %arrayidx41 = getelementptr inbounds %class.btVector3, %class.btVector3* %44, i32 %46
  %call42 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx41)
  %arrayidx43 = getelementptr inbounds float, float* %call42, i32 1
  store float %43, float* %arrayidx43, align 4
  %47 = load %class.btVector3*, %class.btVector3** %point1.addr, align 4
  %call44 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %47)
  %arrayidx45 = getelementptr inbounds float, float* %call44, i32 2
  %48 = load float, float* %arrayidx45, align 4
  %49 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  %50 = load i32*, i32** %clipped_count.addr, align 4
  %51 = load i32, i32* %50, align 4
  %arrayidx46 = getelementptr inbounds %class.btVector3, %class.btVector3* %49, i32 %51
  %call47 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx46)
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 2
  store float %48, float* %arrayidx48, align 4
  %52 = load i32*, i32** %clipped_count.addr, align 4
  %53 = load i32, i32* %52, align 4
  %inc49 = add i32 %53, 1
  store i32 %inc49, i32* %52, align 4
  br label %if.end50

if.end50:                                         ; preds = %if.then33, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_Z26PLANE_CLIP_POLYGON_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_PKT_jPS6_T1_(%class.btVector4* nonnull align 4 dereferenceable(16) %plane, %class.btVector3* %polygon_points, i32 %polygon_point_count, %class.btVector3* %clipped) #1 comdat {
entry:
  %distance_func = alloca %class.DISTANCE_PLANE_3D_FUNC, align 1
  %plane.addr = alloca %class.btVector4*, align 4
  %polygon_points.addr = alloca %class.btVector3*, align 4
  %polygon_point_count.addr = alloca i32, align 4
  %clipped.addr = alloca %class.btVector3*, align 4
  %clipped_count = alloca i32, align 4
  %firstdist = alloca float, align 4
  %olddist = alloca float, align 4
  %_i = alloca i32, align 4
  %dist = alloca float, align 4
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4
  store %class.btVector3* %polygon_points, %class.btVector3** %polygon_points.addr, align 4
  store i32 %polygon_point_count, i32* %polygon_point_count.addr, align 4
  store %class.btVector3* %clipped, %class.btVector3** %clipped.addr, align 4
  store i32 0, i32* %clipped_count, align 4
  %0 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0
  %call = call float @_ZN22DISTANCE_PLANE_3D_FUNCclI9btVector39btVector4EEfRKT0_RKT_(%class.DISTANCE_PLANE_3D_FUNC* %distance_func, %class.btVector4* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  store float %call, float* %firstdist, align 4
  %2 = load float, float* %firstdist, align 4
  %cmp = fcmp ogt float %2, 0x3E80000000000000
  br i1 %cmp, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %3 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4
  %arrayidx1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx1)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 0
  %4 = load float, float* %arrayidx3, align 4
  %5 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  %6 = load i32, i32* %clipped_count, align 4
  %arrayidx4 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  %call5 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx4)
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 0
  store float %4, float* %arrayidx6, align 4
  %7 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4
  %arrayidx7 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx7)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 1
  %8 = load float, float* %arrayidx9, align 4
  %9 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  %10 = load i32, i32* %clipped_count, align 4
  %arrayidx10 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 %10
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx10)
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 1
  store float %8, float* %arrayidx12, align 4
  %11 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4
  %arrayidx13 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx13)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %12 = load float, float* %arrayidx15, align 4
  %13 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  %14 = load i32, i32* %clipped_count, align 4
  %arrayidx16 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 %14
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx16)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 2
  store float %12, float* %arrayidx18, align 4
  %15 = load i32, i32* %clipped_count, align 4
  %inc = add i32 %15, 1
  store i32 %inc, i32* %clipped_count, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %16 = load float, float* %firstdist, align 4
  store float %16, float* %olddist, align 4
  store i32 1, i32* %_i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %17 = load i32, i32* %_i, align 4
  %18 = load i32, i32* %polygon_point_count.addr, align 4
  %cmp19 = icmp ult i32 %17, %18
  br i1 %cmp19, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %19 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %20 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4
  %21 = load i32, i32* %_i, align 4
  %arrayidx20 = getelementptr inbounds %class.btVector3, %class.btVector3* %20, i32 %21
  %call21 = call float @_ZN22DISTANCE_PLANE_3D_FUNCclI9btVector39btVector4EEfRKT0_RKT_(%class.DISTANCE_PLANE_3D_FUNC* %distance_func, %class.btVector4* nonnull align 4 dereferenceable(16) %19, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx20)
  store float %call21, float* %dist, align 4
  %22 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4
  %23 = load i32, i32* %_i, align 4
  %sub = sub i32 %23, 1
  %arrayidx22 = getelementptr inbounds %class.btVector3, %class.btVector3* %22, i32 %sub
  %24 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4
  %25 = load i32, i32* %_i, align 4
  %arrayidx23 = getelementptr inbounds %class.btVector3, %class.btVector3* %24, i32 %25
  %26 = load float, float* %olddist, align 4
  %27 = load float, float* %dist, align 4
  %28 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  call void @_Z26PLANE_CLIP_POLYGON_COLLECTI9btVector3EvRKT_S3_ffPS1_Rj(%class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx22, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx23, float %26, float %27, %class.btVector3* %28, i32* nonnull align 4 dereferenceable(4) %clipped_count)
  %29 = load float, float* %dist, align 4
  store float %29, float* %olddist, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %30 = load i32, i32* %_i, align 4
  %inc24 = add i32 %30, 1
  store i32 %inc24, i32* %_i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %31 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4
  %32 = load i32, i32* %polygon_point_count.addr, align 4
  %sub25 = sub i32 %32, 1
  %arrayidx26 = getelementptr inbounds %class.btVector3, %class.btVector3* %31, i32 %sub25
  %33 = load %class.btVector3*, %class.btVector3** %polygon_points.addr, align 4
  %arrayidx27 = getelementptr inbounds %class.btVector3, %class.btVector3* %33, i32 0
  %34 = load float, float* %olddist, align 4
  %35 = load float, float* %firstdist, align 4
  %36 = load %class.btVector3*, %class.btVector3** %clipped.addr, align 4
  call void @_Z26PLANE_CLIP_POLYGON_COLLECTI9btVector3EvRKT_S3_ffPS1_Rj(%class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx26, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx27, float %34, float %35, %class.btVector3* %36, i32* nonnull align 4 dereferenceable(4) %clipped_count)
  %37 = load i32, i32* %clipped_count, align 4
  ret i32 %37
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN25GIM_TRIANGLE_CONTACT_DATA19mergepoints_genericI22DISTANCE_PLANE_3D_FUNC9btVector4EEvRKT0_fPK9btVector3jT_(%struct.GIM_TRIANGLE_CONTACT_DATA* %this, %class.btVector4* nonnull align 4 dereferenceable(16) %plane, float %margin, %class.btVector3* %points, i32 %point_count) #1 comdat {
entry:
  %distance_func = alloca %class.DISTANCE_PLANE_3D_FUNC, align 1
  %this.addr = alloca %struct.GIM_TRIANGLE_CONTACT_DATA*, align 4
  %plane.addr = alloca %class.btVector4*, align 4
  %margin.addr = alloca float, align 4
  %points.addr = alloca %class.btVector3*, align 4
  %point_count.addr = alloca i32, align 4
  %point_indices = alloca [16 x i32], align 16
  %_k = alloca i32, align 4
  %_dist = alloca float, align 4
  store %struct.GIM_TRIANGLE_CONTACT_DATA* %this, %struct.GIM_TRIANGLE_CONTACT_DATA** %this.addr, align 4
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4
  store float %margin, float* %margin.addr, align 4
  store %class.btVector3* %points, %class.btVector3** %points.addr, align 4
  store i32 %point_count, i32* %point_count.addr, align 4
  %this1 = load %struct.GIM_TRIANGLE_CONTACT_DATA*, %struct.GIM_TRIANGLE_CONTACT_DATA** %this.addr, align 4
  %m_point_count = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 1
  store i32 0, i32* %m_point_count, align 4
  %m_penetration_depth = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 0
  store float -1.000000e+03, float* %m_penetration_depth, align 4
  store i32 0, i32* %_k, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %_k, align 4
  %1 = load i32, i32* %point_count.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %points.addr, align 4
  %4 = load i32, i32* %_k, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  %call = call float @_ZN22DISTANCE_PLANE_3D_FUNCclI9btVector39btVector4EEfRKT0_RKT_(%class.DISTANCE_PLANE_3D_FUNC* %distance_func, %class.btVector4* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  %fneg = fneg float %call
  %5 = load float, float* %margin.addr, align 4
  %add = fadd float %fneg, %5
  store float %add, float* %_dist, align 4
  %6 = load float, float* %_dist, align 4
  %cmp2 = fcmp oge float %6, 0.000000e+00
  br i1 %cmp2, label %if.then, label %if.end17

if.then:                                          ; preds = %for.body
  %7 = load float, float* %_dist, align 4
  %m_penetration_depth3 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 0
  %8 = load float, float* %m_penetration_depth3, align 4
  %cmp4 = fcmp ogt float %7, %8
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.then
  %9 = load float, float* %_dist, align 4
  %m_penetration_depth6 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 0
  store float %9, float* %m_penetration_depth6, align 4
  %10 = load i32, i32* %_k, align 4
  %arrayidx7 = getelementptr inbounds [16 x i32], [16 x i32]* %point_indices, i32 0, i32 0
  store i32 %10, i32* %arrayidx7, align 16
  %m_point_count8 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 1
  store i32 1, i32* %m_point_count8, align 4
  br label %if.end16

if.else:                                          ; preds = %if.then
  %11 = load float, float* %_dist, align 4
  %add9 = fadd float %11, 0x3E80000000000000
  %m_penetration_depth10 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 0
  %12 = load float, float* %m_penetration_depth10, align 4
  %cmp11 = fcmp oge float %add9, %12
  br i1 %cmp11, label %if.then12, label %if.end

if.then12:                                        ; preds = %if.else
  %13 = load i32, i32* %_k, align 4
  %m_point_count13 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 1
  %14 = load i32, i32* %m_point_count13, align 4
  %arrayidx14 = getelementptr inbounds [16 x i32], [16 x i32]* %point_indices, i32 0, i32 %14
  store i32 %13, i32* %arrayidx14, align 4
  %m_point_count15 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 1
  %15 = load i32, i32* %m_point_count15, align 4
  %inc = add i32 %15, 1
  store i32 %inc, i32* %m_point_count15, align 4
  br label %if.end

if.end:                                           ; preds = %if.then12, %if.else
  br label %if.end16

if.end16:                                         ; preds = %if.end, %if.then5
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end17
  %16 = load i32, i32* %_k, align 4
  %inc18 = add i32 %16, 1
  store i32 %inc18, i32* %_k, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %_k, align 4
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc26, %for.end
  %17 = load i32, i32* %_k, align 4
  %m_point_count20 = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 1
  %18 = load i32, i32* %m_point_count20, align 4
  %cmp21 = icmp ult i32 %17, %18
  br i1 %cmp21, label %for.body22, label %for.end28

for.body22:                                       ; preds = %for.cond19
  %19 = load %class.btVector3*, %class.btVector3** %points.addr, align 4
  %20 = load i32, i32* %_k, align 4
  %arrayidx23 = getelementptr inbounds [16 x i32], [16 x i32]* %point_indices, i32 0, i32 %20
  %21 = load i32, i32* %arrayidx23, align 4
  %arrayidx24 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 %21
  %m_points = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT_DATA, %struct.GIM_TRIANGLE_CONTACT_DATA* %this1, i32 0, i32 3
  %22 = load i32, i32* %_k, align 4
  %arrayidx25 = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %m_points, i32 0, i32 %22
  %23 = bitcast %class.btVector3* %arrayidx25 to i8*
  %24 = bitcast %class.btVector3* %arrayidx24 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false)
  br label %for.inc26

for.inc26:                                        ; preds = %for.body22
  %25 = load i32, i32* %_k, align 4
  %inc27 = add i32 %25, 1
  store i32 %inc27, i32* %_k, align 4
  br label %for.cond19

for.end28:                                        ; preds = %for.cond19
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_gim_tri_collision.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
