; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btConvex2dShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btConvex2dShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btConvex2dShape = type { %class.btConvexShape, %class.btConvexShape* }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btVector3 = type { [4 x float] }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btSerializer = type opaque

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN15btConvex2dShapedlEPv = comdat any

$_ZNK15btConvex2dShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZNK16btCollisionShape28calculateSerializeBufferSizeEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV15btConvex2dShape = hidden unnamed_addr constant { [25 x i8*] } { [25 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btConvex2dShape to i8*), i8* bitcast (%class.btConvex2dShape* (%class.btConvex2dShape*)* @_ZN15btConvex2dShapeD1Ev to i8*), i8* bitcast (void (%class.btConvex2dShape*)* @_ZN15btConvex2dShapeD0Ev to i8*), i8* bitcast (void (%class.btConvex2dShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK15btConvex2dShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btConvex2dShape*, %class.btVector3*)* @_ZN15btConvex2dShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvex2dShape*)* @_ZNK15btConvex2dShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btConvex2dShape*, float, %class.btVector3*)* @_ZNK15btConvex2dShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btConvex2dShape*)* @_ZNK15btConvex2dShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConvex2dShape*, float)* @_ZN15btConvex2dShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvex2dShape*)* @_ZNK15btConvex2dShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btCollisionShape*)* @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)* @_ZNK16btCollisionShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvex2dShape*, %class.btVector3*)* @_ZNK15btConvex2dShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvex2dShape*, %class.btVector3*)* @_ZNK15btConvex2dShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*, %class.btVector3*, %class.btVector3*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_RS3_S7_ to i8*), i8* bitcast (void (%class.btConvex2dShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK15btConvex2dShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvex2dShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK15btConvex2dShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btConvex2dShape*)* @_ZNK15btConvex2dShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btConvex2dShape*, i32, %class.btVector3*)* @_ZNK15btConvex2dShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS15btConvex2dShape = hidden constant [18 x i8] c"15btConvex2dShape\00", align 1
@_ZTI13btConvexShape = external constant i8*
@_ZTI15btConvex2dShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @_ZTS15btConvex2dShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI13btConvexShape to i8*) }, align 4
@.str = private unnamed_addr constant [14 x i8] c"Convex2dShape\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btConvex2dShape.cpp, i8* null }]

@_ZN15btConvex2dShapeC1EP13btConvexShape = hidden unnamed_addr alias %class.btConvex2dShape* (%class.btConvex2dShape*, %class.btConvexShape*), %class.btConvex2dShape* (%class.btConvex2dShape*, %class.btConvexShape*)* @_ZN15btConvex2dShapeC2EP13btConvexShape
@_ZN15btConvex2dShapeD1Ev = hidden unnamed_addr alias %class.btConvex2dShape* (%class.btConvex2dShape*), %class.btConvex2dShape* (%class.btConvex2dShape*)* @_ZN15btConvex2dShapeD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btConvex2dShape* @_ZN15btConvex2dShapeC2EP13btConvexShape(%class.btConvex2dShape* returned %this, %class.btConvexShape* %convexChildShape) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvex2dShape*, align 4
  %convexChildShape.addr = alloca %class.btConvexShape*, align 4
  store %class.btConvex2dShape* %this, %class.btConvex2dShape** %this.addr, align 4
  store %class.btConvexShape* %convexChildShape, %class.btConvexShape** %convexChildShape.addr, align 4
  %this1 = load %class.btConvex2dShape*, %class.btConvex2dShape** %this.addr, align 4
  %0 = bitcast %class.btConvex2dShape* %this1 to %class.btConvexShape*
  %call = call %class.btConvexShape* @_ZN13btConvexShapeC2Ev(%class.btConvexShape* %0)
  %1 = bitcast %class.btConvex2dShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [25 x i8*] }, { [25 x i8*] }* @_ZTV15btConvex2dShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_childConvexShape = getelementptr inbounds %class.btConvex2dShape, %class.btConvex2dShape* %this1, i32 0, i32 1
  %2 = load %class.btConvexShape*, %class.btConvexShape** %convexChildShape.addr, align 4
  store %class.btConvexShape* %2, %class.btConvexShape** %m_childConvexShape, align 4
  %3 = bitcast %class.btConvex2dShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %3, i32 0, i32 1
  store i32 18, i32* %m_shapeType, align 4
  ret %class.btConvex2dShape* %this1
}

declare %class.btConvexShape* @_ZN13btConvexShapeC2Ev(%class.btConvexShape* returned) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define hidden %class.btConvex2dShape* @_ZN15btConvex2dShapeD2Ev(%class.btConvex2dShape* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConvex2dShape*, align 4
  store %class.btConvex2dShape* %this, %class.btConvex2dShape** %this.addr, align 4
  %this1 = load %class.btConvex2dShape*, %class.btConvex2dShape** %this.addr, align 4
  %0 = bitcast %class.btConvex2dShape* %this1 to %class.btConvexShape*
  %call = call %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* %0) #5
  ret %class.btConvex2dShape* %this1
}

; Function Attrs: nounwind
declare %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN15btConvex2dShapeD0Ev(%class.btConvex2dShape* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConvex2dShape*, align 4
  store %class.btConvex2dShape* %this, %class.btConvex2dShape** %this.addr, align 4
  %this1 = load %class.btConvex2dShape*, %class.btConvex2dShape** %this.addr, align 4
  %call = call %class.btConvex2dShape* @_ZN15btConvex2dShapeD1Ev(%class.btConvex2dShape* %this1) #5
  %0 = bitcast %class.btConvex2dShape* %this1 to i8*
  call void @_ZN15btConvex2dShapedlEPv(i8* %0) #5
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btConvex2dShapedlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK15btConvex2dShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btConvex2dShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvex2dShape*, align 4
  %vec.addr = alloca %class.btVector3*, align 4
  store %class.btConvex2dShape* %this, %class.btConvex2dShape** %this.addr, align 4
  store %class.btVector3* %vec, %class.btVector3** %vec.addr, align 4
  %this1 = load %class.btConvex2dShape*, %class.btConvex2dShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btConvex2dShape, %class.btConvex2dShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4
  %2 = bitcast %class.btConvexShape* %0 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)***
  %vtable = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*** %2, align 4
  %vfn = getelementptr inbounds void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vtable, i64 17
  %3 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vfn, align 4
  call void %3(%class.btVector3* sret align 4 %agg.result, %class.btConvexShape* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK15btConvex2dShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btConvex2dShape* %this, %class.btVector3* %vectors, %class.btVector3* %supportVerticesOut, i32 %numVectors) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvex2dShape*, align 4
  %vectors.addr = alloca %class.btVector3*, align 4
  %supportVerticesOut.addr = alloca %class.btVector3*, align 4
  %numVectors.addr = alloca i32, align 4
  store %class.btConvex2dShape* %this, %class.btConvex2dShape** %this.addr, align 4
  store %class.btVector3* %vectors, %class.btVector3** %vectors.addr, align 4
  store %class.btVector3* %supportVerticesOut, %class.btVector3** %supportVerticesOut.addr, align 4
  store i32 %numVectors, i32* %numVectors.addr, align 4
  %this1 = load %class.btConvex2dShape*, %class.btConvex2dShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btConvex2dShape, %class.btConvex2dShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = load %class.btVector3*, %class.btVector3** %vectors.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4
  %3 = load i32, i32* %numVectors.addr, align 4
  %4 = bitcast %class.btConvexShape* %0 to void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)***
  %vtable = load void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)**, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*** %4, align 4
  %vfn = getelementptr inbounds void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)** %vtable, i64 19
  %5 = load void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)** %vfn, align 4
  call void %5(%class.btConvexShape* %0, %class.btVector3* %1, %class.btVector3* %2, i32 %3)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK15btConvex2dShape24localGetSupportingVertexERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btConvex2dShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvex2dShape*, align 4
  %vec.addr = alloca %class.btVector3*, align 4
  store %class.btConvex2dShape* %this, %class.btConvex2dShape** %this.addr, align 4
  store %class.btVector3* %vec, %class.btVector3** %vec.addr, align 4
  %this1 = load %class.btConvex2dShape*, %class.btConvex2dShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btConvex2dShape, %class.btConvex2dShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4
  %2 = bitcast %class.btConvexShape* %0 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)***
  %vtable = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*** %2, align 4
  %vfn = getelementptr inbounds void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vtable, i64 16
  %3 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vfn, align 4
  call void %3(%class.btVector3* sret align 4 %agg.result, %class.btConvexShape* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK15btConvex2dShape21calculateLocalInertiaEfR9btVector3(%class.btConvex2dShape* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvex2dShape*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  store %class.btConvex2dShape* %this, %class.btConvex2dShape** %this.addr, align 4
  store float %mass, float* %mass.addr, align 4
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4
  %this1 = load %class.btConvex2dShape*, %class.btConvex2dShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btConvex2dShape, %class.btConvex2dShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = bitcast %class.btConvexShape* %0 to %class.btCollisionShape*
  %2 = load float, float* %mass.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %4 = bitcast %class.btCollisionShape* %1 to void (%class.btCollisionShape*, float, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, float, %class.btVector3*)**, void (%class.btCollisionShape*, float, %class.btVector3*)*** %4, align 4
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, float, %class.btVector3*)*, void (%class.btCollisionShape*, float, %class.btVector3*)** %vtable, i64 8
  %5 = load void (%class.btCollisionShape*, float, %class.btVector3*)*, void (%class.btCollisionShape*, float, %class.btVector3*)** %vfn, align 4
  call void %5(%class.btCollisionShape* %1, float %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK15btConvex2dShape7getAabbERK11btTransformR9btVector3S4_(%class.btConvex2dShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvex2dShape*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btConvex2dShape* %this, %class.btConvex2dShape** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btConvex2dShape*, %class.btConvex2dShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btConvex2dShape, %class.btConvex2dShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %4 = bitcast %class.btConvexShape* %0 to void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %4, align 4
  %vfn = getelementptr inbounds void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %5 = load void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %5(%class.btConvexShape* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK15btConvex2dShape11getAabbSlowERK11btTransformR9btVector3S4_(%class.btConvex2dShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvex2dShape*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btConvex2dShape* %this, %class.btConvex2dShape** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btConvex2dShape*, %class.btConvex2dShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btConvex2dShape, %class.btConvex2dShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %4 = bitcast %class.btConvexShape* %0 to void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %4, align 4
  %vfn = getelementptr inbounds void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 20
  %5 = load void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %5(%class.btConvexShape* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN15btConvex2dShape15setLocalScalingERK9btVector3(%class.btConvex2dShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvex2dShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  store %class.btConvex2dShape* %this, %class.btConvex2dShape** %this.addr, align 4
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4
  %this1 = load %class.btConvex2dShape*, %class.btConvex2dShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btConvex2dShape, %class.btConvex2dShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4
  %2 = bitcast %class.btConvexShape* %0 to void (%class.btConvexShape*, %class.btVector3*)***
  %vtable = load void (%class.btConvexShape*, %class.btVector3*)**, void (%class.btConvexShape*, %class.btVector3*)*** %2, align 4
  %vfn = getelementptr inbounds void (%class.btConvexShape*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btVector3*)** %vtable, i64 6
  %3 = load void (%class.btConvexShape*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btVector3*)** %vfn, align 4
  call void %3(%class.btConvexShape* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret void
}

; Function Attrs: noinline optnone
define hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btConvex2dShape15getLocalScalingEv(%class.btConvex2dShape* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvex2dShape*, align 4
  store %class.btConvex2dShape* %this, %class.btConvex2dShape** %this.addr, align 4
  %this1 = load %class.btConvex2dShape*, %class.btConvex2dShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btConvex2dShape, %class.btConvex2dShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = bitcast %class.btConvexShape* %0 to %class.btVector3* (%class.btConvexShape*)***
  %vtable = load %class.btVector3* (%class.btConvexShape*)**, %class.btVector3* (%class.btConvexShape*)*** %1, align 4
  %vfn = getelementptr inbounds %class.btVector3* (%class.btConvexShape*)*, %class.btVector3* (%class.btConvexShape*)** %vtable, i64 7
  %2 = load %class.btVector3* (%class.btConvexShape*)*, %class.btVector3* (%class.btConvexShape*)** %vfn, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* %2(%class.btConvexShape* %0)
  ret %class.btVector3* %call
}

; Function Attrs: noinline optnone
define hidden void @_ZN15btConvex2dShape9setMarginEf(%class.btConvex2dShape* %this, float %margin) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvex2dShape*, align 4
  %margin.addr = alloca float, align 4
  store %class.btConvex2dShape* %this, %class.btConvex2dShape** %this.addr, align 4
  store float %margin, float* %margin.addr, align 4
  %this1 = load %class.btConvex2dShape*, %class.btConvex2dShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btConvex2dShape, %class.btConvex2dShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = load float, float* %margin.addr, align 4
  %2 = bitcast %class.btConvexShape* %0 to void (%class.btConvexShape*, float)***
  %vtable = load void (%class.btConvexShape*, float)**, void (%class.btConvexShape*, float)*** %2, align 4
  %vfn = getelementptr inbounds void (%class.btConvexShape*, float)*, void (%class.btConvexShape*, float)** %vtable, i64 11
  %3 = load void (%class.btConvexShape*, float)*, void (%class.btConvexShape*, float)** %vfn, align 4
  call void %3(%class.btConvexShape* %0, float %1)
  ret void
}

; Function Attrs: noinline optnone
define hidden float @_ZNK15btConvex2dShape9getMarginEv(%class.btConvex2dShape* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvex2dShape*, align 4
  store %class.btConvex2dShape* %this, %class.btConvex2dShape** %this.addr, align 4
  %this1 = load %class.btConvex2dShape*, %class.btConvex2dShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btConvex2dShape, %class.btConvex2dShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = bitcast %class.btConvexShape* %0 to float (%class.btConvexShape*)***
  %vtable = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %1, align 4
  %vfn = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable, i64 12
  %2 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn, align 4
  %call = call float %2(%class.btConvexShape* %0)
  ret float %call
}

; Function Attrs: noinline optnone
define hidden i32 @_ZNK15btConvex2dShape36getNumPreferredPenetrationDirectionsEv(%class.btConvex2dShape* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvex2dShape*, align 4
  store %class.btConvex2dShape* %this, %class.btConvex2dShape** %this.addr, align 4
  %this1 = load %class.btConvex2dShape*, %class.btConvex2dShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btConvex2dShape, %class.btConvex2dShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = bitcast %class.btConvexShape* %0 to i32 (%class.btConvexShape*)***
  %vtable = load i32 (%class.btConvexShape*)**, i32 (%class.btConvexShape*)*** %1, align 4
  %vfn = getelementptr inbounds i32 (%class.btConvexShape*)*, i32 (%class.btConvexShape*)** %vtable, i64 21
  %2 = load i32 (%class.btConvexShape*)*, i32 (%class.btConvexShape*)** %vfn, align 4
  %call = call i32 %2(%class.btConvexShape* %0)
  ret i32 %call
}

; Function Attrs: noinline optnone
define hidden void @_ZNK15btConvex2dShape32getPreferredPenetrationDirectionEiR9btVector3(%class.btConvex2dShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %penetrationVector) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvex2dShape*, align 4
  %index.addr = alloca i32, align 4
  %penetrationVector.addr = alloca %class.btVector3*, align 4
  store %class.btConvex2dShape* %this, %class.btConvex2dShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store %class.btVector3* %penetrationVector, %class.btVector3** %penetrationVector.addr, align 4
  %this1 = load %class.btConvex2dShape*, %class.btConvex2dShape** %this.addr, align 4
  %m_childConvexShape = getelementptr inbounds %class.btConvex2dShape, %class.btConvex2dShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_childConvexShape, align 4
  %1 = load i32, i32* %index.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4
  %3 = bitcast %class.btConvexShape* %0 to void (%class.btConvexShape*, i32, %class.btVector3*)***
  %vtable = load void (%class.btConvexShape*, i32, %class.btVector3*)**, void (%class.btConvexShape*, i32, %class.btVector3*)*** %3, align 4
  %vfn = getelementptr inbounds void (%class.btConvexShape*, i32, %class.btVector3*)*, void (%class.btConvexShape*, i32, %class.btVector3*)** %vtable, i64 22
  %4 = load void (%class.btConvexShape*, i32, %class.btVector3*)*, void (%class.btConvexShape*, i32, %class.btVector3*)** %vfn, align 4
  call void %4(%class.btConvexShape* %0, i32 %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #3

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #3

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK15btConvex2dShape7getNameEv(%class.btConvex2dShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvex2dShape*, align 4
  store %class.btConvex2dShape* %this, %class.btConvex2dShape** %this.addr, align 4
  %this1 = load %class.btConvex2dShape*, %class.btConvex2dShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  store float 1.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv(%class.btCollisionShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  ret i32 12
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #3

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #3

declare void @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_RS3_S7_(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float* nonnull align 4 dereferenceable(4), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btConvex2dShape.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
