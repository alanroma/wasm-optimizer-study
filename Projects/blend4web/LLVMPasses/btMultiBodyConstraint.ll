; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Featherstone/btMultiBodyConstraint.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Featherstone/btMultiBodyConstraint.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btMultiBodyConstraint = type { i32 (...)**, %class.btMultiBody*, %class.btMultiBody*, i32, i32, i32, i32, i32, i32, i8, i32, float, %class.btAlignedObjectArray.4 }
%class.btMultiBody = type <{ i32 (...)**, %class.btMultiBodyLinkCollider*, i8*, %class.btVector3, %class.btQuaternion, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, i8, i8, i8, i8, float, i8*, i32, i32, i32, float, float, i8, [3 x i8], float, float, i8, i8, [2 x i8], i32, i32, i8, i8, i8, i8 }>
%class.btMultiBodyLinkCollider = type opaque
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btMultibodyLink*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btMultibodyLink = type { float, %class.btVector3, i32, %class.btQuaternion, %class.btVector3, %class.btVector3, %struct.btSpatialMotionVector, %struct.btSpatialMotionVector, [6 x %struct.btSpatialMotionVector], i32, i32, %class.btQuaternion, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, [7 x float], [6 x float], %class.btMultiBodyLinkCollider*, i32, i32, i32, i32, %struct.btMultiBodyJointFeedback*, %class.btTransform, i8*, i8*, i8*, float, float }
%struct.btSpatialMotionVector = type { %class.btVector3, %class.btVector3 }
%struct.btMultiBodyJointFeedback = type opaque
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btMultiBodyLinkCollider**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %class.btMatrix3x3*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%struct.btMultiBodyJacobianData = type { %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.16*, i32 }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %struct.btSolverBody*, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.23, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray.19, i32, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray.19 = type <{ %class.btAlignedAllocator.20, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.20 = type { i8 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.23 = type <{ %class.btAlignedAllocator.24, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.24 = type { i8 }
%class.btTypedConstraint = type opaque
%struct.btMultiBodySolverConstraint = type { i32, i32, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.28, i32, i32, i32, %class.btMultiBody*, i32, i32, %class.btMultiBody*, i32, %class.btMultiBodyConstraint*, i32 }
%union.anon.28 = type { i8* }
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float, float }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN20btAlignedObjectArrayIfEC2Ev = comdat any

$_ZNK11btMultiBody10getNumDofsEv = comdat any

$_ZN20btAlignedObjectArrayIfE6resizeEiRKf = comdat any

$_ZN20btAlignedObjectArrayIfED2Ev = comdat any

$_ZN20btAlignedObjectArrayIfEixEi = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyE2atEi = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK12btSolverBody17getWorldTransformEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK11btMultiBody10getBasePosEv = comdat any

$_ZN11btMultiBody7getLinkEi = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZNK11btMultiBody14getCompanionIdEv = comdat any

$_ZNK20btAlignedObjectArrayIfE4sizeEv = comdat any

$_ZN11btMultiBody14setCompanionIdEi = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btRigidBody24getInvInertiaTensorWorldEv = comdat any

$_ZNK11btRigidBody16getAngularFactorEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK11btRigidBody10getInvMassEv = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK11btMultiBody17getVelocityVectorEv = comdat any

$_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3 = comdat any

$_ZN21btMultiBodyConstraint11setFrameInBERK11btMatrix3x3 = comdat any

$_ZN21btMultiBodyConstraint11setPivotInBERK9btVector3 = comdat any

$_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK20btAlignedObjectArrayIfEixEi = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZN18btAlignedAllocatorIfLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIfE4initEv = comdat any

$_ZN20btAlignedObjectArrayIfE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIfE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIfE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf = comdat any

$_ZN20btAlignedObjectArrayIfE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIfE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIfE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIfE4copyEiiPf = comdat any

$_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV21btMultiBodyConstraint = hidden unnamed_addr constant { [11 x i8*] } { [11 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI21btMultiBodyConstraint to i8*), i8* bitcast (%class.btMultiBodyConstraint* (%class.btMultiBodyConstraint*)* @_ZN21btMultiBodyConstraintD1Ev to i8*), i8* bitcast (void (%class.btMultiBodyConstraint*)* @_ZN21btMultiBodyConstraintD0Ev to i8*), i8* bitcast (void (%class.btMultiBodyConstraint*, %class.btMatrix3x3*)* @_ZN21btMultiBodyConstraint11setFrameInBERK11btMatrix3x3 to i8*), i8* bitcast (void (%class.btMultiBodyConstraint*, %class.btVector3*)* @_ZN21btMultiBodyConstraint11setPivotInBERK9btVector3 to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, align 4
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS21btMultiBodyConstraint = hidden constant [24 x i8] c"21btMultiBodyConstraint\00", align 1
@_ZTI21btMultiBodyConstraint = hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @_ZTS21btMultiBodyConstraint, i32 0, i32 0) }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btMultiBodyConstraint.cpp, i8* null }]

@_ZN21btMultiBodyConstraintD1Ev = hidden unnamed_addr alias %class.btMultiBodyConstraint* (%class.btMultiBodyConstraint*), %class.btMultiBodyConstraint* (%class.btMultiBodyConstraint*)* @_ZN21btMultiBodyConstraintD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintC2EP11btMultiBodyS1_iiib(%class.btMultiBodyConstraint* returned %this, %class.btMultiBody* %bodyA, %class.btMultiBody* %bodyB, i32 %linkA, i32 %linkB, i32 %numRows, i1 zeroext %isUnilateral) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %bodyA.addr = alloca %class.btMultiBody*, align 4
  %bodyB.addr = alloca %class.btMultiBody*, align 4
  %linkA.addr = alloca i32, align 4
  %linkB.addr = alloca i32, align 4
  %numRows.addr = alloca i32, align 4
  %isUnilateral.addr = alloca i8, align 1
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4
  store %class.btMultiBody* %bodyA, %class.btMultiBody** %bodyA.addr, align 4
  store %class.btMultiBody* %bodyB, %class.btMultiBody** %bodyB.addr, align 4
  store i32 %linkA, i32* %linkA.addr, align 4
  store i32 %linkB, i32* %linkB.addr, align 4
  store i32 %numRows, i32* %numRows.addr, align 4
  %frombool = zext i1 %isUnilateral to i8
  store i8 %frombool, i8* %isUnilateral.addr, align 1
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [11 x i8*] }, { [11 x i8*] }* @_ZTV21btMultiBodyConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_bodyA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 1
  %1 = load %class.btMultiBody*, %class.btMultiBody** %bodyA.addr, align 4
  store %class.btMultiBody* %1, %class.btMultiBody** %m_bodyA, align 4
  %m_bodyB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 2
  %2 = load %class.btMultiBody*, %class.btMultiBody** %bodyB.addr, align 4
  store %class.btMultiBody* %2, %class.btMultiBody** %m_bodyB, align 4
  %m_linkA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 3
  %3 = load i32, i32* %linkA.addr, align 4
  store i32 %3, i32* %m_linkA, align 4
  %m_linkB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 4
  %4 = load i32, i32* %linkB.addr, align 4
  store i32 %4, i32* %m_linkB, align 4
  %m_numRows = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 5
  %5 = load i32, i32* %numRows.addr, align 4
  store i32 %5, i32* %m_numRows, align 4
  %m_jacSizeA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 6
  store i32 0, i32* %m_jacSizeA, align 4
  %m_jacSizeBoth = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 7
  store i32 0, i32* %m_jacSizeBoth, align 4
  %m_isUnilateral = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 9
  %6 = load i8, i8* %isUnilateral.addr, align 1
  %tobool = trunc i8 %6 to i1
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %m_isUnilateral, align 4
  %m_numDofsFinalized = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 10
  store i32 -1, i32* %m_numDofsFinalized, align 4
  %m_maxAppliedImpulse = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 11
  store float 1.000000e+02, float* %m_maxAppliedImpulse, align 4
  %m_data = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 12
  %call = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.4* %m_data)
  ret %class.btMultiBodyConstraint* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.5* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btMultiBodyConstraint19updateJacobianSizesEv(%class.btMultiBodyConstraint* %this) #2 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %m_bodyA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 1
  %0 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA, align 4
  %tobool = icmp ne %class.btMultiBody* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_bodyA2 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 1
  %1 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA2, align 4
  %call = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %1)
  %add = add nsw i32 6, %call
  %m_jacSizeA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 6
  store i32 %add, i32* %m_jacSizeA, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_bodyB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 2
  %2 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB, align 4
  %tobool3 = icmp ne %class.btMultiBody* %2, null
  br i1 %tobool3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.end
  %m_jacSizeA5 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 6
  %3 = load i32, i32* %m_jacSizeA5, align 4
  %add6 = add nsw i32 %3, 6
  %m_bodyB7 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 2
  %4 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB7, align 4
  %call8 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %4)
  %add9 = add nsw i32 %add6, %call8
  %m_jacSizeBoth = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 7
  store i32 %add9, i32* %m_jacSizeBoth, align 4
  br label %if.end12

if.else:                                          ; preds = %if.end
  %m_jacSizeA10 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 6
  %5 = load i32, i32* %m_jacSizeA10, align 4
  %m_jacSizeBoth11 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 7
  store i32 %5, i32* %m_jacSizeBoth11, align 4
  br label %if.end12

if.end12:                                         ; preds = %if.else, %if.then4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_dofCount = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 39
  %0 = load i32, i32* %m_dofCount, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btMultiBodyConstraint25allocateJacobiansMultiDofEv(%class.btMultiBodyConstraint* %this) #2 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  call void @_ZN21btMultiBodyConstraint19updateJacobianSizesEv(%class.btMultiBodyConstraint* %this1)
  %m_jacSizeBoth = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 7
  %0 = load i32, i32* %m_jacSizeBoth, align 4
  %add = add nsw i32 1, %0
  %m_numRows = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 5
  %1 = load i32, i32* %m_numRows, align 4
  %mul = mul nsw i32 %add, %1
  %m_posOffset = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 8
  store i32 %mul, i32* %m_posOffset, align 4
  %m_data = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 12
  %m_jacSizeBoth2 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 7
  %2 = load i32, i32* %m_jacSizeBoth2, align 4
  %add3 = add nsw i32 2, %2
  %m_numRows4 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 5
  %3 = load i32, i32* %m_numRows4, align 4
  %mul5 = mul nsw i32 %add3, %3
  store float 0.000000e+00, float* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_data, i32 %mul5, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %this, i32 %newsize, float* nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca float*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store float* %fillData, float** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %5 = load float*, float** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %14 = load float*, float** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds float, float* %14, i32 %15
  %16 = bitcast float* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to float*
  %18 = load float*, float** %fillData.addr, align 4
  %19 = load float, float* %18, align 4
  store float %19, float* %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintD2Ev(%class.btMultiBodyConstraint* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [11 x i8*] }, { [11 x i8*] }* @_ZTV21btMultiBodyConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_data = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 12
  %call = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.4* %m_data) #6
  ret %class.btMultiBodyConstraint* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN21btMultiBodyConstraintD0Ev(%class.btMultiBodyConstraint* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  call void @llvm.trap() #7
  unreachable
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #3

; Function Attrs: noinline optnone
define hidden void @_ZN21btMultiBodyConstraint13applyDeltaVeeER23btMultiBodyJacobianDataPffii(%class.btMultiBodyConstraint* %this, %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128) %data, float* %delta_vee, float %impulse, i32 %velocityIndex, i32 %ndof) #2 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %data.addr = alloca %struct.btMultiBodyJacobianData*, align 4
  %delta_vee.addr = alloca float*, align 4
  %impulse.addr = alloca float, align 4
  %velocityIndex.addr = alloca i32, align 4
  %ndof.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4
  store %struct.btMultiBodyJacobianData* %data, %struct.btMultiBodyJacobianData** %data.addr, align 4
  store float* %delta_vee, float** %delta_vee.addr, align 4
  store float %impulse, float* %impulse.addr, align 4
  store i32 %velocityIndex, i32* %velocityIndex.addr, align 4
  store i32 %ndof, i32* %ndof.addr, align 4
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %ndof.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load float*, float** %delta_vee.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %2, i32 %3
  %4 = load float, float* %arrayidx, align 4
  %5 = load float, float* %impulse.addr, align 4
  %mul = fmul float %4, %5
  %6 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_deltaVelocities = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %6, i32 0, i32 2
  %7 = load i32, i32* %velocityIndex.addr, align 4
  %8 = load i32, i32* %i, align 4
  %add = add nsw i32 %7, %8
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_deltaVelocities, i32 %add)
  %9 = load float, float* %call, align 4
  %add2 = fadd float %9, %mul
  store float %add2, float* %call, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define hidden float @_ZN21btMultiBodyConstraint23fillMultiBodyConstraintER27btMultiBodySolverConstraintR23btMultiBodyJacobianDataPfS4_RK9btVector3S7_S7_S7_fRK19btContactSolverInfoffbfbff(%class.btMultiBodyConstraint* %this, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %solverConstraint, %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128) %data, float* %jacOrgA, float* %jacOrgB, %class.btVector3* nonnull align 4 dereferenceable(16) %constraintNormalAng, %class.btVector3* nonnull align 4 dereferenceable(16) %constraintNormalLin, %class.btVector3* nonnull align 4 dereferenceable(16) %posAworld, %class.btVector3* nonnull align 4 dereferenceable(16) %posBworld, float %posError, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal, float %lowerLimit, float %upperLimit, i1 zeroext %angConstraint, float %relaxation, i1 zeroext %isFriction, float %desiredVelocity, float %cfmSlip) #2 {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %solverConstraint.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  %data.addr = alloca %struct.btMultiBodyJacobianData*, align 4
  %jacOrgA.addr = alloca float*, align 4
  %jacOrgB.addr = alloca float*, align 4
  %constraintNormalAng.addr = alloca %class.btVector3*, align 4
  %constraintNormalLin.addr = alloca %class.btVector3*, align 4
  %posAworld.addr = alloca %class.btVector3*, align 4
  %posBworld.addr = alloca %class.btVector3*, align 4
  %posError.addr = alloca float, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %lowerLimit.addr = alloca float, align 4
  %upperLimit.addr = alloca float, align 4
  %angConstraint.addr = alloca i8, align 1
  %relaxation.addr = alloca float, align 4
  %isFriction.addr = alloca i8, align 1
  %desiredVelocity.addr = alloca float, align 4
  %cfmSlip.addr = alloca float, align 4
  %multiBodyA = alloca %class.btMultiBody*, align 4
  %multiBodyB = alloca %class.btMultiBody*, align 4
  %bodyA = alloca %struct.btSolverBody*, align 4
  %bodyB = alloca %struct.btSolverBody*, align 4
  %rb0 = alloca %class.btRigidBody*, align 4
  %rb1 = alloca %class.btRigidBody*, align 4
  %rel_pos1 = alloca %class.btVector3, align 4
  %rel_pos2 = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp40 = alloca %class.btVector3, align 4
  %ref.tmp42 = alloca %class.btVector3, align 4
  %ndofA = alloca i32, align 4
  %ref.tmp59 = alloca float, align 4
  %ref.tmp67 = alloca float, align 4
  %i = alloca i32, align 4
  %jac1 = alloca float*, align 4
  %ref.tmp84 = alloca float, align 4
  %delta = alloca float*, align 4
  %torqueAxis0 = alloca %class.btVector3, align 4
  %ref.tmp97 = alloca %class.btVector3, align 4
  %torqueAxis0100 = alloca %class.btVector3, align 4
  %ref.tmp105 = alloca %class.btVector3, align 4
  %ref.tmp107 = alloca %class.btVector3, align 4
  %ref.tmp110 = alloca %class.btVector3, align 4
  %ref.tmp114 = alloca float, align 4
  %ref.tmp115 = alloca float, align 4
  %ref.tmp116 = alloca float, align 4
  %ref.tmp127 = alloca %class.btVector3, align 4
  %ref.tmp130 = alloca %class.btVector3, align 4
  %ndofB = alloca i32, align 4
  %ref.tmp150 = alloca float, align 4
  %ref.tmp158 = alloca float, align 4
  %i161 = alloca i32, align 4
  %ref.tmp175 = alloca %class.btVector3, align 4
  %ref.tmp176 = alloca %class.btVector3, align 4
  %ref.tmp188 = alloca float, align 4
  %delta189 = alloca float*, align 4
  %torqueAxis1 = alloca %class.btVector3, align 4
  %ref.tmp202 = alloca %class.btVector3, align 4
  %ref.tmp204 = alloca %class.btVector3, align 4
  %ref.tmp205 = alloca %class.btVector3, align 4
  %torqueAxis1207 = alloca %class.btVector3, align 4
  %ref.tmp212 = alloca %class.btVector3, align 4
  %ref.tmp214 = alloca %class.btVector3, align 4
  %ref.tmp217 = alloca %class.btVector3, align 4
  %ref.tmp219 = alloca %class.btVector3, align 4
  %ref.tmp222 = alloca float, align 4
  %ref.tmp223 = alloca float, align 4
  %ref.tmp224 = alloca float, align 4
  %ref.tmp227 = alloca %class.btVector3, align 4
  %ref.tmp229 = alloca %class.btVector3, align 4
  %vec = alloca %class.btVector3, align 4
  %denom0 = alloca float, align 4
  %denom1 = alloca float, align 4
  %jacB = alloca float*, align 4
  %jacA = alloca float*, align 4
  %deltaVelA = alloca float*, align 4
  %deltaVelB = alloca float*, align 4
  %ndofA233 = alloca i32, align 4
  %i244 = alloca i32, align 4
  %j = alloca float, align 4
  %l = alloca float, align 4
  %ref.tmp257 = alloca %class.btVector3, align 4
  %ndofB273 = alloca i32, align 4
  %i282 = alloca i32, align 4
  %j286 = alloca float, align 4
  %l288 = alloca float, align 4
  %ref.tmp298 = alloca %class.btVector3, align 4
  %ref.tmp299 = alloca %class.btVector3, align 4
  %d = alloca float, align 4
  %penetration = alloca float, align 4
  %rel_vel = alloca float, align 4
  %ndofA324 = alloca i32, align 4
  %ndofB325 = alloca i32, align 4
  %vel1 = alloca %class.btVector3, align 4
  %vel2 = alloca %class.btVector3, align 4
  %jacA332 = alloca float*, align 4
  %i336 = alloca i32, align 4
  %ref.tmp351 = alloca %class.btVector3, align 4
  %jacB361 = alloca float*, align 4
  %i365 = alloca i32, align 4
  %ref.tmp380 = alloca %class.btVector3, align 4
  %positionalError = alloca float, align 4
  %velocityError = alloca float, align 4
  %erp = alloca float, align 4
  %penetrationImpulse = alloca float, align 4
  %velocityImpulse = alloca float, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4
  store %struct.btMultiBodySolverConstraint* %solverConstraint, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  store %struct.btMultiBodyJacobianData* %data, %struct.btMultiBodyJacobianData** %data.addr, align 4
  store float* %jacOrgA, float** %jacOrgA.addr, align 4
  store float* %jacOrgB, float** %jacOrgB.addr, align 4
  store %class.btVector3* %constraintNormalAng, %class.btVector3** %constraintNormalAng.addr, align 4
  store %class.btVector3* %constraintNormalLin, %class.btVector3** %constraintNormalLin.addr, align 4
  store %class.btVector3* %posAworld, %class.btVector3** %posAworld.addr, align 4
  store %class.btVector3* %posBworld, %class.btVector3** %posBworld.addr, align 4
  store float %posError, float* %posError.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  store float %lowerLimit, float* %lowerLimit.addr, align 4
  store float %upperLimit, float* %upperLimit.addr, align 4
  %frombool = zext i1 %angConstraint to i8
  store i8 %frombool, i8* %angConstraint.addr, align 1
  store float %relaxation, float* %relaxation.addr, align 4
  %frombool1 = zext i1 %isFriction to i8
  store i8 %frombool1, i8* %isFriction.addr, align 1
  store float %desiredVelocity, float* %desiredVelocity.addr, align 4
  store float %cfmSlip, float* %cfmSlip.addr, align 4
  %this2 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %m_bodyA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this2, i32 0, i32 1
  %0 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA, align 4
  %1 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_multiBodyA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %1, i32 0, i32 23
  store %class.btMultiBody* %0, %class.btMultiBody** %m_multiBodyA, align 4
  %m_bodyB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this2, i32 0, i32 2
  %2 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB, align 4
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_multiBodyB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %3, i32 0, i32 26
  store %class.btMultiBody* %2, %class.btMultiBody** %m_multiBodyB, align 4
  %m_linkA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this2, i32 0, i32 3
  %4 = load i32, i32* %m_linkA, align 4
  %5 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkA3 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %5, i32 0, i32 24
  store i32 %4, i32* %m_linkA3, align 4
  %m_linkB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this2, i32 0, i32 4
  %6 = load i32, i32* %m_linkB, align 4
  %7 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkB4 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %7, i32 0, i32 27
  store i32 %6, i32* %m_linkB4, align 4
  %8 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_multiBodyA5 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %8, i32 0, i32 23
  %9 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyA5, align 4
  store %class.btMultiBody* %9, %class.btMultiBody** %multiBodyA, align 4
  %10 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_multiBodyB6 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %10, i32 0, i32 26
  %11 = load %class.btMultiBody*, %class.btMultiBody** %m_multiBodyB6, align 4
  store %class.btMultiBody* %11, %class.btMultiBody** %multiBodyB, align 4
  %12 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %tobool = icmp ne %class.btMultiBody* %12, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %13 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_solverBodyPool = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %13, i32 0, i32 6
  %14 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %m_solverBodyPool, align 4
  %15 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_solverBodyIdA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %15, i32 0, i32 22
  %16 = load i32, i32* %m_solverBodyIdA, align 4
  %call = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyE2atEi(%class.btAlignedObjectArray.16* %14, i32 %16)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btSolverBody* [ null, %cond.true ], [ %call, %cond.false ]
  store %struct.btSolverBody* %cond, %struct.btSolverBody** %bodyA, align 4
  %17 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %tobool7 = icmp ne %class.btMultiBody* %17, null
  br i1 %tobool7, label %cond.true8, label %cond.false9

cond.true8:                                       ; preds = %cond.end
  br label %cond.end12

cond.false9:                                      ; preds = %cond.end
  %18 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_solverBodyPool10 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %18, i32 0, i32 6
  %19 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %m_solverBodyPool10, align 4
  %20 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_solverBodyIdB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %20, i32 0, i32 25
  %21 = load i32, i32* %m_solverBodyIdB, align 4
  %call11 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyE2atEi(%class.btAlignedObjectArray.16* %19, i32 %21)
  br label %cond.end12

cond.end12:                                       ; preds = %cond.false9, %cond.true8
  %cond13 = phi %struct.btSolverBody* [ null, %cond.true8 ], [ %call11, %cond.false9 ]
  store %struct.btSolverBody* %cond13, %struct.btSolverBody** %bodyB, align 4
  %22 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %tobool14 = icmp ne %class.btMultiBody* %22, null
  br i1 %tobool14, label %cond.true15, label %cond.false16

cond.true15:                                      ; preds = %cond.end12
  br label %cond.end17

cond.false16:                                     ; preds = %cond.end12
  %23 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %23, i32 0, i32 12
  %24 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  br label %cond.end17

cond.end17:                                       ; preds = %cond.false16, %cond.true15
  %cond18 = phi %class.btRigidBody* [ null, %cond.true15 ], [ %24, %cond.false16 ]
  store %class.btRigidBody* %cond18, %class.btRigidBody** %rb0, align 4
  %25 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %tobool19 = icmp ne %class.btMultiBody* %25, null
  br i1 %tobool19, label %cond.true20, label %cond.false21

cond.true20:                                      ; preds = %cond.end17
  br label %cond.end23

cond.false21:                                     ; preds = %cond.end17
  %26 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %m_originalBody22 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %26, i32 0, i32 12
  %27 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody22, align 4
  br label %cond.end23

cond.end23:                                       ; preds = %cond.false21, %cond.true20
  %cond24 = phi %class.btRigidBody* [ null, %cond.true20 ], [ %27, %cond.false21 ]
  store %class.btRigidBody* %cond24, %class.btRigidBody** %rb1, align 4
  %call25 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %rel_pos1)
  %call26 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %rel_pos2)
  %28 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %tobool27 = icmp ne %struct.btSolverBody* %28, null
  br i1 %tobool27, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end23
  %29 = load %class.btVector3*, %class.btVector3** %posAworld.addr, align 4
  %30 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %call28 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK12btSolverBody17getWorldTransformEv(%struct.btSolverBody* %30)
  %call29 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call28)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %29, %class.btVector3* nonnull align 4 dereferenceable(16) %call29)
  %31 = bitcast %class.btVector3* %rel_pos1 to i8*
  %32 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %31, i8* align 4 %32, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end23
  %33 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %tobool30 = icmp ne %struct.btSolverBody* %33, null
  br i1 %tobool30, label %if.then31, label %if.end35

if.then31:                                        ; preds = %if.end
  %34 = load %class.btVector3*, %class.btVector3** %posBworld.addr, align 4
  %35 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %call33 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK12btSolverBody17getWorldTransformEv(%struct.btSolverBody* %35)
  %call34 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call33)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp32, %class.btVector3* nonnull align 4 dereferenceable(16) %34, %class.btVector3* nonnull align 4 dereferenceable(16) %call34)
  %36 = bitcast %class.btVector3* %rel_pos2 to i8*
  %37 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %36, i8* align 4 %37, i32 16, i1 false)
  br label %if.end35

if.end35:                                         ; preds = %if.then31, %if.end
  %38 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %tobool36 = icmp ne %class.btMultiBody* %38, null
  br i1 %tobool36, label %if.then37, label %if.else99

if.then37:                                        ; preds = %if.end35
  %39 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkA38 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %39, i32 0, i32 24
  %40 = load i32, i32* %m_linkA38, align 4
  %cmp = icmp slt i32 %40, 0
  br i1 %cmp, label %if.then39, label %if.else

if.then39:                                        ; preds = %if.then37
  %41 = load %class.btVector3*, %class.btVector3** %posAworld.addr, align 4
  %42 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %call41 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getBasePosEv(%class.btMultiBody* %42)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp40, %class.btVector3* nonnull align 4 dereferenceable(16) %41, %class.btVector3* nonnull align 4 dereferenceable(16) %call41)
  %43 = bitcast %class.btVector3* %rel_pos1 to i8*
  %44 = bitcast %class.btVector3* %ref.tmp40 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %43, i8* align 4 %44, i32 16, i1 false)
  br label %if.end46

if.else:                                          ; preds = %if.then37
  %45 = load %class.btVector3*, %class.btVector3** %posAworld.addr, align 4
  %46 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %47 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkA43 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %47, i32 0, i32 24
  %48 = load i32, i32* %m_linkA43, align 4
  %call44 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %46, i32 %48)
  %m_cachedWorldTransform = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call44, i32 0, i32 25
  %call45 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_cachedWorldTransform)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp42, %class.btVector3* nonnull align 4 dereferenceable(16) %45, %class.btVector3* nonnull align 4 dereferenceable(16) %call45)
  %49 = bitcast %class.btVector3* %rel_pos1 to i8*
  %50 = bitcast %class.btVector3* %ref.tmp42 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %49, i8* align 4 %50, i32 16, i1 false)
  br label %if.end46

if.end46:                                         ; preds = %if.else, %if.then39
  %51 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %call47 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %51)
  %add = add nsw i32 %call47, 6
  store i32 %add, i32* %ndofA, align 4
  %52 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %call48 = call i32 @_ZNK11btMultiBody14getCompanionIdEv(%class.btMultiBody* %52)
  %53 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelAindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %53, i32 0, i32 0
  store i32 %call48, i32* %m_deltaVelAindex, align 4
  %54 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelAindex49 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %54, i32 0, i32 0
  %55 = load i32, i32* %m_deltaVelAindex49, align 4
  %cmp50 = icmp slt i32 %55, 0
  br i1 %cmp50, label %if.then51, label %if.else60

if.then51:                                        ; preds = %if.end46
  %56 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_deltaVelocities = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %56, i32 0, i32 2
  %call52 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_deltaVelocities)
  %57 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelAindex53 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %57, i32 0, i32 0
  store i32 %call52, i32* %m_deltaVelAindex53, align 4
  %58 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %59 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelAindex54 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %59, i32 0, i32 0
  %60 = load i32, i32* %m_deltaVelAindex54, align 4
  call void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %58, i32 %60)
  %61 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_deltaVelocities55 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %61, i32 0, i32 2
  %62 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_deltaVelocities56 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %62, i32 0, i32 2
  %call57 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_deltaVelocities56)
  %63 = load i32, i32* %ndofA, align 4
  %add58 = add nsw i32 %call57, %63
  store float 0.000000e+00, float* %ref.tmp59, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_deltaVelocities55, i32 %add58, float* nonnull align 4 dereferenceable(4) %ref.tmp59)
  br label %if.end61

if.else60:                                        ; preds = %if.end46
  br label %if.end61

if.end61:                                         ; preds = %if.else60, %if.then51
  %64 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_jacobians = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %64, i32 0, i32 0
  %call62 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_jacobians)
  %65 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %65, i32 0, i32 1
  store i32 %call62, i32* %m_jacAindex, align 4
  %66 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_jacobians63 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %66, i32 0, i32 0
  %67 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_jacobians64 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %67, i32 0, i32 0
  %call65 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_jacobians64)
  %68 = load i32, i32* %ndofA, align 4
  %add66 = add nsw i32 %call65, %68
  store float 0.000000e+00, float* %ref.tmp67, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_jacobians63, i32 %add66, float* nonnull align 4 dereferenceable(4) %ref.tmp67)
  %69 = load float*, float** %jacOrgA.addr, align 4
  %tobool68 = icmp ne float* %69, null
  br i1 %tobool68, label %if.then69, label %if.else75

if.then69:                                        ; preds = %if.end61
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then69
  %70 = load i32, i32* %i, align 4
  %71 = load i32, i32* %ndofA, align 4
  %cmp70 = icmp slt i32 %70, %71
  br i1 %cmp70, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %72 = load float*, float** %jacOrgA.addr, align 4
  %73 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %72, i32 %73
  %74 = load float, float* %arrayidx, align 4
  %75 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_jacobians71 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %75, i32 0, i32 0
  %76 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex72 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %76, i32 0, i32 1
  %77 = load i32, i32* %m_jacAindex72, align 4
  %78 = load i32, i32* %i, align 4
  %add73 = add nsw i32 %77, %78
  %call74 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians71, i32 %add73)
  store float %74, float* %call74, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %79 = load i32, i32* %i, align 4
  %inc = add nsw i32 %79, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end80

if.else75:                                        ; preds = %if.end61
  %80 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_jacobians76 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %80, i32 0, i32 0
  %81 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex77 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %81, i32 0, i32 1
  %82 = load i32, i32* %m_jacAindex77, align 4
  %call78 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians76, i32 %82)
  store float* %call78, float** %jac1, align 4
  %83 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %84 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkA79 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %84, i32 0, i32 24
  %85 = load i32, i32* %m_linkA79, align 4
  %86 = load %class.btVector3*, %class.btVector3** %posAworld.addr, align 4
  %87 = load %class.btVector3*, %class.btVector3** %constraintNormalAng.addr, align 4
  %88 = load %class.btVector3*, %class.btVector3** %constraintNormalLin.addr, align 4
  %89 = load float*, float** %jac1, align 4
  %90 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %scratch_r = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %90, i32 0, i32 3
  %91 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %scratch_v = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %91, i32 0, i32 4
  %92 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %scratch_m = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %92, i32 0, i32 5
  call void @_ZNK11btMultiBody30fillConstraintJacobianMultiDofEiRK9btVector3S2_S2_PfR20btAlignedObjectArrayIfERS4_IS0_ERS4_I11btMatrix3x3E(%class.btMultiBody* %83, i32 %85, %class.btVector3* nonnull align 4 dereferenceable(16) %86, %class.btVector3* nonnull align 4 dereferenceable(16) %87, %class.btVector3* nonnull align 4 dereferenceable(16) %88, float* %89, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %scratch_r, %class.btAlignedObjectArray.8* nonnull align 4 dereferenceable(17) %scratch_v, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %scratch_m)
  br label %if.end80

if.end80:                                         ; preds = %if.else75, %for.end
  %93 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_deltaVelocitiesUnitImpulse = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %93, i32 0, i32 1
  %94 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_deltaVelocitiesUnitImpulse81 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %94, i32 0, i32 1
  %call82 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse81)
  %95 = load i32, i32* %ndofA, align 4
  %add83 = add nsw i32 %call82, %95
  store float 0.000000e+00, float* %ref.tmp84, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse, i32 %add83, float* nonnull align 4 dereferenceable(4) %ref.tmp84)
  %96 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_deltaVelocitiesUnitImpulse85 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %96, i32 0, i32 1
  %97 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex86 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %97, i32 0, i32 1
  %98 = load i32, i32* %m_jacAindex86, align 4
  %call87 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse85, i32 %98)
  store float* %call87, float** %delta, align 4
  %99 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %100 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_jacobians88 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %100, i32 0, i32 0
  %101 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex89 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %101, i32 0, i32 1
  %102 = load i32, i32* %m_jacAindex89, align 4
  %call90 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians88, i32 %102)
  %103 = load float*, float** %delta, align 4
  %104 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %scratch_r91 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %104, i32 0, i32 3
  %105 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %scratch_v92 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %105, i32 0, i32 4
  call void @_ZNK11btMultiBody30calcAccelerationDeltasMultiDofEPKfPfR20btAlignedObjectArrayIfERS3_I9btVector3E(%class.btMultiBody* %99, float* %call90, float* %103, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %scratch_r91, %class.btAlignedObjectArray.8* nonnull align 4 dereferenceable(17) %scratch_v92)
  %call93 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %torqueAxis0)
  %106 = load i8, i8* %angConstraint.addr, align 1
  %tobool94 = trunc i8 %106 to i1
  br i1 %tobool94, label %if.then95, label %if.else96

if.then95:                                        ; preds = %if.end80
  %107 = load %class.btVector3*, %class.btVector3** %constraintNormalAng.addr, align 4
  %108 = bitcast %class.btVector3* %torqueAxis0 to i8*
  %109 = bitcast %class.btVector3* %107 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %108, i8* align 4 %109, i32 16, i1 false)
  br label %if.end98

if.else96:                                        ; preds = %if.end80
  %110 = load %class.btVector3*, %class.btVector3** %constraintNormalLin.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp97, %class.btVector3* %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %110)
  %111 = bitcast %class.btVector3* %torqueAxis0 to i8*
  %112 = bitcast %class.btVector3* %ref.tmp97 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %111, i8* align 4 %112, i32 16, i1 false)
  br label %if.end98

if.end98:                                         ; preds = %if.else96, %if.then95
  %113 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %113, i32 0, i32 4
  %114 = bitcast %class.btVector3* %m_relpos1CrossNormal to i8*
  %115 = bitcast %class.btVector3* %torqueAxis0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %114, i8* align 4 %115, i32 16, i1 false)
  %116 = load %class.btVector3*, %class.btVector3** %constraintNormalLin.addr, align 4
  %117 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %117, i32 0, i32 5
  %118 = bitcast %class.btVector3* %m_contactNormal1 to i8*
  %119 = bitcast %class.btVector3* %116 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %118, i8* align 4 %119, i32 16, i1 false)
  br label %if.end121

if.else99:                                        ; preds = %if.end35
  %call101 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %torqueAxis0100)
  %120 = load i8, i8* %angConstraint.addr, align 1
  %tobool102 = trunc i8 %120 to i1
  br i1 %tobool102, label %if.then103, label %if.else104

if.then103:                                       ; preds = %if.else99
  %121 = load %class.btVector3*, %class.btVector3** %constraintNormalAng.addr, align 4
  %122 = bitcast %class.btVector3* %torqueAxis0100 to i8*
  %123 = bitcast %class.btVector3* %121 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %122, i8* align 4 %123, i32 16, i1 false)
  br label %if.end106

if.else104:                                       ; preds = %if.else99
  %124 = load %class.btVector3*, %class.btVector3** %constraintNormalLin.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp105, %class.btVector3* %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %124)
  %125 = bitcast %class.btVector3* %torqueAxis0100 to i8*
  %126 = bitcast %class.btVector3* %ref.tmp105 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %125, i8* align 4 %126, i32 16, i1 false)
  br label %if.end106

if.end106:                                        ; preds = %if.else104, %if.then103
  %127 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %tobool108 = icmp ne %class.btRigidBody* %127, null
  br i1 %tobool108, label %cond.true109, label %cond.false113

cond.true109:                                     ; preds = %if.end106
  %128 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call111 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %128)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp110, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call111, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis0100)
  %129 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call112 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %129)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp107, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp110, %class.btVector3* nonnull align 4 dereferenceable(16) %call112)
  br label %cond.end118

cond.false113:                                    ; preds = %if.end106
  store float 0.000000e+00, float* %ref.tmp114, align 4
  store float 0.000000e+00, float* %ref.tmp115, align 4
  store float 0.000000e+00, float* %ref.tmp116, align 4
  %call117 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp107, float* nonnull align 4 dereferenceable(4) %ref.tmp114, float* nonnull align 4 dereferenceable(4) %ref.tmp115, float* nonnull align 4 dereferenceable(4) %ref.tmp116)
  br label %cond.end118

cond.end118:                                      ; preds = %cond.false113, %cond.true109
  %130 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %130, i32 0, i32 8
  %131 = bitcast %class.btVector3* %m_angularComponentA to i8*
  %132 = bitcast %class.btVector3* %ref.tmp107 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %131, i8* align 4 %132, i32 16, i1 false)
  %133 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_relpos1CrossNormal119 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %133, i32 0, i32 4
  %134 = bitcast %class.btVector3* %m_relpos1CrossNormal119 to i8*
  %135 = bitcast %class.btVector3* %torqueAxis0100 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %134, i8* align 4 %135, i32 16, i1 false)
  %136 = load %class.btVector3*, %class.btVector3** %constraintNormalLin.addr, align 4
  %137 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal1120 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %137, i32 0, i32 5
  %138 = bitcast %class.btVector3* %m_contactNormal1120 to i8*
  %139 = bitcast %class.btVector3* %136 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %138, i8* align 4 %139, i32 16, i1 false)
  br label %if.end121

if.end121:                                        ; preds = %cond.end118, %if.end98
  %140 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %tobool122 = icmp ne %class.btMultiBody* %140, null
  br i1 %tobool122, label %if.then123, label %if.else206

if.then123:                                       ; preds = %if.end121
  %141 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkB124 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %141, i32 0, i32 27
  %142 = load i32, i32* %m_linkB124, align 4
  %cmp125 = icmp slt i32 %142, 0
  br i1 %cmp125, label %if.then126, label %if.else129

if.then126:                                       ; preds = %if.then123
  %143 = load %class.btVector3*, %class.btVector3** %posBworld.addr, align 4
  %144 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %call128 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getBasePosEv(%class.btMultiBody* %144)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp127, %class.btVector3* nonnull align 4 dereferenceable(16) %143, %class.btVector3* nonnull align 4 dereferenceable(16) %call128)
  %145 = bitcast %class.btVector3* %rel_pos2 to i8*
  %146 = bitcast %class.btVector3* %ref.tmp127 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %145, i8* align 4 %146, i32 16, i1 false)
  br label %if.end135

if.else129:                                       ; preds = %if.then123
  %147 = load %class.btVector3*, %class.btVector3** %posBworld.addr, align 4
  %148 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %149 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkB131 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %149, i32 0, i32 27
  %150 = load i32, i32* %m_linkB131, align 4
  %call132 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %148, i32 %150)
  %m_cachedWorldTransform133 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call132, i32 0, i32 25
  %call134 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_cachedWorldTransform133)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp130, %class.btVector3* nonnull align 4 dereferenceable(16) %147, %class.btVector3* nonnull align 4 dereferenceable(16) %call134)
  %151 = bitcast %class.btVector3* %rel_pos2 to i8*
  %152 = bitcast %class.btVector3* %ref.tmp130 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %151, i8* align 4 %152, i32 16, i1 false)
  br label %if.end135

if.end135:                                        ; preds = %if.else129, %if.then126
  %153 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %call136 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %153)
  %add137 = add nsw i32 %call136, 6
  store i32 %add137, i32* %ndofB, align 4
  %154 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %call138 = call i32 @_ZNK11btMultiBody14getCompanionIdEv(%class.btMultiBody* %154)
  %155 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelBindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %155, i32 0, i32 2
  store i32 %call138, i32* %m_deltaVelBindex, align 4
  %156 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelBindex139 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %156, i32 0, i32 2
  %157 = load i32, i32* %m_deltaVelBindex139, align 4
  %cmp140 = icmp slt i32 %157, 0
  br i1 %cmp140, label %if.then141, label %if.end151

if.then141:                                       ; preds = %if.end135
  %158 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_deltaVelocities142 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %158, i32 0, i32 2
  %call143 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_deltaVelocities142)
  %159 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelBindex144 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %159, i32 0, i32 2
  store i32 %call143, i32* %m_deltaVelBindex144, align 4
  %160 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %161 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_deltaVelBindex145 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %161, i32 0, i32 2
  %162 = load i32, i32* %m_deltaVelBindex145, align 4
  call void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %160, i32 %162)
  %163 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_deltaVelocities146 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %163, i32 0, i32 2
  %164 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_deltaVelocities147 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %164, i32 0, i32 2
  %call148 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_deltaVelocities147)
  %165 = load i32, i32* %ndofB, align 4
  %add149 = add nsw i32 %call148, %165
  store float 0.000000e+00, float* %ref.tmp150, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_deltaVelocities146, i32 %add149, float* nonnull align 4 dereferenceable(4) %ref.tmp150)
  br label %if.end151

if.end151:                                        ; preds = %if.then141, %if.end135
  %166 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_jacobians152 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %166, i32 0, i32 0
  %call153 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_jacobians152)
  %167 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %167, i32 0, i32 3
  store i32 %call153, i32* %m_jacBindex, align 4
  %168 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_jacobians154 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %168, i32 0, i32 0
  %169 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_jacobians155 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %169, i32 0, i32 0
  %call156 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_jacobians155)
  %170 = load i32, i32* %ndofB, align 4
  %add157 = add nsw i32 %call156, %170
  store float 0.000000e+00, float* %ref.tmp158, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_jacobians154, i32 %add157, float* nonnull align 4 dereferenceable(4) %ref.tmp158)
  %171 = load float*, float** %jacOrgB.addr, align 4
  %tobool159 = icmp ne float* %171, null
  br i1 %tobool159, label %if.then160, label %if.else173

if.then160:                                       ; preds = %if.end151
  store i32 0, i32* %i161, align 4
  br label %for.cond162

for.cond162:                                      ; preds = %for.inc170, %if.then160
  %172 = load i32, i32* %i161, align 4
  %173 = load i32, i32* %ndofB, align 4
  %cmp163 = icmp slt i32 %172, %173
  br i1 %cmp163, label %for.body164, label %for.end172

for.body164:                                      ; preds = %for.cond162
  %174 = load float*, float** %jacOrgB.addr, align 4
  %175 = load i32, i32* %i161, align 4
  %arrayidx165 = getelementptr inbounds float, float* %174, i32 %175
  %176 = load float, float* %arrayidx165, align 4
  %177 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_jacobians166 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %177, i32 0, i32 0
  %178 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex167 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %178, i32 0, i32 3
  %179 = load i32, i32* %m_jacBindex167, align 4
  %180 = load i32, i32* %i161, align 4
  %add168 = add nsw i32 %179, %180
  %call169 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians166, i32 %add168)
  store float %176, float* %call169, align 4
  br label %for.inc170

for.inc170:                                       ; preds = %for.body164
  %181 = load i32, i32* %i161, align 4
  %inc171 = add nsw i32 %181, 1
  store i32 %inc171, i32* %i161, align 4
  br label %for.cond162

for.end172:                                       ; preds = %for.cond162
  br label %if.end183

if.else173:                                       ; preds = %if.end151
  %182 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %183 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_linkB174 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %183, i32 0, i32 27
  %184 = load i32, i32* %m_linkB174, align 4
  %185 = load %class.btVector3*, %class.btVector3** %posBworld.addr, align 4
  %186 = load %class.btVector3*, %class.btVector3** %constraintNormalAng.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp175, %class.btVector3* nonnull align 4 dereferenceable(16) %186)
  %187 = load %class.btVector3*, %class.btVector3** %constraintNormalLin.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp176, %class.btVector3* nonnull align 4 dereferenceable(16) %187)
  %188 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_jacobians177 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %188, i32 0, i32 0
  %189 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex178 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %189, i32 0, i32 3
  %190 = load i32, i32* %m_jacBindex178, align 4
  %call179 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians177, i32 %190)
  %191 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %scratch_r180 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %191, i32 0, i32 3
  %192 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %scratch_v181 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %192, i32 0, i32 4
  %193 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %scratch_m182 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %193, i32 0, i32 5
  call void @_ZNK11btMultiBody30fillConstraintJacobianMultiDofEiRK9btVector3S2_S2_PfR20btAlignedObjectArrayIfERS4_IS0_ERS4_I11btMatrix3x3E(%class.btMultiBody* %182, i32 %184, %class.btVector3* nonnull align 4 dereferenceable(16) %185, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp175, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp176, float* %call179, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %scratch_r180, %class.btAlignedObjectArray.8* nonnull align 4 dereferenceable(17) %scratch_v181, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %scratch_m182)
  br label %if.end183

if.end183:                                        ; preds = %if.else173, %for.end172
  %194 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_deltaVelocitiesUnitImpulse184 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %194, i32 0, i32 1
  %195 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_deltaVelocitiesUnitImpulse185 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %195, i32 0, i32 1
  %call186 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse185)
  %196 = load i32, i32* %ndofB, align 4
  %add187 = add nsw i32 %call186, %196
  store float 0.000000e+00, float* %ref.tmp188, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse184, i32 %add187, float* nonnull align 4 dereferenceable(4) %ref.tmp188)
  %197 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_deltaVelocitiesUnitImpulse190 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %197, i32 0, i32 1
  %198 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex191 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %198, i32 0, i32 3
  %199 = load i32, i32* %m_jacBindex191, align 4
  %call192 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse190, i32 %199)
  store float* %call192, float** %delta189, align 4
  %200 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %201 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_jacobians193 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %201, i32 0, i32 0
  %202 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex194 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %202, i32 0, i32 3
  %203 = load i32, i32* %m_jacBindex194, align 4
  %call195 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians193, i32 %203)
  %204 = load float*, float** %delta189, align 4
  %205 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %scratch_r196 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %205, i32 0, i32 3
  %206 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %scratch_v197 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %206, i32 0, i32 4
  call void @_ZNK11btMultiBody30calcAccelerationDeltasMultiDofEPKfPfR20btAlignedObjectArrayIfERS3_I9btVector3E(%class.btMultiBody* %200, float* %call195, float* %204, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %scratch_r196, %class.btAlignedObjectArray.8* nonnull align 4 dereferenceable(17) %scratch_v197)
  %call198 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %torqueAxis1)
  %207 = load i8, i8* %angConstraint.addr, align 1
  %tobool199 = trunc i8 %207 to i1
  br i1 %tobool199, label %if.then200, label %if.else201

if.then200:                                       ; preds = %if.end183
  %208 = load %class.btVector3*, %class.btVector3** %constraintNormalAng.addr, align 4
  %209 = bitcast %class.btVector3* %torqueAxis1 to i8*
  %210 = bitcast %class.btVector3* %208 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %209, i8* align 4 %210, i32 16, i1 false)
  br label %if.end203

if.else201:                                       ; preds = %if.end183
  %211 = load %class.btVector3*, %class.btVector3** %constraintNormalLin.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp202, %class.btVector3* %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %211)
  %212 = bitcast %class.btVector3* %torqueAxis1 to i8*
  %213 = bitcast %class.btVector3* %ref.tmp202 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %212, i8* align 4 %213, i32 16, i1 false)
  br label %if.end203

if.end203:                                        ; preds = %if.else201, %if.then200
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp204, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis1)
  %214 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %214, i32 0, i32 6
  %215 = bitcast %class.btVector3* %m_relpos2CrossNormal to i8*
  %216 = bitcast %class.btVector3* %ref.tmp204 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %215, i8* align 4 %216, i32 16, i1 false)
  %217 = load %class.btVector3*, %class.btVector3** %constraintNormalLin.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp205, %class.btVector3* nonnull align 4 dereferenceable(16) %217)
  %218 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %218, i32 0, i32 7
  %219 = bitcast %class.btVector3* %m_contactNormal2 to i8*
  %220 = bitcast %class.btVector3* %ref.tmp205 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %219, i8* align 4 %220, i32 16, i1 false)
  br label %if.end231

if.else206:                                       ; preds = %if.end121
  %call208 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %torqueAxis1207)
  %221 = load i8, i8* %angConstraint.addr, align 1
  %tobool209 = trunc i8 %221 to i1
  br i1 %tobool209, label %if.then210, label %if.else211

if.then210:                                       ; preds = %if.else206
  %222 = load %class.btVector3*, %class.btVector3** %constraintNormalAng.addr, align 4
  %223 = bitcast %class.btVector3* %torqueAxis1207 to i8*
  %224 = bitcast %class.btVector3* %222 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %223, i8* align 4 %224, i32 16, i1 false)
  br label %if.end213

if.else211:                                       ; preds = %if.else206
  %225 = load %class.btVector3*, %class.btVector3** %constraintNormalLin.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp212, %class.btVector3* %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %225)
  %226 = bitcast %class.btVector3* %torqueAxis1207 to i8*
  %227 = bitcast %class.btVector3* %ref.tmp212 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %226, i8* align 4 %227, i32 16, i1 false)
  br label %if.end213

if.end213:                                        ; preds = %if.else211, %if.then210
  %228 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %tobool215 = icmp ne %class.btRigidBody* %228, null
  br i1 %tobool215, label %cond.true216, label %cond.false221

cond.true216:                                     ; preds = %if.end213
  %229 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call218 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %229)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp219, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis1207)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp217, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call218, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp219)
  %230 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call220 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %230)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp214, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp217, %class.btVector3* nonnull align 4 dereferenceable(16) %call220)
  br label %cond.end226

cond.false221:                                    ; preds = %if.end213
  store float 0.000000e+00, float* %ref.tmp222, align 4
  store float 0.000000e+00, float* %ref.tmp223, align 4
  store float 0.000000e+00, float* %ref.tmp224, align 4
  %call225 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp214, float* nonnull align 4 dereferenceable(4) %ref.tmp222, float* nonnull align 4 dereferenceable(4) %ref.tmp223, float* nonnull align 4 dereferenceable(4) %ref.tmp224)
  br label %cond.end226

cond.end226:                                      ; preds = %cond.false221, %cond.true216
  %231 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %231, i32 0, i32 9
  %232 = bitcast %class.btVector3* %m_angularComponentB to i8*
  %233 = bitcast %class.btVector3* %ref.tmp214 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %232, i8* align 4 %233, i32 16, i1 false)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp227, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis1207)
  %234 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_relpos2CrossNormal228 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %234, i32 0, i32 6
  %235 = bitcast %class.btVector3* %m_relpos2CrossNormal228 to i8*
  %236 = bitcast %class.btVector3* %ref.tmp227 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %235, i8* align 4 %236, i32 16, i1 false)
  %237 = load %class.btVector3*, %class.btVector3** %constraintNormalLin.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp229, %class.btVector3* nonnull align 4 dereferenceable(16) %237)
  %238 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal2230 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %238, i32 0, i32 7
  %239 = bitcast %class.btVector3* %m_contactNormal2230 to i8*
  %240 = bitcast %class.btVector3* %ref.tmp229 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %239, i8* align 4 %240, i32 16, i1 false)
  br label %if.end231

if.end231:                                        ; preds = %cond.end226, %if.end203
  %call232 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vec)
  store float 0.000000e+00, float* %denom0, align 4
  store float 0.000000e+00, float* %denom1, align 4
  store float* null, float** %jacB, align 4
  store float* null, float** %jacA, align 4
  store float* null, float** %deltaVelA, align 4
  store float* null, float** %deltaVelB, align 4
  store i32 0, i32* %ndofA233, align 4
  %241 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %tobool234 = icmp ne %class.btMultiBody* %241, null
  br i1 %tobool234, label %if.then235, label %if.else254

if.then235:                                       ; preds = %if.end231
  %242 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %call236 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %242)
  %add237 = add nsw i32 %call236, 6
  store i32 %add237, i32* %ndofA233, align 4
  %243 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_jacobians238 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %243, i32 0, i32 0
  %244 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex239 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %244, i32 0, i32 1
  %245 = load i32, i32* %m_jacAindex239, align 4
  %call240 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians238, i32 %245)
  store float* %call240, float** %jacA, align 4
  %246 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_deltaVelocitiesUnitImpulse241 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %246, i32 0, i32 1
  %247 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex242 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %247, i32 0, i32 1
  %248 = load i32, i32* %m_jacAindex242, align 4
  %call243 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse241, i32 %248)
  store float* %call243, float** %deltaVelA, align 4
  store i32 0, i32* %i244, align 4
  br label %for.cond245

for.cond245:                                      ; preds = %for.inc251, %if.then235
  %249 = load i32, i32* %i244, align 4
  %250 = load i32, i32* %ndofA233, align 4
  %cmp246 = icmp slt i32 %249, %250
  br i1 %cmp246, label %for.body247, label %for.end253

for.body247:                                      ; preds = %for.cond245
  %251 = load float*, float** %jacA, align 4
  %252 = load i32, i32* %i244, align 4
  %arrayidx248 = getelementptr inbounds float, float* %251, i32 %252
  %253 = load float, float* %arrayidx248, align 4
  store float %253, float* %j, align 4
  %254 = load float*, float** %deltaVelA, align 4
  %255 = load i32, i32* %i244, align 4
  %arrayidx249 = getelementptr inbounds float, float* %254, i32 %255
  %256 = load float, float* %arrayidx249, align 4
  store float %256, float* %l, align 4
  %257 = load float, float* %j, align 4
  %258 = load float, float* %l, align 4
  %mul = fmul float %257, %258
  %259 = load float, float* %denom0, align 4
  %add250 = fadd float %259, %mul
  store float %add250, float* %denom0, align 4
  br label %for.inc251

for.inc251:                                       ; preds = %for.body247
  %260 = load i32, i32* %i244, align 4
  %inc252 = add nsw i32 %260, 1
  store i32 %inc252, i32* %i244, align 4
  br label %for.cond245

for.end253:                                       ; preds = %for.cond245
  br label %if.end270

if.else254:                                       ; preds = %if.end231
  %261 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %tobool255 = icmp ne %class.btRigidBody* %261, null
  br i1 %tobool255, label %if.then256, label %if.end269

if.then256:                                       ; preds = %if.else254
  %262 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentA258 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %262, i32 0, i32 8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp257, %class.btVector3* %m_angularComponentA258, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1)
  %263 = bitcast %class.btVector3* %vec to i8*
  %264 = bitcast %class.btVector3* %ref.tmp257 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %263, i8* align 4 %264, i32 16, i1 false)
  %265 = load i8, i8* %angConstraint.addr, align 1
  %tobool259 = trunc i8 %265 to i1
  br i1 %tobool259, label %if.then260, label %if.else264

if.then260:                                       ; preds = %if.then256
  %266 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call261 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %266)
  %267 = load %class.btVector3*, %class.btVector3** %constraintNormalAng.addr, align 4
  %call262 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %267, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %add263 = fadd float %call261, %call262
  store float %add263, float* %denom0, align 4
  br label %if.end268

if.else264:                                       ; preds = %if.then256
  %268 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call265 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %268)
  %269 = load %class.btVector3*, %class.btVector3** %constraintNormalLin.addr, align 4
  %call266 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %269, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %add267 = fadd float %call265, %call266
  store float %add267, float* %denom0, align 4
  br label %if.end268

if.end268:                                        ; preds = %if.else264, %if.then260
  br label %if.end269

if.end269:                                        ; preds = %if.end268, %if.else254
  br label %if.end270

if.end270:                                        ; preds = %if.end269, %for.end253
  %270 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %tobool271 = icmp ne %class.btMultiBody* %270, null
  br i1 %tobool271, label %if.then272, label %if.else295

if.then272:                                       ; preds = %if.end270
  %271 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %call274 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %271)
  %add275 = add nsw i32 %call274, 6
  store i32 %add275, i32* %ndofB273, align 4
  %272 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_jacobians276 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %272, i32 0, i32 0
  %273 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex277 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %273, i32 0, i32 3
  %274 = load i32, i32* %m_jacBindex277, align 4
  %call278 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians276, i32 %274)
  store float* %call278, float** %jacB, align 4
  %275 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_deltaVelocitiesUnitImpulse279 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %275, i32 0, i32 1
  %276 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex280 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %276, i32 0, i32 3
  %277 = load i32, i32* %m_jacBindex280, align 4
  %call281 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_deltaVelocitiesUnitImpulse279, i32 %277)
  store float* %call281, float** %deltaVelB, align 4
  store i32 0, i32* %i282, align 4
  br label %for.cond283

for.cond283:                                      ; preds = %for.inc292, %if.then272
  %278 = load i32, i32* %i282, align 4
  %279 = load i32, i32* %ndofB273, align 4
  %cmp284 = icmp slt i32 %278, %279
  br i1 %cmp284, label %for.body285, label %for.end294

for.body285:                                      ; preds = %for.cond283
  %280 = load float*, float** %jacB, align 4
  %281 = load i32, i32* %i282, align 4
  %arrayidx287 = getelementptr inbounds float, float* %280, i32 %281
  %282 = load float, float* %arrayidx287, align 4
  store float %282, float* %j286, align 4
  %283 = load float*, float** %deltaVelB, align 4
  %284 = load i32, i32* %i282, align 4
  %arrayidx289 = getelementptr inbounds float, float* %283, i32 %284
  %285 = load float, float* %arrayidx289, align 4
  store float %285, float* %l288, align 4
  %286 = load float, float* %j286, align 4
  %287 = load float, float* %l288, align 4
  %mul290 = fmul float %286, %287
  %288 = load float, float* %denom1, align 4
  %add291 = fadd float %288, %mul290
  store float %add291, float* %denom1, align 4
  br label %for.inc292

for.inc292:                                       ; preds = %for.body285
  %289 = load i32, i32* %i282, align 4
  %inc293 = add nsw i32 %289, 1
  store i32 %inc293, i32* %i282, align 4
  br label %for.cond283

for.end294:                                       ; preds = %for.cond283
  br label %if.end312

if.else295:                                       ; preds = %if.end270
  %290 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %tobool296 = icmp ne %class.btRigidBody* %290, null
  br i1 %tobool296, label %if.then297, label %if.end311

if.then297:                                       ; preds = %if.else295
  %291 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentB300 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %291, i32 0, i32 9
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp299, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB300)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp298, %class.btVector3* %ref.tmp299, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2)
  %292 = bitcast %class.btVector3* %vec to i8*
  %293 = bitcast %class.btVector3* %ref.tmp298 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %292, i8* align 4 %293, i32 16, i1 false)
  %294 = load i8, i8* %angConstraint.addr, align 1
  %tobool301 = trunc i8 %294 to i1
  br i1 %tobool301, label %if.then302, label %if.else306

if.then302:                                       ; preds = %if.then297
  %295 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call303 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %295)
  %296 = load %class.btVector3*, %class.btVector3** %constraintNormalAng.addr, align 4
  %call304 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %296, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %add305 = fadd float %call303, %call304
  store float %add305, float* %denom1, align 4
  br label %if.end310

if.else306:                                       ; preds = %if.then297
  %297 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call307 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %297)
  %298 = load %class.btVector3*, %class.btVector3** %constraintNormalLin.addr, align 4
  %call308 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %298, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %add309 = fadd float %call307, %call308
  store float %add309, float* %denom1, align 4
  br label %if.end310

if.end310:                                        ; preds = %if.else306, %if.then302
  br label %if.end311

if.end311:                                        ; preds = %if.end310, %if.else295
  br label %if.end312

if.end312:                                        ; preds = %if.end311, %for.end294
  %299 = load float, float* %denom0, align 4
  %300 = load float, float* %denom1, align 4
  %add313 = fadd float %299, %300
  store float %add313, float* %d, align 4
  %301 = load float, float* %d, align 4
  %cmp314 = fcmp ogt float %301, 0x3E80000000000000
  br i1 %cmp314, label %if.then315, label %if.else316

if.then315:                                       ; preds = %if.end312
  %302 = load float, float* %relaxation.addr, align 4
  %303 = load float, float* %d, align 4
  %div = fdiv float %302, %303
  %304 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %304, i32 0, i32 13
  store float %div, float* %m_jacDiagABInv, align 4
  br label %if.end318

if.else316:                                       ; preds = %if.end312
  %305 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv317 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %305, i32 0, i32 13
  store float 0.000000e+00, float* %m_jacDiagABInv317, align 4
  br label %if.end318

if.end318:                                        ; preds = %if.else316, %if.then315
  %306 = load i8, i8* %isFriction.addr, align 1
  %tobool319 = trunc i8 %306 to i1
  br i1 %tobool319, label %cond.true320, label %cond.false321

cond.true320:                                     ; preds = %if.end318
  br label %cond.end322

cond.false321:                                    ; preds = %if.end318
  %307 = load float, float* %posError.addr, align 4
  br label %cond.end322

cond.end322:                                      ; preds = %cond.false321, %cond.true320
  %cond323 = phi float [ 0.000000e+00, %cond.true320 ], [ %307, %cond.false321 ]
  store float %cond323, float* %penetration, align 4
  store float 0.000000e+00, float* %rel_vel, align 4
  store i32 0, i32* %ndofA324, align 4
  store i32 0, i32* %ndofB325, align 4
  %call326 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel1)
  %call327 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel2)
  %308 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %tobool328 = icmp ne %class.btMultiBody* %308, null
  br i1 %tobool328, label %if.then329, label %if.else348

if.then329:                                       ; preds = %cond.end322
  %309 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %call330 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %309)
  %add331 = add nsw i32 %call330, 6
  store i32 %add331, i32* %ndofA324, align 4
  %310 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_jacobians333 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %310, i32 0, i32 0
  %311 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacAindex334 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %311, i32 0, i32 1
  %312 = load i32, i32* %m_jacAindex334, align 4
  %call335 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians333, i32 %312)
  store float* %call335, float** %jacA332, align 4
  store i32 0, i32* %i336, align 4
  br label %for.cond337

for.cond337:                                      ; preds = %for.inc345, %if.then329
  %313 = load i32, i32* %i336, align 4
  %314 = load i32, i32* %ndofA324, align 4
  %cmp338 = icmp slt i32 %313, %314
  br i1 %cmp338, label %for.body339, label %for.end347

for.body339:                                      ; preds = %for.cond337
  %315 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyA, align 4
  %call340 = call float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %315)
  %316 = load i32, i32* %i336, align 4
  %arrayidx341 = getelementptr inbounds float, float* %call340, i32 %316
  %317 = load float, float* %arrayidx341, align 4
  %318 = load float*, float** %jacA332, align 4
  %319 = load i32, i32* %i336, align 4
  %arrayidx342 = getelementptr inbounds float, float* %318, i32 %319
  %320 = load float, float* %arrayidx342, align 4
  %mul343 = fmul float %317, %320
  %321 = load float, float* %rel_vel, align 4
  %add344 = fadd float %321, %mul343
  store float %add344, float* %rel_vel, align 4
  br label %for.inc345

for.inc345:                                       ; preds = %for.body339
  %322 = load i32, i32* %i336, align 4
  %inc346 = add nsw i32 %322, 1
  store i32 %inc346, i32* %i336, align 4
  br label %for.cond337

for.end347:                                       ; preds = %for.cond337
  br label %if.end356

if.else348:                                       ; preds = %cond.end322
  %323 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %tobool349 = icmp ne %class.btRigidBody* %323, null
  br i1 %tobool349, label %if.then350, label %if.end355

if.then350:                                       ; preds = %if.else348
  %324 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp351, %class.btRigidBody* %324, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1)
  %325 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal1352 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %325, i32 0, i32 5
  %call353 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp351, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1352)
  %326 = load float, float* %rel_vel, align 4
  %add354 = fadd float %326, %call353
  store float %add354, float* %rel_vel, align 4
  br label %if.end355

if.end355:                                        ; preds = %if.then350, %if.else348
  br label %if.end356

if.end356:                                        ; preds = %if.end355, %for.end347
  %327 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %tobool357 = icmp ne %class.btMultiBody* %327, null
  br i1 %tobool357, label %if.then358, label %if.else377

if.then358:                                       ; preds = %if.end356
  %328 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %call359 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %328)
  %add360 = add nsw i32 %call359, 6
  store i32 %add360, i32* %ndofB325, align 4
  %329 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_jacobians362 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %329, i32 0, i32 0
  %330 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacBindex363 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %330, i32 0, i32 3
  %331 = load i32, i32* %m_jacBindex363, align 4
  %call364 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_jacobians362, i32 %331)
  store float* %call364, float** %jacB361, align 4
  store i32 0, i32* %i365, align 4
  br label %for.cond366

for.cond366:                                      ; preds = %for.inc374, %if.then358
  %332 = load i32, i32* %i365, align 4
  %333 = load i32, i32* %ndofB325, align 4
  %cmp367 = icmp slt i32 %332, %333
  br i1 %cmp367, label %for.body368, label %for.end376

for.body368:                                      ; preds = %for.cond366
  %334 = load %class.btMultiBody*, %class.btMultiBody** %multiBodyB, align 4
  %call369 = call float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %334)
  %335 = load i32, i32* %i365, align 4
  %arrayidx370 = getelementptr inbounds float, float* %call369, i32 %335
  %336 = load float, float* %arrayidx370, align 4
  %337 = load float*, float** %jacB361, align 4
  %338 = load i32, i32* %i365, align 4
  %arrayidx371 = getelementptr inbounds float, float* %337, i32 %338
  %339 = load float, float* %arrayidx371, align 4
  %mul372 = fmul float %336, %339
  %340 = load float, float* %rel_vel, align 4
  %add373 = fadd float %340, %mul372
  store float %add373, float* %rel_vel, align 4
  br label %for.inc374

for.inc374:                                       ; preds = %for.body368
  %341 = load i32, i32* %i365, align 4
  %inc375 = add nsw i32 %341, 1
  store i32 %inc375, i32* %i365, align 4
  br label %for.cond366

for.end376:                                       ; preds = %for.cond366
  br label %if.end385

if.else377:                                       ; preds = %if.end356
  %342 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %tobool378 = icmp ne %class.btRigidBody* %342, null
  br i1 %tobool378, label %if.then379, label %if.end384

if.then379:                                       ; preds = %if.else377
  %343 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp380, %class.btRigidBody* %343, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2)
  %344 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal2381 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %344, i32 0, i32 7
  %call382 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp380, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2381)
  %345 = load float, float* %rel_vel, align 4
  %add383 = fadd float %345, %call382
  store float %add383, float* %rel_vel, align 4
  br label %if.end384

if.end384:                                        ; preds = %if.then379, %if.else377
  br label %if.end385

if.end385:                                        ; preds = %if.end384, %for.end376
  %346 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_friction = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %346, i32 0, i32 12
  store float 0.000000e+00, float* %m_friction, align 4
  %347 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_appliedImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %347, i32 0, i32 11
  store float 0.000000e+00, float* %m_appliedImpulse, align 4
  %348 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_appliedPushImpulse = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %348, i32 0, i32 10
  store float 0.000000e+00, float* %m_appliedPushImpulse, align 4
  store float 0.000000e+00, float* %positionalError, align 4
  %349 = load float, float* %desiredVelocity.addr, align 4
  %350 = load float, float* %rel_vel, align 4
  %sub = fsub float %349, %350
  store float %sub, float* %velocityError, align 4
  %351 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %352 = bitcast %struct.btContactSolverInfo* %351 to %struct.btContactSolverInfoData*
  %m_erp2 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %352, i32 0, i32 9
  %353 = load float, float* %m_erp2, align 4
  store float %353, float* %erp, align 4
  %354 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %355 = bitcast %struct.btContactSolverInfo* %354 to %struct.btContactSolverInfoData*
  %m_erp = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %355, i32 0, i32 8
  %356 = load float, float* %m_erp, align 4
  store float %356, float* %erp, align 4
  %357 = load float, float* %penetration, align 4
  %fneg = fneg float %357
  %358 = load float, float* %erp, align 4
  %mul386 = fmul float %fneg, %358
  %359 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %360 = bitcast %struct.btContactSolverInfo* %359 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %360, i32 0, i32 3
  %361 = load float, float* %m_timeStep, align 4
  %div387 = fdiv float %mul386, %361
  store float %div387, float* %positionalError, align 4
  %362 = load float, float* %positionalError, align 4
  %363 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv388 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %363, i32 0, i32 13
  %364 = load float, float* %m_jacDiagABInv388, align 4
  %mul389 = fmul float %362, %364
  store float %mul389, float* %penetrationImpulse, align 4
  %365 = load float, float* %velocityError, align 4
  %366 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv390 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %366, i32 0, i32 13
  %367 = load float, float* %m_jacDiagABInv390, align 4
  %mul391 = fmul float %365, %367
  store float %mul391, float* %velocityImpulse, align 4
  %368 = load float, float* %penetrationImpulse, align 4
  %369 = load float, float* %velocityImpulse, align 4
  %add392 = fadd float %368, %369
  %370 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_rhs = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %370, i32 0, i32 14
  store float %add392, float* %m_rhs, align 4
  %371 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_rhsPenetration = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %371, i32 0, i32 18
  store float 0.000000e+00, float* %m_rhsPenetration, align 4
  %372 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_cfm = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %372, i32 0, i32 15
  store float 0.000000e+00, float* %m_cfm, align 4
  %373 = load float, float* %lowerLimit.addr, align 4
  %374 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_lowerLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %374, i32 0, i32 16
  store float %373, float* %m_lowerLimit, align 4
  %375 = load float, float* %upperLimit.addr, align 4
  %376 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %solverConstraint.addr, align 4
  %m_upperLimit = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %376, i32 0, i32 17
  store float %375, float* %m_upperLimit, align 4
  %377 = load float, float* %rel_vel, align 4
  ret float %377
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyE2atEi(%class.btAlignedObjectArray.16* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load %struct.btSolverBody*, %struct.btSolverBody** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %0, i32 %1
  ret %struct.btSolverBody* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK12btSolverBody17getWorldTransformEv(%struct.btSolverBody* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 0
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getBasePosEv(%class.btMultiBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_basePos = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 3
  ret %class.btVector3* %m_basePos
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %index.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 11
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %m_links, i32 %0)
  ret %struct.btMultibodyLink* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK11btMultiBody14getCompanionIdEv(%class.btMultiBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_companionId = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 29
  %0 = load i32, i32* %m_companionId, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMultiBody14setCompanionIdEi(%class.btMultiBody* %this, i32 %id) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %id.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  store i32 %id, i32* %id.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load i32, i32* %id.addr, align 4
  %m_companionId = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 29
  store i32 %0, i32* %m_companionId, align 4
  ret void
}

declare void @_ZNK11btMultiBody30fillConstraintJacobianMultiDofEiRK9btVector3S2_S2_PfR20btAlignedObjectArrayIfERS4_IS0_ERS4_I11btMatrix3x3E(%class.btMultiBody*, i32, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), float*, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray.8* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17)) #5

declare void @_ZNK11btMultiBody30calcAccelerationDeltasMultiDofEPKfPfR20btAlignedObjectArrayIfERS3_I9btVector3E(%class.btMultiBody*, float*, float*, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray.8* nonnull align 4 dereferenceable(17)) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  ret %class.btMatrix3x3* %m_invInertiaTensorWorld
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  ret %class.btVector3* %m_angularFactor
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_realBuf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 14
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_realBuf, i32 0)
  ret float* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %rel_pos.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %rel_pos, %class.btVector3** %rel_pos.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %0 = load %class.btVector3*, %class.btVector3** %rel_pos.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btMultiBodyConstraint11setFrameInBERK11btMatrix3x3(%class.btMultiBodyConstraint* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %frameInB) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %frameInB.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4
  store %class.btMatrix3x3* %frameInB, %class.btMatrix3x3** %frameInB.addr, align 4
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btMultiBodyConstraint11setPivotInBERK9btVector3(%class.btMultiBodyConstraint* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInB) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %pivotInB.addr = alloca %class.btVector3*, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4
  store %class.btVector3* %pivotInB, %class.btVector3** %pivotInB.addr, align 4
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  ret void
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %0, i32 %1
  ret %struct.btMultibodyLink* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.5* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  ret %class.btAlignedAllocator.5* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store float* null, float** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %3 = load float*, float** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %tobool = icmp ne float* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load float*, float** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.5* %m_allocator, float* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store float* null, float** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.5* %this, float* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca float*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store float* %ptr, float** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load float*, float** %ptr.addr, align 4
  %1 = bitcast float* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca float*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %1)
  %2 = bitcast i8* %call2 to float*
  store float* %2, float** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %3 = load float*, float** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, float* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load float*, float** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store float* %4, float** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.5* %m_allocator, i32 %1, float** null)
  %2 = bitcast float* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, float* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca float*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store float* %dest, float** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load float*, float** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  %5 = bitcast float* %arrayidx to i8*
  %6 = bitcast i8* %5 to float*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %7 = load float*, float** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds float, float* %7, i32 %8
  %9 = load float, float* %arrayidx2, align 4
  store float %9, float* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.5* %this, i32 %n, float** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca float**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store float** %hint, float*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to float*
  ret float* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #5

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btMultiBodyConstraint.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { cold noreturn nounwind }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
