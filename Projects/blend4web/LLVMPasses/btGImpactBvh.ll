; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/Gimpact/btGImpactBvh.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/Gimpact/btGImpactBvh.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btBvhTree = type { i32, %class.GIM_BVH_TREE_NODE_ARRAY }
%class.GIM_BVH_TREE_NODE_ARRAY = type { %class.btAlignedObjectArray.base, [3 x i8] }
%class.btAlignedObjectArray.base = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.GIM_BVH_TREE_NODE*, i8 }>
%class.btAlignedAllocator = type { i8 }
%class.GIM_BVH_TREE_NODE = type { %class.btAABB, i32 }
%class.btAABB = type { %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.GIM_BVH_DATA_ARRAY = type { %class.btAlignedObjectArray.base.3, [3 x i8] }
%class.btAlignedObjectArray.base.3 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.GIM_BVH_DATA*, i8 }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.GIM_BVH_DATA = type { %class.btAABB, i32 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.GIM_BVH_DATA*, i8, [3 x i8] }>
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.GIM_BVH_TREE_NODE*, i8, [3 x i8] }>
%class.btGImpactBvh = type { %class.btBvhTree, %class.btPrimitiveManagerBase* }
%class.btPrimitiveManagerBase = type { i32 (...)** }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btPairSet = type { %class.btAlignedObjectArray.base.11, [3 x i8] }
%class.btAlignedObjectArray.base.11 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %struct.GIM_PAIR*, i8 }>
%class.btAlignedAllocator.9 = type { i8 }
%struct.GIM_PAIR = type { i32, i32 }
%class.BT_BOX_BOX_TRANSFORM_CACHE = type { %class.btVector3, %class.btMatrix3x3, %class.btMatrix3x3 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %struct.GIM_PAIR*, i8, [3 x i8] }>

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZNK9btVector37maxAxisEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE4swapEii = comdat any

$_ZN9btBvhTree12setNodeBoundEiRK6btAABB = comdat any

$_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi = comdat any

$_ZN17GIM_BVH_TREE_NODE12setDataIndexEi = comdat any

$_ZN6btAABBC2Ev = comdat any

$_ZN6btAABB10invalidateEv = comdat any

$_ZN6btAABB5mergeERKS_ = comdat any

$_ZN17GIM_BVH_TREE_NODE14setEscapeIndexEi = comdat any

$_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE6resizeEiRKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4sizeEv = comdat any

$_ZN17GIM_BVH_TREE_NODEC2Ev = comdat any

$_ZNK12btGImpactBvh12getNodeCountEv = comdat any

$_ZNK12btGImpactBvh10isLeafNodeEi = comdat any

$_ZNK12btGImpactBvh11getNodeDataEi = comdat any

$_ZN12btGImpactBvh12setNodeBoundEiRK6btAABB = comdat any

$_ZNK12btGImpactBvh11getLeftNodeEi = comdat any

$_ZNK12btGImpactBvh12getNodeBoundEiR6btAABB = comdat any

$_ZNK12btGImpactBvh12getRightNodeEi = comdat any

$_ZN18GIM_BVH_DATA_ARRAYC2Ev = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE6resizeEiRKS0_ = comdat any

$_ZN12GIM_BVH_DATAC2Ev = comdat any

$_ZN18GIM_BVH_DATA_ARRAYD2Ev = comdat any

$_ZNK6btAABB13has_collisionERKS_ = comdat any

$_ZN20btAlignedObjectArrayIiE9push_backERKi = comdat any

$_ZNK12btGImpactBvh18getEscapeNodeIndexEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZNK6btAABB11collide_rayERK9btVector3S2_ = comdat any

$_ZN26BT_BOX_BOX_TRANSFORM_CACHEC2Ev = comdat any

$_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK9btBvhTree12getNodeCountEv = comdat any

$_ZNK9btBvhTree10isLeafNodeEi = comdat any

$_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi = comdat any

$_ZNK17GIM_BVH_TREE_NODE10isLeafNodeEv = comdat any

$_ZNK9btBvhTree11getNodeDataEi = comdat any

$_ZNK17GIM_BVH_TREE_NODE12getDataIndexEv = comdat any

$_ZNK9btBvhTree11getLeftNodeEi = comdat any

$_ZNK9btBvhTree12getNodeBoundEiR6btAABB = comdat any

$_ZNK9btBvhTree12getRightNodeEi = comdat any

$_ZNK17GIM_BVH_TREE_NODE14getEscapeIndexEv = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEC2Ev = comdat any

$_ZN18btAlignedAllocatorI12GIM_BVH_DATALj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE4initEv = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAED2Ev = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI12GIM_BVH_DATALj16EE10deallocateEPS0_ = comdat any

$_ZNK9btBvhTree18getEscapeNodeIndexEi = comdat any

$_ZNK6btAABB17get_center_extendER9btVector3S1_ = comdat any

$_Z6btFabsf = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZNK11btTransform7inverseEv = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZN26BT_BOX_BOX_TRANSFORM_CACHE20calc_absolute_matrixEv = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_Z15_node_collisionP12btGImpactBvhS0_RK26BT_BOX_BOX_TRANSFORM_CACHEiib = comdat any

$_ZN9btPairSet9push_pairEii = comdat any

$_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb = comdat any

$_Z15bt_mat3_dot_colRK11btMatrix3x3RK9btVector3i = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE9push_backERKS0_ = comdat any

$_ZN8GIM_PAIRC2Eii = comdat any

$_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI8GIM_PAIRE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE9allocSizeEi = comdat any

$_ZN8GIM_PAIRC2ERKS_ = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI8GIM_PAIRE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI8GIM_PAIRLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI8GIM_PAIRLj16EE10deallocateEPS0_ = comdat any

$_ZN12GIM_BVH_DATAC2ERKS_ = comdat any

$_ZN6btAABBC2ERKS_ = comdat any

$_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE7reserveEi = comdat any

$_ZN17GIM_BVH_TREE_NODEC2ERKS_ = comdat any

$_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI17GIM_BVH_TREE_NODELj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI17GIM_BVH_TREE_NODELj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI12GIM_BVH_DATALj16EE8allocateEiPPKS0_ = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIiE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btGImpactBvh.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN9btBvhTree20_calc_splitting_axisER18GIM_BVH_DATA_ARRAYii(%class.btBvhTree* %this, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %primitive_boxes, i32 %startIndex, i32 %endIndex) #2 {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %primitive_boxes.addr = alloca %class.GIM_BVH_DATA_ARRAY*, align 4
  %startIndex.addr = alloca i32, align 4
  %endIndex.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %means = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %variance = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %numIndices = alloca i32, align 4
  %center = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca float, align 4
  %center19 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %diff2 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca float, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4
  store %class.GIM_BVH_DATA_ARRAY* %primitive_boxes, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  store i32 %startIndex, i32* %startIndex.addr, align 4
  store i32 %endIndex, i32* %endIndex.addr, align 4
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %means, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 0.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %variance, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %0 = load i32, i32* %endIndex.addr, align 4
  %1 = load i32, i32* %startIndex.addr, align 4
  %sub = sub nsw i32 %0, %1
  store i32 %sub, i32* %numIndices, align 4
  %2 = load i32, i32* %startIndex.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %endIndex.addr, align 4
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  store float 5.000000e-01, float* %ref.tmp8, align 4
  %5 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %6 = bitcast %class.GIM_BVH_DATA_ARRAY* %5 to %class.btAlignedObjectArray.0*
  %7 = load i32, i32* %i, align 4
  %call10 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %6, i32 %7)
  %m_bound = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call10, i32 0, i32 0
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %m_bound, i32 0, i32 1
  %8 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %9 = bitcast %class.GIM_BVH_DATA_ARRAY* %8 to %class.btAlignedObjectArray.0*
  %10 = load i32, i32* %i, align 4
  %call11 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %9, i32 %10)
  %m_bound12 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call11, i32 0, i32 0
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %m_bound12, i32 0, i32 0
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %center, float* nonnull align 4 dereferenceable(4) %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %means, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = load i32, i32* %numIndices, align 4
  %conv = sitofp i32 %12 to float
  %div = fdiv float 1.000000e+00, %conv
  store float %div, float* %ref.tmp14, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %means, float* nonnull align 4 dereferenceable(4) %ref.tmp14)
  %13 = load i32, i32* %startIndex.addr, align 4
  store i32 %13, i32* %i, align 4
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc30, %for.end
  %14 = load i32, i32* %i, align 4
  %15 = load i32, i32* %endIndex.addr, align 4
  %cmp17 = icmp slt i32 %14, %15
  br i1 %cmp17, label %for.body18, label %for.end32

for.body18:                                       ; preds = %for.cond16
  store float 5.000000e-01, float* %ref.tmp20, align 4
  %16 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %17 = bitcast %class.GIM_BVH_DATA_ARRAY* %16 to %class.btAlignedObjectArray.0*
  %18 = load i32, i32* %i, align 4
  %call22 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %17, i32 %18)
  %m_bound23 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call22, i32 0, i32 0
  %m_max24 = getelementptr inbounds %class.btAABB, %class.btAABB* %m_bound23, i32 0, i32 1
  %19 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %20 = bitcast %class.GIM_BVH_DATA_ARRAY* %19 to %class.btAlignedObjectArray.0*
  %21 = load i32, i32* %i, align 4
  %call25 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %20, i32 %21)
  %m_bound26 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call25, i32 0, i32 0
  %m_min27 = getelementptr inbounds %class.btAABB, %class.btAABB* %m_bound26, i32 0, i32 0
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max24, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min27)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %center19, float* nonnull align 4 dereferenceable(4) %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %diff2, %class.btVector3* nonnull align 4 dereferenceable(16) %center19, %class.btVector3* nonnull align 4 dereferenceable(16) %means)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp28, %class.btVector3* nonnull align 4 dereferenceable(16) %diff2, %class.btVector3* nonnull align 4 dereferenceable(16) %diff2)
  %22 = bitcast %class.btVector3* %diff2 to i8*
  %23 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 16, i1 false)
  %call29 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %variance, %class.btVector3* nonnull align 4 dereferenceable(16) %diff2)
  br label %for.inc30

for.inc30:                                        ; preds = %for.body18
  %24 = load i32, i32* %i, align 4
  %inc31 = add nsw i32 %24, 1
  store i32 %inc31, i32* %i, align 4
  br label %for.cond16

for.end32:                                        ; preds = %for.cond16
  %25 = load i32, i32* %numIndices, align 4
  %conv34 = sitofp i32 %25 to float
  %sub35 = fsub float %conv34, 1.000000e+00
  %div36 = fdiv float 1.000000e+00, %sub35
  store float %div36, float* %ref.tmp33, align 4
  %call37 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %variance, float* nonnull align 4 dereferenceable(4) %ref.tmp33)
  %call38 = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %variance)
  ret i32 %call38
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %0, i32 %1
  ret %struct.GIM_BVH_DATA* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %2 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %3 = load float, float* %arrayidx7, align 4
  %cmp8 = fcmp olt float %2, %3
  %4 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 0
  %5 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %6 = load float, float* %arrayidx12, align 4
  %cmp13 = fcmp olt float %5, %6
  %7 = zext i1 %cmp13 to i64
  %cond14 = select i1 %cmp13, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond15 = phi i32 [ %cond, %cond.true ], [ %cond14, %cond.false ]
  ret i32 %cond15
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN9btBvhTree30_sort_and_calc_splitting_indexER18GIM_BVH_DATA_ARRAYiii(%class.btBvhTree* %this, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %primitive_boxes, i32 %startIndex, i32 %endIndex, i32 %splitAxis) #2 {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %primitive_boxes.addr = alloca %class.GIM_BVH_DATA_ARRAY*, align 4
  %startIndex.addr = alloca i32, align 4
  %endIndex.addr = alloca i32, align 4
  %splitAxis.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %splitIndex = alloca i32, align 4
  %numIndices = alloca i32, align 4
  %splitValue = alloca float, align 4
  %means = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %center = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca float, align 4
  %center16 = alloca %class.btVector3, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca %class.btVector3, align 4
  %rangeBalancedIndices = alloca i32, align 4
  %unbalanced = alloca i8, align 1
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4
  store %class.GIM_BVH_DATA_ARRAY* %primitive_boxes, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  store i32 %startIndex, i32* %startIndex.addr, align 4
  store i32 %endIndex, i32* %endIndex.addr, align 4
  store i32 %splitAxis, i32* %splitAxis.addr, align 4
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %0 = load i32, i32* %startIndex.addr, align 4
  store i32 %0, i32* %splitIndex, align 4
  %1 = load i32, i32* %endIndex.addr, align 4
  %2 = load i32, i32* %startIndex.addr, align 4
  %sub = sub nsw i32 %1, %2
  store i32 %sub, i32* %numIndices, align 4
  store float 0.000000e+00, float* %splitValue, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %means, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = load i32, i32* %startIndex.addr, align 4
  store i32 %3, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %i, align 4
  %5 = load i32, i32* %endIndex.addr, align 4
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  store float 5.000000e-01, float* %ref.tmp4, align 4
  %6 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %7 = bitcast %class.GIM_BVH_DATA_ARRAY* %6 to %class.btAlignedObjectArray.0*
  %8 = load i32, i32* %i, align 4
  %call6 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %7, i32 %8)
  %m_bound = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call6, i32 0, i32 0
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %m_bound, i32 0, i32 1
  %9 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %10 = bitcast %class.GIM_BVH_DATA_ARRAY* %9 to %class.btAlignedObjectArray.0*
  %11 = load i32, i32* %i, align 4
  %call7 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %10, i32 %11)
  %m_bound8 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call7, i32 0, i32 0
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %m_bound8, i32 0, i32 0
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %center, float* nonnull align 4 dereferenceable(4) %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %means, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = load i32, i32* %numIndices, align 4
  %conv = sitofp i32 %13 to float
  %div = fdiv float 1.000000e+00, %conv
  store float %div, float* %ref.tmp10, align 4
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %means, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %means)
  %14 = load i32, i32* %splitAxis.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %call12, i32 %14
  %15 = load float, float* %arrayidx, align 4
  store float %15, float* %splitValue, align 4
  %16 = load i32, i32* %startIndex.addr, align 4
  store i32 %16, i32* %i, align 4
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc29, %for.end
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %endIndex.addr, align 4
  %cmp14 = icmp slt i32 %17, %18
  br i1 %cmp14, label %for.body15, label %for.end31

for.body15:                                       ; preds = %for.cond13
  store float 5.000000e-01, float* %ref.tmp17, align 4
  %19 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %20 = bitcast %class.GIM_BVH_DATA_ARRAY* %19 to %class.btAlignedObjectArray.0*
  %21 = load i32, i32* %i, align 4
  %call19 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %20, i32 %21)
  %m_bound20 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call19, i32 0, i32 0
  %m_max21 = getelementptr inbounds %class.btAABB, %class.btAABB* %m_bound20, i32 0, i32 1
  %22 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %23 = bitcast %class.GIM_BVH_DATA_ARRAY* %22 to %class.btAlignedObjectArray.0*
  %24 = load i32, i32* %i, align 4
  %call22 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %23, i32 %24)
  %m_bound23 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call22, i32 0, i32 0
  %m_min24 = getelementptr inbounds %class.btAABB, %class.btAABB* %m_bound23, i32 0, i32 0
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp18, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max21, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min24)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %center16, float* nonnull align 4 dereferenceable(4) %ref.tmp17, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp18)
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center16)
  %25 = load i32, i32* %splitAxis.addr, align 4
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 %25
  %26 = load float, float* %arrayidx26, align 4
  %27 = load float, float* %splitValue, align 4
  %cmp27 = fcmp ogt float %26, %27
  br i1 %cmp27, label %if.then, label %if.end

if.then:                                          ; preds = %for.body15
  %28 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %29 = bitcast %class.GIM_BVH_DATA_ARRAY* %28 to %class.btAlignedObjectArray.0*
  %30 = load i32, i32* %i, align 4
  %31 = load i32, i32* %splitIndex, align 4
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE4swapEii(%class.btAlignedObjectArray.0* %29, i32 %30, i32 %31)
  %32 = load i32, i32* %splitIndex, align 4
  %inc28 = add nsw i32 %32, 1
  store i32 %inc28, i32* %splitIndex, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body15
  br label %for.inc29

for.inc29:                                        ; preds = %if.end
  %33 = load i32, i32* %i, align 4
  %inc30 = add nsw i32 %33, 1
  store i32 %inc30, i32* %i, align 4
  br label %for.cond13

for.end31:                                        ; preds = %for.cond13
  %34 = load i32, i32* %numIndices, align 4
  %div32 = sdiv i32 %34, 3
  store i32 %div32, i32* %rangeBalancedIndices, align 4
  %35 = load i32, i32* %splitIndex, align 4
  %36 = load i32, i32* %startIndex.addr, align 4
  %37 = load i32, i32* %rangeBalancedIndices, align 4
  %add = add nsw i32 %36, %37
  %cmp33 = icmp sle i32 %35, %add
  br i1 %cmp33, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %for.end31
  %38 = load i32, i32* %splitIndex, align 4
  %39 = load i32, i32* %endIndex.addr, align 4
  %sub34 = sub nsw i32 %39, 1
  %40 = load i32, i32* %rangeBalancedIndices, align 4
  %sub35 = sub nsw i32 %sub34, %40
  %cmp36 = icmp sge i32 %38, %sub35
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %for.end31
  %41 = phi i1 [ true, %for.end31 ], [ %cmp36, %lor.rhs ]
  %frombool = zext i1 %41 to i8
  store i8 %frombool, i8* %unbalanced, align 1
  %42 = load i8, i8* %unbalanced, align 1
  %tobool = trunc i8 %42 to i1
  br i1 %tobool, label %if.then37, label %if.end39

if.then37:                                        ; preds = %lor.end
  %43 = load i32, i32* %startIndex.addr, align 4
  %44 = load i32, i32* %numIndices, align 4
  %shr = ashr i32 %44, 1
  %add38 = add nsw i32 %43, %shr
  store i32 %add38, i32* %splitIndex, align 4
  br label %if.end39

if.end39:                                         ; preds = %if.then37, %lor.end
  %45 = load i32, i32* %splitIndex, align 4
  ret i32 %45
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE4swapEii(%class.btAlignedObjectArray.0* %this, i32 %index0, i32 %index1) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %struct.GIM_BVH_DATA, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %0, i32 %1
  %call = call %struct.GIM_BVH_DATA* @_ZN12GIM_BVH_DATAC2ERKS_(%struct.GIM_BVH_DATA* %temp, %struct.GIM_BVH_DATA* nonnull align 4 dereferenceable(36) %arrayidx)
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data2, align 4
  %3 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %2, i32 %3
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %4 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data4, align 4
  %5 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %4, i32 %5
  %6 = bitcast %struct.GIM_BVH_DATA* %arrayidx5 to i8*
  %7 = bitcast %struct.GIM_BVH_DATA* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 36, i1 false)
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data6, align 4
  %9 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %8, i32 %9
  %10 = bitcast %struct.GIM_BVH_DATA* %arrayidx7 to i8*
  %11 = bitcast %struct.GIM_BVH_DATA* %temp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 36, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN9btBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii(%class.btBvhTree* %this, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %primitive_boxes, i32 %startIndex, i32 %endIndex) #2 {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %primitive_boxes.addr = alloca %class.GIM_BVH_DATA_ARRAY*, align 4
  %startIndex.addr = alloca i32, align 4
  %endIndex.addr = alloca i32, align 4
  %curIndex = alloca i32, align 4
  %splitIndex = alloca i32, align 4
  %node_bound = alloca %class.btAABB, align 4
  %i = alloca i32, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4
  store %class.GIM_BVH_DATA_ARRAY* %primitive_boxes, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  store i32 %startIndex, i32* %startIndex.addr, align 4
  store i32 %endIndex, i32* %endIndex.addr, align 4
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %m_num_nodes = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_num_nodes, align 4
  store i32 %0, i32* %curIndex, align 4
  %m_num_nodes2 = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 0
  %1 = load i32, i32* %m_num_nodes2, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %m_num_nodes2, align 4
  %2 = load i32, i32* %endIndex.addr, align 4
  %3 = load i32, i32* %startIndex.addr, align 4
  %sub = sub nsw i32 %2, %3
  %cmp = icmp eq i32 %sub, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load i32, i32* %curIndex, align 4
  %5 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %6 = bitcast %class.GIM_BVH_DATA_ARRAY* %5 to %class.btAlignedObjectArray.0*
  %7 = load i32, i32* %startIndex.addr, align 4
  %call = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %6, i32 %7)
  %m_bound = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call, i32 0, i32 0
  call void @_ZN9btBvhTree12setNodeBoundEiRK6btAABB(%class.btBvhTree* %this1, i32 %4, %class.btAABB* nonnull align 4 dereferenceable(32) %m_bound)
  %m_node_array = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %8 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array to %class.btAlignedObjectArray*
  %9 = load i32, i32* %curIndex, align 4
  %call3 = call nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %8, i32 %9)
  %10 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %11 = bitcast %class.GIM_BVH_DATA_ARRAY* %10 to %class.btAlignedObjectArray.0*
  %12 = load i32, i32* %startIndex.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %11, i32 %12)
  %m_data = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call4, i32 0, i32 1
  %13 = load i32, i32* %m_data, align 4
  call void @_ZN17GIM_BVH_TREE_NODE12setDataIndexEi(%class.GIM_BVH_TREE_NODE* %call3, i32 %13)
  br label %return

if.end:                                           ; preds = %entry
  %14 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %15 = load i32, i32* %startIndex.addr, align 4
  %16 = load i32, i32* %endIndex.addr, align 4
  %call5 = call i32 @_ZN9btBvhTree20_calc_splitting_axisER18GIM_BVH_DATA_ARRAYii(%class.btBvhTree* %this1, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %14, i32 %15, i32 %16)
  store i32 %call5, i32* %splitIndex, align 4
  %17 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %18 = load i32, i32* %startIndex.addr, align 4
  %19 = load i32, i32* %endIndex.addr, align 4
  %20 = load i32, i32* %splitIndex, align 4
  %call6 = call i32 @_ZN9btBvhTree30_sort_and_calc_splitting_indexER18GIM_BVH_DATA_ARRAYiii(%class.btBvhTree* %this1, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %17, i32 %18, i32 %19, i32 %20)
  store i32 %call6, i32* %splitIndex, align 4
  %call7 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %node_bound)
  call void @_ZN6btAABB10invalidateEv(%class.btAABB* %node_bound)
  %21 = load i32, i32* %startIndex.addr, align 4
  store i32 %21, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %22 = load i32, i32* %i, align 4
  %23 = load i32, i32* %endIndex.addr, align 4
  %cmp8 = icmp slt i32 %22, %23
  br i1 %cmp8, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %24 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %25 = bitcast %class.GIM_BVH_DATA_ARRAY* %24 to %class.btAlignedObjectArray.0*
  %26 = load i32, i32* %i, align 4
  %call9 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %25, i32 %26)
  %m_bound10 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call9, i32 0, i32 0
  call void @_ZN6btAABB5mergeERKS_(%class.btAABB* %node_bound, %class.btAABB* nonnull align 4 dereferenceable(32) %m_bound10)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %27 = load i32, i32* %i, align 4
  %inc11 = add nsw i32 %27, 1
  store i32 %inc11, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %28 = load i32, i32* %curIndex, align 4
  call void @_ZN9btBvhTree12setNodeBoundEiRK6btAABB(%class.btBvhTree* %this1, i32 %28, %class.btAABB* nonnull align 4 dereferenceable(32) %node_bound)
  %29 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %30 = load i32, i32* %startIndex.addr, align 4
  %31 = load i32, i32* %splitIndex, align 4
  call void @_ZN9btBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii(%class.btBvhTree* %this1, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %29, i32 %30, i32 %31)
  %32 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %33 = load i32, i32* %splitIndex, align 4
  %34 = load i32, i32* %endIndex.addr, align 4
  call void @_ZN9btBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii(%class.btBvhTree* %this1, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %32, i32 %33, i32 %34)
  %m_node_array12 = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %35 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array12 to %class.btAlignedObjectArray*
  %36 = load i32, i32* %curIndex, align 4
  %call13 = call nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %35, i32 %36)
  %m_num_nodes14 = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 0
  %37 = load i32, i32* %m_num_nodes14, align 4
  %38 = load i32, i32* %curIndex, align 4
  %sub15 = sub nsw i32 %37, %38
  call void @_ZN17GIM_BVH_TREE_NODE14setEscapeIndexEi(%class.GIM_BVH_TREE_NODE* %call13, i32 %sub15)
  br label %return

return:                                           ; preds = %for.end, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btBvhTree12setNodeBoundEiRK6btAABB(%class.btBvhTree* %this, i32 %nodeindex, %class.btAABB* nonnull align 4 dereferenceable(32) %bound) #2 comdat {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %nodeindex.addr = alloca i32, align 4
  %bound.addr = alloca %class.btAABB*, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4
  store i32 %nodeindex, i32* %nodeindex.addr, align 4
  store %class.btAABB* %bound, %class.btAABB** %bound.addr, align 4
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %0 = load %class.btAABB*, %class.btAABB** %bound.addr, align 4
  %m_node_array = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %1 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array to %class.btAlignedObjectArray*
  %2 = load i32, i32* %nodeindex.addr, align 4
  %call = call nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %1, i32 %2)
  %m_bound = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %call, i32 0, i32 0
  %3 = bitcast %class.btAABB* %m_bound to i8*
  %4 = bitcast %class.btAABB* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 32, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %0, i32 %1
  ret %class.GIM_BVH_TREE_NODE* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17GIM_BVH_TREE_NODE12setDataIndexEi(%class.GIM_BVH_TREE_NODE* %this, i32 %index) #1 comdat {
entry:
  %this.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  %index.addr = alloca i32, align 4
  store %class.GIM_BVH_TREE_NODE* %this, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %m_escapeIndexOrDataIndex = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %this1, i32 0, i32 1
  store i32 %0, i32* %m_escapeIndexOrDataIndex, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAABB*, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_min)
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_max)
  ret %class.btAABB* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN6btAABB10invalidateEv(%class.btAABB* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAABB*, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  store float 0x47EFFFFFE0000000, float* %arrayidx, align 4
  %m_min2 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call3 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 1
  store float 0x47EFFFFFE0000000, float* %arrayidx4, align 4
  %m_min5 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min5)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  store float 0x47EFFFFFE0000000, float* %arrayidx7, align 4
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 0
  store float 0xC7EFFFFFE0000000, float* %arrayidx9, align 4
  %m_max10 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max10)
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 1
  store float 0xC7EFFFFFE0000000, float* %arrayidx12, align 4
  %m_max13 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max13)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  store float 0xC7EFFFFFE0000000, float* %arrayidx15, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN6btAABB5mergeERKS_(%class.btAABB* %this, %class.btAABB* nonnull align 4 dereferenceable(32) %box) #2 comdat {
entry:
  %this.addr = alloca %class.btAABB*, align 4
  %box.addr = alloca %class.btAABB*, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4
  store %class.btAABB* %box, %class.btAABB** %box.addr, align 4
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_min2 = getelementptr inbounds %class.btAABB, %class.btAABB* %1, i32 0, i32 0
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 0
  %2 = load float, float* %arrayidx4, align 4
  %cmp = fcmp ogt float %0, %2
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %3 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_min5 = getelementptr inbounds %class.btAABB, %class.btAABB* %3, i32 0, i32 0
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min5)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 0
  %4 = load float, float* %arrayidx7, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_min8 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min8)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 0
  %5 = load float, float* %arrayidx10, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %4, %cond.true ], [ %5, %cond.false ]
  %m_min11 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min11)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 0
  store float %cond, float* %arrayidx13, align 4
  %m_min14 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min14)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 1
  %6 = load float, float* %arrayidx16, align 4
  %7 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_min17 = getelementptr inbounds %class.btAABB, %class.btAABB* %7, i32 0, i32 0
  %call18 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min17)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 1
  %8 = load float, float* %arrayidx19, align 4
  %cmp20 = fcmp ogt float %6, %8
  br i1 %cmp20, label %cond.true21, label %cond.false25

cond.true21:                                      ; preds = %cond.end
  %9 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_min22 = getelementptr inbounds %class.btAABB, %class.btAABB* %9, i32 0, i32 0
  %call23 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min22)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 1
  %10 = load float, float* %arrayidx24, align 4
  br label %cond.end29

cond.false25:                                     ; preds = %cond.end
  %m_min26 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call27 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min26)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 1
  %11 = load float, float* %arrayidx28, align 4
  br label %cond.end29

cond.end29:                                       ; preds = %cond.false25, %cond.true21
  %cond30 = phi float [ %10, %cond.true21 ], [ %11, %cond.false25 ]
  %m_min31 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call32 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min31)
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 1
  store float %cond30, float* %arrayidx33, align 4
  %m_min34 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min34)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 2
  %12 = load float, float* %arrayidx36, align 4
  %13 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_min37 = getelementptr inbounds %class.btAABB, %class.btAABB* %13, i32 0, i32 0
  %call38 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min37)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 2
  %14 = load float, float* %arrayidx39, align 4
  %cmp40 = fcmp ogt float %12, %14
  br i1 %cmp40, label %cond.true41, label %cond.false45

cond.true41:                                      ; preds = %cond.end29
  %15 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_min42 = getelementptr inbounds %class.btAABB, %class.btAABB* %15, i32 0, i32 0
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min42)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 2
  %16 = load float, float* %arrayidx44, align 4
  br label %cond.end49

cond.false45:                                     ; preds = %cond.end29
  %m_min46 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call47 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min46)
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 2
  %17 = load float, float* %arrayidx48, align 4
  br label %cond.end49

cond.end49:                                       ; preds = %cond.false45, %cond.true41
  %cond50 = phi float [ %16, %cond.true41 ], [ %17, %cond.false45 ]
  %m_min51 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call52 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min51)
  %arrayidx53 = getelementptr inbounds float, float* %call52, i32 2
  store float %cond50, float* %arrayidx53, align 4
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call54 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max)
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 0
  %18 = load float, float* %arrayidx55, align 4
  %19 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_max56 = getelementptr inbounds %class.btAABB, %class.btAABB* %19, i32 0, i32 1
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max56)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 0
  %20 = load float, float* %arrayidx58, align 4
  %cmp59 = fcmp olt float %18, %20
  br i1 %cmp59, label %cond.true60, label %cond.false64

cond.true60:                                      ; preds = %cond.end49
  %21 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_max61 = getelementptr inbounds %class.btAABB, %class.btAABB* %21, i32 0, i32 1
  %call62 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max61)
  %arrayidx63 = getelementptr inbounds float, float* %call62, i32 0
  %22 = load float, float* %arrayidx63, align 4
  br label %cond.end68

cond.false64:                                     ; preds = %cond.end49
  %m_max65 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max65)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 0
  %23 = load float, float* %arrayidx67, align 4
  br label %cond.end68

cond.end68:                                       ; preds = %cond.false64, %cond.true60
  %cond69 = phi float [ %22, %cond.true60 ], [ %23, %cond.false64 ]
  %m_max70 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call71 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max70)
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 0
  store float %cond69, float* %arrayidx72, align 4
  %m_max73 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call74 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max73)
  %arrayidx75 = getelementptr inbounds float, float* %call74, i32 1
  %24 = load float, float* %arrayidx75, align 4
  %25 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_max76 = getelementptr inbounds %class.btAABB, %class.btAABB* %25, i32 0, i32 1
  %call77 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max76)
  %arrayidx78 = getelementptr inbounds float, float* %call77, i32 1
  %26 = load float, float* %arrayidx78, align 4
  %cmp79 = fcmp olt float %24, %26
  br i1 %cmp79, label %cond.true80, label %cond.false84

cond.true80:                                      ; preds = %cond.end68
  %27 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_max81 = getelementptr inbounds %class.btAABB, %class.btAABB* %27, i32 0, i32 1
  %call82 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max81)
  %arrayidx83 = getelementptr inbounds float, float* %call82, i32 1
  %28 = load float, float* %arrayidx83, align 4
  br label %cond.end88

cond.false84:                                     ; preds = %cond.end68
  %m_max85 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max85)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 1
  %29 = load float, float* %arrayidx87, align 4
  br label %cond.end88

cond.end88:                                       ; preds = %cond.false84, %cond.true80
  %cond89 = phi float [ %28, %cond.true80 ], [ %29, %cond.false84 ]
  %m_max90 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call91 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max90)
  %arrayidx92 = getelementptr inbounds float, float* %call91, i32 1
  store float %cond89, float* %arrayidx92, align 4
  %m_max93 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call94 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max93)
  %arrayidx95 = getelementptr inbounds float, float* %call94, i32 2
  %30 = load float, float* %arrayidx95, align 4
  %31 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_max96 = getelementptr inbounds %class.btAABB, %class.btAABB* %31, i32 0, i32 1
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max96)
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 2
  %32 = load float, float* %arrayidx98, align 4
  %cmp99 = fcmp olt float %30, %32
  br i1 %cmp99, label %cond.true100, label %cond.false104

cond.true100:                                     ; preds = %cond.end88
  %33 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_max101 = getelementptr inbounds %class.btAABB, %class.btAABB* %33, i32 0, i32 1
  %call102 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max101)
  %arrayidx103 = getelementptr inbounds float, float* %call102, i32 2
  %34 = load float, float* %arrayidx103, align 4
  br label %cond.end108

cond.false104:                                    ; preds = %cond.end88
  %m_max105 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call106 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max105)
  %arrayidx107 = getelementptr inbounds float, float* %call106, i32 2
  %35 = load float, float* %arrayidx107, align 4
  br label %cond.end108

cond.end108:                                      ; preds = %cond.false104, %cond.true100
  %cond109 = phi float [ %34, %cond.true100 ], [ %35, %cond.false104 ]
  %m_max110 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call111 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max110)
  %arrayidx112 = getelementptr inbounds float, float* %call111, i32 2
  store float %cond109, float* %arrayidx112, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17GIM_BVH_TREE_NODE14setEscapeIndexEi(%class.GIM_BVH_TREE_NODE* %this, i32 %index) #1 comdat {
entry:
  %this.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  %index.addr = alloca i32, align 4
  store %class.GIM_BVH_TREE_NODE* %this, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %sub = sub nsw i32 0, %0
  %m_escapeIndexOrDataIndex = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %this1, i32 0, i32 1
  store i32 %sub, i32* %m_escapeIndexOrDataIndex, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN9btBvhTree10build_treeER18GIM_BVH_DATA_ARRAY(%class.btBvhTree* %this, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %primitive_boxes) #2 {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %primitive_boxes.addr = alloca %class.GIM_BVH_DATA_ARRAY*, align 4
  %ref.tmp = alloca %class.GIM_BVH_TREE_NODE, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4
  store %class.GIM_BVH_DATA_ARRAY* %primitive_boxes, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %m_num_nodes = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 0
  store i32 0, i32* %m_num_nodes, align 4
  %m_node_array = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %0 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array to %class.btAlignedObjectArray*
  %1 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %2 = bitcast %class.GIM_BVH_DATA_ARRAY* %1 to %class.btAlignedObjectArray.0*
  %call = call i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4sizeEv(%class.btAlignedObjectArray.0* %2)
  %mul = mul nsw i32 %call, 2
  %call2 = call %class.GIM_BVH_TREE_NODE* @_ZN17GIM_BVH_TREE_NODEC2Ev(%class.GIM_BVH_TREE_NODE* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE6resizeEiRKS0_(%class.btAlignedObjectArray* %0, i32 %mul, %class.GIM_BVH_TREE_NODE* nonnull align 4 dereferenceable(36) %ref.tmp)
  %3 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %4 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %primitive_boxes.addr, align 4
  %5 = bitcast %class.GIM_BVH_DATA_ARRAY* %4 to %class.btAlignedObjectArray.0*
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4sizeEv(%class.btAlignedObjectArray.0* %5)
  call void @_ZN9btBvhTree15_build_sub_treeER18GIM_BVH_DATA_ARRAYii(%class.btBvhTree* %this1, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %3, i32 0, i32 %call3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %class.GIM_BVH_TREE_NODE* nonnull align 4 dereferenceable(36) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.GIM_BVH_TREE_NODE* %fillData, %class.GIM_BVH_TREE_NODE** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end15

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %14 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %14, i32 %15
  %16 = bitcast %class.GIM_BVH_TREE_NODE* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.GIM_BVH_TREE_NODE*
  %18 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %fillData.addr, align 4
  %call11 = call %class.GIM_BVH_TREE_NODE* @_ZN17GIM_BVH_TREE_NODEC2ERKS_(%class.GIM_BVH_TREE_NODE* %17, %class.GIM_BVH_TREE_NODE* nonnull align 4 dereferenceable(36) %18)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %19 = load i32, i32* %i5, align 4
  %inc13 = add nsw i32 %19, 1
  store i32 %inc13, i32* %i5, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  br label %if.end15

if.end15:                                         ; preds = %for.end14, %for.end
  %20 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %20, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.GIM_BVH_TREE_NODE* @_ZN17GIM_BVH_TREE_NODEC2Ev(%class.GIM_BVH_TREE_NODE* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  store %class.GIM_BVH_TREE_NODE* %this, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  %this1 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  %m_bound = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %this1, i32 0, i32 0
  %call = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %m_bound)
  %m_escapeIndexOrDataIndex = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %this1, i32 0, i32 1
  store i32 0, i32* %m_escapeIndexOrDataIndex, align 4
  ret %class.GIM_BVH_TREE_NODE* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN12btGImpactBvh5refitEv(%class.btGImpactBvh* %this) #2 {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %nodecount = alloca i32, align 4
  %leafbox = alloca %class.btAABB, align 4
  %bound = alloca %class.btAABB, align 4
  %temp_box = alloca %class.btAABB, align 4
  %child_node = alloca i32, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %call = call i32 @_ZNK12btGImpactBvh12getNodeCountEv(%class.btGImpactBvh* %this1)
  store i32 %call, i32* %nodecount, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end14, %entry
  %0 = load i32, i32* %nodecount, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %nodecount, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load i32, i32* %nodecount, align 4
  %call2 = call zeroext i1 @_ZNK12btGImpactBvh10isLeafNodeEi(%class.btGImpactBvh* %this1, i32 %1)
  br i1 %call2, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %call3 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %leafbox)
  %m_primitive_manager = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 1
  %2 = load %class.btPrimitiveManagerBase*, %class.btPrimitiveManagerBase** %m_primitive_manager, align 4
  %3 = load i32, i32* %nodecount, align 4
  %call4 = call i32 @_ZNK12btGImpactBvh11getNodeDataEi(%class.btGImpactBvh* %this1, i32 %3)
  %4 = bitcast %class.btPrimitiveManagerBase* %2 to void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)***
  %vtable = load void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)**, void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)*** %4, align 4
  %vfn = getelementptr inbounds void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)*, void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)** %vtable, i64 4
  %5 = load void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)*, void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)** %vfn, align 4
  call void %5(%class.btPrimitiveManagerBase* %2, i32 %call4, %class.btAABB* nonnull align 4 dereferenceable(32) %leafbox)
  %6 = load i32, i32* %nodecount, align 4
  call void @_ZN12btGImpactBvh12setNodeBoundEiRK6btAABB(%class.btGImpactBvh* %this1, i32 %6, %class.btAABB* nonnull align 4 dereferenceable(32) %leafbox)
  br label %if.end14

if.else:                                          ; preds = %while.body
  %call5 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %bound)
  call void @_ZN6btAABB10invalidateEv(%class.btAABB* %bound)
  %call6 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %temp_box)
  %7 = load i32, i32* %nodecount, align 4
  %call7 = call i32 @_ZNK12btGImpactBvh11getLeftNodeEi(%class.btGImpactBvh* %this1, i32 %7)
  store i32 %call7, i32* %child_node, align 4
  %8 = load i32, i32* %child_node, align 4
  %tobool8 = icmp ne i32 %8, 0
  br i1 %tobool8, label %if.then9, label %if.end

if.then9:                                         ; preds = %if.else
  %9 = load i32, i32* %child_node, align 4
  call void @_ZNK12btGImpactBvh12getNodeBoundEiR6btAABB(%class.btGImpactBvh* %this1, i32 %9, %class.btAABB* nonnull align 4 dereferenceable(32) %temp_box)
  call void @_ZN6btAABB5mergeERKS_(%class.btAABB* %bound, %class.btAABB* nonnull align 4 dereferenceable(32) %temp_box)
  br label %if.end

if.end:                                           ; preds = %if.then9, %if.else
  %10 = load i32, i32* %nodecount, align 4
  %call10 = call i32 @_ZNK12btGImpactBvh12getRightNodeEi(%class.btGImpactBvh* %this1, i32 %10)
  store i32 %call10, i32* %child_node, align 4
  %11 = load i32, i32* %child_node, align 4
  %tobool11 = icmp ne i32 %11, 0
  br i1 %tobool11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %if.end
  %12 = load i32, i32* %child_node, align 4
  call void @_ZNK12btGImpactBvh12getNodeBoundEiR6btAABB(%class.btGImpactBvh* %this1, i32 %12, %class.btAABB* nonnull align 4 dereferenceable(32) %temp_box)
  call void @_ZN6btAABB5mergeERKS_(%class.btAABB* %bound, %class.btAABB* nonnull align 4 dereferenceable(32) %temp_box)
  br label %if.end13

if.end13:                                         ; preds = %if.then12, %if.end
  %13 = load i32, i32* %nodecount, align 4
  call void @_ZN12btGImpactBvh12setNodeBoundEiRK6btAABB(%class.btGImpactBvh* %this1, i32 %13, %class.btAABB* nonnull align 4 dereferenceable(32) %bound)
  br label %if.end14

if.end14:                                         ; preds = %if.end13, %if.then
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK12btGImpactBvh12getNodeCountEv(%class.btGImpactBvh* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 0
  %call = call i32 @_ZNK9btBvhTree12getNodeCountEv(%class.btBvhTree* %m_box_tree)
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK12btGImpactBvh10isLeafNodeEi(%class.btGImpactBvh* %this, i32 %nodeindex) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4
  store i32 %nodeindex, i32* %nodeindex.addr, align 4
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 0
  %0 = load i32, i32* %nodeindex.addr, align 4
  %call = call zeroext i1 @_ZNK9btBvhTree10isLeafNodeEi(%class.btBvhTree* %m_box_tree, i32 %0)
  ret i1 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK12btGImpactBvh11getNodeDataEi(%class.btGImpactBvh* %this, i32 %nodeindex) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4
  store i32 %nodeindex, i32* %nodeindex.addr, align 4
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 0
  %0 = load i32, i32* %nodeindex.addr, align 4
  %call = call i32 @_ZNK9btBvhTree11getNodeDataEi(%class.btBvhTree* %m_box_tree, i32 %0)
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btGImpactBvh12setNodeBoundEiRK6btAABB(%class.btGImpactBvh* %this, i32 %nodeindex, %class.btAABB* nonnull align 4 dereferenceable(32) %bound) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %nodeindex.addr = alloca i32, align 4
  %bound.addr = alloca %class.btAABB*, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4
  store i32 %nodeindex, i32* %nodeindex.addr, align 4
  store %class.btAABB* %bound, %class.btAABB** %bound.addr, align 4
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 0
  %0 = load i32, i32* %nodeindex.addr, align 4
  %1 = load %class.btAABB*, %class.btAABB** %bound.addr, align 4
  call void @_ZN9btBvhTree12setNodeBoundEiRK6btAABB(%class.btBvhTree* %m_box_tree, i32 %0, %class.btAABB* nonnull align 4 dereferenceable(32) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK12btGImpactBvh11getLeftNodeEi(%class.btGImpactBvh* %this, i32 %nodeindex) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4
  store i32 %nodeindex, i32* %nodeindex.addr, align 4
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 0
  %0 = load i32, i32* %nodeindex.addr, align 4
  %call = call i32 @_ZNK9btBvhTree11getLeftNodeEi(%class.btBvhTree* %m_box_tree, i32 %0)
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12btGImpactBvh12getNodeBoundEiR6btAABB(%class.btGImpactBvh* %this, i32 %nodeindex, %class.btAABB* nonnull align 4 dereferenceable(32) %bound) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %nodeindex.addr = alloca i32, align 4
  %bound.addr = alloca %class.btAABB*, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4
  store i32 %nodeindex, i32* %nodeindex.addr, align 4
  store %class.btAABB* %bound, %class.btAABB** %bound.addr, align 4
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 0
  %0 = load i32, i32* %nodeindex.addr, align 4
  %1 = load %class.btAABB*, %class.btAABB** %bound.addr, align 4
  call void @_ZNK9btBvhTree12getNodeBoundEiR6btAABB(%class.btBvhTree* %m_box_tree, i32 %0, %class.btAABB* nonnull align 4 dereferenceable(32) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK12btGImpactBvh12getRightNodeEi(%class.btGImpactBvh* %this, i32 %nodeindex) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4
  store i32 %nodeindex, i32* %nodeindex.addr, align 4
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 0
  %0 = load i32, i32* %nodeindex.addr, align 4
  %call = call i32 @_ZNK9btBvhTree12getRightNodeEi(%class.btBvhTree* %m_box_tree, i32 %0)
  ret i32 %call
}

; Function Attrs: noinline optnone
define hidden void @_ZN12btGImpactBvh8buildSetEv(%class.btGImpactBvh* %this) #2 {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %primitive_boxes = alloca %class.GIM_BVH_DATA_ARRAY, align 4
  %ref.tmp = alloca %struct.GIM_BVH_DATA, align 4
  %i = alloca i32, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %call = call %class.GIM_BVH_DATA_ARRAY* @_ZN18GIM_BVH_DATA_ARRAYC2Ev(%class.GIM_BVH_DATA_ARRAY* %primitive_boxes)
  %0 = bitcast %class.GIM_BVH_DATA_ARRAY* %primitive_boxes to %class.btAlignedObjectArray.0*
  %m_primitive_manager = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 1
  %1 = load %class.btPrimitiveManagerBase*, %class.btPrimitiveManagerBase** %m_primitive_manager, align 4
  %2 = bitcast %class.btPrimitiveManagerBase* %1 to i32 (%class.btPrimitiveManagerBase*)***
  %vtable = load i32 (%class.btPrimitiveManagerBase*)**, i32 (%class.btPrimitiveManagerBase*)*** %2, align 4
  %vfn = getelementptr inbounds i32 (%class.btPrimitiveManagerBase*)*, i32 (%class.btPrimitiveManagerBase*)** %vtable, i64 3
  %3 = load i32 (%class.btPrimitiveManagerBase*)*, i32 (%class.btPrimitiveManagerBase*)** %vfn, align 4
  %call2 = call i32 %3(%class.btPrimitiveManagerBase* %1)
  %4 = bitcast %struct.GIM_BVH_DATA* %ref.tmp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %4, i8 0, i32 36, i1 false)
  %call3 = call %struct.GIM_BVH_DATA* @_ZN12GIM_BVH_DATAC2Ev(%struct.GIM_BVH_DATA* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %0, i32 %call2, %struct.GIM_BVH_DATA* nonnull align 4 dereferenceable(36) %ref.tmp)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4
  %6 = bitcast %class.GIM_BVH_DATA_ARRAY* %primitive_boxes to %class.btAlignedObjectArray.0*
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4sizeEv(%class.btAlignedObjectArray.0* %6)
  %cmp = icmp slt i32 %5, %call4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_primitive_manager5 = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 1
  %7 = load %class.btPrimitiveManagerBase*, %class.btPrimitiveManagerBase** %m_primitive_manager5, align 4
  %8 = load i32, i32* %i, align 4
  %9 = bitcast %class.GIM_BVH_DATA_ARRAY* %primitive_boxes to %class.btAlignedObjectArray.0*
  %10 = load i32, i32* %i, align 4
  %call6 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %9, i32 %10)
  %m_bound = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call6, i32 0, i32 0
  %11 = bitcast %class.btPrimitiveManagerBase* %7 to void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)***
  %vtable7 = load void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)**, void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)*** %11, align 4
  %vfn8 = getelementptr inbounds void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)*, void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)** %vtable7, i64 4
  %12 = load void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)*, void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)** %vfn8, align 4
  call void %12(%class.btPrimitiveManagerBase* %7, i32 %8, %class.btAABB* nonnull align 4 dereferenceable(32) %m_bound)
  %13 = load i32, i32* %i, align 4
  %14 = bitcast %class.GIM_BVH_DATA_ARRAY* %primitive_boxes to %class.btAlignedObjectArray.0*
  %15 = load i32, i32* %i, align 4
  %call9 = call nonnull align 4 dereferenceable(36) %struct.GIM_BVH_DATA* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEixEi(%class.btAlignedObjectArray.0* %14, i32 %15)
  %m_data = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %call9, i32 0, i32 1
  store i32 %13, i32* %m_data, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %16 = load i32, i32* %i, align 4
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_box_tree = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 0
  call void @_ZN9btBvhTree10build_treeER18GIM_BVH_DATA_ARRAY(%class.btBvhTree* %m_box_tree, %class.GIM_BVH_DATA_ARRAY* nonnull align 4 dereferenceable(17) %primitive_boxes)
  %call10 = call %class.GIM_BVH_DATA_ARRAY* @_ZN18GIM_BVH_DATA_ARRAYD2Ev(%class.GIM_BVH_DATA_ARRAY* %primitive_boxes) #7
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.GIM_BVH_DATA_ARRAY* @_ZN18GIM_BVH_DATA_ARRAYC2Ev(%class.GIM_BVH_DATA_ARRAY* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.GIM_BVH_DATA_ARRAY*, align 4
  store %class.GIM_BVH_DATA_ARRAY* %this, %class.GIM_BVH_DATA_ARRAY** %this.addr, align 4
  %this1 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %this.addr, align 4
  %0 = bitcast %class.GIM_BVH_DATA_ARRAY* %this1 to %class.btAlignedObjectArray.0*
  %call = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEC2Ev(%class.btAlignedObjectArray.0* %0)
  ret %class.GIM_BVH_DATA_ARRAY* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %this, i32 %newsize, %struct.GIM_BVH_DATA* nonnull align 4 dereferenceable(36) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.GIM_BVH_DATA*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %struct.GIM_BVH_DATA* %fillData, %struct.GIM_BVH_DATA** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %5 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end15

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %14 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %14, i32 %15
  %16 = bitcast %struct.GIM_BVH_DATA* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %struct.GIM_BVH_DATA*
  %18 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %fillData.addr, align 4
  %call11 = call %struct.GIM_BVH_DATA* @_ZN12GIM_BVH_DATAC2ERKS_(%struct.GIM_BVH_DATA* %17, %struct.GIM_BVH_DATA* nonnull align 4 dereferenceable(36) %18)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %19 = load i32, i32* %i5, align 4
  %inc13 = add nsw i32 %19, 1
  store i32 %inc13, i32* %i5, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  br label %if.end15

if.end15:                                         ; preds = %for.end14, %for.end
  %20 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %20, i32* %m_size, align 4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.GIM_BVH_DATA* @_ZN12GIM_BVH_DATAC2Ev(%struct.GIM_BVH_DATA* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.GIM_BVH_DATA*, align 4
  store %struct.GIM_BVH_DATA* %this, %struct.GIM_BVH_DATA** %this.addr, align 4
  %this1 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %this.addr, align 4
  %m_bound = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %this1, i32 0, i32 0
  %call = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %m_bound)
  ret %struct.GIM_BVH_DATA* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.GIM_BVH_DATA_ARRAY* @_ZN18GIM_BVH_DATA_ARRAYD2Ev(%class.GIM_BVH_DATA_ARRAY* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.GIM_BVH_DATA_ARRAY*, align 4
  store %class.GIM_BVH_DATA_ARRAY* %this, %class.GIM_BVH_DATA_ARRAY** %this.addr, align 4
  %this1 = load %class.GIM_BVH_DATA_ARRAY*, %class.GIM_BVH_DATA_ARRAY** %this.addr, align 4
  %0 = bitcast %class.GIM_BVH_DATA_ARRAY* %this1 to %class.btAlignedObjectArray.0*
  %call = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAED2Ev(%class.btAlignedObjectArray.0* %0) #7
  ret %class.GIM_BVH_DATA_ARRAY* %this1
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZNK12btGImpactBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE(%class.btGImpactBvh* %this, %class.btAABB* nonnull align 4 dereferenceable(32) %box, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %collided_results) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %box.addr = alloca %class.btAABB*, align 4
  %collided_results.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %curIndex = alloca i32, align 4
  %numNodes = alloca i32, align 4
  %bound = alloca %class.btAABB, align 4
  %aabbOverlap = alloca i8, align 1
  %isleafnode = alloca i8, align 1
  %ref.tmp = alloca i32, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4
  store %class.btAABB* %box, %class.btAABB** %box.addr, align 4
  store %class.btAlignedObjectArray.4* %collided_results, %class.btAlignedObjectArray.4** %collided_results.addr, align 4
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  store i32 0, i32* %curIndex, align 4
  %call = call i32 @_ZNK12btGImpactBvh12getNodeCountEv(%class.btGImpactBvh* %this1)
  store i32 %call, i32* %numNodes, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end12, %entry
  %0 = load i32, i32* %curIndex, align 4
  %1 = load i32, i32* %numNodes, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call2 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %bound)
  %2 = load i32, i32* %curIndex, align 4
  call void @_ZNK12btGImpactBvh12getNodeBoundEiR6btAABB(%class.btGImpactBvh* %this1, i32 %2, %class.btAABB* nonnull align 4 dereferenceable(32) %bound)
  %3 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %call3 = call zeroext i1 @_ZNK6btAABB13has_collisionERKS_(%class.btAABB* %bound, %class.btAABB* nonnull align 4 dereferenceable(32) %3)
  %frombool = zext i1 %call3 to i8
  store i8 %frombool, i8* %aabbOverlap, align 1
  %4 = load i32, i32* %curIndex, align 4
  %call4 = call zeroext i1 @_ZNK12btGImpactBvh10isLeafNodeEi(%class.btGImpactBvh* %this1, i32 %4)
  %frombool5 = zext i1 %call4 to i8
  store i8 %frombool5, i8* %isleafnode, align 1
  %5 = load i8, i8* %isleafnode, align 1
  %tobool = trunc i8 %5 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %while.body
  %6 = load i8, i8* %aabbOverlap, align 1
  %tobool6 = trunc i8 %6 to i1
  br i1 %tobool6, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %7 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %collided_results.addr, align 4
  %8 = load i32, i32* %curIndex, align 4
  %call7 = call i32 @_ZNK12btGImpactBvh11getNodeDataEi(%class.btGImpactBvh* %this1, i32 %8)
  store i32 %call7, i32* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.4* %7, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %while.body
  %9 = load i8, i8* %aabbOverlap, align 1
  %tobool8 = trunc i8 %9 to i1
  br i1 %tobool8, label %if.then10, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %10 = load i8, i8* %isleafnode, align 1
  %tobool9 = trunc i8 %10 to i1
  br i1 %tobool9, label %if.then10, label %if.else

if.then10:                                        ; preds = %lor.lhs.false, %if.end
  %11 = load i32, i32* %curIndex, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %curIndex, align 4
  br label %if.end12

if.else:                                          ; preds = %lor.lhs.false
  %12 = load i32, i32* %curIndex, align 4
  %call11 = call i32 @_ZNK12btGImpactBvh18getEscapeNodeIndexEi(%class.btGImpactBvh* %this1, i32 %12)
  %13 = load i32, i32* %curIndex, align 4
  %add = add nsw i32 %13, %call11
  store i32 %add, i32* %curIndex, align 4
  br label %if.end12

if.end12:                                         ; preds = %if.else, %if.then10
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %14 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %collided_results.addr, align 4
  %call13 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %14)
  %cmp14 = icmp sgt i32 %call13, 0
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %while.end
  store i1 true, i1* %retval, align 1
  br label %return

if.end16:                                         ; preds = %while.end
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end16, %if.then15
  %15 = load i1, i1* %retval, align 1
  ret i1 %15
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK6btAABB13has_collisionERKS_(%class.btAABB* %this, %class.btAABB* nonnull align 4 dereferenceable(32) %other) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btAABB*, align 4
  %other.addr = alloca %class.btAABB*, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4
  store %class.btAABB* %other, %class.btAABB** %other.addr, align 4
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btAABB*, %class.btAABB** %other.addr, align 4
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %1, i32 0, i32 1
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %cmp = fcmp ogt float %0, %2
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %m_max4 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max4)
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 0
  %3 = load float, float* %arrayidx6, align 4
  %4 = load %class.btAABB*, %class.btAABB** %other.addr, align 4
  %m_min7 = getelementptr inbounds %class.btAABB, %class.btAABB* %4, i32 0, i32 0
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min7)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 0
  %5 = load float, float* %arrayidx9, align 4
  %cmp10 = fcmp olt float %3, %5
  br i1 %cmp10, label %if.then, label %lor.lhs.false11

lor.lhs.false11:                                  ; preds = %lor.lhs.false
  %m_min12 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min12)
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 1
  %6 = load float, float* %arrayidx14, align 4
  %7 = load %class.btAABB*, %class.btAABB** %other.addr, align 4
  %m_max15 = getelementptr inbounds %class.btAABB, %class.btAABB* %7, i32 0, i32 1
  %call16 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max15)
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 1
  %8 = load float, float* %arrayidx17, align 4
  %cmp18 = fcmp ogt float %6, %8
  br i1 %cmp18, label %if.then, label %lor.lhs.false19

lor.lhs.false19:                                  ; preds = %lor.lhs.false11
  %m_max20 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call21 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max20)
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 1
  %9 = load float, float* %arrayidx22, align 4
  %10 = load %class.btAABB*, %class.btAABB** %other.addr, align 4
  %m_min23 = getelementptr inbounds %class.btAABB, %class.btAABB* %10, i32 0, i32 0
  %call24 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min23)
  %arrayidx25 = getelementptr inbounds float, float* %call24, i32 1
  %11 = load float, float* %arrayidx25, align 4
  %cmp26 = fcmp olt float %9, %11
  br i1 %cmp26, label %if.then, label %lor.lhs.false27

lor.lhs.false27:                                  ; preds = %lor.lhs.false19
  %m_min28 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call29 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min28)
  %arrayidx30 = getelementptr inbounds float, float* %call29, i32 2
  %12 = load float, float* %arrayidx30, align 4
  %13 = load %class.btAABB*, %class.btAABB** %other.addr, align 4
  %m_max31 = getelementptr inbounds %class.btAABB, %class.btAABB* %13, i32 0, i32 1
  %call32 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max31)
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 2
  %14 = load float, float* %arrayidx33, align 4
  %cmp34 = fcmp ogt float %12, %14
  br i1 %cmp34, label %if.then, label %lor.lhs.false35

lor.lhs.false35:                                  ; preds = %lor.lhs.false27
  %m_max36 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call37 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max36)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 2
  %15 = load float, float* %arrayidx38, align 4
  %16 = load %class.btAABB*, %class.btAABB** %other.addr, align 4
  %m_min39 = getelementptr inbounds %class.btAABB, %class.btAABB* %16, i32 0, i32 0
  %call40 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min39)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 2
  %17 = load float, float* %arrayidx41, align 4
  %cmp42 = fcmp olt float %15, %17
  br i1 %cmp42, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false35, %lor.lhs.false27, %lor.lhs.false19, %lor.lhs.false11, %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false35
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %18 = load i1, i1* %retval, align 1
  ret i1 %18
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.4* %this, i32* nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Val.addr = alloca i32*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32* %_Val, i32** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIiE9allocSizeEi(%class.btAlignedObjectArray.4* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %1 = load i32*, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  %3 = bitcast i32* %arrayidx to i8*
  %4 = bitcast i8* %3 to i32*
  %5 = load i32*, i32** %_Val.addr, align 4
  %6 = load i32, i32* %5, align 4
  store i32 %6, i32* %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK12btGImpactBvh18getEscapeNodeIndexEi(%class.btGImpactBvh* %this, i32 %nodeindex) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4
  store i32 %nodeindex, i32* %nodeindex.addr, align 4
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactBvh, %class.btGImpactBvh* %this1, i32 0, i32 0
  %0 = load i32, i32* %nodeindex.addr, align 4
  %call = call i32 @_ZNK9btBvhTree18getEscapeNodeIndexEi(%class.btBvhTree* %m_box_tree, i32 %0)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZNK12btGImpactBvh8rayQueryERK9btVector3S2_R20btAlignedObjectArrayIiE(%class.btGImpactBvh* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %ray_dir, %class.btVector3* nonnull align 4 dereferenceable(16) %ray_origin, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %collided_results) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btGImpactBvh*, align 4
  %ray_dir.addr = alloca %class.btVector3*, align 4
  %ray_origin.addr = alloca %class.btVector3*, align 4
  %collided_results.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %curIndex = alloca i32, align 4
  %numNodes = alloca i32, align 4
  %bound = alloca %class.btAABB, align 4
  %aabbOverlap = alloca i8, align 1
  %isleafnode = alloca i8, align 1
  %ref.tmp = alloca i32, align 4
  store %class.btGImpactBvh* %this, %class.btGImpactBvh** %this.addr, align 4
  store %class.btVector3* %ray_dir, %class.btVector3** %ray_dir.addr, align 4
  store %class.btVector3* %ray_origin, %class.btVector3** %ray_origin.addr, align 4
  store %class.btAlignedObjectArray.4* %collided_results, %class.btAlignedObjectArray.4** %collided_results.addr, align 4
  %this1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %this.addr, align 4
  store i32 0, i32* %curIndex, align 4
  %call = call i32 @_ZNK12btGImpactBvh12getNodeCountEv(%class.btGImpactBvh* %this1)
  store i32 %call, i32* %numNodes, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end12, %entry
  %0 = load i32, i32* %curIndex, align 4
  %1 = load i32, i32* %numNodes, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call2 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %bound)
  %2 = load i32, i32* %curIndex, align 4
  call void @_ZNK12btGImpactBvh12getNodeBoundEiR6btAABB(%class.btGImpactBvh* %this1, i32 %2, %class.btAABB* nonnull align 4 dereferenceable(32) %bound)
  %3 = load %class.btVector3*, %class.btVector3** %ray_origin.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %ray_dir.addr, align 4
  %call3 = call zeroext i1 @_ZNK6btAABB11collide_rayERK9btVector3S2_(%class.btAABB* %bound, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %frombool = zext i1 %call3 to i8
  store i8 %frombool, i8* %aabbOverlap, align 1
  %5 = load i32, i32* %curIndex, align 4
  %call4 = call zeroext i1 @_ZNK12btGImpactBvh10isLeafNodeEi(%class.btGImpactBvh* %this1, i32 %5)
  %frombool5 = zext i1 %call4 to i8
  store i8 %frombool5, i8* %isleafnode, align 1
  %6 = load i8, i8* %isleafnode, align 1
  %tobool = trunc i8 %6 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %while.body
  %7 = load i8, i8* %aabbOverlap, align 1
  %tobool6 = trunc i8 %7 to i1
  br i1 %tobool6, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %8 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %collided_results.addr, align 4
  %9 = load i32, i32* %curIndex, align 4
  %call7 = call i32 @_ZNK12btGImpactBvh11getNodeDataEi(%class.btGImpactBvh* %this1, i32 %9)
  store i32 %call7, i32* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.4* %8, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %while.body
  %10 = load i8, i8* %aabbOverlap, align 1
  %tobool8 = trunc i8 %10 to i1
  br i1 %tobool8, label %if.then10, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %11 = load i8, i8* %isleafnode, align 1
  %tobool9 = trunc i8 %11 to i1
  br i1 %tobool9, label %if.then10, label %if.else

if.then10:                                        ; preds = %lor.lhs.false, %if.end
  %12 = load i32, i32* %curIndex, align 4
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %curIndex, align 4
  br label %if.end12

if.else:                                          ; preds = %lor.lhs.false
  %13 = load i32, i32* %curIndex, align 4
  %call11 = call i32 @_ZNK12btGImpactBvh18getEscapeNodeIndexEi(%class.btGImpactBvh* %this1, i32 %13)
  %14 = load i32, i32* %curIndex, align 4
  %add = add nsw i32 %14, %call11
  store i32 %add, i32* %curIndex, align 4
  br label %if.end12

if.end12:                                         ; preds = %if.else, %if.then10
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %15 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %collided_results.addr, align 4
  %call13 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %15)
  %cmp14 = icmp sgt i32 %call13, 0
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %while.end
  store i1 true, i1* %retval, align 1
  br label %return

if.end16:                                         ; preds = %while.end
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end16, %if.then15
  %16 = load i1, i1* %retval, align 1
  ret i1 %16
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK6btAABB11collide_rayERK9btVector3S2_(%class.btAABB* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vorigin, %class.btVector3* nonnull align 4 dereferenceable(16) %vdir) #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btAABB*, align 4
  %vorigin.addr = alloca %class.btVector3*, align 4
  %vdir.addr = alloca %class.btVector3*, align 4
  %extents = alloca %class.btVector3, align 4
  %center = alloca %class.btVector3, align 4
  %Dx = alloca float, align 4
  %Dy = alloca float, align 4
  %Dz = alloca float, align 4
  %f = alloca float, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4
  store %class.btVector3* %vorigin, %class.btVector3** %vorigin.addr, align 4
  store %class.btVector3* %vdir, %class.btVector3** %vdir.addr, align 4
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %extents)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %center)
  call void @_ZNK6btAABB17get_center_extendER9btVector3S1_(%class.btAABB* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extents)
  %0 = load %class.btVector3*, %class.btVector3** %vorigin.addr, align 4
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call3, i32 0
  %1 = load float, float* %arrayidx, align 4
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 0
  %2 = load float, float* %arrayidx5, align 4
  %sub = fsub float %1, %2
  store float %sub, float* %Dx, align 4
  %3 = load float, float* %Dx, align 4
  %call6 = call float @_Z6btFabsf(float %3)
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %extents)
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 0
  %4 = load float, float* %arrayidx8, align 4
  %cmp = fcmp ogt float %call6, %4
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %5 = load float, float* %Dx, align 4
  %6 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 0
  %7 = load float, float* %arrayidx10, align 4
  %mul = fmul float %5, %7
  %cmp11 = fcmp oge float %mul, 0.000000e+00
  br i1 %cmp11, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %8 = load %class.btVector3*, %class.btVector3** %vorigin.addr, align 4
  %call12 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 1
  %9 = load float, float* %arrayidx13, align 4
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 1
  %10 = load float, float* %arrayidx15, align 4
  %sub16 = fsub float %9, %10
  store float %sub16, float* %Dy, align 4
  %11 = load float, float* %Dy, align 4
  %call17 = call float @_Z6btFabsf(float %11)
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %extents)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 1
  %12 = load float, float* %arrayidx19, align 4
  %cmp20 = fcmp ogt float %call17, %12
  br i1 %cmp20, label %land.lhs.true21, label %if.end27

land.lhs.true21:                                  ; preds = %if.end
  %13 = load float, float* %Dy, align 4
  %14 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4
  %call22 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %14)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 1
  %15 = load float, float* %arrayidx23, align 4
  %mul24 = fmul float %13, %15
  %cmp25 = fcmp oge float %mul24, 0.000000e+00
  br i1 %cmp25, label %if.then26, label %if.end27

if.then26:                                        ; preds = %land.lhs.true21
  store i1 false, i1* %retval, align 1
  br label %return

if.end27:                                         ; preds = %land.lhs.true21, %if.end
  %16 = load %class.btVector3*, %class.btVector3** %vorigin.addr, align 4
  %call28 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %16)
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 2
  %17 = load float, float* %arrayidx29, align 4
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %18 = load float, float* %arrayidx31, align 4
  %sub32 = fsub float %17, %18
  store float %sub32, float* %Dz, align 4
  %19 = load float, float* %Dz, align 4
  %call33 = call float @_Z6btFabsf(float %19)
  %call34 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %extents)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 2
  %20 = load float, float* %arrayidx35, align 4
  %cmp36 = fcmp ogt float %call33, %20
  br i1 %cmp36, label %land.lhs.true37, label %if.end43

land.lhs.true37:                                  ; preds = %if.end27
  %21 = load float, float* %Dz, align 4
  %22 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4
  %call38 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %22)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 2
  %23 = load float, float* %arrayidx39, align 4
  %mul40 = fmul float %21, %23
  %cmp41 = fcmp oge float %mul40, 0.000000e+00
  br i1 %cmp41, label %if.then42, label %if.end43

if.then42:                                        ; preds = %land.lhs.true37
  store i1 false, i1* %retval, align 1
  br label %return

if.end43:                                         ; preds = %land.lhs.true37, %if.end27
  %24 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4
  %call44 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %24)
  %arrayidx45 = getelementptr inbounds float, float* %call44, i32 1
  %25 = load float, float* %arrayidx45, align 4
  %26 = load float, float* %Dz, align 4
  %mul46 = fmul float %25, %26
  %27 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4
  %call47 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %27)
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 2
  %28 = load float, float* %arrayidx48, align 4
  %29 = load float, float* %Dy, align 4
  %mul49 = fmul float %28, %29
  %sub50 = fsub float %mul46, %mul49
  store float %sub50, float* %f, align 4
  %30 = load float, float* %f, align 4
  %call51 = call float @_Z6btFabsf(float %30)
  %call52 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %extents)
  %arrayidx53 = getelementptr inbounds float, float* %call52, i32 1
  %31 = load float, float* %arrayidx53, align 4
  %32 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4
  %call54 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %32)
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 2
  %33 = load float, float* %arrayidx55, align 4
  %call56 = call float @_Z6btFabsf(float %33)
  %mul57 = fmul float %31, %call56
  %call58 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %extents)
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 2
  %34 = load float, float* %arrayidx59, align 4
  %35 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4
  %call60 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %35)
  %arrayidx61 = getelementptr inbounds float, float* %call60, i32 1
  %36 = load float, float* %arrayidx61, align 4
  %call62 = call float @_Z6btFabsf(float %36)
  %mul63 = fmul float %34, %call62
  %add = fadd float %mul57, %mul63
  %cmp64 = fcmp ogt float %call51, %add
  br i1 %cmp64, label %if.then65, label %if.end66

if.then65:                                        ; preds = %if.end43
  store i1 false, i1* %retval, align 1
  br label %return

if.end66:                                         ; preds = %if.end43
  %37 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4
  %call67 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %37)
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 2
  %38 = load float, float* %arrayidx68, align 4
  %39 = load float, float* %Dx, align 4
  %mul69 = fmul float %38, %39
  %40 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4
  %call70 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %40)
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 0
  %41 = load float, float* %arrayidx71, align 4
  %42 = load float, float* %Dz, align 4
  %mul72 = fmul float %41, %42
  %sub73 = fsub float %mul69, %mul72
  store float %sub73, float* %f, align 4
  %43 = load float, float* %f, align 4
  %call74 = call float @_Z6btFabsf(float %43)
  %call75 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %extents)
  %arrayidx76 = getelementptr inbounds float, float* %call75, i32 0
  %44 = load float, float* %arrayidx76, align 4
  %45 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4
  %call77 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %45)
  %arrayidx78 = getelementptr inbounds float, float* %call77, i32 2
  %46 = load float, float* %arrayidx78, align 4
  %call79 = call float @_Z6btFabsf(float %46)
  %mul80 = fmul float %44, %call79
  %call81 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %extents)
  %arrayidx82 = getelementptr inbounds float, float* %call81, i32 2
  %47 = load float, float* %arrayidx82, align 4
  %48 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4
  %call83 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %48)
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 0
  %49 = load float, float* %arrayidx84, align 4
  %call85 = call float @_Z6btFabsf(float %49)
  %mul86 = fmul float %47, %call85
  %add87 = fadd float %mul80, %mul86
  %cmp88 = fcmp ogt float %call74, %add87
  br i1 %cmp88, label %if.then89, label %if.end90

if.then89:                                        ; preds = %if.end66
  store i1 false, i1* %retval, align 1
  br label %return

if.end90:                                         ; preds = %if.end66
  %50 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4
  %call91 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %50)
  %arrayidx92 = getelementptr inbounds float, float* %call91, i32 0
  %51 = load float, float* %arrayidx92, align 4
  %52 = load float, float* %Dy, align 4
  %mul93 = fmul float %51, %52
  %53 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4
  %call94 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %53)
  %arrayidx95 = getelementptr inbounds float, float* %call94, i32 1
  %54 = load float, float* %arrayidx95, align 4
  %55 = load float, float* %Dx, align 4
  %mul96 = fmul float %54, %55
  %sub97 = fsub float %mul93, %mul96
  store float %sub97, float* %f, align 4
  %56 = load float, float* %f, align 4
  %call98 = call float @_Z6btFabsf(float %56)
  %call99 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %extents)
  %arrayidx100 = getelementptr inbounds float, float* %call99, i32 0
  %57 = load float, float* %arrayidx100, align 4
  %58 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4
  %call101 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %58)
  %arrayidx102 = getelementptr inbounds float, float* %call101, i32 1
  %59 = load float, float* %arrayidx102, align 4
  %call103 = call float @_Z6btFabsf(float %59)
  %mul104 = fmul float %57, %call103
  %call105 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %extents)
  %arrayidx106 = getelementptr inbounds float, float* %call105, i32 1
  %60 = load float, float* %arrayidx106, align 4
  %61 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4
  %call107 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %61)
  %arrayidx108 = getelementptr inbounds float, float* %call107, i32 0
  %62 = load float, float* %arrayidx108, align 4
  %call109 = call float @_Z6btFabsf(float %62)
  %mul110 = fmul float %60, %call109
  %add111 = fadd float %mul104, %mul110
  %cmp112 = fcmp ogt float %call98, %add111
  br i1 %cmp112, label %if.then113, label %if.end114

if.then113:                                       ; preds = %if.end90
  store i1 false, i1* %retval, align 1
  br label %return

if.end114:                                        ; preds = %if.end90
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end114, %if.then113, %if.then89, %if.then65, %if.then42, %if.then26, %if.then
  %63 = load i1, i1* %retval, align 1
  ret i1 %63
}

; Function Attrs: noinline optnone
define hidden void @_ZN12btGImpactBvh14find_collisionEPS_RK11btTransformS0_S3_R9btPairSet(%class.btGImpactBvh* %boxset0, %class.btTransform* nonnull align 4 dereferenceable(64) %trans0, %class.btGImpactBvh* %boxset1, %class.btTransform* nonnull align 4 dereferenceable(64) %trans1, %class.btPairSet* nonnull align 4 dereferenceable(17) %collision_pairs) #2 {
entry:
  %boxset0.addr = alloca %class.btGImpactBvh*, align 4
  %trans0.addr = alloca %class.btTransform*, align 4
  %boxset1.addr = alloca %class.btGImpactBvh*, align 4
  %trans1.addr = alloca %class.btTransform*, align 4
  %collision_pairs.addr = alloca %class.btPairSet*, align 4
  %trans_cache_1to0 = alloca %class.BT_BOX_BOX_TRANSFORM_CACHE, align 4
  store %class.btGImpactBvh* %boxset0, %class.btGImpactBvh** %boxset0.addr, align 4
  store %class.btTransform* %trans0, %class.btTransform** %trans0.addr, align 4
  store %class.btGImpactBvh* %boxset1, %class.btGImpactBvh** %boxset1.addr, align 4
  store %class.btTransform* %trans1, %class.btTransform** %trans1.addr, align 4
  store %class.btPairSet* %collision_pairs, %class.btPairSet** %collision_pairs.addr, align 4
  %0 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %call = call i32 @_ZNK12btGImpactBvh12getNodeCountEv(%class.btGImpactBvh* %0)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %call1 = call i32 @_ZNK12btGImpactBvh12getNodeCountEv(%class.btGImpactBvh* %1)
  %cmp2 = icmp eq i32 %call1, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %call3 = call %class.BT_BOX_BOX_TRANSFORM_CACHE* @_ZN26BT_BOX_BOX_TRANSFORM_CACHEC2Ev(%class.BT_BOX_BOX_TRANSFORM_CACHE* %trans_cache_1to0)
  %2 = load %class.btTransform*, %class.btTransform** %trans0.addr, align 4
  %3 = load %class.btTransform*, %class.btTransform** %trans1.addr, align 4
  call void @_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_(%class.BT_BOX_BOX_TRANSFORM_CACHE* %trans_cache_1to0, %class.btTransform* nonnull align 4 dereferenceable(64) %2, %class.btTransform* nonnull align 4 dereferenceable(64) %3)
  %4 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %5 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %6 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4
  call void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %4, %class.btGImpactBvh* %5, %class.btPairSet* %6, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %trans_cache_1to0, i32 0, i32 0, i1 zeroext true)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.BT_BOX_BOX_TRANSFORM_CACHE* @_ZN26BT_BOX_BOX_TRANSFORM_CACHEC2Ev(%class.BT_BOX_BOX_TRANSFORM_CACHE* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.BT_BOX_BOX_TRANSFORM_CACHE*, align 4
  store %class.BT_BOX_BOX_TRANSFORM_CACHE* %this, %class.BT_BOX_BOX_TRANSFORM_CACHE** %this.addr, align 4
  %this1 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %this.addr, align 4
  %m_T1to0 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_T1to0)
  %m_R1to0 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %this1, i32 0, i32 1
  %call2 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_R1to0)
  %m_AR = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %this1, i32 0, i32 2
  %call3 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_AR)
  ret %class.BT_BOX_BOX_TRANSFORM_CACHE* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN26BT_BOX_BOX_TRANSFORM_CACHE19calc_from_homogenicERK11btTransformS2_(%class.BT_BOX_BOX_TRANSFORM_CACHE* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans0, %class.btTransform* nonnull align 4 dereferenceable(64) %trans1) #2 comdat {
entry:
  %this.addr = alloca %class.BT_BOX_BOX_TRANSFORM_CACHE*, align 4
  %trans0.addr = alloca %class.btTransform*, align 4
  %trans1.addr = alloca %class.btTransform*, align 4
  %temp_trans = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btTransform, align 4
  store %class.BT_BOX_BOX_TRANSFORM_CACHE* %this, %class.BT_BOX_BOX_TRANSFORM_CACHE** %this.addr, align 4
  store %class.btTransform* %trans0, %class.btTransform** %trans0.addr, align 4
  store %class.btTransform* %trans1, %class.btTransform** %trans1.addr, align 4
  %this1 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %trans0.addr, align 4
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %temp_trans, %class.btTransform* %0)
  %1 = load %class.btTransform*, %class.btTransform** %trans1.addr, align 4
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp, %class.btTransform* %temp_trans, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %temp_trans, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp)
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %temp_trans)
  %m_T1to0 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %this1, i32 0, i32 0
  %2 = bitcast %class.btVector3* %m_T1to0 to i8*
  %3 = bitcast %class.btVector3* %call2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %call3 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %temp_trans)
  %m_R1to0 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %this1, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_R1to0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call3)
  call void @_ZN26BT_BOX_BOX_TRANSFORM_CACHE20calc_absolute_matrixEv(%class.BT_BOX_BOX_TRANSFORM_CACHE* %this1)
  ret void
}

; Function Attrs: noinline optnone
define internal void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %boxset0, %class.btGImpactBvh* %boxset1, %class.btPairSet* %collision_pairs, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %trans_cache_1to0, i32 %node0, i32 %node1, i1 zeroext %complete_primitive_tests) #2 {
entry:
  %boxset0.addr = alloca %class.btGImpactBvh*, align 4
  %boxset1.addr = alloca %class.btGImpactBvh*, align 4
  %collision_pairs.addr = alloca %class.btPairSet*, align 4
  %trans_cache_1to0.addr = alloca %class.BT_BOX_BOX_TRANSFORM_CACHE*, align 4
  %node0.addr = alloca i32, align 4
  %node1.addr = alloca i32, align 4
  %complete_primitive_tests.addr = alloca i8, align 1
  store %class.btGImpactBvh* %boxset0, %class.btGImpactBvh** %boxset0.addr, align 4
  store %class.btGImpactBvh* %boxset1, %class.btGImpactBvh** %boxset1.addr, align 4
  store %class.btPairSet* %collision_pairs, %class.btPairSet** %collision_pairs.addr, align 4
  store %class.BT_BOX_BOX_TRANSFORM_CACHE* %trans_cache_1to0, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4
  store i32 %node0, i32* %node0.addr, align 4
  store i32 %node1, i32* %node1.addr, align 4
  %frombool = zext i1 %complete_primitive_tests to i8
  store i8 %frombool, i8* %complete_primitive_tests.addr, align 1
  %0 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %1 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %2 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4
  %3 = load i32, i32* %node0.addr, align 4
  %4 = load i32, i32* %node1.addr, align 4
  %5 = load i8, i8* %complete_primitive_tests.addr, align 1
  %tobool = trunc i8 %5 to i1
  %call = call zeroext i1 @_Z15_node_collisionP12btGImpactBvhS0_RK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %0, %class.btGImpactBvh* %1, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %2, i32 %3, i32 %4, i1 zeroext %tobool)
  %conv = zext i1 %call to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %if.end25

if.end:                                           ; preds = %entry
  %6 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %7 = load i32, i32* %node0.addr, align 4
  %call1 = call zeroext i1 @_ZNK12btGImpactBvh10isLeafNodeEi(%class.btGImpactBvh* %6, i32 %7)
  br i1 %call1, label %if.then2, label %if.else10

if.then2:                                         ; preds = %if.end
  %8 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %9 = load i32, i32* %node1.addr, align 4
  %call3 = call zeroext i1 @_ZNK12btGImpactBvh10isLeafNodeEi(%class.btGImpactBvh* %8, i32 %9)
  br i1 %call3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.then2
  %10 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4
  %11 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %12 = load i32, i32* %node0.addr, align 4
  %call5 = call i32 @_ZNK12btGImpactBvh11getNodeDataEi(%class.btGImpactBvh* %11, i32 %12)
  %13 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %14 = load i32, i32* %node1.addr, align 4
  %call6 = call i32 @_ZNK12btGImpactBvh11getNodeDataEi(%class.btGImpactBvh* %13, i32 %14)
  call void @_ZN9btPairSet9push_pairEii(%class.btPairSet* %10, i32 %call5, i32 %call6)
  br label %if.end25

if.else:                                          ; preds = %if.then2
  %15 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %16 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %17 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4
  %18 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4
  %19 = load i32, i32* %node0.addr, align 4
  %20 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %21 = load i32, i32* %node1.addr, align 4
  %call7 = call i32 @_ZNK12btGImpactBvh11getLeftNodeEi(%class.btGImpactBvh* %20, i32 %21)
  call void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %15, %class.btGImpactBvh* %16, %class.btPairSet* %17, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %18, i32 %19, i32 %call7, i1 zeroext false)
  %22 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %23 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %24 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4
  %25 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4
  %26 = load i32, i32* %node0.addr, align 4
  %27 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %28 = load i32, i32* %node1.addr, align 4
  %call8 = call i32 @_ZNK12btGImpactBvh12getRightNodeEi(%class.btGImpactBvh* %27, i32 %28)
  call void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %22, %class.btGImpactBvh* %23, %class.btPairSet* %24, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %25, i32 %26, i32 %call8, i1 zeroext false)
  br label %if.end9

if.end9:                                          ; preds = %if.else
  br label %if.end25

if.else10:                                        ; preds = %if.end
  %29 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %30 = load i32, i32* %node1.addr, align 4
  %call11 = call zeroext i1 @_ZNK12btGImpactBvh10isLeafNodeEi(%class.btGImpactBvh* %29, i32 %30)
  br i1 %call11, label %if.then12, label %if.else15

if.then12:                                        ; preds = %if.else10
  %31 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %32 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %33 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4
  %34 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4
  %35 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %36 = load i32, i32* %node0.addr, align 4
  %call13 = call i32 @_ZNK12btGImpactBvh11getLeftNodeEi(%class.btGImpactBvh* %35, i32 %36)
  %37 = load i32, i32* %node1.addr, align 4
  call void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %31, %class.btGImpactBvh* %32, %class.btPairSet* %33, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %34, i32 %call13, i32 %37, i1 zeroext false)
  %38 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %39 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %40 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4
  %41 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4
  %42 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %43 = load i32, i32* %node0.addr, align 4
  %call14 = call i32 @_ZNK12btGImpactBvh12getRightNodeEi(%class.btGImpactBvh* %42, i32 %43)
  %44 = load i32, i32* %node1.addr, align 4
  call void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %38, %class.btGImpactBvh* %39, %class.btPairSet* %40, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %41, i32 %call14, i32 %44, i1 zeroext false)
  br label %if.end24

if.else15:                                        ; preds = %if.else10
  %45 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %46 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %47 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4
  %48 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4
  %49 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %50 = load i32, i32* %node0.addr, align 4
  %call16 = call i32 @_ZNK12btGImpactBvh11getLeftNodeEi(%class.btGImpactBvh* %49, i32 %50)
  %51 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %52 = load i32, i32* %node1.addr, align 4
  %call17 = call i32 @_ZNK12btGImpactBvh11getLeftNodeEi(%class.btGImpactBvh* %51, i32 %52)
  call void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %45, %class.btGImpactBvh* %46, %class.btPairSet* %47, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %48, i32 %call16, i32 %call17, i1 zeroext false)
  %53 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %54 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %55 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4
  %56 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4
  %57 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %58 = load i32, i32* %node0.addr, align 4
  %call18 = call i32 @_ZNK12btGImpactBvh11getLeftNodeEi(%class.btGImpactBvh* %57, i32 %58)
  %59 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %60 = load i32, i32* %node1.addr, align 4
  %call19 = call i32 @_ZNK12btGImpactBvh12getRightNodeEi(%class.btGImpactBvh* %59, i32 %60)
  call void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %53, %class.btGImpactBvh* %54, %class.btPairSet* %55, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %56, i32 %call18, i32 %call19, i1 zeroext false)
  %61 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %62 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %63 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4
  %64 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4
  %65 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %66 = load i32, i32* %node0.addr, align 4
  %call20 = call i32 @_ZNK12btGImpactBvh12getRightNodeEi(%class.btGImpactBvh* %65, i32 %66)
  %67 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %68 = load i32, i32* %node1.addr, align 4
  %call21 = call i32 @_ZNK12btGImpactBvh11getLeftNodeEi(%class.btGImpactBvh* %67, i32 %68)
  call void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %61, %class.btGImpactBvh* %62, %class.btPairSet* %63, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %64, i32 %call20, i32 %call21, i1 zeroext false)
  %69 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %70 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %71 = load %class.btPairSet*, %class.btPairSet** %collision_pairs.addr, align 4
  %72 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4
  %73 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %74 = load i32, i32* %node0.addr, align 4
  %call22 = call i32 @_ZNK12btGImpactBvh12getRightNodeEi(%class.btGImpactBvh* %73, i32 %74)
  %75 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %76 = load i32, i32* %node1.addr, align 4
  %call23 = call i32 @_ZNK12btGImpactBvh12getRightNodeEi(%class.btGImpactBvh* %75, i32 %76)
  call void @_ZL31_find_collision_pairs_recursiveP12btGImpactBvhS0_P9btPairSetRK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %69, %class.btGImpactBvh* %70, %class.btPairSet* %71, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %72, i32 %call22, i32 %call23, i1 zeroext false)
  br label %if.end24

if.end24:                                         ; preds = %if.else15, %if.then12
  br label %if.end25

if.end25:                                         ; preds = %if.then, %if.then4, %if.end24, %if.end9
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btBvhTree12getNodeCountEv(%class.btBvhTree* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %m_num_nodes = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_num_nodes, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK9btBvhTree10isLeafNodeEi(%class.btBvhTree* %this, i32 %nodeindex) #2 comdat {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4
  store i32 %nodeindex, i32* %nodeindex.addr, align 4
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %m_node_array = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %0 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array to %class.btAlignedObjectArray*
  %1 = load i32, i32* %nodeindex.addr, align 4
  %call = call nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %0, i32 %1)
  %call2 = call zeroext i1 @_ZNK17GIM_BVH_TREE_NODE10isLeafNodeEv(%class.GIM_BVH_TREE_NODE* %call)
  ret i1 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %0, i32 %1
  ret %class.GIM_BVH_TREE_NODE* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17GIM_BVH_TREE_NODE10isLeafNodeEv(%class.GIM_BVH_TREE_NODE* %this) #1 comdat {
entry:
  %this.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  store %class.GIM_BVH_TREE_NODE* %this, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  %this1 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  %m_escapeIndexOrDataIndex = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_escapeIndexOrDataIndex, align 4
  %cmp = icmp sge i32 %0, 0
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK9btBvhTree11getNodeDataEi(%class.btBvhTree* %this, i32 %nodeindex) #2 comdat {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4
  store i32 %nodeindex, i32* %nodeindex.addr, align 4
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %m_node_array = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %0 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array to %class.btAlignedObjectArray*
  %1 = load i32, i32* %nodeindex.addr, align 4
  %call = call nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %0, i32 %1)
  %call2 = call i32 @_ZNK17GIM_BVH_TREE_NODE12getDataIndexEv(%class.GIM_BVH_TREE_NODE* %call)
  ret i32 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17GIM_BVH_TREE_NODE12getDataIndexEv(%class.GIM_BVH_TREE_NODE* %this) #1 comdat {
entry:
  %this.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  store %class.GIM_BVH_TREE_NODE* %this, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  %this1 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  %m_escapeIndexOrDataIndex = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_escapeIndexOrDataIndex, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btBvhTree11getLeftNodeEi(%class.btBvhTree* %this, i32 %nodeindex) #1 comdat {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4
  store i32 %nodeindex, i32* %nodeindex.addr, align 4
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %0 = load i32, i32* %nodeindex.addr, align 4
  %add = add nsw i32 %0, 1
  ret i32 %add
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btBvhTree12getNodeBoundEiR6btAABB(%class.btBvhTree* %this, i32 %nodeindex, %class.btAABB* nonnull align 4 dereferenceable(32) %bound) #1 comdat {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %nodeindex.addr = alloca i32, align 4
  %bound.addr = alloca %class.btAABB*, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4
  store i32 %nodeindex, i32* %nodeindex.addr, align 4
  store %class.btAABB* %bound, %class.btAABB** %bound.addr, align 4
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %m_node_array = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %0 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array to %class.btAlignedObjectArray*
  %1 = load i32, i32* %nodeindex.addr, align 4
  %call = call nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %0, i32 %1)
  %m_bound = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %call, i32 0, i32 0
  %2 = load %class.btAABB*, %class.btAABB** %bound.addr, align 4
  %3 = bitcast %class.btAABB* %2 to i8*
  %4 = bitcast %class.btAABB* %m_bound to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 32, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK9btBvhTree12getRightNodeEi(%class.btBvhTree* %this, i32 %nodeindex) #2 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btBvhTree*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4
  store i32 %nodeindex, i32* %nodeindex.addr, align 4
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %m_node_array = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %0 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array to %class.btAlignedObjectArray*
  %1 = load i32, i32* %nodeindex.addr, align 4
  %add = add nsw i32 %1, 1
  %call = call nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %0, i32 %add)
  %call2 = call zeroext i1 @_ZNK17GIM_BVH_TREE_NODE10isLeafNodeEv(%class.GIM_BVH_TREE_NODE* %call)
  br i1 %call2, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %nodeindex.addr, align 4
  %add3 = add nsw i32 %2, 2
  store i32 %add3, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i32, i32* %nodeindex.addr, align 4
  %add4 = add nsw i32 %3, 1
  %m_node_array5 = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %4 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array5 to %class.btAlignedObjectArray*
  %5 = load i32, i32* %nodeindex.addr, align 4
  %add6 = add nsw i32 %5, 1
  %call7 = call nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %4, i32 %add6)
  %call8 = call i32 @_ZNK17GIM_BVH_TREE_NODE14getEscapeIndexEv(%class.GIM_BVH_TREE_NODE* %call7)
  %add9 = add nsw i32 %add4, %call8
  store i32 %add9, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17GIM_BVH_TREE_NODE14getEscapeIndexEv(%class.GIM_BVH_TREE_NODE* %this) #1 comdat {
entry:
  %this.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  store %class.GIM_BVH_TREE_NODE* %this, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  %this1 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  %m_escapeIndexOrDataIndex = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_escapeIndexOrDataIndex, align 4
  %sub = sub nsw i32 0, %0
  ret i32 %sub
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI12GIM_BVH_DATALj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI12GIM_BVH_DATALj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.GIM_BVH_DATA* null, %struct.GIM_BVH_DATA** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE5clearEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE10deallocateEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data, align 4
  %tobool = icmp ne %struct.GIM_BVH_DATA* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI12GIM_BVH_DATALj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %m_allocator, %struct.GIM_BVH_DATA* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.GIM_BVH_DATA* null, %struct.GIM_BVH_DATA** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI12GIM_BVH_DATALj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %this, %struct.GIM_BVH_DATA* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %struct.GIM_BVH_DATA*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store %struct.GIM_BVH_DATA* %ptr, %struct.GIM_BVH_DATA** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %ptr.addr, align 4
  %1 = bitcast %struct.GIM_BVH_DATA* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btBvhTree18getEscapeNodeIndexEi(%class.btBvhTree* %this, i32 %nodeindex) #1 comdat {
entry:
  %this.addr = alloca %class.btBvhTree*, align 4
  %nodeindex.addr = alloca i32, align 4
  store %class.btBvhTree* %this, %class.btBvhTree** %this.addr, align 4
  store i32 %nodeindex, i32* %nodeindex.addr, align 4
  %this1 = load %class.btBvhTree*, %class.btBvhTree** %this.addr, align 4
  %m_node_array = getelementptr inbounds %class.btBvhTree, %class.btBvhTree* %this1, i32 0, i32 1
  %0 = bitcast %class.GIM_BVH_TREE_NODE_ARRAY* %m_node_array to %class.btAlignedObjectArray*
  %1 = load i32, i32* %nodeindex.addr, align 4
  %call = call nonnull align 4 dereferenceable(36) %class.GIM_BVH_TREE_NODE* @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEEixEi(%class.btAlignedObjectArray* %0, i32 %1)
  %call2 = call i32 @_ZNK17GIM_BVH_TREE_NODE14getEscapeIndexEv(%class.GIM_BVH_TREE_NODE* %call)
  ret i32 %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK6btAABB17get_center_extendER9btVector3S1_(%class.btAABB* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extend) #2 comdat {
entry:
  %this.addr = alloca %class.btAABB*, align 4
  %center.addr = alloca %class.btVector3*, align 4
  %extend.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4
  store %class.btVector3* %center, %class.btVector3** %center.addr, align 4
  store %class.btVector3* %extend, %class.btVector3** %extend.addr, align 4
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min)
  store float 5.000000e-01, float* %ref.tmp3, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %0 = load %class.btVector3*, %class.btVector3** %center.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %m_max5 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %3 = load %class.btVector3*, %class.btVector3** %center.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max5, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %4 = load %class.btVector3*, %class.btVector3** %extend.addr, align 4
  %5 = bitcast %class.btVector3* %4 to i8*
  %6 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #6

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform7inverseEv(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %inv = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %inv, %class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN26BT_BOX_BOX_TRANSFORM_CACHE20calc_absolute_matrixEv(%class.BT_BOX_BOX_TRANSFORM_CACHE* %this) #2 comdat {
entry:
  %this.addr = alloca %class.BT_BOX_BOX_TRANSFORM_CACHE*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %class.BT_BOX_BOX_TRANSFORM_CACHE* %this, %class.BT_BOX_BOX_TRANSFORM_CACHE** %this.addr, align 4
  %this1 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc10, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end12

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %1 = load i32, i32* %j, align 4
  %cmp3 = icmp slt i32 %1, 3
  br i1 %cmp3, label %for.body4, label %for.end

for.body4:                                        ; preds = %for.cond2
  %m_R1to0 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %this1, i32 0, i32 1
  %2 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %m_R1to0, i32 %2)
  %call5 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call)
  %3 = load i32, i32* %j, align 4
  %arrayidx = getelementptr inbounds float, float* %call5, i32 %3
  %4 = load float, float* %arrayidx, align 4
  %call6 = call float @_Z6btFabsf(float %4)
  %add = fadd float 0x3EB0C6F7A0000000, %call6
  %m_AR = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %this1, i32 0, i32 2
  %5 = load i32, i32* %i, align 4
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %m_AR, i32 %5)
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call7)
  %6 = load i32, i32* %j, align 4
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 %6
  store float %add, float* %arrayidx9, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %7 = load i32, i32* %j, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  br label %for.inc10

for.inc10:                                        ; preds = %for.end
  %8 = load i32, i32* %i, align 4
  %inc11 = add nsw i32 %8, 1
  store i32 %inc11, i32* %i, align 4
  br label %for.cond

for.end12:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %10, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_Z15_node_collisionP12btGImpactBvhS0_RK26BT_BOX_BOX_TRANSFORM_CACHEiib(%class.btGImpactBvh* %boxset0, %class.btGImpactBvh* %boxset1, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %trans_cache_1to0, i32 %node0, i32 %node1, i1 zeroext %complete_primitive_tests) #2 comdat {
entry:
  %boxset0.addr = alloca %class.btGImpactBvh*, align 4
  %boxset1.addr = alloca %class.btGImpactBvh*, align 4
  %trans_cache_1to0.addr = alloca %class.BT_BOX_BOX_TRANSFORM_CACHE*, align 4
  %node0.addr = alloca i32, align 4
  %node1.addr = alloca i32, align 4
  %complete_primitive_tests.addr = alloca i8, align 1
  %box0 = alloca %class.btAABB, align 4
  %box1 = alloca %class.btAABB, align 4
  store %class.btGImpactBvh* %boxset0, %class.btGImpactBvh** %boxset0.addr, align 4
  store %class.btGImpactBvh* %boxset1, %class.btGImpactBvh** %boxset1.addr, align 4
  store %class.BT_BOX_BOX_TRANSFORM_CACHE* %trans_cache_1to0, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4
  store i32 %node0, i32* %node0.addr, align 4
  store i32 %node1, i32* %node1.addr, align 4
  %frombool = zext i1 %complete_primitive_tests to i8
  store i8 %frombool, i8* %complete_primitive_tests.addr, align 1
  %call = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %box0)
  %0 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset0.addr, align 4
  %1 = load i32, i32* %node0.addr, align 4
  call void @_ZNK12btGImpactBvh12getNodeBoundEiR6btAABB(%class.btGImpactBvh* %0, i32 %1, %class.btAABB* nonnull align 4 dereferenceable(32) %box0)
  %call1 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %box1)
  %2 = load %class.btGImpactBvh*, %class.btGImpactBvh** %boxset1.addr, align 4
  %3 = load i32, i32* %node1.addr, align 4
  call void @_ZNK12btGImpactBvh12getNodeBoundEiR6btAABB(%class.btGImpactBvh* %2, i32 %3, %class.btAABB* nonnull align 4 dereferenceable(32) %box1)
  %4 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %trans_cache_1to0.addr, align 4
  %5 = load i8, i8* %complete_primitive_tests.addr, align 1
  %tobool = trunc i8 %5 to i1
  %call2 = call zeroext i1 @_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb(%class.btAABB* %box0, %class.btAABB* nonnull align 4 dereferenceable(32) %box1, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %4, i1 zeroext %tobool)
  ret i1 %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btPairSet9push_pairEii(%class.btPairSet* %this, i32 %index1, i32 %index2) #2 comdat {
entry:
  %this.addr = alloca %class.btPairSet*, align 4
  %index1.addr = alloca i32, align 4
  %index2.addr = alloca i32, align 4
  %ref.tmp = alloca %struct.GIM_PAIR, align 4
  store %class.btPairSet* %this, %class.btPairSet** %this.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  store i32 %index2, i32* %index2.addr, align 4
  %this1 = load %class.btPairSet*, %class.btPairSet** %this.addr, align 4
  %0 = bitcast %class.btPairSet* %this1 to %class.btAlignedObjectArray.8*
  %1 = load i32, i32* %index1.addr, align 4
  %2 = load i32, i32* %index2.addr, align 4
  %call = call %struct.GIM_PAIR* @_ZN8GIM_PAIRC2Eii(%struct.GIM_PAIR* %ref.tmp, i32 %1, i32 %2)
  call void @_ZN20btAlignedObjectArrayI8GIM_PAIRE9push_backERKS0_(%class.btAlignedObjectArray.8* %0, %struct.GIM_PAIR* nonnull align 4 dereferenceable(8) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK6btAABB23overlapping_trans_cacheERKS_RK26BT_BOX_BOX_TRANSFORM_CACHEb(%class.btAABB* %this, %class.btAABB* nonnull align 4 dereferenceable(32) %box, %class.BT_BOX_BOX_TRANSFORM_CACHE* nonnull align 4 dereferenceable(112) %transcache, i1 zeroext %fulltest) #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btAABB*, align 4
  %box.addr = alloca %class.btAABB*, align 4
  %transcache.addr = alloca %class.BT_BOX_BOX_TRANSFORM_CACHE*, align 4
  %fulltest.addr = alloca i8, align 1
  %ea = alloca %class.btVector3, align 4
  %eb = alloca %class.btVector3, align 4
  %ca = alloca %class.btVector3, align 4
  %cb = alloca %class.btVector3, align 4
  %T = alloca %class.btVector3, align 4
  %t = alloca float, align 4
  %t2 = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %m = alloca i32, align 4
  %n = alloca i32, align 4
  %o = alloca i32, align 4
  %p = alloca i32, align 4
  %q = alloca i32, align 4
  %r = alloca i32, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4
  store %class.btAABB* %box, %class.btAABB** %box.addr, align 4
  store %class.BT_BOX_BOX_TRANSFORM_CACHE* %transcache, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4
  %frombool = zext i1 %fulltest to i8
  store i8 %frombool, i8* %fulltest.addr, align 1
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ea)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %eb)
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ca)
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %cb)
  call void @_ZNK6btAABB17get_center_extendER9btVector3S1_(%class.btAABB* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %ca, %class.btVector3* nonnull align 4 dereferenceable(16) %ea)
  %0 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  call void @_ZNK6btAABB17get_center_extendER9btVector3S1_(%class.btAABB* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %cb, %class.btVector3* nonnull align 4 dereferenceable(16) %eb)
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %T)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %1, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4
  %m_R1to0 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %2, i32 0, i32 1
  %3 = load i32, i32* %i, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_R1to0, i32 %3)
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %cb)
  %4 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4
  %m_T1to0 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %4, i32 0, i32 0
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_T1to0)
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %call8, i32 %5
  %6 = load float, float* %arrayidx, align 4
  %add = fadd float %call7, %6
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ca)
  %7 = load i32, i32* %i, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %7
  %8 = load float, float* %arrayidx10, align 4
  %sub = fsub float %add, %8
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %T)
  %9 = load i32, i32* %i, align 4
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 %9
  store float %sub, float* %arrayidx12, align 4
  %10 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4
  %m_AR = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %10, i32 0, i32 2
  %11 = load i32, i32* %i, align 4
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_AR, i32 %11)
  %call14 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call13, %class.btVector3* nonnull align 4 dereferenceable(16) %eb)
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ea)
  %12 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 %12
  %13 = load float, float* %arrayidx16, align 4
  %add17 = fadd float %call14, %13
  store float %add17, float* %t, align 4
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %T)
  %14 = load i32, i32* %i, align 4
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 %14
  %15 = load float, float* %arrayidx19, align 4
  %call20 = call float @_Z6btFabsf(float %15)
  %16 = load float, float* %t, align 4
  %cmp21 = fcmp ogt float %call20, %16
  br i1 %cmp21, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %17 = load i32, i32* %i, align 4
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc36, %for.end
  %18 = load i32, i32* %i, align 4
  %cmp23 = icmp slt i32 %18, 3
  br i1 %cmp23, label %for.body24, label %for.end38

for.body24:                                       ; preds = %for.cond22
  %19 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4
  %m_R1to025 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %19, i32 0, i32 1
  %20 = load i32, i32* %i, align 4
  %call26 = call float @_Z15bt_mat3_dot_colRK11btMatrix3x3RK9btVector3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_R1to025, %class.btVector3* nonnull align 4 dereferenceable(16) %T, i32 %20)
  store float %call26, float* %t, align 4
  %21 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4
  %m_AR27 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %21, i32 0, i32 2
  %22 = load i32, i32* %i, align 4
  %call28 = call float @_Z15bt_mat3_dot_colRK11btMatrix3x3RK9btVector3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_AR27, %class.btVector3* nonnull align 4 dereferenceable(16) %ea, i32 %22)
  %call29 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %eb)
  %23 = load i32, i32* %i, align 4
  %arrayidx30 = getelementptr inbounds float, float* %call29, i32 %23
  %24 = load float, float* %arrayidx30, align 4
  %add31 = fadd float %call28, %24
  store float %add31, float* %t2, align 4
  %25 = load float, float* %t, align 4
  %call32 = call float @_Z6btFabsf(float %25)
  %26 = load float, float* %t2, align 4
  %cmp33 = fcmp ogt float %call32, %26
  br i1 %cmp33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %for.body24
  store i1 false, i1* %retval, align 1
  br label %return

if.end35:                                         ; preds = %for.body24
  br label %for.inc36

for.inc36:                                        ; preds = %if.end35
  %27 = load i32, i32* %i, align 4
  %inc37 = add nsw i32 %27, 1
  store i32 %inc37, i32* %i, align 4
  br label %for.cond22

for.end38:                                        ; preds = %for.cond22
  %28 = load i8, i8* %fulltest.addr, align 1
  %tobool = trunc i8 %28 to i1
  br i1 %tobool, label %if.then39, label %if.end111

if.then39:                                        ; preds = %for.end38
  store i32 0, i32* %i, align 4
  br label %for.cond40

for.cond40:                                       ; preds = %for.inc108, %if.then39
  %29 = load i32, i32* %i, align 4
  %cmp41 = icmp slt i32 %29, 3
  br i1 %cmp41, label %for.body42, label %for.end110

for.body42:                                       ; preds = %for.cond40
  %30 = load i32, i32* %i, align 4
  %add43 = add nsw i32 %30, 1
  %rem = srem i32 %add43, 3
  store i32 %rem, i32* %m, align 4
  %31 = load i32, i32* %i, align 4
  %add44 = add nsw i32 %31, 2
  %rem45 = srem i32 %add44, 3
  store i32 %rem45, i32* %n, align 4
  %32 = load i32, i32* %i, align 4
  %cmp46 = icmp eq i32 %32, 0
  %33 = zext i1 %cmp46 to i64
  %cond = select i1 %cmp46, i32 1, i32 0
  store i32 %cond, i32* %o, align 4
  %34 = load i32, i32* %i, align 4
  %cmp47 = icmp eq i32 %34, 2
  %35 = zext i1 %cmp47 to i64
  %cond48 = select i1 %cmp47, i32 1, i32 2
  store i32 %cond48, i32* %p, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond49

for.cond49:                                       ; preds = %for.inc105, %for.body42
  %36 = load i32, i32* %j, align 4
  %cmp50 = icmp slt i32 %36, 3
  br i1 %cmp50, label %for.body51, label %for.end107

for.body51:                                       ; preds = %for.cond49
  %37 = load i32, i32* %j, align 4
  %cmp52 = icmp eq i32 %37, 2
  %38 = zext i1 %cmp52 to i64
  %cond53 = select i1 %cmp52, i32 1, i32 2
  store i32 %cond53, i32* %q, align 4
  %39 = load i32, i32* %j, align 4
  %cmp54 = icmp eq i32 %39, 0
  %40 = zext i1 %cmp54 to i64
  %cond55 = select i1 %cmp54, i32 1, i32 0
  store i32 %cond55, i32* %r, align 4
  %call56 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %T)
  %41 = load i32, i32* %n, align 4
  %arrayidx57 = getelementptr inbounds float, float* %call56, i32 %41
  %42 = load float, float* %arrayidx57, align 4
  %43 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4
  %m_R1to058 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %43, i32 0, i32 1
  %44 = load i32, i32* %m, align 4
  %call59 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_R1to058, i32 %44)
  %call60 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call59)
  %45 = load i32, i32* %j, align 4
  %arrayidx61 = getelementptr inbounds float, float* %call60, i32 %45
  %46 = load float, float* %arrayidx61, align 4
  %mul = fmul float %42, %46
  %call62 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %T)
  %47 = load i32, i32* %m, align 4
  %arrayidx63 = getelementptr inbounds float, float* %call62, i32 %47
  %48 = load float, float* %arrayidx63, align 4
  %49 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4
  %m_R1to064 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %49, i32 0, i32 1
  %50 = load i32, i32* %n, align 4
  %call65 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_R1to064, i32 %50)
  %call66 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call65)
  %51 = load i32, i32* %j, align 4
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 %51
  %52 = load float, float* %arrayidx67, align 4
  %mul68 = fmul float %48, %52
  %sub69 = fsub float %mul, %mul68
  store float %sub69, float* %t, align 4
  %call70 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ea)
  %53 = load i32, i32* %o, align 4
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 %53
  %54 = load float, float* %arrayidx71, align 4
  %55 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4
  %m_AR72 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %55, i32 0, i32 2
  %56 = load i32, i32* %p, align 4
  %call73 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_AR72, i32 %56)
  %call74 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call73)
  %57 = load i32, i32* %j, align 4
  %arrayidx75 = getelementptr inbounds float, float* %call74, i32 %57
  %58 = load float, float* %arrayidx75, align 4
  %mul76 = fmul float %54, %58
  %call77 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ea)
  %59 = load i32, i32* %p, align 4
  %arrayidx78 = getelementptr inbounds float, float* %call77, i32 %59
  %60 = load float, float* %arrayidx78, align 4
  %61 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4
  %m_AR79 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %61, i32 0, i32 2
  %62 = load i32, i32* %o, align 4
  %call80 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_AR79, i32 %62)
  %call81 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call80)
  %63 = load i32, i32* %j, align 4
  %arrayidx82 = getelementptr inbounds float, float* %call81, i32 %63
  %64 = load float, float* %arrayidx82, align 4
  %mul83 = fmul float %60, %64
  %add84 = fadd float %mul76, %mul83
  %call85 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %eb)
  %65 = load i32, i32* %r, align 4
  %arrayidx86 = getelementptr inbounds float, float* %call85, i32 %65
  %66 = load float, float* %arrayidx86, align 4
  %67 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4
  %m_AR87 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %67, i32 0, i32 2
  %68 = load i32, i32* %i, align 4
  %call88 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_AR87, i32 %68)
  %call89 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call88)
  %69 = load i32, i32* %q, align 4
  %arrayidx90 = getelementptr inbounds float, float* %call89, i32 %69
  %70 = load float, float* %arrayidx90, align 4
  %mul91 = fmul float %66, %70
  %add92 = fadd float %add84, %mul91
  %call93 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %eb)
  %71 = load i32, i32* %q, align 4
  %arrayidx94 = getelementptr inbounds float, float* %call93, i32 %71
  %72 = load float, float* %arrayidx94, align 4
  %73 = load %class.BT_BOX_BOX_TRANSFORM_CACHE*, %class.BT_BOX_BOX_TRANSFORM_CACHE** %transcache.addr, align 4
  %m_AR95 = getelementptr inbounds %class.BT_BOX_BOX_TRANSFORM_CACHE, %class.BT_BOX_BOX_TRANSFORM_CACHE* %73, i32 0, i32 2
  %74 = load i32, i32* %i, align 4
  %call96 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_AR95, i32 %74)
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call96)
  %75 = load i32, i32* %r, align 4
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 %75
  %76 = load float, float* %arrayidx98, align 4
  %mul99 = fmul float %72, %76
  %add100 = fadd float %add92, %mul99
  store float %add100, float* %t2, align 4
  %77 = load float, float* %t, align 4
  %call101 = call float @_Z6btFabsf(float %77)
  %78 = load float, float* %t2, align 4
  %cmp102 = fcmp ogt float %call101, %78
  br i1 %cmp102, label %if.then103, label %if.end104

if.then103:                                       ; preds = %for.body51
  store i1 false, i1* %retval, align 1
  br label %return

if.end104:                                        ; preds = %for.body51
  br label %for.inc105

for.inc105:                                       ; preds = %if.end104
  %79 = load i32, i32* %j, align 4
  %inc106 = add nsw i32 %79, 1
  store i32 %inc106, i32* %j, align 4
  br label %for.cond49

for.end107:                                       ; preds = %for.cond49
  br label %for.inc108

for.inc108:                                       ; preds = %for.end107
  %80 = load i32, i32* %i, align 4
  %inc109 = add nsw i32 %80, 1
  store i32 %inc109, i32* %i, align 4
  br label %for.cond40

for.end110:                                       ; preds = %for.cond40
  br label %if.end111

if.end111:                                        ; preds = %for.end110, %for.end38
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end111, %if.then103, %if.then34, %if.then
  %81 = load i1, i1* %retval, align 1
  ret i1 %81
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z15bt_mat3_dot_colRK11btMatrix3x3RK9btVector3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %mat, %class.btVector3* nonnull align 4 dereferenceable(16) %vec3, i32 %colindex) #1 comdat {
entry:
  %mat.addr = alloca %class.btMatrix3x3*, align 4
  %vec3.addr = alloca %class.btVector3*, align 4
  %colindex.addr = alloca i32, align 4
  store %class.btMatrix3x3* %mat, %class.btMatrix3x3** %mat.addr, align 4
  store %class.btVector3* %vec3, %class.btVector3** %vec3.addr, align 4
  store i32 %colindex, i32* %colindex.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %vec3.addr, align 4
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call1)
  %3 = load i32, i32* %colindex.addr, align 4
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 %3
  %4 = load float, float* %arrayidx3, align 4
  %mul = fmul float %1, %4
  %5 = load %class.btVector3*, %class.btVector3** %vec3.addr, align 4
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %5)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %6 = load float, float* %arrayidx5, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 1)
  %call7 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call6)
  %8 = load i32, i32* %colindex.addr, align 4
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 %8
  %9 = load float, float* %arrayidx8, align 4
  %mul9 = fmul float %6, %9
  %add = fadd float %mul, %mul9
  %10 = load %class.btVector3*, %class.btVector3** %vec3.addr, align 4
  %call10 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %10)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 2
  %11 = load float, float* %arrayidx11, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 2)
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call12)
  %13 = load i32, i32* %colindex.addr, align 4
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 %13
  %14 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %11, %14
  %add16 = fadd float %add, %mul15
  ret float %add16
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI8GIM_PAIRE9push_backERKS0_(%class.btAlignedObjectArray.8* %this, %struct.GIM_PAIR* nonnull align 4 dereferenceable(8) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Val.addr = alloca %struct.GIM_PAIR*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store %struct.GIM_PAIR* %_Val, %struct.GIM_PAIR** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI8GIM_PAIRE9allocSizeEi(%class.btAlignedObjectArray.8* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI8GIM_PAIRE7reserveEi(%class.btAlignedObjectArray.8* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %1 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %1, i32 %2
  %3 = bitcast %struct.GIM_PAIR* %arrayidx to i8*
  %4 = bitcast i8* %3 to %struct.GIM_PAIR*
  %5 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %_Val.addr, align 4
  %call5 = call %struct.GIM_PAIR* @_ZN8GIM_PAIRC2ERKS_(%struct.GIM_PAIR* %4, %struct.GIM_PAIR* nonnull align 4 dereferenceable(8) %5)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %6 = load i32, i32* %m_size6, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %m_size6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.GIM_PAIR* @_ZN8GIM_PAIRC2Eii(%struct.GIM_PAIR* returned %this, i32 %index1, i32 %index2) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.GIM_PAIR*, align 4
  %index1.addr = alloca i32, align 4
  %index2.addr = alloca i32, align 4
  store %struct.GIM_PAIR* %this, %struct.GIM_PAIR** %this.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  store i32 %index2, i32* %index2.addr, align 4
  %this1 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %this.addr, align 4
  %0 = load i32, i32* %index1.addr, align 4
  %m_index1 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %this1, i32 0, i32 0
  store i32 %0, i32* %m_index1, align 4
  %1 = load i32, i32* %index2.addr, align 4
  %m_index2 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %this1, i32 0, i32 1
  store i32 %1, i32* %m_index2, align 4
  ret %struct.GIM_PAIR* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE8capacityEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI8GIM_PAIRE7reserveEi(%class.btAlignedObjectArray.8* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.GIM_PAIR*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI8GIM_PAIRE8allocateEi(%class.btAlignedObjectArray.8* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.GIM_PAIR*
  store %struct.GIM_PAIR* %2, %struct.GIM_PAIR** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %3 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4copyEiiPS0_(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call3, %struct.GIM_PAIR* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI8GIM_PAIRE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI8GIM_PAIRE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %struct.GIM_PAIR* %4, %struct.GIM_PAIR** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI8GIM_PAIRE9allocSizeEi(%class.btAlignedObjectArray.8* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.GIM_PAIR* @_ZN8GIM_PAIRC2ERKS_(%struct.GIM_PAIR* returned %this, %struct.GIM_PAIR* nonnull align 4 dereferenceable(8) %p) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.GIM_PAIR*, align 4
  %p.addr = alloca %struct.GIM_PAIR*, align 4
  store %struct.GIM_PAIR* %this, %struct.GIM_PAIR** %this.addr, align 4
  store %struct.GIM_PAIR* %p, %struct.GIM_PAIR** %p.addr, align 4
  %this1 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %this.addr, align 4
  %0 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %p.addr, align 4
  %m_index1 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %0, i32 0, i32 0
  %1 = load i32, i32* %m_index1, align 4
  %m_index12 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %this1, i32 0, i32 0
  store i32 %1, i32* %m_index12, align 4
  %2 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %p.addr, align 4
  %m_index2 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %2, i32 0, i32 1
  %3 = load i32, i32* %m_index2, align 4
  %m_index23 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %this1, i32 0, i32 1
  store i32 %3, i32* %m_index23, align 4
  ret %struct.GIM_PAIR* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI8GIM_PAIRE8allocateEi(%class.btAlignedObjectArray.8* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.GIM_PAIR* @_ZN18btAlignedAllocatorI8GIM_PAIRLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.9* %m_allocator, i32 %1, %struct.GIM_PAIR** null)
  %2 = bitcast %struct.GIM_PAIR* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4copyEiiPS0_(%class.btAlignedObjectArray.8* %this, i32 %start, i32 %end, %struct.GIM_PAIR* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.GIM_PAIR*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.GIM_PAIR* %dest, %struct.GIM_PAIR** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %3, i32 %4
  %5 = bitcast %struct.GIM_PAIR* %arrayidx to i8*
  %6 = bitcast i8* %5 to %struct.GIM_PAIR*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %7 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %7, i32 %8
  %call = call %struct.GIM_PAIR* @_ZN8GIM_PAIRC2ERKS_(%struct.GIM_PAIR* %6, %struct.GIM_PAIR* nonnull align 4 dereferenceable(8) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI8GIM_PAIRE7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %3 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI8GIM_PAIRE10deallocateEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %m_data, align 4
  %tobool = icmp ne %struct.GIM_PAIR* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI8GIM_PAIRLj16EE10deallocateEPS0_(%class.btAlignedAllocator.9* %m_allocator, %struct.GIM_PAIR* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %struct.GIM_PAIR* null, %struct.GIM_PAIR** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.GIM_PAIR* @_ZN18btAlignedAllocatorI8GIM_PAIRLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.9* %this, i32 %n, %struct.GIM_PAIR** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.GIM_PAIR**, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.GIM_PAIR** %hint, %struct.GIM_PAIR*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 8, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.GIM_PAIR*
  ret %struct.GIM_PAIR* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI8GIM_PAIRLj16EE10deallocateEPS0_(%class.btAlignedAllocator.9* %this, %struct.GIM_PAIR* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca %struct.GIM_PAIR*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store %struct.GIM_PAIR* %ptr, %struct.GIM_PAIR** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %ptr.addr, align 4
  %1 = bitcast %struct.GIM_PAIR* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.GIM_BVH_DATA* @_ZN12GIM_BVH_DATAC2ERKS_(%struct.GIM_BVH_DATA* returned %this, %struct.GIM_BVH_DATA* nonnull align 4 dereferenceable(36) %0) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.GIM_BVH_DATA*, align 4
  %.addr = alloca %struct.GIM_BVH_DATA*, align 4
  store %struct.GIM_BVH_DATA* %this, %struct.GIM_BVH_DATA** %this.addr, align 4
  store %struct.GIM_BVH_DATA* %0, %struct.GIM_BVH_DATA** %.addr, align 4
  %this1 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %this.addr, align 4
  %m_bound = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %this1, i32 0, i32 0
  %1 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %.addr, align 4
  %m_bound2 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %1, i32 0, i32 0
  %call = call %class.btAABB* @_ZN6btAABBC2ERKS_(%class.btAABB* %m_bound, %class.btAABB* nonnull align 4 dereferenceable(32) %m_bound2)
  %m_data = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %this1, i32 0, i32 1
  %2 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %.addr, align 4
  %m_data3 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %2, i32 0, i32 1
  %3 = load i32, i32* %m_data3, align 4
  store i32 %3, i32* %m_data, align 4
  ret %struct.GIM_BVH_DATA* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAABB* @_ZN6btAABBC2ERKS_(%class.btAABB* returned %this, %class.btAABB* nonnull align 4 dereferenceable(32) %other) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAABB*, align 4
  %other.addr = alloca %class.btAABB*, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4
  store %class.btAABB* %other, %class.btAABB** %other.addr, align 4
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %0 = load %class.btAABB*, %class.btAABB** %other.addr, align 4
  %m_min2 = getelementptr inbounds %class.btAABB, %class.btAABB* %0, i32 0, i32 0
  %1 = bitcast %class.btVector3* %m_min to i8*
  %2 = bitcast %class.btVector3* %m_min2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %3 = load %class.btAABB*, %class.btAABB** %other.addr, align 4
  %m_max3 = getelementptr inbounds %class.btAABB, %class.btAABB* %3, i32 0, i32 1
  %4 = bitcast %class.btVector3* %m_max to i8*
  %5 = bitcast %class.btVector3* %m_max3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  ret %class.btAABB* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.GIM_BVH_TREE_NODE*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.GIM_BVH_TREE_NODE*
  store %class.GIM_BVH_TREE_NODE* %2, %class.GIM_BVH_TREE_NODE** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.GIM_BVH_TREE_NODE* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.GIM_BVH_TREE_NODE* %4, %class.GIM_BVH_TREE_NODE** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.GIM_BVH_TREE_NODE* @_ZN17GIM_BVH_TREE_NODEC2ERKS_(%class.GIM_BVH_TREE_NODE* returned %this, %class.GIM_BVH_TREE_NODE* nonnull align 4 dereferenceable(36) %0) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  %.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  store %class.GIM_BVH_TREE_NODE* %this, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  store %class.GIM_BVH_TREE_NODE* %0, %class.GIM_BVH_TREE_NODE** %.addr, align 4
  %this1 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %this.addr, align 4
  %m_bound = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %this1, i32 0, i32 0
  %1 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %.addr, align 4
  %m_bound2 = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %1, i32 0, i32 0
  %call = call %class.btAABB* @_ZN6btAABBC2ERKS_(%class.btAABB* %m_bound, %class.btAABB* nonnull align 4 dereferenceable(32) %m_bound2)
  %m_escapeIndexOrDataIndex = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %this1, i32 0, i32 1
  %2 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %.addr, align 4
  %m_escapeIndexOrDataIndex3 = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %2, i32 0, i32 1
  %3 = load i32, i32* %m_escapeIndexOrDataIndex3, align 4
  store i32 %3, i32* %m_escapeIndexOrDataIndex, align 4
  ret %class.GIM_BVH_TREE_NODE* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.GIM_BVH_TREE_NODE* @_ZN18btAlignedAllocatorI17GIM_BVH_TREE_NODELj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.GIM_BVH_TREE_NODE** null)
  %2 = bitcast %class.GIM_BVH_TREE_NODE* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.GIM_BVH_TREE_NODE* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.GIM_BVH_TREE_NODE* %dest, %class.GIM_BVH_TREE_NODE** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %3, i32 %4
  %5 = bitcast %class.GIM_BVH_TREE_NODE* %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.GIM_BVH_TREE_NODE*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %7, i32 %8
  %call = call %class.GIM_BVH_TREE_NODE* @_ZN17GIM_BVH_TREE_NODEC2ERKS_(%class.GIM_BVH_TREE_NODE* %6, %class.GIM_BVH_TREE_NODE* nonnull align 4 dereferenceable(36) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.GIM_BVH_TREE_NODE, %class.GIM_BVH_TREE_NODE* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17GIM_BVH_TREE_NODEE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %m_data, align 4
  %tobool = icmp ne %class.GIM_BVH_TREE_NODE* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI17GIM_BVH_TREE_NODELj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %class.GIM_BVH_TREE_NODE* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.GIM_BVH_TREE_NODE* null, %class.GIM_BVH_TREE_NODE** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.GIM_BVH_TREE_NODE* @_ZN18btAlignedAllocatorI17GIM_BVH_TREE_NODELj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %class.GIM_BVH_TREE_NODE** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.GIM_BVH_TREE_NODE**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.GIM_BVH_TREE_NODE** %hint, %class.GIM_BVH_TREE_NODE*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 36, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.GIM_BVH_TREE_NODE*
  ret %class.GIM_BVH_TREE_NODE* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI17GIM_BVH_TREE_NODELj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %class.GIM_BVH_TREE_NODE* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.GIM_BVH_TREE_NODE*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %class.GIM_BVH_TREE_NODE* %ptr, %class.GIM_BVH_TREE_NODE** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.GIM_BVH_TREE_NODE*, %class.GIM_BVH_TREE_NODE** %ptr.addr, align 4
  %1 = bitcast %class.GIM_BVH_TREE_NODE* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.GIM_BVH_DATA*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.GIM_BVH_DATA*
  store %struct.GIM_BVH_DATA* %2, %struct.GIM_BVH_DATA** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %3 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %struct.GIM_BVH_DATA* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.GIM_BVH_DATA* %4, %struct.GIM_BVH_DATA** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE8capacityEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI12GIM_BVH_DATAE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.GIM_BVH_DATA* @_ZN18btAlignedAllocatorI12GIM_BVH_DATALj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %struct.GIM_BVH_DATA** null)
  %2 = bitcast %struct.GIM_BVH_DATA* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI12GIM_BVH_DATAE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %struct.GIM_BVH_DATA* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.GIM_BVH_DATA*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.GIM_BVH_DATA* %dest, %struct.GIM_BVH_DATA** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %3, i32 %4
  %5 = bitcast %struct.GIM_BVH_DATA* %arrayidx to i8*
  %6 = bitcast i8* %5 to %struct.GIM_BVH_DATA*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %7 = load %struct.GIM_BVH_DATA*, %struct.GIM_BVH_DATA** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.GIM_BVH_DATA, %struct.GIM_BVH_DATA* %7, i32 %8
  %call = call %struct.GIM_BVH_DATA* @_ZN12GIM_BVH_DATAC2ERKS_(%struct.GIM_BVH_DATA* %6, %struct.GIM_BVH_DATA* nonnull align 4 dereferenceable(36) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.GIM_BVH_DATA* @_ZN18btAlignedAllocatorI12GIM_BVH_DATALj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %this, i32 %n, %struct.GIM_BVH_DATA** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.GIM_BVH_DATA**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.GIM_BVH_DATA** %hint, %struct.GIM_BVH_DATA*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 36, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.GIM_BVH_DATA*
  ret %struct.GIM_BVH_DATA* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %1)
  %2 = bitcast i8* %call2 to i32*
  store i32* %2, i32** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %3 = load i32*, i32** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, i32* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load i32*, i32** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store i32* %4, i32** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIiE9allocSizeEi(%class.btAlignedObjectArray.4* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.5* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, i32* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store i32* %dest, i32** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = bitcast i32* %arrayidx to i8*
  %6 = bitcast i8* %5 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %7 = load i32*, i32** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %7, i32 %8
  %9 = load i32, i32* %arrayidx2, align 4
  store i32 %9, i32* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.5* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.5* %this, i32 %n, i32** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32** %hint, i32*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.5* %this, i32* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store i32* %ptr, i32** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btGImpactBvh.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
