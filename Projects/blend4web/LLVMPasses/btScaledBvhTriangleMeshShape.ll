; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btScaledBvhTriangleMeshShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btScaledBvhTriangleMeshShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btScaledBvhTriangleMeshShape = type { %class.btConcaveShape, %class.btVector3, %class.btBvhTriangleMeshShape* }
%class.btConcaveShape = type { %class.btCollisionShape, float }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btVector3 = type { [4 x float] }
%class.btBvhTriangleMeshShape = type <{ %class.btTriangleMeshShape, %class.btOptimizedBvh*, %struct.btTriangleInfoMap*, i8, i8, [11 x i8], [3 x i8] }>
%class.btTriangleMeshShape = type { %class.btConcaveShape, %class.btVector3, %class.btVector3, %class.btStridingMeshInterface* }
%class.btStridingMeshInterface = type { i32 (...)**, %class.btVector3 }
%class.btOptimizedBvh = type { %class.btQuantizedBvh }
%class.btQuantizedBvh = type { i32 (...)**, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32, i8, [3 x i8], %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0, i32, %class.btAlignedObjectArray.4, i32 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btOptimizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btOptimizedBvhNode = type { %class.btVector3, %class.btVector3, i32, i32, i32, [20 x i8] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btQuantizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btQuantizedBvhNode = type { [3 x i16], [3 x i16], i32 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btBvhSubtreeInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btBvhSubtreeInfo = type { [3 x i16], [3 x i16], i32, i32, [3 x i32] }
%struct.btTriangleInfoMap = type { i32 (...)**, %class.btHashMap, float, float, float, float, float, float }
%class.btHashMap = type { %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.16 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %struct.btTriangleInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%struct.btTriangleInfo = type { i32, float, float, float }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %class.btHashInt*, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%class.btHashInt = type { i32 }
%class.btTriangleCallback = type { i32 (...)** }
%class.btScaledTriangleCallback = type { %class.btTriangleCallback, %class.btTriangleCallback*, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btSerializer = type { i32 (...)** }
%struct.btScaledTriangleMeshShapeData = type { %struct.btTriangleMeshShapeData, %struct.btVector3FloatData }
%struct.btTriangleMeshShapeData = type { %struct.btCollisionShapeData, %struct.btStridingMeshInterfaceData, %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhDoubleData*, %struct.btTriangleInfoMapData*, float, [4 x i8] }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btStridingMeshInterfaceData = type { %struct.btMeshPartData*, %struct.btVector3FloatData, i32, [4 x i8] }
%struct.btMeshPartData = type { %struct.btVector3FloatData*, %struct.btVector3DoubleData*, %struct.btIntIndexData*, %struct.btShortIntIndexTripletData*, %struct.btCharIndexTripletData*, %struct.btShortIntIndexData*, i32, i32 }
%struct.btVector3DoubleData = type { [4 x double] }
%struct.btIntIndexData = type { i32 }
%struct.btShortIntIndexTripletData = type { [3 x i16], [2 x i8] }
%struct.btCharIndexTripletData = type { [3 x i8], i8 }
%struct.btShortIntIndexData = type { i16, [2 x i8] }
%struct.btQuantizedBvhFloatData = type { %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, i32, i32, i32, i32, %struct.btOptimizedBvhNodeFloatData*, %struct.btQuantizedBvhNodeData*, %struct.btBvhSubtreeInfoData*, i32, i32 }
%struct.btOptimizedBvhNodeFloatData = type { %struct.btVector3FloatData, %struct.btVector3FloatData, i32, i32, i32, [4 x i8] }
%struct.btQuantizedBvhNodeData = type { [3 x i16], [3 x i16], i32 }
%struct.btBvhSubtreeInfoData = type { i32, i32, [3 x i16], [3 x i16] }
%struct.btQuantizedBvhDoubleData = type { %struct.btVector3DoubleData, %struct.btVector3DoubleData, %struct.btVector3DoubleData, i32, i32, i32, i32, %struct.btOptimizedBvhNodeDoubleData*, %struct.btQuantizedBvhNodeData*, i32, i32, %struct.btBvhSubtreeInfoData* }
%struct.btOptimizedBvhNodeDoubleData = type { %struct.btVector3DoubleData, %struct.btVector3DoubleData, i32, i32, i32, [4 x i8] }
%struct.btTriangleInfoMapData = type { i32*, i32*, %struct.btTriangleInfoData*, i32*, float, float, float, float, float, i32, i32, i32, i32, [4 x i8] }
%struct.btTriangleInfoData = type { i32, float, float, float }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN28btScaledBvhTriangleMeshShapedlEPv = comdat any

$_ZN24btScaledTriangleCallbackC2EP18btTriangleCallbackRK9btVector3 = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN24btScaledTriangleCallbackD2Ev = comdat any

$_ZNK19btTriangleMeshShape15getLocalAabbMinEv = comdat any

$_ZNK19btTriangleMeshShape15getLocalAabbMaxEv = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x38absoluteEv = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_ZNK28btScaledBvhTriangleMeshShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN14btConcaveShape9setMarginEf = comdat any

$_ZNK14btConcaveShape9getMarginEv = comdat any

$_ZNK28btScaledBvhTriangleMeshShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK28btScaledBvhTriangleMeshShape9serializeEPvP12btSerializer = comdat any

$_ZN18btTriangleCallbackC2Ev = comdat any

$_ZN24btScaledTriangleCallbackD0Ev = comdat any

$_ZN24btScaledTriangleCallback15processTriangleEP9btVector3ii = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_Z6btFabsf = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

$_ZTV24btScaledTriangleCallback = comdat any

$_ZTS24btScaledTriangleCallback = comdat any

$_ZTI24btScaledTriangleCallback = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV28btScaledBvhTriangleMeshShape = hidden unnamed_addr constant { [19 x i8*] } { [19 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI28btScaledBvhTriangleMeshShape to i8*), i8* bitcast (%class.btScaledBvhTriangleMeshShape* (%class.btScaledBvhTriangleMeshShape*)* @_ZN28btScaledBvhTriangleMeshShapeD1Ev to i8*), i8* bitcast (void (%class.btScaledBvhTriangleMeshShape*)* @_ZN28btScaledBvhTriangleMeshShapeD0Ev to i8*), i8* bitcast (void (%class.btScaledBvhTriangleMeshShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK28btScaledBvhTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btScaledBvhTriangleMeshShape*, %class.btVector3*)* @_ZN28btScaledBvhTriangleMeshShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btScaledBvhTriangleMeshShape*)* @_ZNK28btScaledBvhTriangleMeshShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btScaledBvhTriangleMeshShape*, float, %class.btVector3*)* @_ZNK28btScaledBvhTriangleMeshShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btScaledBvhTriangleMeshShape*)* @_ZNK28btScaledBvhTriangleMeshShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConcaveShape*, float)* @_ZN14btConcaveShape9setMarginEf to i8*), i8* bitcast (float (%class.btConcaveShape*)* @_ZNK14btConcaveShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btScaledBvhTriangleMeshShape*)* @_ZNK28btScaledBvhTriangleMeshShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btScaledBvhTriangleMeshShape*, i8*, %class.btSerializer*)* @_ZNK28btScaledBvhTriangleMeshShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btScaledBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK28btScaledBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_ to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS28btScaledBvhTriangleMeshShape = hidden constant [31 x i8] c"28btScaledBvhTriangleMeshShape\00", align 1
@_ZTI14btConcaveShape = external constant i8*
@_ZTI28btScaledBvhTriangleMeshShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([31 x i8], [31 x i8]* @_ZTS28btScaledBvhTriangleMeshShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI14btConcaveShape to i8*) }, align 4
@_ZTV24btScaledTriangleCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI24btScaledTriangleCallback to i8*), i8* bitcast (%class.btScaledTriangleCallback* (%class.btScaledTriangleCallback*)* @_ZN24btScaledTriangleCallbackD2Ev to i8*), i8* bitcast (void (%class.btScaledTriangleCallback*)* @_ZN24btScaledTriangleCallbackD0Ev to i8*), i8* bitcast (void (%class.btScaledTriangleCallback*, %class.btVector3*, i32, i32)* @_ZN24btScaledTriangleCallback15processTriangleEP9btVector3ii to i8*)] }, comdat, align 4
@_ZTS24btScaledTriangleCallback = linkonce_odr hidden constant [27 x i8] c"24btScaledTriangleCallback\00", comdat, align 1
@_ZTI18btTriangleCallback = external constant i8*
@_ZTI24btScaledTriangleCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([27 x i8], [27 x i8]* @_ZTS24btScaledTriangleCallback, i32 0, i32 0), i8* bitcast (i8** @_ZTI18btTriangleCallback to i8*) }, comdat, align 4
@_ZTV18btTriangleCallback = external unnamed_addr constant { [5 x i8*] }, align 4
@.str = private unnamed_addr constant [22 x i8] c"SCALEDBVHTRIANGLEMESH\00", align 1
@.str.1 = private unnamed_addr constant [30 x i8] c"btScaledTriangleMeshShapeData\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btScaledBvhTriangleMeshShape.cpp, i8* null }]

@_ZN28btScaledBvhTriangleMeshShapeC1EP22btBvhTriangleMeshShapeRK9btVector3 = hidden unnamed_addr alias %class.btScaledBvhTriangleMeshShape* (%class.btScaledBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape*, %class.btVector3*), %class.btScaledBvhTriangleMeshShape* (%class.btScaledBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape*, %class.btVector3*)* @_ZN28btScaledBvhTriangleMeshShapeC2EP22btBvhTriangleMeshShapeRK9btVector3
@_ZN28btScaledBvhTriangleMeshShapeD1Ev = hidden unnamed_addr alias %class.btScaledBvhTriangleMeshShape* (%class.btScaledBvhTriangleMeshShape*), %class.btScaledBvhTriangleMeshShape* (%class.btScaledBvhTriangleMeshShape*)* @_ZN28btScaledBvhTriangleMeshShapeD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btScaledBvhTriangleMeshShape* @_ZN28btScaledBvhTriangleMeshShapeC2EP22btBvhTriangleMeshShapeRK9btVector3(%class.btScaledBvhTriangleMeshShape* returned %this, %class.btBvhTriangleMeshShape* %childShape, %class.btVector3* nonnull align 4 dereferenceable(16) %localScaling) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  %childShape.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %localScaling.addr = alloca %class.btVector3*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  store %class.btBvhTriangleMeshShape* %childShape, %class.btBvhTriangleMeshShape** %childShape.addr, align 4
  store %class.btVector3* %localScaling, %class.btVector3** %localScaling.addr, align 4
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %0 = bitcast %class.btScaledBvhTriangleMeshShape* %this1 to %class.btConcaveShape*
  %call = call %class.btConcaveShape* @_ZN14btConcaveShapeC2Ev(%class.btConcaveShape* %0)
  %1 = bitcast %class.btScaledBvhTriangleMeshShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV28btScaledBvhTriangleMeshShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_localScaling = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %2 = load %class.btVector3*, %class.btVector3** %localScaling.addr, align 4
  %3 = bitcast %class.btVector3* %m_localScaling to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %m_bvhTriMeshShape = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 2
  %5 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %childShape.addr, align 4
  store %class.btBvhTriangleMeshShape* %5, %class.btBvhTriangleMeshShape** %m_bvhTriMeshShape, align 4
  %6 = bitcast %class.btScaledBvhTriangleMeshShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %6, i32 0, i32 1
  store i32 22, i32* %m_shapeType, align 4
  ret %class.btScaledBvhTriangleMeshShape* %this1
}

declare %class.btConcaveShape* @_ZN14btConcaveShapeC2Ev(%class.btConcaveShape* returned) unnamed_addr #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline nounwind optnone
define hidden %class.btScaledBvhTriangleMeshShape* @_ZN28btScaledBvhTriangleMeshShapeD2Ev(%class.btScaledBvhTriangleMeshShape* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %0 = bitcast %class.btScaledBvhTriangleMeshShape* %this1 to %class.btConcaveShape*
  %call = call %class.btConcaveShape* @_ZN14btConcaveShapeD2Ev(%class.btConcaveShape* %0) #8
  ret %class.btScaledBvhTriangleMeshShape* %this1
}

; Function Attrs: nounwind
declare %class.btConcaveShape* @_ZN14btConcaveShapeD2Ev(%class.btConcaveShape* returned) unnamed_addr #5

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN28btScaledBvhTriangleMeshShapeD0Ev(%class.btScaledBvhTriangleMeshShape* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %call = call %class.btScaledBvhTriangleMeshShape* @_ZN28btScaledBvhTriangleMeshShapeD1Ev(%class.btScaledBvhTriangleMeshShape* %this1) #8
  %0 = bitcast %class.btScaledBvhTriangleMeshShape* %this1 to i8*
  call void @_ZN28btScaledBvhTriangleMeshShapedlEPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN28btScaledBvhTriangleMeshShapedlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK28btScaledBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_(%class.btScaledBvhTriangleMeshShape* %this, %class.btTriangleCallback* %callback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %scaledCallback = alloca %class.btScaledTriangleCallback, align 4
  %invLocalScaling = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %scaledAabbMin = alloca %class.btVector3, align 4
  %scaledAabbMax = alloca %class.btVector3, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %0 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call = call %class.btScaledTriangleCallback* @_ZN24btScaledTriangleCallbackC2EP18btTriangleCallbackRK9btVector3(%class.btScaledTriangleCallback* %scaledCallback, %class.btTriangleCallback* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  %m_localScaling2 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_localScaling2)
  %1 = load float, float* %call3, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %m_localScaling5 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %m_localScaling5)
  %2 = load float, float* %call6, align 4
  %div7 = fdiv float 1.000000e+00, %2
  store float %div7, float* %ref.tmp4, align 4
  %m_localScaling9 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %m_localScaling9)
  %3 = load float, float* %call10, align 4
  %div11 = fdiv float 1.000000e+00, %3
  store float %div11, float* %ref.tmp8, align 4
  %call12 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %invLocalScaling, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %call13 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %scaledAabbMin)
  %call14 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %scaledAabbMax)
  %m_localScaling15 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_localScaling15)
  %4 = load float, float* %call16, align 4
  %conv = fpext float %4 to double
  %cmp = fcmp oge double %conv, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %5 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %call17 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %5)
  %arrayidx = getelementptr inbounds float, float* %call17, i32 0
  %6 = load float, float* %arrayidx, align 4
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 0
  %7 = load float, float* %arrayidx19, align 4
  %mul = fmul float %6, %7
  br label %cond.end

cond.false:                                       ; preds = %entry
  %8 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %call20 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 0
  %9 = load float, float* %arrayidx21, align 4
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 0
  %10 = load float, float* %arrayidx23, align 4
  %mul24 = fmul float %9, %10
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %mul, %cond.true ], [ %mul24, %cond.false ]
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaledAabbMin)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 0
  store float %cond, float* %arrayidx26, align 4
  %m_localScaling27 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %m_localScaling27)
  %11 = load float, float* %call28, align 4
  %conv29 = fpext float %11 to double
  %cmp30 = fcmp oge double %conv29, 0.000000e+00
  br i1 %cmp30, label %cond.true31, label %cond.false37

cond.true31:                                      ; preds = %cond.end
  %12 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %call32 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %12)
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 1
  %13 = load float, float* %arrayidx33, align 4
  %call34 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 1
  %14 = load float, float* %arrayidx35, align 4
  %mul36 = fmul float %13, %14
  br label %cond.end43

cond.false37:                                     ; preds = %cond.end
  %15 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %call38 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %15)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 1
  %16 = load float, float* %arrayidx39, align 4
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 1
  %17 = load float, float* %arrayidx41, align 4
  %mul42 = fmul float %16, %17
  br label %cond.end43

cond.end43:                                       ; preds = %cond.false37, %cond.true31
  %cond44 = phi float [ %mul36, %cond.true31 ], [ %mul42, %cond.false37 ]
  %call45 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaledAabbMin)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 1
  store float %cond44, float* %arrayidx46, align 4
  %m_localScaling47 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %m_localScaling47)
  %18 = load float, float* %call48, align 4
  %conv49 = fpext float %18 to double
  %cmp50 = fcmp oge double %conv49, 0.000000e+00
  br i1 %cmp50, label %cond.true51, label %cond.false57

cond.true51:                                      ; preds = %cond.end43
  %19 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %call52 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %19)
  %arrayidx53 = getelementptr inbounds float, float* %call52, i32 2
  %20 = load float, float* %arrayidx53, align 4
  %call54 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 2
  %21 = load float, float* %arrayidx55, align 4
  %mul56 = fmul float %20, %21
  br label %cond.end63

cond.false57:                                     ; preds = %cond.end43
  %22 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %call58 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %22)
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 2
  %23 = load float, float* %arrayidx59, align 4
  %call60 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx61 = getelementptr inbounds float, float* %call60, i32 2
  %24 = load float, float* %arrayidx61, align 4
  %mul62 = fmul float %23, %24
  br label %cond.end63

cond.end63:                                       ; preds = %cond.false57, %cond.true51
  %cond64 = phi float [ %mul56, %cond.true51 ], [ %mul62, %cond.false57 ]
  %call65 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaledAabbMin)
  %arrayidx66 = getelementptr inbounds float, float* %call65, i32 2
  store float %cond64, float* %arrayidx66, align 4
  %call67 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaledAabbMin)
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 3
  store float 0.000000e+00, float* %arrayidx68, align 4
  %m_localScaling69 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call70 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_localScaling69)
  %25 = load float, float* %call70, align 4
  %conv71 = fpext float %25 to double
  %cmp72 = fcmp ole double %conv71, 0.000000e+00
  br i1 %cmp72, label %cond.true73, label %cond.false79

cond.true73:                                      ; preds = %cond.end63
  %26 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %call74 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %26)
  %arrayidx75 = getelementptr inbounds float, float* %call74, i32 0
  %27 = load float, float* %arrayidx75, align 4
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 0
  %28 = load float, float* %arrayidx77, align 4
  %mul78 = fmul float %27, %28
  br label %cond.end85

cond.false79:                                     ; preds = %cond.end63
  %29 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %call80 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %29)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 0
  %30 = load float, float* %arrayidx81, align 4
  %call82 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx83 = getelementptr inbounds float, float* %call82, i32 0
  %31 = load float, float* %arrayidx83, align 4
  %mul84 = fmul float %30, %31
  br label %cond.end85

cond.end85:                                       ; preds = %cond.false79, %cond.true73
  %cond86 = phi float [ %mul78, %cond.true73 ], [ %mul84, %cond.false79 ]
  %call87 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaledAabbMax)
  %arrayidx88 = getelementptr inbounds float, float* %call87, i32 0
  store float %cond86, float* %arrayidx88, align 4
  %m_localScaling89 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call90 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %m_localScaling89)
  %32 = load float, float* %call90, align 4
  %conv91 = fpext float %32 to double
  %cmp92 = fcmp ole double %conv91, 0.000000e+00
  br i1 %cmp92, label %cond.true93, label %cond.false99

cond.true93:                                      ; preds = %cond.end85
  %33 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %call94 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %33)
  %arrayidx95 = getelementptr inbounds float, float* %call94, i32 1
  %34 = load float, float* %arrayidx95, align 4
  %call96 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx97 = getelementptr inbounds float, float* %call96, i32 1
  %35 = load float, float* %arrayidx97, align 4
  %mul98 = fmul float %34, %35
  br label %cond.end105

cond.false99:                                     ; preds = %cond.end85
  %36 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %call100 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %36)
  %arrayidx101 = getelementptr inbounds float, float* %call100, i32 1
  %37 = load float, float* %arrayidx101, align 4
  %call102 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx103 = getelementptr inbounds float, float* %call102, i32 1
  %38 = load float, float* %arrayidx103, align 4
  %mul104 = fmul float %37, %38
  br label %cond.end105

cond.end105:                                      ; preds = %cond.false99, %cond.true93
  %cond106 = phi float [ %mul98, %cond.true93 ], [ %mul104, %cond.false99 ]
  %call107 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaledAabbMax)
  %arrayidx108 = getelementptr inbounds float, float* %call107, i32 1
  store float %cond106, float* %arrayidx108, align 4
  %m_localScaling109 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call110 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %m_localScaling109)
  %39 = load float, float* %call110, align 4
  %conv111 = fpext float %39 to double
  %cmp112 = fcmp ole double %conv111, 0.000000e+00
  br i1 %cmp112, label %cond.true113, label %cond.false119

cond.true113:                                     ; preds = %cond.end105
  %40 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %call114 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %40)
  %arrayidx115 = getelementptr inbounds float, float* %call114, i32 2
  %41 = load float, float* %arrayidx115, align 4
  %call116 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx117 = getelementptr inbounds float, float* %call116, i32 2
  %42 = load float, float* %arrayidx117, align 4
  %mul118 = fmul float %41, %42
  br label %cond.end125

cond.false119:                                    ; preds = %cond.end105
  %43 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %call120 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %43)
  %arrayidx121 = getelementptr inbounds float, float* %call120, i32 2
  %44 = load float, float* %arrayidx121, align 4
  %call122 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %invLocalScaling)
  %arrayidx123 = getelementptr inbounds float, float* %call122, i32 2
  %45 = load float, float* %arrayidx123, align 4
  %mul124 = fmul float %44, %45
  br label %cond.end125

cond.end125:                                      ; preds = %cond.false119, %cond.true113
  %cond126 = phi float [ %mul118, %cond.true113 ], [ %mul124, %cond.false119 ]
  %call127 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaledAabbMax)
  %arrayidx128 = getelementptr inbounds float, float* %call127, i32 2
  store float %cond126, float* %arrayidx128, align 4
  %call129 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scaledAabbMax)
  %arrayidx130 = getelementptr inbounds float, float* %call129, i32 3
  store float 0.000000e+00, float* %arrayidx130, align 4
  %m_bvhTriMeshShape = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 2
  %46 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %m_bvhTriMeshShape, align 4
  %47 = bitcast %class.btScaledTriangleCallback* %scaledCallback to %class.btTriangleCallback*
  %48 = bitcast %class.btBvhTriangleMeshShape* %46 to void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*** %48, align 4
  %vfn = getelementptr inbounds void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)** %vtable, i64 16
  %49 = load void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %49(%class.btBvhTriangleMeshShape* %46, %class.btTriangleCallback* %47, %class.btVector3* nonnull align 4 dereferenceable(16) %scaledAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %scaledAabbMax)
  %call131 = call %class.btScaledTriangleCallback* @_ZN24btScaledTriangleCallbackD2Ev(%class.btScaledTriangleCallback* %scaledCallback) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btScaledTriangleCallback* @_ZN24btScaledTriangleCallbackC2EP18btTriangleCallbackRK9btVector3(%class.btScaledTriangleCallback* returned %this, %class.btTriangleCallback* %originalCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %localScaling) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btScaledTriangleCallback*, align 4
  %originalCallback.addr = alloca %class.btTriangleCallback*, align 4
  %localScaling.addr = alloca %class.btVector3*, align 4
  store %class.btScaledTriangleCallback* %this, %class.btScaledTriangleCallback** %this.addr, align 4
  store %class.btTriangleCallback* %originalCallback, %class.btTriangleCallback** %originalCallback.addr, align 4
  store %class.btVector3* %localScaling, %class.btVector3** %localScaling.addr, align 4
  %this1 = load %class.btScaledTriangleCallback*, %class.btScaledTriangleCallback** %this.addr, align 4
  %0 = bitcast %class.btScaledTriangleCallback* %this1 to %class.btTriangleCallback*
  %call = call %class.btTriangleCallback* @_ZN18btTriangleCallbackC2Ev(%class.btTriangleCallback* %0) #8
  %1 = bitcast %class.btScaledTriangleCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV24btScaledTriangleCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_originalCallback = getelementptr inbounds %class.btScaledTriangleCallback, %class.btScaledTriangleCallback* %this1, i32 0, i32 1
  %2 = load %class.btTriangleCallback*, %class.btTriangleCallback** %originalCallback.addr, align 4
  store %class.btTriangleCallback* %2, %class.btTriangleCallback** %m_originalCallback, align 4
  %m_localScaling = getelementptr inbounds %class.btScaledTriangleCallback, %class.btScaledTriangleCallback* %this1, i32 0, i32 2
  %3 = load %class.btVector3*, %class.btVector3** %localScaling.addr, align 4
  %4 = bitcast %class.btVector3* %m_localScaling to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  ret %class.btScaledTriangleCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btScaledTriangleCallback* @_ZN24btScaledTriangleCallbackD2Ev(%class.btScaledTriangleCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btScaledTriangleCallback*, align 4
  store %class.btScaledTriangleCallback* %this, %class.btScaledTriangleCallback** %this.addr, align 4
  %this1 = load %class.btScaledTriangleCallback*, %class.btScaledTriangleCallback** %this.addr, align 4
  %0 = bitcast %class.btScaledTriangleCallback* %this1 to %class.btTriangleCallback*
  %call = call %class.btTriangleCallback* @_ZN18btTriangleCallbackD2Ev(%class.btTriangleCallback* %0) #8
  ret %class.btScaledTriangleCallback* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZNK28btScaledBvhTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_(%class.btScaledBvhTriangleMeshShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %localAabbMin = alloca %class.btVector3, align 4
  %localAabbMax = alloca %class.btVector3, align 4
  %tmpLocalAabbMin = alloca %class.btVector3, align 4
  %tmpLocalAabbMax = alloca %class.btVector3, align 4
  %localHalfExtents = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp82 = alloca %class.btVector3, align 4
  %margin = alloca float, align 4
  %ref.tmp85 = alloca %class.btVector3, align 4
  %localCenter = alloca %class.btVector3, align 4
  %ref.tmp88 = alloca float, align 4
  %ref.tmp89 = alloca %class.btVector3, align 4
  %abs_b = alloca %class.btMatrix3x3, align 4
  %center = alloca %class.btVector3, align 4
  %extent = alloca %class.btVector3, align 4
  %ref.tmp94 = alloca %class.btVector3, align 4
  %ref.tmp95 = alloca %class.btVector3, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %m_bvhTriMeshShape = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 2
  %0 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %m_bvhTriMeshShape, align 4
  %1 = bitcast %class.btBvhTriangleMeshShape* %0 to %class.btTriangleMeshShape*
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK19btTriangleMeshShape15getLocalAabbMinEv(%class.btTriangleMeshShape* %1)
  %2 = bitcast %class.btVector3* %localAabbMin to i8*
  %3 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %m_bvhTriMeshShape2 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 2
  %4 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %m_bvhTriMeshShape2, align 4
  %5 = bitcast %class.btBvhTriangleMeshShape* %4 to %class.btTriangleMeshShape*
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK19btTriangleMeshShape15getLocalAabbMaxEv(%class.btTriangleMeshShape* %5)
  %6 = bitcast %class.btVector3* %localAabbMax to i8*
  %7 = bitcast %class.btVector3* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_localScaling = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %tmpLocalAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  %m_localScaling4 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %tmpLocalAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling4)
  %m_localScaling5 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_localScaling5)
  %8 = load float, float* %call6, align 4
  %conv = fpext float %8 to double
  %cmp = fcmp oge double %conv, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMin)
  %arrayidx = getelementptr inbounds float, float* %call7, i32 0
  %9 = load float, float* %arrayidx, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMax)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 0
  %10 = load float, float* %arrayidx9, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %9, %cond.true ], [ %10, %cond.false ]
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMin)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 0
  store float %cond, float* %arrayidx11, align 4
  %m_localScaling12 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %m_localScaling12)
  %11 = load float, float* %call13, align 4
  %conv14 = fpext float %11 to double
  %cmp15 = fcmp oge double %conv14, 0.000000e+00
  br i1 %cmp15, label %cond.true16, label %cond.false19

cond.true16:                                      ; preds = %cond.end
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMin)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  %12 = load float, float* %arrayidx18, align 4
  br label %cond.end22

cond.false19:                                     ; preds = %cond.end
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMax)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 1
  %13 = load float, float* %arrayidx21, align 4
  br label %cond.end22

cond.end22:                                       ; preds = %cond.false19, %cond.true16
  %cond23 = phi float [ %12, %cond.true16 ], [ %13, %cond.false19 ]
  %call24 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMin)
  %arrayidx25 = getelementptr inbounds float, float* %call24, i32 1
  store float %cond23, float* %arrayidx25, align 4
  %m_localScaling26 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %m_localScaling26)
  %14 = load float, float* %call27, align 4
  %conv28 = fpext float %14 to double
  %cmp29 = fcmp oge double %conv28, 0.000000e+00
  br i1 %cmp29, label %cond.true30, label %cond.false33

cond.true30:                                      ; preds = %cond.end22
  %call31 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMin)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 2
  %15 = load float, float* %arrayidx32, align 4
  br label %cond.end36

cond.false33:                                     ; preds = %cond.end22
  %call34 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMax)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 2
  %16 = load float, float* %arrayidx35, align 4
  br label %cond.end36

cond.end36:                                       ; preds = %cond.false33, %cond.true30
  %cond37 = phi float [ %15, %cond.true30 ], [ %16, %cond.false33 ]
  %call38 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMin)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 2
  store float %cond37, float* %arrayidx39, align 4
  %m_localScaling40 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_localScaling40)
  %17 = load float, float* %call41, align 4
  %conv42 = fpext float %17 to double
  %cmp43 = fcmp ole double %conv42, 0.000000e+00
  br i1 %cmp43, label %cond.true44, label %cond.false47

cond.true44:                                      ; preds = %cond.end36
  %call45 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMin)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 0
  %18 = load float, float* %arrayidx46, align 4
  br label %cond.end50

cond.false47:                                     ; preds = %cond.end36
  %call48 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMax)
  %arrayidx49 = getelementptr inbounds float, float* %call48, i32 0
  %19 = load float, float* %arrayidx49, align 4
  br label %cond.end50

cond.end50:                                       ; preds = %cond.false47, %cond.true44
  %cond51 = phi float [ %18, %cond.true44 ], [ %19, %cond.false47 ]
  %call52 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMax)
  %arrayidx53 = getelementptr inbounds float, float* %call52, i32 0
  store float %cond51, float* %arrayidx53, align 4
  %m_localScaling54 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %m_localScaling54)
  %20 = load float, float* %call55, align 4
  %conv56 = fpext float %20 to double
  %cmp57 = fcmp ole double %conv56, 0.000000e+00
  br i1 %cmp57, label %cond.true58, label %cond.false61

cond.true58:                                      ; preds = %cond.end50
  %call59 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMin)
  %arrayidx60 = getelementptr inbounds float, float* %call59, i32 1
  %21 = load float, float* %arrayidx60, align 4
  br label %cond.end64

cond.false61:                                     ; preds = %cond.end50
  %call62 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMax)
  %arrayidx63 = getelementptr inbounds float, float* %call62, i32 1
  %22 = load float, float* %arrayidx63, align 4
  br label %cond.end64

cond.end64:                                       ; preds = %cond.false61, %cond.true58
  %cond65 = phi float [ %21, %cond.true58 ], [ %22, %cond.false61 ]
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMax)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 1
  store float %cond65, float* %arrayidx67, align 4
  %m_localScaling68 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %call69 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %m_localScaling68)
  %23 = load float, float* %call69, align 4
  %conv70 = fpext float %23 to double
  %cmp71 = fcmp ole double %conv70, 0.000000e+00
  br i1 %cmp71, label %cond.true72, label %cond.false75

cond.true72:                                      ; preds = %cond.end64
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMin)
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 2
  %24 = load float, float* %arrayidx74, align 4
  br label %cond.end78

cond.false75:                                     ; preds = %cond.end64
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpLocalAabbMax)
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 2
  %25 = load float, float* %arrayidx77, align 4
  br label %cond.end78

cond.end78:                                       ; preds = %cond.false75, %cond.true72
  %cond79 = phi float [ %24, %cond.true72 ], [ %25, %cond.false75 ]
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMax)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 2
  store float %cond79, float* %arrayidx81, align 4
  store float 5.000000e-01, float* %ref.tmp, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp82, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localHalfExtents, float* nonnull align 4 dereferenceable(4) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp82)
  %m_bvhTriMeshShape83 = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 2
  %26 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %m_bvhTriMeshShape83, align 4
  %27 = bitcast %class.btBvhTriangleMeshShape* %26 to %class.btConcaveShape*
  %28 = bitcast %class.btConcaveShape* %27 to float (%class.btConcaveShape*)***
  %vtable = load float (%class.btConcaveShape*)**, float (%class.btConcaveShape*)*** %28, align 4
  %vfn = getelementptr inbounds float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vtable, i64 12
  %29 = load float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vfn, align 4
  %call84 = call float %29(%class.btConcaveShape* %27)
  store float %call84, float* %margin, align 4
  %call86 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp85, float* nonnull align 4 dereferenceable(4) %margin, float* nonnull align 4 dereferenceable(4) %margin, float* nonnull align 4 dereferenceable(4) %margin)
  %call87 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp85)
  store float 5.000000e-01, float* %ref.tmp88, align 4
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp89, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localCenter, float* nonnull align 4 dereferenceable(4) %ref.tmp88, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp89)
  %30 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  %call90 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %30)
  call void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* sret align 4 %abs_b, %class.btMatrix3x3* %call90)
  %31 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %center, %class.btTransform* %31, %class.btVector3* nonnull align 4 dereferenceable(16) %localCenter)
  %call91 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 0)
  %call92 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 1)
  %call93 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %extent, %class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %call91, %class.btVector3* nonnull align 4 dereferenceable(16) %call92, %class.btVector3* nonnull align 4 dereferenceable(16) %call93)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp94, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %32 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %33 = bitcast %class.btVector3* %32 to i8*
  %34 = bitcast %class.btVector3* %ref.tmp94 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 4 %34, i32 16, i1 false)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp95, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %35 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %36 = bitcast %class.btVector3* %35 to i8*
  %37 = bitcast %class.btVector3* %ref.tmp95 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %36, i8* align 4 %37, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK19btTriangleMeshShape15getLocalAabbMinEv(%class.btTriangleMeshShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleMeshShape*, align 4
  store %class.btTriangleMeshShape* %this, %class.btTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btTriangleMeshShape*, %class.btTriangleMeshShape** %this.addr, align 4
  %m_localAabbMin = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localAabbMin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK19btTriangleMeshShape15getLocalAabbMaxEv(%class.btTriangleMeshShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleMeshShape*, align 4
  store %class.btTriangleMeshShape* %this, %class.btTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btTriangleMeshShape*, %class.btTriangleMeshShape** %this.addr, align 4
  %m_localAabbMax = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %this1, i32 0, i32 2
  ret %class.btVector3* %m_localAabbMax
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %1 = load float, float* %call, align 4
  %call2 = call float @_Z6btFabsf(float %1)
  store float %call2, float* %ref.tmp, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx5)
  %2 = load float, float* %call6, align 4
  %call7 = call float @_Z6btFabsf(float %2)
  store float %call7, float* %ref.tmp3, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 0
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx10)
  %3 = load float, float* %call11, align 4
  %call12 = call float @_Z6btFabsf(float %3)
  store float %call12, float* %ref.tmp8, align 4
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx15)
  %4 = load float, float* %call16, align 4
  %call17 = call float @_Z6btFabsf(float %4)
  store float %call17, float* %ref.tmp13, align 4
  %m_el19 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el19, i32 0, i32 1
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx20)
  %5 = load float, float* %call21, align 4
  %call22 = call float @_Z6btFabsf(float %5)
  store float %call22, float* %ref.tmp18, align 4
  %m_el24 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el24, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx25)
  %6 = load float, float* %call26, align 4
  %call27 = call float @_Z6btFabsf(float %6)
  store float %call27, float* %ref.tmp23, align 4
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 2
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %7 = load float, float* %call31, align 4
  %call32 = call float @_Z6btFabsf(float %7)
  store float %call32, float* %ref.tmp28, align 4
  %m_el34 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el34, i32 0, i32 2
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx35)
  %8 = load float, float* %call36, align 4
  %call37 = call float @_Z6btFabsf(float %8)
  store float %call37, float* %ref.tmp33, align 4
  %m_el39 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el39, i32 0, i32 2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx40)
  %9 = load float, float* %call41, align 4
  %call42 = call float @_Z6btFabsf(float %9)
  store float %call42, float* %ref.tmp38, align 4
  %call43 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp38)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN28btScaledBvhTriangleMeshShape15setLocalScalingERK9btVector3(%class.btScaledBvhTriangleMeshShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_localScaling to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK28btScaledBvhTriangleMeshShape15getLocalScalingEv(%class.btScaledBvhTriangleMeshShape* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localScaling
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK28btScaledBvhTriangleMeshShape21calculateLocalInertiaEfR9btVector3(%class.btScaledBvhTriangleMeshShape* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  store float %mass, float* %mass.addr, align 4
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #3

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #3

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK28btScaledBvhTriangleMeshShape7getNameEv(%class.btScaledBvhTriangleMeshShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  store float 1.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN14btConcaveShape9setMarginEf(%class.btConcaveShape* %this, float %collisionMargin) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConcaveShape*, align 4
  %collisionMargin.addr = alloca float, align 4
  store %class.btConcaveShape* %this, %class.btConcaveShape** %this.addr, align 4
  store float %collisionMargin, float* %collisionMargin.addr, align 4
  %this1 = load %class.btConcaveShape*, %class.btConcaveShape** %this.addr, align 4
  %0 = load float, float* %collisionMargin.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %this1, i32 0, i32 1
  store float %0, float* %m_collisionMargin, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK14btConcaveShape9getMarginEv(%class.btConcaveShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConcaveShape*, align 4
  store %class.btConcaveShape* %this, %class.btConcaveShape** %this.addr, align 4
  %this1 = load %class.btConcaveShape*, %class.btConcaveShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %this1, i32 0, i32 1
  %0 = load float, float* %m_collisionMargin, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK28btScaledBvhTriangleMeshShape28calculateSerializeBufferSizeEv(%class.btScaledBvhTriangleMeshShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  ret i32 76
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZNK28btScaledBvhTriangleMeshShape9serializeEPvP12btSerializer(%class.btScaledBvhTriangleMeshShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %scaledMeshData = alloca %struct.btScaledTriangleMeshShapeData*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btScaledTriangleMeshShapeData*
  store %struct.btScaledTriangleMeshShapeData* %1, %struct.btScaledTriangleMeshShapeData** %scaledMeshData, align 4
  %m_bvhTriMeshShape = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 2
  %2 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %m_bvhTriMeshShape, align 4
  %3 = load %struct.btScaledTriangleMeshShapeData*, %struct.btScaledTriangleMeshShapeData** %scaledMeshData, align 4
  %m_trimeshShapeData = getelementptr inbounds %struct.btScaledTriangleMeshShapeData, %struct.btScaledTriangleMeshShapeData* %3, i32 0, i32 0
  %4 = bitcast %struct.btTriangleMeshShapeData* %m_trimeshShapeData to i8*
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %6 = bitcast %class.btBvhTriangleMeshShape* %2 to i8* (%class.btBvhTriangleMeshShape*, i8*, %class.btSerializer*)***
  %vtable = load i8* (%class.btBvhTriangleMeshShape*, i8*, %class.btSerializer*)**, i8* (%class.btBvhTriangleMeshShape*, i8*, %class.btSerializer*)*** %6, align 4
  %vfn = getelementptr inbounds i8* (%class.btBvhTriangleMeshShape*, i8*, %class.btSerializer*)*, i8* (%class.btBvhTriangleMeshShape*, i8*, %class.btSerializer*)** %vtable, i64 14
  %7 = load i8* (%class.btBvhTriangleMeshShape*, i8*, %class.btSerializer*)*, i8* (%class.btBvhTriangleMeshShape*, i8*, %class.btSerializer*)** %vfn, align 4
  %call = call i8* %7(%class.btBvhTriangleMeshShape* %2, i8* %4, %class.btSerializer* %5)
  %8 = load %struct.btScaledTriangleMeshShapeData*, %struct.btScaledTriangleMeshShapeData** %scaledMeshData, align 4
  %m_trimeshShapeData2 = getelementptr inbounds %struct.btScaledTriangleMeshShapeData, %struct.btScaledTriangleMeshShapeData* %8, i32 0, i32 0
  %m_collisionShapeData = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %m_trimeshShapeData2, i32 0, i32 0
  %m_shapeType = getelementptr inbounds %struct.btCollisionShapeData, %struct.btCollisionShapeData* %m_collisionShapeData, i32 0, i32 1
  store i32 22, i32* %m_shapeType, align 4
  %m_localScaling = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 1
  %9 = load %struct.btScaledTriangleMeshShapeData*, %struct.btScaledTriangleMeshShapeData** %scaledMeshData, align 4
  %m_localScaling3 = getelementptr inbounds %struct.btScaledTriangleMeshShapeData, %struct.btScaledTriangleMeshShapeData* %9, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_localScaling, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_localScaling3)
  ret i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.1, i32 0, i32 0)
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #3

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btTriangleCallback* @_ZN18btTriangleCallbackC2Ev(%class.btTriangleCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleCallback*, align 4
  store %class.btTriangleCallback* %this, %class.btTriangleCallback** %this.addr, align 4
  %this1 = load %class.btTriangleCallback*, %class.btTriangleCallback** %this.addr, align 4
  %0 = bitcast %class.btTriangleCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV18btTriangleCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btTriangleCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN24btScaledTriangleCallbackD0Ev(%class.btScaledTriangleCallback* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btScaledTriangleCallback*, align 4
  store %class.btScaledTriangleCallback* %this, %class.btScaledTriangleCallback** %this.addr, align 4
  %this1 = load %class.btScaledTriangleCallback*, %class.btScaledTriangleCallback** %this.addr, align 4
  %call = call %class.btScaledTriangleCallback* @_ZN24btScaledTriangleCallbackD2Ev(%class.btScaledTriangleCallback* %this1) #8
  %0 = bitcast %class.btScaledTriangleCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN24btScaledTriangleCallback15processTriangleEP9btVector3ii(%class.btScaledTriangleCallback* %this, %class.btVector3* %triangle, i32 %partId, i32 %triangleIndex) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btScaledTriangleCallback*, align 4
  %triangle.addr = alloca %class.btVector3*, align 4
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  %newTriangle = alloca [3 x %class.btVector3], align 16
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  store %class.btScaledTriangleCallback* %this, %class.btScaledTriangleCallback** %this.addr, align 4
  store %class.btVector3* %triangle, %class.btVector3** %triangle.addr, align 4
  store i32 %partId, i32* %partId.addr, align 4
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4
  %this1 = load %class.btScaledTriangleCallback*, %class.btScaledTriangleCallback** %this.addr, align 4
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %newTriangle, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0
  %m_localScaling = getelementptr inbounds %class.btScaledTriangleCallback, %class.btScaledTriangleCallback* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  %arrayidx2 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %newTriangle, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx2 to i8*
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 1
  %m_localScaling5 = getelementptr inbounds %class.btScaledTriangleCallback, %class.btScaledTriangleCallback* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling5)
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %newTriangle, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx6 to i8*
  %5 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx8 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 2
  %m_localScaling9 = getelementptr inbounds %class.btScaledTriangleCallback, %class.btScaledTriangleCallback* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx8, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling9)
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %newTriangle, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx10 to i8*
  %8 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %7, i8* align 4 %8, i32 16, i1 false)
  %m_originalCallback = getelementptr inbounds %class.btScaledTriangleCallback, %class.btScaledTriangleCallback* %this1, i32 0, i32 1
  %9 = load %class.btTriangleCallback*, %class.btTriangleCallback** %m_originalCallback, align 4
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %newTriangle, i32 0, i32 0
  %10 = load i32, i32* %partId.addr, align 4
  %11 = load i32, i32* %triangleIndex.addr, align 4
  %12 = bitcast %class.btTriangleCallback* %9 to void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)***
  %vtable = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)**, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*** %12, align 4
  %vfn = getelementptr inbounds void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vtable, i64 2
  %13 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vfn, align 4
  call void %13(%class.btTriangleCallback* %9, %class.btVector3* %arrayidx11, i32 %10, i32 %11)
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

; Function Attrs: nounwind
declare %class.btTriangleCallback* @_ZN18btTriangleCallbackD2Ev(%class.btTriangleCallback* returned) unnamed_addr #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #7

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %2, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btScaledBvhTriangleMeshShape.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
