; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Featherstone/btMultiBodyDynamicsWorld.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Featherstone/btMultiBodyDynamicsWorld.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btMultiBodyDynamicsWorld = type { %class.btDiscreteDynamicsWorld, %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51, %class.btMultiBodyConstraintSolver*, %struct.MultiBodyInplaceSolverIslandCallback*, %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.46 }
%class.btDiscreteDynamicsWorld = type { %class.btDynamicsWorld, %class.btAlignedObjectArray.4, %struct.InplaceSolverIslandCallback*, %class.btConstraintSolver*, %class.btSimulationIslandManager*, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.19, %class.btVector3, float, float, i8, i8, i8, i8, %class.btAlignedObjectArray.23, i32, i8, [3 x i8], %class.btAlignedObjectArray.13, %class.btSpinMutex }
%class.btDynamicsWorld = type { %class.btCollisionWorld.base, void (%class.btDynamicsWorld*, float)*, void (%class.btDynamicsWorld*, float)*, i8*, %struct.btContactSolverInfo }
%class.btCollisionWorld.base = type <{ i32 (...)**, %class.btAlignedObjectArray, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8 }>
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray.0, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type { i32 (...)** }
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float, float }
%struct.InplaceSolverIslandCallback = type opaque
%class.btConstraintSolver = type { i32 (...)** }
%class.btSimulationIslandManager = type <{ i32 (...)**, %class.btUnionFind, %class.btAlignedObjectArray.13, %class.btAlignedObjectArray, i8, [3 x i8] }>
%class.btUnionFind = type { %class.btAlignedObjectArray.9 }
%class.btAlignedObjectArray.9 = type <{ %class.btAlignedAllocator.10, [3 x i8], i32, i32, %struct.btElement*, i8, [3 x i8] }>
%class.btAlignedAllocator.10 = type { i8 }
%struct.btElement = type { i32, i32 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.7, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.7 = type { i32 }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.4, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btMotionState = type { i32 (...)** }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.19 = type <{ %class.btAlignedAllocator.20, [3 x i8], i32, i32, %class.btRigidBody**, i8, [3 x i8] }>
%class.btAlignedAllocator.20 = type { i8 }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray.23 = type <{ %class.btAlignedAllocator.24, [3 x i8], i32, i32, %class.btActionInterface**, i8, [3 x i8] }>
%class.btAlignedAllocator.24 = type { i8 }
%class.btActionInterface = type opaque
%class.btAlignedObjectArray.13 = type <{ %class.btAlignedAllocator.14, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.14 = type { i8 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.16, %union.anon.17, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.16 = type { float }
%union.anon.17 = type { float }
%class.btSpinMutex = type { i32 }
%class.btAlignedObjectArray.27 = type <{ %class.btAlignedAllocator.28, [3 x i8], i32, i32, %class.btMultiBody**, i8, [3 x i8] }>
%class.btAlignedAllocator.28 = type { i8 }
%class.btMultiBody = type <{ i32 (...)**, %class.btMultiBodyLinkCollider*, i8*, %class.btVector3, %class.btQuaternion, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.34, %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.46, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, i8, i8, i8, i8, float, i8*, i32, i32, i32, float, float, i8, [3 x i8], float, float, i8, i8, [2 x i8], i32, i32, i8, i8, i8, i8 }>
%class.btMultiBodyLinkCollider = type { %class.btCollisionObject, %class.btMultiBody*, i32 }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btAlignedObjectArray.30 = type <{ %class.btAlignedAllocator.31, [3 x i8], i32, i32, %struct.btMultibodyLink*, i8, [3 x i8] }>
%class.btAlignedAllocator.31 = type { i8 }
%struct.btMultibodyLink = type { float, %class.btVector3, i32, %class.btQuaternion, %class.btVector3, %class.btVector3, %struct.btSpatialMotionVector, %struct.btSpatialMotionVector, [6 x %struct.btSpatialMotionVector], i32, i32, %class.btQuaternion, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, [7 x float], [6 x float], %class.btMultiBodyLinkCollider*, i32, i32, i32, i32, %struct.btMultiBodyJointFeedback*, %class.btTransform, i8*, i8*, i8*, float, float }
%struct.btSpatialMotionVector = type { %class.btVector3, %class.btVector3 }
%struct.btMultiBodyJointFeedback = type opaque
%class.btAlignedObjectArray.34 = type <{ %class.btAlignedAllocator.35, [3 x i8], i32, i32, %class.btMultiBodyLinkCollider**, i8, [3 x i8] }>
%class.btAlignedAllocator.35 = type { i8 }
%class.btAlignedObjectArray.51 = type <{ %class.btAlignedAllocator.52, [3 x i8], i32, i32, %class.btMultiBodyConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.52 = type { i8 }
%class.btMultiBodyConstraint = type { i32 (...)**, %class.btMultiBody*, %class.btMultiBody*, i32, i32, i32, i32, i32, i32, i8, i32, float, %class.btAlignedObjectArray.38 }
%struct.MultiBodyInplaceSolverIslandCallback = type { %"struct.btSimulationIslandManager::IslandCallback", %struct.btContactSolverInfo*, %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraint**, i32, %class.btTypedConstraint**, i32, %class.btIDebugDraw*, %class.btDispatcher*, %class.btAlignedObjectArray, %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.51 }
%"struct.btSimulationIslandManager::IslandCallback" = type { i32 (...)** }
%class.btAlignedObjectArray.77 = type <{ %class.btAlignedAllocator.78, [3 x i8], i32, i32, %class.btQuaternion*, i8, [3 x i8] }>
%class.btAlignedAllocator.78 = type { i8 }
%class.btAlignedObjectArray.38 = type <{ %class.btAlignedAllocator.39, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.39 = type { i8 }
%class.btAlignedObjectArray.42 = type <{ %class.btAlignedAllocator.43, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.43 = type { i8 }
%class.btAlignedObjectArray.46 = type <{ %class.btAlignedAllocator.47, [3 x i8], i32, i32, %class.btMatrix3x3*, i8, [3 x i8] }>
%class.btAlignedAllocator.47 = type { i8 }
%class.btDispatcher = type { i32 (...)** }
%class.btBroadphaseInterface = type { i32 (...)** }
%class.btMultiBodyConstraintSolver = type { %class.btSequentialImpulseConstraintSolver, %class.btAlignedObjectArray.72, %class.btAlignedObjectArray.72, %class.btAlignedObjectArray.72, %struct.btMultiBodyJacobianData, %class.btMultiBodyConstraint**, i32 }
%class.btSequentialImpulseConstraintSolver = type { %class.btConstraintSolver, %class.btAlignedObjectArray.55, %class.btAlignedObjectArray.59, %class.btAlignedObjectArray.59, %class.btAlignedObjectArray.59, %class.btAlignedObjectArray.59, %class.btAlignedObjectArray.64, %class.btAlignedObjectArray.64, %class.btAlignedObjectArray.64, %class.btAlignedObjectArray.68, i32, i32, %class.btAlignedObjectArray.64, float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)*, float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)*, float, i32 }
%class.btAlignedObjectArray.55 = type <{ %class.btAlignedAllocator.56, [3 x i8], i32, i32, %struct.btSolverBody*, i8, [3 x i8] }>
%class.btAlignedAllocator.56 = type { i8 }
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btAlignedObjectArray.59 = type <{ %class.btAlignedAllocator.60, [3 x i8], i32, i32, %struct.btSolverConstraint*, i8, [3 x i8] }>
%class.btAlignedAllocator.60 = type { i8 }
%struct.btSolverConstraint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.62, i32, i32, i32, i32 }
%union.anon.62 = type { i8* }
%class.btAlignedObjectArray.68 = type <{ %class.btAlignedAllocator.69, [3 x i8], i32, i32, %"struct.btTypedConstraint::btConstraintInfo1"*, i8, [3 x i8] }>
%class.btAlignedAllocator.69 = type { i8 }
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%class.btAlignedObjectArray.64 = type <{ %class.btAlignedAllocator.65, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.65 = type { i8 }
%class.btAlignedObjectArray.72 = type <{ %class.btAlignedAllocator.73, [3 x i8], i32, i32, %struct.btMultiBodySolverConstraint*, i8, [3 x i8] }>
%class.btAlignedAllocator.73 = type { i8 }
%struct.btMultiBodySolverConstraint = type { i32, i32, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.75, i32, i32, i32, %class.btMultiBody*, i32, i32, %class.btMultiBody*, i32, %class.btMultiBodyConstraint*, i32 }
%union.anon.75 = type { i8* }
%struct.btMultiBodyJacobianData = type { %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.55*, i32 }
%class.btCollisionConfiguration = type opaque
%class.CProfileSample = type { i8 }
%class.btCollisionWorld = type <{ i32 (...)**, %class.btAlignedObjectArray, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8, [3 x i8] }>
%class.btSortConstraintOnIslandPredicate2 = type { i8 }
%class.btSortMultiBodyConstraintOnIslandPredicate = type { i8 }
%struct.anon = type { %class.btMultiBody*, float*, float* }
%struct.anon.81 = type { i8 }
%struct.anon.82 = type { i8 }
%struct.anon.83 = type { i8 }
%class.btVector4 = type { %class.btVector3 }
%class.btSerializer = type { i32 (...)** }
%class.btChunk = type { i32, i32, i8*, i32, i32 }
%"struct.btCollisionWorld::RayResultCallback" = type { i32 (...)**, float, %class.btCollisionObject*, i32, i32, i32 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE9push_backERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE6removeERKS1_ = comdat any

$_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv = comdat any

$_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv = comdat any

$_ZN16btCollisionWorld13getDispatcherEv = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK20btPersistentManifold8getBody1Ev = comdat any

$_ZNK17btCollisionObject25isStaticOrKinematicObjectEv = comdat any

$_ZN25btSimulationIslandManager12getUnionFindEv = comdat any

$_ZN11btUnionFind5uniteEii = comdat any

$_ZNK17btCollisionObject12getIslandTagEv = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi = comdat any

$_ZNK17btTypedConstraint9isEnabledEv = comdat any

$_ZN17btTypedConstraint13getRigidBodyAEv = comdat any

$_ZN17btTypedConstraint13getRigidBodyBEv = comdat any

$_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi = comdat any

$_ZN11btMultiBody15getBaseColliderEv = comdat any

$_ZNK11btMultiBody11getNumLinksEv = comdat any

$_ZN11btMultiBody7getLinkEi = comdat any

$_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEixEi = comdat any

$_ZNK11btMultiBody7isAwakeEv = comdat any

$_ZNK17btCollisionObject18getActivationStateEv = comdat any

$_ZN17btCollisionObject19setDeactivationTimeEf = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EC2Ev = comdat any

$_ZN20btAlignedObjectArrayIfEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3EC2Ev = comdat any

$_ZN15btDynamicsWorld13getSolverInfoEv = comdat any

$_ZN36MultiBodyInplaceSolverIslandCallbackC2EP27btMultiBodyConstraintSolverP12btDispatcher = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3ED2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZN20btAlignedObjectArrayIfED2Ev = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyED2Ev = comdat any

$_ZN23btDiscreteDynamicsWorlddlEPv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE9quickSortI34btSortConstraintOnIslandPredicate2EEvRKT_ = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9quickSortI42btSortMultiBodyConstraintOnIslandPredicateEEvRKT_ = comdat any

$_ZN36MultiBodyInplaceSolverIslandCallback5setupEP19btContactSolverInfoPP17btTypedConstraintiPP21btMultiBodyConstraintiP12btIDebugDraw = comdat any

$_ZNK16btCollisionWorld22getNumCollisionObjectsEv = comdat any

$_ZN20btAlignedObjectArrayIfE6resizeEiRKf = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E6resizeEiRKS0_ = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZNK11btMultiBody21isUsingRK4IntegrationEv = comdat any

$_ZNK11btMultiBody10getNumDofsEv = comdat any

$_ZNK11btMultiBody13getNumPosVarsEv = comdat any

$_ZN20btAlignedObjectArrayIfEixEi = comdat any

$_ZNK11btMultiBody17getWorldToBaseRotEv = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZNK11btMultiBody10getBasePosEv = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK11btMultiBody17getVelocityVectorEv = comdat any

$_ZN11btMultiBody21applyDeltaVeeMultiDofEPKff = comdat any

$_ZN11btMultiBody13setPosUpdatedEb = comdat any

$_ZN15btMultibodyLink19updateCacheMultiDofEPf = comdat any

$_ZN36MultiBodyInplaceSolverIslandCallback18processConstraintsEv = comdat any

$_ZN11btMultiBody24processDeltaVeeMultiDof2Ev = comdat any

$_ZNK11btMultiBody12isPosUpdatedEv = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE6resizeEiRKS0_ = comdat any

$_ZN12btQuaternionC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9push_backERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE6removeERKS1_ = comdat any

$_ZNK11btMultiBody21getBaseWorldTransformEv = comdat any

$_Z10quatRotateRK12btQuaternionRK9btVector3 = comdat any

$_ZNK11btTransform11getRotationEv = comdat any

$_ZN9btVector4C2ERKfS1_S1_S1_ = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZN11btMultiBody12addBaseForceERK9btVector3 = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK11btMultiBody11getBaseMassEv = comdat any

$_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw = comdat any

$_ZN16btCollisionWorld14getDebugDrawerEv = comdat any

$_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv = comdat any

$_ZN23btDiscreteDynamicsWorld11setNumTasksEi = comdat any

$_ZN23btDiscreteDynamicsWorld14updateVehiclesEf = comdat any

$_ZNK24btMultiBodyDynamicsWorld17getNumMultibodiesEv = comdat any

$_ZNK24btMultiBodyDynamicsWorld26getNumMultiBodyConstraintsEv = comdat any

$_ZN24btMultiBodyDynamicsWorld22getMultiBodyConstraintEi = comdat any

$_ZNK24btMultiBodyDynamicsWorld22getMultiBodyConstraintEi = comdat any

$_ZN11btUnionFind4findEi = comdat any

$_ZN20btAlignedObjectArrayI9btElementEixEi = comdat any

$_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi = comdat any

$_ZN25btSimulationIslandManager14IslandCallbackC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev = comdat any

$_ZN36MultiBodyInplaceSolverIslandCallbackD2Ev = comdat any

$_ZN36MultiBodyInplaceSolverIslandCallbackD0Ev = comdat any

$_ZN36MultiBodyInplaceSolverIslandCallback13processIslandEPP17btCollisionObjectiPP20btPersistentManifoldii = comdat any

$_ZN25btSimulationIslandManager14IslandCallbackD2Ev = comdat any

$_ZN25btSimulationIslandManager14IslandCallbackD0Ev = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_ = comdat any

$_Z24btGetConstraintIslandId2PK17btTypedConstraint = comdat any

$_Z32btGetMultiBodyConstraintIslandIdPK21btMultiBodyConstraint = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_ = comdat any

$_ZNK17btTypedConstraint13getRigidBodyAEv = comdat any

$_ZNK17btTypedConstraint13getRigidBodyBEv = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE8allocateEiPPKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIfEixEi = comdat any

$_Z7btClampIfEvRT_RKS0_S3_ = comdat any

$_ZmlRK12btQuaternionS1_ = comdat any

$_ZNK15btMultibodyLink10getAxisTopEi = comdat any

$_ZN12btQuaternionC2ERK9btVector3RKf = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZNK15btMultibodyLink13getAxisBottomEi = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_ZN12btQuaternion11setRotationERK9btVector3RKf = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_Z5btSinf = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_Z5btCosf = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZN11btTransform11setRotationERK12btQuaternion = comdat any

$_ZNK12btQuaternion7inverseEv = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZmlRK12btQuaternionRK9btVector3 = comdat any

$_ZN12btQuaternionmLERKS_ = comdat any

$_ZNK10btQuadWord4getXEv = comdat any

$_ZNK10btQuadWord4getYEv = comdat any

$_ZNK10btQuadWord4getZEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK11btMatrix3x311getRotationER12btQuaternion = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintEixEi = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIfE4initEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIfE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIfE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIfE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIfE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf = comdat any

$_ZNK20btAlignedObjectArrayIP11btMultiBodyE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP11btMultiBodyE4copyEiiPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP11btMultiBodyLj16EE8allocateEiPPKS1_ = comdat any

$_ZN18btAlignedAllocatorIP11btMultiBodyLj16EE10deallocateEPS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP11btMultiBodyE16findLinearSearchERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE13removeAtIndexEi = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE8pop_backEv = comdat any

$_ZN18btAlignedAllocatorIP21btMultiBodyConstraintLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP21btMultiBodyConstraintLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP21btMultiBodyConstraintLj16EE8allocateEiPPKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9allocSizeEi = comdat any

$_ZN18btAlignedAllocatorIP11btMultiBodyLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE4initEv = comdat any

$_ZN18btAlignedAllocatorI12btQuaternionLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE4initEv = comdat any

$_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E4initEv = comdat any

$_ZN20btAlignedObjectArrayIP11btMultiBodyE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI12btQuaternionLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI34btSortConstraintOnIslandPredicate2EEvRKT_ii = comdat any

$_ZNK34btSortConstraintOnIslandPredicate2clEPK17btTypedConstraintS2_ = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE17quickSortInternalI42btSortMultiBodyConstraintOnIslandPredicateEEvRKT_ii = comdat any

$_ZNK42btSortMultiBodyConstraintOnIslandPredicateclEPK21btMultiBodyConstraintS2_ = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIfE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIfE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIfE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIfE4copyEiiPf = comdat any

$_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E7reserveEi = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZNK20btAlignedObjectArrayI11btMatrix3x3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI11btMatrix3x3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI11btMatrix3x3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayI12btQuaternionE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI12btQuaternionE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI12btQuaternionE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI12btQuaternionLj16EE8allocateEiPPKS0_ = comdat any

$_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE16findLinearSearchERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE13removeAtIndexEi = comdat any

$_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE8pop_backEv = comdat any

$_ZTV36MultiBodyInplaceSolverIslandCallback = comdat any

$_ZTS36MultiBodyInplaceSolverIslandCallback = comdat any

$_ZTSN25btSimulationIslandManager14IslandCallbackE = comdat any

$_ZTIN25btSimulationIslandManager14IslandCallbackE = comdat any

$_ZTI36MultiBodyInplaceSolverIslandCallback = comdat any

$_ZTVN25btSimulationIslandManager14IslandCallbackE = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@.str = private unnamed_addr constant [27 x i8] c"calculateSimulationIslands\00", align 1
@.str.1 = private unnamed_addr constant [48 x i8] c"btMultiBodyDynamicsWorld::updateActivationState\00", align 1
@_ZTV24btMultiBodyDynamicsWorld = hidden unnamed_addr constant { [61 x i8*] } { [61 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI24btMultiBodyDynamicsWorld to i8*), i8* bitcast (%class.btMultiBodyDynamicsWorld* (%class.btMultiBodyDynamicsWorld*)* @_ZN24btMultiBodyDynamicsWorldD1Ev to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*)* @_ZN24btMultiBodyDynamicsWorldD0Ev to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld11updateAabbsEv to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld23computeOverlappingPairsEv to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btIDebugDraw*)* @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw to i8*), i8* bitcast (%class.btIDebugDraw* (%class.btCollisionWorld*)* @_ZN16btCollisionWorld14getDebugDrawerEv to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*)* @_ZN24btMultiBodyDynamicsWorld14debugDrawWorldEv to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btTransform*, %class.btCollisionShape*, %class.btVector3*)* @_ZN16btCollisionWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3 to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)* @_ZNK16btCollisionWorld7rayTestERK9btVector3S2_RNS_17RayResultCallbackE to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32)* @_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectii to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*)* @_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*, %class.btSerializer*)* @_ZN24btMultiBodyDynamicsWorld9serializeEP12btSerializer to i8*), i8* bitcast (i32 (%class.btDiscreteDynamicsWorld*, float, i32, float)* @_ZN23btDiscreteDynamicsWorld14stepSimulationEfif to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*, i1)* @_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*)* @_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btVector3*)* @_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btDiscreteDynamicsWorld*)* @_ZNK23btDiscreteDynamicsWorld10getGravityEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*)* @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*, i32, i32)* @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyii to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*)* @_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btConstraintSolver*)* @_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver to i8*), i8* bitcast (%class.btConstraintSolver* (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv to i8*), i8* bitcast (i32 (%class.btDiscreteDynamicsWorld*)* @_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btDiscreteDynamicsWorld*, i32)* @_ZN23btDiscreteDynamicsWorld13getConstraintEi to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btDiscreteDynamicsWorld*, i32)* @_ZNK23btDiscreteDynamicsWorld13getConstraintEi to i8*), i8* bitcast (i32 (%class.btDiscreteDynamicsWorld*)* @_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*)* @_ZN24btMultiBodyDynamicsWorld11clearForcesEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*, float)* @_ZN24btMultiBodyDynamicsWorld19integrateTransformsEf to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*)* @_ZN24btMultiBodyDynamicsWorld26calculateSimulationIslandsEv to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*, %struct.btContactSolverInfo*)* @_ZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfo to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*, float)* @_ZN24btMultiBodyDynamicsWorld21updateActivationStateEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld24createPredictiveContactsEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*)* @_ZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraint to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*)* @_ZN24btMultiBodyDynamicsWorld12applyGravityEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, i32)* @_ZN23btDiscreteDynamicsWorld11setNumTasksEi to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld14updateVehiclesEf to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*, %class.btSerializer*)* @_ZN24btMultiBodyDynamicsWorld20serializeMultiBodiesEP12btSerializer to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*, %class.btMultiBody*, i32, i32)* @_ZN24btMultiBodyDynamicsWorld12addMultiBodyEP11btMultiBodyii to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*, %class.btMultiBody*)* @_ZN24btMultiBodyDynamicsWorld15removeMultiBodyEP11btMultiBody to i8*), i8* bitcast (i32 (%class.btMultiBodyDynamicsWorld*)* @_ZNK24btMultiBodyDynamicsWorld17getNumMultibodiesEv to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*, %class.btMultiBodyConstraint*)* @_ZN24btMultiBodyDynamicsWorld22addMultiBodyConstraintEP21btMultiBodyConstraint to i8*), i8* bitcast (i32 (%class.btMultiBodyDynamicsWorld*)* @_ZNK24btMultiBodyDynamicsWorld26getNumMultiBodyConstraintsEv to i8*), i8* bitcast (%class.btMultiBodyConstraint* (%class.btMultiBodyDynamicsWorld*, i32)* @_ZN24btMultiBodyDynamicsWorld22getMultiBodyConstraintEi to i8*), i8* bitcast (%class.btMultiBodyConstraint* (%class.btMultiBodyDynamicsWorld*, i32)* @_ZNK24btMultiBodyDynamicsWorld22getMultiBodyConstraintEi to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*, %class.btMultiBodyConstraint*)* @_ZN24btMultiBodyDynamicsWorld25removeMultiBodyConstraintEP21btMultiBodyConstraint to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*, %class.btMultiBodyConstraint*)* @_ZN24btMultiBodyDynamicsWorld28debugDrawMultiBodyConstraintEP21btMultiBodyConstraint to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*)* @_ZN24btMultiBodyDynamicsWorld30clearMultiBodyConstraintForcesEv to i8*), i8* bitcast (void (%class.btMultiBodyDynamicsWorld*)* @_ZN24btMultiBodyDynamicsWorld20clearMultiBodyForcesEv to i8*)] }, align 4
@.str.2 = private unnamed_addr constant [17 x i8] c"solveConstraints\00", align 1
@.str.3 = private unnamed_addr constant [27 x i8] c"btMultiBody stepVelocities\00", align 1
@.str.4 = private unnamed_addr constant [26 x i8] c"btMultiBody stepPositions\00", align 1
@.str.5 = private unnamed_addr constant [40 x i8] c"btMultiBodyDynamicsWorld debugDrawWorld\00", align 1
@.str.6 = private unnamed_addr constant [27 x i8] c"btMultiBody debugDrawWorld\00", align 1
@.str.7 = private unnamed_addr constant [23 x i8] c"btMultiBody addGravity\00", align 1
@.str.8 = private unnamed_addr constant [21 x i8] c"clearMultiBodyForces\00", align 1
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS24btMultiBodyDynamicsWorld = hidden constant [27 x i8] c"24btMultiBodyDynamicsWorld\00", align 1
@_ZTI23btDiscreteDynamicsWorld = external constant i8*
@_ZTI24btMultiBodyDynamicsWorld = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([27 x i8], [27 x i8]* @_ZTS24btMultiBodyDynamicsWorld, i32 0, i32 0), i8* bitcast (i8** @_ZTI23btDiscreteDynamicsWorld to i8*) }, align 4
@_ZTV36MultiBodyInplaceSolverIslandCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI36MultiBodyInplaceSolverIslandCallback to i8*), i8* bitcast (%struct.MultiBodyInplaceSolverIslandCallback* (%struct.MultiBodyInplaceSolverIslandCallback*)* @_ZN36MultiBodyInplaceSolverIslandCallbackD2Ev to i8*), i8* bitcast (void (%struct.MultiBodyInplaceSolverIslandCallback*)* @_ZN36MultiBodyInplaceSolverIslandCallbackD0Ev to i8*), i8* bitcast (void (%struct.MultiBodyInplaceSolverIslandCallback*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)* @_ZN36MultiBodyInplaceSolverIslandCallback13processIslandEPP17btCollisionObjectiPP20btPersistentManifoldii to i8*)] }, comdat, align 4
@_ZTS36MultiBodyInplaceSolverIslandCallback = linkonce_odr hidden constant [39 x i8] c"36MultiBodyInplaceSolverIslandCallback\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTSN25btSimulationIslandManager14IslandCallbackE = linkonce_odr hidden constant [46 x i8] c"N25btSimulationIslandManager14IslandCallbackE\00", comdat, align 1
@_ZTIN25btSimulationIslandManager14IslandCallbackE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([46 x i8], [46 x i8]* @_ZTSN25btSimulationIslandManager14IslandCallbackE, i32 0, i32 0) }, comdat, align 4
@_ZTI36MultiBodyInplaceSolverIslandCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([39 x i8], [39 x i8]* @_ZTS36MultiBodyInplaceSolverIslandCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN25btSimulationIslandManager14IslandCallbackE to i8*) }, comdat, align 4
@_ZTVN25btSimulationIslandManager14IslandCallbackE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN25btSimulationIslandManager14IslandCallbackE to i8*), i8* bitcast (%"struct.btSimulationIslandManager::IslandCallback"* (%"struct.btSimulationIslandManager::IslandCallback"*)* @_ZN25btSimulationIslandManager14IslandCallbackD2Ev to i8*), i8* bitcast (void (%"struct.btSimulationIslandManager::IslandCallback"*)* @_ZN25btSimulationIslandManager14IslandCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btMultiBodyDynamicsWorld.cpp, i8* null }]

@_ZN24btMultiBodyDynamicsWorldC1EP12btDispatcherP21btBroadphaseInterfaceP27btMultiBodyConstraintSolverP24btCollisionConfiguration = hidden unnamed_addr alias %class.btMultiBodyDynamicsWorld* (%class.btMultiBodyDynamicsWorld*, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btMultiBodyConstraintSolver*, %class.btCollisionConfiguration*), %class.btMultiBodyDynamicsWorld* (%class.btMultiBodyDynamicsWorld*, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btMultiBodyConstraintSolver*, %class.btCollisionConfiguration*)* @_ZN24btMultiBodyDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP27btMultiBodyConstraintSolverP24btCollisionConfiguration
@_ZN24btMultiBodyDynamicsWorldD1Ev = hidden unnamed_addr alias %class.btMultiBodyDynamicsWorld* (%class.btMultiBodyDynamicsWorld*), %class.btMultiBodyDynamicsWorld* (%class.btMultiBodyDynamicsWorld*)* @_ZN24btMultiBodyDynamicsWorldD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN24btMultiBodyDynamicsWorld12addMultiBodyEP11btMultiBodyii(%class.btMultiBodyDynamicsWorld* %this, %class.btMultiBody* %body, i32 %group, i32 %mask) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %body.addr = alloca %class.btMultiBody*, align 4
  %group.addr = alloca i32, align 4
  %mask.addr = alloca i32, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  store %class.btMultiBody* %body, %class.btMultiBody** %body.addr, align 4
  store i32 %group, i32* %group.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE9push_backERKS1_(%class.btAlignedObjectArray.27* %m_multiBodies, %class.btMultiBody** nonnull align 4 dereferenceable(4) %body.addr)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btMultiBodyE9push_backERKS1_(%class.btAlignedObjectArray.27* %this, %class.btMultiBody** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.27*, align 4
  %_Val.addr = alloca %class.btMultiBody**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.27* %this, %class.btAlignedObjectArray.27** %this.addr, align 4
  store %class.btMultiBody** %_Val, %class.btMultiBody*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.27*, %class.btAlignedObjectArray.27** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE8capacityEv(%class.btAlignedObjectArray.27* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP11btMultiBodyE9allocSizeEi(%class.btAlignedObjectArray.27* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE7reserveEi(%class.btAlignedObjectArray.27* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 4
  %1 = load %class.btMultiBody**, %class.btMultiBody*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %1, i32 %2
  %3 = bitcast %class.btMultiBody** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btMultiBody**
  %5 = load %class.btMultiBody**, %class.btMultiBody*** %_Val.addr, align 4
  %6 = load %class.btMultiBody*, %class.btMultiBody** %5, align 4
  store %class.btMultiBody* %6, %class.btMultiBody** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN24btMultiBodyDynamicsWorld15removeMultiBodyEP11btMultiBody(%class.btMultiBodyDynamicsWorld* %this, %class.btMultiBody* %body) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %body.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  store %class.btMultiBody* %body, %class.btMultiBody** %body.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE6removeERKS1_(%class.btAlignedObjectArray.27* %m_multiBodies, %class.btMultiBody** nonnull align 4 dereferenceable(4) %body.addr)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btMultiBodyE6removeERKS1_(%class.btAlignedObjectArray.27* %this, %class.btMultiBody** nonnull align 4 dereferenceable(4) %key) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.27*, align 4
  %key.addr = alloca %class.btMultiBody**, align 4
  %findIndex = alloca i32, align 4
  store %class.btAlignedObjectArray.27* %this, %class.btAlignedObjectArray.27** %this.addr, align 4
  store %class.btMultiBody** %key, %class.btMultiBody*** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray.27*, %class.btAlignedObjectArray.27** %this.addr, align 4
  %0 = load %class.btMultiBody**, %class.btMultiBody*** %key.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE16findLinearSearchERKS1_(%class.btAlignedObjectArray.27* %this1, %class.btMultiBody** nonnull align 4 dereferenceable(4) %0)
  store i32 %call, i32* %findIndex, align 4
  %1 = load i32, i32* %findIndex, align 4
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE13removeAtIndexEi(%class.btAlignedObjectArray.27* %this1, i32 %1)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN24btMultiBodyDynamicsWorld26calculateSimulationIslandsEv(%class.btMultiBodyDynamicsWorld* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %i = alloca i32, align 4
  %manifold = alloca %class.btPersistentManifold*, align 4
  %colObj0 = alloca %class.btCollisionObject*, align 4
  %colObj1 = alloca %class.btCollisionObject*, align 4
  %i20 = alloca i32, align 4
  %numConstraints = alloca i32, align 4
  %constraint = alloca %class.btTypedConstraint*, align 4
  %colObj029 = alloca %class.btRigidBody*, align 4
  %colObj131 = alloca %class.btRigidBody*, align 4
  %i50 = alloca i32, align 4
  %body = alloca %class.btMultiBody*, align 4
  %prev = alloca %class.btMultiBodyLinkCollider*, align 4
  %b = alloca i32, align 4
  %cur = alloca %class.btMultiBodyLinkCollider*, align 4
  %tagPrev = alloca i32, align 4
  %tagCur = alloca i32, align 4
  %i87 = alloca i32, align 4
  %c = alloca %class.btMultiBodyConstraint*, align 4
  %tagA = alloca i32, align 4
  %tagB = alloca i32, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0))
  %0 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call2 = call %class.btSimulationIslandManager* @_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv(%class.btDiscreteDynamicsWorld* %0)
  %1 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call3 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %1)
  %2 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call4 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %2)
  %call5 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %call4)
  %3 = bitcast %class.btSimulationIslandManager* %call2 to void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)***
  %vtable = load void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)**, void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)*** %3, align 4
  %vfn = getelementptr inbounds void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)*, void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)** %vtable, i64 2
  %4 = load void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)*, void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)** %vfn, align 4
  call void %4(%class.btSimulationIslandManager* %call2, %class.btCollisionWorld* %call3, %class.btDispatcher* %call5)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4
  %6 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_predictiveManifolds = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %6, i32 0, i32 18
  %call6 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %m_predictiveManifolds)
  %cmp = icmp slt i32 %5, %call6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_predictiveManifolds7 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %7, i32 0, i32 18
  %8 = load i32, i32* %i, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.13* %m_predictiveManifolds7, i32 %8)
  %9 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call8, align 4
  store %class.btPersistentManifold* %9, %class.btPersistentManifold** %manifold, align 4
  %10 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %call9 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %10)
  store %class.btCollisionObject* %call9, %class.btCollisionObject** %colObj0, align 4
  %11 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %call10 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %11)
  store %class.btCollisionObject* %call10, %class.btCollisionObject** %colObj1, align 4
  %12 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %tobool = icmp ne %class.btCollisionObject* %12, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %13 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call11 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %13)
  br i1 %call11, label %if.end, label %land.lhs.true12

land.lhs.true12:                                  ; preds = %land.lhs.true
  %14 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %tobool13 = icmp ne %class.btCollisionObject* %14, null
  br i1 %tobool13, label %land.lhs.true14, label %if.end

land.lhs.true14:                                  ; preds = %land.lhs.true12
  %15 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call15 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %15)
  br i1 %call15, label %if.end, label %if.then

if.then:                                          ; preds = %land.lhs.true14
  %16 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call16 = call %class.btSimulationIslandManager* @_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv(%class.btDiscreteDynamicsWorld* %16)
  %call17 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %call16)
  %17 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call18 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %17)
  %18 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call19 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %18)
  call void @_ZN11btUnionFind5uniteEii(%class.btUnionFind* %call17, i32 %call18, i32 %call19)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true14, %land.lhs.true12, %land.lhs.true, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %19 = load i32, i32* %i, align 4
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %20 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_constraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %20, i32 0, i32 5
  %call21 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %m_constraints)
  store i32 %call21, i32* %numConstraints, align 4
  store i32 0, i32* %i20, align 4
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc47, %for.end
  %21 = load i32, i32* %i20, align 4
  %22 = load i32, i32* %numConstraints, align 4
  %cmp23 = icmp slt i32 %21, %22
  br i1 %cmp23, label %for.body24, label %for.end49

for.body24:                                       ; preds = %for.cond22
  %23 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_constraints25 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %23, i32 0, i32 5
  %24 = load i32, i32* %i20, align 4
  %call26 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.4* %m_constraints25, i32 %24)
  %25 = load %class.btTypedConstraint*, %class.btTypedConstraint** %call26, align 4
  store %class.btTypedConstraint* %25, %class.btTypedConstraint** %constraint, align 4
  %26 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint, align 4
  %call27 = call zeroext i1 @_ZNK17btTypedConstraint9isEnabledEv(%class.btTypedConstraint* %26)
  br i1 %call27, label %if.then28, label %if.end46

if.then28:                                        ; preds = %for.body24
  %27 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint, align 4
  %call30 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %27)
  store %class.btRigidBody* %call30, %class.btRigidBody** %colObj029, align 4
  %28 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint, align 4
  %call32 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %28)
  store %class.btRigidBody* %call32, %class.btRigidBody** %colObj131, align 4
  %29 = load %class.btRigidBody*, %class.btRigidBody** %colObj029, align 4
  %tobool33 = icmp ne %class.btRigidBody* %29, null
  br i1 %tobool33, label %land.lhs.true34, label %if.end45

land.lhs.true34:                                  ; preds = %if.then28
  %30 = load %class.btRigidBody*, %class.btRigidBody** %colObj029, align 4
  %31 = bitcast %class.btRigidBody* %30 to %class.btCollisionObject*
  %call35 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %31)
  br i1 %call35, label %if.end45, label %land.lhs.true36

land.lhs.true36:                                  ; preds = %land.lhs.true34
  %32 = load %class.btRigidBody*, %class.btRigidBody** %colObj131, align 4
  %tobool37 = icmp ne %class.btRigidBody* %32, null
  br i1 %tobool37, label %land.lhs.true38, label %if.end45

land.lhs.true38:                                  ; preds = %land.lhs.true36
  %33 = load %class.btRigidBody*, %class.btRigidBody** %colObj131, align 4
  %34 = bitcast %class.btRigidBody* %33 to %class.btCollisionObject*
  %call39 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %34)
  br i1 %call39, label %if.end45, label %if.then40

if.then40:                                        ; preds = %land.lhs.true38
  %35 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call41 = call %class.btSimulationIslandManager* @_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv(%class.btDiscreteDynamicsWorld* %35)
  %call42 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %call41)
  %36 = load %class.btRigidBody*, %class.btRigidBody** %colObj029, align 4
  %37 = bitcast %class.btRigidBody* %36 to %class.btCollisionObject*
  %call43 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %37)
  %38 = load %class.btRigidBody*, %class.btRigidBody** %colObj131, align 4
  %39 = bitcast %class.btRigidBody* %38 to %class.btCollisionObject*
  %call44 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %39)
  call void @_ZN11btUnionFind5uniteEii(%class.btUnionFind* %call42, i32 %call43, i32 %call44)
  br label %if.end45

if.end45:                                         ; preds = %if.then40, %land.lhs.true38, %land.lhs.true36, %land.lhs.true34, %if.then28
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %for.body24
  br label %for.inc47

for.inc47:                                        ; preds = %if.end46
  %40 = load i32, i32* %i20, align 4
  %inc48 = add nsw i32 %40, 1
  store i32 %inc48, i32* %i20, align 4
  br label %for.cond22

for.end49:                                        ; preds = %for.cond22
  store i32 0, i32* %i50, align 4
  br label %for.cond51

for.cond51:                                       ; preds = %for.inc84, %for.end49
  %41 = load i32, i32* %i50, align 4
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call52 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %m_multiBodies)
  %cmp53 = icmp slt i32 %41, %call52
  br i1 %cmp53, label %for.body54, label %for.end86

for.body54:                                       ; preds = %for.cond51
  %m_multiBodies55 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %42 = load i32, i32* %i50, align 4
  %call56 = call nonnull align 4 dereferenceable(4) %class.btMultiBody** @_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi(%class.btAlignedObjectArray.27* %m_multiBodies55, i32 %42)
  %43 = load %class.btMultiBody*, %class.btMultiBody** %call56, align 4
  store %class.btMultiBody* %43, %class.btMultiBody** %body, align 4
  %44 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4
  %call57 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %44)
  store %class.btMultiBodyLinkCollider* %call57, %class.btMultiBodyLinkCollider** %prev, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond58

for.cond58:                                       ; preds = %for.inc81, %for.body54
  %45 = load i32, i32* %b, align 4
  %46 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4
  %call59 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %46)
  %cmp60 = icmp slt i32 %45, %call59
  br i1 %cmp60, label %for.body61, label %for.end83

for.body61:                                       ; preds = %for.cond58
  %47 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4
  %48 = load i32, i32* %b, align 4
  %call62 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %47, i32 %48)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call62, i32 0, i32 19
  %49 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4
  store %class.btMultiBodyLinkCollider* %49, %class.btMultiBodyLinkCollider** %cur, align 4
  %50 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %cur, align 4
  %tobool63 = icmp ne %class.btMultiBodyLinkCollider* %50, null
  br i1 %tobool63, label %land.lhs.true64, label %if.end75

land.lhs.true64:                                  ; preds = %for.body61
  %51 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %cur, align 4
  %52 = bitcast %class.btMultiBodyLinkCollider* %51 to %class.btCollisionObject*
  %call65 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %52)
  br i1 %call65, label %if.end75, label %land.lhs.true66

land.lhs.true66:                                  ; preds = %land.lhs.true64
  %53 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %prev, align 4
  %tobool67 = icmp ne %class.btMultiBodyLinkCollider* %53, null
  br i1 %tobool67, label %land.lhs.true68, label %if.end75

land.lhs.true68:                                  ; preds = %land.lhs.true66
  %54 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %prev, align 4
  %55 = bitcast %class.btMultiBodyLinkCollider* %54 to %class.btCollisionObject*
  %call69 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %55)
  br i1 %call69, label %if.end75, label %if.then70

if.then70:                                        ; preds = %land.lhs.true68
  %56 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %prev, align 4
  %57 = bitcast %class.btMultiBodyLinkCollider* %56 to %class.btCollisionObject*
  %call71 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %57)
  store i32 %call71, i32* %tagPrev, align 4
  %58 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %cur, align 4
  %59 = bitcast %class.btMultiBodyLinkCollider* %58 to %class.btCollisionObject*
  %call72 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %59)
  store i32 %call72, i32* %tagCur, align 4
  %60 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call73 = call %class.btSimulationIslandManager* @_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv(%class.btDiscreteDynamicsWorld* %60)
  %call74 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %call73)
  %61 = load i32, i32* %tagPrev, align 4
  %62 = load i32, i32* %tagCur, align 4
  call void @_ZN11btUnionFind5uniteEii(%class.btUnionFind* %call74, i32 %61, i32 %62)
  br label %if.end75

if.end75:                                         ; preds = %if.then70, %land.lhs.true68, %land.lhs.true66, %land.lhs.true64, %for.body61
  %63 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %cur, align 4
  %tobool76 = icmp ne %class.btMultiBodyLinkCollider* %63, null
  br i1 %tobool76, label %land.lhs.true77, label %if.end80

land.lhs.true77:                                  ; preds = %if.end75
  %64 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %cur, align 4
  %65 = bitcast %class.btMultiBodyLinkCollider* %64 to %class.btCollisionObject*
  %call78 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %65)
  br i1 %call78, label %if.end80, label %if.then79

if.then79:                                        ; preds = %land.lhs.true77
  %66 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %cur, align 4
  store %class.btMultiBodyLinkCollider* %66, %class.btMultiBodyLinkCollider** %prev, align 4
  br label %if.end80

if.end80:                                         ; preds = %if.then79, %land.lhs.true77, %if.end75
  br label %for.inc81

for.inc81:                                        ; preds = %if.end80
  %67 = load i32, i32* %b, align 4
  %inc82 = add nsw i32 %67, 1
  store i32 %inc82, i32* %b, align 4
  br label %for.cond58

for.end83:                                        ; preds = %for.cond58
  br label %for.inc84

for.inc84:                                        ; preds = %for.end83
  %68 = load i32, i32* %i50, align 4
  %inc85 = add nsw i32 %68, 1
  store i32 %inc85, i32* %i50, align 4
  br label %for.cond51

for.end86:                                        ; preds = %for.cond51
  store i32 0, i32* %i87, align 4
  br label %for.cond88

for.cond88:                                       ; preds = %for.inc107, %for.end86
  %69 = load i32, i32* %i87, align 4
  %m_multiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  %call89 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %m_multiBodyConstraints)
  %cmp90 = icmp slt i32 %69, %call89
  br i1 %cmp90, label %for.body91, label %for.end109

for.body91:                                       ; preds = %for.cond88
  %m_multiBodyConstraints92 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  %70 = load i32, i32* %i87, align 4
  %call93 = call nonnull align 4 dereferenceable(4) %class.btMultiBodyConstraint** @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEixEi(%class.btAlignedObjectArray.51* %m_multiBodyConstraints92, i32 %70)
  %71 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %call93, align 4
  store %class.btMultiBodyConstraint* %71, %class.btMultiBodyConstraint** %c, align 4
  %72 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %c, align 4
  %73 = bitcast %class.btMultiBodyConstraint* %72 to i32 (%class.btMultiBodyConstraint*)***
  %vtable94 = load i32 (%class.btMultiBodyConstraint*)**, i32 (%class.btMultiBodyConstraint*)*** %73, align 4
  %vfn95 = getelementptr inbounds i32 (%class.btMultiBodyConstraint*)*, i32 (%class.btMultiBodyConstraint*)** %vtable94, i64 5
  %74 = load i32 (%class.btMultiBodyConstraint*)*, i32 (%class.btMultiBodyConstraint*)** %vfn95, align 4
  %call96 = call i32 %74(%class.btMultiBodyConstraint* %72)
  store i32 %call96, i32* %tagA, align 4
  %75 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %c, align 4
  %76 = bitcast %class.btMultiBodyConstraint* %75 to i32 (%class.btMultiBodyConstraint*)***
  %vtable97 = load i32 (%class.btMultiBodyConstraint*)**, i32 (%class.btMultiBodyConstraint*)*** %76, align 4
  %vfn98 = getelementptr inbounds i32 (%class.btMultiBodyConstraint*)*, i32 (%class.btMultiBodyConstraint*)** %vtable97, i64 6
  %77 = load i32 (%class.btMultiBodyConstraint*)*, i32 (%class.btMultiBodyConstraint*)** %vfn98, align 4
  %call99 = call i32 %77(%class.btMultiBodyConstraint* %75)
  store i32 %call99, i32* %tagB, align 4
  %78 = load i32, i32* %tagA, align 4
  %cmp100 = icmp sge i32 %78, 0
  br i1 %cmp100, label %land.lhs.true101, label %if.end106

land.lhs.true101:                                 ; preds = %for.body91
  %79 = load i32, i32* %tagB, align 4
  %cmp102 = icmp sge i32 %79, 0
  br i1 %cmp102, label %if.then103, label %if.end106

if.then103:                                       ; preds = %land.lhs.true101
  %80 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call104 = call %class.btSimulationIslandManager* @_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv(%class.btDiscreteDynamicsWorld* %80)
  %call105 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %call104)
  %81 = load i32, i32* %tagA, align 4
  %82 = load i32, i32* %tagB, align 4
  call void @_ZN11btUnionFind5uniteEii(%class.btUnionFind* %call105, i32 %81, i32 %82)
  br label %if.end106

if.end106:                                        ; preds = %if.then103, %land.lhs.true101, %for.body91
  br label %for.inc107

for.inc107:                                       ; preds = %if.end106
  %83 = load i32, i32* %i87, align 4
  %inc108 = add nsw i32 %83, 1
  store i32 %inc108, i32* %i87, align 4
  br label %for.cond88

for.end109:                                       ; preds = %for.cond88
  %84 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call110 = call %class.btSimulationIslandManager* @_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv(%class.btDiscreteDynamicsWorld* %84)
  %85 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call111 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %85)
  %86 = bitcast %class.btSimulationIslandManager* %call110 to void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)***
  %vtable112 = load void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)**, void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)*** %86, align 4
  %vfn113 = getelementptr inbounds void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)*, void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)** %vtable112, i64 3
  %87 = load void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)*, void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)** %vfn113, align 4
  call void %87(%class.btSimulationIslandManager* %call110, %class.btCollisionWorld* %call111)
  %call114 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

declare %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* returned, i8*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btSimulationIslandManager* @_ZN23btDiscreteDynamicsWorld26getSimulationIslandManagerEv(%class.btDiscreteDynamicsWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %m_islandManager = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %this1, i32 0, i32 4
  %0 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %m_islandManager, align 4
  ret %class.btSimulationIslandManager* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  ret %class.btCollisionWorld* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 2
  %0 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  ret %class.btDispatcher* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.13* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %0, i32 %1
  ret %class.btPersistentManifold** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4
  ret %class.btCollisionObject* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body1 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 3
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body1, align 4
  ret %class.btCollisionObject* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4
  %and = and i32 %0, 3
  %cmp = icmp ne i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %m_unionFind = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 1
  ret %class.btUnionFind* %m_unionFind
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btUnionFind5uniteEii(%class.btUnionFind* %this, i32 %p, i32 %q) #2 comdat {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %p.addr = alloca i32, align 4
  %q.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4
  store i32 %p, i32* %p.addr, align 4
  store i32 %q, i32* %q.addr, align 4
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %0 = load i32, i32* %p.addr, align 4
  %call = call i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %this1, i32 %0)
  store i32 %call, i32* %i, align 4
  %1 = load i32, i32* %q.addr, align 4
  %call2 = call i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %this1, i32 %1)
  store i32 %call2, i32* %j, align 4
  %2 = load i32, i32* %i, align 4
  %3 = load i32, i32* %j, align 4
  %cmp = icmp eq i32 %2, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %4 = load i32, i32* %j, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %5 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.9* %m_elements, i32 %5)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call3, i32 0, i32 0
  store i32 %4, i32* %m_id, align 4
  %m_elements4 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %6 = load i32, i32* %i, align 4
  %call5 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.9* %m_elements4, i32 %6)
  %m_sz = getelementptr inbounds %struct.btElement, %struct.btElement* %call5, i32 0, i32 1
  %7 = load i32, i32* %m_sz, align 4
  %m_elements6 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %8 = load i32, i32* %j, align 4
  %call7 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.9* %m_elements6, i32 %8)
  %m_sz8 = getelementptr inbounds %struct.btElement, %struct.btElement* %call7, i32 0, i32 1
  %9 = load i32, i32* %m_sz8, align 4
  %add = add nsw i32 %9, %7
  store i32 %add, i32* %m_sz8, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_islandTag1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 13
  %0 = load i32, i32* %m_islandTag1, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %0, i32 %1
  ret %class.btTypedConstraint** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btTypedConstraint9isEnabledEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_isEnabled = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 5
  %0 = load i8, i8* %m_isEnabled, align 4
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 8
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  ret %class.btRigidBody* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 9
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  ret %class.btRigidBody* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.27*, align 4
  store %class.btAlignedObjectArray.27* %this, %class.btAlignedObjectArray.27** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.27*, %class.btAlignedObjectArray.27** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btMultiBody** @_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi(%class.btAlignedObjectArray.27* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.27*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.27* %this, %class.btAlignedObjectArray.27** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.27*, %class.btAlignedObjectArray.27** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 4
  %0 = load %class.btMultiBody**, %class.btMultiBody*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %0, i32 %1
  ret %class.btMultiBody** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_baseCollider = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 1
  %0 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_baseCollider, align 4
  ret %class.btMultiBodyLinkCollider* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 11
  %call = call i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray.30* %m_links)
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %index.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 11
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray.30* %m_links, i32 %0)
  ret %struct.btMultibodyLink* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btMultiBodyConstraint** @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEixEi(%class.btAlignedObjectArray.51* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  %0 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %0, i32 %1
  ret %class.btMultiBodyConstraint** %arrayidx
}

; Function Attrs: nounwind
declare %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* returned) unnamed_addr #4

; Function Attrs: noinline optnone
define hidden void @_ZN24btMultiBodyDynamicsWorld21updateActivationStateEf(%class.btMultiBodyDynamicsWorld* %this, float %timeStep) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %i = alloca i32, align 4
  %body = alloca %class.btMultiBody*, align 4
  %col = alloca %class.btMultiBodyLinkCollider*, align 4
  %b = alloca i32, align 4
  %col16 = alloca %class.btMultiBodyLinkCollider*, align 4
  %col24 = alloca %class.btMultiBodyLinkCollider*, align 4
  %b32 = alloca i32, align 4
  %col37 = alloca %class.btMultiBodyLinkCollider*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.1, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc51, %entry
  %0 = load i32, i32* %i, align 4
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %m_multiBodies)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end53

for.body:                                         ; preds = %for.cond
  %m_multiBodies3 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %1 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %class.btMultiBody** @_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi(%class.btAlignedObjectArray.27* %m_multiBodies3, i32 %1)
  %2 = load %class.btMultiBody*, %class.btMultiBody** %call4, align 4
  store %class.btMultiBody* %2, %class.btMultiBody** %body, align 4
  %3 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4
  %tobool = icmp ne %class.btMultiBody* %3, null
  br i1 %tobool, label %if.then, label %if.end50

if.then:                                          ; preds = %for.body
  %4 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4
  %5 = load float, float* %timeStep.addr, align 4
  call void @_ZN11btMultiBody29checkMotionAndSleepIfRequiredEf(%class.btMultiBody* %4, float %5)
  %6 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4
  %call5 = call zeroext i1 @_ZNK11btMultiBody7isAwakeEv(%class.btMultiBody* %6)
  br i1 %call5, label %if.else, label %if.then6

if.then6:                                         ; preds = %if.then
  %7 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4
  %call7 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %7)
  store %class.btMultiBodyLinkCollider* %call7, %class.btMultiBodyLinkCollider** %col, align 4
  %8 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4
  %tobool8 = icmp ne %class.btMultiBodyLinkCollider* %8, null
  br i1 %tobool8, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then6
  %9 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4
  %10 = bitcast %class.btMultiBodyLinkCollider* %9 to %class.btCollisionObject*
  %call9 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %10)
  %cmp10 = icmp eq i32 %call9, 1
  br i1 %cmp10, label %if.then11, label %if.end

if.then11:                                        ; preds = %land.lhs.true
  %11 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4
  %12 = bitcast %class.btMultiBodyLinkCollider* %11 to %class.btCollisionObject*
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %12, i32 3)
  %13 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4
  %14 = bitcast %class.btMultiBodyLinkCollider* %13 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject19setDeactivationTimeEf(%class.btCollisionObject* %14, float 0.000000e+00)
  br label %if.end

if.end:                                           ; preds = %if.then11, %land.lhs.true, %if.then6
  store i32 0, i32* %b, align 4
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc, %if.end
  %15 = load i32, i32* %b, align 4
  %16 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4
  %call13 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %16)
  %cmp14 = icmp slt i32 %15, %call13
  br i1 %cmp14, label %for.body15, label %for.end

for.body15:                                       ; preds = %for.cond12
  %17 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4
  %18 = load i32, i32* %b, align 4
  %call17 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %17, i32 %18)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call17, i32 0, i32 19
  %19 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4
  store %class.btMultiBodyLinkCollider* %19, %class.btMultiBodyLinkCollider** %col16, align 4
  %20 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col16, align 4
  %tobool18 = icmp ne %class.btMultiBodyLinkCollider* %20, null
  br i1 %tobool18, label %land.lhs.true19, label %if.end23

land.lhs.true19:                                  ; preds = %for.body15
  %21 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col16, align 4
  %22 = bitcast %class.btMultiBodyLinkCollider* %21 to %class.btCollisionObject*
  %call20 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %22)
  %cmp21 = icmp eq i32 %call20, 1
  br i1 %cmp21, label %if.then22, label %if.end23

if.then22:                                        ; preds = %land.lhs.true19
  %23 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col16, align 4
  %24 = bitcast %class.btMultiBodyLinkCollider* %23 to %class.btCollisionObject*
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %24, i32 3)
  %25 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col16, align 4
  %26 = bitcast %class.btMultiBodyLinkCollider* %25 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject19setDeactivationTimeEf(%class.btCollisionObject* %26, float 0.000000e+00)
  br label %if.end23

if.end23:                                         ; preds = %if.then22, %land.lhs.true19, %for.body15
  br label %for.inc

for.inc:                                          ; preds = %if.end23
  %27 = load i32, i32* %b, align 4
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %b, align 4
  br label %for.cond12

for.end:                                          ; preds = %for.cond12
  br label %if.end49

if.else:                                          ; preds = %if.then
  %28 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4
  %call25 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %28)
  store %class.btMultiBodyLinkCollider* %call25, %class.btMultiBodyLinkCollider** %col24, align 4
  %29 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col24, align 4
  %tobool26 = icmp ne %class.btMultiBodyLinkCollider* %29, null
  br i1 %tobool26, label %land.lhs.true27, label %if.end31

land.lhs.true27:                                  ; preds = %if.else
  %30 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col24, align 4
  %31 = bitcast %class.btMultiBodyLinkCollider* %30 to %class.btCollisionObject*
  %call28 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %31)
  %cmp29 = icmp ne i32 %call28, 4
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %land.lhs.true27
  %32 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col24, align 4
  %33 = bitcast %class.btMultiBodyLinkCollider* %32 to %class.btCollisionObject*
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %33, i32 1)
  br label %if.end31

if.end31:                                         ; preds = %if.then30, %land.lhs.true27, %if.else
  store i32 0, i32* %b32, align 4
  br label %for.cond33

for.cond33:                                       ; preds = %for.inc46, %if.end31
  %34 = load i32, i32* %b32, align 4
  %35 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4
  %call34 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %35)
  %cmp35 = icmp slt i32 %34, %call34
  br i1 %cmp35, label %for.body36, label %for.end48

for.body36:                                       ; preds = %for.cond33
  %36 = load %class.btMultiBody*, %class.btMultiBody** %body, align 4
  %37 = load i32, i32* %b32, align 4
  %call38 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %36, i32 %37)
  %m_collider39 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call38, i32 0, i32 19
  %38 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider39, align 4
  store %class.btMultiBodyLinkCollider* %38, %class.btMultiBodyLinkCollider** %col37, align 4
  %39 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col37, align 4
  %tobool40 = icmp ne %class.btMultiBodyLinkCollider* %39, null
  br i1 %tobool40, label %land.lhs.true41, label %if.end45

land.lhs.true41:                                  ; preds = %for.body36
  %40 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col37, align 4
  %41 = bitcast %class.btMultiBodyLinkCollider* %40 to %class.btCollisionObject*
  %call42 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %41)
  %cmp43 = icmp ne i32 %call42, 4
  br i1 %cmp43, label %if.then44, label %if.end45

if.then44:                                        ; preds = %land.lhs.true41
  %42 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col37, align 4
  %43 = bitcast %class.btMultiBodyLinkCollider* %42 to %class.btCollisionObject*
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %43, i32 1)
  br label %if.end45

if.end45:                                         ; preds = %if.then44, %land.lhs.true41, %for.body36
  br label %for.inc46

for.inc46:                                        ; preds = %if.end45
  %44 = load i32, i32* %b32, align 4
  %inc47 = add nsw i32 %44, 1
  store i32 %inc47, i32* %b32, align 4
  br label %for.cond33

for.end48:                                        ; preds = %for.cond33
  br label %if.end49

if.end49:                                         ; preds = %for.end48, %for.end
  br label %if.end50

if.end50:                                         ; preds = %if.end49, %for.body
  br label %for.inc51

for.inc51:                                        ; preds = %if.end50
  %45 = load i32, i32* %i, align 4
  %inc52 = add nsw i32 %45, 1
  store i32 %inc52, i32* %i, align 4
  br label %for.cond

for.end53:                                        ; preds = %for.cond
  %46 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %47 = load float, float* %timeStep.addr, align 4
  call void @_ZN23btDiscreteDynamicsWorld21updateActivationStateEf(%class.btDiscreteDynamicsWorld* %46, float %47)
  %call54 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

declare void @_ZN11btMultiBody29checkMotionAndSleepIfRequiredEf(%class.btMultiBody*, float) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK11btMultiBody7isAwakeEv(%class.btMultiBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_awake = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 23
  %0 = load i8, i8* %m_awake, align 2
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_activationState1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 16
  %0 = load i32, i32* %m_activationState1, align 4
  ret i32 %0
}

declare void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject*, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btCollisionObject19setDeactivationTimeEf(%class.btCollisionObject* %this, float %time) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %time.addr = alloca float, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store float %time, float* %time.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load float, float* %time.addr, align 4
  %m_deactivationTime = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 17
  store float %0, float* %m_deactivationTime, align 4
  ret void
}

declare void @_ZN23btDiscreteDynamicsWorld21updateActivationStateEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #3

; Function Attrs: noinline optnone
define hidden %class.btMultiBodyDynamicsWorld* @_ZN24btMultiBodyDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP27btMultiBodyConstraintSolverP24btCollisionConfiguration(%class.btMultiBodyDynamicsWorld* returned %this, %class.btDispatcher* %dispatcher, %class.btBroadphaseInterface* %pairCache, %class.btMultiBodyConstraintSolver* %constraintSolver, %class.btCollisionConfiguration* %collisionConfiguration) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %pairCache.addr = alloca %class.btBroadphaseInterface*, align 4
  %constraintSolver.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %collisionConfiguration.addr = alloca %class.btCollisionConfiguration*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  store %class.btBroadphaseInterface* %pairCache, %class.btBroadphaseInterface** %pairCache.addr, align 4
  store %class.btMultiBodyConstraintSolver* %constraintSolver, %class.btMultiBodyConstraintSolver** %constraintSolver.addr, align 4
  store %class.btCollisionConfiguration* %collisionConfiguration, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %1 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %2 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %pairCache.addr, align 4
  %3 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %constraintSolver.addr, align 4
  %4 = bitcast %class.btMultiBodyConstraintSolver* %3 to %class.btConstraintSolver*
  %5 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4
  %call = call %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration(%class.btDiscreteDynamicsWorld* %0, %class.btDispatcher* %1, %class.btBroadphaseInterface* %2, %class.btConstraintSolver* %4, %class.btCollisionConfiguration* %5)
  %6 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [61 x i8*] }, { [61 x i8*] }* @_ZTV24btMultiBodyDynamicsWorld, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %6, align 4
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.27* @_ZN20btAlignedObjectArrayIP11btMultiBodyEC2Ev(%class.btAlignedObjectArray.27* %m_multiBodies)
  %m_multiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray.51* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEC2Ev(%class.btAlignedObjectArray.51* %m_multiBodyConstraints)
  %m_sortedMultiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 3
  %call4 = call %class.btAlignedObjectArray.51* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEC2Ev(%class.btAlignedObjectArray.51* %m_sortedMultiBodyConstraints)
  %m_multiBodyConstraintSolver = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 4
  %7 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %constraintSolver.addr, align 4
  store %class.btMultiBodyConstraintSolver* %7, %class.btMultiBodyConstraintSolver** %m_multiBodyConstraintSolver, align 4
  %m_scratch_world_to_local = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 6
  %call5 = call %class.btAlignedObjectArray.77* @_ZN20btAlignedObjectArrayI12btQuaternionEC2Ev(%class.btAlignedObjectArray.77* %m_scratch_world_to_local)
  %m_scratch_local_origin = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 7
  %call6 = call %class.btAlignedObjectArray.42* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.42* %m_scratch_local_origin)
  %m_scratch_world_to_local1 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 8
  %call7 = call %class.btAlignedObjectArray.77* @_ZN20btAlignedObjectArrayI12btQuaternionEC2Ev(%class.btAlignedObjectArray.77* %m_scratch_world_to_local1)
  %m_scratch_local_origin1 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 9
  %call8 = call %class.btAlignedObjectArray.42* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.42* %m_scratch_local_origin1)
  %m_scratch_r = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 10
  %call9 = call %class.btAlignedObjectArray.38* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.38* %m_scratch_r)
  %m_scratch_v = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 11
  %call10 = call %class.btAlignedObjectArray.42* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.42* %m_scratch_v)
  %m_scratch_m = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 12
  %call11 = call %class.btAlignedObjectArray.46* @_ZN20btAlignedObjectArrayI11btMatrix3x3EC2Ev(%class.btAlignedObjectArray.46* %m_scratch_m)
  %8 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call12 = call nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %8)
  %9 = bitcast %struct.btContactSolverInfo* %call12 to %struct.btContactSolverInfoData*
  %m_solverMode = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %9, i32 0, i32 16
  %10 = load i32, i32* %m_solverMode, align 4
  %or = or i32 %10, 16
  store i32 %or, i32* %m_solverMode, align 4
  %call13 = call noalias nonnull i8* @_Znwm(i32 116) #11
  %11 = bitcast i8* %call13 to %struct.MultiBodyInplaceSolverIslandCallback*
  %12 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %constraintSolver.addr, align 4
  %13 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %call14 = call %struct.MultiBodyInplaceSolverIslandCallback* @_ZN36MultiBodyInplaceSolverIslandCallbackC2EP27btMultiBodyConstraintSolverP12btDispatcher(%struct.MultiBodyInplaceSolverIslandCallback* %11, %class.btMultiBodyConstraintSolver* %12, %class.btDispatcher* %13)
  %m_solverMultiBodyIslandCallback = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 5
  store %struct.MultiBodyInplaceSolverIslandCallback* %11, %struct.MultiBodyInplaceSolverIslandCallback** %m_solverMultiBodyIslandCallback, align 4
  ret %class.btMultiBodyDynamicsWorld* %this1
}

declare %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration(%class.btDiscreteDynamicsWorld* returned, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btConstraintSolver*, %class.btCollisionConfiguration*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.27* @_ZN20btAlignedObjectArrayIP11btMultiBodyEC2Ev(%class.btAlignedObjectArray.27* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.27*, align 4
  store %class.btAlignedObjectArray.27* %this, %class.btAlignedObjectArray.27** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.27*, %class.btAlignedObjectArray.27** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.28* @_ZN18btAlignedAllocatorIP11btMultiBodyLj16EEC2Ev(%class.btAlignedAllocator.28* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE4initEv(%class.btAlignedObjectArray.27* %this1)
  ret %class.btAlignedObjectArray.27* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.51* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEC2Ev(%class.btAlignedObjectArray.51* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.52* @_ZN18btAlignedAllocatorIP21btMultiBodyConstraintLj16EEC2Ev(%class.btAlignedAllocator.52* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE4initEv(%class.btAlignedObjectArray.51* %this1)
  ret %class.btAlignedObjectArray.51* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.77* @_ZN20btAlignedObjectArrayI12btQuaternionEC2Ev(%class.btAlignedObjectArray.77* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.78* @_ZN18btAlignedAllocatorI12btQuaternionLj16EEC2Ev(%class.btAlignedAllocator.78* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE4initEv(%class.btAlignedObjectArray.77* %this1)
  ret %class.btAlignedObjectArray.77* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.42* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.42* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.42*, align 4
  store %class.btAlignedObjectArray.42* %this, %class.btAlignedObjectArray.42** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.42*, %class.btAlignedObjectArray.42** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.43* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator.43* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.42* %this1)
  ret %class.btAlignedObjectArray.42* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.38* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.38* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.39* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.39* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.38* %this1)
  ret %class.btAlignedObjectArray.38* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.46* @_ZN20btAlignedObjectArrayI11btMatrix3x3EC2Ev(%class.btAlignedObjectArray.46* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.46*, align 4
  store %class.btAlignedObjectArray.46* %this, %class.btAlignedObjectArray.46** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.46*, %class.btAlignedObjectArray.46** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.47* @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EEC2Ev(%class.btAlignedAllocator.47* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E4initEv(%class.btAlignedObjectArray.46* %this1)
  ret %class.btAlignedObjectArray.46* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(88) %struct.btContactSolverInfo* @_ZN15btDynamicsWorld13getSolverInfoEv(%class.btDynamicsWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  %m_solverInfo = getelementptr inbounds %class.btDynamicsWorld, %class.btDynamicsWorld* %this1, i32 0, i32 4
  ret %struct.btContactSolverInfo* %m_solverInfo
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.MultiBodyInplaceSolverIslandCallback* @_ZN36MultiBodyInplaceSolverIslandCallbackC2EP27btMultiBodyConstraintSolverP12btDispatcher(%struct.MultiBodyInplaceSolverIslandCallback* returned %this, %class.btMultiBodyConstraintSolver* %solver, %class.btDispatcher* %dispatcher) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.MultiBodyInplaceSolverIslandCallback*, align 4
  %solver.addr = alloca %class.btMultiBodyConstraintSolver*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %struct.MultiBodyInplaceSolverIslandCallback* %this, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4
  store %class.btMultiBodyConstraintSolver* %solver, %class.btMultiBodyConstraintSolver** %solver.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4
  %0 = bitcast %struct.MultiBodyInplaceSolverIslandCallback* %this1 to %"struct.btSimulationIslandManager::IslandCallback"*
  %call = call %"struct.btSimulationIslandManager::IslandCallback"* @_ZN25btSimulationIslandManager14IslandCallbackC2Ev(%"struct.btSimulationIslandManager::IslandCallback"* %0) #10
  %1 = bitcast %struct.MultiBodyInplaceSolverIslandCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV36MultiBodyInplaceSolverIslandCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_solverInfo = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 1
  store %struct.btContactSolverInfo* null, %struct.btContactSolverInfo** %m_solverInfo, align 4
  %m_solver = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 2
  %2 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %solver.addr, align 4
  store %class.btMultiBodyConstraintSolver* %2, %class.btMultiBodyConstraintSolver** %m_solver, align 4
  %m_multiBodySortedConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 3
  store %class.btMultiBodyConstraint** null, %class.btMultiBodyConstraint*** %m_multiBodySortedConstraints, align 4
  %m_numConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 6
  store i32 0, i32* %m_numConstraints, align 4
  %m_debugDrawer = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 7
  store %class.btIDebugDraw* null, %class.btIDebugDraw** %m_debugDrawer, align 4
  %m_dispatcher = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 8
  %3 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  store %class.btDispatcher* %3, %class.btDispatcher** %m_dispatcher, align 4
  %m_bodies = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 9
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev(%class.btAlignedObjectArray* %m_bodies)
  %m_manifolds = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 10
  %call3 = call %class.btAlignedObjectArray.13* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.13* %m_manifolds)
  %m_constraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 11
  %call4 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev(%class.btAlignedObjectArray.4* %m_constraints)
  %m_multiBodyConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 12
  %call5 = call %class.btAlignedObjectArray.51* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEC2Ev(%class.btAlignedObjectArray.51* %m_multiBodyConstraints)
  ret %struct.MultiBodyInplaceSolverIslandCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btMultiBodyDynamicsWorld* @_ZN24btMultiBodyDynamicsWorldD2Ev(%class.btMultiBodyDynamicsWorld* returned %this) unnamed_addr #1 {
entry:
  %retval = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  store %class.btMultiBodyDynamicsWorld* %this1, %class.btMultiBodyDynamicsWorld** %retval, align 4
  %0 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [61 x i8*] }, { [61 x i8*] }* @_ZTV24btMultiBodyDynamicsWorld, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_solverMultiBodyIslandCallback = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 5
  %1 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %m_solverMultiBodyIslandCallback, align 4
  %isnull = icmp eq %struct.MultiBodyInplaceSolverIslandCallback* %1, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %2 = bitcast %struct.MultiBodyInplaceSolverIslandCallback* %1 to void (%struct.MultiBodyInplaceSolverIslandCallback*)***
  %vtable = load void (%struct.MultiBodyInplaceSolverIslandCallback*)**, void (%struct.MultiBodyInplaceSolverIslandCallback*)*** %2, align 4
  %vfn = getelementptr inbounds void (%struct.MultiBodyInplaceSolverIslandCallback*)*, void (%struct.MultiBodyInplaceSolverIslandCallback*)** %vtable, i64 1
  %3 = load void (%struct.MultiBodyInplaceSolverIslandCallback*)*, void (%struct.MultiBodyInplaceSolverIslandCallback*)** %vfn, align 4
  call void %3(%struct.MultiBodyInplaceSolverIslandCallback* %1) #10
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  %m_scratch_m = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 12
  %call = call %class.btAlignedObjectArray.46* @_ZN20btAlignedObjectArrayI11btMatrix3x3ED2Ev(%class.btAlignedObjectArray.46* %m_scratch_m) #10
  %m_scratch_v = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 11
  %call2 = call %class.btAlignedObjectArray.42* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.42* %m_scratch_v) #10
  %m_scratch_r = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 10
  %call3 = call %class.btAlignedObjectArray.38* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.38* %m_scratch_r) #10
  %m_scratch_local_origin1 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 9
  %call4 = call %class.btAlignedObjectArray.42* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.42* %m_scratch_local_origin1) #10
  %m_scratch_world_to_local1 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 8
  %call5 = call %class.btAlignedObjectArray.77* @_ZN20btAlignedObjectArrayI12btQuaternionED2Ev(%class.btAlignedObjectArray.77* %m_scratch_world_to_local1) #10
  %m_scratch_local_origin = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 7
  %call6 = call %class.btAlignedObjectArray.42* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.42* %m_scratch_local_origin) #10
  %m_scratch_world_to_local = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 6
  %call7 = call %class.btAlignedObjectArray.77* @_ZN20btAlignedObjectArrayI12btQuaternionED2Ev(%class.btAlignedObjectArray.77* %m_scratch_world_to_local) #10
  %m_sortedMultiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 3
  %call8 = call %class.btAlignedObjectArray.51* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintED2Ev(%class.btAlignedObjectArray.51* %m_sortedMultiBodyConstraints) #10
  %m_multiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  %call9 = call %class.btAlignedObjectArray.51* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintED2Ev(%class.btAlignedObjectArray.51* %m_multiBodyConstraints) #10
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call10 = call %class.btAlignedObjectArray.27* @_ZN20btAlignedObjectArrayIP11btMultiBodyED2Ev(%class.btAlignedObjectArray.27* %m_multiBodies) #10
  %4 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call11 = call %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldD2Ev(%class.btDiscreteDynamicsWorld* %4) #10
  %5 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %retval, align 4
  ret %class.btMultiBodyDynamicsWorld* %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.46* @_ZN20btAlignedObjectArrayI11btMatrix3x3ED2Ev(%class.btAlignedObjectArray.46* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.46*, align 4
  store %class.btAlignedObjectArray.46* %this, %class.btAlignedObjectArray.46** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.46*, %class.btAlignedObjectArray.46** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E5clearEv(%class.btAlignedObjectArray.46* %this1)
  ret %class.btAlignedObjectArray.46* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.42* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.42* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.42*, align 4
  store %class.btAlignedObjectArray.42* %this, %class.btAlignedObjectArray.42** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.42*, %class.btAlignedObjectArray.42** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.42* %this1)
  ret %class.btAlignedObjectArray.42* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.38* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.38* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.38* %this1)
  ret %class.btAlignedObjectArray.38* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.77* @_ZN20btAlignedObjectArrayI12btQuaternionED2Ev(%class.btAlignedObjectArray.77* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE5clearEv(%class.btAlignedObjectArray.77* %this1)
  ret %class.btAlignedObjectArray.77* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.51* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintED2Ev(%class.btAlignedObjectArray.51* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE5clearEv(%class.btAlignedObjectArray.51* %this1)
  ret %class.btAlignedObjectArray.51* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.27* @_ZN20btAlignedObjectArrayIP11btMultiBodyED2Ev(%class.btAlignedObjectArray.27* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.27*, align 4
  store %class.btAlignedObjectArray.27* %this, %class.btAlignedObjectArray.27** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.27*, %class.btAlignedObjectArray.27** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE5clearEv(%class.btAlignedObjectArray.27* %this1)
  ret %class.btAlignedObjectArray.27* %this1
}

; Function Attrs: nounwind
declare %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldD2Ev(%class.btDiscreteDynamicsWorld* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN24btMultiBodyDynamicsWorldD0Ev(%class.btMultiBodyDynamicsWorld* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %call = call %class.btMultiBodyDynamicsWorld* @_ZN24btMultiBodyDynamicsWorldD1Ev(%class.btMultiBodyDynamicsWorld* %this1) #10
  %0 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to i8*
  call void @_ZN23btDiscreteDynamicsWorlddlEPv(i8* %0) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN23btDiscreteDynamicsWorlddlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN24btMultiBodyDynamicsWorld17forwardKinematicsEv(%class.btMultiBodyDynamicsWorld* %this) #2 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %b = alloca i32, align 4
  %bod = alloca %class.btMultiBody*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %b, align 4
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %m_multiBodies)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_multiBodies2 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %1 = load i32, i32* %b, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btMultiBody** @_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi(%class.btAlignedObjectArray.27* %m_multiBodies2, i32 %1)
  %2 = load %class.btMultiBody*, %class.btMultiBody** %call3, align 4
  store %class.btMultiBody* %2, %class.btMultiBody** %bod, align 4
  %3 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %m_scratch_world_to_local = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 6
  %m_scratch_local_origin = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 7
  call void @_ZN11btMultiBody17forwardKinematicsER20btAlignedObjectArrayI12btQuaternionERS0_I9btVector3E(%class.btMultiBody* %3, %class.btAlignedObjectArray.77* nonnull align 4 dereferenceable(17) %m_scratch_world_to_local, %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17) %m_scratch_local_origin)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %b, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %b, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

declare void @_ZN11btMultiBody17forwardKinematicsER20btAlignedObjectArrayI12btQuaternionERS0_I9btVector3E(%class.btMultiBody*, %class.btAlignedObjectArray.77* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17)) #3

; Function Attrs: noinline optnone
define hidden void @_ZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfo(%class.btMultiBodyDynamicsWorld* %this, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %solverInfo) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %solverInfo.addr = alloca %struct.btContactSolverInfo*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %ref.tmp = alloca %class.btTypedConstraint*, align 4
  %i = alloca i32, align 4
  %ref.tmp9 = alloca %class.btSortConstraintOnIslandPredicate2, align 1
  %constraintsPtr = alloca %class.btTypedConstraint**, align 4
  %ref.tmp16 = alloca %class.btMultiBodyConstraint*, align 4
  %ref.tmp30 = alloca %class.btSortMultiBodyConstraintOnIslandPredicate, align 1
  %sortedMultiBodyConstraints = alloca %class.btMultiBodyConstraint**, align 4
  %__profile60 = alloca %class.CProfileSample, align 1
  %i62 = alloca i32, align 4
  %bod = alloca %class.btMultiBody*, align 4
  %isSleeping = alloca i8, align 1
  %b = alloca i32, align 4
  %ref.tmp93 = alloca float, align 4
  %ref.tmp96 = alloca %class.btVector3, align 4
  %ref.tmp100 = alloca %class.btMatrix3x3, align 4
  %doNotUpdatePos = alloca i8, align 1
  %numDofs = alloca i32, align 4
  %numPosVars = alloca i32, align 4
  %scratch_r2 = alloca %class.btAlignedObjectArray.38, align 4
  %ref.tmp114 = alloca float, align 4
  %pMem = alloca float*, align 4
  %scratch_q0 = alloca float*, align 4
  %scratch_qx = alloca float*, align 4
  %scratch_qd0 = alloca float*, align 4
  %scratch_qd1 = alloca float*, align 4
  %scratch_qd2 = alloca float*, align 4
  %scratch_qd3 = alloca float*, align 4
  %scratch_qdd0 = alloca float*, align 4
  %scratch_qdd1 = alloca float*, align 4
  %scratch_qdd2 = alloca float*, align 4
  %scratch_qdd3 = alloca float*, align 4
  %link = alloca i32, align 4
  %dof = alloca i32, align 4
  %dof165 = alloca i32, align 4
  %pResetQx = alloca %struct.anon, align 4
  %pEulerIntegrate = alloca %struct.anon.81, align 1
  %pCopyToVelocityVector = alloca %struct.anon.82, align 1
  %pCopy = alloca %struct.anon.83, align 1
  %h = alloca float, align 4
  %delta_q = alloca %class.btAlignedObjectArray.38, align 4
  %ref.tmp208 = alloca float, align 4
  %delta_qd = alloca %class.btAlignedObjectArray.38, align 4
  %ref.tmp210 = alloca float, align 4
  %i211 = alloca i32, align 4
  %pRealBuf = alloca float*, align 4
  %i252 = alloca i32, align 4
  %link262 = alloca i32, align 4
  %__profile289 = alloca %class.CProfileSample, align 1
  %i291 = alloca i32, align 4
  %bod297 = alloca %class.btMultiBody*, align 4
  %isSleeping300 = alloca i8, align 1
  %b309 = alloca i32, align 4
  %ref.tmp332 = alloca float, align 4
  %ref.tmp336 = alloca %class.btVector3, align 4
  %ref.tmp341 = alloca %class.btMatrix3x3, align 4
  %isConstraintPass = alloca i8, align 1
  %i356 = alloca i32, align 4
  %bod362 = alloca %class.btMultiBody*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  store %struct.btContactSolverInfo* %solverInfo, %struct.btContactSolverInfo** %solverInfo.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  call void @_ZN24btMultiBodyDynamicsWorld17forwardKinematicsEv(%class.btMultiBodyDynamicsWorld* %this1)
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.2, i32 0, i32 0))
  %0 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_sortedConstraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %0, i32 0, i32 1
  %1 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_constraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %1, i32 0, i32 5
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %m_constraints)
  store %class.btTypedConstraint* null, %class.btTypedConstraint** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.4* %m_sortedConstraints, i32 %call2, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %ref.tmp)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4
  %3 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %4 = bitcast %class.btDiscreteDynamicsWorld* %3 to i32 (%class.btDiscreteDynamicsWorld*)***
  %vtable = load i32 (%class.btDiscreteDynamicsWorld*)**, i32 (%class.btDiscreteDynamicsWorld*)*** %4, align 4
  %vfn = getelementptr inbounds i32 (%class.btDiscreteDynamicsWorld*)*, i32 (%class.btDiscreteDynamicsWorld*)** %vtable, i64 26
  %5 = load i32 (%class.btDiscreteDynamicsWorld*)*, i32 (%class.btDiscreteDynamicsWorld*)** %vfn, align 4
  %call3 = call i32 %5(%class.btDiscreteDynamicsWorld* %3)
  %cmp = icmp slt i32 %2, %call3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %6 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_constraints4 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %6, i32 0, i32 5
  %7 = load i32, i32* %i, align 4
  %call5 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.4* %m_constraints4, i32 %7)
  %8 = load %class.btTypedConstraint*, %class.btTypedConstraint** %call5, align 4
  %9 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_sortedConstraints6 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %9, i32 0, i32 1
  %10 = load i32, i32* %i, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.4* %m_sortedConstraints6, i32 %10)
  store %class.btTypedConstraint* %8, %class.btTypedConstraint** %call7, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_sortedConstraints8 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %12, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9quickSortI34btSortConstraintOnIslandPredicate2EEvRKT_(%class.btAlignedObjectArray.4* %m_sortedConstraints8, %class.btSortConstraintOnIslandPredicate2* nonnull align 1 dereferenceable(1) %ref.tmp9)
  %13 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %14 = bitcast %class.btDiscreteDynamicsWorld* %13 to i32 (%class.btDiscreteDynamicsWorld*)***
  %vtable10 = load i32 (%class.btDiscreteDynamicsWorld*)**, i32 (%class.btDiscreteDynamicsWorld*)*** %14, align 4
  %vfn11 = getelementptr inbounds i32 (%class.btDiscreteDynamicsWorld*)*, i32 (%class.btDiscreteDynamicsWorld*)** %vtable10, i64 26
  %15 = load i32 (%class.btDiscreteDynamicsWorld*)*, i32 (%class.btDiscreteDynamicsWorld*)** %vfn11, align 4
  %call12 = call i32 %15(%class.btDiscreteDynamicsWorld* %13)
  %tobool = icmp ne i32 %call12, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end
  %16 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_sortedConstraints13 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %16, i32 0, i32 1
  %call14 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.4* %m_sortedConstraints13, i32 0)
  br label %cond.end

cond.false:                                       ; preds = %for.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btTypedConstraint** [ %call14, %cond.true ], [ null, %cond.false ]
  store %class.btTypedConstraint** %cond, %class.btTypedConstraint*** %constraintsPtr, align 4
  %m_sortedMultiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 3
  %m_multiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  %call15 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %m_multiBodyConstraints)
  store %class.btMultiBodyConstraint* null, %class.btMultiBodyConstraint** %ref.tmp16, align 4
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.51* %m_sortedMultiBodyConstraints, i32 %call15, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %ref.tmp16)
  store i32 0, i32* %i, align 4
  br label %for.cond17

for.cond17:                                       ; preds = %for.inc26, %cond.end
  %17 = load i32, i32* %i, align 4
  %m_multiBodyConstraints18 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  %call19 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %m_multiBodyConstraints18)
  %cmp20 = icmp slt i32 %17, %call19
  br i1 %cmp20, label %for.body21, label %for.end28

for.body21:                                       ; preds = %for.cond17
  %m_multiBodyConstraints22 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  %18 = load i32, i32* %i, align 4
  %call23 = call nonnull align 4 dereferenceable(4) %class.btMultiBodyConstraint** @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEixEi(%class.btAlignedObjectArray.51* %m_multiBodyConstraints22, i32 %18)
  %19 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %call23, align 4
  %m_sortedMultiBodyConstraints24 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 3
  %20 = load i32, i32* %i, align 4
  %call25 = call nonnull align 4 dereferenceable(4) %class.btMultiBodyConstraint** @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEixEi(%class.btAlignedObjectArray.51* %m_sortedMultiBodyConstraints24, i32 %20)
  store %class.btMultiBodyConstraint* %19, %class.btMultiBodyConstraint** %call25, align 4
  br label %for.inc26

for.inc26:                                        ; preds = %for.body21
  %21 = load i32, i32* %i, align 4
  %inc27 = add nsw i32 %21, 1
  store i32 %inc27, i32* %i, align 4
  br label %for.cond17

for.end28:                                        ; preds = %for.cond17
  %m_sortedMultiBodyConstraints29 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 3
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9quickSortI42btSortMultiBodyConstraintOnIslandPredicateEEvRKT_(%class.btAlignedObjectArray.51* %m_sortedMultiBodyConstraints29, %class.btSortMultiBodyConstraintOnIslandPredicate* nonnull align 1 dereferenceable(1) %ref.tmp30)
  %m_sortedMultiBodyConstraints31 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 3
  %call32 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %m_sortedMultiBodyConstraints31)
  %tobool33 = icmp ne i32 %call32, 0
  br i1 %tobool33, label %cond.true34, label %cond.false37

cond.true34:                                      ; preds = %for.end28
  %m_sortedMultiBodyConstraints35 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 3
  %call36 = call nonnull align 4 dereferenceable(4) %class.btMultiBodyConstraint** @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEixEi(%class.btAlignedObjectArray.51* %m_sortedMultiBodyConstraints35, i32 0)
  br label %cond.end38

cond.false37:                                     ; preds = %for.end28
  br label %cond.end38

cond.end38:                                       ; preds = %cond.false37, %cond.true34
  %cond39 = phi %class.btMultiBodyConstraint** [ %call36, %cond.true34 ], [ null, %cond.false37 ]
  store %class.btMultiBodyConstraint** %cond39, %class.btMultiBodyConstraint*** %sortedMultiBodyConstraints, align 4
  %m_solverMultiBodyIslandCallback = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 5
  %22 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %m_solverMultiBodyIslandCallback, align 4
  %23 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %solverInfo.addr, align 4
  %24 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraintsPtr, align 4
  %25 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_sortedConstraints40 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %25, i32 0, i32 1
  %call41 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %m_sortedConstraints40)
  %26 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %sortedMultiBodyConstraints, align 4
  %m_sortedMultiBodyConstraints42 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 3
  %call43 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %m_sortedMultiBodyConstraints42)
  %27 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btCollisionWorld*
  %28 = bitcast %class.btCollisionWorld* %27 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable44 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %28, align 4
  %vfn45 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable44, i64 5
  %29 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn45, align 4
  %call46 = call %class.btIDebugDraw* %29(%class.btCollisionWorld* %27)
  call void @_ZN36MultiBodyInplaceSolverIslandCallback5setupEP19btContactSolverInfoPP17btTypedConstraintiPP21btMultiBodyConstraintiP12btIDebugDraw(%struct.MultiBodyInplaceSolverIslandCallback* %22, %struct.btContactSolverInfo* %23, %class.btTypedConstraint** %24, i32 %call41, %class.btMultiBodyConstraint** %26, i32 %call43, %class.btIDebugDraw* %call46)
  %30 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_constraintSolver = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %30, i32 0, i32 3
  %31 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver, align 4
  %32 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call47 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %32)
  %call48 = call i32 @_ZNK16btCollisionWorld22getNumCollisionObjectsEv(%class.btCollisionWorld* %call47)
  %33 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call49 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %33)
  %call50 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %call49)
  %34 = bitcast %class.btDispatcher* %call50 to i32 (%class.btDispatcher*)***
  %vtable51 = load i32 (%class.btDispatcher*)**, i32 (%class.btDispatcher*)*** %34, align 4
  %vfn52 = getelementptr inbounds i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vtable51, i64 9
  %35 = load i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vfn52, align 4
  %call53 = call i32 %35(%class.btDispatcher* %call50)
  %36 = bitcast %class.btConstraintSolver* %31 to void (%class.btConstraintSolver*, i32, i32)***
  %vtable54 = load void (%class.btConstraintSolver*, i32, i32)**, void (%class.btConstraintSolver*, i32, i32)*** %36, align 4
  %vfn55 = getelementptr inbounds void (%class.btConstraintSolver*, i32, i32)*, void (%class.btConstraintSolver*, i32, i32)** %vtable54, i64 2
  %37 = load void (%class.btConstraintSolver*, i32, i32)*, void (%class.btConstraintSolver*, i32, i32)** %vfn55, align 4
  call void %37(%class.btConstraintSolver* %31, i32 %call48, i32 %call53)
  %38 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_islandManager = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %38, i32 0, i32 4
  %39 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %m_islandManager, align 4
  %40 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call56 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %40)
  %call57 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %call56)
  %41 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call58 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %41)
  %m_solverMultiBodyIslandCallback59 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 5
  %42 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %m_solverMultiBodyIslandCallback59, align 4
  %43 = bitcast %struct.MultiBodyInplaceSolverIslandCallback* %42 to %"struct.btSimulationIslandManager::IslandCallback"*
  call void @_ZN25btSimulationIslandManager22buildAndProcessIslandsEP12btDispatcherP16btCollisionWorldPNS_14IslandCallbackE(%class.btSimulationIslandManager* %39, %class.btDispatcher* %call57, %class.btCollisionWorld* %call58, %"struct.btSimulationIslandManager::IslandCallback"* %43)
  %call61 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile60, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.3, i32 0, i32 0))
  store i32 0, i32* %i62, align 4
  br label %for.cond63

for.cond63:                                       ; preds = %for.inc279, %cond.end38
  %44 = load i32, i32* %i62, align 4
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call64 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %m_multiBodies)
  %cmp65 = icmp slt i32 %44, %call64
  br i1 %cmp65, label %for.body66, label %for.end281

for.body66:                                       ; preds = %for.cond63
  %m_multiBodies67 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %45 = load i32, i32* %i62, align 4
  %call68 = call nonnull align 4 dereferenceable(4) %class.btMultiBody** @_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi(%class.btAlignedObjectArray.27* %m_multiBodies67, i32 %45)
  %46 = load %class.btMultiBody*, %class.btMultiBody** %call68, align 4
  store %class.btMultiBody* %46, %class.btMultiBody** %bod, align 4
  store i8 0, i8* %isSleeping, align 1
  %47 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call69 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %47)
  %tobool70 = icmp ne %class.btMultiBodyLinkCollider* %call69, null
  br i1 %tobool70, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body66
  %48 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call71 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %48)
  %49 = bitcast %class.btMultiBodyLinkCollider* %call71 to %class.btCollisionObject*
  %call72 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %49)
  %cmp73 = icmp eq i32 %call72, 2
  br i1 %cmp73, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i8 1, i8* %isSleeping, align 1
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %for.body66
  store i32 0, i32* %b, align 4
  br label %for.cond74

for.cond74:                                       ; preds = %for.inc87, %if.end
  %50 = load i32, i32* %b, align 4
  %51 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call75 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %51)
  %cmp76 = icmp slt i32 %50, %call75
  br i1 %cmp76, label %for.body77, label %for.end89

for.body77:                                       ; preds = %for.cond74
  %52 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %53 = load i32, i32* %b, align 4
  %call78 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %52, i32 %53)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call78, i32 0, i32 19
  %54 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4
  %tobool79 = icmp ne %class.btMultiBodyLinkCollider* %54, null
  br i1 %tobool79, label %land.lhs.true80, label %if.end86

land.lhs.true80:                                  ; preds = %for.body77
  %55 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %56 = load i32, i32* %b, align 4
  %call81 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %55, i32 %56)
  %m_collider82 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call81, i32 0, i32 19
  %57 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider82, align 4
  %58 = bitcast %class.btMultiBodyLinkCollider* %57 to %class.btCollisionObject*
  %call83 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %58)
  %cmp84 = icmp eq i32 %call83, 2
  br i1 %cmp84, label %if.then85, label %if.end86

if.then85:                                        ; preds = %land.lhs.true80
  store i8 1, i8* %isSleeping, align 1
  br label %if.end86

if.end86:                                         ; preds = %if.then85, %land.lhs.true80, %for.body77
  br label %for.inc87

for.inc87:                                        ; preds = %if.end86
  %59 = load i32, i32* %b, align 4
  %inc88 = add nsw i32 %59, 1
  store i32 %inc88, i32* %b, align 4
  br label %for.cond74

for.end89:                                        ; preds = %for.cond74
  %60 = load i8, i8* %isSleeping, align 1
  %tobool90 = trunc i8 %60 to i1
  br i1 %tobool90, label %if.end278, label %if.then91

if.then91:                                        ; preds = %for.end89
  %m_scratch_r = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 10
  %61 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call92 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %61)
  %add = add nsw i32 %call92, 1
  store float 0.000000e+00, float* %ref.tmp93, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.38* %m_scratch_r, i32 %add, float* nonnull align 4 dereferenceable(4) %ref.tmp93)
  %m_scratch_v = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 11
  %62 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call94 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %62)
  %add95 = add nsw i32 %call94, 1
  %call97 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp96)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.42* %m_scratch_v, i32 %add95, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp96)
  %m_scratch_m = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 12
  %63 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call98 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %63)
  %add99 = add nsw i32 %call98, 1
  %call101 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %ref.tmp100)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E6resizeEiRKS0_(%class.btAlignedObjectArray.46* %m_scratch_m, i32 %add99, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp100)
  store i8 0, i8* %doNotUpdatePos, align 1
  %64 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call102 = call zeroext i1 @_ZNK11btMultiBody21isUsingRK4IntegrationEv(%class.btMultiBody* %64)
  br i1 %call102, label %if.else, label %if.then103

if.then103:                                       ; preds = %if.then91
  %65 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %66 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %solverInfo.addr, align 4
  %67 = bitcast %struct.btContactSolverInfo* %66 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %67, i32 0, i32 3
  %68 = load float, float* %m_timeStep, align 4
  %m_scratch_r104 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 10
  %m_scratch_v105 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 11
  %m_scratch_m106 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 12
  call void @_ZN11btMultiBody52computeAccelerationsArticulatedBodyAlgorithmMultiDofEfR20btAlignedObjectArrayIfERS0_I9btVector3ERS0_I11btMatrix3x3Eb(%class.btMultiBody* %65, float %68, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17) %m_scratch_r104, %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17) %m_scratch_v105, %class.btAlignedObjectArray.46* nonnull align 4 dereferenceable(17) %m_scratch_m106, i1 zeroext false)
  br label %if.end277

if.else:                                          ; preds = %if.then91
  %69 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call107 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %69)
  %add108 = add nsw i32 %call107, 6
  store i32 %add108, i32* %numDofs, align 4
  %70 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call109 = call i32 @_ZNK11btMultiBody13getNumPosVarsEv(%class.btMultiBody* %70)
  %add110 = add nsw i32 %call109, 7
  store i32 %add110, i32* %numPosVars, align 4
  %call111 = call %class.btAlignedObjectArray.38* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.38* %scratch_r2)
  %71 = load i32, i32* %numPosVars, align 4
  %mul = mul nsw i32 2, %71
  %72 = load i32, i32* %numDofs, align 4
  %mul112 = mul nsw i32 8, %72
  %add113 = add nsw i32 %mul, %mul112
  store float 0.000000e+00, float* %ref.tmp114, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.38* %scratch_r2, i32 %add113, float* nonnull align 4 dereferenceable(4) %ref.tmp114)
  %call115 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.38* %scratch_r2, i32 0)
  store float* %call115, float** %pMem, align 4
  %73 = load float*, float** %pMem, align 4
  store float* %73, float** %scratch_q0, align 4
  %74 = load i32, i32* %numPosVars, align 4
  %75 = load float*, float** %pMem, align 4
  %add.ptr = getelementptr inbounds float, float* %75, i32 %74
  store float* %add.ptr, float** %pMem, align 4
  %76 = load float*, float** %pMem, align 4
  store float* %76, float** %scratch_qx, align 4
  %77 = load i32, i32* %numPosVars, align 4
  %78 = load float*, float** %pMem, align 4
  %add.ptr116 = getelementptr inbounds float, float* %78, i32 %77
  store float* %add.ptr116, float** %pMem, align 4
  %79 = load float*, float** %pMem, align 4
  store float* %79, float** %scratch_qd0, align 4
  %80 = load i32, i32* %numDofs, align 4
  %81 = load float*, float** %pMem, align 4
  %add.ptr117 = getelementptr inbounds float, float* %81, i32 %80
  store float* %add.ptr117, float** %pMem, align 4
  %82 = load float*, float** %pMem, align 4
  store float* %82, float** %scratch_qd1, align 4
  %83 = load i32, i32* %numDofs, align 4
  %84 = load float*, float** %pMem, align 4
  %add.ptr118 = getelementptr inbounds float, float* %84, i32 %83
  store float* %add.ptr118, float** %pMem, align 4
  %85 = load float*, float** %pMem, align 4
  store float* %85, float** %scratch_qd2, align 4
  %86 = load i32, i32* %numDofs, align 4
  %87 = load float*, float** %pMem, align 4
  %add.ptr119 = getelementptr inbounds float, float* %87, i32 %86
  store float* %add.ptr119, float** %pMem, align 4
  %88 = load float*, float** %pMem, align 4
  store float* %88, float** %scratch_qd3, align 4
  %89 = load i32, i32* %numDofs, align 4
  %90 = load float*, float** %pMem, align 4
  %add.ptr120 = getelementptr inbounds float, float* %90, i32 %89
  store float* %add.ptr120, float** %pMem, align 4
  %91 = load float*, float** %pMem, align 4
  store float* %91, float** %scratch_qdd0, align 4
  %92 = load i32, i32* %numDofs, align 4
  %93 = load float*, float** %pMem, align 4
  %add.ptr121 = getelementptr inbounds float, float* %93, i32 %92
  store float* %add.ptr121, float** %pMem, align 4
  %94 = load float*, float** %pMem, align 4
  store float* %94, float** %scratch_qdd1, align 4
  %95 = load i32, i32* %numDofs, align 4
  %96 = load float*, float** %pMem, align 4
  %add.ptr122 = getelementptr inbounds float, float* %96, i32 %95
  store float* %add.ptr122, float** %pMem, align 4
  %97 = load float*, float** %pMem, align 4
  store float* %97, float** %scratch_qdd2, align 4
  %98 = load i32, i32* %numDofs, align 4
  %99 = load float*, float** %pMem, align 4
  %add.ptr123 = getelementptr inbounds float, float* %99, i32 %98
  store float* %add.ptr123, float** %pMem, align 4
  %100 = load float*, float** %pMem, align 4
  store float* %100, float** %scratch_qdd3, align 4
  %101 = load i32, i32* %numDofs, align 4
  %102 = load float*, float** %pMem, align 4
  %add.ptr124 = getelementptr inbounds float, float* %102, i32 %101
  store float* %add.ptr124, float** %pMem, align 4
  %103 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call125 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody17getWorldToBaseRotEv(%class.btMultiBody* %103)
  %104 = bitcast %class.btQuaternion* %call125 to %class.btQuadWord*
  %call126 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %104)
  %105 = load float, float* %call126, align 4
  %106 = load float*, float** %scratch_q0, align 4
  %arrayidx = getelementptr inbounds float, float* %106, i32 0
  store float %105, float* %arrayidx, align 4
  %107 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call127 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody17getWorldToBaseRotEv(%class.btMultiBody* %107)
  %108 = bitcast %class.btQuaternion* %call127 to %class.btQuadWord*
  %call128 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %108)
  %109 = load float, float* %call128, align 4
  %110 = load float*, float** %scratch_q0, align 4
  %arrayidx129 = getelementptr inbounds float, float* %110, i32 1
  store float %109, float* %arrayidx129, align 4
  %111 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call130 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody17getWorldToBaseRotEv(%class.btMultiBody* %111)
  %112 = bitcast %class.btQuaternion* %call130 to %class.btQuadWord*
  %call131 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %112)
  %113 = load float, float* %call131, align 4
  %114 = load float*, float** %scratch_q0, align 4
  %arrayidx132 = getelementptr inbounds float, float* %114, i32 2
  store float %113, float* %arrayidx132, align 4
  %115 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call133 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody17getWorldToBaseRotEv(%class.btMultiBody* %115)
  %116 = bitcast %class.btQuaternion* %call133 to %class.btQuadWord*
  %call134 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %116)
  %117 = load float, float* %call134, align 4
  %118 = load float*, float** %scratch_q0, align 4
  %arrayidx135 = getelementptr inbounds float, float* %118, i32 3
  store float %117, float* %arrayidx135, align 4
  %119 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call136 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getBasePosEv(%class.btMultiBody* %119)
  %call137 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call136)
  %120 = load float, float* %call137, align 4
  %121 = load float*, float** %scratch_q0, align 4
  %arrayidx138 = getelementptr inbounds float, float* %121, i32 4
  store float %120, float* %arrayidx138, align 4
  %122 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call139 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getBasePosEv(%class.btMultiBody* %122)
  %call140 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call139)
  %123 = load float, float* %call140, align 4
  %124 = load float*, float** %scratch_q0, align 4
  %arrayidx141 = getelementptr inbounds float, float* %124, i32 5
  store float %123, float* %arrayidx141, align 4
  %125 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call142 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getBasePosEv(%class.btMultiBody* %125)
  %call143 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call142)
  %126 = load float, float* %call143, align 4
  %127 = load float*, float** %scratch_q0, align 4
  %arrayidx144 = getelementptr inbounds float, float* %127, i32 6
  store float %126, float* %arrayidx144, align 4
  store i32 0, i32* %link, align 4
  br label %for.cond145

for.cond145:                                      ; preds = %for.inc162, %if.else
  %128 = load i32, i32* %link, align 4
  %129 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call146 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %129)
  %cmp147 = icmp slt i32 %128, %call146
  br i1 %cmp147, label %for.body148, label %for.end164

for.body148:                                      ; preds = %for.cond145
  store i32 0, i32* %dof, align 4
  br label %for.cond149

for.cond149:                                      ; preds = %for.inc159, %for.body148
  %130 = load i32, i32* %dof, align 4
  %131 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %132 = load i32, i32* %link, align 4
  %call150 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %131, i32 %132)
  %m_posVarCount = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call150, i32 0, i32 22
  %133 = load i32, i32* %m_posVarCount, align 4
  %cmp151 = icmp slt i32 %130, %133
  br i1 %cmp151, label %for.body152, label %for.end161

for.body152:                                      ; preds = %for.cond149
  %134 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %135 = load i32, i32* %link, align 4
  %call153 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %134, i32 %135)
  %m_jointPos = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call153, i32 0, i32 17
  %136 = load i32, i32* %dof, align 4
  %arrayidx154 = getelementptr inbounds [7 x float], [7 x float]* %m_jointPos, i32 0, i32 %136
  %137 = load float, float* %arrayidx154, align 4
  %138 = load float*, float** %scratch_q0, align 4
  %139 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %140 = load i32, i32* %link, align 4
  %call155 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %139, i32 %140)
  %m_cfgOffset = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call155, i32 0, i32 10
  %141 = load i32, i32* %m_cfgOffset, align 4
  %add156 = add nsw i32 7, %141
  %142 = load i32, i32* %dof, align 4
  %add157 = add nsw i32 %add156, %142
  %arrayidx158 = getelementptr inbounds float, float* %138, i32 %add157
  store float %137, float* %arrayidx158, align 4
  br label %for.inc159

for.inc159:                                       ; preds = %for.body152
  %143 = load i32, i32* %dof, align 4
  %inc160 = add nsw i32 %143, 1
  store i32 %inc160, i32* %dof, align 4
  br label %for.cond149

for.end161:                                       ; preds = %for.cond149
  br label %for.inc162

for.inc162:                                       ; preds = %for.end161
  %144 = load i32, i32* %link, align 4
  %inc163 = add nsw i32 %144, 1
  store i32 %inc163, i32* %link, align 4
  br label %for.cond145

for.end164:                                       ; preds = %for.cond145
  store i32 0, i32* %dof165, align 4
  br label %for.cond166

for.cond166:                                      ; preds = %for.inc172, %for.end164
  %145 = load i32, i32* %dof165, align 4
  %146 = load i32, i32* %numDofs, align 4
  %cmp167 = icmp slt i32 %145, %146
  br i1 %cmp167, label %for.body168, label %for.end174

for.body168:                                      ; preds = %for.cond166
  %147 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call169 = call float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %147)
  %148 = load i32, i32* %dof165, align 4
  %arrayidx170 = getelementptr inbounds float, float* %call169, i32 %148
  %149 = load float, float* %arrayidx170, align 4
  %150 = load float*, float** %scratch_qd0, align 4
  %151 = load i32, i32* %dof165, align 4
  %arrayidx171 = getelementptr inbounds float, float* %150, i32 %151
  store float %149, float* %arrayidx171, align 4
  br label %for.inc172

for.inc172:                                       ; preds = %for.body168
  %152 = load i32, i32* %dof165, align 4
  %inc173 = add nsw i32 %152, 1
  store i32 %inc173, i32* %dof165, align 4
  br label %for.cond166

for.end174:                                       ; preds = %for.cond166
  %bod175 = getelementptr inbounds %struct.anon, %struct.anon* %pResetQx, i32 0, i32 0
  %153 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  store %class.btMultiBody* %153, %class.btMultiBody** %bod175, align 4
  %scratch_qx176 = getelementptr inbounds %struct.anon, %struct.anon* %pResetQx, i32 0, i32 1
  %154 = load float*, float** %scratch_qx, align 4
  store float* %154, float** %scratch_qx176, align 4
  %scratch_q0177 = getelementptr inbounds %struct.anon, %struct.anon* %pResetQx, i32 0, i32 2
  %155 = load float*, float** %scratch_q0, align 4
  store float* %155, float** %scratch_q0177, align 4
  %156 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %solverInfo.addr, align 4
  %157 = bitcast %struct.btContactSolverInfo* %156 to %struct.btContactSolverInfoData*
  %m_timeStep178 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %157, i32 0, i32 3
  %158 = load float, float* %m_timeStep178, align 4
  store float %158, float* %h, align 4
  %159 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %m_scratch_r179 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 10
  %m_scratch_v180 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 11
  %m_scratch_m181 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 12
  call void @_ZN11btMultiBody52computeAccelerationsArticulatedBodyAlgorithmMultiDofEfR20btAlignedObjectArrayIfERS0_I9btVector3ERS0_I11btMatrix3x3Eb(%class.btMultiBody* %159, float 0.000000e+00, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17) %m_scratch_r179, %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17) %m_scratch_v180, %class.btAlignedObjectArray.46* nonnull align 4 dereferenceable(17) %m_scratch_m181, i1 zeroext false)
  %m_scratch_r182 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 10
  %160 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call183 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %160)
  %call184 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.38* %m_scratch_r182, i32 %call183)
  %161 = load float*, float** %scratch_qdd0, align 4
  %162 = load i32, i32* %numDofs, align 4
  call void @"_ZZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfoEN3$_0clEPKfPfii"(%struct.anon.83* %pCopy, float* %call184, float* %161, i32 0, i32 %162)
  call void @"_ZZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfoEN3$_1clEv"(%struct.anon* %pResetQx)
  %163 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %164 = load float, float* %h, align 4
  %mul185 = fmul float 5.000000e-01, %164
  %165 = load float*, float** %scratch_qx, align 4
  %166 = load float*, float** %scratch_qd0, align 4
  call void @_ZN11btMultiBody21stepPositionsMultiDofEfPfS0_(%class.btMultiBody* %163, float %mul185, float* %165, float* %166)
  %167 = load float, float* %h, align 4
  %mul186 = fmul float 5.000000e-01, %167
  %168 = load float*, float** %scratch_qdd0, align 4
  %169 = load float*, float** %scratch_qd0, align 4
  %170 = load float*, float** %scratch_qd1, align 4
  %171 = load i32, i32* %numDofs, align 4
  call void @"_ZZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfoEN3$_2clEfPKfS4_Pfi"(%struct.anon.81* %pEulerIntegrate, float %mul186, float* %168, float* %169, float* %170, i32 %171)
  %172 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %173 = load float*, float** %scratch_qd1, align 4
  call void @"_ZZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfoEN3$_3clEP11btMultiBodyPKf"(%struct.anon.82* %pCopyToVelocityVector, %class.btMultiBody* %172, float* %173)
  %174 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %m_scratch_r187 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 10
  %m_scratch_v188 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 11
  %m_scratch_m189 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 12
  call void @_ZN11btMultiBody52computeAccelerationsArticulatedBodyAlgorithmMultiDofEfR20btAlignedObjectArrayIfERS0_I9btVector3ERS0_I11btMatrix3x3Eb(%class.btMultiBody* %174, float 0.000000e+00, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17) %m_scratch_r187, %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17) %m_scratch_v188, %class.btAlignedObjectArray.46* nonnull align 4 dereferenceable(17) %m_scratch_m189, i1 zeroext false)
  %m_scratch_r190 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 10
  %175 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call191 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %175)
  %call192 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.38* %m_scratch_r190, i32 %call191)
  %176 = load float*, float** %scratch_qdd1, align 4
  %177 = load i32, i32* %numDofs, align 4
  call void @"_ZZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfoEN3$_0clEPKfPfii"(%struct.anon.83* %pCopy, float* %call192, float* %176, i32 0, i32 %177)
  call void @"_ZZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfoEN3$_1clEv"(%struct.anon* %pResetQx)
  %178 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %179 = load float, float* %h, align 4
  %mul193 = fmul float 5.000000e-01, %179
  %180 = load float*, float** %scratch_qx, align 4
  %181 = load float*, float** %scratch_qd1, align 4
  call void @_ZN11btMultiBody21stepPositionsMultiDofEfPfS0_(%class.btMultiBody* %178, float %mul193, float* %180, float* %181)
  %182 = load float, float* %h, align 4
  %mul194 = fmul float 5.000000e-01, %182
  %183 = load float*, float** %scratch_qdd1, align 4
  %184 = load float*, float** %scratch_qd0, align 4
  %185 = load float*, float** %scratch_qd2, align 4
  %186 = load i32, i32* %numDofs, align 4
  call void @"_ZZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfoEN3$_2clEfPKfS4_Pfi"(%struct.anon.81* %pEulerIntegrate, float %mul194, float* %183, float* %184, float* %185, i32 %186)
  %187 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %188 = load float*, float** %scratch_qd2, align 4
  call void @"_ZZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfoEN3$_3clEP11btMultiBodyPKf"(%struct.anon.82* %pCopyToVelocityVector, %class.btMultiBody* %187, float* %188)
  %189 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %m_scratch_r195 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 10
  %m_scratch_v196 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 11
  %m_scratch_m197 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 12
  call void @_ZN11btMultiBody52computeAccelerationsArticulatedBodyAlgorithmMultiDofEfR20btAlignedObjectArrayIfERS0_I9btVector3ERS0_I11btMatrix3x3Eb(%class.btMultiBody* %189, float 0.000000e+00, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17) %m_scratch_r195, %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17) %m_scratch_v196, %class.btAlignedObjectArray.46* nonnull align 4 dereferenceable(17) %m_scratch_m197, i1 zeroext false)
  %m_scratch_r198 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 10
  %190 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call199 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %190)
  %call200 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.38* %m_scratch_r198, i32 %call199)
  %191 = load float*, float** %scratch_qdd2, align 4
  %192 = load i32, i32* %numDofs, align 4
  call void @"_ZZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfoEN3$_0clEPKfPfii"(%struct.anon.83* %pCopy, float* %call200, float* %191, i32 0, i32 %192)
  call void @"_ZZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfoEN3$_1clEv"(%struct.anon* %pResetQx)
  %193 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %194 = load float, float* %h, align 4
  %195 = load float*, float** %scratch_qx, align 4
  %196 = load float*, float** %scratch_qd2, align 4
  call void @_ZN11btMultiBody21stepPositionsMultiDofEfPfS0_(%class.btMultiBody* %193, float %194, float* %195, float* %196)
  %197 = load float, float* %h, align 4
  %198 = load float*, float** %scratch_qdd2, align 4
  %199 = load float*, float** %scratch_qd0, align 4
  %200 = load float*, float** %scratch_qd3, align 4
  %201 = load i32, i32* %numDofs, align 4
  call void @"_ZZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfoEN3$_2clEfPKfS4_Pfi"(%struct.anon.81* %pEulerIntegrate, float %197, float* %198, float* %199, float* %200, i32 %201)
  %202 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %203 = load float*, float** %scratch_qd3, align 4
  call void @"_ZZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfoEN3$_3clEP11btMultiBodyPKf"(%struct.anon.82* %pCopyToVelocityVector, %class.btMultiBody* %202, float* %203)
  %204 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %m_scratch_r201 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 10
  %m_scratch_v202 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 11
  %m_scratch_m203 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 12
  call void @_ZN11btMultiBody52computeAccelerationsArticulatedBodyAlgorithmMultiDofEfR20btAlignedObjectArrayIfERS0_I9btVector3ERS0_I11btMatrix3x3Eb(%class.btMultiBody* %204, float 0.000000e+00, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17) %m_scratch_r201, %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17) %m_scratch_v202, %class.btAlignedObjectArray.46* nonnull align 4 dereferenceable(17) %m_scratch_m203, i1 zeroext false)
  %m_scratch_r204 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 10
  %205 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call205 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %205)
  %call206 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.38* %m_scratch_r204, i32 %call205)
  %206 = load float*, float** %scratch_qdd3, align 4
  %207 = load i32, i32* %numDofs, align 4
  call void @"_ZZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfoEN3$_0clEPKfPfii"(%struct.anon.83* %pCopy, float* %call206, float* %206, i32 0, i32 %207)
  %call207 = call %class.btAlignedObjectArray.38* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.38* %delta_q)
  %208 = load i32, i32* %numDofs, align 4
  store float 0.000000e+00, float* %ref.tmp208, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.38* %delta_q, i32 %208, float* nonnull align 4 dereferenceable(4) %ref.tmp208)
  %call209 = call %class.btAlignedObjectArray.38* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.38* %delta_qd)
  %209 = load i32, i32* %numDofs, align 4
  store float 0.000000e+00, float* %ref.tmp210, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.38* %delta_qd, i32 %209, float* nonnull align 4 dereferenceable(4) %ref.tmp210)
  store i32 0, i32* %i211, align 4
  br label %for.cond212

for.cond212:                                      ; preds = %for.inc238, %for.end174
  %210 = load i32, i32* %i211, align 4
  %211 = load i32, i32* %numDofs, align 4
  %cmp213 = icmp slt i32 %210, %211
  br i1 %cmp213, label %for.body214, label %for.end240

for.body214:                                      ; preds = %for.cond212
  %212 = load float, float* %h, align 4
  %div = fdiv float %212, 6.000000e+00
  %213 = load float*, float** %scratch_qd0, align 4
  %214 = load i32, i32* %i211, align 4
  %arrayidx215 = getelementptr inbounds float, float* %213, i32 %214
  %215 = load float, float* %arrayidx215, align 4
  %216 = load float*, float** %scratch_qd1, align 4
  %217 = load i32, i32* %i211, align 4
  %arrayidx216 = getelementptr inbounds float, float* %216, i32 %217
  %218 = load float, float* %arrayidx216, align 4
  %mul217 = fmul float 2.000000e+00, %218
  %add218 = fadd float %215, %mul217
  %219 = load float*, float** %scratch_qd2, align 4
  %220 = load i32, i32* %i211, align 4
  %arrayidx219 = getelementptr inbounds float, float* %219, i32 %220
  %221 = load float, float* %arrayidx219, align 4
  %mul220 = fmul float 2.000000e+00, %221
  %add221 = fadd float %add218, %mul220
  %222 = load float*, float** %scratch_qd3, align 4
  %223 = load i32, i32* %i211, align 4
  %arrayidx222 = getelementptr inbounds float, float* %222, i32 %223
  %224 = load float, float* %arrayidx222, align 4
  %add223 = fadd float %add221, %224
  %mul224 = fmul float %div, %add223
  %225 = load i32, i32* %i211, align 4
  %call225 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.38* %delta_q, i32 %225)
  store float %mul224, float* %call225, align 4
  %226 = load float, float* %h, align 4
  %div226 = fdiv float %226, 6.000000e+00
  %227 = load float*, float** %scratch_qdd0, align 4
  %228 = load i32, i32* %i211, align 4
  %arrayidx227 = getelementptr inbounds float, float* %227, i32 %228
  %229 = load float, float* %arrayidx227, align 4
  %230 = load float*, float** %scratch_qdd1, align 4
  %231 = load i32, i32* %i211, align 4
  %arrayidx228 = getelementptr inbounds float, float* %230, i32 %231
  %232 = load float, float* %arrayidx228, align 4
  %mul229 = fmul float 2.000000e+00, %232
  %add230 = fadd float %229, %mul229
  %233 = load float*, float** %scratch_qdd2, align 4
  %234 = load i32, i32* %i211, align 4
  %arrayidx231 = getelementptr inbounds float, float* %233, i32 %234
  %235 = load float, float* %arrayidx231, align 4
  %mul232 = fmul float 2.000000e+00, %235
  %add233 = fadd float %add230, %mul232
  %236 = load float*, float** %scratch_qdd3, align 4
  %237 = load i32, i32* %i211, align 4
  %arrayidx234 = getelementptr inbounds float, float* %236, i32 %237
  %238 = load float, float* %arrayidx234, align 4
  %add235 = fadd float %add233, %238
  %mul236 = fmul float %div226, %add235
  %239 = load i32, i32* %i211, align 4
  %call237 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.38* %delta_qd, i32 %239)
  store float %mul236, float* %call237, align 4
  br label %for.inc238

for.inc238:                                       ; preds = %for.body214
  %240 = load i32, i32* %i211, align 4
  %inc239 = add nsw i32 %240, 1
  store i32 %inc239, i32* %i211, align 4
  br label %for.cond212

for.end240:                                       ; preds = %for.cond212
  %241 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %242 = load float*, float** %scratch_qd0, align 4
  call void @"_ZZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfoEN3$_3clEP11btMultiBodyPKf"(%struct.anon.82* %pCopyToVelocityVector, %class.btMultiBody* %241, float* %242)
  %243 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call241 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.38* %delta_qd, i32 0)
  call void @_ZN11btMultiBody21applyDeltaVeeMultiDofEPKff(%class.btMultiBody* %243, float* %call241, float 1.000000e+00)
  %244 = load i8, i8* %doNotUpdatePos, align 1
  %tobool242 = trunc i8 %244 to i1
  br i1 %tobool242, label %if.end261, label %if.then243

if.then243:                                       ; preds = %for.end240
  %245 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call244 = call float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %245)
  store float* %call244, float** %pRealBuf, align 4
  %246 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call245 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %246)
  %add246 = add nsw i32 6, %call245
  %247 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call247 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %247)
  %248 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call248 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %248)
  %mul249 = mul nsw i32 %call247, %call248
  %add250 = add nsw i32 %add246, %mul249
  %249 = load float*, float** %pRealBuf, align 4
  %add.ptr251 = getelementptr inbounds float, float* %249, i32 %add250
  store float* %add.ptr251, float** %pRealBuf, align 4
  store i32 0, i32* %i252, align 4
  br label %for.cond253

for.cond253:                                      ; preds = %for.inc258, %if.then243
  %250 = load i32, i32* %i252, align 4
  %251 = load i32, i32* %numDofs, align 4
  %cmp254 = icmp slt i32 %250, %251
  br i1 %cmp254, label %for.body255, label %for.end260

for.body255:                                      ; preds = %for.cond253
  %252 = load i32, i32* %i252, align 4
  %call256 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.38* %delta_q, i32 %252)
  %253 = load float, float* %call256, align 4
  %254 = load float*, float** %pRealBuf, align 4
  %255 = load i32, i32* %i252, align 4
  %arrayidx257 = getelementptr inbounds float, float* %254, i32 %255
  store float %253, float* %arrayidx257, align 4
  br label %for.inc258

for.inc258:                                       ; preds = %for.body255
  %256 = load i32, i32* %i252, align 4
  %inc259 = add nsw i32 %256, 1
  store i32 %inc259, i32* %i252, align 4
  br label %for.cond253

for.end260:                                       ; preds = %for.cond253
  %257 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  call void @_ZN11btMultiBody13setPosUpdatedEb(%class.btMultiBody* %257, i1 zeroext true)
  br label %if.end261

if.end261:                                        ; preds = %for.end260, %for.end240
  store i32 0, i32* %link262, align 4
  br label %for.cond263

for.cond263:                                      ; preds = %for.inc268, %if.end261
  %258 = load i32, i32* %link262, align 4
  %259 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call264 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %259)
  %cmp265 = icmp slt i32 %258, %call264
  br i1 %cmp265, label %for.body266, label %for.end270

for.body266:                                      ; preds = %for.cond263
  %260 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %261 = load i32, i32* %link262, align 4
  %call267 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %260, i32 %261)
  call void @_ZN15btMultibodyLink19updateCacheMultiDofEPf(%struct.btMultibodyLink* %call267, float* null)
  br label %for.inc268

for.inc268:                                       ; preds = %for.body266
  %262 = load i32, i32* %link262, align 4
  %inc269 = add nsw i32 %262, 1
  store i32 %inc269, i32* %link262, align 4
  br label %for.cond263

for.end270:                                       ; preds = %for.cond263
  %263 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %m_scratch_r271 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 10
  %m_scratch_v272 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 11
  %m_scratch_m273 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 12
  call void @_ZN11btMultiBody52computeAccelerationsArticulatedBodyAlgorithmMultiDofEfR20btAlignedObjectArrayIfERS0_I9btVector3ERS0_I11btMatrix3x3Eb(%class.btMultiBody* %263, float 0.000000e+00, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17) %m_scratch_r271, %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17) %m_scratch_v272, %class.btAlignedObjectArray.46* nonnull align 4 dereferenceable(17) %m_scratch_m273, i1 zeroext false)
  %call274 = call %class.btAlignedObjectArray.38* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.38* %delta_qd) #10
  %call275 = call %class.btAlignedObjectArray.38* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.38* %delta_q) #10
  %call276 = call %class.btAlignedObjectArray.38* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.38* %scratch_r2) #10
  br label %if.end277

if.end277:                                        ; preds = %for.end270, %if.then103
  br label %if.end278

if.end278:                                        ; preds = %if.end277, %for.end89
  br label %for.inc279

for.inc279:                                       ; preds = %if.end278
  %264 = load i32, i32* %i62, align 4
  %inc280 = add nsw i32 %264, 1
  store i32 %inc280, i32* %i62, align 4
  br label %for.cond63

for.end281:                                       ; preds = %for.cond63
  %call282 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile60) #10
  %265 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to void (%class.btMultiBodyDynamicsWorld*)***
  %vtable283 = load void (%class.btMultiBodyDynamicsWorld*)**, void (%class.btMultiBodyDynamicsWorld*)*** %265, align 4
  %vfn284 = getelementptr inbounds void (%class.btMultiBodyDynamicsWorld*)*, void (%class.btMultiBodyDynamicsWorld*)** %vtable283, i64 57
  %266 = load void (%class.btMultiBodyDynamicsWorld*)*, void (%class.btMultiBodyDynamicsWorld*)** %vfn284, align 4
  call void %266(%class.btMultiBodyDynamicsWorld* %this1)
  %m_solverMultiBodyIslandCallback285 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 5
  %267 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %m_solverMultiBodyIslandCallback285, align 4
  call void @_ZN36MultiBodyInplaceSolverIslandCallback18processConstraintsEv(%struct.MultiBodyInplaceSolverIslandCallback* %267)
  %268 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_constraintSolver286 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %268, i32 0, i32 3
  %269 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver286, align 4
  %270 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %solverInfo.addr, align 4
  %271 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %271, i32 0, i32 5
  %272 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4
  %273 = bitcast %class.btConstraintSolver* %269 to void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)***
  %vtable287 = load void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)**, void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*** %273, align 4
  %vfn288 = getelementptr inbounds void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*, void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)** %vtable287, i64 4
  %274 = load void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*, void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)** %vfn288, align 4
  call void %274(%class.btConstraintSolver* %269, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %270, %class.btIDebugDraw* %272)
  %call290 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile289, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.3, i32 0, i32 0))
  store i32 0, i32* %i291, align 4
  br label %for.cond292

for.cond292:                                      ; preds = %for.inc352, %for.end281
  %275 = load i32, i32* %i291, align 4
  %m_multiBodies293 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call294 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %m_multiBodies293)
  %cmp295 = icmp slt i32 %275, %call294
  br i1 %cmp295, label %for.body296, label %for.end354

for.body296:                                      ; preds = %for.cond292
  %m_multiBodies298 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %276 = load i32, i32* %i291, align 4
  %call299 = call nonnull align 4 dereferenceable(4) %class.btMultiBody** @_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi(%class.btAlignedObjectArray.27* %m_multiBodies298, i32 %276)
  %277 = load %class.btMultiBody*, %class.btMultiBody** %call299, align 4
  store %class.btMultiBody* %277, %class.btMultiBody** %bod297, align 4
  store i8 0, i8* %isSleeping300, align 1
  %278 = load %class.btMultiBody*, %class.btMultiBody** %bod297, align 4
  %call301 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %278)
  %tobool302 = icmp ne %class.btMultiBodyLinkCollider* %call301, null
  br i1 %tobool302, label %land.lhs.true303, label %if.end308

land.lhs.true303:                                 ; preds = %for.body296
  %279 = load %class.btMultiBody*, %class.btMultiBody** %bod297, align 4
  %call304 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %279)
  %280 = bitcast %class.btMultiBodyLinkCollider* %call304 to %class.btCollisionObject*
  %call305 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %280)
  %cmp306 = icmp eq i32 %call305, 2
  br i1 %cmp306, label %if.then307, label %if.end308

if.then307:                                       ; preds = %land.lhs.true303
  store i8 1, i8* %isSleeping300, align 1
  br label %if.end308

if.end308:                                        ; preds = %if.then307, %land.lhs.true303, %for.body296
  store i32 0, i32* %b309, align 4
  br label %for.cond310

for.cond310:                                      ; preds = %for.inc324, %if.end308
  %281 = load i32, i32* %b309, align 4
  %282 = load %class.btMultiBody*, %class.btMultiBody** %bod297, align 4
  %call311 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %282)
  %cmp312 = icmp slt i32 %281, %call311
  br i1 %cmp312, label %for.body313, label %for.end326

for.body313:                                      ; preds = %for.cond310
  %283 = load %class.btMultiBody*, %class.btMultiBody** %bod297, align 4
  %284 = load i32, i32* %b309, align 4
  %call314 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %283, i32 %284)
  %m_collider315 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call314, i32 0, i32 19
  %285 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider315, align 4
  %tobool316 = icmp ne %class.btMultiBodyLinkCollider* %285, null
  br i1 %tobool316, label %land.lhs.true317, label %if.end323

land.lhs.true317:                                 ; preds = %for.body313
  %286 = load %class.btMultiBody*, %class.btMultiBody** %bod297, align 4
  %287 = load i32, i32* %b309, align 4
  %call318 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %286, i32 %287)
  %m_collider319 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call318, i32 0, i32 19
  %288 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider319, align 4
  %289 = bitcast %class.btMultiBodyLinkCollider* %288 to %class.btCollisionObject*
  %call320 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %289)
  %cmp321 = icmp eq i32 %call320, 2
  br i1 %cmp321, label %if.then322, label %if.end323

if.then322:                                       ; preds = %land.lhs.true317
  store i8 1, i8* %isSleeping300, align 1
  br label %if.end323

if.end323:                                        ; preds = %if.then322, %land.lhs.true317, %for.body313
  br label %for.inc324

for.inc324:                                       ; preds = %if.end323
  %290 = load i32, i32* %b309, align 4
  %inc325 = add nsw i32 %290, 1
  store i32 %inc325, i32* %b309, align 4
  br label %for.cond310

for.end326:                                       ; preds = %for.cond310
  %291 = load i8, i8* %isSleeping300, align 1
  %tobool327 = trunc i8 %291 to i1
  br i1 %tobool327, label %if.end351, label %if.then328

if.then328:                                       ; preds = %for.end326
  %m_scratch_r329 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 10
  %292 = load %class.btMultiBody*, %class.btMultiBody** %bod297, align 4
  %call330 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %292)
  %add331 = add nsw i32 %call330, 1
  store float 0.000000e+00, float* %ref.tmp332, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.38* %m_scratch_r329, i32 %add331, float* nonnull align 4 dereferenceable(4) %ref.tmp332)
  %m_scratch_v333 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 11
  %293 = load %class.btMultiBody*, %class.btMultiBody** %bod297, align 4
  %call334 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %293)
  %add335 = add nsw i32 %call334, 1
  %call337 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp336)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.42* %m_scratch_v333, i32 %add335, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp336)
  %m_scratch_m338 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 12
  %294 = load %class.btMultiBody*, %class.btMultiBody** %bod297, align 4
  %call339 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %294)
  %add340 = add nsw i32 %call339, 1
  %call342 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %ref.tmp341)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E6resizeEiRKS0_(%class.btAlignedObjectArray.46* %m_scratch_m338, i32 %add340, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp341)
  %295 = load %class.btMultiBody*, %class.btMultiBody** %bod297, align 4
  %call343 = call zeroext i1 @_ZNK11btMultiBody21isUsingRK4IntegrationEv(%class.btMultiBody* %295)
  br i1 %call343, label %if.end350, label %if.then344

if.then344:                                       ; preds = %if.then328
  store i8 1, i8* %isConstraintPass, align 1
  %296 = load %class.btMultiBody*, %class.btMultiBody** %bod297, align 4
  %297 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %solverInfo.addr, align 4
  %298 = bitcast %struct.btContactSolverInfo* %297 to %struct.btContactSolverInfoData*
  %m_timeStep345 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %298, i32 0, i32 3
  %299 = load float, float* %m_timeStep345, align 4
  %m_scratch_r346 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 10
  %m_scratch_v347 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 11
  %m_scratch_m348 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 12
  %300 = load i8, i8* %isConstraintPass, align 1
  %tobool349 = trunc i8 %300 to i1
  call void @_ZN11btMultiBody52computeAccelerationsArticulatedBodyAlgorithmMultiDofEfR20btAlignedObjectArrayIfERS0_I9btVector3ERS0_I11btMatrix3x3Eb(%class.btMultiBody* %296, float %299, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17) %m_scratch_r346, %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17) %m_scratch_v347, %class.btAlignedObjectArray.46* nonnull align 4 dereferenceable(17) %m_scratch_m348, i1 zeroext %tobool349)
  br label %if.end350

if.end350:                                        ; preds = %if.then344, %if.then328
  br label %if.end351

if.end351:                                        ; preds = %if.end350, %for.end326
  br label %for.inc352

for.inc352:                                       ; preds = %if.end351
  %301 = load i32, i32* %i291, align 4
  %inc353 = add nsw i32 %301, 1
  store i32 %inc353, i32* %i291, align 4
  br label %for.cond292

for.end354:                                       ; preds = %for.cond292
  %call355 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile289) #10
  store i32 0, i32* %i356, align 4
  br label %for.cond357

for.cond357:                                      ; preds = %for.inc365, %for.end354
  %302 = load i32, i32* %i356, align 4
  %m_multiBodies358 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call359 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %m_multiBodies358)
  %cmp360 = icmp slt i32 %302, %call359
  br i1 %cmp360, label %for.body361, label %for.end367

for.body361:                                      ; preds = %for.cond357
  %m_multiBodies363 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %303 = load i32, i32* %i356, align 4
  %call364 = call nonnull align 4 dereferenceable(4) %class.btMultiBody** @_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi(%class.btAlignedObjectArray.27* %m_multiBodies363, i32 %303)
  %304 = load %class.btMultiBody*, %class.btMultiBody** %call364, align 4
  store %class.btMultiBody* %304, %class.btMultiBody** %bod362, align 4
  %305 = load %class.btMultiBody*, %class.btMultiBody** %bod362, align 4
  call void @_ZN11btMultiBody24processDeltaVeeMultiDof2Ev(%class.btMultiBody* %305)
  br label %for.inc365

for.inc365:                                       ; preds = %for.body361
  %306 = load i32, i32* %i356, align 4
  %inc366 = add nsw i32 %306, 1
  store i32 %inc366, i32* %i356, align 4
  br label %for.cond357

for.end367:                                       ; preds = %for.cond357
  %call368 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.4* %this, i32 %newsize, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btTypedConstraint**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btTypedConstraint** %fillData, %class.btTypedConstraint*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %5 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %14 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %14, i32 %15
  %16 = bitcast %class.btTypedConstraint** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btTypedConstraint**
  %18 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %fillData.addr, align 4
  %19 = load %class.btTypedConstraint*, %class.btTypedConstraint** %18, align 4
  store %class.btTypedConstraint* %19, %class.btTypedConstraint** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9quickSortI34btSortConstraintOnIslandPredicate2EEvRKT_(%class.btAlignedObjectArray.4* %this, %class.btSortConstraintOnIslandPredicate2* nonnull align 1 dereferenceable(1) %CompareFunc) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %CompareFunc.addr = alloca %class.btSortConstraintOnIslandPredicate2*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store %class.btSortConstraintOnIslandPredicate2* %CompareFunc, %class.btSortConstraintOnIslandPredicate2** %CompareFunc.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.btSortConstraintOnIslandPredicate2*, %class.btSortConstraintOnIslandPredicate2** %CompareFunc.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI34btSortConstraintOnIslandPredicate2EEvRKT_ii(%class.btAlignedObjectArray.4* %this1, %class.btSortConstraintOnIslandPredicate2* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.51* %this, i32 %newsize, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btMultiBodyConstraint**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btMultiBodyConstraint** %fillData, %class.btMultiBodyConstraint*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  %5 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE7reserveEi(%class.btAlignedObjectArray.51* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  %14 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %14, i32 %15
  %16 = bitcast %class.btMultiBodyConstraint** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btMultiBodyConstraint**
  %18 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %fillData.addr, align 4
  %19 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %18, align 4
  store %class.btMultiBodyConstraint* %19, %class.btMultiBodyConstraint** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9quickSortI42btSortMultiBodyConstraintOnIslandPredicateEEvRKT_(%class.btAlignedObjectArray.51* %this, %class.btSortMultiBodyConstraintOnIslandPredicate* nonnull align 1 dereferenceable(1) %CompareFunc) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  %CompareFunc.addr = alloca %class.btSortMultiBodyConstraintOnIslandPredicate*, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  store %class.btSortMultiBodyConstraintOnIslandPredicate* %CompareFunc, %class.btSortMultiBodyConstraintOnIslandPredicate** %CompareFunc.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.btSortMultiBodyConstraintOnIslandPredicate*, %class.btSortMultiBodyConstraintOnIslandPredicate** %CompareFunc.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE17quickSortInternalI42btSortMultiBodyConstraintOnIslandPredicateEEvRKT_ii(%class.btAlignedObjectArray.51* %this1, %class.btSortMultiBodyConstraintOnIslandPredicate* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN36MultiBodyInplaceSolverIslandCallback5setupEP19btContactSolverInfoPP17btTypedConstraintiPP21btMultiBodyConstraintiP12btIDebugDraw(%struct.MultiBodyInplaceSolverIslandCallback* %this, %struct.btContactSolverInfo* %solverInfo, %class.btTypedConstraint** %sortedConstraints, i32 %numConstraints, %class.btMultiBodyConstraint** %sortedMultiBodyConstraints, i32 %numMultiBodyConstraints, %class.btIDebugDraw* %debugDrawer) #2 comdat {
entry:
  %this.addr = alloca %struct.MultiBodyInplaceSolverIslandCallback*, align 4
  %solverInfo.addr = alloca %struct.btContactSolverInfo*, align 4
  %sortedConstraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %sortedMultiBodyConstraints.addr = alloca %class.btMultiBodyConstraint**, align 4
  %numMultiBodyConstraints.addr = alloca i32, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %ref.tmp = alloca %class.btCollisionObject*, align 4
  %ref.tmp2 = alloca %class.btPersistentManifold*, align 4
  %ref.tmp3 = alloca %class.btTypedConstraint*, align 4
  %ref.tmp4 = alloca %class.btMultiBodyConstraint*, align 4
  store %struct.MultiBodyInplaceSolverIslandCallback* %this, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4
  store %struct.btContactSolverInfo* %solverInfo, %struct.btContactSolverInfo** %solverInfo.addr, align 4
  store %class.btTypedConstraint** %sortedConstraints, %class.btTypedConstraint*** %sortedConstraints.addr, align 4
  store i32 %numConstraints, i32* %numConstraints.addr, align 4
  store %class.btMultiBodyConstraint** %sortedMultiBodyConstraints, %class.btMultiBodyConstraint*** %sortedMultiBodyConstraints.addr, align 4
  store i32 %numMultiBodyConstraints, i32* %numMultiBodyConstraints.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %this1 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4
  %0 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %solverInfo.addr, align 4
  %m_solverInfo = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 1
  store %struct.btContactSolverInfo* %0, %struct.btContactSolverInfo** %m_solverInfo, align 4
  %1 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %sortedMultiBodyConstraints.addr, align 4
  %m_multiBodySortedConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 3
  store %class.btMultiBodyConstraint** %1, %class.btMultiBodyConstraint*** %m_multiBodySortedConstraints, align 4
  %2 = load i32, i32* %numMultiBodyConstraints.addr, align 4
  %m_numMultiBodyConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 4
  store i32 %2, i32* %m_numMultiBodyConstraints, align 4
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %sortedConstraints.addr, align 4
  %m_sortedConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 5
  store %class.btTypedConstraint** %3, %class.btTypedConstraint*** %m_sortedConstraints, align 4
  %4 = load i32, i32* %numConstraints.addr, align 4
  %m_numConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 6
  store i32 %4, i32* %m_numConstraints, align 4
  %5 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %m_debugDrawer = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 7
  store %class.btIDebugDraw* %5, %class.btIDebugDraw** %m_debugDrawer, align 4
  %m_bodies = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 9
  store %class.btCollisionObject* null, %class.btCollisionObject** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_(%class.btAlignedObjectArray* %m_bodies, i32 0, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %ref.tmp)
  %m_manifolds = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 10
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %ref.tmp2, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.13* %m_manifolds, i32 0, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %ref.tmp2)
  %m_constraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 11
  store %class.btTypedConstraint* null, %class.btTypedConstraint** %ref.tmp3, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.4* %m_constraints, i32 0, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %ref.tmp3)
  %m_multiBodyConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 12
  store %class.btMultiBodyConstraint* null, %class.btMultiBodyConstraint** %ref.tmp4, align 4
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.51* %m_multiBodyConstraints, i32 0, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK16btCollisionWorld22getNumCollisionObjectsEv(%class.btCollisionWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_collisionObjects)
  ret i32 %call
}

declare void @_ZN25btSimulationIslandManager22buildAndProcessIslandsEP12btDispatcherP16btCollisionWorldPNS_14IslandCallbackE(%class.btSimulationIslandManager*, %class.btDispatcher*, %class.btCollisionWorld*, %"struct.btSimulationIslandManager::IslandCallback"*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.38* %this, i32 %newsize, float* nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca float*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store float* %fillData, float** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.38* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  %5 = load float*, float** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.38* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  %14 = load float*, float** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds float, float* %14, i32 %15
  %16 = bitcast float* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to float*
  %18 = load float*, float** %fillData.addr, align 4
  %19 = load float, float* %18, align 4
  store float %19, float* %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.42* %this, i32 %newsize, %class.btVector3* nonnull align 4 dereferenceable(16) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.42*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btVector3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.42* %this, %class.btAlignedObjectArray.42** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btVector3* %fillData, %class.btVector3** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.42*, %class.btAlignedObjectArray.42** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.42* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 4
  %5 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end15

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.42* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 4
  %14 = load %class.btVector3*, %class.btVector3** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btVector3, %class.btVector3* %14, i32 %15
  %16 = bitcast %class.btVector3* %arrayidx10 to i8*
  %call11 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %16)
  %17 = bitcast i8* %call11 to %class.btVector3*
  %18 = load %class.btVector3*, %class.btVector3** %fillData.addr, align 4
  %19 = bitcast %class.btVector3* %17 to i8*
  %20 = bitcast %class.btVector3* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc13 = add nsw i32 %21, 1
  store i32 %inc13, i32* %i5, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  br label %if.end15

if.end15:                                         ; preds = %for.end14, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E6resizeEiRKS0_(%class.btAlignedObjectArray.46* %this, i32 %newsize, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.46*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btMatrix3x3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.46* %this, %class.btAlignedObjectArray.46** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btMatrix3x3* %fillData, %class.btMatrix3x3** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.46*, %class.btAlignedObjectArray.46** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.46* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end15

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E7reserveEi(%class.btAlignedObjectArray.46* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %14, i32 %15
  %16 = bitcast %class.btMatrix3x3* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btMatrix3x3*
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %fillData.addr, align 4
  %call11 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %17, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %18)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %19 = load i32, i32* %i5, align 4
  %inc13 = add nsw i32 %19, 1
  store i32 %inc13, i32* %i5, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  br label %if.end15

if.end15:                                         ; preds = %for.end14, %for.end
  %20 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 2
  store i32 %20, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK11btMultiBody21isUsingRK4IntegrationEv(%class.btMultiBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_useRK4 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 41
  %0 = load i8, i8* %m_useRK4, align 4
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

declare void @_ZN11btMultiBody52computeAccelerationsArticulatedBodyAlgorithmMultiDofEfR20btAlignedObjectArrayIfERS0_I9btVector3ERS0_I11btMatrix3x3Eb(%class.btMultiBody*, float, %class.btAlignedObjectArray.38* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray.46* nonnull align 4 dereferenceable(17), i1 zeroext) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_dofCount = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 39
  %0 = load i32, i32* %m_dofCount, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK11btMultiBody13getNumPosVarsEv(%class.btMultiBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_posVarCnt = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 40
  %0 = load i32, i32* %m_posVarCnt, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.38* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody17getWorldToBaseRotEv(%class.btMultiBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_baseQuat = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 4
  ret %class.btQuaternion* %m_baseQuat
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getBasePosEv(%class.btMultiBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_basePos = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 3
  ret %class.btVector3* %m_basePos
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_realBuf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 14
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.38* %m_realBuf, i32 0)
  ret float* %call
}

; Function Attrs: noinline nounwind optnone
define internal void @"_ZZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfoEN3$_0clEPKfPfii"(%struct.anon.83* %this, float* %pSrc, float* %pDst, i32 %start, i32 %size) #1 {
entry:
  %this.addr = alloca %struct.anon.83*, align 4
  %pSrc.addr = alloca float*, align 4
  %pDst.addr = alloca float*, align 4
  %start.addr = alloca i32, align 4
  %size.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.anon.83* %this, %struct.anon.83** %this.addr, align 4
  store float* %pSrc, float** %pSrc.addr, align 4
  store float* %pDst, float** %pDst.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %struct.anon.83*, %struct.anon.83** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %size.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load float*, float** %pSrc.addr, align 4
  %3 = load i32, i32* %start.addr, align 4
  %4 = load i32, i32* %i, align 4
  %add = add nsw i32 %3, %4
  %arrayidx = getelementptr inbounds float, float* %2, i32 %add
  %5 = load float, float* %arrayidx, align 4
  %6 = load float*, float** %pDst.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds float, float* %6, i32 %7
  store float %5, float* %arrayidx2, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @"_ZZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfoEN3$_1clEv"(%struct.anon* %this) #1 {
entry:
  %this.addr = alloca %struct.anon*, align 4
  %dof = alloca i32, align 4
  store %struct.anon* %this, %struct.anon** %this.addr, align 4
  %this1 = load %struct.anon*, %struct.anon** %this.addr, align 4
  store i32 0, i32* %dof, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %dof, align 4
  %bod = getelementptr inbounds %struct.anon, %struct.anon* %this1, i32 0, i32 0
  %1 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call = call i32 @_ZNK11btMultiBody13getNumPosVarsEv(%class.btMultiBody* %1)
  %add = add nsw i32 %call, 7
  %cmp = icmp slt i32 %0, %add
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %scratch_q0 = getelementptr inbounds %struct.anon, %struct.anon* %this1, i32 0, i32 2
  %2 = load float*, float** %scratch_q0, align 4
  %3 = load i32, i32* %dof, align 4
  %arrayidx = getelementptr inbounds float, float* %2, i32 %3
  %4 = load float, float* %arrayidx, align 4
  %scratch_qx = getelementptr inbounds %struct.anon, %struct.anon* %this1, i32 0, i32 1
  %5 = load float*, float** %scratch_qx, align 4
  %6 = load i32, i32* %dof, align 4
  %arrayidx2 = getelementptr inbounds float, float* %5, i32 %6
  store float %4, float* %arrayidx2, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %dof, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %dof, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

declare void @_ZN11btMultiBody21stepPositionsMultiDofEfPfS0_(%class.btMultiBody*, float, float*, float*) #3

; Function Attrs: noinline nounwind optnone
define internal void @"_ZZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfoEN3$_2clEfPKfS4_Pfi"(%struct.anon.81* %this, float %dt, float* %pDer, float* %pCurVal, float* %pVal, i32 %size) #1 {
entry:
  %this.addr = alloca %struct.anon.81*, align 4
  %dt.addr = alloca float, align 4
  %pDer.addr = alloca float*, align 4
  %pCurVal.addr = alloca float*, align 4
  %pVal.addr = alloca float*, align 4
  %size.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.anon.81* %this, %struct.anon.81** %this.addr, align 4
  store float %dt, float* %dt.addr, align 4
  store float* %pDer, float** %pDer.addr, align 4
  store float* %pCurVal, float** %pCurVal.addr, align 4
  store float* %pVal, float** %pVal.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %struct.anon.81*, %struct.anon.81** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %size.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load float*, float** %pCurVal.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %2, i32 %3
  %4 = load float, float* %arrayidx, align 4
  %5 = load float, float* %dt.addr, align 4
  %6 = load float*, float** %pDer.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds float, float* %6, i32 %7
  %8 = load float, float* %arrayidx2, align 4
  %mul = fmul float %5, %8
  %add = fadd float %4, %mul
  %9 = load float*, float** %pVal.addr, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds float, float* %9, i32 %10
  store float %add, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define internal void @"_ZZN24btMultiBodyDynamicsWorld16solveConstraintsER19btContactSolverInfoEN3$_3clEP11btMultiBodyPKf"(%struct.anon.82* %this, %class.btMultiBody* %pBody, float* %pData) #2 {
entry:
  %this.addr = alloca %struct.anon.82*, align 4
  %pBody.addr = alloca %class.btMultiBody*, align 4
  %pData.addr = alloca float*, align 4
  %pVel = alloca float*, align 4
  %i = alloca i32, align 4
  store %struct.anon.82* %this, %struct.anon.82** %this.addr, align 4
  store %class.btMultiBody* %pBody, %class.btMultiBody** %pBody.addr, align 4
  store float* %pData, float** %pData.addr, align 4
  %this1 = load %struct.anon.82*, %struct.anon.82** %this.addr, align 4
  %0 = load %class.btMultiBody*, %class.btMultiBody** %pBody.addr, align 4
  %call = call float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %0)
  store float* %call, float** %pVel, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load %class.btMultiBody*, %class.btMultiBody** %pBody.addr, align 4
  %call2 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %2)
  %add = add nsw i32 %call2, 6
  %cmp = icmp slt i32 %1, %add
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load float*, float** %pData.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  %5 = load float, float* %arrayidx, align 4
  %6 = load float*, float** %pVel, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds float, float* %6, i32 %7
  store float %5, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMultiBody21applyDeltaVeeMultiDofEPKff(%class.btMultiBody* %this, float* %delta_vee, float %multiplier) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %delta_vee.addr = alloca float*, align 4
  %multiplier.addr = alloca float, align 4
  %dof = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  store float* %delta_vee, float** %delta_vee.addr, align 4
  store float %multiplier, float* %multiplier.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  store i32 0, i32* %dof, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %dof, align 4
  %call = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %this1)
  %add = add nsw i32 6, %call
  %cmp = icmp slt i32 %0, %add
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load float*, float** %delta_vee.addr, align 4
  %2 = load i32, i32* %dof, align 4
  %arrayidx = getelementptr inbounds float, float* %1, i32 %2
  %3 = load float, float* %arrayidx, align 4
  %4 = load float, float* %multiplier.addr, align 4
  %mul = fmul float %3, %4
  %m_realBuf = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 14
  %5 = load i32, i32* %dof, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.38* %m_realBuf, i32 %5)
  %6 = load float, float* %call2, align 4
  %add3 = fadd float %6, %mul
  store float %add3, float* %call2, align 4
  %m_realBuf4 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 14
  %7 = load i32, i32* %dof, align 4
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.38* %m_realBuf4, i32 %7)
  %m_maxCoordinateVelocity = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 35
  %8 = load float, float* %m_maxCoordinateVelocity, align 4
  %fneg = fneg float %8
  store float %fneg, float* %ref.tmp, align 4
  %m_maxCoordinateVelocity6 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 35
  call void @_Z7btClampIfEvRT_RKS0_S3_(float* nonnull align 4 dereferenceable(4) %call5, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %m_maxCoordinateVelocity6)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %dof, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %dof, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMultiBody13setPosUpdatedEb(%class.btMultiBody* %this, i1 zeroext %updated) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %updated.addr = alloca i8, align 1
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %frombool = zext i1 %updated to i8
  store i8 %frombool, i8* %updated.addr, align 1
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load i8, i8* %updated.addr, align 1
  %tobool = trunc i8 %0 to i1
  %__posUpdated = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 37
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %__posUpdated, align 1
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN15btMultibodyLink19updateCacheMultiDofEPf(%struct.btMultibodyLink* %this, float* %pq) #2 comdat {
entry:
  %this.addr = alloca %struct.btMultibodyLink*, align 4
  %pq.addr = alloca float*, align 4
  %pJointPos = alloca float*, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  %ref.tmp2 = alloca %class.btQuaternion, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btQuaternion, align 4
  %ref.tmp22 = alloca %class.btQuaternion, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp34 = alloca %class.btVector3, align 4
  %ref.tmp39 = alloca %class.btQuaternion, align 4
  %ref.tmp40 = alloca %class.btQuaternion, align 4
  %ref.tmp42 = alloca float, align 4
  %ref.tmp48 = alloca %class.btVector3, align 4
  %ref.tmp49 = alloca %class.btVector3, align 4
  %ref.tmp50 = alloca %class.btQuaternion, align 4
  %ref.tmp52 = alloca float, align 4
  %ref.tmp56 = alloca %class.btVector3, align 4
  %ref.tmp57 = alloca %class.btVector3, align 4
  %ref.tmp60 = alloca %class.btVector3, align 4
  %ref.tmp63 = alloca %class.btVector3, align 4
  %ref.tmp70 = alloca %class.btVector3, align 4
  %ref.tmp72 = alloca %class.btVector3, align 4
  store %struct.btMultibodyLink* %this, %struct.btMultibodyLink** %this.addr, align 4
  store float* %pq, float** %pq.addr, align 4
  %this1 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %this.addr, align 4
  %0 = load float*, float** %pq.addr, align 4
  %tobool = icmp ne float* %0, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load float*, float** %pq.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_jointPos = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 17
  %arrayidx = getelementptr inbounds [7 x float], [7 x float]* %m_jointPos, i32 0, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float* [ %1, %cond.true ], [ %arrayidx, %cond.false ]
  store float* %cond, float** %pJointPos, align 4
  %m_jointType = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 23
  %2 = load i32, i32* %m_jointType, align 4
  switch i32 %2, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb9
    i32 2, label %sw.bb20
    i32 3, label %sw.bb38
    i32 4, label %sw.bb67
  ]

sw.bb:                                            ; preds = %cond.end
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btMultibodyLink10getAxisTopEi(%struct.btMultibodyLink* %this1, i32 0)
  %3 = load float*, float** %pJointPos, align 4
  %arrayidx4 = getelementptr inbounds float, float* %3, i32 0
  %4 = load float, float* %arrayidx4, align 4
  %fneg = fneg float %4
  store float %fneg, float* %ref.tmp3, align 4
  %call5 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %call, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %m_zeroRotParentToThis = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 3
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_zeroRotParentToThis)
  %m_cachedRotParentToThis = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 11
  %5 = bitcast %class.btQuaternion* %m_cachedRotParentToThis to i8*
  %6 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  %m_dVector = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 4
  %m_cachedRotParentToThis8 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 11
  %m_eVector = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 5
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp7, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_cachedRotParentToThis8, %class.btVector3* nonnull align 4 dereferenceable(16) %m_eVector)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %m_dVector, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp7)
  %m_cachedRVector = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 12
  %7 = bitcast %class.btVector3* %m_cachedRVector to i8*
  %8 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  br label %sw.epilog

sw.bb9:                                           ; preds = %cond.end
  %m_dVector12 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 4
  %m_cachedRotParentToThis14 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 11
  %m_eVector15 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 5
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp13, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_cachedRotParentToThis14, %class.btVector3* nonnull align 4 dereferenceable(16) %m_eVector15)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %m_dVector12, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp13)
  %9 = load float*, float** %pJointPos, align 4
  %arrayidx17 = getelementptr inbounds float, float* %9, i32 0
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btMultibodyLink13getAxisBottomEi(%struct.btMultibodyLink* %this1, i32 0)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp16, float* nonnull align 4 dereferenceable(4) %arrayidx17, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp16)
  %m_cachedRVector19 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 12
  %10 = bitcast %class.btVector3* %m_cachedRVector19 to i8*
  %11 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  br label %sw.epilog

sw.bb20:                                          ; preds = %cond.end
  %12 = load float*, float** %pJointPos, align 4
  %arrayidx23 = getelementptr inbounds float, float* %12, i32 0
  %13 = load float*, float** %pJointPos, align 4
  %arrayidx24 = getelementptr inbounds float, float* %13, i32 1
  %14 = load float*, float** %pJointPos, align 4
  %arrayidx25 = getelementptr inbounds float, float* %14, i32 2
  %15 = load float*, float** %pJointPos, align 4
  %arrayidx27 = getelementptr inbounds float, float* %15, i32 3
  %16 = load float, float* %arrayidx27, align 4
  %fneg28 = fneg float %16
  store float %fneg28, float* %ref.tmp26, align 4
  %call29 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %ref.tmp22, float* nonnull align 4 dereferenceable(4) %arrayidx23, float* nonnull align 4 dereferenceable(4) %arrayidx24, float* nonnull align 4 dereferenceable(4) %arrayidx25, float* nonnull align 4 dereferenceable(4) %ref.tmp26)
  %m_zeroRotParentToThis30 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 3
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %ref.tmp21, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp22, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_zeroRotParentToThis30)
  %m_cachedRotParentToThis31 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 11
  %17 = bitcast %class.btQuaternion* %m_cachedRotParentToThis31 to i8*
  %18 = bitcast %class.btQuaternion* %ref.tmp21 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 16, i1 false)
  %m_dVector33 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 4
  %m_cachedRotParentToThis35 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 11
  %m_eVector36 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 5
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp34, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_cachedRotParentToThis35, %class.btVector3* nonnull align 4 dereferenceable(16) %m_eVector36)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp32, %class.btVector3* nonnull align 4 dereferenceable(16) %m_dVector33, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp34)
  %m_cachedRVector37 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 12
  %19 = bitcast %class.btVector3* %m_cachedRVector37 to i8*
  %20 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  br label %sw.epilog

sw.bb38:                                          ; preds = %cond.end
  %call41 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btMultibodyLink10getAxisTopEi(%struct.btMultibodyLink* %this1, i32 0)
  %21 = load float*, float** %pJointPos, align 4
  %arrayidx43 = getelementptr inbounds float, float* %21, i32 0
  %22 = load float, float* %arrayidx43, align 4
  %fneg44 = fneg float %22
  store float %fneg44, float* %ref.tmp42, align 4
  %call45 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %ref.tmp40, %class.btVector3* nonnull align 4 dereferenceable(16) %call41, float* nonnull align 4 dereferenceable(4) %ref.tmp42)
  %m_zeroRotParentToThis46 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 3
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %ref.tmp39, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp40, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_zeroRotParentToThis46)
  %m_cachedRotParentToThis47 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 11
  %23 = bitcast %class.btQuaternion* %m_cachedRotParentToThis47 to i8*
  %24 = bitcast %class.btQuaternion* %ref.tmp39 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false)
  %call51 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btMultibodyLink10getAxisTopEi(%struct.btMultibodyLink* %this1, i32 0)
  %25 = load float*, float** %pJointPos, align 4
  %arrayidx53 = getelementptr inbounds float, float* %25, i32 0
  %26 = load float, float* %arrayidx53, align 4
  %fneg54 = fneg float %26
  store float %fneg54, float* %ref.tmp52, align 4
  %call55 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %ref.tmp50, %class.btVector3* nonnull align 4 dereferenceable(16) %call51, float* nonnull align 4 dereferenceable(4) %ref.tmp52)
  %27 = load float*, float** %pJointPos, align 4
  %arrayidx58 = getelementptr inbounds float, float* %27, i32 1
  %call59 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btMultibodyLink13getAxisBottomEi(%struct.btMultibodyLink* %this1, i32 1)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp57, float* nonnull align 4 dereferenceable(4) %arrayidx58, %class.btVector3* nonnull align 4 dereferenceable(16) %call59)
  %28 = load float*, float** %pJointPos, align 4
  %arrayidx61 = getelementptr inbounds float, float* %28, i32 2
  %call62 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btMultibodyLink13getAxisBottomEi(%struct.btMultibodyLink* %this1, i32 2)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp60, float* nonnull align 4 dereferenceable(4) %arrayidx61, %class.btVector3* nonnull align 4 dereferenceable(16) %call62)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp56, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp57, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp60)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp49, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp50, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp56)
  %m_cachedRotParentToThis64 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 11
  %m_eVector65 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 5
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp63, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_cachedRotParentToThis64, %class.btVector3* nonnull align 4 dereferenceable(16) %m_eVector65)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp48, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp49, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp63)
  %m_cachedRVector66 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 12
  %29 = bitcast %class.btVector3* %m_cachedRVector66 to i8*
  %30 = bitcast %class.btVector3* %ref.tmp48 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %29, i8* align 4 %30, i32 16, i1 false)
  br label %sw.epilog

sw.bb67:                                          ; preds = %cond.end
  %m_zeroRotParentToThis68 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 3
  %m_cachedRotParentToThis69 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 11
  %31 = bitcast %class.btQuaternion* %m_cachedRotParentToThis69 to i8*
  %32 = bitcast %class.btQuaternion* %m_zeroRotParentToThis68 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %31, i8* align 4 %32, i32 16, i1 false)
  %m_dVector71 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 4
  %m_cachedRotParentToThis73 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 11
  %m_eVector74 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 5
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp72, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_cachedRotParentToThis73, %class.btVector3* nonnull align 4 dereferenceable(16) %m_eVector74)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp70, %class.btVector3* nonnull align 4 dereferenceable(16) %m_dVector71, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp72)
  %m_cachedRVector75 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 12
  %33 = bitcast %class.btVector3* %m_cachedRVector75 to i8*
  %34 = bitcast %class.btVector3* %ref.tmp70 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 4 %34, i32 16, i1 false)
  br label %sw.epilog

sw.default:                                       ; preds = %cond.end
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb67, %sw.bb38, %sw.bb20, %sw.bb9, %sw.bb
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN36MultiBodyInplaceSolverIslandCallback18processConstraintsEv(%struct.MultiBodyInplaceSolverIslandCallback* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.MultiBodyInplaceSolverIslandCallback*, align 4
  %bodies = alloca %class.btCollisionObject**, align 4
  %manifold = alloca %class.btPersistentManifold**, align 4
  %constraints = alloca %class.btTypedConstraint**, align 4
  %multiBodyConstraints = alloca %class.btMultiBodyConstraint**, align 4
  %ref.tmp = alloca %class.btCollisionObject*, align 4
  %ref.tmp38 = alloca %class.btPersistentManifold*, align 4
  %ref.tmp40 = alloca %class.btTypedConstraint*, align 4
  %ref.tmp42 = alloca %class.btMultiBodyConstraint*, align 4
  store %struct.MultiBodyInplaceSolverIslandCallback* %this, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4
  %this1 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4
  %m_bodies = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 9
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_bodies)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_bodies2 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 9
  %call3 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_bodies2, i32 0)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btCollisionObject** [ %call3, %cond.true ], [ null, %cond.false ]
  store %class.btCollisionObject** %cond, %class.btCollisionObject*** %bodies, align 4
  %m_manifolds = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 10
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %m_manifolds)
  %tobool5 = icmp ne i32 %call4, 0
  br i1 %tobool5, label %cond.true6, label %cond.false9

cond.true6:                                       ; preds = %cond.end
  %m_manifolds7 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 10
  %call8 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.13* %m_manifolds7, i32 0)
  br label %cond.end10

cond.false9:                                      ; preds = %cond.end
  br label %cond.end10

cond.end10:                                       ; preds = %cond.false9, %cond.true6
  %cond11 = phi %class.btPersistentManifold** [ %call8, %cond.true6 ], [ null, %cond.false9 ]
  store %class.btPersistentManifold** %cond11, %class.btPersistentManifold*** %manifold, align 4
  %m_constraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 11
  %call12 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %m_constraints)
  %tobool13 = icmp ne i32 %call12, 0
  br i1 %tobool13, label %cond.true14, label %cond.false17

cond.true14:                                      ; preds = %cond.end10
  %m_constraints15 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 11
  %call16 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.4* %m_constraints15, i32 0)
  br label %cond.end18

cond.false17:                                     ; preds = %cond.end10
  br label %cond.end18

cond.end18:                                       ; preds = %cond.false17, %cond.true14
  %cond19 = phi %class.btTypedConstraint** [ %call16, %cond.true14 ], [ null, %cond.false17 ]
  store %class.btTypedConstraint** %cond19, %class.btTypedConstraint*** %constraints, align 4
  %m_multiBodyConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 12
  %call20 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %m_multiBodyConstraints)
  %tobool21 = icmp ne i32 %call20, 0
  br i1 %tobool21, label %cond.true22, label %cond.false25

cond.true22:                                      ; preds = %cond.end18
  %m_multiBodyConstraints23 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 12
  %call24 = call nonnull align 4 dereferenceable(4) %class.btMultiBodyConstraint** @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEixEi(%class.btAlignedObjectArray.51* %m_multiBodyConstraints23, i32 0)
  br label %cond.end26

cond.false25:                                     ; preds = %cond.end18
  br label %cond.end26

cond.end26:                                       ; preds = %cond.false25, %cond.true22
  %cond27 = phi %class.btMultiBodyConstraint** [ %call24, %cond.true22 ], [ null, %cond.false25 ]
  store %class.btMultiBodyConstraint** %cond27, %class.btMultiBodyConstraint*** %multiBodyConstraints, align 4
  %m_solver = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 2
  %0 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %m_solver, align 4
  %1 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies, align 4
  %m_bodies28 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 9
  %call29 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_bodies28)
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifold, align 4
  %m_manifolds30 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 10
  %call31 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %m_manifolds30)
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints, align 4
  %m_constraints32 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 11
  %call33 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %m_constraints32)
  %4 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %multiBodyConstraints, align 4
  %m_multiBodyConstraints34 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 12
  %call35 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %m_multiBodyConstraints34)
  %m_solverInfo = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 1
  %5 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %m_solverInfo, align 4
  %m_debugDrawer = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 7
  %6 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4
  %m_dispatcher = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 8
  %7 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %8 = bitcast %class.btMultiBodyConstraintSolver* %0 to void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)***
  %vtable = load void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)**, void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*** %8, align 4
  %vfn = getelementptr inbounds void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vtable, i64 13
  %9 = load void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vfn, align 4
  call void %9(%class.btMultiBodyConstraintSolver* %0, %class.btCollisionObject** %1, i32 %call29, %class.btPersistentManifold** %2, i32 %call31, %class.btTypedConstraint** %3, i32 %call33, %class.btMultiBodyConstraint** %4, i32 %call35, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %5, %class.btIDebugDraw* %6, %class.btDispatcher* %7)
  %m_bodies36 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 9
  store %class.btCollisionObject* null, %class.btCollisionObject** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_(%class.btAlignedObjectArray* %m_bodies36, i32 0, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %ref.tmp)
  %m_manifolds37 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 10
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %ref.tmp38, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.13* %m_manifolds37, i32 0, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %ref.tmp38)
  %m_constraints39 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 11
  store %class.btTypedConstraint* null, %class.btTypedConstraint** %ref.tmp40, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.4* %m_constraints39, i32 0, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %ref.tmp40)
  %m_multiBodyConstraints41 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 12
  store %class.btMultiBodyConstraint* null, %class.btMultiBodyConstraint** %ref.tmp42, align 4
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.51* %m_multiBodyConstraints41, i32 0, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %ref.tmp42)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMultiBody24processDeltaVeeMultiDof2Ev(%class.btMultiBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %dof = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_deltaV = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 13
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.38* %m_deltaV, i32 0)
  call void @_ZN11btMultiBody21applyDeltaVeeMultiDofEPKff(%class.btMultiBody* %this1, float* %call, float 1.000000e+00)
  store i32 0, i32* %dof, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %dof, align 4
  %call2 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %this1)
  %add = add nsw i32 6, %call2
  %cmp = icmp slt i32 %0, %add
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_deltaV3 = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 13
  %1 = load i32, i32* %dof, align 4
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.38* %m_deltaV3, i32 %1)
  store float 0.000000e+00, float* %call4, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %2 = load i32, i32* %dof, align 4
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %dof, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN24btMultiBodyDynamicsWorld19integrateTransformsEf(%class.btMultiBodyDynamicsWorld* %this, float %timeStep) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %b = alloca i32, align 4
  %bod = alloca %class.btMultiBody*, align 4
  %isSleeping = alloca i8, align 1
  %b9 = alloca i32, align 4
  %nLinks = alloca i32, align 4
  %pRealBuf = alloca float*, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  %ref.tmp37 = alloca %class.btVector3, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %1 = load float, float* %timeStep.addr, align 4
  call void @_ZN23btDiscreteDynamicsWorld19integrateTransformsEf(%class.btDiscreteDynamicsWorld* %0, float %1)
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.4, i32 0, i32 0))
  store i32 0, i32* %b, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc43, %entry
  %2 = load i32, i32* %b, align 4
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %m_multiBodies)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %for.body, label %for.end45

for.body:                                         ; preds = %for.cond
  %m_multiBodies3 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %3 = load i32, i32* %b, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %class.btMultiBody** @_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi(%class.btAlignedObjectArray.27* %m_multiBodies3, i32 %3)
  %4 = load %class.btMultiBody*, %class.btMultiBody** %call4, align 4
  store %class.btMultiBody* %4, %class.btMultiBody** %bod, align 4
  store i8 0, i8* %isSleeping, align 1
  %5 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call5 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %5)
  %tobool = icmp ne %class.btMultiBodyLinkCollider* %call5, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %6 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call6 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %6)
  %7 = bitcast %class.btMultiBodyLinkCollider* %call6 to %class.btCollisionObject*
  %call7 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %7)
  %cmp8 = icmp eq i32 %call7, 2
  br i1 %cmp8, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i8 1, i8* %isSleeping, align 1
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %for.body
  store i32 0, i32* %b9, align 4
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc, %if.end
  %8 = load i32, i32* %b9, align 4
  %9 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call11 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %9)
  %cmp12 = icmp slt i32 %8, %call11
  br i1 %cmp12, label %for.body13, label %for.end

for.body13:                                       ; preds = %for.cond10
  %10 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %11 = load i32, i32* %b9, align 4
  %call14 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %10, i32 %11)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call14, i32 0, i32 19
  %12 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4
  %tobool15 = icmp ne %class.btMultiBodyLinkCollider* %12, null
  br i1 %tobool15, label %land.lhs.true16, label %if.end22

land.lhs.true16:                                  ; preds = %for.body13
  %13 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %14 = load i32, i32* %b9, align 4
  %call17 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %13, i32 %14)
  %m_collider18 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call17, i32 0, i32 19
  %15 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider18, align 4
  %16 = bitcast %class.btMultiBodyLinkCollider* %15 to %class.btCollisionObject*
  %call19 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %16)
  %cmp20 = icmp eq i32 %call19, 2
  br i1 %cmp20, label %if.then21, label %if.end22

if.then21:                                        ; preds = %land.lhs.true16
  store i8 1, i8* %isSleeping, align 1
  br label %if.end22

if.end22:                                         ; preds = %if.then21, %land.lhs.true16, %for.body13
  br label %for.inc

for.inc:                                          ; preds = %if.end22
  %17 = load i32, i32* %b9, align 4
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %b9, align 4
  br label %for.cond10

for.end:                                          ; preds = %for.cond10
  %18 = load i8, i8* %isSleeping, align 1
  %tobool23 = trunc i8 %18 to i1
  br i1 %tobool23, label %if.else41, label %if.then24

if.then24:                                        ; preds = %for.end
  %19 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call25 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %19)
  store i32 %call25, i32* %nLinks, align 4
  %20 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call26 = call zeroext i1 @_ZNK11btMultiBody12isPosUpdatedEv(%class.btMultiBody* %20)
  br i1 %call26, label %if.else, label %if.then27

if.then27:                                        ; preds = %if.then24
  %21 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %22 = load float, float* %timeStep.addr, align 4
  call void @_ZN11btMultiBody21stepPositionsMultiDofEfPfS0_(%class.btMultiBody* %21, float %22, float* null, float* null)
  br label %if.end33

if.else:                                          ; preds = %if.then24
  %23 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call28 = call float* @_ZNK11btMultiBody17getVelocityVectorEv(%class.btMultiBody* %23)
  store float* %call28, float** %pRealBuf, align 4
  %24 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call29 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %24)
  %add = add nsw i32 6, %call29
  %25 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call30 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %25)
  %26 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call31 = call i32 @_ZNK11btMultiBody10getNumDofsEv(%class.btMultiBody* %26)
  %mul = mul nsw i32 %call30, %call31
  %add32 = add nsw i32 %add, %mul
  %27 = load float*, float** %pRealBuf, align 4
  %add.ptr = getelementptr inbounds float, float* %27, i32 %add32
  store float* %add.ptr, float** %pRealBuf, align 4
  %28 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %29 = load float*, float** %pRealBuf, align 4
  call void @_ZN11btMultiBody21stepPositionsMultiDofEfPfS0_(%class.btMultiBody* %28, float 1.000000e+00, float* null, float* %29)
  %30 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  call void @_ZN11btMultiBody13setPosUpdatedEb(%class.btMultiBody* %30, i1 zeroext false)
  br label %if.end33

if.end33:                                         ; preds = %if.else, %if.then27
  %m_scratch_world_to_local = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 6
  %31 = load i32, i32* %nLinks, align 4
  %add34 = add nsw i32 %31, 1
  %call35 = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE6resizeEiRKS0_(%class.btAlignedObjectArray.77* %m_scratch_world_to_local, i32 %add34, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp)
  %m_scratch_local_origin = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 7
  %32 = load i32, i32* %nLinks, align 4
  %add36 = add nsw i32 %32, 1
  %call38 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp37)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.42* %m_scratch_local_origin, i32 %add36, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp37)
  %33 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %m_scratch_world_to_local39 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 6
  %m_scratch_local_origin40 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 7
  call void @_ZN11btMultiBody36updateCollisionObjectWorldTransformsER20btAlignedObjectArrayI12btQuaternionERS0_I9btVector3E(%class.btMultiBody* %33, %class.btAlignedObjectArray.77* nonnull align 4 dereferenceable(17) %m_scratch_world_to_local39, %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17) %m_scratch_local_origin40)
  br label %if.end42

if.else41:                                        ; preds = %for.end
  %34 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  call void @_ZN11btMultiBody15clearVelocitiesEv(%class.btMultiBody* %34)
  br label %if.end42

if.end42:                                         ; preds = %if.else41, %if.end33
  br label %for.inc43

for.inc43:                                        ; preds = %if.end42
  %35 = load i32, i32* %b, align 4
  %inc44 = add nsw i32 %35, 1
  store i32 %inc44, i32* %b, align 4
  br label %for.cond

for.end45:                                        ; preds = %for.cond
  %call46 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

declare void @_ZN23btDiscreteDynamicsWorld19integrateTransformsEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK11btMultiBody12isPosUpdatedEv(%class.btMultiBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %__posUpdated = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 37
  %0 = load i8, i8* %__posUpdated, align 1
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btQuaternionE6resizeEiRKS0_(%class.btAlignedObjectArray.77* %this, i32 %newsize, %class.btQuaternion* nonnull align 4 dereferenceable(16) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btQuaternion*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btQuaternion* %fillData, %class.btQuaternion** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv(%class.btAlignedObjectArray.77* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  %5 = load %class.btQuaternion*, %class.btQuaternion** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btQuaternion, %class.btQuaternion* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE7reserveEi(%class.btAlignedObjectArray.77* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  %14 = load %class.btQuaternion*, %class.btQuaternion** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btQuaternion, %class.btQuaternion* %14, i32 %15
  %16 = bitcast %class.btQuaternion* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btQuaternion*
  %18 = load %class.btQuaternion*, %class.btQuaternion** %fillData.addr, align 4
  %19 = bitcast %class.btQuaternion* %17 to i8*
  %20 = bitcast %class.btQuaternion* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %21, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  ret %class.btQuaternion* %this1
}

declare void @_ZN11btMultiBody36updateCollisionObjectWorldTransformsER20btAlignedObjectArrayI12btQuaternionERS0_I9btVector3E(%class.btMultiBody*, %class.btAlignedObjectArray.77* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17)) #3

declare void @_ZN11btMultiBody15clearVelocitiesEv(%class.btMultiBody*) #3

; Function Attrs: noinline optnone
define hidden void @_ZN24btMultiBodyDynamicsWorld22addMultiBodyConstraintEP21btMultiBodyConstraint(%class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyConstraint* %constraint) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %constraint.addr = alloca %class.btMultiBodyConstraint*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  store %class.btMultiBodyConstraint* %constraint, %class.btMultiBodyConstraint** %constraint.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %m_multiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9push_backERKS1_(%class.btAlignedObjectArray.51* %m_multiBodyConstraints, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %constraint.addr)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9push_backERKS1_(%class.btAlignedObjectArray.51* %this, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  %_Val.addr = alloca %class.btMultiBodyConstraint**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  store %class.btMultiBodyConstraint** %_Val, %class.btMultiBodyConstraint*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE8capacityEv(%class.btAlignedObjectArray.51* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9allocSizeEi(%class.btAlignedObjectArray.51* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE7reserveEi(%class.btAlignedObjectArray.51* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  %1 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %1, i32 %2
  %3 = bitcast %class.btMultiBodyConstraint** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btMultiBodyConstraint**
  %5 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %_Val.addr, align 4
  %6 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %5, align 4
  store %class.btMultiBodyConstraint* %6, %class.btMultiBodyConstraint** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN24btMultiBodyDynamicsWorld25removeMultiBodyConstraintEP21btMultiBodyConstraint(%class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyConstraint* %constraint) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %constraint.addr = alloca %class.btMultiBodyConstraint*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  store %class.btMultiBodyConstraint* %constraint, %class.btMultiBodyConstraint** %constraint.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %m_multiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE6removeERKS1_(%class.btAlignedObjectArray.51* %m_multiBodyConstraints, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %constraint.addr)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE6removeERKS1_(%class.btAlignedObjectArray.51* %this, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %key) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  %key.addr = alloca %class.btMultiBodyConstraint**, align 4
  %findIndex = alloca i32, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  store %class.btMultiBodyConstraint** %key, %class.btMultiBodyConstraint*** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %0 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %key.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE16findLinearSearchERKS1_(%class.btAlignedObjectArray.51* %this1, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %0)
  store i32 %call, i32* %findIndex, align 4
  %1 = load i32, i32* %findIndex, align 4
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE13removeAtIndexEi(%class.btAlignedObjectArray.51* %this1, i32 %1)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN24btMultiBodyDynamicsWorld28debugDrawMultiBodyConstraintEP21btMultiBodyConstraint(%class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyConstraint* %constraint) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %constraint.addr = alloca %class.btMultiBodyConstraint*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  store %class.btMultiBodyConstraint* %constraint, %class.btMultiBodyConstraint** %constraint.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %0 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %constraint.addr, align 4
  %1 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btCollisionWorld*
  %2 = bitcast %class.btCollisionWorld* %1 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %2, align 4
  %vfn = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable, i64 5
  %3 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn, align 4
  %call = call %class.btIDebugDraw* %3(%class.btCollisionWorld* %1)
  %4 = bitcast %class.btMultiBodyConstraint* %0 to void (%class.btMultiBodyConstraint*, %class.btIDebugDraw*)***
  %vtable2 = load void (%class.btMultiBodyConstraint*, %class.btIDebugDraw*)**, void (%class.btMultiBodyConstraint*, %class.btIDebugDraw*)*** %4, align 4
  %vfn3 = getelementptr inbounds void (%class.btMultiBodyConstraint*, %class.btIDebugDraw*)*, void (%class.btMultiBodyConstraint*, %class.btIDebugDraw*)** %vtable2, i64 8
  %5 = load void (%class.btMultiBodyConstraint*, %class.btIDebugDraw*)*, void (%class.btMultiBodyConstraint*, %class.btIDebugDraw*)** %vfn3, align 4
  call void %5(%class.btMultiBodyConstraint* %0, %class.btIDebugDraw* %call)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN24btMultiBodyDynamicsWorld14debugDrawWorldEv(%class.btMultiBodyDynamicsWorld* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %drawConstraints = alloca i8, align 1
  %mode = alloca i32, align 4
  %__profile13 = alloca %class.CProfileSample, align 1
  %c = alloca i32, align 4
  %constraint = alloca %class.btMultiBodyConstraint*, align 4
  %b = alloca i32, align 4
  %bod = alloca %class.btMultiBody*, align 4
  %ref.tmp = alloca %class.btTransform, align 4
  %m = alloca i32, align 4
  %tr = alloca %class.btTransform*, align 4
  %vec = alloca %class.btVector3, align 4
  %ref.tmp44 = alloca %class.btQuaternion, align 4
  %color = alloca %class.btVector4, align 4
  %ref.tmp46 = alloca float, align 4
  %ref.tmp47 = alloca float, align 4
  %ref.tmp48 = alloca float, align 4
  %ref.tmp49 = alloca float, align 4
  %from = alloca %class.btVector3, align 4
  %ref.tmp51 = alloca %class.btVector3, align 4
  %ref.tmp53 = alloca %class.btVector3, align 4
  %ref.tmp54 = alloca %class.btQuaternion, align 4
  %to = alloca %class.btVector3, align 4
  %ref.tmp57 = alloca %class.btVector3, align 4
  %ref.tmp58 = alloca %class.btQuaternion, align 4
  %vec71 = alloca %class.btVector3, align 4
  %ref.tmp72 = alloca %class.btQuaternion, align 4
  %color76 = alloca %class.btVector4, align 4
  %ref.tmp77 = alloca float, align 4
  %ref.tmp78 = alloca float, align 4
  %ref.tmp79 = alloca float, align 4
  %ref.tmp80 = alloca float, align 4
  %from82 = alloca %class.btVector3, align 4
  %ref.tmp83 = alloca %class.btVector3, align 4
  %ref.tmp85 = alloca %class.btVector3, align 4
  %ref.tmp86 = alloca %class.btQuaternion, align 4
  %to89 = alloca %class.btVector3, align 4
  %ref.tmp91 = alloca %class.btVector3, align 4
  %ref.tmp92 = alloca %class.btQuaternion, align 4
  %vec105 = alloca %class.btVector3, align 4
  %ref.tmp106 = alloca %class.btQuaternion, align 4
  %color111 = alloca %class.btVector4, align 4
  %ref.tmp112 = alloca float, align 4
  %ref.tmp113 = alloca float, align 4
  %ref.tmp114 = alloca float, align 4
  %ref.tmp115 = alloca float, align 4
  %from117 = alloca %class.btVector3, align 4
  %ref.tmp118 = alloca %class.btVector3, align 4
  %ref.tmp120 = alloca %class.btVector3, align 4
  %ref.tmp121 = alloca %class.btQuaternion, align 4
  %to124 = alloca %class.btVector3, align 4
  %ref.tmp126 = alloca %class.btVector3, align 4
  %ref.tmp127 = alloca %class.btQuaternion, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.5, i32 0, i32 0))
  store i8 0, i8* %drawConstraints, align 1
  %0 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btCollisionWorld*
  %1 = bitcast %class.btCollisionWorld* %0 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %1, align 4
  %vfn = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable, i64 5
  %2 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn, align 4
  %call2 = call %class.btIDebugDraw* %2(%class.btCollisionWorld* %0)
  %tobool = icmp ne %class.btIDebugDraw* %call2, null
  br i1 %tobool, label %if.then, label %if.end144

if.then:                                          ; preds = %entry
  %3 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btCollisionWorld*
  %4 = bitcast %class.btCollisionWorld* %3 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable3 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %4, align 4
  %vfn4 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable3, i64 5
  %5 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn4, align 4
  %call5 = call %class.btIDebugDraw* %5(%class.btCollisionWorld* %3)
  %6 = bitcast %class.btIDebugDraw* %call5 to i32 (%class.btIDebugDraw*)***
  %vtable6 = load i32 (%class.btIDebugDraw*)**, i32 (%class.btIDebugDraw*)*** %6, align 4
  %vfn7 = getelementptr inbounds i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vtable6, i64 14
  %7 = load i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vfn7, align 4
  %call8 = call i32 %7(%class.btIDebugDraw* %call5)
  store i32 %call8, i32* %mode, align 4
  %8 = load i32, i32* %mode, align 4
  %and = and i32 %8, 6144
  %tobool9 = icmp ne i32 %and, 0
  br i1 %tobool9, label %if.then10, label %if.end

if.then10:                                        ; preds = %if.then
  store i8 1, i8* %drawConstraints, align 1
  br label %if.end

if.end:                                           ; preds = %if.then10, %if.then
  %9 = load i8, i8* %drawConstraints, align 1
  %tobool11 = trunc i8 %9 to i1
  br i1 %tobool11, label %if.then12, label %if.end143

if.then12:                                        ; preds = %if.end
  %call14 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile13, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.6, i32 0, i32 0))
  store i32 0, i32* %c, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then12
  %10 = load i32, i32* %c, align 4
  %m_multiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  %call15 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %m_multiBodyConstraints)
  %cmp = icmp slt i32 %10, %call15
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_multiBodyConstraints16 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  %11 = load i32, i32* %c, align 4
  %call17 = call nonnull align 4 dereferenceable(4) %class.btMultiBodyConstraint** @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEixEi(%class.btAlignedObjectArray.51* %m_multiBodyConstraints16, i32 %11)
  %12 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %call17, align 4
  store %class.btMultiBodyConstraint* %12, %class.btMultiBodyConstraint** %constraint, align 4
  %13 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %constraint, align 4
  %14 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to void (%class.btMultiBodyDynamicsWorld*, %class.btMultiBodyConstraint*)***
  %vtable18 = load void (%class.btMultiBodyDynamicsWorld*, %class.btMultiBodyConstraint*)**, void (%class.btMultiBodyDynamicsWorld*, %class.btMultiBodyConstraint*)*** %14, align 4
  %vfn19 = getelementptr inbounds void (%class.btMultiBodyDynamicsWorld*, %class.btMultiBodyConstraint*)*, void (%class.btMultiBodyDynamicsWorld*, %class.btMultiBodyConstraint*)** %vtable18, i64 56
  %15 = load void (%class.btMultiBodyDynamicsWorld*, %class.btMultiBodyConstraint*)*, void (%class.btMultiBodyDynamicsWorld*, %class.btMultiBodyConstraint*)** %vfn19, align 4
  call void %15(%class.btMultiBodyDynamicsWorld* %this1, %class.btMultiBodyConstraint* %13)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %16 = load i32, i32* %c, align 4
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %c, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %b, align 4
  br label %for.cond20

for.cond20:                                       ; preds = %for.inc139, %for.end
  %17 = load i32, i32* %b, align 4
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call21 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %m_multiBodies)
  %cmp22 = icmp slt i32 %17, %call21
  br i1 %cmp22, label %for.body23, label %for.end141

for.body23:                                       ; preds = %for.cond20
  %m_multiBodies24 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %18 = load i32, i32* %b, align 4
  %call25 = call nonnull align 4 dereferenceable(4) %class.btMultiBody** @_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi(%class.btAlignedObjectArray.27* %m_multiBodies24, i32 %18)
  %19 = load %class.btMultiBody*, %class.btMultiBody** %call25, align 4
  store %class.btMultiBody* %19, %class.btMultiBody** %bod, align 4
  %20 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %m_scratch_world_to_local1 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 8
  %m_scratch_local_origin1 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 9
  call void @_ZN11btMultiBody17forwardKinematicsER20btAlignedObjectArrayI12btQuaternionERS0_I9btVector3E(%class.btMultiBody* %20, %class.btAlignedObjectArray.77* nonnull align 4 dereferenceable(17) %m_scratch_world_to_local1, %class.btAlignedObjectArray.42* nonnull align 4 dereferenceable(17) %m_scratch_local_origin1)
  %21 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btCollisionWorld*
  %22 = bitcast %class.btCollisionWorld* %21 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable26 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %22, align 4
  %vfn27 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable26, i64 5
  %23 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn27, align 4
  %call28 = call %class.btIDebugDraw* %23(%class.btCollisionWorld* %21)
  %24 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  call void @_ZNK11btMultiBody21getBaseWorldTransformEv(%class.btTransform* sret align 4 %ref.tmp, %class.btMultiBody* %24)
  %25 = bitcast %class.btIDebugDraw* %call28 to void (%class.btIDebugDraw*, %class.btTransform*, float)***
  %vtable29 = load void (%class.btIDebugDraw*, %class.btTransform*, float)**, void (%class.btIDebugDraw*, %class.btTransform*, float)*** %25, align 4
  %vfn30 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vtable29, i64 16
  %26 = load void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vfn30, align 4
  call void %26(%class.btIDebugDraw* %call28, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp, float 0x3FB99999A0000000)
  store i32 0, i32* %m, align 4
  br label %for.cond31

for.cond31:                                       ; preds = %for.inc136, %for.body23
  %27 = load i32, i32* %m, align 4
  %28 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call32 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %28)
  %cmp33 = icmp slt i32 %27, %call32
  br i1 %cmp33, label %for.body34, label %for.end138

for.body34:                                       ; preds = %for.cond31
  %29 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %30 = load i32, i32* %m, align 4
  %call35 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %29, i32 %30)
  %m_cachedWorldTransform = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call35, i32 0, i32 25
  store %class.btTransform* %m_cachedWorldTransform, %class.btTransform** %tr, align 4
  %31 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btCollisionWorld*
  %32 = bitcast %class.btCollisionWorld* %31 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable36 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %32, align 4
  %vfn37 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable36, i64 5
  %33 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn37, align 4
  %call38 = call %class.btIDebugDraw* %33(%class.btCollisionWorld* %31)
  %34 = load %class.btTransform*, %class.btTransform** %tr, align 4
  %35 = bitcast %class.btIDebugDraw* %call38 to void (%class.btIDebugDraw*, %class.btTransform*, float)***
  %vtable39 = load void (%class.btIDebugDraw*, %class.btTransform*, float)**, void (%class.btIDebugDraw*, %class.btTransform*, float)*** %35, align 4
  %vfn40 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vtable39, i64 16
  %36 = load void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vfn40, align 4
  call void %36(%class.btIDebugDraw* %call38, %class.btTransform* nonnull align 4 dereferenceable(64) %34, float 0x3FB99999A0000000)
  %37 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %38 = load i32, i32* %m, align 4
  %call41 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %37, i32 %38)
  %m_jointType = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call41, i32 0, i32 23
  %39 = load i32, i32* %m_jointType, align 4
  %cmp42 = icmp eq i32 %39, 0
  br i1 %cmp42, label %if.then43, label %if.end66

if.then43:                                        ; preds = %for.body34
  %40 = load %class.btTransform*, %class.btTransform** %tr, align 4
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp44, %class.btTransform* %40)
  %41 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %42 = load i32, i32* %m, align 4
  %call45 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %41, i32 %42)
  %m_axes = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call45, i32 0, i32 8
  %arrayidx = getelementptr inbounds [6 x %struct.btSpatialMotionVector], [6 x %struct.btSpatialMotionVector]* %m_axes, i32 0, i32 0
  %m_topVec = getelementptr inbounds %struct.btSpatialMotionVector, %struct.btSpatialMotionVector* %arrayidx, i32 0, i32 0
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %vec, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp44, %class.btVector3* nonnull align 4 dereferenceable(16) %m_topVec)
  store float 0.000000e+00, float* %ref.tmp46, align 4
  store float 0.000000e+00, float* %ref.tmp47, align 4
  store float 0.000000e+00, float* %ref.tmp48, align 4
  store float 1.000000e+00, float* %ref.tmp49, align 4
  %call50 = call %class.btVector4* @_ZN9btVector4C2ERKfS1_S1_S1_(%class.btVector4* %color, float* nonnull align 4 dereferenceable(4) %ref.tmp46, float* nonnull align 4 dereferenceable(4) %ref.tmp47, float* nonnull align 4 dereferenceable(4) %ref.tmp48, float* nonnull align 4 dereferenceable(4) %ref.tmp49)
  %43 = load %class.btTransform*, %class.btTransform** %tr, align 4
  %call52 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %43)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp51, %class.btVector3* nonnull align 4 dereferenceable(16) %vec, %class.btVector3* nonnull align 4 dereferenceable(16) %call52)
  %44 = load %class.btTransform*, %class.btTransform** %tr, align 4
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp54, %class.btTransform* %44)
  %45 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %46 = load i32, i32* %m, align 4
  %call55 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %45, i32 %46)
  %m_dVector = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call55, i32 0, i32 4
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp53, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp54, %class.btVector3* nonnull align 4 dereferenceable(16) %m_dVector)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %from, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp51, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp53)
  %47 = load %class.btTransform*, %class.btTransform** %tr, align 4
  %call56 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %47)
  %48 = load %class.btTransform*, %class.btTransform** %tr, align 4
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp58, %class.btTransform* %48)
  %49 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %50 = load i32, i32* %m, align 4
  %call59 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %49, i32 %50)
  %m_dVector60 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call59, i32 0, i32 4
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp57, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp58, %class.btVector3* nonnull align 4 dereferenceable(16) %m_dVector60)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %to, %class.btVector3* nonnull align 4 dereferenceable(16) %call56, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp57)
  %51 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btCollisionWorld*
  %52 = bitcast %class.btCollisionWorld* %51 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable61 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %52, align 4
  %vfn62 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable61, i64 5
  %53 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn62, align 4
  %call63 = call %class.btIDebugDraw* %53(%class.btCollisionWorld* %51)
  %54 = bitcast %class.btVector4* %color to %class.btVector3*
  %55 = bitcast %class.btIDebugDraw* %call63 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable64 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %55, align 4
  %vfn65 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable64, i64 4
  %56 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn65, align 4
  call void %56(%class.btIDebugDraw* %call63, %class.btVector3* nonnull align 4 dereferenceable(16) %from, %class.btVector3* nonnull align 4 dereferenceable(16) %to, %class.btVector3* nonnull align 4 dereferenceable(16) %54)
  br label %if.end66

if.end66:                                         ; preds = %if.then43, %for.body34
  %57 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %58 = load i32, i32* %m, align 4
  %call67 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %57, i32 %58)
  %m_jointType68 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call67, i32 0, i32 23
  %59 = load i32, i32* %m_jointType68, align 4
  %cmp69 = icmp eq i32 %59, 4
  br i1 %cmp69, label %if.then70, label %if.end100

if.then70:                                        ; preds = %if.end66
  %60 = load %class.btTransform*, %class.btTransform** %tr, align 4
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp72, %class.btTransform* %60)
  %61 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %62 = load i32, i32* %m, align 4
  %call73 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %61, i32 %62)
  %m_axes74 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call73, i32 0, i32 8
  %arrayidx75 = getelementptr inbounds [6 x %struct.btSpatialMotionVector], [6 x %struct.btSpatialMotionVector]* %m_axes74, i32 0, i32 0
  %m_bottomVec = getelementptr inbounds %struct.btSpatialMotionVector, %struct.btSpatialMotionVector* %arrayidx75, i32 0, i32 1
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %vec71, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp72, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bottomVec)
  store float 0.000000e+00, float* %ref.tmp77, align 4
  store float 0.000000e+00, float* %ref.tmp78, align 4
  store float 0.000000e+00, float* %ref.tmp79, align 4
  store float 1.000000e+00, float* %ref.tmp80, align 4
  %call81 = call %class.btVector4* @_ZN9btVector4C2ERKfS1_S1_S1_(%class.btVector4* %color76, float* nonnull align 4 dereferenceable(4) %ref.tmp77, float* nonnull align 4 dereferenceable(4) %ref.tmp78, float* nonnull align 4 dereferenceable(4) %ref.tmp79, float* nonnull align 4 dereferenceable(4) %ref.tmp80)
  %63 = load %class.btTransform*, %class.btTransform** %tr, align 4
  %call84 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %63)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp83, %class.btVector3* nonnull align 4 dereferenceable(16) %vec71, %class.btVector3* nonnull align 4 dereferenceable(16) %call84)
  %64 = load %class.btTransform*, %class.btTransform** %tr, align 4
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp86, %class.btTransform* %64)
  %65 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %66 = load i32, i32* %m, align 4
  %call87 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %65, i32 %66)
  %m_dVector88 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call87, i32 0, i32 4
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp85, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp86, %class.btVector3* nonnull align 4 dereferenceable(16) %m_dVector88)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %from82, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp83, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp85)
  %67 = load %class.btTransform*, %class.btTransform** %tr, align 4
  %call90 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %67)
  %68 = load %class.btTransform*, %class.btTransform** %tr, align 4
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp92, %class.btTransform* %68)
  %69 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %70 = load i32, i32* %m, align 4
  %call93 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %69, i32 %70)
  %m_dVector94 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call93, i32 0, i32 4
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp91, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp92, %class.btVector3* nonnull align 4 dereferenceable(16) %m_dVector94)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %to89, %class.btVector3* nonnull align 4 dereferenceable(16) %call90, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp91)
  %71 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btCollisionWorld*
  %72 = bitcast %class.btCollisionWorld* %71 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable95 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %72, align 4
  %vfn96 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable95, i64 5
  %73 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn96, align 4
  %call97 = call %class.btIDebugDraw* %73(%class.btCollisionWorld* %71)
  %74 = bitcast %class.btVector4* %color76 to %class.btVector3*
  %75 = bitcast %class.btIDebugDraw* %call97 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable98 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %75, align 4
  %vfn99 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable98, i64 4
  %76 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn99, align 4
  call void %76(%class.btIDebugDraw* %call97, %class.btVector3* nonnull align 4 dereferenceable(16) %from82, %class.btVector3* nonnull align 4 dereferenceable(16) %to89, %class.btVector3* nonnull align 4 dereferenceable(16) %74)
  br label %if.end100

if.end100:                                        ; preds = %if.then70, %if.end66
  %77 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %78 = load i32, i32* %m, align 4
  %call101 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %77, i32 %78)
  %m_jointType102 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call101, i32 0, i32 23
  %79 = load i32, i32* %m_jointType102, align 4
  %cmp103 = icmp eq i32 %79, 1
  br i1 %cmp103, label %if.then104, label %if.end135

if.then104:                                       ; preds = %if.end100
  %80 = load %class.btTransform*, %class.btTransform** %tr, align 4
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp106, %class.btTransform* %80)
  %81 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %82 = load i32, i32* %m, align 4
  %call107 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %81, i32 %82)
  %m_axes108 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call107, i32 0, i32 8
  %arrayidx109 = getelementptr inbounds [6 x %struct.btSpatialMotionVector], [6 x %struct.btSpatialMotionVector]* %m_axes108, i32 0, i32 0
  %m_bottomVec110 = getelementptr inbounds %struct.btSpatialMotionVector, %struct.btSpatialMotionVector* %arrayidx109, i32 0, i32 1
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %vec105, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp106, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bottomVec110)
  store float 0.000000e+00, float* %ref.tmp112, align 4
  store float 0.000000e+00, float* %ref.tmp113, align 4
  store float 0.000000e+00, float* %ref.tmp114, align 4
  store float 1.000000e+00, float* %ref.tmp115, align 4
  %call116 = call %class.btVector4* @_ZN9btVector4C2ERKfS1_S1_S1_(%class.btVector4* %color111, float* nonnull align 4 dereferenceable(4) %ref.tmp112, float* nonnull align 4 dereferenceable(4) %ref.tmp113, float* nonnull align 4 dereferenceable(4) %ref.tmp114, float* nonnull align 4 dereferenceable(4) %ref.tmp115)
  %83 = load %class.btTransform*, %class.btTransform** %tr, align 4
  %call119 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %83)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp118, %class.btVector3* nonnull align 4 dereferenceable(16) %vec105, %class.btVector3* nonnull align 4 dereferenceable(16) %call119)
  %84 = load %class.btTransform*, %class.btTransform** %tr, align 4
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp121, %class.btTransform* %84)
  %85 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %86 = load i32, i32* %m, align 4
  %call122 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %85, i32 %86)
  %m_dVector123 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call122, i32 0, i32 4
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp120, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp121, %class.btVector3* nonnull align 4 dereferenceable(16) %m_dVector123)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %from117, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp118, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp120)
  %87 = load %class.btTransform*, %class.btTransform** %tr, align 4
  %call125 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %87)
  %88 = load %class.btTransform*, %class.btTransform** %tr, align 4
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp127, %class.btTransform* %88)
  %89 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %90 = load i32, i32* %m, align 4
  %call128 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %89, i32 %90)
  %m_dVector129 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call128, i32 0, i32 4
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp126, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp127, %class.btVector3* nonnull align 4 dereferenceable(16) %m_dVector129)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %to124, %class.btVector3* nonnull align 4 dereferenceable(16) %call125, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp126)
  %91 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btCollisionWorld*
  %92 = bitcast %class.btCollisionWorld* %91 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable130 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %92, align 4
  %vfn131 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable130, i64 5
  %93 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn131, align 4
  %call132 = call %class.btIDebugDraw* %93(%class.btCollisionWorld* %91)
  %94 = bitcast %class.btVector4* %color111 to %class.btVector3*
  %95 = bitcast %class.btIDebugDraw* %call132 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable133 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %95, align 4
  %vfn134 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable133, i64 4
  %96 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn134, align 4
  call void %96(%class.btIDebugDraw* %call132, %class.btVector3* nonnull align 4 dereferenceable(16) %from117, %class.btVector3* nonnull align 4 dereferenceable(16) %to124, %class.btVector3* nonnull align 4 dereferenceable(16) %94)
  br label %if.end135

if.end135:                                        ; preds = %if.then104, %if.end100
  br label %for.inc136

for.inc136:                                       ; preds = %if.end135
  %97 = load i32, i32* %m, align 4
  %inc137 = add nsw i32 %97, 1
  store i32 %inc137, i32* %m, align 4
  br label %for.cond31

for.end138:                                       ; preds = %for.cond31
  br label %for.inc139

for.inc139:                                       ; preds = %for.end138
  %98 = load i32, i32* %b, align 4
  %inc140 = add nsw i32 %98, 1
  store i32 %inc140, i32* %b, align 4
  br label %for.cond20

for.end141:                                       ; preds = %for.cond20
  %call142 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile13) #10
  br label %if.end143

if.end143:                                        ; preds = %for.end141, %if.end
  br label %if.end144

if.end144:                                        ; preds = %if.end143, %entry
  %99 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  call void @_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv(%class.btDiscreteDynamicsWorld* %99)
  %call145 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMultiBody21getBaseWorldTransformEv(%class.btTransform* noalias sret align 4 %agg.result, %class.btMultiBody* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMultiBody*, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %agg.result)
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMultiBody10getBasePosEv(%class.btMultiBody* %this1)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZNK11btMultiBody17getWorldToBaseRotEv(%class.btMultiBody* %this1)
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* %call3)
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotation, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %rotation.addr = alloca %class.btQuaternion*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %q = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  store %class.btQuaternion* %rotation, %class.btQuaternion** %rotation.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  call void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* sret align 4 %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* %2)
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp)
  %3 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %3)
  %4 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %4)
  %5 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %5)
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call1, float* nonnull align 4 dereferenceable(4) %call2, float* nonnull align 4 dereferenceable(4) %call3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %agg.result)
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %agg.result)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector4* @_ZN9btVector4C2ERKfS1_S1_S1_(%class.btVector4* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector4*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btVector4* %this, %class.btVector4** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btVector4*, %class.btVector4** %this.addr, align 4
  %0 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %1 = load float*, float** %_x.addr, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float*, float** %_z.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3)
  %4 = load float*, float** %_w.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  store float %5, float* %arrayidx, align 4
  ret %class.btVector4* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

declare void @_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

; Function Attrs: noinline optnone
define hidden void @_ZN24btMultiBodyDynamicsWorld12applyGravityEv(%class.btMultiBodyDynamicsWorld* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %i = alloca i32, align 4
  %bod = alloca %class.btMultiBody*, align 4
  %isSleeping = alloca i8, align 1
  %b = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp24 = alloca float, align 4
  %j = alloca i32, align 4
  %ref.tmp30 = alloca %class.btVector3, align 4
  %ref.tmp32 = alloca float, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  call void @_ZN23btDiscreteDynamicsWorld12applyGravityEv(%class.btDiscreteDynamicsWorld* %0)
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.7, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc38, %entry
  %1 = load i32, i32* %i, align 4
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %m_multiBodies)
  %cmp = icmp slt i32 %1, %call2
  br i1 %cmp, label %for.body, label %for.end40

for.body:                                         ; preds = %for.cond
  %m_multiBodies3 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %2 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %class.btMultiBody** @_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi(%class.btAlignedObjectArray.27* %m_multiBodies3, i32 %2)
  %3 = load %class.btMultiBody*, %class.btMultiBody** %call4, align 4
  store %class.btMultiBody* %3, %class.btMultiBody** %bod, align 4
  store i8 0, i8* %isSleeping, align 1
  %4 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call5 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %4)
  %tobool = icmp ne %class.btMultiBodyLinkCollider* %call5, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %5 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call6 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %5)
  %6 = bitcast %class.btMultiBodyLinkCollider* %call6 to %class.btCollisionObject*
  %call7 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %6)
  %cmp8 = icmp eq i32 %call7, 2
  br i1 %cmp8, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i8 1, i8* %isSleeping, align 1
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %for.body
  store i32 0, i32* %b, align 4
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc, %if.end
  %7 = load i32, i32* %b, align 4
  %8 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call10 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %8)
  %cmp11 = icmp slt i32 %7, %call10
  br i1 %cmp11, label %for.body12, label %for.end

for.body12:                                       ; preds = %for.cond9
  %9 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %10 = load i32, i32* %b, align 4
  %call13 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %9, i32 %10)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call13, i32 0, i32 19
  %11 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4
  %tobool14 = icmp ne %class.btMultiBodyLinkCollider* %11, null
  br i1 %tobool14, label %land.lhs.true15, label %if.end21

land.lhs.true15:                                  ; preds = %for.body12
  %12 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %13 = load i32, i32* %b, align 4
  %call16 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %12, i32 %13)
  %m_collider17 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call16, i32 0, i32 19
  %14 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider17, align 4
  %15 = bitcast %class.btMultiBodyLinkCollider* %14 to %class.btCollisionObject*
  %call18 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %15)
  %cmp19 = icmp eq i32 %call18, 2
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %land.lhs.true15
  store i8 1, i8* %isSleeping, align 1
  br label %if.end21

if.end21:                                         ; preds = %if.then20, %land.lhs.true15, %for.body12
  br label %for.inc

for.inc:                                          ; preds = %if.end21
  %16 = load i32, i32* %b, align 4
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %b, align 4
  br label %for.cond9

for.end:                                          ; preds = %for.cond9
  %17 = load i8, i8* %isSleeping, align 1
  %tobool22 = trunc i8 %17 to i1
  br i1 %tobool22, label %if.end37, label %if.then23

if.then23:                                        ; preds = %for.end
  %18 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %19 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_gravity = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %19, i32 0, i32 7
  %20 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call25 = call float @_ZNK11btMultiBody11getBaseMassEv(%class.btMultiBody* %20)
  store float %call25, float* %ref.tmp24, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_gravity, float* nonnull align 4 dereferenceable(4) %ref.tmp24)
  call void @_ZN11btMultiBody12addBaseForceERK9btVector3(%class.btMultiBody* %18, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  store i32 0, i32* %j, align 4
  br label %for.cond26

for.cond26:                                       ; preds = %for.inc34, %if.then23
  %21 = load i32, i32* %j, align 4
  %22 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call27 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %22)
  %cmp28 = icmp slt i32 %21, %call27
  br i1 %cmp28, label %for.body29, label %for.end36

for.body29:                                       ; preds = %for.cond26
  %23 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %24 = load i32, i32* %j, align 4
  %25 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %m_gravity31 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %25, i32 0, i32 7
  %26 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %27 = load i32, i32* %j, align 4
  %call33 = call float @_ZNK11btMultiBody11getLinkMassEi(%class.btMultiBody* %26, i32 %27)
  store float %call33, float* %ref.tmp32, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp30, %class.btVector3* nonnull align 4 dereferenceable(16) %m_gravity31, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  call void @_ZN11btMultiBody12addLinkForceEiRK9btVector3(%class.btMultiBody* %23, i32 %24, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp30)
  br label %for.inc34

for.inc34:                                        ; preds = %for.body29
  %28 = load i32, i32* %j, align 4
  %inc35 = add nsw i32 %28, 1
  store i32 %inc35, i32* %j, align 4
  br label %for.cond26

for.end36:                                        ; preds = %for.cond26
  br label %if.end37

if.end37:                                         ; preds = %for.end36, %for.end
  br label %for.inc38

for.inc38:                                        ; preds = %if.end37
  %29 = load i32, i32* %i, align 4
  %inc39 = add nsw i32 %29, 1
  store i32 %inc39, i32* %i, align 4
  br label %for.cond

for.end40:                                        ; preds = %for.cond
  %call41 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

declare void @_ZN23btDiscreteDynamicsWorld12applyGravityEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMultiBody12addBaseForceERK9btVector3(%class.btMultiBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %f) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %f.addr = alloca %class.btVector3*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  store %class.btVector3* %f, %class.btVector3** %f.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %f.addr, align 4
  %m_baseForce = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_baseForce, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMultiBody11getBaseMassEv(%class.btMultiBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_baseMass = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 5
  %0 = load float, float* %m_baseMass, align 4
  ret float %0
}

declare void @_ZN11btMultiBody12addLinkForceEiRK9btVector3(%class.btMultiBody*, i32, %class.btVector3* nonnull align 4 dereferenceable(16)) #3

declare float @_ZNK11btMultiBody11getLinkMassEi(%class.btMultiBody*, i32) #3

; Function Attrs: noinline optnone
define hidden void @_ZN24btMultiBodyDynamicsWorld30clearMultiBodyConstraintForcesEv(%class.btMultiBodyDynamicsWorld* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %i = alloca i32, align 4
  %bod = alloca %class.btMultiBody*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %m_multiBodies)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_multiBodies2 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %1 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btMultiBody** @_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi(%class.btAlignedObjectArray.27* %m_multiBodies2, i32 %1)
  %2 = load %class.btMultiBody*, %class.btMultiBody** %call3, align 4
  store %class.btMultiBody* %2, %class.btMultiBody** %bod, align 4
  %3 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  call void @_ZN11btMultiBody21clearConstraintForcesEv(%class.btMultiBody* %3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

declare void @_ZN11btMultiBody21clearConstraintForcesEv(%class.btMultiBody*) #3

; Function Attrs: noinline optnone
define hidden void @_ZN24btMultiBodyDynamicsWorld20clearMultiBodyForcesEv(%class.btMultiBodyDynamicsWorld* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %i = alloca i32, align 4
  %bod = alloca %class.btMultiBody*, align 4
  %isSleeping = alloca i8, align 1
  %b = alloca i32, align 4
  %bod24 = alloca %class.btMultiBody*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.8, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc28, %entry
  %0 = load i32, i32* %i, align 4
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %m_multiBodies)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end30

for.body:                                         ; preds = %for.cond
  %m_multiBodies3 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %1 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %class.btMultiBody** @_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi(%class.btAlignedObjectArray.27* %m_multiBodies3, i32 %1)
  %2 = load %class.btMultiBody*, %class.btMultiBody** %call4, align 4
  store %class.btMultiBody* %2, %class.btMultiBody** %bod, align 4
  store i8 0, i8* %isSleeping, align 1
  %3 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call5 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %3)
  %tobool = icmp ne %class.btMultiBodyLinkCollider* %call5, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %4 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call6 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %4)
  %5 = bitcast %class.btMultiBodyLinkCollider* %call6 to %class.btCollisionObject*
  %call7 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %5)
  %cmp8 = icmp eq i32 %call7, 2
  br i1 %cmp8, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i8 1, i8* %isSleeping, align 1
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %for.body
  store i32 0, i32* %b, align 4
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc, %if.end
  %6 = load i32, i32* %b, align 4
  %7 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %call10 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %7)
  %cmp11 = icmp slt i32 %6, %call10
  br i1 %cmp11, label %for.body12, label %for.end

for.body12:                                       ; preds = %for.cond9
  %8 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %9 = load i32, i32* %b, align 4
  %call13 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %8, i32 %9)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call13, i32 0, i32 19
  %10 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4
  %tobool14 = icmp ne %class.btMultiBodyLinkCollider* %10, null
  br i1 %tobool14, label %land.lhs.true15, label %if.end21

land.lhs.true15:                                  ; preds = %for.body12
  %11 = load %class.btMultiBody*, %class.btMultiBody** %bod, align 4
  %12 = load i32, i32* %b, align 4
  %call16 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %11, i32 %12)
  %m_collider17 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call16, i32 0, i32 19
  %13 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider17, align 4
  %14 = bitcast %class.btMultiBodyLinkCollider* %13 to %class.btCollisionObject*
  %call18 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %14)
  %cmp19 = icmp eq i32 %call18, 2
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %land.lhs.true15
  store i8 1, i8* %isSleeping, align 1
  br label %if.end21

if.end21:                                         ; preds = %if.then20, %land.lhs.true15, %for.body12
  br label %for.inc

for.inc:                                          ; preds = %if.end21
  %15 = load i32, i32* %b, align 4
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %b, align 4
  br label %for.cond9

for.end:                                          ; preds = %for.cond9
  %16 = load i8, i8* %isSleeping, align 1
  %tobool22 = trunc i8 %16 to i1
  br i1 %tobool22, label %if.end27, label %if.then23

if.then23:                                        ; preds = %for.end
  %m_multiBodies25 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %17 = load i32, i32* %i, align 4
  %call26 = call nonnull align 4 dereferenceable(4) %class.btMultiBody** @_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi(%class.btAlignedObjectArray.27* %m_multiBodies25, i32 %17)
  %18 = load %class.btMultiBody*, %class.btMultiBody** %call26, align 4
  store %class.btMultiBody* %18, %class.btMultiBody** %bod24, align 4
  %19 = load %class.btMultiBody*, %class.btMultiBody** %bod24, align 4
  call void @_ZN11btMultiBody21clearForcesAndTorquesEv(%class.btMultiBody* %19)
  br label %if.end27

if.end27:                                         ; preds = %if.then23, %for.end
  br label %for.inc28

for.inc28:                                        ; preds = %if.end27
  %20 = load i32, i32* %i, align 4
  %inc29 = add nsw i32 %20, 1
  store i32 %inc29, i32* %i, align 4
  br label %for.cond

for.end30:                                        ; preds = %for.cond
  %call31 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

declare void @_ZN11btMultiBody21clearForcesAndTorquesEv(%class.btMultiBody*) #3

; Function Attrs: noinline optnone
define hidden void @_ZN24btMultiBodyDynamicsWorld11clearForcesEv(%class.btMultiBodyDynamicsWorld* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  call void @_ZN23btDiscreteDynamicsWorld11clearForcesEv(%class.btDiscreteDynamicsWorld* %0)
  %1 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to void (%class.btMultiBodyDynamicsWorld*)***
  %vtable = load void (%class.btMultiBodyDynamicsWorld*)**, void (%class.btMultiBodyDynamicsWorld*)*** %1, align 4
  %vfn = getelementptr inbounds void (%class.btMultiBodyDynamicsWorld*)*, void (%class.btMultiBodyDynamicsWorld*)** %vtable, i64 58
  %2 = load void (%class.btMultiBodyDynamicsWorld*)*, void (%class.btMultiBodyDynamicsWorld*)** %vfn, align 4
  call void %2(%class.btMultiBodyDynamicsWorld* %this1)
  ret void
}

declare void @_ZN23btDiscreteDynamicsWorld11clearForcesEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

; Function Attrs: noinline optnone
define hidden void @_ZN24btMultiBodyDynamicsWorld9serializeEP12btSerializer(%class.btMultiBodyDynamicsWorld* %this, %class.btSerializer* %serializer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %0 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %1 = bitcast %class.btSerializer* %0 to void (%class.btSerializer*)***
  %vtable = load void (%class.btSerializer*)**, void (%class.btSerializer*)*** %1, align 4
  %vfn = getelementptr inbounds void (%class.btSerializer*)*, void (%class.btSerializer*)** %vtable, i64 8
  %2 = load void (%class.btSerializer*)*, void (%class.btSerializer*)** %vfn, align 4
  call void %2(%class.btSerializer* %0)
  %3 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %4 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  call void @_ZN23btDiscreteDynamicsWorld26serializeDynamicsWorldInfoEP12btSerializer(%class.btDiscreteDynamicsWorld* %3, %class.btSerializer* %4)
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %6 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to void (%class.btMultiBodyDynamicsWorld*, %class.btSerializer*)***
  %vtable2 = load void (%class.btMultiBodyDynamicsWorld*, %class.btSerializer*)**, void (%class.btMultiBodyDynamicsWorld*, %class.btSerializer*)*** %6, align 4
  %vfn3 = getelementptr inbounds void (%class.btMultiBodyDynamicsWorld*, %class.btSerializer*)*, void (%class.btMultiBodyDynamicsWorld*, %class.btSerializer*)** %vtable2, i64 47
  %7 = load void (%class.btMultiBodyDynamicsWorld*, %class.btSerializer*)*, void (%class.btMultiBodyDynamicsWorld*, %class.btSerializer*)** %vfn3, align 4
  call void %7(%class.btMultiBodyDynamicsWorld* %this1, %class.btSerializer* %5)
  %8 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %9 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  call void @_ZN23btDiscreteDynamicsWorld20serializeRigidBodiesEP12btSerializer(%class.btDiscreteDynamicsWorld* %8, %class.btSerializer* %9)
  %10 = bitcast %class.btMultiBodyDynamicsWorld* %this1 to %class.btCollisionWorld*
  %11 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  call void @_ZN16btCollisionWorld25serializeCollisionObjectsEP12btSerializer(%class.btCollisionWorld* %10, %class.btSerializer* %11)
  %12 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %13 = bitcast %class.btSerializer* %12 to void (%class.btSerializer*)***
  %vtable4 = load void (%class.btSerializer*)**, void (%class.btSerializer*)*** %13, align 4
  %vfn5 = getelementptr inbounds void (%class.btSerializer*)*, void (%class.btSerializer*)** %vtable4, i64 9
  %14 = load void (%class.btSerializer*)*, void (%class.btSerializer*)** %vfn5, align 4
  call void %14(%class.btSerializer* %12)
  ret void
}

declare void @_ZN23btDiscreteDynamicsWorld26serializeDynamicsWorldInfoEP12btSerializer(%class.btDiscreteDynamicsWorld*, %class.btSerializer*) #3

declare void @_ZN23btDiscreteDynamicsWorld20serializeRigidBodiesEP12btSerializer(%class.btDiscreteDynamicsWorld*, %class.btSerializer*) #3

declare void @_ZN16btCollisionWorld25serializeCollisionObjectsEP12btSerializer(%class.btCollisionWorld*, %class.btSerializer*) #3

; Function Attrs: noinline optnone
define hidden void @_ZN24btMultiBodyDynamicsWorld20serializeMultiBodiesEP12btSerializer(%class.btMultiBodyDynamicsWorld* %this, %class.btSerializer* %serializer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %i = alloca i32, align 4
  %mb = alloca %class.btMultiBody*, align 4
  %len = alloca i32, align 4
  %chunk = alloca %class.btChunk*, align 4
  %structType = alloca i8*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %m_multiBodies)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_multiBodies2 = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %1 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btMultiBody** @_ZN20btAlignedObjectArrayIP11btMultiBodyEixEi(%class.btAlignedObjectArray.27* %m_multiBodies2, i32 %1)
  %2 = load %class.btMultiBody*, %class.btMultiBody** %call3, align 4
  store %class.btMultiBody* %2, %class.btMultiBody** %mb, align 4
  %3 = load %class.btMultiBody*, %class.btMultiBody** %mb, align 4
  %4 = bitcast %class.btMultiBody* %3 to i32 (%class.btMultiBody*)***
  %vtable = load i32 (%class.btMultiBody*)**, i32 (%class.btMultiBody*)*** %4, align 4
  %vfn = getelementptr inbounds i32 (%class.btMultiBody*)*, i32 (%class.btMultiBody*)** %vtable, i64 2
  %5 = load i32 (%class.btMultiBody*)*, i32 (%class.btMultiBody*)** %vfn, align 4
  %call4 = call i32 %5(%class.btMultiBody* %3)
  store i32 %call4, i32* %len, align 4
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %7 = load i32, i32* %len, align 4
  %8 = bitcast %class.btSerializer* %6 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable5 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %8, align 4
  %vfn6 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable5, i64 4
  %9 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn6, align 4
  %call7 = call %class.btChunk* %9(%class.btSerializer* %6, i32 %7, i32 1)
  store %class.btChunk* %call7, %class.btChunk** %chunk, align 4
  %10 = load %class.btMultiBody*, %class.btMultiBody** %mb, align 4
  %11 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %11, i32 0, i32 2
  %12 = load i8*, i8** %m_oldPtr, align 4
  %13 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %14 = bitcast %class.btMultiBody* %10 to i8* (%class.btMultiBody*, i8*, %class.btSerializer*)***
  %vtable8 = load i8* (%class.btMultiBody*, i8*, %class.btSerializer*)**, i8* (%class.btMultiBody*, i8*, %class.btSerializer*)*** %14, align 4
  %vfn9 = getelementptr inbounds i8* (%class.btMultiBody*, i8*, %class.btSerializer*)*, i8* (%class.btMultiBody*, i8*, %class.btSerializer*)** %vtable8, i64 3
  %15 = load i8* (%class.btMultiBody*, i8*, %class.btSerializer*)*, i8* (%class.btMultiBody*, i8*, %class.btSerializer*)** %vfn9, align 4
  %call10 = call i8* %15(%class.btMultiBody* %10, i8* %12, %class.btSerializer* %13)
  store i8* %call10, i8** %structType, align 4
  %16 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %17 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %18 = load i8*, i8** %structType, align 4
  %19 = load %class.btMultiBody*, %class.btMultiBody** %mb, align 4
  %20 = bitcast %class.btMultiBody* %19 to i8*
  %21 = bitcast %class.btSerializer* %16 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable11 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %21, align 4
  %vfn12 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable11, i64 5
  %22 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn12, align 4
  call void %22(%class.btSerializer* %16, %class.btChunk* %17, i8* %18, i32 1497645645, i8* %20)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %23 = load i32, i32* %i, align 4
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

declare void @_ZN16btCollisionWorld11updateAabbsEv(%class.btCollisionWorld*) unnamed_addr #3

declare void @_ZN16btCollisionWorld23computeOverlappingPairsEv(%class.btCollisionWorld*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw(%class.btCollisionWorld* %this, %class.btIDebugDraw* %debugDrawer) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 5
  store %class.btIDebugDraw* %0, %class.btIDebugDraw** %m_debugDrawer, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btIDebugDraw* @_ZN16btCollisionWorld14getDebugDrawerEv(%class.btCollisionWorld* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 5
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4
  ret %class.btIDebugDraw* %0
}

declare void @_ZN16btCollisionWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3(%class.btCollisionWorld*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

declare void @_ZNK16btCollisionWorld7rayTestERK9btVector3S2_RNS_17RayResultCallbackE(%class.btCollisionWorld*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(24)) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectii(%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject(%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*) unnamed_addr #3

declare void @_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv(%class.btCollisionWorld*) unnamed_addr #3

declare i32 @_ZN23btDiscreteDynamicsWorld14stepSimulationEfif(%class.btDiscreteDynamicsWorld*, float, i32, float) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb(%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*, i1 zeroext) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint(%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3(%class.btDiscreteDynamicsWorld*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

declare void @_ZNK23btDiscreteDynamicsWorld10getGravityEv(%class.btVector3* sret align 4, %class.btDiscreteDynamicsWorld*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody(%class.btDiscreteDynamicsWorld*, %class.btRigidBody*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyii(%class.btDiscreteDynamicsWorld*, %class.btRigidBody*, i32, i32) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody(%class.btDiscreteDynamicsWorld*, %class.btRigidBody*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver(%class.btDiscreteDynamicsWorld*, %class.btConstraintSolver*) unnamed_addr #3

declare %class.btConstraintSolver* @_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

declare i32 @_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

declare %class.btTypedConstraint* @_ZN23btDiscreteDynamicsWorld13getConstraintEi(%class.btDiscreteDynamicsWorld*, i32) unnamed_addr #3

declare %class.btTypedConstraint* @_ZNK23btDiscreteDynamicsWorld13getConstraintEi(%class.btDiscreteDynamicsWorld*, i32) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv(%class.btDiscreteDynamicsWorld* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  ret i32 2
}

declare void @_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld24createPredictiveContactsEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraint(%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN23btDiscreteDynamicsWorld11setNumTasksEi(%class.btDiscreteDynamicsWorld* %this, i32 %numTasks) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %numTasks.addr = alloca i32, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store i32 %numTasks, i32* %numTasks.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN23btDiscreteDynamicsWorld14updateVehiclesEf(%class.btDiscreteDynamicsWorld* %this, float %timeStep) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = load float, float* %timeStep.addr, align 4
  call void @_ZN23btDiscreteDynamicsWorld13updateActionsEf(%class.btDiscreteDynamicsWorld* %this1, float %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK24btMultiBodyDynamicsWorld17getNumMultibodiesEv(%class.btMultiBodyDynamicsWorld* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %m_multiBodies = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %m_multiBodies)
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK24btMultiBodyDynamicsWorld26getNumMultiBodyConstraintsEv(%class.btMultiBodyDynamicsWorld* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %m_multiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %m_multiBodyConstraints)
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMultiBodyConstraint* @_ZN24btMultiBodyDynamicsWorld22getMultiBodyConstraintEi(%class.btMultiBodyDynamicsWorld* %this, i32 %constraintIndex) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %constraintIndex.addr = alloca i32, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  store i32 %constraintIndex, i32* %constraintIndex.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %m_multiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  %0 = load i32, i32* %constraintIndex.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %class.btMultiBodyConstraint** @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintEixEi(%class.btAlignedObjectArray.51* %m_multiBodyConstraints, i32 %0)
  %1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %call, align 4
  ret %class.btMultiBodyConstraint* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMultiBodyConstraint* @_ZNK24btMultiBodyDynamicsWorld22getMultiBodyConstraintEi(%class.btMultiBodyDynamicsWorld* %this, i32 %constraintIndex) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyDynamicsWorld*, align 4
  %constraintIndex.addr = alloca i32, align 4
  store %class.btMultiBodyDynamicsWorld* %this, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  store i32 %constraintIndex, i32* %constraintIndex.addr, align 4
  %this1 = load %class.btMultiBodyDynamicsWorld*, %class.btMultiBodyDynamicsWorld** %this.addr, align 4
  %m_multiBodyConstraints = getelementptr inbounds %class.btMultiBodyDynamicsWorld, %class.btMultiBodyDynamicsWorld* %this1, i32 0, i32 2
  %0 = load i32, i32* %constraintIndex.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %class.btMultiBodyConstraint** @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintEixEi(%class.btAlignedObjectArray.51* %m_multiBodyConstraints, i32 %0)
  %1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %call, align 4
  ret %class.btMultiBodyConstraint* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %this, i32 %x) #2 comdat {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %x.addr = alloca i32, align 4
  %elementPtr = alloca %struct.btElement*, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4
  store i32 %x, i32* %x.addr, align 4
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %x.addr, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %1 = load i32, i32* %x.addr, align 4
  %call = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.9* %m_elements, i32 %1)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call, i32 0, i32 0
  %2 = load i32, i32* %m_id, align 4
  %cmp = icmp ne i32 %0, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %m_elements2 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %m_elements3 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %3 = load i32, i32* %x.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.9* %m_elements3, i32 %3)
  %m_id5 = getelementptr inbounds %struct.btElement, %struct.btElement* %call4, i32 0, i32 0
  %4 = load i32, i32* %m_id5, align 4
  %call6 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.9* %m_elements2, i32 %4)
  store %struct.btElement* %call6, %struct.btElement** %elementPtr, align 4
  %5 = load %struct.btElement*, %struct.btElement** %elementPtr, align 4
  %m_id7 = getelementptr inbounds %struct.btElement, %struct.btElement* %5, i32 0, i32 0
  %6 = load i32, i32* %m_id7, align 4
  %m_elements8 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %7 = load i32, i32* %x.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.9* %m_elements8, i32 %7)
  %m_id10 = getelementptr inbounds %struct.btElement, %struct.btElement* %call9, i32 0, i32 0
  store i32 %6, i32* %m_id10, align 4
  %8 = load %struct.btElement*, %struct.btElement** %elementPtr, align 4
  %m_id11 = getelementptr inbounds %struct.btElement, %struct.btElement* %8, i32 0, i32 0
  %9 = load i32, i32* %m_id11, align 4
  store i32 %9, i32* %x.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %10 = load i32, i32* %x.addr, align 4
  ret i32 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray.9* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %struct.btElement*, %struct.btElement** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btElement, %struct.btElement* %0, i32 %1
  ret %struct.btElement* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray.30* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray.30* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.30*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.30* %this, %class.btAlignedObjectArray.30** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.30*, %class.btAlignedObjectArray.30** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.30, %class.btAlignedObjectArray.30* %this1, i32 0, i32 4
  %0 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %0, i32 %1
  ret %struct.btMultibodyLink* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btSimulationIslandManager::IslandCallback"* @_ZN25btSimulationIslandManager14IslandCallbackC2Ev(%"struct.btSimulationIslandManager::IslandCallback"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSimulationIslandManager::IslandCallback"*, align 4
  store %"struct.btSimulationIslandManager::IslandCallback"* %this, %"struct.btSimulationIslandManager::IslandCallback"** %this.addr, align 4
  %this1 = load %"struct.btSimulationIslandManager::IslandCallback"*, %"struct.btSimulationIslandManager::IslandCallback"** %this.addr, align 4
  %0 = bitcast %"struct.btSimulationIslandManager::IslandCallback"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN25btSimulationIslandManager14IslandCallbackE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %"struct.btSimulationIslandManager::IslandCallback"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.13* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.13* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.14* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.14* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.13* %this1)
  ret %class.btAlignedObjectArray.13* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EEC2Ev(%class.btAlignedAllocator.5* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.MultiBodyInplaceSolverIslandCallback* @_ZN36MultiBodyInplaceSolverIslandCallbackD2Ev(%struct.MultiBodyInplaceSolverIslandCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.MultiBodyInplaceSolverIslandCallback*, align 4
  store %struct.MultiBodyInplaceSolverIslandCallback* %this, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4
  %this1 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4
  %0 = bitcast %struct.MultiBodyInplaceSolverIslandCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV36MultiBodyInplaceSolverIslandCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_multiBodyConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 12
  %call = call %class.btAlignedObjectArray.51* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintED2Ev(%class.btAlignedObjectArray.51* %m_multiBodyConstraints) #10
  %m_constraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 11
  %call2 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray.4* %m_constraints) #10
  %m_manifolds = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 10
  %call3 = call %class.btAlignedObjectArray.13* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.13* %m_manifolds) #10
  %m_bodies = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 9
  %call4 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev(%class.btAlignedObjectArray* %m_bodies) #10
  %1 = bitcast %struct.MultiBodyInplaceSolverIslandCallback* %this1 to %"struct.btSimulationIslandManager::IslandCallback"*
  %call5 = call %"struct.btSimulationIslandManager::IslandCallback"* @_ZN25btSimulationIslandManager14IslandCallbackD2Ev(%"struct.btSimulationIslandManager::IslandCallback"* %1) #10
  ret %struct.MultiBodyInplaceSolverIslandCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN36MultiBodyInplaceSolverIslandCallbackD0Ev(%struct.MultiBodyInplaceSolverIslandCallback* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.MultiBodyInplaceSolverIslandCallback*, align 4
  store %struct.MultiBodyInplaceSolverIslandCallback* %this, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4
  %this1 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4
  %call = call %struct.MultiBodyInplaceSolverIslandCallback* @_ZN36MultiBodyInplaceSolverIslandCallbackD2Ev(%struct.MultiBodyInplaceSolverIslandCallback* %this1) #10
  %0 = bitcast %struct.MultiBodyInplaceSolverIslandCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #12
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN36MultiBodyInplaceSolverIslandCallback13processIslandEPP17btCollisionObjectiPP20btPersistentManifoldii(%struct.MultiBodyInplaceSolverIslandCallback* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %class.btPersistentManifold** %manifolds, i32 %numManifolds, i32 %islandId) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.MultiBodyInplaceSolverIslandCallback*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %manifolds.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %islandId.addr = alloca i32, align 4
  %startConstraint = alloca %class.btTypedConstraint**, align 4
  %startMultiBodyConstraint = alloca %class.btMultiBodyConstraint**, align 4
  %numCurConstraints = alloca i32, align 4
  %numCurMultiBodyConstraints = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.MultiBodyInplaceSolverIslandCallback* %this, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4
  store i32 %numBodies, i32* %numBodies.addr, align 4
  store %class.btPersistentManifold** %manifolds, %class.btPersistentManifold*** %manifolds.addr, align 4
  store i32 %numManifolds, i32* %numManifolds.addr, align 4
  store i32 %islandId, i32* %islandId.addr, align 4
  %this1 = load %struct.MultiBodyInplaceSolverIslandCallback*, %struct.MultiBodyInplaceSolverIslandCallback** %this.addr, align 4
  %0 = load i32, i32* %islandId.addr, align 4
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_solver = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 2
  %1 = load %class.btMultiBodyConstraintSolver*, %class.btMultiBodyConstraintSolver** %m_solver, align 4
  %2 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %3 = load i32, i32* %numBodies.addr, align 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifolds.addr, align 4
  %5 = load i32, i32* %numManifolds.addr, align 4
  %m_sortedConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 5
  %6 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_sortedConstraints, align 4
  %m_numConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 6
  %7 = load i32, i32* %m_numConstraints, align 4
  %m_multiBodySortedConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 3
  %8 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_multiBodySortedConstraints, align 4
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %8, i32 0
  %m_numConstraints2 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 6
  %9 = load i32, i32* %m_numConstraints2, align 4
  %m_solverInfo = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 1
  %10 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %m_solverInfo, align 4
  %m_debugDrawer = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 7
  %11 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4
  %m_dispatcher = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 8
  %12 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %13 = bitcast %class.btMultiBodyConstraintSolver* %1 to void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)***
  %vtable = load void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)**, void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*** %13, align 4
  %vfn = getelementptr inbounds void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vtable, i64 13
  %14 = load void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, void (%class.btMultiBodyConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %class.btMultiBodyConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vfn, align 4
  call void %14(%class.btMultiBodyConstraintSolver* %1, %class.btCollisionObject** %2, i32 %3, %class.btPersistentManifold** %4, i32 %5, %class.btTypedConstraint** %6, i32 %7, %class.btMultiBodyConstraint** %arrayidx, i32 %9, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %10, %class.btIDebugDraw* %11, %class.btDispatcher* %12)
  br label %if.end90

if.else:                                          ; preds = %entry
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %startConstraint, align 4
  store %class.btMultiBodyConstraint** null, %class.btMultiBodyConstraint*** %startMultiBodyConstraint, align 4
  store i32 0, i32* %numCurConstraints, align 4
  store i32 0, i32* %numCurMultiBodyConstraints, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %15 = load i32, i32* %i, align 4
  %m_numConstraints3 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 6
  %16 = load i32, i32* %m_numConstraints3, align 4
  %cmp4 = icmp slt i32 %15, %16
  br i1 %cmp4, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_sortedConstraints5 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 5
  %17 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_sortedConstraints5, align 4
  %18 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %17, i32 %18
  %19 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx6, align 4
  %call = call i32 @_Z24btGetConstraintIslandId2PK17btTypedConstraint(%class.btTypedConstraint* %19)
  %20 = load i32, i32* %islandId.addr, align 4
  %cmp7 = icmp eq i32 %call, %20
  br i1 %cmp7, label %if.then8, label %if.end

if.then8:                                         ; preds = %for.body
  %m_sortedConstraints9 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 5
  %21 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_sortedConstraints9, align 4
  %22 = load i32, i32* %i, align 4
  %arrayidx10 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %21, i32 %22
  store %class.btTypedConstraint** %arrayidx10, %class.btTypedConstraint*** %startConstraint, align 4
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %23 = load i32, i32* %i, align 4
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then8, %for.cond
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc22, %for.end
  %24 = load i32, i32* %i, align 4
  %m_numConstraints12 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 6
  %25 = load i32, i32* %m_numConstraints12, align 4
  %cmp13 = icmp slt i32 %24, %25
  br i1 %cmp13, label %for.body14, label %for.end24

for.body14:                                       ; preds = %for.cond11
  %m_sortedConstraints15 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 5
  %26 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_sortedConstraints15, align 4
  %27 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %26, i32 %27
  %28 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx16, align 4
  %call17 = call i32 @_Z24btGetConstraintIslandId2PK17btTypedConstraint(%class.btTypedConstraint* %28)
  %29 = load i32, i32* %islandId.addr, align 4
  %cmp18 = icmp eq i32 %call17, %29
  br i1 %cmp18, label %if.then19, label %if.end21

if.then19:                                        ; preds = %for.body14
  %30 = load i32, i32* %numCurConstraints, align 4
  %inc20 = add nsw i32 %30, 1
  store i32 %inc20, i32* %numCurConstraints, align 4
  br label %if.end21

if.end21:                                         ; preds = %if.then19, %for.body14
  br label %for.inc22

for.inc22:                                        ; preds = %if.end21
  %31 = load i32, i32* %i, align 4
  %inc23 = add nsw i32 %31, 1
  store i32 %inc23, i32* %i, align 4
  br label %for.cond11

for.end24:                                        ; preds = %for.cond11
  store i32 0, i32* %i, align 4
  br label %for.cond25

for.cond25:                                       ; preds = %for.inc36, %for.end24
  %32 = load i32, i32* %i, align 4
  %m_numMultiBodyConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 4
  %33 = load i32, i32* %m_numMultiBodyConstraints, align 4
  %cmp26 = icmp slt i32 %32, %33
  br i1 %cmp26, label %for.body27, label %for.end38

for.body27:                                       ; preds = %for.cond25
  %m_multiBodySortedConstraints28 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 3
  %34 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_multiBodySortedConstraints28, align 4
  %35 = load i32, i32* %i, align 4
  %arrayidx29 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %34, i32 %35
  %36 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx29, align 4
  %call30 = call i32 @_Z32btGetMultiBodyConstraintIslandIdPK21btMultiBodyConstraint(%class.btMultiBodyConstraint* %36)
  %37 = load i32, i32* %islandId.addr, align 4
  %cmp31 = icmp eq i32 %call30, %37
  br i1 %cmp31, label %if.then32, label %if.end35

if.then32:                                        ; preds = %for.body27
  %m_multiBodySortedConstraints33 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 3
  %38 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_multiBodySortedConstraints33, align 4
  %39 = load i32, i32* %i, align 4
  %arrayidx34 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %38, i32 %39
  store %class.btMultiBodyConstraint** %arrayidx34, %class.btMultiBodyConstraint*** %startMultiBodyConstraint, align 4
  br label %for.end38

if.end35:                                         ; preds = %for.body27
  br label %for.inc36

for.inc36:                                        ; preds = %if.end35
  %40 = load i32, i32* %i, align 4
  %inc37 = add nsw i32 %40, 1
  store i32 %inc37, i32* %i, align 4
  br label %for.cond25

for.end38:                                        ; preds = %if.then32, %for.cond25
  br label %for.cond39

for.cond39:                                       ; preds = %for.inc50, %for.end38
  %41 = load i32, i32* %i, align 4
  %m_numMultiBodyConstraints40 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 4
  %42 = load i32, i32* %m_numMultiBodyConstraints40, align 4
  %cmp41 = icmp slt i32 %41, %42
  br i1 %cmp41, label %for.body42, label %for.end52

for.body42:                                       ; preds = %for.cond39
  %m_multiBodySortedConstraints43 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 3
  %43 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_multiBodySortedConstraints43, align 4
  %44 = load i32, i32* %i, align 4
  %arrayidx44 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %43, i32 %44
  %45 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx44, align 4
  %call45 = call i32 @_Z32btGetMultiBodyConstraintIslandIdPK21btMultiBodyConstraint(%class.btMultiBodyConstraint* %45)
  %46 = load i32, i32* %islandId.addr, align 4
  %cmp46 = icmp eq i32 %call45, %46
  br i1 %cmp46, label %if.then47, label %if.end49

if.then47:                                        ; preds = %for.body42
  %47 = load i32, i32* %numCurMultiBodyConstraints, align 4
  %inc48 = add nsw i32 %47, 1
  store i32 %inc48, i32* %numCurMultiBodyConstraints, align 4
  br label %if.end49

if.end49:                                         ; preds = %if.then47, %for.body42
  br label %for.inc50

for.inc50:                                        ; preds = %if.end49
  %48 = load i32, i32* %i, align 4
  %inc51 = add nsw i32 %48, 1
  store i32 %inc51, i32* %i, align 4
  br label %for.cond39

for.end52:                                        ; preds = %for.cond39
  store i32 0, i32* %i, align 4
  br label %for.cond53

for.cond53:                                       ; preds = %for.inc57, %for.end52
  %49 = load i32, i32* %i, align 4
  %50 = load i32, i32* %numBodies.addr, align 4
  %cmp54 = icmp slt i32 %49, %50
  br i1 %cmp54, label %for.body55, label %for.end59

for.body55:                                       ; preds = %for.cond53
  %m_bodies = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 9
  %51 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %52 = load i32, i32* %i, align 4
  %arrayidx56 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %51, i32 %52
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_(%class.btAlignedObjectArray* %m_bodies, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %arrayidx56)
  br label %for.inc57

for.inc57:                                        ; preds = %for.body55
  %53 = load i32, i32* %i, align 4
  %inc58 = add nsw i32 %53, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond53

for.end59:                                        ; preds = %for.cond53
  store i32 0, i32* %i, align 4
  br label %for.cond60

for.cond60:                                       ; preds = %for.inc64, %for.end59
  %54 = load i32, i32* %i, align 4
  %55 = load i32, i32* %numManifolds.addr, align 4
  %cmp61 = icmp slt i32 %54, %55
  br i1 %cmp61, label %for.body62, label %for.end66

for.body62:                                       ; preds = %for.cond60
  %m_manifolds = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 10
  %56 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifolds.addr, align 4
  %57 = load i32, i32* %i, align 4
  %arrayidx63 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %56, i32 %57
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.13* %m_manifolds, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %arrayidx63)
  br label %for.inc64

for.inc64:                                        ; preds = %for.body62
  %58 = load i32, i32* %i, align 4
  %inc65 = add nsw i32 %58, 1
  store i32 %inc65, i32* %i, align 4
  br label %for.cond60

for.end66:                                        ; preds = %for.cond60
  store i32 0, i32* %i, align 4
  br label %for.cond67

for.cond67:                                       ; preds = %for.inc71, %for.end66
  %59 = load i32, i32* %i, align 4
  %60 = load i32, i32* %numCurConstraints, align 4
  %cmp68 = icmp slt i32 %59, %60
  br i1 %cmp68, label %for.body69, label %for.end73

for.body69:                                       ; preds = %for.cond67
  %m_constraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 11
  %61 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %startConstraint, align 4
  %62 = load i32, i32* %i, align 4
  %arrayidx70 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %61, i32 %62
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_(%class.btAlignedObjectArray.4* %m_constraints, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %arrayidx70)
  br label %for.inc71

for.inc71:                                        ; preds = %for.body69
  %63 = load i32, i32* %i, align 4
  %inc72 = add nsw i32 %63, 1
  store i32 %inc72, i32* %i, align 4
  br label %for.cond67

for.end73:                                        ; preds = %for.cond67
  store i32 0, i32* %i, align 4
  br label %for.cond74

for.cond74:                                       ; preds = %for.inc78, %for.end73
  %64 = load i32, i32* %i, align 4
  %65 = load i32, i32* %numCurMultiBodyConstraints, align 4
  %cmp75 = icmp slt i32 %64, %65
  br i1 %cmp75, label %for.body76, label %for.end80

for.body76:                                       ; preds = %for.cond74
  %m_multiBodyConstraints = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 12
  %66 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %startMultiBodyConstraint, align 4
  %67 = load i32, i32* %i, align 4
  %arrayidx77 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %66, i32 %67
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9push_backERKS1_(%class.btAlignedObjectArray.51* %m_multiBodyConstraints, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %arrayidx77)
  br label %for.inc78

for.inc78:                                        ; preds = %for.body76
  %68 = load i32, i32* %i, align 4
  %inc79 = add nsw i32 %68, 1
  store i32 %inc79, i32* %i, align 4
  br label %for.cond74

for.end80:                                        ; preds = %for.cond74
  %m_constraints81 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 11
  %call82 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %m_constraints81)
  %m_manifolds83 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 10
  %call84 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %m_manifolds83)
  %add = add nsw i32 %call82, %call84
  %m_solverInfo85 = getelementptr inbounds %struct.MultiBodyInplaceSolverIslandCallback, %struct.MultiBodyInplaceSolverIslandCallback* %this1, i32 0, i32 1
  %69 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %m_solverInfo85, align 4
  %70 = bitcast %struct.btContactSolverInfo* %69 to %struct.btContactSolverInfoData*
  %m_minimumSolverBatchSize = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %70, i32 0, i32 18
  %71 = load i32, i32* %m_minimumSolverBatchSize, align 4
  %cmp86 = icmp sgt i32 %add, %71
  br i1 %cmp86, label %if.then87, label %if.else88

if.then87:                                        ; preds = %for.end80
  call void @_ZN36MultiBodyInplaceSolverIslandCallback18processConstraintsEv(%struct.MultiBodyInplaceSolverIslandCallback* %this1)
  br label %if.end89

if.else88:                                        ; preds = %for.end80
  br label %if.end89

if.end89:                                         ; preds = %if.else88, %if.then87
  br label %if.end90

if.end90:                                         ; preds = %if.end89, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btSimulationIslandManager::IslandCallback"* @_ZN25btSimulationIslandManager14IslandCallbackD2Ev(%"struct.btSimulationIslandManager::IslandCallback"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSimulationIslandManager::IslandCallback"*, align 4
  store %"struct.btSimulationIslandManager::IslandCallback"* %this, %"struct.btSimulationIslandManager::IslandCallback"** %this.addr, align 4
  %this1 = load %"struct.btSimulationIslandManager::IslandCallback"*, %"struct.btSimulationIslandManager::IslandCallback"** %this.addr, align 4
  ret %"struct.btSimulationIslandManager::IslandCallback"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN25btSimulationIslandManager14IslandCallbackD0Ev(%"struct.btSimulationIslandManager::IslandCallback"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSimulationIslandManager::IslandCallback"*, align 4
  store %"struct.btSimulationIslandManager::IslandCallback"* %this, %"struct.btSimulationIslandManager::IslandCallback"** %this.addr, align 4
  %this1 = load %"struct.btSimulationIslandManager::IslandCallback"*, %"struct.btSimulationIslandManager::IslandCallback"** %this.addr, align 4
  call void @llvm.trap() #13
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btCollisionObject** null, %class.btCollisionObject*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.14* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.14* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.14*, align 4
  store %class.btAlignedAllocator.14* %this, %class.btAlignedAllocator.14** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.14*, %class.btAlignedAllocator.14** %this.addr, align 4
  ret %class.btAlignedAllocator.14* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.13* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EEC2Ev(%class.btAlignedAllocator.5* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  ret %class.btAlignedAllocator.5* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.13* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.13* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.13* %this1)
  ret %class.btAlignedObjectArray.13* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %tobool = icmp ne %class.btTypedConstraint** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.5* %m_allocator, %class.btTypedConstraint** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.5* %this, %class.btTypedConstraint** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca %class.btTypedConstraint**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store %class.btTypedConstraint** %ptr, %class.btTypedConstraint*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %ptr.addr, align 4
  %1 = bitcast %class.btTypedConstraint** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.13* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.13* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.13* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.13* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.13* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.13* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.14* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.14* %this, %class.btPersistentManifold** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.14*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator.14* %this, %class.btAlignedAllocator.14** %this.addr, align 4
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.14*, %class.btAlignedAllocator.14** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %tobool = icmp ne %class.btCollisionObject** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %m_allocator, %class.btCollisionObject** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btCollisionObject** null, %class.btCollisionObject*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %this, %class.btCollisionObject** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btCollisionObject**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %class.btCollisionObject** %ptr, %class.btCollisionObject*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %ptr.addr, align 4
  %1 = bitcast %class.btCollisionObject** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #7

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_Z24btGetConstraintIslandId2PK17btTypedConstraint(%class.btTypedConstraint* %lhs) #2 comdat {
entry:
  %lhs.addr = alloca %class.btTypedConstraint*, align 4
  %islandId = alloca i32, align 4
  %rcolObj0 = alloca %class.btCollisionObject*, align 4
  %rcolObj1 = alloca %class.btCollisionObject*, align 4
  store %class.btTypedConstraint* %lhs, %class.btTypedConstraint** %lhs.addr, align 4
  %0 = load %class.btTypedConstraint*, %class.btTypedConstraint** %lhs.addr, align 4
  %call = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %0)
  %1 = bitcast %class.btRigidBody* %call to %class.btCollisionObject*
  store %class.btCollisionObject* %1, %class.btCollisionObject** %rcolObj0, align 4
  %2 = load %class.btTypedConstraint*, %class.btTypedConstraint** %lhs.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %2)
  %3 = bitcast %class.btRigidBody* %call1 to %class.btCollisionObject*
  store %class.btCollisionObject* %3, %class.btCollisionObject** %rcolObj1, align 4
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj0, align 4
  %call2 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %4)
  %cmp = icmp sge i32 %call2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj0, align 4
  %call3 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %5)
  br label %cond.end

cond.false:                                       ; preds = %entry
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj1, align 4
  %call4 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %6)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call3, %cond.true ], [ %call4, %cond.false ]
  store i32 %cond, i32* %islandId, align 4
  %7 = load i32, i32* %islandId, align 4
  ret i32 %7
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_Z32btGetMultiBodyConstraintIslandIdPK21btMultiBodyConstraint(%class.btMultiBodyConstraint* %lhs) #2 comdat {
entry:
  %lhs.addr = alloca %class.btMultiBodyConstraint*, align 4
  %islandId = alloca i32, align 4
  %islandTagA = alloca i32, align 4
  %islandTagB = alloca i32, align 4
  store %class.btMultiBodyConstraint* %lhs, %class.btMultiBodyConstraint** %lhs.addr, align 4
  %0 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %lhs.addr, align 4
  %1 = bitcast %class.btMultiBodyConstraint* %0 to i32 (%class.btMultiBodyConstraint*)***
  %vtable = load i32 (%class.btMultiBodyConstraint*)**, i32 (%class.btMultiBodyConstraint*)*** %1, align 4
  %vfn = getelementptr inbounds i32 (%class.btMultiBodyConstraint*)*, i32 (%class.btMultiBodyConstraint*)** %vtable, i64 5
  %2 = load i32 (%class.btMultiBodyConstraint*)*, i32 (%class.btMultiBodyConstraint*)** %vfn, align 4
  %call = call i32 %2(%class.btMultiBodyConstraint* %0)
  store i32 %call, i32* %islandTagA, align 4
  %3 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %lhs.addr, align 4
  %4 = bitcast %class.btMultiBodyConstraint* %3 to i32 (%class.btMultiBodyConstraint*)***
  %vtable1 = load i32 (%class.btMultiBodyConstraint*)**, i32 (%class.btMultiBodyConstraint*)*** %4, align 4
  %vfn2 = getelementptr inbounds i32 (%class.btMultiBodyConstraint*)*, i32 (%class.btMultiBodyConstraint*)** %vtable1, i64 6
  %5 = load i32 (%class.btMultiBodyConstraint*)*, i32 (%class.btMultiBodyConstraint*)** %vfn2, align 4
  %call3 = call i32 %5(%class.btMultiBodyConstraint* %3)
  store i32 %call3, i32* %islandTagB, align 4
  %6 = load i32, i32* %islandTagA, align 4
  %cmp = icmp sge i32 %6, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %7 = load i32, i32* %islandTagA, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %8 = load i32, i32* %islandTagB, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %7, %cond.true ], [ %8, %cond.false ]
  store i32 %cond, i32* %islandId, align 4
  %9 = load i32, i32* %islandId, align 4
  ret i32 %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_(%class.btAlignedObjectArray* %this, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.btCollisionObject**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btCollisionObject** %_Val, %class.btCollisionObject*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %1, i32 %2
  %3 = bitcast %class.btCollisionObject** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btCollisionObject**
  %5 = load %class.btCollisionObject**, %class.btCollisionObject*** %_Val.addr, align 4
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %5, align 4
  store %class.btCollisionObject* %6, %class.btCollisionObject** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.13* %this, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %_Val.addr = alloca %class.btPersistentManifold**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store %class.btPersistentManifold** %_Val, %class.btPersistentManifold*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.13* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.13* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.13* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %1 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %1, i32 %2
  %3 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btPersistentManifold**
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %_Val.addr, align 4
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %5, align 4
  store %class.btPersistentManifold* %6, %class.btPersistentManifold** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_(%class.btAlignedObjectArray.4* %this, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Val.addr = alloca %class.btTypedConstraint**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store %class.btTypedConstraint** %_Val, %class.btTypedConstraint*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9allocSizeEi(%class.btAlignedObjectArray.4* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %1 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %1, i32 %2
  %3 = bitcast %class.btTypedConstraint** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btTypedConstraint**
  %5 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %_Val.addr, align 4
  %6 = load %class.btTypedConstraint*, %class.btTypedConstraint** %5, align 4
  store %class.btTypedConstraint* %6, %class.btTypedConstraint** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 8
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  ret %class.btRigidBody* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 9
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  ret %class.btRigidBody* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btCollisionObject**, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btCollisionObject**
  store %class.btCollisionObject** %2, %class.btCollisionObject*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btCollisionObject** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btCollisionObject**, %class.btCollisionObject*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btCollisionObject** %4, %class.btCollisionObject*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btCollisionObject** @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btCollisionObject*** null)
  %2 = bitcast %class.btCollisionObject** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btCollisionObject** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btCollisionObject**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btCollisionObject** %dest, %class.btCollisionObject*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %3, i32 %4
  %5 = bitcast %class.btCollisionObject** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btCollisionObject**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %7, i32 %8
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %arrayidx2, align 4
  store %class.btCollisionObject* %9, %class.btCollisionObject** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionObject** @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %this, i32 %n, %class.btCollisionObject*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btCollisionObject***, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btCollisionObject*** %hint, %class.btCollisionObject**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btCollisionObject**
  ret %class.btCollisionObject** %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.13* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.13* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.13* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.13* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %2, %class.btPersistentManifold*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.13* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.13* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.13* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %4, %class.btPersistentManifold*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.13* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.13* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.14* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.13* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  %5 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %7 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %7, i32 %8
  %9 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4
  store %class.btPersistentManifold* %9, %class.btPersistentManifold** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.14* %this, i32 %n, %class.btPersistentManifold*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.14*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator.14* %this, %class.btAlignedAllocator.14** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.14*, %class.btAlignedAllocator.14** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btTypedConstraint**, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP17btTypedConstraintE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btTypedConstraint**
  store %class.btTypedConstraint** %2, %class.btTypedConstraint*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, %class.btTypedConstraint** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btTypedConstraint** %4, %class.btTypedConstraint*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9allocSizeEi(%class.btAlignedObjectArray.4* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP17btTypedConstraintE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btTypedConstraint** @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.5* %m_allocator, i32 %1, %class.btTypedConstraint*** null)
  %2 = bitcast %class.btTypedConstraint** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, %class.btTypedConstraint** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btTypedConstraint**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btTypedConstraint** %dest, %class.btTypedConstraint*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %3, i32 %4
  %5 = bitcast %class.btTypedConstraint** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btTypedConstraint**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %7 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %7, i32 %8
  %9 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx2, align 4
  store %class.btTypedConstraint* %9, %class.btTypedConstraint** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTypedConstraint** @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.5* %this, i32 %n, %class.btTypedConstraint*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btTypedConstraint***, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btTypedConstraint*** %hint, %class.btTypedConstraint**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btTypedConstraint**
  ret %class.btTypedConstraint** %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_(%class.btAlignedObjectArray* %this, i32 %newsize, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btCollisionObject**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btCollisionObject** %fillData, %class.btCollisionObject*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %14 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %14, i32 %15
  %16 = bitcast %class.btCollisionObject** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btCollisionObject**
  %18 = load %class.btCollisionObject**, %class.btCollisionObject*** %fillData.addr, align 4
  %19 = load %class.btCollisionObject*, %class.btCollisionObject** %18, align 4
  store %class.btCollisionObject* %19, %class.btCollisionObject** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.13* %this, i32 %newsize, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btPersistentManifold**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btPersistentManifold** %fillData, %class.btPersistentManifold*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.13* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %14 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %14, i32 %15
  %16 = bitcast %class.btPersistentManifold** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btPersistentManifold**
  %18 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %fillData.addr, align 4
  %19 = load %class.btPersistentManifold*, %class.btPersistentManifold** %18, align 4
  store %class.btPersistentManifold* %19, %class.btPersistentManifold** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.38* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z7btClampIfEvRT_RKS0_S3_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %lb, float* nonnull align 4 dereferenceable(4) %ub) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %lb.addr = alloca float*, align 4
  %ub.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %lb, float** %lb.addr, align 4
  store float* %ub, float** %ub.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %lb.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load float*, float** %lb.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = load float*, float** %a.addr, align 4
  store float %5, float* %6, align 4
  br label %if.end3

if.else:                                          ; preds = %entry
  %7 = load float*, float** %ub.addr, align 4
  %8 = load float, float* %7, align 4
  %9 = load float*, float** %a.addr, align 4
  %10 = load float, float* %9, align 4
  %cmp1 = fcmp olt float %8, %10
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.else
  %11 = load float*, float** %ub.addr, align 4
  %12 = load float, float* %11, align 4
  %13 = load float*, float** %a.addr, align 4
  store float %12, float* %13, align 4
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.else
  br label %if.end3

if.end3:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q2) #2 comdat {
entry:
  %q1.addr = alloca %class.btQuaternion*, align 4
  %q2.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  store %class.btQuaternion* %q1, %class.btQuaternion** %q1.addr, align 4
  store %class.btQuaternion* %q2, %class.btQuaternion** %q2.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %1 = bitcast %class.btQuaternion* %0 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %1)
  %2 = load float, float* %call, align 4
  %3 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %4 = bitcast %class.btQuaternion* %3 to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %4)
  %5 = load float, float* %call1, align 4
  %mul = fmul float %2, %5
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %7)
  %8 = load float, float* %call2, align 4
  %9 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %10 = bitcast %class.btQuaternion* %9 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %10)
  %11 = load float, float* %call3, align 4
  %mul4 = fmul float %8, %11
  %add = fadd float %mul, %mul4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %13)
  %14 = load float, float* %call5, align 4
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %16)
  %17 = load float, float* %call6, align 4
  %mul7 = fmul float %14, %17
  %add8 = fadd float %add, %mul7
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %22 = bitcast %class.btQuaternion* %21 to %class.btQuadWord*
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %22)
  %23 = load float, float* %call10, align 4
  %mul11 = fmul float %20, %23
  %sub = fsub float %add8, %mul11
  store float %sub, float* %ref.tmp, align 4
  %24 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %25 = bitcast %class.btQuaternion* %24 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %25)
  %26 = load float, float* %call13, align 4
  %27 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %28 = bitcast %class.btQuaternion* %27 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %28)
  %29 = load float, float* %call14, align 4
  %mul15 = fmul float %26, %29
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %31)
  %32 = load float, float* %call16, align 4
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %34)
  %35 = load float, float* %call17, align 4
  %mul18 = fmul float %32, %35
  %add19 = fadd float %mul15, %mul18
  %36 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %37 = bitcast %class.btQuaternion* %36 to %class.btQuadWord*
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %37)
  %38 = load float, float* %call20, align 4
  %39 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %40 = bitcast %class.btQuaternion* %39 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %40)
  %41 = load float, float* %call21, align 4
  %mul22 = fmul float %38, %41
  %add23 = fadd float %add19, %mul22
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %43)
  %44 = load float, float* %call24, align 4
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %46)
  %47 = load float, float* %call25, align 4
  %mul26 = fmul float %44, %47
  %sub27 = fsub float %add23, %mul26
  store float %sub27, float* %ref.tmp12, align 4
  %48 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %49 = bitcast %class.btQuaternion* %48 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %49)
  %50 = load float, float* %call29, align 4
  %51 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %52 = bitcast %class.btQuaternion* %51 to %class.btQuadWord*
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %52)
  %53 = load float, float* %call30, align 4
  %mul31 = fmul float %50, %53
  %54 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %55 = bitcast %class.btQuaternion* %54 to %class.btQuadWord*
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %55)
  %56 = load float, float* %call32, align 4
  %57 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %58 = bitcast %class.btQuaternion* %57 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %58)
  %59 = load float, float* %call33, align 4
  %mul34 = fmul float %56, %59
  %add35 = fadd float %mul31, %mul34
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %61)
  %62 = load float, float* %call36, align 4
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %64)
  %65 = load float, float* %call37, align 4
  %mul38 = fmul float %62, %65
  %add39 = fadd float %add35, %mul38
  %66 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %67 = bitcast %class.btQuaternion* %66 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %67)
  %68 = load float, float* %call40, align 4
  %69 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %70 = bitcast %class.btQuaternion* %69 to %class.btQuadWord*
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %70)
  %71 = load float, float* %call41, align 4
  %mul42 = fmul float %68, %71
  %sub43 = fsub float %add39, %mul42
  store float %sub43, float* %ref.tmp28, align 4
  %72 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %73 = bitcast %class.btQuaternion* %72 to %class.btQuadWord*
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %73)
  %74 = load float, float* %call45, align 4
  %75 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %76 = bitcast %class.btQuaternion* %75 to %class.btQuadWord*
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %76)
  %77 = load float, float* %call46, align 4
  %mul47 = fmul float %74, %77
  %78 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %79 = bitcast %class.btQuaternion* %78 to %class.btQuadWord*
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %79)
  %80 = load float, float* %call48, align 4
  %81 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %82 = bitcast %class.btQuaternion* %81 to %class.btQuadWord*
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %82)
  %83 = load float, float* %call49, align 4
  %mul50 = fmul float %80, %83
  %sub51 = fsub float %mul47, %mul50
  %84 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %85 = bitcast %class.btQuaternion* %84 to %class.btQuadWord*
  %call52 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %85)
  %86 = load float, float* %call52, align 4
  %87 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %88 = bitcast %class.btQuaternion* %87 to %class.btQuadWord*
  %call53 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %88)
  %89 = load float, float* %call53, align 4
  %mul54 = fmul float %86, %89
  %sub55 = fsub float %sub51, %mul54
  %90 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %91 = bitcast %class.btQuaternion* %90 to %class.btQuadWord*
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %91)
  %92 = load float, float* %call56, align 4
  %93 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %94 = bitcast %class.btQuaternion* %93 to %class.btQuadWord*
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %94)
  %95 = load float, float* %call57, align 4
  %mul58 = fmul float %92, %95
  %sub59 = fsub float %sub55, %mul58
  store float %sub59, float* %ref.tmp44, align 4
  %call60 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp44)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btMultibodyLink10getAxisTopEi(%struct.btMultibodyLink* %this, i32 %dof) #1 comdat {
entry:
  %this.addr = alloca %struct.btMultibodyLink*, align 4
  %dof.addr = alloca i32, align 4
  store %struct.btMultibodyLink* %this, %struct.btMultibodyLink** %this.addr, align 4
  store i32 %dof, i32* %dof.addr, align 4
  %this1 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %this.addr, align 4
  %m_axes = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 8
  %0 = load i32, i32* %dof.addr, align 4
  %arrayidx = getelementptr inbounds [6 x %struct.btSpatialMotionVector], [6 x %struct.btSpatialMotionVector]* %m_axes, i32 0, i32 %0
  %m_topVec = getelementptr inbounds %struct.btSpatialMotionVector, %struct.btSpatialMotionVector* %arrayidx, i32 0, i32 0
  ret %class.btVector3* %m_topVec
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_axis, float* nonnull align 4 dereferenceable(4) %_angle) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btVector3* %_axis, %class.btVector3** %_axis.addr, align 4
  store float* %_angle, float** %_angle.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  %1 = load %class.btVector3*, %class.btVector3** %_axis.addr, align 4
  %2 = load float*, float** %_angle.addr, align 4
  call void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %2)
  ret %class.btQuaternion* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #8

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btMultibodyLink13getAxisBottomEi(%struct.btMultibodyLink* %this, i32 %dof) #1 comdat {
entry:
  %this.addr = alloca %struct.btMultibodyLink*, align 4
  %dof.addr = alloca i32, align 4
  store %struct.btMultibodyLink* %this, %struct.btMultibodyLink** %this.addr, align 4
  store i32 %dof, i32* %dof.addr, align 4
  %this1 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %this.addr, align 4
  %m_axes = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %this1, i32 0, i32 8
  %0 = load i32, i32* %dof.addr, align 4
  %arrayidx = getelementptr inbounds [6 x %struct.btSpatialMotionVector], [6 x %struct.btSpatialMotionVector]* %m_axes, i32 0, i32 %0
  %m_bottomVec = getelementptr inbounds %struct.btSpatialMotionVector, %struct.btSpatialMotionVector* %arrayidx, i32 0, i32 1
  ret %class.btVector3* %m_bottomVec
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float*, float** %_z.addr, align 4
  %4 = load float*, float** %_w.addr, align 4
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %_angle) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4
  store float* %_angle, float** %_angle.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %0)
  store float %call, float* %d, align 4
  %1 = load float*, float** %_angle.addr, align 4
  %2 = load float, float* %1, align 4
  %mul = fmul float %2, 5.000000e-01
  %call2 = call float @_Z5btSinf(float %mul)
  %3 = load float, float* %d, align 4
  %div = fdiv float %call2, %3
  store float %div, float* %s, align 4
  %4 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %5 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %5)
  %6 = load float, float* %call3, align 4
  %7 = load float, float* %s, align 4
  %mul4 = fmul float %6, %7
  store float %mul4, float* %ref.tmp, align 4
  %8 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %8)
  %9 = load float, float* %call6, align 4
  %10 = load float, float* %s, align 4
  %mul7 = fmul float %9, %10
  store float %mul7, float* %ref.tmp5, align 4
  %11 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %11)
  %12 = load float, float* %call9, align 4
  %13 = load float, float* %s, align 4
  %mul10 = fmul float %12, %13
  store float %mul10, float* %ref.tmp8, align 4
  %14 = load float*, float** %_angle.addr, align 4
  %15 = load float, float* %14, align 4
  %mul12 = fmul float %15, 5.000000e-01
  %call13 = call float @_Z5btCosf(float %mul12)
  store float %call13, float* %ref.tmp11, align 4
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %4, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btSinf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.sin.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btCosf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #9

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sin.f32(float) #9

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #9

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %0, i32 %1
  ret %class.btCollisionObject** %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats3 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [4 x float], [4 x float]* %m_floats3, i32 0, i32 1
  %3 = load float, float* %arrayidx4, align 4
  %fneg5 = fneg float %3
  store float %fneg5, float* %ref.tmp2, align 4
  %4 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %4, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 2
  %5 = load float, float* %arrayidx8, align 4
  %fneg9 = fneg float %5
  store float %fneg9, float* %ref.tmp6, align 4
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats10 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 3
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %0)
  store float %call, float* %d, align 4
  %1 = load float, float* %d, align 4
  %div = fdiv float 2.000000e+00, %1
  store float %div, float* %s, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call2, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %4, %5
  store float %mul, float* %xs, align 4
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load float, float* %call3, align 4
  %9 = load float, float* %s, align 4
  %mul4 = fmul float %8, %9
  store float %mul4, float* %ys, align 4
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call5, align 4
  %13 = load float, float* %s, align 4
  %mul6 = fmul float %12, %13
  store float %mul6, float* %zs, align 4
  %14 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %15 = bitcast %class.btQuaternion* %14 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %15)
  %16 = load float, float* %call7, align 4
  %17 = load float, float* %xs, align 4
  %mul8 = fmul float %16, %17
  store float %mul8, float* %wx, align 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load float, float* %ys, align 4
  %mul10 = fmul float %20, %21
  store float %mul10, float* %wy, align 4
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %23)
  %24 = load float, float* %call11, align 4
  %25 = load float, float* %zs, align 4
  %mul12 = fmul float %24, %25
  store float %mul12, float* %wz, align 4
  %26 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %27 = bitcast %class.btQuaternion* %26 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %27)
  %28 = load float, float* %call13, align 4
  %29 = load float, float* %xs, align 4
  %mul14 = fmul float %28, %29
  store float %mul14, float* %xx, align 4
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %31)
  %32 = load float, float* %call15, align 4
  %33 = load float, float* %ys, align 4
  %mul16 = fmul float %32, %33
  store float %mul16, float* %xy, align 4
  %34 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %35 = bitcast %class.btQuaternion* %34 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %35)
  %36 = load float, float* %call17, align 4
  %37 = load float, float* %zs, align 4
  %mul18 = fmul float %36, %37
  store float %mul18, float* %xz, align 4
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %39)
  %40 = load float, float* %call19, align 4
  %41 = load float, float* %ys, align 4
  %mul20 = fmul float %40, %41
  store float %mul20, float* %yy, align 4
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %43)
  %44 = load float, float* %call21, align 4
  %45 = load float, float* %zs, align 4
  %mul22 = fmul float %44, %45
  store float %mul22, float* %yz, align 4
  %46 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %47 = bitcast %class.btQuaternion* %46 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %47)
  %48 = load float, float* %call23, align 4
  %49 = load float, float* %zs, align 4
  %mul24 = fmul float %48, %49
  store float %mul24, float* %zz, align 4
  %50 = load float, float* %yy, align 4
  %51 = load float, float* %zz, align 4
  %add = fadd float %50, %51
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4
  %52 = load float, float* %xy, align 4
  %53 = load float, float* %wz, align 4
  %sub26 = fsub float %52, %53
  store float %sub26, float* %ref.tmp25, align 4
  %54 = load float, float* %xz, align 4
  %55 = load float, float* %wy, align 4
  %add28 = fadd float %54, %55
  store float %add28, float* %ref.tmp27, align 4
  %56 = load float, float* %xy, align 4
  %57 = load float, float* %wz, align 4
  %add30 = fadd float %56, %57
  store float %add30, float* %ref.tmp29, align 4
  %58 = load float, float* %xx, align 4
  %59 = load float, float* %zz, align 4
  %add32 = fadd float %58, %59
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4
  %60 = load float, float* %yz, align 4
  %61 = load float, float* %wx, align 4
  %sub35 = fsub float %60, %61
  store float %sub35, float* %ref.tmp34, align 4
  %62 = load float, float* %xz, align 4
  %63 = load float, float* %wy, align 4
  %sub37 = fsub float %62, %63
  store float %sub37, float* %ref.tmp36, align 4
  %64 = load float, float* %yz, align 4
  %65 = load float, float* %wx, align 4
  %add39 = fadd float %64, %65
  store float %add39, float* %ref.tmp38, align 4
  %66 = load float, float* %xx, align 4
  %67 = load float, float* %yy, align 4
  %add41 = fadd float %66, %67
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %w) #2 comdat {
entry:
  %q.addr = alloca %class.btQuaternion*, align 4
  %w.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  store %class.btVector3* %w, %class.btVector3** %w.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %1 = bitcast %class.btQuaternion* %0 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %1)
  %2 = load float, float* %call, align 4
  %3 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %3)
  %4 = load float, float* %call1, align 4
  %mul = fmul float %2, %4
  %5 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %6 = bitcast %class.btQuaternion* %5 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %6)
  %7 = load float, float* %call2, align 4
  %8 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %8)
  %9 = load float, float* %call3, align 4
  %mul4 = fmul float %7, %9
  %add = fadd float %mul, %mul4
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call5, align 4
  %13 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %13)
  %14 = load float, float* %call6, align 4
  %mul7 = fmul float %12, %14
  %sub = fsub float %add, %mul7
  store float %sub, float* %ref.tmp, align 4
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %16)
  %17 = load float, float* %call9, align 4
  %18 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %18)
  %19 = load float, float* %call10, align 4
  %mul11 = fmul float %17, %19
  %20 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %21 = bitcast %class.btQuaternion* %20 to %class.btQuadWord*
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %21)
  %22 = load float, float* %call12, align 4
  %23 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %23)
  %24 = load float, float* %call13, align 4
  %mul14 = fmul float %22, %24
  %add15 = fadd float %mul11, %mul14
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %26)
  %27 = load float, float* %call16, align 4
  %28 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %28)
  %29 = load float, float* %call17, align 4
  %mul18 = fmul float %27, %29
  %sub19 = fsub float %add15, %mul18
  store float %sub19, float* %ref.tmp8, align 4
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %31)
  %32 = load float, float* %call21, align 4
  %33 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %33)
  %34 = load float, float* %call22, align 4
  %mul23 = fmul float %32, %34
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call24, align 4
  %38 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %38)
  %39 = load float, float* %call25, align 4
  %mul26 = fmul float %37, %39
  %add27 = fadd float %mul23, %mul26
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %41)
  %42 = load float, float* %call28, align 4
  %43 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %43)
  %44 = load float, float* %call29, align 4
  %mul30 = fmul float %42, %44
  %sub31 = fsub float %add27, %mul30
  store float %sub31, float* %ref.tmp20, align 4
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %46)
  %47 = load float, float* %call33, align 4
  %fneg = fneg float %47
  %48 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %48)
  %49 = load float, float* %call34, align 4
  %mul35 = fmul float %fneg, %49
  %50 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %51 = bitcast %class.btQuaternion* %50 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %51)
  %52 = load float, float* %call36, align 4
  %53 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %53)
  %54 = load float, float* %call37, align 4
  %mul38 = fmul float %52, %54
  %sub39 = fsub float %mul35, %mul38
  %55 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %56 = bitcast %class.btQuaternion* %55 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %56)
  %57 = load float, float* %call40, align 4
  %58 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %58)
  %59 = load float, float* %call41, align 4
  %mul42 = fmul float %57, %59
  %sub43 = fsub float %sub39, %mul42
  store float %sub43, float* %ref.tmp32, align 4
  %call44 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp58 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %2 = load float, float* %arrayidx, align 4
  %3 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %4 = bitcast %class.btQuaternion* %3 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %4)
  %5 = load float, float* %call, align 4
  %mul = fmul float %2, %5
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %7 = load float, float* %arrayidx3, align 4
  %8 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %9 = bitcast %class.btQuaternion* %8 to %class.btQuadWord*
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %9, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 3
  %10 = load float, float* %arrayidx5, align 4
  %mul6 = fmul float %7, %10
  %add = fadd float %mul, %mul6
  %11 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %11, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %12 = load float, float* %arrayidx8, align 4
  %13 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %14 = bitcast %class.btQuaternion* %13 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %14)
  %15 = load float, float* %call9, align 4
  %mul10 = fmul float %12, %15
  %add11 = fadd float %add, %mul10
  %16 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats12 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %16, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %17 = load float, float* %arrayidx13, align 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %19)
  %20 = load float, float* %call14, align 4
  %mul15 = fmul float %17, %20
  %sub = fsub float %add11, %mul15
  store float %sub, float* %ref.tmp, align 4
  %21 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats17 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %21, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 3
  %22 = load float, float* %arrayidx18, align 4
  %23 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %24 = bitcast %class.btQuaternion* %23 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %24)
  %25 = load float, float* %call19, align 4
  %mul20 = fmul float %22, %25
  %26 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats21 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %26, i32 0, i32 0
  %arrayidx22 = getelementptr inbounds [4 x float], [4 x float]* %m_floats21, i32 0, i32 1
  %27 = load float, float* %arrayidx22, align 4
  %28 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %29 = bitcast %class.btQuaternion* %28 to %class.btQuadWord*
  %m_floats23 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %29, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [4 x float], [4 x float]* %m_floats23, i32 0, i32 3
  %30 = load float, float* %arrayidx24, align 4
  %mul25 = fmul float %27, %30
  %add26 = fadd float %mul20, %mul25
  %31 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats27 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %31, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 2
  %32 = load float, float* %arrayidx28, align 4
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %34)
  %35 = load float, float* %call29, align 4
  %mul30 = fmul float %32, %35
  %add31 = fadd float %add26, %mul30
  %36 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats32 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %36, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [4 x float], [4 x float]* %m_floats32, i32 0, i32 0
  %37 = load float, float* %arrayidx33, align 4
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %39)
  %40 = load float, float* %call34, align 4
  %mul35 = fmul float %37, %40
  %sub36 = fsub float %add31, %mul35
  store float %sub36, float* %ref.tmp16, align 4
  %41 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats38 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %41, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [4 x float], [4 x float]* %m_floats38, i32 0, i32 3
  %42 = load float, float* %arrayidx39, align 4
  %43 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %44 = bitcast %class.btQuaternion* %43 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %44)
  %45 = load float, float* %call40, align 4
  %mul41 = fmul float %42, %45
  %46 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats42 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %46, i32 0, i32 0
  %arrayidx43 = getelementptr inbounds [4 x float], [4 x float]* %m_floats42, i32 0, i32 2
  %47 = load float, float* %arrayidx43, align 4
  %48 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %49 = bitcast %class.btQuaternion* %48 to %class.btQuadWord*
  %m_floats44 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %49, i32 0, i32 0
  %arrayidx45 = getelementptr inbounds [4 x float], [4 x float]* %m_floats44, i32 0, i32 3
  %50 = load float, float* %arrayidx45, align 4
  %mul46 = fmul float %47, %50
  %add47 = fadd float %mul41, %mul46
  %51 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats48 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %51, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [4 x float], [4 x float]* %m_floats48, i32 0, i32 0
  %52 = load float, float* %arrayidx49, align 4
  %53 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %54 = bitcast %class.btQuaternion* %53 to %class.btQuadWord*
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %54)
  %55 = load float, float* %call50, align 4
  %mul51 = fmul float %52, %55
  %add52 = fadd float %add47, %mul51
  %56 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats53 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %56, i32 0, i32 0
  %arrayidx54 = getelementptr inbounds [4 x float], [4 x float]* %m_floats53, i32 0, i32 1
  %57 = load float, float* %arrayidx54, align 4
  %58 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %59 = bitcast %class.btQuaternion* %58 to %class.btQuadWord*
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %59)
  %60 = load float, float* %call55, align 4
  %mul56 = fmul float %57, %60
  %sub57 = fsub float %add52, %mul56
  store float %sub57, float* %ref.tmp37, align 4
  %61 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats59 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %61, i32 0, i32 0
  %arrayidx60 = getelementptr inbounds [4 x float], [4 x float]* %m_floats59, i32 0, i32 3
  %62 = load float, float* %arrayidx60, align 4
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %m_floats61 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %64, i32 0, i32 0
  %arrayidx62 = getelementptr inbounds [4 x float], [4 x float]* %m_floats61, i32 0, i32 3
  %65 = load float, float* %arrayidx62, align 4
  %mul63 = fmul float %62, %65
  %66 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats64 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %66, i32 0, i32 0
  %arrayidx65 = getelementptr inbounds [4 x float], [4 x float]* %m_floats64, i32 0, i32 0
  %67 = load float, float* %arrayidx65, align 4
  %68 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %69 = bitcast %class.btQuaternion* %68 to %class.btQuadWord*
  %call66 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %69)
  %70 = load float, float* %call66, align 4
  %mul67 = fmul float %67, %70
  %sub68 = fsub float %mul63, %mul67
  %71 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats69 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %71, i32 0, i32 0
  %arrayidx70 = getelementptr inbounds [4 x float], [4 x float]* %m_floats69, i32 0, i32 1
  %72 = load float, float* %arrayidx70, align 4
  %73 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %74 = bitcast %class.btQuaternion* %73 to %class.btQuadWord*
  %call71 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %74)
  %75 = load float, float* %call71, align 4
  %mul72 = fmul float %72, %75
  %sub73 = fsub float %sub68, %mul72
  %76 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats74 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %76, i32 0, i32 0
  %arrayidx75 = getelementptr inbounds [4 x float], [4 x float]* %m_floats74, i32 0, i32 2
  %77 = load float, float* %arrayidx75, align 4
  %78 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %79 = bitcast %class.btQuaternion* %78 to %class.btQuadWord*
  %call76 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %79)
  %80 = load float, float* %call76, align 4
  %mul77 = fmul float %77, %80
  %sub78 = fsub float %sub73, %mul77
  store float %sub78, float* %ref.tmp58, align 4
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp58)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %trace = alloca float, align 4
  %temp = alloca [4 x float], align 16
  %s = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %s64 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx3)
  %1 = load float, float* %call4, align 4
  %add = fadd float %0, %1
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx6)
  %2 = load float, float* %call7, align 4
  %add8 = fadd float %add, %2
  store float %add8, float* %trace, align 4
  %3 = load float, float* %trace, align 4
  %cmp = fcmp ogt float %3, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load float, float* %trace, align 4
  %add9 = fadd float %4, 1.000000e+00
  %call10 = call float @_Z6btSqrtf(float %add9)
  store float %call10, float* %s, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %5, 5.000000e-01
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul, float* %arrayidx11, align 4
  %6 = load float, float* %s, align 4
  %div = fdiv float 5.000000e-01, %6
  store float %div, float* %s, align 4
  %m_el12 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el12, i32 0, i32 2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx13)
  %7 = load float, float* %call14, align 4
  %m_el15 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el15, i32 0, i32 1
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx16)
  %8 = load float, float* %call17, align 4
  %sub = fsub float %7, %8
  %9 = load float, float* %s, align 4
  %mul18 = fmul float %sub, %9
  %arrayidx19 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  store float %mul18, float* %arrayidx19, align 16
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 0
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %10 = load float, float* %call22, align 4
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx24)
  %11 = load float, float* %call25, align 4
  %sub26 = fsub float %10, %11
  %12 = load float, float* %s, align 4
  %mul27 = fmul float %sub26, %12
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  store float %mul27, float* %arrayidx28, align 4
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 1
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %13 = load float, float* %call31, align 4
  %m_el32 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el32, i32 0, i32 0
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx33)
  %14 = load float, float* %call34, align 4
  %sub35 = fsub float %13, %14
  %15 = load float, float* %s, align 4
  %mul36 = fmul float %sub35, %15
  %arrayidx37 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  store float %mul36, float* %arrayidx37, align 8
  br label %if.end

if.else:                                          ; preds = %entry
  %m_el38 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el38, i32 0, i32 0
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx39)
  %16 = load float, float* %call40, align 4
  %m_el41 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx42 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el41, i32 0, i32 1
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx42)
  %17 = load float, float* %call43, align 4
  %cmp44 = fcmp olt float %16, %17
  br i1 %cmp44, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 1
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx46)
  %18 = load float, float* %call47, align 4
  %m_el48 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el48, i32 0, i32 2
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx49)
  %19 = load float, float* %call50, align 4
  %cmp51 = fcmp olt float %18, %19
  %20 = zext i1 %cmp51 to i64
  %cond = select i1 %cmp51, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 0
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx53)
  %21 = load float, float* %call54, align 4
  %m_el55 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el55, i32 0, i32 2
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx56)
  %22 = load float, float* %call57, align 4
  %cmp58 = fcmp olt float %21, %22
  %23 = zext i1 %cmp58 to i64
  %cond59 = select i1 %cmp58, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond60 = phi i32 [ %cond, %cond.true ], [ %cond59, %cond.false ]
  store i32 %cond60, i32* %i, align 4
  %24 = load i32, i32* %i, align 4
  %add61 = add nsw i32 %24, 1
  %rem = srem i32 %add61, 3
  store i32 %rem, i32* %j, align 4
  %25 = load i32, i32* %i, align 4
  %add62 = add nsw i32 %25, 2
  %rem63 = srem i32 %add62, 3
  store i32 %rem63, i32* %k, align 4
  %m_el65 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %26 = load i32, i32* %i, align 4
  %arrayidx66 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el65, i32 0, i32 %26
  %call67 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx66)
  %27 = load i32, i32* %i, align 4
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 %27
  %28 = load float, float* %arrayidx68, align 4
  %m_el69 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %29 = load i32, i32* %j, align 4
  %arrayidx70 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el69, i32 0, i32 %29
  %call71 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx70)
  %30 = load i32, i32* %j, align 4
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 %30
  %31 = load float, float* %arrayidx72, align 4
  %sub73 = fsub float %28, %31
  %m_el74 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %32 = load i32, i32* %k, align 4
  %arrayidx75 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el74, i32 0, i32 %32
  %call76 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx75)
  %33 = load i32, i32* %k, align 4
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 %33
  %34 = load float, float* %arrayidx77, align 4
  %sub78 = fsub float %sub73, %34
  %add79 = fadd float %sub78, 1.000000e+00
  %call80 = call float @_Z6btSqrtf(float %add79)
  store float %call80, float* %s64, align 4
  %35 = load float, float* %s64, align 4
  %mul81 = fmul float %35, 5.000000e-01
  %36 = load i32, i32* %i, align 4
  %arrayidx82 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %36
  store float %mul81, float* %arrayidx82, align 4
  %37 = load float, float* %s64, align 4
  %div83 = fdiv float 5.000000e-01, %37
  store float %div83, float* %s64, align 4
  %m_el84 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %38 = load i32, i32* %k, align 4
  %arrayidx85 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el84, i32 0, i32 %38
  %call86 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx85)
  %39 = load i32, i32* %j, align 4
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 %39
  %40 = load float, float* %arrayidx87, align 4
  %m_el88 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %41 = load i32, i32* %j, align 4
  %arrayidx89 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el88, i32 0, i32 %41
  %call90 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx89)
  %42 = load i32, i32* %k, align 4
  %arrayidx91 = getelementptr inbounds float, float* %call90, i32 %42
  %43 = load float, float* %arrayidx91, align 4
  %sub92 = fsub float %40, %43
  %44 = load float, float* %s64, align 4
  %mul93 = fmul float %sub92, %44
  %arrayidx94 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul93, float* %arrayidx94, align 4
  %m_el95 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %45 = load i32, i32* %j, align 4
  %arrayidx96 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el95, i32 0, i32 %45
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx96)
  %46 = load i32, i32* %i, align 4
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 %46
  %47 = load float, float* %arrayidx98, align 4
  %m_el99 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %48 = load i32, i32* %i, align 4
  %arrayidx100 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el99, i32 0, i32 %48
  %call101 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx100)
  %49 = load i32, i32* %j, align 4
  %arrayidx102 = getelementptr inbounds float, float* %call101, i32 %49
  %50 = load float, float* %arrayidx102, align 4
  %add103 = fadd float %47, %50
  %51 = load float, float* %s64, align 4
  %mul104 = fmul float %add103, %51
  %52 = load i32, i32* %j, align 4
  %arrayidx105 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %52
  store float %mul104, float* %arrayidx105, align 4
  %m_el106 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %53 = load i32, i32* %k, align 4
  %arrayidx107 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el106, i32 0, i32 %53
  %call108 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx107)
  %54 = load i32, i32* %i, align 4
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 %54
  %55 = load float, float* %arrayidx109, align 4
  %m_el110 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %56 = load i32, i32* %i, align 4
  %arrayidx111 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el110, i32 0, i32 %56
  %call112 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx111)
  %57 = load i32, i32* %k, align 4
  %arrayidx113 = getelementptr inbounds float, float* %call112, i32 %57
  %58 = load float, float* %arrayidx113, align 4
  %add114 = fadd float %55, %58
  %59 = load float, float* %s64, align 4
  %mul115 = fmul float %add114, %59
  %60 = load i32, i32* %k, align 4
  %arrayidx116 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %60
  store float %mul115, float* %arrayidx116, align 4
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.then
  %61 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %62 = bitcast %class.btQuaternion* %61 to %class.btQuadWord*
  %arrayidx117 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  %arrayidx118 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  %arrayidx119 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  %arrayidx120 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %62, float* nonnull align 4 dereferenceable(4) %arrayidx117, float* nonnull align 4 dereferenceable(4) %arrayidx118, float* nonnull align 4 dereferenceable(4) %arrayidx119, float* nonnull align 4 dereferenceable(4) %arrayidx120)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

declare void @_ZN23btDiscreteDynamicsWorld13updateActionsEf(%class.btDiscreteDynamicsWorld*, float) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btMultiBodyConstraint** @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintEixEi(%class.btAlignedObjectArray.51* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  %0 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %0, i32 %1
  ret %class.btMultiBodyConstraint** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.43* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator.43* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.43*, align 4
  store %class.btAlignedAllocator.43* %this, %class.btAlignedAllocator.43** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.43*, %class.btAlignedAllocator.43** %this.addr, align 4
  ret %class.btAlignedAllocator.43* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.42* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.42*, align 4
  store %class.btAlignedObjectArray.42* %this, %class.btAlignedObjectArray.42** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.42*, %class.btAlignedObjectArray.42** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.39* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.39* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.39*, align 4
  store %class.btAlignedAllocator.39* %this, %class.btAlignedAllocator.39** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.39*, %class.btAlignedAllocator.39** %this.addr, align 4
  ret %class.btAlignedAllocator.39* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.38* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  store float* null, float** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.42* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.42*, align 4
  store %class.btAlignedObjectArray.42* %this, %class.btAlignedObjectArray.42** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.42*, %class.btAlignedObjectArray.42** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.42* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.42* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.42* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.42* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.42* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.42*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.42* %this, %class.btAlignedObjectArray.42** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.42*, %class.btAlignedObjectArray.42** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.42* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.42*, align 4
  store %class.btAlignedObjectArray.42* %this, %class.btAlignedObjectArray.42** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.42*, %class.btAlignedObjectArray.42** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.42* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.42*, align 4
  store %class.btAlignedObjectArray.42* %this, %class.btAlignedObjectArray.42** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.42*, %class.btAlignedObjectArray.42** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.43* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.43* %this, %class.btVector3* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.43*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator.43* %this, %class.btAlignedAllocator.43** %this.addr, align 4
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.43*, %class.btAlignedAllocator.43** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.38* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.38* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.38* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.38* %this1)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.38* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.38* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  %3 = load float*, float** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.38* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.38* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %tobool = icmp ne float* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  %2 = load float*, float** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.39* %m_allocator, float* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  store float* null, float** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.39* %this, float* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.39*, align 4
  %ptr.addr = alloca float*, align 4
  store %class.btAlignedAllocator.39* %this, %class.btAlignedAllocator.39** %this.addr, align 4
  store float* %ptr, float** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.39*, %class.btAlignedAllocator.39** %this.addr, align 4
  %0 = load float*, float** %ptr.addr, align 4
  %1 = bitcast float* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE8capacityEv(%class.btAlignedObjectArray.27* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.27*, align 4
  store %class.btAlignedObjectArray.27* %this, %class.btAlignedObjectArray.27** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.27*, %class.btAlignedObjectArray.27** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btMultiBodyE7reserveEi(%class.btAlignedObjectArray.27* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.27*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btMultiBody**, align 4
  store %class.btAlignedObjectArray.27* %this, %class.btAlignedObjectArray.27** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.27*, %class.btAlignedObjectArray.27** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE8capacityEv(%class.btAlignedObjectArray.27* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP11btMultiBodyE8allocateEi(%class.btAlignedObjectArray.27* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btMultiBody**
  store %class.btMultiBody** %2, %class.btMultiBody*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %this1)
  %3 = load %class.btMultiBody**, %class.btMultiBody*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4copyEiiPS1_(%class.btAlignedObjectArray.27* %this1, i32 0, i32 %call3, %class.btMultiBody** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %this1)
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE7destroyEii(%class.btAlignedObjectArray.27* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE10deallocateEv(%class.btAlignedObjectArray.27* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btMultiBody**, %class.btMultiBody*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 4
  store %class.btMultiBody** %4, %class.btMultiBody*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP11btMultiBodyE9allocSizeEi(%class.btAlignedObjectArray.27* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.27*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.27* %this, %class.btAlignedObjectArray.27** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.27*, %class.btAlignedObjectArray.27** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP11btMultiBodyE8allocateEi(%class.btAlignedObjectArray.27* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.27*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.27* %this, %class.btAlignedObjectArray.27** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.27*, %class.btAlignedObjectArray.27** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btMultiBody** @_ZN18btAlignedAllocatorIP11btMultiBodyLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.28* %m_allocator, i32 %1, %class.btMultiBody*** null)
  %2 = bitcast %class.btMultiBody** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4copyEiiPS1_(%class.btAlignedObjectArray.27* %this, i32 %start, i32 %end, %class.btMultiBody** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.27*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btMultiBody**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.27* %this, %class.btAlignedObjectArray.27** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btMultiBody** %dest, %class.btMultiBody*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.27*, %class.btAlignedObjectArray.27** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btMultiBody**, %class.btMultiBody*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %3, i32 %4
  %5 = bitcast %class.btMultiBody** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btMultiBody**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 4
  %7 = load %class.btMultiBody**, %class.btMultiBody*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %7, i32 %8
  %9 = load %class.btMultiBody*, %class.btMultiBody** %arrayidx2, align 4
  store %class.btMultiBody* %9, %class.btMultiBody** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btMultiBodyE7destroyEii(%class.btAlignedObjectArray.27* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.27*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.27* %this, %class.btAlignedObjectArray.27** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.27*, %class.btAlignedObjectArray.27** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 4
  %3 = load %class.btMultiBody**, %class.btMultiBody*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btMultiBodyE10deallocateEv(%class.btAlignedObjectArray.27* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.27*, align 4
  store %class.btAlignedObjectArray.27* %this, %class.btAlignedObjectArray.27** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.27*, %class.btAlignedObjectArray.27** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 4
  %0 = load %class.btMultiBody**, %class.btMultiBody*** %m_data, align 4
  %tobool = icmp ne %class.btMultiBody** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 4
  %2 = load %class.btMultiBody**, %class.btMultiBody*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP11btMultiBodyLj16EE10deallocateEPS1_(%class.btAlignedAllocator.28* %m_allocator, %class.btMultiBody** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 4
  store %class.btMultiBody** null, %class.btMultiBody*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMultiBody** @_ZN18btAlignedAllocatorIP11btMultiBodyLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.28* %this, i32 %n, %class.btMultiBody*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.28*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btMultiBody***, align 4
  store %class.btAlignedAllocator.28* %this, %class.btAlignedAllocator.28** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btMultiBody*** %hint, %class.btMultiBody**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.28*, %class.btAlignedAllocator.28** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btMultiBody**
  ret %class.btMultiBody** %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP11btMultiBodyLj16EE10deallocateEPS1_(%class.btAlignedAllocator.28* %this, %class.btMultiBody** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.28*, align 4
  %ptr.addr = alloca %class.btMultiBody**, align 4
  store %class.btAlignedAllocator.28* %this, %class.btAlignedAllocator.28** %this.addr, align 4
  store %class.btMultiBody** %ptr, %class.btMultiBody*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.28*, %class.btAlignedAllocator.28** %this.addr, align 4
  %0 = load %class.btMultiBody**, %class.btMultiBody*** %ptr.addr, align 4
  %1 = bitcast %class.btMultiBody** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE16findLinearSearchERKS1_(%class.btAlignedObjectArray.27* %this, %class.btMultiBody** nonnull align 4 dereferenceable(4) %key) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.27*, align 4
  %key.addr = alloca %class.btMultiBody**, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.27* %this, %class.btAlignedObjectArray.27** %this.addr, align 4
  store %class.btMultiBody** %key, %class.btMultiBody*** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray.27*, %class.btAlignedObjectArray.27** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %this1)
  store i32 %call, i32* %index, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %this1)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 4
  %1 = load %class.btMultiBody**, %class.btMultiBody*** %m_data, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %1, i32 %2
  %3 = load %class.btMultiBody*, %class.btMultiBody** %arrayidx, align 4
  %4 = load %class.btMultiBody**, %class.btMultiBody*** %key.addr, align 4
  %5 = load %class.btMultiBody*, %class.btMultiBody** %4, align 4
  %cmp3 = icmp eq %class.btMultiBody* %3, %5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  store i32 %6, i32* %index, align 4
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %8 = load i32, i32* %index, align 4
  ret i32 %8
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btMultiBodyE13removeAtIndexEi(%class.btAlignedObjectArray.27* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.27*, align 4
  %index.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.27* %this, %class.btAlignedObjectArray.27** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btAlignedObjectArray.27*, %class.btAlignedObjectArray.27** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %this1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %index.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE4swapEii(%class.btAlignedObjectArray.27* %this1, i32 %1, i32 %sub)
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE8pop_backEv(%class.btAlignedObjectArray.27* %this1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btMultiBodyE4swapEii(%class.btAlignedObjectArray.27* %this, i32 %index0, i32 %index1) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.27*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %class.btMultiBody*, align 4
  store %class.btAlignedObjectArray.27* %this, %class.btAlignedObjectArray.27** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray.27*, %class.btAlignedObjectArray.27** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 4
  %0 = load %class.btMultiBody**, %class.btMultiBody*** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %0, i32 %1
  %2 = load %class.btMultiBody*, %class.btMultiBody** %arrayidx, align 4
  store %class.btMultiBody* %2, %class.btMultiBody** %temp, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 4
  %3 = load %class.btMultiBody**, %class.btMultiBody*** %m_data2, align 4
  %4 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %3, i32 %4
  %5 = load %class.btMultiBody*, %class.btMultiBody** %arrayidx3, align 4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 4
  %6 = load %class.btMultiBody**, %class.btMultiBody*** %m_data4, align 4
  %7 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %6, i32 %7
  store %class.btMultiBody* %5, %class.btMultiBody** %arrayidx5, align 4
  %8 = load %class.btMultiBody*, %class.btMultiBody** %temp, align 4
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 4
  %9 = load %class.btMultiBody**, %class.btMultiBody*** %m_data6, align 4
  %10 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %9, i32 %10
  store %class.btMultiBody* %8, %class.btMultiBody** %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btMultiBodyE8pop_backEv(%class.btAlignedObjectArray.27* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.27*, align 4
  store %class.btAlignedObjectArray.27* %this, %class.btAlignedObjectArray.27** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.27*, %class.btAlignedObjectArray.27** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 4
  %1 = load %class.btMultiBody**, %class.btMultiBody*** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds %class.btMultiBody*, %class.btMultiBody** %1, i32 %2
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.52* @_ZN18btAlignedAllocatorIP21btMultiBodyConstraintLj16EEC2Ev(%class.btAlignedAllocator.52* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.52*, align 4
  store %class.btAlignedAllocator.52* %this, %class.btAlignedAllocator.52** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.52*, %class.btAlignedAllocator.52** %this.addr, align 4
  ret %class.btAlignedAllocator.52* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE4initEv(%class.btAlignedObjectArray.51* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  store %class.btMultiBodyConstraint** null, %class.btMultiBodyConstraint*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE5clearEv(%class.btAlignedObjectArray.51* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %this1)
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE7destroyEii(%class.btAlignedObjectArray.51* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE10deallocateEv(%class.btAlignedObjectArray.51* %this1)
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE4initEv(%class.btAlignedObjectArray.51* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE7destroyEii(%class.btAlignedObjectArray.51* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  %3 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE10deallocateEv(%class.btAlignedObjectArray.51* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  %0 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4
  %tobool = icmp ne %class.btMultiBodyConstraint** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  %2 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP21btMultiBodyConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.52* %m_allocator, %class.btMultiBodyConstraint** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  store %class.btMultiBodyConstraint** null, %class.btMultiBodyConstraint*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP21btMultiBodyConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.52* %this, %class.btMultiBodyConstraint** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.52*, align 4
  %ptr.addr = alloca %class.btMultiBodyConstraint**, align 4
  store %class.btAlignedAllocator.52* %this, %class.btAlignedAllocator.52** %this.addr, align 4
  store %class.btMultiBodyConstraint** %ptr, %class.btMultiBodyConstraint*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.52*, %class.btAlignedAllocator.52** %this.addr, align 4
  %0 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %ptr.addr, align 4
  %1 = bitcast %class.btMultiBodyConstraint** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE7reserveEi(%class.btAlignedObjectArray.51* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btMultiBodyConstraint**, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE8capacityEv(%class.btAlignedObjectArray.51* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE8allocateEi(%class.btAlignedObjectArray.51* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btMultiBodyConstraint**
  store %class.btMultiBodyConstraint** %2, %class.btMultiBodyConstraint*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %this1)
  %3 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4copyEiiPS1_(%class.btAlignedObjectArray.51* %this1, i32 0, i32 %call3, %class.btMultiBodyConstraint** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %this1)
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE7destroyEii(%class.btAlignedObjectArray.51* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE10deallocateEv(%class.btAlignedObjectArray.51* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  store %class.btMultiBodyConstraint** %4, %class.btMultiBodyConstraint*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE8capacityEv(%class.btAlignedObjectArray.51* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE8allocateEi(%class.btAlignedObjectArray.51* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btMultiBodyConstraint** @_ZN18btAlignedAllocatorIP21btMultiBodyConstraintLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.52* %m_allocator, i32 %1, %class.btMultiBodyConstraint*** null)
  %2 = bitcast %class.btMultiBodyConstraint** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4copyEiiPS1_(%class.btAlignedObjectArray.51* %this, i32 %start, i32 %end, %class.btMultiBodyConstraint** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btMultiBodyConstraint**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btMultiBodyConstraint** %dest, %class.btMultiBodyConstraint*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %3, i32 %4
  %5 = bitcast %class.btMultiBodyConstraint** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btMultiBodyConstraint**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  %7 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %7, i32 %8
  %9 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx2, align 4
  store %class.btMultiBodyConstraint* %9, %class.btMultiBodyConstraint** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMultiBodyConstraint** @_ZN18btAlignedAllocatorIP21btMultiBodyConstraintLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.52* %this, i32 %n, %class.btMultiBodyConstraint*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.52*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btMultiBodyConstraint***, align 4
  store %class.btAlignedAllocator.52* %this, %class.btAlignedAllocator.52** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btMultiBodyConstraint*** %hint, %class.btMultiBodyConstraint**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.52*, %class.btAlignedAllocator.52** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btMultiBodyConstraint**
  ret %class.btMultiBodyConstraint** %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE9allocSizeEi(%class.btAlignedObjectArray.51* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.28* @_ZN18btAlignedAllocatorIP11btMultiBodyLj16EEC2Ev(%class.btAlignedAllocator.28* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.28*, align 4
  store %class.btAlignedAllocator.28* %this, %class.btAlignedAllocator.28** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.28*, %class.btAlignedAllocator.28** %this.addr, align 4
  ret %class.btAlignedAllocator.28* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btMultiBodyE4initEv(%class.btAlignedObjectArray.27* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.27*, align 4
  store %class.btAlignedObjectArray.27* %this, %class.btAlignedObjectArray.27** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.27*, %class.btAlignedObjectArray.27** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 4
  store %class.btMultiBody** null, %class.btMultiBody*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.78* @_ZN18btAlignedAllocatorI12btQuaternionLj16EEC2Ev(%class.btAlignedAllocator.78* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.78*, align 4
  store %class.btAlignedAllocator.78* %this, %class.btAlignedAllocator.78** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.78*, %class.btAlignedAllocator.78** %this.addr, align 4
  ret %class.btAlignedAllocator.78* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btQuaternionE4initEv(%class.btAlignedObjectArray.77* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  store %class.btQuaternion* null, %class.btQuaternion** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.47* @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EEC2Ev(%class.btAlignedAllocator.47* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.47*, align 4
  store %class.btAlignedAllocator.47* %this, %class.btAlignedAllocator.47** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.47*, %class.btAlignedAllocator.47** %this.addr, align 4
  ret %class.btAlignedAllocator.47* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E4initEv(%class.btAlignedObjectArray.46* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.46*, align 4
  store %class.btAlignedObjectArray.46* %this, %class.btAlignedObjectArray.46** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.46*, %class.btAlignedObjectArray.46** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 4
  store %class.btMatrix3x3* null, %class.btMatrix3x3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP11btMultiBodyE5clearEv(%class.btAlignedObjectArray.27* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.27*, align 4
  store %class.btAlignedObjectArray.27* %this, %class.btAlignedObjectArray.27** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.27*, %class.btAlignedObjectArray.27** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP11btMultiBodyE4sizeEv(%class.btAlignedObjectArray.27* %this1)
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE7destroyEii(%class.btAlignedObjectArray.27* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE10deallocateEv(%class.btAlignedObjectArray.27* %this1)
  call void @_ZN20btAlignedObjectArrayIP11btMultiBodyE4initEv(%class.btAlignedObjectArray.27* %this1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btQuaternionE5clearEv(%class.btAlignedObjectArray.77* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv(%class.btAlignedObjectArray.77* %this1)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE7destroyEii(%class.btAlignedObjectArray.77* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE10deallocateEv(%class.btAlignedObjectArray.77* %this1)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE4initEv(%class.btAlignedObjectArray.77* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btQuaternionE7destroyEii(%class.btAlignedObjectArray.77* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  %3 = load %class.btQuaternion*, %class.btQuaternion** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btQuaternion, %class.btQuaternion* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv(%class.btAlignedObjectArray.77* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btQuaternionE10deallocateEv(%class.btAlignedObjectArray.77* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %m_data, align 4
  %tobool = icmp ne %class.btQuaternion* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI12btQuaternionLj16EE10deallocateEPS0_(%class.btAlignedAllocator.78* %m_allocator, %class.btQuaternion* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  store %class.btQuaternion* null, %class.btQuaternion** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI12btQuaternionLj16EE10deallocateEPS0_(%class.btAlignedAllocator.78* %this, %class.btQuaternion* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.78*, align 4
  %ptr.addr = alloca %class.btQuaternion*, align 4
  store %class.btAlignedAllocator.78* %this, %class.btAlignedAllocator.78** %this.addr, align 4
  store %class.btQuaternion* %ptr, %class.btQuaternion** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.78*, %class.btAlignedAllocator.78** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %ptr.addr, align 4
  %1 = bitcast %class.btQuaternion* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E5clearEv(%class.btAlignedObjectArray.46* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.46*, align 4
  store %class.btAlignedObjectArray.46* %this, %class.btAlignedObjectArray.46** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.46*, %class.btAlignedObjectArray.46** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.46* %this1)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E7destroyEii(%class.btAlignedObjectArray.46* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E10deallocateEv(%class.btAlignedObjectArray.46* %this1)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E4initEv(%class.btAlignedObjectArray.46* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E7destroyEii(%class.btAlignedObjectArray.46* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.46*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.46* %this, %class.btAlignedObjectArray.46** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.46*, %class.btAlignedObjectArray.46** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.46* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.46*, align 4
  store %class.btAlignedObjectArray.46* %this, %class.btAlignedObjectArray.46** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.46*, %class.btAlignedObjectArray.46** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E10deallocateEv(%class.btAlignedObjectArray.46* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.46*, align 4
  store %class.btAlignedObjectArray.46* %this, %class.btAlignedObjectArray.46** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.46*, %class.btAlignedObjectArray.46** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data, align 4
  %tobool = icmp ne %class.btMatrix3x3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.47* %m_allocator, %class.btMatrix3x3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 4
  store %class.btMatrix3x3* null, %class.btMatrix3x3** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.47* %this, %class.btMatrix3x3* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.47*, align 4
  %ptr.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btAlignedAllocator.47* %this, %class.btAlignedAllocator.47** %this.addr, align 4
  store %class.btMatrix3x3* %ptr, %class.btMatrix3x3** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.47*, %class.btAlignedAllocator.47** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %ptr.addr, align 4
  %1 = bitcast %class.btMatrix3x3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI34btSortConstraintOnIslandPredicate2EEvRKT_ii(%class.btAlignedObjectArray.4* %this, %class.btSortConstraintOnIslandPredicate2* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %CompareFunc.addr = alloca %class.btSortConstraintOnIslandPredicate2*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %class.btTypedConstraint*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store %class.btSortConstraintOnIslandPredicate2* %CompareFunc, %class.btSortConstraintOnIslandPredicate2** %CompareFunc.addr, align 4
  store i32 %lo, i32* %lo.addr, align 4
  store i32 %hi, i32* %hi.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %lo.addr, align 4
  store i32 %0, i32* %i, align 4
  %1 = load i32, i32* %hi.addr, align 4
  store i32 %1, i32* %j, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %3 = load i32, i32* %lo.addr, align 4
  %4 = load i32, i32* %hi.addr, align 4
  %add = add nsw i32 %3, %4
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %2, i32 %div
  %5 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx, align 4
  store %class.btTypedConstraint* %5, %class.btTypedConstraint** %x, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %6 = load %class.btSortConstraintOnIslandPredicate2*, %class.btSortConstraintOnIslandPredicate2** %CompareFunc.addr, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %7 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data2, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %7, i32 %8
  %9 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx3, align 4
  %10 = load %class.btTypedConstraint*, %class.btTypedConstraint** %x, align 4
  %call = call zeroext i1 @_ZNK34btSortConstraintOnIslandPredicate2clEPK17btTypedConstraintS2_(%class.btSortConstraintOnIslandPredicate2* %6, %class.btTypedConstraint* %9, %class.btTypedConstraint* %10)
  br i1 %call, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond4

while.cond4:                                      ; preds = %while.body8, %while.end
  %12 = load %class.btSortConstraintOnIslandPredicate2*, %class.btSortConstraintOnIslandPredicate2** %CompareFunc.addr, align 4
  %13 = load %class.btTypedConstraint*, %class.btTypedConstraint** %x, align 4
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %14 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data5, align 4
  %15 = load i32, i32* %j, align 4
  %arrayidx6 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %14, i32 %15
  %16 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx6, align 4
  %call7 = call zeroext i1 @_ZNK34btSortConstraintOnIslandPredicate2clEPK17btTypedConstraintS2_(%class.btSortConstraintOnIslandPredicate2* %12, %class.btTypedConstraint* %13, %class.btTypedConstraint* %16)
  br i1 %call7, label %while.body8, label %while.end9

while.body8:                                      ; preds = %while.cond4
  %17 = load i32, i32* %j, align 4
  %dec = add nsw i32 %17, -1
  store i32 %dec, i32* %j, align 4
  br label %while.cond4

while.end9:                                       ; preds = %while.cond4
  %18 = load i32, i32* %i, align 4
  %19 = load i32, i32* %j, align 4
  %cmp = icmp sle i32 %18, %19
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end9
  %20 = load i32, i32* %i, align 4
  %21 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4swapEii(%class.btAlignedObjectArray.4* %this1, i32 %20, i32 %21)
  %22 = load i32, i32* %i, align 4
  %inc10 = add nsw i32 %22, 1
  store i32 %inc10, i32* %i, align 4
  %23 = load i32, i32* %j, align 4
  %dec11 = add nsw i32 %23, -1
  store i32 %dec11, i32* %j, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end9
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %24 = load i32, i32* %i, align 4
  %25 = load i32, i32* %j, align 4
  %cmp12 = icmp sle i32 %24, %25
  br i1 %cmp12, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %26 = load i32, i32* %lo.addr, align 4
  %27 = load i32, i32* %j, align 4
  %cmp13 = icmp slt i32 %26, %27
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %do.end
  %28 = load %class.btSortConstraintOnIslandPredicate2*, %class.btSortConstraintOnIslandPredicate2** %CompareFunc.addr, align 4
  %29 = load i32, i32* %lo.addr, align 4
  %30 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI34btSortConstraintOnIslandPredicate2EEvRKT_ii(%class.btAlignedObjectArray.4* %this1, %class.btSortConstraintOnIslandPredicate2* nonnull align 1 dereferenceable(1) %28, i32 %29, i32 %30)
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %do.end
  %31 = load i32, i32* %i, align 4
  %32 = load i32, i32* %hi.addr, align 4
  %cmp16 = icmp slt i32 %31, %32
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end15
  %33 = load %class.btSortConstraintOnIslandPredicate2*, %class.btSortConstraintOnIslandPredicate2** %CompareFunc.addr, align 4
  %34 = load i32, i32* %i, align 4
  %35 = load i32, i32* %hi.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI34btSortConstraintOnIslandPredicate2EEvRKT_ii(%class.btAlignedObjectArray.4* %this1, %class.btSortConstraintOnIslandPredicate2* nonnull align 1 dereferenceable(1) %33, i32 %34, i32 %35)
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %if.end15
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK34btSortConstraintOnIslandPredicate2clEPK17btTypedConstraintS2_(%class.btSortConstraintOnIslandPredicate2* %this, %class.btTypedConstraint* %lhs, %class.btTypedConstraint* %rhs) #2 comdat {
entry:
  %this.addr = alloca %class.btSortConstraintOnIslandPredicate2*, align 4
  %lhs.addr = alloca %class.btTypedConstraint*, align 4
  %rhs.addr = alloca %class.btTypedConstraint*, align 4
  %rIslandId0 = alloca i32, align 4
  %lIslandId0 = alloca i32, align 4
  store %class.btSortConstraintOnIslandPredicate2* %this, %class.btSortConstraintOnIslandPredicate2** %this.addr, align 4
  store %class.btTypedConstraint* %lhs, %class.btTypedConstraint** %lhs.addr, align 4
  store %class.btTypedConstraint* %rhs, %class.btTypedConstraint** %rhs.addr, align 4
  %this1 = load %class.btSortConstraintOnIslandPredicate2*, %class.btSortConstraintOnIslandPredicate2** %this.addr, align 4
  %0 = load %class.btTypedConstraint*, %class.btTypedConstraint** %rhs.addr, align 4
  %call = call i32 @_Z24btGetConstraintIslandId2PK17btTypedConstraint(%class.btTypedConstraint* %0)
  store i32 %call, i32* %rIslandId0, align 4
  %1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %lhs.addr, align 4
  %call2 = call i32 @_Z24btGetConstraintIslandId2PK17btTypedConstraint(%class.btTypedConstraint* %1)
  store i32 %call2, i32* %lIslandId0, align 4
  %2 = load i32, i32* %lIslandId0, align 4
  %3 = load i32, i32* %rIslandId0, align 4
  %cmp = icmp slt i32 %2, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4swapEii(%class.btAlignedObjectArray.4* %this, i32 %index0, i32 %index1) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %class.btTypedConstraint*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %0, i32 %1
  %2 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx, align 4
  store %class.btTypedConstraint* %2, %class.btTypedConstraint** %temp, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data2, align 4
  %4 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %3, i32 %4
  %5 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx3, align 4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %6 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data4, align 4
  %7 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %6, i32 %7
  store %class.btTypedConstraint* %5, %class.btTypedConstraint** %arrayidx5, align 4
  %8 = load %class.btTypedConstraint*, %class.btTypedConstraint** %temp, align 4
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %9 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data6, align 4
  %10 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %9, i32 %10
  store %class.btTypedConstraint* %8, %class.btTypedConstraint** %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE17quickSortInternalI42btSortMultiBodyConstraintOnIslandPredicateEEvRKT_ii(%class.btAlignedObjectArray.51* %this, %class.btSortMultiBodyConstraintOnIslandPredicate* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  %CompareFunc.addr = alloca %class.btSortMultiBodyConstraintOnIslandPredicate*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %class.btMultiBodyConstraint*, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  store %class.btSortMultiBodyConstraintOnIslandPredicate* %CompareFunc, %class.btSortMultiBodyConstraintOnIslandPredicate** %CompareFunc.addr, align 4
  store i32 %lo, i32* %lo.addr, align 4
  store i32 %hi, i32* %hi.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %0 = load i32, i32* %lo.addr, align 4
  store i32 %0, i32* %i, align 4
  %1 = load i32, i32* %hi.addr, align 4
  store i32 %1, i32* %j, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  %2 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4
  %3 = load i32, i32* %lo.addr, align 4
  %4 = load i32, i32* %hi.addr, align 4
  %add = add nsw i32 %3, %4
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %2, i32 %div
  %5 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx, align 4
  store %class.btMultiBodyConstraint* %5, %class.btMultiBodyConstraint** %x, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %6 = load %class.btSortMultiBodyConstraintOnIslandPredicate*, %class.btSortMultiBodyConstraintOnIslandPredicate** %CompareFunc.addr, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  %7 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data2, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %7, i32 %8
  %9 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx3, align 4
  %10 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %x, align 4
  %call = call zeroext i1 @_ZNK42btSortMultiBodyConstraintOnIslandPredicateclEPK21btMultiBodyConstraintS2_(%class.btSortMultiBodyConstraintOnIslandPredicate* %6, %class.btMultiBodyConstraint* %9, %class.btMultiBodyConstraint* %10)
  br i1 %call, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond4

while.cond4:                                      ; preds = %while.body8, %while.end
  %12 = load %class.btSortMultiBodyConstraintOnIslandPredicate*, %class.btSortMultiBodyConstraintOnIslandPredicate** %CompareFunc.addr, align 4
  %13 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %x, align 4
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  %14 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data5, align 4
  %15 = load i32, i32* %j, align 4
  %arrayidx6 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %14, i32 %15
  %16 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx6, align 4
  %call7 = call zeroext i1 @_ZNK42btSortMultiBodyConstraintOnIslandPredicateclEPK21btMultiBodyConstraintS2_(%class.btSortMultiBodyConstraintOnIslandPredicate* %12, %class.btMultiBodyConstraint* %13, %class.btMultiBodyConstraint* %16)
  br i1 %call7, label %while.body8, label %while.end9

while.body8:                                      ; preds = %while.cond4
  %17 = load i32, i32* %j, align 4
  %dec = add nsw i32 %17, -1
  store i32 %dec, i32* %j, align 4
  br label %while.cond4

while.end9:                                       ; preds = %while.cond4
  %18 = load i32, i32* %i, align 4
  %19 = load i32, i32* %j, align 4
  %cmp = icmp sle i32 %18, %19
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end9
  %20 = load i32, i32* %i, align 4
  %21 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE4swapEii(%class.btAlignedObjectArray.51* %this1, i32 %20, i32 %21)
  %22 = load i32, i32* %i, align 4
  %inc10 = add nsw i32 %22, 1
  store i32 %inc10, i32* %i, align 4
  %23 = load i32, i32* %j, align 4
  %dec11 = add nsw i32 %23, -1
  store i32 %dec11, i32* %j, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end9
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %24 = load i32, i32* %i, align 4
  %25 = load i32, i32* %j, align 4
  %cmp12 = icmp sle i32 %24, %25
  br i1 %cmp12, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %26 = load i32, i32* %lo.addr, align 4
  %27 = load i32, i32* %j, align 4
  %cmp13 = icmp slt i32 %26, %27
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %do.end
  %28 = load %class.btSortMultiBodyConstraintOnIslandPredicate*, %class.btSortMultiBodyConstraintOnIslandPredicate** %CompareFunc.addr, align 4
  %29 = load i32, i32* %lo.addr, align 4
  %30 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE17quickSortInternalI42btSortMultiBodyConstraintOnIslandPredicateEEvRKT_ii(%class.btAlignedObjectArray.51* %this1, %class.btSortMultiBodyConstraintOnIslandPredicate* nonnull align 1 dereferenceable(1) %28, i32 %29, i32 %30)
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %do.end
  %31 = load i32, i32* %i, align 4
  %32 = load i32, i32* %hi.addr, align 4
  %cmp16 = icmp slt i32 %31, %32
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end15
  %33 = load %class.btSortMultiBodyConstraintOnIslandPredicate*, %class.btSortMultiBodyConstraintOnIslandPredicate** %CompareFunc.addr, align 4
  %34 = load i32, i32* %i, align 4
  %35 = load i32, i32* %hi.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE17quickSortInternalI42btSortMultiBodyConstraintOnIslandPredicateEEvRKT_ii(%class.btAlignedObjectArray.51* %this1, %class.btSortMultiBodyConstraintOnIslandPredicate* nonnull align 1 dereferenceable(1) %33, i32 %34, i32 %35)
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %if.end15
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK42btSortMultiBodyConstraintOnIslandPredicateclEPK21btMultiBodyConstraintS2_(%class.btSortMultiBodyConstraintOnIslandPredicate* %this, %class.btMultiBodyConstraint* %lhs, %class.btMultiBodyConstraint* %rhs) #2 comdat {
entry:
  %this.addr = alloca %class.btSortMultiBodyConstraintOnIslandPredicate*, align 4
  %lhs.addr = alloca %class.btMultiBodyConstraint*, align 4
  %rhs.addr = alloca %class.btMultiBodyConstraint*, align 4
  %rIslandId0 = alloca i32, align 4
  %lIslandId0 = alloca i32, align 4
  store %class.btSortMultiBodyConstraintOnIslandPredicate* %this, %class.btSortMultiBodyConstraintOnIslandPredicate** %this.addr, align 4
  store %class.btMultiBodyConstraint* %lhs, %class.btMultiBodyConstraint** %lhs.addr, align 4
  store %class.btMultiBodyConstraint* %rhs, %class.btMultiBodyConstraint** %rhs.addr, align 4
  %this1 = load %class.btSortMultiBodyConstraintOnIslandPredicate*, %class.btSortMultiBodyConstraintOnIslandPredicate** %this.addr, align 4
  %0 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %rhs.addr, align 4
  %call = call i32 @_Z32btGetMultiBodyConstraintIslandIdPK21btMultiBodyConstraint(%class.btMultiBodyConstraint* %0)
  store i32 %call, i32* %rIslandId0, align 4
  %1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %lhs.addr, align 4
  %call2 = call i32 @_Z32btGetMultiBodyConstraintIslandIdPK21btMultiBodyConstraint(%class.btMultiBodyConstraint* %1)
  store i32 %call2, i32* %lIslandId0, align 4
  %2 = load i32, i32* %lIslandId0, align 4
  %3 = load i32, i32* %rIslandId0, align 4
  %cmp = icmp slt i32 %2, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE4swapEii(%class.btAlignedObjectArray.51* %this, i32 %index0, i32 %index1) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %class.btMultiBodyConstraint*, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  %0 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %0, i32 %1
  %2 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx, align 4
  store %class.btMultiBodyConstraint* %2, %class.btMultiBodyConstraint** %temp, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  %3 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data2, align 4
  %4 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %3, i32 %4
  %5 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx3, align 4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  %6 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data4, align 4
  %7 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %6, i32 %7
  store %class.btMultiBodyConstraint* %5, %class.btMultiBodyConstraint** %arrayidx5, align 4
  %8 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %temp, align 4
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  %9 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data6, align 4
  %10 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %9, i32 %10
  store %class.btMultiBodyConstraint* %8, %class.btMultiBodyConstraint** %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.38* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca float*, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.38* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.38* %this1, i32 %1)
  %2 = bitcast i8* %call2 to float*
  store float* %2, float** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.38* %this1)
  %3 = load float*, float** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.38* %this1, i32 0, i32 %call3, float* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.38* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.38* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.38* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load float*, float** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  store float* %4, float** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.38* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.38* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.39* %m_allocator, i32 %1, float** null)
  %2 = bitcast float* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.38* %this, i32 %start, i32 %end, float* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.38*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca float*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.38* %this, %class.btAlignedObjectArray.38** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store float* %dest, float** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.38*, %class.btAlignedObjectArray.38** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load float*, float** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  %5 = bitcast float* %arrayidx to i8*
  %6 = bitcast i8* %5 to float*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.38, %class.btAlignedObjectArray.38* %this1, i32 0, i32 4
  %7 = load float*, float** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds float, float* %7, i32 %8
  %9 = load float, float* %arrayidx2, align 4
  store float %9, float* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.39* %this, i32 %n, float** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.39*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca float**, align 4
  store %class.btAlignedAllocator.39* %this, %class.btAlignedAllocator.39** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store float** %hint, float*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.39*, %class.btAlignedAllocator.39** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to float*
  ret float* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.42* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.42*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray.42* %this, %class.btAlignedObjectArray.42** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.42*, %class.btAlignedObjectArray.42** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.42* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray.42* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %2, %class.btVector3** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.42* %this1)
  %3 = load %class.btVector3*, %class.btVector3** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray.42* %this1, i32 0, i32 %call3, %class.btVector3* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.42* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.42* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.42* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 4
  store %class.btVector3* %4, %class.btVector3** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.42* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.42*, align 4
  store %class.btAlignedObjectArray.42* %this, %class.btAlignedObjectArray.42** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.42*, %class.btAlignedObjectArray.42** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray.42* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.42*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.42* %this, %class.btAlignedObjectArray.42** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.42*, %class.btAlignedObjectArray.42** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.43* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray.42* %this, i32 %start, i32 %end, %class.btVector3* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.42*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.42* %this, %class.btAlignedObjectArray.42** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.42*, %class.btAlignedObjectArray.42** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  %5 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.42, %class.btAlignedObjectArray.42* %this1, i32 0, i32 4
  %7 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  %9 = bitcast %class.btVector3* %6 to i8*
  %10 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.43* %this, i32 %n, %class.btVector3** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.43*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator.43* %this, %class.btAlignedAllocator.43** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.43*, %class.btAlignedAllocator.43** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btMatrix3x3E7reserveEi(%class.btAlignedObjectArray.46* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.46*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btMatrix3x3*, align 4
  store %class.btAlignedObjectArray.46* %this, %class.btAlignedObjectArray.46** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.46*, %class.btAlignedObjectArray.46** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E8capacityEv(%class.btAlignedObjectArray.46* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI11btMatrix3x3E8allocateEi(%class.btAlignedObjectArray.46* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btMatrix3x3*
  store %class.btMatrix3x3* %2, %class.btMatrix3x3** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.46* %this1)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4copyEiiPS0_(%class.btAlignedObjectArray.46* %this1, i32 0, i32 %call3, %class.btMatrix3x3* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4sizeEv(%class.btAlignedObjectArray.46* %this1)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E7destroyEii(%class.btAlignedObjectArray.46* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI11btMatrix3x3E10deallocateEv(%class.btAlignedObjectArray.46* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 4
  store %class.btMatrix3x3* %4, %class.btMatrix3x3** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11btMatrix3x3E8capacityEv(%class.btAlignedObjectArray.46* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.46*, align 4
  store %class.btAlignedObjectArray.46* %this, %class.btAlignedObjectArray.46** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.46*, %class.btAlignedObjectArray.46** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI11btMatrix3x3E8allocateEi(%class.btAlignedObjectArray.46* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.46*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.46* %this, %class.btAlignedObjectArray.46** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.46*, %class.btAlignedObjectArray.46** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.47* %m_allocator, i32 %1, %class.btMatrix3x3** null)
  %2 = bitcast %class.btMatrix3x3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI11btMatrix3x3E4copyEiiPS0_(%class.btAlignedObjectArray.46* %this, i32 %start, i32 %end, %class.btMatrix3x3* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.46*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btMatrix3x3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.46* %this, %class.btAlignedObjectArray.46** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btMatrix3x3* %dest, %class.btMatrix3x3** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.46*, %class.btAlignedObjectArray.46** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 %4
  %5 = bitcast %class.btMatrix3x3* %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btMatrix3x3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.46, %class.btAlignedObjectArray.46* %this1, i32 0, i32 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %7, i32 %8
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %6, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN18btAlignedAllocatorI11btMatrix3x3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.47* %this, i32 %n, %class.btMatrix3x3** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.47*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btMatrix3x3**, align 4
  store %class.btAlignedAllocator.47* %this, %class.btAlignedAllocator.47** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btMatrix3x3** %hint, %class.btMatrix3x3*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.47*, %class.btAlignedAllocator.47** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 48, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btMatrix3x3*
  ret %class.btMatrix3x3* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btQuaternionE7reserveEi(%class.btAlignedObjectArray.77* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btQuaternion*, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE8capacityEv(%class.btAlignedObjectArray.77* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI12btQuaternionE8allocateEi(%class.btAlignedObjectArray.77* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btQuaternion*
  store %class.btQuaternion* %2, %class.btQuaternion** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv(%class.btAlignedObjectArray.77* %this1)
  %3 = load %class.btQuaternion*, %class.btQuaternion** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI12btQuaternionE4copyEiiPS0_(%class.btAlignedObjectArray.77* %this1, i32 0, i32 %call3, %class.btQuaternion* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE4sizeEv(%class.btAlignedObjectArray.77* %this1)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE7destroyEii(%class.btAlignedObjectArray.77* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI12btQuaternionE10deallocateEv(%class.btAlignedObjectArray.77* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btQuaternion*, %class.btQuaternion** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  store %class.btQuaternion* %4, %class.btQuaternion** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12btQuaternionE8capacityEv(%class.btAlignedObjectArray.77* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI12btQuaternionE8allocateEi(%class.btAlignedObjectArray.77* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btQuaternion* @_ZN18btAlignedAllocatorI12btQuaternionLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.78* %m_allocator, i32 %1, %class.btQuaternion** null)
  %2 = bitcast %class.btQuaternion* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI12btQuaternionE4copyEiiPS0_(%class.btAlignedObjectArray.77* %this, i32 %start, i32 %end, %class.btQuaternion* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btQuaternion*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btQuaternion* %dest, %class.btQuaternion** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btQuaternion*, %class.btQuaternion** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btQuaternion, %class.btQuaternion* %3, i32 %4
  %5 = bitcast %class.btQuaternion* %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btQuaternion*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  %7 = load %class.btQuaternion*, %class.btQuaternion** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btQuaternion, %class.btQuaternion* %7, i32 %8
  %9 = bitcast %class.btQuaternion* %6 to i8*
  %10 = bitcast %class.btQuaternion* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN18btAlignedAllocatorI12btQuaternionLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.78* %this, i32 %n, %class.btQuaternion** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.78*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btQuaternion**, align 4
  store %class.btAlignedAllocator.78* %this, %class.btAlignedAllocator.78** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btQuaternion** %hint, %class.btQuaternion*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.78*, %class.btAlignedAllocator.78** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btQuaternion*
  ret %class.btQuaternion* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE16findLinearSearchERKS1_(%class.btAlignedObjectArray.51* %this, %class.btMultiBodyConstraint** nonnull align 4 dereferenceable(4) %key) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  %key.addr = alloca %class.btMultiBodyConstraint**, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  store %class.btMultiBodyConstraint** %key, %class.btMultiBodyConstraint*** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %this1)
  store i32 %call, i32* %index, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %this1)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  %1 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %1, i32 %2
  %3 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %arrayidx, align 4
  %4 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %key.addr, align 4
  %5 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %4, align 4
  %cmp3 = icmp eq %class.btMultiBodyConstraint* %3, %5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  store i32 %6, i32* %index, align 4
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %8 = load i32, i32* %index, align 4
  ret i32 %8
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE13removeAtIndexEi(%class.btAlignedObjectArray.51* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  %index.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %this1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %index.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP21btMultiBodyConstraintE4sizeEv(%class.btAlignedObjectArray.51* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE4swapEii(%class.btAlignedObjectArray.51* %this1, i32 %1, i32 %sub)
  call void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE8pop_backEv(%class.btAlignedObjectArray.51* %this1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP21btMultiBodyConstraintE8pop_backEv(%class.btAlignedObjectArray.51* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.51*, align 4
  store %class.btAlignedObjectArray.51* %this, %class.btAlignedObjectArray.51** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.51*, %class.btAlignedObjectArray.51** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 4
  %1 = load %class.btMultiBodyConstraint**, %class.btMultiBodyConstraint*** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.51, %class.btAlignedObjectArray.51* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %1, i32 %2
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btMultiBodyDynamicsWorld.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { cold noreturn nounwind }
attributes #7 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { argmemonly nounwind willreturn }
attributes #9 = { nounwind readnone speculatable willreturn }
attributes #10 = { nounwind }
attributes #11 = { builtin allocsize(0) }
attributes #12 = { builtin nounwind }
attributes #13 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
