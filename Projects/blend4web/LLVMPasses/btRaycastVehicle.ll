; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Vehicle/btRaycastVehicle.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Vehicle/btRaycastVehicle.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.0, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.3, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.3 = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btRaycastVehicle = type { %class.btActionInterface, %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9, i32, i32, %struct.btVehicleRaycaster*, float, float, float, %class.btRigidBody*, i32, i32, i32, %class.btAlignedObjectArray.13 }
%class.btActionInterface = type { i32 (...)** }
%class.btAlignedObjectArray.5 = type <{ %class.btAlignedAllocator.6, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.6 = type { i8 }
%class.btAlignedObjectArray.9 = type <{ %class.btAlignedAllocator.10, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.10 = type { i8 }
%class.btAlignedObjectArray.13 = type <{ %class.btAlignedAllocator.14, [3 x i8], i32, i32, %struct.btWheelInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.14 = type { i8 }
%struct.btWheelInfo = type { %"struct.btWheelInfo::RaycastInfo", %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, float, float, float, float, float, i8, i8*, float, float, float, float }
%"struct.btWheelInfo::RaycastInfo" = type { %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, i8, i8* }
%"class.btRaycastVehicle::btVehicleTuning" = type { float, float, float, float, float, float }
%struct.btVehicleRaycaster = type { i32 (...)** }
%struct.btWheelInfoConstructionInfo = type <{ %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, i8, [3 x i8] }>
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%"struct.btVehicleRaycaster::btVehicleRaycasterResult" = type { %class.btVector3, %class.btVector3, float }
%struct.btWheelContactPoint = type { %class.btRigidBody*, %class.btRigidBody*, %class.btVector3, %class.btVector3, float, float }
%class.btIDebugDraw = type { i32 (...)** }
%class.btDefaultVehicleRaycaster = type { %struct.btVehicleRaycaster, %class.btDynamicsWorld* }
%class.btDynamicsWorld = type { %class.btCollisionWorld.base, void (%class.btDynamicsWorld*, float)*, void (%class.btDynamicsWorld*, float)*, i8*, %struct.btContactSolverInfo }
%class.btCollisionWorld.base = type <{ i32 (...)**, %class.btAlignedObjectArray.17, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8 }>
%class.btAlignedObjectArray.17 = type <{ %class.btAlignedAllocator.18, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.18 = type { i8 }
%class.btDispatcher = type { i32 (...)** }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btBroadphaseInterface = type { i32 (...)** }
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float, float }
%"struct.btCollisionWorld::ClosestRayResultCallback" = type { %"struct.btCollisionWorld::RayResultCallback", %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%"struct.btCollisionWorld::RayResultCallback" = type { i32 (...)**, float, %class.btCollisionObject*, i32, i32, i32 }
%class.btCollisionWorld = type <{ i32 (...)**, %class.btAlignedObjectArray.17, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8, [3 x i8] }>
%"struct.btCollisionWorld::LocalRayResult" = type { %class.btCollisionObject*, %"struct.btCollisionWorld::LocalShapeInfo"*, %class.btVector3, float }
%"struct.btCollisionWorld::LocalShapeInfo" = type { i32, i32 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN11btRigidBodyD2Ev = comdat any

$_ZN17btActionInterfaceC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EC2Ev = comdat any

$_ZN20btAlignedObjectArrayIfEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoED2Ev = comdat any

$_ZN20btAlignedObjectArrayIfED2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZN27btWheelInfoConstructionInfoC2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoE9push_backERKS0_ = comdat any

$_ZN11btWheelInfoC2ER27btWheelInfoConstructionInfo = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoEixEi = comdat any

$_ZNK16btRaycastVehicle12getNumWheelsEv = comdat any

$_ZNK20btAlignedObjectArrayI11btWheelInfoEixEi = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZN12btQuaternionC2ERK9btVector3RKf = comdat any

$_ZN11btMatrix3x3C2ERK12btQuaternion = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btTransform8setBasisERK11btMatrix3x3 = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN16btRaycastVehicle12getRigidBodyEv = comdat any

$_ZN11btRigidBody14getMotionStateEv = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZN18btVehicleRaycaster24btVehicleRaycasterResultC2Ev = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK11btRigidBody23getCenterOfMassPositionEv = comdat any

$_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3 = comdat any

$_ZNK16btRaycastVehicle12getRigidBodyEv = comdat any

$_ZNK11btRigidBody24getCenterOfMassTransformEv = comdat any

$_ZNK11btRigidBody17getLinearVelocityEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZN11btRigidBody12applyImpulseERK9btVector3S2_ = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_ZNK11btRigidBody10getInvMassEv = comdat any

$_Z8btSetMinIfEvRT_RKS0_ = comdat any

$_Z8btSetMaxIfEvRT_RKS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_ = comdat any

$_ZN20btAlignedObjectArrayIfE6resizeEiRKf = comdat any

$_ZN20btAlignedObjectArrayIfEixEi = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZN19btWheelContactPointC2EP11btRigidBodyS1_RK9btVector3S4_f = comdat any

$_Z6btSqrtf = comdat any

$_ZNK11btMatrix3x39getColumnEi = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZNK16btRaycastVehicle12getRightAxisEv = comdat any

$_ZN16btCollisionWorld24ClosestRayResultCallbackC2ERK9btVector3S3_ = comdat any

$_ZNK16btCollisionWorld17RayResultCallback6hasHitEv = comdat any

$_ZN11btRigidBody6upcastEPK17btCollisionObject = comdat any

$_ZNK17btCollisionObject18hasContactResponseEv = comdat any

$_ZN16btCollisionWorld24ClosestRayResultCallbackD2Ev = comdat any

$_ZN25btDefaultVehicleRaycasterD2Ev = comdat any

$_ZN25btDefaultVehicleRaycasterD0Ev = comdat any

$_ZN16btRaycastVehicle12updateActionEP16btCollisionWorldf = comdat any

$_ZN16btRaycastVehicle19setCoordinateSystemEiii = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_ = comdat any

$_ZN17btActionInterfaceD2Ev = comdat any

$_ZN17btActionInterfaceD0Ev = comdat any

$_ZN11btWheelInfo11RaycastInfoC2Ev = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_ZN12btQuaternion11setRotationERK9btVector3RKf = comdat any

$_Z5btSinf = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_Z5btCosf = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN11btRigidBody19applyCentralImpulseERK9btVector3 = comdat any

$_ZN11btRigidBody18applyTorqueImpulseERK9btVector3 = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZNK11btRigidBody25computeImpulseDenominatorERK9btVector3S2_ = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZNK11btRigidBody24getInvInertiaTensorWorldEv = comdat any

$_ZN16btCollisionWorld17RayResultCallbackC2Ev = comdat any

$_ZN16btCollisionWorld24ClosestRayResultCallbackD0Ev = comdat any

$_ZNK16btCollisionWorld17RayResultCallback14needsCollisionEP17btBroadphaseProxy = comdat any

$_ZN16btCollisionWorld24ClosestRayResultCallback15addSingleResultERNS_14LocalRayResultEb = comdat any

$_ZN16btCollisionWorld17RayResultCallbackD2Ev = comdat any

$_ZN16btCollisionWorld17RayResultCallbackD0Ev = comdat any

$_ZNK17btCollisionObject17getWorldTransformEv = comdat any

$_ZN9btVector315setInterpolate3ERKS_S1_f = comdat any

$_ZNK17btCollisionObject15getInternalTypeEv = comdat any

$_ZN18btVehicleRaycasterD2Ev = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIfE4initEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIfE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIfE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIfE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIfE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf = comdat any

$_ZN18btAlignedAllocatorI11btWheelInfoLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoE4initEv = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI11btWheelInfoLj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayI11btWheelInfoE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoE9allocSizeEi = comdat any

$_ZN11btWheelInfoC2ERKS_ = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI11btWheelInfoE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI11btWheelInfoLj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayIfE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIfE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIfE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIfE4copyEiiPf = comdat any

$_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf = comdat any

$_ZTS18btVehicleRaycaster = comdat any

$_ZTI18btVehicleRaycaster = comdat any

$_ZTS17btActionInterface = comdat any

$_ZTI17btActionInterface = comdat any

$_ZTV17btActionInterface = comdat any

$_ZTVN16btCollisionWorld24ClosestRayResultCallbackE = comdat any

$_ZTSN16btCollisionWorld24ClosestRayResultCallbackE = comdat any

$_ZTSN16btCollisionWorld17RayResultCallbackE = comdat any

$_ZTIN16btCollisionWorld17RayResultCallbackE = comdat any

$_ZTIN16btCollisionWorld24ClosestRayResultCallbackE = comdat any

$_ZTVN16btCollisionWorld17RayResultCallbackE = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZZN17btActionInterface12getFixedBodyEvE7s_fixed = internal global %class.btRigidBody zeroinitializer, align 4
@_ZGVZN17btActionInterface12getFixedBodyEvE7s_fixed = internal global i32 0, align 4
@__dso_handle = external hidden global i8
@_ZTV16btRaycastVehicle = hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI16btRaycastVehicle to i8*), i8* bitcast (%class.btRaycastVehicle* (%class.btRaycastVehicle*)* @_ZN16btRaycastVehicleD1Ev to i8*), i8* bitcast (void (%class.btRaycastVehicle*)* @_ZN16btRaycastVehicleD0Ev to i8*), i8* bitcast (void (%class.btRaycastVehicle*, %class.btCollisionWorld*, float)* @_ZN16btRaycastVehicle12updateActionEP16btCollisionWorldf to i8*), i8* bitcast (void (%class.btRaycastVehicle*, %class.btIDebugDraw*)* @_ZN16btRaycastVehicle9debugDrawEP12btIDebugDraw to i8*), i8* bitcast (void (%class.btRaycastVehicle*, float)* @_ZN16btRaycastVehicle13updateVehicleEf to i8*), i8* bitcast (void (%class.btRaycastVehicle*, float)* @_ZN16btRaycastVehicle14updateFrictionEf to i8*), i8* bitcast (void (%class.btRaycastVehicle*, i32, i32, i32)* @_ZN16btRaycastVehicle19setCoordinateSystemEiii to i8*)] }, align 4
@sideFrictionStiffness2 = hidden global float 1.000000e+00, align 4
@_ZTV25btDefaultVehicleRaycaster = hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI25btDefaultVehicleRaycaster to i8*), i8* bitcast (%class.btDefaultVehicleRaycaster* (%class.btDefaultVehicleRaycaster*)* @_ZN25btDefaultVehicleRaycasterD2Ev to i8*), i8* bitcast (void (%class.btDefaultVehicleRaycaster*)* @_ZN25btDefaultVehicleRaycasterD0Ev to i8*), i8* bitcast (i8* (%class.btDefaultVehicleRaycaster*, %class.btVector3*, %class.btVector3*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*)* @_ZN25btDefaultVehicleRaycaster7castRayERK9btVector3S2_RN18btVehicleRaycaster24btVehicleRaycasterResultE to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS25btDefaultVehicleRaycaster = hidden constant [28 x i8] c"25btDefaultVehicleRaycaster\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS18btVehicleRaycaster = linkonce_odr hidden constant [21 x i8] c"18btVehicleRaycaster\00", comdat, align 1
@_ZTI18btVehicleRaycaster = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([21 x i8], [21 x i8]* @_ZTS18btVehicleRaycaster, i32 0, i32 0) }, comdat, align 4
@_ZTI25btDefaultVehicleRaycaster = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([28 x i8], [28 x i8]* @_ZTS25btDefaultVehicleRaycaster, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI18btVehicleRaycaster to i8*) }, align 4
@_ZTS16btRaycastVehicle = hidden constant [19 x i8] c"16btRaycastVehicle\00", align 1
@_ZTS17btActionInterface = linkonce_odr hidden constant [20 x i8] c"17btActionInterface\00", comdat, align 1
@_ZTI17btActionInterface = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17btActionInterface, i32 0, i32 0) }, comdat, align 4
@_ZTI16btRaycastVehicle = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTS16btRaycastVehicle, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI17btActionInterface to i8*) }, align 4
@_ZTV11btRigidBody = external unnamed_addr constant { [9 x i8*] }, align 4
@_ZTV17btActionInterface = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI17btActionInterface to i8*), i8* bitcast (%class.btActionInterface* (%class.btActionInterface*)* @_ZN17btActionInterfaceD2Ev to i8*), i8* bitcast (void (%class.btActionInterface*)* @_ZN17btActionInterfaceD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTVN16btCollisionWorld24ClosestRayResultCallbackE = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN16btCollisionWorld24ClosestRayResultCallbackE to i8*), i8* bitcast (%"struct.btCollisionWorld::ClosestRayResultCallback"* (%"struct.btCollisionWorld::ClosestRayResultCallback"*)* @_ZN16btCollisionWorld24ClosestRayResultCallbackD2Ev to i8*), i8* bitcast (void (%"struct.btCollisionWorld::ClosestRayResultCallback"*)* @_ZN16btCollisionWorld24ClosestRayResultCallbackD0Ev to i8*), i8* bitcast (i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)* @_ZNK16btCollisionWorld17RayResultCallback14needsCollisionEP17btBroadphaseProxy to i8*), i8* bitcast (float (%"struct.btCollisionWorld::ClosestRayResultCallback"*, %"struct.btCollisionWorld::LocalRayResult"*, i1)* @_ZN16btCollisionWorld24ClosestRayResultCallback15addSingleResultERNS_14LocalRayResultEb to i8*)] }, comdat, align 4
@_ZTSN16btCollisionWorld24ClosestRayResultCallbackE = linkonce_odr hidden constant [47 x i8] c"N16btCollisionWorld24ClosestRayResultCallbackE\00", comdat, align 1
@_ZTSN16btCollisionWorld17RayResultCallbackE = linkonce_odr hidden constant [40 x i8] c"N16btCollisionWorld17RayResultCallbackE\00", comdat, align 1
@_ZTIN16btCollisionWorld17RayResultCallbackE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([40 x i8], [40 x i8]* @_ZTSN16btCollisionWorld17RayResultCallbackE, i32 0, i32 0) }, comdat, align 4
@_ZTIN16btCollisionWorld24ClosestRayResultCallbackE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([47 x i8], [47 x i8]* @_ZTSN16btCollisionWorld24ClosestRayResultCallbackE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN16btCollisionWorld17RayResultCallbackE to i8*) }, comdat, align 4
@_ZTVN16btCollisionWorld17RayResultCallbackE = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN16btCollisionWorld17RayResultCallbackE to i8*), i8* bitcast (%"struct.btCollisionWorld::RayResultCallback"* (%"struct.btCollisionWorld::RayResultCallback"*)* @_ZN16btCollisionWorld17RayResultCallbackD2Ev to i8*), i8* bitcast (void (%"struct.btCollisionWorld::RayResultCallback"*)* @_ZN16btCollisionWorld17RayResultCallbackD0Ev to i8*), i8* bitcast (i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)* @_ZNK16btCollisionWorld17RayResultCallback14needsCollisionEP17btBroadphaseProxy to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btRaycastVehicle.cpp, i8* null }]

@_ZN16btRaycastVehicleC1ERKNS_15btVehicleTuningEP11btRigidBodyP18btVehicleRaycaster = hidden unnamed_addr alias %class.btRaycastVehicle* (%class.btRaycastVehicle*, %"class.btRaycastVehicle::btVehicleTuning"*, %class.btRigidBody*, %struct.btVehicleRaycaster*), %class.btRaycastVehicle* (%class.btRaycastVehicle*, %"class.btRaycastVehicle::btVehicleTuning"*, %class.btRigidBody*, %struct.btVehicleRaycaster*)* @_ZN16btRaycastVehicleC2ERKNS_15btVehicleTuningEP11btRigidBodyP18btVehicleRaycaster
@_ZN16btRaycastVehicleD1Ev = hidden unnamed_addr alias %class.btRaycastVehicle* (%class.btRaycastVehicle*), %class.btRaycastVehicle* (%class.btRaycastVehicle*)* @_ZN16btRaycastVehicleD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btActionInterface12getFixedBodyEv() #2 {
entry:
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZN17btActionInterface12getFixedBodyEvE7s_fixed to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !2

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN17btActionInterface12getFixedBodyEvE7s_fixed) #3
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  store float 0.000000e+00, float* %ref.tmp1, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %call4 = call %class.btRigidBody* @_ZN11btRigidBodyC1EfP13btMotionStateP16btCollisionShapeRK9btVector3(%class.btRigidBody* @_ZZN17btActionInterface12getFixedBodyEvE7s_fixed, float 0.000000e+00, %class.btMotionState* null, %class.btCollisionShape* null, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %3 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor, i8* null, i8* @__dso_handle) #3
  call void @__cxa_guard_release(i32* @_ZGVZN17btActionInterface12getFixedBodyEvE7s_fixed) #3
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  call void @_ZN11btRigidBody12setMassPropsEfRK9btVector3(%class.btRigidBody* @_ZZN17btActionInterface12getFixedBodyEvE7s_fixed, float 0.000000e+00, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  ret %class.btRigidBody* @_ZZN17btActionInterface12getFixedBodyEvE7s_fixed
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

declare %class.btRigidBody* @_ZN11btRigidBodyC1EfP13btMotionStateP16btCollisionShapeRK9btVector3(%class.btRigidBody* returned, float, %class.btMotionState*, %class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #4

; Function Attrs: noinline
define internal void @__cxx_global_array_dtor(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4
  %call = call %class.btRigidBody* @_ZN11btRigidBodyD2Ev(%class.btRigidBody* @_ZZN17btActionInterface12getFixedBodyEvE7s_fixed) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btRigidBody* @_ZN11btRigidBodyD2Ev(%class.btRigidBody* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV11btRigidBody, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_constraintRefs = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %call = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray.0* %m_constraintRefs) #3
  %1 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call2 = call %class.btCollisionObject* @_ZN17btCollisionObjectD2Ev(%class.btCollisionObject* %1) #3
  ret %class.btRigidBody* %this1
}

; Function Attrs: nounwind
declare i32 @__cxa_atexit(void (i8*)*, i8*, i8*) #3

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #3

declare void @_ZN11btRigidBody12setMassPropsEfRK9btVector3(%class.btRigidBody*, float, %class.btVector3* nonnull align 4 dereferenceable(16)) #4

; Function Attrs: noinline optnone
define hidden %class.btRaycastVehicle* @_ZN16btRaycastVehicleC2ERKNS_15btVehicleTuningEP11btRigidBodyP18btVehicleRaycaster(%class.btRaycastVehicle* returned %this, %"class.btRaycastVehicle::btVehicleTuning"* nonnull align 4 dereferenceable(24) %tuning, %class.btRigidBody* %chassis, %struct.btVehicleRaycaster* %raycaster) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %tuning.addr = alloca %"class.btRaycastVehicle::btVehicleTuning"*, align 4
  %chassis.addr = alloca %class.btRigidBody*, align 4
  %raycaster.addr = alloca %struct.btVehicleRaycaster*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  store %"class.btRaycastVehicle::btVehicleTuning"* %tuning, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4
  store %class.btRigidBody* %chassis, %class.btRigidBody** %chassis.addr, align 4
  store %struct.btVehicleRaycaster* %raycaster, %struct.btVehicleRaycaster** %raycaster.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = bitcast %class.btRaycastVehicle* %this1 to %class.btActionInterface*
  %call = call %class.btActionInterface* @_ZN17btActionInterfaceC2Ev(%class.btActionInterface* %0) #3
  %1 = bitcast %class.btRaycastVehicle* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV16btRaycastVehicle, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_forwardWS = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.5* %m_forwardWS)
  %m_axle = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.5* %m_axle)
  %m_forwardImpulse = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %call4 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.9* %m_forwardImpulse)
  %m_sideImpulse = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %call5 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.9* %m_sideImpulse)
  %m_vehicleRaycaster = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 7
  %2 = load %struct.btVehicleRaycaster*, %struct.btVehicleRaycaster** %raycaster.addr, align 4
  store %struct.btVehicleRaycaster* %2, %struct.btVehicleRaycaster** %m_vehicleRaycaster, align 4
  %m_pitchControl = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 8
  store float 0.000000e+00, float* %m_pitchControl, align 4
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %call6 = call %class.btAlignedObjectArray.13* @_ZN20btAlignedObjectArrayI11btWheelInfoEC2Ev(%class.btAlignedObjectArray.13* %m_wheelInfo)
  %3 = load %class.btRigidBody*, %class.btRigidBody** %chassis.addr, align 4
  %m_chassisBody = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 11
  store %class.btRigidBody* %3, %class.btRigidBody** %m_chassisBody, align 4
  %m_indexRightAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 12
  store i32 0, i32* %m_indexRightAxis, align 4
  %m_indexUpAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 13
  store i32 2, i32* %m_indexUpAxis, align 4
  %m_indexForwardAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 14
  store i32 1, i32* %m_indexForwardAxis, align 4
  %4 = load %"class.btRaycastVehicle::btVehicleTuning"*, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4
  call void @_ZN16btRaycastVehicle11defaultInitERKNS_15btVehicleTuningE(%class.btRaycastVehicle* %this1, %"class.btRaycastVehicle::btVehicleTuning"* nonnull align 4 dereferenceable(24) %4)
  ret %class.btRaycastVehicle* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btActionInterface* @_ZN17btActionInterfaceC2Ev(%class.btActionInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btActionInterface*, align 4
  store %class.btActionInterface* %this, %class.btActionInterface** %this.addr, align 4
  %this1 = load %class.btActionInterface*, %class.btActionInterface** %this.addr, align 4
  %0 = bitcast %class.btActionInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTV17btActionInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btActionInterface* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.5* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.6* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator.6* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.5* %this1)
  ret %class.btAlignedObjectArray.5* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.9* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.10* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.10* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.9* %this1)
  ret %class.btAlignedObjectArray.9* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.13* @_ZN20btAlignedObjectArrayI11btWheelInfoEC2Ev(%class.btAlignedObjectArray.13* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.14* @_ZN18btAlignedAllocatorI11btWheelInfoLj16EEC2Ev(%class.btAlignedAllocator.14* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI11btWheelInfoE4initEv(%class.btAlignedObjectArray.13* %this1)
  ret %class.btAlignedObjectArray.13* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN16btRaycastVehicle11defaultInitERKNS_15btVehicleTuningE(%class.btRaycastVehicle* %this, %"class.btRaycastVehicle::btVehicleTuning"* nonnull align 4 dereferenceable(24) %tuning) #1 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %tuning.addr = alloca %"class.btRaycastVehicle::btVehicleTuning"*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  store %"class.btRaycastVehicle::btVehicleTuning"* %tuning, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = load %"class.btRaycastVehicle::btVehicleTuning"*, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4
  %m_currentVehicleSpeedKmHour = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 10
  store float 0.000000e+00, float* %m_currentVehicleSpeedKmHour, align 4
  %m_steeringValue = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 9
  store float 0.000000e+00, float* %m_steeringValue, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btRaycastVehicle* @_ZN16btRaycastVehicleD2Ev(%class.btRaycastVehicle* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = bitcast %class.btRaycastVehicle* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV16btRaycastVehicle, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %call = call %class.btAlignedObjectArray.13* @_ZN20btAlignedObjectArrayI11btWheelInfoED2Ev(%class.btAlignedObjectArray.13* %m_wheelInfo) #3
  %m_sideImpulse = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %call2 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.9* %m_sideImpulse) #3
  %m_forwardImpulse = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %call3 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.9* %m_forwardImpulse) #3
  %m_axle = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %call4 = call %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.5* %m_axle) #3
  %m_forwardWS = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 1
  %call5 = call %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.5* %m_forwardWS) #3
  %1 = bitcast %class.btRaycastVehicle* %this1 to %class.btActionInterface*
  %call6 = call %class.btActionInterface* @_ZN17btActionInterfaceD2Ev(%class.btActionInterface* %1) #3
  ret %class.btRaycastVehicle* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.13* @_ZN20btAlignedObjectArrayI11btWheelInfoED2Ev(%class.btAlignedObjectArray.13* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI11btWheelInfoE5clearEv(%class.btAlignedObjectArray.13* %this1)
  ret %class.btAlignedObjectArray.13* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.9* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.9* %this1)
  ret %class.btAlignedObjectArray.9* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.5* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.5* %this1)
  ret %class.btAlignedObjectArray.5* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN16btRaycastVehicleD0Ev(%class.btRaycastVehicle* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %call = call %class.btRaycastVehicle* @_ZN16btRaycastVehicleD1Ev(%class.btRaycastVehicle* %this1) #3
  %0 = bitcast %class.btRaycastVehicle* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

; Function Attrs: noinline optnone
define hidden nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle8addWheelERK9btVector3S2_S2_ffRKNS_15btVehicleTuningEb(%class.btRaycastVehicle* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %connectionPointCS, %class.btVector3* nonnull align 4 dereferenceable(16) %wheelDirectionCS0, %class.btVector3* nonnull align 4 dereferenceable(16) %wheelAxleCS, float %suspensionRestLength, float %wheelRadius, %"class.btRaycastVehicle::btVehicleTuning"* nonnull align 4 dereferenceable(24) %tuning, i1 zeroext %isFrontWheel) #2 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %connectionPointCS.addr = alloca %class.btVector3*, align 4
  %wheelDirectionCS0.addr = alloca %class.btVector3*, align 4
  %wheelAxleCS.addr = alloca %class.btVector3*, align 4
  %suspensionRestLength.addr = alloca float, align 4
  %wheelRadius.addr = alloca float, align 4
  %tuning.addr = alloca %"class.btRaycastVehicle::btVehicleTuning"*, align 4
  %isFrontWheel.addr = alloca i8, align 1
  %ci = alloca %struct.btWheelInfoConstructionInfo, align 4
  %ref.tmp = alloca %struct.btWheelInfo, align 4
  %wheel = alloca %struct.btWheelInfo*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  store %class.btVector3* %connectionPointCS, %class.btVector3** %connectionPointCS.addr, align 4
  store %class.btVector3* %wheelDirectionCS0, %class.btVector3** %wheelDirectionCS0.addr, align 4
  store %class.btVector3* %wheelAxleCS, %class.btVector3** %wheelAxleCS.addr, align 4
  store float %suspensionRestLength, float* %suspensionRestLength.addr, align 4
  store float %wheelRadius, float* %wheelRadius.addr, align 4
  store %"class.btRaycastVehicle::btVehicleTuning"* %tuning, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4
  %frombool = zext i1 %isFrontWheel to i8
  store i8 %frombool, i8* %isFrontWheel.addr, align 1
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %call = call %struct.btWheelInfoConstructionInfo* @_ZN27btWheelInfoConstructionInfoC2Ev(%struct.btWheelInfoConstructionInfo* %ci)
  %0 = load %class.btVector3*, %class.btVector3** %connectionPointCS.addr, align 4
  %m_chassisConnectionCS = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 0
  %1 = bitcast %class.btVector3* %m_chassisConnectionCS to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btVector3*, %class.btVector3** %wheelDirectionCS0.addr, align 4
  %m_wheelDirectionCS = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 1
  %4 = bitcast %class.btVector3* %m_wheelDirectionCS to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btVector3*, %class.btVector3** %wheelAxleCS.addr, align 4
  %m_wheelAxleCS = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 2
  %7 = bitcast %class.btVector3* %m_wheelAxleCS to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load float, float* %suspensionRestLength.addr, align 4
  %m_suspensionRestLength = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 3
  store float %9, float* %m_suspensionRestLength, align 4
  %10 = load float, float* %wheelRadius.addr, align 4
  %m_wheelRadius = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 5
  store float %10, float* %m_wheelRadius, align 4
  %11 = load %"class.btRaycastVehicle::btVehicleTuning"*, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4
  %m_suspensionStiffness = getelementptr inbounds %"class.btRaycastVehicle::btVehicleTuning", %"class.btRaycastVehicle::btVehicleTuning"* %11, i32 0, i32 0
  %12 = load float, float* %m_suspensionStiffness, align 4
  %m_suspensionStiffness2 = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 6
  store float %12, float* %m_suspensionStiffness2, align 4
  %13 = load %"class.btRaycastVehicle::btVehicleTuning"*, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4
  %m_suspensionCompression = getelementptr inbounds %"class.btRaycastVehicle::btVehicleTuning", %"class.btRaycastVehicle::btVehicleTuning"* %13, i32 0, i32 1
  %14 = load float, float* %m_suspensionCompression, align 4
  %m_wheelsDampingCompression = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 7
  store float %14, float* %m_wheelsDampingCompression, align 4
  %15 = load %"class.btRaycastVehicle::btVehicleTuning"*, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4
  %m_suspensionDamping = getelementptr inbounds %"class.btRaycastVehicle::btVehicleTuning", %"class.btRaycastVehicle::btVehicleTuning"* %15, i32 0, i32 2
  %16 = load float, float* %m_suspensionDamping, align 4
  %m_wheelsDampingRelaxation = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 8
  store float %16, float* %m_wheelsDampingRelaxation, align 4
  %17 = load %"class.btRaycastVehicle::btVehicleTuning"*, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4
  %m_frictionSlip = getelementptr inbounds %"class.btRaycastVehicle::btVehicleTuning", %"class.btRaycastVehicle::btVehicleTuning"* %17, i32 0, i32 4
  %18 = load float, float* %m_frictionSlip, align 4
  %m_frictionSlip3 = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 9
  store float %18, float* %m_frictionSlip3, align 4
  %19 = load i8, i8* %isFrontWheel.addr, align 1
  %tobool = trunc i8 %19 to i1
  %m_bIsFrontWheel = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 11
  %frombool4 = zext i1 %tobool to i8
  store i8 %frombool4, i8* %m_bIsFrontWheel, align 4
  %20 = load %"class.btRaycastVehicle::btVehicleTuning"*, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4
  %m_maxSuspensionTravelCm = getelementptr inbounds %"class.btRaycastVehicle::btVehicleTuning", %"class.btRaycastVehicle::btVehicleTuning"* %20, i32 0, i32 3
  %21 = load float, float* %m_maxSuspensionTravelCm, align 4
  %m_maxSuspensionTravelCm5 = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 4
  store float %21, float* %m_maxSuspensionTravelCm5, align 4
  %22 = load %"class.btRaycastVehicle::btVehicleTuning"*, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4
  %m_maxSuspensionForce = getelementptr inbounds %"class.btRaycastVehicle::btVehicleTuning", %"class.btRaycastVehicle::btVehicleTuning"* %22, i32 0, i32 5
  %23 = load float, float* %m_maxSuspensionForce, align 4
  %m_maxSuspensionForce6 = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 10
  store float %23, float* %m_maxSuspensionForce6, align 4
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %call7 = call %struct.btWheelInfo* @_ZN11btWheelInfoC2ER27btWheelInfoConstructionInfo(%struct.btWheelInfo* %ref.tmp, %struct.btWheelInfoConstructionInfo* nonnull align 4 dereferenceable(81) %ci)
  call void @_ZN20btAlignedObjectArrayI11btWheelInfoE9push_backERKS0_(%class.btAlignedObjectArray.13* %m_wheelInfo, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %ref.tmp)
  %m_wheelInfo8 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %call9 = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %sub = sub nsw i32 %call9, 1
  %call10 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo8, i32 %sub)
  store %struct.btWheelInfo* %call10, %struct.btWheelInfo** %wheel, align 4
  %24 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  call void @_ZN16btRaycastVehicle23updateWheelTransformsWSER11btWheelInfob(%class.btRaycastVehicle* %this1, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %24, i1 zeroext false)
  %call11 = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %sub12 = sub nsw i32 %call11, 1
  call void @_ZN16btRaycastVehicle20updateWheelTransformEib(%class.btRaycastVehicle* %this1, i32 %sub12, i1 zeroext false)
  %25 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  ret %struct.btWheelInfo* %25
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btWheelInfoConstructionInfo* @_ZN27btWheelInfoConstructionInfoC2Ev(%struct.btWheelInfoConstructionInfo* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btWheelInfoConstructionInfo*, align 4
  store %struct.btWheelInfoConstructionInfo* %this, %struct.btWheelInfoConstructionInfo** %this.addr, align 4
  %this1 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %this.addr, align 4
  %m_chassisConnectionCS = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_chassisConnectionCS)
  %m_wheelDirectionCS = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_wheelDirectionCS)
  %m_wheelAxleCS = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_wheelAxleCS)
  ret %struct.btWheelInfoConstructionInfo* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #6

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btWheelInfoE9push_backERKS0_(%class.btAlignedObjectArray.13* %this, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %_Val.addr = alloca %struct.btWheelInfo*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store %struct.btWheelInfo* %_Val, %struct.btWheelInfo** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE8capacityEv(%class.btAlignedObjectArray.13* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI11btWheelInfoE9allocSizeEi(%class.btAlignedObjectArray.13* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI11btWheelInfoE7reserveEi(%class.btAlignedObjectArray.13* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %1 = load %struct.btWheelInfo*, %struct.btWheelInfo** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %1, i32 %2
  %3 = bitcast %struct.btWheelInfo* %arrayidx to i8*
  %4 = bitcast i8* %3 to %struct.btWheelInfo*
  %5 = load %struct.btWheelInfo*, %struct.btWheelInfo** %_Val.addr, align 4
  %call5 = call %struct.btWheelInfo* @_ZN11btWheelInfoC2ERKS_(%struct.btWheelInfo* %4, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %5)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 2
  %6 = load i32, i32* %m_size6, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %m_size6, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btWheelInfo* @_ZN11btWheelInfoC2ER27btWheelInfoConstructionInfo(%struct.btWheelInfo* returned %this, %struct.btWheelInfoConstructionInfo* nonnull align 4 dereferenceable(81) %ci) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btWheelInfo*, align 4
  %ci.addr = alloca %struct.btWheelInfoConstructionInfo*, align 4
  store %struct.btWheelInfo* %this, %struct.btWheelInfo** %this.addr, align 4
  store %struct.btWheelInfoConstructionInfo* %ci, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4
  %this1 = load %struct.btWheelInfo*, %struct.btWheelInfo** %this.addr, align 4
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 0
  %call = call %"struct.btWheelInfo::RaycastInfo"* @_ZN11btWheelInfo11RaycastInfoC2Ev(%"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo)
  %m_worldTransform = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 1
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_worldTransform)
  %m_chassisConnectionPointCS = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_chassisConnectionPointCS)
  %m_wheelDirectionCS = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_wheelDirectionCS)
  %m_wheelAxleCS = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_wheelAxleCS)
  %0 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4
  %m_suspensionRestLength = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %0, i32 0, i32 3
  %1 = load float, float* %m_suspensionRestLength, align 4
  %m_suspensionRestLength1 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 5
  store float %1, float* %m_suspensionRestLength1, align 4
  %2 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4
  %m_maxSuspensionTravelCm = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %2, i32 0, i32 4
  %3 = load float, float* %m_maxSuspensionTravelCm, align 4
  %m_maxSuspensionTravelCm6 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 6
  store float %3, float* %m_maxSuspensionTravelCm6, align 4
  %4 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4
  %m_wheelRadius = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %4, i32 0, i32 5
  %5 = load float, float* %m_wheelRadius, align 4
  %m_wheelsRadius = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 7
  store float %5, float* %m_wheelsRadius, align 4
  %6 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4
  %m_suspensionStiffness = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %6, i32 0, i32 6
  %7 = load float, float* %m_suspensionStiffness, align 4
  %m_suspensionStiffness7 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 8
  store float %7, float* %m_suspensionStiffness7, align 4
  %8 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4
  %m_wheelsDampingCompression = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %8, i32 0, i32 7
  %9 = load float, float* %m_wheelsDampingCompression, align 4
  %m_wheelsDampingCompression8 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 9
  store float %9, float* %m_wheelsDampingCompression8, align 4
  %10 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4
  %m_wheelsDampingRelaxation = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %10, i32 0, i32 8
  %11 = load float, float* %m_wheelsDampingRelaxation, align 4
  %m_wheelsDampingRelaxation9 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 10
  store float %11, float* %m_wheelsDampingRelaxation9, align 4
  %12 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4
  %m_chassisConnectionCS = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %12, i32 0, i32 0
  %m_chassisConnectionPointCS10 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 2
  %13 = bitcast %class.btVector3* %m_chassisConnectionPointCS10 to i8*
  %14 = bitcast %class.btVector3* %m_chassisConnectionCS to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false)
  %15 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4
  %m_wheelDirectionCS11 = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %15, i32 0, i32 1
  %m_wheelDirectionCS12 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 3
  %16 = bitcast %class.btVector3* %m_wheelDirectionCS12 to i8*
  %17 = bitcast %class.btVector3* %m_wheelDirectionCS11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false)
  %18 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4
  %m_wheelAxleCS13 = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %18, i32 0, i32 2
  %m_wheelAxleCS14 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 4
  %19 = bitcast %class.btVector3* %m_wheelAxleCS14 to i8*
  %20 = bitcast %class.btVector3* %m_wheelAxleCS13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  %21 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4
  %m_frictionSlip = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %21, i32 0, i32 9
  %22 = load float, float* %m_frictionSlip, align 4
  %m_frictionSlip15 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 11
  store float %22, float* %m_frictionSlip15, align 4
  %m_steering = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 12
  store float 0.000000e+00, float* %m_steering, align 4
  %m_engineForce = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 17
  store float 0.000000e+00, float* %m_engineForce, align 4
  %m_rotation = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 13
  store float 0.000000e+00, float* %m_rotation, align 4
  %m_deltaRotation = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 14
  store float 0.000000e+00, float* %m_deltaRotation, align 4
  %m_brake = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 18
  store float 0.000000e+00, float* %m_brake, align 4
  %m_rollInfluence = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 15
  store float 0x3FB99999A0000000, float* %m_rollInfluence, align 4
  %23 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4
  %m_bIsFrontWheel = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %23, i32 0, i32 11
  %24 = load i8, i8* %m_bIsFrontWheel, align 4
  %tobool = trunc i8 %24 to i1
  %m_bIsFrontWheel16 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 19
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %m_bIsFrontWheel16, align 4
  %25 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4
  %m_maxSuspensionForce = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %25, i32 0, i32 10
  %26 = load float, float* %m_maxSuspensionForce, align 4
  %m_maxSuspensionForce17 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 16
  store float %26, float* %m_maxSuspensionForce17, align 4
  ret %struct.btWheelInfo* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %0 = load %struct.btWheelInfo*, %struct.btWheelInfo** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %0, i32 %1
  ret %struct.btWheelInfo* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.13* %m_wheelInfo)
  ret i32 %call
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btRaycastVehicle23updateWheelTransformsWSER11btWheelInfob(%class.btRaycastVehicle* %this, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %wheel, i1 zeroext %interpolatedTransform) #2 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %wheel.addr = alloca %struct.btWheelInfo*, align 4
  %interpolatedTransform.addr = alloca i8, align 1
  %chassisTrans = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  store %struct.btWheelInfo* %wheel, %struct.btWheelInfo** %wheel.addr, align 4
  %frombool = zext i1 %interpolatedTransform to i8
  store i8 %frombool, i8* %interpolatedTransform.addr, align 1
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %0, i32 0, i32 0
  %m_isInContact = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo, i32 0, i32 6
  store i8 0, i8* %m_isInContact, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK16btRaycastVehicle24getChassisWorldTransformEv(%class.btRaycastVehicle* %this1)
  %call2 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %chassisTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %call)
  %1 = load i8, i8* %interpolatedTransform.addr, align 1
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %call3 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  %call4 = call %class.btMotionState* @_ZN11btRigidBody14getMotionStateEv(%class.btRigidBody* %call3)
  %tobool5 = icmp ne %class.btMotionState* %call4, null
  br i1 %tobool5, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %call6 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  %call7 = call %class.btMotionState* @_ZN11btRigidBody14getMotionStateEv(%class.btRigidBody* %call6)
  %2 = bitcast %class.btMotionState* %call7 to void (%class.btMotionState*, %class.btTransform*)***
  %vtable = load void (%class.btMotionState*, %class.btTransform*)**, void (%class.btMotionState*, %class.btTransform*)*** %2, align 4
  %vfn = getelementptr inbounds void (%class.btMotionState*, %class.btTransform*)*, void (%class.btMotionState*, %class.btTransform*)** %vtable, i64 2
  %3 = load void (%class.btMotionState*, %class.btTransform*)*, void (%class.btMotionState*, %class.btTransform*)** %vfn, align 4
  call void %3(%class.btMotionState* %call7, %class.btTransform* nonnull align 4 dereferenceable(64) %chassisTrans)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %4 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_chassisConnectionPointCS = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %4, i32 0, i32 2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btTransform* %chassisTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %m_chassisConnectionPointCS)
  %5 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo8 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %5, i32 0, i32 0
  %m_hardPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo8, i32 0, i32 3
  %6 = bitcast %class.btVector3* %m_hardPointWS to i8*
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %call10 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %chassisTrans)
  %8 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_wheelDirectionCS = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %8, i32 0, i32 3
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp9, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call10, %class.btVector3* nonnull align 4 dereferenceable(16) %m_wheelDirectionCS)
  %9 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo11 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %9, i32 0, i32 0
  %m_wheelDirectionWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo11, i32 0, i32 4
  %10 = bitcast %class.btVector3* %m_wheelDirectionWS to i8*
  %11 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  %call13 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %chassisTrans)
  %12 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_wheelAxleCS = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %12, i32 0, i32 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp12, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call13, %class.btVector3* nonnull align 4 dereferenceable(16) %m_wheelAxleCS)
  %13 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo14 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %13, i32 0, i32 0
  %m_wheelAxleWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo14, i32 0, i32 5
  %14 = bitcast %class.btVector3* %m_wheelAxleWS to i8*
  %15 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btRaycastVehicle20updateWheelTransformEib(%class.btRaycastVehicle* %this, i32 %wheelIndex, i1 zeroext %interpolatedTransform) #2 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %wheelIndex.addr = alloca i32, align 4
  %interpolatedTransform.addr = alloca i8, align 1
  %wheel = alloca %struct.btWheelInfo*, align 4
  %up = alloca %class.btVector3, align 4
  %right = alloca %class.btVector3*, align 4
  %fwd = alloca %class.btVector3, align 4
  %steering = alloca float, align 4
  %steeringOrn = alloca %class.btQuaternion, align 4
  %steeringMat = alloca %class.btMatrix3x3, align 4
  %rotatingOrn = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca float, align 4
  %rotatingMat = alloca %class.btMatrix3x3, align 4
  %basis2 = alloca %class.btMatrix3x3, align 4
  %ref.tmp26 = alloca %class.btMatrix3x3, align 4
  %ref.tmp27 = alloca %class.btMatrix3x3, align 4
  %ref.tmp29 = alloca %class.btVector3, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  store i32 %wheelIndex, i32* %wheelIndex.addr, align 4
  %frombool = zext i1 %interpolatedTransform to i8
  store i8 %frombool, i8* %interpolatedTransform.addr, align 1
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %0 = load i32, i32* %wheelIndex.addr, align 4
  %call = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo, i32 %0)
  store %struct.btWheelInfo* %call, %struct.btWheelInfo** %wheel, align 4
  %1 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %2 = load i8, i8* %interpolatedTransform.addr, align 1
  %tobool = trunc i8 %2 to i1
  call void @_ZN16btRaycastVehicle23updateWheelTransformsWSER11btWheelInfob(%class.btRaycastVehicle* %this1, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %1, i1 zeroext %tobool)
  %3 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %3, i32 0, i32 0
  %m_wheelDirectionWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo, i32 0, i32 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %up, %class.btVector3* nonnull align 4 dereferenceable(16) %m_wheelDirectionWS)
  %4 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_raycastInfo2 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %4, i32 0, i32 0
  %m_wheelAxleWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo2, i32 0, i32 5
  store %class.btVector3* %m_wheelAxleWS, %class.btVector3** %right, align 4
  %5 = load %class.btVector3*, %class.btVector3** %right, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %fwd, %class.btVector3* %up, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %fwd)
  %6 = bitcast %class.btVector3* %fwd to i8*
  %7 = bitcast %class.btVector3* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %8 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_steering = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %8, i32 0, i32 12
  %9 = load float, float* %m_steering, align 4
  store float %9, float* %steering, align 4
  %call4 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %steeringOrn, %class.btVector3* nonnull align 4 dereferenceable(16) %up, float* nonnull align 4 dereferenceable(4) %steering)
  %call5 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %steeringMat, %class.btQuaternion* nonnull align 4 dereferenceable(16) %steeringOrn)
  %10 = load %class.btVector3*, %class.btVector3** %right, align 4
  %11 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_rotation = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %11, i32 0, i32 13
  %12 = load float, float* %m_rotation, align 4
  %fneg = fneg float %12
  store float %fneg, float* %ref.tmp, align 4
  %call6 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %rotatingOrn, %class.btVector3* nonnull align 4 dereferenceable(16) %10, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %call7 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %rotatingMat, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotatingOrn)
  %13 = load %class.btVector3*, %class.btVector3** %right, align 4
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %13)
  %arrayidx = getelementptr inbounds float, float* %call8, i32 0
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %fwd)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 0
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %up)
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 0
  %14 = load %class.btVector3*, %class.btVector3** %right, align 4
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %14)
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 1
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %fwd)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 1
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %up)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  %15 = load %class.btVector3*, %class.btVector3** %right, align 4
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %15)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 2
  %call21 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %fwd)
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 2
  %call23 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %up)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 2
  %call25 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %basis2, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx10, float* nonnull align 4 dereferenceable(4) %arrayidx12, float* nonnull align 4 dereferenceable(4) %arrayidx14, float* nonnull align 4 dereferenceable(4) %arrayidx16, float* nonnull align 4 dereferenceable(4) %arrayidx18, float* nonnull align 4 dereferenceable(4) %arrayidx20, float* nonnull align 4 dereferenceable(4) %arrayidx22, float* nonnull align 4 dereferenceable(4) %arrayidx24)
  %16 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_worldTransform = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %16, i32 0, i32 1
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp27, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %steeringMat, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %rotatingMat)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp26, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp27, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %basis2)
  call void @_ZN11btTransform8setBasisERK11btMatrix3x3(%class.btTransform* %m_worldTransform, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp26)
  %17 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_worldTransform28 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %17, i32 0, i32 1
  %18 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_raycastInfo30 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %18, i32 0, i32 0
  %m_hardPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo30, i32 0, i32 3
  %19 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_raycastInfo32 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %19, i32 0, i32 0
  %m_wheelDirectionWS33 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo32, i32 0, i32 4
  %20 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_raycastInfo34 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %20, i32 0, i32 0
  %m_suspensionLength = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo34, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %m_wheelDirectionWS33, float* nonnull align 4 dereferenceable(4) %m_suspensionLength)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp29, %class.btVector3* nonnull align 4 dereferenceable(16) %m_hardPointWS, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp31)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %m_worldTransform28, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp29)
  ret void
}

; Function Attrs: noinline optnone
define hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK16btRaycastVehicle19getWheelTransformWSEi(%class.btRaycastVehicle* %this, i32 %wheelIndex) #2 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %wheelIndex.addr = alloca i32, align 4
  %wheel = alloca %struct.btWheelInfo*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  store i32 %wheelIndex, i32* %wheelIndex.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %0 = load i32, i32* %wheelIndex.addr, align 4
  %call = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZNK20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo, i32 %0)
  store %struct.btWheelInfo* %call, %struct.btWheelInfo** %wheel, align 4
  %1 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_worldTransform = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZNK20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %0 = load %struct.btWheelInfo*, %struct.btWheelInfo** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %0, i32 %1
  ret %struct.btWheelInfo* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_axis, float* nonnull align 4 dereferenceable(4) %_angle) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btVector3* %_axis, %class.btVector3** %_axis.addr, align 4
  store float* %_angle, float** %_angle.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  %1 = load %class.btVector3*, %class.btVector3** %_axis.addr, align 4
  %2 = load float*, float** %_angle.addr, align 4
  call void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %2)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* returned %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform8setBasisERK11btMatrix3x3(%class.btTransform* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %basis) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %basis.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btMatrix3x3* %basis, %class.btMatrix3x3** %basis.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %basis.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %10, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btRaycastVehicle15resetSuspensionEv(%class.btRaycastVehicle* %this) #2 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %i = alloca i32, align 4
  %wheel = alloca %struct.btWheelInfo*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.13* %m_wheelInfo)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_wheelInfo2 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %1 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo2, i32 %1)
  store %struct.btWheelInfo* %call3, %struct.btWheelInfo** %wheel, align 4
  %2 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %call4 = call float @_ZNK11btWheelInfo23getSuspensionRestLengthEv(%struct.btWheelInfo* %2)
  %3 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %3, i32 0, i32 0
  %m_suspensionLength = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo, i32 0, i32 2
  store float %call4, float* %m_suspensionLength, align 4
  %4 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_suspensionRelativeVelocity = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %4, i32 0, i32 22
  store float 0.000000e+00, float* %m_suspensionRelativeVelocity, align 4
  %5 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_raycastInfo5 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %5, i32 0, i32 0
  %m_wheelDirectionWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo5, i32 0, i32 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_wheelDirectionWS)
  %6 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_raycastInfo6 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %6, i32 0, i32 0
  %m_contactNormalWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo6, i32 0, i32 0
  %7 = bitcast %class.btVector3* %m_contactNormalWS to i8*
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_clippedInvContactDotSuspension = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %9, i32 0, i32 21
  store float 1.000000e+00, float* %m_clippedInvContactDotSuspension, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.13* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

declare float @_ZNK11btWheelInfo23getSuspensionRestLengthEv(%struct.btWheelInfo*) #4

; Function Attrs: noinline optnone
define hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK16btRaycastVehicle24getChassisWorldTransformEv(%class.btRaycastVehicle* %this) #2 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %call = call %class.btRigidBody* @_ZNK16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call)
  ret %class.btTransform* %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %m_chassisBody = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 11
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_chassisBody, align 4
  ret %class.btRigidBody* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btMotionState* @_ZN11btRigidBody14getMotionStateEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_optionalMotionState = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 20
  %0 = load %class.btMotionState*, %class.btMotionState** %m_optionalMotionState, align 4
  ret %class.btMotionState* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define hidden float @_ZN16btRaycastVehicle7rayCastER11btWheelInfo(%class.btRaycastVehicle* %this, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %wheel) #2 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %wheel.addr = alloca %struct.btWheelInfo*, align 4
  %depth = alloca float, align 4
  %raylen = alloca float, align 4
  %rayvector = alloca %class.btVector3, align 4
  %source = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %target = alloca %class.btVector3*, align 4
  %param = alloca float, align 4
  %rayResults = alloca %"struct.btVehicleRaycaster::btVehicleRaycasterResult", align 4
  %object = alloca i8*, align 4
  %hitDistance = alloca float, align 4
  %minSuspensionLength = alloca float, align 4
  %maxSuspensionLength = alloca float, align 4
  %denominator = alloca float, align 4
  %chassis_velocity_at_contactPoint = alloca %class.btVector3, align 4
  %relpos = alloca %class.btVector3, align 4
  %ref.tmp49 = alloca %class.btVector3, align 4
  %projVel = alloca float, align 4
  %inv = alloca float, align 4
  %ref.tmp65 = alloca %class.btVector3, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  store %struct.btWheelInfo* %wheel, %struct.btWheelInfo** %wheel.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  call void @_ZN16btRaycastVehicle23updateWheelTransformsWSER11btWheelInfob(%class.btRaycastVehicle* %this1, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %0, i1 zeroext false)
  store float -1.000000e+00, float* %depth, align 4
  %1 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %call = call float @_ZNK11btWheelInfo23getSuspensionRestLengthEv(%struct.btWheelInfo* %1)
  %2 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_wheelsRadius = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %2, i32 0, i32 7
  %3 = load float, float* %m_wheelsRadius, align 4
  %add = fadd float %call, %3
  store float %add, float* %raylen, align 4
  %4 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %4, i32 0, i32 0
  %m_wheelDirectionWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo, i32 0, i32 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %rayvector, %class.btVector3* nonnull align 4 dereferenceable(16) %m_wheelDirectionWS, float* nonnull align 4 dereferenceable(4) %raylen)
  %5 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo2 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %5, i32 0, i32 0
  %m_hardPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo2, i32 0, i32 3
  store %class.btVector3* %m_hardPointWS, %class.btVector3** %source, align 4
  %6 = load %class.btVector3*, %class.btVector3** %source, align 4
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %6, %class.btVector3* nonnull align 4 dereferenceable(16) %rayvector)
  %7 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo3 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %7, i32 0, i32 0
  %m_contactPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo3, i32 0, i32 1
  %8 = bitcast %class.btVector3* %m_contactPointWS to i8*
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  %10 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo4 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %10, i32 0, i32 0
  %m_contactPointWS5 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo4, i32 0, i32 1
  store %class.btVector3* %m_contactPointWS5, %class.btVector3** %target, align 4
  store float 0.000000e+00, float* %param, align 4
  %call6 = call %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* @_ZN18btVehicleRaycaster24btVehicleRaycasterResultC2Ev(%"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %rayResults)
  %m_vehicleRaycaster = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 7
  %11 = load %struct.btVehicleRaycaster*, %struct.btVehicleRaycaster** %m_vehicleRaycaster, align 4
  %12 = load %class.btVector3*, %class.btVector3** %source, align 4
  %13 = load %class.btVector3*, %class.btVector3** %target, align 4
  %14 = bitcast %struct.btVehicleRaycaster* %11 to i8* (%struct.btVehicleRaycaster*, %class.btVector3*, %class.btVector3*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*)***
  %vtable = load i8* (%struct.btVehicleRaycaster*, %class.btVector3*, %class.btVector3*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*)**, i8* (%struct.btVehicleRaycaster*, %class.btVector3*, %class.btVector3*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*)*** %14, align 4
  %vfn = getelementptr inbounds i8* (%struct.btVehicleRaycaster*, %class.btVector3*, %class.btVector3*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*)*, i8* (%struct.btVehicleRaycaster*, %class.btVector3*, %class.btVector3*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*)** %vtable, i64 2
  %15 = load i8* (%struct.btVehicleRaycaster*, %class.btVector3*, %class.btVector3*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*)*, i8* (%struct.btVehicleRaycaster*, %class.btVector3*, %class.btVector3*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*)** %vfn, align 4
  %call7 = call i8* %15(%struct.btVehicleRaycaster* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* nonnull align 4 dereferenceable(36) %rayResults)
  store i8* %call7, i8** %object, align 4
  %16 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo8 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %16, i32 0, i32 0
  %m_groundObject = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo8, i32 0, i32 7
  store i8* null, i8** %m_groundObject, align 4
  %17 = load i8*, i8** %object, align 4
  %tobool = icmp ne i8* %17, null
  br i1 %tobool, label %if.then, label %if.else60

if.then:                                          ; preds = %entry
  %m_distFraction = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %rayResults, i32 0, i32 2
  %18 = load float, float* %m_distFraction, align 4
  store float %18, float* %param, align 4
  %19 = load float, float* %raylen, align 4
  %m_distFraction9 = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %rayResults, i32 0, i32 2
  %20 = load float, float* %m_distFraction9, align 4
  %mul = fmul float %19, %20
  store float %mul, float* %depth, align 4
  %m_hitNormalInWorld = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %rayResults, i32 0, i32 1
  %21 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo10 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %21, i32 0, i32 0
  %m_contactNormalWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo10, i32 0, i32 0
  %22 = bitcast %class.btVector3* %m_contactNormalWS to i8*
  %23 = bitcast %class.btVector3* %m_hitNormalInWorld to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 16, i1 false)
  %24 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo11 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %24, i32 0, i32 0
  %m_isInContact = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo11, i32 0, i32 6
  store i8 1, i8* %m_isInContact, align 4
  %call12 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btActionInterface12getFixedBodyEv()
  %25 = bitcast %class.btRigidBody* %call12 to i8*
  %26 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo13 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %26, i32 0, i32 0
  %m_groundObject14 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo13, i32 0, i32 7
  store i8* %25, i8** %m_groundObject14, align 4
  %27 = load float, float* %param, align 4
  %28 = load float, float* %raylen, align 4
  %mul15 = fmul float %27, %28
  store float %mul15, float* %hitDistance, align 4
  %29 = load float, float* %hitDistance, align 4
  %30 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_wheelsRadius16 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %30, i32 0, i32 7
  %31 = load float, float* %m_wheelsRadius16, align 4
  %sub = fsub float %29, %31
  %32 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo17 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %32, i32 0, i32 0
  %m_suspensionLength = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo17, i32 0, i32 2
  store float %sub, float* %m_suspensionLength, align 4
  %33 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %call18 = call float @_ZNK11btWheelInfo23getSuspensionRestLengthEv(%struct.btWheelInfo* %33)
  %34 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_maxSuspensionTravelCm = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %34, i32 0, i32 6
  %35 = load float, float* %m_maxSuspensionTravelCm, align 4
  %mul19 = fmul float %35, 0x3F847AE140000000
  %sub20 = fsub float %call18, %mul19
  store float %sub20, float* %minSuspensionLength, align 4
  %36 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %call21 = call float @_ZNK11btWheelInfo23getSuspensionRestLengthEv(%struct.btWheelInfo* %36)
  %37 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_maxSuspensionTravelCm22 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %37, i32 0, i32 6
  %38 = load float, float* %m_maxSuspensionTravelCm22, align 4
  %mul23 = fmul float %38, 0x3F847AE140000000
  %add24 = fadd float %call21, %mul23
  store float %add24, float* %maxSuspensionLength, align 4
  %39 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo25 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %39, i32 0, i32 0
  %m_suspensionLength26 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo25, i32 0, i32 2
  %40 = load float, float* %m_suspensionLength26, align 4
  %41 = load float, float* %minSuspensionLength, align 4
  %cmp = fcmp olt float %40, %41
  br i1 %cmp, label %if.then27, label %if.end

if.then27:                                        ; preds = %if.then
  %42 = load float, float* %minSuspensionLength, align 4
  %43 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo28 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %43, i32 0, i32 0
  %m_suspensionLength29 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo28, i32 0, i32 2
  store float %42, float* %m_suspensionLength29, align 4
  br label %if.end

if.end:                                           ; preds = %if.then27, %if.then
  %44 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo30 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %44, i32 0, i32 0
  %m_suspensionLength31 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo30, i32 0, i32 2
  %45 = load float, float* %m_suspensionLength31, align 4
  %46 = load float, float* %maxSuspensionLength, align 4
  %cmp32 = fcmp ogt float %45, %46
  br i1 %cmp32, label %if.then33, label %if.end36

if.then33:                                        ; preds = %if.end
  %47 = load float, float* %maxSuspensionLength, align 4
  %48 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo34 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %48, i32 0, i32 0
  %m_suspensionLength35 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo34, i32 0, i32 2
  store float %47, float* %m_suspensionLength35, align 4
  br label %if.end36

if.end36:                                         ; preds = %if.then33, %if.end
  %m_hitPointInWorld = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %rayResults, i32 0, i32 0
  %49 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo37 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %49, i32 0, i32 0
  %m_contactPointWS38 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo37, i32 0, i32 1
  %50 = bitcast %class.btVector3* %m_contactPointWS38 to i8*
  %51 = bitcast %class.btVector3* %m_hitPointInWorld to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %50, i8* align 4 %51, i32 16, i1 false)
  %52 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo39 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %52, i32 0, i32 0
  %m_contactNormalWS40 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo39, i32 0, i32 0
  %53 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo41 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %53, i32 0, i32 0
  %m_wheelDirectionWS42 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo41, i32 0, i32 4
  %call43 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormalWS40, %class.btVector3* nonnull align 4 dereferenceable(16) %m_wheelDirectionWS42)
  store float %call43, float* %denominator, align 4
  %call44 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %chassis_velocity_at_contactPoint)
  %54 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo45 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %54, i32 0, i32 0
  %m_contactPointWS46 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo45, i32 0, i32 1
  %call47 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  %call48 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %call47)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %relpos, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactPointWS46, %class.btVector3* nonnull align 4 dereferenceable(16) %call48)
  %call50 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp49, %class.btRigidBody* %call50, %class.btVector3* nonnull align 4 dereferenceable(16) %relpos)
  %55 = bitcast %class.btVector3* %chassis_velocity_at_contactPoint to i8*
  %56 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %55, i8* align 4 %56, i32 16, i1 false)
  %57 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo51 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %57, i32 0, i32 0
  %m_contactNormalWS52 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo51, i32 0, i32 0
  %call53 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormalWS52, %class.btVector3* nonnull align 4 dereferenceable(16) %chassis_velocity_at_contactPoint)
  store float %call53, float* %projVel, align 4
  %58 = load float, float* %denominator, align 4
  %cmp54 = fcmp oge float %58, 0xBFB99999A0000000
  br i1 %cmp54, label %if.then55, label %if.else

if.then55:                                        ; preds = %if.end36
  %59 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_suspensionRelativeVelocity = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %59, i32 0, i32 22
  store float 0.000000e+00, float* %m_suspensionRelativeVelocity, align 4
  %60 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_clippedInvContactDotSuspension = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %60, i32 0, i32 21
  store float 1.000000e+01, float* %m_clippedInvContactDotSuspension, align 4
  br label %if.end59

if.else:                                          ; preds = %if.end36
  %61 = load float, float* %denominator, align 4
  %div = fdiv float -1.000000e+00, %61
  store float %div, float* %inv, align 4
  %62 = load float, float* %projVel, align 4
  %63 = load float, float* %inv, align 4
  %mul56 = fmul float %62, %63
  %64 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_suspensionRelativeVelocity57 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %64, i32 0, i32 22
  store float %mul56, float* %m_suspensionRelativeVelocity57, align 4
  %65 = load float, float* %inv, align 4
  %66 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_clippedInvContactDotSuspension58 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %66, i32 0, i32 21
  store float %65, float* %m_clippedInvContactDotSuspension58, align 4
  br label %if.end59

if.end59:                                         ; preds = %if.else, %if.then55
  br label %if.end71

if.else60:                                        ; preds = %entry
  %67 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %call61 = call float @_ZNK11btWheelInfo23getSuspensionRestLengthEv(%struct.btWheelInfo* %67)
  %68 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo62 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %68, i32 0, i32 0
  %m_suspensionLength63 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo62, i32 0, i32 2
  store float %call61, float* %m_suspensionLength63, align 4
  %69 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_suspensionRelativeVelocity64 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %69, i32 0, i32 22
  store float 0.000000e+00, float* %m_suspensionRelativeVelocity64, align 4
  %70 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo66 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %70, i32 0, i32 0
  %m_wheelDirectionWS67 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo66, i32 0, i32 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp65, %class.btVector3* nonnull align 4 dereferenceable(16) %m_wheelDirectionWS67)
  %71 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_raycastInfo68 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %71, i32 0, i32 0
  %m_contactNormalWS69 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo68, i32 0, i32 0
  %72 = bitcast %class.btVector3* %m_contactNormalWS69 to i8*
  %73 = bitcast %class.btVector3* %ref.tmp65 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %72, i8* align 4 %73, i32 16, i1 false)
  %74 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4
  %m_clippedInvContactDotSuspension70 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %74, i32 0, i32 21
  store float 1.000000e+00, float* %m_clippedInvContactDotSuspension70, align 4
  br label %if.end71

if.end71:                                         ; preds = %if.else60, %if.end59
  %75 = load float, float* %depth, align 4
  ret float %75
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* @_ZN18btVehicleRaycaster24btVehicleRaycasterResultC2Ev(%"struct.btVehicleRaycaster::btVehicleRaycasterResult"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*, align 4
  store %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %this, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"** %this.addr, align 4
  %this1 = load %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"** %this.addr, align 4
  %m_hitPointInWorld = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hitPointInWorld)
  %m_hitNormalInWorld = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hitNormalInWorld)
  %m_distFraction = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %this1, i32 0, i32 2
  store float -1.000000e+00, float* %m_distFraction, align 4
  ret %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %m_worldTransform)
  ret %class.btVector3* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %rel_pos.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %rel_pos, %class.btVector3** %rel_pos.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %0 = load %class.btVector3*, %class.btVector3** %rel_pos.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btRigidBody* @_ZNK16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %m_chassisBody = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 11
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_chassisBody, align 4
  ret %class.btRigidBody* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btRaycastVehicle13updateVehicleEf(%class.btRaycastVehicle* %this, float %step) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %step.addr = alloca float, align 4
  %i = alloca i32, align 4
  %chassisTrans = alloca %class.btTransform*, align 4
  %forwardW = alloca %class.btVector3, align 4
  %i26 = alloca i32, align 4
  %wheel = alloca %struct.btWheelInfo*, align 4
  %suspensionForce = alloca float, align 4
  %impulse = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %relpos = alloca %class.btVector3, align 4
  %wheel60 = alloca %struct.btWheelInfo*, align 4
  %relpos63 = alloca %class.btVector3, align 4
  %vel = alloca %class.btVector3, align 4
  %chassisWorldTransform = alloca %class.btTransform*, align 4
  %fwd = alloca %class.btVector3, align 4
  %proj = alloca float, align 4
  %ref.tmp90 = alloca %class.btVector3, align 4
  %proj2 = alloca float, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  store float %step, float* %step.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %call = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  call void @_ZN16btRaycastVehicle20updateWheelTransformEib(%class.btRaycastVehicle* %this1, i32 %1, i1 zeroext false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %2 = load i32, i32* %i, align 4
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call2 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %call2)
  %call4 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %call3)
  %mul = fmul float 0x400CCCCCC0000000, %call4
  %m_currentVehicleSpeedKmHour = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 10
  store float %mul, float* %m_currentVehicleSpeedKmHour, align 4
  %call5 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK16btRaycastVehicle24getChassisWorldTransformEv(%class.btRaycastVehicle* %this1)
  store %class.btTransform* %call5, %class.btTransform** %chassisTrans, align 4
  %3 = load %class.btTransform*, %class.btTransform** %chassisTrans, align 4
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %3)
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call6, i32 0)
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call7)
  %m_indexForwardAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 14
  %4 = load i32, i32* %m_indexForwardAxis, align 4
  %arrayidx = getelementptr inbounds float, float* %call8, i32 %4
  %5 = load %class.btTransform*, %class.btTransform** %chassisTrans, align 4
  %call9 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %5)
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call9, i32 1)
  %call11 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call10)
  %m_indexForwardAxis12 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 14
  %6 = load i32, i32* %m_indexForwardAxis12, align 4
  %arrayidx13 = getelementptr inbounds float, float* %call11, i32 %6
  %7 = load %class.btTransform*, %class.btTransform** %chassisTrans, align 4
  %call14 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %7)
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call14, i32 2)
  %call16 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call15)
  %m_indexForwardAxis17 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 14
  %8 = load i32, i32* %m_indexForwardAxis17, align 4
  %arrayidx18 = getelementptr inbounds float, float* %call16, i32 %8
  %call19 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %forwardW, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %arrayidx18)
  %call20 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %call20)
  %call22 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %forwardW, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  %cmp23 = fcmp olt float %call22, 0.000000e+00
  br i1 %cmp23, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %m_currentVehicleSpeedKmHour24 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 10
  %9 = load float, float* %m_currentVehicleSpeedKmHour24, align 4
  %mul25 = fmul float %9, -1.000000e+00
  store float %mul25, float* %m_currentVehicleSpeedKmHour24, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  store i32 0, i32* %i26, align 4
  store i32 0, i32* %i26, align 4
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc34, %if.end
  %10 = load i32, i32* %i26, align 4
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %call28 = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.13* %m_wheelInfo)
  %cmp29 = icmp slt i32 %10, %call28
  br i1 %cmp29, label %for.body30, label %for.end36

for.body30:                                       ; preds = %for.cond27
  %m_wheelInfo31 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %11 = load i32, i32* %i26, align 4
  %call32 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo31, i32 %11)
  %call33 = call float @_ZN16btRaycastVehicle7rayCastER11btWheelInfo(%class.btRaycastVehicle* %this1, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %call32)
  br label %for.inc34

for.inc34:                                        ; preds = %for.body30
  %12 = load i32, i32* %i26, align 4
  %inc35 = add nsw i32 %12, 1
  store i32 %inc35, i32* %i26, align 4
  br label %for.cond27

for.end36:                                        ; preds = %for.cond27
  %13 = load float, float* %step.addr, align 4
  call void @_ZN16btRaycastVehicle16updateSuspensionEf(%class.btRaycastVehicle* %this1, float %13)
  store i32 0, i32* %i26, align 4
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc52, %for.end36
  %14 = load i32, i32* %i26, align 4
  %m_wheelInfo38 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %call39 = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.13* %m_wheelInfo38)
  %cmp40 = icmp slt i32 %14, %call39
  br i1 %cmp40, label %for.body41, label %for.end54

for.body41:                                       ; preds = %for.cond37
  %m_wheelInfo42 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %15 = load i32, i32* %i26, align 4
  %call43 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo42, i32 %15)
  store %struct.btWheelInfo* %call43, %struct.btWheelInfo** %wheel, align 4
  %16 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_wheelsSuspensionForce = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %16, i32 0, i32 23
  %17 = load float, float* %m_wheelsSuspensionForce, align 4
  store float %17, float* %suspensionForce, align 4
  %18 = load float, float* %suspensionForce, align 4
  %19 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_maxSuspensionForce = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %19, i32 0, i32 16
  %20 = load float, float* %m_maxSuspensionForce, align 4
  %cmp44 = fcmp ogt float %18, %20
  br i1 %cmp44, label %if.then45, label %if.end47

if.then45:                                        ; preds = %for.body41
  %21 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_maxSuspensionForce46 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %21, i32 0, i32 16
  %22 = load float, float* %m_maxSuspensionForce46, align 4
  store float %22, float* %suspensionForce, align 4
  br label %if.end47

if.end47:                                         ; preds = %if.then45, %for.body41
  %23 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %23, i32 0, i32 0
  %m_contactNormalWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo, i32 0, i32 0
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormalWS, float* nonnull align 4 dereferenceable(4) %suspensionForce)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %impulse, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %step.addr)
  %24 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4
  %m_raycastInfo48 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %24, i32 0, i32 0
  %m_contactPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo48, i32 0, i32 1
  %call49 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  %call50 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %call49)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %relpos, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactPointWS, %class.btVector3* nonnull align 4 dereferenceable(16) %call50)
  %call51 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  call void @_ZN11btRigidBody12applyImpulseERK9btVector3S2_(%class.btRigidBody* %call51, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse, %class.btVector3* nonnull align 4 dereferenceable(16) %relpos)
  br label %for.inc52

for.inc52:                                        ; preds = %if.end47
  %25 = load i32, i32* %i26, align 4
  %inc53 = add nsw i32 %25, 1
  store i32 %inc53, i32* %i26, align 4
  br label %for.cond37

for.end54:                                        ; preds = %for.cond37
  %26 = load float, float* %step.addr, align 4
  %27 = bitcast %class.btRaycastVehicle* %this1 to void (%class.btRaycastVehicle*, float)***
  %vtable = load void (%class.btRaycastVehicle*, float)**, void (%class.btRaycastVehicle*, float)*** %27, align 4
  %vfn = getelementptr inbounds void (%class.btRaycastVehicle*, float)*, void (%class.btRaycastVehicle*, float)** %vtable, i64 5
  %28 = load void (%class.btRaycastVehicle*, float)*, void (%class.btRaycastVehicle*, float)** %vfn, align 4
  call void %28(%class.btRaycastVehicle* %this1, float %26)
  store i32 0, i32* %i26, align 4
  br label %for.cond55

for.cond55:                                       ; preds = %for.inc103, %for.end54
  %29 = load i32, i32* %i26, align 4
  %m_wheelInfo56 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %call57 = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.13* %m_wheelInfo56)
  %cmp58 = icmp slt i32 %29, %call57
  br i1 %cmp58, label %for.body59, label %for.end105

for.body59:                                       ; preds = %for.cond55
  %m_wheelInfo61 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %30 = load i32, i32* %i26, align 4
  %call62 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo61, i32 %30)
  store %struct.btWheelInfo* %call62, %struct.btWheelInfo** %wheel60, align 4
  %31 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4
  %m_raycastInfo64 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %31, i32 0, i32 0
  %m_hardPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo64, i32 0, i32 3
  %call65 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  %call66 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %call65)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %relpos63, %class.btVector3* nonnull align 4 dereferenceable(16) %m_hardPointWS, %class.btVector3* nonnull align 4 dereferenceable(16) %call66)
  %call67 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %vel, %class.btRigidBody* %call67, %class.btVector3* nonnull align 4 dereferenceable(16) %relpos63)
  %32 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4
  %m_raycastInfo68 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %32, i32 0, i32 0
  %m_isInContact = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo68, i32 0, i32 6
  %33 = load i8, i8* %m_isInContact, align 4
  %tobool = trunc i8 %33 to i1
  br i1 %tobool, label %if.then69, label %if.else

if.then69:                                        ; preds = %for.body59
  %call70 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK16btRaycastVehicle24getChassisWorldTransformEv(%class.btRaycastVehicle* %this1)
  store %class.btTransform* %call70, %class.btTransform** %chassisWorldTransform, align 4
  %34 = load %class.btTransform*, %class.btTransform** %chassisWorldTransform, align 4
  %call71 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %34)
  %call72 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call71, i32 0)
  %call73 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call72)
  %m_indexForwardAxis74 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 14
  %35 = load i32, i32* %m_indexForwardAxis74, align 4
  %arrayidx75 = getelementptr inbounds float, float* %call73, i32 %35
  %36 = load %class.btTransform*, %class.btTransform** %chassisWorldTransform, align 4
  %call76 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %36)
  %call77 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call76, i32 1)
  %call78 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call77)
  %m_indexForwardAxis79 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 14
  %37 = load i32, i32* %m_indexForwardAxis79, align 4
  %arrayidx80 = getelementptr inbounds float, float* %call78, i32 %37
  %38 = load %class.btTransform*, %class.btTransform** %chassisWorldTransform, align 4
  %call81 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %38)
  %call82 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call81, i32 2)
  %call83 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call82)
  %m_indexForwardAxis84 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 14
  %39 = load i32, i32* %m_indexForwardAxis84, align 4
  %arrayidx85 = getelementptr inbounds float, float* %call83, i32 %39
  %call86 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %fwd, float* nonnull align 4 dereferenceable(4) %arrayidx75, float* nonnull align 4 dereferenceable(4) %arrayidx80, float* nonnull align 4 dereferenceable(4) %arrayidx85)
  %40 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4
  %m_raycastInfo87 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %40, i32 0, i32 0
  %m_contactNormalWS88 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo87, i32 0, i32 0
  %call89 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %fwd, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormalWS88)
  store float %call89, float* %proj, align 4
  %41 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4
  %m_raycastInfo91 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %41, i32 0, i32 0
  %m_contactNormalWS92 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo91, i32 0, i32 0
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp90, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormalWS92, float* nonnull align 4 dereferenceable(4) %proj)
  %call93 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %fwd, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp90)
  %call94 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %fwd, %class.btVector3* nonnull align 4 dereferenceable(16) %vel)
  store float %call94, float* %proj2, align 4
  %42 = load float, float* %proj2, align 4
  %43 = load float, float* %step.addr, align 4
  %mul95 = fmul float %42, %43
  %44 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4
  %m_wheelsRadius = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %44, i32 0, i32 7
  %45 = load float, float* %m_wheelsRadius, align 4
  %div = fdiv float %mul95, %45
  %46 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4
  %m_deltaRotation = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %46, i32 0, i32 14
  store float %div, float* %m_deltaRotation, align 4
  %47 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4
  %m_deltaRotation96 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %47, i32 0, i32 14
  %48 = load float, float* %m_deltaRotation96, align 4
  %49 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4
  %m_rotation = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %49, i32 0, i32 13
  %50 = load float, float* %m_rotation, align 4
  %add = fadd float %50, %48
  store float %add, float* %m_rotation, align 4
  br label %if.end100

if.else:                                          ; preds = %for.body59
  %51 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4
  %m_deltaRotation97 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %51, i32 0, i32 14
  %52 = load float, float* %m_deltaRotation97, align 4
  %53 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4
  %m_rotation98 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %53, i32 0, i32 13
  %54 = load float, float* %m_rotation98, align 4
  %add99 = fadd float %54, %52
  store float %add99, float* %m_rotation98, align 4
  br label %if.end100

if.end100:                                        ; preds = %if.else, %if.then69
  %55 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4
  %m_deltaRotation101 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %55, i32 0, i32 14
  %56 = load float, float* %m_deltaRotation101, align 4
  %mul102 = fmul float %56, 0x3FEFAE1480000000
  store float %mul102, float* %m_deltaRotation101, align 4
  br label %for.inc103

for.inc103:                                       ; preds = %if.end100
  %57 = load i32, i32* %i26, align 4
  %inc104 = add nsw i32 %57, 1
  store i32 %inc104, i32* %i26, align 4
  br label %for.cond55

for.end105:                                       ; preds = %for.cond55
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  ret %class.btVector3* %m_linearVelocity
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btRaycastVehicle16updateSuspensionEf(%class.btRaycastVehicle* %this, float %deltaTime) #2 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %deltaTime.addr = alloca float, align 4
  %chassisMass = alloca float, align 4
  %w_it = alloca i32, align 4
  %wheel_info = alloca %struct.btWheelInfo*, align 4
  %force = alloca float, align 4
  %susp_length = alloca float, align 4
  %current_length = alloca float, align 4
  %length_diff = alloca float, align 4
  %projected_rel_vel = alloca float, align 4
  %susp_damping = alloca float, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  store float %deltaTime, float* %deltaTime.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %m_chassisBody = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 11
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_chassisBody, align 4
  %call = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %0)
  %div = fdiv float 1.000000e+00, %call
  store float %div, float* %chassisMass, align 4
  store i32 0, i32* %w_it, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %w_it, align 4
  %call2 = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %cmp = icmp slt i32 %1, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %2 = load i32, i32* %w_it, align 4
  %call3 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo, i32 %2)
  store %struct.btWheelInfo* %call3, %struct.btWheelInfo** %wheel_info, align 4
  %3 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %3, i32 0, i32 0
  %m_isInContact = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo, i32 0, i32 6
  %4 = load i8, i8* %m_isInContact, align 4
  %tobool = trunc i8 %4 to i1
  br i1 %tobool, label %if.then, label %if.else17

if.then:                                          ; preds = %for.body
  %5 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4
  %call4 = call float @_ZNK11btWheelInfo23getSuspensionRestLengthEv(%struct.btWheelInfo* %5)
  store float %call4, float* %susp_length, align 4
  %6 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4
  %m_raycastInfo5 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %6, i32 0, i32 0
  %m_suspensionLength = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo5, i32 0, i32 2
  %7 = load float, float* %m_suspensionLength, align 4
  store float %7, float* %current_length, align 4
  %8 = load float, float* %susp_length, align 4
  %9 = load float, float* %current_length, align 4
  %sub = fsub float %8, %9
  store float %sub, float* %length_diff, align 4
  %10 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4
  %m_suspensionStiffness = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %10, i32 0, i32 8
  %11 = load float, float* %m_suspensionStiffness, align 4
  %12 = load float, float* %length_diff, align 4
  %mul = fmul float %11, %12
  %13 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4
  %m_clippedInvContactDotSuspension = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %13, i32 0, i32 21
  %14 = load float, float* %m_clippedInvContactDotSuspension, align 4
  %mul6 = fmul float %mul, %14
  store float %mul6, float* %force, align 4
  %15 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4
  %m_suspensionRelativeVelocity = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %15, i32 0, i32 22
  %16 = load float, float* %m_suspensionRelativeVelocity, align 4
  store float %16, float* %projected_rel_vel, align 4
  %17 = load float, float* %projected_rel_vel, align 4
  %cmp7 = fcmp olt float %17, 0.000000e+00
  br i1 %cmp7, label %if.then8, label %if.else

if.then8:                                         ; preds = %if.then
  %18 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4
  %m_wheelsDampingCompression = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %18, i32 0, i32 9
  %19 = load float, float* %m_wheelsDampingCompression, align 4
  store float %19, float* %susp_damping, align 4
  br label %if.end

if.else:                                          ; preds = %if.then
  %20 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4
  %m_wheelsDampingRelaxation = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %20, i32 0, i32 10
  %21 = load float, float* %m_wheelsDampingRelaxation, align 4
  store float %21, float* %susp_damping, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then8
  %22 = load float, float* %susp_damping, align 4
  %23 = load float, float* %projected_rel_vel, align 4
  %mul9 = fmul float %22, %23
  %24 = load float, float* %force, align 4
  %sub10 = fsub float %24, %mul9
  store float %sub10, float* %force, align 4
  %25 = load float, float* %force, align 4
  %26 = load float, float* %chassisMass, align 4
  %mul11 = fmul float %25, %26
  %27 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4
  %m_wheelsSuspensionForce = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %27, i32 0, i32 23
  store float %mul11, float* %m_wheelsSuspensionForce, align 4
  %28 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4
  %m_wheelsSuspensionForce12 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %28, i32 0, i32 23
  %29 = load float, float* %m_wheelsSuspensionForce12, align 4
  %cmp13 = fcmp olt float %29, 0.000000e+00
  br i1 %cmp13, label %if.then14, label %if.end16

if.then14:                                        ; preds = %if.end
  %30 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4
  %m_wheelsSuspensionForce15 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %30, i32 0, i32 23
  store float 0.000000e+00, float* %m_wheelsSuspensionForce15, align 4
  br label %if.end16

if.end16:                                         ; preds = %if.then14, %if.end
  br label %if.end19

if.else17:                                        ; preds = %for.body
  %31 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4
  %m_wheelsSuspensionForce18 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %31, i32 0, i32 23
  store float 0.000000e+00, float* %m_wheelsSuspensionForce18, align 4
  br label %if.end19

if.end19:                                         ; preds = %if.else17, %if.end16
  br label %for.inc

for.inc:                                          ; preds = %if.end19
  %32 = load i32, i32* %w_it, align 4
  %inc = add nsw i32 %32, 1
  store i32 %inc, i32* %w_it, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btRigidBody12applyImpulseERK9btVector3S2_(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %impulse.addr = alloca %class.btVector3*, align 4
  %rel_pos.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %impulse, %class.btVector3** %impulse.addr, align 4
  store %class.btVector3* %rel_pos, %class.btVector3** %rel_pos.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4
  %cmp = fcmp une float %0, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load %class.btVector3*, %class.btVector3** %impulse.addr, align 4
  call void @_ZN11btRigidBody19applyCentralImpulseERK9btVector3(%class.btRigidBody* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_angularFactor)
  %tobool = icmp ne float* %call, null
  br i1 %tobool, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %2 = load %class.btVector3*, %class.btVector3** %rel_pos.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %impulse.addr, align 4
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  call void @_ZN11btRigidBody18applyTorqueImpulseERK9btVector3(%class.btRigidBody* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btRaycastVehicle16setSteeringValueEfi(%class.btRaycastVehicle* %this, float %steering, i32 %wheel) #2 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %steering.addr = alloca float, align 4
  %wheel.addr = alloca i32, align 4
  %wheelInfo = alloca %struct.btWheelInfo*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  store float %steering, float* %steering.addr, align 4
  store i32 %wheel, i32* %wheel.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = load i32, i32* %wheel.addr, align 4
  %call = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %0)
  store %struct.btWheelInfo* %call, %struct.btWheelInfo** %wheelInfo, align 4
  %1 = load float, float* %steering.addr, align 4
  %2 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo, align 4
  %m_steering = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %2, i32 0, i32 12
  store float %1, float* %m_steering, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this, i32 %index) #2 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %index.addr = alloca i32, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo, i32 %0)
  ret %struct.btWheelInfo* %call
}

; Function Attrs: noinline optnone
define hidden float @_ZNK16btRaycastVehicle16getSteeringValueEi(%class.btRaycastVehicle* %this, i32 %wheel) #2 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %wheel.addr = alloca i32, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  store i32 %wheel, i32* %wheel.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = load i32, i32* %wheel.addr, align 4
  %call = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZNK16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %0)
  %m_steering = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call, i32 0, i32 12
  %1 = load float, float* %m_steering, align 4
  ret float %1
}

; Function Attrs: noinline optnone
define hidden nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZNK16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this, i32 %index) #2 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %index.addr = alloca i32, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZNK20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo, i32 %0)
  ret %struct.btWheelInfo* %call
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btRaycastVehicle16applyEngineForceEfi(%class.btRaycastVehicle* %this, float %force, i32 %wheel) #2 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %force.addr = alloca float, align 4
  %wheel.addr = alloca i32, align 4
  %wheelInfo = alloca %struct.btWheelInfo*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  store float %force, float* %force.addr, align 4
  store i32 %wheel, i32* %wheel.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = load i32, i32* %wheel.addr, align 4
  %call = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %0)
  store %struct.btWheelInfo* %call, %struct.btWheelInfo** %wheelInfo, align 4
  %1 = load float, float* %force.addr, align 4
  %2 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo, align 4
  %m_engineForce = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %2, i32 0, i32 17
  store float %1, float* %m_engineForce, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btRaycastVehicle8setBrakeEfi(%class.btRaycastVehicle* %this, float %brake, i32 %wheelIndex) #2 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %brake.addr = alloca float, align 4
  %wheelIndex.addr = alloca i32, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  store float %brake, float* %brake.addr, align 4
  store i32 %wheelIndex, i32* %wheelIndex.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = load float, float* %brake.addr, align 4
  %1 = load i32, i32* %wheelIndex.addr, align 4
  %call = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %1)
  %m_brake = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call, i32 0, i32 18
  store float %0, float* %m_brake, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4
  ret float %0
}

; Function Attrs: noinline optnone
define hidden float @_Z19calcRollingFrictionR19btWheelContactPoint(%struct.btWheelContactPoint* nonnull align 4 dereferenceable(48) %contactPoint) #2 {
entry:
  %contactPoint.addr = alloca %struct.btWheelContactPoint*, align 4
  %j1 = alloca float, align 4
  %contactPosWorld = alloca %class.btVector3*, align 4
  %rel_pos1 = alloca %class.btVector3, align 4
  %rel_pos2 = alloca %class.btVector3, align 4
  %maxImpulse = alloca float, align 4
  %vel1 = alloca %class.btVector3, align 4
  %vel2 = alloca %class.btVector3, align 4
  %vel = alloca %class.btVector3, align 4
  %vrel = alloca float, align 4
  %ref.tmp = alloca float, align 4
  store %struct.btWheelContactPoint* %contactPoint, %struct.btWheelContactPoint** %contactPoint.addr, align 4
  store float 0.000000e+00, float* %j1, align 4
  %0 = load %struct.btWheelContactPoint*, %struct.btWheelContactPoint** %contactPoint.addr, align 4
  %m_frictionPositionWorld = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %0, i32 0, i32 2
  store %class.btVector3* %m_frictionPositionWorld, %class.btVector3** %contactPosWorld, align 4
  %1 = load %class.btVector3*, %class.btVector3** %contactPosWorld, align 4
  %2 = load %struct.btWheelContactPoint*, %struct.btWheelContactPoint** %contactPoint.addr, align 4
  %m_body0 = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %2, i32 0, i32 0
  %3 = load %class.btRigidBody*, %class.btRigidBody** %m_body0, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %3)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  %4 = load %class.btVector3*, %class.btVector3** %contactPosWorld, align 4
  %5 = load %struct.btWheelContactPoint*, %struct.btWheelContactPoint** %contactPoint.addr, align 4
  %m_body1 = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %5, i32 0, i32 1
  %6 = load %class.btRigidBody*, %class.btRigidBody** %m_body1, align 4
  %call1 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %6)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %call1)
  %7 = load %struct.btWheelContactPoint*, %struct.btWheelContactPoint** %contactPoint.addr, align 4
  %m_maxImpulse = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %7, i32 0, i32 5
  %8 = load float, float* %m_maxImpulse, align 4
  store float %8, float* %maxImpulse, align 4
  %9 = load %struct.btWheelContactPoint*, %struct.btWheelContactPoint** %contactPoint.addr, align 4
  %m_body02 = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %9, i32 0, i32 0
  %10 = load %class.btRigidBody*, %class.btRigidBody** %m_body02, align 4
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %vel1, %class.btRigidBody* %10, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1)
  %11 = load %struct.btWheelContactPoint*, %struct.btWheelContactPoint** %contactPoint.addr, align 4
  %m_body13 = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %11, i32 0, i32 1
  %12 = load %class.btRigidBody*, %class.btRigidBody** %m_body13, align 4
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %vel2, %class.btRigidBody* %12, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %vel, %class.btVector3* nonnull align 4 dereferenceable(16) %vel1, %class.btVector3* nonnull align 4 dereferenceable(16) %vel2)
  %13 = load %struct.btWheelContactPoint*, %struct.btWheelContactPoint** %contactPoint.addr, align 4
  %m_frictionDirectionWorld = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %13, i32 0, i32 3
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_frictionDirectionWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %vel)
  store float %call4, float* %vrel, align 4
  %14 = load float, float* %vrel, align 4
  %fneg = fneg float %14
  %15 = load %struct.btWheelContactPoint*, %struct.btWheelContactPoint** %contactPoint.addr, align 4
  %m_jacDiagABInv = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %15, i32 0, i32 4
  %16 = load float, float* %m_jacDiagABInv, align 4
  %mul = fmul float %fneg, %16
  store float %mul, float* %j1, align 4
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %j1, float* nonnull align 4 dereferenceable(4) %maxImpulse)
  %17 = load float, float* %maxImpulse, align 4
  %fneg5 = fneg float %17
  store float %fneg5, float* %ref.tmp, align 4
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %j1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %18 = load float, float* %j1, align 4
  ret float %18
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %b.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %a.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = load float*, float** %a.addr, align 4
  store float %5, float* %6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %b.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = load float*, float** %a.addr, align 4
  store float %5, float* %6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btRaycastVehicle14updateFrictionEf(%class.btRaycastVehicle* %this, float %timeStep) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %timeStep.addr = alloca float, align 4
  %numWheel = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %numWheelsOnGround = alloca i32, align 4
  %i = alloca i32, align 4
  %wheelInfo = alloca %struct.btWheelInfo*, align 4
  %groundObject = alloca %class.btRigidBody*, align 4
  %i17 = alloca i32, align 4
  %wheelInfo22 = alloca %struct.btWheelInfo*, align 4
  %groundObject25 = alloca %class.btRigidBody*, align 4
  %wheelTrans = alloca %class.btTransform*, align 4
  %wheelBasis0 = alloca %class.btMatrix3x3, align 4
  %ref.tmp33 = alloca %class.btVector3, align 4
  %surfNormalWS = alloca %class.btVector3*, align 4
  %proj = alloca float, align 4
  %ref.tmp51 = alloca %class.btVector3, align 4
  %ref.tmp60 = alloca %class.btVector3, align 4
  %sideFactor = alloca float, align 4
  %fwdFactor = alloca float, align 4
  %sliding = alloca i8, align 1
  %wheel = alloca i32, align 4
  %wheelInfo85 = alloca %struct.btWheelInfo*, align 4
  %groundObject88 = alloca %class.btRigidBody*, align 4
  %rollingFriction = alloca float, align 4
  %defaultRollingFrictionImpulse = alloca float, align 4
  %maxImpulse = alloca float, align 4
  %contactPt = alloca %struct.btWheelContactPoint, align 4
  %maximp = alloca float, align 4
  %maximpSide = alloca float, align 4
  %maximpSquared = alloca float, align 4
  %x = alloca float, align 4
  %y = alloca float, align 4
  %impulseSquared = alloca float, align 4
  %factor = alloca float, align 4
  %wheel144 = alloca i32, align 4
  %wheel176 = alloca i32, align 4
  %wheelInfo181 = alloca %struct.btWheelInfo*, align 4
  %rel_pos = alloca %class.btVector3, align 4
  %ref.tmp193 = alloca %class.btVector3, align 4
  %groundObject203 = alloca %class.btRigidBody*, align 4
  %rel_pos2 = alloca %class.btVector3, align 4
  %sideImp = alloca %class.btVector3, align 4
  %vChassisWorldUp = alloca %class.btVector3, align 4
  %ref.tmp218 = alloca %class.btVector3, align 4
  %ref.tmp219 = alloca float, align 4
  %ref.tmp224 = alloca %class.btVector3, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %call = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  store i32 %call, i32* %numWheel, align 4
  %0 = load i32, i32* %numWheel, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %for.end228

if.end:                                           ; preds = %entry
  %m_forwardWS = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 1
  %1 = load i32, i32* %numWheel, align 4
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.5* %m_forwardWS, i32 %1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %m_axle = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %2 = load i32, i32* %numWheel, align 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp3)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.5* %m_axle, i32 %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %m_forwardImpulse = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %3 = load i32, i32* %numWheel, align 4
  store float 0.000000e+00, float* %ref.tmp5, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.9* %m_forwardImpulse, i32 %3, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %m_sideImpulse = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %4 = load i32, i32* %numWheel, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.9* %m_sideImpulse, i32 %4, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  store i32 0, i32* %numWheelsOnGround, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %5 = load i32, i32* %i, align 4
  %call7 = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %cmp = icmp slt i32 %5, %call7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %6 = load i32, i32* %i, align 4
  %call8 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo, i32 %6)
  store %struct.btWheelInfo* %call8, %struct.btWheelInfo** %wheelInfo, align 4
  %7 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo, align 4
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %7, i32 0, i32 0
  %m_groundObject = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo, i32 0, i32 7
  %8 = load i8*, i8** %m_groundObject, align 4
  %9 = bitcast i8* %8 to %class.btRigidBody*
  store %class.btRigidBody* %9, %class.btRigidBody** %groundObject, align 4
  %10 = load %class.btRigidBody*, %class.btRigidBody** %groundObject, align 4
  %tobool9 = icmp ne %class.btRigidBody* %10, null
  br i1 %tobool9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %for.body
  %11 = load i32, i32* %numWheelsOnGround, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %numWheelsOnGround, align 4
  br label %if.end11

if.end11:                                         ; preds = %if.then10, %for.body
  %m_sideImpulse12 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %12 = load i32, i32* %i, align 4
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.9* %m_sideImpulse12, i32 %12)
  store float 0.000000e+00, float* %call13, align 4
  %m_forwardImpulse14 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %13 = load i32, i32* %i, align 4
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.9* %m_forwardImpulse14, i32 %13)
  store float 0.000000e+00, float* %call15, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end11
  %14 = load i32, i32* %i, align 4
  %inc16 = add nsw i32 %14, 1
  store i32 %inc16, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i17, align 4
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc78, %for.end
  %15 = load i32, i32* %i17, align 4
  %call19 = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %cmp20 = icmp slt i32 %15, %call19
  br i1 %cmp20, label %for.body21, label %for.end80

for.body21:                                       ; preds = %for.cond18
  %m_wheelInfo23 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %16 = load i32, i32* %i17, align 4
  %call24 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo23, i32 %16)
  store %struct.btWheelInfo* %call24, %struct.btWheelInfo** %wheelInfo22, align 4
  %17 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo22, align 4
  %m_raycastInfo26 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %17, i32 0, i32 0
  %m_groundObject27 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo26, i32 0, i32 7
  %18 = load i8*, i8** %m_groundObject27, align 4
  %19 = bitcast i8* %18 to %class.btRigidBody*
  store %class.btRigidBody* %19, %class.btRigidBody** %groundObject25, align 4
  %20 = load %class.btRigidBody*, %class.btRigidBody** %groundObject25, align 4
  %tobool28 = icmp ne %class.btRigidBody* %20, null
  br i1 %tobool28, label %if.then29, label %if.end77

if.then29:                                        ; preds = %for.body21
  %21 = load i32, i32* %i17, align 4
  %call30 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK16btRaycastVehicle19getWheelTransformWSEi(%class.btRaycastVehicle* %this1, i32 %21)
  store %class.btTransform* %call30, %class.btTransform** %wheelTrans, align 4
  %22 = load %class.btTransform*, %class.btTransform** %wheelTrans, align 4
  %call31 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %22)
  %call32 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %wheelBasis0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call31)
  %call34 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %wheelBasis0, i32 0)
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call34)
  %m_indexRightAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 12
  %23 = load i32, i32* %m_indexRightAxis, align 4
  %arrayidx = getelementptr inbounds float, float* %call35, i32 %23
  %call36 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %wheelBasis0, i32 1)
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call36)
  %m_indexRightAxis38 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 12
  %24 = load i32, i32* %m_indexRightAxis38, align 4
  %arrayidx39 = getelementptr inbounds float, float* %call37, i32 %24
  %call40 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %wheelBasis0, i32 2)
  %call41 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call40)
  %m_indexRightAxis42 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 12
  %25 = load i32, i32* %m_indexRightAxis42, align 4
  %arrayidx43 = getelementptr inbounds float, float* %call41, i32 %25
  %call44 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp33, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx39, float* nonnull align 4 dereferenceable(4) %arrayidx43)
  %m_axle45 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %26 = load i32, i32* %i17, align 4
  %call46 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.5* %m_axle45, i32 %26)
  %27 = bitcast %class.btVector3* %call46 to i8*
  %28 = bitcast %class.btVector3* %ref.tmp33 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 4 %28, i32 16, i1 false)
  %29 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo22, align 4
  %m_raycastInfo47 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %29, i32 0, i32 0
  %m_contactNormalWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo47, i32 0, i32 0
  store %class.btVector3* %m_contactNormalWS, %class.btVector3** %surfNormalWS, align 4
  %m_axle48 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %30 = load i32, i32* %i17, align 4
  %call49 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.5* %m_axle48, i32 %30)
  %31 = load %class.btVector3*, %class.btVector3** %surfNormalWS, align 4
  %call50 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call49, %class.btVector3* nonnull align 4 dereferenceable(16) %31)
  store float %call50, float* %proj, align 4
  %32 = load %class.btVector3*, %class.btVector3** %surfNormalWS, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp51, %class.btVector3* nonnull align 4 dereferenceable(16) %32, float* nonnull align 4 dereferenceable(4) %proj)
  %m_axle52 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %33 = load i32, i32* %i17, align 4
  %call53 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.5* %m_axle52, i32 %33)
  %call54 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %call53, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp51)
  %m_axle55 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %34 = load i32, i32* %i17, align 4
  %call56 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.5* %m_axle55, i32 %34)
  %call57 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %call56)
  %m_axle58 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %35 = load i32, i32* %i17, align 4
  %call59 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.5* %m_axle58, i32 %35)
  %36 = bitcast %class.btVector3* %call59 to i8*
  %37 = bitcast %class.btVector3* %call57 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %36, i8* align 4 %37, i32 16, i1 false)
  %38 = load %class.btVector3*, %class.btVector3** %surfNormalWS, align 4
  %m_axle61 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %39 = load i32, i32* %i17, align 4
  %call62 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.5* %m_axle61, i32 %39)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp60, %class.btVector3* %38, %class.btVector3* nonnull align 4 dereferenceable(16) %call62)
  %m_forwardWS63 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 1
  %40 = load i32, i32* %i17, align 4
  %call64 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.5* %m_forwardWS63, i32 %40)
  %41 = bitcast %class.btVector3* %call64 to i8*
  %42 = bitcast %class.btVector3* %ref.tmp60 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %41, i8* align 4 %42, i32 16, i1 false)
  %m_forwardWS65 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 1
  %43 = load i32, i32* %i17, align 4
  %call66 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.5* %m_forwardWS65, i32 %43)
  %call67 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %call66)
  %m_chassisBody = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 11
  %44 = load %class.btRigidBody*, %class.btRigidBody** %m_chassisBody, align 4
  %45 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo22, align 4
  %m_raycastInfo68 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %45, i32 0, i32 0
  %m_contactPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo68, i32 0, i32 1
  %46 = load %class.btRigidBody*, %class.btRigidBody** %groundObject25, align 4
  %47 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo22, align 4
  %m_raycastInfo69 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %47, i32 0, i32 0
  %m_contactPointWS70 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo69, i32 0, i32 1
  %m_axle71 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %48 = load i32, i32* %i17, align 4
  %call72 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.5* %m_axle71, i32 %48)
  %m_sideImpulse73 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %49 = load i32, i32* %i17, align 4
  %call74 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.9* %m_sideImpulse73, i32 %49)
  %50 = load float, float* %timeStep.addr, align 4
  call void @_Z22resolveSingleBilateralR11btRigidBodyRK9btVector3S0_S3_fS3_Rff(%class.btRigidBody* nonnull align 4 dereferenceable(676) %44, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactPointWS, %class.btRigidBody* nonnull align 4 dereferenceable(676) %46, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactPointWS70, float 0.000000e+00, %class.btVector3* nonnull align 4 dereferenceable(16) %call72, float* nonnull align 4 dereferenceable(4) %call74, float %50)
  %51 = load float, float* @sideFrictionStiffness2, align 4
  %m_sideImpulse75 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %52 = load i32, i32* %i17, align 4
  %call76 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.9* %m_sideImpulse75, i32 %52)
  %53 = load float, float* %call76, align 4
  %mul = fmul float %53, %51
  store float %mul, float* %call76, align 4
  br label %if.end77

if.end77:                                         ; preds = %if.then29, %for.body21
  br label %for.inc78

for.inc78:                                        ; preds = %if.end77
  %54 = load i32, i32* %i17, align 4
  %inc79 = add nsw i32 %54, 1
  store i32 %inc79, i32* %i17, align 4
  br label %for.cond18

for.end80:                                        ; preds = %for.cond18
  store float 1.000000e+00, float* %sideFactor, align 4
  store float 5.000000e-01, float* %fwdFactor, align 4
  store i8 0, i8* %sliding, align 1
  store i32 0, i32* %wheel, align 4
  br label %for.cond81

for.cond81:                                       ; preds = %for.inc139, %for.end80
  %55 = load i32, i32* %wheel, align 4
  %call82 = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %cmp83 = icmp slt i32 %55, %call82
  br i1 %cmp83, label %for.body84, label %for.end141

for.body84:                                       ; preds = %for.cond81
  %m_wheelInfo86 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %56 = load i32, i32* %wheel, align 4
  %call87 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo86, i32 %56)
  store %struct.btWheelInfo* %call87, %struct.btWheelInfo** %wheelInfo85, align 4
  %57 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo85, align 4
  %m_raycastInfo89 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %57, i32 0, i32 0
  %m_groundObject90 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo89, i32 0, i32 7
  %58 = load i8*, i8** %m_groundObject90, align 4
  %59 = bitcast i8* %58 to %class.btRigidBody*
  store %class.btRigidBody* %59, %class.btRigidBody** %groundObject88, align 4
  store float 0.000000e+00, float* %rollingFriction, align 4
  %60 = load %class.btRigidBody*, %class.btRigidBody** %groundObject88, align 4
  %tobool91 = icmp ne %class.btRigidBody* %60, null
  br i1 %tobool91, label %if.then92, label %if.end107

if.then92:                                        ; preds = %for.body84
  %61 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo85, align 4
  %m_engineForce = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %61, i32 0, i32 17
  %62 = load float, float* %m_engineForce, align 4
  %cmp93 = fcmp une float %62, 0.000000e+00
  br i1 %cmp93, label %if.then94, label %if.else

if.then94:                                        ; preds = %if.then92
  %63 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo85, align 4
  %m_engineForce95 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %63, i32 0, i32 17
  %64 = load float, float* %m_engineForce95, align 4
  %65 = load float, float* %timeStep.addr, align 4
  %mul96 = fmul float %64, %65
  store float %mul96, float* %rollingFriction, align 4
  br label %if.end106

if.else:                                          ; preds = %if.then92
  store float 0.000000e+00, float* %defaultRollingFrictionImpulse, align 4
  %66 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo85, align 4
  %m_brake = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %66, i32 0, i32 18
  %67 = load float, float* %m_brake, align 4
  %tobool97 = fcmp une float %67, 0.000000e+00
  br i1 %tobool97, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %68 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo85, align 4
  %m_brake98 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %68, i32 0, i32 18
  %69 = load float, float* %m_brake98, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %70 = load float, float* %defaultRollingFrictionImpulse, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %69, %cond.true ], [ %70, %cond.false ]
  store float %cond, float* %maxImpulse, align 4
  %m_chassisBody99 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 11
  %71 = load %class.btRigidBody*, %class.btRigidBody** %m_chassisBody99, align 4
  %72 = load %class.btRigidBody*, %class.btRigidBody** %groundObject88, align 4
  %73 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo85, align 4
  %m_raycastInfo100 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %73, i32 0, i32 0
  %m_contactPointWS101 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo100, i32 0, i32 1
  %m_forwardWS102 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 1
  %74 = load i32, i32* %wheel, align 4
  %call103 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.5* %m_forwardWS102, i32 %74)
  %75 = load float, float* %maxImpulse, align 4
  %call104 = call %struct.btWheelContactPoint* @_ZN19btWheelContactPointC2EP11btRigidBodyS1_RK9btVector3S4_f(%struct.btWheelContactPoint* %contactPt, %class.btRigidBody* %71, %class.btRigidBody* %72, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactPointWS101, %class.btVector3* nonnull align 4 dereferenceable(16) %call103, float %75)
  %call105 = call float @_Z19calcRollingFrictionR19btWheelContactPoint(%struct.btWheelContactPoint* nonnull align 4 dereferenceable(48) %contactPt)
  store float %call105, float* %rollingFriction, align 4
  br label %if.end106

if.end106:                                        ; preds = %cond.end, %if.then94
  br label %if.end107

if.end107:                                        ; preds = %if.end106, %for.body84
  %m_forwardImpulse108 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %76 = load i32, i32* %wheel, align 4
  %call109 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.9* %m_forwardImpulse108, i32 %76)
  store float 0.000000e+00, float* %call109, align 4
  %m_wheelInfo110 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %77 = load i32, i32* %wheel, align 4
  %call111 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo110, i32 %77)
  %m_skidInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call111, i32 0, i32 24
  store float 1.000000e+00, float* %m_skidInfo, align 4
  %78 = load %class.btRigidBody*, %class.btRigidBody** %groundObject88, align 4
  %tobool112 = icmp ne %class.btRigidBody* %78, null
  br i1 %tobool112, label %if.then113, label %if.end138

if.then113:                                       ; preds = %if.end107
  %m_wheelInfo114 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %79 = load i32, i32* %wheel, align 4
  %call115 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo114, i32 %79)
  %m_skidInfo116 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call115, i32 0, i32 24
  store float 1.000000e+00, float* %m_skidInfo116, align 4
  %80 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo85, align 4
  %m_wheelsSuspensionForce = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %80, i32 0, i32 23
  %81 = load float, float* %m_wheelsSuspensionForce, align 4
  %82 = load float, float* %timeStep.addr, align 4
  %mul117 = fmul float %81, %82
  %83 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo85, align 4
  %m_frictionSlip = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %83, i32 0, i32 11
  %84 = load float, float* %m_frictionSlip, align 4
  %mul118 = fmul float %mul117, %84
  store float %mul118, float* %maximp, align 4
  %85 = load float, float* %maximp, align 4
  store float %85, float* %maximpSide, align 4
  %86 = load float, float* %maximp, align 4
  %87 = load float, float* %maximpSide, align 4
  %mul119 = fmul float %86, %87
  store float %mul119, float* %maximpSquared, align 4
  %88 = load float, float* %rollingFriction, align 4
  %m_forwardImpulse120 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %89 = load i32, i32* %wheel, align 4
  %call121 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.9* %m_forwardImpulse120, i32 %89)
  store float %88, float* %call121, align 4
  %m_forwardImpulse122 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %90 = load i32, i32* %wheel, align 4
  %call123 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.9* %m_forwardImpulse122, i32 %90)
  %91 = load float, float* %call123, align 4
  %92 = load float, float* %fwdFactor, align 4
  %mul124 = fmul float %91, %92
  store float %mul124, float* %x, align 4
  %m_sideImpulse125 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %93 = load i32, i32* %wheel, align 4
  %call126 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.9* %m_sideImpulse125, i32 %93)
  %94 = load float, float* %call126, align 4
  %95 = load float, float* %sideFactor, align 4
  %mul127 = fmul float %94, %95
  store float %mul127, float* %y, align 4
  %96 = load float, float* %x, align 4
  %97 = load float, float* %x, align 4
  %mul128 = fmul float %96, %97
  %98 = load float, float* %y, align 4
  %99 = load float, float* %y, align 4
  %mul129 = fmul float %98, %99
  %add = fadd float %mul128, %mul129
  store float %add, float* %impulseSquared, align 4
  %100 = load float, float* %impulseSquared, align 4
  %101 = load float, float* %maximpSquared, align 4
  %cmp130 = fcmp ogt float %100, %101
  br i1 %cmp130, label %if.then131, label %if.end137

if.then131:                                       ; preds = %if.then113
  store i8 1, i8* %sliding, align 1
  %102 = load float, float* %maximp, align 4
  %103 = load float, float* %impulseSquared, align 4
  %call132 = call float @_Z6btSqrtf(float %103)
  %div = fdiv float %102, %call132
  store float %div, float* %factor, align 4
  %104 = load float, float* %factor, align 4
  %m_wheelInfo133 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %105 = load i32, i32* %wheel, align 4
  %call134 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo133, i32 %105)
  %m_skidInfo135 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call134, i32 0, i32 24
  %106 = load float, float* %m_skidInfo135, align 4
  %mul136 = fmul float %106, %104
  store float %mul136, float* %m_skidInfo135, align 4
  br label %if.end137

if.end137:                                        ; preds = %if.then131, %if.then113
  br label %if.end138

if.end138:                                        ; preds = %if.end137, %if.end107
  br label %for.inc139

for.inc139:                                       ; preds = %if.end138
  %107 = load i32, i32* %wheel, align 4
  %inc140 = add nsw i32 %107, 1
  store i32 %inc140, i32* %wheel, align 4
  br label %for.cond81

for.end141:                                       ; preds = %for.cond81
  %108 = load i8, i8* %sliding, align 1
  %tobool142 = trunc i8 %108 to i1
  br i1 %tobool142, label %if.then143, label %if.end175

if.then143:                                       ; preds = %for.end141
  store i32 0, i32* %wheel144, align 4
  br label %for.cond145

for.cond145:                                      ; preds = %for.inc172, %if.then143
  %109 = load i32, i32* %wheel144, align 4
  %call146 = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %cmp147 = icmp slt i32 %109, %call146
  br i1 %cmp147, label %for.body148, label %for.end174

for.body148:                                      ; preds = %for.cond145
  %m_sideImpulse149 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %110 = load i32, i32* %wheel144, align 4
  %call150 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.9* %m_sideImpulse149, i32 %110)
  %111 = load float, float* %call150, align 4
  %cmp151 = fcmp une float %111, 0.000000e+00
  br i1 %cmp151, label %if.then152, label %if.end171

if.then152:                                       ; preds = %for.body148
  %m_wheelInfo153 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %112 = load i32, i32* %wheel144, align 4
  %call154 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo153, i32 %112)
  %m_skidInfo155 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call154, i32 0, i32 24
  %113 = load float, float* %m_skidInfo155, align 4
  %cmp156 = fcmp olt float %113, 1.000000e+00
  br i1 %cmp156, label %if.then157, label %if.end170

if.then157:                                       ; preds = %if.then152
  %m_wheelInfo158 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %114 = load i32, i32* %wheel144, align 4
  %call159 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo158, i32 %114)
  %m_skidInfo160 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call159, i32 0, i32 24
  %115 = load float, float* %m_skidInfo160, align 4
  %m_forwardImpulse161 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %116 = load i32, i32* %wheel144, align 4
  %call162 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.9* %m_forwardImpulse161, i32 %116)
  %117 = load float, float* %call162, align 4
  %mul163 = fmul float %117, %115
  store float %mul163, float* %call162, align 4
  %m_wheelInfo164 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %118 = load i32, i32* %wheel144, align 4
  %call165 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo164, i32 %118)
  %m_skidInfo166 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call165, i32 0, i32 24
  %119 = load float, float* %m_skidInfo166, align 4
  %m_sideImpulse167 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %120 = load i32, i32* %wheel144, align 4
  %call168 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.9* %m_sideImpulse167, i32 %120)
  %121 = load float, float* %call168, align 4
  %mul169 = fmul float %121, %119
  store float %mul169, float* %call168, align 4
  br label %if.end170

if.end170:                                        ; preds = %if.then157, %if.then152
  br label %if.end171

if.end171:                                        ; preds = %if.end170, %for.body148
  br label %for.inc172

for.inc172:                                       ; preds = %if.end171
  %122 = load i32, i32* %wheel144, align 4
  %inc173 = add nsw i32 %122, 1
  store i32 %inc173, i32* %wheel144, align 4
  br label %for.cond145

for.end174:                                       ; preds = %for.cond145
  br label %if.end175

if.end175:                                        ; preds = %for.end174, %for.end141
  store i32 0, i32* %wheel176, align 4
  br label %for.cond177

for.cond177:                                      ; preds = %for.inc226, %if.end175
  %123 = load i32, i32* %wheel176, align 4
  %call178 = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %cmp179 = icmp slt i32 %123, %call178
  br i1 %cmp179, label %for.body180, label %for.end228

for.body180:                                      ; preds = %for.cond177
  %m_wheelInfo182 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %124 = load i32, i32* %wheel176, align 4
  %call183 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo182, i32 %124)
  store %struct.btWheelInfo* %call183, %struct.btWheelInfo** %wheelInfo181, align 4
  %125 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo181, align 4
  %m_raycastInfo184 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %125, i32 0, i32 0
  %m_contactPointWS185 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo184, i32 0, i32 1
  %m_chassisBody186 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 11
  %126 = load %class.btRigidBody*, %class.btRigidBody** %m_chassisBody186, align 4
  %call187 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %126)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rel_pos, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactPointWS185, %class.btVector3* nonnull align 4 dereferenceable(16) %call187)
  %m_forwardImpulse188 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %127 = load i32, i32* %wheel176, align 4
  %call189 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.9* %m_forwardImpulse188, i32 %127)
  %128 = load float, float* %call189, align 4
  %cmp190 = fcmp une float %128, 0.000000e+00
  br i1 %cmp190, label %if.then191, label %if.end198

if.then191:                                       ; preds = %for.body180
  %m_chassisBody192 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 11
  %129 = load %class.btRigidBody*, %class.btRigidBody** %m_chassisBody192, align 4
  %m_forwardWS194 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 1
  %130 = load i32, i32* %wheel176, align 4
  %call195 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.5* %m_forwardWS194, i32 %130)
  %m_forwardImpulse196 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %131 = load i32, i32* %wheel176, align 4
  %call197 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.9* %m_forwardImpulse196, i32 %131)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp193, %class.btVector3* nonnull align 4 dereferenceable(16) %call195, float* nonnull align 4 dereferenceable(4) %call197)
  call void @_ZN11btRigidBody12applyImpulseERK9btVector3S2_(%class.btRigidBody* %129, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp193, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos)
  br label %if.end198

if.end198:                                        ; preds = %if.then191, %for.body180
  %m_sideImpulse199 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %132 = load i32, i32* %wheel176, align 4
  %call200 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.9* %m_sideImpulse199, i32 %132)
  %133 = load float, float* %call200, align 4
  %cmp201 = fcmp une float %133, 0.000000e+00
  br i1 %cmp201, label %if.then202, label %if.end225

if.then202:                                       ; preds = %if.end198
  %m_wheelInfo204 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %134 = load i32, i32* %wheel176, align 4
  %call205 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.13* %m_wheelInfo204, i32 %134)
  %m_raycastInfo206 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call205, i32 0, i32 0
  %m_groundObject207 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo206, i32 0, i32 7
  %135 = load i8*, i8** %m_groundObject207, align 4
  %136 = bitcast i8* %135 to %class.btRigidBody*
  store %class.btRigidBody* %136, %class.btRigidBody** %groundObject203, align 4
  %137 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo181, align 4
  %m_raycastInfo208 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %137, i32 0, i32 0
  %m_contactPointWS209 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo208, i32 0, i32 1
  %138 = load %class.btRigidBody*, %class.btRigidBody** %groundObject203, align 4
  %call210 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %138)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactPointWS209, %class.btVector3* nonnull align 4 dereferenceable(16) %call210)
  %m_axle211 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %139 = load i32, i32* %wheel176, align 4
  %call212 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.5* %m_axle211, i32 %139)
  %m_sideImpulse213 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %140 = load i32, i32* %wheel176, align 4
  %call214 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.9* %m_sideImpulse213, i32 %140)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %sideImp, %class.btVector3* nonnull align 4 dereferenceable(16) %call212, float* nonnull align 4 dereferenceable(4) %call214)
  %call215 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  %call216 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call215)
  %call217 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call216)
  %m_indexUpAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 13
  %141 = load i32, i32* %m_indexUpAxis, align 4
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %vChassisWorldUp, %class.btMatrix3x3* %call217, i32 %141)
  %call220 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %vChassisWorldUp, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos)
  %142 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo181, align 4
  %m_rollInfluence = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %142, i32 0, i32 15
  %143 = load float, float* %m_rollInfluence, align 4
  %sub = fsub float 1.000000e+00, %143
  %mul221 = fmul float %call220, %sub
  store float %mul221, float* %ref.tmp219, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp218, %class.btVector3* nonnull align 4 dereferenceable(16) %vChassisWorldUp, float* nonnull align 4 dereferenceable(4) %ref.tmp219)
  %call222 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %rel_pos, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp218)
  %m_chassisBody223 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 11
  %144 = load %class.btRigidBody*, %class.btRigidBody** %m_chassisBody223, align 4
  call void @_ZN11btRigidBody12applyImpulseERK9btVector3S2_(%class.btRigidBody* %144, %class.btVector3* nonnull align 4 dereferenceable(16) %sideImp, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos)
  %145 = load %class.btRigidBody*, %class.btRigidBody** %groundObject203, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp224, %class.btVector3* nonnull align 4 dereferenceable(16) %sideImp)
  call void @_ZN11btRigidBody12applyImpulseERK9btVector3S2_(%class.btRigidBody* %145, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp224, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2)
  br label %if.end225

if.end225:                                        ; preds = %if.then202, %if.end198
  br label %for.inc226

for.inc226:                                       ; preds = %if.end225
  %146 = load i32, i32* %wheel176, align 4
  %inc227 = add nsw i32 %146, 1
  store i32 %inc227, i32* %wheel176, align 4
  br label %for.cond177

for.end228:                                       ; preds = %if.then, %for.cond177
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.5* %this, i32 %newsize, %class.btVector3* nonnull align 4 dereferenceable(16) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btVector3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btVector3* %fillData, %class.btVector3** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.5* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %5 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end15

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.5* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %14 = load %class.btVector3*, %class.btVector3** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btVector3, %class.btVector3* %14, i32 %15
  %16 = bitcast %class.btVector3* %arrayidx10 to i8*
  %call11 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %16)
  %17 = bitcast i8* %call11 to %class.btVector3*
  %18 = load %class.btVector3*, %class.btVector3** %fillData.addr, align 4
  %19 = bitcast %class.btVector3* %17 to i8*
  %20 = bitcast %class.btVector3* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc13 = add nsw i32 %21, 1
  store i32 %inc13, i32* %i5, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  br label %if.end15

if.end15:                                         ; preds = %for.end14, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.9* %this, i32 %newsize, float* nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca float*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store float* %fillData, float** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %5 = load float*, float** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.9* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %14 = load float*, float** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds float, float* %14, i32 %15
  %16 = bitcast float* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to float*
  %18 = load float*, float** %fillData.addr, align 4
  %19 = load float, float* %18, align 4
  store float %19, float* %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.9* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.5* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

declare void @_Z22resolveSingleBilateralR11btRigidBodyRK9btVector3S0_S3_fS3_Rff(%class.btRigidBody* nonnull align 4 dereferenceable(676), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btRigidBody* nonnull align 4 dereferenceable(676), %class.btVector3* nonnull align 4 dereferenceable(16), float, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btWheelContactPoint* @_ZN19btWheelContactPointC2EP11btRigidBodyS1_RK9btVector3S4_f(%struct.btWheelContactPoint* returned %this, %class.btRigidBody* %body0, %class.btRigidBody* %body1, %class.btVector3* nonnull align 4 dereferenceable(16) %frictionPosWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %frictionDirectionWorld, float %maxImpulse) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btWheelContactPoint*, align 4
  %body0.addr = alloca %class.btRigidBody*, align 4
  %body1.addr = alloca %class.btRigidBody*, align 4
  %frictionPosWorld.addr = alloca %class.btVector3*, align 4
  %frictionDirectionWorld.addr = alloca %class.btVector3*, align 4
  %maxImpulse.addr = alloca float, align 4
  %denom0 = alloca float, align 4
  %denom1 = alloca float, align 4
  %relaxation = alloca float, align 4
  store %struct.btWheelContactPoint* %this, %struct.btWheelContactPoint** %this.addr, align 4
  store %class.btRigidBody* %body0, %class.btRigidBody** %body0.addr, align 4
  store %class.btRigidBody* %body1, %class.btRigidBody** %body1.addr, align 4
  store %class.btVector3* %frictionPosWorld, %class.btVector3** %frictionPosWorld.addr, align 4
  store %class.btVector3* %frictionDirectionWorld, %class.btVector3** %frictionDirectionWorld.addr, align 4
  store float %maxImpulse, float* %maxImpulse.addr, align 4
  %this1 = load %struct.btWheelContactPoint*, %struct.btWheelContactPoint** %this.addr, align 4
  %m_body0 = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %this1, i32 0, i32 0
  %0 = load %class.btRigidBody*, %class.btRigidBody** %body0.addr, align 4
  store %class.btRigidBody* %0, %class.btRigidBody** %m_body0, align 4
  %m_body1 = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %this1, i32 0, i32 1
  %1 = load %class.btRigidBody*, %class.btRigidBody** %body1.addr, align 4
  store %class.btRigidBody* %1, %class.btRigidBody** %m_body1, align 4
  %m_frictionPositionWorld = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %this1, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %frictionPosWorld.addr, align 4
  %3 = bitcast %class.btVector3* %m_frictionPositionWorld to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %m_frictionDirectionWorld = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %this1, i32 0, i32 3
  %5 = load %class.btVector3*, %class.btVector3** %frictionDirectionWorld.addr, align 4
  %6 = bitcast %class.btVector3* %m_frictionDirectionWorld to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_maxImpulse = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %this1, i32 0, i32 5
  %8 = load float, float* %maxImpulse.addr, align 4
  store float %8, float* %m_maxImpulse, align 4
  %9 = load %class.btRigidBody*, %class.btRigidBody** %body0.addr, align 4
  %10 = load %class.btVector3*, %class.btVector3** %frictionPosWorld.addr, align 4
  %11 = load %class.btVector3*, %class.btVector3** %frictionDirectionWorld.addr, align 4
  %call = call float @_ZNK11btRigidBody25computeImpulseDenominatorERK9btVector3S2_(%class.btRigidBody* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %10, %class.btVector3* nonnull align 4 dereferenceable(16) %11)
  store float %call, float* %denom0, align 4
  %12 = load %class.btRigidBody*, %class.btRigidBody** %body1.addr, align 4
  %13 = load %class.btVector3*, %class.btVector3** %frictionPosWorld.addr, align 4
  %14 = load %class.btVector3*, %class.btVector3** %frictionDirectionWorld.addr, align 4
  %call2 = call float @_ZNK11btRigidBody25computeImpulseDenominatorERK9btVector3S2_(%class.btRigidBody* %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %14)
  store float %call2, float* %denom1, align 4
  store float 1.000000e+00, float* %relaxation, align 4
  %15 = load float, float* %relaxation, align 4
  %16 = load float, float* %denom0, align 4
  %17 = load float, float* %denom1, align 4
  %add = fadd float %16, %17
  %div = fdiv float %15, %add
  %m_jacDiagABInv = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %this1, i32 0, i32 4
  store float %div, float* %m_jacDiagABInv, align 4
  ret %struct.btWheelContactPoint* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %1 = load i32, i32* %i.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 2
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %2 = load i32, i32* %i.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %2
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %arrayidx2, float* nonnull align 4 dereferenceable(4) %arrayidx6, float* nonnull align 4 dereferenceable(4) %arrayidx10)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btRaycastVehicle9debugDrawEP12btIDebugDraw(%class.btRaycastVehicle* %this, %class.btIDebugDraw* %debugDrawer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %v = alloca i32, align 4
  %wheelColor = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %wheelPosWS = alloca %class.btVector3, align 4
  %axle = alloca %class.btVector3, align 4
  %ref.tmp35 = alloca %class.btVector3, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  store i32 0, i32* %v, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %v, align 4
  %call = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  store float 1.000000e+00, float* %ref.tmp3, align 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %wheelColor, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %v, align 4
  %call5 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %1)
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call5, i32 0, i32 0
  %m_isInContact = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo, i32 0, i32 6
  %2 = load i8, i8* %m_isInContact, align 4
  %tobool = trunc i8 %2 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 1.000000e+00, float* %ref.tmp8, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %wheelColor, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  br label %if.end

if.else:                                          ; preds = %for.body
  store float 1.000000e+00, float* %ref.tmp9, align 4
  store float 0.000000e+00, float* %ref.tmp10, align 4
  store float 1.000000e+00, float* %ref.tmp11, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %wheelColor, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %3 = load i32, i32* %v, align 4
  %call12 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %3)
  %m_worldTransform = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call12, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_worldTransform)
  %4 = bitcast %class.btVector3* %wheelPosWS to i8*
  %5 = bitcast %class.btVector3* %call13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load i32, i32* %v, align 4
  %call14 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %6)
  %m_worldTransform15 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call14, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_worldTransform15)
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %call16, i32 0)
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call17)
  %call19 = call i32 @_ZNK16btRaycastVehicle12getRightAxisEv(%class.btRaycastVehicle* %this1)
  %arrayidx = getelementptr inbounds float, float* %call18, i32 %call19
  %7 = load i32, i32* %v, align 4
  %call20 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %7)
  %m_worldTransform21 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_worldTransform21)
  %call23 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %call22, i32 1)
  %call24 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call23)
  %call25 = call i32 @_ZNK16btRaycastVehicle12getRightAxisEv(%class.btRaycastVehicle* %this1)
  %arrayidx26 = getelementptr inbounds float, float* %call24, i32 %call25
  %8 = load i32, i32* %v, align 4
  %call27 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %8)
  %m_worldTransform28 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call27, i32 0, i32 1
  %call29 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_worldTransform28)
  %call30 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %call29, i32 2)
  %call31 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call30)
  %call32 = call i32 @_ZNK16btRaycastVehicle12getRightAxisEv(%class.btRaycastVehicle* %this1)
  %arrayidx33 = getelementptr inbounds float, float* %call31, i32 %call32
  %call34 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %axle, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx26, float* nonnull align 4 dereferenceable(4) %arrayidx33)
  %9 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp35, %class.btVector3* nonnull align 4 dereferenceable(16) %wheelPosWS, %class.btVector3* nonnull align 4 dereferenceable(16) %axle)
  %10 = bitcast %class.btIDebugDraw* %9 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %10, align 4
  %vfn = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable, i64 4
  %11 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %11(%class.btIDebugDraw* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %wheelPosWS, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp35, %class.btVector3* nonnull align 4 dereferenceable(16) %wheelColor)
  %12 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %13 = load i32, i32* %v, align 4
  %call36 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %13)
  %m_raycastInfo37 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call36, i32 0, i32 0
  %m_contactPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo37, i32 0, i32 1
  %14 = bitcast %class.btIDebugDraw* %12 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable38 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %14, align 4
  %vfn39 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable38, i64 4
  %15 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn39, align 4
  call void %15(%class.btIDebugDraw* %12, %class.btVector3* nonnull align 4 dereferenceable(16) %wheelPosWS, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactPointWS, %class.btVector3* nonnull align 4 dereferenceable(16) %wheelColor)
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %16 = load i32, i32* %v, align 4
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %v, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK16btRaycastVehicle12getRightAxisEv(%class.btRaycastVehicle* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %m_indexRightAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_indexRightAxis, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define hidden i8* @_ZN25btDefaultVehicleRaycaster7castRayERK9btVector3S2_RN18btVehicleRaycaster24btVehicleRaycasterResultE(%class.btDefaultVehicleRaycaster* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %from, %class.btVector3* nonnull align 4 dereferenceable(16) %to, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* nonnull align 4 dereferenceable(36) %result) unnamed_addr #2 {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btDefaultVehicleRaycaster*, align 4
  %from.addr = alloca %class.btVector3*, align 4
  %to.addr = alloca %class.btVector3*, align 4
  %result.addr = alloca %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*, align 4
  %rayCallback = alloca %"struct.btCollisionWorld::ClosestRayResultCallback", align 4
  %body = alloca %class.btRigidBody*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btDefaultVehicleRaycaster* %this, %class.btDefaultVehicleRaycaster** %this.addr, align 4
  store %class.btVector3* %from, %class.btVector3** %from.addr, align 4
  store %class.btVector3* %to, %class.btVector3** %to.addr, align 4
  store %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %result, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"** %result.addr, align 4
  %this1 = load %class.btDefaultVehicleRaycaster*, %class.btDefaultVehicleRaycaster** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %from.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %to.addr, align 4
  %call = call %"struct.btCollisionWorld::ClosestRayResultCallback"* @_ZN16btCollisionWorld24ClosestRayResultCallbackC2ERK9btVector3S3_(%"struct.btCollisionWorld::ClosestRayResultCallback"* %rayCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %m_dynamicsWorld = getelementptr inbounds %class.btDefaultVehicleRaycaster, %class.btDefaultVehicleRaycaster* %this1, i32 0, i32 1
  %2 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %m_dynamicsWorld, align 4
  %3 = bitcast %class.btDynamicsWorld* %2 to %class.btCollisionWorld*
  %4 = load %class.btVector3*, %class.btVector3** %from.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %to.addr, align 4
  %6 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %rayCallback to %"struct.btCollisionWorld::RayResultCallback"*
  %7 = bitcast %class.btCollisionWorld* %3 to void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)***
  %vtable = load void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)**, void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)*** %7, align 4
  %vfn = getelementptr inbounds void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)*, void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)** %vtable, i64 8
  %8 = load void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)*, void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)** %vfn, align 4
  call void %8(%class.btCollisionWorld* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(24) %6)
  %9 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %rayCallback to %"struct.btCollisionWorld::RayResultCallback"*
  %call2 = call zeroext i1 @_ZNK16btCollisionWorld17RayResultCallback6hasHitEv(%"struct.btCollisionWorld::RayResultCallback"* %9)
  br i1 %call2, label %if.then, label %if.end8

if.then:                                          ; preds = %entry
  %10 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %rayCallback to %"struct.btCollisionWorld::RayResultCallback"*
  %m_collisionObject = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %10, i32 0, i32 2
  %11 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4
  %call3 = call %class.btRigidBody* @_ZN11btRigidBody6upcastEPK17btCollisionObject(%class.btCollisionObject* %11)
  store %class.btRigidBody* %call3, %class.btRigidBody** %body, align 4
  %12 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %tobool = icmp ne %class.btRigidBody* %12, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %13 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %14 = bitcast %class.btRigidBody* %13 to %class.btCollisionObject*
  %call4 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %14)
  br i1 %call4, label %if.then5, label %if.end

if.then5:                                         ; preds = %land.lhs.true
  %m_hitPointWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %rayCallback, i32 0, i32 4
  %15 = load %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"** %result.addr, align 4
  %m_hitPointInWorld = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %15, i32 0, i32 0
  %16 = bitcast %class.btVector3* %m_hitPointInWorld to i8*
  %17 = bitcast %class.btVector3* %m_hitPointWorld to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false)
  %m_hitNormalWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %rayCallback, i32 0, i32 3
  %18 = load %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"** %result.addr, align 4
  %m_hitNormalInWorld = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %18, i32 0, i32 1
  %19 = bitcast %class.btVector3* %m_hitNormalInWorld to i8*
  %20 = bitcast %class.btVector3* %m_hitNormalWorld to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  %21 = load %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"** %result.addr, align 4
  %m_hitNormalInWorld6 = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %21, i32 0, i32 1
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %m_hitNormalInWorld6)
  %22 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %rayCallback to %"struct.btCollisionWorld::RayResultCallback"*
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %22, i32 0, i32 1
  %23 = load float, float* %m_closestHitFraction, align 4
  %24 = load %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"** %result.addr, align 4
  %m_distFraction = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %24, i32 0, i32 2
  store float %23, float* %m_distFraction, align 4
  %25 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %26 = bitcast %class.btRigidBody* %25 to i8*
  store i8* %26, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %land.lhs.true, %if.then
  br label %if.end8

if.end8:                                          ; preds = %if.end, %entry
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end8, %if.then5
  %call9 = call %"struct.btCollisionWorld::ClosestRayResultCallback"* @_ZN16btCollisionWorld24ClosestRayResultCallbackD2Ev(%"struct.btCollisionWorld::ClosestRayResultCallback"* %rayCallback) #3
  %27 = load i8*, i8** %retval, align 4
  ret i8* %27
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btCollisionWorld::ClosestRayResultCallback"* @_ZN16btCollisionWorld24ClosestRayResultCallbackC2ERK9btVector3S3_(%"struct.btCollisionWorld::ClosestRayResultCallback"* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFromWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %rayToWorld) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ClosestRayResultCallback"*, align 4
  %rayFromWorld.addr = alloca %class.btVector3*, align 4
  %rayToWorld.addr = alloca %class.btVector3*, align 4
  store %"struct.btCollisionWorld::ClosestRayResultCallback"* %this, %"struct.btCollisionWorld::ClosestRayResultCallback"** %this.addr, align 4
  store %class.btVector3* %rayFromWorld, %class.btVector3** %rayFromWorld.addr, align 4
  store %class.btVector3* %rayToWorld, %class.btVector3** %rayToWorld.addr, align 4
  %this1 = load %"struct.btCollisionWorld::ClosestRayResultCallback"*, %"struct.btCollisionWorld::ClosestRayResultCallback"** %this.addr, align 4
  %0 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1 to %"struct.btCollisionWorld::RayResultCallback"*
  %call = call %"struct.btCollisionWorld::RayResultCallback"* @_ZN16btCollisionWorld17RayResultCallbackC2Ev(%"struct.btCollisionWorld::RayResultCallback"* %0)
  %1 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTVN16btCollisionWorld24ClosestRayResultCallbackE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_rayFromWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1, i32 0, i32 1
  %2 = load %class.btVector3*, %class.btVector3** %rayFromWorld.addr, align 4
  %3 = bitcast %class.btVector3* %m_rayFromWorld to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %m_rayToWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1, i32 0, i32 2
  %5 = load %class.btVector3*, %class.btVector3** %rayToWorld.addr, align 4
  %6 = bitcast %class.btVector3* %m_rayToWorld to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_hitNormalWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1, i32 0, i32 3
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hitNormalWorld)
  %m_hitPointWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1, i32 0, i32 4
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hitPointWorld)
  ret %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionWorld17RayResultCallback6hasHitEv(%"struct.btCollisionWorld::RayResultCallback"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::RayResultCallback"*, align 4
  store %"struct.btCollisionWorld::RayResultCallback"* %this, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4
  %this1 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4
  %m_collisionObject = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4
  %cmp = icmp ne %class.btCollisionObject* %0, null
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btRigidBody* @_ZN11btRigidBody6upcastEPK17btCollisionObject(%class.btCollisionObject* %colObj) #2 comdat {
entry:
  %retval = alloca %class.btRigidBody*, align 4
  %colObj.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %colObj, %class.btCollisionObject** %colObj.addr, align 4
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4
  %call = call i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %0)
  %and = and i32 %call, 2
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4
  %2 = bitcast %class.btCollisionObject* %1 to %class.btRigidBody*
  store %class.btRigidBody* %2, %class.btRigidBody** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store %class.btRigidBody* null, %class.btRigidBody** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load %class.btRigidBody*, %class.btRigidBody** %retval, align 4
  ret %class.btRigidBody* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4
  %and = and i32 %0, 4
  %cmp = icmp eq i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btCollisionWorld::ClosestRayResultCallback"* @_ZN16btCollisionWorld24ClosestRayResultCallbackD2Ev(%"struct.btCollisionWorld::ClosestRayResultCallback"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ClosestRayResultCallback"*, align 4
  store %"struct.btCollisionWorld::ClosestRayResultCallback"* %this, %"struct.btCollisionWorld::ClosestRayResultCallback"** %this.addr, align 4
  %this1 = load %"struct.btCollisionWorld::ClosestRayResultCallback"*, %"struct.btCollisionWorld::ClosestRayResultCallback"** %this.addr, align 4
  %0 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1 to %"struct.btCollisionWorld::RayResultCallback"*
  %call = call %"struct.btCollisionWorld::RayResultCallback"* @_ZN16btCollisionWorld17RayResultCallbackD2Ev(%"struct.btCollisionWorld::RayResultCallback"* %0) #3
  ret %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btDefaultVehicleRaycaster* @_ZN25btDefaultVehicleRaycasterD2Ev(%class.btDefaultVehicleRaycaster* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDefaultVehicleRaycaster*, align 4
  store %class.btDefaultVehicleRaycaster* %this, %class.btDefaultVehicleRaycaster** %this.addr, align 4
  %this1 = load %class.btDefaultVehicleRaycaster*, %class.btDefaultVehicleRaycaster** %this.addr, align 4
  %0 = bitcast %class.btDefaultVehicleRaycaster* %this1 to %struct.btVehicleRaycaster*
  %call = call %struct.btVehicleRaycaster* @_ZN18btVehicleRaycasterD2Ev(%struct.btVehicleRaycaster* %0) #3
  ret %class.btDefaultVehicleRaycaster* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN25btDefaultVehicleRaycasterD0Ev(%class.btDefaultVehicleRaycaster* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDefaultVehicleRaycaster*, align 4
  store %class.btDefaultVehicleRaycaster* %this, %class.btDefaultVehicleRaycaster** %this.addr, align 4
  %this1 = load %class.btDefaultVehicleRaycaster*, %class.btDefaultVehicleRaycaster** %this.addr, align 4
  %call = call %class.btDefaultVehicleRaycaster* @_ZN25btDefaultVehicleRaycasterD2Ev(%class.btDefaultVehicleRaycaster* %this1) #3
  %0 = bitcast %class.btDefaultVehicleRaycaster* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN16btRaycastVehicle12updateActionEP16btCollisionWorldf(%class.btRaycastVehicle* %this, %class.btCollisionWorld* %collisionWorld, float %step) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %step.addr = alloca float, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4
  store float %step, float* %step.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = load float, float* %step.addr, align 4
  %1 = bitcast %class.btRaycastVehicle* %this1 to void (%class.btRaycastVehicle*, float)***
  %vtable = load void (%class.btRaycastVehicle*, float)**, void (%class.btRaycastVehicle*, float)*** %1, align 4
  %vfn = getelementptr inbounds void (%class.btRaycastVehicle*, float)*, void (%class.btRaycastVehicle*, float)** %vtable, i64 4
  %2 = load void (%class.btRaycastVehicle*, float)*, void (%class.btRaycastVehicle*, float)** %vfn, align 4
  call void %2(%class.btRaycastVehicle* %this1, float %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btRaycastVehicle19setCoordinateSystemEiii(%class.btRaycastVehicle* %this, i32 %rightIndex, i32 %upIndex, i32 %forwardIndex) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %rightIndex.addr = alloca i32, align 4
  %upIndex.addr = alloca i32, align 4
  %forwardIndex.addr = alloca i32, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4
  store i32 %rightIndex, i32* %rightIndex.addr, align 4
  store i32 %upIndex, i32* %upIndex.addr, align 4
  store i32 %forwardIndex, i32* %forwardIndex.addr, align 4
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = load i32, i32* %rightIndex.addr, align 4
  %m_indexRightAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 12
  store i32 %0, i32* %m_indexRightAxis, align 4
  %1 = load i32, i32* %upIndex.addr, align 4
  %m_indexUpAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 13
  store i32 %1, i32* %m_indexUpAxis, align 4
  %2 = load i32, i32* %forwardIndex.addr, align 4
  %m_indexForwardAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 14
  store i32 %2, i32* %m_indexForwardAxis, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: nounwind
declare %class.btCollisionObject* @_ZN17btCollisionObjectD2Ev(%class.btCollisionObject* returned) unnamed_addr #7

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %tobool = icmp ne %class.btTypedConstraint** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.1* %m_allocator, %class.btTypedConstraint** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.1* %this, %class.btTypedConstraint** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %class.btTypedConstraint**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store %class.btTypedConstraint** %ptr, %class.btTypedConstraint*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %ptr.addr, align 4
  %1 = bitcast %class.btTypedConstraint** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btActionInterface* @_ZN17btActionInterfaceD2Ev(%class.btActionInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btActionInterface*, align 4
  store %class.btActionInterface* %this, %class.btActionInterface** %this.addr, align 4
  %this1 = load %class.btActionInterface*, %class.btActionInterface** %this.addr, align 4
  ret %class.btActionInterface* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btActionInterfaceD0Ev(%class.btActionInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btActionInterface*, align 4
  store %class.btActionInterface* %this, %class.btActionInterface** %this.addr, align 4
  %this1 = load %class.btActionInterface*, %class.btActionInterface** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #8

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btWheelInfo::RaycastInfo"* @_ZN11btWheelInfo11RaycastInfoC2Ev(%"struct.btWheelInfo::RaycastInfo"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btWheelInfo::RaycastInfo"*, align 4
  store %"struct.btWheelInfo::RaycastInfo"* %this, %"struct.btWheelInfo::RaycastInfo"** %this.addr, align 4
  %this1 = load %"struct.btWheelInfo::RaycastInfo"*, %"struct.btWheelInfo::RaycastInfo"** %this.addr, align 4
  %m_contactNormalWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_contactNormalWS)
  %m_contactPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_contactPointWS)
  %m_hardPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %this1, i32 0, i32 3
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hardPointWS)
  %m_wheelDirectionWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %this1, i32 0, i32 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_wheelDirectionWS)
  %m_wheelAxleWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %this1, i32 0, i32 5
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_wheelAxleWS)
  ret %"struct.btWheelInfo::RaycastInfo"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %_angle) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4
  store float* %_angle, float** %_angle.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %0)
  store float %call, float* %d, align 4
  %1 = load float*, float** %_angle.addr, align 4
  %2 = load float, float* %1, align 4
  %mul = fmul float %2, 5.000000e-01
  %call2 = call float @_Z5btSinf(float %mul)
  %3 = load float, float* %d, align 4
  %div = fdiv float %call2, %3
  store float %div, float* %s, align 4
  %4 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %5 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %5)
  %6 = load float, float* %call3, align 4
  %7 = load float, float* %s, align 4
  %mul4 = fmul float %6, %7
  store float %mul4, float* %ref.tmp, align 4
  %8 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %8)
  %9 = load float, float* %call6, align 4
  %10 = load float, float* %s, align 4
  %mul7 = fmul float %9, %10
  store float %mul7, float* %ref.tmp5, align 4
  %11 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %11)
  %12 = load float, float* %call9, align 4
  %13 = load float, float* %s, align 4
  %mul10 = fmul float %12, %13
  store float %mul10, float* %ref.tmp8, align 4
  %14 = load float*, float** %_angle.addr, align 4
  %15 = load float, float* %14, align 4
  %mul12 = fmul float %15, 5.000000e-01
  %call13 = call float @_Z5btCosf(float %mul12)
  store float %call13, float* %ref.tmp11, align 4
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %4, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btSinf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.sin.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btCosf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sin.f32(float) #9

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #9

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %0)
  store float %call, float* %d, align 4
  %1 = load float, float* %d, align 4
  %div = fdiv float 2.000000e+00, %1
  store float %div, float* %s, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call2, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %4, %5
  store float %mul, float* %xs, align 4
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load float, float* %call3, align 4
  %9 = load float, float* %s, align 4
  %mul4 = fmul float %8, %9
  store float %mul4, float* %ys, align 4
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call5, align 4
  %13 = load float, float* %s, align 4
  %mul6 = fmul float %12, %13
  store float %mul6, float* %zs, align 4
  %14 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %15 = bitcast %class.btQuaternion* %14 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %15)
  %16 = load float, float* %call7, align 4
  %17 = load float, float* %xs, align 4
  %mul8 = fmul float %16, %17
  store float %mul8, float* %wx, align 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load float, float* %ys, align 4
  %mul10 = fmul float %20, %21
  store float %mul10, float* %wy, align 4
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %23)
  %24 = load float, float* %call11, align 4
  %25 = load float, float* %zs, align 4
  %mul12 = fmul float %24, %25
  store float %mul12, float* %wz, align 4
  %26 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %27 = bitcast %class.btQuaternion* %26 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %27)
  %28 = load float, float* %call13, align 4
  %29 = load float, float* %xs, align 4
  %mul14 = fmul float %28, %29
  store float %mul14, float* %xx, align 4
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %31)
  %32 = load float, float* %call15, align 4
  %33 = load float, float* %ys, align 4
  %mul16 = fmul float %32, %33
  store float %mul16, float* %xy, align 4
  %34 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %35 = bitcast %class.btQuaternion* %34 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %35)
  %36 = load float, float* %call17, align 4
  %37 = load float, float* %zs, align 4
  %mul18 = fmul float %36, %37
  store float %mul18, float* %xz, align 4
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %39)
  %40 = load float, float* %call19, align 4
  %41 = load float, float* %ys, align 4
  %mul20 = fmul float %40, %41
  store float %mul20, float* %yy, align 4
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %43)
  %44 = load float, float* %call21, align 4
  %45 = load float, float* %zs, align 4
  %mul22 = fmul float %44, %45
  store float %mul22, float* %yz, align 4
  %46 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %47 = bitcast %class.btQuaternion* %46 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %47)
  %48 = load float, float* %call23, align 4
  %49 = load float, float* %zs, align 4
  %mul24 = fmul float %48, %49
  store float %mul24, float* %zz, align 4
  %50 = load float, float* %yy, align 4
  %51 = load float, float* %zz, align 4
  %add = fadd float %50, %51
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4
  %52 = load float, float* %xy, align 4
  %53 = load float, float* %wz, align 4
  %sub26 = fsub float %52, %53
  store float %sub26, float* %ref.tmp25, align 4
  %54 = load float, float* %xz, align 4
  %55 = load float, float* %wy, align 4
  %add28 = fadd float %54, %55
  store float %add28, float* %ref.tmp27, align 4
  %56 = load float, float* %xy, align 4
  %57 = load float, float* %wz, align 4
  %add30 = fadd float %56, %57
  store float %add30, float* %ref.tmp29, align 4
  %58 = load float, float* %xx, align 4
  %59 = load float, float* %zz, align 4
  %add32 = fadd float %58, %59
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4
  %60 = load float, float* %yz, align 4
  %61 = load float, float* %wx, align 4
  %sub35 = fsub float %60, %61
  store float %sub35, float* %ref.tmp34, align 4
  %62 = load float, float* %xz, align 4
  %63 = load float, float* %wy, align 4
  %sub37 = fsub float %62, %63
  store float %sub37, float* %ref.tmp36, align 4
  %64 = load float, float* %yz, align 4
  %65 = load float, float* %wx, align 4
  %add39 = fadd float %64, %65
  store float %add39, float* %ref.tmp38, align 4
  %66 = load float, float* %xx, align 4
  %67 = load float, float* %yy, align 4
  %add41 = fadd float %66, %67
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btRigidBody19applyCentralImpulseERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %impulse.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %impulse, %class.btVector3** %impulse.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %impulse.addr, align 4
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %m_inverseMass)
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btRigidBody18applyTorqueImpulseERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %torque) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %torque.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %torque, %class.btVector3** %torque.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  %0 = load %class.btVector3*, %class.btVector3** %torque.addr, align 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_invInertiaTensorWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularFactor)
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btRigidBody25computeImpulseDenominatorERK9btVector3S2_(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %pos, %class.btVector3* nonnull align 4 dereferenceable(16) %normal) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %pos.addr = alloca %class.btVector3*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %r0 = alloca %class.btVector3, align 4
  %c0 = alloca %class.btVector3, align 4
  %vec = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %pos, %class.btVector3** %pos.addr, align 4
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %pos.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %this1)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %r0, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  %1 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %c0, %class.btVector3* %r0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %call2 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %this1)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %c0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call2)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %vec, %class.btVector3* %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %r0)
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %2 = load float, float* %m_inverseMass, align 4
  %3 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %add = fadd float %2, %call3
  ret float %add
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call2, float* %ref.tmp1, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp3, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  ret %class.btMatrix3x3* %m_invInertiaTensorWorld
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #9

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btCollisionWorld::RayResultCallback"* @_ZN16btCollisionWorld17RayResultCallbackC2Ev(%"struct.btCollisionWorld::RayResultCallback"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::RayResultCallback"*, align 4
  store %"struct.btCollisionWorld::RayResultCallback"* %this, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4
  %this1 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4
  %0 = bitcast %"struct.btCollisionWorld::RayResultCallback"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTVN16btCollisionWorld17RayResultCallbackE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %this1, i32 0, i32 1
  store float 1.000000e+00, float* %m_closestHitFraction, align 4
  %m_collisionObject = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %this1, i32 0, i32 2
  store %class.btCollisionObject* null, %class.btCollisionObject** %m_collisionObject, align 4
  %m_collisionFilterGroup = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %this1, i32 0, i32 3
  store i32 1, i32* %m_collisionFilterGroup, align 4
  %m_collisionFilterMask = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %this1, i32 0, i32 4
  store i32 -1, i32* %m_collisionFilterMask, align 4
  %m_flags = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %this1, i32 0, i32 5
  store i32 0, i32* %m_flags, align 4
  ret %"struct.btCollisionWorld::RayResultCallback"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btCollisionWorld24ClosestRayResultCallbackD0Ev(%"struct.btCollisionWorld::ClosestRayResultCallback"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ClosestRayResultCallback"*, align 4
  store %"struct.btCollisionWorld::ClosestRayResultCallback"* %this, %"struct.btCollisionWorld::ClosestRayResultCallback"** %this.addr, align 4
  %this1 = load %"struct.btCollisionWorld::ClosestRayResultCallback"*, %"struct.btCollisionWorld::ClosestRayResultCallback"** %this.addr, align 4
  %call = call %"struct.btCollisionWorld::ClosestRayResultCallback"* @_ZN16btCollisionWorld24ClosestRayResultCallbackD2Ev(%"struct.btCollisionWorld::ClosestRayResultCallback"* %this1) #3
  %0 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionWorld17RayResultCallback14needsCollisionEP17btBroadphaseProxy(%"struct.btCollisionWorld::RayResultCallback"* %this, %struct.btBroadphaseProxy* %proxy0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::RayResultCallback"*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %collides = alloca i8, align 1
  store %"struct.btCollisionWorld::RayResultCallback"* %this, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %this1 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %0, i32 0, i32 1
  %1 = load i32, i32* %m_collisionFilterGroup, align 4
  %m_collisionFilterMask = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %this1, i32 0, i32 4
  %2 = load i32, i32* %m_collisionFilterMask, align 4
  %and = and i32 %1, %2
  %cmp = icmp ne i32 %and, 0
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %collides, align 1
  %3 = load i8, i8* %collides, align 1
  %tobool = trunc i8 %3 to i1
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %m_collisionFilterGroup2 = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %this1, i32 0, i32 3
  %4 = load i32, i32* %m_collisionFilterGroup2, align 4
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %m_collisionFilterMask3 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %5, i32 0, i32 2
  %6 = load i32, i32* %m_collisionFilterMask3, align 4
  %and4 = and i32 %4, %6
  %tobool5 = icmp ne i32 %and4, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %7 = phi i1 [ false, %entry ], [ %tobool5, %land.rhs ]
  %frombool6 = zext i1 %7 to i8
  store i8 %frombool6, i8* %collides, align 1
  %8 = load i8, i8* %collides, align 1
  %tobool7 = trunc i8 %8 to i1
  ret i1 %tobool7
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZN16btCollisionWorld24ClosestRayResultCallback15addSingleResultERNS_14LocalRayResultEb(%"struct.btCollisionWorld::ClosestRayResultCallback"* %this, %"struct.btCollisionWorld::LocalRayResult"* nonnull align 4 dereferenceable(28) %rayResult, i1 zeroext %normalInWorldSpace) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ClosestRayResultCallback"*, align 4
  %rayResult.addr = alloca %"struct.btCollisionWorld::LocalRayResult"*, align 4
  %normalInWorldSpace.addr = alloca i8, align 1
  %ref.tmp = alloca %class.btVector3, align 4
  store %"struct.btCollisionWorld::ClosestRayResultCallback"* %this, %"struct.btCollisionWorld::ClosestRayResultCallback"** %this.addr, align 4
  store %"struct.btCollisionWorld::LocalRayResult"* %rayResult, %"struct.btCollisionWorld::LocalRayResult"** %rayResult.addr, align 4
  %frombool = zext i1 %normalInWorldSpace to i8
  store i8 %frombool, i8* %normalInWorldSpace.addr, align 1
  %this1 = load %"struct.btCollisionWorld::ClosestRayResultCallback"*, %"struct.btCollisionWorld::ClosestRayResultCallback"** %this.addr, align 4
  %0 = load %"struct.btCollisionWorld::LocalRayResult"*, %"struct.btCollisionWorld::LocalRayResult"** %rayResult.addr, align 4
  %m_hitFraction = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %0, i32 0, i32 3
  %1 = load float, float* %m_hitFraction, align 4
  %2 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1 to %"struct.btCollisionWorld::RayResultCallback"*
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %2, i32 0, i32 1
  store float %1, float* %m_closestHitFraction, align 4
  %3 = load %"struct.btCollisionWorld::LocalRayResult"*, %"struct.btCollisionWorld::LocalRayResult"** %rayResult.addr, align 4
  %m_collisionObject = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %3, i32 0, i32 0
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4
  %5 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1 to %"struct.btCollisionWorld::RayResultCallback"*
  %m_collisionObject2 = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %5, i32 0, i32 2
  store %class.btCollisionObject* %4, %class.btCollisionObject** %m_collisionObject2, align 4
  %6 = load i8, i8* %normalInWorldSpace.addr, align 1
  %tobool = trunc i8 %6 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %7 = load %"struct.btCollisionWorld::LocalRayResult"*, %"struct.btCollisionWorld::LocalRayResult"** %rayResult.addr, align 4
  %m_hitNormalLocal = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %7, i32 0, i32 2
  %m_hitNormalWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1, i32 0, i32 3
  %8 = bitcast %class.btVector3* %m_hitNormalWorld to i8*
  %9 = bitcast %class.btVector3* %m_hitNormalLocal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  br label %if.end

if.else:                                          ; preds = %entry
  %10 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1 to %"struct.btCollisionWorld::RayResultCallback"*
  %m_collisionObject3 = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %10, i32 0, i32 2
  %11 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject3, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %11)
  %call4 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call)
  %12 = load %"struct.btCollisionWorld::LocalRayResult"*, %"struct.btCollisionWorld::LocalRayResult"** %rayResult.addr, align 4
  %m_hitNormalLocal5 = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %12, i32 0, i32 2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_hitNormalLocal5)
  %m_hitNormalWorld6 = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1, i32 0, i32 3
  %13 = bitcast %class.btVector3* %m_hitNormalWorld6 to i8*
  %14 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %m_hitPointWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1, i32 0, i32 4
  %m_rayFromWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1, i32 0, i32 1
  %m_rayToWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1, i32 0, i32 2
  %15 = load %"struct.btCollisionWorld::LocalRayResult"*, %"struct.btCollisionWorld::LocalRayResult"** %rayResult.addr, align 4
  %m_hitFraction7 = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %15, i32 0, i32 3
  %16 = load float, float* %m_hitFraction7, align 4
  call void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %m_hitPointWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %m_rayFromWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %m_rayToWorld, float %16)
  %17 = load %"struct.btCollisionWorld::LocalRayResult"*, %"struct.btCollisionWorld::LocalRayResult"** %rayResult.addr, align 4
  %m_hitFraction8 = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %17, i32 0, i32 3
  %18 = load float, float* %m_hitFraction8, align 4
  ret float %18
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btCollisionWorld::RayResultCallback"* @_ZN16btCollisionWorld17RayResultCallbackD2Ev(%"struct.btCollisionWorld::RayResultCallback"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::RayResultCallback"*, align 4
  store %"struct.btCollisionWorld::RayResultCallback"* %this, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4
  %this1 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4
  ret %"struct.btCollisionWorld::RayResultCallback"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btCollisionWorld17RayResultCallbackD0Ev(%"struct.btCollisionWorld::RayResultCallback"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::RayResultCallback"*, align 4
  store %"struct.btCollisionWorld::RayResultCallback"* %this, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4
  %this1 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, float %rt) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %rt.addr = alloca float, align 4
  %s = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store float %rt, float* %rt.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %rt.addr, align 4
  %sub = fsub float 1.000000e+00, %0
  store float %sub, float* %s, align 4
  %1 = load float, float* %s, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %3 = load float, float* %arrayidx, align 4
  %mul = fmul float %1, %3
  %4 = load float, float* %rt.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %6 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %4, %6
  %add = fadd float %mul, %mul4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 0
  store float %add, float* %arrayidx6, align 4
  %7 = load float, float* %s, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %m_floats7 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %9 = load float, float* %arrayidx8, align 4
  %mul9 = fmul float %7, %9
  %10 = load float, float* %rt.addr, align 4
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 1
  %12 = load float, float* %arrayidx11, align 4
  %mul12 = fmul float %10, %12
  %add13 = fadd float %mul9, %mul12
  %m_floats14 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [4 x float], [4 x float]* %m_floats14, i32 0, i32 1
  store float %add13, float* %arrayidx15, align 4
  %13 = load float, float* %s, align 4
  %14 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %m_floats16 = getelementptr inbounds %class.btVector3, %class.btVector3* %14, i32 0, i32 0
  %arrayidx17 = getelementptr inbounds [4 x float], [4 x float]* %m_floats16, i32 0, i32 2
  %15 = load float, float* %arrayidx17, align 4
  %mul18 = fmul float %13, %15
  %16 = load float, float* %rt.addr, align 4
  %17 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats19 = getelementptr inbounds %class.btVector3, %class.btVector3* %17, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [4 x float], [4 x float]* %m_floats19, i32 0, i32 2
  %18 = load float, float* %arrayidx20, align 4
  %mul21 = fmul float %16, %18
  %add22 = fadd float %mul18, %mul21
  %m_floats23 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [4 x float], [4 x float]* %m_floats23, i32 0, i32 2
  store float %add22, float* %arrayidx24, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_internalType = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 24
  %0 = load i32, i32* %m_internalType, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btVehicleRaycaster* @_ZN18btVehicleRaycasterD2Ev(%struct.btVehicleRaycaster* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btVehicleRaycaster*, align 4
  store %struct.btVehicleRaycaster* %this, %struct.btVehicleRaycaster** %this.addr, align 4
  %this1 = load %struct.btVehicleRaycaster*, %struct.btVehicleRaycaster** %this.addr, align 4
  ret %struct.btVehicleRaycaster* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.6* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator.6* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.6*, align 4
  store %class.btAlignedAllocator.6* %this, %class.btAlignedAllocator.6** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.6*, %class.btAlignedAllocator.6** %this.addr, align 4
  ret %class.btAlignedAllocator.6* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.5* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.10* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.10* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  ret %class.btAlignedAllocator.10* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store float* null, float** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.5* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.5* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.5* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.5* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.5* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.5* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.5* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.5* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.6* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.6* %this, %class.btVector3* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.6*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator.6* %this, %class.btAlignedAllocator.6** %this.addr, align 4
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.6*, %class.btAlignedAllocator.6** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.9* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.9* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.9* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %3 = load float*, float** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.9* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %tobool = icmp ne float* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %2 = load float*, float** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.10* %m_allocator, float* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store float* null, float** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.10* %this, float* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  %ptr.addr = alloca float*, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4
  store float* %ptr, float** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  %0 = load float*, float** %ptr.addr, align 4
  %1 = bitcast float* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.14* @_ZN18btAlignedAllocatorI11btWheelInfoLj16EEC2Ev(%class.btAlignedAllocator.14* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.14*, align 4
  store %class.btAlignedAllocator.14* %this, %class.btAlignedAllocator.14** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.14*, %class.btAlignedAllocator.14** %this.addr, align 4
  ret %class.btAlignedAllocator.14* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btWheelInfoE4initEv(%class.btAlignedObjectArray.13* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  store %struct.btWheelInfo* null, %struct.btWheelInfo** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btWheelInfoE5clearEv(%class.btAlignedObjectArray.13* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  call void @_ZN20btAlignedObjectArrayI11btWheelInfoE7destroyEii(%class.btAlignedObjectArray.13* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI11btWheelInfoE10deallocateEv(%class.btAlignedObjectArray.13* %this1)
  call void @_ZN20btAlignedObjectArrayI11btWheelInfoE4initEv(%class.btAlignedObjectArray.13* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btWheelInfoE7destroyEii(%class.btAlignedObjectArray.13* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %3 = load %struct.btWheelInfo*, %struct.btWheelInfo** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btWheelInfoE10deallocateEv(%class.btAlignedObjectArray.13* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %0 = load %struct.btWheelInfo*, %struct.btWheelInfo** %m_data, align 4
  %tobool = icmp ne %struct.btWheelInfo* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %2 = load %struct.btWheelInfo*, %struct.btWheelInfo** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI11btWheelInfoLj16EE10deallocateEPS0_(%class.btAlignedAllocator.14* %m_allocator, %struct.btWheelInfo* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  store %struct.btWheelInfo* null, %struct.btWheelInfo** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI11btWheelInfoLj16EE10deallocateEPS0_(%class.btAlignedAllocator.14* %this, %struct.btWheelInfo* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.14*, align 4
  %ptr.addr = alloca %struct.btWheelInfo*, align 4
  store %class.btAlignedAllocator.14* %this, %class.btAlignedAllocator.14** %this.addr, align 4
  store %struct.btWheelInfo* %ptr, %struct.btWheelInfo** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.14*, %class.btAlignedAllocator.14** %this.addr, align 4
  %0 = load %struct.btWheelInfo*, %struct.btWheelInfo** %ptr.addr, align 4
  %1 = bitcast %struct.btWheelInfo* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE8capacityEv(%class.btAlignedObjectArray.13* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btWheelInfoE7reserveEi(%class.btAlignedObjectArray.13* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btWheelInfo*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE8capacityEv(%class.btAlignedObjectArray.13* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI11btWheelInfoE8allocateEi(%class.btAlignedObjectArray.13* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btWheelInfo*
  store %struct.btWheelInfo* %2, %struct.btWheelInfo** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  %3 = load %struct.btWheelInfo*, %struct.btWheelInfo** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI11btWheelInfoE4copyEiiPS0_(%class.btAlignedObjectArray.13* %this1, i32 0, i32 %call3, %struct.btWheelInfo* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  call void @_ZN20btAlignedObjectArrayI11btWheelInfoE7destroyEii(%class.btAlignedObjectArray.13* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI11btWheelInfoE10deallocateEv(%class.btAlignedObjectArray.13* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btWheelInfo*, %struct.btWheelInfo** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  store %struct.btWheelInfo* %4, %struct.btWheelInfo** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI11btWheelInfoE9allocSizeEi(%class.btAlignedObjectArray.13* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btWheelInfo* @_ZN11btWheelInfoC2ERKS_(%struct.btWheelInfo* returned %this, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %0) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btWheelInfo*, align 4
  %.addr = alloca %struct.btWheelInfo*, align 4
  store %struct.btWheelInfo* %this, %struct.btWheelInfo** %this.addr, align 4
  store %struct.btWheelInfo* %0, %struct.btWheelInfo** %.addr, align 4
  %this1 = load %struct.btWheelInfo*, %struct.btWheelInfo** %this.addr, align 4
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 0
  %1 = load %struct.btWheelInfo*, %struct.btWheelInfo** %.addr, align 4
  %m_raycastInfo2 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %1, i32 0, i32 0
  %2 = bitcast %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo to i8*
  %3 = bitcast %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 92, i1 false)
  %m_worldTransform = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 1
  %4 = load %struct.btWheelInfo*, %struct.btWheelInfo** %.addr, align 4
  %m_worldTransform3 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %4, i32 0, i32 1
  %call = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_worldTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform3)
  %m_chassisConnectionPointCS = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 2
  %5 = load %struct.btWheelInfo*, %struct.btWheelInfo** %.addr, align 4
  %m_chassisConnectionPointCS4 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %5, i32 0, i32 2
  %6 = bitcast %class.btVector3* %m_chassisConnectionPointCS to i8*
  %7 = bitcast %class.btVector3* %m_chassisConnectionPointCS4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 4 %6, i8* align 4 %7, i64 128, i1 false)
  ret %struct.btWheelInfo* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI11btWheelInfoE8allocateEi(%class.btAlignedObjectArray.13* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btWheelInfo* @_ZN18btAlignedAllocatorI11btWheelInfoLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.14* %m_allocator, i32 %1, %struct.btWheelInfo** null)
  %2 = bitcast %struct.btWheelInfo* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI11btWheelInfoE4copyEiiPS0_(%class.btAlignedObjectArray.13* %this, i32 %start, i32 %end, %struct.btWheelInfo* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btWheelInfo*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btWheelInfo* %dest, %struct.btWheelInfo** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btWheelInfo*, %struct.btWheelInfo** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %3, i32 %4
  %5 = bitcast %struct.btWheelInfo* %arrayidx to i8*
  %6 = bitcast i8* %5 to %struct.btWheelInfo*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %7 = load %struct.btWheelInfo*, %struct.btWheelInfo** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %7, i32 %8
  %call = call %struct.btWheelInfo* @_ZN11btWheelInfoC2ERKS_(%struct.btWheelInfo* %6, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btWheelInfo* @_ZN18btAlignedAllocatorI11btWheelInfoLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.14* %this, i32 %n, %struct.btWheelInfo** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.14*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btWheelInfo**, align 4
  store %class.btAlignedAllocator.14* %this, %class.btAlignedAllocator.14** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btWheelInfo** %hint, %struct.btWheelInfo*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.14*, %class.btAlignedAllocator.14** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 284, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btWheelInfo*
  ret %struct.btWheelInfo* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #4

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i64, i1 immarg) #6

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.5* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.5* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray.5* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %2, %class.btVector3** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.5* %this1)
  %3 = load %class.btVector3*, %class.btVector3** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray.5* %this1, i32 0, i32 %call3, %class.btVector3* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.5* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.5* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.5* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  store %class.btVector3* %4, %class.btVector3** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.5* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray.5* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.6* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray.5* %this, i32 %start, i32 %end, %class.btVector3* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  %5 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %7 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  %9 = bitcast %class.btVector3* %6 to i8*
  %10 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.6* %this, i32 %n, %class.btVector3** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.6*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator.6* %this, %class.btAlignedAllocator.6** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.6*, %class.btAlignedAllocator.6** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.9* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca float*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.9* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.9* %this1, i32 %1)
  %2 = bitcast i8* %call2 to float*
  store float* %2, float** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %3 = load float*, float** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call3, float* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.9* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load float*, float** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store float* %4, float** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.9* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.10* %m_allocator, i32 %1, float** null)
  %2 = bitcast float* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.9* %this, i32 %start, i32 %end, float* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca float*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store float* %dest, float** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load float*, float** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  %5 = bitcast float* %arrayidx to i8*
  %6 = bitcast i8* %5 to float*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %7 = load float*, float** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds float, float* %7, i32 %8
  %9 = load float, float* %arrayidx2, align 4
  store float %9, float* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.10* %this, i32 %n, float** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca float**, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store float** %hint, float*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to float*
  ret float* %1
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btRaycastVehicle.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind willreturn }
attributes #7 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { cold noreturn nounwind }
attributes #9 = { nounwind readnone speculatable willreturn }
attributes #10 = { builtin nounwind }
attributes #11 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!"branch_weights", i32 1, i32 1048575}
