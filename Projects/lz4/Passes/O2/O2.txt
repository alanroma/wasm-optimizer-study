[PassRunner] running passes...
[PassRunner]   running pass: duplicate-function-elimination... 0.00661465 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 4.1216e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0180752 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0266925 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00294286 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.011268 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00394851 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.01132 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0106891 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0496085 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0216982 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0067846 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0143119 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0356648 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.051112 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0181592 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00375884 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0356187 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00363194 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0171371 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0153786 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0164276 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00231993 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0144099 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00946504 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0103296 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.043233 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0162083 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.0542864 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0255687 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00156729 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   1.8975e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.00246944 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.00218956 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      6.838e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.00228998 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.0333168 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.598564 seconds.
[PassRunner] (final validation)
