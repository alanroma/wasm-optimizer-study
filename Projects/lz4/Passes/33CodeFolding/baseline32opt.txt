[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                        1.0527e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                    0.000664013 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                        1.5108e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                    0.00222834 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination...     0.00258509 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                     4.8295e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                        0.0600569 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                                0.0185152 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.0268082 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...                0.00300072 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...              0.011689 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                    0.0042202 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...               0.117147 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants-propagate... 0.111566 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                       0.0119093 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...        0.0569273 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.0218125 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.007155 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.0139973 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-locals...                       0.0592471 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    0.0403445 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                    0.0490061 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.0182779 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.00377647 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    0.0364541 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.00374864 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.0173587 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.69857 seconds.
[PassRunner] (final validation)
