; ModuleID = 'lz4cli.c'
source_filename = "lz4cli.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct._IO_FILE = type opaque
%struct.LZ4IO_prefs_s = type opaque
%struct.stat = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i64, i32, i32, %struct.timespec, %struct.timespec, %struct.timespec, i64 }
%struct.timespec = type { i32, i32 }
%struct.__dirstream = type opaque
%struct.dirent = type { i64, i64, i16, i8, [256 x i8] }

@__const.main.nullOutput = private unnamed_addr constant [5 x i8] c"null\00", align 1
@__const.main.extension = private unnamed_addr constant [5 x i8] c".lz4\00", align 1
@stderr = external constant %struct._IO_FILE*, align 4
@.str = private unnamed_addr constant [39 x i8] c"Allocation error : not enough memory \0A\00", align 1
@stdinmark = internal constant [6 x i8] c"stdin\00", align 1
@.str.1 = private unnamed_addr constant [7 x i8] c"lz4cat\00", align 1
@stdoutmark = internal constant [7 x i8] c"stdout\00", align 1
@displayLevel = internal global i32 2, align 4
@.str.2 = private unnamed_addr constant [6 x i8] c"unlz4\00", align 1
@.str.3 = private unnamed_addr constant [5 x i8] c"lz4c\00", align 1
@g_lz4c_legacy_commands = internal global i32 0, align 4
@.str.4 = private unnamed_addr constant [3 x i8] c"--\00", align 1
@.str.5 = private unnamed_addr constant [11 x i8] c"--compress\00", align 1
@.str.6 = private unnamed_addr constant [13 x i8] c"--decompress\00", align 1
@.str.7 = private unnamed_addr constant [13 x i8] c"--uncompress\00", align 1
@.str.8 = private unnamed_addr constant [11 x i8] c"--multiple\00", align 1
@.str.9 = private unnamed_addr constant [7 x i8] c"--test\00", align 1
@.str.10 = private unnamed_addr constant [8 x i8] c"--force\00", align 1
@.str.11 = private unnamed_addr constant [11 x i8] c"--no-force\00", align 1
@.str.12 = private unnamed_addr constant [9 x i8] c"--stdout\00", align 1
@.str.13 = private unnamed_addr constant [12 x i8] c"--to-stdout\00", align 1
@.str.14 = private unnamed_addr constant [12 x i8] c"--frame-crc\00", align 1
@.str.15 = private unnamed_addr constant [15 x i8] c"--no-frame-crc\00", align 1
@.str.16 = private unnamed_addr constant [15 x i8] c"--content-size\00", align 1
@.str.17 = private unnamed_addr constant [18 x i8] c"--no-content-size\00", align 1
@.str.18 = private unnamed_addr constant [7 x i8] c"--list\00", align 1
@.str.19 = private unnamed_addr constant [9 x i8] c"--sparse\00", align 1
@.str.20 = private unnamed_addr constant [12 x i8] c"--no-sparse\00", align 1
@.str.21 = private unnamed_addr constant [17 x i8] c"--favor-decSpeed\00", align 1
@.str.22 = private unnamed_addr constant [10 x i8] c"--verbose\00", align 1
@.str.23 = private unnamed_addr constant [8 x i8] c"--quiet\00", align 1
@.str.24 = private unnamed_addr constant [10 x i8] c"--version\00", align 1
@stdout = external constant %struct._IO_FILE*, align 4
@.str.25 = private unnamed_addr constant [31 x i8] c"*** %s %i-bits v%s, by %s ***\0A\00", align 1
@.str.26 = private unnamed_addr constant [27 x i8] c"LZ4 command line interface\00", align 1
@.str.27 = private unnamed_addr constant [12 x i8] c"Yann Collet\00", align 1
@.str.28 = private unnamed_addr constant [7 x i8] c"--help\00", align 1
@.str.29 = private unnamed_addr constant [7 x i8] c"--keep\00", align 1
@.str.30 = private unnamed_addr constant [5 x i8] c"--rm\00", align 1
@.str.31 = private unnamed_addr constant [7 x i8] c"--fast\00", align 1
@.str.32 = private unnamed_addr constant [7 x i8] c"--best\00", align 1
@.str.33 = private unnamed_addr constant [3 x i8] c"c0\00", align 1
@.str.34 = private unnamed_addr constant [3 x i8] c"c1\00", align 1
@.str.35 = private unnamed_addr constant [3 x i8] c"c2\00", align 1
@.str.36 = private unnamed_addr constant [3 x i8] c"hc\00", align 1
@.str.37 = private unnamed_addr constant [2 x i8] c"y\00", align 1
@.str.38 = private unnamed_addr constant [29 x i8] c"using blocks of size %u KB \0A\00", align 1
@.str.39 = private unnamed_addr constant [32 x i8] c"using blocks of size %u bytes \0A\00", align 1
@nulmark = internal constant [10 x i8] c"/dev/null\00", align 1
@.str.40 = private unnamed_addr constant [71 x i8] c"Warning : %s won't be used ! Do you want multiple input files (-m) ? \0A\00", align 1
@.str.41 = private unnamed_addr constant [30 x i8] c"_POSIX_VERSION defined: %ldL\0A\00", align 1
@.str.42 = private unnamed_addr constant [38 x i8] c"PLATFORM_POSIX_VERSION defined: %ldL\0A\00", align 1
@.str.43 = private unnamed_addr constant [33 x i8] c"_FILE_OFFSET_BITS defined: %ldL\0A\00", align 1
@.str.44 = private unnamed_addr constant [21 x i8] c"Blocks size : %u KB\0A\00", align 1
@.str.45 = private unnamed_addr constant [7 x i8] c"%u %s\0A\00", align 1
@stdin = external constant %struct._IO_FILE*, align 4
@.str.46 = private unnamed_addr constant [33 x i8] c"refusing to read from a console\0A\00", align 1
@.str.47 = private unnamed_addr constant [28 x i8] c"%s: is not a regular file \0A\00", align 1
@.str.48 = private unnamed_addr constant [102 x i8] c"Warning : using stdout as default output. Do not rely on this behavior: use explicit `-c` instead ! \0A\00", align 1
@.str.49 = private unnamed_addr constant [5 x i8] c".lz4\00", align 1
@.str.50 = private unnamed_addr constant [35 x i8] c"Compressed filename will be : %s \0A\00", align 1
@.str.51 = private unnamed_addr constant [37 x i8] c"Cannot determine an output filename\0A\00", align 1
@.str.52 = private unnamed_addr constant [19 x i8] c"Decoding file %s \0A\00", align 1
@.str.53 = private unnamed_addr constant [53 x i8] c"refusing to read from standard input in --list mode\0A\00", align 1
@.str.54 = private unnamed_addr constant [16 x i8] c"output_filename\00", align 1
@.str.55 = private unnamed_addr constant [9 x i8] c"lz4cli.c\00", align 1
@__func__.main = private unnamed_addr constant [5 x i8] c"main\00", align 1
@.str.56 = private unnamed_addr constant [12 x i8] c"*\\dummy^!//\00", align 1
@.str.57 = private unnamed_addr constant [42 x i8] c"refusing to write to console without -c \0A\00", align 1
@.str.58 = private unnamed_addr constant [18 x i8] c"ifnIdx <= INT_MAX\00", align 1
@.str.59 = private unnamed_addr constant [48 x i8] c"! Generating LZ4 Legacy format (deprecated) ! \0A\00", align 1
@.str.60 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.61 = private unnamed_addr constant [22 x i8] c"Advanced arguments :\0A\00", align 1
@.str.62 = private unnamed_addr constant [44 x i8] c" -V     : display Version number and exit \0A\00", align 1
@.str.63 = private unnamed_addr constant [25 x i8] c" -v     : verbose mode \0A\00", align 1
@.str.64 = private unnamed_addr constant [67 x i8] c" -q     : suppress warnings; specify twice to suppress errors too\0A\00", align 1
@.str.65 = private unnamed_addr constant [69 x i8] c" -c     : force write to standard output, even if it is the console\0A\00", align 1
@.str.66 = private unnamed_addr constant [42 x i8] c" -t     : test compressed file integrity\0A\00", align 1
@.str.67 = private unnamed_addr constant [69 x i8] c" -m     : multiple input files (implies automatic output filenames)\0A\00", align 1
@.str.68 = private unnamed_addr constant [62 x i8] c" -r     : operate recursively on directories (sets also -m) \0A\00", align 1
@.str.69 = private unnamed_addr constant [67 x i8] c" -l     : compress using Legacy format (Linux kernel compression)\0A\00", align 1
@.str.70 = private unnamed_addr constant [55 x i8] c" -B#    : cut file into blocks of size # bytes [32+] \0A\00", align 1
@.str.71 = private unnamed_addr constant [67 x i8] c"                     or predefined block size [4-7] (default: 7) \0A\00", align 1
@.str.72 = private unnamed_addr constant [41 x i8] c" -BI    : Block Independence (default) \0A\00", align 1
@.str.73 = private unnamed_addr constant [58 x i8] c" -BD    : Block dependency (improves compression ratio) \0A\00", align 1
@.str.74 = private unnamed_addr constant [53 x i8] c" -BX    : enable block checksum (default:disabled) \0A\00", align 1
@.str.75 = private unnamed_addr constant [61 x i8] c"--no-frame-crc : disable stream checksum (default:enabled) \0A\00", align 1
@.str.76 = private unnamed_addr constant [80 x i8] c"--content-size : compressed frame includes original size (default:not present)\0A\00", align 1
@.str.77 = private unnamed_addr constant [105 x i8] c"--list FILE : lists information about .lz4 files (useful for files compressed with --content-size flag)\0A\00", align 1
@.str.78 = private unnamed_addr constant [76 x i8] c"--[no-]sparse  : sparse mode (default:enabled on file, disabled on stdout)\0A\00", align 1
@.str.79 = private unnamed_addr constant [80 x i8] c"--favor-decSpeed: compressed files decompress faster, but are less compressed \0A\00", align 1
@.str.80 = private unnamed_addr constant [66 x i8] c"--fast[=#]: switch to ultra fast compression level (default: %i)\0A\00", align 1
@.str.81 = private unnamed_addr constant [23 x i8] c"--best  : same as -%d\0A\00", align 1
@.str.82 = private unnamed_addr constant [24 x i8] c"Benchmark arguments : \0A\00", align 1
@.str.83 = private unnamed_addr constant [71 x i8] c" -b#    : benchmark file(s), using # compression level (default : 1) \0A\00", align 1
@.str.84 = private unnamed_addr constant [67 x i8] c" -e#    : test all compression levels from -bX to # (default : 1)\0A\00", align 1
@.str.85 = private unnamed_addr constant [62 x i8] c" -i#    : minimum evaluation time in seconds (default : 3s) \0A\00", align 1
@.str.86 = private unnamed_addr constant [21 x i8] c"Legacy arguments : \0A\00", align 1
@.str.87 = private unnamed_addr constant [29 x i8] c" -c0    : fast compression \0A\00", align 1
@.str.88 = private unnamed_addr constant [29 x i8] c" -c1    : high compression \0A\00", align 1
@.str.89 = private unnamed_addr constant [34 x i8] c" -c2,-hc: very high compression \0A\00", align 1
@.str.90 = private unnamed_addr constant [47 x i8] c" -y     : overwrite output without prompting \0A\00", align 1
@.str.91 = private unnamed_addr constant [10 x i8] c"Usage : \0A\00", align 1
@.str.92 = private unnamed_addr constant [34 x i8] c"      %s [arg] [input] [output] \0A\00", align 1
@.str.93 = private unnamed_addr constant [23 x i8] c"input   : a filename \0A\00", align 1
@.str.94 = private unnamed_addr constant [70 x i8] c"          with no FILE, or when FILE is - or %s, read standard input\0A\00", align 1
@.str.95 = private unnamed_addr constant [14 x i8] c"Arguments : \0A\00", align 1
@.str.96 = private unnamed_addr constant [39 x i8] c" -1     : Fast compression (default) \0A\00", align 1
@.str.97 = private unnamed_addr constant [29 x i8] c" -9     : High compression \0A\00", align 1
@.str.98 = private unnamed_addr constant [52 x i8] c" -d     : decompression (default for %s extension)\0A\00", align 1
@.str.99 = private unnamed_addr constant [30 x i8] c" -z     : force compression \0A\00", align 1
@.str.100 = private unnamed_addr constant [35 x i8] c" -D FILE: use FILE as dictionary \0A\00", align 1
@.str.101 = private unnamed_addr constant [47 x i8] c" -f     : overwrite output without prompting \0A\00", align 1
@.str.102 = private unnamed_addr constant [48 x i8] c" -k     : preserve source files(s)  (default) \0A\00", align 1
@.str.103 = private unnamed_addr constant [66 x i8] c"--rm    : remove source file(s) after successful de/compression \0A\00", align 1
@.str.104 = private unnamed_addr constant [44 x i8] c" -h/-H  : display help/long help and exit \0A\00", align 1
@.str.105 = private unnamed_addr constant [22 x i8] c"Incorrect parameters\0A\00", align 1
@.str.106 = private unnamed_addr constant [30 x i8] c"****************************\0A\00", align 1
@.str.107 = private unnamed_addr constant [30 x i8] c"***** Advanced comment *****\0A\00", align 1
@.str.108 = private unnamed_addr constant [35 x i8] c"Which values can [output] have ? \0A\00", align 1
@.str.109 = private unnamed_addr constant [35 x i8] c"---------------------------------\0A\00", align 1
@.str.110 = private unnamed_addr constant [24 x i8] c"[output] : a filename \0A\00", align 1
@.str.111 = private unnamed_addr constant [56 x i8] c"          '%s', or '-' for standard output (pipe mode)\0A\00", align 1
@.str.112 = private unnamed_addr constant [47 x i8] c"          '%s' to discard output (test mode) \0A\00", align 1
@.str.113 = private unnamed_addr constant [5 x i8] c"null\00", align 1
@.str.114 = private unnamed_addr constant [77 x i8] c"[output] can be left empty. In this case, it receives the following value :\0A\00", align 1
@.str.115 = private unnamed_addr constant [67 x i8] c"          - if stdout is not the console, then [output] = stdout \0A\00", align 1
@.str.116 = private unnamed_addr constant [37 x i8] c"          - if stdout is console : \0A\00", align 1
@.str.117 = private unnamed_addr constant [57 x i8] c"               + for compression, output to filename%s \0A\00", align 1
@.str.118 = private unnamed_addr constant [69 x i8] c"               + for decompression, output to filename without '%s'\0A\00", align 1
@.str.119 = private unnamed_addr constant [72 x i8] c"                    > if input filename has no '%s' extension : error \0A\00", align 1
@.str.120 = private unnamed_addr constant [23 x i8] c"Compression levels : \0A\00", align 1
@.str.121 = private unnamed_addr constant [23 x i8] c"---------------------\0A\00", align 1
@.str.122 = private unnamed_addr constant [48 x i8] c"-0 ... -2  => Fast compression, all identicals\0A\00", align 1
@.str.123 = private unnamed_addr constant [78 x i8] c"-3 ... -%d => High compression; higher number == more compression but slower\0A\00", align 1
@.str.124 = private unnamed_addr constant [34 x i8] c"stdin, stdout and the console : \0A\00", align 1
@.str.125 = private unnamed_addr constant [34 x i8] c"--------------------------------\0A\00", align 1
@.str.126 = private unnamed_addr constant [68 x i8] c"To protect the console from binary flooding (bad argument mistake)\0A\00", align 1
@.str.127 = private unnamed_addr constant [59 x i8] c"%s will refuse to read from console, or write to console \0A\00", align 1
@.str.128 = private unnamed_addr constant [66 x i8] c"except if '-c' command is specified, to force output to console \0A\00", align 1
@.str.129 = private unnamed_addr constant [18 x i8] c"Simple example :\0A\00", align 1
@.str.130 = private unnamed_addr constant [18 x i8] c"----------------\0A\00", align 1
@.str.131 = private unnamed_addr constant [72 x i8] c"1 : compress 'filename' fast, using default output name 'filename.lz4'\0A\00", align 1
@.str.132 = private unnamed_addr constant [23 x i8] c"          %s filename\0A\00", align 1
@.str.133 = private unnamed_addr constant [50 x i8] c"Short arguments can be aggregated. For example :\0A\00", align 1
@.str.134 = private unnamed_addr constant [36 x i8] c"----------------------------------\0A\00", align 1
@.str.135 = private unnamed_addr constant [78 x i8] c"2 : compress 'filename' in high compression mode, overwrite output if exists\0A\00", align 1
@.str.136 = private unnamed_addr constant [30 x i8] c"          %s -9 -f filename \0A\00", align 1
@.str.137 = private unnamed_addr constant [24 x i8] c"    is equivalent to :\0A\00", align 1
@.str.138 = private unnamed_addr constant [28 x i8] c"          %s -9f filename \0A\00", align 1
@.str.139 = private unnamed_addr constant [51 x i8] c"%s can be used in 'pure pipe mode'. For example :\0A\00", align 1
@.str.140 = private unnamed_addr constant [39 x i8] c"-------------------------------------\0A\00", align 1
@.str.141 = private unnamed_addr constant [70 x i8] c"3 : compress data stream from 'generator', send result to 'consumer'\0A\00", align 1
@.str.142 = private unnamed_addr constant [38 x i8] c"          generator | %s | consumer \0A\00", align 1
@.str.143 = private unnamed_addr constant [23 x i8] c"***** Warning  ***** \0A\00", align 1
@.str.144 = private unnamed_addr constant [48 x i8] c"Legacy arguments take precedence. Therefore : \0A\00", align 1
@.str.145 = private unnamed_addr constant [36 x i8] c"--------------------------------- \0A\00", align 1
@.str.146 = private unnamed_addr constant [28 x i8] c"          %s -hc filename \0A\00", align 1
@.str.147 = private unnamed_addr constant [53 x i8] c"means 'compress filename in high compression mode' \0A\00", align 1
@.str.148 = private unnamed_addr constant [28 x i8] c"It is not equivalent to : \0A\00", align 1
@.str.149 = private unnamed_addr constant [30 x i8] c"          %s -h -c filename \0A\00", align 1
@.str.150 = private unnamed_addr constant [37 x i8] c"which displays help text and exits \0A\00", align 1
@.str.151 = private unnamed_addr constant [20 x i8] c"pos + len < bufSize\00", align 1
@.str.152 = private unnamed_addr constant [9 x i8] c"./util.h\00", align 1
@__func__.UTIL_createFileList = private unnamed_addr constant [20 x i8] c"UTIL_createFileList\00", align 1
@.str.153 = private unnamed_addr constant [13 x i8] c"bufend > buf\00", align 1
@.str.154 = private unnamed_addr constant [32 x i8] c"Cannot open directory '%s': %s\0A\00", align 1
@.str.155 = private unnamed_addr constant [3 x i8] c"..\00", align 1
@.str.156 = private unnamed_addr constant [2 x i8] c".\00", align 1
@.str.157 = private unnamed_addr constant [23 x i8] c"readdir(%s) error: %s\0A\00", align 1
@.str.158 = private unnamed_addr constant [28 x i8] c"Press enter to continue...\0A\00", align 1

; Function Attrs: nounwind
define hidden i32 @main(i32 %argc, i8** %argv) #0 {
entry:
  %retval = alloca i32, align 4
  %argc.addr = alloca i32, align 4
  %argv.addr = alloca i8**, align 4
  %i = alloca i32, align 4
  %cLevel = alloca i32, align 4
  %cLevelLast = alloca i32, align 4
  %legacy_format = alloca i32, align 4
  %forceStdout = alloca i32, align 4
  %main_pause = alloca i32, align 4
  %multiple_inputs = alloca i32, align 4
  %all_arguments_are_files = alloca i32, align 4
  %operationResult = alloca i32, align 4
  %mode = alloca i32, align 4
  %input_filename = alloca i8*, align 4
  %output_filename = alloca i8*, align 4
  %dictionary_filename = alloca i8*, align 4
  %dynNameSpace = alloca i8*, align 4
  %inFileNames = alloca i8**, align 4
  %ifnIdx = alloca i32, align 4
  %prefs = alloca %struct.LZ4IO_prefs_s*, align 4
  %nullOutput = alloca [5 x i8], align 1
  %extension = alloca [5 x i8], align 1
  %blockSize = alloca i32, align 4
  %exeName = alloca i8*, align 4
  %extendedFileList = alloca i8**, align 4
  %fileNamesBuf = alloca i8*, align 4
  %fileNamesNb = alloca i32, align 4
  %recursive = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %argument = alloca i8*, align 4
  %fastLevel = alloca i32, align 4
  %exitBlockProperties = alloca i32, align 4
  %B = alloca i32, align 4
  %iters = alloca i32, align 4
  %u = alloca i32, align 4
  %l = alloca i32, align 4
  %outl = alloca i32, align 4
  %inl = alloca i32, align 4
  store i32 0, i32* %retval, align 4
  store i32 %argc, i32* %argc.addr, align 4, !tbaa !2
  store i8** %argv, i8*** %argv.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %cLevel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 1, i32* %cLevel, align 4, !tbaa !2
  %2 = bitcast i32* %cLevelLast to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 -10000, i32* %cLevelLast, align 4, !tbaa !2
  %3 = bitcast i32* %legacy_format to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  store i32 0, i32* %legacy_format, align 4, !tbaa !2
  %4 = bitcast i32* %forceStdout to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 0, i32* %forceStdout, align 4, !tbaa !2
  %5 = bitcast i32* %main_pause to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  store i32 0, i32* %main_pause, align 4, !tbaa !2
  %6 = bitcast i32* %multiple_inputs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  store i32 0, i32* %multiple_inputs, align 4, !tbaa !2
  %7 = bitcast i32* %all_arguments_are_files to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  store i32 0, i32* %all_arguments_are_files, align 4, !tbaa !2
  %8 = bitcast i32* %operationResult to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  store i32 0, i32* %operationResult, align 4, !tbaa !2
  %9 = bitcast i32* %mode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  store i32 0, i32* %mode, align 4, !tbaa !8
  %10 = bitcast i8** %input_filename to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  store i8* null, i8** %input_filename, align 4, !tbaa !6
  %11 = bitcast i8** %output_filename to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  store i8* null, i8** %output_filename, align 4, !tbaa !6
  %12 = bitcast i8** %dictionary_filename to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  store i8* null, i8** %dictionary_filename, align 4, !tbaa !6
  %13 = bitcast i8** %dynNameSpace to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  store i8* null, i8** %dynNameSpace, align 4, !tbaa !6
  %14 = bitcast i8*** %inFileNames to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = load i32, i32* %argc.addr, align 4, !tbaa !2
  %call = call i8* @calloc(i32 %15, i32 4)
  %16 = bitcast i8* %call to i8**
  store i8** %16, i8*** %inFileNames, align 4, !tbaa !6
  %17 = bitcast i32* %ifnIdx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  store i32 0, i32* %ifnIdx, align 4, !tbaa !2
  %18 = bitcast %struct.LZ4IO_prefs_s** %prefs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %call1 = call %struct.LZ4IO_prefs_s* @LZ4IO_defaultPreferences()
  store %struct.LZ4IO_prefs_s* %call1, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %19 = bitcast [5 x i8]* %nullOutput to i8*
  call void @llvm.lifetime.start.p0i8(i64 5, i8* %19) #4
  %20 = bitcast [5 x i8]* %nullOutput to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %20, i8* align 1 getelementptr inbounds ([5 x i8], [5 x i8]* @__const.main.nullOutput, i32 0, i32 0), i32 5, i1 false)
  %21 = bitcast [5 x i8]* %extension to i8*
  call void @llvm.lifetime.start.p0i8(i64 5, i8* %21) #4
  %22 = bitcast [5 x i8]* %extension to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %22, i8* align 1 getelementptr inbounds ([5 x i8], [5 x i8]* @__const.main.extension, i32 0, i32 0), i32 5, i1 false)
  %23 = bitcast i32* %blockSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %call2 = call i32 @LZ4IO_setBlockSizeID(%struct.LZ4IO_prefs_s* %24, i32 7)
  store i32 %call2, i32* %blockSize, align 4, !tbaa !9
  %25 = bitcast i8** %exeName to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #4
  %26 = load i8**, i8*** %argv.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8*, i8** %26, i32 0
  %27 = load i8*, i8** %arrayidx, align 4, !tbaa !6
  %call3 = call i8* @lastNameFromPath(i8* %27)
  store i8* %call3, i8** %exeName, align 4, !tbaa !6
  %28 = bitcast i8*** %extendedFileList to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #4
  store i8** null, i8*** %extendedFileList, align 4, !tbaa !6
  %29 = bitcast i8** %fileNamesBuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  store i8* null, i8** %fileNamesBuf, align 4, !tbaa !6
  %30 = bitcast i32* %fileNamesNb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  %31 = bitcast i32* %recursive to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #4
  store i32 0, i32* %recursive, align 4, !tbaa !2
  %32 = load i8**, i8*** %inFileNames, align 4, !tbaa !6
  %cmp = icmp eq i8** %32, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %33 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call4 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %33, i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str, i32 0, i32 0))
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup722

if.end:                                           ; preds = %entry
  %34 = load i8**, i8*** %inFileNames, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds i8*, i8** %34, i32 0
  store i8* getelementptr inbounds ([6 x i8], [6 x i8]* @stdinmark, i32 0, i32 0), i8** %arrayidx5, align 4, !tbaa !6
  %35 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %call6 = call i32 @LZ4IO_setOverwrite(%struct.LZ4IO_prefs_s* %35, i32 0)
  %36 = load i8*, i8** %exeName, align 4, !tbaa !6
  %call7 = call i32 @exeNameMatch(i8* %36, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.1, i32 0, i32 0))
  %tobool = icmp ne i32 %call7, 0
  br i1 %tobool, label %if.then8, label %if.end11

if.then8:                                         ; preds = %if.end
  store i32 2, i32* %mode, align 4, !tbaa !8
  %37 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %call9 = call i32 @LZ4IO_setOverwrite(%struct.LZ4IO_prefs_s* %37, i32 1)
  %38 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %call10 = call i32 @LZ4IO_setPassThrough(%struct.LZ4IO_prefs_s* %38, i32 1)
  %39 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  call void @LZ4IO_setRemoveSrcFile(%struct.LZ4IO_prefs_s* %39, i32 0)
  store i32 1, i32* %forceStdout, align 4, !tbaa !2
  store i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0), i8** %output_filename, align 4, !tbaa !6
  store i32 1, i32* @displayLevel, align 4, !tbaa !2
  store i32 1, i32* %multiple_inputs, align 4, !tbaa !2
  br label %if.end11

if.end11:                                         ; preds = %if.then8, %if.end
  %40 = load i8*, i8** %exeName, align 4, !tbaa !6
  %call12 = call i32 @exeNameMatch(i8* %40, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.2, i32 0, i32 0))
  %tobool13 = icmp ne i32 %call12, 0
  br i1 %tobool13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.end11
  store i32 2, i32* %mode, align 4, !tbaa !8
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %if.end11
  %41 = load i8*, i8** %exeName, align 4, !tbaa !6
  %call16 = call i32 @exeNameMatch(i8* %41, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.3, i32 0, i32 0))
  %tobool17 = icmp ne i32 %call16, 0
  br i1 %tobool17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end15
  store i32 1, i32* @g_lz4c_legacy_commands, align 4, !tbaa !2
  br label %if.end19

if.end19:                                         ; preds = %if.then18, %if.end15
  store i32 1, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end19
  %42 = load i32, i32* %i, align 4, !tbaa !2
  %43 = load i32, i32* %argc.addr, align 4, !tbaa !2
  %cmp20 = icmp slt i32 %42, %43
  br i1 %cmp20, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %44 = bitcast i8** %argument to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #4
  %45 = load i8**, i8*** %argv.addr, align 4, !tbaa !6
  %46 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i8*, i8** %45, i32 %46
  %47 = load i8*, i8** %arrayidx21, align 4, !tbaa !6
  store i8* %47, i8** %argument, align 4, !tbaa !6
  %48 = load i8*, i8** %argument, align 4, !tbaa !6
  %tobool22 = icmp ne i8* %48, null
  br i1 %tobool22, label %if.end24, label %if.then23

if.then23:                                        ; preds = %for.body
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end24:                                         ; preds = %for.body
  %49 = load i32, i32* %all_arguments_are_files, align 4, !tbaa !2
  %tobool25 = icmp ne i32 %49, 0
  br i1 %tobool25, label %if.end355, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.end24
  %50 = load i8*, i8** %argument, align 4, !tbaa !6
  %arrayidx26 = getelementptr inbounds i8, i8* %50, i32 0
  %51 = load i8, i8* %arrayidx26, align 1, !tbaa !8
  %conv = sext i8 %51 to i32
  %cmp27 = icmp eq i32 %conv, 45
  br i1 %cmp27, label %if.then29, label %if.end355

if.then29:                                        ; preds = %land.lhs.true
  %52 = load i8*, i8** %argument, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds i8, i8* %52, i32 1
  %53 = load i8, i8* %arrayidx30, align 1, !tbaa !8
  %conv31 = sext i8 %53 to i32
  %cmp32 = icmp eq i32 %conv31, 0
  br i1 %cmp32, label %if.then34, label %if.end38

if.then34:                                        ; preds = %if.then29
  %54 = load i8*, i8** %input_filename, align 4, !tbaa !6
  %tobool35 = icmp ne i8* %54, null
  br i1 %tobool35, label %if.else, label %if.then36

if.then36:                                        ; preds = %if.then34
  store i8* getelementptr inbounds ([6 x i8], [6 x i8]* @stdinmark, i32 0, i32 0), i8** %input_filename, align 4, !tbaa !6
  br label %if.end37

if.else:                                          ; preds = %if.then34
  store i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0), i8** %output_filename, align 4, !tbaa !6
  br label %if.end37

if.end37:                                         ; preds = %if.else, %if.then36
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end38:                                         ; preds = %if.then29
  %55 = load i8*, i8** %argument, align 4, !tbaa !6
  %arrayidx39 = getelementptr inbounds i8, i8* %55, i32 1
  %56 = load i8, i8* %arrayidx39, align 1, !tbaa !8
  %conv40 = sext i8 %56 to i32
  %cmp41 = icmp eq i32 %conv40, 45
  br i1 %cmp41, label %if.then43, label %if.end178

if.then43:                                        ; preds = %if.end38
  %57 = load i8*, i8** %argument, align 4, !tbaa !6
  %call44 = call i32 @strcmp(i8* %57, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.4, i32 0, i32 0))
  %tobool45 = icmp ne i32 %call44, 0
  br i1 %tobool45, label %if.end47, label %if.then46

if.then46:                                        ; preds = %if.then43
  store i32 1, i32* %all_arguments_are_files, align 4, !tbaa !2
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end47:                                         ; preds = %if.then43
  %58 = load i8*, i8** %argument, align 4, !tbaa !6
  %call48 = call i32 @strcmp(i8* %58, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.5, i32 0, i32 0))
  %tobool49 = icmp ne i32 %call48, 0
  br i1 %tobool49, label %if.end51, label %if.then50

if.then50:                                        ; preds = %if.end47
  store i32 1, i32* %mode, align 4, !tbaa !8
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end51:                                         ; preds = %if.end47
  %59 = load i8*, i8** %argument, align 4, !tbaa !6
  %call52 = call i32 @strcmp(i8* %59, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.6, i32 0, i32 0))
  %tobool53 = icmp ne i32 %call52, 0
  br i1 %tobool53, label %lor.lhs.false, label %if.then56

lor.lhs.false:                                    ; preds = %if.end51
  %60 = load i8*, i8** %argument, align 4, !tbaa !6
  %call54 = call i32 @strcmp(i8* %60, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.7, i32 0, i32 0))
  %tobool55 = icmp ne i32 %call54, 0
  br i1 %tobool55, label %if.end57, label %if.then56

if.then56:                                        ; preds = %lor.lhs.false, %if.end51
  store i32 2, i32* %mode, align 4, !tbaa !8
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end57:                                         ; preds = %lor.lhs.false
  %61 = load i8*, i8** %argument, align 4, !tbaa !6
  %call58 = call i32 @strcmp(i8* %61, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.8, i32 0, i32 0))
  %tobool59 = icmp ne i32 %call58, 0
  br i1 %tobool59, label %if.end61, label %if.then60

if.then60:                                        ; preds = %if.end57
  store i32 1, i32* %multiple_inputs, align 4, !tbaa !2
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end61:                                         ; preds = %if.end57
  %62 = load i8*, i8** %argument, align 4, !tbaa !6
  %call62 = call i32 @strcmp(i8* %62, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.9, i32 0, i32 0))
  %tobool63 = icmp ne i32 %call62, 0
  br i1 %tobool63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %if.end61
  store i32 3, i32* %mode, align 4, !tbaa !8
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end65:                                         ; preds = %if.end61
  %63 = load i8*, i8** %argument, align 4, !tbaa !6
  %call66 = call i32 @strcmp(i8* %63, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.10, i32 0, i32 0))
  %tobool67 = icmp ne i32 %call66, 0
  br i1 %tobool67, label %if.end70, label %if.then68

if.then68:                                        ; preds = %if.end65
  %64 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %call69 = call i32 @LZ4IO_setOverwrite(%struct.LZ4IO_prefs_s* %64, i32 1)
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end70:                                         ; preds = %if.end65
  %65 = load i8*, i8** %argument, align 4, !tbaa !6
  %call71 = call i32 @strcmp(i8* %65, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.11, i32 0, i32 0))
  %tobool72 = icmp ne i32 %call71, 0
  br i1 %tobool72, label %if.end75, label %if.then73

if.then73:                                        ; preds = %if.end70
  %66 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %call74 = call i32 @LZ4IO_setOverwrite(%struct.LZ4IO_prefs_s* %66, i32 0)
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end75:                                         ; preds = %if.end70
  %67 = load i8*, i8** %argument, align 4, !tbaa !6
  %call76 = call i32 @strcmp(i8* %67, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.12, i32 0, i32 0))
  %tobool77 = icmp ne i32 %call76, 0
  br i1 %tobool77, label %lor.lhs.false78, label %if.then81

lor.lhs.false78:                                  ; preds = %if.end75
  %68 = load i8*, i8** %argument, align 4, !tbaa !6
  %call79 = call i32 @strcmp(i8* %68, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.13, i32 0, i32 0))
  %tobool80 = icmp ne i32 %call79, 0
  br i1 %tobool80, label %if.end82, label %if.then81

if.then81:                                        ; preds = %lor.lhs.false78, %if.end75
  store i32 1, i32* %forceStdout, align 4, !tbaa !2
  store i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0), i8** %output_filename, align 4, !tbaa !6
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end82:                                         ; preds = %lor.lhs.false78
  %69 = load i8*, i8** %argument, align 4, !tbaa !6
  %call83 = call i32 @strcmp(i8* %69, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.14, i32 0, i32 0))
  %tobool84 = icmp ne i32 %call83, 0
  br i1 %tobool84, label %if.end87, label %if.then85

if.then85:                                        ; preds = %if.end82
  %70 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %call86 = call i32 @LZ4IO_setStreamChecksumMode(%struct.LZ4IO_prefs_s* %70, i32 1)
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end87:                                         ; preds = %if.end82
  %71 = load i8*, i8** %argument, align 4, !tbaa !6
  %call88 = call i32 @strcmp(i8* %71, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.15, i32 0, i32 0))
  %tobool89 = icmp ne i32 %call88, 0
  br i1 %tobool89, label %if.end92, label %if.then90

if.then90:                                        ; preds = %if.end87
  %72 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %call91 = call i32 @LZ4IO_setStreamChecksumMode(%struct.LZ4IO_prefs_s* %72, i32 0)
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end92:                                         ; preds = %if.end87
  %73 = load i8*, i8** %argument, align 4, !tbaa !6
  %call93 = call i32 @strcmp(i8* %73, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.16, i32 0, i32 0))
  %tobool94 = icmp ne i32 %call93, 0
  br i1 %tobool94, label %if.end97, label %if.then95

if.then95:                                        ; preds = %if.end92
  %74 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %call96 = call i32 @LZ4IO_setContentSize(%struct.LZ4IO_prefs_s* %74, i32 1)
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end97:                                         ; preds = %if.end92
  %75 = load i8*, i8** %argument, align 4, !tbaa !6
  %call98 = call i32 @strcmp(i8* %75, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.17, i32 0, i32 0))
  %tobool99 = icmp ne i32 %call98, 0
  br i1 %tobool99, label %if.end102, label %if.then100

if.then100:                                       ; preds = %if.end97
  %76 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %call101 = call i32 @LZ4IO_setContentSize(%struct.LZ4IO_prefs_s* %76, i32 0)
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end102:                                        ; preds = %if.end97
  %77 = load i8*, i8** %argument, align 4, !tbaa !6
  %call103 = call i32 @strcmp(i8* %77, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.18, i32 0, i32 0))
  %tobool104 = icmp ne i32 %call103, 0
  br i1 %tobool104, label %if.end106, label %if.then105

if.then105:                                       ; preds = %if.end102
  store i32 5, i32* %mode, align 4, !tbaa !8
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end106:                                        ; preds = %if.end102
  %78 = load i8*, i8** %argument, align 4, !tbaa !6
  %call107 = call i32 @strcmp(i8* %78, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.19, i32 0, i32 0))
  %tobool108 = icmp ne i32 %call107, 0
  br i1 %tobool108, label %if.end111, label %if.then109

if.then109:                                       ; preds = %if.end106
  %79 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %call110 = call i32 @LZ4IO_setSparseFile(%struct.LZ4IO_prefs_s* %79, i32 2)
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end111:                                        ; preds = %if.end106
  %80 = load i8*, i8** %argument, align 4, !tbaa !6
  %call112 = call i32 @strcmp(i8* %80, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.20, i32 0, i32 0))
  %tobool113 = icmp ne i32 %call112, 0
  br i1 %tobool113, label %if.end116, label %if.then114

if.then114:                                       ; preds = %if.end111
  %81 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %call115 = call i32 @LZ4IO_setSparseFile(%struct.LZ4IO_prefs_s* %81, i32 0)
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end116:                                        ; preds = %if.end111
  %82 = load i8*, i8** %argument, align 4, !tbaa !6
  %call117 = call i32 @strcmp(i8* %82, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.21, i32 0, i32 0))
  %tobool118 = icmp ne i32 %call117, 0
  br i1 %tobool118, label %if.end120, label %if.then119

if.then119:                                       ; preds = %if.end116
  %83 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  call void @LZ4IO_favorDecSpeed(%struct.LZ4IO_prefs_s* %83, i32 1)
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end120:                                        ; preds = %if.end116
  %84 = load i8*, i8** %argument, align 4, !tbaa !6
  %call121 = call i32 @strcmp(i8* %84, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.22, i32 0, i32 0))
  %tobool122 = icmp ne i32 %call121, 0
  br i1 %tobool122, label %if.end124, label %if.then123

if.then123:                                       ; preds = %if.end120
  %85 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %inc = add i32 %85, 1
  store i32 %inc, i32* @displayLevel, align 4, !tbaa !2
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end124:                                        ; preds = %if.end120
  %86 = load i8*, i8** %argument, align 4, !tbaa !6
  %call125 = call i32 @strcmp(i8* %86, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.23, i32 0, i32 0))
  %tobool126 = icmp ne i32 %call125, 0
  br i1 %tobool126, label %if.end131, label %if.then127

if.then127:                                       ; preds = %if.end124
  %87 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %tobool128 = icmp ne i32 %87, 0
  br i1 %tobool128, label %if.then129, label %if.end130

if.then129:                                       ; preds = %if.then127
  %88 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %dec = add i32 %88, -1
  store i32 %dec, i32* @displayLevel, align 4, !tbaa !2
  br label %if.end130

if.end130:                                        ; preds = %if.then129, %if.then127
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end131:                                        ; preds = %if.end124
  %89 = load i8*, i8** %argument, align 4, !tbaa !6
  %call132 = call i32 @strcmp(i8* %89, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.24, i32 0, i32 0))
  %tobool133 = icmp ne i32 %call132, 0
  br i1 %tobool133, label %if.end137, label %if.then134

if.then134:                                       ; preds = %if.end131
  %90 = load %struct._IO_FILE*, %struct._IO_FILE** @stdout, align 4, !tbaa !6
  %call135 = call i8* @LZ4_versionString()
  %call136 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %90, i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.25, i32 0, i32 0), i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.26, i32 0, i32 0), i32 32, i8* %call135, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.27, i32 0, i32 0))
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end137:                                        ; preds = %if.end131
  %91 = load i8*, i8** %argument, align 4, !tbaa !6
  %call138 = call i32 @strcmp(i8* %91, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.28, i32 0, i32 0))
  %tobool139 = icmp ne i32 %call138, 0
  br i1 %tobool139, label %if.end142, label %if.then140

if.then140:                                       ; preds = %if.end137
  %92 = load i8*, i8** %exeName, align 4, !tbaa !6
  %call141 = call i32 @usage_advanced(i8* %92)
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end142:                                        ; preds = %if.end137
  %93 = load i8*, i8** %argument, align 4, !tbaa !6
  %call143 = call i32 @strcmp(i8* %93, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.29, i32 0, i32 0))
  %tobool144 = icmp ne i32 %call143, 0
  br i1 %tobool144, label %if.end146, label %if.then145

if.then145:                                       ; preds = %if.end142
  %94 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  call void @LZ4IO_setRemoveSrcFile(%struct.LZ4IO_prefs_s* %94, i32 0)
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end146:                                        ; preds = %if.end142
  %95 = load i8*, i8** %argument, align 4, !tbaa !6
  %call147 = call i32 @strcmp(i8* %95, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.30, i32 0, i32 0))
  %tobool148 = icmp ne i32 %call147, 0
  br i1 %tobool148, label %if.end150, label %if.then149

if.then149:                                       ; preds = %if.end146
  %96 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  call void @LZ4IO_setRemoveSrcFile(%struct.LZ4IO_prefs_s* %96, i32 1)
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end150:                                        ; preds = %if.end146
  %call151 = call i32 @longCommandWArg(i8** %argument, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.31, i32 0, i32 0))
  %tobool152 = icmp ne i32 %call151, 0
  br i1 %tobool152, label %if.then153, label %if.end173

if.then153:                                       ; preds = %if.end150
  %97 = load i8*, i8** %argument, align 4, !tbaa !6
  %98 = load i8, i8* %97, align 1, !tbaa !8
  %conv154 = sext i8 %98 to i32
  %cmp155 = icmp eq i32 %conv154, 61
  br i1 %cmp155, label %if.then157, label %if.else164

if.then157:                                       ; preds = %if.then153
  %99 = bitcast i32* %fastLevel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #4
  %100 = load i8*, i8** %argument, align 4, !tbaa !6
  %incdec.ptr = getelementptr inbounds i8, i8* %100, i32 1
  store i8* %incdec.ptr, i8** %argument, align 4, !tbaa !6
  %call158 = call i32 @readU32FromChar(i8** %argument)
  store i32 %call158, i32* %fastLevel, align 4, !tbaa !2
  %101 = load i32, i32* %fastLevel, align 4, !tbaa !2
  %tobool159 = icmp ne i32 %101, 0
  br i1 %tobool159, label %if.then160, label %if.else161

if.then160:                                       ; preds = %if.then157
  %102 = load i32, i32* %fastLevel, align 4, !tbaa !2
  %sub = sub nsw i32 0, %102
  store i32 %sub, i32* %cLevel, align 4, !tbaa !2
  br label %if.end163

if.else161:                                       ; preds = %if.then157
  %103 = load i8*, i8** %exeName, align 4, !tbaa !6
  %call162 = call i32 @badusage(i8* %103)
  br label %if.end163

if.end163:                                        ; preds = %if.else161, %if.then160
  %104 = bitcast i32* %fastLevel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #4
  br label %if.end172

if.else164:                                       ; preds = %if.then153
  %105 = load i8*, i8** %argument, align 4, !tbaa !6
  %106 = load i8, i8* %105, align 1, !tbaa !8
  %conv165 = sext i8 %106 to i32
  %cmp166 = icmp ne i32 %conv165, 0
  br i1 %cmp166, label %if.then168, label %if.else170

if.then168:                                       ; preds = %if.else164
  %107 = load i8*, i8** %exeName, align 4, !tbaa !6
  %call169 = call i32 @badusage(i8* %107)
  br label %if.end171

if.else170:                                       ; preds = %if.else164
  store i32 -1, i32* %cLevel, align 4, !tbaa !2
  br label %if.end171

if.end171:                                        ; preds = %if.else170, %if.then168
  br label %if.end172

if.end172:                                        ; preds = %if.end171, %if.end163
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end173:                                        ; preds = %if.end150
  %108 = load i8*, i8** %argument, align 4, !tbaa !6
  %call174 = call i32 @strcmp(i8* %108, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.32, i32 0, i32 0))
  %tobool175 = icmp ne i32 %call174, 0
  br i1 %tobool175, label %if.end177, label %if.then176

if.then176:                                       ; preds = %if.end173
  store i32 12, i32* %cLevel, align 4, !tbaa !2
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end177:                                        ; preds = %if.end173
  br label %if.end178

if.end178:                                        ; preds = %if.end177, %if.end38
  br label %while.cond

while.cond:                                       ; preds = %sw.epilog353, %if.then219, %if.then208, %if.then203, %if.then198, %if.then193, %if.then188, %if.end178
  %109 = load i8*, i8** %argument, align 4, !tbaa !6
  %arrayidx179 = getelementptr inbounds i8, i8* %109, i32 1
  %110 = load i8, i8* %arrayidx179, align 1, !tbaa !8
  %conv180 = sext i8 %110 to i32
  %cmp181 = icmp ne i32 %conv180, 0
  br i1 %cmp181, label %while.body, label %while.end354

while.body:                                       ; preds = %while.cond
  %111 = load i8*, i8** %argument, align 4, !tbaa !6
  %incdec.ptr183 = getelementptr inbounds i8, i8* %111, i32 1
  store i8* %incdec.ptr183, i8** %argument, align 4, !tbaa !6
  %112 = load i32, i32* @g_lz4c_legacy_commands, align 4, !tbaa !2
  %tobool184 = icmp ne i32 %112, 0
  br i1 %tobool184, label %if.then185, label %if.end211

if.then185:                                       ; preds = %while.body
  %113 = load i8*, i8** %argument, align 4, !tbaa !6
  %call186 = call i32 @strcmp(i8* %113, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.33, i32 0, i32 0))
  %tobool187 = icmp ne i32 %call186, 0
  br i1 %tobool187, label %if.end190, label %if.then188

if.then188:                                       ; preds = %if.then185
  store i32 0, i32* %cLevel, align 4, !tbaa !2
  %114 = load i8*, i8** %argument, align 4, !tbaa !6
  %incdec.ptr189 = getelementptr inbounds i8, i8* %114, i32 1
  store i8* %incdec.ptr189, i8** %argument, align 4, !tbaa !6
  br label %while.cond

if.end190:                                        ; preds = %if.then185
  %115 = load i8*, i8** %argument, align 4, !tbaa !6
  %call191 = call i32 @strcmp(i8* %115, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.34, i32 0, i32 0))
  %tobool192 = icmp ne i32 %call191, 0
  br i1 %tobool192, label %if.end195, label %if.then193

if.then193:                                       ; preds = %if.end190
  store i32 9, i32* %cLevel, align 4, !tbaa !2
  %116 = load i8*, i8** %argument, align 4, !tbaa !6
  %incdec.ptr194 = getelementptr inbounds i8, i8* %116, i32 1
  store i8* %incdec.ptr194, i8** %argument, align 4, !tbaa !6
  br label %while.cond

if.end195:                                        ; preds = %if.end190
  %117 = load i8*, i8** %argument, align 4, !tbaa !6
  %call196 = call i32 @strcmp(i8* %117, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.35, i32 0, i32 0))
  %tobool197 = icmp ne i32 %call196, 0
  br i1 %tobool197, label %if.end200, label %if.then198

if.then198:                                       ; preds = %if.end195
  store i32 12, i32* %cLevel, align 4, !tbaa !2
  %118 = load i8*, i8** %argument, align 4, !tbaa !6
  %incdec.ptr199 = getelementptr inbounds i8, i8* %118, i32 1
  store i8* %incdec.ptr199, i8** %argument, align 4, !tbaa !6
  br label %while.cond

if.end200:                                        ; preds = %if.end195
  %119 = load i8*, i8** %argument, align 4, !tbaa !6
  %call201 = call i32 @strcmp(i8* %119, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.36, i32 0, i32 0))
  %tobool202 = icmp ne i32 %call201, 0
  br i1 %tobool202, label %if.end205, label %if.then203

if.then203:                                       ; preds = %if.end200
  store i32 12, i32* %cLevel, align 4, !tbaa !2
  %120 = load i8*, i8** %argument, align 4, !tbaa !6
  %incdec.ptr204 = getelementptr inbounds i8, i8* %120, i32 1
  store i8* %incdec.ptr204, i8** %argument, align 4, !tbaa !6
  br label %while.cond

if.end205:                                        ; preds = %if.end200
  %121 = load i8*, i8** %argument, align 4, !tbaa !6
  %call206 = call i32 @strcmp(i8* %121, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.37, i32 0, i32 0))
  %tobool207 = icmp ne i32 %call206, 0
  br i1 %tobool207, label %if.end210, label %if.then208

if.then208:                                       ; preds = %if.end205
  %122 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %call209 = call i32 @LZ4IO_setOverwrite(%struct.LZ4IO_prefs_s* %122, i32 1)
  br label %while.cond

if.end210:                                        ; preds = %if.end205
  br label %if.end211

if.end211:                                        ; preds = %if.end210, %while.body
  %123 = load i8*, i8** %argument, align 4, !tbaa !6
  %124 = load i8, i8* %123, align 1, !tbaa !8
  %conv212 = sext i8 %124 to i32
  %cmp213 = icmp sge i32 %conv212, 48
  br i1 %cmp213, label %land.lhs.true215, label %if.end222

land.lhs.true215:                                 ; preds = %if.end211
  %125 = load i8*, i8** %argument, align 4, !tbaa !6
  %126 = load i8, i8* %125, align 1, !tbaa !8
  %conv216 = sext i8 %126 to i32
  %cmp217 = icmp sle i32 %conv216, 57
  br i1 %cmp217, label %if.then219, label %if.end222

if.then219:                                       ; preds = %land.lhs.true215
  %call220 = call i32 @readU32FromChar(i8** %argument)
  store i32 %call220, i32* %cLevel, align 4, !tbaa !2
  %127 = load i8*, i8** %argument, align 4, !tbaa !6
  %incdec.ptr221 = getelementptr inbounds i8, i8* %127, i32 -1
  store i8* %incdec.ptr221, i8** %argument, align 4, !tbaa !6
  br label %while.cond

if.end222:                                        ; preds = %land.lhs.true215, %if.end211
  %128 = load i8*, i8** %argument, align 4, !tbaa !6
  %arrayidx223 = getelementptr inbounds i8, i8* %128, i32 0
  %129 = load i8, i8* %arrayidx223, align 1, !tbaa !8
  %conv224 = sext i8 %129 to i32
  switch i32 %conv224, label %sw.default351 [
    i32 86, label %sw.bb
    i32 104, label %sw.bb227
    i32 72, label %sw.bb229
    i32 101, label %sw.bb231
    i32 122, label %sw.bb235
    i32 68, label %sw.bb236
    i32 108, label %sw.bb254
    i32 100, label %sw.bb255
    i32 99, label %sw.bb256
    i32 116, label %sw.bb258
    i32 102, label %sw.bb259
    i32 118, label %sw.bb261
    i32 113, label %sw.bb263
    i32 107, label %sw.bb268
    i32 66, label %sw.bb269
    i32 98, label %sw.bb342
    i32 83, label %sw.bb343
    i32 114, label %sw.bb344
    i32 109, label %sw.bb345
    i32 105, label %sw.bb346
    i32 112, label %sw.bb350
  ]

sw.bb:                                            ; preds = %if.end222
  %130 = load %struct._IO_FILE*, %struct._IO_FILE** @stdout, align 4, !tbaa !6
  %call225 = call i8* @LZ4_versionString()
  %call226 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %130, i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.25, i32 0, i32 0), i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.26, i32 0, i32 0), i32 32, i8* %call225, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.27, i32 0, i32 0))
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

sw.bb227:                                         ; preds = %if.end222
  %131 = load i8*, i8** %exeName, align 4, !tbaa !6
  %call228 = call i32 @usage_advanced(i8* %131)
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

sw.bb229:                                         ; preds = %if.end222
  %132 = load i8*, i8** %exeName, align 4, !tbaa !6
  %call230 = call i32 @usage_longhelp(i8* %132)
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

sw.bb231:                                         ; preds = %if.end222
  %133 = load i8*, i8** %argument, align 4, !tbaa !6
  %incdec.ptr232 = getelementptr inbounds i8, i8* %133, i32 1
  store i8* %incdec.ptr232, i8** %argument, align 4, !tbaa !6
  %call233 = call i32 @readU32FromChar(i8** %argument)
  store i32 %call233, i32* %cLevelLast, align 4, !tbaa !2
  %134 = load i8*, i8** %argument, align 4, !tbaa !6
  %incdec.ptr234 = getelementptr inbounds i8, i8* %134, i32 -1
  store i8* %incdec.ptr234, i8** %argument, align 4, !tbaa !6
  br label %sw.epilog353

sw.bb235:                                         ; preds = %if.end222
  store i32 1, i32* %mode, align 4, !tbaa !8
  br label %sw.epilog353

sw.bb236:                                         ; preds = %if.end222
  %135 = load i8*, i8** %argument, align 4, !tbaa !6
  %arrayidx237 = getelementptr inbounds i8, i8* %135, i32 1
  %136 = load i8, i8* %arrayidx237, align 1, !tbaa !8
  %conv238 = sext i8 %136 to i32
  %cmp239 = icmp eq i32 %conv238, 0
  br i1 %cmp239, label %if.then241, label %if.else249

if.then241:                                       ; preds = %sw.bb236
  %137 = load i32, i32* %i, align 4, !tbaa !2
  %add = add nsw i32 %137, 1
  %138 = load i32, i32* %argc.addr, align 4, !tbaa !2
  %cmp242 = icmp eq i32 %add, %138
  br i1 %cmp242, label %if.then244, label %if.end246

if.then244:                                       ; preds = %if.then241
  %139 = load i8*, i8** %exeName, align 4, !tbaa !6
  %call245 = call i32 @badusage(i8* %139)
  br label %if.end246

if.end246:                                        ; preds = %if.then244, %if.then241
  %140 = load i8**, i8*** %argv.addr, align 4, !tbaa !6
  %141 = load i32, i32* %i, align 4, !tbaa !2
  %inc247 = add nsw i32 %141, 1
  store i32 %inc247, i32* %i, align 4, !tbaa !2
  %arrayidx248 = getelementptr inbounds i8*, i8** %140, i32 %inc247
  %142 = load i8*, i8** %arrayidx248, align 4, !tbaa !6
  store i8* %142, i8** %dictionary_filename, align 4, !tbaa !6
  br label %if.end250

if.else249:                                       ; preds = %sw.bb236
  %143 = load i8*, i8** %argument, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %143, i32 1
  store i8* %add.ptr, i8** %dictionary_filename, align 4, !tbaa !6
  br label %if.end250

if.end250:                                        ; preds = %if.else249, %if.end246
  %144 = load i8*, i8** %argument, align 4, !tbaa !6
  %call251 = call i32 @strlen(i8* %144)
  %sub252 = sub i32 %call251, 1
  %145 = load i8*, i8** %argument, align 4, !tbaa !6
  %add.ptr253 = getelementptr inbounds i8, i8* %145, i32 %sub252
  store i8* %add.ptr253, i8** %argument, align 4, !tbaa !6
  br label %sw.epilog353

sw.bb254:                                         ; preds = %if.end222
  store i32 1, i32* %legacy_format, align 4, !tbaa !2
  store i32 8388608, i32* %blockSize, align 4, !tbaa !9
  br label %sw.epilog353

sw.bb255:                                         ; preds = %if.end222
  store i32 2, i32* %mode, align 4, !tbaa !8
  br label %sw.epilog353

sw.bb256:                                         ; preds = %if.end222
  store i32 1, i32* %forceStdout, align 4, !tbaa !2
  store i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0), i8** %output_filename, align 4, !tbaa !6
  %146 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %call257 = call i32 @LZ4IO_setPassThrough(%struct.LZ4IO_prefs_s* %146, i32 1)
  br label %sw.epilog353

sw.bb258:                                         ; preds = %if.end222
  store i32 3, i32* %mode, align 4, !tbaa !8
  br label %sw.epilog353

sw.bb259:                                         ; preds = %if.end222
  %147 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %call260 = call i32 @LZ4IO_setOverwrite(%struct.LZ4IO_prefs_s* %147, i32 1)
  br label %sw.epilog353

sw.bb261:                                         ; preds = %if.end222
  %148 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %inc262 = add i32 %148, 1
  store i32 %inc262, i32* @displayLevel, align 4, !tbaa !2
  br label %sw.epilog353

sw.bb263:                                         ; preds = %if.end222
  %149 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %tobool264 = icmp ne i32 %149, 0
  br i1 %tobool264, label %if.then265, label %if.end267

if.then265:                                       ; preds = %sw.bb263
  %150 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %dec266 = add i32 %150, -1
  store i32 %dec266, i32* @displayLevel, align 4, !tbaa !2
  br label %if.end267

if.end267:                                        ; preds = %if.then265, %sw.bb263
  br label %sw.epilog353

sw.bb268:                                         ; preds = %if.end222
  %151 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  call void @LZ4IO_setRemoveSrcFile(%struct.LZ4IO_prefs_s* %151, i32 0)
  br label %sw.epilog353

sw.bb269:                                         ; preds = %if.end222
  br label %while.cond270

while.cond270:                                    ; preds = %cleanup.cont, %sw.bb269
  %152 = load i8*, i8** %argument, align 4, !tbaa !6
  %arrayidx271 = getelementptr inbounds i8, i8* %152, i32 1
  %153 = load i8, i8* %arrayidx271, align 1, !tbaa !8
  %conv272 = sext i8 %153 to i32
  %cmp273 = icmp ne i32 %conv272, 0
  br i1 %cmp273, label %while.body275, label %while.end

while.body275:                                    ; preds = %while.cond270
  %154 = bitcast i32* %exitBlockProperties to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %154) #4
  store i32 0, i32* %exitBlockProperties, align 4, !tbaa !2
  %155 = load i8*, i8** %argument, align 4, !tbaa !6
  %arrayidx276 = getelementptr inbounds i8, i8* %155, i32 1
  %156 = load i8, i8* %arrayidx276, align 1, !tbaa !8
  %conv277 = sext i8 %156 to i32
  switch i32 %conv277, label %sw.default [
    i32 68, label %sw.bb278
    i32 73, label %sw.bb281
    i32 88, label %sw.bb284
  ]

sw.bb278:                                         ; preds = %while.body275
  %157 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %call279 = call i32 @LZ4IO_setBlockMode(%struct.LZ4IO_prefs_s* %157, i32 0)
  %158 = load i8*, i8** %argument, align 4, !tbaa !6
  %incdec.ptr280 = getelementptr inbounds i8, i8* %158, i32 1
  store i8* %incdec.ptr280, i8** %argument, align 4, !tbaa !6
  br label %sw.epilog

sw.bb281:                                         ; preds = %while.body275
  %159 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %call282 = call i32 @LZ4IO_setBlockMode(%struct.LZ4IO_prefs_s* %159, i32 1)
  %160 = load i8*, i8** %argument, align 4, !tbaa !6
  %incdec.ptr283 = getelementptr inbounds i8, i8* %160, i32 1
  store i8* %incdec.ptr283, i8** %argument, align 4, !tbaa !6
  br label %sw.epilog

sw.bb284:                                         ; preds = %while.body275
  %161 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %call285 = call i32 @LZ4IO_setBlockChecksumMode(%struct.LZ4IO_prefs_s* %161, i32 1)
  %162 = load i8*, i8** %argument, align 4, !tbaa !6
  %incdec.ptr286 = getelementptr inbounds i8, i8* %162, i32 1
  store i8* %incdec.ptr286, i8** %argument, align 4, !tbaa !6
  br label %sw.epilog

sw.default:                                       ; preds = %while.body275
  %163 = load i8*, i8** %argument, align 4, !tbaa !6
  %arrayidx287 = getelementptr inbounds i8, i8* %163, i32 1
  %164 = load i8, i8* %arrayidx287, align 1, !tbaa !8
  %conv288 = sext i8 %164 to i32
  %cmp289 = icmp slt i32 %conv288, 48
  br i1 %cmp289, label %if.then296, label %lor.lhs.false291

lor.lhs.false291:                                 ; preds = %sw.default
  %165 = load i8*, i8** %argument, align 4, !tbaa !6
  %arrayidx292 = getelementptr inbounds i8, i8* %165, i32 1
  %166 = load i8, i8* %arrayidx292, align 1, !tbaa !8
  %conv293 = sext i8 %166 to i32
  %cmp294 = icmp sgt i32 %conv293, 57
  br i1 %cmp294, label %if.then296, label %if.else297

if.then296:                                       ; preds = %lor.lhs.false291, %sw.default
  store i32 1, i32* %exitBlockProperties, align 4, !tbaa !2
  br label %sw.epilog

if.else297:                                       ; preds = %lor.lhs.false291
  %167 = bitcast i32* %B to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %167) #4
  %168 = load i8*, i8** %argument, align 4, !tbaa !6
  %incdec.ptr298 = getelementptr inbounds i8, i8* %168, i32 1
  store i8* %incdec.ptr298, i8** %argument, align 4, !tbaa !6
  %call299 = call i32 @readU32FromChar(i8** %argument)
  store i32 %call299, i32* %B, align 4, !tbaa !2
  %169 = load i8*, i8** %argument, align 4, !tbaa !6
  %incdec.ptr300 = getelementptr inbounds i8, i8* %169, i32 -1
  store i8* %incdec.ptr300, i8** %argument, align 4, !tbaa !6
  %170 = load i32, i32* %B, align 4, !tbaa !2
  %cmp301 = icmp ult i32 %170, 4
  br i1 %cmp301, label %if.then303, label %if.end305

if.then303:                                       ; preds = %if.else297
  %171 = load i8*, i8** %exeName, align 4, !tbaa !6
  %call304 = call i32 @badusage(i8* %171)
  br label %if.end305

if.end305:                                        ; preds = %if.then303, %if.else297
  %172 = load i32, i32* %B, align 4, !tbaa !2
  %cmp306 = icmp ule i32 %172, 7
  br i1 %cmp306, label %if.then308, label %if.else315

if.then308:                                       ; preds = %if.end305
  %173 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %174 = load i32, i32* %B, align 4, !tbaa !2
  %call309 = call i32 @LZ4IO_setBlockSizeID(%struct.LZ4IO_prefs_s* %173, i32 %174)
  store i32 %call309, i32* %blockSize, align 4, !tbaa !9
  %175 = load i32, i32* %blockSize, align 4, !tbaa !9
  call void @BMK_setBlockSize(i32 %175)
  %176 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp310 = icmp uge i32 %176, 2
  br i1 %cmp310, label %if.then312, label %if.end314

if.then312:                                       ; preds = %if.then308
  %177 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %178 = load i32, i32* %blockSize, align 4, !tbaa !9
  %shr = lshr i32 %178, 10
  %call313 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %177, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.38, i32 0, i32 0), i32 %shr)
  br label %if.end314

if.end314:                                        ; preds = %if.then312, %if.then308
  br label %if.end338

if.else315:                                       ; preds = %if.end305
  %179 = load i32, i32* %B, align 4, !tbaa !2
  %cmp316 = icmp ult i32 %179, 32
  br i1 %cmp316, label %if.then318, label %if.end320

if.then318:                                       ; preds = %if.else315
  %180 = load i8*, i8** %exeName, align 4, !tbaa !6
  %call319 = call i32 @badusage(i8* %180)
  br label %if.end320

if.end320:                                        ; preds = %if.then318, %if.else315
  %181 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %182 = load i32, i32* %B, align 4, !tbaa !2
  %call321 = call i32 @LZ4IO_setBlockSize(%struct.LZ4IO_prefs_s* %181, i32 %182)
  store i32 %call321, i32* %blockSize, align 4, !tbaa !9
  %183 = load i32, i32* %blockSize, align 4, !tbaa !9
  call void @BMK_setBlockSize(i32 %183)
  %184 = load i32, i32* %blockSize, align 4, !tbaa !9
  %cmp322 = icmp uge i32 %184, 1024
  br i1 %cmp322, label %if.then324, label %if.else331

if.then324:                                       ; preds = %if.end320
  %185 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp325 = icmp uge i32 %185, 2
  br i1 %cmp325, label %if.then327, label %if.end330

if.then327:                                       ; preds = %if.then324
  %186 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %187 = load i32, i32* %blockSize, align 4, !tbaa !9
  %shr328 = lshr i32 %187, 10
  %call329 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %186, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.38, i32 0, i32 0), i32 %shr328)
  br label %if.end330

if.end330:                                        ; preds = %if.then327, %if.then324
  br label %if.end337

if.else331:                                       ; preds = %if.end320
  %188 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp332 = icmp uge i32 %188, 2
  br i1 %cmp332, label %if.then334, label %if.end336

if.then334:                                       ; preds = %if.else331
  %189 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %190 = load i32, i32* %blockSize, align 4, !tbaa !9
  %call335 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %189, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.39, i32 0, i32 0), i32 %190)
  br label %if.end336

if.end336:                                        ; preds = %if.then334, %if.else331
  br label %if.end337

if.end337:                                        ; preds = %if.end336, %if.end330
  br label %if.end338

if.end338:                                        ; preds = %if.end337, %if.end314
  store i32 11, i32* %cleanup.dest.slot, align 4
  %191 = bitcast i32* %B to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #4
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end338, %if.then296, %sw.bb284, %sw.bb281, %sw.bb278
  %192 = load i32, i32* %exitBlockProperties, align 4, !tbaa !2
  %tobool339 = icmp ne i32 %192, 0
  br i1 %tobool339, label %if.then340, label %if.end341

if.then340:                                       ; preds = %sw.epilog
  store i32 10, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end341:                                        ; preds = %sw.epilog
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end341, %if.then340
  %193 = bitcast i32* %exitBlockProperties to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 10, label %while.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond270

while.end:                                        ; preds = %cleanup, %while.cond270
  br label %sw.epilog353

sw.bb342:                                         ; preds = %if.end222
  store i32 4, i32* %mode, align 4, !tbaa !8
  store i32 1, i32* %multiple_inputs, align 4, !tbaa !2
  br label %sw.epilog353

sw.bb343:                                         ; preds = %if.end222
  call void @BMK_setBenchSeparately(i32 1)
  br label %sw.epilog353

sw.bb344:                                         ; preds = %if.end222
  store i32 1, i32* %recursive, align 4, !tbaa !2
  br label %sw.bb345

sw.bb345:                                         ; preds = %if.end222, %sw.bb344
  store i32 1, i32* %multiple_inputs, align 4, !tbaa !2
  br label %sw.epilog353

sw.bb346:                                         ; preds = %if.end222
  %194 = bitcast i32* %iters to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %194) #4
  %195 = load i8*, i8** %argument, align 4, !tbaa !6
  %incdec.ptr347 = getelementptr inbounds i8, i8* %195, i32 1
  store i8* %incdec.ptr347, i8** %argument, align 4, !tbaa !6
  %call348 = call i32 @readU32FromChar(i8** %argument)
  store i32 %call348, i32* %iters, align 4, !tbaa !2
  %196 = load i8*, i8** %argument, align 4, !tbaa !6
  %incdec.ptr349 = getelementptr inbounds i8, i8* %196, i32 -1
  store i8* %incdec.ptr349, i8** %argument, align 4, !tbaa !6
  %197 = load i32, i32* @displayLevel, align 4, !tbaa !2
  call void @BMK_setNotificationLevel(i32 %197)
  %198 = load i32, i32* %iters, align 4, !tbaa !2
  call void @BMK_setNbSeconds(i32 %198)
  %199 = bitcast i32* %iters to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #4
  br label %sw.epilog353

sw.bb350:                                         ; preds = %if.end222
  store i32 1, i32* %main_pause, align 4, !tbaa !2
  br label %sw.epilog353

sw.default351:                                    ; preds = %if.end222
  %200 = load i8*, i8** %exeName, align 4, !tbaa !6
  %call352 = call i32 @badusage(i8* %200)
  br label %sw.epilog353

sw.epilog353:                                     ; preds = %sw.default351, %sw.bb350, %sw.bb346, %sw.bb345, %sw.bb343, %sw.bb342, %while.end, %sw.bb268, %if.end267, %sw.bb261, %sw.bb259, %sw.bb258, %sw.bb256, %sw.bb255, %sw.bb254, %if.end250, %sw.bb235, %sw.bb231
  br label %while.cond

while.end354:                                     ; preds = %while.cond
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end355:                                        ; preds = %land.lhs.true, %if.end24
  %201 = load i32, i32* %multiple_inputs, align 4, !tbaa !2
  %tobool356 = icmp ne i32 %201, 0
  br i1 %tobool356, label %if.then357, label %if.end360

if.then357:                                       ; preds = %if.end355
  %202 = load i8*, i8** %argument, align 4, !tbaa !6
  %203 = load i8**, i8*** %inFileNames, align 4, !tbaa !6
  %204 = load i32, i32* %ifnIdx, align 4, !tbaa !2
  %inc358 = add i32 %204, 1
  store i32 %inc358, i32* %ifnIdx, align 4, !tbaa !2
  %arrayidx359 = getelementptr inbounds i8*, i8** %203, i32 %204
  store i8* %202, i8** %arrayidx359, align 4, !tbaa !6
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end360:                                        ; preds = %if.end355
  %205 = load i8*, i8** %input_filename, align 4, !tbaa !6
  %tobool361 = icmp ne i8* %205, null
  br i1 %tobool361, label %if.end363, label %if.then362

if.then362:                                       ; preds = %if.end360
  %206 = load i8*, i8** %argument, align 4, !tbaa !6
  store i8* %206, i8** %input_filename, align 4, !tbaa !6
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end363:                                        ; preds = %if.end360
  %207 = load i8*, i8** %output_filename, align 4, !tbaa !6
  %tobool364 = icmp ne i8* %207, null
  br i1 %tobool364, label %if.end370, label %if.then365

if.then365:                                       ; preds = %if.end363
  %208 = load i8*, i8** %argument, align 4, !tbaa !6
  store i8* %208, i8** %output_filename, align 4, !tbaa !6
  %209 = load i8*, i8** %output_filename, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [5 x i8], [5 x i8]* %nullOutput, i32 0, i32 0
  %call366 = call i32 @strcmp(i8* %209, i8* %arraydecay)
  %tobool367 = icmp ne i32 %call366, 0
  br i1 %tobool367, label %if.end369, label %if.then368

if.then368:                                       ; preds = %if.then365
  store i8* getelementptr inbounds ([10 x i8], [10 x i8]* @nulmark, i32 0, i32 0), i8** %output_filename, align 4, !tbaa !6
  br label %if.end369

if.end369:                                        ; preds = %if.then368, %if.then365
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

if.end370:                                        ; preds = %if.end363
  %210 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp371 = icmp uge i32 %210, 1
  br i1 %cmp371, label %if.then373, label %if.end375

if.then373:                                       ; preds = %if.end370
  %211 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %212 = load i8*, i8** %argument, align 4, !tbaa !6
  %call374 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %211, i8* getelementptr inbounds ([71 x i8], [71 x i8]* @.str.40, i32 0, i32 0), i8* %212)
  br label %if.end375

if.end375:                                        ; preds = %if.then373, %if.end370
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup376

cleanup376:                                       ; preds = %sw.bb229, %sw.bb227, %sw.bb, %if.then140, %if.end375, %if.end369, %if.then362, %if.then357, %while.end354, %if.then176, %if.end172, %if.then149, %if.then145, %if.then134, %if.end130, %if.then123, %if.then119, %if.then114, %if.then109, %if.then105, %if.then100, %if.then95, %if.then90, %if.then85, %if.then81, %if.then73, %if.then68, %if.then64, %if.then60, %if.then56, %if.then50, %if.then46, %if.end37, %if.then23
  %213 = bitcast i8** %argument to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #4
  %cleanup.dest377 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest377, label %cleanup722 [
    i32 0, label %cleanup.cont378
    i32 4, label %for.inc
    i32 5, label %_cleanup
  ]

cleanup.cont378:                                  ; preds = %cleanup376
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont378, %cleanup376
  %214 = load i32, i32* %i, align 4, !tbaa !2
  %inc379 = add nsw i32 %214, 1
  store i32 %inc379, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %215 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp380 = icmp uge i32 %215, 3
  br i1 %cmp380, label %if.then382, label %if.end385

if.then382:                                       ; preds = %for.end
  %216 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call383 = call i8* @LZ4_versionString()
  %call384 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %216, i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.25, i32 0, i32 0), i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.26, i32 0, i32 0), i32 32, i8* %call383, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.27, i32 0, i32 0))
  br label %if.end385

if.end385:                                        ; preds = %if.then382, %for.end
  %217 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp386 = icmp uge i32 %217, 4
  br i1 %cmp386, label %if.then388, label %if.end390

if.then388:                                       ; preds = %if.end385
  %218 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call389 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %218, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.41, i32 0, i32 0), i32 200809)
  br label %if.end390

if.end390:                                        ; preds = %if.then388, %if.end385
  %219 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp391 = icmp uge i32 %219, 4
  br i1 %cmp391, label %if.then393, label %if.end395

if.then393:                                       ; preds = %if.end390
  %220 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call394 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %220, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.42, i32 0, i32 0), i32 200809)
  br label %if.end395

if.end395:                                        ; preds = %if.then393, %if.end390
  %221 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp396 = icmp uge i32 %221, 4
  br i1 %cmp396, label %if.then398, label %if.end400

if.then398:                                       ; preds = %if.end395
  %222 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call399 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %222, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.43, i32 0, i32 0), i32 64)
  br label %if.end400

if.end400:                                        ; preds = %if.then398, %if.end395
  %223 = load i32, i32* %mode, align 4, !tbaa !8
  %cmp401 = icmp eq i32 %223, 1
  br i1 %cmp401, label %if.then406, label %lor.lhs.false403

lor.lhs.false403:                                 ; preds = %if.end400
  %224 = load i32, i32* %mode, align 4, !tbaa !8
  %cmp404 = icmp eq i32 %224, 4
  br i1 %cmp404, label %if.then406, label %if.end413

if.then406:                                       ; preds = %lor.lhs.false403, %if.end400
  %225 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp407 = icmp uge i32 %225, 4
  br i1 %cmp407, label %if.then409, label %if.end412

if.then409:                                       ; preds = %if.then406
  %226 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %227 = load i32, i32* %blockSize, align 4, !tbaa !9
  %shr410 = lshr i32 %227, 10
  %call411 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %226, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.44, i32 0, i32 0), i32 %shr410)
  br label %if.end412

if.end412:                                        ; preds = %if.then409, %if.then406
  br label %if.end413

if.end413:                                        ; preds = %if.end412, %lor.lhs.false403
  %228 = load i32, i32* %multiple_inputs, align 4, !tbaa !2
  %tobool414 = icmp ne i32 %228, 0
  br i1 %tobool414, label %if.then415, label %if.end437

if.then415:                                       ; preds = %if.end413
  %229 = load i8**, i8*** %inFileNames, align 4, !tbaa !6
  %arrayidx416 = getelementptr inbounds i8*, i8** %229, i32 0
  %230 = load i8*, i8** %arrayidx416, align 4, !tbaa !6
  store i8* %230, i8** %input_filename, align 4, !tbaa !6
  %231 = load i32, i32* %recursive, align 4, !tbaa !2
  %tobool417 = icmp ne i32 %231, 0
  br i1 %tobool417, label %if.then418, label %if.end436

if.then418:                                       ; preds = %if.then415
  %232 = load i8**, i8*** %inFileNames, align 4, !tbaa !6
  %233 = load i32, i32* %ifnIdx, align 4, !tbaa !2
  %call419 = call i8** @UTIL_createFileList(i8** %232, i32 %233, i8** %fileNamesBuf, i32* %fileNamesNb)
  store i8** %call419, i8*** %extendedFileList, align 4, !tbaa !6
  %234 = load i8**, i8*** %extendedFileList, align 4, !tbaa !6
  %tobool420 = icmp ne i8** %234, null
  br i1 %tobool420, label %if.then421, label %if.end435

if.then421:                                       ; preds = %if.then418
  %235 = bitcast i32* %u to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %235) #4
  store i32 0, i32* %u, align 4, !tbaa !2
  br label %for.cond422

for.cond422:                                      ; preds = %for.inc432, %if.then421
  %236 = load i32, i32* %u, align 4, !tbaa !2
  %237 = load i32, i32* %fileNamesNb, align 4, !tbaa !2
  %cmp423 = icmp ult i32 %236, %237
  br i1 %cmp423, label %for.body425, label %for.end434

for.body425:                                      ; preds = %for.cond422
  %238 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp426 = icmp uge i32 %238, 4
  br i1 %cmp426, label %if.then428, label %if.end431

if.then428:                                       ; preds = %for.body425
  %239 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %240 = load i32, i32* %u, align 4, !tbaa !2
  %241 = load i8**, i8*** %extendedFileList, align 4, !tbaa !6
  %242 = load i32, i32* %u, align 4, !tbaa !2
  %arrayidx429 = getelementptr inbounds i8*, i8** %241, i32 %242
  %243 = load i8*, i8** %arrayidx429, align 4, !tbaa !6
  %call430 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %239, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.45, i32 0, i32 0), i32 %240, i8* %243)
  br label %if.end431

if.end431:                                        ; preds = %if.then428, %for.body425
  br label %for.inc432

for.inc432:                                       ; preds = %if.end431
  %244 = load i32, i32* %u, align 4, !tbaa !2
  %inc433 = add i32 %244, 1
  store i32 %inc433, i32* %u, align 4, !tbaa !2
  br label %for.cond422

for.end434:                                       ; preds = %for.cond422
  %245 = load i8**, i8*** %inFileNames, align 4, !tbaa !6
  %246 = bitcast i8** %245 to i8*
  call void @free(i8* %246)
  %247 = load i8**, i8*** %extendedFileList, align 4, !tbaa !6
  store i8** %247, i8*** %inFileNames, align 4, !tbaa !6
  %248 = load i32, i32* %fileNamesNb, align 4, !tbaa !2
  store i32 %248, i32* %ifnIdx, align 4, !tbaa !2
  %249 = bitcast i32* %u to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %249) #4
  br label %if.end435

if.end435:                                        ; preds = %for.end434, %if.then418
  br label %if.end436

if.end436:                                        ; preds = %if.end435, %if.then415
  br label %if.end437

if.end437:                                        ; preds = %if.end436, %if.end413
  %250 = load i8*, i8** %dictionary_filename, align 4, !tbaa !6
  %tobool438 = icmp ne i8* %250, null
  br i1 %tobool438, label %if.then439, label %if.end454

if.then439:                                       ; preds = %if.end437
  %251 = load i8*, i8** %dictionary_filename, align 4, !tbaa !6
  %call440 = call i32 @strcmp(i8* %251, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @stdinmark, i32 0, i32 0))
  %tobool441 = icmp ne i32 %call440, 0
  br i1 %tobool441, label %if.end452, label %land.lhs.true442

land.lhs.true442:                                 ; preds = %if.then439
  %252 = load %struct._IO_FILE*, %struct._IO_FILE** @stdin, align 4, !tbaa !6
  %call443 = call i32 @fileno(%struct._IO_FILE* %252)
  %call444 = call i32 @isatty(i32 %call443)
  %tobool445 = icmp ne i32 %call444, 0
  br i1 %tobool445, label %if.then446, label %if.end452

if.then446:                                       ; preds = %land.lhs.true442
  %253 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp447 = icmp uge i32 %253, 1
  br i1 %cmp447, label %if.then449, label %if.end451

if.then449:                                       ; preds = %if.then446
  %254 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call450 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %254, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.46, i32 0, i32 0))
  br label %if.end451

if.end451:                                        ; preds = %if.then449, %if.then446
  call void @exit(i32 1) #5
  unreachable

if.end452:                                        ; preds = %land.lhs.true442, %if.then439
  %255 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %256 = load i8*, i8** %dictionary_filename, align 4, !tbaa !6
  %call453 = call i32 @LZ4IO_setDictionaryFilename(%struct.LZ4IO_prefs_s* %255, i8* %256)
  br label %if.end454

if.end454:                                        ; preds = %if.end452, %if.end437
  %257 = load i32, i32* %mode, align 4, !tbaa !8
  %cmp455 = icmp eq i32 %257, 4
  br i1 %cmp455, label %if.then457, label %if.end459

if.then457:                                       ; preds = %if.end454
  %258 = load i32, i32* @displayLevel, align 4, !tbaa !2
  call void @BMK_setNotificationLevel(i32 %258)
  %259 = load i8**, i8*** %inFileNames, align 4, !tbaa !6
  %260 = load i32, i32* %ifnIdx, align 4, !tbaa !2
  %261 = load i32, i32* %cLevel, align 4, !tbaa !2
  %262 = load i32, i32* %cLevelLast, align 4, !tbaa !2
  %263 = load i8*, i8** %dictionary_filename, align 4, !tbaa !6
  %call458 = call i32 @BMK_benchFiles(i8** %259, i32 %260, i32 %261, i32 %262, i8* %263)
  store i32 %call458, i32* %operationResult, align 4, !tbaa !2
  br label %_cleanup

if.end459:                                        ; preds = %if.end454
  %264 = load i32, i32* %mode, align 4, !tbaa !8
  %cmp460 = icmp eq i32 %264, 3
  br i1 %cmp460, label %if.then462, label %if.end464

if.then462:                                       ; preds = %if.end459
  %265 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %call463 = call i32 @LZ4IO_setTestMode(%struct.LZ4IO_prefs_s* %265, i32 1)
  store i8* getelementptr inbounds ([10 x i8], [10 x i8]* @nulmark, i32 0, i32 0), i8** %output_filename, align 4, !tbaa !6
  store i32 2, i32* %mode, align 4, !tbaa !8
  br label %if.end464

if.end464:                                        ; preds = %if.then462, %if.end459
  %266 = load i8*, i8** %input_filename, align 4, !tbaa !6
  %tobool465 = icmp ne i8* %266, null
  br i1 %tobool465, label %if.end467, label %if.then466

if.then466:                                       ; preds = %if.end464
  store i8* getelementptr inbounds ([6 x i8], [6 x i8]* @stdinmark, i32 0, i32 0), i8** %input_filename, align 4, !tbaa !6
  br label %if.end467

if.end467:                                        ; preds = %if.then466, %if.end464
  %267 = load i8*, i8** %input_filename, align 4, !tbaa !6
  %call468 = call i32 @strcmp(i8* %267, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @stdinmark, i32 0, i32 0))
  %tobool469 = icmp ne i32 %call468, 0
  br i1 %tobool469, label %if.end480, label %land.lhs.true470

land.lhs.true470:                                 ; preds = %if.end467
  %268 = load %struct._IO_FILE*, %struct._IO_FILE** @stdin, align 4, !tbaa !6
  %call471 = call i32 @fileno(%struct._IO_FILE* %268)
  %call472 = call i32 @isatty(i32 %call471)
  %tobool473 = icmp ne i32 %call472, 0
  br i1 %tobool473, label %if.then474, label %if.end480

if.then474:                                       ; preds = %land.lhs.true470
  %269 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp475 = icmp uge i32 %269, 1
  br i1 %cmp475, label %if.then477, label %if.end479

if.then477:                                       ; preds = %if.then474
  %270 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call478 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %270, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.46, i32 0, i32 0))
  br label %if.end479

if.end479:                                        ; preds = %if.then477, %if.then474
  call void @exit(i32 1) #5
  unreachable

if.end480:                                        ; preds = %land.lhs.true470, %if.end467
  %271 = load i8*, i8** %input_filename, align 4, !tbaa !6
  %call481 = call i32 @strcmp(i8* %271, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @stdinmark, i32 0, i32 0))
  %tobool482 = icmp ne i32 %call481, 0
  br i1 %tobool482, label %if.else487, label %if.then483

if.then483:                                       ; preds = %if.end480
  %272 = load i8*, i8** %output_filename, align 4, !tbaa !6
  %tobool484 = icmp ne i8* %272, null
  br i1 %tobool484, label %if.end486, label %if.then485

if.then485:                                       ; preds = %if.then483
  store i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0), i8** %output_filename, align 4, !tbaa !6
  br label %if.end486

if.end486:                                        ; preds = %if.then485, %if.then483
  br label %if.end499

if.else487:                                       ; preds = %if.end480
  %273 = load i32, i32* %recursive, align 4, !tbaa !2
  %tobool488 = icmp ne i32 %273, 0
  br i1 %tobool488, label %if.end498, label %land.lhs.true489

land.lhs.true489:                                 ; preds = %if.else487
  %274 = load i8*, i8** %input_filename, align 4, !tbaa !6
  %call490 = call i32 @UTIL_isRegFile(i8* %274)
  %tobool491 = icmp ne i32 %call490, 0
  br i1 %tobool491, label %if.end498, label %if.then492

if.then492:                                       ; preds = %land.lhs.true489
  %275 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp493 = icmp uge i32 %275, 1
  br i1 %cmp493, label %if.then495, label %if.end497

if.then495:                                       ; preds = %if.then492
  %276 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %277 = load i8*, i8** %input_filename, align 4, !tbaa !6
  %call496 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %276, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.47, i32 0, i32 0), i8* %277)
  br label %if.end497

if.end497:                                        ; preds = %if.then495, %if.then492
  call void @exit(i32 1) #5
  unreachable

if.end498:                                        ; preds = %land.lhs.true489, %if.else487
  br label %if.end499

if.end499:                                        ; preds = %if.end498, %if.end486
  br label %while.cond500

while.cond500:                                    ; preds = %if.end499
  %278 = load i8*, i8** %output_filename, align 4, !tbaa !6
  %tobool501 = icmp ne i8* %278, null
  br i1 %tobool501, label %land.end, label %land.rhs

land.rhs:                                         ; preds = %while.cond500
  %279 = load i32, i32* %multiple_inputs, align 4, !tbaa !2
  %cmp502 = icmp eq i32 %279, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond500
  %280 = phi i1 [ false, %while.cond500 ], [ %cmp502, %land.rhs ]
  br i1 %280, label %while.body504, label %while.end592

while.body504:                                    ; preds = %land.end
  %281 = load %struct._IO_FILE*, %struct._IO_FILE** @stdout, align 4, !tbaa !6
  %call505 = call i32 @fileno(%struct._IO_FILE* %281)
  %call506 = call i32 @isatty(i32 %call505)
  %tobool507 = icmp ne i32 %call506, 0
  br i1 %tobool507, label %if.end517, label %land.lhs.true508

land.lhs.true508:                                 ; preds = %while.body504
  %282 = load i32, i32* %mode, align 4, !tbaa !8
  %cmp509 = icmp ne i32 %282, 5
  br i1 %cmp509, label %if.then511, label %if.end517

if.then511:                                       ; preds = %land.lhs.true508
  %283 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp512 = icmp uge i32 %283, 1
  br i1 %cmp512, label %if.then514, label %if.end516

if.then514:                                       ; preds = %if.then511
  %284 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call515 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %284, i8* getelementptr inbounds ([102 x i8], [102 x i8]* @.str.48, i32 0, i32 0))
  br label %if.end516

if.end516:                                        ; preds = %if.then514, %if.then511
  store i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0), i8** %output_filename, align 4, !tbaa !6
  br label %while.end592

if.end517:                                        ; preds = %land.lhs.true508, %while.body504
  %285 = load i32, i32* %mode, align 4, !tbaa !8
  %cmp518 = icmp eq i32 %285, 0
  br i1 %cmp518, label %if.then520, label %if.end522

if.then520:                                       ; preds = %if.end517
  %286 = load i8*, i8** %input_filename, align 4, !tbaa !6
  %call521 = call i32 @determineOpMode(i8* %286)
  store i32 %call521, i32* %mode, align 4, !tbaa !8
  br label %if.end522

if.end522:                                        ; preds = %if.then520, %if.end517
  %287 = load i32, i32* %mode, align 4, !tbaa !8
  %cmp523 = icmp eq i32 %287, 1
  br i1 %cmp523, label %if.then525, label %if.end541

if.then525:                                       ; preds = %if.end522
  %288 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %288) #4
  %289 = load i8*, i8** %input_filename, align 4, !tbaa !6
  %call526 = call i32 @strlen(i8* %289)
  store i32 %call526, i32* %l, align 4, !tbaa !9
  %290 = load i32, i32* %l, align 4, !tbaa !9
  %add527 = add i32 %290, 5
  %call528 = call i8* @calloc(i32 1, i32 %add527)
  store i8* %call528, i8** %dynNameSpace, align 4, !tbaa !6
  %291 = load i8*, i8** %dynNameSpace, align 4, !tbaa !6
  %cmp529 = icmp eq i8* %291, null
  br i1 %cmp529, label %if.then531, label %if.end532

if.then531:                                       ; preds = %if.then525
  %292 = load i8*, i8** %exeName, align 4, !tbaa !6
  call void @perror(i8* %292)
  call void @exit(i32 1) #5
  unreachable

if.end532:                                        ; preds = %if.then525
  %293 = load i8*, i8** %dynNameSpace, align 4, !tbaa !6
  %294 = load i8*, i8** %input_filename, align 4, !tbaa !6
  %call533 = call i8* @strcpy(i8* %293, i8* %294)
  %295 = load i8*, i8** %dynNameSpace, align 4, !tbaa !6
  %call534 = call i8* @strcat(i8* %295, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.49, i32 0, i32 0))
  %296 = load i8*, i8** %dynNameSpace, align 4, !tbaa !6
  store i8* %296, i8** %output_filename, align 4, !tbaa !6
  %297 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp535 = icmp uge i32 %297, 2
  br i1 %cmp535, label %if.then537, label %if.end539

if.then537:                                       ; preds = %if.end532
  %298 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %299 = load i8*, i8** %output_filename, align 4, !tbaa !6
  %call538 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %298, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.50, i32 0, i32 0), i8* %299)
  br label %if.end539

if.end539:                                        ; preds = %if.then537, %if.end532
  store i32 16, i32* %cleanup.dest.slot, align 4
  %300 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %300) #4
  br label %while.end592

if.end541:                                        ; preds = %if.end522
  %301 = load i32, i32* %mode, align 4, !tbaa !8
  %cmp542 = icmp eq i32 %301, 2
  br i1 %cmp542, label %if.then544, label %if.end591

if.then544:                                       ; preds = %if.end541
  %302 = bitcast i32* %outl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %302) #4
  %303 = bitcast i32* %inl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %303) #4
  %304 = load i8*, i8** %input_filename, align 4, !tbaa !6
  %call545 = call i32 @strlen(i8* %304)
  store i32 %call545, i32* %inl, align 4, !tbaa !9
  %305 = load i32, i32* %inl, align 4, !tbaa !9
  %add546 = add i32 %305, 1
  %call547 = call i8* @calloc(i32 1, i32 %add546)
  store i8* %call547, i8** %dynNameSpace, align 4, !tbaa !6
  %306 = load i8*, i8** %dynNameSpace, align 4, !tbaa !6
  %cmp548 = icmp eq i8* %306, null
  br i1 %cmp548, label %if.then550, label %if.end551

if.then550:                                       ; preds = %if.then544
  %307 = load i8*, i8** %exeName, align 4, !tbaa !6
  call void @perror(i8* %307)
  call void @exit(i32 1) #5
  unreachable

if.end551:                                        ; preds = %if.then544
  %308 = load i8*, i8** %dynNameSpace, align 4, !tbaa !6
  %309 = load i8*, i8** %input_filename, align 4, !tbaa !6
  %call552 = call i8* @strcpy(i8* %308, i8* %309)
  %310 = load i32, i32* %inl, align 4, !tbaa !9
  store i32 %310, i32* %outl, align 4, !tbaa !9
  %311 = load i32, i32* %inl, align 4, !tbaa !9
  %cmp553 = icmp ugt i32 %311, 4
  br i1 %cmp553, label %if.then555, label %if.end574

if.then555:                                       ; preds = %if.end551
  br label %while.cond556

while.cond556:                                    ; preds = %while.body570, %if.then555
  %312 = load i32, i32* %outl, align 4, !tbaa !9
  %313 = load i32, i32* %inl, align 4, !tbaa !9
  %sub557 = sub i32 %313, 4
  %cmp558 = icmp uge i32 %312, %sub557
  br i1 %cmp558, label %land.rhs560, label %land.end569

land.rhs560:                                      ; preds = %while.cond556
  %314 = load i8*, i8** %input_filename, align 4, !tbaa !6
  %315 = load i32, i32* %outl, align 4, !tbaa !9
  %arrayidx561 = getelementptr inbounds i8, i8* %314, i32 %315
  %316 = load i8, i8* %arrayidx561, align 1, !tbaa !8
  %conv562 = sext i8 %316 to i32
  %317 = load i32, i32* %outl, align 4, !tbaa !9
  %318 = load i32, i32* %inl, align 4, !tbaa !9
  %sub563 = sub i32 %317, %318
  %add564 = add i32 %sub563, 4
  %arrayidx565 = getelementptr inbounds [5 x i8], [5 x i8]* %extension, i32 0, i32 %add564
  %319 = load i8, i8* %arrayidx565, align 1, !tbaa !8
  %conv566 = sext i8 %319 to i32
  %cmp567 = icmp eq i32 %conv562, %conv566
  br label %land.end569

land.end569:                                      ; preds = %land.rhs560, %while.cond556
  %320 = phi i1 [ false, %while.cond556 ], [ %cmp567, %land.rhs560 ]
  br i1 %320, label %while.body570, label %while.end573

while.body570:                                    ; preds = %land.end569
  %321 = load i8*, i8** %dynNameSpace, align 4, !tbaa !6
  %322 = load i32, i32* %outl, align 4, !tbaa !9
  %dec571 = add i32 %322, -1
  store i32 %dec571, i32* %outl, align 4, !tbaa !9
  %arrayidx572 = getelementptr inbounds i8, i8* %321, i32 %322
  store i8 0, i8* %arrayidx572, align 1, !tbaa !8
  br label %while.cond556

while.end573:                                     ; preds = %land.end569
  br label %if.end574

if.end574:                                        ; preds = %while.end573, %if.end551
  %323 = load i32, i32* %outl, align 4, !tbaa !9
  %324 = load i32, i32* %inl, align 4, !tbaa !9
  %sub575 = sub i32 %324, 5
  %cmp576 = icmp ne i32 %323, %sub575
  br i1 %cmp576, label %if.then578, label %if.end585

if.then578:                                       ; preds = %if.end574
  %325 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp579 = icmp uge i32 %325, 1
  br i1 %cmp579, label %if.then581, label %if.end583

if.then581:                                       ; preds = %if.then578
  %326 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call582 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %326, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.51, i32 0, i32 0))
  br label %if.end583

if.end583:                                        ; preds = %if.then581, %if.then578
  %327 = load i8*, i8** %exeName, align 4, !tbaa !6
  %call584 = call i32 @badusage(i8* %327)
  br label %if.end585

if.end585:                                        ; preds = %if.end583, %if.end574
  %328 = load i8*, i8** %dynNameSpace, align 4, !tbaa !6
  store i8* %328, i8** %output_filename, align 4, !tbaa !6
  %329 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp586 = icmp uge i32 %329, 2
  br i1 %cmp586, label %if.then588, label %if.end590

if.then588:                                       ; preds = %if.end585
  %330 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %331 = load i8*, i8** %output_filename, align 4, !tbaa !6
  %call589 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %330, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.52, i32 0, i32 0), i8* %331)
  br label %if.end590

if.end590:                                        ; preds = %if.then588, %if.end585
  %332 = bitcast i32* %inl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %332) #4
  %333 = bitcast i32* %outl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %333) #4
  br label %if.end591

if.end591:                                        ; preds = %if.end590, %if.end541
  br label %while.end592

while.end592:                                     ; preds = %if.end591, %if.end539, %if.end516, %land.end
  %334 = load i32, i32* %mode, align 4, !tbaa !8
  %cmp593 = icmp eq i32 %334, 5
  br i1 %cmp593, label %if.then595, label %if.else610

if.then595:                                       ; preds = %while.end592
  %335 = load i8*, i8** %input_filename, align 4, !tbaa !6
  %call596 = call i32 @strcmp(i8* %335, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @stdinmark, i32 0, i32 0))
  %tobool597 = icmp ne i32 %call596, 0
  br i1 %tobool597, label %if.end604, label %if.then598

if.then598:                                       ; preds = %if.then595
  %336 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp599 = icmp uge i32 %336, 1
  br i1 %cmp599, label %if.then601, label %if.end603

if.then601:                                       ; preds = %if.then598
  %337 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call602 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %337, i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.53, i32 0, i32 0))
  br label %if.end603

if.end603:                                        ; preds = %if.then601, %if.then598
  call void @exit(i32 1) #5
  unreachable

if.end604:                                        ; preds = %if.then595
  %338 = load i32, i32* %multiple_inputs, align 4, !tbaa !2
  %tobool605 = icmp ne i32 %338, 0
  br i1 %tobool605, label %if.end609, label %if.then606

if.then606:                                       ; preds = %if.end604
  %339 = load i8*, i8** %input_filename, align 4, !tbaa !6
  %340 = load i8**, i8*** %inFileNames, align 4, !tbaa !6
  %341 = load i32, i32* %ifnIdx, align 4, !tbaa !2
  %inc607 = add i32 %341, 1
  store i32 %inc607, i32* %ifnIdx, align 4, !tbaa !2
  %arrayidx608 = getelementptr inbounds i8*, i8** %340, i32 %341
  store i8* %339, i8** %arrayidx608, align 4, !tbaa !6
  br label %if.end609

if.end609:                                        ; preds = %if.then606, %if.end604
  br label %if.end616

if.else610:                                       ; preds = %while.end592
  %342 = load i32, i32* %multiple_inputs, align 4, !tbaa !2
  %cmp611 = icmp eq i32 %342, 0
  br i1 %cmp611, label %if.then613, label %if.end615

if.then613:                                       ; preds = %if.else610
  %343 = load i8*, i8** %output_filename, align 4, !tbaa !6
  %tobool614 = icmp ne i8* %343, null
  br i1 %tobool614, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.then613
  call void @__assert_fail(i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.54, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.55, i32 0, i32 0), i32 725, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @__func__.main, i32 0, i32 0)) #5
  unreachable

344:                                              ; No predecessors!
  br label %lor.end

lor.end:                                          ; preds = %344, %if.then613
  %345 = phi i1 [ true, %if.then613 ], [ false, %344 ]
  %lor.ext = zext i1 %345 to i32
  br label %if.end615

if.end615:                                        ; preds = %lor.end, %if.else610
  br label %if.end616

if.end616:                                        ; preds = %if.end615, %if.end609
  %346 = load i8*, i8** %output_filename, align 4, !tbaa !6
  %tobool617 = icmp ne i8* %346, null
  br i1 %tobool617, label %if.end619, label %if.then618

if.then618:                                       ; preds = %if.end616
  store i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.56, i32 0, i32 0), i8** %output_filename, align 4, !tbaa !6
  br label %if.end619

if.end619:                                        ; preds = %if.then618, %if.end616
  %347 = load i8*, i8** %output_filename, align 4, !tbaa !6
  %call620 = call i32 @strcmp(i8* %347, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0))
  %tobool621 = icmp ne i32 %call620, 0
  br i1 %tobool621, label %if.end634, label %land.lhs.true622

land.lhs.true622:                                 ; preds = %if.end619
  %348 = load %struct._IO_FILE*, %struct._IO_FILE** @stdout, align 4, !tbaa !6
  %call623 = call i32 @fileno(%struct._IO_FILE* %348)
  %call624 = call i32 @isatty(i32 %call623)
  %tobool625 = icmp ne i32 %call624, 0
  br i1 %tobool625, label %land.lhs.true626, label %if.end634

land.lhs.true626:                                 ; preds = %land.lhs.true622
  %349 = load i32, i32* %forceStdout, align 4, !tbaa !2
  %tobool627 = icmp ne i32 %349, 0
  br i1 %tobool627, label %if.end634, label %if.then628

if.then628:                                       ; preds = %land.lhs.true626
  %350 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp629 = icmp uge i32 %350, 1
  br i1 %cmp629, label %if.then631, label %if.end633

if.then631:                                       ; preds = %if.then628
  %351 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call632 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %351, i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.57, i32 0, i32 0))
  br label %if.end633

if.end633:                                        ; preds = %if.then631, %if.then628
  call void @exit(i32 1) #5
  unreachable

if.end634:                                        ; preds = %land.lhs.true626, %land.lhs.true622, %if.end619
  %352 = load i8*, i8** %output_filename, align 4, !tbaa !6
  %call635 = call i32 @strcmp(i8* %352, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0))
  %tobool636 = icmp ne i32 %call635, 0
  br i1 %tobool636, label %if.end641, label %land.lhs.true637

land.lhs.true637:                                 ; preds = %if.end634
  %353 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp638 = icmp eq i32 %353, 2
  br i1 %cmp638, label %if.then640, label %if.end641

if.then640:                                       ; preds = %land.lhs.true637
  store i32 1, i32* @displayLevel, align 4, !tbaa !2
  br label %if.end641

if.end641:                                        ; preds = %if.then640, %land.lhs.true637, %if.end634
  %354 = load i32, i32* %multiple_inputs, align 4, !tbaa !2
  %tobool642 = icmp ne i32 %354, 0
  br i1 %tobool642, label %land.lhs.true643, label %if.end647

land.lhs.true643:                                 ; preds = %if.end641
  %355 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp644 = icmp eq i32 %355, 2
  br i1 %cmp644, label %if.then646, label %if.end647

if.then646:                                       ; preds = %land.lhs.true643
  store i32 1, i32* @displayLevel, align 4, !tbaa !2
  br label %if.end647

if.end647:                                        ; preds = %if.then646, %land.lhs.true643, %if.end641
  %356 = load i32, i32* %mode, align 4, !tbaa !8
  %cmp648 = icmp eq i32 %356, 0
  br i1 %cmp648, label %if.then650, label %if.end652

if.then650:                                       ; preds = %if.end647
  %357 = load i8*, i8** %input_filename, align 4, !tbaa !6
  %call651 = call i32 @determineOpMode(i8* %357)
  store i32 %call651, i32* %mode, align 4, !tbaa !8
  br label %if.end652

if.end652:                                        ; preds = %if.then650, %if.end647
  %358 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %call653 = call i32 @LZ4IO_setNotificationLevel(i32 %358)
  %359 = load i32, i32* %ifnIdx, align 4, !tbaa !2
  %cmp654 = icmp eq i32 %359, 0
  br i1 %cmp654, label %if.then656, label %if.end657

if.then656:                                       ; preds = %if.end652
  store i32 0, i32* %multiple_inputs, align 4, !tbaa !2
  br label %if.end657

if.end657:                                        ; preds = %if.then656, %if.end652
  %360 = load i32, i32* %mode, align 4, !tbaa !8
  %cmp658 = icmp eq i32 %360, 2
  br i1 %cmp658, label %if.then660, label %if.else674

if.then660:                                       ; preds = %if.end657
  %361 = load i32, i32* %multiple_inputs, align 4, !tbaa !2
  %tobool661 = icmp ne i32 %361, 0
  br i1 %tobool661, label %if.then662, label %if.else671

if.then662:                                       ; preds = %if.then660
  %362 = load i32, i32* %ifnIdx, align 4, !tbaa !2
  %cmp663 = icmp ule i32 %362, 2147483647
  br i1 %cmp663, label %lor.end666, label %lor.rhs665

lor.rhs665:                                       ; preds = %if.then662
  call void @__assert_fail(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.58, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.55, i32 0, i32 0), i32 750, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @__func__.main, i32 0, i32 0)) #5
  unreachable

363:                                              ; No predecessors!
  br label %lor.end666

lor.end666:                                       ; preds = %363, %if.then662
  %364 = phi i1 [ true, %if.then662 ], [ false, %363 ]
  %lor.ext667 = zext i1 %364 to i32
  %365 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %366 = load i8**, i8*** %inFileNames, align 4, !tbaa !6
  %367 = load i32, i32* %ifnIdx, align 4, !tbaa !2
  %368 = load i8*, i8** %output_filename, align 4, !tbaa !6
  %call668 = call i32 @strcmp(i8* %368, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0))
  %tobool669 = icmp ne i32 %call668, 0
  %lnot = xor i1 %tobool669, true
  %369 = zext i1 %lnot to i64
  %cond = select i1 %lnot, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.49, i32 0, i32 0)
  %call670 = call i32 @LZ4IO_decompressMultipleFilenames(%struct.LZ4IO_prefs_s* %365, i8** %366, i32 %367, i8* %cond)
  store i32 %call670, i32* %operationResult, align 4, !tbaa !2
  br label %if.end673

if.else671:                                       ; preds = %if.then660
  %370 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %371 = load i8*, i8** %input_filename, align 4, !tbaa !6
  %372 = load i8*, i8** %output_filename, align 4, !tbaa !6
  %call672 = call i32 @LZ4IO_decompressFilename(%struct.LZ4IO_prefs_s* %370, i8* %371, i8* %372)
  store i32 %call672, i32* %operationResult, align 4, !tbaa !2
  br label %if.end673

if.end673:                                        ; preds = %if.else671, %lor.end666
  br label %if.end715

if.else674:                                       ; preds = %if.end657
  %373 = load i32, i32* %mode, align 4, !tbaa !8
  %cmp675 = icmp eq i32 %373, 5
  br i1 %cmp675, label %if.then677, label %if.else679

if.then677:                                       ; preds = %if.else674
  %374 = load i8**, i8*** %inFileNames, align 4, !tbaa !6
  %375 = load i32, i32* %ifnIdx, align 4, !tbaa !2
  %call678 = call i32 @LZ4IO_displayCompressedFilesInfo(i8** %374, i32 %375)
  store i32 %call678, i32* %operationResult, align 4, !tbaa !2
  br label %if.end714

if.else679:                                       ; preds = %if.else674
  %376 = load i32, i32* %legacy_format, align 4, !tbaa !2
  %tobool680 = icmp ne i32 %376, 0
  br i1 %tobool680, label %if.then681, label %if.else697

if.then681:                                       ; preds = %if.else679
  %377 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp682 = icmp uge i32 %377, 3
  br i1 %cmp682, label %if.then684, label %if.end686

if.then684:                                       ; preds = %if.then681
  %378 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call685 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %378, i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.59, i32 0, i32 0))
  br label %if.end686

if.end686:                                        ; preds = %if.then684, %if.then681
  %379 = load i32, i32* %multiple_inputs, align 4, !tbaa !2
  %tobool687 = icmp ne i32 %379, 0
  br i1 %tobool687, label %if.then688, label %if.else694

if.then688:                                       ; preds = %if.end686
  %380 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %381 = load i8**, i8*** %inFileNames, align 4, !tbaa !6
  %382 = load i32, i32* %ifnIdx, align 4, !tbaa !2
  %383 = load i8*, i8** %output_filename, align 4, !tbaa !6
  %call689 = call i32 @strcmp(i8* %383, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0))
  %tobool690 = icmp ne i32 %call689, 0
  %lnot691 = xor i1 %tobool690, true
  %384 = zext i1 %lnot691 to i64
  %cond692 = select i1 %lnot691, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.49, i32 0, i32 0)
  %385 = load i32, i32* %cLevel, align 4, !tbaa !2
  %call693 = call i32 @LZ4IO_compressMultipleFilenames_Legacy(%struct.LZ4IO_prefs_s* %380, i8** %381, i32 %382, i8* %cond692, i32 %385)
  br label %if.end696

if.else694:                                       ; preds = %if.end686
  %386 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %387 = load i8*, i8** %input_filename, align 4, !tbaa !6
  %388 = load i8*, i8** %output_filename, align 4, !tbaa !6
  %389 = load i32, i32* %cLevel, align 4, !tbaa !2
  %call695 = call i32 @LZ4IO_compressFilename_Legacy(%struct.LZ4IO_prefs_s* %386, i8* %387, i8* %388, i32 %389)
  br label %if.end696

if.end696:                                        ; preds = %if.else694, %if.then688
  br label %if.end713

if.else697:                                       ; preds = %if.else679
  %390 = load i32, i32* %multiple_inputs, align 4, !tbaa !2
  %tobool698 = icmp ne i32 %390, 0
  br i1 %tobool698, label %if.then699, label %if.else710

if.then699:                                       ; preds = %if.else697
  %391 = load i32, i32* %ifnIdx, align 4, !tbaa !2
  %cmp700 = icmp ule i32 %391, 2147483647
  br i1 %cmp700, label %lor.end703, label %lor.rhs702

lor.rhs702:                                       ; preds = %if.then699
  call void @__assert_fail(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.58, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.55, i32 0, i32 0), i32 767, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @__func__.main, i32 0, i32 0)) #5
  unreachable

392:                                              ; No predecessors!
  br label %lor.end703

lor.end703:                                       ; preds = %392, %if.then699
  %393 = phi i1 [ true, %if.then699 ], [ false, %392 ]
  %lor.ext704 = zext i1 %393 to i32
  %394 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %395 = load i8**, i8*** %inFileNames, align 4, !tbaa !6
  %396 = load i32, i32* %ifnIdx, align 4, !tbaa !2
  %397 = load i8*, i8** %output_filename, align 4, !tbaa !6
  %call705 = call i32 @strcmp(i8* %397, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0))
  %tobool706 = icmp ne i32 %call705, 0
  %lnot707 = xor i1 %tobool706, true
  %398 = zext i1 %lnot707 to i64
  %cond708 = select i1 %lnot707, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.49, i32 0, i32 0)
  %399 = load i32, i32* %cLevel, align 4, !tbaa !2
  %call709 = call i32 @LZ4IO_compressMultipleFilenames(%struct.LZ4IO_prefs_s* %394, i8** %395, i32 %396, i8* %cond708, i32 %399)
  store i32 %call709, i32* %operationResult, align 4, !tbaa !2
  br label %if.end712

if.else710:                                       ; preds = %if.else697
  %400 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  %401 = load i8*, i8** %input_filename, align 4, !tbaa !6
  %402 = load i8*, i8** %output_filename, align 4, !tbaa !6
  %403 = load i32, i32* %cLevel, align 4, !tbaa !2
  %call711 = call i32 @LZ4IO_compressFilename(%struct.LZ4IO_prefs_s* %400, i8* %401, i8* %402, i32 %403)
  store i32 %call711, i32* %operationResult, align 4, !tbaa !2
  br label %if.end712

if.end712:                                        ; preds = %if.else710, %lor.end703
  br label %if.end713

if.end713:                                        ; preds = %if.end712, %if.end696
  br label %if.end714

if.end714:                                        ; preds = %if.end713, %if.then677
  br label %if.end715

if.end715:                                        ; preds = %if.end714, %if.end673
  br label %_cleanup

_cleanup:                                         ; preds = %if.end715, %cleanup376, %if.then457
  %404 = load i32, i32* %main_pause, align 4, !tbaa !2
  %tobool716 = icmp ne i32 %404, 0
  br i1 %tobool716, label %if.then717, label %if.end718

if.then717:                                       ; preds = %_cleanup
  call void @waitEnter()
  br label %if.end718

if.end718:                                        ; preds = %if.then717, %_cleanup
  %405 = load i8*, i8** %dynNameSpace, align 4, !tbaa !6
  call void @free(i8* %405)
  %406 = load i8**, i8*** %extendedFileList, align 4, !tbaa !6
  %tobool719 = icmp ne i8** %406, null
  br i1 %tobool719, label %if.then720, label %if.end721

if.then720:                                       ; preds = %if.end718
  %407 = load i8**, i8*** %extendedFileList, align 4, !tbaa !6
  %408 = load i8*, i8** %fileNamesBuf, align 4, !tbaa !6
  call void @UTIL_freeFileList(i8** %407, i8* %408)
  store i8** null, i8*** %inFileNames, align 4, !tbaa !6
  br label %if.end721

if.end721:                                        ; preds = %if.then720, %if.end718
  %409 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs, align 4, !tbaa !6
  call void @LZ4IO_freePreferences(%struct.LZ4IO_prefs_s* %409)
  %410 = load i8**, i8*** %inFileNames, align 4, !tbaa !6
  %411 = bitcast i8** %410 to i8*
  call void @free(i8* %411)
  %412 = load i32, i32* %operationResult, align 4, !tbaa !2
  store i32 %412, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup722

cleanup722:                                       ; preds = %if.end721, %cleanup376, %if.then
  %413 = bitcast i32* %recursive to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %413) #4
  %414 = bitcast i32* %fileNamesNb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %414) #4
  %415 = bitcast i8** %fileNamesBuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %415) #4
  %416 = bitcast i8*** %extendedFileList to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %416) #4
  %417 = bitcast i8** %exeName to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %417) #4
  %418 = bitcast i32* %blockSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %418) #4
  %419 = bitcast [5 x i8]* %extension to i8*
  call void @llvm.lifetime.end.p0i8(i64 5, i8* %419) #4
  %420 = bitcast [5 x i8]* %nullOutput to i8*
  call void @llvm.lifetime.end.p0i8(i64 5, i8* %420) #4
  %421 = bitcast %struct.LZ4IO_prefs_s** %prefs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %421) #4
  %422 = bitcast i32* %ifnIdx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %422) #4
  %423 = bitcast i8*** %inFileNames to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %423) #4
  %424 = bitcast i8** %dynNameSpace to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %424) #4
  %425 = bitcast i8** %dictionary_filename to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %425) #4
  %426 = bitcast i8** %output_filename to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %426) #4
  %427 = bitcast i8** %input_filename to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %427) #4
  %428 = bitcast i32* %mode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %428) #4
  %429 = bitcast i32* %operationResult to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %429) #4
  %430 = bitcast i32* %all_arguments_are_files to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %430) #4
  %431 = bitcast i32* %multiple_inputs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %431) #4
  %432 = bitcast i32* %main_pause to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %432) #4
  %433 = bitcast i32* %forceStdout to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %433) #4
  %434 = bitcast i32* %legacy_format to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %434) #4
  %435 = bitcast i32* %cLevelLast to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %435) #4
  %436 = bitcast i32* %cLevel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %436) #4
  %437 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %437) #4
  %438 = load i32, i32* %retval, align 4
  ret i32 %438

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare i8* @calloc(i32, i32) #2

declare %struct.LZ4IO_prefs_s* @LZ4IO_defaultPreferences() #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

declare i32 @LZ4IO_setBlockSizeID(%struct.LZ4IO_prefs_s*, i32) #2

; Function Attrs: nounwind
define internal i8* @lastNameFromPath(i8* %path) #0 {
entry:
  %path.addr = alloca i8*, align 4
  %name = alloca i8*, align 4
  store i8* %path, i8** %path.addr, align 4, !tbaa !6
  %0 = bitcast i8** %name to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %path.addr, align 4, !tbaa !6
  store i8* %1, i8** %name, align 4, !tbaa !6
  %2 = load i8*, i8** %name, align 4, !tbaa !6
  %call = call i8* @strrchr(i8* %2, i32 47)
  %tobool = icmp ne i8* %call, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i8*, i8** %name, align 4, !tbaa !6
  %call1 = call i8* @strrchr(i8* %3, i32 47)
  %add.ptr = getelementptr inbounds i8, i8* %call1, i32 1
  store i8* %add.ptr, i8** %name, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load i8*, i8** %name, align 4, !tbaa !6
  %call2 = call i8* @strrchr(i8* %4, i32 92)
  %tobool3 = icmp ne i8* %call2, null
  br i1 %tobool3, label %if.then4, label %if.end7

if.then4:                                         ; preds = %if.end
  %5 = load i8*, i8** %name, align 4, !tbaa !6
  %call5 = call i8* @strrchr(i8* %5, i32 92)
  %add.ptr6 = getelementptr inbounds i8, i8* %call5, i32 1
  store i8* %add.ptr6, i8** %name, align 4, !tbaa !6
  br label %if.end7

if.end7:                                          ; preds = %if.then4, %if.end
  %6 = load i8*, i8** %name, align 4, !tbaa !6
  %7 = bitcast i8** %name to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  ret i8* %6
}

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #2

declare i32 @LZ4IO_setOverwrite(%struct.LZ4IO_prefs_s*, i32) #2

; Function Attrs: nounwind
define internal i32 @exeNameMatch(i8* %exeName, i8* %test) #0 {
entry:
  %exeName.addr = alloca i8*, align 4
  %test.addr = alloca i8*, align 4
  store i8* %exeName, i8** %exeName.addr, align 4, !tbaa !6
  store i8* %test, i8** %test.addr, align 4, !tbaa !6
  %0 = load i8*, i8** %exeName.addr, align 4, !tbaa !6
  %1 = load i8*, i8** %test.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %test.addr, align 4, !tbaa !6
  %call = call i32 @strlen(i8* %2)
  %call1 = call i32 @strncmp(i8* %0, i8* %1, i32 %call)
  %tobool = icmp ne i32 %call1, 0
  br i1 %tobool, label %land.end, label %land.rhs

land.rhs:                                         ; preds = %entry
  %3 = load i8*, i8** %exeName.addr, align 4, !tbaa !6
  %4 = load i8*, i8** %test.addr, align 4, !tbaa !6
  %call2 = call i32 @strlen(i8* %4)
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 %call2
  %5 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = sext i8 %5 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %land.rhs
  %6 = load i8*, i8** %exeName.addr, align 4, !tbaa !6
  %7 = load i8*, i8** %test.addr, align 4, !tbaa !6
  %call4 = call i32 @strlen(i8* %7)
  %arrayidx5 = getelementptr inbounds i8, i8* %6, i32 %call4
  %8 = load i8, i8* %arrayidx5, align 1, !tbaa !8
  %conv6 = sext i8 %8 to i32
  %cmp7 = icmp eq i32 %conv6, 46
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %land.rhs
  %9 = phi i1 [ true, %land.rhs ], [ %cmp7, %lor.rhs ]
  br label %land.end

land.end:                                         ; preds = %lor.end, %entry
  %10 = phi i1 [ false, %entry ], [ %9, %lor.end ]
  %land.ext = zext i1 %10 to i32
  ret i32 %land.ext
}

declare i32 @LZ4IO_setPassThrough(%struct.LZ4IO_prefs_s*, i32) #2

declare void @LZ4IO_setRemoveSrcFile(%struct.LZ4IO_prefs_s*, i32) #2

declare i32 @strcmp(i8*, i8*) #2

declare i32 @LZ4IO_setStreamChecksumMode(%struct.LZ4IO_prefs_s*, i32) #2

declare i32 @LZ4IO_setContentSize(%struct.LZ4IO_prefs_s*, i32) #2

declare i32 @LZ4IO_setSparseFile(%struct.LZ4IO_prefs_s*, i32) #2

declare void @LZ4IO_favorDecSpeed(%struct.LZ4IO_prefs_s*, i32) #2

declare i8* @LZ4_versionString() #2

; Function Attrs: nounwind
define internal i32 @usage_advanced(i8* %exeName) #0 {
entry:
  %exeName.addr = alloca i8*, align 4
  store i8* %exeName, i8** %exeName.addr, align 4, !tbaa !6
  %0 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call = call i8* @LZ4_versionString()
  %call1 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %0, i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.25, i32 0, i32 0), i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.26, i32 0, i32 0), i32 32, i8* %call, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.27, i32 0, i32 0))
  %1 = load i8*, i8** %exeName.addr, align 4, !tbaa !6
  %call2 = call i32 @usage(i8* %1)
  %2 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call3 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %2, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.60, i32 0, i32 0))
  %3 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call4 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %3, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.61, i32 0, i32 0))
  %4 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call5 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %4, i8* getelementptr inbounds ([44 x i8], [44 x i8]* @.str.62, i32 0, i32 0))
  %5 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call6 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %5, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.63, i32 0, i32 0))
  %6 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call7 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %6, i8* getelementptr inbounds ([67 x i8], [67 x i8]* @.str.64, i32 0, i32 0))
  %7 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call8 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %7, i8* getelementptr inbounds ([69 x i8], [69 x i8]* @.str.65, i32 0, i32 0))
  %8 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call9 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %8, i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.66, i32 0, i32 0))
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call10 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %9, i8* getelementptr inbounds ([69 x i8], [69 x i8]* @.str.67, i32 0, i32 0))
  %10 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call11 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %10, i8* getelementptr inbounds ([62 x i8], [62 x i8]* @.str.68, i32 0, i32 0))
  %11 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call12 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %11, i8* getelementptr inbounds ([67 x i8], [67 x i8]* @.str.69, i32 0, i32 0))
  %12 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call13 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %12, i8* getelementptr inbounds ([55 x i8], [55 x i8]* @.str.70, i32 0, i32 0))
  %13 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call14 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %13, i8* getelementptr inbounds ([67 x i8], [67 x i8]* @.str.71, i32 0, i32 0))
  %14 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call15 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %14, i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.72, i32 0, i32 0))
  %15 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call16 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %15, i8* getelementptr inbounds ([58 x i8], [58 x i8]* @.str.73, i32 0, i32 0))
  %16 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call17 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %16, i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.74, i32 0, i32 0))
  %17 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call18 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %17, i8* getelementptr inbounds ([61 x i8], [61 x i8]* @.str.75, i32 0, i32 0))
  %18 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call19 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %18, i8* getelementptr inbounds ([80 x i8], [80 x i8]* @.str.76, i32 0, i32 0))
  %19 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call20 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %19, i8* getelementptr inbounds ([105 x i8], [105 x i8]* @.str.77, i32 0, i32 0))
  %20 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call21 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %20, i8* getelementptr inbounds ([76 x i8], [76 x i8]* @.str.78, i32 0, i32 0))
  %21 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call22 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %21, i8* getelementptr inbounds ([80 x i8], [80 x i8]* @.str.79, i32 0, i32 0))
  %22 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call23 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %22, i8* getelementptr inbounds ([66 x i8], [66 x i8]* @.str.80, i32 0, i32 0), i32 1)
  %23 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call24 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %23, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.81, i32 0, i32 0), i32 12)
  %24 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call25 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %24, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.82, i32 0, i32 0))
  %25 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call26 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %25, i8* getelementptr inbounds ([71 x i8], [71 x i8]* @.str.83, i32 0, i32 0))
  %26 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call27 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %26, i8* getelementptr inbounds ([67 x i8], [67 x i8]* @.str.84, i32 0, i32 0))
  %27 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call28 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %27, i8* getelementptr inbounds ([62 x i8], [62 x i8]* @.str.85, i32 0, i32 0))
  %28 = load i32, i32* @g_lz4c_legacy_commands, align 4, !tbaa !2
  %tobool = icmp ne i32 %28, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %29 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call29 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %29, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.86, i32 0, i32 0))
  %30 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call30 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %30, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.87, i32 0, i32 0))
  %31 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call31 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %31, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.88, i32 0, i32 0))
  %32 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call32 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %32, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.89, i32 0, i32 0))
  %33 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call33 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %33, i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.90, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @longCommandWArg(i8** %stringPtr, i8* %longCommand) #0 {
entry:
  %stringPtr.addr = alloca i8**, align 4
  %longCommand.addr = alloca i8*, align 4
  %comSize = alloca i32, align 4
  %result = alloca i32, align 4
  store i8** %stringPtr, i8*** %stringPtr.addr, align 4, !tbaa !6
  store i8* %longCommand, i8** %longCommand.addr, align 4, !tbaa !6
  %0 = bitcast i32* %comSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %longCommand.addr, align 4, !tbaa !6
  %call = call i32 @strlen(i8* %1)
  store i32 %call, i32* %comSize, align 4, !tbaa !9
  %2 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load i8**, i8*** %stringPtr.addr, align 4, !tbaa !6
  %4 = load i8*, i8** %3, align 4, !tbaa !6
  %5 = load i8*, i8** %longCommand.addr, align 4, !tbaa !6
  %6 = load i32, i32* %comSize, align 4, !tbaa !9
  %call1 = call i32 @strncmp(i8* %4, i8* %5, i32 %6)
  %tobool = icmp ne i32 %call1, 0
  %lnot = xor i1 %tobool, true
  %lnot.ext = zext i1 %lnot to i32
  store i32 %lnot.ext, i32* %result, align 4, !tbaa !2
  %7 = load i32, i32* %result, align 4, !tbaa !2
  %tobool2 = icmp ne i32 %7, 0
  br i1 %tobool2, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %8 = load i32, i32* %comSize, align 4, !tbaa !9
  %9 = load i8**, i8*** %stringPtr.addr, align 4, !tbaa !6
  %10 = load i8*, i8** %9, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %10, i32 %8
  store i8* %add.ptr, i8** %9, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %11 = load i32, i32* %result, align 4, !tbaa !2
  %12 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i32* %comSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  ret i32 %11
}

; Function Attrs: nounwind
define internal i32 @readU32FromChar(i8** %stringPtr) #0 {
entry:
  %stringPtr.addr = alloca i8**, align 4
  %result = alloca i32, align 4
  store i8** %stringPtr, i8*** %stringPtr.addr, align 4, !tbaa !6
  %0 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %result, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i8**, i8*** %stringPtr.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %1, align 4, !tbaa !6
  %3 = load i8, i8* %2, align 1, !tbaa !8
  %conv = sext i8 %3 to i32
  %cmp = icmp sge i32 %conv, 48
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %4 = load i8**, i8*** %stringPtr.addr, align 4, !tbaa !6
  %5 = load i8*, i8** %4, align 4, !tbaa !6
  %6 = load i8, i8* %5, align 1, !tbaa !8
  %conv2 = sext i8 %6 to i32
  %cmp3 = icmp sle i32 %conv2, 57
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %7 = phi i1 [ false, %while.cond ], [ %cmp3, %land.rhs ]
  br i1 %7, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %8 = load i32, i32* %result, align 4, !tbaa !2
  %mul = mul i32 %8, 10
  store i32 %mul, i32* %result, align 4, !tbaa !2
  %9 = load i8**, i8*** %stringPtr.addr, align 4, !tbaa !6
  %10 = load i8*, i8** %9, align 4, !tbaa !6
  %11 = load i8, i8* %10, align 1, !tbaa !8
  %conv5 = sext i8 %11 to i32
  %sub = sub nsw i32 %conv5, 48
  %12 = load i32, i32* %result, align 4, !tbaa !2
  %add = add i32 %12, %sub
  store i32 %add, i32* %result, align 4, !tbaa !2
  %13 = load i8**, i8*** %stringPtr.addr, align 4, !tbaa !6
  %14 = load i8*, i8** %13, align 4, !tbaa !6
  %incdec.ptr = getelementptr inbounds i8, i8* %14, i32 1
  store i8* %incdec.ptr, i8** %13, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %land.end
  %15 = load i8**, i8*** %stringPtr.addr, align 4, !tbaa !6
  %16 = load i8*, i8** %15, align 4, !tbaa !6
  %17 = load i8, i8* %16, align 1, !tbaa !8
  %conv6 = sext i8 %17 to i32
  %cmp7 = icmp eq i32 %conv6, 75
  br i1 %cmp7, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %while.end
  %18 = load i8**, i8*** %stringPtr.addr, align 4, !tbaa !6
  %19 = load i8*, i8** %18, align 4, !tbaa !6
  %20 = load i8, i8* %19, align 1, !tbaa !8
  %conv9 = sext i8 %20 to i32
  %cmp10 = icmp eq i32 %conv9, 77
  br i1 %cmp10, label %if.then, label %if.end30

if.then:                                          ; preds = %lor.lhs.false, %while.end
  %21 = load i32, i32* %result, align 4, !tbaa !2
  %shl = shl i32 %21, 10
  store i32 %shl, i32* %result, align 4, !tbaa !2
  %22 = load i8**, i8*** %stringPtr.addr, align 4, !tbaa !6
  %23 = load i8*, i8** %22, align 4, !tbaa !6
  %24 = load i8, i8* %23, align 1, !tbaa !8
  %conv12 = sext i8 %24 to i32
  %cmp13 = icmp eq i32 %conv12, 77
  br i1 %cmp13, label %if.then15, label %if.end

if.then15:                                        ; preds = %if.then
  %25 = load i32, i32* %result, align 4, !tbaa !2
  %shl16 = shl i32 %25, 10
  store i32 %shl16, i32* %result, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.then15, %if.then
  %26 = load i8**, i8*** %stringPtr.addr, align 4, !tbaa !6
  %27 = load i8*, i8** %26, align 4, !tbaa !6
  %incdec.ptr17 = getelementptr inbounds i8, i8* %27, i32 1
  store i8* %incdec.ptr17, i8** %26, align 4, !tbaa !6
  %28 = load i8**, i8*** %stringPtr.addr, align 4, !tbaa !6
  %29 = load i8*, i8** %28, align 4, !tbaa !6
  %30 = load i8, i8* %29, align 1, !tbaa !8
  %conv18 = sext i8 %30 to i32
  %cmp19 = icmp eq i32 %conv18, 105
  br i1 %cmp19, label %if.then21, label %if.end23

if.then21:                                        ; preds = %if.end
  %31 = load i8**, i8*** %stringPtr.addr, align 4, !tbaa !6
  %32 = load i8*, i8** %31, align 4, !tbaa !6
  %incdec.ptr22 = getelementptr inbounds i8, i8* %32, i32 1
  store i8* %incdec.ptr22, i8** %31, align 4, !tbaa !6
  br label %if.end23

if.end23:                                         ; preds = %if.then21, %if.end
  %33 = load i8**, i8*** %stringPtr.addr, align 4, !tbaa !6
  %34 = load i8*, i8** %33, align 4, !tbaa !6
  %35 = load i8, i8* %34, align 1, !tbaa !8
  %conv24 = sext i8 %35 to i32
  %cmp25 = icmp eq i32 %conv24, 66
  br i1 %cmp25, label %if.then27, label %if.end29

if.then27:                                        ; preds = %if.end23
  %36 = load i8**, i8*** %stringPtr.addr, align 4, !tbaa !6
  %37 = load i8*, i8** %36, align 4, !tbaa !6
  %incdec.ptr28 = getelementptr inbounds i8, i8* %37, i32 1
  store i8* %incdec.ptr28, i8** %36, align 4, !tbaa !6
  br label %if.end29

if.end29:                                         ; preds = %if.then27, %if.end23
  br label %if.end30

if.end30:                                         ; preds = %if.end29, %lor.lhs.false
  %38 = load i32, i32* %result, align 4, !tbaa !2
  %39 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  ret i32 %38
}

; Function Attrs: nounwind
define internal i32 @badusage(i8* %exeName) #0 {
entry:
  %exeName.addr = alloca i8*, align 4
  store i8* %exeName, i8** %exeName.addr, align 4, !tbaa !6
  %0 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp = icmp uge i32 %0, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %1, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.105, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load i32, i32* @displayLevel, align 4, !tbaa !2
  %cmp1 = icmp uge i32 %2, 1
  br i1 %cmp1, label %if.then2, label %if.end4

if.then2:                                         ; preds = %if.end
  %3 = load i8*, i8** %exeName.addr, align 4, !tbaa !6
  %call3 = call i32 @usage(i8* %3)
  br label %if.end4

if.end4:                                          ; preds = %if.then2, %if.end
  call void @exit(i32 1) #5
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal i32 @usage_longhelp(i8* %exeName) #0 {
entry:
  %exeName.addr = alloca i8*, align 4
  store i8* %exeName, i8** %exeName.addr, align 4, !tbaa !6
  %0 = load i8*, i8** %exeName.addr, align 4, !tbaa !6
  %call = call i32 @usage_advanced(i8* %0)
  %1 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call1 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %1, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.60, i32 0, i32 0))
  %2 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call2 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %2, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.106, i32 0, i32 0))
  %3 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call3 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %3, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.107, i32 0, i32 0))
  %4 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call4 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %4, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.106, i32 0, i32 0))
  %5 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call5 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %5, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.60, i32 0, i32 0))
  %6 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call6 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %6, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.108, i32 0, i32 0))
  %7 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call7 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %7, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.109, i32 0, i32 0))
  %8 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call8 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %8, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.110, i32 0, i32 0))
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call9 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %9, i8* getelementptr inbounds ([56 x i8], [56 x i8]* @.str.111, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0))
  %10 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call10 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %10, i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.112, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.113, i32 0, i32 0))
  %11 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call11 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %11, i8* getelementptr inbounds ([77 x i8], [77 x i8]* @.str.114, i32 0, i32 0))
  %12 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call12 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %12, i8* getelementptr inbounds ([67 x i8], [67 x i8]* @.str.115, i32 0, i32 0))
  %13 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call13 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %13, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.116, i32 0, i32 0))
  %14 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call14 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %14, i8* getelementptr inbounds ([57 x i8], [57 x i8]* @.str.117, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.49, i32 0, i32 0))
  %15 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call15 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %15, i8* getelementptr inbounds ([69 x i8], [69 x i8]* @.str.118, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.49, i32 0, i32 0))
  %16 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call16 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %16, i8* getelementptr inbounds ([72 x i8], [72 x i8]* @.str.119, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.49, i32 0, i32 0))
  %17 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call17 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %17, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.60, i32 0, i32 0))
  %18 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call18 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %18, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.120, i32 0, i32 0))
  %19 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call19 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %19, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.121, i32 0, i32 0))
  %20 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call20 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %20, i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.122, i32 0, i32 0))
  %21 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call21 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %21, i8* getelementptr inbounds ([78 x i8], [78 x i8]* @.str.123, i32 0, i32 0), i32 12)
  %22 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call22 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %22, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.60, i32 0, i32 0))
  %23 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call23 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %23, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.124, i32 0, i32 0))
  %24 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call24 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %24, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.125, i32 0, i32 0))
  %25 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call25 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %25, i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str.126, i32 0, i32 0))
  %26 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %27 = load i8*, i8** %exeName.addr, align 4, !tbaa !6
  %call26 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %26, i8* getelementptr inbounds ([59 x i8], [59 x i8]* @.str.127, i32 0, i32 0), i8* %27)
  %28 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call27 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %28, i8* getelementptr inbounds ([66 x i8], [66 x i8]* @.str.128, i32 0, i32 0))
  %29 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call28 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %29, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.60, i32 0, i32 0))
  %30 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call29 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %30, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.129, i32 0, i32 0))
  %31 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call30 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %31, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.130, i32 0, i32 0))
  %32 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call31 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %32, i8* getelementptr inbounds ([72 x i8], [72 x i8]* @.str.131, i32 0, i32 0))
  %33 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %34 = load i8*, i8** %exeName.addr, align 4, !tbaa !6
  %call32 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %33, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.132, i32 0, i32 0), i8* %34)
  %35 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call33 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %35, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.60, i32 0, i32 0))
  %36 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call34 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %36, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @.str.133, i32 0, i32 0))
  %37 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call35 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %37, i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.134, i32 0, i32 0))
  %38 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call36 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %38, i8* getelementptr inbounds ([78 x i8], [78 x i8]* @.str.135, i32 0, i32 0))
  %39 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %40 = load i8*, i8** %exeName.addr, align 4, !tbaa !6
  %call37 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %39, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.136, i32 0, i32 0), i8* %40)
  %41 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call38 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %41, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.137, i32 0, i32 0))
  %42 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %43 = load i8*, i8** %exeName.addr, align 4, !tbaa !6
  %call39 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %42, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.138, i32 0, i32 0), i8* %43)
  %44 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call40 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %44, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.60, i32 0, i32 0))
  %45 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %46 = load i8*, i8** %exeName.addr, align 4, !tbaa !6
  %call41 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %45, i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.str.139, i32 0, i32 0), i8* %46)
  %47 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call42 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %47, i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.140, i32 0, i32 0))
  %48 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call43 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %48, i8* getelementptr inbounds ([70 x i8], [70 x i8]* @.str.141, i32 0, i32 0))
  %49 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %50 = load i8*, i8** %exeName.addr, align 4, !tbaa !6
  %call44 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %49, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.142, i32 0, i32 0), i8* %50)
  %51 = load i32, i32* @g_lz4c_legacy_commands, align 4, !tbaa !2
  %tobool = icmp ne i32 %51, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %52 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call45 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %52, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.60, i32 0, i32 0))
  %53 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call46 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %53, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.143, i32 0, i32 0))
  %54 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call47 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %54, i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.144, i32 0, i32 0))
  %55 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call48 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %55, i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.145, i32 0, i32 0))
  %56 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %57 = load i8*, i8** %exeName.addr, align 4, !tbaa !6
  %call49 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %56, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.146, i32 0, i32 0), i8* %57)
  %58 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call50 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %58, i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.147, i32 0, i32 0))
  %59 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call51 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %59, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.148, i32 0, i32 0))
  %60 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %61 = load i8*, i8** %exeName.addr, align 4, !tbaa !6
  %call52 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %60, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.149, i32 0, i32 0), i8* %61)
  %62 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call53 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %62, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.150, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret i32 0
}

declare i32 @strlen(i8*) #2

declare i32 @LZ4IO_setBlockMode(%struct.LZ4IO_prefs_s*, i32) #2

declare i32 @LZ4IO_setBlockChecksumMode(%struct.LZ4IO_prefs_s*, i32) #2

declare void @BMK_setBlockSize(i32) #2

declare i32 @LZ4IO_setBlockSize(%struct.LZ4IO_prefs_s*, i32) #2

declare void @BMK_setBenchSeparately(i32) #2

declare void @BMK_setNotificationLevel(i32) #2

declare void @BMK_setNbSeconds(i32) #2

; Function Attrs: nounwind
define internal i8** @UTIL_createFileList(i8** %inputNames, i32 %inputNamesNb, i8** %allocatedBuffer, i32* %allocatedNamesNb) #0 {
entry:
  %retval = alloca i8**, align 4
  %inputNames.addr = alloca i8**, align 4
  %inputNamesNb.addr = alloca i32, align 4
  %allocatedBuffer.addr = alloca i8**, align 4
  %allocatedNamesNb.addr = alloca i32*, align 4
  %pos = alloca i32, align 4
  %i = alloca i32, align 4
  %nbFiles = alloca i32, align 4
  %buf = alloca i8*, align 4
  %bufSize = alloca i32, align 4
  %fileTable = alloca i8**, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %len = alloca i32, align 4
  %bufend = alloca i8*, align 4
  store i8** %inputNames, i8*** %inputNames.addr, align 4, !tbaa !6
  store i32 %inputNamesNb, i32* %inputNamesNb.addr, align 4, !tbaa !2
  store i8** %allocatedBuffer, i8*** %allocatedBuffer.addr, align 4, !tbaa !6
  store i32* %allocatedNamesNb, i32** %allocatedNamesNb.addr, align 4, !tbaa !6
  %0 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %nbFiles to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %call = call i8* @malloc(i32 8192)
  store i8* %call, i8** %buf, align 4, !tbaa !6
  %4 = bitcast i32* %bufSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 8192, i32* %bufSize, align 4, !tbaa !9
  %5 = bitcast i8*** %fileTable to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i8*, i8** %buf, align 4, !tbaa !6
  %tobool = icmp ne i8* %6, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i8** null, i8*** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup61

if.end:                                           ; preds = %entry
  store i32 0, i32* %i, align 4, !tbaa !2
  store i32 0, i32* %pos, align 4, !tbaa !9
  store i32 0, i32* %nbFiles, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %7 = load i32, i32* %i, align 4, !tbaa !2
  %8 = load i32, i32* %inputNamesNb.addr, align 4, !tbaa !2
  %cmp = icmp ult i32 %7, %8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load i8**, i8*** %inputNames.addr, align 4, !tbaa !6
  %10 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8*, i8** %9, i32 %10
  %11 = load i8*, i8** %arrayidx, align 4, !tbaa !6
  %call1 = call i32 @UTIL_isDirectory(i8* %11)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.else, label %if.then3

if.then3:                                         ; preds = %for.body
  %12 = bitcast i32* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load i8**, i8*** %inputNames.addr, align 4, !tbaa !6
  %14 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8*, i8** %13, i32 %14
  %15 = load i8*, i8** %arrayidx4, align 4, !tbaa !6
  %call5 = call i32 @strlen(i8* %15)
  store i32 %call5, i32* %len, align 4, !tbaa !9
  %16 = load i32, i32* %pos, align 4, !tbaa !9
  %17 = load i32, i32* %len, align 4, !tbaa !9
  %add = add i32 %16, %17
  %18 = load i32, i32* %bufSize, align 4, !tbaa !9
  %cmp6 = icmp uge i32 %add, %18
  br i1 %cmp6, label %if.then7, label %if.end15

if.then7:                                         ; preds = %if.then3
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then7
  %19 = load i32, i32* %pos, align 4, !tbaa !9
  %20 = load i32, i32* %len, align 4, !tbaa !9
  %add8 = add i32 %19, %20
  %21 = load i32, i32* %bufSize, align 4, !tbaa !9
  %cmp9 = icmp uge i32 %add8, %21
  br i1 %cmp9, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %22 = load i32, i32* %bufSize, align 4, !tbaa !9
  %add10 = add i32 %22, 8192
  store i32 %add10, i32* %bufSize, align 4, !tbaa !9
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %23 = load i8*, i8** %buf, align 4, !tbaa !6
  %24 = load i32, i32* %bufSize, align 4, !tbaa !9
  %call11 = call i8* @UTIL_realloc(i8* %23, i32 %24)
  store i8* %call11, i8** %buf, align 4, !tbaa !6
  %25 = load i8*, i8** %buf, align 4, !tbaa !6
  %tobool12 = icmp ne i8* %25, null
  br i1 %tobool12, label %if.end14, label %if.then13

if.then13:                                        ; preds = %while.end
  store i8** null, i8*** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %while.end
  br label %if.end15

if.end15:                                         ; preds = %if.end14, %if.then3
  %26 = load i32, i32* %pos, align 4, !tbaa !9
  %27 = load i32, i32* %len, align 4, !tbaa !9
  %add16 = add i32 %26, %27
  %28 = load i32, i32* %bufSize, align 4, !tbaa !9
  %cmp17 = icmp ult i32 %add16, %28
  br i1 %cmp17, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.end15
  call void @__assert_fail(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.151, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.152, i32 0, i32 0), i32 565, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @__func__.UTIL_createFileList, i32 0, i32 0)) #5
  unreachable

29:                                               ; No predecessors!
  br label %lor.end

lor.end:                                          ; preds = %29, %if.end15
  %30 = phi i1 [ true, %if.end15 ], [ false, %29 ]
  %lor.ext = zext i1 %30 to i32
  %31 = load i8*, i8** %buf, align 4, !tbaa !6
  %32 = load i32, i32* %pos, align 4, !tbaa !9
  %add.ptr = getelementptr inbounds i8, i8* %31, i32 %32
  %33 = load i8**, i8*** %inputNames.addr, align 4, !tbaa !6
  %34 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds i8*, i8** %33, i32 %34
  %35 = load i8*, i8** %arrayidx18, align 4, !tbaa !6
  %36 = load i32, i32* %bufSize, align 4, !tbaa !9
  %37 = load i32, i32* %pos, align 4, !tbaa !9
  %sub = sub i32 %36, %37
  %call19 = call i8* @strncpy(i8* %add.ptr, i8* %35, i32 %sub)
  %38 = load i32, i32* %len, align 4, !tbaa !9
  %add20 = add i32 %38, 1
  %39 = load i32, i32* %pos, align 4, !tbaa !9
  %add21 = add i32 %39, %add20
  store i32 %add21, i32* %pos, align 4, !tbaa !9
  %40 = load i32, i32* %nbFiles, align 4, !tbaa !2
  %inc = add i32 %40, 1
  store i32 %inc, i32* %nbFiles, align 4, !tbaa !2
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %lor.end, %if.then13
  %41 = bitcast i32* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup61 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end36

if.else:                                          ; preds = %for.body
  %42 = bitcast i8** %bufend to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #4
  %43 = load i8*, i8** %buf, align 4, !tbaa !6
  %44 = load i32, i32* %bufSize, align 4, !tbaa !9
  %add.ptr22 = getelementptr inbounds i8, i8* %43, i32 %44
  store i8* %add.ptr22, i8** %bufend, align 4, !tbaa !6
  %45 = load i8**, i8*** %inputNames.addr, align 4, !tbaa !6
  %46 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds i8*, i8** %45, i32 %46
  %47 = load i8*, i8** %arrayidx23, align 4, !tbaa !6
  %call24 = call i32 @UTIL_prepareFileList(i8* %47, i8** %buf, i32* %pos, i8** %bufend)
  %48 = load i32, i32* %nbFiles, align 4, !tbaa !2
  %add25 = add i32 %48, %call24
  store i32 %add25, i32* %nbFiles, align 4, !tbaa !2
  %49 = load i8*, i8** %buf, align 4, !tbaa !6
  %cmp26 = icmp eq i8* %49, null
  br i1 %cmp26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %if.else
  store i8** null, i8*** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup33

if.end28:                                         ; preds = %if.else
  %50 = load i8*, i8** %bufend, align 4, !tbaa !6
  %51 = load i8*, i8** %buf, align 4, !tbaa !6
  %cmp29 = icmp ugt i8* %50, %51
  br i1 %cmp29, label %lor.end31, label %lor.rhs30

lor.rhs30:                                        ; preds = %if.end28
  call void @__assert_fail(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.153, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.152, i32 0, i32 0), i32 573, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @__func__.UTIL_createFileList, i32 0, i32 0)) #5
  unreachable

52:                                               ; No predecessors!
  br label %lor.end31

lor.end31:                                        ; preds = %52, %if.end28
  %53 = phi i1 [ true, %if.end28 ], [ false, %52 ]
  %lor.ext32 = zext i1 %53 to i32
  %54 = load i8*, i8** %bufend, align 4, !tbaa !6
  %55 = load i8*, i8** %buf, align 4, !tbaa !6
  %sub.ptr.lhs.cast = ptrtoint i8* %54 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %55 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %bufSize, align 4, !tbaa !9
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup33

cleanup33:                                        ; preds = %lor.end31, %if.then27
  %56 = bitcast i8** %bufend to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #4
  %cleanup.dest34 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest34, label %cleanup61 [
    i32 0, label %cleanup.cont35
  ]

cleanup.cont35:                                   ; preds = %cleanup33
  br label %if.end36

if.end36:                                         ; preds = %cleanup.cont35, %cleanup.cont
  br label %for.inc

for.inc:                                          ; preds = %if.end36
  %57 = load i32, i32* %i, align 4, !tbaa !2
  %inc37 = add i32 %57, 1
  store i32 %inc37, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %58 = load i32, i32* %nbFiles, align 4, !tbaa !2
  %cmp38 = icmp eq i32 %58, 0
  br i1 %cmp38, label %if.then39, label %if.end40

if.then39:                                        ; preds = %for.end
  %59 = load i8*, i8** %buf, align 4, !tbaa !6
  call void @free(i8* %59)
  store i8** null, i8*** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup61

if.end40:                                         ; preds = %for.end
  %60 = load i32, i32* %nbFiles, align 4, !tbaa !2
  %add41 = add i32 %60, 1
  %mul = mul i32 %add41, 4
  %call42 = call i8* @malloc(i32 %mul)
  %61 = bitcast i8* %call42 to i8**
  store i8** %61, i8*** %fileTable, align 4, !tbaa !6
  %62 = load i8**, i8*** %fileTable, align 4, !tbaa !6
  %tobool43 = icmp ne i8** %62, null
  br i1 %tobool43, label %if.end45, label %if.then44

if.then44:                                        ; preds = %if.end40
  %63 = load i8*, i8** %buf, align 4, !tbaa !6
  call void @free(i8* %63)
  store i8** null, i8*** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup61

if.end45:                                         ; preds = %if.end40
  store i32 0, i32* %i, align 4, !tbaa !2
  store i32 0, i32* %pos, align 4, !tbaa !9
  br label %for.cond46

for.cond46:                                       ; preds = %for.inc55, %if.end45
  %64 = load i32, i32* %i, align 4, !tbaa !2
  %65 = load i32, i32* %nbFiles, align 4, !tbaa !2
  %cmp47 = icmp ult i32 %64, %65
  br i1 %cmp47, label %for.body48, label %for.end57

for.body48:                                       ; preds = %for.cond46
  %66 = load i8*, i8** %buf, align 4, !tbaa !6
  %67 = load i32, i32* %pos, align 4, !tbaa !9
  %add.ptr49 = getelementptr inbounds i8, i8* %66, i32 %67
  %68 = load i8**, i8*** %fileTable, align 4, !tbaa !6
  %69 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds i8*, i8** %68, i32 %69
  store i8* %add.ptr49, i8** %arrayidx50, align 4, !tbaa !6
  %70 = load i8**, i8*** %fileTable, align 4, !tbaa !6
  %71 = load i32, i32* %i, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds i8*, i8** %70, i32 %71
  %72 = load i8*, i8** %arrayidx51, align 4, !tbaa !6
  %call52 = call i32 @strlen(i8* %72)
  %add53 = add i32 %call52, 1
  %73 = load i32, i32* %pos, align 4, !tbaa !9
  %add54 = add i32 %73, %add53
  store i32 %add54, i32* %pos, align 4, !tbaa !9
  br label %for.inc55

for.inc55:                                        ; preds = %for.body48
  %74 = load i32, i32* %i, align 4, !tbaa !2
  %inc56 = add i32 %74, 1
  store i32 %inc56, i32* %i, align 4, !tbaa !2
  br label %for.cond46

for.end57:                                        ; preds = %for.cond46
  %75 = load i32, i32* %pos, align 4, !tbaa !9
  %76 = load i32, i32* %bufSize, align 4, !tbaa !9
  %cmp58 = icmp ugt i32 %75, %76
  br i1 %cmp58, label %if.then59, label %if.end60

if.then59:                                        ; preds = %for.end57
  %77 = load i8*, i8** %buf, align 4, !tbaa !6
  call void @free(i8* %77)
  %78 = load i8**, i8*** %fileTable, align 4, !tbaa !6
  %79 = bitcast i8** %78 to i8*
  call void @free(i8* %79)
  store i8** null, i8*** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup61

if.end60:                                         ; preds = %for.end57
  %80 = load i8*, i8** %buf, align 4, !tbaa !6
  %81 = load i8**, i8*** %allocatedBuffer.addr, align 4, !tbaa !6
  store i8* %80, i8** %81, align 4, !tbaa !6
  %82 = load i32, i32* %nbFiles, align 4, !tbaa !2
  %83 = load i32*, i32** %allocatedNamesNb.addr, align 4, !tbaa !6
  store i32 %82, i32* %83, align 4, !tbaa !2
  %84 = load i8**, i8*** %fileTable, align 4, !tbaa !6
  store i8** %84, i8*** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup61

cleanup61:                                        ; preds = %if.end60, %if.then59, %if.then44, %if.then39, %cleanup33, %cleanup, %if.then
  %85 = bitcast i8*** %fileTable to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i32* %bufSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast i8** %buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast i32* %nbFiles to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %90 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #4
  %91 = load i8**, i8*** %retval, align 4
  ret i8** %91
}

declare void @free(i8*) #2

declare i32 @isatty(i32) #2

declare i32 @fileno(%struct._IO_FILE*) #2

; Function Attrs: noreturn
declare void @exit(i32) #3

declare i32 @LZ4IO_setDictionaryFilename(%struct.LZ4IO_prefs_s*, i8*) #2

declare i32 @BMK_benchFiles(i8**, i32, i32, i32, i8*) #2

declare i32 @LZ4IO_setTestMode(%struct.LZ4IO_prefs_s*, i32) #2

; Function Attrs: nounwind
define internal i32 @UTIL_isRegFile(i8* %infilename) #0 {
entry:
  %infilename.addr = alloca i8*, align 4
  %statbuf = alloca %struct.stat, align 8
  store i8* %infilename, i8** %infilename.addr, align 4, !tbaa !6
  %0 = bitcast %struct.stat* %statbuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 88, i8* %0) #4
  %1 = load i8*, i8** %infilename.addr, align 4, !tbaa !6
  %call = call i32 @UTIL_getFileStat(i8* %1, %struct.stat* %statbuf)
  %2 = bitcast %struct.stat* %statbuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 88, i8* %2) #4
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @determineOpMode(i8* %inputFilename) #0 {
entry:
  %retval = alloca i32, align 4
  %inputFilename.addr = alloca i8*, align 4
  %inSize = alloca i32, align 4
  %extSize = alloca i32, align 4
  %extStart = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %inputFilename, i8** %inputFilename.addr, align 4, !tbaa !6
  %0 = bitcast i32* %inSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %inputFilename.addr, align 4, !tbaa !6
  %call = call i32 @strlen(i8* %1)
  store i32 %call, i32* %inSize, align 4, !tbaa !9
  %2 = bitcast i32* %extSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 4, i32* %extSize, align 4, !tbaa !9
  %3 = bitcast i32* %extStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load i32, i32* %inSize, align 4, !tbaa !9
  %cmp = icmp ugt i32 %4, 4
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %5 = load i32, i32* %inSize, align 4, !tbaa !9
  %sub = sub i32 %5, 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub, %cond.true ], [ 0, %cond.false ]
  store i32 %cond, i32* %extStart, align 4, !tbaa !9
  %6 = load i8*, i8** %inputFilename.addr, align 4, !tbaa !6
  %7 = load i32, i32* %extStart, align 4, !tbaa !9
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 %7
  %call1 = call i32 @strcmp(i8* %add.ptr, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.49, i32 0, i32 0))
  %tobool = icmp ne i32 %call1, 0
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %cond.end
  store i32 2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %cond.end
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then
  %8 = bitcast i32* %extStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %extSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i32* %inSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #4
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

declare void @perror(i8*) #2

declare i8* @strcpy(i8*, i8*) #2

declare i8* @strcat(i8*, i8*) #2

; Function Attrs: noreturn
declare void @__assert_fail(i8*, i8*, i32, i8*) #3

declare i32 @LZ4IO_setNotificationLevel(i32) #2

declare i32 @LZ4IO_decompressMultipleFilenames(%struct.LZ4IO_prefs_s*, i8**, i32, i8*) #2

declare i32 @LZ4IO_decompressFilename(%struct.LZ4IO_prefs_s*, i8*, i8*) #2

declare i32 @LZ4IO_displayCompressedFilesInfo(i8**, i32) #2

declare i32 @LZ4IO_compressMultipleFilenames_Legacy(%struct.LZ4IO_prefs_s*, i8**, i32, i8*, i32) #2

declare i32 @LZ4IO_compressFilename_Legacy(%struct.LZ4IO_prefs_s*, i8*, i8*, i32) #2

declare i32 @LZ4IO_compressMultipleFilenames(%struct.LZ4IO_prefs_s*, i8**, i32, i8*, i32) #2

declare i32 @LZ4IO_compressFilename(%struct.LZ4IO_prefs_s*, i8*, i8*, i32) #2

; Function Attrs: nounwind
define internal void @waitEnter() #0 {
entry:
  %0 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %0, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.158, i32 0, i32 0))
  %call1 = call i32 @getchar()
  ret void
}

; Function Attrs: nounwind
define internal void @UTIL_freeFileList(i8** %filenameTable, i8* %allocatedBuffer) #0 {
entry:
  %filenameTable.addr = alloca i8**, align 4
  %allocatedBuffer.addr = alloca i8*, align 4
  store i8** %filenameTable, i8*** %filenameTable.addr, align 4, !tbaa !6
  store i8* %allocatedBuffer, i8** %allocatedBuffer.addr, align 4, !tbaa !6
  %0 = load i8*, i8** %allocatedBuffer.addr, align 4, !tbaa !6
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i8*, i8** %allocatedBuffer.addr, align 4, !tbaa !6
  call void @free(i8* %1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load i8**, i8*** %filenameTable.addr, align 4, !tbaa !6
  %tobool1 = icmp ne i8** %2, null
  br i1 %tobool1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  %3 = load i8**, i8*** %filenameTable.addr, align 4, !tbaa !6
  %4 = bitcast i8** %3 to i8*
  call void @free(i8* %4)
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  ret void
}

declare void @LZ4IO_freePreferences(%struct.LZ4IO_prefs_s*) #2

declare i8* @strrchr(i8*, i32) #2

declare i32 @strncmp(i8*, i8*, i32) #2

; Function Attrs: nounwind
define internal i32 @usage(i8* %exeName) #0 {
entry:
  %exeName.addr = alloca i8*, align 4
  store i8* %exeName, i8** %exeName.addr, align 4, !tbaa !6
  %0 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %0, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.91, i32 0, i32 0))
  %1 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %2 = load i8*, i8** %exeName.addr, align 4, !tbaa !6
  %call1 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %1, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.92, i32 0, i32 0), i8* %2)
  %3 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call2 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %3, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.60, i32 0, i32 0))
  %4 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call3 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %4, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.93, i32 0, i32 0))
  %5 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call4 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %5, i8* getelementptr inbounds ([70 x i8], [70 x i8]* @.str.94, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @stdinmark, i32 0, i32 0))
  %6 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call5 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %6, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.95, i32 0, i32 0))
  %7 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call6 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %7, i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.96, i32 0, i32 0))
  %8 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call7 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %8, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.97, i32 0, i32 0))
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call8 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %9, i8* getelementptr inbounds ([52 x i8], [52 x i8]* @.str.98, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.49, i32 0, i32 0))
  %10 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call9 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %10, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.99, i32 0, i32 0))
  %11 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call10 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %11, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.100, i32 0, i32 0))
  %12 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call11 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %12, i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.101, i32 0, i32 0))
  %13 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call12 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %13, i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.102, i32 0, i32 0))
  %14 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call13 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %14, i8* getelementptr inbounds ([66 x i8], [66 x i8]* @.str.103, i32 0, i32 0))
  %15 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call14 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %15, i8* getelementptr inbounds ([44 x i8], [44 x i8]* @.str.104, i32 0, i32 0))
  ret i32 0
}

declare i8* @malloc(i32) #2

; Function Attrs: nounwind
define internal i32 @UTIL_isDirectory(i8* %infilename) #0 {
entry:
  %retval = alloca i32, align 4
  %infilename.addr = alloca i8*, align 4
  %r = alloca i32, align 4
  %statbuf = alloca %struct.stat, align 8
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %infilename, i8** %infilename.addr, align 4, !tbaa !6
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast %struct.stat* %statbuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 88, i8* %1) #4
  %2 = load i8*, i8** %infilename.addr, align 4, !tbaa !6
  %call = call i32 @stat(i8* %2, %struct.stat* %statbuf)
  store i32 %call, i32* %r, align 4, !tbaa !2
  %3 = load i32, i32* %r, align 4, !tbaa !2
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.end, label %land.lhs.true

land.lhs.true:                                    ; preds = %entry
  %st_mode = getelementptr inbounds %struct.stat, %struct.stat* %statbuf, i32 0, i32 3
  %4 = load i32, i32* %st_mode, align 4, !tbaa !11
  %and = and i32 %4, 61440
  %cmp = icmp eq i32 %and, 16384
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %land.lhs.true, %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %5 = bitcast %struct.stat* %statbuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 88, i8* %5) #4
  %6 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #4
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: nounwind
define internal i8* @UTIL_realloc(i8* %ptr, i32 %size) #0 {
entry:
  %retval = alloca i8*, align 4
  %ptr.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %newptr = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !6
  store i32 %size, i32* %size.addr, align 4, !tbaa !9
  %0 = bitcast i8** %newptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !6
  %2 = load i32, i32* %size.addr, align 4, !tbaa !9
  %call = call i8* @realloc(i8* %1, i32 %2)
  store i8* %call, i8** %newptr, align 4, !tbaa !6
  %3 = load i8*, i8** %newptr, align 4, !tbaa !6
  %tobool = icmp ne i8* %3, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load i8*, i8** %newptr, align 4, !tbaa !6
  store i8* %4, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %5 = load i8*, i8** %ptr.addr, align 4, !tbaa !6
  call void @free(i8* %5)
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %6 = bitcast i8** %newptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #4
  %7 = load i8*, i8** %retval, align 4
  ret i8* %7
}

declare i8* @strncpy(i8*, i8*, i32) #2

; Function Attrs: nounwind
define internal i32 @UTIL_prepareFileList(i8* %dirName, i8** %bufStart, i32* %pos, i8** %bufEnd) #0 {
entry:
  %retval = alloca i32, align 4
  %dirName.addr = alloca i8*, align 4
  %bufStart.addr = alloca i8**, align 4
  %pos.addr = alloca i32*, align 4
  %bufEnd.addr = alloca i8**, align 4
  %dir = alloca %struct.__dirstream*, align 4
  %entry1 = alloca %struct.dirent*, align 4
  %dirLength = alloca i32, align 4
  %nbFiles = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %path = alloca i8*, align 4
  %fnameLength = alloca i32, align 4
  %pathLength = alloca i32, align 4
  %newListSize = alloca i32, align 4
  store i8* %dirName, i8** %dirName.addr, align 4, !tbaa !6
  store i8** %bufStart, i8*** %bufStart.addr, align 4, !tbaa !6
  store i32* %pos, i32** %pos.addr, align 4, !tbaa !6
  store i8** %bufEnd, i8*** %bufEnd.addr, align 4, !tbaa !6
  %0 = bitcast %struct.__dirstream** %dir to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast %struct.dirent** %entry1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %dirLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %nbFiles to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  store i32 0, i32* %nbFiles, align 4, !tbaa !2
  %4 = load i8*, i8** %dirName.addr, align 4, !tbaa !6
  %call = call %struct.__dirstream* @opendir(i8* %4)
  store %struct.__dirstream* %call, %struct.__dirstream** %dir, align 4, !tbaa !6
  %tobool = icmp ne %struct.__dirstream* %call, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %5 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %6 = load i8*, i8** %dirName.addr, align 4, !tbaa !6
  %call2 = call i32* @__errno_location()
  %7 = load i32, i32* %call2, align 4, !tbaa !2
  %call3 = call i8* @strerror(i32 %7)
  %call4 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %5, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.154, i32 0, i32 0), i8* %6, i8* %call3)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup80

if.end:                                           ; preds = %entry
  %8 = load i8*, i8** %dirName.addr, align 4, !tbaa !6
  %call5 = call i32 @strlen(i8* %8)
  store i32 %call5, i32* %dirLength, align 4, !tbaa !2
  %call6 = call i32* @__errno_location()
  store i32 0, i32* %call6, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont71, %cleanup67, %if.end
  %9 = load %struct.__dirstream*, %struct.__dirstream** %dir, align 4, !tbaa !6
  %call7 = call %struct.dirent* @readdir(%struct.__dirstream* %9)
  store %struct.dirent* %call7, %struct.dirent** %entry1, align 4, !tbaa !6
  %cmp = icmp ne %struct.dirent* %call7, null
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %10 = bitcast i8** %path to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i32* %fnameLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %pathLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.dirent*, %struct.dirent** %entry1, align 4, !tbaa !6
  %d_name = getelementptr inbounds %struct.dirent, %struct.dirent* %13, i32 0, i32 4
  %arraydecay = getelementptr inbounds [256 x i8], [256 x i8]* %d_name, i32 0, i32 0
  %call8 = call i32 @strcmp(i8* %arraydecay, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.155, i32 0, i32 0))
  %cmp9 = icmp eq i32 %call8, 0
  br i1 %cmp9, label %if.then14, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %while.body
  %14 = load %struct.dirent*, %struct.dirent** %entry1, align 4, !tbaa !6
  %d_name10 = getelementptr inbounds %struct.dirent, %struct.dirent* %14, i32 0, i32 4
  %arraydecay11 = getelementptr inbounds [256 x i8], [256 x i8]* %d_name10, i32 0, i32 0
  %call12 = call i32 @strcmp(i8* %arraydecay11, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.156, i32 0, i32 0))
  %cmp13 = icmp eq i32 %call12, 0
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %lor.lhs.false, %while.body
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup67

if.end15:                                         ; preds = %lor.lhs.false
  %15 = load %struct.dirent*, %struct.dirent** %entry1, align 4, !tbaa !6
  %d_name16 = getelementptr inbounds %struct.dirent, %struct.dirent* %15, i32 0, i32 4
  %arraydecay17 = getelementptr inbounds [256 x i8], [256 x i8]* %d_name16, i32 0, i32 0
  %call18 = call i32 @strlen(i8* %arraydecay17)
  store i32 %call18, i32* %fnameLength, align 4, !tbaa !2
  %16 = load i32, i32* %dirLength, align 4, !tbaa !2
  %17 = load i32, i32* %fnameLength, align 4, !tbaa !2
  %add = add nsw i32 %16, %17
  %add19 = add nsw i32 %add, 2
  %call20 = call i8* @malloc(i32 %add19)
  store i8* %call20, i8** %path, align 4, !tbaa !6
  %18 = load i8*, i8** %path, align 4, !tbaa !6
  %tobool21 = icmp ne i8* %18, null
  br i1 %tobool21, label %if.end24, label %if.then22

if.then22:                                        ; preds = %if.end15
  %19 = load %struct.__dirstream*, %struct.__dirstream** %dir, align 4, !tbaa !6
  %call23 = call i32 @closedir(%struct.__dirstream* %19)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup67

if.end24:                                         ; preds = %if.end15
  %20 = load i8*, i8** %path, align 4, !tbaa !6
  %21 = load i8*, i8** %dirName.addr, align 4, !tbaa !6
  %22 = load i32, i32* %dirLength, align 4, !tbaa !2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %20, i8* align 1 %21, i32 %22, i1 false)
  %23 = load i8*, i8** %path, align 4, !tbaa !6
  %24 = load i32, i32* %dirLength, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %23, i32 %24
  store i8 47, i8* %arrayidx, align 1, !tbaa !8
  %25 = load i8*, i8** %path, align 4, !tbaa !6
  %26 = load i32, i32* %dirLength, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %25, i32 %26
  %add.ptr25 = getelementptr inbounds i8, i8* %add.ptr, i32 1
  %27 = load %struct.dirent*, %struct.dirent** %entry1, align 4, !tbaa !6
  %d_name26 = getelementptr inbounds %struct.dirent, %struct.dirent* %27, i32 0, i32 4
  %arraydecay27 = getelementptr inbounds [256 x i8], [256 x i8]* %d_name26, i32 0, i32 0
  %28 = load i32, i32* %fnameLength, align 4, !tbaa !2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr25, i8* align 1 %arraydecay27, i32 %28, i1 false)
  %29 = load i32, i32* %dirLength, align 4, !tbaa !2
  %add28 = add nsw i32 %29, 1
  %30 = load i32, i32* %fnameLength, align 4, !tbaa !2
  %add29 = add nsw i32 %add28, %30
  store i32 %add29, i32* %pathLength, align 4, !tbaa !2
  %31 = load i8*, i8** %path, align 4, !tbaa !6
  %32 = load i32, i32* %pathLength, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i8, i8* %31, i32 %32
  store i8 0, i8* %arrayidx30, align 1, !tbaa !8
  %33 = load i8*, i8** %path, align 4, !tbaa !6
  %call31 = call i32 @UTIL_isDirectory(i8* %33)
  %tobool32 = icmp ne i32 %call31, 0
  br i1 %tobool32, label %if.then33, label %if.else

if.then33:                                        ; preds = %if.end24
  %34 = load i8*, i8** %path, align 4, !tbaa !6
  %35 = load i8**, i8*** %bufStart.addr, align 4, !tbaa !6
  %36 = load i32*, i32** %pos.addr, align 4, !tbaa !6
  %37 = load i8**, i8*** %bufEnd.addr, align 4, !tbaa !6
  %call34 = call i32 @UTIL_prepareFileList(i8* %34, i8** %35, i32* %36, i8** %37)
  %38 = load i32, i32* %nbFiles, align 4, !tbaa !2
  %add35 = add nsw i32 %38, %call34
  store i32 %add35, i32* %nbFiles, align 4, !tbaa !2
  %39 = load i8**, i8*** %bufStart.addr, align 4, !tbaa !6
  %40 = load i8*, i8** %39, align 4, !tbaa !6
  %cmp36 = icmp eq i8* %40, null
  br i1 %cmp36, label %if.then37, label %if.end39

if.then37:                                        ; preds = %if.then33
  %41 = load i8*, i8** %path, align 4, !tbaa !6
  call void @free(i8* %41)
  %42 = load %struct.__dirstream*, %struct.__dirstream** %dir, align 4, !tbaa !6
  %call38 = call i32 @closedir(%struct.__dirstream* %42)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup67

if.end39:                                         ; preds = %if.then33
  br label %if.end65

if.else:                                          ; preds = %if.end24
  %43 = load i8**, i8*** %bufStart.addr, align 4, !tbaa !6
  %44 = load i8*, i8** %43, align 4, !tbaa !6
  %45 = load i32*, i32** %pos.addr, align 4, !tbaa !6
  %46 = load i32, i32* %45, align 4, !tbaa !9
  %add.ptr40 = getelementptr inbounds i8, i8* %44, i32 %46
  %47 = load i32, i32* %pathLength, align 4, !tbaa !2
  %add.ptr41 = getelementptr inbounds i8, i8* %add.ptr40, i32 %47
  %48 = load i8**, i8*** %bufEnd.addr, align 4, !tbaa !6
  %49 = load i8*, i8** %48, align 4, !tbaa !6
  %cmp42 = icmp uge i8* %add.ptr41, %49
  br i1 %cmp42, label %if.then43, label %if.end51

if.then43:                                        ; preds = %if.else
  %50 = bitcast i32* %newListSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #4
  %51 = load i8**, i8*** %bufEnd.addr, align 4, !tbaa !6
  %52 = load i8*, i8** %51, align 4, !tbaa !6
  %53 = load i8**, i8*** %bufStart.addr, align 4, !tbaa !6
  %54 = load i8*, i8** %53, align 4, !tbaa !6
  %sub.ptr.lhs.cast = ptrtoint i8* %52 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %54 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %add44 = add nsw i32 %sub.ptr.sub, 8192
  store i32 %add44, i32* %newListSize, align 4, !tbaa !9
  %55 = load i8**, i8*** %bufStart.addr, align 4, !tbaa !6
  %56 = load i8*, i8** %55, align 4, !tbaa !6
  %57 = load i32, i32* %newListSize, align 4, !tbaa !9
  %call45 = call i8* @UTIL_realloc(i8* %56, i32 %57)
  %58 = load i8**, i8*** %bufStart.addr, align 4, !tbaa !6
  store i8* %call45, i8** %58, align 4, !tbaa !6
  %59 = load i8**, i8*** %bufStart.addr, align 4, !tbaa !6
  %60 = load i8*, i8** %59, align 4, !tbaa !6
  %61 = load i32, i32* %newListSize, align 4, !tbaa !9
  %add.ptr46 = getelementptr inbounds i8, i8* %60, i32 %61
  %62 = load i8**, i8*** %bufEnd.addr, align 4, !tbaa !6
  store i8* %add.ptr46, i8** %62, align 4, !tbaa !6
  %63 = load i8**, i8*** %bufStart.addr, align 4, !tbaa !6
  %64 = load i8*, i8** %63, align 4, !tbaa !6
  %cmp47 = icmp eq i8* %64, null
  br i1 %cmp47, label %if.then48, label %if.end50

if.then48:                                        ; preds = %if.then43
  %65 = load i8*, i8** %path, align 4, !tbaa !6
  call void @free(i8* %65)
  %66 = load %struct.__dirstream*, %struct.__dirstream** %dir, align 4, !tbaa !6
  %call49 = call i32 @closedir(%struct.__dirstream* %66)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end50:                                         ; preds = %if.then43
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end50, %if.then48
  %67 = bitcast i32* %newListSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup67 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end51

if.end51:                                         ; preds = %cleanup.cont, %if.else
  %68 = load i8**, i8*** %bufStart.addr, align 4, !tbaa !6
  %69 = load i8*, i8** %68, align 4, !tbaa !6
  %70 = load i32*, i32** %pos.addr, align 4, !tbaa !6
  %71 = load i32, i32* %70, align 4, !tbaa !9
  %add.ptr52 = getelementptr inbounds i8, i8* %69, i32 %71
  %72 = load i32, i32* %pathLength, align 4, !tbaa !2
  %add.ptr53 = getelementptr inbounds i8, i8* %add.ptr52, i32 %72
  %73 = load i8**, i8*** %bufEnd.addr, align 4, !tbaa !6
  %74 = load i8*, i8** %73, align 4, !tbaa !6
  %cmp54 = icmp ult i8* %add.ptr53, %74
  br i1 %cmp54, label %if.then55, label %if.end64

if.then55:                                        ; preds = %if.end51
  %75 = load i8**, i8*** %bufStart.addr, align 4, !tbaa !6
  %76 = load i8*, i8** %75, align 4, !tbaa !6
  %77 = load i32*, i32** %pos.addr, align 4, !tbaa !6
  %78 = load i32, i32* %77, align 4, !tbaa !9
  %add.ptr56 = getelementptr inbounds i8, i8* %76, i32 %78
  %79 = load i8*, i8** %path, align 4, !tbaa !6
  %80 = load i8**, i8*** %bufEnd.addr, align 4, !tbaa !6
  %81 = load i8*, i8** %80, align 4, !tbaa !6
  %82 = load i8**, i8*** %bufStart.addr, align 4, !tbaa !6
  %83 = load i8*, i8** %82, align 4, !tbaa !6
  %84 = load i32*, i32** %pos.addr, align 4, !tbaa !6
  %85 = load i32, i32* %84, align 4, !tbaa !9
  %add.ptr57 = getelementptr inbounds i8, i8* %83, i32 %85
  %sub.ptr.lhs.cast58 = ptrtoint i8* %81 to i32
  %sub.ptr.rhs.cast59 = ptrtoint i8* %add.ptr57 to i32
  %sub.ptr.sub60 = sub i32 %sub.ptr.lhs.cast58, %sub.ptr.rhs.cast59
  %call61 = call i8* @strncpy(i8* %add.ptr56, i8* %79, i32 %sub.ptr.sub60)
  %86 = load i32, i32* %pathLength, align 4, !tbaa !2
  %add62 = add nsw i32 %86, 1
  %87 = load i32*, i32** %pos.addr, align 4, !tbaa !6
  %88 = load i32, i32* %87, align 4, !tbaa !9
  %add63 = add i32 %88, %add62
  store i32 %add63, i32* %87, align 4, !tbaa !9
  %89 = load i32, i32* %nbFiles, align 4, !tbaa !2
  %inc = add nsw i32 %89, 1
  store i32 %inc, i32* %nbFiles, align 4, !tbaa !2
  br label %if.end64

if.end64:                                         ; preds = %if.then55, %if.end51
  br label %if.end65

if.end65:                                         ; preds = %if.end64, %if.end39
  %90 = load i8*, i8** %path, align 4, !tbaa !6
  call void @free(i8* %90)
  %call66 = call i32* @__errno_location()
  store i32 0, i32* %call66, align 4, !tbaa !2
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup67

cleanup67:                                        ; preds = %if.end65, %cleanup, %if.then37, %if.then22, %if.then14
  %91 = bitcast i32* %pathLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #4
  %92 = bitcast i32* %fnameLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  %93 = bitcast i8** %path to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #4
  %cleanup.dest70 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest70, label %cleanup80 [
    i32 0, label %cleanup.cont71
    i32 2, label %while.cond
  ]

cleanup.cont71:                                   ; preds = %cleanup67
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %call72 = call i32* @__errno_location()
  %94 = load i32, i32* %call72, align 4, !tbaa !2
  %cmp73 = icmp ne i32 %94, 0
  br i1 %cmp73, label %if.then74, label %if.end78

if.then74:                                        ; preds = %while.end
  %95 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %96 = load i8*, i8** %dirName.addr, align 4, !tbaa !6
  %call75 = call i32* @__errno_location()
  %97 = load i32, i32* %call75, align 4, !tbaa !2
  %call76 = call i8* @strerror(i32 %97)
  %call77 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %95, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.157, i32 0, i32 0), i8* %96, i8* %call76)
  %98 = load i8**, i8*** %bufStart.addr, align 4, !tbaa !6
  %99 = load i8*, i8** %98, align 4, !tbaa !6
  call void @free(i8* %99)
  %100 = load i8**, i8*** %bufStart.addr, align 4, !tbaa !6
  store i8* null, i8** %100, align 4, !tbaa !6
  br label %if.end78

if.end78:                                         ; preds = %if.then74, %while.end
  %101 = load %struct.__dirstream*, %struct.__dirstream** %dir, align 4, !tbaa !6
  %call79 = call i32 @closedir(%struct.__dirstream* %101)
  %102 = load i32, i32* %nbFiles, align 4, !tbaa !2
  store i32 %102, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup80

cleanup80:                                        ; preds = %if.end78, %cleanup67, %if.then
  %103 = bitcast i32* %nbFiles to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #4
  %104 = bitcast i32* %dirLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #4
  %105 = bitcast %struct.dirent** %entry1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #4
  %106 = bitcast %struct.__dirstream** %dir to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #4
  %107 = load i32, i32* %retval, align 4
  ret i32 %107
}

declare i32 @stat(i8*, %struct.stat*) #2

declare i8* @realloc(i8*, i32) #2

declare %struct.__dirstream* @opendir(i8*) #2

declare i8* @strerror(i32) #2

declare i32* @__errno_location() #2

declare %struct.dirent* @readdir(%struct.__dirstream*) #2

declare i32 @closedir(%struct.__dirstream*) #2

; Function Attrs: nounwind
define internal i32 @UTIL_getFileStat(i8* %infilename, %struct.stat* %statbuf) #0 {
entry:
  %retval = alloca i32, align 4
  %infilename.addr = alloca i8*, align 4
  %statbuf.addr = alloca %struct.stat*, align 4
  %r = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %infilename, i8** %infilename.addr, align 4, !tbaa !6
  store %struct.stat* %statbuf, %struct.stat** %statbuf.addr, align 4, !tbaa !6
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %infilename.addr, align 4, !tbaa !6
  %2 = load %struct.stat*, %struct.stat** %statbuf.addr, align 4, !tbaa !6
  %call = call i32 @stat(i8* %1, %struct.stat* %2)
  store i32 %call, i32* %r, align 4, !tbaa !2
  %3 = load i32, i32* %r, align 4, !tbaa !2
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %4 = load %struct.stat*, %struct.stat** %statbuf.addr, align 4, !tbaa !6
  %st_mode = getelementptr inbounds %struct.stat, %struct.stat* %4, i32 0, i32 3
  %5 = load i32, i32* %st_mode, align 4, !tbaa !11
  %and = and i32 %5, 61440
  %cmp = icmp eq i32 %and, 32768
  br i1 %cmp, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %6 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #4
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

declare i32 @getchar() #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }
attributes #5 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"int", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"any pointer", !4, i64 0}
!8 = !{!4, !4, i64 0}
!9 = !{!10, !10, i64 0}
!10 = !{!"long", !4, i64 0}
!11 = !{!12, !3, i64 12}
!12 = !{!"stat", !3, i64 0, !3, i64 4, !10, i64 8, !3, i64 12, !10, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !13, i64 40, !10, i64 48, !3, i64 52, !14, i64 56, !14, i64 64, !14, i64 72, !13, i64 80}
!13 = !{!"long long", !4, i64 0}
!14 = !{!"timespec", !10, i64 0, !10, i64 4}
