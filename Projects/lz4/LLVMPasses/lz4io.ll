; ModuleID = 'lz4io.c'
source_filename = "lz4io.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct._IO_FILE = type opaque
%struct.LZ4IO_prefs_s = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8*, i32 }
%struct.cRess_t = type { i8*, i32, i8*, i32, %struct.LZ4F_cctx_s*, %struct.LZ4F_CDict_s* }
%struct.LZ4F_cctx_s = type opaque
%struct.LZ4F_CDict_s = type opaque
%struct.LZ4F_preferences_t = type { %struct.LZ4F_frameInfo_t, i32, i32, i32, [3 x i32] }
%struct.LZ4F_frameInfo_t = type { i32, i32, i32, i32, i64, i32, i32 }
%struct.stat = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i64, i32, i32, %struct.timespec, %struct.timespec, %struct.timespec, i64 }
%struct.timespec = type { i32, i32 }
%struct.LZ4F_compressOptions_t = type { i32, [3 x i32] }
%struct.dRess_t = type { i8*, i32, i8*, i32, %struct._IO_FILE*, %struct.LZ4F_dctx_s*, i8*, i32 }
%struct.LZ4F_dctx_s = type opaque
%struct.LZ4IO_cFileInfo_t = type { i8*, i64, i64, %struct.LZ4IO_frameInfo_t, i16, i16, i16 }
%struct.LZ4IO_frameInfo_t = type { %struct.LZ4F_frameInfo_t, i32 }
%struct.LZ4F_decompressOptions_t = type { i32, [3 x i32] }

@g_displayLevel = internal global i32 0, align 4
@stderr = external constant %struct._IO_FILE*, align 4
@.str = private unnamed_addr constant [12 x i8] c"Error %i : \00", align 1
@.str.1 = private unnamed_addr constant [37 x i8] c"Allocation error : not enough memory\00", align 1
@.str.2 = private unnamed_addr constant [3 x i8] c" \0A\00", align 1
@LZ4IO_setBlockSizeID.blockSizeTable = internal constant [4 x i32] [i32 65536, i32 262144, i32 1048576, i32 4194304], align 16
@LZ4IO_setBlockSizeID.minBlockSizeID = internal constant i32 4, align 4
@LZ4IO_setBlockSizeID.maxBlockSizeID = internal constant i32 7, align 4
@LZ4IO_setBlockSize.minBlockSize = internal constant i32 32, align 4
@LZ4IO_setBlockSize.maxBlockSize = internal constant i32 4194304, align 4
@.str.3 = private unnamed_addr constant [22 x i8] c"%s : open file error \00", align 1
@.str.4 = private unnamed_addr constant [34 x i8] c"Write error : cannot write header\00", align 1
@.str.5 = private unnamed_addr constant [27 x i8] c"inSize <= LEGACY_BLOCKSIZE\00", align 1
@.str.6 = private unnamed_addr constant [8 x i8] c"lz4io.c\00", align 1
@__func__.LZ4IO_compressFilename_Legacy = private unnamed_addr constant [30 x i8] c"LZ4IO_compressFilename_Legacy\00", align 1
@g_time = internal global i32 0, align 4
@.str.7 = private unnamed_addr constant [29 x i8] c"\0DRead : %i MB  ==> %.2f%%   \00", align 1
@.str.8 = private unnamed_addr constant [12 x i8] c"outSize > 0\00", align 1
@.str.9 = private unnamed_addr constant [22 x i8] c"outSize < outBuffSize\00", align 1
@.str.10 = private unnamed_addr constant [44 x i8] c"Write error : cannot write compressed block\00", align 1
@.str.11 = private unnamed_addr constant [24 x i8] c"Error while reading %s \00", align 1
@.str.12 = private unnamed_addr constant [7 x i8] c"\0D%79s\0D\00", align 1
@.str.13 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@.str.14 = private unnamed_addr constant [50 x i8] c"Compressed %llu bytes into %llu bytes ==> %.2f%%\0A\00", align 1
@.str.15 = private unnamed_addr constant [30 x i8] c"Done in %.2f s ==> %.2f MB/s\0A\00", align 1
@stdoutmark = internal constant [7 x i8] c"stdout\00", align 1
@.str.16 = private unnamed_addr constant [44 x i8] c"Completed in %.2f sec  (cpu load : %.0f%%)\0A\00", align 1
@.str.17 = private unnamed_addr constant [20 x i8] c"Done in %.2f sec  \0A\00", align 1
@.str.18 = private unnamed_addr constant [86 x i8] c"File extension doesn't match expected LZ4_EXTENSION (%4s); will not process file: %s\0A\00", align 1
@.str.19 = private unnamed_addr constant [12 x i8] c"sizeID >= 4\00", align 1
@__func__.LZ4IO_blockTypeID = private unnamed_addr constant [18 x i8] c"LZ4IO_blockTypeID\00", align 1
@.str.20 = private unnamed_addr constant [12 x i8] c"sizeID <= 7\00", align 1
@stdout = external constant %struct._IO_FILE*, align 4
@.str.21 = private unnamed_addr constant [34 x i8] c"%10s %14s %5s %11s %13s %9s   %s\0A\00", align 1
@.str.22 = private unnamed_addr constant [7 x i8] c"Frames\00", align 1
@.str.23 = private unnamed_addr constant [5 x i8] c"Type\00", align 1
@.str.24 = private unnamed_addr constant [6 x i8] c"Block\00", align 1
@.str.25 = private unnamed_addr constant [11 x i8] c"Compressed\00", align 1
@.str.26 = private unnamed_addr constant [13 x i8] c"Uncompressed\00", align 1
@.str.27 = private unnamed_addr constant [6 x i8] c"Ratio\00", align 1
@.str.28 = private unnamed_addr constant [9 x i8] c"Filename\00", align 1
@.str.29 = private unnamed_addr constant [32 x i8] c"lz4: %s is not a regular file \0A\00", align 1
@.str.30 = private unnamed_addr constant [15 x i8] c"%s(%llu/%llu)\0A\00", align 1
@.str.31 = private unnamed_addr constant [36 x i8] c"    %6s %14s %5s %8s %20s %20s %9s\0A\00", align 1
@.str.32 = private unnamed_addr constant [6 x i8] c"Frame\00", align 1
@.str.33 = private unnamed_addr constant [9 x i8] c"Checksum\00", align 1
@.str.34 = private unnamed_addr constant [36 x i8] c"op_result == LZ4IO_format_not_known\00", align 1
@__func__.LZ4IO_displayCompressedFilesInfo = private unnamed_addr constant [33 x i8] c"LZ4IO_displayCompressedFilesInfo\00", align 1
@.str.35 = private unnamed_addr constant [38 x i8] c"lz4: %s: File format not recognized \0A\00", align 1
@.str.36 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.37 = private unnamed_addr constant [27 x i8] c"%10llu %14s %5s %11s %13s \00", align 1
@LZ4IO_frameTypeNames = internal global [3 x i8*] [i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.101, i32 0, i32 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.102, i32 0, i32 0), i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.100, i32 0, i32 0)], align 4
@.str.38 = private unnamed_addr constant [2 x i8] c"-\00", align 1
@.str.39 = private unnamed_addr constant [14 x i8] c"%9.2f%%  %s \0A\00", align 1
@.str.40 = private unnamed_addr constant [10 x i8] c"%9s   %s\0A\00", align 1
@stdinmark = internal constant [6 x i8] c"stdin\00", align 1
@.str.41 = private unnamed_addr constant [23 x i8] c"Using stdin for input\0A\00", align 1
@stdin = external constant %struct._IO_FILE*, align 4
@.str.42 = private unnamed_addr constant [3 x i8] c"rb\00", align 1
@.str.43 = private unnamed_addr constant [9 x i8] c"%s: %s \0A\00", align 1
@.str.44 = private unnamed_addr constant [20 x i8] c"dstFileName != NULL\00", align 1
@__func__.LZ4IO_openDstFile = private unnamed_addr constant [18 x i8] c"LZ4IO_openDstFile\00", align 1
@.str.45 = private unnamed_addr constant [25 x i8] c"Using stdout for output\0A\00", align 1
@.str.46 = private unnamed_addr constant [73 x i8] c"Sparse File Support is automatically disabled on stdout ; try --sparse \0A\00", align 1
@nulmark = internal constant [10 x i8] c"/dev/null\00", align 1
@.str.47 = private unnamed_addr constant [38 x i8] c"%s already exists; not overwritten  \0A\00", align 1
@.str.48 = private unnamed_addr constant [53 x i8] c"%s already exists; do you wish to overwrite (y/N) ? \00", align 1
@.str.49 = private unnamed_addr constant [23 x i8] c"    not overwritten  \0A\00", align 1
@.str.50 = private unnamed_addr constant [3 x i8] c"wb\00", align 1
@.str.51 = private unnamed_addr constant [8 x i8] c"%s: %s\0A\00", align 1
@.str.52 = private unnamed_addr constant [50 x i8] c"Allocation error : can't create LZ4F context : %s\00", align 1
@.str.53 = private unnamed_addr constant [47 x i8] c"Dictionary error : could not create dictionary\00", align 1
@.str.54 = private unnamed_addr constant [40 x i8] c"Dictionary error : no filename provided\00", align 1
@.str.55 = private unnamed_addr constant [50 x i8] c"Dictionary error : could not open dictionary file\00", align 1
@.str.56 = private unnamed_addr constant [48 x i8] c"Warning : cannot determine input content size \0A\00", align 1
@.str.57 = private unnamed_addr constant [18 x i8] c"Error reading %s \00", align 1
@.str.58 = private unnamed_addr constant [24 x i8] c"Compression failed : %s\00", align 1
@.str.59 = private unnamed_addr constant [30 x i8] c"\0DRead : %u MB   ==> %.2f%%   \00", align 1
@.str.60 = private unnamed_addr constant [35 x i8] c"File header generation failed : %s\00", align 1
@.str.61 = private unnamed_addr constant [35 x i8] c"End of file generation failed : %s\00", align 1
@.str.62 = private unnamed_addr constant [41 x i8] c"Write error : cannot write end of stream\00", align 1
@.str.63 = private unnamed_addr constant [22 x i8] c"Remove error : %s: %s\00", align 1
@.str.64 = private unnamed_addr constant [46 x i8] c"Error : can't free LZ4F context resource : %s\00", align 1
@.str.65 = private unnamed_addr constant [31 x i8] c"Can't create LZ4F context : %s\00", align 1
@.str.66 = private unnamed_addr constant [32 x i8] c"%-20.20s : decoded %llu bytes \0A\00", align 1
@selectDecoder.nbFrames = internal global i32 0, align 4
@g_magicRead = internal global i32 0, align 4
@.str.67 = private unnamed_addr constant [46 x i8] c"Unrecognized header : Magic Number unreadable\00", align 1
@.str.68 = private unnamed_addr constant [27 x i8] c"Detected : Legacy format \0A\00", align 1
@.str.69 = private unnamed_addr constant [35 x i8] c"Skipping detected skippable area \0A\00", align 1
@.str.70 = private unnamed_addr constant [41 x i8] c"Stream error : skippable size unreadable\00", align 1
@.str.71 = private unnamed_addr constant [42 x i8] c"Stream error : cannot skip skippable area\00", align 1
@.str.72 = private unnamed_addr constant [45 x i8] c"Unrecognized header : file cannot be decoded\00", align 1
@.str.73 = private unnamed_addr constant [37 x i8] c"Stream followed by undecodable data \00", align 1
@.str.74 = private unnamed_addr constant [16 x i8] c"at position %i \00", align 1
@.str.75 = private unnamed_addr constant [18 x i8] c"Header error : %s\00", align 1
@.str.76 = private unnamed_addr constant [25 x i8] c"Decompression error : %s\00", align 1
@.str.77 = private unnamed_addr constant [24 x i8] c"\0DDecompressed : %u MB  \00", align 1
@.str.78 = private unnamed_addr constant [11 x i8] c"Read error\00", align 1
@.str.79 = private unnamed_addr constant [18 x i8] c"Unfinished stream\00", align 1
@.str.80 = private unnamed_addr constant [41 x i8] c"Write error : cannot write decoded block\00", align 1
@.str.81 = private unnamed_addr constant [38 x i8] c"1 GB skip error (sparse file support)\00", align 1
@.str.82 = private unnamed_addr constant [44 x i8] c"Sparse skip error(%d): %s ; try --no-sparse\00", align 1
@.str.83 = private unnamed_addr constant [36 x i8] c"Sparse skip error ; try --no-sparse\00", align 1
@.str.84 = private unnamed_addr constant [48 x i8] c"Write error : cannot write decoded end of block\00", align 1
@.str.85 = private unnamed_addr constant [32 x i8] c"Final skip error (sparse file)\0A\00", align 1
@.str.86 = private unnamed_addr constant [38 x i8] c"Write error : cannot write last zero\0A\00", align 1
@.str.87 = private unnamed_addr constant [39 x i8] c"Read error : cannot access block size \00", align 1
@.str.88 = private unnamed_addr constant [46 x i8] c"Read error : cannot access compressed block !\00", align 1
@.str.89 = private unnamed_addr constant [45 x i8] c"Decoding Failed ! Corrupted input detected !\00", align 1
@.str.90 = private unnamed_addr constant [20 x i8] c"Read error : ferror\00", align 1
@.str.91 = private unnamed_addr constant [25 x i8] c"Pass-through write error\00", align 1
@.str.92 = private unnamed_addr constant [11 x i8] c"Read Error\00", align 1
@.str.93 = private unnamed_addr constant [17 x i8] c"Error reading %s\00", align 1
@.str.94 = private unnamed_addr constant [23 x i8] c"    %6llu %14s %5s %8s\00", align 1
@.str.95 = private unnamed_addr constant [6 x i8] c"XXH32\00", align 1
@.str.96 = private unnamed_addr constant [24 x i8] c" %20llu %20llu %9.2f%%\0A\00", align 1
@.str.97 = private unnamed_addr constant [19 x i8] c" %20llu %20s %9s \0A\00", align 1
@.str.98 = private unnamed_addr constant [40 x i8] c"    %6llu %14s %5s %8s %20llu %20s %9s\0A\00", align 1
@.str.99 = private unnamed_addr constant [38 x i8] c"    %6llu %14s %5s %8s %20u %20s %9s\0A\00", align 1
@.str.100 = private unnamed_addr constant [15 x i8] c"SkippableFrame\00", align 1
@.str.101 = private unnamed_addr constant [9 x i8] c"LZ4Frame\00", align 1
@.str.102 = private unnamed_addr constant [12 x i8] c"LegacyFrame\00", align 1
@__const.LZ4IO_toHuman.units = private unnamed_addr constant [10 x i8] c"\00KMGTPEZY\00", align 1
@.str.103 = private unnamed_addr constant [8 x i8] c"%.2Lf%c\00", align 1

; Function Attrs: nounwind
define hidden %struct.LZ4IO_prefs_s* @LZ4IO_defaultPreferences() #0 {
entry:
  %ret = alloca %struct.LZ4IO_prefs_s*, align 4
  %0 = bitcast %struct.LZ4IO_prefs_s** %ret to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %call = call i8* @malloc(i32 56)
  %1 = bitcast i8* %call to %struct.LZ4IO_prefs_s*
  store %struct.LZ4IO_prefs_s* %1, %struct.LZ4IO_prefs_s** %ret, align 4, !tbaa !2
  %2 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %ret, align 4, !tbaa !2
  %tobool = icmp ne %struct.LZ4IO_prefs_s* %2, null
  br i1 %tobool, label %if.end11, label %if.then

if.then:                                          ; preds = %entry
  %3 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp = icmp sge i32 %3, 1
  br i1 %cmp, label %if.then1, label %if.end

if.then1:                                         ; preds = %if.then
  %4 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call2 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %4, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 21)
  br label %if.end

if.end:                                           ; preds = %if.then1, %if.then
  %5 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp3 = icmp sge i32 %5, 1
  br i1 %cmp3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.end
  %6 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call5 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %6, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.1, i32 0, i32 0))
  br label %if.end6

if.end6:                                          ; preds = %if.then4, %if.end
  %7 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp7 = icmp sge i32 %7, 1
  br i1 %cmp7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %if.end6
  %8 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call9 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %8, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end10

if.end10:                                         ; preds = %if.then8, %if.end6
  call void @exit(i32 21) #6
  unreachable

if.end11:                                         ; preds = %entry
  %9 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %ret, align 4, !tbaa !2
  %passThrough = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %9, i32 0, i32 0
  store i32 0, i32* %passThrough, align 4, !tbaa !8
  %10 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %ret, align 4, !tbaa !2
  %overwrite = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %10, i32 0, i32 1
  store i32 1, i32* %overwrite, align 4, !tbaa !11
  %11 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %ret, align 4, !tbaa !2
  %testMode = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %11, i32 0, i32 2
  store i32 0, i32* %testMode, align 4, !tbaa !12
  %12 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %ret, align 4, !tbaa !2
  %blockSizeId = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %12, i32 0, i32 3
  store i32 7, i32* %blockSizeId, align 4, !tbaa !13
  %13 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %ret, align 4, !tbaa !2
  %blockSize = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %13, i32 0, i32 4
  store i32 0, i32* %blockSize, align 4, !tbaa !14
  %14 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %ret, align 4, !tbaa !2
  %blockChecksum = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %14, i32 0, i32 5
  store i32 0, i32* %blockChecksum, align 4, !tbaa !15
  %15 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %ret, align 4, !tbaa !2
  %streamChecksum = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %15, i32 0, i32 6
  store i32 1, i32* %streamChecksum, align 4, !tbaa !16
  %16 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %ret, align 4, !tbaa !2
  %blockIndependence = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %16, i32 0, i32 7
  store i32 1, i32* %blockIndependence, align 4, !tbaa !17
  %17 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %ret, align 4, !tbaa !2
  %sparseFileSupport = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %17, i32 0, i32 8
  store i32 1, i32* %sparseFileSupport, align 4, !tbaa !18
  %18 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %ret, align 4, !tbaa !2
  %contentSizeFlag = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %18, i32 0, i32 9
  store i32 0, i32* %contentSizeFlag, align 4, !tbaa !19
  %19 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %ret, align 4, !tbaa !2
  %useDictionary = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %19, i32 0, i32 10
  store i32 0, i32* %useDictionary, align 4, !tbaa !20
  %20 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %ret, align 4, !tbaa !2
  %favorDecSpeed = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %20, i32 0, i32 11
  store i32 0, i32* %favorDecSpeed, align 4, !tbaa !21
  %21 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %ret, align 4, !tbaa !2
  %dictionaryFilename = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %21, i32 0, i32 12
  store i8* null, i8** %dictionaryFilename, align 4, !tbaa !22
  %22 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %ret, align 4, !tbaa !2
  %removeSrcFile = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %22, i32 0, i32 13
  store i32 0, i32* %removeSrcFile, align 4, !tbaa !23
  %23 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %ret, align 4, !tbaa !2
  %24 = bitcast %struct.LZ4IO_prefs_s** %ret to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #5
  ret %struct.LZ4IO_prefs_s* %23
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare i8* @malloc(i32) #2

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #2

; Function Attrs: noreturn
declare void @exit(i32) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @LZ4IO_freePreferences(%struct.LZ4IO_prefs_s* %prefs) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %0 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %1 = bitcast %struct.LZ4IO_prefs_s* %0 to i8*
  call void @free(i8* %1)
  ret void
}

declare void @free(i8*) #2

; Function Attrs: nounwind
define hidden i32 @LZ4IO_setDictionaryFilename(%struct.LZ4IO_prefs_s* %prefs, i8* %dictionaryFilename) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %dictionaryFilename.addr = alloca i8*, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i8* %dictionaryFilename, i8** %dictionaryFilename.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %dictionaryFilename.addr, align 4, !tbaa !2
  %1 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %dictionaryFilename1 = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %1, i32 0, i32 12
  store i8* %0, i8** %dictionaryFilename1, align 4, !tbaa !22
  %2 = load i8*, i8** %dictionaryFilename.addr, align 4, !tbaa !2
  %cmp = icmp ne i8* %2, null
  %conv = zext i1 %cmp to i32
  %3 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %useDictionary = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %3, i32 0, i32 10
  store i32 %conv, i32* %useDictionary, align 4, !tbaa !20
  %4 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %useDictionary2 = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %4, i32 0, i32 10
  %5 = load i32, i32* %useDictionary2, align 4, !tbaa !20
  ret i32 %5
}

; Function Attrs: nounwind
define hidden i32 @LZ4IO_setPassThrough(%struct.LZ4IO_prefs_s* %prefs, i32 %yes) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %yes.addr = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i32 %yes, i32* %yes.addr, align 4, !tbaa !6
  %0 = load i32, i32* %yes.addr, align 4, !tbaa !6
  %cmp = icmp ne i32 %0, 0
  %conv = zext i1 %cmp to i32
  %1 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %passThrough = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %1, i32 0, i32 0
  store i32 %conv, i32* %passThrough, align 4, !tbaa !8
  %2 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %passThrough1 = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %2, i32 0, i32 0
  %3 = load i32, i32* %passThrough1, align 4, !tbaa !8
  ret i32 %3
}

; Function Attrs: nounwind
define hidden i32 @LZ4IO_setOverwrite(%struct.LZ4IO_prefs_s* %prefs, i32 %yes) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %yes.addr = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i32 %yes, i32* %yes.addr, align 4, !tbaa !6
  %0 = load i32, i32* %yes.addr, align 4, !tbaa !6
  %cmp = icmp ne i32 %0, 0
  %conv = zext i1 %cmp to i32
  %1 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %overwrite = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %1, i32 0, i32 1
  store i32 %conv, i32* %overwrite, align 4, !tbaa !11
  %2 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %overwrite1 = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %2, i32 0, i32 1
  %3 = load i32, i32* %overwrite1, align 4, !tbaa !11
  ret i32 %3
}

; Function Attrs: nounwind
define hidden i32 @LZ4IO_setTestMode(%struct.LZ4IO_prefs_s* %prefs, i32 %yes) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %yes.addr = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i32 %yes, i32* %yes.addr, align 4, !tbaa !6
  %0 = load i32, i32* %yes.addr, align 4, !tbaa !6
  %cmp = icmp ne i32 %0, 0
  %conv = zext i1 %cmp to i32
  %1 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %testMode = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %1, i32 0, i32 2
  store i32 %conv, i32* %testMode, align 4, !tbaa !12
  %2 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %testMode1 = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %2, i32 0, i32 2
  %3 = load i32, i32* %testMode1, align 4, !tbaa !12
  ret i32 %3
}

; Function Attrs: nounwind
define hidden i32 @LZ4IO_setBlockSizeID(%struct.LZ4IO_prefs_s* %prefs, i32 %bsid) #0 {
entry:
  %retval = alloca i32, align 4
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %bsid.addr = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i32 %bsid, i32* %bsid.addr, align 4, !tbaa !6
  %0 = load i32, i32* %bsid.addr, align 4, !tbaa !6
  %cmp = icmp ult i32 %0, 4
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %bsid.addr, align 4, !tbaa !6
  %cmp1 = icmp ugt i32 %1, 7
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = load i32, i32* %bsid.addr, align 4, !tbaa !6
  %3 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %blockSizeId = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %3, i32 0, i32 3
  store i32 %2, i32* %blockSizeId, align 4, !tbaa !13
  %4 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %blockSizeId2 = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %4, i32 0, i32 3
  %5 = load i32, i32* %blockSizeId2, align 4, !tbaa !13
  %sub = sub i32 %5, 4
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* @LZ4IO_setBlockSizeID.blockSizeTable, i32 0, i32 %sub
  %6 = load i32, i32* %arrayidx, align 4, !tbaa !24
  %7 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %blockSize = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %7, i32 0, i32 4
  store i32 %6, i32* %blockSize, align 4, !tbaa !14
  %8 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %blockSize3 = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %8, i32 0, i32 4
  %9 = load i32, i32* %blockSize3, align 4, !tbaa !14
  store i32 %9, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: nounwind
define hidden i32 @LZ4IO_setBlockSize(%struct.LZ4IO_prefs_s* %prefs, i32 %blockSize) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %blockSize.addr = alloca i32, align 4
  %bsid = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i32 %blockSize, i32* %blockSize.addr, align 4, !tbaa !24
  %0 = bitcast i32* %bsid to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %bsid, align 4, !tbaa !6
  %1 = load i32, i32* %blockSize.addr, align 4, !tbaa !24
  %cmp = icmp ult i32 %1, 32
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 32, i32* %blockSize.addr, align 4, !tbaa !24
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load i32, i32* %blockSize.addr, align 4, !tbaa !24
  %cmp1 = icmp ugt i32 %2, 4194304
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 4194304, i32* %blockSize.addr, align 4, !tbaa !24
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %3 = load i32, i32* %blockSize.addr, align 4, !tbaa !24
  %4 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %blockSize4 = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %4, i32 0, i32 4
  store i32 %3, i32* %blockSize4, align 4, !tbaa !14
  %5 = load i32, i32* %blockSize.addr, align 4, !tbaa !24
  %dec = add i32 %5, -1
  store i32 %dec, i32* %blockSize.addr, align 4, !tbaa !24
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end3
  %6 = load i32, i32* %blockSize.addr, align 4, !tbaa !24
  %shr = lshr i32 %6, 2
  store i32 %shr, i32* %blockSize.addr, align 4, !tbaa !24
  %tobool = icmp ne i32 %shr, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load i32, i32* %bsid, align 4, !tbaa !6
  %inc = add i32 %7, 1
  store i32 %inc, i32* %bsid, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %8 = load i32, i32* %bsid, align 4, !tbaa !6
  %cmp5 = icmp ult i32 %8, 7
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %while.end
  store i32 7, i32* %bsid, align 4, !tbaa !6
  br label %if.end7

if.end7:                                          ; preds = %if.then6, %while.end
  %9 = load i32, i32* %bsid, align 4, !tbaa !6
  %sub = sub i32 %9, 3
  %10 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %blockSizeId = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %10, i32 0, i32 3
  store i32 %sub, i32* %blockSizeId, align 4, !tbaa !13
  %11 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %blockSize8 = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %11, i32 0, i32 4
  %12 = load i32, i32* %blockSize8, align 4, !tbaa !14
  %13 = bitcast i32* %bsid to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  ret i32 %12
}

; Function Attrs: nounwind
define hidden i32 @LZ4IO_setBlockMode(%struct.LZ4IO_prefs_s* %prefs, i32 %blockMode) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %blockMode.addr = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i32 %blockMode, i32* %blockMode.addr, align 4, !tbaa !25
  %0 = load i32, i32* %blockMode.addr, align 4, !tbaa !25
  %cmp = icmp eq i32 %0, 1
  %conv = zext i1 %cmp to i32
  %1 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %blockIndependence = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %1, i32 0, i32 7
  store i32 %conv, i32* %blockIndependence, align 4, !tbaa !17
  %2 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %blockIndependence1 = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %2, i32 0, i32 7
  %3 = load i32, i32* %blockIndependence1, align 4, !tbaa !17
  ret i32 %3
}

; Function Attrs: nounwind
define hidden i32 @LZ4IO_setBlockChecksumMode(%struct.LZ4IO_prefs_s* %prefs, i32 %enable) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %enable.addr = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i32 %enable, i32* %enable.addr, align 4, !tbaa !6
  %0 = load i32, i32* %enable.addr, align 4, !tbaa !6
  %cmp = icmp ne i32 %0, 0
  %conv = zext i1 %cmp to i32
  %1 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %blockChecksum = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %1, i32 0, i32 5
  store i32 %conv, i32* %blockChecksum, align 4, !tbaa !15
  %2 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %blockChecksum1 = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %2, i32 0, i32 5
  %3 = load i32, i32* %blockChecksum1, align 4, !tbaa !15
  ret i32 %3
}

; Function Attrs: nounwind
define hidden i32 @LZ4IO_setStreamChecksumMode(%struct.LZ4IO_prefs_s* %prefs, i32 %enable) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %enable.addr = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i32 %enable, i32* %enable.addr, align 4, !tbaa !6
  %0 = load i32, i32* %enable.addr, align 4, !tbaa !6
  %cmp = icmp ne i32 %0, 0
  %conv = zext i1 %cmp to i32
  %1 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %streamChecksum = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %1, i32 0, i32 6
  store i32 %conv, i32* %streamChecksum, align 4, !tbaa !16
  %2 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %streamChecksum1 = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %2, i32 0, i32 6
  %3 = load i32, i32* %streamChecksum1, align 4, !tbaa !16
  ret i32 %3
}

; Function Attrs: nounwind
define hidden i32 @LZ4IO_setNotificationLevel(i32 %level) #0 {
entry:
  %level.addr = alloca i32, align 4
  store i32 %level, i32* %level.addr, align 4, !tbaa !6
  %0 = load i32, i32* %level.addr, align 4, !tbaa !6
  store i32 %0, i32* @g_displayLevel, align 4, !tbaa !6
  %1 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  ret i32 %1
}

; Function Attrs: nounwind
define hidden i32 @LZ4IO_setSparseFile(%struct.LZ4IO_prefs_s* %prefs, i32 %enable) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %enable.addr = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i32 %enable, i32* %enable.addr, align 4, !tbaa !6
  %0 = load i32, i32* %enable.addr, align 4, !tbaa !6
  %cmp = icmp ne i32 %0, 0
  %conv = zext i1 %cmp to i32
  %1 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %sparseFileSupport = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %1, i32 0, i32 8
  store i32 %conv, i32* %sparseFileSupport, align 4, !tbaa !18
  %2 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %sparseFileSupport1 = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %2, i32 0, i32 8
  %3 = load i32, i32* %sparseFileSupport1, align 4, !tbaa !18
  ret i32 %3
}

; Function Attrs: nounwind
define hidden i32 @LZ4IO_setContentSize(%struct.LZ4IO_prefs_s* %prefs, i32 %enable) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %enable.addr = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i32 %enable, i32* %enable.addr, align 4, !tbaa !6
  %0 = load i32, i32* %enable.addr, align 4, !tbaa !6
  %cmp = icmp ne i32 %0, 0
  %conv = zext i1 %cmp to i32
  %1 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %contentSizeFlag = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %1, i32 0, i32 9
  store i32 %conv, i32* %contentSizeFlag, align 4, !tbaa !19
  %2 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %contentSizeFlag1 = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %2, i32 0, i32 9
  %3 = load i32, i32* %contentSizeFlag1, align 4, !tbaa !19
  ret i32 %3
}

; Function Attrs: nounwind
define hidden void @LZ4IO_favorDecSpeed(%struct.LZ4IO_prefs_s* %prefs, i32 %favor) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %favor.addr = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i32 %favor, i32* %favor.addr, align 4, !tbaa !6
  %0 = load i32, i32* %favor.addr, align 4, !tbaa !6
  %cmp = icmp ne i32 %0, 0
  %conv = zext i1 %cmp to i32
  %1 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %favorDecSpeed = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %1, i32 0, i32 11
  store i32 %conv, i32* %favorDecSpeed, align 4, !tbaa !21
  ret void
}

; Function Attrs: nounwind
define hidden void @LZ4IO_setRemoveSrcFile(%struct.LZ4IO_prefs_s* %prefs, i32 %flag) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %flag.addr = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i32 %flag, i32* %flag.addr, align 4, !tbaa !6
  %0 = load i32, i32* %flag.addr, align 4, !tbaa !6
  %cmp = icmp ugt i32 %0, 0
  %conv = zext i1 %cmp to i32
  %1 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %removeSrcFile = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %1, i32 0, i32 13
  store i32 %conv, i32* %removeSrcFile, align 4, !tbaa !23
  ret void
}

; Function Attrs: nounwind
define hidden i32 @LZ4IO_compressFilename_Legacy(%struct.LZ4IO_prefs_s* %prefs, i8* %input_filename, i8* %output_filename, i32 %compressionlevel) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %input_filename.addr = alloca i8*, align 4
  %output_filename.addr = alloca i8*, align 4
  %compressionlevel.addr = alloca i32, align 4
  %compressionFunction = alloca i32 (i8*, i8*, i32, i32, i32)*, align 4
  %filesize = alloca i64, align 8
  %compressedfilesize = alloca i64, align 8
  %in_buff = alloca i8*, align 4
  %out_buff = alloca i8*, align 4
  %outBuffSize = alloca i32, align 4
  %finput = alloca %struct._IO_FILE*, align 4
  %foutput = alloca %struct._IO_FILE*, align 4
  %clockEnd = alloca i32, align 4
  %clockStart = alloca i32, align 4
  %writeSize = alloca i32, align 4
  %outSize = alloca i32, align 4
  %inSize = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %writeSize108 = alloca i32, align 4
  %seconds = alloca double, align 8
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i8* %input_filename, i8** %input_filename.addr, align 4, !tbaa !2
  store i8* %output_filename, i8** %output_filename.addr, align 4, !tbaa !2
  store i32 %compressionlevel, i32* %compressionlevel.addr, align 4, !tbaa !6
  %0 = bitcast i32 (i8*, i8*, i32, i32, i32)** %compressionFunction to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %compressionlevel.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, 3
  %2 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 (i8*, i8*, i32, i32, i32)* @LZ4IO_LZ4_compress, i32 (i8*, i8*, i32, i32, i32)* @LZ4_compress_HC
  store i32 (i8*, i8*, i32, i32, i32)* %cond, i32 (i8*, i8*, i32, i32, i32)** %compressionFunction, align 4, !tbaa !2
  %3 = bitcast i64* %filesize to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #5
  store i64 0, i64* %filesize, align 8, !tbaa !26
  %4 = bitcast i64* %compressedfilesize to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #5
  store i64 4, i64* %compressedfilesize, align 8, !tbaa !26
  %5 = bitcast i8** %in_buff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = bitcast i8** %out_buff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = bitcast i32* %outBuffSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %call = call i32 @LZ4_compressBound(i32 8388608)
  store i32 %call, i32* %outBuffSize, align 4, !tbaa !6
  %8 = bitcast %struct._IO_FILE** %finput to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load i8*, i8** %input_filename.addr, align 4, !tbaa !2
  %call1 = call %struct._IO_FILE* @LZ4IO_openSrcFile(i8* %9)
  store %struct._IO_FILE* %call1, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %10 = bitcast %struct._IO_FILE** %foutput to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = bitcast i32* %clockEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %12 = bitcast i32* %clockStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %call2 = call i32 @clock()
  store i32 %call2, i32* %clockStart, align 4, !tbaa !24
  %13 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %cmp3 = icmp eq %struct._IO_FILE* %13, null
  br i1 %cmp3, label %if.then, label %if.end15

if.then:                                          ; preds = %entry
  %14 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp4 = icmp sge i32 %14, 1
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.then
  %15 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call6 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %15, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 20)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.then
  %16 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp7 = icmp sge i32 %16, 1
  br i1 %cmp7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %if.end
  %17 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %18 = load i8*, i8** %input_filename.addr, align 4, !tbaa !2
  %call9 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %17, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.3, i32 0, i32 0), i8* %18)
  br label %if.end10

if.end10:                                         ; preds = %if.then8, %if.end
  %19 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp11 = icmp sge i32 %19, 1
  br i1 %cmp11, label %if.then12, label %if.end14

if.then12:                                        ; preds = %if.end10
  %20 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call13 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %20, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end14

if.end14:                                         ; preds = %if.then12, %if.end10
  call void @exit(i32 20) #6
  unreachable

if.end15:                                         ; preds = %entry
  %21 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %22 = load i8*, i8** %output_filename.addr, align 4, !tbaa !2
  %call16 = call %struct._IO_FILE* @LZ4IO_openDstFile(%struct.LZ4IO_prefs_s* %21, i8* %22)
  store %struct._IO_FILE* %call16, %struct._IO_FILE** %foutput, align 4, !tbaa !2
  %23 = load %struct._IO_FILE*, %struct._IO_FILE** %foutput, align 4, !tbaa !2
  %cmp17 = icmp eq %struct._IO_FILE* %23, null
  br i1 %cmp17, label %if.then18, label %if.end32

if.then18:                                        ; preds = %if.end15
  %24 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %call19 = call i32 @fclose(%struct._IO_FILE* %24)
  %25 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp20 = icmp sge i32 %25, 1
  br i1 %cmp20, label %if.then21, label %if.end23

if.then21:                                        ; preds = %if.then18
  %26 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call22 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %26, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 20)
  br label %if.end23

if.end23:                                         ; preds = %if.then21, %if.then18
  %27 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp24 = icmp sge i32 %27, 1
  br i1 %cmp24, label %if.then25, label %if.end27

if.then25:                                        ; preds = %if.end23
  %28 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %29 = load i8*, i8** %input_filename.addr, align 4, !tbaa !2
  %call26 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %28, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.3, i32 0, i32 0), i8* %29)
  br label %if.end27

if.end27:                                         ; preds = %if.then25, %if.end23
  %30 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp28 = icmp sge i32 %30, 1
  br i1 %cmp28, label %if.then29, label %if.end31

if.then29:                                        ; preds = %if.end27
  %31 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call30 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %31, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end31

if.end31:                                         ; preds = %if.then29, %if.end27
  call void @exit(i32 20) #6
  unreachable

if.end32:                                         ; preds = %if.end15
  %call33 = call i8* @malloc(i32 8388608)
  store i8* %call33, i8** %in_buff, align 4, !tbaa !2
  %32 = load i32, i32* %outBuffSize, align 4, !tbaa !6
  %add = add i32 %32, 4
  %call34 = call i8* @malloc(i32 %add)
  store i8* %call34, i8** %out_buff, align 4, !tbaa !2
  %33 = load i8*, i8** %in_buff, align 4, !tbaa !2
  %tobool = icmp ne i8* %33, null
  br i1 %tobool, label %lor.lhs.false, label %if.then36

lor.lhs.false:                                    ; preds = %if.end32
  %34 = load i8*, i8** %out_buff, align 4, !tbaa !2
  %tobool35 = icmp ne i8* %34, null
  br i1 %tobool35, label %if.end49, label %if.then36

if.then36:                                        ; preds = %lor.lhs.false, %if.end32
  %35 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp37 = icmp sge i32 %35, 1
  br i1 %cmp37, label %if.then38, label %if.end40

if.then38:                                        ; preds = %if.then36
  %36 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call39 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %36, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 21)
  br label %if.end40

if.end40:                                         ; preds = %if.then38, %if.then36
  %37 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp41 = icmp sge i32 %37, 1
  br i1 %cmp41, label %if.then42, label %if.end44

if.then42:                                        ; preds = %if.end40
  %38 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call43 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %38, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.1, i32 0, i32 0))
  br label %if.end44

if.end44:                                         ; preds = %if.then42, %if.end40
  %39 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp45 = icmp sge i32 %39, 1
  br i1 %cmp45, label %if.then46, label %if.end48

if.then46:                                        ; preds = %if.end44
  %40 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call47 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %40, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end48

if.end48:                                         ; preds = %if.then46, %if.end44
  call void @exit(i32 21) #6
  unreachable

if.end49:                                         ; preds = %lor.lhs.false
  %41 = load i8*, i8** %out_buff, align 4, !tbaa !2
  call void @LZ4IO_writeLE32(i8* %41, i32 407642370)
  %42 = bitcast i32* %writeSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #5
  %43 = load i8*, i8** %out_buff, align 4, !tbaa !2
  %44 = load %struct._IO_FILE*, %struct._IO_FILE** %foutput, align 4, !tbaa !2
  %call50 = call i32 @fwrite(i8* %43, i32 1, i32 4, %struct._IO_FILE* %44)
  store i32 %call50, i32* %writeSize, align 4, !tbaa !24
  %45 = load i32, i32* %writeSize, align 4, !tbaa !24
  %cmp51 = icmp ne i32 %45, 4
  br i1 %cmp51, label %if.then52, label %if.end65

if.then52:                                        ; preds = %if.end49
  %46 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp53 = icmp sge i32 %46, 1
  br i1 %cmp53, label %if.then54, label %if.end56

if.then54:                                        ; preds = %if.then52
  %47 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call55 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %47, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 22)
  br label %if.end56

if.end56:                                         ; preds = %if.then54, %if.then52
  %48 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp57 = icmp sge i32 %48, 1
  br i1 %cmp57, label %if.then58, label %if.end60

if.then58:                                        ; preds = %if.end56
  %49 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call59 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %49, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end60

if.end60:                                         ; preds = %if.then58, %if.end56
  %50 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp61 = icmp sge i32 %50, 1
  br i1 %cmp61, label %if.then62, label %if.end64

if.then62:                                        ; preds = %if.end60
  %51 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call63 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %51, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end64

if.end64:                                         ; preds = %if.then62, %if.end60
  call void @exit(i32 22) #6
  unreachable

if.end65:                                         ; preds = %if.end49
  %52 = bitcast i32* %writeSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #5
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %if.end65
  br label %while.body

while.body:                                       ; preds = %while.cond
  %53 = bitcast i32* %outSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #5
  %54 = bitcast i32* %inSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #5
  %55 = load i8*, i8** %in_buff, align 4, !tbaa !2
  %56 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %call66 = call i32 @fread(i8* %55, i32 1, i32 8388608, %struct._IO_FILE* %56)
  store i32 %call66, i32* %inSize, align 4, !tbaa !24
  %57 = load i32, i32* %inSize, align 4, !tbaa !24
  %cmp67 = icmp ule i32 %57, 8388608
  br i1 %cmp67, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %while.body
  call void @__assert_fail(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.5, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.6, i32 0, i32 0), i32 437, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @__func__.LZ4IO_compressFilename_Legacy, i32 0, i32 0)) #6
  unreachable

58:                                               ; No predecessors!
  br label %lor.end

lor.end:                                          ; preds = %58, %while.body
  %59 = phi i1 [ true, %while.body ], [ false, %58 ]
  %lor.ext = zext i1 %59 to i32
  %60 = load i32, i32* %inSize, align 4, !tbaa !24
  %cmp68 = icmp eq i32 %60, 0
  br i1 %cmp68, label %if.then69, label %if.end70

if.then69:                                        ; preds = %lor.end
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end70:                                         ; preds = %lor.end
  %61 = load i32, i32* %inSize, align 4, !tbaa !24
  %conv = zext i32 %61 to i64
  %62 = load i64, i64* %filesize, align 8, !tbaa !26
  %add71 = add i64 %62, %conv
  store i64 %add71, i64* %filesize, align 8, !tbaa !26
  %63 = load i32 (i8*, i8*, i32, i32, i32)*, i32 (i8*, i8*, i32, i32, i32)** %compressionFunction, align 4, !tbaa !2
  %64 = load i8*, i8** %in_buff, align 4, !tbaa !2
  %65 = load i8*, i8** %out_buff, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %65, i32 4
  %66 = load i32, i32* %inSize, align 4, !tbaa !24
  %67 = load i32, i32* %outBuffSize, align 4, !tbaa !6
  %68 = load i32, i32* %compressionlevel.addr, align 4, !tbaa !6
  %call72 = call i32 %63(i8* %64, i8* %add.ptr, i32 %66, i32 %67, i32 %68)
  store i32 %call72, i32* %outSize, align 4, !tbaa !6
  %69 = load i32, i32* %outSize, align 4, !tbaa !6
  %add73 = add nsw i32 %69, 4
  %conv74 = sext i32 %add73 to i64
  %70 = load i64, i64* %compressedfilesize, align 8, !tbaa !26
  %add75 = add i64 %70, %conv74
  store i64 %add75, i64* %compressedfilesize, align 8, !tbaa !26
  %71 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp76 = icmp sge i32 %71, 2
  br i1 %cmp76, label %if.then78, label %if.end97

if.then78:                                        ; preds = %if.end70
  %call79 = call i32 @clock()
  %72 = load i32, i32* @g_time, align 4, !tbaa !24
  %sub = sub nsw i32 %call79, %72
  %cmp80 = icmp sgt i32 %sub, 166666
  br i1 %cmp80, label %if.then85, label %lor.lhs.false82

lor.lhs.false82:                                  ; preds = %if.then78
  %73 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp83 = icmp sge i32 %73, 4
  br i1 %cmp83, label %if.then85, label %if.end96

if.then85:                                        ; preds = %lor.lhs.false82, %if.then78
  %call86 = call i32 @clock()
  store i32 %call86, i32* @g_time, align 4, !tbaa !24
  %74 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %75 = load i64, i64* %filesize, align 8, !tbaa !26
  %shr = lshr i64 %75, 20
  %conv87 = trunc i64 %shr to i32
  %76 = load i64, i64* %compressedfilesize, align 8, !tbaa !26
  %conv88 = uitofp i64 %76 to double
  %77 = load i64, i64* %filesize, align 8, !tbaa !26
  %conv89 = uitofp i64 %77 to double
  %div = fdiv double %conv88, %conv89
  %mul = fmul double %div, 1.000000e+02
  %call90 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %74, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.7, i32 0, i32 0), i32 %conv87, double %mul)
  %78 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp91 = icmp sge i32 %78, 4
  br i1 %cmp91, label %if.then93, label %if.end95

if.then93:                                        ; preds = %if.then85
  %79 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call94 = call i32 @fflush(%struct._IO_FILE* %79)
  br label %if.end95

if.end95:                                         ; preds = %if.then93, %if.then85
  br label %if.end96

if.end96:                                         ; preds = %if.end95, %lor.lhs.false82
  br label %if.end97

if.end97:                                         ; preds = %if.end96, %if.end70
  %80 = load i32, i32* %outSize, align 4, !tbaa !6
  %cmp98 = icmp sgt i32 %80, 0
  br i1 %cmp98, label %lor.end101, label %lor.rhs100

lor.rhs100:                                       ; preds = %if.end97
  call void @__assert_fail(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.8, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.6, i32 0, i32 0), i32 448, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @__func__.LZ4IO_compressFilename_Legacy, i32 0, i32 0)) #6
  unreachable

81:                                               ; No predecessors!
  br label %lor.end101

lor.end101:                                       ; preds = %81, %if.end97
  %82 = phi i1 [ true, %if.end97 ], [ false, %81 ]
  %lor.ext102 = zext i1 %82 to i32
  %83 = load i32, i32* %outSize, align 4, !tbaa !6
  %84 = load i32, i32* %outBuffSize, align 4, !tbaa !6
  %cmp103 = icmp slt i32 %83, %84
  br i1 %cmp103, label %lor.end106, label %lor.rhs105

lor.rhs105:                                       ; preds = %lor.end101
  call void @__assert_fail(i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.9, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.6, i32 0, i32 0), i32 449, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @__func__.LZ4IO_compressFilename_Legacy, i32 0, i32 0)) #6
  unreachable

85:                                               ; No predecessors!
  br label %lor.end106

lor.end106:                                       ; preds = %85, %lor.end101
  %86 = phi i1 [ true, %lor.end101 ], [ false, %85 ]
  %lor.ext107 = zext i1 %86 to i32
  %87 = load i8*, i8** %out_buff, align 4, !tbaa !2
  %88 = load i32, i32* %outSize, align 4, !tbaa !6
  call void @LZ4IO_writeLE32(i8* %87, i32 %88)
  %89 = bitcast i32* %writeSize108 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #5
  %90 = load i8*, i8** %out_buff, align 4, !tbaa !2
  %91 = load i32, i32* %outSize, align 4, !tbaa !6
  %add109 = add nsw i32 %91, 4
  %92 = load %struct._IO_FILE*, %struct._IO_FILE** %foutput, align 4, !tbaa !2
  %call110 = call i32 @fwrite(i8* %90, i32 1, i32 %add109, %struct._IO_FILE* %92)
  store i32 %call110, i32* %writeSize108, align 4, !tbaa !24
  %93 = load i32, i32* %writeSize108, align 4, !tbaa !24
  %94 = load i32, i32* %outSize, align 4, !tbaa !6
  %add111 = add nsw i32 %94, 4
  %cmp112 = icmp ne i32 %93, %add111
  br i1 %cmp112, label %if.then114, label %if.end130

if.then114:                                       ; preds = %lor.end106
  %95 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp115 = icmp sge i32 %95, 1
  br i1 %cmp115, label %if.then117, label %if.end119

if.then117:                                       ; preds = %if.then114
  %96 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call118 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %96, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 24)
  br label %if.end119

if.end119:                                        ; preds = %if.then117, %if.then114
  %97 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp120 = icmp sge i32 %97, 1
  br i1 %cmp120, label %if.then122, label %if.end124

if.then122:                                       ; preds = %if.end119
  %98 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call123 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %98, i8* getelementptr inbounds ([44 x i8], [44 x i8]* @.str.10, i32 0, i32 0))
  br label %if.end124

if.end124:                                        ; preds = %if.then122, %if.end119
  %99 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp125 = icmp sge i32 %99, 1
  br i1 %cmp125, label %if.then127, label %if.end129

if.then127:                                       ; preds = %if.end124
  %100 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call128 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %100, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end129

if.end129:                                        ; preds = %if.then127, %if.end124
  call void @exit(i32 24) #6
  unreachable

if.end130:                                        ; preds = %lor.end106
  %101 = bitcast i32* %writeSize108 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #5
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end130, %if.then69
  %102 = bitcast i32* %inSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #5
  %103 = bitcast i32* %outSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 3, label %while.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %cleanup
  %104 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %call132 = call i32 @ferror(%struct._IO_FILE* %104)
  %tobool133 = icmp ne i32 %call132, 0
  br i1 %tobool133, label %if.then134, label %if.end150

if.then134:                                       ; preds = %while.end
  %105 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp135 = icmp sge i32 %105, 1
  br i1 %cmp135, label %if.then137, label %if.end139

if.then137:                                       ; preds = %if.then134
  %106 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call138 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %106, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 25)
  br label %if.end139

if.end139:                                        ; preds = %if.then137, %if.then134
  %107 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp140 = icmp sge i32 %107, 1
  br i1 %cmp140, label %if.then142, label %if.end144

if.then142:                                       ; preds = %if.end139
  %108 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %109 = load i8*, i8** %input_filename.addr, align 4, !tbaa !2
  %call143 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %108, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.11, i32 0, i32 0), i8* %109)
  br label %if.end144

if.end144:                                        ; preds = %if.then142, %if.end139
  %110 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp145 = icmp sge i32 %110, 1
  br i1 %cmp145, label %if.then147, label %if.end149

if.then147:                                       ; preds = %if.end144
  %111 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call148 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %111, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end149

if.end149:                                        ; preds = %if.then147, %if.end144
  call void @exit(i32 25) #6
  unreachable

if.end150:                                        ; preds = %while.end
  %call151 = call i32 @clock()
  store i32 %call151, i32* %clockEnd, align 4, !tbaa !24
  %112 = load i32, i32* %clockEnd, align 4, !tbaa !24
  %113 = load i32, i32* %clockStart, align 4, !tbaa !24
  %cmp152 = icmp eq i32 %112, %113
  br i1 %cmp152, label %if.then154, label %if.end156

if.then154:                                       ; preds = %if.end150
  %114 = load i32, i32* %clockEnd, align 4, !tbaa !24
  %add155 = add nsw i32 %114, 1
  store i32 %add155, i32* %clockEnd, align 4, !tbaa !24
  br label %if.end156

if.end156:                                        ; preds = %if.then154, %if.end150
  %115 = load i64, i64* %filesize, align 8, !tbaa !26
  %tobool157 = icmp ne i64 %115, 0
  %lnot = xor i1 %tobool157, true
  %lnot.ext = zext i1 %lnot to i32
  %conv158 = sext i32 %lnot.ext to i64
  %116 = load i64, i64* %filesize, align 8, !tbaa !26
  %add159 = add i64 %116, %conv158
  store i64 %add159, i64* %filesize, align 8, !tbaa !26
  %117 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp160 = icmp sge i32 %117, 2
  br i1 %cmp160, label %if.then162, label %if.end164

if.then162:                                       ; preds = %if.end156
  %118 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call163 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %118, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.12, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @.str.13, i32 0, i32 0))
  br label %if.end164

if.end164:                                        ; preds = %if.then162, %if.end156
  %119 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp165 = icmp sge i32 %119, 2
  br i1 %cmp165, label %if.then167, label %if.end173

if.then167:                                       ; preds = %if.end164
  %120 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %121 = load i64, i64* %filesize, align 8, !tbaa !26
  %122 = load i64, i64* %compressedfilesize, align 8, !tbaa !26
  %123 = load i64, i64* %compressedfilesize, align 8, !tbaa !26
  %conv168 = uitofp i64 %123 to double
  %124 = load i64, i64* %filesize, align 8, !tbaa !26
  %conv169 = uitofp i64 %124 to double
  %div170 = fdiv double %conv168, %conv169
  %mul171 = fmul double %div170, 1.000000e+02
  %call172 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %120, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @.str.14, i32 0, i32 0), i64 %121, i64 %122, double %mul171)
  br label %if.end173

if.end173:                                        ; preds = %if.then167, %if.end164
  %125 = bitcast double* %seconds to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %125) #5
  %126 = load i32, i32* %clockEnd, align 4, !tbaa !24
  %127 = load i32, i32* %clockStart, align 4, !tbaa !24
  %sub174 = sub nsw i32 %126, %127
  %conv175 = sitofp i32 %sub174 to double
  %div176 = fdiv double %conv175, 1.000000e+06
  store double %div176, double* %seconds, align 8, !tbaa !28
  %128 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp177 = icmp sge i32 %128, 4
  br i1 %cmp177, label %if.then179, label %if.end185

if.then179:                                       ; preds = %if.end173
  %129 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %130 = load double, double* %seconds, align 8, !tbaa !28
  %131 = load i64, i64* %filesize, align 8, !tbaa !26
  %conv180 = uitofp i64 %131 to double
  %132 = load double, double* %seconds, align 8, !tbaa !28
  %div181 = fdiv double %conv180, %132
  %div182 = fdiv double %div181, 1.024000e+03
  %div183 = fdiv double %div182, 1.024000e+03
  %call184 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %129, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.15, i32 0, i32 0), double %130, double %div183)
  br label %if.end185

if.end185:                                        ; preds = %if.then179, %if.end173
  %133 = bitcast double* %seconds to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %133) #5
  %134 = load i8*, i8** %in_buff, align 4, !tbaa !2
  call void @free(i8* %134)
  %135 = load i8*, i8** %out_buff, align 4, !tbaa !2
  call void @free(i8* %135)
  %136 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %call186 = call i32 @fclose(%struct._IO_FILE* %136)
  %137 = load i8*, i8** %output_filename.addr, align 4, !tbaa !2
  %call187 = call i32 @strcmp(i8* %137, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0))
  %tobool188 = icmp ne i32 %call187, 0
  br i1 %tobool188, label %if.then189, label %if.end191

if.then189:                                       ; preds = %if.end185
  %138 = load %struct._IO_FILE*, %struct._IO_FILE** %foutput, align 4, !tbaa !2
  %call190 = call i32 @fclose(%struct._IO_FILE* %138)
  br label %if.end191

if.end191:                                        ; preds = %if.then189, %if.end185
  store i32 1, i32* %cleanup.dest.slot, align 4
  %139 = bitcast i32* %clockStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #5
  %140 = bitcast i32* %clockEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #5
  %141 = bitcast %struct._IO_FILE** %foutput to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #5
  %142 = bitcast %struct._IO_FILE** %finput to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #5
  %143 = bitcast i32* %outBuffSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #5
  %144 = bitcast i8** %out_buff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #5
  %145 = bitcast i8** %in_buff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #5
  %146 = bitcast i64* %compressedfilesize to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %146) #5
  %147 = bitcast i64* %filesize to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %147) #5
  %148 = bitcast i32 (i8*, i8*, i32, i32, i32)** %compressionFunction to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #5
  ret i32 0

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal i32 @LZ4IO_LZ4_compress(i8* %src, i8* %dst, i32 %srcSize, i32 %dstSize, i32 %cLevel) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %dstSize.addr = alloca i32, align 4
  %cLevel.addr = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !6
  store i32 %dstSize, i32* %dstSize.addr, align 4, !tbaa !6
  store i32 %cLevel, i32* %cLevel.addr, align 4, !tbaa !6
  %0 = load i32, i32* %cLevel.addr, align 4, !tbaa !6
  %1 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %2 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %3 = load i32, i32* %srcSize.addr, align 4, !tbaa !6
  %4 = load i32, i32* %dstSize.addr, align 4, !tbaa !6
  %call = call i32 @LZ4_compress_fast(i8* %1, i8* %2, i32 %3, i32 %4, i32 1)
  ret i32 %call
}

declare i32 @LZ4_compress_HC(i8*, i8*, i32, i32, i32) #2

declare i32 @LZ4_compressBound(i32) #2

; Function Attrs: nounwind
define internal %struct._IO_FILE* @LZ4IO_openSrcFile(i8* %srcFileName) #0 {
entry:
  %srcFileName.addr = alloca i8*, align 4
  %f = alloca %struct._IO_FILE*, align 4
  store i8* %srcFileName, i8** %srcFileName.addr, align 4, !tbaa !2
  %0 = bitcast %struct._IO_FILE** %f to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %srcFileName.addr, align 4, !tbaa !2
  %call = call i32 @strcmp(i8* %1, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @stdinmark, i32 0, i32 0))
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  %2 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp = icmp sge i32 %2, 4
  br i1 %cmp, label %if.then1, label %if.end

if.then1:                                         ; preds = %if.then
  %3 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call2 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %3, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.41, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then1, %if.then
  %4 = load %struct._IO_FILE*, %struct._IO_FILE** @stdin, align 4, !tbaa !2
  store %struct._IO_FILE* %4, %struct._IO_FILE** %f, align 4, !tbaa !2
  br label %if.end13

if.else:                                          ; preds = %entry
  %5 = load i8*, i8** %srcFileName.addr, align 4, !tbaa !2
  %call3 = call %struct._IO_FILE* @fopen(i8* %5, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.42, i32 0, i32 0))
  store %struct._IO_FILE* %call3, %struct._IO_FILE** %f, align 4, !tbaa !2
  %6 = load %struct._IO_FILE*, %struct._IO_FILE** %f, align 4, !tbaa !2
  %cmp4 = icmp eq %struct._IO_FILE* %6, null
  br i1 %cmp4, label %if.then5, label %if.end12

if.then5:                                         ; preds = %if.else
  %7 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp6 = icmp sge i32 %7, 1
  br i1 %cmp6, label %if.then7, label %if.end11

if.then7:                                         ; preds = %if.then5
  %8 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %9 = load i8*, i8** %srcFileName.addr, align 4, !tbaa !2
  %call8 = call i32* @__errno_location()
  %10 = load i32, i32* %call8, align 4, !tbaa !6
  %call9 = call i8* @strerror(i32 %10)
  %call10 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %8, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.43, i32 0, i32 0), i8* %9, i8* %call9)
  br label %if.end11

if.end11:                                         ; preds = %if.then7, %if.then5
  br label %if.end12

if.end12:                                         ; preds = %if.end11, %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end
  %11 = load %struct._IO_FILE*, %struct._IO_FILE** %f, align 4, !tbaa !2
  %12 = bitcast %struct._IO_FILE** %f to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #5
  ret %struct._IO_FILE* %11
}

declare i32 @clock() #2

; Function Attrs: nounwind
define internal %struct._IO_FILE* @LZ4IO_openDstFile(%struct.LZ4IO_prefs_s* %prefs, i8* %dstFileName) #0 {
entry:
  %retval = alloca %struct._IO_FILE*, align 4
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %dstFileName.addr = alloca i8*, align 4
  %f = alloca %struct._IO_FILE*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ch = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i8* %dstFileName, i8** %dstFileName.addr, align 4, !tbaa !2
  %0 = bitcast %struct._IO_FILE** %f to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %dstFileName.addr, align 4, !tbaa !2
  %cmp = icmp ne i8* %1, null
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  call void @__assert_fail(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.44, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.6, i32 0, i32 0), i32 333, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @__func__.LZ4IO_openDstFile, i32 0, i32 0)) #6
  unreachable

2:                                                ; No predecessors!
  br label %lor.end

lor.end:                                          ; preds = %2, %entry
  %3 = phi i1 [ true, %entry ], [ false, %2 ]
  %lor.ext = zext i1 %3 to i32
  %4 = load i8*, i8** %dstFileName.addr, align 4, !tbaa !2
  %call = call i32 @strcmp(i8* %4, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0))
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %lor.end
  %5 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp1 = icmp sge i32 %5, 4
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %6 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call3 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %6, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.45, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  %7 = load %struct._IO_FILE*, %struct._IO_FILE** @stdout, align 4, !tbaa !2
  store %struct._IO_FILE* %7, %struct._IO_FILE** %f, align 4, !tbaa !2
  %8 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %sparseFileSupport = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %8, i32 0, i32 8
  %9 = load i32, i32* %sparseFileSupport, align 4, !tbaa !18
  %cmp4 = icmp eq i32 %9, 1
  br i1 %cmp4, label %if.then5, label %if.end11

if.then5:                                         ; preds = %if.end
  %10 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %sparseFileSupport6 = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %10, i32 0, i32 8
  store i32 0, i32* %sparseFileSupport6, align 4, !tbaa !18
  %11 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp7 = icmp sge i32 %11, 4
  br i1 %cmp7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %if.then5
  %12 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call9 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %12, i8* getelementptr inbounds ([73 x i8], [73 x i8]* @.str.46, i32 0, i32 0))
  br label %if.end10

if.end10:                                         ; preds = %if.then8, %if.then5
  br label %if.end11

if.end11:                                         ; preds = %if.end10, %if.end
  br label %if.end47

if.else:                                          ; preds = %lor.end
  %13 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %overwrite = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %13, i32 0, i32 1
  %14 = load i32, i32* %overwrite, align 4, !tbaa !11
  %tobool12 = icmp ne i32 %14, 0
  br i1 %tobool12, label %if.end36, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.else
  %15 = load i8*, i8** %dstFileName.addr, align 4, !tbaa !2
  %call13 = call i32 @strcmp(i8* %15, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @nulmark, i32 0, i32 0))
  %tobool14 = icmp ne i32 %call13, 0
  br i1 %tobool14, label %if.then15, label %if.end36

if.then15:                                        ; preds = %land.lhs.true
  %16 = load i8*, i8** %dstFileName.addr, align 4, !tbaa !2
  %call16 = call %struct._IO_FILE* @fopen(i8* %16, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.42, i32 0, i32 0))
  store %struct._IO_FILE* %call16, %struct._IO_FILE** %f, align 4, !tbaa !2
  %17 = load %struct._IO_FILE*, %struct._IO_FILE** %f, align 4, !tbaa !2
  %cmp17 = icmp ne %struct._IO_FILE* %17, null
  br i1 %cmp17, label %if.then18, label %if.end35

if.then18:                                        ; preds = %if.then15
  %18 = load %struct._IO_FILE*, %struct._IO_FILE** %f, align 4, !tbaa !2
  %call19 = call i32 @fclose(%struct._IO_FILE* %18)
  %19 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp20 = icmp sle i32 %19, 1
  br i1 %cmp20, label %if.then21, label %if.end23

if.then21:                                        ; preds = %if.then18
  %20 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %21 = load i8*, i8** %dstFileName.addr, align 4, !tbaa !2
  %call22 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %20, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.47, i32 0, i32 0), i8* %21)
  store %struct._IO_FILE* null, %struct._IO_FILE** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup54

if.end23:                                         ; preds = %if.then18
  %22 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %23 = load i8*, i8** %dstFileName.addr, align 4, !tbaa !2
  %call24 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %22, i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.48, i32 0, i32 0), i8* %23)
  %24 = bitcast i32* %ch to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #5
  %call25 = call i32 @getchar()
  store i32 %call25, i32* %ch, align 4, !tbaa !6
  %25 = load i32, i32* %ch, align 4, !tbaa !6
  %cmp26 = icmp ne i32 %25, 89
  br i1 %cmp26, label %land.lhs.true27, label %if.end31

land.lhs.true27:                                  ; preds = %if.end23
  %26 = load i32, i32* %ch, align 4, !tbaa !6
  %cmp28 = icmp ne i32 %26, 121
  br i1 %cmp28, label %if.then29, label %if.end31

if.then29:                                        ; preds = %land.lhs.true27
  %27 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call30 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %27, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.49, i32 0, i32 0))
  store %struct._IO_FILE* null, %struct._IO_FILE** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end31:                                         ; preds = %land.lhs.true27, %if.end23
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end31
  %28 = load i32, i32* %ch, align 4, !tbaa !6
  %cmp32 = icmp ne i32 %28, -1
  br i1 %cmp32, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %29 = load i32, i32* %ch, align 4, !tbaa !6
  %cmp33 = icmp ne i32 %29, 10
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %30 = phi i1 [ false, %while.cond ], [ %cmp33, %land.rhs ]
  br i1 %30, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %call34 = call i32 @getchar()
  store i32 %call34, i32* %ch, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %land.end
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %while.end, %if.then29
  %31 = bitcast i32* %ch to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup54 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end35

if.end35:                                         ; preds = %cleanup.cont, %if.then15
  br label %if.end36

if.end36:                                         ; preds = %if.end35, %land.lhs.true, %if.else
  %32 = load i8*, i8** %dstFileName.addr, align 4, !tbaa !2
  %call37 = call %struct._IO_FILE* @fopen(i8* %32, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.50, i32 0, i32 0))
  store %struct._IO_FILE* %call37, %struct._IO_FILE** %f, align 4, !tbaa !2
  %33 = load %struct._IO_FILE*, %struct._IO_FILE** %f, align 4, !tbaa !2
  %cmp38 = icmp eq %struct._IO_FILE* %33, null
  br i1 %cmp38, label %if.then39, label %if.end46

if.then39:                                        ; preds = %if.end36
  %34 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp40 = icmp sge i32 %34, 1
  br i1 %cmp40, label %if.then41, label %if.end45

if.then41:                                        ; preds = %if.then39
  %35 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %36 = load i8*, i8** %dstFileName.addr, align 4, !tbaa !2
  %call42 = call i32* @__errno_location()
  %37 = load i32, i32* %call42, align 4, !tbaa !6
  %call43 = call i8* @strerror(i32 %37)
  %call44 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %35, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.51, i32 0, i32 0), i8* %36, i8* %call43)
  br label %if.end45

if.end45:                                         ; preds = %if.then41, %if.then39
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %if.end36
  br label %if.end47

if.end47:                                         ; preds = %if.end46, %if.end11
  %38 = load %struct._IO_FILE*, %struct._IO_FILE** %f, align 4, !tbaa !2
  %tobool48 = icmp ne %struct._IO_FILE* %38, null
  br i1 %tobool48, label %land.lhs.true49, label %if.end53

land.lhs.true49:                                  ; preds = %if.end47
  %39 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %sparseFileSupport50 = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %39, i32 0, i32 8
  %40 = load i32, i32* %sparseFileSupport50, align 4, !tbaa !18
  %tobool51 = icmp ne i32 %40, 0
  br i1 %tobool51, label %if.then52, label %if.end53

if.then52:                                        ; preds = %land.lhs.true49
  br label %if.end53

if.end53:                                         ; preds = %if.then52, %land.lhs.true49, %if.end47
  %41 = load %struct._IO_FILE*, %struct._IO_FILE** %f, align 4, !tbaa !2
  store %struct._IO_FILE* %41, %struct._IO_FILE** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup54

cleanup54:                                        ; preds = %if.end53, %cleanup, %if.then21
  %42 = bitcast %struct._IO_FILE** %f to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #5
  %43 = load %struct._IO_FILE*, %struct._IO_FILE** %retval, align 4
  ret %struct._IO_FILE* %43
}

declare i32 @fclose(%struct._IO_FILE*) #2

; Function Attrs: nounwind
define internal void @LZ4IO_writeLE32(i8* %p, i32 %value32) #0 {
entry:
  %p.addr = alloca i8*, align 4
  %value32.addr = alloca i32, align 4
  %dstPtr = alloca i8*, align 4
  store i8* %p, i8** %p.addr, align 4, !tbaa !2
  store i32 %value32, i32* %value32.addr, align 4, !tbaa !6
  %0 = bitcast i8** %dstPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %p.addr, align 4, !tbaa !2
  store i8* %1, i8** %dstPtr, align 4, !tbaa !2
  %2 = load i32, i32* %value32.addr, align 4, !tbaa !6
  %conv = trunc i32 %2 to i8
  %3 = load i8*, i8** %dstPtr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 0
  store i8 %conv, i8* %arrayidx, align 1, !tbaa !25
  %4 = load i32, i32* %value32.addr, align 4, !tbaa !6
  %shr = lshr i32 %4, 8
  %conv1 = trunc i32 %shr to i8
  %5 = load i8*, i8** %dstPtr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %5, i32 1
  store i8 %conv1, i8* %arrayidx2, align 1, !tbaa !25
  %6 = load i32, i32* %value32.addr, align 4, !tbaa !6
  %shr3 = lshr i32 %6, 16
  %conv4 = trunc i32 %shr3 to i8
  %7 = load i8*, i8** %dstPtr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %7, i32 2
  store i8 %conv4, i8* %arrayidx5, align 1, !tbaa !25
  %8 = load i32, i32* %value32.addr, align 4, !tbaa !6
  %shr6 = lshr i32 %8, 24
  %conv7 = trunc i32 %shr6 to i8
  %9 = load i8*, i8** %dstPtr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8, i8* %9, i32 3
  store i8 %conv7, i8* %arrayidx8, align 1, !tbaa !25
  %10 = bitcast i8** %dstPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #5
  ret void
}

declare i32 @fwrite(i8*, i32, i32, %struct._IO_FILE*) #2

declare i32 @fread(i8*, i32, i32, %struct._IO_FILE*) #2

; Function Attrs: noreturn
declare void @__assert_fail(i8*, i8*, i32, i8*) #3

declare i32 @fflush(%struct._IO_FILE*) #2

declare i32 @ferror(%struct._IO_FILE*) #2

declare i32 @strcmp(i8*, i8*) #2

; Function Attrs: nounwind
define hidden i32 @LZ4IO_compressMultipleFilenames_Legacy(%struct.LZ4IO_prefs_s* %prefs, i8** %inFileNamesTable, i32 %ifntSize, i8* %suffix, i32 %compressionLevel) #0 {
entry:
  %retval = alloca i32, align 4
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %inFileNamesTable.addr = alloca i8**, align 4
  %ifntSize.addr = alloca i32, align 4
  %suffix.addr = alloca i8*, align 4
  %compressionLevel.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %missed_files = alloca i32, align 4
  %dstFileName = alloca i8*, align 4
  %ofnSize = alloca i32, align 4
  %suffixSize = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ifnSize = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i8** %inFileNamesTable, i8*** %inFileNamesTable.addr, align 4, !tbaa !2
  store i32 %ifntSize, i32* %ifntSize.addr, align 4, !tbaa !6
  store i8* %suffix, i8** %suffix.addr, align 4, !tbaa !2
  store i32 %compressionLevel, i32* %compressionLevel.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %missed_files to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 0, i32* %missed_files, align 4, !tbaa !6
  %2 = bitcast i8** %dstFileName to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %call = call i8* @malloc(i32 30)
  store i8* %call, i8** %dstFileName, align 4, !tbaa !2
  %3 = bitcast i32* %ofnSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  store i32 30, i32* %ofnSize, align 4, !tbaa !24
  %4 = bitcast i32* %suffixSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i8*, i8** %suffix.addr, align 4, !tbaa !2
  %call1 = call i32 @strlen(i8* %5)
  store i32 %call1, i32* %suffixSize, align 4, !tbaa !24
  %6 = load i8*, i8** %dstFileName, align 4, !tbaa !2
  %cmp = icmp eq i8* %6, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load i32, i32* %ifntSize.addr, align 4, !tbaa !6
  store i32 %7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup25

if.end:                                           ; preds = %entry
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %9 = load i32, i32* %ifntSize.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %8, %9
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = bitcast i32* %ifnSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load i8**, i8*** %inFileNamesTable.addr, align 4, !tbaa !2
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8*, i8** %11, i32 %12
  %13 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %call3 = call i32 @strlen(i8* %13)
  store i32 %call3, i32* %ifnSize, align 4, !tbaa !24
  %14 = load i8*, i8** %suffix.addr, align 4, !tbaa !2
  %call4 = call i32 @strcmp(i8* %14, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0))
  %tobool = icmp ne i32 %call4, 0
  br i1 %tobool, label %if.end8, label %if.then5

if.then5:                                         ; preds = %for.body
  %15 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %16 = load i8**, i8*** %inFileNamesTable.addr, align 4, !tbaa !2
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds i8*, i8** %16, i32 %17
  %18 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  %19 = load i32, i32* %compressionLevel.addr, align 4, !tbaa !6
  %call7 = call i32 @LZ4IO_compressFilename_Legacy(%struct.LZ4IO_prefs_s* %15, i8* %18, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0), i32 %19)
  %20 = load i32, i32* %missed_files, align 4, !tbaa !6
  %add = add nsw i32 %20, %call7
  store i32 %add, i32* %missed_files, align 4, !tbaa !6
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %for.body
  %21 = load i32, i32* %ofnSize, align 4, !tbaa !24
  %22 = load i32, i32* %ifnSize, align 4, !tbaa !24
  %23 = load i32, i32* %suffixSize, align 4, !tbaa !24
  %add9 = add i32 %22, %23
  %add10 = add i32 %add9, 1
  %cmp11 = icmp ule i32 %21, %add10
  br i1 %cmp11, label %if.then12, label %if.end18

if.then12:                                        ; preds = %if.end8
  %24 = load i8*, i8** %dstFileName, align 4, !tbaa !2
  call void @free(i8* %24)
  %25 = load i32, i32* %ifnSize, align 4, !tbaa !24
  %add13 = add i32 %25, 20
  store i32 %add13, i32* %ofnSize, align 4, !tbaa !24
  %26 = load i32, i32* %ofnSize, align 4, !tbaa !24
  %call14 = call i8* @malloc(i32 %26)
  store i8* %call14, i8** %dstFileName, align 4, !tbaa !2
  %27 = load i8*, i8** %dstFileName, align 4, !tbaa !2
  %cmp15 = icmp eq i8* %27, null
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.then12
  %28 = load i32, i32* %ifntSize.addr, align 4, !tbaa !6
  store i32 %28, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end17:                                         ; preds = %if.then12
  br label %if.end18

if.end18:                                         ; preds = %if.end17, %if.end8
  %29 = load i8*, i8** %dstFileName, align 4, !tbaa !2
  %30 = load i8**, i8*** %inFileNamesTable.addr, align 4, !tbaa !2
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx19 = getelementptr inbounds i8*, i8** %30, i32 %31
  %32 = load i8*, i8** %arrayidx19, align 4, !tbaa !2
  %call20 = call i8* @strcpy(i8* %29, i8* %32)
  %33 = load i8*, i8** %dstFileName, align 4, !tbaa !2
  %34 = load i8*, i8** %suffix.addr, align 4, !tbaa !2
  %call21 = call i8* @strcat(i8* %33, i8* %34)
  %35 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %36 = load i8**, i8*** %inFileNamesTable.addr, align 4, !tbaa !2
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds i8*, i8** %36, i32 %37
  %38 = load i8*, i8** %arrayidx22, align 4, !tbaa !2
  %39 = load i8*, i8** %dstFileName, align 4, !tbaa !2
  %40 = load i32, i32* %compressionLevel.addr, align 4, !tbaa !6
  %call23 = call i32 @LZ4IO_compressFilename_Legacy(%struct.LZ4IO_prefs_s* %35, i8* %38, i8* %39, i32 %40)
  %41 = load i32, i32* %missed_files, align 4, !tbaa !6
  %add24 = add nsw i32 %41, %call23
  store i32 %add24, i32* %missed_files, align 4, !tbaa !6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end18, %if.then16, %if.then5
  %42 = bitcast i32* %ifnSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup25 [
    i32 0, label %cleanup.cont
    i32 4, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %43 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %43, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %44 = load i8*, i8** %dstFileName, align 4, !tbaa !2
  call void @free(i8* %44)
  %45 = load i32, i32* %missed_files, align 4, !tbaa !6
  store i32 %45, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup25

cleanup25:                                        ; preds = %for.end, %cleanup, %if.then
  %46 = bitcast i32* %suffixSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #5
  %47 = bitcast i32* %ofnSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #5
  %48 = bitcast i8** %dstFileName to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #5
  %49 = bitcast i32* %missed_files to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #5
  %50 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #5
  %51 = load i32, i32* %retval, align 4
  ret i32 %51
}

declare i32 @strlen(i8*) #2

declare i8* @strcpy(i8*, i8*) #2

declare i8* @strcat(i8*, i8*) #2

; Function Attrs: nounwind
define hidden i32 @LZ4IO_compressFilename(%struct.LZ4IO_prefs_s* %prefs, i8* %srcFileName, i8* %dstFileName, i32 %compressionLevel) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %srcFileName.addr = alloca i8*, align 4
  %dstFileName.addr = alloca i8*, align 4
  %compressionLevel.addr = alloca i32, align 4
  %timeStart = alloca i32, align 4
  %cpuStart = alloca i32, align 4
  %ress = alloca %struct.cRess_t, align 4
  %result = alloca i32, align 4
  %cpuEnd = alloca i32, align 4
  %cpuLoad_s = alloca double, align 8
  %timeLength_ns = alloca i64, align 8
  %timeLength_s = alloca double, align 8
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i8* %srcFileName, i8** %srcFileName.addr, align 4, !tbaa !2
  store i8* %dstFileName, i8** %dstFileName.addr, align 4, !tbaa !2
  store i32 %compressionLevel, i32* %compressionLevel.addr, align 4, !tbaa !6
  %0 = bitcast i32* %timeStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %call = call i32 @UTIL_getTime()
  store i32 %call, i32* %timeStart, align 4, !tbaa !24
  %1 = bitcast i32* %cpuStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %call1 = call i32 @clock()
  store i32 %call1, i32* %cpuStart, align 4, !tbaa !24
  %2 = bitcast %struct.cRess_t* %ress to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %2) #5
  %3 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  call void @LZ4IO_createCResources(%struct.cRess_t* sret align 4 %ress, %struct.LZ4IO_prefs_s* %3)
  %4 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %6 = load i8*, i8** %srcFileName.addr, align 4, !tbaa !2
  %7 = load i8*, i8** %dstFileName.addr, align 4, !tbaa !2
  %8 = load i32, i32* %compressionLevel.addr, align 4, !tbaa !6
  %call2 = call i32 @LZ4IO_compressFilename_extRess(%struct.LZ4IO_prefs_s* %5, %struct.cRess_t* byval(%struct.cRess_t) align 4 %ress, i8* %6, i8* %7, i32 %8)
  store i32 %call2, i32* %result, align 4, !tbaa !6
  call void @LZ4IO_freeCResources(%struct.cRess_t* byval(%struct.cRess_t) align 4 %ress)
  %9 = bitcast i32* %cpuEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %call3 = call i32 @clock()
  store i32 %call3, i32* %cpuEnd, align 4, !tbaa !24
  %10 = bitcast double* %cpuLoad_s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %10) #5
  %11 = load i32, i32* %cpuEnd, align 4, !tbaa !24
  %12 = load i32, i32* %cpuStart, align 4, !tbaa !24
  %sub = sub nsw i32 %11, %12
  %conv = sitofp i32 %sub to double
  %div = fdiv double %conv, 1.000000e+06
  store double %div, double* %cpuLoad_s, align 8, !tbaa !28
  %13 = bitcast i64* %timeLength_ns to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %13) #5
  %14 = load i32, i32* %timeStart, align 4, !tbaa !24
  %call4 = call i64 @UTIL_clockSpanNano(i32 %14)
  store i64 %call4, i64* %timeLength_ns, align 8, !tbaa !26
  %15 = bitcast double* %timeLength_s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %15) #5
  %16 = load i64, i64* %timeLength_ns, align 8, !tbaa !26
  %conv5 = uitofp i64 %16 to double
  %div6 = fdiv double %conv5, 1.000000e+09
  store double %div6, double* %timeLength_s, align 8, !tbaa !28
  %17 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp = icmp sge i32 %17, 4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %18 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %19 = load double, double* %timeLength_s, align 8, !tbaa !28
  %20 = load double, double* %cpuLoad_s, align 8, !tbaa !28
  %21 = load double, double* %timeLength_s, align 8, !tbaa !28
  %div8 = fdiv double %20, %21
  %mul = fmul double %div8, 1.000000e+02
  %call9 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %18, i8* getelementptr inbounds ([44 x i8], [44 x i8]* @.str.16, i32 0, i32 0), double %19, double %mul)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %22 = bitcast double* %timeLength_s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %22) #5
  %23 = bitcast i64* %timeLength_ns to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %23) #5
  %24 = bitcast double* %cpuLoad_s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %24) #5
  %25 = bitcast i32* %cpuEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  %26 = load i32, i32* %result, align 4, !tbaa !6
  %27 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #5
  %28 = bitcast %struct.cRess_t* %ress to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %28) #5
  %29 = bitcast i32* %cpuStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #5
  %30 = bitcast i32* %timeStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #5
  ret i32 %26
}

; Function Attrs: nounwind
define internal i32 @UTIL_getTime() #0 {
entry:
  %call = call i32 @clock()
  ret i32 %call
}

; Function Attrs: nounwind
define internal void @LZ4IO_createCResources(%struct.cRess_t* noalias sret align 4 %agg.result, %struct.LZ4IO_prefs_s* %prefs) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %blockSize = alloca i32, align 4
  %errorCode = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %0 = bitcast i32* %blockSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %blockSize1 = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %1, i32 0, i32 4
  %2 = load i32, i32* %blockSize1, align 4, !tbaa !14
  store i32 %2, i32* %blockSize, align 4, !tbaa !24
  %3 = bitcast i32* %errorCode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %ctx = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %agg.result, i32 0, i32 4
  %call = call i32 @LZ4F_createCompressionContext(%struct.LZ4F_cctx_s** %ctx, i32 100)
  store i32 %call, i32* %errorCode, align 4, !tbaa !24
  %4 = load i32, i32* %errorCode, align 4, !tbaa !24
  %call2 = call i32 @LZ4F_isError(i32 %4)
  %tobool = icmp ne i32 %call2, 0
  br i1 %tobool, label %if.then, label %if.end14

if.then:                                          ; preds = %entry
  %5 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp = icmp sge i32 %5, 1
  br i1 %cmp, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %6 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call4 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %6, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 30)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %7 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp5 = icmp sge i32 %7, 1
  br i1 %cmp5, label %if.then6, label %if.end9

if.then6:                                         ; preds = %if.end
  %8 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %9 = load i32, i32* %errorCode, align 4, !tbaa !24
  %call7 = call i8* @LZ4F_getErrorName(i32 %9)
  %call8 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %8, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @.str.52, i32 0, i32 0), i8* %call7)
  br label %if.end9

if.end9:                                          ; preds = %if.then6, %if.end
  %10 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp10 = icmp sge i32 %10, 1
  br i1 %cmp10, label %if.then11, label %if.end13

if.then11:                                        ; preds = %if.end9
  %11 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call12 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %11, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end13

if.end13:                                         ; preds = %if.then11, %if.end9
  call void @exit(i32 30) #6
  unreachable

if.end14:                                         ; preds = %entry
  %12 = load i32, i32* %blockSize, align 4, !tbaa !24
  %call15 = call i8* @malloc(i32 %12)
  %srcBuffer = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %agg.result, i32 0, i32 0
  store i8* %call15, i8** %srcBuffer, align 4, !tbaa !30
  %13 = load i32, i32* %blockSize, align 4, !tbaa !24
  %srcBufferSize = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %agg.result, i32 0, i32 1
  store i32 %13, i32* %srcBufferSize, align 4, !tbaa !32
  %14 = load i32, i32* %blockSize, align 4, !tbaa !24
  %call16 = call i32 @LZ4F_compressFrameBound(i32 %14, %struct.LZ4F_preferences_t* null)
  %dstBufferSize = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %agg.result, i32 0, i32 3
  store i32 %call16, i32* %dstBufferSize, align 4, !tbaa !33
  %dstBufferSize17 = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %agg.result, i32 0, i32 3
  %15 = load i32, i32* %dstBufferSize17, align 4, !tbaa !33
  %call18 = call i8* @malloc(i32 %15)
  %dstBuffer = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %agg.result, i32 0, i32 2
  store i8* %call18, i8** %dstBuffer, align 4, !tbaa !34
  %srcBuffer19 = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %agg.result, i32 0, i32 0
  %16 = load i8*, i8** %srcBuffer19, align 4, !tbaa !30
  %tobool20 = icmp ne i8* %16, null
  br i1 %tobool20, label %lor.lhs.false, label %if.then23

lor.lhs.false:                                    ; preds = %if.end14
  %dstBuffer21 = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %agg.result, i32 0, i32 2
  %17 = load i8*, i8** %dstBuffer21, align 4, !tbaa !34
  %tobool22 = icmp ne i8* %17, null
  br i1 %tobool22, label %if.end36, label %if.then23

if.then23:                                        ; preds = %lor.lhs.false, %if.end14
  %18 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp24 = icmp sge i32 %18, 1
  br i1 %cmp24, label %if.then25, label %if.end27

if.then25:                                        ; preds = %if.then23
  %19 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call26 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %19, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 31)
  br label %if.end27

if.end27:                                         ; preds = %if.then25, %if.then23
  %20 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp28 = icmp sge i32 %20, 1
  br i1 %cmp28, label %if.then29, label %if.end31

if.then29:                                        ; preds = %if.end27
  %21 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call30 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %21, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.1, i32 0, i32 0))
  br label %if.end31

if.end31:                                         ; preds = %if.then29, %if.end27
  %22 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp32 = icmp sge i32 %22, 1
  br i1 %cmp32, label %if.then33, label %if.end35

if.then33:                                        ; preds = %if.end31
  %23 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call34 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %23, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end35

if.end35:                                         ; preds = %if.then33, %if.end31
  call void @exit(i32 31) #6
  unreachable

if.end36:                                         ; preds = %lor.lhs.false
  %24 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %call37 = call %struct.LZ4F_CDict_s* @LZ4IO_createCDict(%struct.LZ4IO_prefs_s* %24)
  %cdict = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %agg.result, i32 0, i32 5
  store %struct.LZ4F_CDict_s* %call37, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !35
  %25 = bitcast i32* %errorCode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  %26 = bitcast i32* %blockSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #5
  ret void
}

; Function Attrs: nounwind
define internal i32 @LZ4IO_compressFilename_extRess(%struct.LZ4IO_prefs_s* %io_prefs, %struct.cRess_t* byval(%struct.cRess_t) align 4 %ress, i8* %srcFileName, i8* %dstFileName, i32 %compressionLevel) #0 {
entry:
  %retval = alloca i32, align 4
  %io_prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %srcFileName.addr = alloca i8*, align 4
  %dstFileName.addr = alloca i8*, align 4
  %compressionLevel.addr = alloca i32, align 4
  %filesize = alloca i64, align 8
  %compressedfilesize = alloca i64, align 8
  %srcFile = alloca %struct._IO_FILE*, align 4
  %dstFile = alloca %struct._IO_FILE*, align 4
  %srcBuffer = alloca i8*, align 4
  %dstBuffer = alloca i8*, align 4
  %dstBufferSize = alloca i32, align 4
  %blockSize = alloca i32, align 4
  %readSize = alloca i32, align 4
  %ctx = alloca %struct.LZ4F_cctx_s*, align 4
  %prefs = alloca %struct.LZ4F_preferences_t, align 8
  %cleanup.dest.slot = alloca i32, align 4
  %fileSize = alloca i64, align 8
  %cSize = alloca i32, align 4
  %sizeCheck = alloca i32, align 4
  %headerSize = alloca i32, align 4
  %sizeCheck135 = alloca i32, align 4
  %outSize = alloca i32, align 4
  %sizeCheck209 = alloca i32, align 4
  %sizeCheck273 = alloca i32, align 4
  %statbuf = alloca %struct.stat, align 8
  store %struct.LZ4IO_prefs_s* %io_prefs, %struct.LZ4IO_prefs_s** %io_prefs.addr, align 4, !tbaa !2
  store i8* %srcFileName, i8** %srcFileName.addr, align 4, !tbaa !2
  store i8* %dstFileName, i8** %dstFileName.addr, align 4, !tbaa !2
  store i32 %compressionLevel, i32* %compressionLevel.addr, align 4, !tbaa !6
  %0 = bitcast i64* %filesize to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #5
  store i64 0, i64* %filesize, align 8, !tbaa !26
  %1 = bitcast i64* %compressedfilesize to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #5
  store i64 0, i64* %compressedfilesize, align 8, !tbaa !26
  %2 = bitcast %struct._IO_FILE** %srcFile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast %struct._IO_FILE** %dstFile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = bitcast i8** %srcBuffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %srcBuffer1 = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %ress, i32 0, i32 0
  %5 = load i8*, i8** %srcBuffer1, align 4, !tbaa !30
  store i8* %5, i8** %srcBuffer, align 4, !tbaa !2
  %6 = bitcast i8** %dstBuffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %dstBuffer2 = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %ress, i32 0, i32 2
  %7 = load i8*, i8** %dstBuffer2, align 4, !tbaa !34
  store i8* %7, i8** %dstBuffer, align 4, !tbaa !2
  %8 = bitcast i32* %dstBufferSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %dstBufferSize3 = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %ress, i32 0, i32 3
  %9 = load i32, i32* %dstBufferSize3, align 4, !tbaa !33
  store i32 %9, i32* %dstBufferSize, align 4, !tbaa !24
  %10 = bitcast i32* %blockSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %io_prefs.addr, align 4, !tbaa !2
  %blockSize4 = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %11, i32 0, i32 4
  %12 = load i32, i32* %blockSize4, align 4, !tbaa !14
  store i32 %12, i32* %blockSize, align 4, !tbaa !24
  %13 = bitcast i32* %readSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  %14 = bitcast %struct.LZ4F_cctx_s** %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %ctx5 = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %ress, i32 0, i32 4
  %15 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %ctx5, align 4, !tbaa !36
  store %struct.LZ4F_cctx_s* %15, %struct.LZ4F_cctx_s** %ctx, align 4, !tbaa !2
  %16 = bitcast %struct.LZ4F_preferences_t* %prefs to i8*
  call void @llvm.lifetime.start.p0i8(i64 56, i8* %16) #5
  %17 = load i8*, i8** %srcFileName.addr, align 4, !tbaa !2
  %call = call %struct._IO_FILE* @LZ4IO_openSrcFile(i8* %17)
  store %struct._IO_FILE* %call, %struct._IO_FILE** %srcFile, align 4, !tbaa !2
  %18 = load %struct._IO_FILE*, %struct._IO_FILE** %srcFile, align 4, !tbaa !2
  %cmp = icmp eq %struct._IO_FILE* %18, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %19 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %io_prefs.addr, align 4, !tbaa !2
  %20 = load i8*, i8** %dstFileName.addr, align 4, !tbaa !2
  %call6 = call %struct._IO_FILE* @LZ4IO_openDstFile(%struct.LZ4IO_prefs_s* %19, i8* %20)
  store %struct._IO_FILE* %call6, %struct._IO_FILE** %dstFile, align 4, !tbaa !2
  %21 = load %struct._IO_FILE*, %struct._IO_FILE** %dstFile, align 4, !tbaa !2
  %cmp7 = icmp eq %struct._IO_FILE* %21, null
  br i1 %cmp7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %if.end
  %22 = load %struct._IO_FILE*, %struct._IO_FILE** %srcFile, align 4, !tbaa !2
  %call9 = call i32 @fclose(%struct._IO_FILE* %22)
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end10:                                         ; preds = %if.end
  %23 = bitcast %struct.LZ4F_preferences_t* %prefs to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %23, i8 0, i32 56, i1 false)
  %autoFlush = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 2
  store i32 1, i32* %autoFlush, align 4, !tbaa !37
  %24 = load i32, i32* %compressionLevel.addr, align 4, !tbaa !6
  %compressionLevel11 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 1
  store i32 %24, i32* %compressionLevel11, align 8, !tbaa !40
  %25 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %io_prefs.addr, align 4, !tbaa !2
  %blockIndependence = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %25, i32 0, i32 7
  %26 = load i32, i32* %blockIndependence, align 4, !tbaa !17
  %frameInfo = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 0
  %blockMode = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo, i32 0, i32 1
  store i32 %26, i32* %blockMode, align 4, !tbaa !41
  %27 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %io_prefs.addr, align 4, !tbaa !2
  %blockSizeId = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %27, i32 0, i32 3
  %28 = load i32, i32* %blockSizeId, align 4, !tbaa !13
  %frameInfo12 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 0
  %blockSizeID = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo12, i32 0, i32 0
  store i32 %28, i32* %blockSizeID, align 8, !tbaa !42
  %29 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %io_prefs.addr, align 4, !tbaa !2
  %blockChecksum = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %29, i32 0, i32 5
  %30 = load i32, i32* %blockChecksum, align 4, !tbaa !15
  %frameInfo13 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 0
  %blockChecksumFlag = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo13, i32 0, i32 6
  store i32 %30, i32* %blockChecksumFlag, align 4, !tbaa !43
  %31 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %io_prefs.addr, align 4, !tbaa !2
  %streamChecksum = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %31, i32 0, i32 6
  %32 = load i32, i32* %streamChecksum, align 4, !tbaa !16
  %frameInfo14 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 0
  %contentChecksumFlag = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo14, i32 0, i32 2
  store i32 %32, i32* %contentChecksumFlag, align 8, !tbaa !44
  %33 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %io_prefs.addr, align 4, !tbaa !2
  %favorDecSpeed = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %33, i32 0, i32 11
  %34 = load i32, i32* %favorDecSpeed, align 4, !tbaa !21
  %favorDecSpeed15 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 3
  store i32 %34, i32* %favorDecSpeed15, align 8, !tbaa !45
  %35 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %io_prefs.addr, align 4, !tbaa !2
  %contentSizeFlag = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %35, i32 0, i32 9
  %36 = load i32, i32* %contentSizeFlag, align 4, !tbaa !19
  %tobool = icmp ne i32 %36, 0
  br i1 %tobool, label %if.then16, label %if.end26

if.then16:                                        ; preds = %if.end10
  %37 = bitcast i64* %fileSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %37) #5
  %38 = load i8*, i8** %srcFileName.addr, align 4, !tbaa !2
  %call17 = call i64 @UTIL_getFileSize(i8* %38)
  store i64 %call17, i64* %fileSize, align 8, !tbaa !26
  %39 = load i64, i64* %fileSize, align 8, !tbaa !26
  %frameInfo18 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 0
  %contentSize = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo18, i32 0, i32 4
  store i64 %39, i64* %contentSize, align 8, !tbaa !46
  %40 = load i64, i64* %fileSize, align 8, !tbaa !26
  %cmp19 = icmp eq i64 %40, 0
  br i1 %cmp19, label %if.then20, label %if.end25

if.then20:                                        ; preds = %if.then16
  %41 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp21 = icmp sge i32 %41, 3
  br i1 %cmp21, label %if.then22, label %if.end24

if.then22:                                        ; preds = %if.then20
  %42 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call23 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %42, i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.56, i32 0, i32 0))
  br label %if.end24

if.end24:                                         ; preds = %if.then22, %if.then20
  br label %if.end25

if.end25:                                         ; preds = %if.end24, %if.then16
  %43 = bitcast i64* %fileSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %43) #5
  br label %if.end26

if.end26:                                         ; preds = %if.end25, %if.end10
  %44 = load i8*, i8** %srcBuffer, align 4, !tbaa !2
  %45 = load i32, i32* %blockSize, align 4, !tbaa !24
  %46 = load %struct._IO_FILE*, %struct._IO_FILE** %srcFile, align 4, !tbaa !2
  %call27 = call i32 @fread(i8* %44, i32 1, i32 %45, %struct._IO_FILE* %46)
  store i32 %call27, i32* %readSize, align 4, !tbaa !24
  %47 = load %struct._IO_FILE*, %struct._IO_FILE** %srcFile, align 4, !tbaa !2
  %call28 = call i32 @ferror(%struct._IO_FILE* %47)
  %tobool29 = icmp ne i32 %call28, 0
  br i1 %tobool29, label %if.then30, label %if.end43

if.then30:                                        ; preds = %if.end26
  %48 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp31 = icmp sge i32 %48, 1
  br i1 %cmp31, label %if.then32, label %if.end34

if.then32:                                        ; preds = %if.then30
  %49 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call33 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %49, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 30)
  br label %if.end34

if.end34:                                         ; preds = %if.then32, %if.then30
  %50 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp35 = icmp sge i32 %50, 1
  br i1 %cmp35, label %if.then36, label %if.end38

if.then36:                                        ; preds = %if.end34
  %51 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %52 = load i8*, i8** %srcFileName.addr, align 4, !tbaa !2
  %call37 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %51, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.57, i32 0, i32 0), i8* %52)
  br label %if.end38

if.end38:                                         ; preds = %if.then36, %if.end34
  %53 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp39 = icmp sge i32 %53, 1
  br i1 %cmp39, label %if.then40, label %if.end42

if.then40:                                        ; preds = %if.end38
  %54 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call41 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %54, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end42

if.end42:                                         ; preds = %if.then40, %if.end38
  call void @exit(i32 30) #6
  unreachable

if.end43:                                         ; preds = %if.end26
  %55 = load i32, i32* %readSize, align 4, !tbaa !24
  %conv = zext i32 %55 to i64
  %56 = load i64, i64* %filesize, align 8, !tbaa !26
  %add = add i64 %56, %conv
  store i64 %add, i64* %filesize, align 8, !tbaa !26
  %57 = load i32, i32* %readSize, align 4, !tbaa !24
  %58 = load i32, i32* %blockSize, align 4, !tbaa !24
  %cmp44 = icmp ult i32 %57, %58
  br i1 %cmp44, label %if.then46, label %if.else

if.then46:                                        ; preds = %if.end43
  %59 = bitcast i32* %cSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #5
  %60 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %ctx, align 4, !tbaa !2
  %61 = load i8*, i8** %dstBuffer, align 4, !tbaa !2
  %62 = load i32, i32* %dstBufferSize, align 4, !tbaa !24
  %63 = load i8*, i8** %srcBuffer, align 4, !tbaa !2
  %64 = load i32, i32* %readSize, align 4, !tbaa !24
  %cdict = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %ress, i32 0, i32 5
  %65 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !35
  %call47 = call i32 @LZ4F_compressFrame_usingCDict(%struct.LZ4F_cctx_s* %60, i8* %61, i32 %62, i8* %63, i32 %64, %struct.LZ4F_CDict_s* %65, %struct.LZ4F_preferences_t* %prefs)
  store i32 %call47, i32* %cSize, align 4, !tbaa !24
  %66 = load i32, i32* %cSize, align 4, !tbaa !24
  %call48 = call i32 @LZ4F_isError(i32 %66)
  %tobool49 = icmp ne i32 %call48, 0
  br i1 %tobool49, label %if.then50, label %if.end67

if.then50:                                        ; preds = %if.then46
  %67 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp51 = icmp sge i32 %67, 1
  br i1 %cmp51, label %if.then53, label %if.end55

if.then53:                                        ; preds = %if.then50
  %68 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call54 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %68, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 31)
  br label %if.end55

if.end55:                                         ; preds = %if.then53, %if.then50
  %69 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp56 = icmp sge i32 %69, 1
  br i1 %cmp56, label %if.then58, label %if.end61

if.then58:                                        ; preds = %if.end55
  %70 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %71 = load i32, i32* %cSize, align 4, !tbaa !24
  %call59 = call i8* @LZ4F_getErrorName(i32 %71)
  %call60 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %70, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.58, i32 0, i32 0), i8* %call59)
  br label %if.end61

if.end61:                                         ; preds = %if.then58, %if.end55
  %72 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp62 = icmp sge i32 %72, 1
  br i1 %cmp62, label %if.then64, label %if.end66

if.then64:                                        ; preds = %if.end61
  %73 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call65 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %73, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end66

if.end66:                                         ; preds = %if.then64, %if.end61
  call void @exit(i32 31) #6
  unreachable

if.end67:                                         ; preds = %if.then46
  %74 = load i32, i32* %cSize, align 4, !tbaa !24
  %conv68 = zext i32 %74 to i64
  store i64 %conv68, i64* %compressedfilesize, align 8, !tbaa !26
  %75 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp69 = icmp sge i32 %75, 2
  br i1 %cmp69, label %if.then71, label %if.end92

if.then71:                                        ; preds = %if.end67
  %call72 = call i32 @clock()
  %76 = load i32, i32* @g_time, align 4, !tbaa !24
  %sub = sub nsw i32 %call72, %76
  %cmp73 = icmp sgt i32 %sub, 166666
  br i1 %cmp73, label %if.then77, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then71
  %77 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp75 = icmp sge i32 %77, 4
  br i1 %cmp75, label %if.then77, label %if.end91

if.then77:                                        ; preds = %lor.lhs.false, %if.then71
  %call78 = call i32 @clock()
  store i32 %call78, i32* @g_time, align 4, !tbaa !24
  %78 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %79 = load i64, i64* %filesize, align 8, !tbaa !26
  %shr = lshr i64 %79, 20
  %conv79 = trunc i64 %shr to i32
  %80 = load i64, i64* %compressedfilesize, align 8, !tbaa !26
  %conv80 = uitofp i64 %80 to double
  %81 = load i64, i64* %filesize, align 8, !tbaa !26
  %82 = load i64, i64* %filesize, align 8, !tbaa !26
  %tobool81 = icmp ne i64 %82, 0
  %lnot = xor i1 %tobool81, true
  %lnot.ext = zext i1 %lnot to i32
  %conv82 = sext i32 %lnot.ext to i64
  %add83 = add i64 %81, %conv82
  %conv84 = uitofp i64 %add83 to double
  %div = fdiv double %conv80, %conv84
  %mul = fmul double %div, 1.000000e+02
  %call85 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %78, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.59, i32 0, i32 0), i32 %conv79, double %mul)
  %83 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp86 = icmp sge i32 %83, 4
  br i1 %cmp86, label %if.then88, label %if.end90

if.then88:                                        ; preds = %if.then77
  %84 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call89 = call i32 @fflush(%struct._IO_FILE* %84)
  br label %if.end90

if.end90:                                         ; preds = %if.then88, %if.then77
  br label %if.end91

if.end91:                                         ; preds = %if.end90, %lor.lhs.false
  br label %if.end92

if.end92:                                         ; preds = %if.end91, %if.end67
  %85 = bitcast i32* %sizeCheck to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #5
  %86 = load i8*, i8** %dstBuffer, align 4, !tbaa !2
  %87 = load i32, i32* %cSize, align 4, !tbaa !24
  %88 = load %struct._IO_FILE*, %struct._IO_FILE** %dstFile, align 4, !tbaa !2
  %call93 = call i32 @fwrite(i8* %86, i32 1, i32 %87, %struct._IO_FILE* %88)
  store i32 %call93, i32* %sizeCheck, align 4, !tbaa !24
  %89 = load i32, i32* %sizeCheck, align 4, !tbaa !24
  %90 = load i32, i32* %cSize, align 4, !tbaa !24
  %cmp94 = icmp ne i32 %89, %90
  br i1 %cmp94, label %if.then96, label %if.end112

if.then96:                                        ; preds = %if.end92
  %91 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp97 = icmp sge i32 %91, 1
  br i1 %cmp97, label %if.then99, label %if.end101

if.then99:                                        ; preds = %if.then96
  %92 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call100 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %92, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 32)
  br label %if.end101

if.end101:                                        ; preds = %if.then99, %if.then96
  %93 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp102 = icmp sge i32 %93, 1
  br i1 %cmp102, label %if.then104, label %if.end106

if.then104:                                       ; preds = %if.end101
  %94 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call105 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %94, i8* getelementptr inbounds ([44 x i8], [44 x i8]* @.str.10, i32 0, i32 0))
  br label %if.end106

if.end106:                                        ; preds = %if.then104, %if.end101
  %95 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp107 = icmp sge i32 %95, 1
  br i1 %cmp107, label %if.then109, label %if.end111

if.then109:                                       ; preds = %if.end106
  %96 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call110 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %96, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end111

if.end111:                                        ; preds = %if.then109, %if.end106
  call void @exit(i32 32) #6
  unreachable

if.end112:                                        ; preds = %if.end92
  %97 = bitcast i32* %sizeCheck to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #5
  %98 = bitcast i32* %cSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #5
  br label %if.end296

if.else:                                          ; preds = %if.end43
  %99 = bitcast i32* %headerSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #5
  %100 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %ctx, align 4, !tbaa !2
  %101 = load i8*, i8** %dstBuffer, align 4, !tbaa !2
  %102 = load i32, i32* %dstBufferSize, align 4, !tbaa !24
  %cdict113 = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %ress, i32 0, i32 5
  %103 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict113, align 4, !tbaa !35
  %call114 = call i32 @LZ4F_compressBegin_usingCDict(%struct.LZ4F_cctx_s* %100, i8* %101, i32 %102, %struct.LZ4F_CDict_s* %103, %struct.LZ4F_preferences_t* %prefs)
  store i32 %call114, i32* %headerSize, align 4, !tbaa !24
  %104 = load i32, i32* %headerSize, align 4, !tbaa !24
  %call115 = call i32 @LZ4F_isError(i32 %104)
  %tobool116 = icmp ne i32 %call115, 0
  br i1 %tobool116, label %if.then117, label %if.end134

if.then117:                                       ; preds = %if.else
  %105 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp118 = icmp sge i32 %105, 1
  br i1 %cmp118, label %if.then120, label %if.end122

if.then120:                                       ; preds = %if.then117
  %106 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call121 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %106, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 33)
  br label %if.end122

if.end122:                                        ; preds = %if.then120, %if.then117
  %107 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp123 = icmp sge i32 %107, 1
  br i1 %cmp123, label %if.then125, label %if.end128

if.then125:                                       ; preds = %if.end122
  %108 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %109 = load i32, i32* %headerSize, align 4, !tbaa !24
  %call126 = call i8* @LZ4F_getErrorName(i32 %109)
  %call127 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %108, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.60, i32 0, i32 0), i8* %call126)
  br label %if.end128

if.end128:                                        ; preds = %if.then125, %if.end122
  %110 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp129 = icmp sge i32 %110, 1
  br i1 %cmp129, label %if.then131, label %if.end133

if.then131:                                       ; preds = %if.end128
  %111 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call132 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %111, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end133

if.end133:                                        ; preds = %if.then131, %if.end128
  call void @exit(i32 33) #6
  unreachable

if.end134:                                        ; preds = %if.else
  %112 = bitcast i32* %sizeCheck135 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %112) #5
  %113 = load i8*, i8** %dstBuffer, align 4, !tbaa !2
  %114 = load i32, i32* %headerSize, align 4, !tbaa !24
  %115 = load %struct._IO_FILE*, %struct._IO_FILE** %dstFile, align 4, !tbaa !2
  %call136 = call i32 @fwrite(i8* %113, i32 1, i32 %114, %struct._IO_FILE* %115)
  store i32 %call136, i32* %sizeCheck135, align 4, !tbaa !24
  %116 = load i32, i32* %sizeCheck135, align 4, !tbaa !24
  %117 = load i32, i32* %headerSize, align 4, !tbaa !24
  %cmp137 = icmp ne i32 %116, %117
  br i1 %cmp137, label %if.then139, label %if.end155

if.then139:                                       ; preds = %if.end134
  %118 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp140 = icmp sge i32 %118, 1
  br i1 %cmp140, label %if.then142, label %if.end144

if.then142:                                       ; preds = %if.then139
  %119 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call143 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %119, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 34)
  br label %if.end144

if.end144:                                        ; preds = %if.then142, %if.then139
  %120 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp145 = icmp sge i32 %120, 1
  br i1 %cmp145, label %if.then147, label %if.end149

if.then147:                                       ; preds = %if.end144
  %121 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call148 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %121, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end149

if.end149:                                        ; preds = %if.then147, %if.end144
  %122 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp150 = icmp sge i32 %122, 1
  br i1 %cmp150, label %if.then152, label %if.end154

if.then152:                                       ; preds = %if.end149
  %123 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call153 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %123, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end154

if.end154:                                        ; preds = %if.then152, %if.end149
  call void @exit(i32 34) #6
  unreachable

if.end155:                                        ; preds = %if.end134
  %124 = bitcast i32* %sizeCheck135 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #5
  %125 = load i32, i32* %headerSize, align 4, !tbaa !24
  %conv156 = zext i32 %125 to i64
  %126 = load i64, i64* %compressedfilesize, align 8, !tbaa !26
  %add157 = add i64 %126, %conv156
  store i64 %add157, i64* %compressedfilesize, align 8, !tbaa !26
  br label %while.cond

while.cond:                                       ; preds = %if.end229, %if.end155
  %127 = load i32, i32* %readSize, align 4, !tbaa !24
  %cmp158 = icmp ugt i32 %127, 0
  br i1 %cmp158, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %128 = bitcast i32* %outSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %128) #5
  %129 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %ctx, align 4, !tbaa !2
  %130 = load i8*, i8** %dstBuffer, align 4, !tbaa !2
  %131 = load i32, i32* %dstBufferSize, align 4, !tbaa !24
  %132 = load i8*, i8** %srcBuffer, align 4, !tbaa !2
  %133 = load i32, i32* %readSize, align 4, !tbaa !24
  %call160 = call i32 @LZ4F_compressUpdate(%struct.LZ4F_cctx_s* %129, i8* %130, i32 %131, i8* %132, i32 %133, %struct.LZ4F_compressOptions_t* null)
  store i32 %call160, i32* %outSize, align 4, !tbaa !24
  %134 = load i32, i32* %outSize, align 4, !tbaa !24
  %call161 = call i32 @LZ4F_isError(i32 %134)
  %tobool162 = icmp ne i32 %call161, 0
  br i1 %tobool162, label %if.then163, label %if.end180

if.then163:                                       ; preds = %while.body
  %135 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp164 = icmp sge i32 %135, 1
  br i1 %cmp164, label %if.then166, label %if.end168

if.then166:                                       ; preds = %if.then163
  %136 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call167 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %136, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 35)
  br label %if.end168

if.end168:                                        ; preds = %if.then166, %if.then163
  %137 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp169 = icmp sge i32 %137, 1
  br i1 %cmp169, label %if.then171, label %if.end174

if.then171:                                       ; preds = %if.end168
  %138 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %139 = load i32, i32* %outSize, align 4, !tbaa !24
  %call172 = call i8* @LZ4F_getErrorName(i32 %139)
  %call173 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %138, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.58, i32 0, i32 0), i8* %call172)
  br label %if.end174

if.end174:                                        ; preds = %if.then171, %if.end168
  %140 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp175 = icmp sge i32 %140, 1
  br i1 %cmp175, label %if.then177, label %if.end179

if.then177:                                       ; preds = %if.end174
  %141 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call178 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %141, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end179

if.end179:                                        ; preds = %if.then177, %if.end174
  call void @exit(i32 35) #6
  unreachable

if.end180:                                        ; preds = %while.body
  %142 = load i32, i32* %outSize, align 4, !tbaa !24
  %conv181 = zext i32 %142 to i64
  %143 = load i64, i64* %compressedfilesize, align 8, !tbaa !26
  %add182 = add i64 %143, %conv181
  store i64 %add182, i64* %compressedfilesize, align 8, !tbaa !26
  %144 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp183 = icmp sge i32 %144, 2
  br i1 %cmp183, label %if.then185, label %if.end208

if.then185:                                       ; preds = %if.end180
  %call186 = call i32 @clock()
  %145 = load i32, i32* @g_time, align 4, !tbaa !24
  %sub187 = sub nsw i32 %call186, %145
  %cmp188 = icmp sgt i32 %sub187, 166666
  br i1 %cmp188, label %if.then193, label %lor.lhs.false190

lor.lhs.false190:                                 ; preds = %if.then185
  %146 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp191 = icmp sge i32 %146, 4
  br i1 %cmp191, label %if.then193, label %if.end207

if.then193:                                       ; preds = %lor.lhs.false190, %if.then185
  %call194 = call i32 @clock()
  store i32 %call194, i32* @g_time, align 4, !tbaa !24
  %147 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %148 = load i64, i64* %filesize, align 8, !tbaa !26
  %shr195 = lshr i64 %148, 20
  %conv196 = trunc i64 %shr195 to i32
  %149 = load i64, i64* %compressedfilesize, align 8, !tbaa !26
  %conv197 = uitofp i64 %149 to double
  %150 = load i64, i64* %filesize, align 8, !tbaa !26
  %conv198 = uitofp i64 %150 to double
  %div199 = fdiv double %conv197, %conv198
  %mul200 = fmul double %div199, 1.000000e+02
  %call201 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %147, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.59, i32 0, i32 0), i32 %conv196, double %mul200)
  %151 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp202 = icmp sge i32 %151, 4
  br i1 %cmp202, label %if.then204, label %if.end206

if.then204:                                       ; preds = %if.then193
  %152 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call205 = call i32 @fflush(%struct._IO_FILE* %152)
  br label %if.end206

if.end206:                                        ; preds = %if.then204, %if.then193
  br label %if.end207

if.end207:                                        ; preds = %if.end206, %lor.lhs.false190
  br label %if.end208

if.end208:                                        ; preds = %if.end207, %if.end180
  %153 = bitcast i32* %sizeCheck209 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %153) #5
  %154 = load i8*, i8** %dstBuffer, align 4, !tbaa !2
  %155 = load i32, i32* %outSize, align 4, !tbaa !24
  %156 = load %struct._IO_FILE*, %struct._IO_FILE** %dstFile, align 4, !tbaa !2
  %call210 = call i32 @fwrite(i8* %154, i32 1, i32 %155, %struct._IO_FILE* %156)
  store i32 %call210, i32* %sizeCheck209, align 4, !tbaa !24
  %157 = load i32, i32* %sizeCheck209, align 4, !tbaa !24
  %158 = load i32, i32* %outSize, align 4, !tbaa !24
  %cmp211 = icmp ne i32 %157, %158
  br i1 %cmp211, label %if.then213, label %if.end229

if.then213:                                       ; preds = %if.end208
  %159 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp214 = icmp sge i32 %159, 1
  br i1 %cmp214, label %if.then216, label %if.end218

if.then216:                                       ; preds = %if.then213
  %160 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call217 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %160, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 36)
  br label %if.end218

if.end218:                                        ; preds = %if.then216, %if.then213
  %161 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp219 = icmp sge i32 %161, 1
  br i1 %cmp219, label %if.then221, label %if.end223

if.then221:                                       ; preds = %if.end218
  %162 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call222 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %162, i8* getelementptr inbounds ([44 x i8], [44 x i8]* @.str.10, i32 0, i32 0))
  br label %if.end223

if.end223:                                        ; preds = %if.then221, %if.end218
  %163 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp224 = icmp sge i32 %163, 1
  br i1 %cmp224, label %if.then226, label %if.end228

if.then226:                                       ; preds = %if.end223
  %164 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call227 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %164, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end228

if.end228:                                        ; preds = %if.then226, %if.end223
  call void @exit(i32 36) #6
  unreachable

if.end229:                                        ; preds = %if.end208
  %165 = bitcast i32* %sizeCheck209 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #5
  %166 = load i8*, i8** %srcBuffer, align 4, !tbaa !2
  %167 = load i32, i32* %blockSize, align 4, !tbaa !24
  %168 = load %struct._IO_FILE*, %struct._IO_FILE** %srcFile, align 4, !tbaa !2
  %call230 = call i32 @fread(i8* %166, i32 1, i32 %167, %struct._IO_FILE* %168)
  store i32 %call230, i32* %readSize, align 4, !tbaa !24
  %169 = load i32, i32* %readSize, align 4, !tbaa !24
  %conv231 = zext i32 %169 to i64
  %170 = load i64, i64* %filesize, align 8, !tbaa !26
  %add232 = add i64 %170, %conv231
  store i64 %add232, i64* %filesize, align 8, !tbaa !26
  %171 = bitcast i32* %outSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #5
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %172 = load %struct._IO_FILE*, %struct._IO_FILE** %srcFile, align 4, !tbaa !2
  %call233 = call i32 @ferror(%struct._IO_FILE* %172)
  %tobool234 = icmp ne i32 %call233, 0
  br i1 %tobool234, label %if.then235, label %if.end251

if.then235:                                       ; preds = %while.end
  %173 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp236 = icmp sge i32 %173, 1
  br i1 %cmp236, label %if.then238, label %if.end240

if.then238:                                       ; preds = %if.then235
  %174 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call239 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %174, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 37)
  br label %if.end240

if.end240:                                        ; preds = %if.then238, %if.then235
  %175 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp241 = icmp sge i32 %175, 1
  br i1 %cmp241, label %if.then243, label %if.end245

if.then243:                                       ; preds = %if.end240
  %176 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %177 = load i8*, i8** %srcFileName.addr, align 4, !tbaa !2
  %call244 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %176, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.57, i32 0, i32 0), i8* %177)
  br label %if.end245

if.end245:                                        ; preds = %if.then243, %if.end240
  %178 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp246 = icmp sge i32 %178, 1
  br i1 %cmp246, label %if.then248, label %if.end250

if.then248:                                       ; preds = %if.end245
  %179 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call249 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %179, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end250

if.end250:                                        ; preds = %if.then248, %if.end245
  call void @exit(i32 37) #6
  unreachable

if.end251:                                        ; preds = %while.end
  %180 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %ctx, align 4, !tbaa !2
  %181 = load i8*, i8** %dstBuffer, align 4, !tbaa !2
  %182 = load i32, i32* %dstBufferSize, align 4, !tbaa !24
  %call252 = call i32 @LZ4F_compressEnd(%struct.LZ4F_cctx_s* %180, i8* %181, i32 %182, %struct.LZ4F_compressOptions_t* null)
  store i32 %call252, i32* %headerSize, align 4, !tbaa !24
  %183 = load i32, i32* %headerSize, align 4, !tbaa !24
  %call253 = call i32 @LZ4F_isError(i32 %183)
  %tobool254 = icmp ne i32 %call253, 0
  br i1 %tobool254, label %if.then255, label %if.end272

if.then255:                                       ; preds = %if.end251
  %184 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp256 = icmp sge i32 %184, 1
  br i1 %cmp256, label %if.then258, label %if.end260

if.then258:                                       ; preds = %if.then255
  %185 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call259 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %185, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 38)
  br label %if.end260

if.end260:                                        ; preds = %if.then258, %if.then255
  %186 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp261 = icmp sge i32 %186, 1
  br i1 %cmp261, label %if.then263, label %if.end266

if.then263:                                       ; preds = %if.end260
  %187 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %188 = load i32, i32* %headerSize, align 4, !tbaa !24
  %call264 = call i8* @LZ4F_getErrorName(i32 %188)
  %call265 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %187, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.61, i32 0, i32 0), i8* %call264)
  br label %if.end266

if.end266:                                        ; preds = %if.then263, %if.end260
  %189 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp267 = icmp sge i32 %189, 1
  br i1 %cmp267, label %if.then269, label %if.end271

if.then269:                                       ; preds = %if.end266
  %190 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call270 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %190, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end271

if.end271:                                        ; preds = %if.then269, %if.end266
  call void @exit(i32 38) #6
  unreachable

if.end272:                                        ; preds = %if.end251
  %191 = bitcast i32* %sizeCheck273 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %191) #5
  %192 = load i8*, i8** %dstBuffer, align 4, !tbaa !2
  %193 = load i32, i32* %headerSize, align 4, !tbaa !24
  %194 = load %struct._IO_FILE*, %struct._IO_FILE** %dstFile, align 4, !tbaa !2
  %call274 = call i32 @fwrite(i8* %192, i32 1, i32 %193, %struct._IO_FILE* %194)
  store i32 %call274, i32* %sizeCheck273, align 4, !tbaa !24
  %195 = load i32, i32* %sizeCheck273, align 4, !tbaa !24
  %196 = load i32, i32* %headerSize, align 4, !tbaa !24
  %cmp275 = icmp ne i32 %195, %196
  br i1 %cmp275, label %if.then277, label %if.end293

if.then277:                                       ; preds = %if.end272
  %197 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp278 = icmp sge i32 %197, 1
  br i1 %cmp278, label %if.then280, label %if.end282

if.then280:                                       ; preds = %if.then277
  %198 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call281 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %198, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 39)
  br label %if.end282

if.end282:                                        ; preds = %if.then280, %if.then277
  %199 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp283 = icmp sge i32 %199, 1
  br i1 %cmp283, label %if.then285, label %if.end287

if.then285:                                       ; preds = %if.end282
  %200 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call286 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %200, i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.62, i32 0, i32 0))
  br label %if.end287

if.end287:                                        ; preds = %if.then285, %if.end282
  %201 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp288 = icmp sge i32 %201, 1
  br i1 %cmp288, label %if.then290, label %if.end292

if.then290:                                       ; preds = %if.end287
  %202 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call291 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %202, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end292

if.end292:                                        ; preds = %if.then290, %if.end287
  call void @exit(i32 39) #6
  unreachable

if.end293:                                        ; preds = %if.end272
  %203 = bitcast i32* %sizeCheck273 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #5
  %204 = load i32, i32* %headerSize, align 4, !tbaa !24
  %conv294 = zext i32 %204 to i64
  %205 = load i64, i64* %compressedfilesize, align 8, !tbaa !26
  %add295 = add i64 %205, %conv294
  store i64 %add295, i64* %compressedfilesize, align 8, !tbaa !26
  %206 = bitcast i32* %headerSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #5
  br label %if.end296

if.end296:                                        ; preds = %if.end293, %if.end112
  %207 = load %struct._IO_FILE*, %struct._IO_FILE** %srcFile, align 4, !tbaa !2
  %call297 = call i32 @fclose(%struct._IO_FILE* %207)
  %208 = load i8*, i8** %dstFileName.addr, align 4, !tbaa !2
  %call298 = call i32 @strcmp(i8* %208, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0))
  %tobool299 = icmp ne i32 %call298, 0
  br i1 %tobool299, label %if.then300, label %if.end302

if.then300:                                       ; preds = %if.end296
  %209 = load %struct._IO_FILE*, %struct._IO_FILE** %dstFile, align 4, !tbaa !2
  %call301 = call i32 @fclose(%struct._IO_FILE* %209)
  br label %if.end302

if.end302:                                        ; preds = %if.then300, %if.end296
  %210 = bitcast %struct.stat* %statbuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 88, i8* %210) #5
  %211 = load i8*, i8** %srcFileName.addr, align 4, !tbaa !2
  %call303 = call i32 @strcmp(i8* %211, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @stdinmark, i32 0, i32 0))
  %tobool304 = icmp ne i32 %call303, 0
  br i1 %tobool304, label %land.lhs.true, label %if.end315

land.lhs.true:                                    ; preds = %if.end302
  %212 = load i8*, i8** %dstFileName.addr, align 4, !tbaa !2
  %call305 = call i32 @strcmp(i8* %212, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0))
  %tobool306 = icmp ne i32 %call305, 0
  br i1 %tobool306, label %land.lhs.true307, label %if.end315

land.lhs.true307:                                 ; preds = %land.lhs.true
  %213 = load i8*, i8** %dstFileName.addr, align 4, !tbaa !2
  %call308 = call i32 @strcmp(i8* %213, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @nulmark, i32 0, i32 0))
  %tobool309 = icmp ne i32 %call308, 0
  br i1 %tobool309, label %land.lhs.true310, label %if.end315

land.lhs.true310:                                 ; preds = %land.lhs.true307
  %214 = load i8*, i8** %srcFileName.addr, align 4, !tbaa !2
  %call311 = call i32 @UTIL_getFileStat(i8* %214, %struct.stat* %statbuf)
  %tobool312 = icmp ne i32 %call311, 0
  br i1 %tobool312, label %if.then313, label %if.end315

if.then313:                                       ; preds = %land.lhs.true310
  %215 = load i8*, i8** %dstFileName.addr, align 4, !tbaa !2
  %call314 = call i32 @UTIL_setFileStat(i8* %215, %struct.stat* %statbuf)
  br label %if.end315

if.end315:                                        ; preds = %if.then313, %land.lhs.true310, %land.lhs.true307, %land.lhs.true, %if.end302
  %216 = bitcast %struct.stat* %statbuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 88, i8* %216) #5
  %217 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %io_prefs.addr, align 4, !tbaa !2
  %removeSrcFile = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %217, i32 0, i32 13
  %218 = load i32, i32* %removeSrcFile, align 4, !tbaa !23
  %tobool316 = icmp ne i32 %218, 0
  br i1 %tobool316, label %if.then317, label %if.end339

if.then317:                                       ; preds = %if.end315
  %219 = load i8*, i8** %srcFileName.addr, align 4, !tbaa !2
  %call318 = call i32 @remove(i8* %219)
  %tobool319 = icmp ne i32 %call318, 0
  br i1 %tobool319, label %if.then320, label %if.end338

if.then320:                                       ; preds = %if.then317
  %220 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp321 = icmp sge i32 %220, 1
  br i1 %cmp321, label %if.then323, label %if.end325

if.then323:                                       ; preds = %if.then320
  %221 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call324 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %221, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 40)
  br label %if.end325

if.end325:                                        ; preds = %if.then323, %if.then320
  %222 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp326 = icmp sge i32 %222, 1
  br i1 %cmp326, label %if.then328, label %if.end332

if.then328:                                       ; preds = %if.end325
  %223 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %224 = load i8*, i8** %srcFileName.addr, align 4, !tbaa !2
  %call329 = call i32* @__errno_location()
  %225 = load i32, i32* %call329, align 4, !tbaa !6
  %call330 = call i8* @strerror(i32 %225)
  %call331 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %223, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.63, i32 0, i32 0), i8* %224, i8* %call330)
  br label %if.end332

if.end332:                                        ; preds = %if.then328, %if.end325
  %226 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp333 = icmp sge i32 %226, 1
  br i1 %cmp333, label %if.then335, label %if.end337

if.then335:                                       ; preds = %if.end332
  %227 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call336 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %227, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end337

if.end337:                                        ; preds = %if.then335, %if.end332
  call void @exit(i32 40) #6
  unreachable

if.end338:                                        ; preds = %if.then317
  br label %if.end339

if.end339:                                        ; preds = %if.end338, %if.end315
  %228 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp340 = icmp sge i32 %228, 2
  br i1 %cmp340, label %if.then342, label %if.end344

if.then342:                                       ; preds = %if.end339
  %229 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call343 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %229, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.12, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @.str.13, i32 0, i32 0))
  br label %if.end344

if.end344:                                        ; preds = %if.then342, %if.end339
  %230 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp345 = icmp sge i32 %230, 2
  br i1 %cmp345, label %if.then347, label %if.end358

if.then347:                                       ; preds = %if.end344
  %231 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %232 = load i64, i64* %filesize, align 8, !tbaa !26
  %233 = load i64, i64* %compressedfilesize, align 8, !tbaa !26
  %234 = load i64, i64* %compressedfilesize, align 8, !tbaa !26
  %conv348 = uitofp i64 %234 to double
  %235 = load i64, i64* %filesize, align 8, !tbaa !26
  %236 = load i64, i64* %filesize, align 8, !tbaa !26
  %tobool349 = icmp ne i64 %236, 0
  %lnot350 = xor i1 %tobool349, true
  %lnot.ext351 = zext i1 %lnot350 to i32
  %conv352 = sext i32 %lnot.ext351 to i64
  %add353 = add i64 %235, %conv352
  %conv354 = uitofp i64 %add353 to double
  %div355 = fdiv double %conv348, %conv354
  %mul356 = fmul double %div355, 1.000000e+02
  %call357 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %231, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @.str.14, i32 0, i32 0), i64 %232, i64 %233, double %mul356)
  br label %if.end358

if.end358:                                        ; preds = %if.then347, %if.end344
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end358, %if.then8, %if.then
  %237 = bitcast %struct.LZ4F_preferences_t* %prefs to i8*
  call void @llvm.lifetime.end.p0i8(i64 56, i8* %237) #5
  %238 = bitcast %struct.LZ4F_cctx_s** %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %238) #5
  %239 = bitcast i32* %readSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %239) #5
  %240 = bitcast i32* %blockSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %240) #5
  %241 = bitcast i32* %dstBufferSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %241) #5
  %242 = bitcast i8** %dstBuffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %242) #5
  %243 = bitcast i8** %srcBuffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %243) #5
  %244 = bitcast %struct._IO_FILE** %dstFile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %244) #5
  %245 = bitcast %struct._IO_FILE** %srcFile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %245) #5
  %246 = bitcast i64* %compressedfilesize to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %246) #5
  %247 = bitcast i64* %filesize to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %247) #5
  %248 = load i32, i32* %retval, align 4
  ret i32 %248
}

; Function Attrs: nounwind
define internal void @LZ4IO_freeCResources(%struct.cRess_t* byval(%struct.cRess_t) align 4 %ress) #0 {
entry:
  %errorCode = alloca i32, align 4
  %srcBuffer = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %ress, i32 0, i32 0
  %0 = load i8*, i8** %srcBuffer, align 4, !tbaa !30
  call void @free(i8* %0)
  %dstBuffer = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %ress, i32 0, i32 2
  %1 = load i8*, i8** %dstBuffer, align 4, !tbaa !34
  call void @free(i8* %1)
  %cdict = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %ress, i32 0, i32 5
  %2 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !35
  call void @LZ4F_freeCDict(%struct.LZ4F_CDict_s* %2)
  %cdict1 = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %ress, i32 0, i32 5
  store %struct.LZ4F_CDict_s* null, %struct.LZ4F_CDict_s** %cdict1, align 4, !tbaa !35
  %3 = bitcast i32* %errorCode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %ctx = getelementptr inbounds %struct.cRess_t, %struct.cRess_t* %ress, i32 0, i32 4
  %4 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %ctx, align 4, !tbaa !36
  %call = call i32 @LZ4F_freeCompressionContext(%struct.LZ4F_cctx_s* %4)
  store i32 %call, i32* %errorCode, align 4, !tbaa !24
  %5 = load i32, i32* %errorCode, align 4, !tbaa !24
  %call2 = call i32 @LZ4F_isError(i32 %5)
  %tobool = icmp ne i32 %call2, 0
  br i1 %tobool, label %if.then, label %if.end14

if.then:                                          ; preds = %entry
  %6 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp = icmp sge i32 %6, 1
  br i1 %cmp, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %7 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call4 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %7, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 38)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %8 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp5 = icmp sge i32 %8, 1
  br i1 %cmp5, label %if.then6, label %if.end9

if.then6:                                         ; preds = %if.end
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %10 = load i32, i32* %errorCode, align 4, !tbaa !24
  %call7 = call i8* @LZ4F_getErrorName(i32 %10)
  %call8 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %9, i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.64, i32 0, i32 0), i8* %call7)
  br label %if.end9

if.end9:                                          ; preds = %if.then6, %if.end
  %11 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp10 = icmp sge i32 %11, 1
  br i1 %cmp10, label %if.then11, label %if.end13

if.then11:                                        ; preds = %if.end9
  %12 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call12 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %12, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end13

if.end13:                                         ; preds = %if.then11, %if.end9
  call void @exit(i32 38) #6
  unreachable

if.end14:                                         ; preds = %entry
  %13 = bitcast i32* %errorCode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  ret void
}

; Function Attrs: nounwind
define internal i64 @UTIL_clockSpanNano(i32 %clockStart) #0 {
entry:
  %clockStart.addr = alloca i32, align 4
  %clockEnd = alloca i32, align 4
  store i32 %clockStart, i32* %clockStart.addr, align 4, !tbaa !24
  %0 = bitcast i32* %clockEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %call = call i32 @UTIL_getTime()
  store i32 %call, i32* %clockEnd, align 4, !tbaa !24
  %1 = load i32, i32* %clockStart.addr, align 4, !tbaa !24
  %2 = load i32, i32* %clockEnd, align 4, !tbaa !24
  %call1 = call i64 @UTIL_getSpanTimeNano(i32 %1, i32 %2)
  %3 = bitcast i32* %clockEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #5
  ret i64 %call1
}

; Function Attrs: nounwind
define hidden i32 @LZ4IO_compressMultipleFilenames(%struct.LZ4IO_prefs_s* %prefs, i8** %inFileNamesTable, i32 %ifntSize, i8* %suffix, i32 %compressionLevel) #0 {
entry:
  %retval = alloca i32, align 4
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %inFileNamesTable.addr = alloca i8**, align 4
  %ifntSize.addr = alloca i32, align 4
  %suffix.addr = alloca i8*, align 4
  %compressionLevel.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %missed_files = alloca i32, align 4
  %dstFileName = alloca i8*, align 4
  %ofnSize = alloca i32, align 4
  %suffixSize = alloca i32, align 4
  %ress = alloca %struct.cRess_t, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %tmp = alloca %struct.cRess_t, align 4
  %ifnSize = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i8** %inFileNamesTable, i8*** %inFileNamesTable.addr, align 4, !tbaa !2
  store i32 %ifntSize, i32* %ifntSize.addr, align 4, !tbaa !6
  store i8* %suffix, i8** %suffix.addr, align 4, !tbaa !2
  store i32 %compressionLevel, i32* %compressionLevel.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %missed_files to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 0, i32* %missed_files, align 4, !tbaa !6
  %2 = bitcast i8** %dstFileName to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %call = call i8* @malloc(i32 30)
  store i8* %call, i8** %dstFileName, align 4, !tbaa !2
  %3 = bitcast i32* %ofnSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  store i32 30, i32* %ofnSize, align 4, !tbaa !24
  %4 = bitcast i32* %suffixSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i8*, i8** %suffix.addr, align 4, !tbaa !2
  %call1 = call i32 @strlen(i8* %5)
  store i32 %call1, i32* %suffixSize, align 4, !tbaa !24
  %6 = bitcast %struct.cRess_t* %ress to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %6) #5
  %7 = load i8*, i8** %dstFileName, align 4, !tbaa !2
  %cmp = icmp eq i8* %7, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %8 = load i32, i32* %ifntSize.addr, align 4, !tbaa !6
  store i32 %8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup25

if.end:                                           ; preds = %entry
  %9 = bitcast %struct.cRess_t* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %9) #5
  %10 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  call void @LZ4IO_createCResources(%struct.cRess_t* sret align 4 %tmp, %struct.LZ4IO_prefs_s* %10)
  %11 = bitcast %struct.cRess_t* %ress to i8*
  %12 = bitcast %struct.cRess_t* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 24, i1 false), !tbaa.struct !47
  %13 = bitcast %struct.cRess_t* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %13) #5
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %15 = load i32, i32* %ifntSize.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %14, %15
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %16 = bitcast i32* %ifnSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #5
  %17 = load i8**, i8*** %inFileNamesTable.addr, align 4, !tbaa !2
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8*, i8** %17, i32 %18
  %19 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %call3 = call i32 @strlen(i8* %19)
  store i32 %call3, i32* %ifnSize, align 4, !tbaa !24
  %20 = load i8*, i8** %suffix.addr, align 4, !tbaa !2
  %call4 = call i32 @strcmp(i8* %20, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0))
  %tobool = icmp ne i32 %call4, 0
  br i1 %tobool, label %if.end8, label %if.then5

if.then5:                                         ; preds = %for.body
  %21 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %22 = load i8**, i8*** %inFileNamesTable.addr, align 4, !tbaa !2
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds i8*, i8** %22, i32 %23
  %24 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  %25 = load i32, i32* %compressionLevel.addr, align 4, !tbaa !6
  %call7 = call i32 @LZ4IO_compressFilename_extRess(%struct.LZ4IO_prefs_s* %21, %struct.cRess_t* byval(%struct.cRess_t) align 4 %ress, i8* %24, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0), i32 %25)
  %26 = load i32, i32* %missed_files, align 4, !tbaa !6
  %add = add nsw i32 %26, %call7
  store i32 %add, i32* %missed_files, align 4, !tbaa !6
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %for.body
  %27 = load i32, i32* %ofnSize, align 4, !tbaa !24
  %28 = load i32, i32* %ifnSize, align 4, !tbaa !24
  %29 = load i32, i32* %suffixSize, align 4, !tbaa !24
  %add9 = add i32 %28, %29
  %add10 = add i32 %add9, 1
  %cmp11 = icmp ule i32 %27, %add10
  br i1 %cmp11, label %if.then12, label %if.end18

if.then12:                                        ; preds = %if.end8
  %30 = load i8*, i8** %dstFileName, align 4, !tbaa !2
  call void @free(i8* %30)
  %31 = load i32, i32* %ifnSize, align 4, !tbaa !24
  %add13 = add i32 %31, 20
  store i32 %add13, i32* %ofnSize, align 4, !tbaa !24
  %32 = load i32, i32* %ofnSize, align 4, !tbaa !24
  %call14 = call i8* @malloc(i32 %32)
  store i8* %call14, i8** %dstFileName, align 4, !tbaa !2
  %33 = load i8*, i8** %dstFileName, align 4, !tbaa !2
  %cmp15 = icmp eq i8* %33, null
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.then12
  call void @LZ4IO_freeCResources(%struct.cRess_t* byval(%struct.cRess_t) align 4 %ress)
  %34 = load i32, i32* %ifntSize.addr, align 4, !tbaa !6
  store i32 %34, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end17:                                         ; preds = %if.then12
  br label %if.end18

if.end18:                                         ; preds = %if.end17, %if.end8
  %35 = load i8*, i8** %dstFileName, align 4, !tbaa !2
  %36 = load i8**, i8*** %inFileNamesTable.addr, align 4, !tbaa !2
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx19 = getelementptr inbounds i8*, i8** %36, i32 %37
  %38 = load i8*, i8** %arrayidx19, align 4, !tbaa !2
  %call20 = call i8* @strcpy(i8* %35, i8* %38)
  %39 = load i8*, i8** %dstFileName, align 4, !tbaa !2
  %40 = load i8*, i8** %suffix.addr, align 4, !tbaa !2
  %call21 = call i8* @strcat(i8* %39, i8* %40)
  %41 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %42 = load i8**, i8*** %inFileNamesTable.addr, align 4, !tbaa !2
  %43 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds i8*, i8** %42, i32 %43
  %44 = load i8*, i8** %arrayidx22, align 4, !tbaa !2
  %45 = load i8*, i8** %dstFileName, align 4, !tbaa !2
  %46 = load i32, i32* %compressionLevel.addr, align 4, !tbaa !6
  %call23 = call i32 @LZ4IO_compressFilename_extRess(%struct.LZ4IO_prefs_s* %41, %struct.cRess_t* byval(%struct.cRess_t) align 4 %ress, i8* %44, i8* %45, i32 %46)
  %47 = load i32, i32* %missed_files, align 4, !tbaa !6
  %add24 = add nsw i32 %47, %call23
  store i32 %add24, i32* %missed_files, align 4, !tbaa !6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end18, %if.then16, %if.then5
  %48 = bitcast i32* %ifnSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup25 [
    i32 0, label %cleanup.cont
    i32 4, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %49 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %49, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @LZ4IO_freeCResources(%struct.cRess_t* byval(%struct.cRess_t) align 4 %ress)
  %50 = load i8*, i8** %dstFileName, align 4, !tbaa !2
  call void @free(i8* %50)
  %51 = load i32, i32* %missed_files, align 4, !tbaa !6
  store i32 %51, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup25

cleanup25:                                        ; preds = %for.end, %cleanup, %if.then
  %52 = bitcast %struct.cRess_t* %ress to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %52) #5
  %53 = bitcast i32* %suffixSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #5
  %54 = bitcast i32* %ofnSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #5
  %55 = bitcast i8** %dstFileName to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #5
  %56 = bitcast i32* %missed_files to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #5
  %57 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #5
  %58 = load i32, i32* %retval, align 4
  ret i32 %58
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define hidden i32 @LZ4IO_decompressFilename(%struct.LZ4IO_prefs_s* %prefs, i8* %input_filename, i8* %output_filename) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %input_filename.addr = alloca i8*, align 4
  %output_filename.addr = alloca i8*, align 4
  %ress = alloca %struct.dRess_t, align 4
  %start = alloca i32, align 4
  %missingFiles = alloca i32, align 4
  %end = alloca i32, align 4
  %seconds = alloca double, align 8
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i8* %input_filename, i8** %input_filename.addr, align 4, !tbaa !2
  store i8* %output_filename, i8** %output_filename.addr, align 4, !tbaa !2
  %0 = bitcast %struct.dRess_t* %ress to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %0) #5
  %1 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  call void @LZ4IO_createDResources(%struct.dRess_t* sret align 4 %ress, %struct.LZ4IO_prefs_s* %1)
  %2 = bitcast i32* %start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %call = call i32 @clock()
  store i32 %call, i32* %start, align 4, !tbaa !24
  %3 = bitcast i32* %missingFiles to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %5 = load i8*, i8** %input_filename.addr, align 4, !tbaa !2
  %6 = load i8*, i8** %output_filename.addr, align 4, !tbaa !2
  %call1 = call i32 @LZ4IO_decompressDstFile(%struct.LZ4IO_prefs_s* %4, %struct.dRess_t* byval(%struct.dRess_t) align 4 %ress, i8* %5, i8* %6)
  store i32 %call1, i32* %missingFiles, align 4, !tbaa !6
  %7 = bitcast i32* %end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %call2 = call i32 @clock()
  store i32 %call2, i32* %end, align 4, !tbaa !24
  %8 = bitcast double* %seconds to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %8) #5
  %9 = load i32, i32* %end, align 4, !tbaa !24
  %10 = load i32, i32* %start, align 4, !tbaa !24
  %sub = sub nsw i32 %9, %10
  %conv = sitofp i32 %sub to double
  %div = fdiv double %conv, 1.000000e+06
  store double %div, double* %seconds, align 8, !tbaa !28
  %11 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp = icmp sge i32 %11, 4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %12 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %13 = load double, double* %seconds, align 8, !tbaa !28
  %call4 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %12, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.17, i32 0, i32 0), double %13)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  call void @LZ4IO_freeDResources(%struct.dRess_t* byval(%struct.dRess_t) align 4 %ress)
  %14 = load i32, i32* %missingFiles, align 4, !tbaa !6
  %15 = bitcast double* %seconds to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %15) #5
  %16 = bitcast i32* %end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #5
  %17 = bitcast i32* %missingFiles to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  %18 = bitcast i32* %start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #5
  %19 = bitcast %struct.dRess_t* %ress to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %19) #5
  ret i32 %14
}

; Function Attrs: nounwind
define internal void @LZ4IO_createDResources(%struct.dRess_t* noalias sret align 4 %agg.result, %struct.LZ4IO_prefs_s* %prefs) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %errorCode = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %0 = bitcast i32* %errorCode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %dCtx = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %agg.result, i32 0, i32 5
  %call = call i32 @LZ4F_createDecompressionContext(%struct.LZ4F_dctx_s** %dCtx, i32 100)
  store i32 %call, i32* %errorCode, align 4, !tbaa !24
  %1 = load i32, i32* %errorCode, align 4, !tbaa !24
  %call1 = call i32 @LZ4F_isError(i32 %1)
  %tobool = icmp ne i32 %call1, 0
  br i1 %tobool, label %if.then, label %if.end13

if.then:                                          ; preds = %entry
  %2 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp = icmp sge i32 %2, 1
  br i1 %cmp, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %3 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call3 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %3, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 60)
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  %4 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp4 = icmp sge i32 %4, 1
  br i1 %cmp4, label %if.then5, label %if.end8

if.then5:                                         ; preds = %if.end
  %5 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %6 = load i32, i32* %errorCode, align 4, !tbaa !24
  %call6 = call i8* @LZ4F_getErrorName(i32 %6)
  %call7 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %5, i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.65, i32 0, i32 0), i8* %call6)
  br label %if.end8

if.end8:                                          ; preds = %if.then5, %if.end
  %7 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp9 = icmp sge i32 %7, 1
  br i1 %cmp9, label %if.then10, label %if.end12

if.then10:                                        ; preds = %if.end8
  %8 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call11 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %8, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end12

if.end12:                                         ; preds = %if.then10, %if.end8
  call void @exit(i32 60) #6
  unreachable

if.end13:                                         ; preds = %entry
  %srcBufferSize = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %agg.result, i32 0, i32 1
  store i32 65536, i32* %srcBufferSize, align 4, !tbaa !48
  %srcBufferSize14 = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %agg.result, i32 0, i32 1
  %9 = load i32, i32* %srcBufferSize14, align 4, !tbaa !48
  %call15 = call i8* @malloc(i32 %9)
  %srcBuffer = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %agg.result, i32 0, i32 0
  store i8* %call15, i8** %srcBuffer, align 4, !tbaa !50
  %dstBufferSize = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %agg.result, i32 0, i32 3
  store i32 65536, i32* %dstBufferSize, align 4, !tbaa !51
  %dstBufferSize16 = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %agg.result, i32 0, i32 3
  %10 = load i32, i32* %dstBufferSize16, align 4, !tbaa !51
  %call17 = call i8* @malloc(i32 %10)
  %dstBuffer = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %agg.result, i32 0, i32 2
  store i8* %call17, i8** %dstBuffer, align 4, !tbaa !52
  %srcBuffer18 = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %agg.result, i32 0, i32 0
  %11 = load i8*, i8** %srcBuffer18, align 4, !tbaa !50
  %tobool19 = icmp ne i8* %11, null
  br i1 %tobool19, label %lor.lhs.false, label %if.then22

lor.lhs.false:                                    ; preds = %if.end13
  %dstBuffer20 = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %agg.result, i32 0, i32 2
  %12 = load i8*, i8** %dstBuffer20, align 4, !tbaa !52
  %tobool21 = icmp ne i8* %12, null
  br i1 %tobool21, label %if.end35, label %if.then22

if.then22:                                        ; preds = %lor.lhs.false, %if.end13
  %13 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp23 = icmp sge i32 %13, 1
  br i1 %cmp23, label %if.then24, label %if.end26

if.then24:                                        ; preds = %if.then22
  %14 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call25 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %14, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 61)
  br label %if.end26

if.end26:                                         ; preds = %if.then24, %if.then22
  %15 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp27 = icmp sge i32 %15, 1
  br i1 %cmp27, label %if.then28, label %if.end30

if.then28:                                        ; preds = %if.end26
  %16 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call29 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %16, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.1, i32 0, i32 0))
  br label %if.end30

if.end30:                                         ; preds = %if.then28, %if.end26
  %17 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp31 = icmp sge i32 %17, 1
  br i1 %cmp31, label %if.then32, label %if.end34

if.then32:                                        ; preds = %if.end30
  %18 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call33 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %18, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end34

if.end34:                                         ; preds = %if.then32, %if.end30
  call void @exit(i32 61) #6
  unreachable

if.end35:                                         ; preds = %lor.lhs.false
  %19 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  call void @LZ4IO_loadDDict(%struct.LZ4IO_prefs_s* %19, %struct.dRess_t* %agg.result)
  %dstFile = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %agg.result, i32 0, i32 4
  store %struct._IO_FILE* null, %struct._IO_FILE** %dstFile, align 4, !tbaa !53
  %20 = bitcast i32* %errorCode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  ret void
}

; Function Attrs: nounwind
define internal i32 @LZ4IO_decompressDstFile(%struct.LZ4IO_prefs_s* %prefs, %struct.dRess_t* byval(%struct.dRess_t) align 4 %ress, i8* %input_filename, i8* %output_filename) #0 {
entry:
  %retval = alloca i32, align 4
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %input_filename.addr = alloca i8*, align 4
  %output_filename.addr = alloca i8*, align 4
  %statbuf = alloca %struct.stat, align 8
  %stat_result = alloca i32, align 4
  %foutput = alloca %struct._IO_FILE*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i8* %input_filename, i8** %input_filename.addr, align 4, !tbaa !2
  store i8* %output_filename, i8** %output_filename.addr, align 4, !tbaa !2
  %0 = bitcast %struct.stat* %statbuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 88, i8* %0) #5
  %1 = bitcast i32* %stat_result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 0, i32* %stat_result, align 4, !tbaa !6
  %2 = bitcast %struct._IO_FILE** %foutput to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %output_filename.addr, align 4, !tbaa !2
  %call = call %struct._IO_FILE* @LZ4IO_openDstFile(%struct.LZ4IO_prefs_s* %3, i8* %4)
  store %struct._IO_FILE* %call, %struct._IO_FILE** %foutput, align 4, !tbaa !2
  %5 = load %struct._IO_FILE*, %struct._IO_FILE** %foutput, align 4, !tbaa !2
  %cmp = icmp eq %struct._IO_FILE* %5, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %6 = load i8*, i8** %input_filename.addr, align 4, !tbaa !2
  %call1 = call i32 @strcmp(i8* %6, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @stdinmark, i32 0, i32 0))
  %tobool = icmp ne i32 %call1, 0
  br i1 %tobool, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %7 = load i8*, i8** %input_filename.addr, align 4, !tbaa !2
  %call2 = call i32 @UTIL_getFileStat(i8* %7, %struct.stat* %statbuf)
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %land.lhs.true
  store i32 1, i32* %stat_result, align 4, !tbaa !6
  br label %if.end5

if.end5:                                          ; preds = %if.then4, %land.lhs.true, %if.end
  %8 = load %struct._IO_FILE*, %struct._IO_FILE** %foutput, align 4, !tbaa !2
  %dstFile = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 4
  store %struct._IO_FILE* %8, %struct._IO_FILE** %dstFile, align 4, !tbaa !53
  %9 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %input_filename.addr, align 4, !tbaa !2
  %11 = load i8*, i8** %output_filename.addr, align 4, !tbaa !2
  %call6 = call i32 @LZ4IO_decompressSrcFile(%struct.LZ4IO_prefs_s* %9, %struct.dRess_t* byval(%struct.dRess_t) align 4 %ress, i8* %10, i8* %11)
  %12 = load %struct._IO_FILE*, %struct._IO_FILE** %foutput, align 4, !tbaa !2
  %call7 = call i32 @fclose(%struct._IO_FILE* %12)
  %13 = load i32, i32* %stat_result, align 4, !tbaa !6
  %cmp8 = icmp ne i32 %13, 0
  br i1 %cmp8, label %land.lhs.true9, label %if.end17

land.lhs.true9:                                   ; preds = %if.end5
  %14 = load i8*, i8** %output_filename.addr, align 4, !tbaa !2
  %call10 = call i32 @strcmp(i8* %14, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0))
  %tobool11 = icmp ne i32 %call10, 0
  br i1 %tobool11, label %land.lhs.true12, label %if.end17

land.lhs.true12:                                  ; preds = %land.lhs.true9
  %15 = load i8*, i8** %output_filename.addr, align 4, !tbaa !2
  %call13 = call i32 @strcmp(i8* %15, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @nulmark, i32 0, i32 0))
  %tobool14 = icmp ne i32 %call13, 0
  br i1 %tobool14, label %if.then15, label %if.end17

if.then15:                                        ; preds = %land.lhs.true12
  %16 = load i8*, i8** %output_filename.addr, align 4, !tbaa !2
  %call16 = call i32 @UTIL_setFileStat(i8* %16, %struct.stat* %statbuf)
  br label %if.end17

if.end17:                                         ; preds = %if.then15, %land.lhs.true12, %land.lhs.true9, %if.end5
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end17, %if.then
  %17 = bitcast %struct._IO_FILE** %foutput to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  %18 = bitcast i32* %stat_result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #5
  %19 = bitcast %struct.stat* %statbuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 88, i8* %19) #5
  %20 = load i32, i32* %retval, align 4
  ret i32 %20
}

; Function Attrs: nounwind
define internal void @LZ4IO_freeDResources(%struct.dRess_t* byval(%struct.dRess_t) align 4 %ress) #0 {
entry:
  %errorCode = alloca i32, align 4
  %0 = bitcast i32* %errorCode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %dCtx = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 5
  %1 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dCtx, align 4, !tbaa !54
  %call = call i32 @LZ4F_freeDecompressionContext(%struct.LZ4F_dctx_s* %1)
  store i32 %call, i32* %errorCode, align 4, !tbaa !24
  %2 = load i32, i32* %errorCode, align 4, !tbaa !24
  %call1 = call i32 @LZ4F_isError(i32 %2)
  %tobool = icmp ne i32 %call1, 0
  br i1 %tobool, label %if.then, label %if.end13

if.then:                                          ; preds = %entry
  %3 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp = icmp sge i32 %3, 1
  br i1 %cmp, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %4 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call3 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %4, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 69)
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  %5 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp4 = icmp sge i32 %5, 1
  br i1 %cmp4, label %if.then5, label %if.end8

if.then5:                                         ; preds = %if.end
  %6 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %7 = load i32, i32* %errorCode, align 4, !tbaa !24
  %call6 = call i8* @LZ4F_getErrorName(i32 %7)
  %call7 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %6, i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.64, i32 0, i32 0), i8* %call6)
  br label %if.end8

if.end8:                                          ; preds = %if.then5, %if.end
  %8 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp9 = icmp sge i32 %8, 1
  br i1 %cmp9, label %if.then10, label %if.end12

if.then10:                                        ; preds = %if.end8
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call11 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %9, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end12

if.end12:                                         ; preds = %if.then10, %if.end8
  call void @exit(i32 69) #6
  unreachable

if.end13:                                         ; preds = %entry
  %srcBuffer = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 0
  %10 = load i8*, i8** %srcBuffer, align 4, !tbaa !50
  call void @free(i8* %10)
  %dstBuffer = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 2
  %11 = load i8*, i8** %dstBuffer, align 4, !tbaa !52
  call void @free(i8* %11)
  %dictBuffer = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 6
  %12 = load i8*, i8** %dictBuffer, align 4, !tbaa !55
  call void @free(i8* %12)
  %13 = bitcast i32* %errorCode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  ret void
}

; Function Attrs: nounwind
define hidden i32 @LZ4IO_decompressMultipleFilenames(%struct.LZ4IO_prefs_s* %prefs, i8** %inFileNamesTable, i32 %ifntSize, i8* %suffix) #0 {
entry:
  %retval = alloca i32, align 4
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %inFileNamesTable.addr = alloca i8**, align 4
  %ifntSize.addr = alloca i32, align 4
  %suffix.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %skippedFiles = alloca i32, align 4
  %missingFiles = alloca i32, align 4
  %outFileName = alloca i8*, align 4
  %ofnSize = alloca i32, align 4
  %suffixSize = alloca i32, align 4
  %ress = alloca %struct.dRess_t, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ifnSize = alloca i32, align 4
  %suffixPtr = alloca i8*, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i8** %inFileNamesTable, i8*** %inFileNamesTable.addr, align 4, !tbaa !2
  store i32 %ifntSize, i32* %ifntSize.addr, align 4, !tbaa !6
  store i8* %suffix, i8** %suffix.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %skippedFiles to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 0, i32* %skippedFiles, align 4, !tbaa !6
  %2 = bitcast i32* %missingFiles to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i32 0, i32* %missingFiles, align 4, !tbaa !6
  %3 = bitcast i8** %outFileName to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %call = call i8* @malloc(i32 30)
  store i8* %call, i8** %outFileName, align 4, !tbaa !2
  %4 = bitcast i32* %ofnSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  store i32 30, i32* %ofnSize, align 4, !tbaa !24
  %5 = bitcast i32* %suffixSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load i8*, i8** %suffix.addr, align 4, !tbaa !2
  %call1 = call i32 @strlen(i8* %6)
  store i32 %call1, i32* %suffixSize, align 4, !tbaa !24
  %7 = bitcast %struct.dRess_t* %ress to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %7) #5
  %8 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  call void @LZ4IO_createDResources(%struct.dRess_t* sret align 4 %ress, %struct.LZ4IO_prefs_s* %8)
  %9 = load i8*, i8** %outFileName, align 4, !tbaa !2
  %cmp = icmp eq i8* %9, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %10 = load i32, i32* %ifntSize.addr, align 4, !tbaa !6
  store i32 %10, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup41

if.end:                                           ; preds = %entry
  %11 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %call2 = call %struct._IO_FILE* @LZ4IO_openDstFile(%struct.LZ4IO_prefs_s* %11, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0))
  %dstFile = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 4
  store %struct._IO_FILE* %call2, %struct._IO_FILE** %dstFile, align 4, !tbaa !53
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %13 = load i32, i32* %ifntSize.addr, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %12, %13
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %14 = bitcast i32* %ifnSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %15 = load i8**, i8*** %inFileNamesTable.addr, align 4, !tbaa !2
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8*, i8** %15, i32 %16
  %17 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %call4 = call i32 @strlen(i8* %17)
  store i32 %call4, i32* %ifnSize, align 4, !tbaa !24
  %18 = bitcast i8** %suffixPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #5
  %19 = load i8**, i8*** %inFileNamesTable.addr, align 4, !tbaa !2
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds i8*, i8** %19, i32 %20
  %21 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  %22 = load i32, i32* %ifnSize, align 4, !tbaa !24
  %add.ptr = getelementptr inbounds i8, i8* %21, i32 %22
  %23 = load i32, i32* %suffixSize, align 4, !tbaa !24
  %idx.neg = sub i32 0, %23
  %add.ptr6 = getelementptr inbounds i8, i8* %add.ptr, i32 %idx.neg
  store i8* %add.ptr6, i8** %suffixPtr, align 4, !tbaa !2
  %24 = load i8*, i8** %suffix.addr, align 4, !tbaa !2
  %call7 = call i32 @strcmp(i8* %24, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0))
  %tobool = icmp ne i32 %call7, 0
  br i1 %tobool, label %if.end11, label %if.then8

if.then8:                                         ; preds = %for.body
  %25 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %26 = load i8**, i8*** %inFileNamesTable.addr, align 4, !tbaa !2
  %27 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds i8*, i8** %26, i32 %27
  %28 = load i8*, i8** %arrayidx9, align 4, !tbaa !2
  %call10 = call i32 @LZ4IO_decompressSrcFile(%struct.LZ4IO_prefs_s* %25, %struct.dRess_t* byval(%struct.dRess_t) align 4 %ress, i8* %28, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @stdoutmark, i32 0, i32 0))
  %29 = load i32, i32* %missingFiles, align 4, !tbaa !6
  %add = add nsw i32 %29, %call10
  store i32 %add, i32* %missingFiles, align 4, !tbaa !6
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end11:                                         ; preds = %for.body
  %30 = load i32, i32* %ofnSize, align 4, !tbaa !24
  %31 = load i32, i32* %ifnSize, align 4, !tbaa !24
  %32 = load i32, i32* %suffixSize, align 4, !tbaa !24
  %sub = sub i32 %31, %32
  %add12 = add i32 %sub, 1
  %cmp13 = icmp ule i32 %30, %add12
  br i1 %cmp13, label %if.then14, label %if.end20

if.then14:                                        ; preds = %if.end11
  %33 = load i8*, i8** %outFileName, align 4, !tbaa !2
  call void @free(i8* %33)
  %34 = load i32, i32* %ifnSize, align 4, !tbaa !24
  %add15 = add i32 %34, 20
  store i32 %add15, i32* %ofnSize, align 4, !tbaa !24
  %35 = load i32, i32* %ofnSize, align 4, !tbaa !24
  %call16 = call i8* @malloc(i32 %35)
  store i8* %call16, i8** %outFileName, align 4, !tbaa !2
  %36 = load i8*, i8** %outFileName, align 4, !tbaa !2
  %cmp17 = icmp eq i8* %36, null
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.then14
  %37 = load i32, i32* %ifntSize.addr, align 4, !tbaa !6
  store i32 %37, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end19:                                         ; preds = %if.then14
  br label %if.end20

if.end20:                                         ; preds = %if.end19, %if.end11
  %38 = load i32, i32* %ifnSize, align 4, !tbaa !24
  %39 = load i32, i32* %suffixSize, align 4, !tbaa !24
  %cmp21 = icmp ule i32 %38, %39
  br i1 %cmp21, label %if.then24, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end20
  %40 = load i8*, i8** %suffixPtr, align 4, !tbaa !2
  %41 = load i8*, i8** %suffix.addr, align 4, !tbaa !2
  %call22 = call i32 @strcmp(i8* %40, i8* %41)
  %cmp23 = icmp ne i32 %call22, 0
  br i1 %cmp23, label %if.then24, label %if.end30

if.then24:                                        ; preds = %lor.lhs.false, %if.end20
  %42 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp25 = icmp sge i32 %42, 1
  br i1 %cmp25, label %if.then26, label %if.end29

if.then26:                                        ; preds = %if.then24
  %43 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %44 = load i8*, i8** %suffix.addr, align 4, !tbaa !2
  %45 = load i8**, i8*** %inFileNamesTable.addr, align 4, !tbaa !2
  %46 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx27 = getelementptr inbounds i8*, i8** %45, i32 %46
  %47 = load i8*, i8** %arrayidx27, align 4, !tbaa !2
  %call28 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %43, i8* getelementptr inbounds ([86 x i8], [86 x i8]* @.str.18, i32 0, i32 0), i8* %44, i8* %47)
  br label %if.end29

if.end29:                                         ; preds = %if.then26, %if.then24
  %48 = load i32, i32* %skippedFiles, align 4, !tbaa !6
  %inc = add nsw i32 %48, 1
  store i32 %inc, i32* %skippedFiles, align 4, !tbaa !6
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end30:                                         ; preds = %lor.lhs.false
  %49 = load i8*, i8** %outFileName, align 4, !tbaa !2
  %50 = load i8**, i8*** %inFileNamesTable.addr, align 4, !tbaa !2
  %51 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx31 = getelementptr inbounds i8*, i8** %50, i32 %51
  %52 = load i8*, i8** %arrayidx31, align 4, !tbaa !2
  %53 = load i32, i32* %ifnSize, align 4, !tbaa !24
  %54 = load i32, i32* %suffixSize, align 4, !tbaa !24
  %sub32 = sub i32 %53, %54
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %49, i8* align 1 %52, i32 %sub32, i1 false)
  %55 = load i8*, i8** %outFileName, align 4, !tbaa !2
  %56 = load i32, i32* %ifnSize, align 4, !tbaa !24
  %57 = load i32, i32* %suffixSize, align 4, !tbaa !24
  %sub33 = sub i32 %56, %57
  %arrayidx34 = getelementptr inbounds i8, i8* %55, i32 %sub33
  store i8 0, i8* %arrayidx34, align 1, !tbaa !25
  %58 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %59 = load i8**, i8*** %inFileNamesTable.addr, align 4, !tbaa !2
  %60 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx35 = getelementptr inbounds i8*, i8** %59, i32 %60
  %61 = load i8*, i8** %arrayidx35, align 4, !tbaa !2
  %62 = load i8*, i8** %outFileName, align 4, !tbaa !2
  %call36 = call i32 @LZ4IO_decompressDstFile(%struct.LZ4IO_prefs_s* %58, %struct.dRess_t* byval(%struct.dRess_t) align 4 %ress, i8* %61, i8* %62)
  %63 = load i32, i32* %missingFiles, align 4, !tbaa !6
  %add37 = add nsw i32 %63, %call36
  store i32 %add37, i32* %missingFiles, align 4, !tbaa !6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end30, %if.end29, %if.then18, %if.then8
  %64 = bitcast i8** %suffixPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #5
  %65 = bitcast i32* %ifnSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup41 [
    i32 0, label %cleanup.cont
    i32 4, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %66 = load i32, i32* %i, align 4, !tbaa !6
  %inc39 = add nsw i32 %66, 1
  store i32 %inc39, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @LZ4IO_freeDResources(%struct.dRess_t* byval(%struct.dRess_t) align 4 %ress)
  %67 = load i8*, i8** %outFileName, align 4, !tbaa !2
  call void @free(i8* %67)
  %68 = load i32, i32* %missingFiles, align 4, !tbaa !6
  %69 = load i32, i32* %skippedFiles, align 4, !tbaa !6
  %add40 = add nsw i32 %68, %69
  store i32 %add40, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup41

cleanup41:                                        ; preds = %for.end, %cleanup, %if.then
  %70 = bitcast %struct.dRess_t* %ress to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %70) #5
  %71 = bitcast i32* %suffixSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #5
  %72 = bitcast i32* %ofnSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #5
  %73 = bitcast i8** %outFileName to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #5
  %74 = bitcast i32* %missingFiles to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #5
  %75 = bitcast i32* %skippedFiles to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #5
  %76 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #5
  %77 = load i32, i32* %retval, align 4
  ret i32 %77
}

; Function Attrs: nounwind
define internal i32 @LZ4IO_decompressSrcFile(%struct.LZ4IO_prefs_s* %prefs, %struct.dRess_t* byval(%struct.dRess_t) align 4 %ress, i8* %input_filename, i8* %output_filename) #0 {
entry:
  %retval = alloca i32, align 4
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %input_filename.addr = alloca i8*, align 4
  %output_filename.addr = alloca i8*, align 4
  %foutput = alloca %struct._IO_FILE*, align 4
  %filesize = alloca i64, align 8
  %finput = alloca %struct._IO_FILE*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %decodedSize = alloca i64, align 8
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i8* %input_filename, i8** %input_filename.addr, align 4, !tbaa !2
  store i8* %output_filename, i8** %output_filename.addr, align 4, !tbaa !2
  %0 = bitcast %struct._IO_FILE** %foutput to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %dstFile = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 4
  %1 = load %struct._IO_FILE*, %struct._IO_FILE** %dstFile, align 4, !tbaa !53
  store %struct._IO_FILE* %1, %struct._IO_FILE** %foutput, align 4, !tbaa !2
  %2 = bitcast i64* %filesize to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %2) #5
  store i64 0, i64* %filesize, align 8, !tbaa !26
  %3 = bitcast %struct._IO_FILE** %finput to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load i8*, i8** %input_filename.addr, align 4, !tbaa !2
  %call = call %struct._IO_FILE* @LZ4IO_openSrcFile(i8* %4)
  store %struct._IO_FILE* %call, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %5 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %cmp = icmp eq %struct._IO_FILE* %5, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup34

if.end:                                           ; preds = %entry
  br label %for.cond

for.cond:                                         ; preds = %cleanup.cont, %if.end
  %6 = bitcast i64* %decodedSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #5
  %7 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %8 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** %foutput, align 4, !tbaa !2
  %call1 = call i64 @selectDecoder(%struct.LZ4IO_prefs_s* %7, %struct.dRess_t* byval(%struct.dRess_t) align 4 %ress, %struct._IO_FILE* %8, %struct._IO_FILE* %9)
  store i64 %call1, i64* %decodedSize, align 8, !tbaa !26
  %10 = load i64, i64* %decodedSize, align 8, !tbaa !26
  %cmp2 = icmp eq i64 %10, -1
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end4:                                          ; preds = %for.cond
  %11 = load i64, i64* %decodedSize, align 8, !tbaa !26
  %12 = load i64, i64* %filesize, align 8, !tbaa !26
  %add = add i64 %12, %11
  store i64 %add, i64* %filesize, align 8, !tbaa !26
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end4, %if.then3
  %13 = bitcast i64* %decodedSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %13) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 2, label %for.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.cond

for.end:                                          ; preds = %cleanup
  %14 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %call5 = call i32 @fclose(%struct._IO_FILE* %14)
  %15 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %removeSrcFile = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %15, i32 0, i32 13
  %16 = load i32, i32* %removeSrcFile, align 4, !tbaa !23
  %tobool = icmp ne i32 %16, 0
  br i1 %tobool, label %if.then6, label %if.end25

if.then6:                                         ; preds = %for.end
  %17 = load i8*, i8** %input_filename.addr, align 4, !tbaa !2
  %call7 = call i32 @remove(i8* %17)
  %tobool8 = icmp ne i32 %call7, 0
  br i1 %tobool8, label %if.then9, label %if.end24

if.then9:                                         ; preds = %if.then6
  %18 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp10 = icmp sge i32 %18, 1
  br i1 %cmp10, label %if.then11, label %if.end13

if.then11:                                        ; preds = %if.then9
  %19 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call12 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %19, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 45)
  br label %if.end13

if.end13:                                         ; preds = %if.then11, %if.then9
  %20 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp14 = icmp sge i32 %20, 1
  br i1 %cmp14, label %if.then15, label %if.end19

if.then15:                                        ; preds = %if.end13
  %21 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %22 = load i8*, i8** %input_filename.addr, align 4, !tbaa !2
  %call16 = call i32* @__errno_location()
  %23 = load i32, i32* %call16, align 4, !tbaa !6
  %call17 = call i8* @strerror(i32 %23)
  %call18 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %21, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.63, i32 0, i32 0), i8* %22, i8* %call17)
  br label %if.end19

if.end19:                                         ; preds = %if.then15, %if.end13
  %24 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp20 = icmp sge i32 %24, 1
  br i1 %cmp20, label %if.then21, label %if.end23

if.then21:                                        ; preds = %if.end19
  %25 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call22 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %25, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end23

if.end23:                                         ; preds = %if.then21, %if.end19
  call void @exit(i32 45) #6
  unreachable

if.end24:                                         ; preds = %if.then6
  br label %if.end25

if.end25:                                         ; preds = %if.end24, %for.end
  %26 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp26 = icmp sge i32 %26, 2
  br i1 %cmp26, label %if.then27, label %if.end29

if.then27:                                        ; preds = %if.end25
  %27 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call28 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %27, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.12, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @.str.13, i32 0, i32 0))
  br label %if.end29

if.end29:                                         ; preds = %if.then27, %if.end25
  %28 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp30 = icmp sge i32 %28, 2
  br i1 %cmp30, label %if.then31, label %if.end33

if.then31:                                        ; preds = %if.end29
  %29 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %30 = load i8*, i8** %input_filename.addr, align 4, !tbaa !2
  %31 = load i64, i64* %filesize, align 8, !tbaa !26
  %call32 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %29, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.66, i32 0, i32 0), i8* %30, i64 %31)
  br label %if.end33

if.end33:                                         ; preds = %if.then31, %if.end29
  %32 = load i8*, i8** %output_filename.addr, align 4, !tbaa !2
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup34

cleanup34:                                        ; preds = %if.end33, %if.then
  %33 = bitcast %struct._IO_FILE** %finput to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #5
  %34 = bitcast i64* %filesize to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %34) #5
  %35 = bitcast %struct._IO_FILE** %foutput to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #5
  %36 = load i32, i32* %retval, align 4
  ret i32 %36

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden i8* @LZ4IO_blockTypeID(i32 %sizeID, i32 %blockMode, i8* %buffer) #0 {
entry:
  %sizeID.addr = alloca i32, align 4
  %blockMode.addr = alloca i32, align 4
  %buffer.addr = alloca i8*, align 4
  store i32 %sizeID, i32* %sizeID.addr, align 4, !tbaa !6
  store i32 %blockMode, i32* %blockMode.addr, align 4, !tbaa !6
  store i8* %buffer, i8** %buffer.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %0, i32 0
  store i8 66, i8* %arrayidx, align 1, !tbaa !25
  %1 = load i32, i32* %sizeID.addr, align 4, !tbaa !6
  %cmp = icmp sge i32 %1, 4
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  call void @__assert_fail(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.19, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.6, i32 0, i32 0), i32 1440, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @__func__.LZ4IO_blockTypeID, i32 0, i32 0)) #6
  unreachable

2:                                                ; No predecessors!
  br label %lor.end

lor.end:                                          ; preds = %2, %entry
  %3 = phi i1 [ true, %entry ], [ false, %2 ]
  %lor.ext = zext i1 %3 to i32
  %4 = load i32, i32* %sizeID.addr, align 4, !tbaa !6
  %cmp1 = icmp sle i32 %4, 7
  br i1 %cmp1, label %lor.end3, label %lor.rhs2

lor.rhs2:                                         ; preds = %lor.end
  call void @__assert_fail(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.20, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.6, i32 0, i32 0), i32 1440, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @__func__.LZ4IO_blockTypeID, i32 0, i32 0)) #6
  unreachable

5:                                                ; No predecessors!
  br label %lor.end3

lor.end3:                                         ; preds = %5, %lor.end
  %6 = phi i1 [ true, %lor.end ], [ false, %5 ]
  %lor.ext4 = zext i1 %6 to i32
  %7 = load i32, i32* %sizeID.addr, align 4, !tbaa !6
  %add = add nsw i32 %7, 48
  %conv = trunc i32 %add to i8
  %8 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %8, i32 1
  store i8 %conv, i8* %arrayidx5, align 1, !tbaa !25
  %9 = load i32, i32* %blockMode.addr, align 4, !tbaa !6
  %cmp6 = icmp eq i32 %9, 1
  %10 = zext i1 %cmp6 to i64
  %cond = select i1 %cmp6, i32 73, i32 68
  %conv8 = trunc i32 %cond to i8
  %11 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i8, i8* %11, i32 2
  store i8 %conv8, i8* %arrayidx9, align 1, !tbaa !25
  %12 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %12, i32 3
  store i8 0, i8* %arrayidx10, align 1, !tbaa !25
  %13 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  ret i8* %13
}

; Function Attrs: nounwind
define hidden i32 @LZ4IO_displayCompressedFilesInfo(i8** %inFileNames, i32 %ifnIdx) #0 {
entry:
  %retval = alloca i32, align 4
  %inFileNames.addr = alloca i8**, align 4
  %ifnIdx.addr = alloca i32, align 4
  %result = alloca i32, align 4
  %idx = alloca i32, align 4
  %cfinfo = alloca %struct.LZ4IO_cFileInfo_t, align 8
  %cleanup.dest.slot = alloca i32, align 4
  %op_result = alloca i32, align 4
  %buffers = alloca [3 x [10 x i8]], align 16
  %ratio = alloca double, align 8
  store i8** %inFileNames, i8*** %inFileNames.addr, align 4, !tbaa !2
  store i32 %ifnIdx, i32* %ifnIdx.addr, align 4, !tbaa !24
  %0 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %result, align 4, !tbaa !6
  %1 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 0, i32* %idx, align 4, !tbaa !24
  %2 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, 3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct._IO_FILE*, %struct._IO_FILE** @stdout, align 4, !tbaa !2
  %call = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %3, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.21, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.22, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.23, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.24, i32 0, i32 0), i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.25, i32 0, i32 0), i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.26, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.27, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.28, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %4 = load i32, i32* %idx, align 4, !tbaa !24
  %5 = load i32, i32* %ifnIdx.addr, align 4, !tbaa !24
  %cmp1 = icmp ult i32 %4, %5
  br i1 %cmp1, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %6 = bitcast %struct.LZ4IO_cFileInfo_t* %cfinfo to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %6) #5
  %7 = bitcast %struct.LZ4IO_cFileInfo_t* %cfinfo to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %7, i8 0, i32 72, i1 false)
  %8 = bitcast i8* %7 to %struct.LZ4IO_cFileInfo_t*
  %9 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %8, i32 0, i32 4
  store i16 1, i16* %9, align 8
  %10 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %8, i32 0, i32 5
  store i16 1, i16* %10, align 2
  %11 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %8, i32 0, i32 6
  store i16 1, i16* %11, align 4
  %12 = load i8**, i8*** %inFileNames.addr, align 4, !tbaa !2
  %13 = load i32, i32* %idx, align 4, !tbaa !24
  %arrayidx = getelementptr inbounds i8*, i8** %12, i32 %13
  %14 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %call2 = call i8* @LZ4IO_baseName(i8* %14)
  %fileName = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %cfinfo, i32 0, i32 0
  store i8* %call2, i8** %fileName, align 8, !tbaa !56
  %15 = load i8**, i8*** %inFileNames.addr, align 4, !tbaa !2
  %16 = load i32, i32* %idx, align 4, !tbaa !24
  %arrayidx3 = getelementptr inbounds i8*, i8** %15, i32 %16
  %17 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  %call4 = call i32 @UTIL_isRegFile(i8* %17)
  %tobool = icmp ne i32 %call4, 0
  br i1 %tobool, label %if.end11, label %if.then5

if.then5:                                         ; preds = %for.body
  %18 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp6 = icmp sge i32 %18, 1
  br i1 %cmp6, label %if.then7, label %if.end10

if.then7:                                         ; preds = %if.then5
  %19 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %20 = load i8**, i8*** %inFileNames.addr, align 4, !tbaa !2
  %21 = load i32, i32* %idx, align 4, !tbaa !24
  %arrayidx8 = getelementptr inbounds i8*, i8** %20, i32 %21
  %22 = load i8*, i8** %arrayidx8, align 4, !tbaa !2
  %call9 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %19, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.29, i32 0, i32 0), i8* %22)
  br label %if.end10

if.end10:                                         ; preds = %if.then7, %if.then5
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup91

if.end11:                                         ; preds = %for.body
  %23 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp12 = icmp sge i32 %23, 3
  br i1 %cmp12, label %if.then13, label %if.end17

if.then13:                                        ; preds = %if.end11
  %24 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %fileName14 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %cfinfo, i32 0, i32 0
  %25 = load i8*, i8** %fileName14, align 8, !tbaa !56
  %26 = load i32, i32* %idx, align 4, !tbaa !24
  %conv = zext i32 %26 to i64
  %add = add i64 %conv, 1
  %27 = load i32, i32* %ifnIdx.addr, align 4, !tbaa !24
  %conv15 = zext i32 %27 to i64
  %call16 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %24, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.30, i32 0, i32 0), i8* %25, i64 %add, i64 %conv15)
  br label %if.end17

if.end17:                                         ; preds = %if.then13, %if.end11
  %28 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp18 = icmp sge i32 %28, 3
  br i1 %cmp18, label %if.then20, label %if.end22

if.then20:                                        ; preds = %if.end17
  %29 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call21 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %29, i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.31, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.32, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.23, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.24, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.33, i32 0, i32 0), i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.25, i32 0, i32 0), i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.26, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.27, i32 0, i32 0))
  br label %if.end22

if.end22:                                         ; preds = %if.then20, %if.end17
  %30 = bitcast i32* %op_result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #5
  %31 = load i8**, i8*** %inFileNames.addr, align 4, !tbaa !2
  %32 = load i32, i32* %idx, align 4, !tbaa !24
  %arrayidx23 = getelementptr inbounds i8*, i8** %31, i32 %32
  %33 = load i8*, i8** %arrayidx23, align 4, !tbaa !2
  %call24 = call i32 @LZ4IO_getCompressedFileInfo(%struct.LZ4IO_cFileInfo_t* %cfinfo, i8* %33)
  store i32 %call24, i32* %op_result, align 4, !tbaa !25
  %34 = load i32, i32* %op_result, align 4, !tbaa !25
  %cmp25 = icmp ne i32 %34, 0
  br i1 %cmp25, label %if.then27, label %if.end36

if.then27:                                        ; preds = %if.end22
  %35 = load i32, i32* %op_result, align 4, !tbaa !25
  %cmp28 = icmp eq i32 %35, 1
  br i1 %cmp28, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.then27
  call void @__assert_fail(i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.34, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.6, i32 0, i32 0), i32 1628, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @__func__.LZ4IO_displayCompressedFilesInfo, i32 0, i32 0)) #6
  unreachable

36:                                               ; No predecessors!
  br label %lor.end

lor.end:                                          ; preds = %36, %if.then27
  %37 = phi i1 [ true, %if.then27 ], [ false, %36 ]
  %lor.ext = zext i1 %37 to i32
  %38 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp30 = icmp sge i32 %38, 1
  br i1 %cmp30, label %if.then32, label %if.end35

if.then32:                                        ; preds = %lor.end
  %39 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %40 = load i8**, i8*** %inFileNames.addr, align 4, !tbaa !2
  %41 = load i32, i32* %idx, align 4, !tbaa !24
  %arrayidx33 = getelementptr inbounds i8*, i8** %40, i32 %41
  %42 = load i8*, i8** %arrayidx33, align 4, !tbaa !2
  %call34 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %39, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.35, i32 0, i32 0), i8* %42)
  br label %if.end35

if.end35:                                         ; preds = %if.then32, %lor.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end36:                                         ; preds = %if.end22
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end36, %if.end35
  %43 = bitcast i32* %op_result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup91 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  %44 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp37 = icmp sge i32 %44, 3
  br i1 %cmp37, label %if.then39, label %if.end41

if.then39:                                        ; preds = %cleanup.cont
  %45 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call40 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %45, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.36, i32 0, i32 0))
  br label %if.end41

if.end41:                                         ; preds = %if.then39, %cleanup.cont
  %46 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp42 = icmp slt i32 %46, 3
  br i1 %cmp42, label %if.then44, label %if.end90

if.then44:                                        ; preds = %if.end41
  %47 = bitcast [3 x [10 x i8]]* %buffers to i8*
  call void @llvm.lifetime.start.p0i8(i64 30, i8* %47) #5
  %48 = load %struct._IO_FILE*, %struct._IO_FILE** @stdout, align 4, !tbaa !2
  %frameCount = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %cfinfo, i32 0, i32 2
  %49 = load i64, i64* %frameCount, align 8, !tbaa !60
  %eqFrameTypes = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %cfinfo, i32 0, i32 4
  %50 = load i16, i16* %eqFrameTypes, align 8, !tbaa !61
  %conv45 = zext i16 %50 to i32
  %tobool46 = icmp ne i32 %conv45, 0
  br i1 %tobool46, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then44
  %frameSummary = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %cfinfo, i32 0, i32 3
  %frameType = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameSummary, i32 0, i32 1
  %51 = load i32, i32* %frameType, align 8, !tbaa !62
  %arrayidx47 = getelementptr inbounds [3 x i8*], [3 x i8*]* @LZ4IO_frameTypeNames, i32 0, i32 %51
  %52 = load i8*, i8** %arrayidx47, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %if.then44
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %52, %cond.true ], [ getelementptr inbounds ([2 x i8], [2 x i8]* @.str.38, i32 0, i32 0), %cond.false ]
  %eqBlockTypes = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %cfinfo, i32 0, i32 5
  %53 = load i16, i16* %eqBlockTypes, align 2, !tbaa !63
  %conv48 = zext i16 %53 to i32
  %tobool49 = icmp ne i32 %conv48, 0
  br i1 %tobool49, label %cond.true50, label %cond.false56

cond.true50:                                      ; preds = %cond.end
  %frameSummary51 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %cfinfo, i32 0, i32 3
  %lz4FrameInfo = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameSummary51, i32 0, i32 0
  %blockSizeID = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %lz4FrameInfo, i32 0, i32 0
  %54 = load i32, i32* %blockSizeID, align 8, !tbaa !64
  %frameSummary52 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %cfinfo, i32 0, i32 3
  %lz4FrameInfo53 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameSummary52, i32 0, i32 0
  %blockMode = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %lz4FrameInfo53, i32 0, i32 1
  %55 = load i32, i32* %blockMode, align 4, !tbaa !65
  %arrayidx54 = getelementptr inbounds [3 x [10 x i8]], [3 x [10 x i8]]* %buffers, i32 0, i32 0
  %arraydecay = getelementptr inbounds [10 x i8], [10 x i8]* %arrayidx54, i32 0, i32 0
  %call55 = call i8* @LZ4IO_blockTypeID(i32 %54, i32 %55, i8* %arraydecay)
  br label %cond.end57

cond.false56:                                     ; preds = %cond.end
  br label %cond.end57

cond.end57:                                       ; preds = %cond.false56, %cond.true50
  %cond58 = phi i8* [ %call55, %cond.true50 ], [ getelementptr inbounds ([2 x i8], [2 x i8]* @.str.38, i32 0, i32 0), %cond.false56 ]
  %fileSize = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %cfinfo, i32 0, i32 1
  %56 = load i64, i64* %fileSize, align 8, !tbaa !66
  %conv59 = uitofp i64 %56 to fp128
  %arrayidx60 = getelementptr inbounds [3 x [10 x i8]], [3 x [10 x i8]]* %buffers, i32 0, i32 1
  %arraydecay61 = getelementptr inbounds [10 x i8], [10 x i8]* %arrayidx60, i32 0, i32 0
  %call62 = call i8* @LZ4IO_toHuman(fp128 %conv59, i8* %arraydecay61)
  %allContentSize = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %cfinfo, i32 0, i32 6
  %57 = load i16, i16* %allContentSize, align 4, !tbaa !67
  %conv63 = zext i16 %57 to i32
  %tobool64 = icmp ne i32 %conv63, 0
  br i1 %tobool64, label %cond.true65, label %cond.false72

cond.true65:                                      ; preds = %cond.end57
  %frameSummary66 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %cfinfo, i32 0, i32 3
  %lz4FrameInfo67 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameSummary66, i32 0, i32 0
  %contentSize = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %lz4FrameInfo67, i32 0, i32 4
  %58 = load i64, i64* %contentSize, align 8, !tbaa !68
  %conv68 = uitofp i64 %58 to fp128
  %arrayidx69 = getelementptr inbounds [3 x [10 x i8]], [3 x [10 x i8]]* %buffers, i32 0, i32 2
  %arraydecay70 = getelementptr inbounds [10 x i8], [10 x i8]* %arrayidx69, i32 0, i32 0
  %call71 = call i8* @LZ4IO_toHuman(fp128 %conv68, i8* %arraydecay70)
  br label %cond.end73

cond.false72:                                     ; preds = %cond.end57
  br label %cond.end73

cond.end73:                                       ; preds = %cond.false72, %cond.true65
  %cond74 = phi i8* [ %call71, %cond.true65 ], [ getelementptr inbounds ([2 x i8], [2 x i8]* @.str.38, i32 0, i32 0), %cond.false72 ]
  %call75 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %48, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.37, i32 0, i32 0), i64 %49, i8* %cond, i8* %cond58, i8* %call62, i8* %cond74)
  %allContentSize76 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %cfinfo, i32 0, i32 6
  %59 = load i16, i16* %allContentSize76, align 4, !tbaa !67
  %tobool77 = icmp ne i16 %59, 0
  br i1 %tobool77, label %if.then78, label %if.else

if.then78:                                        ; preds = %cond.end73
  %60 = bitcast double* %ratio to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %60) #5
  %fileSize79 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %cfinfo, i32 0, i32 1
  %61 = load i64, i64* %fileSize79, align 8, !tbaa !66
  %conv80 = uitofp i64 %61 to double
  %frameSummary81 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %cfinfo, i32 0, i32 3
  %lz4FrameInfo82 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameSummary81, i32 0, i32 0
  %contentSize83 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %lz4FrameInfo82, i32 0, i32 4
  %62 = load i64, i64* %contentSize83, align 8, !tbaa !68
  %conv84 = uitofp i64 %62 to double
  %div = fdiv double %conv80, %conv84
  %mul = fmul double %div, 1.000000e+02
  store double %mul, double* %ratio, align 8, !tbaa !28
  %63 = load %struct._IO_FILE*, %struct._IO_FILE** @stdout, align 4, !tbaa !2
  %64 = load double, double* %ratio, align 8, !tbaa !28
  %fileName85 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %cfinfo, i32 0, i32 0
  %65 = load i8*, i8** %fileName85, align 8, !tbaa !56
  %call86 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %63, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.39, i32 0, i32 0), double %64, i8* %65)
  %66 = bitcast double* %ratio to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %66) #5
  br label %if.end89

if.else:                                          ; preds = %cond.end73
  %67 = load %struct._IO_FILE*, %struct._IO_FILE** @stdout, align 4, !tbaa !2
  %fileName87 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %cfinfo, i32 0, i32 0
  %68 = load i8*, i8** %fileName87, align 8, !tbaa !56
  %call88 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %67, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.40, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.38, i32 0, i32 0), i8* %68)
  br label %if.end89

if.end89:                                         ; preds = %if.else, %if.then78
  %69 = bitcast [3 x [10 x i8]]* %buffers to i8*
  call void @llvm.lifetime.end.p0i8(i64 30, i8* %69) #5
  br label %if.end90

if.end90:                                         ; preds = %if.end89, %if.end41
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup91

cleanup91:                                        ; preds = %if.end90, %cleanup, %if.end10
  %70 = bitcast %struct.LZ4IO_cFileInfo_t* %cfinfo to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %70) #5
  %cleanup.dest92 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest92, label %cleanup94 [
    i32 0, label %cleanup.cont93
  ]

cleanup.cont93:                                   ; preds = %cleanup91
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont93
  %71 = load i32, i32* %idx, align 4, !tbaa !24
  %inc = add i32 %71, 1
  store i32 %inc, i32* %idx, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %72 = load i32, i32* %result, align 4, !tbaa !6
  store i32 %72, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup94

cleanup94:                                        ; preds = %for.end, %cleanup91
  %73 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #5
  %74 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #5
  %75 = load i32, i32* %retval, align 4
  ret i32 %75
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: nounwind
define internal i8* @LZ4IO_baseName(i8* %input_filename) #0 {
entry:
  %retval = alloca i8*, align 4
  %input_filename.addr = alloca i8*, align 4
  %b = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %input_filename, i8** %input_filename.addr, align 4, !tbaa !2
  %0 = bitcast i8** %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %input_filename.addr, align 4, !tbaa !2
  %call = call i8* @strrchr(i8* %1, i32 47)
  store i8* %call, i8** %b, align 4, !tbaa !2
  %2 = load i8*, i8** %b, align 4, !tbaa !2
  %tobool = icmp ne i8* %2, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %3 = load i8*, i8** %input_filename.addr, align 4, !tbaa !2
  %call1 = call i8* @strrchr(i8* %3, i32 92)
  store i8* %call1, i8** %b, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load i8*, i8** %b, align 4, !tbaa !2
  %tobool2 = icmp ne i8* %4, null
  br i1 %tobool2, label %if.end4, label %if.then3

if.then3:                                         ; preds = %if.end
  %5 = load i8*, i8** %input_filename.addr, align 4, !tbaa !2
  store i8* %5, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end4:                                          ; preds = %if.end
  %6 = load i8*, i8** %b, align 4, !tbaa !2
  %tobool5 = icmp ne i8* %6, null
  br i1 %tobool5, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end4
  %7 = load i8*, i8** %b, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %7, i32 1
  br label %cond.end

cond.false:                                       ; preds = %if.end4
  %8 = load i8*, i8** %b, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %add.ptr, %cond.true ], [ %8, %cond.false ]
  store i8* %cond, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %cond.end, %if.then3
  %9 = bitcast i8** %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #5
  %10 = load i8*, i8** %retval, align 4
  ret i8* %10
}

; Function Attrs: nounwind
define internal i32 @UTIL_isRegFile(i8* %infilename) #0 {
entry:
  %infilename.addr = alloca i8*, align 4
  %statbuf = alloca %struct.stat, align 8
  store i8* %infilename, i8** %infilename.addr, align 4, !tbaa !2
  %0 = bitcast %struct.stat* %statbuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 88, i8* %0) #5
  %1 = load i8*, i8** %infilename.addr, align 4, !tbaa !2
  %call = call i32 @UTIL_getFileStat(i8* %1, %struct.stat* %statbuf)
  %2 = bitcast %struct.stat* %statbuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 88, i8* %2) #5
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @LZ4IO_getCompressedFileInfo(%struct.LZ4IO_cFileInfo_t* %cfinfo, i8* %input_filename) #0 {
entry:
  %cfinfo.addr = alloca %struct.LZ4IO_cFileInfo_t*, align 4
  %input_filename.addr = alloca i8*, align 4
  %result = alloca i32, align 4
  %buffer = alloca [19 x i8], align 16
  %finput = alloca %struct._IO_FILE*, align 4
  %frameInfo = alloca %struct.LZ4IO_frameInfo_t, align 8
  %magicNumber = alloca i32, align 4
  %nbReadBytes = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %readBytes = alloca i32, align 4
  %hSize = alloca i32, align 4
  %readBytes53 = alloca i32, align 4
  %dctx = alloca %struct.LZ4F_dctx_s*, align 4
  %isError = alloca i32, align 4
  %totalBlocksSize = alloca i64, align 8
  %bTypeBuffer = alloca [5 x i8], align 1
  %ratio = alloca double, align 8
  %totalBlocksSize171 = alloca i64, align 8
  %size = alloca i32, align 4
  %errorNb = alloca i32, align 4
  %position = alloca i32, align 4
  store %struct.LZ4IO_cFileInfo_t* %cfinfo, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  store i8* %input_filename, i8** %input_filename.addr, align 4, !tbaa !2
  %0 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 1, i32* %result, align 4, !tbaa !25
  %1 = bitcast [19 x i8]* %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 19, i8* %1) #5
  %2 = bitcast %struct._IO_FILE** %finput to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i8*, i8** %input_filename.addr, align 4, !tbaa !2
  %call = call %struct._IO_FILE* @LZ4IO_openSrcFile(i8* %3)
  store %struct._IO_FILE* %call, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %4 = load i8*, i8** %input_filename.addr, align 4, !tbaa !2
  %call1 = call i64 @UTIL_getFileSize(i8* %4)
  %5 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %fileSize = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %5, i32 0, i32 1
  store i64 %call1, i64* %fileSize, align 8, !tbaa !66
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %entry
  %6 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %call2 = call i32 @feof(%struct._IO_FILE* %6)
  %tobool = icmp ne i32 %call2, 0
  %lnot = xor i1 %tobool, true
  br i1 %lnot, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = bitcast %struct.LZ4IO_frameInfo_t* %frameInfo to i8*
  call void @llvm.lifetime.start.p0i8(i64 40, i8* %7) #5
  %8 = bitcast %struct.LZ4IO_frameInfo_t* %frameInfo to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %8, i8 0, i32 40, i1 false)
  %9 = bitcast i32* %magicNumber to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = bitcast i32* %nbReadBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %arraydecay = getelementptr inbounds [19 x i8], [19 x i8]* %buffer, i32 0, i32 0
  %11 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %call3 = call i32 @fread(i8* %arraydecay, i32 1, i32 4, %struct._IO_FILE* %11)
  store i32 %call3, i32* %nbReadBytes, align 4, !tbaa !24
  %12 = load i32, i32* %nbReadBytes, align 4, !tbaa !24
  %cmp = icmp eq i32 %12, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %while.body
  store i32 1, i32* %result, align 4, !tbaa !25
  %13 = load i32, i32* %nbReadBytes, align 4, !tbaa !24
  %cmp4 = icmp ne i32 %13, 4
  br i1 %cmp4, label %if.then5, label %if.end18

if.then5:                                         ; preds = %if.end
  %14 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp6 = icmp sge i32 %14, 1
  br i1 %cmp6, label %if.then7, label %if.end9

if.then7:                                         ; preds = %if.then5
  %15 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call8 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %15, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 40)
  br label %if.end9

if.end9:                                          ; preds = %if.then7, %if.then5
  %16 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp10 = icmp sge i32 %16, 1
  br i1 %cmp10, label %if.then11, label %if.end13

if.then11:                                        ; preds = %if.end9
  %17 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call12 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %17, i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.67, i32 0, i32 0))
  br label %if.end13

if.end13:                                         ; preds = %if.then11, %if.end9
  %18 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp14 = icmp sge i32 %18, 1
  br i1 %cmp14, label %if.then15, label %if.end17

if.then15:                                        ; preds = %if.end13
  %19 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call16 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %19, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end17

if.end17:                                         ; preds = %if.then15, %if.end13
  call void @exit(i32 40) #6
  unreachable

if.end18:                                         ; preds = %if.end
  %arraydecay19 = getelementptr inbounds [19 x i8], [19 x i8]* %buffer, i32 0, i32 0
  %call20 = call i32 @LZ4IO_readLE32(i8* %arraydecay19)
  store i32 %call20, i32* %magicNumber, align 4, !tbaa !6
  %20 = load i32, i32* %magicNumber, align 4, !tbaa !6
  %call21 = call i32 @LZ4IO_isSkippableMagicNumber(i32 %20)
  %tobool22 = icmp ne i32 %call21, 0
  br i1 %tobool22, label %if.then23, label %if.end24

if.then23:                                        ; preds = %if.end18
  store i32 407710288, i32* %magicNumber, align 4, !tbaa !6
  br label %if.end24

if.end24:                                         ; preds = %if.then23, %if.end18
  %21 = load i32, i32* %magicNumber, align 4, !tbaa !6
  switch i32 %21, label %sw.default [
    i32 407708164, label %sw.bb
    i32 407642370, label %sw.bb156
    i32 407710288, label %sw.bb186
  ]

sw.bb:                                            ; preds = %if.end24
  %22 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %frameSummary = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %22, i32 0, i32 3
  %frameType = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameSummary, i32 0, i32 1
  %23 = load i32, i32* %frameType, align 8, !tbaa !62
  %cmp25 = icmp ne i32 %23, 0
  br i1 %cmp25, label %if.then26, label %if.end27

if.then26:                                        ; preds = %sw.bb
  %24 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %eqFrameTypes = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %24, i32 0, i32 4
  store i16 0, i16* %eqFrameTypes, align 8, !tbaa !61
  br label %if.end27

if.end27:                                         ; preds = %if.then26, %sw.bb
  %25 = bitcast i32* %readBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #5
  %arraydecay28 = getelementptr inbounds [19 x i8], [19 x i8]* %buffer, i32 0, i32 0
  %add.ptr = getelementptr inbounds i8, i8* %arraydecay28, i32 4
  %26 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %call29 = call i32 @fread(i8* %add.ptr, i32 1, i32 3, %struct._IO_FILE* %26)
  store i32 %call29, i32* %readBytes, align 4, !tbaa !24
  %27 = load i32, i32* %readBytes, align 4, !tbaa !24
  %tobool30 = icmp ne i32 %27, 0
  br i1 %tobool30, label %lor.lhs.false, label %if.then33

lor.lhs.false:                                    ; preds = %if.end27
  %28 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %call31 = call i32 @ferror(%struct._IO_FILE* %28)
  %tobool32 = icmp ne i32 %call31, 0
  br i1 %tobool32, label %if.then33, label %if.end46

if.then33:                                        ; preds = %lor.lhs.false, %if.end27
  %29 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp34 = icmp sge i32 %29, 1
  br i1 %cmp34, label %if.then35, label %if.end37

if.then35:                                        ; preds = %if.then33
  %30 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call36 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %30, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 71)
  br label %if.end37

if.end37:                                         ; preds = %if.then35, %if.then33
  %31 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp38 = icmp sge i32 %31, 1
  br i1 %cmp38, label %if.then39, label %if.end41

if.then39:                                        ; preds = %if.end37
  %32 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %33 = load i8*, i8** %input_filename.addr, align 4, !tbaa !2
  %call40 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %32, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.93, i32 0, i32 0), i8* %33)
  br label %if.end41

if.end41:                                         ; preds = %if.then39, %if.end37
  %34 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp42 = icmp sge i32 %34, 1
  br i1 %cmp42, label %if.then43, label %if.end45

if.then43:                                        ; preds = %if.end41
  %35 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call44 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %35, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end45

if.end45:                                         ; preds = %if.then43, %if.end41
  call void @exit(i32 71) #6
  unreachable

if.end46:                                         ; preds = %lor.lhs.false
  %36 = bitcast i32* %readBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #5
  %37 = bitcast i32* %hSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #5
  %38 = bitcast [19 x i8]* %buffer to i8*
  %call47 = call i32 @LZ4F_headerSize(i8* %38, i32 7)
  store i32 %call47, i32* %hSize, align 4, !tbaa !24
  %39 = load i32, i32* %hSize, align 4, !tbaa !24
  %call48 = call i32 @LZ4F_isError(i32 %39)
  %tobool49 = icmp ne i32 %call48, 0
  br i1 %tobool49, label %if.end155, label %if.then50

if.then50:                                        ; preds = %if.end46
  %40 = load i32, i32* %hSize, align 4, !tbaa !24
  %cmp51 = icmp ugt i32 %40, 11
  br i1 %cmp51, label %if.then52, label %if.end75

if.then52:                                        ; preds = %if.then50
  %41 = bitcast i32* %readBytes53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #5
  %arraydecay54 = getelementptr inbounds [19 x i8], [19 x i8]* %buffer, i32 0, i32 0
  %add.ptr55 = getelementptr inbounds i8, i8* %arraydecay54, i32 7
  %42 = load i32, i32* %hSize, align 4, !tbaa !24
  %sub = sub i32 %42, 7
  %43 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %call56 = call i32 @fread(i8* %add.ptr55, i32 1, i32 %sub, %struct._IO_FILE* %43)
  store i32 %call56, i32* %readBytes53, align 4, !tbaa !24
  %44 = load i32, i32* %readBytes53, align 4, !tbaa !24
  %tobool57 = icmp ne i32 %44, 0
  br i1 %tobool57, label %lor.lhs.false58, label %if.then61

lor.lhs.false58:                                  ; preds = %if.then52
  %45 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %call59 = call i32 @ferror(%struct._IO_FILE* %45)
  %tobool60 = icmp ne i32 %call59, 0
  br i1 %tobool60, label %if.then61, label %if.end74

if.then61:                                        ; preds = %lor.lhs.false58, %if.then52
  %46 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp62 = icmp sge i32 %46, 1
  br i1 %cmp62, label %if.then63, label %if.end65

if.then63:                                        ; preds = %if.then61
  %47 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call64 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %47, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 72)
  br label %if.end65

if.end65:                                         ; preds = %if.then63, %if.then61
  %48 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp66 = icmp sge i32 %48, 1
  br i1 %cmp66, label %if.then67, label %if.end69

if.then67:                                        ; preds = %if.end65
  %49 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %50 = load i8*, i8** %input_filename.addr, align 4, !tbaa !2
  %call68 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %49, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.93, i32 0, i32 0), i8* %50)
  br label %if.end69

if.end69:                                         ; preds = %if.then67, %if.end65
  %51 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp70 = icmp sge i32 %51, 1
  br i1 %cmp70, label %if.then71, label %if.end73

if.then71:                                        ; preds = %if.end69
  %52 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call72 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %52, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end73

if.end73:                                         ; preds = %if.then71, %if.end69
  call void @exit(i32 72) #6
  unreachable

if.end74:                                         ; preds = %lor.lhs.false58
  %53 = bitcast i32* %readBytes53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #5
  br label %if.end75

if.end75:                                         ; preds = %if.end74, %if.then50
  %54 = bitcast %struct.LZ4F_dctx_s** %dctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #5
  %55 = bitcast i32* %isError to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #5
  %call76 = call i32 @LZ4F_createDecompressionContext(%struct.LZ4F_dctx_s** %dctx, i32 100)
  %call77 = call i32 @LZ4F_isError(i32 %call76)
  store i32 %call77, i32* %isError, align 4, !tbaa !6
  %56 = load i32, i32* %isError, align 4, !tbaa !6
  %tobool78 = icmp ne i32 %56, 0
  br i1 %tobool78, label %if.end154, label %if.then79

if.then79:                                        ; preds = %if.end75
  %57 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx, align 4, !tbaa !2
  %lz4FrameInfo = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameInfo, i32 0, i32 0
  %arraydecay80 = getelementptr inbounds [19 x i8], [19 x i8]* %buffer, i32 0, i32 0
  %call81 = call i32 @LZ4F_getFrameInfo(%struct.LZ4F_dctx_s* %57, %struct.LZ4F_frameInfo_t* %lz4FrameInfo, i8* %arraydecay80, i32* %hSize)
  %call82 = call i32 @LZ4F_isError(i32 %call81)
  store i32 %call82, i32* %isError, align 4, !tbaa !6
  %58 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx, align 4, !tbaa !2
  %call83 = call i32 @LZ4F_freeDecompressionContext(%struct.LZ4F_dctx_s* %58)
  %59 = load i32, i32* %isError, align 4, !tbaa !6
  %tobool84 = icmp ne i32 %59, 0
  br i1 %tobool84, label %if.end153, label %if.then85

if.then85:                                        ; preds = %if.then79
  %60 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %frameSummary86 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %60, i32 0, i32 3
  %lz4FrameInfo87 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameSummary86, i32 0, i32 0
  %blockSizeID = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %lz4FrameInfo87, i32 0, i32 0
  %61 = load i32, i32* %blockSizeID, align 8, !tbaa !64
  %lz4FrameInfo88 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameInfo, i32 0, i32 0
  %blockSizeID89 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %lz4FrameInfo88, i32 0, i32 0
  %62 = load i32, i32* %blockSizeID89, align 8, !tbaa !69
  %cmp90 = icmp ne i32 %61, %62
  br i1 %cmp90, label %land.lhs.true, label %lor.lhs.false91

lor.lhs.false91:                                  ; preds = %if.then85
  %63 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %frameSummary92 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %63, i32 0, i32 3
  %lz4FrameInfo93 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameSummary92, i32 0, i32 0
  %blockMode = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %lz4FrameInfo93, i32 0, i32 1
  %64 = load i32, i32* %blockMode, align 4, !tbaa !65
  %lz4FrameInfo94 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameInfo, i32 0, i32 0
  %blockMode95 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %lz4FrameInfo94, i32 0, i32 1
  %65 = load i32, i32* %blockMode95, align 4, !tbaa !70
  %cmp96 = icmp ne i32 %64, %65
  br i1 %cmp96, label %land.lhs.true, label %if.end99

land.lhs.true:                                    ; preds = %lor.lhs.false91, %if.then85
  %66 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %frameCount = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %66, i32 0, i32 2
  %67 = load i64, i64* %frameCount, align 8, !tbaa !60
  %cmp97 = icmp ne i64 %67, 0
  br i1 %cmp97, label %if.then98, label %if.end99

if.then98:                                        ; preds = %land.lhs.true
  %68 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %eqBlockTypes = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %68, i32 0, i32 5
  store i16 0, i16* %eqBlockTypes, align 2, !tbaa !63
  br label %if.end99

if.end99:                                         ; preds = %if.then98, %land.lhs.true, %lor.lhs.false91
  %69 = bitcast i64* %totalBlocksSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %69) #5
  %70 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %lz4FrameInfo100 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameInfo, i32 0, i32 0
  %blockChecksumFlag = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %lz4FrameInfo100, i32 0, i32 6
  %71 = load i32, i32* %blockChecksumFlag, align 4, !tbaa !71
  %lz4FrameInfo101 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameInfo, i32 0, i32 0
  %contentChecksumFlag = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %lz4FrameInfo101, i32 0, i32 2
  %72 = load i32, i32* %contentChecksumFlag, align 8, !tbaa !72
  %call102 = call i64 @LZ4IO_skipBlocksData(%struct._IO_FILE* %70, i32 %71, i32 %72)
  store i64 %call102, i64* %totalBlocksSize, align 8, !tbaa !26
  %73 = load i64, i64* %totalBlocksSize, align 8, !tbaa !26
  %tobool103 = icmp ne i64 %73, 0
  br i1 %tobool103, label %if.then104, label %if.end152

if.then104:                                       ; preds = %if.end99
  %74 = bitcast [5 x i8]* %bTypeBuffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 5, i8* %74) #5
  %lz4FrameInfo105 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameInfo, i32 0, i32 0
  %blockSizeID106 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %lz4FrameInfo105, i32 0, i32 0
  %75 = load i32, i32* %blockSizeID106, align 8, !tbaa !69
  %lz4FrameInfo107 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameInfo, i32 0, i32 0
  %blockMode108 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %lz4FrameInfo107, i32 0, i32 1
  %76 = load i32, i32* %blockMode108, align 4, !tbaa !70
  %arraydecay109 = getelementptr inbounds [5 x i8], [5 x i8]* %bTypeBuffer, i32 0, i32 0
  %call110 = call i8* @LZ4IO_blockTypeID(i32 %75, i32 %76, i8* %arraydecay109)
  %77 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp111 = icmp sge i32 %77, 3
  br i1 %cmp111, label %if.then112, label %if.end120

if.then112:                                       ; preds = %if.then104
  %78 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %79 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %frameCount113 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %79, i32 0, i32 2
  %80 = load i64, i64* %frameCount113, align 8, !tbaa !60
  %add = add i64 %80, 1
  %frameType114 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameInfo, i32 0, i32 1
  %81 = load i32, i32* %frameType114, align 8, !tbaa !73
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* @LZ4IO_frameTypeNames, i32 0, i32 %81
  %82 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %arraydecay115 = getelementptr inbounds [5 x i8], [5 x i8]* %bTypeBuffer, i32 0, i32 0
  %lz4FrameInfo116 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameInfo, i32 0, i32 0
  %contentChecksumFlag117 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %lz4FrameInfo116, i32 0, i32 2
  %83 = load i32, i32* %contentChecksumFlag117, align 8, !tbaa !72
  %tobool118 = icmp ne i32 %83, 0
  %84 = zext i1 %tobool118 to i64
  %cond = select i1 %tobool118, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.95, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.38, i32 0, i32 0)
  %call119 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %78, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.94, i32 0, i32 0), i64 %add, i8* %82, i8* %arraydecay115, i8* %cond)
  br label %if.end120

if.end120:                                        ; preds = %if.then112, %if.then104
  %lz4FrameInfo121 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameInfo, i32 0, i32 0
  %contentSize = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %lz4FrameInfo121, i32 0, i32 4
  %85 = load i64, i64* %contentSize, align 8, !tbaa !74
  %tobool122 = icmp ne i64 %85, 0
  br i1 %tobool122, label %if.then123, label %if.else

if.then123:                                       ; preds = %if.end120
  %86 = bitcast double* %ratio to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %86) #5
  %87 = load i64, i64* %totalBlocksSize, align 8, !tbaa !26
  %88 = load i32, i32* %hSize, align 4, !tbaa !24
  %conv = zext i32 %88 to i64
  %add124 = add i64 %87, %conv
  %conv125 = uitofp i64 %add124 to double
  %lz4FrameInfo126 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameInfo, i32 0, i32 0
  %contentSize127 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %lz4FrameInfo126, i32 0, i32 4
  %89 = load i64, i64* %contentSize127, align 8, !tbaa !74
  %conv128 = uitofp i64 %89 to double
  %div = fdiv double %conv125, %conv128
  %mul = fmul double %div, 1.000000e+02
  store double %mul, double* %ratio, align 8, !tbaa !28
  %90 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp129 = icmp sge i32 %90, 3
  br i1 %cmp129, label %if.then131, label %if.end137

if.then131:                                       ; preds = %if.then123
  %91 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %92 = load i64, i64* %totalBlocksSize, align 8, !tbaa !26
  %93 = load i32, i32* %hSize, align 4, !tbaa !24
  %conv132 = zext i32 %93 to i64
  %add133 = add i64 %92, %conv132
  %lz4FrameInfo134 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameInfo, i32 0, i32 0
  %contentSize135 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %lz4FrameInfo134, i32 0, i32 4
  %94 = load i64, i64* %contentSize135, align 8, !tbaa !74
  %95 = load double, double* %ratio, align 8, !tbaa !28
  %call136 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %91, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.96, i32 0, i32 0), i64 %add133, i64 %94, double %95)
  br label %if.end137

if.end137:                                        ; preds = %if.then131, %if.then123
  %96 = bitcast double* %ratio to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %96) #5
  %97 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %frameSummary138 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %97, i32 0, i32 3
  %lz4FrameInfo139 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameSummary138, i32 0, i32 0
  %contentSize140 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %lz4FrameInfo139, i32 0, i32 4
  %98 = load i64, i64* %contentSize140, align 8, !tbaa !68
  %lz4FrameInfo141 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameInfo, i32 0, i32 0
  %contentSize142 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %lz4FrameInfo141, i32 0, i32 4
  %99 = load i64, i64* %contentSize142, align 8, !tbaa !74
  %add143 = add i64 %99, %98
  store i64 %add143, i64* %contentSize142, align 8, !tbaa !74
  br label %if.end151

if.else:                                          ; preds = %if.end120
  %100 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp144 = icmp sge i32 %100, 3
  br i1 %cmp144, label %if.then146, label %if.end150

if.then146:                                       ; preds = %if.else
  %101 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %102 = load i64, i64* %totalBlocksSize, align 8, !tbaa !26
  %103 = load i32, i32* %hSize, align 4, !tbaa !24
  %conv147 = zext i32 %103 to i64
  %add148 = add i64 %102, %conv147
  %call149 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %101, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.97, i32 0, i32 0), i64 %add148, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.38, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.38, i32 0, i32 0))
  br label %if.end150

if.end150:                                        ; preds = %if.then146, %if.else
  %104 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %allContentSize = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %104, i32 0, i32 6
  store i16 0, i16* %allContentSize, align 4, !tbaa !67
  br label %if.end151

if.end151:                                        ; preds = %if.end150, %if.end137
  store i32 0, i32* %result, align 4, !tbaa !25
  %105 = bitcast [5 x i8]* %bTypeBuffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 5, i8* %105) #5
  br label %if.end152

if.end152:                                        ; preds = %if.end151, %if.end99
  %106 = bitcast i64* %totalBlocksSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %106) #5
  br label %if.end153

if.end153:                                        ; preds = %if.end152, %if.then79
  br label %if.end154

if.end154:                                        ; preds = %if.end153, %if.end75
  %107 = bitcast i32* %isError to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #5
  %108 = bitcast %struct.LZ4F_dctx_s** %dctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #5
  br label %if.end155

if.end155:                                        ; preds = %if.end154, %if.end46
  %109 = bitcast i32* %hSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #5
  br label %sw.epilog

sw.bb156:                                         ; preds = %if.end24
  %frameType157 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameInfo, i32 0, i32 1
  store i32 1, i32* %frameType157, align 8, !tbaa !73
  %110 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %frameSummary158 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %110, i32 0, i32 3
  %frameType159 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameSummary158, i32 0, i32 1
  %111 = load i32, i32* %frameType159, align 8, !tbaa !62
  %cmp160 = icmp ne i32 %111, 1
  br i1 %cmp160, label %land.lhs.true162, label %if.end168

land.lhs.true162:                                 ; preds = %sw.bb156
  %112 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %frameCount163 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %112, i32 0, i32 2
  %113 = load i64, i64* %frameCount163, align 8, !tbaa !60
  %cmp164 = icmp ne i64 %113, 0
  br i1 %cmp164, label %if.then166, label %if.end168

if.then166:                                       ; preds = %land.lhs.true162
  %114 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %eqFrameTypes167 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %114, i32 0, i32 4
  store i16 0, i16* %eqFrameTypes167, align 8, !tbaa !61
  br label %if.end168

if.end168:                                        ; preds = %if.then166, %land.lhs.true162, %sw.bb156
  %115 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %eqBlockTypes169 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %115, i32 0, i32 5
  store i16 0, i16* %eqBlockTypes169, align 2, !tbaa !63
  %116 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %allContentSize170 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %116, i32 0, i32 6
  store i16 0, i16* %allContentSize170, align 4, !tbaa !67
  %117 = bitcast i64* %totalBlocksSize171 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %117) #5
  %118 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %call172 = call i64 @LZ4IO_skipLegacyBlocksData(%struct._IO_FILE* %118)
  store i64 %call172, i64* %totalBlocksSize171, align 8, !tbaa !26
  %119 = load i64, i64* %totalBlocksSize171, align 8, !tbaa !26
  %tobool173 = icmp ne i64 %119, 0
  br i1 %tobool173, label %if.then174, label %if.end185

if.then174:                                       ; preds = %if.end168
  %120 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp175 = icmp sge i32 %120, 3
  br i1 %cmp175, label %if.then177, label %if.end184

if.then177:                                       ; preds = %if.then174
  %121 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %122 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %frameCount178 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %122, i32 0, i32 2
  %123 = load i64, i64* %frameCount178, align 8, !tbaa !60
  %add179 = add i64 %123, 1
  %frameType180 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameInfo, i32 0, i32 1
  %124 = load i32, i32* %frameType180, align 8, !tbaa !73
  %arrayidx181 = getelementptr inbounds [3 x i8*], [3 x i8*]* @LZ4IO_frameTypeNames, i32 0, i32 %124
  %125 = load i8*, i8** %arrayidx181, align 4, !tbaa !2
  %126 = load i64, i64* %totalBlocksSize171, align 8, !tbaa !26
  %add182 = add i64 %126, 4
  %call183 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %121, i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.98, i32 0, i32 0), i64 %add179, i8* %125, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.38, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.38, i32 0, i32 0), i64 %add182, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.38, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.38, i32 0, i32 0))
  br label %if.end184

if.end184:                                        ; preds = %if.then177, %if.then174
  store i32 0, i32* %result, align 4, !tbaa !25
  br label %if.end185

if.end185:                                        ; preds = %if.end184, %if.end168
  %127 = bitcast i64* %totalBlocksSize171 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %127) #5
  br label %sw.epilog

sw.bb186:                                         ; preds = %if.end24
  %frameType187 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameInfo, i32 0, i32 1
  store i32 2, i32* %frameType187, align 8, !tbaa !73
  %128 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %frameSummary188 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %128, i32 0, i32 3
  %frameType189 = getelementptr inbounds %struct.LZ4IO_frameInfo_t, %struct.LZ4IO_frameInfo_t* %frameSummary188, i32 0, i32 1
  %129 = load i32, i32* %frameType189, align 8, !tbaa !62
  %cmp190 = icmp ne i32 %129, 2
  br i1 %cmp190, label %land.lhs.true192, label %if.end198

land.lhs.true192:                                 ; preds = %sw.bb186
  %130 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %frameCount193 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %130, i32 0, i32 2
  %131 = load i64, i64* %frameCount193, align 8, !tbaa !60
  %cmp194 = icmp ne i64 %131, 0
  br i1 %cmp194, label %if.then196, label %if.end198

if.then196:                                       ; preds = %land.lhs.true192
  %132 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %eqFrameTypes197 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %132, i32 0, i32 4
  store i16 0, i16* %eqFrameTypes197, align 8, !tbaa !61
  br label %if.end198

if.end198:                                        ; preds = %if.then196, %land.lhs.true192, %sw.bb186
  %133 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %eqBlockTypes199 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %133, i32 0, i32 5
  store i16 0, i16* %eqBlockTypes199, align 2, !tbaa !63
  %134 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %allContentSize200 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %134, i32 0, i32 6
  store i16 0, i16* %allContentSize200, align 4, !tbaa !67
  %arraydecay201 = getelementptr inbounds [19 x i8], [19 x i8]* %buffer, i32 0, i32 0
  %135 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %call202 = call i32 @fread(i8* %arraydecay201, i32 1, i32 4, %struct._IO_FILE* %135)
  store i32 %call202, i32* %nbReadBytes, align 4, !tbaa !24
  %136 = load i32, i32* %nbReadBytes, align 4, !tbaa !24
  %cmp203 = icmp ne i32 %136, 4
  br i1 %cmp203, label %if.then205, label %if.end221

if.then205:                                       ; preds = %if.end198
  %137 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp206 = icmp sge i32 %137, 1
  br i1 %cmp206, label %if.then208, label %if.end210

if.then208:                                       ; preds = %if.then205
  %138 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call209 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %138, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 42)
  br label %if.end210

if.end210:                                        ; preds = %if.then208, %if.then205
  %139 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp211 = icmp sge i32 %139, 1
  br i1 %cmp211, label %if.then213, label %if.end215

if.then213:                                       ; preds = %if.end210
  %140 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call214 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %140, i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.70, i32 0, i32 0))
  br label %if.end215

if.end215:                                        ; preds = %if.then213, %if.end210
  %141 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp216 = icmp sge i32 %141, 1
  br i1 %cmp216, label %if.then218, label %if.end220

if.then218:                                       ; preds = %if.end215
  %142 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call219 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %142, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end220

if.end220:                                        ; preds = %if.then218, %if.end215
  call void @exit(i32 42) #6
  unreachable

if.end221:                                        ; preds = %if.end198
  %143 = bitcast i32* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %143) #5
  %arraydecay222 = getelementptr inbounds [19 x i8], [19 x i8]* %buffer, i32 0, i32 0
  %call223 = call i32 @LZ4IO_readLE32(i8* %arraydecay222)
  store i32 %call223, i32* %size, align 4, !tbaa !6
  %144 = bitcast i32* %errorNb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %144) #5
  %145 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %146 = load i32, i32* %size, align 4, !tbaa !6
  %call224 = call i32 @fseek_u32(%struct._IO_FILE* %145, i32 %146, i32 1)
  store i32 %call224, i32* %errorNb, align 4, !tbaa !6
  %147 = load i32, i32* %errorNb, align 4, !tbaa !6
  %cmp225 = icmp ne i32 %147, 0
  br i1 %cmp225, label %if.then227, label %if.end243

if.then227:                                       ; preds = %if.end221
  %148 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp228 = icmp sge i32 %148, 1
  br i1 %cmp228, label %if.then230, label %if.end232

if.then230:                                       ; preds = %if.then227
  %149 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call231 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %149, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 43)
  br label %if.end232

if.end232:                                        ; preds = %if.then230, %if.then227
  %150 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp233 = icmp sge i32 %150, 1
  br i1 %cmp233, label %if.then235, label %if.end237

if.then235:                                       ; preds = %if.end232
  %151 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call236 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %151, i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.71, i32 0, i32 0))
  br label %if.end237

if.end237:                                        ; preds = %if.then235, %if.end232
  %152 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp238 = icmp sge i32 %152, 1
  br i1 %cmp238, label %if.then240, label %if.end242

if.then240:                                       ; preds = %if.end237
  %153 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call241 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %153, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end242

if.end242:                                        ; preds = %if.then240, %if.end237
  call void @exit(i32 43) #6
  unreachable

if.end243:                                        ; preds = %if.end221
  %154 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp244 = icmp sge i32 %154, 3
  br i1 %cmp244, label %if.then246, label %if.end251

if.then246:                                       ; preds = %if.end243
  %155 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %156 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %frameCount247 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %156, i32 0, i32 2
  %157 = load i64, i64* %frameCount247, align 8, !tbaa !60
  %add248 = add i64 %157, 1
  %158 = load i32, i32* %size, align 4, !tbaa !6
  %add249 = add i32 %158, 8
  %call250 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %155, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.99, i32 0, i32 0), i64 %add248, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.100, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.38, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.38, i32 0, i32 0), i32 %add249, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.38, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.38, i32 0, i32 0))
  br label %if.end251

if.end251:                                        ; preds = %if.then246, %if.end243
  store i32 0, i32* %result, align 4, !tbaa !25
  %159 = bitcast i32* %errorNb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #5
  %160 = bitcast i32* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #5
  br label %sw.epilog

sw.default:                                       ; preds = %if.end24
  %161 = bitcast i32* %position to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %161) #5
  %162 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %call252 = call i32 @ftell(%struct._IO_FILE* %162)
  store i32 %call252, i32* %position, align 4, !tbaa !24
  %163 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp253 = icmp sge i32 %163, 3
  br i1 %cmp253, label %if.then255, label %if.end257

if.then255:                                       ; preds = %sw.default
  %164 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call256 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %164, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.73, i32 0, i32 0))
  br label %if.end257

if.end257:                                        ; preds = %if.then255, %sw.default
  %165 = load i32, i32* %position, align 4, !tbaa !24
  %cmp258 = icmp ne i32 %165, -1
  br i1 %cmp258, label %if.then260, label %if.end266

if.then260:                                       ; preds = %if.end257
  %166 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp261 = icmp sge i32 %166, 3
  br i1 %cmp261, label %if.then263, label %if.end265

if.then263:                                       ; preds = %if.then260
  %167 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %168 = load i32, i32* %position, align 4, !tbaa !24
  %call264 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %167, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.74, i32 0, i32 0), i32 %168)
  br label %if.end265

if.end265:                                        ; preds = %if.then263, %if.then260
  br label %if.end266

if.end266:                                        ; preds = %if.end265, %if.end257
  %169 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp267 = icmp sge i32 %169, 3
  br i1 %cmp267, label %if.then269, label %if.end271

if.then269:                                       ; preds = %if.end266
  %170 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call270 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %170, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.36, i32 0, i32 0))
  br label %if.end271

if.end271:                                        ; preds = %if.then269, %if.end266
  %171 = bitcast i32* %position to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #5
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end271, %if.end251, %if.end185, %if.end155
  %172 = load i32, i32* %result, align 4, !tbaa !25
  %cmp272 = icmp ne i32 %172, 0
  br i1 %cmp272, label %if.then274, label %if.end275

if.then274:                                       ; preds = %sw.epilog
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end275:                                        ; preds = %sw.epilog
  %173 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %frameSummary276 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %173, i32 0, i32 3
  %174 = bitcast %struct.LZ4IO_frameInfo_t* %frameSummary276 to i8*
  %175 = bitcast %struct.LZ4IO_frameInfo_t* %frameInfo to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %174, i8* align 8 %175, i32 40, i1 false), !tbaa.struct !75
  %176 = load %struct.LZ4IO_cFileInfo_t*, %struct.LZ4IO_cFileInfo_t** %cfinfo.addr, align 4, !tbaa !2
  %frameCount277 = getelementptr inbounds %struct.LZ4IO_cFileInfo_t, %struct.LZ4IO_cFileInfo_t* %176, i32 0, i32 2
  %177 = load i64, i64* %frameCount277, align 8, !tbaa !60
  %inc = add i64 %177, 1
  store i64 %inc, i64* %frameCount277, align 8, !tbaa !60
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end275, %if.then274, %if.then
  %178 = bitcast i32* %nbReadBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #5
  %179 = bitcast i32* %magicNumber to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #5
  %180 = bitcast %struct.LZ4IO_frameInfo_t* %frameInfo to i8*
  call void @llvm.lifetime.end.p0i8(i64 40, i8* %180) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 3, label %while.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %cleanup, %while.cond
  %181 = load %struct._IO_FILE*, %struct._IO_FILE** %finput, align 4, !tbaa !2
  %call280 = call i32 @fclose(%struct._IO_FILE* %181)
  %182 = load i32, i32* %result, align 4, !tbaa !25
  store i32 1, i32* %cleanup.dest.slot, align 4
  %183 = bitcast %struct._IO_FILE** %finput to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #5
  %184 = bitcast [19 x i8]* %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 19, i8* %184) #5
  %185 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #5
  ret i32 %182

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal i8* @LZ4IO_toHuman(fp128 %size, i8* %buf) #0 {
entry:
  %size.addr = alloca fp128, align 16
  %buf.addr = alloca i8*, align 4
  %units = alloca [10 x i8], align 1
  %i = alloca i32, align 4
  store fp128 %size, fp128* %size.addr, align 16, !tbaa !76
  store i8* %buf, i8** %buf.addr, align 4, !tbaa !2
  %0 = bitcast [10 x i8]* %units to i8*
  call void @llvm.lifetime.start.p0i8(i64 10, i8* %0) #5
  %1 = bitcast [10 x i8]* %units to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %1, i8* align 1 getelementptr inbounds ([10 x i8], [10 x i8]* @__const.LZ4IO_toHuman.units, i32 0, i32 0), i32 10, i1 false)
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i32 0, i32* %i, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load fp128, fp128* %size.addr, align 16, !tbaa !76
  %cmp = fcmp oge fp128 %3, 0xL00000000000000004009000000000000
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load fp128, fp128* %size.addr, align 16, !tbaa !76
  %div = fdiv fp128 %4, 0xL00000000000000004009000000000000
  store fp128 %div, fp128* %size.addr, align 16, !tbaa !76
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4, !tbaa !24
  %inc = add i32 %5, 1
  store i32 %inc, i32* %i, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %6 = load i8*, i8** %buf.addr, align 4, !tbaa !2
  %7 = load fp128, fp128* %size.addr, align 16, !tbaa !76
  %8 = load i32, i32* %i, align 4, !tbaa !24
  %arrayidx = getelementptr inbounds [10 x i8], [10 x i8]* %units, i32 0, i32 %8
  %9 = load i8, i8* %arrayidx, align 1, !tbaa !25
  %conv = sext i8 %9 to i32
  %call = call i32 (i8*, i8*, ...) @sprintf(i8* %6, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.103, i32 0, i32 0), fp128 %7, i32 %conv)
  %10 = load i8*, i8** %buf.addr, align 4, !tbaa !2
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #5
  %12 = bitcast [10 x i8]* %units to i8*
  call void @llvm.lifetime.end.p0i8(i64 10, i8* %12) #5
  ret i8* %10
}

declare i32 @LZ4_compress_fast(i8*, i8*, i32, i32, i32) #2

declare %struct._IO_FILE* @fopen(i8*, i8*) #2

declare i8* @strerror(i32) #2

declare i32* @__errno_location() #2

declare i32 @getchar() #2

declare i32 @LZ4F_createCompressionContext(%struct.LZ4F_cctx_s**, i32) #2

declare i32 @LZ4F_isError(i32) #2

declare i8* @LZ4F_getErrorName(i32) #2

declare i32 @LZ4F_compressFrameBound(i32, %struct.LZ4F_preferences_t*) #2

; Function Attrs: nounwind
define internal %struct.LZ4F_CDict_s* @LZ4IO_createCDict(%struct.LZ4IO_prefs_s* %prefs) #0 {
entry:
  %retval = alloca %struct.LZ4F_CDict_s*, align 4
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %dictionarySize = alloca i32, align 4
  %dictionaryBuffer = alloca i8*, align 4
  %cdict = alloca %struct.LZ4F_CDict_s*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %0 = bitcast i32* %dictionarySize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i8** %dictionaryBuffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast %struct.LZ4F_CDict_s** %cdict to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %useDictionary = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %3, i32 0, i32 10
  %4 = load i32, i32* %useDictionary, align 4, !tbaa !20
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.LZ4F_CDict_s* null, %struct.LZ4F_CDict_s** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %5 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %call = call i8* @LZ4IO_createDict(%struct.LZ4IO_prefs_s* %5, i32* %dictionarySize)
  store i8* %call, i8** %dictionaryBuffer, align 4, !tbaa !2
  %6 = load i8*, i8** %dictionaryBuffer, align 4, !tbaa !2
  %tobool1 = icmp ne i8* %6, null
  br i1 %tobool1, label %if.end14, label %if.then2

if.then2:                                         ; preds = %if.end
  %7 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp = icmp sge i32 %7, 1
  br i1 %cmp, label %if.then3, label %if.end5

if.then3:                                         ; preds = %if.then2
  %8 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call4 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %8, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 25)
  br label %if.end5

if.end5:                                          ; preds = %if.then3, %if.then2
  %9 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp6 = icmp sge i32 %9, 1
  br i1 %cmp6, label %if.then7, label %if.end9

if.then7:                                         ; preds = %if.end5
  %10 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call8 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %10, i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.53, i32 0, i32 0))
  br label %if.end9

if.end9:                                          ; preds = %if.then7, %if.end5
  %11 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp10 = icmp sge i32 %11, 1
  br i1 %cmp10, label %if.then11, label %if.end13

if.then11:                                        ; preds = %if.end9
  %12 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call12 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %12, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end13

if.end13:                                         ; preds = %if.then11, %if.end9
  call void @exit(i32 25) #6
  unreachable

if.end14:                                         ; preds = %if.end
  %13 = load i8*, i8** %dictionaryBuffer, align 4, !tbaa !2
  %14 = load i32, i32* %dictionarySize, align 4, !tbaa !24
  %call15 = call %struct.LZ4F_CDict_s* @LZ4F_createCDict(i8* %13, i32 %14)
  store %struct.LZ4F_CDict_s* %call15, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !2
  %15 = load i8*, i8** %dictionaryBuffer, align 4, !tbaa !2
  call void @free(i8* %15)
  %16 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !2
  store %struct.LZ4F_CDict_s* %16, %struct.LZ4F_CDict_s** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end14, %if.then
  %17 = bitcast %struct.LZ4F_CDict_s** %cdict to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  %18 = bitcast i8** %dictionaryBuffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #5
  %19 = bitcast i32* %dictionarySize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #5
  %20 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %retval, align 4
  ret %struct.LZ4F_CDict_s* %20
}

; Function Attrs: nounwind
define internal i8* @LZ4IO_createDict(%struct.LZ4IO_prefs_s* %prefs, i32* %dictSize) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %dictSize.addr = alloca i32*, align 4
  %readSize = alloca i32, align 4
  %dictEnd = alloca i32, align 4
  %dictLen = alloca i32, align 4
  %dictStart = alloca i32, align 4
  %circularBufSize = alloca i32, align 4
  %circularBuf = alloca i8*, align 4
  %dictBuf = alloca i8*, align 4
  %dictFilename = alloca i8*, align 4
  %dictFile = alloca %struct._IO_FILE*, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store i32* %dictSize, i32** %dictSize.addr, align 4, !tbaa !2
  %0 = bitcast i32* %readSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %dictEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 0, i32* %dictEnd, align 4, !tbaa !24
  %2 = bitcast i32* %dictLen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i32 0, i32* %dictLen, align 4, !tbaa !24
  %3 = bitcast i32* %dictStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = bitcast i32* %circularBufSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  store i32 65536, i32* %circularBufSize, align 4, !tbaa !24
  %5 = bitcast i8** %circularBuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = bitcast i8** %dictBuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = bitcast i8** %dictFilename to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %dictionaryFilename = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %8, i32 0, i32 12
  %9 = load i8*, i8** %dictionaryFilename, align 4, !tbaa !22
  store i8* %9, i8** %dictFilename, align 4, !tbaa !2
  %10 = bitcast %struct._IO_FILE** %dictFile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load i8*, i8** %dictFilename, align 4, !tbaa !2
  %tobool = icmp ne i8* %11, null
  br i1 %tobool, label %if.end10, label %if.then

if.then:                                          ; preds = %entry
  %12 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp = icmp sge i32 %12, 1
  br i1 %cmp, label %if.then1, label %if.end

if.then1:                                         ; preds = %if.then
  %13 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %13, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 25)
  br label %if.end

if.end:                                           ; preds = %if.then1, %if.then
  %14 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp2 = icmp sge i32 %14, 1
  br i1 %cmp2, label %if.then3, label %if.end5

if.then3:                                         ; preds = %if.end
  %15 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call4 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %15, i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.54, i32 0, i32 0))
  br label %if.end5

if.end5:                                          ; preds = %if.then3, %if.end
  %16 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp6 = icmp sge i32 %16, 1
  br i1 %cmp6, label %if.then7, label %if.end9

if.then7:                                         ; preds = %if.end5
  %17 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call8 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %17, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end9

if.end9:                                          ; preds = %if.then7, %if.end5
  call void @exit(i32 25) #6
  unreachable

if.end10:                                         ; preds = %entry
  %18 = load i32, i32* %circularBufSize, align 4, !tbaa !24
  %call11 = call i8* @malloc(i32 %18)
  store i8* %call11, i8** %circularBuf, align 4, !tbaa !2
  %19 = load i8*, i8** %circularBuf, align 4, !tbaa !2
  %tobool12 = icmp ne i8* %19, null
  br i1 %tobool12, label %if.end26, label %if.then13

if.then13:                                        ; preds = %if.end10
  %20 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp14 = icmp sge i32 %20, 1
  br i1 %cmp14, label %if.then15, label %if.end17

if.then15:                                        ; preds = %if.then13
  %21 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call16 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %21, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 25)
  br label %if.end17

if.end17:                                         ; preds = %if.then15, %if.then13
  %22 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp18 = icmp sge i32 %22, 1
  br i1 %cmp18, label %if.then19, label %if.end21

if.then19:                                        ; preds = %if.end17
  %23 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call20 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %23, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.1, i32 0, i32 0))
  br label %if.end21

if.end21:                                         ; preds = %if.then19, %if.end17
  %24 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp22 = icmp sge i32 %24, 1
  br i1 %cmp22, label %if.then23, label %if.end25

if.then23:                                        ; preds = %if.end21
  %25 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call24 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %25, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end25

if.end25:                                         ; preds = %if.then23, %if.end21
  call void @exit(i32 25) #6
  unreachable

if.end26:                                         ; preds = %if.end10
  %26 = load i8*, i8** %dictFilename, align 4, !tbaa !2
  %call27 = call %struct._IO_FILE* @LZ4IO_openSrcFile(i8* %26)
  store %struct._IO_FILE* %call27, %struct._IO_FILE** %dictFile, align 4, !tbaa !2
  %27 = load %struct._IO_FILE*, %struct._IO_FILE** %dictFile, align 4, !tbaa !2
  %tobool28 = icmp ne %struct._IO_FILE* %27, null
  br i1 %tobool28, label %if.end42, label %if.then29

if.then29:                                        ; preds = %if.end26
  %28 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp30 = icmp sge i32 %28, 1
  br i1 %cmp30, label %if.then31, label %if.end33

if.then31:                                        ; preds = %if.then29
  %29 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call32 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %29, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 25)
  br label %if.end33

if.end33:                                         ; preds = %if.then31, %if.then29
  %30 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp34 = icmp sge i32 %30, 1
  br i1 %cmp34, label %if.then35, label %if.end37

if.then35:                                        ; preds = %if.end33
  %31 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call36 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %31, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @.str.55, i32 0, i32 0))
  br label %if.end37

if.end37:                                         ; preds = %if.then35, %if.end33
  %32 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp38 = icmp sge i32 %32, 1
  br i1 %cmp38, label %if.then39, label %if.end41

if.then39:                                        ; preds = %if.end37
  %33 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call40 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %33, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end41

if.end41:                                         ; preds = %if.then39, %if.end37
  call void @exit(i32 25) #6
  unreachable

if.end42:                                         ; preds = %if.end26
  %34 = load i8*, i8** %dictFilename, align 4, !tbaa !2
  %call43 = call i32 @strcmp(i8* %34, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @stdinmark, i32 0, i32 0))
  %tobool44 = icmp ne i32 %call43, 0
  br i1 %tobool44, label %if.then45, label %if.end47

if.then45:                                        ; preds = %if.end42
  %35 = load %struct._IO_FILE*, %struct._IO_FILE** %dictFile, align 4, !tbaa !2
  %call46 = call i32 @fseeko(%struct._IO_FILE* %35, i64 -65536, i32 2)
  br label %if.end47

if.end47:                                         ; preds = %if.then45, %if.end42
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.end47
  %36 = load i8*, i8** %circularBuf, align 4, !tbaa !2
  %37 = load i32, i32* %dictEnd, align 4, !tbaa !24
  %add.ptr = getelementptr inbounds i8, i8* %36, i32 %37
  %38 = load i32, i32* %circularBufSize, align 4, !tbaa !24
  %39 = load i32, i32* %dictEnd, align 4, !tbaa !24
  %sub = sub i32 %38, %39
  %40 = load %struct._IO_FILE*, %struct._IO_FILE** %dictFile, align 4, !tbaa !2
  %call48 = call i32 @fread(i8* %add.ptr, i32 1, i32 %sub, %struct._IO_FILE* %40)
  store i32 %call48, i32* %readSize, align 4, !tbaa !24
  %41 = load i32, i32* %dictEnd, align 4, !tbaa !24
  %42 = load i32, i32* %readSize, align 4, !tbaa !24
  %add = add i32 %41, %42
  %43 = load i32, i32* %circularBufSize, align 4, !tbaa !24
  %rem = urem i32 %add, %43
  store i32 %rem, i32* %dictEnd, align 4, !tbaa !24
  %44 = load i32, i32* %readSize, align 4, !tbaa !24
  %45 = load i32, i32* %dictLen, align 4, !tbaa !24
  %add49 = add i32 %45, %44
  store i32 %add49, i32* %dictLen, align 4, !tbaa !24
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %46 = load i32, i32* %readSize, align 4, !tbaa !24
  %cmp50 = icmp ugt i32 %46, 0
  br i1 %cmp50, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %47 = load i32, i32* %dictLen, align 4, !tbaa !24
  %cmp51 = icmp ugt i32 %47, 65536
  br i1 %cmp51, label %if.then52, label %if.end53

if.then52:                                        ; preds = %do.end
  store i32 65536, i32* %dictLen, align 4, !tbaa !24
  br label %if.end53

if.end53:                                         ; preds = %if.then52, %do.end
  %48 = load i32, i32* %dictLen, align 4, !tbaa !24
  %49 = load i32*, i32** %dictSize.addr, align 4, !tbaa !2
  store i32 %48, i32* %49, align 4, !tbaa !24
  %50 = load i32, i32* %circularBufSize, align 4, !tbaa !24
  %51 = load i32, i32* %dictEnd, align 4, !tbaa !24
  %add54 = add i32 %50, %51
  %52 = load i32, i32* %dictLen, align 4, !tbaa !24
  %sub55 = sub i32 %add54, %52
  %53 = load i32, i32* %circularBufSize, align 4, !tbaa !24
  %rem56 = urem i32 %sub55, %53
  store i32 %rem56, i32* %dictStart, align 4, !tbaa !24
  %54 = load i32, i32* %dictStart, align 4, !tbaa !24
  %cmp57 = icmp eq i32 %54, 0
  br i1 %cmp57, label %if.then58, label %if.else

if.then58:                                        ; preds = %if.end53
  %55 = load i8*, i8** %circularBuf, align 4, !tbaa !2
  store i8* %55, i8** %dictBuf, align 4, !tbaa !2
  store i8* null, i8** %circularBuf, align 4, !tbaa !2
  br label %if.end82

if.else:                                          ; preds = %if.end53
  %56 = load i32, i32* %dictLen, align 4, !tbaa !24
  %tobool59 = icmp ne i32 %56, 0
  br i1 %tobool59, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %57 = load i32, i32* %dictLen, align 4, !tbaa !24
  br label %cond.end

cond.false:                                       ; preds = %if.else
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %57, %cond.true ], [ 1, %cond.false ]
  %call60 = call i8* @malloc(i32 %cond)
  store i8* %call60, i8** %dictBuf, align 4, !tbaa !2
  %58 = load i8*, i8** %dictBuf, align 4, !tbaa !2
  %tobool61 = icmp ne i8* %58, null
  br i1 %tobool61, label %if.end75, label %if.then62

if.then62:                                        ; preds = %cond.end
  %59 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp63 = icmp sge i32 %59, 1
  br i1 %cmp63, label %if.then64, label %if.end66

if.then64:                                        ; preds = %if.then62
  %60 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call65 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %60, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 25)
  br label %if.end66

if.end66:                                         ; preds = %if.then64, %if.then62
  %61 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp67 = icmp sge i32 %61, 1
  br i1 %cmp67, label %if.then68, label %if.end70

if.then68:                                        ; preds = %if.end66
  %62 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call69 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %62, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.1, i32 0, i32 0))
  br label %if.end70

if.end70:                                         ; preds = %if.then68, %if.end66
  %63 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp71 = icmp sge i32 %63, 1
  br i1 %cmp71, label %if.then72, label %if.end74

if.then72:                                        ; preds = %if.end70
  %64 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call73 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %64, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end74

if.end74:                                         ; preds = %if.then72, %if.end70
  call void @exit(i32 25) #6
  unreachable

if.end75:                                         ; preds = %cond.end
  %65 = load i8*, i8** %dictBuf, align 4, !tbaa !2
  %66 = load i8*, i8** %circularBuf, align 4, !tbaa !2
  %67 = load i32, i32* %dictStart, align 4, !tbaa !24
  %add.ptr76 = getelementptr inbounds i8, i8* %66, i32 %67
  %68 = load i32, i32* %circularBufSize, align 4, !tbaa !24
  %69 = load i32, i32* %dictStart, align 4, !tbaa !24
  %sub77 = sub i32 %68, %69
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %65, i8* align 1 %add.ptr76, i32 %sub77, i1 false)
  %70 = load i8*, i8** %dictBuf, align 4, !tbaa !2
  %71 = load i32, i32* %circularBufSize, align 4, !tbaa !24
  %add.ptr78 = getelementptr inbounds i8, i8* %70, i32 %71
  %72 = load i32, i32* %dictStart, align 4, !tbaa !24
  %idx.neg = sub i32 0, %72
  %add.ptr79 = getelementptr inbounds i8, i8* %add.ptr78, i32 %idx.neg
  %73 = load i8*, i8** %circularBuf, align 4, !tbaa !2
  %74 = load i32, i32* %dictLen, align 4, !tbaa !24
  %75 = load i32, i32* %circularBufSize, align 4, !tbaa !24
  %76 = load i32, i32* %dictStart, align 4, !tbaa !24
  %sub80 = sub i32 %75, %76
  %sub81 = sub i32 %74, %sub80
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr79, i8* align 1 %73, i32 %sub81, i1 false)
  br label %if.end82

if.end82:                                         ; preds = %if.end75, %if.then58
  %77 = load %struct._IO_FILE*, %struct._IO_FILE** %dictFile, align 4, !tbaa !2
  %call83 = call i32 @fclose(%struct._IO_FILE* %77)
  %78 = load i8*, i8** %circularBuf, align 4, !tbaa !2
  call void @free(i8* %78)
  %79 = load i8*, i8** %dictBuf, align 4, !tbaa !2
  %80 = bitcast %struct._IO_FILE** %dictFile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #5
  %81 = bitcast i8** %dictFilename to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #5
  %82 = bitcast i8** %dictBuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #5
  %83 = bitcast i8** %circularBuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #5
  %84 = bitcast i32* %circularBufSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #5
  %85 = bitcast i32* %dictStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #5
  %86 = bitcast i32* %dictLen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #5
  %87 = bitcast i32* %dictEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #5
  %88 = bitcast i32* %readSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #5
  ret i8* %79
}

declare %struct.LZ4F_CDict_s* @LZ4F_createCDict(i8*, i32) #2

declare i32 @fseeko(%struct._IO_FILE*, i64, i32) #2

; Function Attrs: nounwind
define internal i64 @UTIL_getFileSize(i8* %infilename) #0 {
entry:
  %retval = alloca i64, align 8
  %infilename.addr = alloca i8*, align 4
  %r = alloca i32, align 4
  %statbuf = alloca %struct.stat, align 8
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %infilename, i8** %infilename.addr, align 4, !tbaa !2
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast %struct.stat* %statbuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 88, i8* %1) #5
  %2 = load i8*, i8** %infilename.addr, align 4, !tbaa !2
  %call = call i32 @stat(i8* %2, %struct.stat* %statbuf)
  store i32 %call, i32* %r, align 4, !tbaa !6
  %3 = load i32, i32* %r, align 4, !tbaa !6
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %st_mode = getelementptr inbounds %struct.stat, %struct.stat* %statbuf, i32 0, i32 3
  %4 = load i32, i32* %st_mode, align 4, !tbaa !78
  %and = and i32 %4, 61440
  %cmp = icmp eq i32 %and, 32768
  br i1 %cmp, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i64 0, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  %st_size = getelementptr inbounds %struct.stat, %struct.stat* %statbuf, i32 0, i32 9
  %5 = load i64, i64* %st_size, align 8, !tbaa !81
  store i64 %5, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %6 = bitcast %struct.stat* %statbuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 88, i8* %6) #5
  %7 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #5
  %8 = load i64, i64* %retval, align 8
  ret i64 %8
}

declare i32 @LZ4F_compressFrame_usingCDict(%struct.LZ4F_cctx_s*, i8*, i32, i8*, i32, %struct.LZ4F_CDict_s*, %struct.LZ4F_preferences_t*) #2

declare i32 @LZ4F_compressBegin_usingCDict(%struct.LZ4F_cctx_s*, i8*, i32, %struct.LZ4F_CDict_s*, %struct.LZ4F_preferences_t*) #2

declare i32 @LZ4F_compressUpdate(%struct.LZ4F_cctx_s*, i8*, i32, i8*, i32, %struct.LZ4F_compressOptions_t*) #2

declare i32 @LZ4F_compressEnd(%struct.LZ4F_cctx_s*, i8*, i32, %struct.LZ4F_compressOptions_t*) #2

; Function Attrs: nounwind
define internal i32 @UTIL_getFileStat(i8* %infilename, %struct.stat* %statbuf) #0 {
entry:
  %retval = alloca i32, align 4
  %infilename.addr = alloca i8*, align 4
  %statbuf.addr = alloca %struct.stat*, align 4
  %r = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %infilename, i8** %infilename.addr, align 4, !tbaa !2
  store %struct.stat* %statbuf, %struct.stat** %statbuf.addr, align 4, !tbaa !2
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %infilename.addr, align 4, !tbaa !2
  %2 = load %struct.stat*, %struct.stat** %statbuf.addr, align 4, !tbaa !2
  %call = call i32 @stat(i8* %1, %struct.stat* %2)
  store i32 %call, i32* %r, align 4, !tbaa !6
  %3 = load i32, i32* %r, align 4, !tbaa !6
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %4 = load %struct.stat*, %struct.stat** %statbuf.addr, align 4, !tbaa !2
  %st_mode = getelementptr inbounds %struct.stat, %struct.stat* %4, i32 0, i32 3
  %5 = load i32, i32* %st_mode, align 4, !tbaa !78
  %and = and i32 %5, 61440
  %cmp = icmp eq i32 %and, 32768
  br i1 %cmp, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %6 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #5
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: nounwind
define internal i32 @UTIL_setFileStat(i8* %filename, %struct.stat* %statbuf) #0 {
entry:
  %retval = alloca i32, align 4
  %filename.addr = alloca i8*, align 4
  %statbuf.addr = alloca %struct.stat*, align 4
  %res = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %timebuf = alloca [2 x %struct.timespec], align 16
  store i8* %filename, i8** %filename.addr, align 4, !tbaa !2
  store %struct.stat* %statbuf, %struct.stat** %statbuf.addr, align 4, !tbaa !2
  %0 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %res, align 4, !tbaa !6
  %1 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  %call = call i32 @UTIL_isRegFile(i8* %1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %2 = bitcast [2 x %struct.timespec]* %timebuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #5
  %3 = bitcast [2 x %struct.timespec]* %timebuf to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %3, i8 0, i32 16, i1 false)
  %arrayidx = getelementptr inbounds [2 x %struct.timespec], [2 x %struct.timespec]* %timebuf, i32 0, i32 0
  %tv_nsec = getelementptr inbounds %struct.timespec, %struct.timespec* %arrayidx, i32 0, i32 1
  store i32 1073741823, i32* %tv_nsec, align 4, !tbaa !82
  %4 = load %struct.stat*, %struct.stat** %statbuf.addr, align 4, !tbaa !2
  %st_mtim = getelementptr inbounds %struct.stat, %struct.stat* %4, i32 0, i32 13
  %tv_sec = getelementptr inbounds %struct.timespec, %struct.timespec* %st_mtim, i32 0, i32 0
  %5 = load i32, i32* %tv_sec, align 8, !tbaa !83
  %arrayidx1 = getelementptr inbounds [2 x %struct.timespec], [2 x %struct.timespec]* %timebuf, i32 0, i32 1
  %tv_sec2 = getelementptr inbounds %struct.timespec, %struct.timespec* %arrayidx1, i32 0, i32 0
  store i32 %5, i32* %tv_sec2, align 8, !tbaa !84
  %6 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [2 x %struct.timespec], [2 x %struct.timespec]* %timebuf, i32 0, i32 0
  %call3 = call i32 @utimensat(i32 -100, i8* %6, %struct.timespec* %arraydecay, i32 0)
  %7 = load i32, i32* %res, align 4, !tbaa !6
  %add = add nsw i32 %7, %call3
  store i32 %add, i32* %res, align 4, !tbaa !6
  %8 = bitcast [2 x %struct.timespec]* %timebuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #5
  %9 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  %10 = load %struct.stat*, %struct.stat** %statbuf.addr, align 4, !tbaa !2
  %st_uid = getelementptr inbounds %struct.stat, %struct.stat* %10, i32 0, i32 5
  %11 = load i32, i32* %st_uid, align 4, !tbaa !85
  %12 = load %struct.stat*, %struct.stat** %statbuf.addr, align 4, !tbaa !2
  %st_gid = getelementptr inbounds %struct.stat, %struct.stat* %12, i32 0, i32 6
  %13 = load i32, i32* %st_gid, align 8, !tbaa !86
  %call4 = call i32 @chown(i8* %9, i32 %11, i32 %13)
  %14 = load i32, i32* %res, align 4, !tbaa !6
  %add5 = add nsw i32 %14, %call4
  store i32 %add5, i32* %res, align 4, !tbaa !6
  %15 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  %16 = load %struct.stat*, %struct.stat** %statbuf.addr, align 4, !tbaa !2
  %st_mode = getelementptr inbounds %struct.stat, %struct.stat* %16, i32 0, i32 3
  %17 = load i32, i32* %st_mode, align 4, !tbaa !78
  %and = and i32 %17, 4095
  %call6 = call i32 @chmod(i8* %15, i32 %and)
  %18 = load i32, i32* %res, align 4, !tbaa !6
  %add7 = add nsw i32 %18, %call6
  store i32 %add7, i32* %res, align 4, !tbaa !6
  %call8 = call i32* @__errno_location()
  store i32 0, i32* %call8, align 4, !tbaa !6
  %19 = load i32, i32* %res, align 4, !tbaa !6
  %sub = sub nsw i32 0, %19
  store i32 %sub, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %20 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  %21 = load i32, i32* %retval, align 4
  ret i32 %21
}

declare i32 @remove(i8*) #2

declare i32 @stat(i8*, %struct.stat*) #2

declare i32 @utimensat(i32, i8*, %struct.timespec*, i32) #2

declare i32 @chown(i8*, i32, i32) #2

declare i32 @chmod(i8*, i32) #2

declare void @LZ4F_freeCDict(%struct.LZ4F_CDict_s*) #2

declare i32 @LZ4F_freeCompressionContext(%struct.LZ4F_cctx_s*) #2

; Function Attrs: nounwind
define internal i64 @UTIL_getSpanTimeNano(i32 %clockStart, i32 %clockEnd) #0 {
entry:
  %clockStart.addr = alloca i32, align 4
  %clockEnd.addr = alloca i32, align 4
  store i32 %clockStart, i32* %clockStart.addr, align 4, !tbaa !24
  store i32 %clockEnd, i32* %clockEnd.addr, align 4, !tbaa !24
  %0 = load i32, i32* %clockEnd.addr, align 4, !tbaa !24
  %1 = load i32, i32* %clockStart.addr, align 4, !tbaa !24
  %sub = sub nsw i32 %0, %1
  %conv = sext i32 %sub to i64
  %mul = mul i64 1000000000, %conv
  %div = udiv i64 %mul, 1000000
  ret i64 %div
}

declare i32 @LZ4F_createDecompressionContext(%struct.LZ4F_dctx_s**, i32) #2

; Function Attrs: nounwind
define internal void @LZ4IO_loadDDict(%struct.LZ4IO_prefs_s* %prefs, %struct.dRess_t* %ress) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %ress.addr = alloca %struct.dRess_t*, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store %struct.dRess_t* %ress, %struct.dRess_t** %ress.addr, align 4, !tbaa !2
  %0 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %useDictionary = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %0, i32 0, i32 10
  %1 = load i32, i32* %useDictionary, align 4, !tbaa !20
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %2 = load %struct.dRess_t*, %struct.dRess_t** %ress.addr, align 4, !tbaa !2
  %dictBuffer = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %2, i32 0, i32 6
  store i8* null, i8** %dictBuffer, align 4, !tbaa !55
  %3 = load %struct.dRess_t*, %struct.dRess_t** %ress.addr, align 4, !tbaa !2
  %dictBufferSize = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %3, i32 0, i32 7
  store i32 0, i32* %dictBufferSize, align 4, !tbaa !87
  br label %if.end17

if.end:                                           ; preds = %entry
  %4 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %5 = load %struct.dRess_t*, %struct.dRess_t** %ress.addr, align 4, !tbaa !2
  %dictBufferSize1 = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %5, i32 0, i32 7
  %call = call i8* @LZ4IO_createDict(%struct.LZ4IO_prefs_s* %4, i32* %dictBufferSize1)
  %6 = load %struct.dRess_t*, %struct.dRess_t** %ress.addr, align 4, !tbaa !2
  %dictBuffer2 = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %6, i32 0, i32 6
  store i8* %call, i8** %dictBuffer2, align 4, !tbaa !55
  %7 = load %struct.dRess_t*, %struct.dRess_t** %ress.addr, align 4, !tbaa !2
  %dictBuffer3 = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %7, i32 0, i32 6
  %8 = load i8*, i8** %dictBuffer3, align 4, !tbaa !55
  %tobool4 = icmp ne i8* %8, null
  br i1 %tobool4, label %if.end17, label %if.then5

if.then5:                                         ; preds = %if.end
  %9 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp = icmp sge i32 %9, 1
  br i1 %cmp, label %if.then6, label %if.end8

if.then6:                                         ; preds = %if.then5
  %10 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call7 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %10, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 25)
  br label %if.end8

if.end8:                                          ; preds = %if.then6, %if.then5
  %11 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp9 = icmp sge i32 %11, 1
  br i1 %cmp9, label %if.then10, label %if.end12

if.then10:                                        ; preds = %if.end8
  %12 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call11 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %12, i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.53, i32 0, i32 0))
  br label %if.end12

if.end12:                                         ; preds = %if.then10, %if.end8
  %13 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp13 = icmp sge i32 %13, 1
  br i1 %cmp13, label %if.then14, label %if.end16

if.then14:                                        ; preds = %if.end12
  %14 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call15 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %14, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end16

if.end16:                                         ; preds = %if.then14, %if.end12
  call void @exit(i32 25) #6
  unreachable

if.end17:                                         ; preds = %if.then, %if.end
  ret void
}

declare i32 @LZ4F_freeDecompressionContext(%struct.LZ4F_dctx_s*) #2

; Function Attrs: nounwind
define internal i64 @selectDecoder(%struct.LZ4IO_prefs_s* %prefs, %struct.dRess_t* byval(%struct.dRess_t) align 4 %ress, %struct._IO_FILE* %finput, %struct._IO_FILE* %foutput) #0 {
entry:
  %retval = alloca i64, align 8
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %finput.addr = alloca %struct._IO_FILE*, align 4
  %foutput.addr = alloca %struct._IO_FILE*, align 4
  %MNstore = alloca [4 x i8], align 1
  %magicNumber = alloca i32, align 4
  %nbReadBytes = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %nbReadBytes36 = alloca i32, align 4
  %size = alloca i32, align 4
  %errorNb = alloca i32, align 4
  %position = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store %struct._IO_FILE* %finput, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  store %struct._IO_FILE* %foutput, %struct._IO_FILE** %foutput.addr, align 4, !tbaa !2
  %0 = bitcast [4 x i8]* %MNstore to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %magicNumber to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load i32, i32* @selectDecoder.nbFrames, align 4, !tbaa !6
  %inc = add i32 %2, 1
  store i32 %inc, i32* @selectDecoder.nbFrames, align 4, !tbaa !6
  %3 = load i32, i32* @g_magicRead, align 4, !tbaa !6
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load i32, i32* @g_magicRead, align 4, !tbaa !6
  store i32 %4, i32* %magicNumber, align 4, !tbaa !6
  store i32 0, i32* @g_magicRead, align 4, !tbaa !6
  br label %if.end19

if.else:                                          ; preds = %entry
  %5 = bitcast i32* %nbReadBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %MNstore, i32 0, i32 0
  %6 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %call = call i32 @fread(i8* %arraydecay, i32 1, i32 4, %struct._IO_FILE* %6)
  store i32 %call, i32* %nbReadBytes, align 4, !tbaa !24
  %7 = load i32, i32* %nbReadBytes, align 4, !tbaa !24
  %cmp = icmp eq i32 %7, 0
  br i1 %cmp, label %if.then1, label %if.end

if.then1:                                         ; preds = %if.else
  store i32 0, i32* @selectDecoder.nbFrames, align 4, !tbaa !6
  store i64 -1, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.else
  %8 = load i32, i32* %nbReadBytes, align 4, !tbaa !24
  %cmp2 = icmp ne i32 %8, 4
  br i1 %cmp2, label %if.then3, label %if.end16

if.then3:                                         ; preds = %if.end
  %9 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp4 = icmp sge i32 %9, 1
  br i1 %cmp4, label %if.then5, label %if.end7

if.then5:                                         ; preds = %if.then3
  %10 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call6 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %10, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 40)
  br label %if.end7

if.end7:                                          ; preds = %if.then5, %if.then3
  %11 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp8 = icmp sge i32 %11, 1
  br i1 %cmp8, label %if.then9, label %if.end11

if.then9:                                         ; preds = %if.end7
  %12 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call10 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %12, i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.67, i32 0, i32 0))
  br label %if.end11

if.end11:                                         ; preds = %if.then9, %if.end7
  %13 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp12 = icmp sge i32 %13, 1
  br i1 %cmp12, label %if.then13, label %if.end15

if.then13:                                        ; preds = %if.end11
  %14 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call14 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %14, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end15

if.end15:                                         ; preds = %if.then13, %if.end11
  call void @exit(i32 40) #6
  unreachable

if.end16:                                         ; preds = %if.end
  %arraydecay17 = getelementptr inbounds [4 x i8], [4 x i8]* %MNstore, i32 0, i32 0
  %call18 = call i32 @LZ4IO_readLE32(i8* %arraydecay17)
  store i32 %call18, i32* %magicNumber, align 4, !tbaa !6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end16, %if.then1
  %15 = bitcast i32* %nbReadBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup111 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end19

if.end19:                                         ; preds = %cleanup.cont, %if.then
  %16 = load i32, i32* %magicNumber, align 4, !tbaa !6
  %call20 = call i32 @LZ4IO_isSkippableMagicNumber(i32 %16)
  %tobool21 = icmp ne i32 %call20, 0
  br i1 %tobool21, label %if.then22, label %if.end23

if.then22:                                        ; preds = %if.end19
  store i32 407710288, i32* %magicNumber, align 4, !tbaa !6
  br label %if.end23

if.end23:                                         ; preds = %if.then22, %if.end19
  %17 = load i32, i32* %magicNumber, align 4, !tbaa !6
  switch i32 %17, label %sw.default [
    i32 407708164, label %sw.bb
    i32 407642370, label %sw.bb25
    i32 407710288, label %sw.bb31
  ]

sw.bb:                                            ; preds = %if.end23
  %18 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %19 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %20 = load %struct._IO_FILE*, %struct._IO_FILE** %foutput.addr, align 4, !tbaa !2
  %call24 = call i64 @LZ4IO_decompressLZ4F(%struct.LZ4IO_prefs_s* %18, %struct.dRess_t* byval(%struct.dRess_t) align 4 %ress, %struct._IO_FILE* %19, %struct._IO_FILE* %20)
  store i64 %call24, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup111

sw.bb25:                                          ; preds = %if.end23
  %21 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp26 = icmp sge i32 %21, 4
  br i1 %cmp26, label %if.then27, label %if.end29

if.then27:                                        ; preds = %sw.bb25
  %22 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call28 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %22, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.68, i32 0, i32 0))
  br label %if.end29

if.end29:                                         ; preds = %if.then27, %sw.bb25
  %23 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %24 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %25 = load %struct._IO_FILE*, %struct._IO_FILE** %foutput.addr, align 4, !tbaa !2
  %call30 = call i64 @LZ4IO_decodeLegacyStream(%struct.LZ4IO_prefs_s* %23, %struct._IO_FILE* %24, %struct._IO_FILE* %25)
  store i64 %call30, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup111

sw.bb31:                                          ; preds = %if.end23
  %26 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp32 = icmp sge i32 %26, 4
  br i1 %cmp32, label %if.then33, label %if.end35

if.then33:                                        ; preds = %sw.bb31
  %27 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call34 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %27, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.69, i32 0, i32 0))
  br label %if.end35

if.end35:                                         ; preds = %if.then33, %sw.bb31
  %28 = bitcast i32* %nbReadBytes36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #5
  %arraydecay37 = getelementptr inbounds [4 x i8], [4 x i8]* %MNstore, i32 0, i32 0
  %29 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %call38 = call i32 @fread(i8* %arraydecay37, i32 1, i32 4, %struct._IO_FILE* %29)
  store i32 %call38, i32* %nbReadBytes36, align 4, !tbaa !24
  %30 = load i32, i32* %nbReadBytes36, align 4, !tbaa !24
  %cmp39 = icmp ne i32 %30, 4
  br i1 %cmp39, label %if.then40, label %if.end53

if.then40:                                        ; preds = %if.end35
  %31 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp41 = icmp sge i32 %31, 1
  br i1 %cmp41, label %if.then42, label %if.end44

if.then42:                                        ; preds = %if.then40
  %32 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call43 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %32, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 42)
  br label %if.end44

if.end44:                                         ; preds = %if.then42, %if.then40
  %33 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp45 = icmp sge i32 %33, 1
  br i1 %cmp45, label %if.then46, label %if.end48

if.then46:                                        ; preds = %if.end44
  %34 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call47 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %34, i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.70, i32 0, i32 0))
  br label %if.end48

if.end48:                                         ; preds = %if.then46, %if.end44
  %35 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp49 = icmp sge i32 %35, 1
  br i1 %cmp49, label %if.then50, label %if.end52

if.then50:                                        ; preds = %if.end48
  %36 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call51 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %36, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end52

if.end52:                                         ; preds = %if.then50, %if.end48
  call void @exit(i32 42) #6
  unreachable

if.end53:                                         ; preds = %if.end35
  %37 = bitcast i32* %nbReadBytes36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #5
  %38 = bitcast i32* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #5
  %arraydecay54 = getelementptr inbounds [4 x i8], [4 x i8]* %MNstore, i32 0, i32 0
  %call55 = call i32 @LZ4IO_readLE32(i8* %arraydecay54)
  store i32 %call55, i32* %size, align 4, !tbaa !6
  %39 = bitcast i32* %errorNb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #5
  %40 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %41 = load i32, i32* %size, align 4, !tbaa !6
  %call56 = call i32 @fseek_u32(%struct._IO_FILE* %40, i32 %41, i32 1)
  store i32 %call56, i32* %errorNb, align 4, !tbaa !6
  %42 = load i32, i32* %errorNb, align 4, !tbaa !6
  %cmp57 = icmp ne i32 %42, 0
  br i1 %cmp57, label %if.then58, label %if.end71

if.then58:                                        ; preds = %if.end53
  %43 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp59 = icmp sge i32 %43, 1
  br i1 %cmp59, label %if.then60, label %if.end62

if.then60:                                        ; preds = %if.then58
  %44 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call61 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %44, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 43)
  br label %if.end62

if.end62:                                         ; preds = %if.then60, %if.then58
  %45 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp63 = icmp sge i32 %45, 1
  br i1 %cmp63, label %if.then64, label %if.end66

if.then64:                                        ; preds = %if.end62
  %46 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call65 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %46, i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.71, i32 0, i32 0))
  br label %if.end66

if.end66:                                         ; preds = %if.then64, %if.end62
  %47 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp67 = icmp sge i32 %47, 1
  br i1 %cmp67, label %if.then68, label %if.end70

if.then68:                                        ; preds = %if.end66
  %48 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call69 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %48, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end70

if.end70:                                         ; preds = %if.then68, %if.end66
  call void @exit(i32 43) #6
  unreachable

if.end71:                                         ; preds = %if.end53
  %49 = bitcast i32* %errorNb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #5
  %50 = bitcast i32* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #5
  store i64 0, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup111

sw.default:                                       ; preds = %if.end23
  %51 = load i32, i32* @selectDecoder.nbFrames, align 4, !tbaa !6
  %cmp72 = icmp eq i32 %51, 1
  br i1 %cmp72, label %if.then73, label %if.end94

if.then73:                                        ; preds = %sw.default
  %52 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %testMode = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %52, i32 0, i32 2
  %53 = load i32, i32* %testMode, align 4, !tbaa !12
  %tobool74 = icmp ne i32 %53, 0
  br i1 %tobool74, label %if.end81, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.then73
  %54 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %overwrite = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %54, i32 0, i32 1
  %55 = load i32, i32* %overwrite, align 4, !tbaa !11
  %tobool75 = icmp ne i32 %55, 0
  br i1 %tobool75, label %land.lhs.true76, label %if.end81

land.lhs.true76:                                  ; preds = %land.lhs.true
  %56 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %passThrough = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %56, i32 0, i32 0
  %57 = load i32, i32* %passThrough, align 4, !tbaa !8
  %tobool77 = icmp ne i32 %57, 0
  br i1 %tobool77, label %if.then78, label %if.end81

if.then78:                                        ; preds = %land.lhs.true76
  store i32 0, i32* @selectDecoder.nbFrames, align 4, !tbaa !6
  %58 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %59 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %60 = load %struct._IO_FILE*, %struct._IO_FILE** %foutput.addr, align 4, !tbaa !2
  %arraydecay79 = getelementptr inbounds [4 x i8], [4 x i8]* %MNstore, i32 0, i32 0
  %call80 = call i64 @LZ4IO_passThrough(%struct.LZ4IO_prefs_s* %58, %struct._IO_FILE* %59, %struct._IO_FILE* %60, i8* %arraydecay79)
  store i64 %call80, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup111

if.end81:                                         ; preds = %land.lhs.true76, %land.lhs.true, %if.then73
  %61 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp82 = icmp sge i32 %61, 1
  br i1 %cmp82, label %if.then83, label %if.end85

if.then83:                                        ; preds = %if.end81
  %62 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call84 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %62, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 44)
  br label %if.end85

if.end85:                                         ; preds = %if.then83, %if.end81
  %63 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp86 = icmp sge i32 %63, 1
  br i1 %cmp86, label %if.then87, label %if.end89

if.then87:                                        ; preds = %if.end85
  %64 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call88 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %64, i8* getelementptr inbounds ([45 x i8], [45 x i8]* @.str.72, i32 0, i32 0))
  br label %if.end89

if.end89:                                         ; preds = %if.then87, %if.end85
  %65 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp90 = icmp sge i32 %65, 1
  br i1 %cmp90, label %if.then91, label %if.end93

if.then91:                                        ; preds = %if.end89
  %66 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call92 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %66, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end93

if.end93:                                         ; preds = %if.then91, %if.end89
  call void @exit(i32 44) #6
  unreachable

if.end94:                                         ; preds = %sw.default
  %67 = bitcast i32* %position to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #5
  %68 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %call95 = call i32 @ftell(%struct._IO_FILE* %68)
  store i32 %call95, i32* %position, align 4, !tbaa !24
  %69 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp96 = icmp sge i32 %69, 2
  br i1 %cmp96, label %if.then97, label %if.end99

if.then97:                                        ; preds = %if.end94
  %70 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call98 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %70, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.73, i32 0, i32 0))
  br label %if.end99

if.end99:                                         ; preds = %if.then97, %if.end94
  %71 = load i32, i32* %position, align 4, !tbaa !24
  %cmp100 = icmp ne i32 %71, -1
  br i1 %cmp100, label %if.then101, label %if.end106

if.then101:                                       ; preds = %if.end99
  %72 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp102 = icmp sge i32 %72, 2
  br i1 %cmp102, label %if.then103, label %if.end105

if.then103:                                       ; preds = %if.then101
  %73 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %74 = load i32, i32* %position, align 4, !tbaa !24
  %call104 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %73, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.74, i32 0, i32 0), i32 %74)
  br label %if.end105

if.end105:                                        ; preds = %if.then103, %if.then101
  br label %if.end106

if.end106:                                        ; preds = %if.end105, %if.end99
  %75 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp107 = icmp sge i32 %75, 2
  br i1 %cmp107, label %if.then108, label %if.end110

if.then108:                                       ; preds = %if.end106
  %76 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call109 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %76, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.36, i32 0, i32 0))
  br label %if.end110

if.end110:                                        ; preds = %if.then108, %if.end106
  %77 = bitcast i32* %position to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #5
  store i64 -1, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup111

cleanup111:                                       ; preds = %if.end110, %if.then78, %if.end71, %if.end29, %sw.bb, %cleanup
  %78 = bitcast i32* %magicNumber to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #5
  %79 = bitcast [4 x i8]* %MNstore to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #5
  %80 = load i64, i64* %retval, align 8
  ret i64 %80
}

; Function Attrs: nounwind
define internal i32 @LZ4IO_readLE32(i8* %s) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %srcPtr = alloca i8*, align 4
  %value32 = alloca i32, align 4
  store i8* %s, i8** %s.addr, align 4, !tbaa !2
  %0 = bitcast i8** %srcPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %s.addr, align 4, !tbaa !2
  store i8* %1, i8** %srcPtr, align 4, !tbaa !2
  %2 = bitcast i32* %value32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i8*, i8** %srcPtr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 0
  %4 = load i8, i8* %arrayidx, align 1, !tbaa !25
  %conv = zext i8 %4 to i32
  store i32 %conv, i32* %value32, align 4, !tbaa !6
  %5 = load i8*, i8** %srcPtr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %5, i32 1
  %6 = load i8, i8* %arrayidx1, align 1, !tbaa !25
  %conv2 = zext i8 %6 to i32
  %shl = shl i32 %conv2, 8
  %7 = load i32, i32* %value32, align 4, !tbaa !6
  %add = add i32 %7, %shl
  store i32 %add, i32* %value32, align 4, !tbaa !6
  %8 = load i8*, i8** %srcPtr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8, i8* %8, i32 2
  %9 = load i8, i8* %arrayidx3, align 1, !tbaa !25
  %conv4 = zext i8 %9 to i32
  %shl5 = shl i32 %conv4, 16
  %10 = load i32, i32* %value32, align 4, !tbaa !6
  %add6 = add i32 %10, %shl5
  store i32 %add6, i32* %value32, align 4, !tbaa !6
  %11 = load i8*, i8** %srcPtr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %11, i32 3
  %12 = load i8, i8* %arrayidx7, align 1, !tbaa !25
  %conv8 = zext i8 %12 to i32
  %shl9 = shl i32 %conv8, 24
  %13 = load i32, i32* %value32, align 4, !tbaa !6
  %add10 = add i32 %13, %shl9
  store i32 %add10, i32* %value32, align 4, !tbaa !6
  %14 = load i32, i32* %value32, align 4, !tbaa !6
  %15 = bitcast i32* %value32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  %16 = bitcast i8** %srcPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #5
  ret i32 %14
}

; Function Attrs: nounwind
define internal i32 @LZ4IO_isSkippableMagicNumber(i32 %magic) #0 {
entry:
  %magic.addr = alloca i32, align 4
  store i32 %magic, i32* %magic.addr, align 4, !tbaa !6
  %0 = load i32, i32* %magic.addr, align 4, !tbaa !6
  %and = and i32 %0, -16
  %cmp = icmp eq i32 %and, 407710288
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: nounwind
define internal i64 @LZ4IO_decompressLZ4F(%struct.LZ4IO_prefs_s* %prefs, %struct.dRess_t* byval(%struct.dRess_t) align 4 %ress, %struct._IO_FILE* %srcFile, %struct._IO_FILE* %dstFile) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %srcFile.addr = alloca %struct._IO_FILE*, align 4
  %dstFile.addr = alloca %struct._IO_FILE*, align 4
  %filesize = alloca i64, align 8
  %nextToLoad = alloca i32, align 4
  %storedSkips = alloca i32, align 4
  %inSize = alloca i32, align 4
  %outSize = alloca i32, align 4
  %readSize = alloca i32, align 4
  %pos = alloca i32, align 4
  %decodedBytes = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %remaining = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store %struct._IO_FILE* %srcFile, %struct._IO_FILE** %srcFile.addr, align 4, !tbaa !2
  store %struct._IO_FILE* %dstFile, %struct._IO_FILE** %dstFile.addr, align 4, !tbaa !2
  %0 = bitcast i64* %filesize to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #5
  store i64 0, i64* %filesize, align 8, !tbaa !26
  %1 = bitcast i32* %nextToLoad to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %storedSkips to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i32 0, i32* %storedSkips, align 4, !tbaa !6
  %3 = bitcast i32* %inSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  store i32 4, i32* %inSize, align 4, !tbaa !24
  %4 = bitcast i32* %outSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  store i32 0, i32* %outSize, align 4, !tbaa !24
  %srcBuffer = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 0
  %5 = load i8*, i8** %srcBuffer, align 4, !tbaa !50
  call void @LZ4IO_writeLE32(i8* %5, i32 407708164)
  %dCtx = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 5
  %6 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dCtx, align 4, !tbaa !54
  %dstBuffer = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 2
  %7 = load i8*, i8** %dstBuffer, align 4, !tbaa !52
  %srcBuffer1 = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 0
  %8 = load i8*, i8** %srcBuffer1, align 4, !tbaa !50
  %dictBuffer = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 6
  %9 = load i8*, i8** %dictBuffer, align 4, !tbaa !55
  %dictBufferSize = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 7
  %10 = load i32, i32* %dictBufferSize, align 4, !tbaa !87
  %call = call i32 @LZ4F_decompress_usingDict(%struct.LZ4F_dctx_s* %6, i8* %7, i32* %outSize, i8* %8, i32* %inSize, i8* %9, i32 %10, %struct.LZ4F_decompressOptions_t* null)
  store i32 %call, i32* %nextToLoad, align 4, !tbaa !24
  %11 = load i32, i32* %nextToLoad, align 4, !tbaa !24
  %call2 = call i32 @LZ4F_isError(i32 %11)
  %tobool = icmp ne i32 %call2, 0
  br i1 %tobool, label %if.then, label %if.end14

if.then:                                          ; preds = %entry
  %12 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp = icmp sge i32 %12, 1
  br i1 %cmp, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %13 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call4 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %13, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 62)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %14 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp5 = icmp sge i32 %14, 1
  br i1 %cmp5, label %if.then6, label %if.end9

if.then6:                                         ; preds = %if.end
  %15 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %16 = load i32, i32* %nextToLoad, align 4, !tbaa !24
  %call7 = call i8* @LZ4F_getErrorName(i32 %16)
  %call8 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %15, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.75, i32 0, i32 0), i8* %call7)
  br label %if.end9

if.end9:                                          ; preds = %if.then6, %if.end
  %17 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp10 = icmp sge i32 %17, 1
  br i1 %cmp10, label %if.then11, label %if.end13

if.then11:                                        ; preds = %if.end9
  %18 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call12 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %18, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end13

if.end13:                                         ; preds = %if.then11, %if.end9
  call void @exit(i32 62) #6
  unreachable

if.end14:                                         ; preds = %entry
  %19 = bitcast i32* %outSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #5
  %20 = bitcast i32* %inSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  br label %for.cond

for.cond:                                         ; preds = %cleanup.cont88, %if.end14
  %21 = load i32, i32* %nextToLoad, align 4, !tbaa !24
  %tobool15 = icmp ne i32 %21, 0
  br i1 %tobool15, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %22 = bitcast i32* %readSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #5
  %23 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #5
  store i32 0, i32* %pos, align 4, !tbaa !24
  %24 = bitcast i32* %decodedBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #5
  %dstBufferSize = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 3
  %25 = load i32, i32* %dstBufferSize, align 4, !tbaa !51
  store i32 %25, i32* %decodedBytes, align 4, !tbaa !24
  %26 = load i32, i32* %nextToLoad, align 4, !tbaa !24
  %srcBufferSize = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 1
  %27 = load i32, i32* %srcBufferSize, align 4, !tbaa !48
  %cmp16 = icmp ugt i32 %26, %27
  br i1 %cmp16, label %if.then17, label %if.end19

if.then17:                                        ; preds = %for.body
  %srcBufferSize18 = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 1
  %28 = load i32, i32* %srcBufferSize18, align 4, !tbaa !48
  store i32 %28, i32* %nextToLoad, align 4, !tbaa !24
  br label %if.end19

if.end19:                                         ; preds = %if.then17, %for.body
  %srcBuffer20 = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 0
  %29 = load i8*, i8** %srcBuffer20, align 4, !tbaa !50
  %30 = load i32, i32* %nextToLoad, align 4, !tbaa !24
  %31 = load %struct._IO_FILE*, %struct._IO_FILE** %srcFile.addr, align 4, !tbaa !2
  %call21 = call i32 @fread(i8* %29, i32 1, i32 %30, %struct._IO_FILE* %31)
  store i32 %call21, i32* %readSize, align 4, !tbaa !24
  %32 = load i32, i32* %readSize, align 4, !tbaa !24
  %tobool22 = icmp ne i32 %32, 0
  br i1 %tobool22, label %if.end24, label %if.then23

if.then23:                                        ; preds = %if.end19
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup84

if.end24:                                         ; preds = %if.end19
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %if.end24
  %33 = load i32, i32* %pos, align 4, !tbaa !24
  %34 = load i32, i32* %readSize, align 4, !tbaa !24
  %cmp25 = icmp ult i32 %33, %34
  br i1 %cmp25, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %while.cond
  %35 = load i32, i32* %decodedBytes, align 4, !tbaa !24
  %dstBufferSize26 = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 3
  %36 = load i32, i32* %dstBufferSize26, align 4, !tbaa !51
  %cmp27 = icmp eq i32 %35, %36
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %while.cond
  %37 = phi i1 [ true, %while.cond ], [ %cmp27, %lor.rhs ]
  br i1 %37, label %while.body, label %while.end

while.body:                                       ; preds = %lor.end
  %38 = bitcast i32* %remaining to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #5
  %39 = load i32, i32* %readSize, align 4, !tbaa !24
  %40 = load i32, i32* %pos, align 4, !tbaa !24
  %sub = sub i32 %39, %40
  store i32 %sub, i32* %remaining, align 4, !tbaa !24
  %dstBufferSize28 = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 3
  %41 = load i32, i32* %dstBufferSize28, align 4, !tbaa !51
  store i32 %41, i32* %decodedBytes, align 4, !tbaa !24
  %dCtx29 = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 5
  %42 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dCtx29, align 4, !tbaa !54
  %dstBuffer30 = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 2
  %43 = load i8*, i8** %dstBuffer30, align 4, !tbaa !52
  %srcBuffer31 = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 0
  %44 = load i8*, i8** %srcBuffer31, align 4, !tbaa !50
  %45 = load i32, i32* %pos, align 4, !tbaa !24
  %add.ptr = getelementptr inbounds i8, i8* %44, i32 %45
  %dictBuffer32 = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 6
  %46 = load i8*, i8** %dictBuffer32, align 4, !tbaa !55
  %dictBufferSize33 = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 7
  %47 = load i32, i32* %dictBufferSize33, align 4, !tbaa !87
  %call34 = call i32 @LZ4F_decompress_usingDict(%struct.LZ4F_dctx_s* %42, i8* %43, i32* %decodedBytes, i8* %add.ptr, i32* %remaining, i8* %46, i32 %47, %struct.LZ4F_decompressOptions_t* null)
  store i32 %call34, i32* %nextToLoad, align 4, !tbaa !24
  %48 = load i32, i32* %nextToLoad, align 4, !tbaa !24
  %call35 = call i32 @LZ4F_isError(i32 %48)
  %tobool36 = icmp ne i32 %call35, 0
  br i1 %tobool36, label %if.then37, label %if.end51

if.then37:                                        ; preds = %while.body
  %49 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp38 = icmp sge i32 %49, 1
  br i1 %cmp38, label %if.then39, label %if.end41

if.then39:                                        ; preds = %if.then37
  %50 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call40 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %50, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 66)
  br label %if.end41

if.end41:                                         ; preds = %if.then39, %if.then37
  %51 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp42 = icmp sge i32 %51, 1
  br i1 %cmp42, label %if.then43, label %if.end46

if.then43:                                        ; preds = %if.end41
  %52 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %53 = load i32, i32* %nextToLoad, align 4, !tbaa !24
  %call44 = call i8* @LZ4F_getErrorName(i32 %53)
  %call45 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %52, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.76, i32 0, i32 0), i8* %call44)
  br label %if.end46

if.end46:                                         ; preds = %if.then43, %if.end41
  %54 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp47 = icmp sge i32 %54, 1
  br i1 %cmp47, label %if.then48, label %if.end50

if.then48:                                        ; preds = %if.end46
  %55 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call49 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %55, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end50

if.end50:                                         ; preds = %if.then48, %if.end46
  call void @exit(i32 66) #6
  unreachable

if.end51:                                         ; preds = %while.body
  %56 = load i32, i32* %remaining, align 4, !tbaa !24
  %57 = load i32, i32* %pos, align 4, !tbaa !24
  %add = add i32 %57, %56
  store i32 %add, i32* %pos, align 4, !tbaa !24
  %58 = load i32, i32* %decodedBytes, align 4, !tbaa !24
  %tobool52 = icmp ne i32 %58, 0
  br i1 %tobool52, label %if.then53, label %if.end80

if.then53:                                        ; preds = %if.end51
  %59 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %testMode = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %59, i32 0, i32 2
  %60 = load i32, i32* %testMode, align 4, !tbaa !12
  %tobool54 = icmp ne i32 %60, 0
  br i1 %tobool54, label %if.end58, label %if.then55

if.then55:                                        ; preds = %if.then53
  %61 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %62 = load %struct._IO_FILE*, %struct._IO_FILE** %dstFile.addr, align 4, !tbaa !2
  %dstBuffer56 = getelementptr inbounds %struct.dRess_t, %struct.dRess_t* %ress, i32 0, i32 2
  %63 = load i8*, i8** %dstBuffer56, align 4, !tbaa !52
  %64 = load i32, i32* %decodedBytes, align 4, !tbaa !24
  %65 = load i32, i32* %storedSkips, align 4, !tbaa !6
  %call57 = call i32 @LZ4IO_fwriteSparse(%struct.LZ4IO_prefs_s* %61, %struct._IO_FILE* %62, i8* %63, i32 %64, i32 %65)
  store i32 %call57, i32* %storedSkips, align 4, !tbaa !6
  br label %if.end58

if.end58:                                         ; preds = %if.then55, %if.then53
  %66 = load i32, i32* %decodedBytes, align 4, !tbaa !24
  %conv = zext i32 %66 to i64
  %67 = load i64, i64* %filesize, align 8, !tbaa !26
  %add59 = add i64 %67, %conv
  store i64 %add59, i64* %filesize, align 8, !tbaa !26
  %68 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp60 = icmp sge i32 %68, 2
  br i1 %cmp60, label %if.then62, label %if.end79

if.then62:                                        ; preds = %if.end58
  %call63 = call i32 @clock()
  %69 = load i32, i32* @g_time, align 4, !tbaa !24
  %sub64 = sub nsw i32 %call63, %69
  %cmp65 = icmp sgt i32 %sub64, 166666
  br i1 %cmp65, label %if.then69, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then62
  %70 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp67 = icmp sge i32 %70, 4
  br i1 %cmp67, label %if.then69, label %if.end78

if.then69:                                        ; preds = %lor.lhs.false, %if.then62
  %call70 = call i32 @clock()
  store i32 %call70, i32* @g_time, align 4, !tbaa !24
  %71 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %72 = load i64, i64* %filesize, align 8, !tbaa !26
  %shr = lshr i64 %72, 20
  %conv71 = trunc i64 %shr to i32
  %call72 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %71, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.77, i32 0, i32 0), i32 %conv71)
  %73 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp73 = icmp sge i32 %73, 4
  br i1 %cmp73, label %if.then75, label %if.end77

if.then75:                                        ; preds = %if.then69
  %74 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call76 = call i32 @fflush(%struct._IO_FILE* %74)
  br label %if.end77

if.end77:                                         ; preds = %if.then75, %if.then69
  br label %if.end78

if.end78:                                         ; preds = %if.end77, %lor.lhs.false
  br label %if.end79

if.end79:                                         ; preds = %if.end78, %if.end58
  br label %if.end80

if.end80:                                         ; preds = %if.end79, %if.end51
  %75 = load i32, i32* %nextToLoad, align 4, !tbaa !24
  %tobool81 = icmp ne i32 %75, 0
  br i1 %tobool81, label %if.end83, label %if.then82

if.then82:                                        ; preds = %if.end80
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end83:                                         ; preds = %if.end80
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end83, %if.then82
  %76 = bitcast i32* %remaining to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 5, label %while.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %cleanup, %lor.end
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup84

cleanup84:                                        ; preds = %while.end, %if.then23
  %77 = bitcast i32* %decodedBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #5
  %78 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #5
  %79 = bitcast i32* %readSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #5
  %cleanup.dest87 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest87, label %unreachable [
    i32 0, label %cleanup.cont88
    i32 2, label %for.end
  ]

cleanup.cont88:                                   ; preds = %cleanup84
  br label %for.cond

for.end:                                          ; preds = %cleanup84, %for.cond
  %80 = load %struct._IO_FILE*, %struct._IO_FILE** %srcFile.addr, align 4, !tbaa !2
  %call89 = call i32 @ferror(%struct._IO_FILE* %80)
  %tobool90 = icmp ne i32 %call89, 0
  br i1 %tobool90, label %if.then91, label %if.end107

if.then91:                                        ; preds = %for.end
  %81 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp92 = icmp sge i32 %81, 1
  br i1 %cmp92, label %if.then94, label %if.end96

if.then94:                                        ; preds = %if.then91
  %82 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call95 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %82, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 67)
  br label %if.end96

if.end96:                                         ; preds = %if.then94, %if.then91
  %83 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp97 = icmp sge i32 %83, 1
  br i1 %cmp97, label %if.then99, label %if.end101

if.then99:                                        ; preds = %if.end96
  %84 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call100 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %84, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.78, i32 0, i32 0))
  br label %if.end101

if.end101:                                        ; preds = %if.then99, %if.end96
  %85 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp102 = icmp sge i32 %85, 1
  br i1 %cmp102, label %if.then104, label %if.end106

if.then104:                                       ; preds = %if.end101
  %86 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call105 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %86, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end106

if.end106:                                        ; preds = %if.then104, %if.end101
  call void @exit(i32 67) #6
  unreachable

if.end107:                                        ; preds = %for.end
  %87 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %testMode108 = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %87, i32 0, i32 2
  %88 = load i32, i32* %testMode108, align 4, !tbaa !12
  %tobool109 = icmp ne i32 %88, 0
  br i1 %tobool109, label %if.end111, label %if.then110

if.then110:                                       ; preds = %if.end107
  %89 = load %struct._IO_FILE*, %struct._IO_FILE** %dstFile.addr, align 4, !tbaa !2
  %90 = load i32, i32* %storedSkips, align 4, !tbaa !6
  call void @LZ4IO_fwriteSparseEnd(%struct._IO_FILE* %89, i32 %90)
  br label %if.end111

if.end111:                                        ; preds = %if.then110, %if.end107
  %91 = load i32, i32* %nextToLoad, align 4, !tbaa !24
  %cmp112 = icmp ne i32 %91, 0
  br i1 %cmp112, label %if.then114, label %if.end130

if.then114:                                       ; preds = %if.end111
  %92 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp115 = icmp sge i32 %92, 1
  br i1 %cmp115, label %if.then117, label %if.end119

if.then117:                                       ; preds = %if.then114
  %93 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call118 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %93, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 68)
  br label %if.end119

if.end119:                                        ; preds = %if.then117, %if.then114
  %94 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp120 = icmp sge i32 %94, 1
  br i1 %cmp120, label %if.then122, label %if.end124

if.then122:                                       ; preds = %if.end119
  %95 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call123 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %95, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.79, i32 0, i32 0))
  br label %if.end124

if.end124:                                        ; preds = %if.then122, %if.end119
  %96 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp125 = icmp sge i32 %96, 1
  br i1 %cmp125, label %if.then127, label %if.end129

if.then127:                                       ; preds = %if.end124
  %97 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call128 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %97, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end129

if.end129:                                        ; preds = %if.then127, %if.end124
  call void @exit(i32 68) #6
  unreachable

if.end130:                                        ; preds = %if.end111
  %98 = load i64, i64* %filesize, align 8, !tbaa !26
  store i32 1, i32* %cleanup.dest.slot, align 4
  %99 = bitcast i32* %storedSkips to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #5
  %100 = bitcast i32* %nextToLoad to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #5
  %101 = bitcast i64* %filesize to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %101) #5
  ret i64 %98

unreachable:                                      ; preds = %cleanup84, %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal i64 @LZ4IO_decodeLegacyStream(%struct.LZ4IO_prefs_s* %prefs, %struct._IO_FILE* %finput, %struct._IO_FILE* %foutput) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %finput.addr = alloca %struct._IO_FILE*, align 4
  %foutput.addr = alloca %struct._IO_FILE*, align 4
  %streamSize = alloca i64, align 8
  %storedSkips = alloca i32, align 4
  %in_buff = alloca i8*, align 4
  %out_buff = alloca i8*, align 4
  %blockSize = alloca i32, align 4
  %sizeCheck = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %sizeCheck38 = alloca i32, align 4
  %decodeSize = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store %struct._IO_FILE* %finput, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  store %struct._IO_FILE* %foutput, %struct._IO_FILE** %foutput.addr, align 4, !tbaa !2
  %0 = bitcast i64* %streamSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #5
  store i64 0, i64* %streamSize, align 8, !tbaa !26
  %1 = bitcast i32* %storedSkips to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 0, i32* %storedSkips, align 4, !tbaa !6
  %2 = bitcast i8** %in_buff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %call = call i32 @LZ4_compressBound(i32 8388608)
  %call1 = call i8* @malloc(i32 %call)
  store i8* %call1, i8** %in_buff, align 4, !tbaa !2
  %3 = bitcast i8** %out_buff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %call2 = call i8* @malloc(i32 8388608)
  store i8* %call2, i8** %out_buff, align 4, !tbaa !2
  %4 = load i8*, i8** %in_buff, align 4, !tbaa !2
  %tobool = icmp ne i8* %4, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %5 = load i8*, i8** %out_buff, align 4, !tbaa !2
  %tobool3 = icmp ne i8* %5, null
  br i1 %tobool3, label %if.end14, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  %6 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp = icmp sge i32 %6, 1
  br i1 %cmp, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.then
  %7 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call5 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %7, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 51)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.then
  %8 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp6 = icmp sge i32 %8, 1
  br i1 %cmp6, label %if.then7, label %if.end9

if.then7:                                         ; preds = %if.end
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call8 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %9, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.1, i32 0, i32 0))
  br label %if.end9

if.end9:                                          ; preds = %if.then7, %if.end
  %10 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp10 = icmp sge i32 %10, 1
  br i1 %cmp10, label %if.then11, label %if.end13

if.then11:                                        ; preds = %if.end9
  %11 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call12 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %11, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end13

if.end13:                                         ; preds = %if.then11, %if.end9
  call void @exit(i32 51) #6
  unreachable

if.end14:                                         ; preds = %lor.lhs.false
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont74, %if.end14
  br label %while.body

while.body:                                       ; preds = %while.cond
  %12 = bitcast i32* %blockSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = bitcast i32* %sizeCheck to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  %14 = load i8*, i8** %in_buff, align 4, !tbaa !2
  %15 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %call15 = call i32 @fread(i8* %14, i32 1, i32 4, %struct._IO_FILE* %15)
  store i32 %call15, i32* %sizeCheck, align 4, !tbaa !24
  %16 = load i32, i32* %sizeCheck, align 4, !tbaa !24
  %cmp16 = icmp eq i32 %16, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %while.body
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end18:                                         ; preds = %while.body
  %17 = load i32, i32* %sizeCheck, align 4, !tbaa !24
  %cmp19 = icmp ne i32 %17, 4
  br i1 %cmp19, label %if.then20, label %if.end33

if.then20:                                        ; preds = %if.end18
  %18 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp21 = icmp sge i32 %18, 1
  br i1 %cmp21, label %if.then22, label %if.end24

if.then22:                                        ; preds = %if.then20
  %19 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call23 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %19, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 52)
  br label %if.end24

if.end24:                                         ; preds = %if.then22, %if.then20
  %20 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp25 = icmp sge i32 %20, 1
  br i1 %cmp25, label %if.then26, label %if.end28

if.then26:                                        ; preds = %if.end24
  %21 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call27 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %21, i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.87, i32 0, i32 0))
  br label %if.end28

if.end28:                                         ; preds = %if.then26, %if.end24
  %22 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp29 = icmp sge i32 %22, 1
  br i1 %cmp29, label %if.then30, label %if.end32

if.then30:                                        ; preds = %if.end28
  %23 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call31 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %23, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end32

if.end32:                                         ; preds = %if.then30, %if.end28
  call void @exit(i32 52) #6
  unreachable

if.end33:                                         ; preds = %if.end18
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end33, %if.then17
  %24 = bitcast i32* %sizeCheck to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup72 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  %25 = load i8*, i8** %in_buff, align 4, !tbaa !2
  %call34 = call i32 @LZ4IO_readLE32(i8* %25)
  store i32 %call34, i32* %blockSize, align 4, !tbaa !6
  %26 = load i32, i32* %blockSize, align 4, !tbaa !6
  %cmp35 = icmp ugt i32 %26, 8421520
  br i1 %cmp35, label %if.then36, label %if.end37

if.then36:                                        ; preds = %cleanup.cont
  %27 = load i32, i32* %blockSize, align 4, !tbaa !6
  store i32 %27, i32* @g_magicRead, align 4, !tbaa !6
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup72

if.end37:                                         ; preds = %cleanup.cont
  %28 = bitcast i32* %sizeCheck38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #5
  %29 = load i8*, i8** %in_buff, align 4, !tbaa !2
  %30 = load i32, i32* %blockSize, align 4, !tbaa !6
  %31 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %call39 = call i32 @fread(i8* %29, i32 1, i32 %30, %struct._IO_FILE* %31)
  store i32 %call39, i32* %sizeCheck38, align 4, !tbaa !24
  %32 = load i32, i32* %sizeCheck38, align 4, !tbaa !24
  %33 = load i32, i32* %blockSize, align 4, !tbaa !6
  %cmp40 = icmp ne i32 %32, %33
  br i1 %cmp40, label %if.then41, label %if.end54

if.then41:                                        ; preds = %if.end37
  %34 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp42 = icmp sge i32 %34, 1
  br i1 %cmp42, label %if.then43, label %if.end45

if.then43:                                        ; preds = %if.then41
  %35 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call44 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %35, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 52)
  br label %if.end45

if.end45:                                         ; preds = %if.then43, %if.then41
  %36 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp46 = icmp sge i32 %36, 1
  br i1 %cmp46, label %if.then47, label %if.end49

if.then47:                                        ; preds = %if.end45
  %37 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call48 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %37, i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.88, i32 0, i32 0))
  br label %if.end49

if.end49:                                         ; preds = %if.then47, %if.end45
  %38 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp50 = icmp sge i32 %38, 1
  br i1 %cmp50, label %if.then51, label %if.end53

if.then51:                                        ; preds = %if.end49
  %39 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call52 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %39, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end53

if.end53:                                         ; preds = %if.then51, %if.end49
  call void @exit(i32 52) #6
  unreachable

if.end54:                                         ; preds = %if.end37
  %40 = bitcast i32* %sizeCheck38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #5
  %41 = bitcast i32* %decodeSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #5
  %42 = load i8*, i8** %in_buff, align 4, !tbaa !2
  %43 = load i8*, i8** %out_buff, align 4, !tbaa !2
  %44 = load i32, i32* %blockSize, align 4, !tbaa !6
  %call55 = call i32 @LZ4_decompress_safe(i8* %42, i8* %43, i32 %44, i32 8388608)
  store i32 %call55, i32* %decodeSize, align 4, !tbaa !6
  %45 = load i32, i32* %decodeSize, align 4, !tbaa !6
  %cmp56 = icmp slt i32 %45, 0
  br i1 %cmp56, label %if.then57, label %if.end70

if.then57:                                        ; preds = %if.end54
  %46 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp58 = icmp sge i32 %46, 1
  br i1 %cmp58, label %if.then59, label %if.end61

if.then59:                                        ; preds = %if.then57
  %47 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call60 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %47, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 53)
  br label %if.end61

if.end61:                                         ; preds = %if.then59, %if.then57
  %48 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp62 = icmp sge i32 %48, 1
  br i1 %cmp62, label %if.then63, label %if.end65

if.then63:                                        ; preds = %if.end61
  %49 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call64 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %49, i8* getelementptr inbounds ([45 x i8], [45 x i8]* @.str.89, i32 0, i32 0))
  br label %if.end65

if.end65:                                         ; preds = %if.then63, %if.end61
  %50 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp66 = icmp sge i32 %50, 1
  br i1 %cmp66, label %if.then67, label %if.end69

if.then67:                                        ; preds = %if.end65
  %51 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call68 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %51, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end69

if.end69:                                         ; preds = %if.then67, %if.end65
  call void @exit(i32 53) #6
  unreachable

if.end70:                                         ; preds = %if.end54
  %52 = load i32, i32* %decodeSize, align 4, !tbaa !6
  %conv = sext i32 %52 to i64
  %53 = load i64, i64* %streamSize, align 8, !tbaa !26
  %add = add i64 %53, %conv
  store i64 %add, i64* %streamSize, align 8, !tbaa !26
  %54 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %55 = load %struct._IO_FILE*, %struct._IO_FILE** %foutput.addr, align 4, !tbaa !2
  %56 = load i8*, i8** %out_buff, align 4, !tbaa !2
  %57 = load i32, i32* %decodeSize, align 4, !tbaa !6
  %58 = load i32, i32* %storedSkips, align 4, !tbaa !6
  %call71 = call i32 @LZ4IO_fwriteSparse(%struct.LZ4IO_prefs_s* %54, %struct._IO_FILE* %55, i8* %56, i32 %57, i32 %58)
  store i32 %call71, i32* %storedSkips, align 4, !tbaa !6
  %59 = bitcast i32* %decodeSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #5
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup72

cleanup72:                                        ; preds = %if.end70, %if.then36, %cleanup
  %60 = bitcast i32* %blockSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #5
  %cleanup.dest73 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest73, label %unreachable [
    i32 0, label %cleanup.cont74
    i32 3, label %while.end
  ]

cleanup.cont74:                                   ; preds = %cleanup72
  br label %while.cond

while.end:                                        ; preds = %cleanup72
  %61 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %call75 = call i32 @ferror(%struct._IO_FILE* %61)
  %tobool76 = icmp ne i32 %call75, 0
  br i1 %tobool76, label %if.then77, label %if.end93

if.then77:                                        ; preds = %while.end
  %62 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp78 = icmp sge i32 %62, 1
  br i1 %cmp78, label %if.then80, label %if.end82

if.then80:                                        ; preds = %if.then77
  %63 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call81 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %63, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 54)
  br label %if.end82

if.end82:                                         ; preds = %if.then80, %if.then77
  %64 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp83 = icmp sge i32 %64, 1
  br i1 %cmp83, label %if.then85, label %if.end87

if.then85:                                        ; preds = %if.end82
  %65 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call86 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %65, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.90, i32 0, i32 0))
  br label %if.end87

if.end87:                                         ; preds = %if.then85, %if.end82
  %66 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp88 = icmp sge i32 %66, 1
  br i1 %cmp88, label %if.then90, label %if.end92

if.then90:                                        ; preds = %if.end87
  %67 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call91 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %67, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end92

if.end92:                                         ; preds = %if.then90, %if.end87
  call void @exit(i32 54) #6
  unreachable

if.end93:                                         ; preds = %while.end
  %68 = load %struct._IO_FILE*, %struct._IO_FILE** %foutput.addr, align 4, !tbaa !2
  %69 = load i32, i32* %storedSkips, align 4, !tbaa !6
  call void @LZ4IO_fwriteSparseEnd(%struct._IO_FILE* %68, i32 %69)
  %70 = load i8*, i8** %in_buff, align 4, !tbaa !2
  call void @free(i8* %70)
  %71 = load i8*, i8** %out_buff, align 4, !tbaa !2
  call void @free(i8* %71)
  %72 = load i64, i64* %streamSize, align 8, !tbaa !26
  store i32 1, i32* %cleanup.dest.slot, align 4
  %73 = bitcast i8** %out_buff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #5
  %74 = bitcast i8** %in_buff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #5
  %75 = bitcast i32* %storedSkips to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #5
  %76 = bitcast i64* %streamSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %76) #5
  ret i64 %72

unreachable:                                      ; preds = %cleanup72
  unreachable
}

; Function Attrs: nounwind
define internal i32 @fseek_u32(%struct._IO_FILE* %fp, i32 %offset, i32 %where) #0 {
entry:
  %retval = alloca i32, align 4
  %fp.addr = alloca %struct._IO_FILE*, align 4
  %offset.addr = alloca i32, align 4
  %where.addr = alloca i32, align 4
  %stepMax = alloca i32, align 4
  %errorNb = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %s = alloca i32, align 4
  store %struct._IO_FILE* %fp, %struct._IO_FILE** %fp.addr, align 4, !tbaa !2
  store i32 %offset, i32* %offset.addr, align 4, !tbaa !6
  store i32 %where, i32* %where.addr, align 4, !tbaa !6
  %0 = bitcast i32* %stepMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 1073741824, i32* %stepMax, align 4, !tbaa !6
  %1 = bitcast i32* %errorNb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 0, i32* %errorNb, align 4, !tbaa !6
  %2 = load i32, i32* %where.addr, align 4, !tbaa !6
  %cmp = icmp ne i32 %2, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup9

if.end:                                           ; preds = %entry
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %if.end
  %3 = load i32, i32* %offset.addr, align 4, !tbaa !6
  %cmp1 = icmp ugt i32 %3, 0
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i32, i32* %offset.addr, align 4, !tbaa !6
  store i32 %5, i32* %s, align 4, !tbaa !6
  %6 = load i32, i32* %s, align 4, !tbaa !6
  %cmp2 = icmp ugt i32 %6, 1073741824
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %while.body
  store i32 1073741824, i32* %s, align 4, !tbaa !6
  br label %if.end4

if.end4:                                          ; preds = %if.then3, %while.body
  %7 = load %struct._IO_FILE*, %struct._IO_FILE** %fp.addr, align 4, !tbaa !2
  %8 = load i32, i32* %s, align 4, !tbaa !6
  %conv = sext i32 %8 to i64
  %call = call i32 @fseeko(%struct._IO_FILE* %7, i64 %conv, i32 1)
  store i32 %call, i32* %errorNb, align 4, !tbaa !6
  %9 = load i32, i32* %errorNb, align 4, !tbaa !6
  %cmp5 = icmp ne i32 %9, 0
  br i1 %cmp5, label %if.then7, label %if.end8

if.then7:                                         ; preds = %if.end4
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end4
  %10 = load i32, i32* %s, align 4, !tbaa !6
  %11 = load i32, i32* %offset.addr, align 4, !tbaa !6
  %sub = sub i32 %11, %10
  store i32 %sub, i32* %offset.addr, align 4, !tbaa !6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end8, %if.then7
  %12 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 3, label %while.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %cleanup, %while.cond
  %13 = load i32, i32* %errorNb, align 4, !tbaa !6
  store i32 %13, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup9

cleanup9:                                         ; preds = %while.end, %if.then
  %14 = bitcast i32* %errorNb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #5
  %15 = bitcast i32* %stepMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  %16 = load i32, i32* %retval, align 4
  ret i32 %16

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal i64 @LZ4IO_passThrough(%struct.LZ4IO_prefs_s* %prefs, %struct._IO_FILE* %finput, %struct._IO_FILE* %foutput, i8* %MNstore) #0 {
entry:
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %finput.addr = alloca %struct._IO_FILE*, align 4
  %foutput.addr = alloca %struct._IO_FILE*, align 4
  %MNstore.addr = alloca i8*, align 4
  %buffer = alloca [16384 x i32], align 16
  %readBytes = alloca i32, align 4
  %total = alloca i64, align 8
  %storedSkips = alloca i32, align 4
  %sizeCheck = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store %struct._IO_FILE* %finput, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  store %struct._IO_FILE* %foutput, %struct._IO_FILE** %foutput.addr, align 4, !tbaa !2
  store i8* %MNstore, i8** %MNstore.addr, align 4, !tbaa !2
  %0 = bitcast [16384 x i32]* %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 65536, i8* %0) #5
  %1 = bitcast i32* %readBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 1, i32* %readBytes, align 4, !tbaa !24
  %2 = bitcast i64* %total to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %2) #5
  store i64 4, i64* %total, align 8, !tbaa !26
  %3 = bitcast i32* %storedSkips to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  store i32 0, i32* %storedSkips, align 4, !tbaa !6
  %4 = bitcast i32* %sizeCheck to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i8*, i8** %MNstore.addr, align 4, !tbaa !2
  %6 = load %struct._IO_FILE*, %struct._IO_FILE** %foutput.addr, align 4, !tbaa !2
  %call = call i32 @fwrite(i8* %5, i32 1, i32 4, %struct._IO_FILE* %6)
  store i32 %call, i32* %sizeCheck, align 4, !tbaa !24
  %7 = load i32, i32* %sizeCheck, align 4, !tbaa !24
  %cmp = icmp ne i32 %7, 4
  br i1 %cmp, label %if.then, label %if.end12

if.then:                                          ; preds = %entry
  %8 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp1 = icmp sge i32 %8, 1
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call3 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %9, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 50)
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  %10 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp4 = icmp sge i32 %10, 1
  br i1 %cmp4, label %if.then5, label %if.end7

if.then5:                                         ; preds = %if.end
  %11 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call6 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %11, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.91, i32 0, i32 0))
  br label %if.end7

if.end7:                                          ; preds = %if.then5, %if.end
  %12 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp8 = icmp sge i32 %12, 1
  br i1 %cmp8, label %if.then9, label %if.end11

if.then9:                                         ; preds = %if.end7
  %13 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call10 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %13, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end11

if.end11:                                         ; preds = %if.then9, %if.end7
  call void @exit(i32 50) #6
  unreachable

if.end12:                                         ; preds = %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end12
  %14 = load i32, i32* %readBytes, align 4, !tbaa !24
  %tobool = icmp ne i32 %14, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %arraydecay = getelementptr inbounds [16384 x i32], [16384 x i32]* %buffer, i32 0, i32 0
  %15 = bitcast i32* %arraydecay to i8*
  %16 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %call13 = call i32 @fread(i8* %15, i32 1, i32 65536, %struct._IO_FILE* %16)
  store i32 %call13, i32* %readBytes, align 4, !tbaa !24
  %17 = load i32, i32* %readBytes, align 4, !tbaa !24
  %conv = zext i32 %17 to i64
  %18 = load i64, i64* %total, align 8, !tbaa !26
  %add = add i64 %18, %conv
  store i64 %add, i64* %total, align 8, !tbaa !26
  %19 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %20 = load %struct._IO_FILE*, %struct._IO_FILE** %foutput.addr, align 4, !tbaa !2
  %arraydecay14 = getelementptr inbounds [16384 x i32], [16384 x i32]* %buffer, i32 0, i32 0
  %21 = bitcast i32* %arraydecay14 to i8*
  %22 = load i32, i32* %readBytes, align 4, !tbaa !24
  %23 = load i32, i32* %storedSkips, align 4, !tbaa !6
  %call15 = call i32 @LZ4IO_fwriteSparse(%struct.LZ4IO_prefs_s* %19, %struct._IO_FILE* %20, i8* %21, i32 %22, i32 %23)
  store i32 %call15, i32* %storedSkips, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %24 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %call16 = call i32 @ferror(%struct._IO_FILE* %24)
  %tobool17 = icmp ne i32 %call16, 0
  br i1 %tobool17, label %if.then18, label %if.end34

if.then18:                                        ; preds = %while.end
  %25 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp19 = icmp sge i32 %25, 1
  br i1 %cmp19, label %if.then21, label %if.end23

if.then21:                                        ; preds = %if.then18
  %26 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call22 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %26, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 51)
  br label %if.end23

if.end23:                                         ; preds = %if.then21, %if.then18
  %27 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp24 = icmp sge i32 %27, 1
  br i1 %cmp24, label %if.then26, label %if.end28

if.then26:                                        ; preds = %if.end23
  %28 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call27 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %28, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.92, i32 0, i32 0))
  br label %if.end28

if.end28:                                         ; preds = %if.then26, %if.end23
  %29 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp29 = icmp sge i32 %29, 1
  br i1 %cmp29, label %if.then31, label %if.end33

if.then31:                                        ; preds = %if.end28
  %30 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call32 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %30, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end33

if.end33:                                         ; preds = %if.then31, %if.end28
  call void @exit(i32 51) #6
  unreachable

if.end34:                                         ; preds = %while.end
  %31 = load %struct._IO_FILE*, %struct._IO_FILE** %foutput.addr, align 4, !tbaa !2
  %32 = load i32, i32* %storedSkips, align 4, !tbaa !6
  call void @LZ4IO_fwriteSparseEnd(%struct._IO_FILE* %31, i32 %32)
  %33 = load i64, i64* %total, align 8, !tbaa !26
  %34 = bitcast i32* %sizeCheck to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #5
  %35 = bitcast i32* %storedSkips to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #5
  %36 = bitcast i64* %total to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %36) #5
  %37 = bitcast i32* %readBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #5
  %38 = bitcast [16384 x i32]* %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 65536, i8* %38) #5
  ret i64 %33
}

declare i32 @ftell(%struct._IO_FILE*) #2

declare i32 @LZ4F_decompress_usingDict(%struct.LZ4F_dctx_s*, i8*, i32*, i8*, i32*, i8*, i32, %struct.LZ4F_decompressOptions_t*) #2

; Function Attrs: nounwind
define internal i32 @LZ4IO_fwriteSparse(%struct.LZ4IO_prefs_s* %prefs, %struct._IO_FILE* %file, i8* %buffer, i32 %bufferSize, i32 %storedSkips) #0 {
entry:
  %retval = alloca i32, align 4
  %prefs.addr = alloca %struct.LZ4IO_prefs_s*, align 4
  %file.addr = alloca %struct._IO_FILE*, align 4
  %buffer.addr = alloca i8*, align 4
  %bufferSize.addr = alloca i32, align 4
  %storedSkips.addr = alloca i32, align 4
  %sizeT = alloca i32, align 4
  %maskT = alloca i32, align 4
  %bufferT = alloca i32*, align 4
  %ptrT = alloca i32*, align 4
  %bufferSizeT = alloca i32, align 4
  %bufferTEnd = alloca i32*, align 4
  %segmentSizeT = alloca i32, align 4
  %sizeCheck = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %seekResult = alloca i32, align 4
  %seg0SizeT = alloca i32, align 4
  %nb0T = alloca i32, align 4
  %seekResult44 = alloca i32, align 4
  %sizeCheck69 = alloca i32, align 4
  %restStart = alloca i8*, align 4
  %restPtr = alloca i8*, align 4
  %restSize = alloca i32, align 4
  %restEnd = alloca i8*, align 4
  %seekResult111 = alloca i32, align 4
  %sizeCheck132 = alloca i32, align 4
  store %struct.LZ4IO_prefs_s* %prefs, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  store %struct._IO_FILE* %file, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  store i8* %buffer, i8** %buffer.addr, align 4, !tbaa !2
  store i32 %bufferSize, i32* %bufferSize.addr, align 4, !tbaa !24
  store i32 %storedSkips, i32* %storedSkips.addr, align 4, !tbaa !6
  %0 = bitcast i32* %sizeT to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 4, i32* %sizeT, align 4, !tbaa !24
  %1 = bitcast i32* %maskT to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 3, i32* %maskT, align 4, !tbaa !24
  %2 = bitcast i32** %bufferT to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %4 = bitcast i8* %3 to i32*
  store i32* %4, i32** %bufferT, align 4, !tbaa !2
  %5 = bitcast i32** %ptrT to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load i32*, i32** %bufferT, align 4, !tbaa !2
  store i32* %6, i32** %ptrT, align 4, !tbaa !2
  %7 = bitcast i32* %bufferSizeT to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load i32, i32* %bufferSize.addr, align 4, !tbaa !24
  %div = udiv i32 %8, 4
  store i32 %div, i32* %bufferSizeT, align 4, !tbaa !24
  %9 = bitcast i32** %bufferTEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load i32*, i32** %bufferT, align 4, !tbaa !2
  %11 = load i32, i32* %bufferSizeT, align 4, !tbaa !24
  %add.ptr = getelementptr inbounds i32, i32* %10, i32 %11
  store i32* %add.ptr, i32** %bufferTEnd, align 4, !tbaa !2
  %12 = bitcast i32* %segmentSizeT to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  store i32 8192, i32* %segmentSizeT, align 4, !tbaa !24
  %13 = load %struct.LZ4IO_prefs_s*, %struct.LZ4IO_prefs_s** %prefs.addr, align 4, !tbaa !2
  %sparseFileSupport = getelementptr inbounds %struct.LZ4IO_prefs_s, %struct.LZ4IO_prefs_s* %13, i32 0, i32 8
  %14 = load i32, i32* %sparseFileSupport, align 4, !tbaa !18
  %tobool = icmp ne i32 %14, 0
  br i1 %tobool, label %if.end14, label %if.then

if.then:                                          ; preds = %entry
  %15 = bitcast i32* %sizeCheck to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  %16 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %17 = load i32, i32* %bufferSize.addr, align 4, !tbaa !24
  %18 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call = call i32 @fwrite(i8* %16, i32 1, i32 %17, %struct._IO_FILE* %18)
  store i32 %call, i32* %sizeCheck, align 4, !tbaa !24
  %19 = load i32, i32* %sizeCheck, align 4, !tbaa !24
  %20 = load i32, i32* %bufferSize.addr, align 4, !tbaa !24
  %cmp = icmp ne i32 %19, %20
  br i1 %cmp, label %if.then1, label %if.end13

if.then1:                                         ; preds = %if.then
  %21 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp2 = icmp sge i32 %21, 1
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then1
  %22 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call4 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %22, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 70)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then1
  %23 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp5 = icmp sge i32 %23, 1
  br i1 %cmp5, label %if.then6, label %if.end8

if.then6:                                         ; preds = %if.end
  %24 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call7 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %24, i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.80, i32 0, i32 0))
  br label %if.end8

if.end8:                                          ; preds = %if.then6, %if.end
  %25 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp9 = icmp sge i32 %25, 1
  br i1 %cmp9, label %if.then10, label %if.end12

if.then10:                                        ; preds = %if.end8
  %26 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call11 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %26, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end12

if.end12:                                         ; preds = %if.then10, %if.end8
  call void @exit(i32 70) #6
  unreachable

if.end13:                                         ; preds = %if.then
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %27 = bitcast i32* %sizeCheck to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #5
  br label %cleanup

if.end14:                                         ; preds = %entry
  %28 = load i32, i32* %storedSkips.addr, align 4, !tbaa !6
  %cmp15 = icmp ugt i32 %28, 1073741824
  br i1 %cmp15, label %if.then16, label %if.end33

if.then16:                                        ; preds = %if.end14
  %29 = bitcast i32* %seekResult to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #5
  %30 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call17 = call i32 @fseeko(%struct._IO_FILE* %30, i64 1073741824, i32 1)
  store i32 %call17, i32* %seekResult, align 4, !tbaa !6
  %31 = load i32, i32* %seekResult, align 4, !tbaa !6
  %cmp18 = icmp ne i32 %31, 0
  br i1 %cmp18, label %if.then19, label %if.end32

if.then19:                                        ; preds = %if.then16
  %32 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp20 = icmp sge i32 %32, 1
  br i1 %cmp20, label %if.then21, label %if.end23

if.then21:                                        ; preds = %if.then19
  %33 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call22 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %33, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 71)
  br label %if.end23

if.end23:                                         ; preds = %if.then21, %if.then19
  %34 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp24 = icmp sge i32 %34, 1
  br i1 %cmp24, label %if.then25, label %if.end27

if.then25:                                        ; preds = %if.end23
  %35 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call26 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %35, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.81, i32 0, i32 0))
  br label %if.end27

if.end27:                                         ; preds = %if.then25, %if.end23
  %36 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp28 = icmp sge i32 %36, 1
  br i1 %cmp28, label %if.then29, label %if.end31

if.then29:                                        ; preds = %if.end27
  %37 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call30 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %37, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end31

if.end31:                                         ; preds = %if.then29, %if.end27
  call void @exit(i32 71) #6
  unreachable

if.end32:                                         ; preds = %if.then16
  %38 = load i32, i32* %storedSkips.addr, align 4, !tbaa !6
  %sub = sub i32 %38, 1073741824
  store i32 %sub, i32* %storedSkips.addr, align 4, !tbaa !6
  %39 = bitcast i32* %seekResult to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #5
  br label %if.end33

if.end33:                                         ; preds = %if.end32, %if.end14
  br label %while.cond

while.cond:                                       ; preds = %if.end90, %if.end33
  %40 = load i32*, i32** %ptrT, align 4, !tbaa !2
  %41 = load i32*, i32** %bufferTEnd, align 4, !tbaa !2
  %cmp34 = icmp ult i32* %40, %41
  br i1 %cmp34, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %42 = bitcast i32* %seg0SizeT to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #5
  store i32 8192, i32* %seg0SizeT, align 4, !tbaa !24
  %43 = bitcast i32* %nb0T to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #5
  %44 = load i32, i32* %seg0SizeT, align 4, !tbaa !24
  %45 = load i32, i32* %bufferSizeT, align 4, !tbaa !24
  %cmp35 = icmp ugt i32 %44, %45
  br i1 %cmp35, label %if.then36, label %if.end37

if.then36:                                        ; preds = %while.body
  %46 = load i32, i32* %bufferSizeT, align 4, !tbaa !24
  store i32 %46, i32* %seg0SizeT, align 4, !tbaa !24
  br label %if.end37

if.end37:                                         ; preds = %if.then36, %while.body
  %47 = load i32, i32* %seg0SizeT, align 4, !tbaa !24
  %48 = load i32, i32* %bufferSizeT, align 4, !tbaa !24
  %sub38 = sub i32 %48, %47
  store i32 %sub38, i32* %bufferSizeT, align 4, !tbaa !24
  store i32 0, i32* %nb0T, align 4, !tbaa !24
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end37
  %49 = load i32, i32* %nb0T, align 4, !tbaa !24
  %50 = load i32, i32* %seg0SizeT, align 4, !tbaa !24
  %cmp39 = icmp ult i32 %49, %50
  br i1 %cmp39, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond
  %51 = load i32*, i32** %ptrT, align 4, !tbaa !2
  %52 = load i32, i32* %nb0T, align 4, !tbaa !24
  %arrayidx = getelementptr inbounds i32, i32* %51, i32 %52
  %53 = load i32, i32* %arrayidx, align 4, !tbaa !24
  %cmp40 = icmp eq i32 %53, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond
  %54 = phi i1 [ false, %for.cond ], [ %cmp40, %land.rhs ]
  br i1 %54, label %for.body, label %for.end

for.body:                                         ; preds = %land.end
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %55 = load i32, i32* %nb0T, align 4, !tbaa !24
  %inc = add i32 %55, 1
  store i32 %inc, i32* %nb0T, align 4, !tbaa !24
  br label %for.cond

for.end:                                          ; preds = %land.end
  %56 = load i32, i32* %nb0T, align 4, !tbaa !24
  %mul = mul i32 %56, 4
  %57 = load i32, i32* %storedSkips.addr, align 4, !tbaa !6
  %add = add i32 %57, %mul
  store i32 %add, i32* %storedSkips.addr, align 4, !tbaa !6
  %58 = load i32, i32* %nb0T, align 4, !tbaa !24
  %59 = load i32, i32* %seg0SizeT, align 4, !tbaa !24
  %cmp41 = icmp ne i32 %58, %59
  br i1 %cmp41, label %if.then42, label %if.end90

if.then42:                                        ; preds = %for.end
  %call43 = call i32* @__errno_location()
  store i32 0, i32* %call43, align 4, !tbaa !6
  %60 = bitcast i32* %seekResult44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #5
  %61 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %62 = load i32, i32* %storedSkips.addr, align 4, !tbaa !6
  %conv = zext i32 %62 to i64
  %call45 = call i32 @fseeko(%struct._IO_FILE* %61, i64 %conv, i32 1)
  store i32 %call45, i32* %seekResult44, align 4, !tbaa !6
  %63 = load i32, i32* %seekResult44, align 4, !tbaa !6
  %tobool46 = icmp ne i32 %63, 0
  br i1 %tobool46, label %if.then47, label %if.end66

if.then47:                                        ; preds = %if.then42
  %64 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp48 = icmp sge i32 %64, 1
  br i1 %cmp48, label %if.then50, label %if.end52

if.then50:                                        ; preds = %if.then47
  %65 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call51 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %65, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 72)
  br label %if.end52

if.end52:                                         ; preds = %if.then50, %if.then47
  %66 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp53 = icmp sge i32 %66, 1
  br i1 %cmp53, label %if.then55, label %if.end60

if.then55:                                        ; preds = %if.end52
  %67 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call56 = call i32* @__errno_location()
  %68 = load i32, i32* %call56, align 4, !tbaa !6
  %call57 = call i32* @__errno_location()
  %69 = load i32, i32* %call57, align 4, !tbaa !6
  %call58 = call i8* @strerror(i32 %69)
  %call59 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %67, i8* getelementptr inbounds ([44 x i8], [44 x i8]* @.str.82, i32 0, i32 0), i32 %68, i8* %call58)
  br label %if.end60

if.end60:                                         ; preds = %if.then55, %if.end52
  %70 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp61 = icmp sge i32 %70, 1
  br i1 %cmp61, label %if.then63, label %if.end65

if.then63:                                        ; preds = %if.end60
  %71 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call64 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %71, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end65

if.end65:                                         ; preds = %if.then63, %if.end60
  call void @exit(i32 72) #6
  unreachable

if.end66:                                         ; preds = %if.then42
  %72 = bitcast i32* %seekResult44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #5
  store i32 0, i32* %storedSkips.addr, align 4, !tbaa !6
  %73 = load i32, i32* %nb0T, align 4, !tbaa !24
  %74 = load i32, i32* %seg0SizeT, align 4, !tbaa !24
  %sub67 = sub i32 %74, %73
  store i32 %sub67, i32* %seg0SizeT, align 4, !tbaa !24
  %75 = load i32, i32* %nb0T, align 4, !tbaa !24
  %76 = load i32*, i32** %ptrT, align 4, !tbaa !2
  %add.ptr68 = getelementptr inbounds i32, i32* %76, i32 %75
  store i32* %add.ptr68, i32** %ptrT, align 4, !tbaa !2
  %77 = bitcast i32* %sizeCheck69 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #5
  %78 = load i32*, i32** %ptrT, align 4, !tbaa !2
  %79 = bitcast i32* %78 to i8*
  %80 = load i32, i32* %seg0SizeT, align 4, !tbaa !24
  %81 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call70 = call i32 @fwrite(i8* %79, i32 4, i32 %80, %struct._IO_FILE* %81)
  store i32 %call70, i32* %sizeCheck69, align 4, !tbaa !24
  %82 = load i32, i32* %sizeCheck69, align 4, !tbaa !24
  %83 = load i32, i32* %seg0SizeT, align 4, !tbaa !24
  %cmp71 = icmp ne i32 %82, %83
  br i1 %cmp71, label %if.then73, label %if.end89

if.then73:                                        ; preds = %if.end66
  %84 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp74 = icmp sge i32 %84, 1
  br i1 %cmp74, label %if.then76, label %if.end78

if.then76:                                        ; preds = %if.then73
  %85 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call77 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %85, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 73)
  br label %if.end78

if.end78:                                         ; preds = %if.then76, %if.then73
  %86 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp79 = icmp sge i32 %86, 1
  br i1 %cmp79, label %if.then81, label %if.end83

if.then81:                                        ; preds = %if.end78
  %87 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call82 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %87, i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.80, i32 0, i32 0))
  br label %if.end83

if.end83:                                         ; preds = %if.then81, %if.end78
  %88 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp84 = icmp sge i32 %88, 1
  br i1 %cmp84, label %if.then86, label %if.end88

if.then86:                                        ; preds = %if.end83
  %89 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call87 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %89, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end88

if.end88:                                         ; preds = %if.then86, %if.end83
  call void @exit(i32 73) #6
  unreachable

if.end89:                                         ; preds = %if.end66
  %90 = bitcast i32* %sizeCheck69 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #5
  br label %if.end90

if.end90:                                         ; preds = %if.end89, %for.end
  %91 = load i32, i32* %seg0SizeT, align 4, !tbaa !24
  %92 = load i32*, i32** %ptrT, align 4, !tbaa !2
  %add.ptr91 = getelementptr inbounds i32, i32* %92, i32 %91
  store i32* %add.ptr91, i32** %ptrT, align 4, !tbaa !2
  %93 = bitcast i32* %nb0T to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #5
  %94 = bitcast i32* %seg0SizeT to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #5
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %95 = load i32, i32* %bufferSize.addr, align 4, !tbaa !24
  %and = and i32 %95, 3
  %tobool92 = icmp ne i32 %and, 0
  br i1 %tobool92, label %if.then93, label %if.end160

if.then93:                                        ; preds = %while.end
  %96 = bitcast i8** %restStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #5
  %97 = load i32*, i32** %bufferTEnd, align 4, !tbaa !2
  %98 = bitcast i32* %97 to i8*
  store i8* %98, i8** %restStart, align 4, !tbaa !2
  %99 = bitcast i8** %restPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #5
  %100 = load i8*, i8** %restStart, align 4, !tbaa !2
  store i8* %100, i8** %restPtr, align 4, !tbaa !2
  %101 = bitcast i32* %restSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #5
  %102 = load i32, i32* %bufferSize.addr, align 4, !tbaa !24
  %and94 = and i32 %102, 3
  store i32 %and94, i32* %restSize, align 4, !tbaa !24
  %103 = bitcast i8** %restEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %103) #5
  %104 = load i8*, i8** %restStart, align 4, !tbaa !2
  %105 = load i32, i32* %restSize, align 4, !tbaa !24
  %add.ptr95 = getelementptr inbounds i8, i8* %104, i32 %105
  store i8* %add.ptr95, i8** %restEnd, align 4, !tbaa !2
  br label %for.cond96

for.cond96:                                       ; preds = %for.inc105, %if.then93
  %106 = load i8*, i8** %restPtr, align 4, !tbaa !2
  %107 = load i8*, i8** %restEnd, align 4, !tbaa !2
  %cmp97 = icmp ult i8* %106, %107
  br i1 %cmp97, label %land.rhs99, label %land.end103

land.rhs99:                                       ; preds = %for.cond96
  %108 = load i8*, i8** %restPtr, align 4, !tbaa !2
  %109 = load i8, i8* %108, align 1, !tbaa !25
  %conv100 = sext i8 %109 to i32
  %cmp101 = icmp eq i32 %conv100, 0
  br label %land.end103

land.end103:                                      ; preds = %land.rhs99, %for.cond96
  %110 = phi i1 [ false, %for.cond96 ], [ %cmp101, %land.rhs99 ]
  br i1 %110, label %for.body104, label %for.end106

for.body104:                                      ; preds = %land.end103
  br label %for.inc105

for.inc105:                                       ; preds = %for.body104
  %111 = load i8*, i8** %restPtr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %111, i32 1
  store i8* %incdec.ptr, i8** %restPtr, align 4, !tbaa !2
  br label %for.cond96

for.end106:                                       ; preds = %land.end103
  %112 = load i8*, i8** %restPtr, align 4, !tbaa !2
  %113 = load i8*, i8** %restStart, align 4, !tbaa !2
  %sub.ptr.lhs.cast = ptrtoint i8* %112 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %113 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %114 = load i32, i32* %storedSkips.addr, align 4, !tbaa !6
  %add107 = add i32 %114, %sub.ptr.sub
  store i32 %add107, i32* %storedSkips.addr, align 4, !tbaa !6
  %115 = load i8*, i8** %restPtr, align 4, !tbaa !2
  %116 = load i8*, i8** %restEnd, align 4, !tbaa !2
  %cmp108 = icmp ne i8* %115, %116
  br i1 %cmp108, label %if.then110, label %if.end159

if.then110:                                       ; preds = %for.end106
  %117 = bitcast i32* %seekResult111 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %117) #5
  %118 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %119 = load i32, i32* %storedSkips.addr, align 4, !tbaa !6
  %conv112 = zext i32 %119 to i64
  %call113 = call i32 @fseeko(%struct._IO_FILE* %118, i64 %conv112, i32 1)
  store i32 %call113, i32* %seekResult111, align 4, !tbaa !6
  %120 = load i32, i32* %seekResult111, align 4, !tbaa !6
  %tobool114 = icmp ne i32 %120, 0
  br i1 %tobool114, label %if.then115, label %if.end131

if.then115:                                       ; preds = %if.then110
  %121 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp116 = icmp sge i32 %121, 1
  br i1 %cmp116, label %if.then118, label %if.end120

if.then118:                                       ; preds = %if.then115
  %122 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call119 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %122, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 74)
  br label %if.end120

if.end120:                                        ; preds = %if.then118, %if.then115
  %123 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp121 = icmp sge i32 %123, 1
  br i1 %cmp121, label %if.then123, label %if.end125

if.then123:                                       ; preds = %if.end120
  %124 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call124 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %124, i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.83, i32 0, i32 0))
  br label %if.end125

if.end125:                                        ; preds = %if.then123, %if.end120
  %125 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp126 = icmp sge i32 %125, 1
  br i1 %cmp126, label %if.then128, label %if.end130

if.then128:                                       ; preds = %if.end125
  %126 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call129 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %126, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end130

if.end130:                                        ; preds = %if.then128, %if.end125
  call void @exit(i32 74) #6
  unreachable

if.end131:                                        ; preds = %if.then110
  store i32 0, i32* %storedSkips.addr, align 4, !tbaa !6
  %127 = bitcast i32* %sizeCheck132 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %127) #5
  %128 = load i8*, i8** %restPtr, align 4, !tbaa !2
  %129 = load i8*, i8** %restEnd, align 4, !tbaa !2
  %130 = load i8*, i8** %restPtr, align 4, !tbaa !2
  %sub.ptr.lhs.cast133 = ptrtoint i8* %129 to i32
  %sub.ptr.rhs.cast134 = ptrtoint i8* %130 to i32
  %sub.ptr.sub135 = sub i32 %sub.ptr.lhs.cast133, %sub.ptr.rhs.cast134
  %131 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call136 = call i32 @fwrite(i8* %128, i32 1, i32 %sub.ptr.sub135, %struct._IO_FILE* %131)
  store i32 %call136, i32* %sizeCheck132, align 4, !tbaa !24
  %132 = load i32, i32* %sizeCheck132, align 4, !tbaa !24
  %133 = load i8*, i8** %restEnd, align 4, !tbaa !2
  %134 = load i8*, i8** %restPtr, align 4, !tbaa !2
  %sub.ptr.lhs.cast137 = ptrtoint i8* %133 to i32
  %sub.ptr.rhs.cast138 = ptrtoint i8* %134 to i32
  %sub.ptr.sub139 = sub i32 %sub.ptr.lhs.cast137, %sub.ptr.rhs.cast138
  %cmp140 = icmp ne i32 %132, %sub.ptr.sub139
  br i1 %cmp140, label %if.then142, label %if.end158

if.then142:                                       ; preds = %if.end131
  %135 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp143 = icmp sge i32 %135, 1
  br i1 %cmp143, label %if.then145, label %if.end147

if.then145:                                       ; preds = %if.then142
  %136 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call146 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %136, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 75)
  br label %if.end147

if.end147:                                        ; preds = %if.then145, %if.then142
  %137 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp148 = icmp sge i32 %137, 1
  br i1 %cmp148, label %if.then150, label %if.end152

if.then150:                                       ; preds = %if.end147
  %138 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call151 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %138, i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.84, i32 0, i32 0))
  br label %if.end152

if.end152:                                        ; preds = %if.then150, %if.end147
  %139 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp153 = icmp sge i32 %139, 1
  br i1 %cmp153, label %if.then155, label %if.end157

if.then155:                                       ; preds = %if.end152
  %140 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call156 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %140, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end157

if.end157:                                        ; preds = %if.then155, %if.end152
  call void @exit(i32 75) #6
  unreachable

if.end158:                                        ; preds = %if.end131
  %141 = bitcast i32* %sizeCheck132 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #5
  %142 = bitcast i32* %seekResult111 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #5
  br label %if.end159

if.end159:                                        ; preds = %if.end158, %for.end106
  %143 = bitcast i8** %restEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #5
  %144 = bitcast i32* %restSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #5
  %145 = bitcast i8** %restPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #5
  %146 = bitcast i8** %restStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #5
  br label %if.end160

if.end160:                                        ; preds = %if.end159, %while.end
  %147 = load i32, i32* %storedSkips.addr, align 4, !tbaa !6
  store i32 %147, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end160, %if.end13
  %148 = bitcast i32* %segmentSizeT to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #5
  %149 = bitcast i32** %bufferTEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #5
  %150 = bitcast i32* %bufferSizeT to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #5
  %151 = bitcast i32** %ptrT to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #5
  %152 = bitcast i32** %bufferT to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #5
  %153 = bitcast i32* %maskT to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #5
  %154 = bitcast i32* %sizeT to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #5
  %155 = load i32, i32* %retval, align 4
  ret i32 %155
}

; Function Attrs: nounwind
define internal void @LZ4IO_fwriteSparseEnd(%struct._IO_FILE* %file, i32 %storedSkips) #0 {
entry:
  %file.addr = alloca %struct._IO_FILE*, align 4
  %storedSkips.addr = alloca i32, align 4
  %seekResult = alloca i32, align 4
  %lastZeroByte = alloca [1 x i8], align 1
  %sizeCheck = alloca i32, align 4
  store %struct._IO_FILE* %file, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  store i32 %storedSkips, i32* %storedSkips.addr, align 4, !tbaa !6
  %0 = load i32, i32* %storedSkips.addr, align 4, !tbaa !6
  %cmp = icmp ugt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end39

if.then:                                          ; preds = %entry
  %1 = bitcast i32* %seekResult to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %3 = load i32, i32* %storedSkips.addr, align 4, !tbaa !6
  %sub = sub i32 %3, 1
  %conv = zext i32 %sub to i64
  %call = call i32 @fseeko(%struct._IO_FILE* %2, i64 %conv, i32 1)
  store i32 %call, i32* %seekResult, align 4, !tbaa !6
  %4 = load i32, i32* %seekResult, align 4, !tbaa !6
  %cmp1 = icmp ne i32 %4, 0
  br i1 %cmp1, label %if.then3, label %if.end18

if.then3:                                         ; preds = %if.then
  %5 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp4 = icmp sge i32 %5, 1
  br i1 %cmp4, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.then3
  %6 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call7 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %6, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 69)
  br label %if.end

if.end:                                           ; preds = %if.then6, %if.then3
  %7 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp8 = icmp sge i32 %7, 1
  br i1 %cmp8, label %if.then10, label %if.end12

if.then10:                                        ; preds = %if.end
  %8 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call11 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %8, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.85, i32 0, i32 0))
  br label %if.end12

if.end12:                                         ; preds = %if.then10, %if.end
  %9 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp13 = icmp sge i32 %9, 1
  br i1 %cmp13, label %if.then15, label %if.end17

if.then15:                                        ; preds = %if.end12
  %10 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call16 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %10, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end17

if.end17:                                         ; preds = %if.then15, %if.end12
  call void @exit(i32 69) #6
  unreachable

if.end18:                                         ; preds = %if.then
  %11 = bitcast [1 x i8]* %lastZeroByte to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %11) #5
  %12 = bitcast [1 x i8]* %lastZeroByte to i8*
  call void @llvm.memset.p0i8.i32(i8* align 1 %12, i8 0, i32 1, i1 false)
  %13 = bitcast i32* %sizeCheck to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  %arraydecay = getelementptr inbounds [1 x i8], [1 x i8]* %lastZeroByte, i32 0, i32 0
  %14 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call19 = call i32 @fwrite(i8* %arraydecay, i32 1, i32 1, %struct._IO_FILE* %14)
  store i32 %call19, i32* %sizeCheck, align 4, !tbaa !24
  %15 = load i32, i32* %sizeCheck, align 4, !tbaa !24
  %cmp20 = icmp ne i32 %15, 1
  br i1 %cmp20, label %if.then22, label %if.end38

if.then22:                                        ; preds = %if.end18
  %16 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp23 = icmp sge i32 %16, 1
  br i1 %cmp23, label %if.then25, label %if.end27

if.then25:                                        ; preds = %if.then22
  %17 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call26 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %17, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i32 0, i32 0), i32 69)
  br label %if.end27

if.end27:                                         ; preds = %if.then25, %if.then22
  %18 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp28 = icmp sge i32 %18, 1
  br i1 %cmp28, label %if.then30, label %if.end32

if.then30:                                        ; preds = %if.end27
  %19 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call31 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %19, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.86, i32 0, i32 0))
  br label %if.end32

if.end32:                                         ; preds = %if.then30, %if.end27
  %20 = load i32, i32* @g_displayLevel, align 4, !tbaa !6
  %cmp33 = icmp sge i32 %20, 1
  br i1 %cmp33, label %if.then35, label %if.end37

if.then35:                                        ; preds = %if.end32
  %21 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call36 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %21, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.2, i32 0, i32 0))
  br label %if.end37

if.end37:                                         ; preds = %if.then35, %if.end32
  call void @exit(i32 69) #6
  unreachable

if.end38:                                         ; preds = %if.end18
  %22 = bitcast i32* %sizeCheck to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #5
  %23 = bitcast [1 x i8]* %lastZeroByte to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %23) #5
  %24 = bitcast i32* %seekResult to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #5
  br label %if.end39

if.end39:                                         ; preds = %if.end38, %entry
  ret void
}

declare i32 @LZ4_decompress_safe(i8*, i8*, i32, i32) #2

declare i8* @strrchr(i8*, i32) #2

declare i32 @feof(%struct._IO_FILE*) #2

declare i32 @LZ4F_headerSize(i8*, i32) #2

declare i32 @LZ4F_getFrameInfo(%struct.LZ4F_dctx_s*, %struct.LZ4F_frameInfo_t*, i8*, i32*) #2

; Function Attrs: nounwind
define internal i64 @LZ4IO_skipBlocksData(%struct._IO_FILE* %finput, i32 %blockChecksumFlag, i32 %contentChecksumFlag) #0 {
entry:
  %retval = alloca i64, align 8
  %finput.addr = alloca %struct._IO_FILE*, align 4
  %blockChecksumFlag.addr = alloca i32, align 4
  %contentChecksumFlag.addr = alloca i32, align 4
  %blockInfo = alloca [4 x i8], align 1
  %totalBlocksSize = alloca i64, align 8
  %cleanup.dest.slot = alloca i32, align 4
  %nextCBlockSize = alloca i32, align 4
  %nextBlock = alloca i32, align 4
  store %struct._IO_FILE* %finput, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  store i32 %blockChecksumFlag, i32* %blockChecksumFlag.addr, align 4, !tbaa !25
  store i32 %contentChecksumFlag, i32* %contentChecksumFlag.addr, align 4, !tbaa !25
  %0 = bitcast [4 x i8]* %blockInfo to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i64* %totalBlocksSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #5
  store i64 0, i64* %totalBlocksSize, align 8, !tbaa !26
  br label %for.cond

for.cond:                                         ; preds = %cleanup.cont, %entry
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %blockInfo, i32 0, i32 0
  %2 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %call = call i32 @fread(i8* %arraydecay, i32 1, i32 4, %struct._IO_FILE* %2)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end4, label %if.then

if.then:                                          ; preds = %for.cond
  %3 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %call1 = call i32 @feof(%struct._IO_FILE* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %4 = load i64, i64* %totalBlocksSize, align 8, !tbaa !26
  store i64 %4, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup25

if.end:                                           ; preds = %if.then
  store i64 0, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup25

if.end4:                                          ; preds = %for.cond
  %5 = load i64, i64* %totalBlocksSize, align 8, !tbaa !26
  %add = add i64 %5, 4
  store i64 %add, i64* %totalBlocksSize, align 8, !tbaa !26
  %6 = bitcast i32* %nextCBlockSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = bitcast [4 x i8]* %blockInfo to i8*
  %call5 = call i32 @LZ4IO_readLE32(i8* %7)
  %and = and i32 %call5, 2147483647
  store i32 %and, i32* %nextCBlockSize, align 4, !tbaa !24
  %8 = bitcast i32* %nextBlock to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load i32, i32* %nextCBlockSize, align 4, !tbaa !24
  %10 = load i32, i32* %blockChecksumFlag.addr, align 4, !tbaa !25
  %mul = mul i32 %10, 4
  %add6 = add i32 %9, %mul
  store i32 %add6, i32* %nextBlock, align 4, !tbaa !24
  %11 = load i32, i32* %nextCBlockSize, align 4, !tbaa !24
  %cmp = icmp eq i32 %11, 0
  br i1 %cmp, label %if.then7, label %if.end16

if.then7:                                         ; preds = %if.end4
  %12 = load i32, i32* %contentChecksumFlag.addr, align 4, !tbaa !25
  %tobool8 = icmp ne i32 %12, 0
  br i1 %tobool8, label %if.then9, label %if.end15

if.then9:                                         ; preds = %if.then7
  %13 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %call10 = call i32 @fseeko(%struct._IO_FILE* %13, i64 4, i32 1)
  %cmp11 = icmp ne i32 %call10, 0
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %if.then9
  store i64 0, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %if.then9
  %14 = load i64, i64* %totalBlocksSize, align 8, !tbaa !26
  %add14 = add i64 %14, 4
  store i64 %add14, i64* %totalBlocksSize, align 8, !tbaa !26
  br label %if.end15

if.end15:                                         ; preds = %if.end13, %if.then7
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end16:                                         ; preds = %if.end4
  %15 = load i32, i32* %nextBlock, align 4, !tbaa !24
  %conv = zext i32 %15 to i64
  %16 = load i64, i64* %totalBlocksSize, align 8, !tbaa !26
  %add17 = add i64 %16, %conv
  store i64 %add17, i64* %totalBlocksSize, align 8, !tbaa !26
  %17 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %18 = load i32, i32* %nextBlock, align 4, !tbaa !24
  %conv18 = zext i32 %18 to i64
  %call19 = call i32 @fseeko(%struct._IO_FILE* %17, i64 %conv18, i32 1)
  %cmp20 = icmp ne i32 %call19, 0
  br i1 %cmp20, label %if.then22, label %if.end23

if.then22:                                        ; preds = %if.end16
  store i64 0, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end23:                                         ; preds = %if.end16
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end23, %if.then22, %if.end15, %if.then12
  %19 = bitcast i32* %nextBlock to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #5
  %20 = bitcast i32* %nextCBlockSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup25 [
    i32 0, label %cleanup.cont
    i32 2, label %for.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.cond

for.end:                                          ; preds = %cleanup
  %21 = load i64, i64* %totalBlocksSize, align 8, !tbaa !26
  store i64 %21, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup25

cleanup25:                                        ; preds = %for.end, %cleanup, %if.end, %if.then3
  %22 = bitcast i64* %totalBlocksSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %22) #5
  %23 = bitcast [4 x i8]* %blockInfo to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #5
  %24 = load i64, i64* %retval, align 8
  ret i64 %24
}

; Function Attrs: nounwind
define internal i64 @LZ4IO_skipLegacyBlocksData(%struct._IO_FILE* %finput) #0 {
entry:
  %retval = alloca i64, align 8
  %finput.addr = alloca %struct._IO_FILE*, align 4
  %blockInfo = alloca [4 x i8], align 1
  %totalBlocksSize = alloca i64, align 8
  %cleanup.dest.slot = alloca i32, align 4
  %nextCBlockSize = alloca i32, align 4
  store %struct._IO_FILE* %finput, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %0 = bitcast [4 x i8]* %blockInfo to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i64* %totalBlocksSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #5
  store i64 0, i64* %totalBlocksSize, align 8, !tbaa !26
  br label %for.cond

for.cond:                                         ; preds = %cleanup.cont, %entry
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %blockInfo, i32 0, i32 0
  %2 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %call = call i32 @fread(i8* %arraydecay, i32 1, i32 4, %struct._IO_FILE* %2)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end4, label %if.then

if.then:                                          ; preds = %for.cond
  %3 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %call1 = call i32 @feof(%struct._IO_FILE* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %4 = load i64, i64* %totalBlocksSize, align 8, !tbaa !26
  store i64 %4, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

if.end:                                           ; preds = %if.then
  store i64 0, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

if.end4:                                          ; preds = %for.cond
  %5 = bitcast i32* %nextCBlockSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = bitcast [4 x i8]* %blockInfo to i8*
  %call5 = call i32 @LZ4IO_readLE32(i8* %6)
  store i32 %call5, i32* %nextCBlockSize, align 4, !tbaa !6
  %7 = load i32, i32* %nextCBlockSize, align 4, !tbaa !6
  %cmp = icmp eq i32 %7, 407642370
  br i1 %cmp, label %if.then10, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end4
  %8 = load i32, i32* %nextCBlockSize, align 4, !tbaa !6
  %cmp6 = icmp eq i32 %8, 407708164
  br i1 %cmp6, label %if.then10, label %lor.lhs.false7

lor.lhs.false7:                                   ; preds = %lor.lhs.false
  %9 = load i32, i32* %nextCBlockSize, align 4, !tbaa !6
  %call8 = call i32 @LZ4IO_isSkippableMagicNumber(i32 %9)
  %tobool9 = icmp ne i32 %call8, 0
  br i1 %tobool9, label %if.then10, label %if.end15

if.then10:                                        ; preds = %lor.lhs.false7, %lor.lhs.false, %if.end4
  %10 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %call11 = call i32 @fseek(%struct._IO_FILE* %10, i32 -4, i32 1)
  %cmp12 = icmp ne i32 %call11, 0
  br i1 %cmp12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %if.then10
  store i64 0, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %if.then10
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end15:                                         ; preds = %lor.lhs.false7
  %11 = load i32, i32* %nextCBlockSize, align 4, !tbaa !6
  %add = add i32 4, %11
  %conv = zext i32 %add to i64
  %12 = load i64, i64* %totalBlocksSize, align 8, !tbaa !26
  %add16 = add i64 %12, %conv
  store i64 %add16, i64* %totalBlocksSize, align 8, !tbaa !26
  %13 = load %struct._IO_FILE*, %struct._IO_FILE** %finput.addr, align 4, !tbaa !2
  %14 = load i32, i32* %nextCBlockSize, align 4, !tbaa !6
  %conv17 = zext i32 %14 to i64
  %call18 = call i32 @fseeko(%struct._IO_FILE* %13, i64 %conv17, i32 1)
  %cmp19 = icmp ne i32 %call18, 0
  br i1 %cmp19, label %if.then21, label %if.end22

if.then21:                                        ; preds = %if.end15
  store i64 0, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end22:                                         ; preds = %if.end15
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end22, %if.then21, %if.end14, %if.then13
  %15 = bitcast i32* %nextCBlockSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup23 [
    i32 0, label %cleanup.cont
    i32 2, label %for.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.cond

for.end:                                          ; preds = %cleanup
  %16 = load i64, i64* %totalBlocksSize, align 8, !tbaa !26
  store i64 %16, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

cleanup23:                                        ; preds = %for.end, %cleanup, %if.end, %if.then3
  %17 = bitcast i64* %totalBlocksSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %17) #5
  %18 = bitcast [4 x i8]* %blockInfo to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #5
  %19 = load i64, i64* %retval, align 8
  ret i64 %19
}

declare i32 @fseek(%struct._IO_FILE*, i32, i32) #2

declare i32 @sprintf(i8*, i8*, ...) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { nounwind }
attributes #6 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !7, i64 0}
!9 = !{!"LZ4IO_prefs_s", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !10, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !3, i64 48, !7, i64 52}
!10 = !{!"long", !4, i64 0}
!11 = !{!9, !7, i64 4}
!12 = !{!9, !7, i64 8}
!13 = !{!9, !7, i64 12}
!14 = !{!9, !10, i64 16}
!15 = !{!9, !7, i64 20}
!16 = !{!9, !7, i64 24}
!17 = !{!9, !7, i64 28}
!18 = !{!9, !7, i64 32}
!19 = !{!9, !7, i64 36}
!20 = !{!9, !7, i64 40}
!21 = !{!9, !7, i64 44}
!22 = !{!9, !3, i64 48}
!23 = !{!9, !7, i64 52}
!24 = !{!10, !10, i64 0}
!25 = !{!4, !4, i64 0}
!26 = !{!27, !27, i64 0}
!27 = !{!"long long", !4, i64 0}
!28 = !{!29, !29, i64 0}
!29 = !{!"double", !4, i64 0}
!30 = !{!31, !3, i64 0}
!31 = !{!"", !3, i64 0, !10, i64 4, !3, i64 8, !10, i64 12, !3, i64 16, !3, i64 20}
!32 = !{!31, !10, i64 4}
!33 = !{!31, !10, i64 12}
!34 = !{!31, !3, i64 8}
!35 = !{!31, !3, i64 20}
!36 = !{!31, !3, i64 16}
!37 = !{!38, !7, i64 36}
!38 = !{!"", !39, i64 0, !7, i64 32, !7, i64 36, !7, i64 40, !4, i64 44}
!39 = !{!"", !4, i64 0, !4, i64 4, !4, i64 8, !4, i64 12, !27, i64 16, !7, i64 24, !4, i64 28}
!40 = !{!38, !7, i64 32}
!41 = !{!38, !4, i64 4}
!42 = !{!38, !4, i64 0}
!43 = !{!38, !4, i64 28}
!44 = !{!38, !4, i64 8}
!45 = !{!38, !7, i64 40}
!46 = !{!38, !27, i64 16}
!47 = !{i64 0, i64 4, !2, i64 4, i64 4, !24, i64 8, i64 4, !2, i64 12, i64 4, !24, i64 16, i64 4, !2, i64 20, i64 4, !2}
!48 = !{!49, !10, i64 4}
!49 = !{!"", !3, i64 0, !10, i64 4, !3, i64 8, !10, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !10, i64 28}
!50 = !{!49, !3, i64 0}
!51 = !{!49, !10, i64 12}
!52 = !{!49, !3, i64 8}
!53 = !{!49, !3, i64 16}
!54 = !{!49, !3, i64 20}
!55 = !{!49, !3, i64 24}
!56 = !{!57, !3, i64 0}
!57 = !{!"", !3, i64 0, !27, i64 8, !27, i64 16, !58, i64 24, !59, i64 64, !59, i64 66, !59, i64 68}
!58 = !{!"", !39, i64 0, !4, i64 32}
!59 = !{!"short", !4, i64 0}
!60 = !{!57, !27, i64 16}
!61 = !{!57, !59, i64 64}
!62 = !{!57, !4, i64 56}
!63 = !{!57, !59, i64 66}
!64 = !{!57, !4, i64 24}
!65 = !{!57, !4, i64 28}
!66 = !{!57, !27, i64 8}
!67 = !{!57, !59, i64 68}
!68 = !{!57, !27, i64 40}
!69 = !{!58, !4, i64 0}
!70 = !{!58, !4, i64 4}
!71 = !{!58, !4, i64 28}
!72 = !{!58, !4, i64 8}
!73 = !{!58, !4, i64 32}
!74 = !{!58, !27, i64 16}
!75 = !{i64 0, i64 4, !25, i64 4, i64 4, !25, i64 8, i64 4, !25, i64 12, i64 4, !25, i64 16, i64 8, !26, i64 24, i64 4, !6, i64 28, i64 4, !25, i64 32, i64 4, !25}
!76 = !{!77, !77, i64 0}
!77 = !{!"long double", !4, i64 0}
!78 = !{!79, !7, i64 12}
!79 = !{!"stat", !7, i64 0, !7, i64 4, !10, i64 8, !7, i64 12, !10, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !27, i64 40, !10, i64 48, !7, i64 52, !80, i64 56, !80, i64 64, !80, i64 72, !27, i64 80}
!80 = !{!"timespec", !10, i64 0, !10, i64 4}
!81 = !{!79, !27, i64 40}
!82 = !{!80, !10, i64 4}
!83 = !{!79, !10, i64 64}
!84 = !{!80, !10, i64 0}
!85 = !{!79, !7, i64 20}
!86 = !{!79, !7, i64 24}
!87 = !{!49, !10, i64 28}
