; ModuleID = 'lz4frame.c'
source_filename = "lz4frame.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.LZ4F_preferences_t = type { %struct.LZ4F_frameInfo_t, i32, i32, i32, [3 x i32] }
%struct.LZ4F_frameInfo_t = type { i32, i32, i32, i32, i64, i32, i32 }
%struct.LZ4F_cctx_s = type { %struct.LZ4F_preferences_t, i32, i32, %struct.LZ4F_CDict_s*, i32, i32, i8*, i8*, i32, i64, %struct.XXH32_state_s, i8*, i16, i16 }
%struct.XXH32_state_s = type { i32, i32, i32, i32, i32, i32, [4 x i32], i32, i32 }
%struct.LZ4F_CDict_s = type { i8*, %union.LZ4_stream_u*, %union.LZ4_streamHC_u* }
%union.LZ4_stream_u = type { [2052 x i64] }
%union.LZ4_streamHC_u = type { [65550 x i32] }
%struct.LZ4F_compressOptions_t = type { i32, [3 x i32] }
%struct.LZ4F_dctx_s = type { %struct.LZ4F_frameInfo_t, i32, i32, i64, i32, i32, i8*, i32, i32, i8*, i8*, i32, i8*, i32, i32, %struct.XXH32_state_s, %struct.XXH32_state_s, [19 x i8] }
%struct.LZ4F_decompressOptions_t = type { i32, [3 x i32] }

@LZ4F_getErrorName.codeError = internal global i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str, i32 0, i32 0), align 4
@.str = private unnamed_addr constant [23 x i8] c"Unspecified error code\00", align 1
@LZ4F_errorStrings = internal global [21 x i8*] [i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.1, i32 0, i32 0), i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.2, i32 0, i32 0), i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.3, i32 0, i32 0), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.4, i32 0, i32 0), i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.5, i32 0, i32 0), i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.6, i32 0, i32 0), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.7, i32 0, i32 0), i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.8, i32 0, i32 0), i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.9, i32 0, i32 0), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.10, i32 0, i32 0), i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.11, i32 0, i32 0), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.12, i32 0, i32 0), i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.13, i32 0, i32 0), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.14, i32 0, i32 0), i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.15, i32 0, i32 0), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.16, i32 0, i32 0), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.17, i32 0, i32 0), i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.18, i32 0, i32 0), i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.19, i32 0, i32 0), i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.20, i32 0, i32 0), i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.21, i32 0, i32 0)], align 16
@LZ4F_getBlockSize.blockSizes = internal constant [4 x i32] [i32 65536, i32 262144, i32 1048576, i32 4194304], align 16
@.str.1 = private unnamed_addr constant [11 x i8] c"OK_NoError\00", align 1
@.str.2 = private unnamed_addr constant [14 x i8] c"ERROR_GENERIC\00", align 1
@.str.3 = private unnamed_addr constant [27 x i8] c"ERROR_maxBlockSize_invalid\00", align 1
@.str.4 = private unnamed_addr constant [24 x i8] c"ERROR_blockMode_invalid\00", align 1
@.str.5 = private unnamed_addr constant [34 x i8] c"ERROR_contentChecksumFlag_invalid\00", align 1
@.str.6 = private unnamed_addr constant [31 x i8] c"ERROR_compressionLevel_invalid\00", align 1
@.str.7 = private unnamed_addr constant [26 x i8] c"ERROR_headerVersion_wrong\00", align 1
@.str.8 = private unnamed_addr constant [28 x i8] c"ERROR_blockChecksum_invalid\00", align 1
@.str.9 = private unnamed_addr constant [23 x i8] c"ERROR_reservedFlag_set\00", align 1
@.str.10 = private unnamed_addr constant [24 x i8] c"ERROR_allocation_failed\00", align 1
@.str.11 = private unnamed_addr constant [23 x i8] c"ERROR_srcSize_tooLarge\00", align 1
@.str.12 = private unnamed_addr constant [26 x i8] c"ERROR_dstMaxSize_tooSmall\00", align 1
@.str.13 = private unnamed_addr constant [29 x i8] c"ERROR_frameHeader_incomplete\00", align 1
@.str.14 = private unnamed_addr constant [24 x i8] c"ERROR_frameType_unknown\00", align 1
@.str.15 = private unnamed_addr constant [22 x i8] c"ERROR_frameSize_wrong\00", align 1
@.str.16 = private unnamed_addr constant [19 x i8] c"ERROR_srcPtr_wrong\00", align 1
@.str.17 = private unnamed_addr constant [26 x i8] c"ERROR_decompressionFailed\00", align 1
@.str.18 = private unnamed_addr constant [29 x i8] c"ERROR_headerChecksum_invalid\00", align 1
@.str.19 = private unnamed_addr constant [30 x i8] c"ERROR_contentChecksum_invalid\00", align 1
@.str.20 = private unnamed_addr constant [35 x i8] c"ERROR_frameDecoding_alreadyStarted\00", align 1
@.str.21 = private unnamed_addr constant [14 x i8] c"ERROR_maxCode\00", align 1

; Function Attrs: nounwind
define i32 @LZ4F_isError(i32 %code) #0 {
entry:
  %code.addr = alloca i32, align 4
  store i32 %code, i32* %code.addr, align 4, !tbaa !3
  %0 = load i32, i32* %code.addr, align 4, !tbaa !3
  %cmp = icmp ugt i32 %0, -20
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: nounwind
define i8* @LZ4F_getErrorName(i32 %code) #0 {
entry:
  %retval = alloca i8*, align 4
  %code.addr = alloca i32, align 4
  store i32 %code, i32* %code.addr, align 4, !tbaa !3
  %0 = load i32, i32* %code.addr, align 4, !tbaa !3
  %call = call i32 @LZ4F_isError(i32 %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %code.addr, align 4, !tbaa !3
  %sub = sub nsw i32 0, %1
  %arrayidx = getelementptr inbounds [21 x i8*], [21 x i8*]* @LZ4F_errorStrings, i32 0, i32 %sub
  %2 = load i8*, i8** %arrayidx, align 4, !tbaa !7
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i8*, i8** @LZ4F_getErrorName.codeError, align 4, !tbaa !7
  store i8* %3, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i8*, i8** %retval, align 4
  ret i8* %4
}

; Function Attrs: nounwind
define hidden i32 @LZ4F_getErrorCode(i32 %functionResult) #0 {
entry:
  %retval = alloca i32, align 4
  %functionResult.addr = alloca i32, align 4
  store i32 %functionResult, i32* %functionResult.addr, align 4, !tbaa !3
  %0 = load i32, i32* %functionResult.addr, align 4, !tbaa !3
  %call = call i32 @LZ4F_isError(i32 %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %functionResult.addr, align 4, !tbaa !3
  %sub = sub nsw i32 0, %1
  store i32 %sub, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i32, i32* %retval, align 4
  ret i32 %2
}

; Function Attrs: nounwind
define i32 @LZ4F_getVersion() #0 {
entry:
  ret i32 100
}

; Function Attrs: nounwind
define i32 @LZ4F_compressionLevel_max() #0 {
entry:
  ret i32 12
}

; Function Attrs: nounwind
define hidden i32 @LZ4F_getBlockSize(i32 %blockSizeID) #0 {
entry:
  %retval = alloca i32, align 4
  %blockSizeID.addr = alloca i32, align 4
  store i32 %blockSizeID, i32* %blockSizeID.addr, align 4, !tbaa !9
  %0 = load i32, i32* %blockSizeID.addr, align 4, !tbaa !9
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 4, i32* %blockSizeID.addr, align 4, !tbaa !9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load i32, i32* %blockSizeID.addr, align 4, !tbaa !9
  %cmp1 = icmp ult i32 %1, 4
  br i1 %cmp1, label %if.then3, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %2 = load i32, i32* %blockSizeID.addr, align 4, !tbaa !9
  %cmp2 = icmp ugt i32 %2, 7
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %lor.lhs.false, %if.end
  %call = call i32 @err0r(i32 2)
  store i32 %call, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %lor.lhs.false
  %3 = load i32, i32* %blockSizeID.addr, align 4, !tbaa !9
  %sub = sub i32 %3, 4
  store i32 %sub, i32* %blockSizeID.addr, align 4, !tbaa !9
  %4 = load i32, i32* %blockSizeID.addr, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* @LZ4F_getBlockSize.blockSizes, i32 0, i32 %4
  %5 = load i32, i32* %arrayidx, align 4, !tbaa !3
  store i32 %5, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: nounwind
define internal i32 @err0r(i32 %code) #0 {
entry:
  %code.addr = alloca i32, align 4
  store i32 %code, i32* %code.addr, align 4, !tbaa !11
  %0 = load i32, i32* %code.addr, align 4, !tbaa !11
  %sub = sub nsw i32 0, %0
  ret i32 %sub
}

; Function Attrs: nounwind
define i32 @LZ4F_compressFrameBound(i32 %srcSize, %struct.LZ4F_preferences_t* %preferencesPtr) #0 {
entry:
  %srcSize.addr = alloca i32, align 4
  %preferencesPtr.addr = alloca %struct.LZ4F_preferences_t*, align 4
  %prefs = alloca %struct.LZ4F_preferences_t, align 8
  %headerSize = alloca i32, align 4
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !3
  store %struct.LZ4F_preferences_t* %preferencesPtr, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %0 = bitcast %struct.LZ4F_preferences_t* %prefs to i8*
  call void @llvm.lifetime.start.p0i8(i64 56, i8* %0) #4
  %1 = bitcast i32* %headerSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 19, i32* %headerSize, align 4, !tbaa !3
  %2 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %cmp = icmp ne %struct.LZ4F_preferences_t* %2, null
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %4 = bitcast %struct.LZ4F_preferences_t* %prefs to i8*
  %5 = bitcast %struct.LZ4F_preferences_t* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %4, i8* align 8 %5, i32 56, i1 false), !tbaa.struct !12
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = bitcast %struct.LZ4F_preferences_t* %prefs to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %6, i8 0, i32 56, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %autoFlush = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 2
  store i32 1, i32* %autoFlush, align 4, !tbaa !15
  %7 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %call = call i32 @LZ4F_compressBound_internal(i32 %7, %struct.LZ4F_preferences_t* %prefs, i32 0)
  %add = add i32 19, %call
  %8 = bitcast i32* %headerSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #4
  %9 = bitcast %struct.LZ4F_preferences_t* %prefs to i8*
  call void @llvm.lifetime.end.p0i8(i64 56, i8* %9) #4
  ret i32 %add
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: nounwind
define internal i32 @LZ4F_compressBound_internal(i32 %srcSize, %struct.LZ4F_preferences_t* %preferencesPtr, i32 %alreadyBuffered) #0 {
entry:
  %srcSize.addr = alloca i32, align 4
  %preferencesPtr.addr = alloca %struct.LZ4F_preferences_t*, align 4
  %alreadyBuffered.addr = alloca i32, align 4
  %prefsNull = alloca %struct.LZ4F_preferences_t, align 8
  %prefsPtr = alloca %struct.LZ4F_preferences_t*, align 4
  %flush = alloca i32, align 4
  %blockID = alloca i32, align 4
  %blockSize = alloca i32, align 4
  %maxBuffered = alloca i32, align 4
  %bufferedSize = alloca i32, align 4
  %maxSrcSize = alloca i32, align 4
  %nbFullBlocks = alloca i32, align 4
  %partialBlockSize = alloca i32, align 4
  %lastBlockSize = alloca i32, align 4
  %nbBlocks = alloca i32, align 4
  %blockCRCSize = alloca i32, align 4
  %frameEnd = alloca i32, align 4
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !3
  store %struct.LZ4F_preferences_t* %preferencesPtr, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  store i32 %alreadyBuffered, i32* %alreadyBuffered.addr, align 4, !tbaa !3
  %0 = bitcast %struct.LZ4F_preferences_t* %prefsNull to i8*
  call void @llvm.lifetime.start.p0i8(i64 56, i8* %0) #4
  %1 = bitcast %struct.LZ4F_preferences_t* %prefsNull to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %1, i8 0, i32 56, i1 false)
  %frameInfo = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefsNull, i32 0, i32 0
  %contentChecksumFlag = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo, i32 0, i32 2
  store i32 1, i32* %contentChecksumFlag, align 8, !tbaa !18
  %frameInfo1 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefsNull, i32 0, i32 0
  %blockChecksumFlag = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo1, i32 0, i32 6
  store i32 1, i32* %blockChecksumFlag, align 4, !tbaa !19
  %2 = bitcast %struct.LZ4F_preferences_t** %prefsPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %cmp = icmp eq %struct.LZ4F_preferences_t* %3, null
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %4 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.LZ4F_preferences_t* [ %prefsNull, %cond.true ], [ %4, %cond.false ]
  store %struct.LZ4F_preferences_t* %cond, %struct.LZ4F_preferences_t** %prefsPtr, align 4, !tbaa !7
  %5 = bitcast i32* %flush to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %prefsPtr, align 4, !tbaa !7
  %autoFlush = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %6, i32 0, i32 2
  %7 = load i32, i32* %autoFlush, align 4, !tbaa !15
  %8 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %cmp2 = icmp eq i32 %8, 0
  %conv = zext i1 %cmp2 to i32
  %or = or i32 %7, %conv
  store i32 %or, i32* %flush, align 4, !tbaa !9
  %9 = bitcast i32* %blockID to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %prefsPtr, align 4, !tbaa !7
  %frameInfo3 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %10, i32 0, i32 0
  %blockSizeID = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo3, i32 0, i32 0
  %11 = load i32, i32* %blockSizeID, align 8, !tbaa !20
  store i32 %11, i32* %blockID, align 4, !tbaa !11
  %12 = bitcast i32* %blockSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load i32, i32* %blockID, align 4, !tbaa !11
  %call = call i32 @LZ4F_getBlockSize(i32 %13)
  store i32 %call, i32* %blockSize, align 4, !tbaa !3
  %14 = bitcast i32* %maxBuffered to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = load i32, i32* %blockSize, align 4, !tbaa !3
  %sub = sub i32 %15, 1
  store i32 %sub, i32* %maxBuffered, align 4, !tbaa !3
  %16 = bitcast i32* %bufferedSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load i32, i32* %alreadyBuffered.addr, align 4, !tbaa !3
  %18 = load i32, i32* %maxBuffered, align 4, !tbaa !3
  %cmp4 = icmp ult i32 %17, %18
  br i1 %cmp4, label %cond.true6, label %cond.false7

cond.true6:                                       ; preds = %cond.end
  %19 = load i32, i32* %alreadyBuffered.addr, align 4, !tbaa !3
  br label %cond.end8

cond.false7:                                      ; preds = %cond.end
  %20 = load i32, i32* %maxBuffered, align 4, !tbaa !3
  br label %cond.end8

cond.end8:                                        ; preds = %cond.false7, %cond.true6
  %cond9 = phi i32 [ %19, %cond.true6 ], [ %20, %cond.false7 ]
  store i32 %cond9, i32* %bufferedSize, align 4, !tbaa !3
  %21 = bitcast i32* %maxSrcSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %23 = load i32, i32* %bufferedSize, align 4, !tbaa !3
  %add = add i32 %22, %23
  store i32 %add, i32* %maxSrcSize, align 4, !tbaa !3
  %24 = bitcast i32* %nbFullBlocks to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load i32, i32* %maxSrcSize, align 4, !tbaa !3
  %26 = load i32, i32* %blockSize, align 4, !tbaa !3
  %div = udiv i32 %25, %26
  store i32 %div, i32* %nbFullBlocks, align 4, !tbaa !9
  %27 = bitcast i32* %partialBlockSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load i32, i32* %maxSrcSize, align 4, !tbaa !3
  %29 = load i32, i32* %blockSize, align 4, !tbaa !3
  %sub10 = sub i32 %29, 1
  %and = and i32 %28, %sub10
  store i32 %and, i32* %partialBlockSize, align 4, !tbaa !3
  %30 = bitcast i32* %lastBlockSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  %31 = load i32, i32* %flush, align 4, !tbaa !9
  %tobool = icmp ne i32 %31, 0
  br i1 %tobool, label %cond.true11, label %cond.false12

cond.true11:                                      ; preds = %cond.end8
  %32 = load i32, i32* %partialBlockSize, align 4, !tbaa !3
  br label %cond.end13

cond.false12:                                     ; preds = %cond.end8
  br label %cond.end13

cond.end13:                                       ; preds = %cond.false12, %cond.true11
  %cond14 = phi i32 [ %32, %cond.true11 ], [ 0, %cond.false12 ]
  store i32 %cond14, i32* %lastBlockSize, align 4, !tbaa !3
  %33 = bitcast i32* %nbBlocks to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  %34 = load i32, i32* %nbFullBlocks, align 4, !tbaa !9
  %35 = load i32, i32* %lastBlockSize, align 4, !tbaa !3
  %cmp15 = icmp ugt i32 %35, 0
  %conv16 = zext i1 %cmp15 to i32
  %add17 = add i32 %34, %conv16
  store i32 %add17, i32* %nbBlocks, align 4, !tbaa !9
  %36 = bitcast i32* %blockCRCSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #4
  %37 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %prefsPtr, align 4, !tbaa !7
  %frameInfo18 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %37, i32 0, i32 0
  %blockChecksumFlag19 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo18, i32 0, i32 6
  %38 = load i32, i32* %blockChecksumFlag19, align 4, !tbaa !19
  %mul = mul i32 4, %38
  store i32 %mul, i32* %blockCRCSize, align 4, !tbaa !3
  %39 = bitcast i32* %frameEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #4
  %40 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %prefsPtr, align 4, !tbaa !7
  %frameInfo20 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %40, i32 0, i32 0
  %contentChecksumFlag21 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo20, i32 0, i32 2
  %41 = load i32, i32* %contentChecksumFlag21, align 8, !tbaa !18
  %mul22 = mul i32 %41, 4
  %add23 = add i32 4, %mul22
  store i32 %add23, i32* %frameEnd, align 4, !tbaa !3
  %42 = load i32, i32* %blockCRCSize, align 4, !tbaa !3
  %add24 = add i32 4, %42
  %43 = load i32, i32* %nbBlocks, align 4, !tbaa !9
  %mul25 = mul i32 %add24, %43
  %44 = load i32, i32* %blockSize, align 4, !tbaa !3
  %45 = load i32, i32* %nbFullBlocks, align 4, !tbaa !9
  %mul26 = mul i32 %44, %45
  %add27 = add i32 %mul25, %mul26
  %46 = load i32, i32* %lastBlockSize, align 4, !tbaa !3
  %add28 = add i32 %add27, %46
  %47 = load i32, i32* %frameEnd, align 4, !tbaa !3
  %add29 = add i32 %add28, %47
  %48 = bitcast i32* %frameEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #4
  %49 = bitcast i32* %blockCRCSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #4
  %50 = bitcast i32* %nbBlocks to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #4
  %51 = bitcast i32* %lastBlockSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #4
  %52 = bitcast i32* %partialBlockSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #4
  %53 = bitcast i32* %nbFullBlocks to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  %54 = bitcast i32* %maxSrcSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #4
  %55 = bitcast i32* %bufferedSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #4
  %56 = bitcast i32* %maxBuffered to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #4
  %57 = bitcast i32* %blockSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #4
  %58 = bitcast i32* %blockID to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #4
  %59 = bitcast i32* %flush to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #4
  %60 = bitcast %struct.LZ4F_preferences_t** %prefsPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #4
  %61 = bitcast %struct.LZ4F_preferences_t* %prefsNull to i8*
  call void @llvm.lifetime.end.p0i8(i64 56, i8* %61) #4
  ret i32 %add29
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden i32 @LZ4F_compressFrame_usingCDict(%struct.LZ4F_cctx_s* %cctx, i8* %dstBuffer, i32 %dstCapacity, i8* %srcBuffer, i32 %srcSize, %struct.LZ4F_CDict_s* %cdict, %struct.LZ4F_preferences_t* %preferencesPtr) #0 {
entry:
  %retval = alloca i32, align 4
  %cctx.addr = alloca %struct.LZ4F_cctx_s*, align 4
  %dstBuffer.addr = alloca i8*, align 4
  %dstCapacity.addr = alloca i32, align 4
  %srcBuffer.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %cdict.addr = alloca %struct.LZ4F_CDict_s*, align 4
  %preferencesPtr.addr = alloca %struct.LZ4F_preferences_t*, align 4
  %prefs = alloca %struct.LZ4F_preferences_t, align 8
  %options = alloca %struct.LZ4F_compressOptions_t, align 4
  %dstStart = alloca i8*, align 4
  %dstPtr = alloca i8*, align 4
  %dstEnd = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %headerSize = alloca i32, align 4
  %cSize = alloca i32, align 4
  %tailSize = alloca i32, align 4
  store %struct.LZ4F_cctx_s* %cctx, %struct.LZ4F_cctx_s** %cctx.addr, align 4, !tbaa !7
  store i8* %dstBuffer, i8** %dstBuffer.addr, align 4, !tbaa !7
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !3
  store i8* %srcBuffer, i8** %srcBuffer.addr, align 4, !tbaa !7
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !3
  store %struct.LZ4F_CDict_s* %cdict, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  store %struct.LZ4F_preferences_t* %preferencesPtr, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %0 = bitcast %struct.LZ4F_preferences_t* %prefs to i8*
  call void @llvm.lifetime.start.p0i8(i64 56, i8* %0) #4
  %1 = bitcast %struct.LZ4F_compressOptions_t* %options to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #4
  %2 = bitcast i8** %dstStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load i8*, i8** %dstBuffer.addr, align 4, !tbaa !7
  store i8* %3, i8** %dstStart, align 4, !tbaa !7
  %4 = bitcast i8** %dstPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load i8*, i8** %dstStart, align 4, !tbaa !7
  store i8* %5, i8** %dstPtr, align 4, !tbaa !7
  %6 = bitcast i8** %dstEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load i8*, i8** %dstStart, align 4, !tbaa !7
  %8 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %add.ptr = getelementptr inbounds i8, i8* %7, i32 %8
  store i8* %add.ptr, i8** %dstEnd, align 4, !tbaa !7
  %9 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %cmp = icmp ne %struct.LZ4F_preferences_t* %9, null
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %10 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %11 = bitcast %struct.LZ4F_preferences_t* %prefs to i8*
  %12 = bitcast %struct.LZ4F_preferences_t* %10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %11, i8* align 8 %12, i32 56, i1 false), !tbaa.struct !12
  br label %if.end

if.else:                                          ; preds = %entry
  %13 = bitcast %struct.LZ4F_preferences_t* %prefs to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %13, i8 0, i32 56, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %frameInfo = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 0
  %contentSize = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo, i32 0, i32 4
  %14 = load i64, i64* %contentSize, align 8, !tbaa !21
  %cmp1 = icmp ne i64 %14, 0
  br i1 %cmp1, label %if.then2, label %if.end5

if.then2:                                         ; preds = %if.end
  %15 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %conv = zext i32 %15 to i64
  %frameInfo3 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 0
  %contentSize4 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo3, i32 0, i32 4
  store i64 %conv, i64* %contentSize4, align 8, !tbaa !21
  br label %if.end5

if.end5:                                          ; preds = %if.then2, %if.end
  %frameInfo6 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 0
  %blockSizeID = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo6, i32 0, i32 0
  %16 = load i32, i32* %blockSizeID, align 8, !tbaa !20
  %17 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %call = call i32 @LZ4F_optimalBSID(i32 %16, i32 %17)
  %frameInfo7 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 0
  %blockSizeID8 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo7, i32 0, i32 0
  store i32 %call, i32* %blockSizeID8, align 8, !tbaa !20
  %autoFlush = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 2
  store i32 1, i32* %autoFlush, align 4, !tbaa !15
  %18 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %frameInfo9 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 0
  %blockSizeID10 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo9, i32 0, i32 0
  %19 = load i32, i32* %blockSizeID10, align 8, !tbaa !20
  %call11 = call i32 @LZ4F_getBlockSize(i32 %19)
  %cmp12 = icmp ule i32 %18, %call11
  br i1 %cmp12, label %if.then14, label %if.end16

if.then14:                                        ; preds = %if.end5
  %frameInfo15 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 0
  %blockMode = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo15, i32 0, i32 1
  store i32 1, i32* %blockMode, align 4, !tbaa !22
  br label %if.end16

if.end16:                                         ; preds = %if.then14, %if.end5
  %20 = bitcast %struct.LZ4F_compressOptions_t* %options to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %20, i8 0, i32 16, i1 false)
  %stableSrc = getelementptr inbounds %struct.LZ4F_compressOptions_t, %struct.LZ4F_compressOptions_t* %options, i32 0, i32 0
  store i32 1, i32* %stableSrc, align 4, !tbaa !23
  %21 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %22 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %call17 = call i32 @LZ4F_compressFrameBound(i32 %22, %struct.LZ4F_preferences_t* %prefs)
  %cmp18 = icmp ult i32 %21, %call17
  br i1 %cmp18, label %if.then20, label %if.end22

if.then20:                                        ; preds = %if.end16
  %call21 = call i32 @err0r(i32 11)
  store i32 %call21, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup52

if.end22:                                         ; preds = %if.end16
  %23 = bitcast i32* %headerSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctx.addr, align 4, !tbaa !7
  %25 = load i8*, i8** %dstBuffer.addr, align 4, !tbaa !7
  %26 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %27 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %call23 = call i32 @LZ4F_compressBegin_usingCDict(%struct.LZ4F_cctx_s* %24, i8* %25, i32 %26, %struct.LZ4F_CDict_s* %27, %struct.LZ4F_preferences_t* %prefs)
  store i32 %call23, i32* %headerSize, align 4, !tbaa !3
  %28 = load i32, i32* %headerSize, align 4, !tbaa !3
  %call24 = call i32 @LZ4F_isError(i32 %28)
  %tobool = icmp ne i32 %call24, 0
  br i1 %tobool, label %if.then25, label %if.end26

if.then25:                                        ; preds = %if.end22
  %29 = load i32, i32* %headerSize, align 4, !tbaa !3
  store i32 %29, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end26:                                         ; preds = %if.end22
  %30 = load i32, i32* %headerSize, align 4, !tbaa !3
  %31 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %add.ptr27 = getelementptr inbounds i8, i8* %31, i32 %30
  store i8* %add.ptr27, i8** %dstPtr, align 4, !tbaa !7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end26, %if.then25
  %32 = bitcast i32* %headerSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup52 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  %33 = bitcast i32* %cSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  %34 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctx.addr, align 4, !tbaa !7
  %35 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %36 = load i8*, i8** %dstEnd, align 4, !tbaa !7
  %37 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast = ptrtoint i8* %36 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %37 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %38 = load i8*, i8** %srcBuffer.addr, align 4, !tbaa !7
  %39 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %call28 = call i32 @LZ4F_compressUpdate(%struct.LZ4F_cctx_s* %34, i8* %35, i32 %sub.ptr.sub, i8* %38, i32 %39, %struct.LZ4F_compressOptions_t* %options)
  store i32 %call28, i32* %cSize, align 4, !tbaa !3
  %40 = load i32, i32* %cSize, align 4, !tbaa !3
  %call29 = call i32 @LZ4F_isError(i32 %40)
  %tobool30 = icmp ne i32 %call29, 0
  br i1 %tobool30, label %if.then31, label %if.end32

if.then31:                                        ; preds = %cleanup.cont
  %41 = load i32, i32* %cSize, align 4, !tbaa !3
  store i32 %41, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup34

if.end32:                                         ; preds = %cleanup.cont
  %42 = load i32, i32* %cSize, align 4, !tbaa !3
  %43 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %add.ptr33 = getelementptr inbounds i8, i8* %43, i32 %42
  store i8* %add.ptr33, i8** %dstPtr, align 4, !tbaa !7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup34

cleanup34:                                        ; preds = %if.end32, %if.then31
  %44 = bitcast i32* %cSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %cleanup.dest35 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest35, label %cleanup52 [
    i32 0, label %cleanup.cont36
  ]

cleanup.cont36:                                   ; preds = %cleanup34
  %45 = bitcast i32* %tailSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #4
  %46 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctx.addr, align 4, !tbaa !7
  %47 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %48 = load i8*, i8** %dstEnd, align 4, !tbaa !7
  %49 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast37 = ptrtoint i8* %48 to i32
  %sub.ptr.rhs.cast38 = ptrtoint i8* %49 to i32
  %sub.ptr.sub39 = sub i32 %sub.ptr.lhs.cast37, %sub.ptr.rhs.cast38
  %call40 = call i32 @LZ4F_compressEnd(%struct.LZ4F_cctx_s* %46, i8* %47, i32 %sub.ptr.sub39, %struct.LZ4F_compressOptions_t* %options)
  store i32 %call40, i32* %tailSize, align 4, !tbaa !3
  %50 = load i32, i32* %tailSize, align 4, !tbaa !3
  %call41 = call i32 @LZ4F_isError(i32 %50)
  %tobool42 = icmp ne i32 %call41, 0
  br i1 %tobool42, label %if.then43, label %if.end44

if.then43:                                        ; preds = %cleanup.cont36
  %51 = load i32, i32* %tailSize, align 4, !tbaa !3
  store i32 %51, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup46

if.end44:                                         ; preds = %cleanup.cont36
  %52 = load i32, i32* %tailSize, align 4, !tbaa !3
  %53 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %add.ptr45 = getelementptr inbounds i8, i8* %53, i32 %52
  store i8* %add.ptr45, i8** %dstPtr, align 4, !tbaa !7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup46

cleanup46:                                        ; preds = %if.end44, %if.then43
  %54 = bitcast i32* %tailSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #4
  %cleanup.dest47 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest47, label %cleanup52 [
    i32 0, label %cleanup.cont48
  ]

cleanup.cont48:                                   ; preds = %cleanup46
  %55 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %56 = load i8*, i8** %dstStart, align 4, !tbaa !7
  %sub.ptr.lhs.cast49 = ptrtoint i8* %55 to i32
  %sub.ptr.rhs.cast50 = ptrtoint i8* %56 to i32
  %sub.ptr.sub51 = sub i32 %sub.ptr.lhs.cast49, %sub.ptr.rhs.cast50
  store i32 %sub.ptr.sub51, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup52

cleanup52:                                        ; preds = %cleanup.cont48, %cleanup46, %cleanup34, %cleanup, %if.then20
  %57 = bitcast i8** %dstEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #4
  %58 = bitcast i8** %dstPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #4
  %59 = bitcast i8** %dstStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #4
  %60 = bitcast %struct.LZ4F_compressOptions_t* %options to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %60) #4
  %61 = bitcast %struct.LZ4F_preferences_t* %prefs to i8*
  call void @llvm.lifetime.end.p0i8(i64 56, i8* %61) #4
  %62 = load i32, i32* %retval, align 4
  ret i32 %62
}

; Function Attrs: nounwind
define internal i32 @LZ4F_optimalBSID(i32 %requestedBSID, i32 %srcSize) #0 {
entry:
  %retval = alloca i32, align 4
  %requestedBSID.addr = alloca i32, align 4
  %srcSize.addr = alloca i32, align 4
  %proposedBSID = alloca i32, align 4
  %maxBlockSize = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i32 %requestedBSID, i32* %requestedBSID.addr, align 4, !tbaa !11
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !3
  %0 = bitcast i32* %proposedBSID to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 4, i32* %proposedBSID, align 4, !tbaa !11
  %1 = bitcast i32* %maxBlockSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 65536, i32* %maxBlockSize, align 4, !tbaa !3
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %2 = load i32, i32* %requestedBSID.addr, align 4, !tbaa !11
  %3 = load i32, i32* %proposedBSID, align 4, !tbaa !11
  %cmp = icmp ugt i32 %2, %3
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %5 = load i32, i32* %maxBlockSize, align 4, !tbaa !3
  %cmp1 = icmp ule i32 %4, %5
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %6 = load i32, i32* %proposedBSID, align 4, !tbaa !11
  store i32 %6, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %while.body
  %7 = load i32, i32* %proposedBSID, align 4, !tbaa !11
  %add = add nsw i32 %7, 1
  store i32 %add, i32* %proposedBSID, align 4, !tbaa !11
  %8 = load i32, i32* %maxBlockSize, align 4, !tbaa !3
  %shl = shl i32 %8, 2
  store i32 %shl, i32* %maxBlockSize, align 4, !tbaa !3
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %9 = load i32, i32* %requestedBSID.addr, align 4, !tbaa !11
  store i32 %9, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %while.end, %if.then
  %10 = bitcast i32* %maxBlockSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i32* %proposedBSID to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

; Function Attrs: nounwind
define hidden i32 @LZ4F_compressBegin_usingCDict(%struct.LZ4F_cctx_s* %cctxPtr, i8* %dstBuffer, i32 %dstCapacity, %struct.LZ4F_CDict_s* %cdict, %struct.LZ4F_preferences_t* %preferencesPtr) #0 {
entry:
  %retval = alloca i32, align 4
  %cctxPtr.addr = alloca %struct.LZ4F_cctx_s*, align 4
  %dstBuffer.addr = alloca i8*, align 4
  %dstCapacity.addr = alloca i32, align 4
  %cdict.addr = alloca %struct.LZ4F_CDict_s*, align 4
  %preferencesPtr.addr = alloca %struct.LZ4F_preferences_t*, align 4
  %prefNull = alloca %struct.LZ4F_preferences_t, align 8
  %dstStart = alloca i8*, align 4
  %dstPtr = alloca i8*, align 4
  %headerStart = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ctxTypeID = alloca i16, align 2
  %requiredBuffSize = alloca i32, align 4
  store %struct.LZ4F_cctx_s* %cctxPtr, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  store i8* %dstBuffer, i8** %dstBuffer.addr, align 4, !tbaa !7
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !3
  store %struct.LZ4F_CDict_s* %cdict, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  store %struct.LZ4F_preferences_t* %preferencesPtr, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %0 = bitcast %struct.LZ4F_preferences_t* %prefNull to i8*
  call void @llvm.lifetime.start.p0i8(i64 56, i8* %0) #4
  %1 = bitcast i8** %dstStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i8*, i8** %dstBuffer.addr, align 4, !tbaa !7
  store i8* %2, i8** %dstStart, align 4, !tbaa !7
  %3 = bitcast i8** %dstPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load i8*, i8** %dstStart, align 4, !tbaa !7
  store i8* %4, i8** %dstPtr, align 4, !tbaa !7
  %5 = bitcast i8** %headerStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %cmp = icmp ult i32 %6, 19
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call i32 @err0r(i32 11)
  store i32 %call, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup171

if.end:                                           ; preds = %entry
  %7 = bitcast %struct.LZ4F_preferences_t* %prefNull to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %7, i8 0, i32 56, i1 false)
  %8 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %cmp1 = icmp eq %struct.LZ4F_preferences_t* %8, null
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store %struct.LZ4F_preferences_t* %prefNull, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %9 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %9, i32 0, i32 0
  %10 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %11 = bitcast %struct.LZ4F_preferences_t* %prefs to i8*
  %12 = bitcast %struct.LZ4F_preferences_t* %10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %11, i8* align 8 %12, i32 56, i1 false), !tbaa.struct !12
  %13 = bitcast i16* %ctxTypeID to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %13) #4
  %14 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs4 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %14, i32 0, i32 0
  %compressionLevel = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs4, i32 0, i32 1
  %15 = load i32, i32* %compressionLevel, align 8, !tbaa !25
  %cmp5 = icmp slt i32 %15, 3
  %16 = zext i1 %cmp5 to i64
  %cond = select i1 %cmp5, i32 1, i32 2
  %conv = trunc i32 %cond to i16
  store i16 %conv, i16* %ctxTypeID, align 2, !tbaa !29
  %17 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxAlloc = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %17, i32 0, i32 12
  %18 = load i16, i16* %lz4CtxAlloc, align 4, !tbaa !30
  %conv6 = zext i16 %18 to i32
  %19 = load i16, i16* %ctxTypeID, align 2, !tbaa !29
  %conv7 = zext i16 %19 to i32
  %cmp8 = icmp slt i32 %conv6, %conv7
  br i1 %cmp8, label %if.then10, label %if.else28

if.then10:                                        ; preds = %if.end3
  %20 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxPtr = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %20, i32 0, i32 11
  %21 = load i8*, i8** %lz4CtxPtr, align 8, !tbaa !31
  call void @free(i8* %21)
  %22 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs11 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %22, i32 0, i32 0
  %compressionLevel12 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs11, i32 0, i32 1
  %23 = load i32, i32* %compressionLevel12, align 8, !tbaa !25
  %cmp13 = icmp slt i32 %23, 3
  br i1 %cmp13, label %if.then15, label %if.else

if.then15:                                        ; preds = %if.then10
  %call16 = call %union.LZ4_stream_u* @LZ4_createStream()
  %24 = bitcast %union.LZ4_stream_u* %call16 to i8*
  %25 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxPtr17 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %25, i32 0, i32 11
  store i8* %24, i8** %lz4CtxPtr17, align 8, !tbaa !31
  br label %if.end20

if.else:                                          ; preds = %if.then10
  %call18 = call %union.LZ4_streamHC_u* @LZ4_createStreamHC()
  %26 = bitcast %union.LZ4_streamHC_u* %call18 to i8*
  %27 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxPtr19 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %27, i32 0, i32 11
  store i8* %26, i8** %lz4CtxPtr19, align 8, !tbaa !31
  br label %if.end20

if.end20:                                         ; preds = %if.else, %if.then15
  %28 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxPtr21 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %28, i32 0, i32 11
  %29 = load i8*, i8** %lz4CtxPtr21, align 8, !tbaa !31
  %cmp22 = icmp eq i8* %29, null
  br i1 %cmp22, label %if.then24, label %if.end26

if.then24:                                        ; preds = %if.end20
  %call25 = call i32 @err0r(i32 9)
  store i32 %call25, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end26:                                         ; preds = %if.end20
  %30 = load i16, i16* %ctxTypeID, align 2, !tbaa !29
  %31 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxAlloc27 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %31, i32 0, i32 12
  store i16 %30, i16* %lz4CtxAlloc27, align 4, !tbaa !30
  %32 = load i16, i16* %ctxTypeID, align 2, !tbaa !29
  %33 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxState = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %33, i32 0, i32 13
  store i16 %32, i16* %lz4CtxState, align 2, !tbaa !32
  br label %if.end51

if.else28:                                        ; preds = %if.end3
  %34 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxState29 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %34, i32 0, i32 13
  %35 = load i16, i16* %lz4CtxState29, align 2, !tbaa !32
  %conv30 = zext i16 %35 to i32
  %36 = load i16, i16* %ctxTypeID, align 2, !tbaa !29
  %conv31 = zext i16 %36 to i32
  %cmp32 = icmp ne i32 %conv30, %conv31
  br i1 %cmp32, label %if.then34, label %if.end50

if.then34:                                        ; preds = %if.else28
  %37 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs35 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %37, i32 0, i32 0
  %compressionLevel36 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs35, i32 0, i32 1
  %38 = load i32, i32* %compressionLevel36, align 8, !tbaa !25
  %cmp37 = icmp slt i32 %38, 3
  br i1 %cmp37, label %if.then39, label %if.else42

if.then39:                                        ; preds = %if.then34
  %39 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxPtr40 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %39, i32 0, i32 11
  %40 = load i8*, i8** %lz4CtxPtr40, align 8, !tbaa !31
  %41 = bitcast i8* %40 to %union.LZ4_stream_u*
  %42 = bitcast %union.LZ4_stream_u* %41 to i8*
  %call41 = call %union.LZ4_stream_u* @LZ4_initStream(i8* %42, i32 16416)
  br label %if.end48

if.else42:                                        ; preds = %if.then34
  %43 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxPtr43 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %43, i32 0, i32 11
  %44 = load i8*, i8** %lz4CtxPtr43, align 8, !tbaa !31
  %45 = bitcast i8* %44 to %union.LZ4_streamHC_u*
  %46 = bitcast %union.LZ4_streamHC_u* %45 to i8*
  %call44 = call %union.LZ4_streamHC_u* @LZ4_initStreamHC(i8* %46, i32 262200)
  %47 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxPtr45 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %47, i32 0, i32 11
  %48 = load i8*, i8** %lz4CtxPtr45, align 8, !tbaa !31
  %49 = bitcast i8* %48 to %union.LZ4_streamHC_u*
  %50 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs46 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %50, i32 0, i32 0
  %compressionLevel47 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs46, i32 0, i32 1
  %51 = load i32, i32* %compressionLevel47, align 8, !tbaa !25
  call void @LZ4_setCompressionLevel(%union.LZ4_streamHC_u* %49, i32 %51)
  br label %if.end48

if.end48:                                         ; preds = %if.else42, %if.then39
  %52 = load i16, i16* %ctxTypeID, align 2, !tbaa !29
  %53 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxState49 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %53, i32 0, i32 13
  store i16 %52, i16* %lz4CtxState49, align 2, !tbaa !32
  br label %if.end50

if.end50:                                         ; preds = %if.end48, %if.else28
  br label %if.end51

if.end51:                                         ; preds = %if.end50, %if.end26
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end51, %if.then24
  %54 = bitcast i16* %ctxTypeID to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %54) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup171 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  %55 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs52 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %55, i32 0, i32 0
  %frameInfo = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs52, i32 0, i32 0
  %blockSizeID = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo, i32 0, i32 0
  %56 = load i32, i32* %blockSizeID, align 8, !tbaa !33
  %cmp53 = icmp eq i32 %56, 0
  br i1 %cmp53, label %if.then55, label %if.end59

if.then55:                                        ; preds = %cleanup.cont
  %57 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs56 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %57, i32 0, i32 0
  %frameInfo57 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs56, i32 0, i32 0
  %blockSizeID58 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo57, i32 0, i32 0
  store i32 4, i32* %blockSizeID58, align 8, !tbaa !33
  br label %if.end59

if.end59:                                         ; preds = %if.then55, %cleanup.cont
  %58 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs60 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %58, i32 0, i32 0
  %frameInfo61 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs60, i32 0, i32 0
  %blockSizeID62 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo61, i32 0, i32 0
  %59 = load i32, i32* %blockSizeID62, align 8, !tbaa !33
  %call63 = call i32 @LZ4F_getBlockSize(i32 %59)
  %60 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %maxBlockSize = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %60, i32 0, i32 4
  store i32 %call63, i32* %maxBlockSize, align 4, !tbaa !34
  %61 = bitcast i32* %requiredBuffSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #4
  %62 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %autoFlush = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %62, i32 0, i32 2
  %63 = load i32, i32* %autoFlush, align 4, !tbaa !15
  %tobool = icmp ne i32 %63, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end59
  %64 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs64 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %64, i32 0, i32 0
  %frameInfo65 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs64, i32 0, i32 0
  %blockMode = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo65, i32 0, i32 1
  %65 = load i32, i32* %blockMode, align 4, !tbaa !35
  %cmp66 = icmp eq i32 %65, 0
  %66 = zext i1 %cmp66 to i64
  %cond68 = select i1 %cmp66, i32 65536, i32 0
  br label %cond.end

cond.false:                                       ; preds = %if.end59
  %67 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %maxBlockSize69 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %67, i32 0, i32 4
  %68 = load i32, i32* %maxBlockSize69, align 4, !tbaa !34
  %69 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs70 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %69, i32 0, i32 0
  %frameInfo71 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs70, i32 0, i32 0
  %blockMode72 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo71, i32 0, i32 1
  %70 = load i32, i32* %blockMode72, align 4, !tbaa !35
  %cmp73 = icmp eq i32 %70, 0
  %71 = zext i1 %cmp73 to i64
  %cond75 = select i1 %cmp73, i32 131072, i32 0
  %add = add i32 %68, %cond75
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond76 = phi i32 [ %cond68, %cond.true ], [ %add, %cond.false ]
  store i32 %cond76, i32* %requiredBuffSize, align 4, !tbaa !3
  %72 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %maxBufferSize = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %72, i32 0, i32 5
  %73 = load i32, i32* %maxBufferSize, align 8, !tbaa !36
  %74 = load i32, i32* %requiredBuffSize, align 4, !tbaa !3
  %cmp77 = icmp ult i32 %73, %74
  br i1 %cmp77, label %if.then79, label %if.end90

if.then79:                                        ; preds = %cond.end
  %75 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %maxBufferSize80 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %75, i32 0, i32 5
  store i32 0, i32* %maxBufferSize80, align 8, !tbaa !36
  %76 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpBuff = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %76, i32 0, i32 6
  %77 = load i8*, i8** %tmpBuff, align 4, !tbaa !37
  call void @free(i8* %77)
  %78 = load i32, i32* %requiredBuffSize, align 4, !tbaa !3
  %call81 = call i8* @calloc(i32 1, i32 %78)
  %79 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpBuff82 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %79, i32 0, i32 6
  store i8* %call81, i8** %tmpBuff82, align 4, !tbaa !37
  %80 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpBuff83 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %80, i32 0, i32 6
  %81 = load i8*, i8** %tmpBuff83, align 4, !tbaa !37
  %cmp84 = icmp eq i8* %81, null
  br i1 %cmp84, label %if.then86, label %if.end88

if.then86:                                        ; preds = %if.then79
  %call87 = call i32 @err0r(i32 9)
  store i32 %call87, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup91

if.end88:                                         ; preds = %if.then79
  %82 = load i32, i32* %requiredBuffSize, align 4, !tbaa !3
  %83 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %maxBufferSize89 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %83, i32 0, i32 5
  store i32 %82, i32* %maxBufferSize89, align 8, !tbaa !36
  br label %if.end90

if.end90:                                         ; preds = %if.end88, %cond.end
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup91

cleanup91:                                        ; preds = %if.end90, %if.then86
  %84 = bitcast i32* %requiredBuffSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %cleanup.dest92 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest92, label %cleanup171 [
    i32 0, label %cleanup.cont93
  ]

cleanup.cont93:                                   ; preds = %cleanup91
  %85 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpBuff94 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %85, i32 0, i32 6
  %86 = load i8*, i8** %tmpBuff94, align 4, !tbaa !37
  %87 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpIn = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %87, i32 0, i32 7
  store i8* %86, i8** %tmpIn, align 8, !tbaa !38
  %88 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpInSize = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %88, i32 0, i32 8
  store i32 0, i32* %tmpInSize, align 4, !tbaa !39
  %89 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %xxh = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %89, i32 0, i32 10
  %call95 = call i32 @LZ4_XXH32_reset(%struct.XXH32_state_s* %xxh, i32 0)
  %90 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %91 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %cdict96 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %91, i32 0, i32 3
  store %struct.LZ4F_CDict_s* %90, %struct.LZ4F_CDict_s** %cdict96, align 8, !tbaa !40
  %92 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs97 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %92, i32 0, i32 0
  %frameInfo98 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs97, i32 0, i32 0
  %blockMode99 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo98, i32 0, i32 1
  %93 = load i32, i32* %blockMode99, align 4, !tbaa !35
  %cmp100 = icmp eq i32 %93, 0
  br i1 %cmp100, label %if.then102, label %if.end106

if.then102:                                       ; preds = %cleanup.cont93
  %94 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxPtr103 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %94, i32 0, i32 11
  %95 = load i8*, i8** %lz4CtxPtr103, align 8, !tbaa !31
  %96 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %97 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs104 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %97, i32 0, i32 0
  %compressionLevel105 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs104, i32 0, i32 1
  %98 = load i32, i32* %compressionLevel105, align 8, !tbaa !25
  call void @LZ4F_initStream(i8* %95, %struct.LZ4F_CDict_s* %96, i32 %98, i32 0)
  br label %if.end106

if.end106:                                        ; preds = %if.then102, %cleanup.cont93
  %99 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %compressionLevel107 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %99, i32 0, i32 1
  %100 = load i32, i32* %compressionLevel107, align 8, !tbaa !41
  %cmp108 = icmp sge i32 %100, 3
  br i1 %cmp108, label %if.then110, label %if.end112

if.then110:                                       ; preds = %if.end106
  %101 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxPtr111 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %101, i32 0, i32 11
  %102 = load i8*, i8** %lz4CtxPtr111, align 8, !tbaa !31
  %103 = bitcast i8* %102 to %union.LZ4_streamHC_u*
  %104 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %favorDecSpeed = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %104, i32 0, i32 3
  %105 = load i32, i32* %favorDecSpeed, align 8, !tbaa !42
  call void @LZ4_favorDecompressionSpeed(%union.LZ4_streamHC_u* %103, i32 %105)
  br label %if.end112

if.end112:                                        ; preds = %if.then110, %if.end106
  %106 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  call void @LZ4F_writeLE32(i8* %106, i32 407708164)
  %107 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds i8, i8* %107, i32 4
  store i8* %add.ptr, i8** %dstPtr, align 4, !tbaa !7
  %108 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  store i8* %108, i8** %headerStart, align 4, !tbaa !7
  %109 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs113 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %109, i32 0, i32 0
  %frameInfo114 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs113, i32 0, i32 0
  %blockMode115 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo114, i32 0, i32 1
  %110 = load i32, i32* %blockMode115, align 4, !tbaa !35
  %and = and i32 %110, 1
  %shl = shl i32 %and, 5
  %add116 = add i32 64, %shl
  %111 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs117 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %111, i32 0, i32 0
  %frameInfo118 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs117, i32 0, i32 0
  %blockChecksumFlag = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo118, i32 0, i32 6
  %112 = load i32, i32* %blockChecksumFlag, align 4, !tbaa !43
  %and119 = and i32 %112, 1
  %shl120 = shl i32 %and119, 4
  %add121 = add i32 %add116, %shl120
  %113 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs122 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %113, i32 0, i32 0
  %frameInfo123 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs122, i32 0, i32 0
  %contentSize = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo123, i32 0, i32 4
  %114 = load i64, i64* %contentSize, align 8, !tbaa !44
  %cmp124 = icmp ugt i64 %114, 0
  %conv125 = zext i1 %cmp124 to i32
  %shl126 = shl i32 %conv125, 3
  %add127 = add i32 %add121, %shl126
  %115 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs128 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %115, i32 0, i32 0
  %frameInfo129 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs128, i32 0, i32 0
  %contentChecksumFlag = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo129, i32 0, i32 2
  %116 = load i32, i32* %contentChecksumFlag, align 8, !tbaa !45
  %and130 = and i32 %116, 1
  %shl131 = shl i32 %and130, 2
  %add132 = add i32 %add127, %shl131
  %117 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs133 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %117, i32 0, i32 0
  %frameInfo134 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs133, i32 0, i32 0
  %dictID = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo134, i32 0, i32 5
  %118 = load i32, i32* %dictID, align 8, !tbaa !46
  %cmp135 = icmp ugt i32 %118, 0
  %conv136 = zext i1 %cmp135 to i32
  %add137 = add i32 %add132, %conv136
  %conv138 = trunc i32 %add137 to i8
  %119 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %incdec.ptr = getelementptr inbounds i8, i8* %119, i32 1
  store i8* %incdec.ptr, i8** %dstPtr, align 4, !tbaa !7
  store i8 %conv138, i8* %119, align 1, !tbaa !11
  %120 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs139 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %120, i32 0, i32 0
  %frameInfo140 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs139, i32 0, i32 0
  %blockSizeID141 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo140, i32 0, i32 0
  %121 = load i32, i32* %blockSizeID141, align 8, !tbaa !33
  %and142 = and i32 %121, 7
  %shl143 = shl i32 %and142, 4
  %conv144 = trunc i32 %shl143 to i8
  %122 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %incdec.ptr145 = getelementptr inbounds i8, i8* %122, i32 1
  store i8* %incdec.ptr145, i8** %dstPtr, align 4, !tbaa !7
  store i8 %conv144, i8* %122, align 1, !tbaa !11
  %123 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs146 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %123, i32 0, i32 0
  %frameInfo147 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs146, i32 0, i32 0
  %contentSize148 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo147, i32 0, i32 4
  %124 = load i64, i64* %contentSize148, align 8, !tbaa !44
  %tobool149 = icmp ne i64 %124, 0
  br i1 %tobool149, label %if.then150, label %if.end155

if.then150:                                       ; preds = %if.end112
  %125 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %126 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs151 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %126, i32 0, i32 0
  %frameInfo152 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs151, i32 0, i32 0
  %contentSize153 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo152, i32 0, i32 4
  %127 = load i64, i64* %contentSize153, align 8, !tbaa !44
  call void @LZ4F_writeLE64(i8* %125, i64 %127)
  %128 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %add.ptr154 = getelementptr inbounds i8, i8* %128, i32 8
  store i8* %add.ptr154, i8** %dstPtr, align 4, !tbaa !7
  %129 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %totalInSize = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %129, i32 0, i32 9
  store i64 0, i64* %totalInSize, align 8, !tbaa !47
  br label %if.end155

if.end155:                                        ; preds = %if.then150, %if.end112
  %130 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs156 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %130, i32 0, i32 0
  %frameInfo157 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs156, i32 0, i32 0
  %dictID158 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo157, i32 0, i32 5
  %131 = load i32, i32* %dictID158, align 8, !tbaa !46
  %tobool159 = icmp ne i32 %131, 0
  br i1 %tobool159, label %if.then160, label %if.end165

if.then160:                                       ; preds = %if.end155
  %132 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %133 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs161 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %133, i32 0, i32 0
  %frameInfo162 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs161, i32 0, i32 0
  %dictID163 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo162, i32 0, i32 5
  %134 = load i32, i32* %dictID163, align 8, !tbaa !46
  call void @LZ4F_writeLE32(i8* %132, i32 %134)
  %135 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %add.ptr164 = getelementptr inbounds i8, i8* %135, i32 4
  store i8* %add.ptr164, i8** %dstPtr, align 4, !tbaa !7
  br label %if.end165

if.end165:                                        ; preds = %if.then160, %if.end155
  %136 = load i8*, i8** %headerStart, align 4, !tbaa !7
  %137 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %138 = load i8*, i8** %headerStart, align 4, !tbaa !7
  %sub.ptr.lhs.cast = ptrtoint i8* %137 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %138 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %call166 = call zeroext i8 @LZ4F_headerChecksum(i8* %136, i32 %sub.ptr.sub)
  %139 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  store i8 %call166, i8* %139, align 1, !tbaa !11
  %140 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %incdec.ptr167 = getelementptr inbounds i8, i8* %140, i32 1
  store i8* %incdec.ptr167, i8** %dstPtr, align 4, !tbaa !7
  %141 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %cStage = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %141, i32 0, i32 2
  store i32 1, i32* %cStage, align 4, !tbaa !48
  %142 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %143 = load i8*, i8** %dstStart, align 4, !tbaa !7
  %sub.ptr.lhs.cast168 = ptrtoint i8* %142 to i32
  %sub.ptr.rhs.cast169 = ptrtoint i8* %143 to i32
  %sub.ptr.sub170 = sub i32 %sub.ptr.lhs.cast168, %sub.ptr.rhs.cast169
  store i32 %sub.ptr.sub170, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup171

cleanup171:                                       ; preds = %if.end165, %cleanup91, %cleanup, %if.then
  %144 = bitcast i8** %headerStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #4
  %145 = bitcast i8** %dstPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #4
  %146 = bitcast i8** %dstStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #4
  %147 = bitcast %struct.LZ4F_preferences_t* %prefNull to i8*
  call void @llvm.lifetime.end.p0i8(i64 56, i8* %147) #4
  %148 = load i32, i32* %retval, align 4
  ret i32 %148
}

; Function Attrs: nounwind
define i32 @LZ4F_compressUpdate(%struct.LZ4F_cctx_s* %cctxPtr, i8* %dstBuffer, i32 %dstCapacity, i8* %srcBuffer, i32 %srcSize, %struct.LZ4F_compressOptions_t* %compressOptionsPtr) #0 {
entry:
  %retval = alloca i32, align 4
  %cctxPtr.addr = alloca %struct.LZ4F_cctx_s*, align 4
  %dstBuffer.addr = alloca i8*, align 4
  %dstCapacity.addr = alloca i32, align 4
  %srcBuffer.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %compressOptionsPtr.addr = alloca %struct.LZ4F_compressOptions_t*, align 4
  %cOptionsNull = alloca %struct.LZ4F_compressOptions_t, align 4
  %blockSize = alloca i32, align 4
  %srcPtr = alloca i8*, align 4
  %srcEnd = alloca i8*, align 4
  %dstStart = alloca i8*, align 4
  %dstPtr = alloca i8*, align 4
  %lastBlockCompressed = alloca i32, align 4
  %compress = alloca i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %sizeToCopy = alloca i32, align 4
  %realDictSize = alloca i32, align 4
  %realDictSize101 = alloca i32, align 4
  %sizeToCopy109 = alloca i32, align 4
  store %struct.LZ4F_cctx_s* %cctxPtr, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  store i8* %dstBuffer, i8** %dstBuffer.addr, align 4, !tbaa !7
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !3
  store i8* %srcBuffer, i8** %srcBuffer.addr, align 4, !tbaa !7
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !3
  store %struct.LZ4F_compressOptions_t* %compressOptionsPtr, %struct.LZ4F_compressOptions_t** %compressOptionsPtr.addr, align 4, !tbaa !7
  %0 = bitcast %struct.LZ4F_compressOptions_t* %cOptionsNull to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #4
  %1 = bitcast i32* %blockSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %maxBlockSize = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %2, i32 0, i32 4
  %3 = load i32, i32* %maxBlockSize, align 4, !tbaa !34
  store i32 %3, i32* %blockSize, align 4, !tbaa !3
  %4 = bitcast i8** %srcPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load i8*, i8** %srcBuffer.addr, align 4, !tbaa !7
  store i8* %5, i8** %srcPtr, align 4, !tbaa !7
  %6 = bitcast i8** %srcEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %8 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %add.ptr = getelementptr inbounds i8, i8* %7, i32 %8
  store i8* %add.ptr, i8** %srcEnd, align 4, !tbaa !7
  %9 = bitcast i8** %dstStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load i8*, i8** %dstBuffer.addr, align 4, !tbaa !7
  store i8* %10, i8** %dstStart, align 4, !tbaa !7
  %11 = bitcast i8** %dstPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load i8*, i8** %dstStart, align 4, !tbaa !7
  store i8* %12, i8** %dstPtr, align 4, !tbaa !7
  %13 = bitcast i32* %lastBlockCompressed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  store i32 0, i32* %lastBlockCompressed, align 4, !tbaa !11
  %14 = bitcast i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)** %compress to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %15, i32 0, i32 0
  %frameInfo = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 0
  %blockMode = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo, i32 0, i32 1
  %16 = load i32, i32* %blockMode, align 4, !tbaa !35
  %17 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs1 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %17, i32 0, i32 0
  %compressionLevel = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs1, i32 0, i32 1
  %18 = load i32, i32* %compressionLevel, align 8, !tbaa !25
  %call = call i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)* @LZ4F_selectCompression(i32 %16, i32 %18)
  store i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)* %call, i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)** %compress, align 4, !tbaa !7
  %19 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %cStage = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %19, i32 0, i32 2
  %20 = load i32, i32* %cStage, align 4, !tbaa !48
  %cmp = icmp ne i32 %20, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call2 = call i32 @err0r(i32 1)
  store i32 %call2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup126

if.end:                                           ; preds = %entry
  %21 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %22 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %23 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs3 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %23, i32 0, i32 0
  %24 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpInSize = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %24, i32 0, i32 8
  %25 = load i32, i32* %tmpInSize, align 4, !tbaa !39
  %call4 = call i32 @LZ4F_compressBound_internal(i32 %22, %struct.LZ4F_preferences_t* %prefs3, i32 %25)
  %cmp5 = icmp ult i32 %21, %call4
  br i1 %cmp5, label %if.then6, label %if.end8

if.then6:                                         ; preds = %if.end
  %call7 = call i32 @err0r(i32 11)
  store i32 %call7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup126

if.end8:                                          ; preds = %if.end
  %26 = bitcast %struct.LZ4F_compressOptions_t* %cOptionsNull to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %26, i8 0, i32 16, i1 false)
  %27 = load %struct.LZ4F_compressOptions_t*, %struct.LZ4F_compressOptions_t** %compressOptionsPtr.addr, align 4, !tbaa !7
  %cmp9 = icmp eq %struct.LZ4F_compressOptions_t* %27, null
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end8
  store %struct.LZ4F_compressOptions_t* %cOptionsNull, %struct.LZ4F_compressOptions_t** %compressOptionsPtr.addr, align 4, !tbaa !7
  br label %if.end11

if.end11:                                         ; preds = %if.then10, %if.end8
  %28 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpInSize12 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %28, i32 0, i32 8
  %29 = load i32, i32* %tmpInSize12, align 4, !tbaa !39
  %cmp13 = icmp ugt i32 %29, 0
  br i1 %cmp13, label %if.then14, label %if.end42

if.then14:                                        ; preds = %if.end11
  %30 = bitcast i32* %sizeToCopy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  %31 = load i32, i32* %blockSize, align 4, !tbaa !3
  %32 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpInSize15 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %32, i32 0, i32 8
  %33 = load i32, i32* %tmpInSize15, align 4, !tbaa !39
  %sub = sub i32 %31, %33
  store i32 %sub, i32* %sizeToCopy, align 4, !tbaa !3
  %34 = load i32, i32* %sizeToCopy, align 4, !tbaa !3
  %35 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %cmp16 = icmp ugt i32 %34, %35
  br i1 %cmp16, label %if.then17, label %if.else

if.then17:                                        ; preds = %if.then14
  %36 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpIn = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %36, i32 0, i32 7
  %37 = load i8*, i8** %tmpIn, align 8, !tbaa !38
  %38 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpInSize18 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %38, i32 0, i32 8
  %39 = load i32, i32* %tmpInSize18, align 4, !tbaa !39
  %add.ptr19 = getelementptr inbounds i8, i8* %37, i32 %39
  %40 = load i8*, i8** %srcBuffer.addr, align 4, !tbaa !7
  %41 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr19, i8* align 1 %40, i32 %41, i1 false)
  %42 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  store i8* %42, i8** %srcPtr, align 4, !tbaa !7
  %43 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %44 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpInSize20 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %44, i32 0, i32 8
  %45 = load i32, i32* %tmpInSize20, align 4, !tbaa !39
  %add = add i32 %45, %43
  store i32 %add, i32* %tmpInSize20, align 4, !tbaa !39
  br label %if.end41

if.else:                                          ; preds = %if.then14
  store i32 1, i32* %lastBlockCompressed, align 4, !tbaa !11
  %46 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpIn21 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %46, i32 0, i32 7
  %47 = load i8*, i8** %tmpIn21, align 8, !tbaa !38
  %48 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpInSize22 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %48, i32 0, i32 8
  %49 = load i32, i32* %tmpInSize22, align 4, !tbaa !39
  %add.ptr23 = getelementptr inbounds i8, i8* %47, i32 %49
  %50 = load i8*, i8** %srcBuffer.addr, align 4, !tbaa !7
  %51 = load i32, i32* %sizeToCopy, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr23, i8* align 1 %50, i32 %51, i1 false)
  %52 = load i32, i32* %sizeToCopy, align 4, !tbaa !3
  %53 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %add.ptr24 = getelementptr inbounds i8, i8* %53, i32 %52
  store i8* %add.ptr24, i8** %srcPtr, align 4, !tbaa !7
  %54 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %55 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpIn25 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %55, i32 0, i32 7
  %56 = load i8*, i8** %tmpIn25, align 8, !tbaa !38
  %57 = load i32, i32* %blockSize, align 4, !tbaa !3
  %58 = load i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)*, i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)** %compress, align 4, !tbaa !7
  %59 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxPtr = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %59, i32 0, i32 11
  %60 = load i8*, i8** %lz4CtxPtr, align 8, !tbaa !31
  %61 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs26 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %61, i32 0, i32 0
  %compressionLevel27 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs26, i32 0, i32 1
  %62 = load i32, i32* %compressionLevel27, align 8, !tbaa !25
  %63 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %cdict = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %63, i32 0, i32 3
  %64 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 8, !tbaa !40
  %65 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs28 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %65, i32 0, i32 0
  %frameInfo29 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs28, i32 0, i32 0
  %blockChecksumFlag = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo29, i32 0, i32 6
  %66 = load i32, i32* %blockChecksumFlag, align 4, !tbaa !43
  %call30 = call i32 @LZ4F_makeBlock(i8* %54, i8* %56, i32 %57, i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)* %58, i8* %60, i32 %62, %struct.LZ4F_CDict_s* %64, i32 %66)
  %67 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %add.ptr31 = getelementptr inbounds i8, i8* %67, i32 %call30
  store i8* %add.ptr31, i8** %dstPtr, align 4, !tbaa !7
  %68 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs32 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %68, i32 0, i32 0
  %frameInfo33 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs32, i32 0, i32 0
  %blockMode34 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo33, i32 0, i32 1
  %69 = load i32, i32* %blockMode34, align 4, !tbaa !35
  %cmp35 = icmp eq i32 %69, 0
  br i1 %cmp35, label %if.then36, label %if.end39

if.then36:                                        ; preds = %if.else
  %70 = load i32, i32* %blockSize, align 4, !tbaa !3
  %71 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpIn37 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %71, i32 0, i32 7
  %72 = load i8*, i8** %tmpIn37, align 8, !tbaa !38
  %add.ptr38 = getelementptr inbounds i8, i8* %72, i32 %70
  store i8* %add.ptr38, i8** %tmpIn37, align 8, !tbaa !38
  br label %if.end39

if.end39:                                         ; preds = %if.then36, %if.else
  %73 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpInSize40 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %73, i32 0, i32 8
  store i32 0, i32* %tmpInSize40, align 4, !tbaa !39
  br label %if.end41

if.end41:                                         ; preds = %if.end39, %if.then17
  %74 = bitcast i32* %sizeToCopy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #4
  br label %if.end42

if.end42:                                         ; preds = %if.end41, %if.end11
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end42
  %75 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %76 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast = ptrtoint i8* %75 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %76 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %77 = load i32, i32* %blockSize, align 4, !tbaa !3
  %cmp43 = icmp uge i32 %sub.ptr.sub, %77
  br i1 %cmp43, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  store i32 2, i32* %lastBlockCompressed, align 4, !tbaa !11
  %78 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %79 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %80 = load i32, i32* %blockSize, align 4, !tbaa !3
  %81 = load i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)*, i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)** %compress, align 4, !tbaa !7
  %82 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxPtr44 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %82, i32 0, i32 11
  %83 = load i8*, i8** %lz4CtxPtr44, align 8, !tbaa !31
  %84 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs45 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %84, i32 0, i32 0
  %compressionLevel46 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs45, i32 0, i32 1
  %85 = load i32, i32* %compressionLevel46, align 8, !tbaa !25
  %86 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %cdict47 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %86, i32 0, i32 3
  %87 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict47, align 8, !tbaa !40
  %88 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs48 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %88, i32 0, i32 0
  %frameInfo49 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs48, i32 0, i32 0
  %blockChecksumFlag50 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo49, i32 0, i32 6
  %89 = load i32, i32* %blockChecksumFlag50, align 4, !tbaa !43
  %call51 = call i32 @LZ4F_makeBlock(i8* %78, i8* %79, i32 %80, i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)* %81, i8* %83, i32 %85, %struct.LZ4F_CDict_s* %87, i32 %89)
  %90 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %add.ptr52 = getelementptr inbounds i8, i8* %90, i32 %call51
  store i8* %add.ptr52, i8** %dstPtr, align 4, !tbaa !7
  %91 = load i32, i32* %blockSize, align 4, !tbaa !3
  %92 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %add.ptr53 = getelementptr inbounds i8, i8* %92, i32 %91
  store i8* %add.ptr53, i8** %srcPtr, align 4, !tbaa !7
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %93 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs54 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %93, i32 0, i32 0
  %autoFlush = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs54, i32 0, i32 2
  %94 = load i32, i32* %autoFlush, align 4, !tbaa !49
  %tobool = icmp ne i32 %94, 0
  br i1 %tobool, label %land.lhs.true, label %if.end69

land.lhs.true:                                    ; preds = %while.end
  %95 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %96 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %cmp55 = icmp ult i8* %95, %96
  br i1 %cmp55, label %if.then56, label %if.end69

if.then56:                                        ; preds = %land.lhs.true
  store i32 2, i32* %lastBlockCompressed, align 4, !tbaa !11
  %97 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %98 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %99 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %100 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast57 = ptrtoint i8* %99 to i32
  %sub.ptr.rhs.cast58 = ptrtoint i8* %100 to i32
  %sub.ptr.sub59 = sub i32 %sub.ptr.lhs.cast57, %sub.ptr.rhs.cast58
  %101 = load i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)*, i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)** %compress, align 4, !tbaa !7
  %102 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxPtr60 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %102, i32 0, i32 11
  %103 = load i8*, i8** %lz4CtxPtr60, align 8, !tbaa !31
  %104 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs61 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %104, i32 0, i32 0
  %compressionLevel62 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs61, i32 0, i32 1
  %105 = load i32, i32* %compressionLevel62, align 8, !tbaa !25
  %106 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %cdict63 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %106, i32 0, i32 3
  %107 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict63, align 8, !tbaa !40
  %108 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs64 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %108, i32 0, i32 0
  %frameInfo65 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs64, i32 0, i32 0
  %blockChecksumFlag66 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo65, i32 0, i32 6
  %109 = load i32, i32* %blockChecksumFlag66, align 4, !tbaa !43
  %call67 = call i32 @LZ4F_makeBlock(i8* %97, i8* %98, i32 %sub.ptr.sub59, i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)* %101, i8* %103, i32 %105, %struct.LZ4F_CDict_s* %107, i32 %109)
  %110 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %add.ptr68 = getelementptr inbounds i8, i8* %110, i32 %call67
  store i8* %add.ptr68, i8** %dstPtr, align 4, !tbaa !7
  %111 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  store i8* %111, i8** %srcPtr, align 4, !tbaa !7
  br label %if.end69

if.end69:                                         ; preds = %if.then56, %land.lhs.true, %while.end
  %112 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs70 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %112, i32 0, i32 0
  %frameInfo71 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs70, i32 0, i32 0
  %blockMode72 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo71, i32 0, i32 1
  %113 = load i32, i32* %blockMode72, align 4, !tbaa !35
  %cmp73 = icmp eq i32 %113, 0
  br i1 %cmp73, label %land.lhs.true74, label %if.end90

land.lhs.true74:                                  ; preds = %if.end69
  %114 = load i32, i32* %lastBlockCompressed, align 4, !tbaa !11
  %cmp75 = icmp eq i32 %114, 2
  br i1 %cmp75, label %if.then76, label %if.end90

if.then76:                                        ; preds = %land.lhs.true74
  %115 = load %struct.LZ4F_compressOptions_t*, %struct.LZ4F_compressOptions_t** %compressOptionsPtr.addr, align 4, !tbaa !7
  %stableSrc = getelementptr inbounds %struct.LZ4F_compressOptions_t, %struct.LZ4F_compressOptions_t* %115, i32 0, i32 0
  %116 = load i32, i32* %stableSrc, align 4, !tbaa !23
  %tobool77 = icmp ne i32 %116, 0
  br i1 %tobool77, label %if.then78, label %if.else80

if.then78:                                        ; preds = %if.then76
  %117 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpBuff = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %117, i32 0, i32 6
  %118 = load i8*, i8** %tmpBuff, align 4, !tbaa !37
  %119 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpIn79 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %119, i32 0, i32 7
  store i8* %118, i8** %tmpIn79, align 8, !tbaa !38
  br label %if.end89

if.else80:                                        ; preds = %if.then76
  %120 = bitcast i32* %realDictSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %120) #4
  %121 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %call81 = call i32 @LZ4F_localSaveDict(%struct.LZ4F_cctx_s* %121)
  store i32 %call81, i32* %realDictSize, align 4, !tbaa !9
  %122 = load i32, i32* %realDictSize, align 4, !tbaa !9
  %cmp82 = icmp eq i32 %122, 0
  br i1 %cmp82, label %if.then83, label %if.end85

if.then83:                                        ; preds = %if.else80
  %call84 = call i32 @err0r(i32 1)
  store i32 %call84, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end85:                                         ; preds = %if.else80
  %123 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpBuff86 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %123, i32 0, i32 6
  %124 = load i8*, i8** %tmpBuff86, align 4, !tbaa !37
  %125 = load i32, i32* %realDictSize, align 4, !tbaa !9
  %add.ptr87 = getelementptr inbounds i8, i8* %124, i32 %125
  %126 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpIn88 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %126, i32 0, i32 7
  store i8* %add.ptr87, i8** %tmpIn88, align 8, !tbaa !38
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end85, %if.then83
  %127 = bitcast i32* %realDictSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup126 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end89

if.end89:                                         ; preds = %cleanup.cont, %if.then78
  br label %if.end90

if.end90:                                         ; preds = %if.end89, %land.lhs.true74, %if.end69
  %128 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpIn91 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %128, i32 0, i32 7
  %129 = load i8*, i8** %tmpIn91, align 8, !tbaa !38
  %130 = load i32, i32* %blockSize, align 4, !tbaa !3
  %add.ptr92 = getelementptr inbounds i8, i8* %129, i32 %130
  %131 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpBuff93 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %131, i32 0, i32 6
  %132 = load i8*, i8** %tmpBuff93, align 4, !tbaa !37
  %133 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %maxBufferSize = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %133, i32 0, i32 5
  %134 = load i32, i32* %maxBufferSize, align 8, !tbaa !36
  %add.ptr94 = getelementptr inbounds i8, i8* %132, i32 %134
  %cmp95 = icmp ugt i8* %add.ptr92, %add.ptr94
  br i1 %cmp95, label %land.lhs.true96, label %if.end106

land.lhs.true96:                                  ; preds = %if.end90
  %135 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs97 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %135, i32 0, i32 0
  %autoFlush98 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs97, i32 0, i32 2
  %136 = load i32, i32* %autoFlush98, align 4, !tbaa !49
  %tobool99 = icmp ne i32 %136, 0
  br i1 %tobool99, label %if.end106, label %if.then100

if.then100:                                       ; preds = %land.lhs.true96
  %137 = bitcast i32* %realDictSize101 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %137) #4
  %138 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %call102 = call i32 @LZ4F_localSaveDict(%struct.LZ4F_cctx_s* %138)
  store i32 %call102, i32* %realDictSize101, align 4, !tbaa !9
  %139 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpBuff103 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %139, i32 0, i32 6
  %140 = load i8*, i8** %tmpBuff103, align 4, !tbaa !37
  %141 = load i32, i32* %realDictSize101, align 4, !tbaa !9
  %add.ptr104 = getelementptr inbounds i8, i8* %140, i32 %141
  %142 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpIn105 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %142, i32 0, i32 7
  store i8* %add.ptr104, i8** %tmpIn105, align 8, !tbaa !38
  %143 = bitcast i32* %realDictSize101 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #4
  br label %if.end106

if.end106:                                        ; preds = %if.then100, %land.lhs.true96, %if.end90
  %144 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %145 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %cmp107 = icmp ult i8* %144, %145
  br i1 %cmp107, label %if.then108, label %if.end115

if.then108:                                       ; preds = %if.end106
  %146 = bitcast i32* %sizeToCopy109 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %146) #4
  %147 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %148 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast110 = ptrtoint i8* %147 to i32
  %sub.ptr.rhs.cast111 = ptrtoint i8* %148 to i32
  %sub.ptr.sub112 = sub i32 %sub.ptr.lhs.cast110, %sub.ptr.rhs.cast111
  store i32 %sub.ptr.sub112, i32* %sizeToCopy109, align 4, !tbaa !3
  %149 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpIn113 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %149, i32 0, i32 7
  %150 = load i8*, i8** %tmpIn113, align 8, !tbaa !38
  %151 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %152 = load i32, i32* %sizeToCopy109, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %150, i8* align 1 %151, i32 %152, i1 false)
  %153 = load i32, i32* %sizeToCopy109, align 4, !tbaa !3
  %154 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpInSize114 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %154, i32 0, i32 8
  store i32 %153, i32* %tmpInSize114, align 4, !tbaa !39
  %155 = bitcast i32* %sizeToCopy109 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #4
  br label %if.end115

if.end115:                                        ; preds = %if.then108, %if.end106
  %156 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs116 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %156, i32 0, i32 0
  %frameInfo117 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs116, i32 0, i32 0
  %contentChecksumFlag = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo117, i32 0, i32 2
  %157 = load i32, i32* %contentChecksumFlag, align 8, !tbaa !45
  %cmp118 = icmp eq i32 %157, 1
  br i1 %cmp118, label %if.then119, label %if.end121

if.then119:                                       ; preds = %if.end115
  %158 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %xxh = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %158, i32 0, i32 10
  %159 = load i8*, i8** %srcBuffer.addr, align 4, !tbaa !7
  %160 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %call120 = call i32 @LZ4_XXH32_update(%struct.XXH32_state_s* %xxh, i8* %159, i32 %160)
  br label %if.end121

if.end121:                                        ; preds = %if.then119, %if.end115
  %161 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %conv = zext i32 %161 to i64
  %162 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %totalInSize = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %162, i32 0, i32 9
  %163 = load i64, i64* %totalInSize, align 8, !tbaa !47
  %add122 = add i64 %163, %conv
  store i64 %add122, i64* %totalInSize, align 8, !tbaa !47
  %164 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %165 = load i8*, i8** %dstStart, align 4, !tbaa !7
  %sub.ptr.lhs.cast123 = ptrtoint i8* %164 to i32
  %sub.ptr.rhs.cast124 = ptrtoint i8* %165 to i32
  %sub.ptr.sub125 = sub i32 %sub.ptr.lhs.cast123, %sub.ptr.rhs.cast124
  store i32 %sub.ptr.sub125, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup126

cleanup126:                                       ; preds = %if.end121, %cleanup, %if.then6, %if.then
  %166 = bitcast i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)** %compress to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #4
  %167 = bitcast i32* %lastBlockCompressed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #4
  %168 = bitcast i8** %dstPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #4
  %169 = bitcast i8** %dstStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #4
  %170 = bitcast i8** %srcEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #4
  %171 = bitcast i8** %srcPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #4
  %172 = bitcast i32* %blockSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #4
  %173 = bitcast %struct.LZ4F_compressOptions_t* %cOptionsNull to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %173) #4
  %174 = load i32, i32* %retval, align 4
  ret i32 %174
}

; Function Attrs: nounwind
define i32 @LZ4F_compressEnd(%struct.LZ4F_cctx_s* %cctxPtr, i8* %dstBuffer, i32 %dstCapacity, %struct.LZ4F_compressOptions_t* %compressOptionsPtr) #0 {
entry:
  %retval = alloca i32, align 4
  %cctxPtr.addr = alloca %struct.LZ4F_cctx_s*, align 4
  %dstBuffer.addr = alloca i8*, align 4
  %dstCapacity.addr = alloca i32, align 4
  %compressOptionsPtr.addr = alloca %struct.LZ4F_compressOptions_t*, align 4
  %dstStart = alloca i8*, align 4
  %dstPtr = alloca i8*, align 4
  %flushSize = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %xxh = alloca i32, align 4
  store %struct.LZ4F_cctx_s* %cctxPtr, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  store i8* %dstBuffer, i8** %dstBuffer.addr, align 4, !tbaa !7
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !3
  store %struct.LZ4F_compressOptions_t* %compressOptionsPtr, %struct.LZ4F_compressOptions_t** %compressOptionsPtr.addr, align 4, !tbaa !7
  %0 = bitcast i8** %dstStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %dstBuffer.addr, align 4, !tbaa !7
  store i8* %1, i8** %dstStart, align 4, !tbaa !7
  %2 = bitcast i8** %dstPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load i8*, i8** %dstStart, align 4, !tbaa !7
  store i8* %3, i8** %dstPtr, align 4, !tbaa !7
  %4 = bitcast i32* %flushSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %6 = load i8*, i8** %dstBuffer.addr, align 4, !tbaa !7
  %7 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %8 = load %struct.LZ4F_compressOptions_t*, %struct.LZ4F_compressOptions_t** %compressOptionsPtr.addr, align 4, !tbaa !7
  %call = call i32 @LZ4F_flush(%struct.LZ4F_cctx_s* %5, i8* %6, i32 %7, %struct.LZ4F_compressOptions_t* %8)
  store i32 %call, i32* %flushSize, align 4, !tbaa !3
  %9 = load i32, i32* %flushSize, align 4, !tbaa !3
  %call1 = call i32 @LZ4F_isError(i32 %9)
  %tobool = icmp ne i32 %call1, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %10 = load i32, i32* %flushSize, align 4, !tbaa !3
  store i32 %10, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup28

if.end:                                           ; preds = %entry
  %11 = load i32, i32* %flushSize, align 4, !tbaa !3
  %12 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds i8, i8* %12, i32 %11
  store i8* %add.ptr, i8** %dstPtr, align 4, !tbaa !7
  %13 = load i32, i32* %flushSize, align 4, !tbaa !3
  %14 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %sub = sub i32 %14, %13
  store i32 %sub, i32* %dstCapacity.addr, align 4, !tbaa !3
  %15 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %cmp = icmp ult i32 %15, 4
  br i1 %cmp, label %if.then2, label %if.end4

if.then2:                                         ; preds = %if.end
  %call3 = call i32 @err0r(i32 11)
  store i32 %call3, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup28

if.end4:                                          ; preds = %if.end
  %16 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  call void @LZ4F_writeLE32(i8* %16, i32 0)
  %17 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %add.ptr5 = getelementptr inbounds i8, i8* %17, i32 4
  store i8* %add.ptr5, i8** %dstPtr, align 4, !tbaa !7
  %18 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %18, i32 0, i32 0
  %frameInfo = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 0
  %contentChecksumFlag = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo, i32 0, i32 2
  %19 = load i32, i32* %contentChecksumFlag, align 8, !tbaa !45
  %cmp6 = icmp eq i32 %19, 1
  br i1 %cmp6, label %if.then7, label %if.end15

if.then7:                                         ; preds = %if.end4
  %20 = bitcast i32* %xxh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  %21 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %xxh8 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %21, i32 0, i32 10
  %call9 = call i32 @LZ4_XXH32_digest(%struct.XXH32_state_s* %xxh8)
  store i32 %call9, i32* %xxh, align 4, !tbaa !9
  %22 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %cmp10 = icmp ult i32 %22, 8
  br i1 %cmp10, label %if.then11, label %if.end13

if.then11:                                        ; preds = %if.then7
  %call12 = call i32 @err0r(i32 11)
  store i32 %call12, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %if.then7
  %23 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %24 = load i32, i32* %xxh, align 4, !tbaa !9
  call void @LZ4F_writeLE32(i8* %23, i32 %24)
  %25 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %add.ptr14 = getelementptr inbounds i8, i8* %25, i32 4
  store i8* %add.ptr14, i8** %dstPtr, align 4, !tbaa !7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end13, %if.then11
  %26 = bitcast i32* %xxh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup28 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end15

if.end15:                                         ; preds = %cleanup.cont, %if.end4
  %27 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %cStage = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %27, i32 0, i32 2
  store i32 0, i32* %cStage, align 4, !tbaa !48
  %28 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %maxBufferSize = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %28, i32 0, i32 5
  store i32 0, i32* %maxBufferSize, align 8, !tbaa !36
  %29 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs16 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %29, i32 0, i32 0
  %frameInfo17 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs16, i32 0, i32 0
  %contentSize = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo17, i32 0, i32 4
  %30 = load i64, i64* %contentSize, align 8, !tbaa !44
  %tobool18 = icmp ne i64 %30, 0
  br i1 %tobool18, label %if.then19, label %if.end27

if.then19:                                        ; preds = %if.end15
  %31 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs20 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %31, i32 0, i32 0
  %frameInfo21 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs20, i32 0, i32 0
  %contentSize22 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo21, i32 0, i32 4
  %32 = load i64, i64* %contentSize22, align 8, !tbaa !44
  %33 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %totalInSize = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %33, i32 0, i32 9
  %34 = load i64, i64* %totalInSize, align 8, !tbaa !47
  %cmp23 = icmp ne i64 %32, %34
  br i1 %cmp23, label %if.then24, label %if.end26

if.then24:                                        ; preds = %if.then19
  %call25 = call i32 @err0r(i32 14)
  store i32 %call25, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup28

if.end26:                                         ; preds = %if.then19
  br label %if.end27

if.end27:                                         ; preds = %if.end26, %if.end15
  %35 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %36 = load i8*, i8** %dstStart, align 4, !tbaa !7
  %sub.ptr.lhs.cast = ptrtoint i8* %35 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %36 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup28

cleanup28:                                        ; preds = %if.end27, %if.then24, %cleanup, %if.then2, %if.then
  %37 = bitcast i32* %flushSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #4
  %38 = bitcast i8** %dstPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #4
  %39 = bitcast i8** %dstStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  %40 = load i32, i32* %retval, align 4
  ret i32 %40
}

; Function Attrs: nounwind
define i32 @LZ4F_compressFrame(i8* %dstBuffer, i32 %dstCapacity, i8* %srcBuffer, i32 %srcSize, %struct.LZ4F_preferences_t* %preferencesPtr) #0 {
entry:
  %dstBuffer.addr = alloca i8*, align 4
  %dstCapacity.addr = alloca i32, align 4
  %srcBuffer.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %preferencesPtr.addr = alloca %struct.LZ4F_preferences_t*, align 4
  %result = alloca i32, align 4
  %cctx = alloca %struct.LZ4F_cctx_s, align 8
  %lz4ctx = alloca %union.LZ4_stream_u, align 8
  %cctxPtr = alloca %struct.LZ4F_cctx_s*, align 4
  store i8* %dstBuffer, i8** %dstBuffer.addr, align 4, !tbaa !7
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !3
  store i8* %srcBuffer, i8** %srcBuffer.addr, align 4, !tbaa !7
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !3
  store %struct.LZ4F_preferences_t* %preferencesPtr, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %0 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast %struct.LZ4F_cctx_s* %cctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 152, i8* %1) #4
  %2 = bitcast %union.LZ4_stream_u* %lz4ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 16416, i8* %2) #4
  %3 = bitcast %struct.LZ4F_cctx_s** %cctxPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  store %struct.LZ4F_cctx_s* %cctx, %struct.LZ4F_cctx_s** %cctxPtr, align 4, !tbaa !7
  %4 = bitcast %struct.LZ4F_cctx_s* %cctx to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %4, i8 0, i32 152, i1 false)
  %version = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %cctx, i32 0, i32 1
  store i32 100, i32* %version, align 8, !tbaa !50
  %maxBufferSize = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %cctx, i32 0, i32 5
  store i32 5242880, i32* %maxBufferSize, align 8, !tbaa !36
  %5 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %cmp = icmp eq %struct.LZ4F_preferences_t* %5, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %6 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %compressionLevel = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %6, i32 0, i32 1
  %7 = load i32, i32* %compressionLevel, align 8, !tbaa !41
  %cmp1 = icmp slt i32 %7, 3
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %8 = bitcast %union.LZ4_stream_u* %lz4ctx to i8*
  %call = call %union.LZ4_stream_u* @LZ4_initStream(i8* %8, i32 16416)
  %9 = bitcast %union.LZ4_stream_u* %lz4ctx to i8*
  %10 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr, align 4, !tbaa !7
  %lz4CtxPtr = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %10, i32 0, i32 11
  store i8* %9, i8** %lz4CtxPtr, align 8, !tbaa !31
  %11 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr, align 4, !tbaa !7
  %lz4CtxAlloc = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %11, i32 0, i32 12
  store i16 1, i16* %lz4CtxAlloc, align 4, !tbaa !30
  %12 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr, align 4, !tbaa !7
  %lz4CtxState = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %12, i32 0, i32 13
  store i16 1, i16* %lz4CtxState, align 2, !tbaa !32
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  %13 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr, align 4, !tbaa !7
  %14 = load i8*, i8** %dstBuffer.addr, align 4, !tbaa !7
  %15 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %16 = load i8*, i8** %srcBuffer.addr, align 4, !tbaa !7
  %17 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %18 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %call2 = call i32 @LZ4F_compressFrame_usingCDict(%struct.LZ4F_cctx_s* %13, i8* %14, i32 %15, i8* %16, i32 %17, %struct.LZ4F_CDict_s* null, %struct.LZ4F_preferences_t* %18)
  store i32 %call2, i32* %result, align 4, !tbaa !3
  %19 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %cmp3 = icmp ne %struct.LZ4F_preferences_t* %19, null
  br i1 %cmp3, label %land.lhs.true, label %if.end8

land.lhs.true:                                    ; preds = %if.end
  %20 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %compressionLevel4 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %20, i32 0, i32 1
  %21 = load i32, i32* %compressionLevel4, align 8, !tbaa !41
  %cmp5 = icmp sge i32 %21, 3
  br i1 %cmp5, label %if.then6, label %if.end8

if.then6:                                         ; preds = %land.lhs.true
  %22 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr, align 4, !tbaa !7
  %lz4CtxPtr7 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %22, i32 0, i32 11
  %23 = load i8*, i8** %lz4CtxPtr7, align 8, !tbaa !31
  call void @free(i8* %23)
  br label %if.end8

if.end8:                                          ; preds = %if.then6, %land.lhs.true, %if.end
  %24 = load i32, i32* %result, align 4, !tbaa !3
  %25 = bitcast %struct.LZ4F_cctx_s** %cctxPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  %26 = bitcast %union.LZ4_stream_u* %lz4ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 16416, i8* %26) #4
  %27 = bitcast %struct.LZ4F_cctx_s* %cctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 152, i8* %27) #4
  %28 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #4
  ret i32 %24
}

declare %union.LZ4_stream_u* @LZ4_initStream(i8*, i32) #3

declare void @free(i8*) #3

; Function Attrs: nounwind
define hidden %struct.LZ4F_CDict_s* @LZ4F_createCDict(i8* %dictBuffer, i32 %dictSize) #0 {
entry:
  %retval = alloca %struct.LZ4F_CDict_s*, align 4
  %dictBuffer.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  %dictStart = alloca i8*, align 4
  %cdict = alloca %struct.LZ4F_CDict_s*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %dictBuffer, i8** %dictBuffer.addr, align 4, !tbaa !7
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !3
  %0 = bitcast i8** %dictStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %dictBuffer.addr, align 4, !tbaa !7
  store i8* %1, i8** %dictStart, align 4, !tbaa !7
  %2 = bitcast %struct.LZ4F_CDict_s** %cdict to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %call = call i8* @malloc(i32 12)
  %3 = bitcast i8* %call to %struct.LZ4F_CDict_s*
  store %struct.LZ4F_CDict_s* %3, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !7
  %4 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !7
  %tobool = icmp ne %struct.LZ4F_CDict_s* %4, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.LZ4F_CDict_s* null, %struct.LZ4F_CDict_s** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %5 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %cmp = icmp ugt i32 %5, 65536
  br i1 %cmp, label %if.then1, label %if.end2

if.then1:                                         ; preds = %if.end
  %6 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %sub = sub i32 %6, 65536
  %7 = load i8*, i8** %dictStart, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds i8, i8* %7, i32 %sub
  store i8* %add.ptr, i8** %dictStart, align 4, !tbaa !7
  store i32 65536, i32* %dictSize.addr, align 4, !tbaa !3
  br label %if.end2

if.end2:                                          ; preds = %if.then1, %if.end
  %8 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %call3 = call i8* @malloc(i32 %8)
  %9 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !7
  %dictContent = getelementptr inbounds %struct.LZ4F_CDict_s, %struct.LZ4F_CDict_s* %9, i32 0, i32 0
  store i8* %call3, i8** %dictContent, align 4, !tbaa !51
  %call4 = call %union.LZ4_stream_u* @LZ4_createStream()
  %10 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !7
  %fastCtx = getelementptr inbounds %struct.LZ4F_CDict_s, %struct.LZ4F_CDict_s* %10, i32 0, i32 1
  store %union.LZ4_stream_u* %call4, %union.LZ4_stream_u** %fastCtx, align 4, !tbaa !53
  %call5 = call %union.LZ4_streamHC_u* @LZ4_createStreamHC()
  %11 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !7
  %HCCtx = getelementptr inbounds %struct.LZ4F_CDict_s, %struct.LZ4F_CDict_s* %11, i32 0, i32 2
  store %union.LZ4_streamHC_u* %call5, %union.LZ4_streamHC_u** %HCCtx, align 4, !tbaa !54
  %12 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !7
  %dictContent6 = getelementptr inbounds %struct.LZ4F_CDict_s, %struct.LZ4F_CDict_s* %12, i32 0, i32 0
  %13 = load i8*, i8** %dictContent6, align 4, !tbaa !51
  %tobool7 = icmp ne i8* %13, null
  br i1 %tobool7, label %lor.lhs.false, label %if.then13

lor.lhs.false:                                    ; preds = %if.end2
  %14 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !7
  %fastCtx8 = getelementptr inbounds %struct.LZ4F_CDict_s, %struct.LZ4F_CDict_s* %14, i32 0, i32 1
  %15 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %fastCtx8, align 4, !tbaa !53
  %tobool9 = icmp ne %union.LZ4_stream_u* %15, null
  br i1 %tobool9, label %lor.lhs.false10, label %if.then13

lor.lhs.false10:                                  ; preds = %lor.lhs.false
  %16 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !7
  %HCCtx11 = getelementptr inbounds %struct.LZ4F_CDict_s, %struct.LZ4F_CDict_s* %16, i32 0, i32 2
  %17 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %HCCtx11, align 4, !tbaa !54
  %tobool12 = icmp ne %union.LZ4_streamHC_u* %17, null
  br i1 %tobool12, label %if.end14, label %if.then13

if.then13:                                        ; preds = %lor.lhs.false10, %lor.lhs.false, %if.end2
  %18 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !7
  call void @LZ4F_freeCDict(%struct.LZ4F_CDict_s* %18)
  store %struct.LZ4F_CDict_s* null, %struct.LZ4F_CDict_s** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %lor.lhs.false10
  %19 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !7
  %dictContent15 = getelementptr inbounds %struct.LZ4F_CDict_s, %struct.LZ4F_CDict_s* %19, i32 0, i32 0
  %20 = load i8*, i8** %dictContent15, align 4, !tbaa !51
  %21 = load i8*, i8** %dictStart, align 4, !tbaa !7
  %22 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %20, i8* align 1 %21, i32 %22, i1 false)
  %23 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !7
  %fastCtx16 = getelementptr inbounds %struct.LZ4F_CDict_s, %struct.LZ4F_CDict_s* %23, i32 0, i32 1
  %24 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %fastCtx16, align 4, !tbaa !53
  %25 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !7
  %dictContent17 = getelementptr inbounds %struct.LZ4F_CDict_s, %struct.LZ4F_CDict_s* %25, i32 0, i32 0
  %26 = load i8*, i8** %dictContent17, align 4, !tbaa !51
  %27 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %call18 = call i32 @LZ4_loadDict(%union.LZ4_stream_u* %24, i8* %26, i32 %27)
  %28 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !7
  %HCCtx19 = getelementptr inbounds %struct.LZ4F_CDict_s, %struct.LZ4F_CDict_s* %28, i32 0, i32 2
  %29 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %HCCtx19, align 4, !tbaa !54
  call void @LZ4_setCompressionLevel(%union.LZ4_streamHC_u* %29, i32 9)
  %30 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !7
  %HCCtx20 = getelementptr inbounds %struct.LZ4F_CDict_s, %struct.LZ4F_CDict_s* %30, i32 0, i32 2
  %31 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %HCCtx20, align 4, !tbaa !54
  %32 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !7
  %dictContent21 = getelementptr inbounds %struct.LZ4F_CDict_s, %struct.LZ4F_CDict_s* %32, i32 0, i32 0
  %33 = load i8*, i8** %dictContent21, align 4, !tbaa !51
  %34 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %call22 = call i32 @LZ4_loadDictHC(%union.LZ4_streamHC_u* %31, i8* %33, i32 %34)
  %35 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 4, !tbaa !7
  store %struct.LZ4F_CDict_s* %35, %struct.LZ4F_CDict_s** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end14, %if.then13, %if.then
  %36 = bitcast %struct.LZ4F_CDict_s** %cdict to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  %37 = bitcast i8** %dictStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #4
  %38 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %retval, align 4
  ret %struct.LZ4F_CDict_s* %38
}

declare i8* @malloc(i32) #3

declare %union.LZ4_stream_u* @LZ4_createStream() #3

declare %union.LZ4_streamHC_u* @LZ4_createStreamHC() #3

; Function Attrs: nounwind
define hidden void @LZ4F_freeCDict(%struct.LZ4F_CDict_s* %cdict) #0 {
entry:
  %cdict.addr = alloca %struct.LZ4F_CDict_s*, align 4
  store %struct.LZ4F_CDict_s* %cdict, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %0 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %cmp = icmp eq %struct.LZ4F_CDict_s* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %dictContent = getelementptr inbounds %struct.LZ4F_CDict_s, %struct.LZ4F_CDict_s* %1, i32 0, i32 0
  %2 = load i8*, i8** %dictContent, align 4, !tbaa !51
  call void @free(i8* %2)
  %3 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %fastCtx = getelementptr inbounds %struct.LZ4F_CDict_s, %struct.LZ4F_CDict_s* %3, i32 0, i32 1
  %4 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %fastCtx, align 4, !tbaa !53
  %call = call i32 @LZ4_freeStream(%union.LZ4_stream_u* %4)
  %5 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %HCCtx = getelementptr inbounds %struct.LZ4F_CDict_s, %struct.LZ4F_CDict_s* %5, i32 0, i32 2
  %6 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %HCCtx, align 4, !tbaa !54
  %call1 = call i32 @LZ4_freeStreamHC(%union.LZ4_streamHC_u* %6)
  %7 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %8 = bitcast %struct.LZ4F_CDict_s* %7 to i8*
  call void @free(i8* %8)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

declare i32 @LZ4_loadDict(%union.LZ4_stream_u*, i8*, i32) #3

declare void @LZ4_setCompressionLevel(%union.LZ4_streamHC_u*, i32) #3

declare i32 @LZ4_loadDictHC(%union.LZ4_streamHC_u*, i8*, i32) #3

declare i32 @LZ4_freeStream(%union.LZ4_stream_u*) #3

declare i32 @LZ4_freeStreamHC(%union.LZ4_streamHC_u*) #3

; Function Attrs: nounwind
define i32 @LZ4F_createCompressionContext(%struct.LZ4F_cctx_s** %LZ4F_compressionContextPtr, i32 %version) #0 {
entry:
  %retval = alloca i32, align 4
  %LZ4F_compressionContextPtr.addr = alloca %struct.LZ4F_cctx_s**, align 4
  %version.addr = alloca i32, align 4
  %cctxPtr = alloca %struct.LZ4F_cctx_s*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.LZ4F_cctx_s** %LZ4F_compressionContextPtr, %struct.LZ4F_cctx_s*** %LZ4F_compressionContextPtr.addr, align 4, !tbaa !7
  store i32 %version, i32* %version.addr, align 4, !tbaa !9
  %0 = bitcast %struct.LZ4F_cctx_s** %cctxPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %call = call i8* @calloc(i32 1, i32 152)
  %1 = bitcast i8* %call to %struct.LZ4F_cctx_s*
  store %struct.LZ4F_cctx_s* %1, %struct.LZ4F_cctx_s** %cctxPtr, align 4, !tbaa !7
  %2 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr, align 4, !tbaa !7
  %cmp = icmp eq %struct.LZ4F_cctx_s* %2, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call1 = call i32 @err0r(i32 9)
  store i32 %call1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %3 = load i32, i32* %version.addr, align 4, !tbaa !9
  %4 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr, align 4, !tbaa !7
  %version2 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %4, i32 0, i32 1
  store i32 %3, i32* %version2, align 8, !tbaa !50
  %5 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr, align 4, !tbaa !7
  %cStage = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %5, i32 0, i32 2
  store i32 0, i32* %cStage, align 4, !tbaa !48
  %6 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr, align 4, !tbaa !7
  %7 = load %struct.LZ4F_cctx_s**, %struct.LZ4F_cctx_s*** %LZ4F_compressionContextPtr.addr, align 4, !tbaa !7
  store %struct.LZ4F_cctx_s* %6, %struct.LZ4F_cctx_s** %7, align 4, !tbaa !7
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %8 = bitcast %struct.LZ4F_cctx_s** %cctxPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #4
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

declare i8* @calloc(i32, i32) #3

; Function Attrs: nounwind
define i32 @LZ4F_freeCompressionContext(%struct.LZ4F_cctx_s* %LZ4F_compressionContext) #0 {
entry:
  %LZ4F_compressionContext.addr = alloca %struct.LZ4F_cctx_s*, align 4
  %cctxPtr = alloca %struct.LZ4F_cctx_s*, align 4
  store %struct.LZ4F_cctx_s* %LZ4F_compressionContext, %struct.LZ4F_cctx_s** %LZ4F_compressionContext.addr, align 4, !tbaa !7
  %0 = bitcast %struct.LZ4F_cctx_s** %cctxPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %LZ4F_compressionContext.addr, align 4, !tbaa !7
  store %struct.LZ4F_cctx_s* %1, %struct.LZ4F_cctx_s** %cctxPtr, align 4, !tbaa !7
  %2 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr, align 4, !tbaa !7
  %cmp = icmp ne %struct.LZ4F_cctx_s* %2, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr, align 4, !tbaa !7
  %lz4CtxPtr = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %3, i32 0, i32 11
  %4 = load i8*, i8** %lz4CtxPtr, align 8, !tbaa !31
  call void @free(i8* %4)
  %5 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr, align 4, !tbaa !7
  %tmpBuff = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %5, i32 0, i32 6
  %6 = load i8*, i8** %tmpBuff, align 4, !tbaa !37
  call void @free(i8* %6)
  %7 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %LZ4F_compressionContext.addr, align 4, !tbaa !7
  %8 = bitcast %struct.LZ4F_cctx_s* %7 to i8*
  call void @free(i8* %8)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = bitcast %struct.LZ4F_cctx_s** %cctxPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #4
  ret i32 0
}

declare %union.LZ4_streamHC_u* @LZ4_initStreamHC(i8*, i32) #3

declare i32 @LZ4_XXH32_reset(%struct.XXH32_state_s*, i32) #3

; Function Attrs: nounwind
define internal void @LZ4F_initStream(i8* %ctx, %struct.LZ4F_CDict_s* %cdict, i32 %level, i32 %blockMode) #0 {
entry:
  %ctx.addr = alloca i8*, align 4
  %cdict.addr = alloca %struct.LZ4F_CDict_s*, align 4
  %level.addr = alloca i32, align 4
  %blockMode.addr = alloca i32, align 4
  store i8* %ctx, i8** %ctx.addr, align 4, !tbaa !7
  store %struct.LZ4F_CDict_s* %cdict, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  store i32 %level, i32* %level.addr, align 4, !tbaa !9
  store i32 %blockMode, i32* %blockMode.addr, align 4, !tbaa !11
  %0 = load i32, i32* %level.addr, align 4, !tbaa !9
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %cmp1 = icmp ne %struct.LZ4F_CDict_s* %1, null
  br i1 %cmp1, label %if.then3, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %2 = load i32, i32* %blockMode.addr, align 4, !tbaa !11
  %cmp2 = icmp eq i32 %2, 0
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %lor.lhs.false, %if.then
  %3 = load i8*, i8** %ctx.addr, align 4, !tbaa !7
  %4 = bitcast i8* %3 to %union.LZ4_stream_u*
  call void @LZ4_resetStream_fast(%union.LZ4_stream_u* %4)
  br label %if.end

if.end:                                           ; preds = %if.then3, %lor.lhs.false
  %5 = load i8*, i8** %ctx.addr, align 4, !tbaa !7
  %6 = bitcast i8* %5 to %union.LZ4_stream_u*
  %7 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %tobool = icmp ne %struct.LZ4F_CDict_s* %7, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %8 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %fastCtx = getelementptr inbounds %struct.LZ4F_CDict_s, %struct.LZ4F_CDict_s* %8, i32 0, i32 1
  %9 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %fastCtx, align 4, !tbaa !53
  br label %cond.end

cond.false:                                       ; preds = %if.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %union.LZ4_stream_u* [ %9, %cond.true ], [ null, %cond.false ]
  call void @LZ4_attach_dictionary(%union.LZ4_stream_u* %6, %union.LZ4_stream_u* %cond)
  br label %if.end9

if.else:                                          ; preds = %entry
  %10 = load i8*, i8** %ctx.addr, align 4, !tbaa !7
  %11 = bitcast i8* %10 to %union.LZ4_streamHC_u*
  %12 = load i32, i32* %level.addr, align 4, !tbaa !9
  call void @LZ4_resetStreamHC_fast(%union.LZ4_streamHC_u* %11, i32 %12)
  %13 = load i8*, i8** %ctx.addr, align 4, !tbaa !7
  %14 = bitcast i8* %13 to %union.LZ4_streamHC_u*
  %15 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %tobool4 = icmp ne %struct.LZ4F_CDict_s* %15, null
  br i1 %tobool4, label %cond.true5, label %cond.false6

cond.true5:                                       ; preds = %if.else
  %16 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %HCCtx = getelementptr inbounds %struct.LZ4F_CDict_s, %struct.LZ4F_CDict_s* %16, i32 0, i32 2
  %17 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %HCCtx, align 4, !tbaa !54
  br label %cond.end7

cond.false6:                                      ; preds = %if.else
  br label %cond.end7

cond.end7:                                        ; preds = %cond.false6, %cond.true5
  %cond8 = phi %union.LZ4_streamHC_u* [ %17, %cond.true5 ], [ null, %cond.false6 ]
  call void @LZ4_attach_HC_dictionary(%union.LZ4_streamHC_u* %14, %union.LZ4_streamHC_u* %cond8)
  br label %if.end9

if.end9:                                          ; preds = %cond.end7, %cond.end
  ret void
}

declare void @LZ4_favorDecompressionSpeed(%union.LZ4_streamHC_u*, i32) #3

; Function Attrs: nounwind
define internal void @LZ4F_writeLE32(i8* %dst, i32 %value32) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %value32.addr = alloca i32, align 4
  %dstPtr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !7
  store i32 %value32, i32* %value32.addr, align 4, !tbaa !9
  %0 = bitcast i8** %dstPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  store i8* %1, i8** %dstPtr, align 4, !tbaa !7
  %2 = load i32, i32* %value32.addr, align 4, !tbaa !9
  %conv = trunc i32 %2 to i8
  %3 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 0
  store i8 %conv, i8* %arrayidx, align 1, !tbaa !11
  %4 = load i32, i32* %value32.addr, align 4, !tbaa !9
  %shr = lshr i32 %4, 8
  %conv1 = trunc i32 %shr to i8
  %5 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %arrayidx2 = getelementptr inbounds i8, i8* %5, i32 1
  store i8 %conv1, i8* %arrayidx2, align 1, !tbaa !11
  %6 = load i32, i32* %value32.addr, align 4, !tbaa !9
  %shr3 = lshr i32 %6, 16
  %conv4 = trunc i32 %shr3 to i8
  %7 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %arrayidx5 = getelementptr inbounds i8, i8* %7, i32 2
  store i8 %conv4, i8* %arrayidx5, align 1, !tbaa !11
  %8 = load i32, i32* %value32.addr, align 4, !tbaa !9
  %shr6 = lshr i32 %8, 24
  %conv7 = trunc i32 %shr6 to i8
  %9 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %arrayidx8 = getelementptr inbounds i8, i8* %9, i32 3
  store i8 %conv7, i8* %arrayidx8, align 1, !tbaa !11
  %10 = bitcast i8** %dstPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #4
  ret void
}

; Function Attrs: nounwind
define internal void @LZ4F_writeLE64(i8* %dst, i64 %value64) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %value64.addr = alloca i64, align 8
  %dstPtr = alloca i8*, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !7
  store i64 %value64, i64* %value64.addr, align 8, !tbaa !13
  %0 = bitcast i8** %dstPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  store i8* %1, i8** %dstPtr, align 4, !tbaa !7
  %2 = load i64, i64* %value64.addr, align 8, !tbaa !13
  %conv = trunc i64 %2 to i8
  %3 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 0
  store i8 %conv, i8* %arrayidx, align 1, !tbaa !11
  %4 = load i64, i64* %value64.addr, align 8, !tbaa !13
  %shr = lshr i64 %4, 8
  %conv1 = trunc i64 %shr to i8
  %5 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %arrayidx2 = getelementptr inbounds i8, i8* %5, i32 1
  store i8 %conv1, i8* %arrayidx2, align 1, !tbaa !11
  %6 = load i64, i64* %value64.addr, align 8, !tbaa !13
  %shr3 = lshr i64 %6, 16
  %conv4 = trunc i64 %shr3 to i8
  %7 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %arrayidx5 = getelementptr inbounds i8, i8* %7, i32 2
  store i8 %conv4, i8* %arrayidx5, align 1, !tbaa !11
  %8 = load i64, i64* %value64.addr, align 8, !tbaa !13
  %shr6 = lshr i64 %8, 24
  %conv7 = trunc i64 %shr6 to i8
  %9 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %arrayidx8 = getelementptr inbounds i8, i8* %9, i32 3
  store i8 %conv7, i8* %arrayidx8, align 1, !tbaa !11
  %10 = load i64, i64* %value64.addr, align 8, !tbaa !13
  %shr9 = lshr i64 %10, 32
  %conv10 = trunc i64 %shr9 to i8
  %11 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %arrayidx11 = getelementptr inbounds i8, i8* %11, i32 4
  store i8 %conv10, i8* %arrayidx11, align 1, !tbaa !11
  %12 = load i64, i64* %value64.addr, align 8, !tbaa !13
  %shr12 = lshr i64 %12, 40
  %conv13 = trunc i64 %shr12 to i8
  %13 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %arrayidx14 = getelementptr inbounds i8, i8* %13, i32 5
  store i8 %conv13, i8* %arrayidx14, align 1, !tbaa !11
  %14 = load i64, i64* %value64.addr, align 8, !tbaa !13
  %shr15 = lshr i64 %14, 48
  %conv16 = trunc i64 %shr15 to i8
  %15 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %arrayidx17 = getelementptr inbounds i8, i8* %15, i32 6
  store i8 %conv16, i8* %arrayidx17, align 1, !tbaa !11
  %16 = load i64, i64* %value64.addr, align 8, !tbaa !13
  %shr18 = lshr i64 %16, 56
  %conv19 = trunc i64 %shr18 to i8
  %17 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %arrayidx20 = getelementptr inbounds i8, i8* %17, i32 7
  store i8 %conv19, i8* %arrayidx20, align 1, !tbaa !11
  %18 = bitcast i8** %dstPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #4
  ret void
}

; Function Attrs: nounwind
define internal zeroext i8 @LZ4F_headerChecksum(i8* %header, i32 %length) #0 {
entry:
  %header.addr = alloca i8*, align 4
  %length.addr = alloca i32, align 4
  %xxh = alloca i32, align 4
  store i8* %header, i8** %header.addr, align 4, !tbaa !7
  store i32 %length, i32* %length.addr, align 4, !tbaa !3
  %0 = bitcast i32* %xxh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %header.addr, align 4, !tbaa !7
  %2 = load i32, i32* %length.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_XXH32(i8* %1, i32 %2, i32 0)
  store i32 %call, i32* %xxh, align 4, !tbaa !9
  %3 = load i32, i32* %xxh, align 4, !tbaa !9
  %shr = lshr i32 %3, 8
  %conv = trunc i32 %shr to i8
  %4 = bitcast i32* %xxh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #4
  ret i8 %conv
}

; Function Attrs: nounwind
define i32 @LZ4F_compressBegin(%struct.LZ4F_cctx_s* %cctxPtr, i8* %dstBuffer, i32 %dstCapacity, %struct.LZ4F_preferences_t* %preferencesPtr) #0 {
entry:
  %cctxPtr.addr = alloca %struct.LZ4F_cctx_s*, align 4
  %dstBuffer.addr = alloca i8*, align 4
  %dstCapacity.addr = alloca i32, align 4
  %preferencesPtr.addr = alloca %struct.LZ4F_preferences_t*, align 4
  store %struct.LZ4F_cctx_s* %cctxPtr, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  store i8* %dstBuffer, i8** %dstBuffer.addr, align 4, !tbaa !7
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !3
  store %struct.LZ4F_preferences_t* %preferencesPtr, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %0 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %dstBuffer.addr, align 4, !tbaa !7
  %2 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %3 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %call = call i32 @LZ4F_compressBegin_usingCDict(%struct.LZ4F_cctx_s* %0, i8* %1, i32 %2, %struct.LZ4F_CDict_s* null, %struct.LZ4F_preferences_t* %3)
  ret i32 %call
}

; Function Attrs: nounwind
define i32 @LZ4F_compressBound(i32 %srcSize, %struct.LZ4F_preferences_t* %preferencesPtr) #0 {
entry:
  %retval = alloca i32, align 4
  %srcSize.addr = alloca i32, align 4
  %preferencesPtr.addr = alloca %struct.LZ4F_preferences_t*, align 4
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !3
  store %struct.LZ4F_preferences_t* %preferencesPtr, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %0 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %tobool = icmp ne %struct.LZ4F_preferences_t* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %autoFlush = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %1, i32 0, i32 2
  %2 = load i32, i32* %autoFlush, align 4, !tbaa !15
  %tobool1 = icmp ne i32 %2, 0
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %3 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %4 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %call = call i32 @LZ4F_compressBound_internal(i32 %3, %struct.LZ4F_preferences_t* %4, i32 0)
  store i32 %call, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %5 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %6 = load %struct.LZ4F_preferences_t*, %struct.LZ4F_preferences_t** %preferencesPtr.addr, align 4, !tbaa !7
  %call2 = call i32 @LZ4F_compressBound_internal(i32 %5, %struct.LZ4F_preferences_t* %6, i32 -1)
  store i32 %call2, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: nounwind
define internal i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)* @LZ4F_selectCompression(i32 %blockMode, i32 %level) #0 {
entry:
  %retval = alloca i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)*, align 4
  %blockMode.addr = alloca i32, align 4
  %level.addr = alloca i32, align 4
  store i32 %blockMode, i32* %blockMode.addr, align 4, !tbaa !11
  store i32 %level, i32* %level.addr, align 4, !tbaa !9
  %0 = load i32, i32* %level.addr, align 4, !tbaa !9
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %if.then, label %if.end3

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %blockMode.addr, align 4, !tbaa !11
  %cmp1 = icmp eq i32 %1, 1
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  store i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)* @LZ4F_compressBlock, i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)** %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  store i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)* @LZ4F_compressBlock_continue, i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)** %retval, align 4
  br label %return

if.end3:                                          ; preds = %entry
  %2 = load i32, i32* %blockMode.addr, align 4, !tbaa !11
  %cmp4 = icmp eq i32 %2, 1
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)* @LZ4F_compressBlockHC, i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)** %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  store i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)* @LZ4F_compressBlockHC_continue, i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)** %retval, align 4
  br label %return

return:                                           ; preds = %if.end6, %if.then5, %if.end, %if.then2
  %3 = load i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)*, i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)** %retval, align 4
  ret i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)* %3
}

; Function Attrs: nounwind
define internal i32 @LZ4F_makeBlock(i8* %dst, i8* %src, i32 %srcSize, i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)* %compress, i8* %lz4ctx, i32 %level, %struct.LZ4F_CDict_s* %cdict, i32 %crcFlag) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %src.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %compress.addr = alloca i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)*, align 4
  %lz4ctx.addr = alloca i8*, align 4
  %level.addr = alloca i32, align 4
  %cdict.addr = alloca %struct.LZ4F_CDict_s*, align 4
  %crcFlag.addr = alloca i32, align 4
  %cSizePtr = alloca i8*, align 4
  %cSize = alloca i32, align 4
  %crc32 = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !7
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !3
  store i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)* %compress, i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)** %compress.addr, align 4, !tbaa !7
  store i8* %lz4ctx, i8** %lz4ctx.addr, align 4, !tbaa !7
  store i32 %level, i32* %level.addr, align 4, !tbaa !9
  store %struct.LZ4F_CDict_s* %cdict, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  store i32 %crcFlag, i32* %crcFlag.addr, align 4, !tbaa !11
  %0 = bitcast i8** %cSizePtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  store i8* %1, i8** %cSizePtr, align 4, !tbaa !7
  %2 = bitcast i32* %cSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)*, i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)** %compress.addr, align 4, !tbaa !7
  %4 = load i8*, i8** %lz4ctx.addr, align 4, !tbaa !7
  %5 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %6 = load i8*, i8** %cSizePtr, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 4
  %7 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %8 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %sub = sub i32 %8, 1
  %9 = load i32, i32* %level.addr, align 4, !tbaa !9
  %10 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %call = call i32 %3(i8* %4, i8* %5, i8* %add.ptr, i32 %7, i32 %sub, i32 %9, %struct.LZ4F_CDict_s* %10)
  store i32 %call, i32* %cSize, align 4, !tbaa !9
  %11 = load i32, i32* %cSize, align 4, !tbaa !9
  %cmp = icmp eq i32 %11, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %12 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  store i32 %12, i32* %cSize, align 4, !tbaa !9
  %13 = load i8*, i8** %cSizePtr, align 4, !tbaa !7
  %14 = load i32, i32* %cSize, align 4, !tbaa !9
  %or = or i32 %14, -2147483648
  call void @LZ4F_writeLE32(i8* %13, i32 %or)
  %15 = load i8*, i8** %cSizePtr, align 4, !tbaa !7
  %add.ptr1 = getelementptr inbounds i8, i8* %15, i32 4
  %16 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %17 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr1, i8* align 1 %16, i32 %17, i1 false)
  br label %if.end

if.else:                                          ; preds = %entry
  %18 = load i8*, i8** %cSizePtr, align 4, !tbaa !7
  %19 = load i32, i32* %cSize, align 4, !tbaa !9
  call void @LZ4F_writeLE32(i8* %18, i32 %19)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %20 = load i32, i32* %crcFlag.addr, align 4, !tbaa !11
  %tobool = icmp ne i32 %20, 0
  br i1 %tobool, label %if.then2, label %if.end7

if.then2:                                         ; preds = %if.end
  %21 = bitcast i32* %crc32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load i8*, i8** %cSizePtr, align 4, !tbaa !7
  %add.ptr3 = getelementptr inbounds i8, i8* %22, i32 4
  %23 = load i32, i32* %cSize, align 4, !tbaa !9
  %call4 = call i32 @LZ4_XXH32(i8* %add.ptr3, i32 %23, i32 0)
  store i32 %call4, i32* %crc32, align 4, !tbaa !9
  %24 = load i8*, i8** %cSizePtr, align 4, !tbaa !7
  %add.ptr5 = getelementptr inbounds i8, i8* %24, i32 4
  %25 = load i32, i32* %cSize, align 4, !tbaa !9
  %add.ptr6 = getelementptr inbounds i8, i8* %add.ptr5, i32 %25
  %26 = load i32, i32* %crc32, align 4, !tbaa !9
  call void @LZ4F_writeLE32(i8* %add.ptr6, i32 %26)
  %27 = bitcast i32* %crc32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #4
  br label %if.end7

if.end7:                                          ; preds = %if.then2, %if.end
  %28 = load i32, i32* %cSize, align 4, !tbaa !9
  %add = add i32 4, %28
  %29 = load i32, i32* %crcFlag.addr, align 4, !tbaa !11
  %mul = mul i32 %29, 4
  %add8 = add i32 %add, %mul
  %30 = bitcast i32* %cSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #4
  %31 = bitcast i8** %cSizePtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #4
  ret i32 %add8
}

; Function Attrs: nounwind
define internal i32 @LZ4F_localSaveDict(%struct.LZ4F_cctx_s* %cctxPtr) #0 {
entry:
  %retval = alloca i32, align 4
  %cctxPtr.addr = alloca %struct.LZ4F_cctx_s*, align 4
  store %struct.LZ4F_cctx_s* %cctxPtr, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %0 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %0, i32 0, i32 0
  %compressionLevel = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 1
  %1 = load i32, i32* %compressionLevel, align 8, !tbaa !25
  %cmp = icmp slt i32 %1, 3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxPtr = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %2, i32 0, i32 11
  %3 = load i8*, i8** %lz4CtxPtr, align 8, !tbaa !31
  %4 = bitcast i8* %3 to %union.LZ4_stream_u*
  %5 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpBuff = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %5, i32 0, i32 6
  %6 = load i8*, i8** %tmpBuff, align 4, !tbaa !37
  %call = call i32 @LZ4_saveDict(%union.LZ4_stream_u* %4, i8* %6, i32 65536)
  store i32 %call, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %7 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxPtr1 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %7, i32 0, i32 11
  %8 = load i8*, i8** %lz4CtxPtr1, align 8, !tbaa !31
  %9 = bitcast i8* %8 to %union.LZ4_streamHC_u*
  %10 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpBuff2 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %10, i32 0, i32 6
  %11 = load i8*, i8** %tmpBuff2, align 4, !tbaa !37
  %call3 = call i32 @LZ4_saveDictHC(%union.LZ4_streamHC_u* %9, i8* %11, i32 65536)
  store i32 %call3, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

declare i32 @LZ4_XXH32_update(%struct.XXH32_state_s*, i8*, i32) #3

; Function Attrs: nounwind
define i32 @LZ4F_flush(%struct.LZ4F_cctx_s* %cctxPtr, i8* %dstBuffer, i32 %dstCapacity, %struct.LZ4F_compressOptions_t* %compressOptionsPtr) #0 {
entry:
  %retval = alloca i32, align 4
  %cctxPtr.addr = alloca %struct.LZ4F_cctx_s*, align 4
  %dstBuffer.addr = alloca i8*, align 4
  %dstCapacity.addr = alloca i32, align 4
  %compressOptionsPtr.addr = alloca %struct.LZ4F_compressOptions_t*, align 4
  %dstStart = alloca i8*, align 4
  %dstPtr = alloca i8*, align 4
  %compress = alloca i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %realDictSize = alloca i32, align 4
  store %struct.LZ4F_cctx_s* %cctxPtr, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  store i8* %dstBuffer, i8** %dstBuffer.addr, align 4, !tbaa !7
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !3
  store %struct.LZ4F_compressOptions_t* %compressOptionsPtr, %struct.LZ4F_compressOptions_t** %compressOptionsPtr.addr, align 4, !tbaa !7
  %0 = bitcast i8** %dstStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %dstBuffer.addr, align 4, !tbaa !7
  store i8* %1, i8** %dstStart, align 4, !tbaa !7
  %2 = bitcast i8** %dstPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load i8*, i8** %dstStart, align 4, !tbaa !7
  store i8* %3, i8** %dstPtr, align 4, !tbaa !7
  %4 = bitcast i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)** %compress to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpInSize = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %5, i32 0, i32 8
  %6 = load i32, i32* %tmpInSize, align 4, !tbaa !39
  %cmp = icmp eq i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %7 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %cStage = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %7, i32 0, i32 2
  %8 = load i32, i32* %cStage, align 4, !tbaa !48
  %cmp1 = icmp ne i32 %8, 1
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  %call = call i32 @err0r(i32 1)
  store i32 %call, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %if.end
  %9 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %10 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpInSize4 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %10, i32 0, i32 8
  %11 = load i32, i32* %tmpInSize4, align 4, !tbaa !39
  %add = add i32 %11, 4
  %add5 = add i32 %add, 4
  %cmp6 = icmp ult i32 %9, %add5
  br i1 %cmp6, label %if.then7, label %if.end9

if.then7:                                         ; preds = %if.end3
  %call8 = call i32 @err0r(i32 11)
  store i32 %call8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end9:                                          ; preds = %if.end3
  %12 = load %struct.LZ4F_compressOptions_t*, %struct.LZ4F_compressOptions_t** %compressOptionsPtr.addr, align 4, !tbaa !7
  %13 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %13, i32 0, i32 0
  %frameInfo = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs, i32 0, i32 0
  %blockMode = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo, i32 0, i32 1
  %14 = load i32, i32* %blockMode, align 4, !tbaa !35
  %15 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs10 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %15, i32 0, i32 0
  %compressionLevel = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs10, i32 0, i32 1
  %16 = load i32, i32* %compressionLevel, align 8, !tbaa !25
  %call11 = call i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)* @LZ4F_selectCompression(i32 %14, i32 %16)
  store i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)* %call11, i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)** %compress, align 4, !tbaa !7
  %17 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %18 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpIn = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %18, i32 0, i32 7
  %19 = load i8*, i8** %tmpIn, align 8, !tbaa !38
  %20 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpInSize12 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %20, i32 0, i32 8
  %21 = load i32, i32* %tmpInSize12, align 4, !tbaa !39
  %22 = load i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)*, i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)** %compress, align 4, !tbaa !7
  %23 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %lz4CtxPtr = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %23, i32 0, i32 11
  %24 = load i8*, i8** %lz4CtxPtr, align 8, !tbaa !31
  %25 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs13 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %25, i32 0, i32 0
  %compressionLevel14 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs13, i32 0, i32 1
  %26 = load i32, i32* %compressionLevel14, align 8, !tbaa !25
  %27 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %cdict = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %27, i32 0, i32 3
  %28 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict, align 8, !tbaa !40
  %29 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs15 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %29, i32 0, i32 0
  %frameInfo16 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs15, i32 0, i32 0
  %blockChecksumFlag = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo16, i32 0, i32 6
  %30 = load i32, i32* %blockChecksumFlag, align 4, !tbaa !43
  %call17 = call i32 @LZ4F_makeBlock(i8* %17, i8* %19, i32 %21, i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)* %22, i8* %24, i32 %26, %struct.LZ4F_CDict_s* %28, i32 %30)
  %31 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds i8, i8* %31, i32 %call17
  store i8* %add.ptr, i8** %dstPtr, align 4, !tbaa !7
  %32 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %prefs18 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %32, i32 0, i32 0
  %frameInfo19 = getelementptr inbounds %struct.LZ4F_preferences_t, %struct.LZ4F_preferences_t* %prefs18, i32 0, i32 0
  %blockMode20 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo19, i32 0, i32 1
  %33 = load i32, i32* %blockMode20, align 4, !tbaa !35
  %cmp21 = icmp eq i32 %33, 0
  br i1 %cmp21, label %if.then22, label %if.end26

if.then22:                                        ; preds = %if.end9
  %34 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpInSize23 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %34, i32 0, i32 8
  %35 = load i32, i32* %tmpInSize23, align 4, !tbaa !39
  %36 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpIn24 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %36, i32 0, i32 7
  %37 = load i8*, i8** %tmpIn24, align 8, !tbaa !38
  %add.ptr25 = getelementptr inbounds i8, i8* %37, i32 %35
  store i8* %add.ptr25, i8** %tmpIn24, align 8, !tbaa !38
  br label %if.end26

if.end26:                                         ; preds = %if.then22, %if.end9
  %38 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpInSize27 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %38, i32 0, i32 8
  store i32 0, i32* %tmpInSize27, align 4, !tbaa !39
  %39 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpIn28 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %39, i32 0, i32 7
  %40 = load i8*, i8** %tmpIn28, align 8, !tbaa !38
  %41 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %maxBlockSize = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %41, i32 0, i32 4
  %42 = load i32, i32* %maxBlockSize, align 4, !tbaa !34
  %add.ptr29 = getelementptr inbounds i8, i8* %40, i32 %42
  %43 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpBuff = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %43, i32 0, i32 6
  %44 = load i8*, i8** %tmpBuff, align 4, !tbaa !37
  %45 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %maxBufferSize = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %45, i32 0, i32 5
  %46 = load i32, i32* %maxBufferSize, align 8, !tbaa !36
  %add.ptr30 = getelementptr inbounds i8, i8* %44, i32 %46
  %cmp31 = icmp ugt i8* %add.ptr29, %add.ptr30
  br i1 %cmp31, label %if.then32, label %if.end37

if.then32:                                        ; preds = %if.end26
  %47 = bitcast i32* %realDictSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #4
  %48 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %call33 = call i32 @LZ4F_localSaveDict(%struct.LZ4F_cctx_s* %48)
  store i32 %call33, i32* %realDictSize, align 4, !tbaa !9
  %49 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpBuff34 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %49, i32 0, i32 6
  %50 = load i8*, i8** %tmpBuff34, align 4, !tbaa !37
  %51 = load i32, i32* %realDictSize, align 4, !tbaa !9
  %add.ptr35 = getelementptr inbounds i8, i8* %50, i32 %51
  %52 = load %struct.LZ4F_cctx_s*, %struct.LZ4F_cctx_s** %cctxPtr.addr, align 4, !tbaa !7
  %tmpIn36 = getelementptr inbounds %struct.LZ4F_cctx_s, %struct.LZ4F_cctx_s* %52, i32 0, i32 7
  store i8* %add.ptr35, i8** %tmpIn36, align 8, !tbaa !38
  %53 = bitcast i32* %realDictSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  br label %if.end37

if.end37:                                         ; preds = %if.then32, %if.end26
  %54 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %55 = load i8*, i8** %dstStart, align 4, !tbaa !7
  %sub.ptr.lhs.cast = ptrtoint i8* %54 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %55 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end37, %if.then7, %if.then2, %if.then
  %56 = bitcast i32 (i8*, i8*, i8*, i32, i32, i32, %struct.LZ4F_CDict_s*)** %compress to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #4
  %57 = bitcast i8** %dstPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #4
  %58 = bitcast i8** %dstStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #4
  %59 = load i32, i32* %retval, align 4
  ret i32 %59
}

declare i32 @LZ4_XXH32_digest(%struct.XXH32_state_s*) #3

; Function Attrs: nounwind
define i32 @LZ4F_createDecompressionContext(%struct.LZ4F_dctx_s** %LZ4F_decompressionContextPtr, i32 %versionNumber) #0 {
entry:
  %retval = alloca i32, align 4
  %LZ4F_decompressionContextPtr.addr = alloca %struct.LZ4F_dctx_s**, align 4
  %versionNumber.addr = alloca i32, align 4
  %dctx = alloca %struct.LZ4F_dctx_s*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.LZ4F_dctx_s** %LZ4F_decompressionContextPtr, %struct.LZ4F_dctx_s*** %LZ4F_decompressionContextPtr.addr, align 4, !tbaa !7
  store i32 %versionNumber, i32* %versionNumber.addr, align 4, !tbaa !9
  %0 = bitcast %struct.LZ4F_dctx_s** %dctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %call = call i8* @calloc(i32 1, i32 208)
  %1 = bitcast i8* %call to %struct.LZ4F_dctx_s*
  store %struct.LZ4F_dctx_s* %1, %struct.LZ4F_dctx_s** %dctx, align 4, !tbaa !7
  %2 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx, align 4, !tbaa !7
  %cmp = icmp eq %struct.LZ4F_dctx_s* %2, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.LZ4F_dctx_s**, %struct.LZ4F_dctx_s*** %LZ4F_decompressionContextPtr.addr, align 4, !tbaa !7
  store %struct.LZ4F_dctx_s* null, %struct.LZ4F_dctx_s** %3, align 4, !tbaa !7
  %call1 = call i32 @err0r(i32 9)
  store i32 %call1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %4 = load i32, i32* %versionNumber.addr, align 4, !tbaa !9
  %5 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx, align 4, !tbaa !7
  %version = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %5, i32 0, i32 1
  store i32 %4, i32* %version, align 8, !tbaa !55
  %6 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx, align 4, !tbaa !7
  %7 = load %struct.LZ4F_dctx_s**, %struct.LZ4F_dctx_s*** %LZ4F_decompressionContextPtr.addr, align 4, !tbaa !7
  store %struct.LZ4F_dctx_s* %6, %struct.LZ4F_dctx_s** %7, align 4, !tbaa !7
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %8 = bitcast %struct.LZ4F_dctx_s** %dctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #4
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

; Function Attrs: nounwind
define i32 @LZ4F_freeDecompressionContext(%struct.LZ4F_dctx_s* %dctx) #0 {
entry:
  %dctx.addr = alloca %struct.LZ4F_dctx_s*, align 4
  %result = alloca i32, align 4
  store %struct.LZ4F_dctx_s* %dctx, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %0 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %result, align 4, !tbaa !3
  %1 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %cmp = icmp ne %struct.LZ4F_dctx_s* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %2, i32 0, i32 2
  %3 = load i32, i32* %dStage, align 4, !tbaa !57
  store i32 %3, i32* %result, align 4, !tbaa !3
  %4 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpIn = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %4, i32 0, i32 6
  %5 = load i8*, i8** %tmpIn, align 8, !tbaa !58
  call void @free(i8* %5)
  %6 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %6, i32 0, i32 9
  %7 = load i8*, i8** %tmpOutBuffer, align 4, !tbaa !59
  call void @free(i8* %7)
  %8 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %9 = bitcast %struct.LZ4F_dctx_s* %8 to i8*
  call void @free(i8* %9)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = load i32, i32* %result, align 4, !tbaa !3
  %11 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret i32 %10
}

; Function Attrs: nounwind
define void @LZ4F_resetDecompressionContext(%struct.LZ4F_dctx_s* %dctx) #0 {
entry:
  %dctx.addr = alloca %struct.LZ4F_dctx_s*, align 4
  store %struct.LZ4F_dctx_s* %dctx, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %0 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %0, i32 0, i32 2
  store i32 0, i32* %dStage, align 4, !tbaa !57
  %1 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %1, i32 0, i32 10
  store i8* null, i8** %dict, align 8, !tbaa !60
  %2 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %2, i32 0, i32 11
  store i32 0, i32* %dictSize, align 4, !tbaa !61
  ret void
}

; Function Attrs: nounwind
define hidden i32 @LZ4F_headerSize(i8* %src, i32 %srcSize) #0 {
entry:
  %retval = alloca i32, align 4
  %src.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %FLG = alloca i8, align 1
  %contentSizeFlag = alloca i32, align 4
  %dictIDFlag = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !3
  %0 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %cmp = icmp eq i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call i32 @err0r(i32 15)
  store i32 %call, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %cmp1 = icmp ult i32 %1, 5
  br i1 %cmp1, label %if.then2, label %if.end4

if.then2:                                         ; preds = %if.end
  %call3 = call i32 @err0r(i32 12)
  store i32 %call3, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %2 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %call5 = call i32 @LZ4F_readLE32(i8* %2)
  %and = and i32 %call5, -16
  %cmp6 = icmp eq i32 %and, 407710288
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %if.end4
  store i32 8, i32* %retval, align 4
  br label %return

if.end8:                                          ; preds = %if.end4
  %3 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %call9 = call i32 @LZ4F_readLE32(i8* %3)
  %cmp10 = icmp ne i32 %call9, 407708164
  br i1 %cmp10, label %if.then11, label %if.end13

if.then11:                                        ; preds = %if.end8
  %call12 = call i32 @err0r(i32 13)
  store i32 %call12, i32* %retval, align 4
  br label %return

if.end13:                                         ; preds = %if.end8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %FLG) #4
  %4 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 4
  %5 = load i8, i8* %arrayidx, align 1, !tbaa !11
  store i8 %5, i8* %FLG, align 1, !tbaa !11
  %6 = bitcast i32* %contentSizeFlag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load i8, i8* %FLG, align 1, !tbaa !11
  %conv = zext i8 %7 to i32
  %shr = ashr i32 %conv, 3
  %and14 = and i32 %shr, 1
  store i32 %and14, i32* %contentSizeFlag, align 4, !tbaa !9
  %8 = bitcast i32* %dictIDFlag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load i8, i8* %FLG, align 1, !tbaa !11
  %conv15 = zext i8 %9 to i32
  %and16 = and i32 %conv15, 1
  store i32 %and16, i32* %dictIDFlag, align 4, !tbaa !9
  %10 = load i32, i32* %contentSizeFlag, align 4, !tbaa !9
  %tobool = icmp ne i32 %10, 0
  %11 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 8, i32 0
  %add = add i32 7, %cond
  %12 = load i32, i32* %dictIDFlag, align 4, !tbaa !9
  %tobool17 = icmp ne i32 %12, 0
  %13 = zext i1 %tobool17 to i64
  %cond18 = select i1 %tobool17, i32 4, i32 0
  %add19 = add i32 %add, %cond18
  store i32 %add19, i32* %retval, align 4
  %14 = bitcast i32* %dictIDFlag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i32* %contentSizeFlag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %FLG) #4
  br label %return

return:                                           ; preds = %if.end13, %if.then11, %if.then7, %if.then2, %if.then
  %16 = load i32, i32* %retval, align 4
  ret i32 %16
}

; Function Attrs: nounwind
define internal i32 @LZ4F_readLE32(i8* %src) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %srcPtr = alloca i8*, align 4
  %value32 = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  %0 = bitcast i8** %srcPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %src.addr, align 4, !tbaa !7
  store i8* %1, i8** %srcPtr, align 4, !tbaa !7
  %2 = bitcast i32* %value32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 0
  %4 = load i8, i8* %arrayidx, align 1, !tbaa !11
  %conv = zext i8 %4 to i32
  store i32 %conv, i32* %value32, align 4, !tbaa !9
  %5 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %arrayidx1 = getelementptr inbounds i8, i8* %5, i32 1
  %6 = load i8, i8* %arrayidx1, align 1, !tbaa !11
  %conv2 = zext i8 %6 to i32
  %shl = shl i32 %conv2, 8
  %7 = load i32, i32* %value32, align 4, !tbaa !9
  %add = add i32 %7, %shl
  store i32 %add, i32* %value32, align 4, !tbaa !9
  %8 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %arrayidx3 = getelementptr inbounds i8, i8* %8, i32 2
  %9 = load i8, i8* %arrayidx3, align 1, !tbaa !11
  %conv4 = zext i8 %9 to i32
  %shl5 = shl i32 %conv4, 16
  %10 = load i32, i32* %value32, align 4, !tbaa !9
  %add6 = add i32 %10, %shl5
  store i32 %add6, i32* %value32, align 4, !tbaa !9
  %11 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %arrayidx7 = getelementptr inbounds i8, i8* %11, i32 3
  %12 = load i8, i8* %arrayidx7, align 1, !tbaa !11
  %conv8 = zext i8 %12 to i32
  %shl9 = shl i32 %conv8, 24
  %13 = load i32, i32* %value32, align 4, !tbaa !9
  %add10 = add i32 %13, %shl9
  store i32 %add10, i32* %value32, align 4, !tbaa !9
  %14 = load i32, i32* %value32, align 4, !tbaa !9
  %15 = bitcast i32* %value32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  %16 = bitcast i8** %srcPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #4
  ret i32 %14
}

; Function Attrs: nounwind
define i32 @LZ4F_getFrameInfo(%struct.LZ4F_dctx_s* %dctx, %struct.LZ4F_frameInfo_t* %frameInfoPtr, i8* %srcBuffer, i32* %srcSizePtr) #0 {
entry:
  %retval = alloca i32, align 4
  %dctx.addr = alloca %struct.LZ4F_dctx_s*, align 4
  %frameInfoPtr.addr = alloca %struct.LZ4F_frameInfo_t*, align 4
  %srcBuffer.addr = alloca i8*, align 4
  %srcSizePtr.addr = alloca i32*, align 4
  %o = alloca i32, align 4
  %i = alloca i32, align 4
  %hSize = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %decodeResult = alloca i32, align 4
  store %struct.LZ4F_dctx_s* %dctx, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  store %struct.LZ4F_frameInfo_t* %frameInfoPtr, %struct.LZ4F_frameInfo_t** %frameInfoPtr.addr, align 4, !tbaa !7
  store i8* %srcBuffer, i8** %srcBuffer.addr, align 4, !tbaa !7
  store i32* %srcSizePtr, i32** %srcSizePtr.addr, align 4, !tbaa !7
  %0 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %0, i32 0, i32 2
  %1 = load i32, i32* %dStage, align 4, !tbaa !57
  %cmp = icmp ugt i32 %1, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = bitcast i32* %o to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 0, i32* %o, align 4, !tbaa !3
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  store i32 0, i32* %i, align 4, !tbaa !3
  %4 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  store i32 0, i32* %4, align 4, !tbaa !3
  %5 = load %struct.LZ4F_frameInfo_t*, %struct.LZ4F_frameInfo_t** %frameInfoPtr.addr, align 4, !tbaa !7
  %6 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %6, i32 0, i32 0
  %7 = bitcast %struct.LZ4F_frameInfo_t* %5 to i8*
  %8 = bitcast %struct.LZ4F_frameInfo_t* %frameInfo to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %7, i8* align 8 %8, i32 32, i1 false), !tbaa.struct !62
  %9 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %call = call i32 @LZ4F_decompress(%struct.LZ4F_dctx_s* %9, i8* null, i32* %o, i8* null, i32* %i, %struct.LZ4F_decompressOptions_t* null)
  store i32 %call, i32* %retval, align 4
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i32* %o to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  br label %return

if.else:                                          ; preds = %entry
  %12 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage1 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %12, i32 0, i32 2
  %13 = load i32, i32* %dStage1, align 4, !tbaa !57
  %cmp2 = icmp eq i32 %13, 1
  br i1 %cmp2, label %if.then3, label %if.else5

if.then3:                                         ; preds = %if.else
  %14 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  store i32 0, i32* %14, align 4, !tbaa !3
  %call4 = call i32 @err0r(i32 19)
  store i32 %call4, i32* %retval, align 4
  br label %return

if.else5:                                         ; preds = %if.else
  %15 = bitcast i32* %hSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load i8*, i8** %srcBuffer.addr, align 4, !tbaa !7
  %17 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  %18 = load i32, i32* %17, align 4, !tbaa !3
  %call6 = call i32 @LZ4F_headerSize(i8* %16, i32 %18)
  store i32 %call6, i32* %hSize, align 4, !tbaa !3
  %19 = load i32, i32* %hSize, align 4, !tbaa !3
  %call7 = call i32 @LZ4F_isError(i32 %19)
  %tobool = icmp ne i32 %call7, 0
  br i1 %tobool, label %if.then8, label %if.end

if.then8:                                         ; preds = %if.else5
  %20 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  store i32 0, i32* %20, align 4, !tbaa !3
  %21 = load i32, i32* %hSize, align 4, !tbaa !3
  store i32 %21, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.else5
  %22 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  %23 = load i32, i32* %22, align 4, !tbaa !3
  %24 = load i32, i32* %hSize, align 4, !tbaa !3
  %cmp9 = icmp ult i32 %23, %24
  br i1 %cmp9, label %if.then10, label %if.end12

if.then10:                                        ; preds = %if.end
  %25 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  store i32 0, i32* %25, align 4, !tbaa !3
  %call11 = call i32 @err0r(i32 12)
  store i32 %call11, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end12:                                         ; preds = %if.end
  %26 = bitcast i32* %decodeResult to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  %27 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %28 = load i8*, i8** %srcBuffer.addr, align 4, !tbaa !7
  %29 = load i32, i32* %hSize, align 4, !tbaa !3
  %call13 = call i32 @LZ4F_decodeHeader(%struct.LZ4F_dctx_s* %27, i8* %28, i32 %29)
  store i32 %call13, i32* %decodeResult, align 4, !tbaa !3
  %30 = load i32, i32* %decodeResult, align 4, !tbaa !3
  %call14 = call i32 @LZ4F_isError(i32 %30)
  %tobool15 = icmp ne i32 %call14, 0
  br i1 %tobool15, label %if.then16, label %if.else17

if.then16:                                        ; preds = %if.end12
  %31 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  store i32 0, i32* %31, align 4, !tbaa !3
  br label %if.end18

if.else17:                                        ; preds = %if.end12
  %32 = load i32, i32* %decodeResult, align 4, !tbaa !3
  %33 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  store i32 %32, i32* %33, align 4, !tbaa !3
  store i32 4, i32* %decodeResult, align 4, !tbaa !3
  br label %if.end18

if.end18:                                         ; preds = %if.else17, %if.then16
  %34 = load %struct.LZ4F_frameInfo_t*, %struct.LZ4F_frameInfo_t** %frameInfoPtr.addr, align 4, !tbaa !7
  %35 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo19 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %35, i32 0, i32 0
  %36 = bitcast %struct.LZ4F_frameInfo_t* %34 to i8*
  %37 = bitcast %struct.LZ4F_frameInfo_t* %frameInfo19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %36, i8* align 8 %37, i32 32, i1 false), !tbaa.struct !62
  %38 = load i32, i32* %decodeResult, align 4, !tbaa !3
  store i32 %38, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %39 = bitcast i32* %decodeResult to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  br label %cleanup

cleanup:                                          ; preds = %if.end18, %if.then10, %if.then8
  %40 = bitcast i32* %hSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  br label %return

return:                                           ; preds = %cleanup, %if.then3, %if.then
  %41 = load i32, i32* %retval, align 4
  ret i32 %41
}

; Function Attrs: nounwind
define i32 @LZ4F_decompress(%struct.LZ4F_dctx_s* %dctx, i8* %dstBuffer, i32* %dstSizePtr, i8* %srcBuffer, i32* %srcSizePtr, %struct.LZ4F_decompressOptions_t* %decompressOptionsPtr) #0 {
entry:
  %retval = alloca i32, align 4
  %dctx.addr = alloca %struct.LZ4F_dctx_s*, align 4
  %dstBuffer.addr = alloca i8*, align 4
  %dstSizePtr.addr = alloca i32*, align 4
  %srcBuffer.addr = alloca i8*, align 4
  %srcSizePtr.addr = alloca i32*, align 4
  %decompressOptionsPtr.addr = alloca %struct.LZ4F_decompressOptions_t*, align 4
  %optionsNull = alloca %struct.LZ4F_decompressOptions_t, align 4
  %srcStart = alloca i8*, align 4
  %srcEnd = alloca i8*, align 4
  %srcPtr = alloca i8*, align 4
  %dstStart = alloca i8*, align 4
  %dstEnd = alloca i8*, align 4
  %dstPtr = alloca i8*, align 4
  %selectedIn = alloca i8*, align 4
  %doAnotherStage = alloca i32, align 4
  %nextSrcSizeHint = alloca i32, align 4
  %hSize = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %sizeToCopy = alloca i32, align 4
  %hSize46 = alloca i32, align 4
  %bufferNeeded = alloca i32, align 4
  %remainingInput = alloca i32, align 4
  %wantedData = alloca i32, align 4
  %sizeToCopy113 = alloca i32, align 4
  %nextCBlockSize = alloca i32, align 4
  %crcSize = alloca i32, align 4
  %minBuffSize = alloca i32, align 4
  %sizeToCopy192 = alloca i32, align 4
  %crcSrc = alloca i8*, align 4
  %stillToCopy = alloca i32, align 4
  %sizeToCopy266 = alloca i32, align 4
  %readCRC = alloca i32, align 4
  %calcCRC = alloca i32, align 4
  %wantedData329 = alloca i32, align 4
  %inputLeft = alloca i32, align 4
  %sizeToCopy336 = alloca i32, align 4
  %readBlockCrc = alloca i32, align 4
  %calcBlockCrc = alloca i32, align 4
  %dict = alloca i8*, align 4
  %dictSize = alloca i32, align 4
  %decodedSize = alloca i32, align 4
  %reservedDictSpace = alloca i32, align 4
  %dict484 = alloca i8*, align 4
  %dictSize486 = alloca i32, align 4
  %decodedSize488 = alloca i32, align 4
  %sizeToCopy531 = alloca i32, align 4
  %remainingInput597 = alloca i32, align 4
  %wantedData601 = alloca i32, align 4
  %sizeToCopy604 = alloca i32, align 4
  %readCRC631 = alloca i32, align 4
  %resultCRC = alloca i32, align 4
  %sizeToCopy661 = alloca i32, align 4
  %SFrameSize = alloca i32, align 4
  %skipSize = alloca i32, align 4
  %preserveSize = alloca i32, align 4
  %copySize = alloca i32, align 4
  %oldDictEnd = alloca i8*, align 4
  %oldDictEnd791 = alloca i8*, align 4
  %newDictSize = alloca i32, align 4
  store %struct.LZ4F_dctx_s* %dctx, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  store i8* %dstBuffer, i8** %dstBuffer.addr, align 4, !tbaa !7
  store i32* %dstSizePtr, i32** %dstSizePtr.addr, align 4, !tbaa !7
  store i8* %srcBuffer, i8** %srcBuffer.addr, align 4, !tbaa !7
  store i32* %srcSizePtr, i32** %srcSizePtr.addr, align 4, !tbaa !7
  store %struct.LZ4F_decompressOptions_t* %decompressOptionsPtr, %struct.LZ4F_decompressOptions_t** %decompressOptionsPtr.addr, align 4, !tbaa !7
  %0 = bitcast %struct.LZ4F_decompressOptions_t* %optionsNull to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #4
  %1 = bitcast i8** %srcStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i8*, i8** %srcBuffer.addr, align 4, !tbaa !7
  store i8* %2, i8** %srcStart, align 4, !tbaa !7
  %3 = bitcast i8** %srcEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load i8*, i8** %srcStart, align 4, !tbaa !7
  %5 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  %6 = load i32, i32* %5, align 4, !tbaa !3
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %6
  store i8* %add.ptr, i8** %srcEnd, align 4, !tbaa !7
  %7 = bitcast i8** %srcPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load i8*, i8** %srcStart, align 4, !tbaa !7
  store i8* %8, i8** %srcPtr, align 4, !tbaa !7
  %9 = bitcast i8** %dstStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load i8*, i8** %dstBuffer.addr, align 4, !tbaa !7
  store i8* %10, i8** %dstStart, align 4, !tbaa !7
  %11 = bitcast i8** %dstEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load i8*, i8** %dstStart, align 4, !tbaa !7
  %13 = load i32*, i32** %dstSizePtr.addr, align 4, !tbaa !7
  %14 = load i32, i32* %13, align 4, !tbaa !3
  %add.ptr1 = getelementptr inbounds i8, i8* %12, i32 %14
  store i8* %add.ptr1, i8** %dstEnd, align 4, !tbaa !7
  %15 = bitcast i8** %dstPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load i8*, i8** %dstStart, align 4, !tbaa !7
  store i8* %16, i8** %dstPtr, align 4, !tbaa !7
  %17 = bitcast i8** %selectedIn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  store i8* null, i8** %selectedIn, align 4, !tbaa !7
  %18 = bitcast i32* %doAnotherStage to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  store i32 1, i32* %doAnotherStage, align 4, !tbaa !9
  %19 = bitcast i32* %nextSrcSizeHint to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #4
  store i32 1, i32* %nextSrcSizeHint, align 4, !tbaa !3
  %20 = bitcast %struct.LZ4F_decompressOptions_t* %optionsNull to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %20, i8 0, i32 16, i1 false)
  %21 = load %struct.LZ4F_decompressOptions_t*, %struct.LZ4F_decompressOptions_t** %decompressOptionsPtr.addr, align 4, !tbaa !7
  %cmp = icmp eq %struct.LZ4F_decompressOptions_t* %21, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %struct.LZ4F_decompressOptions_t* %optionsNull, %struct.LZ4F_decompressOptions_t** %decompressOptionsPtr.addr, align 4, !tbaa !7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %22 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  store i32 0, i32* %22, align 4, !tbaa !3
  %23 = load i32*, i32** %dstSizePtr.addr, align 4, !tbaa !7
  store i32 0, i32* %23, align 4, !tbaa !3
  br label %while.cond

while.cond:                                       ; preds = %sw.epilog, %if.end
  %24 = load i32, i32* %doAnotherStage, align 4, !tbaa !9
  %tobool = icmp ne i32 %24, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %25 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %25, i32 0, i32 2
  %26 = load i32, i32* %dStage, align 4, !tbaa !57
  switch i32 %26, label %sw.epilog [
    i32 0, label %sw.bb
    i32 1, label %sw.bb20
    i32 2, label %sw.bb57
    i32 3, label %sw.bb94
    i32 4, label %sw.bb107
    i32 5, label %sw.bb174
    i32 6, label %sw.bb252
    i32 7, label %sw.bb314
    i32 8, label %sw.bb328
    i32 9, label %sw.bb530
    i32 10, label %sw.bb570
    i32 11, label %sw.bb596
    i32 12, label %sw.bb643
    i32 13, label %sw.bb660
    i32 14, label %sw.bb710
  ]

sw.bb:                                            ; preds = %while.body
  %27 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %28 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast = ptrtoint i8* %27 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %28 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %cmp2 = icmp uge i32 %sub.ptr.sub, 19
  br i1 %cmp2, label %if.then3, label %if.end12

if.then3:                                         ; preds = %sw.bb
  %29 = bitcast i32* %hSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  %30 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %31 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %32 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %33 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast4 = ptrtoint i8* %32 to i32
  %sub.ptr.rhs.cast5 = ptrtoint i8* %33 to i32
  %sub.ptr.sub6 = sub i32 %sub.ptr.lhs.cast4, %sub.ptr.rhs.cast5
  %call = call i32 @LZ4F_decodeHeader(%struct.LZ4F_dctx_s* %30, i8* %31, i32 %sub.ptr.sub6)
  store i32 %call, i32* %hSize, align 4, !tbaa !3
  %34 = load i32, i32* %hSize, align 4, !tbaa !3
  %call7 = call i32 @LZ4F_isError(i32 %34)
  %tobool8 = icmp ne i32 %call7, 0
  br i1 %tobool8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %if.then3
  %35 = load i32, i32* %hSize, align 4, !tbaa !3
  store i32 %35, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end10:                                         ; preds = %if.then3
  %36 = load i32, i32* %hSize, align 4, !tbaa !3
  %37 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %add.ptr11 = getelementptr inbounds i8, i8* %37, i32 %36
  store i8* %add.ptr11, i8** %srcPtr, align 4, !tbaa !7
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end10, %if.then9
  %38 = bitcast i32* %hSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup824 [
    i32 4, label %sw.epilog
  ]

if.end12:                                         ; preds = %sw.bb
  %39 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %39, i32 0, i32 7
  store i32 0, i32* %tmpInSize, align 4, !tbaa !63
  %40 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %41 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast13 = ptrtoint i8* %40 to i32
  %sub.ptr.rhs.cast14 = ptrtoint i8* %41 to i32
  %sub.ptr.sub15 = sub i32 %sub.ptr.lhs.cast13, %sub.ptr.rhs.cast14
  %cmp16 = icmp eq i32 %sub.ptr.sub15, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end12
  store i32 7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup824

if.end18:                                         ; preds = %if.end12
  %42 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %42, i32 0, i32 8
  store i32 7, i32* %tmpInTarget, align 8, !tbaa !64
  %43 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage19 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %43, i32 0, i32 2
  store i32 1, i32* %dStage19, align 4, !tbaa !57
  br label %sw.bb20

sw.bb20:                                          ; preds = %while.body, %if.end18
  %44 = bitcast i32* %sizeToCopy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #4
  %45 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget21 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %45, i32 0, i32 8
  %46 = load i32, i32* %tmpInTarget21, align 8, !tbaa !64
  %47 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize22 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %47, i32 0, i32 7
  %48 = load i32, i32* %tmpInSize22, align 4, !tbaa !63
  %sub = sub i32 %46, %48
  %49 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %50 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast23 = ptrtoint i8* %49 to i32
  %sub.ptr.rhs.cast24 = ptrtoint i8* %50 to i32
  %sub.ptr.sub25 = sub i32 %sub.ptr.lhs.cast23, %sub.ptr.rhs.cast24
  %cmp26 = icmp ult i32 %sub, %sub.ptr.sub25
  br i1 %cmp26, label %cond.true, label %cond.false

cond.true:                                        ; preds = %sw.bb20
  %51 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget27 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %51, i32 0, i32 8
  %52 = load i32, i32* %tmpInTarget27, align 8, !tbaa !64
  %53 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize28 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %53, i32 0, i32 7
  %54 = load i32, i32* %tmpInSize28, align 4, !tbaa !63
  %sub29 = sub i32 %52, %54
  br label %cond.end

cond.false:                                       ; preds = %sw.bb20
  %55 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %56 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast30 = ptrtoint i8* %55 to i32
  %sub.ptr.rhs.cast31 = ptrtoint i8* %56 to i32
  %sub.ptr.sub32 = sub i32 %sub.ptr.lhs.cast30, %sub.ptr.rhs.cast31
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub29, %cond.true ], [ %sub.ptr.sub32, %cond.false ]
  store i32 %cond, i32* %sizeToCopy, align 4, !tbaa !3
  %57 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %header = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %57, i32 0, i32 17
  %arraydecay = getelementptr inbounds [19 x i8], [19 x i8]* %header, i32 0, i32 0
  %58 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize33 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %58, i32 0, i32 7
  %59 = load i32, i32* %tmpInSize33, align 4, !tbaa !63
  %add.ptr34 = getelementptr inbounds i8, i8* %arraydecay, i32 %59
  %60 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %61 = load i32, i32* %sizeToCopy, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr34, i8* align 1 %60, i32 %61, i1 false)
  %62 = load i32, i32* %sizeToCopy, align 4, !tbaa !3
  %63 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize35 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %63, i32 0, i32 7
  %64 = load i32, i32* %tmpInSize35, align 4, !tbaa !63
  %add = add i32 %64, %62
  store i32 %add, i32* %tmpInSize35, align 4, !tbaa !63
  %65 = load i32, i32* %sizeToCopy, align 4, !tbaa !3
  %66 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %add.ptr36 = getelementptr inbounds i8, i8* %66, i32 %65
  store i8* %add.ptr36, i8** %srcPtr, align 4, !tbaa !7
  %67 = bitcast i32* %sizeToCopy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #4
  %68 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize37 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %68, i32 0, i32 7
  %69 = load i32, i32* %tmpInSize37, align 4, !tbaa !63
  %70 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget38 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %70, i32 0, i32 8
  %71 = load i32, i32* %tmpInTarget38, align 8, !tbaa !64
  %cmp39 = icmp ult i32 %69, %71
  br i1 %cmp39, label %if.then40, label %if.end45

if.then40:                                        ; preds = %cond.end
  %72 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget41 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %72, i32 0, i32 8
  %73 = load i32, i32* %tmpInTarget41, align 8, !tbaa !64
  %74 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize42 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %74, i32 0, i32 7
  %75 = load i32, i32* %tmpInSize42, align 4, !tbaa !63
  %sub43 = sub i32 %73, %75
  %add44 = add i32 %sub43, 4
  store i32 %add44, i32* %nextSrcSizeHint, align 4, !tbaa !3
  store i32 0, i32* %doAnotherStage, align 4, !tbaa !9
  br label %sw.epilog

if.end45:                                         ; preds = %cond.end
  %76 = bitcast i32* %hSize46 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #4
  %77 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %78 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %header47 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %78, i32 0, i32 17
  %arraydecay48 = getelementptr inbounds [19 x i8], [19 x i8]* %header47, i32 0, i32 0
  %79 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget49 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %79, i32 0, i32 8
  %80 = load i32, i32* %tmpInTarget49, align 8, !tbaa !64
  %call50 = call i32 @LZ4F_decodeHeader(%struct.LZ4F_dctx_s* %77, i8* %arraydecay48, i32 %80)
  store i32 %call50, i32* %hSize46, align 4, !tbaa !3
  %81 = load i32, i32* %hSize46, align 4, !tbaa !3
  %call51 = call i32 @LZ4F_isError(i32 %81)
  %tobool52 = icmp ne i32 %call51, 0
  br i1 %tobool52, label %if.then53, label %if.end54

if.then53:                                        ; preds = %if.end45
  %82 = load i32, i32* %hSize46, align 4, !tbaa !3
  store i32 %82, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup55

if.end54:                                         ; preds = %if.end45
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup55

cleanup55:                                        ; preds = %if.end54, %if.then53
  %83 = bitcast i32* %hSize46 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  %cleanup.dest56 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest56, label %cleanup824 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup55
  br label %sw.epilog

sw.bb57:                                          ; preds = %while.body
  %84 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %84, i32 0, i32 0
  %contentChecksumFlag = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo, i32 0, i32 2
  %85 = load i32, i32* %contentChecksumFlag, align 8, !tbaa !65
  %tobool58 = icmp ne i32 %85, 0
  br i1 %tobool58, label %if.then59, label %if.end61

if.then59:                                        ; preds = %sw.bb57
  %86 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %xxh = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %86, i32 0, i32 15
  %call60 = call i32 @LZ4_XXH32_reset(%struct.XXH32_state_s* %xxh, i32 0)
  br label %if.end61

if.end61:                                         ; preds = %if.then59, %sw.bb57
  %87 = bitcast i32* %bufferNeeded to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #4
  %88 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %maxBlockSize = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %88, i32 0, i32 4
  %89 = load i32, i32* %maxBlockSize, align 8, !tbaa !66
  %90 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo62 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %90, i32 0, i32 0
  %blockMode = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo62, i32 0, i32 1
  %91 = load i32, i32* %blockMode, align 4, !tbaa !67
  %cmp63 = icmp eq i32 %91, 0
  %92 = zext i1 %cmp63 to i64
  %cond64 = select i1 %cmp63, i32 131072, i32 0
  %add65 = add i32 %89, %cond64
  store i32 %add65, i32* %bufferNeeded, align 4, !tbaa !3
  %93 = load i32, i32* %bufferNeeded, align 4, !tbaa !3
  %94 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %maxBufferSize = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %94, i32 0, i32 5
  %95 = load i32, i32* %maxBufferSize, align 4, !tbaa !68
  %cmp66 = icmp ugt i32 %93, %95
  br i1 %cmp66, label %if.then67, label %if.end86

if.then67:                                        ; preds = %if.end61
  %96 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %maxBufferSize68 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %96, i32 0, i32 5
  store i32 0, i32* %maxBufferSize68, align 4, !tbaa !68
  %97 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpIn = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %97, i32 0, i32 6
  %98 = load i8*, i8** %tmpIn, align 8, !tbaa !58
  call void @free(i8* %98)
  %99 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %maxBlockSize69 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %99, i32 0, i32 4
  %100 = load i32, i32* %maxBlockSize69, align 8, !tbaa !66
  %add70 = add i32 %100, 4
  %call71 = call i8* @malloc(i32 %add70)
  %101 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpIn72 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %101, i32 0, i32 6
  store i8* %call71, i8** %tmpIn72, align 8, !tbaa !58
  %102 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpIn73 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %102, i32 0, i32 6
  %103 = load i8*, i8** %tmpIn73, align 8, !tbaa !58
  %cmp74 = icmp eq i8* %103, null
  br i1 %cmp74, label %if.then75, label %if.end77

if.then75:                                        ; preds = %if.then67
  %call76 = call i32 @err0r(i32 9)
  store i32 %call76, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup87

if.end77:                                         ; preds = %if.then67
  %104 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %104, i32 0, i32 9
  %105 = load i8*, i8** %tmpOutBuffer, align 4, !tbaa !59
  call void @free(i8* %105)
  %106 = load i32, i32* %bufferNeeded, align 4, !tbaa !3
  %call78 = call i8* @malloc(i32 %106)
  %107 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer79 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %107, i32 0, i32 9
  store i8* %call78, i8** %tmpOutBuffer79, align 4, !tbaa !59
  %108 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer80 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %108, i32 0, i32 9
  %109 = load i8*, i8** %tmpOutBuffer80, align 4, !tbaa !59
  %cmp81 = icmp eq i8* %109, null
  br i1 %cmp81, label %if.then82, label %if.end84

if.then82:                                        ; preds = %if.end77
  %call83 = call i32 @err0r(i32 9)
  store i32 %call83, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup87

if.end84:                                         ; preds = %if.end77
  %110 = load i32, i32* %bufferNeeded, align 4, !tbaa !3
  %111 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %maxBufferSize85 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %111, i32 0, i32 5
  store i32 %110, i32* %maxBufferSize85, align 4, !tbaa !68
  br label %if.end86

if.end86:                                         ; preds = %if.end84, %if.end61
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup87

cleanup87:                                        ; preds = %if.end86, %if.then82, %if.then75
  %112 = bitcast i32* %bufferNeeded to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #4
  %cleanup.dest88 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest88, label %cleanup824 [
    i32 0, label %cleanup.cont89
  ]

cleanup.cont89:                                   ; preds = %cleanup87
  %113 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize90 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %113, i32 0, i32 7
  store i32 0, i32* %tmpInSize90, align 4, !tbaa !63
  %114 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget91 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %114, i32 0, i32 8
  store i32 0, i32* %tmpInTarget91, align 8, !tbaa !64
  %115 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer92 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %115, i32 0, i32 9
  %116 = load i8*, i8** %tmpOutBuffer92, align 4, !tbaa !59
  %117 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOut = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %117, i32 0, i32 12
  store i8* %116, i8** %tmpOut, align 8, !tbaa !69
  %118 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutStart = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %118, i32 0, i32 14
  store i32 0, i32* %tmpOutStart, align 8, !tbaa !70
  %119 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutSize = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %119, i32 0, i32 13
  store i32 0, i32* %tmpOutSize, align 4, !tbaa !71
  %120 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage93 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %120, i32 0, i32 2
  store i32 3, i32* %dStage93, align 4, !tbaa !57
  br label %sw.bb94

sw.bb94:                                          ; preds = %while.body, %cleanup.cont89
  %121 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %122 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast95 = ptrtoint i8* %121 to i32
  %sub.ptr.rhs.cast96 = ptrtoint i8* %122 to i32
  %sub.ptr.sub97 = sub i32 %sub.ptr.lhs.cast95, %sub.ptr.rhs.cast96
  %cmp98 = icmp uge i32 %sub.ptr.sub97, 4
  br i1 %cmp98, label %if.then99, label %if.else

if.then99:                                        ; preds = %sw.bb94
  %123 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  store i8* %123, i8** %selectedIn, align 4, !tbaa !7
  %124 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %add.ptr100 = getelementptr inbounds i8, i8* %124, i32 4
  store i8* %add.ptr100, i8** %srcPtr, align 4, !tbaa !7
  br label %if.end103

if.else:                                          ; preds = %sw.bb94
  %125 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize101 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %125, i32 0, i32 7
  store i32 0, i32* %tmpInSize101, align 4, !tbaa !63
  %126 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage102 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %126, i32 0, i32 2
  store i32 4, i32* %dStage102, align 4, !tbaa !57
  br label %if.end103

if.end103:                                        ; preds = %if.else, %if.then99
  %127 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage104 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %127, i32 0, i32 2
  %128 = load i32, i32* %dStage104, align 4, !tbaa !57
  %cmp105 = icmp eq i32 %128, 4
  br i1 %cmp105, label %if.then106, label %if.end137

if.then106:                                       ; preds = %if.end103
  br label %sw.bb107

sw.bb107:                                         ; preds = %while.body, %if.then106
  %129 = bitcast i32* %remainingInput to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %129) #4
  %130 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %131 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast108 = ptrtoint i8* %130 to i32
  %sub.ptr.rhs.cast109 = ptrtoint i8* %131 to i32
  %sub.ptr.sub110 = sub i32 %sub.ptr.lhs.cast108, %sub.ptr.rhs.cast109
  store i32 %sub.ptr.sub110, i32* %remainingInput, align 4, !tbaa !3
  %132 = bitcast i32* %wantedData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %132) #4
  %133 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize111 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %133, i32 0, i32 7
  %134 = load i32, i32* %tmpInSize111, align 4, !tbaa !63
  %sub112 = sub i32 4, %134
  store i32 %sub112, i32* %wantedData, align 4, !tbaa !3
  %135 = bitcast i32* %sizeToCopy113 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %135) #4
  %136 = load i32, i32* %wantedData, align 4, !tbaa !3
  %137 = load i32, i32* %remainingInput, align 4, !tbaa !3
  %cmp114 = icmp ult i32 %136, %137
  br i1 %cmp114, label %cond.true115, label %cond.false116

cond.true115:                                     ; preds = %sw.bb107
  %138 = load i32, i32* %wantedData, align 4, !tbaa !3
  br label %cond.end117

cond.false116:                                    ; preds = %sw.bb107
  %139 = load i32, i32* %remainingInput, align 4, !tbaa !3
  br label %cond.end117

cond.end117:                                      ; preds = %cond.false116, %cond.true115
  %cond118 = phi i32 [ %138, %cond.true115 ], [ %139, %cond.false116 ]
  store i32 %cond118, i32* %sizeToCopy113, align 4, !tbaa !3
  %140 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpIn119 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %140, i32 0, i32 6
  %141 = load i8*, i8** %tmpIn119, align 8, !tbaa !58
  %142 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize120 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %142, i32 0, i32 7
  %143 = load i32, i32* %tmpInSize120, align 4, !tbaa !63
  %add.ptr121 = getelementptr inbounds i8, i8* %141, i32 %143
  %144 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %145 = load i32, i32* %sizeToCopy113, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr121, i8* align 1 %144, i32 %145, i1 false)
  %146 = load i32, i32* %sizeToCopy113, align 4, !tbaa !3
  %147 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %add.ptr122 = getelementptr inbounds i8, i8* %147, i32 %146
  store i8* %add.ptr122, i8** %srcPtr, align 4, !tbaa !7
  %148 = load i32, i32* %sizeToCopy113, align 4, !tbaa !3
  %149 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize123 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %149, i32 0, i32 7
  %150 = load i32, i32* %tmpInSize123, align 4, !tbaa !63
  %add124 = add i32 %150, %148
  store i32 %add124, i32* %tmpInSize123, align 4, !tbaa !63
  %151 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize125 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %151, i32 0, i32 7
  %152 = load i32, i32* %tmpInSize125, align 4, !tbaa !63
  %cmp126 = icmp ult i32 %152, 4
  br i1 %cmp126, label %if.then127, label %if.end130

if.then127:                                       ; preds = %cond.end117
  %153 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize128 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %153, i32 0, i32 7
  %154 = load i32, i32* %tmpInSize128, align 4, !tbaa !63
  %sub129 = sub i32 4, %154
  store i32 %sub129, i32* %nextSrcSizeHint, align 4, !tbaa !3
  store i32 0, i32* %doAnotherStage, align 4, !tbaa !9
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup132

if.end130:                                        ; preds = %cond.end117
  %155 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpIn131 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %155, i32 0, i32 6
  %156 = load i8*, i8** %tmpIn131, align 8, !tbaa !58
  store i8* %156, i8** %selectedIn, align 4, !tbaa !7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup132

cleanup132:                                       ; preds = %if.end130, %if.then127
  %157 = bitcast i32* %sizeToCopy113 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #4
  %158 = bitcast i32* %wantedData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #4
  %159 = bitcast i32* %remainingInput to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #4
  %cleanup.dest135 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest135, label %unreachable [
    i32 0, label %cleanup.cont136
    i32 4, label %sw.epilog
  ]

cleanup.cont136:                                  ; preds = %cleanup132
  br label %if.end137

if.end137:                                        ; preds = %cleanup.cont136, %if.end103
  %160 = bitcast i32* %nextCBlockSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %160) #4
  %161 = load i8*, i8** %selectedIn, align 4, !tbaa !7
  %call138 = call i32 @LZ4F_readLE32(i8* %161)
  %and = and i32 %call138, 2147483647
  store i32 %and, i32* %nextCBlockSize, align 4, !tbaa !3
  %162 = bitcast i32* %crcSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %162) #4
  %163 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo139 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %163, i32 0, i32 0
  %blockChecksumFlag = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo139, i32 0, i32 6
  %164 = load i32, i32* %blockChecksumFlag, align 4, !tbaa !72
  %mul = mul i32 %164, 4
  store i32 %mul, i32* %crcSize, align 4, !tbaa !3
  %165 = load i32, i32* %nextCBlockSize, align 4, !tbaa !3
  %cmp140 = icmp eq i32 %165, 0
  br i1 %cmp140, label %if.then141, label %if.end143

if.then141:                                       ; preds = %if.end137
  %166 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage142 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %166, i32 0, i32 2
  store i32 10, i32* %dStage142, align 4, !tbaa !57
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup171

if.end143:                                        ; preds = %if.end137
  %167 = load i32, i32* %nextCBlockSize, align 4, !tbaa !3
  %168 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %maxBlockSize144 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %168, i32 0, i32 4
  %169 = load i32, i32* %maxBlockSize144, align 8, !tbaa !66
  %cmp145 = icmp ugt i32 %167, %169
  br i1 %cmp145, label %if.then146, label %if.end148

if.then146:                                       ; preds = %if.end143
  %call147 = call i32 @err0r(i32 2)
  store i32 %call147, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup171

if.end148:                                        ; preds = %if.end143
  %170 = load i8*, i8** %selectedIn, align 4, !tbaa !7
  %call149 = call i32 @LZ4F_readLE32(i8* %170)
  %and150 = and i32 %call149, -2147483648
  %tobool151 = icmp ne i32 %and150, 0
  br i1 %tobool151, label %if.then152, label %if.end161

if.then152:                                       ; preds = %if.end148
  %171 = load i32, i32* %nextCBlockSize, align 4, !tbaa !3
  %172 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget153 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %172, i32 0, i32 8
  store i32 %171, i32* %tmpInTarget153, align 8, !tbaa !64
  %173 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo154 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %173, i32 0, i32 0
  %blockChecksumFlag155 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo154, i32 0, i32 6
  %174 = load i32, i32* %blockChecksumFlag155, align 4, !tbaa !72
  %tobool156 = icmp ne i32 %174, 0
  br i1 %tobool156, label %if.then157, label %if.end159

if.then157:                                       ; preds = %if.then152
  %175 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %blockChecksum = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %175, i32 0, i32 16
  %call158 = call i32 @LZ4_XXH32_reset(%struct.XXH32_state_s* %blockChecksum, i32 0)
  br label %if.end159

if.end159:                                        ; preds = %if.then157, %if.then152
  %176 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage160 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %176, i32 0, i32 2
  store i32 5, i32* %dStage160, align 4, !tbaa !57
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup171

if.end161:                                        ; preds = %if.end148
  %177 = load i32, i32* %nextCBlockSize, align 4, !tbaa !3
  %178 = load i32, i32* %crcSize, align 4, !tbaa !3
  %add162 = add i32 %177, %178
  %179 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget163 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %179, i32 0, i32 8
  store i32 %add162, i32* %tmpInTarget163, align 8, !tbaa !64
  %180 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage164 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %180, i32 0, i32 2
  store i32 7, i32* %dStage164, align 4, !tbaa !57
  %181 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %182 = load i8*, i8** %dstEnd, align 4, !tbaa !7
  %cmp165 = icmp eq i8* %181, %182
  br i1 %cmp165, label %if.then167, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end161
  %183 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %184 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %cmp166 = icmp eq i8* %183, %184
  br i1 %cmp166, label %if.then167, label %if.end170

if.then167:                                       ; preds = %lor.lhs.false, %if.end161
  %185 = load i32, i32* %nextCBlockSize, align 4, !tbaa !3
  %add168 = add i32 4, %185
  %186 = load i32, i32* %crcSize, align 4, !tbaa !3
  %add169 = add i32 %add168, %186
  store i32 %add169, i32* %nextSrcSizeHint, align 4, !tbaa !3
  store i32 0, i32* %doAnotherStage, align 4, !tbaa !9
  br label %if.end170

if.end170:                                        ; preds = %if.then167, %lor.lhs.false
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup171

cleanup171:                                       ; preds = %if.end170, %if.end159, %if.then146, %if.then141
  %187 = bitcast i32* %crcSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #4
  %188 = bitcast i32* %nextCBlockSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %188) #4
  %cleanup.dest173 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest173, label %cleanup824 [
    i32 4, label %sw.epilog
  ]

sw.bb174:                                         ; preds = %while.body
  %189 = bitcast i32* %minBuffSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %189) #4
  %190 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %191 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast175 = ptrtoint i8* %190 to i32
  %sub.ptr.rhs.cast176 = ptrtoint i8* %191 to i32
  %sub.ptr.sub177 = sub i32 %sub.ptr.lhs.cast175, %sub.ptr.rhs.cast176
  %192 = load i8*, i8** %dstEnd, align 4, !tbaa !7
  %193 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast178 = ptrtoint i8* %192 to i32
  %sub.ptr.rhs.cast179 = ptrtoint i8* %193 to i32
  %sub.ptr.sub180 = sub i32 %sub.ptr.lhs.cast178, %sub.ptr.rhs.cast179
  %cmp181 = icmp ult i32 %sub.ptr.sub177, %sub.ptr.sub180
  br i1 %cmp181, label %cond.true182, label %cond.false186

cond.true182:                                     ; preds = %sw.bb174
  %194 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %195 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast183 = ptrtoint i8* %194 to i32
  %sub.ptr.rhs.cast184 = ptrtoint i8* %195 to i32
  %sub.ptr.sub185 = sub i32 %sub.ptr.lhs.cast183, %sub.ptr.rhs.cast184
  br label %cond.end190

cond.false186:                                    ; preds = %sw.bb174
  %196 = load i8*, i8** %dstEnd, align 4, !tbaa !7
  %197 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast187 = ptrtoint i8* %196 to i32
  %sub.ptr.rhs.cast188 = ptrtoint i8* %197 to i32
  %sub.ptr.sub189 = sub i32 %sub.ptr.lhs.cast187, %sub.ptr.rhs.cast188
  br label %cond.end190

cond.end190:                                      ; preds = %cond.false186, %cond.true182
  %cond191 = phi i32 [ %sub.ptr.sub185, %cond.true182 ], [ %sub.ptr.sub189, %cond.false186 ]
  store i32 %cond191, i32* %minBuffSize, align 4, !tbaa !3
  %198 = bitcast i32* %sizeToCopy192 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %198) #4
  %199 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget193 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %199, i32 0, i32 8
  %200 = load i32, i32* %tmpInTarget193, align 8, !tbaa !64
  %201 = load i32, i32* %minBuffSize, align 4, !tbaa !3
  %cmp194 = icmp ult i32 %200, %201
  br i1 %cmp194, label %cond.true195, label %cond.false197

cond.true195:                                     ; preds = %cond.end190
  %202 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget196 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %202, i32 0, i32 8
  %203 = load i32, i32* %tmpInTarget196, align 8, !tbaa !64
  br label %cond.end198

cond.false197:                                    ; preds = %cond.end190
  %204 = load i32, i32* %minBuffSize, align 4, !tbaa !3
  br label %cond.end198

cond.end198:                                      ; preds = %cond.false197, %cond.true195
  %cond199 = phi i32 [ %203, %cond.true195 ], [ %204, %cond.false197 ]
  store i32 %cond199, i32* %sizeToCopy192, align 4, !tbaa !3
  %205 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %206 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %207 = load i32, i32* %sizeToCopy192, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %205, i8* align 1 %206, i32 %207, i1 false)
  %208 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo200 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %208, i32 0, i32 0
  %blockChecksumFlag201 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo200, i32 0, i32 6
  %209 = load i32, i32* %blockChecksumFlag201, align 4, !tbaa !72
  %tobool202 = icmp ne i32 %209, 0
  br i1 %tobool202, label %if.then203, label %if.end206

if.then203:                                       ; preds = %cond.end198
  %210 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %blockChecksum204 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %210, i32 0, i32 16
  %211 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %212 = load i32, i32* %sizeToCopy192, align 4, !tbaa !3
  %call205 = call i32 @LZ4_XXH32_update(%struct.XXH32_state_s* %blockChecksum204, i8* %211, i32 %212)
  br label %if.end206

if.end206:                                        ; preds = %if.then203, %cond.end198
  %213 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo207 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %213, i32 0, i32 0
  %contentChecksumFlag208 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo207, i32 0, i32 2
  %214 = load i32, i32* %contentChecksumFlag208, align 8, !tbaa !65
  %tobool209 = icmp ne i32 %214, 0
  br i1 %tobool209, label %if.then210, label %if.end213

if.then210:                                       ; preds = %if.end206
  %215 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %xxh211 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %215, i32 0, i32 15
  %216 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %217 = load i32, i32* %sizeToCopy192, align 4, !tbaa !3
  %call212 = call i32 @LZ4_XXH32_update(%struct.XXH32_state_s* %xxh211, i8* %216, i32 %217)
  br label %if.end213

if.end213:                                        ; preds = %if.then210, %if.end206
  %218 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo214 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %218, i32 0, i32 0
  %contentSize = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo214, i32 0, i32 4
  %219 = load i64, i64* %contentSize, align 8, !tbaa !73
  %tobool215 = icmp ne i64 %219, 0
  br i1 %tobool215, label %if.then216, label %if.end218

if.then216:                                       ; preds = %if.end213
  %220 = load i32, i32* %sizeToCopy192, align 4, !tbaa !3
  %conv = zext i32 %220 to i64
  %221 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameRemainingSize = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %221, i32 0, i32 3
  %222 = load i64, i64* %frameRemainingSize, align 8, !tbaa !74
  %sub217 = sub i64 %222, %conv
  store i64 %sub217, i64* %frameRemainingSize, align 8, !tbaa !74
  br label %if.end218

if.end218:                                        ; preds = %if.then216, %if.end213
  %223 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo219 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %223, i32 0, i32 0
  %blockMode220 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo219, i32 0, i32 1
  %224 = load i32, i32* %blockMode220, align 4, !tbaa !67
  %cmp221 = icmp eq i32 %224, 0
  br i1 %cmp221, label %if.then223, label %if.end224

if.then223:                                       ; preds = %if.end218
  %225 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %226 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %227 = load i32, i32* %sizeToCopy192, align 4, !tbaa !3
  %228 = load i8*, i8** %dstStart, align 4, !tbaa !7
  call void @LZ4F_updateDict(%struct.LZ4F_dctx_s* %225, i8* %226, i32 %227, i8* %228, i32 0)
  br label %if.end224

if.end224:                                        ; preds = %if.then223, %if.end218
  %229 = load i32, i32* %sizeToCopy192, align 4, !tbaa !3
  %230 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %add.ptr225 = getelementptr inbounds i8, i8* %230, i32 %229
  store i8* %add.ptr225, i8** %srcPtr, align 4, !tbaa !7
  %231 = load i32, i32* %sizeToCopy192, align 4, !tbaa !3
  %232 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %add.ptr226 = getelementptr inbounds i8, i8* %232, i32 %231
  store i8* %add.ptr226, i8** %dstPtr, align 4, !tbaa !7
  %233 = load i32, i32* %sizeToCopy192, align 4, !tbaa !3
  %234 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget227 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %234, i32 0, i32 8
  %235 = load i32, i32* %tmpInTarget227, align 8, !tbaa !64
  %cmp228 = icmp eq i32 %233, %235
  br i1 %cmp228, label %if.then230, label %if.end240

if.then230:                                       ; preds = %if.end224
  %236 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo231 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %236, i32 0, i32 0
  %blockChecksumFlag232 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo231, i32 0, i32 6
  %237 = load i32, i32* %blockChecksumFlag232, align 4, !tbaa !72
  %tobool233 = icmp ne i32 %237, 0
  br i1 %tobool233, label %if.then234, label %if.else237

if.then234:                                       ; preds = %if.then230
  %238 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize235 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %238, i32 0, i32 7
  store i32 0, i32* %tmpInSize235, align 4, !tbaa !63
  %239 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage236 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %239, i32 0, i32 2
  store i32 6, i32* %dStage236, align 4, !tbaa !57
  br label %if.end239

if.else237:                                       ; preds = %if.then230
  %240 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage238 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %240, i32 0, i32 2
  store i32 3, i32* %dStage238, align 4, !tbaa !57
  br label %if.end239

if.end239:                                        ; preds = %if.else237, %if.then234
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup250

if.end240:                                        ; preds = %if.end224
  %241 = load i32, i32* %sizeToCopy192, align 4, !tbaa !3
  %242 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget241 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %242, i32 0, i32 8
  %243 = load i32, i32* %tmpInTarget241, align 8, !tbaa !64
  %sub242 = sub i32 %243, %241
  store i32 %sub242, i32* %tmpInTarget241, align 8, !tbaa !64
  %244 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget243 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %244, i32 0, i32 8
  %245 = load i32, i32* %tmpInTarget243, align 8, !tbaa !64
  %246 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo244 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %246, i32 0, i32 0
  %blockChecksumFlag245 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo244, i32 0, i32 6
  %247 = load i32, i32* %blockChecksumFlag245, align 4, !tbaa !72
  %tobool246 = icmp ne i32 %247, 0
  %248 = zext i1 %tobool246 to i64
  %cond247 = select i1 %tobool246, i32 4, i32 0
  %add248 = add i32 %245, %cond247
  %add249 = add i32 %add248, 4
  store i32 %add249, i32* %nextSrcSizeHint, align 4, !tbaa !3
  store i32 0, i32* %doAnotherStage, align 4, !tbaa !9
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup250

cleanup250:                                       ; preds = %if.end240, %if.end239
  %249 = bitcast i32* %sizeToCopy192 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %249) #4
  %250 = bitcast i32* %minBuffSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %250) #4
  br label %sw.epilog

sw.bb252:                                         ; preds = %while.body
  %251 = bitcast i8** %crcSrc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %251) #4
  %252 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %253 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast253 = ptrtoint i8* %252 to i32
  %sub.ptr.rhs.cast254 = ptrtoint i8* %253 to i32
  %sub.ptr.sub255 = sub i32 %sub.ptr.lhs.cast253, %sub.ptr.rhs.cast254
  %cmp256 = icmp sge i32 %sub.ptr.sub255, 4
  br i1 %cmp256, label %land.lhs.true, label %if.else263

land.lhs.true:                                    ; preds = %sw.bb252
  %254 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize258 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %254, i32 0, i32 7
  %255 = load i32, i32* %tmpInSize258, align 4, !tbaa !63
  %cmp259 = icmp eq i32 %255, 0
  br i1 %cmp259, label %if.then261, label %if.else263

if.then261:                                       ; preds = %land.lhs.true
  %256 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  store i8* %256, i8** %crcSrc, align 4, !tbaa !7
  %257 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %add.ptr262 = getelementptr inbounds i8, i8* %257, i32 4
  store i8* %add.ptr262, i8** %srcPtr, align 4, !tbaa !7
  br label %if.end297

if.else263:                                       ; preds = %land.lhs.true, %sw.bb252
  %258 = bitcast i32* %stillToCopy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %258) #4
  %259 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize264 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %259, i32 0, i32 7
  %260 = load i32, i32* %tmpInSize264, align 4, !tbaa !63
  %sub265 = sub i32 4, %260
  store i32 %sub265, i32* %stillToCopy, align 4, !tbaa !3
  %261 = bitcast i32* %sizeToCopy266 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %261) #4
  %262 = load i32, i32* %stillToCopy, align 4, !tbaa !3
  %263 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %264 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast267 = ptrtoint i8* %263 to i32
  %sub.ptr.rhs.cast268 = ptrtoint i8* %264 to i32
  %sub.ptr.sub269 = sub i32 %sub.ptr.lhs.cast267, %sub.ptr.rhs.cast268
  %cmp270 = icmp ult i32 %262, %sub.ptr.sub269
  br i1 %cmp270, label %cond.true272, label %cond.false273

cond.true272:                                     ; preds = %if.else263
  %265 = load i32, i32* %stillToCopy, align 4, !tbaa !3
  br label %cond.end277

cond.false273:                                    ; preds = %if.else263
  %266 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %267 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast274 = ptrtoint i8* %266 to i32
  %sub.ptr.rhs.cast275 = ptrtoint i8* %267 to i32
  %sub.ptr.sub276 = sub i32 %sub.ptr.lhs.cast274, %sub.ptr.rhs.cast275
  br label %cond.end277

cond.end277:                                      ; preds = %cond.false273, %cond.true272
  %cond278 = phi i32 [ %265, %cond.true272 ], [ %sub.ptr.sub276, %cond.false273 ]
  store i32 %cond278, i32* %sizeToCopy266, align 4, !tbaa !3
  %268 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %header279 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %268, i32 0, i32 17
  %arraydecay280 = getelementptr inbounds [19 x i8], [19 x i8]* %header279, i32 0, i32 0
  %269 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize281 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %269, i32 0, i32 7
  %270 = load i32, i32* %tmpInSize281, align 4, !tbaa !63
  %add.ptr282 = getelementptr inbounds i8, i8* %arraydecay280, i32 %270
  %271 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %272 = load i32, i32* %sizeToCopy266, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr282, i8* align 1 %271, i32 %272, i1 false)
  %273 = load i32, i32* %sizeToCopy266, align 4, !tbaa !3
  %274 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize283 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %274, i32 0, i32 7
  %275 = load i32, i32* %tmpInSize283, align 4, !tbaa !63
  %add284 = add i32 %275, %273
  store i32 %add284, i32* %tmpInSize283, align 4, !tbaa !63
  %276 = load i32, i32* %sizeToCopy266, align 4, !tbaa !3
  %277 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %add.ptr285 = getelementptr inbounds i8, i8* %277, i32 %276
  store i8* %add.ptr285, i8** %srcPtr, align 4, !tbaa !7
  %278 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize286 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %278, i32 0, i32 7
  %279 = load i32, i32* %tmpInSize286, align 4, !tbaa !63
  %cmp287 = icmp ult i32 %279, 4
  br i1 %cmp287, label %if.then289, label %if.end290

if.then289:                                       ; preds = %cond.end277
  store i32 0, i32* %doAnotherStage, align 4, !tbaa !9
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup293

if.end290:                                        ; preds = %cond.end277
  %280 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %header291 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %280, i32 0, i32 17
  %arraydecay292 = getelementptr inbounds [19 x i8], [19 x i8]* %header291, i32 0, i32 0
  store i8* %arraydecay292, i8** %crcSrc, align 4, !tbaa !7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup293

cleanup293:                                       ; preds = %if.end290, %if.then289
  %281 = bitcast i32* %sizeToCopy266 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %281) #4
  %282 = bitcast i32* %stillToCopy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %282) #4
  %cleanup.dest295 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest295, label %cleanup310 [
    i32 0, label %cleanup.cont296
  ]

cleanup.cont296:                                  ; preds = %cleanup293
  br label %if.end297

if.end297:                                        ; preds = %cleanup.cont296, %if.then261
  %283 = bitcast i32* %readCRC to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %283) #4
  %284 = load i8*, i8** %crcSrc, align 4, !tbaa !7
  %call298 = call i32 @LZ4F_readLE32(i8* %284)
  store i32 %call298, i32* %readCRC, align 4, !tbaa !9
  %285 = bitcast i32* %calcCRC to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %285) #4
  %286 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %blockChecksum299 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %286, i32 0, i32 16
  %call300 = call i32 @LZ4_XXH32_digest(%struct.XXH32_state_s* %blockChecksum299)
  store i32 %call300, i32* %calcCRC, align 4, !tbaa !9
  %287 = load i32, i32* %readCRC, align 4, !tbaa !9
  %288 = load i32, i32* %calcCRC, align 4, !tbaa !9
  %cmp301 = icmp ne i32 %287, %288
  br i1 %cmp301, label %if.then303, label %if.end305

if.then303:                                       ; preds = %if.end297
  %call304 = call i32 @err0r(i32 7)
  store i32 %call304, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup306

if.end305:                                        ; preds = %if.end297
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup306

cleanup306:                                       ; preds = %if.end305, %if.then303
  %289 = bitcast i32* %calcCRC to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %289) #4
  %290 = bitcast i32* %readCRC to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %290) #4
  %cleanup.dest308 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest308, label %cleanup310 [
    i32 0, label %cleanup.cont309
  ]

cleanup.cont309:                                  ; preds = %cleanup306
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup310

cleanup310:                                       ; preds = %cleanup.cont309, %cleanup306, %cleanup293
  %291 = bitcast i8** %crcSrc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %291) #4
  %cleanup.dest311 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest311, label %cleanup824 [
    i32 0, label %cleanup.cont312
    i32 4, label %sw.epilog
  ]

cleanup.cont312:                                  ; preds = %cleanup310
  %292 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage313 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %292, i32 0, i32 2
  store i32 3, i32* %dStage313, align 4, !tbaa !57
  br label %sw.epilog

sw.bb314:                                         ; preds = %while.body
  %293 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %294 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast315 = ptrtoint i8* %293 to i32
  %sub.ptr.rhs.cast316 = ptrtoint i8* %294 to i32
  %sub.ptr.sub317 = sub i32 %sub.ptr.lhs.cast315, %sub.ptr.rhs.cast316
  %295 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget318 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %295, i32 0, i32 8
  %296 = load i32, i32* %tmpInTarget318, align 8, !tbaa !64
  %cmp319 = icmp ult i32 %sub.ptr.sub317, %296
  br i1 %cmp319, label %if.then321, label %if.end324

if.then321:                                       ; preds = %sw.bb314
  %297 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize322 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %297, i32 0, i32 7
  store i32 0, i32* %tmpInSize322, align 4, !tbaa !63
  %298 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage323 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %298, i32 0, i32 2
  store i32 8, i32* %dStage323, align 4, !tbaa !57
  br label %sw.epilog

if.end324:                                        ; preds = %sw.bb314
  %299 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  store i8* %299, i8** %selectedIn, align 4, !tbaa !7
  %300 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget325 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %300, i32 0, i32 8
  %301 = load i32, i32* %tmpInTarget325, align 8, !tbaa !64
  %302 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %add.ptr326 = getelementptr inbounds i8, i8* %302, i32 %301
  store i8* %add.ptr326, i8** %srcPtr, align 4, !tbaa !7
  br i1 false, label %if.then327, label %if.end370

if.then327:                                       ; preds = %if.end324
  br label %sw.bb328

sw.bb328:                                         ; preds = %while.body, %if.then327
  %303 = bitcast i32* %wantedData329 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %303) #4
  %304 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget330 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %304, i32 0, i32 8
  %305 = load i32, i32* %tmpInTarget330, align 8, !tbaa !64
  %306 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize331 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %306, i32 0, i32 7
  %307 = load i32, i32* %tmpInSize331, align 4, !tbaa !63
  %sub332 = sub i32 %305, %307
  store i32 %sub332, i32* %wantedData329, align 4, !tbaa !3
  %308 = bitcast i32* %inputLeft to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %308) #4
  %309 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %310 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast333 = ptrtoint i8* %309 to i32
  %sub.ptr.rhs.cast334 = ptrtoint i8* %310 to i32
  %sub.ptr.sub335 = sub i32 %sub.ptr.lhs.cast333, %sub.ptr.rhs.cast334
  store i32 %sub.ptr.sub335, i32* %inputLeft, align 4, !tbaa !3
  %311 = bitcast i32* %sizeToCopy336 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %311) #4
  %312 = load i32, i32* %wantedData329, align 4, !tbaa !3
  %313 = load i32, i32* %inputLeft, align 4, !tbaa !3
  %cmp337 = icmp ult i32 %312, %313
  br i1 %cmp337, label %cond.true339, label %cond.false340

cond.true339:                                     ; preds = %sw.bb328
  %314 = load i32, i32* %wantedData329, align 4, !tbaa !3
  br label %cond.end341

cond.false340:                                    ; preds = %sw.bb328
  %315 = load i32, i32* %inputLeft, align 4, !tbaa !3
  br label %cond.end341

cond.end341:                                      ; preds = %cond.false340, %cond.true339
  %cond342 = phi i32 [ %314, %cond.true339 ], [ %315, %cond.false340 ]
  store i32 %cond342, i32* %sizeToCopy336, align 4, !tbaa !3
  %316 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpIn343 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %316, i32 0, i32 6
  %317 = load i8*, i8** %tmpIn343, align 8, !tbaa !58
  %318 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize344 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %318, i32 0, i32 7
  %319 = load i32, i32* %tmpInSize344, align 4, !tbaa !63
  %add.ptr345 = getelementptr inbounds i8, i8* %317, i32 %319
  %320 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %321 = load i32, i32* %sizeToCopy336, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr345, i8* align 1 %320, i32 %321, i1 false)
  %322 = load i32, i32* %sizeToCopy336, align 4, !tbaa !3
  %323 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize346 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %323, i32 0, i32 7
  %324 = load i32, i32* %tmpInSize346, align 4, !tbaa !63
  %add347 = add i32 %324, %322
  store i32 %add347, i32* %tmpInSize346, align 4, !tbaa !63
  %325 = load i32, i32* %sizeToCopy336, align 4, !tbaa !3
  %326 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %add.ptr348 = getelementptr inbounds i8, i8* %326, i32 %325
  store i8* %add.ptr348, i8** %srcPtr, align 4, !tbaa !7
  %327 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize349 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %327, i32 0, i32 7
  %328 = load i32, i32* %tmpInSize349, align 4, !tbaa !63
  %329 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget350 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %329, i32 0, i32 8
  %330 = load i32, i32* %tmpInTarget350, align 8, !tbaa !64
  %cmp351 = icmp ult i32 %328, %330
  br i1 %cmp351, label %if.then353, label %if.end363

if.then353:                                       ; preds = %cond.end341
  %331 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget354 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %331, i32 0, i32 8
  %332 = load i32, i32* %tmpInTarget354, align 8, !tbaa !64
  %333 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize355 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %333, i32 0, i32 7
  %334 = load i32, i32* %tmpInSize355, align 4, !tbaa !63
  %sub356 = sub i32 %332, %334
  %335 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo357 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %335, i32 0, i32 0
  %blockChecksumFlag358 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo357, i32 0, i32 6
  %336 = load i32, i32* %blockChecksumFlag358, align 4, !tbaa !72
  %tobool359 = icmp ne i32 %336, 0
  %337 = zext i1 %tobool359 to i64
  %cond360 = select i1 %tobool359, i32 4, i32 0
  %add361 = add i32 %sub356, %cond360
  %add362 = add i32 %add361, 4
  store i32 %add362, i32* %nextSrcSizeHint, align 4, !tbaa !3
  store i32 0, i32* %doAnotherStage, align 4, !tbaa !9
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup365

if.end363:                                        ; preds = %cond.end341
  %338 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpIn364 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %338, i32 0, i32 6
  %339 = load i8*, i8** %tmpIn364, align 8, !tbaa !58
  store i8* %339, i8** %selectedIn, align 4, !tbaa !7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup365

cleanup365:                                       ; preds = %if.end363, %if.then353
  %340 = bitcast i32* %sizeToCopy336 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %340) #4
  %341 = bitcast i32* %inputLeft to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %341) #4
  %342 = bitcast i32* %wantedData329 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %342) #4
  %cleanup.dest368 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest368, label %unreachable [
    i32 0, label %cleanup.cont369
    i32 4, label %sw.epilog
  ]

cleanup.cont369:                                  ; preds = %cleanup365
  br label %if.end370

if.end370:                                        ; preds = %cleanup.cont369, %if.end324
  %343 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo371 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %343, i32 0, i32 0
  %blockChecksumFlag372 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo371, i32 0, i32 6
  %344 = load i32, i32* %blockChecksumFlag372, align 4, !tbaa !72
  %tobool373 = icmp ne i32 %344, 0
  br i1 %tobool373, label %if.then374, label %if.end391

if.then374:                                       ; preds = %if.end370
  %345 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget375 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %345, i32 0, i32 8
  %346 = load i32, i32* %tmpInTarget375, align 8, !tbaa !64
  %sub376 = sub i32 %346, 4
  store i32 %sub376, i32* %tmpInTarget375, align 8, !tbaa !64
  %347 = bitcast i32* %readBlockCrc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %347) #4
  %348 = load i8*, i8** %selectedIn, align 4, !tbaa !7
  %349 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget377 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %349, i32 0, i32 8
  %350 = load i32, i32* %tmpInTarget377, align 8, !tbaa !64
  %add.ptr378 = getelementptr inbounds i8, i8* %348, i32 %350
  %call379 = call i32 @LZ4F_readLE32(i8* %add.ptr378)
  store i32 %call379, i32* %readBlockCrc, align 4, !tbaa !9
  %351 = bitcast i32* %calcBlockCrc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %351) #4
  %352 = load i8*, i8** %selectedIn, align 4, !tbaa !7
  %353 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget380 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %353, i32 0, i32 8
  %354 = load i32, i32* %tmpInTarget380, align 8, !tbaa !64
  %call381 = call i32 @LZ4_XXH32(i8* %352, i32 %354, i32 0)
  store i32 %call381, i32* %calcBlockCrc, align 4, !tbaa !9
  %355 = load i32, i32* %readBlockCrc, align 4, !tbaa !9
  %356 = load i32, i32* %calcBlockCrc, align 4, !tbaa !9
  %cmp382 = icmp ne i32 %355, %356
  br i1 %cmp382, label %if.then384, label %if.end386

if.then384:                                       ; preds = %if.then374
  %call385 = call i32 @err0r(i32 7)
  store i32 %call385, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup387

if.end386:                                        ; preds = %if.then374
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup387

cleanup387:                                       ; preds = %if.end386, %if.then384
  %357 = bitcast i32* %calcBlockCrc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %357) #4
  %358 = bitcast i32* %readBlockCrc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %358) #4
  %cleanup.dest389 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest389, label %cleanup824 [
    i32 0, label %cleanup.cont390
  ]

cleanup.cont390:                                  ; preds = %cleanup387
  br label %if.end391

if.end391:                                        ; preds = %cleanup.cont390, %if.end370
  %359 = load i8*, i8** %dstEnd, align 4, !tbaa !7
  %360 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast392 = ptrtoint i8* %359 to i32
  %sub.ptr.rhs.cast393 = ptrtoint i8* %360 to i32
  %sub.ptr.sub394 = sub i32 %sub.ptr.lhs.cast392, %sub.ptr.rhs.cast393
  %361 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %maxBlockSize395 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %361, i32 0, i32 4
  %362 = load i32, i32* %maxBlockSize395, align 8, !tbaa !66
  %cmp396 = icmp uge i32 %sub.ptr.sub394, %362
  br i1 %cmp396, label %if.then398, label %if.end444

if.then398:                                       ; preds = %if.end391
  %363 = bitcast i8** %dict to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %363) #4
  %364 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict399 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %364, i32 0, i32 10
  %365 = load i8*, i8** %dict399, align 8, !tbaa !60
  store i8* %365, i8** %dict, align 4, !tbaa !7
  %366 = bitcast i32* %dictSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %366) #4
  %367 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize400 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %367, i32 0, i32 11
  %368 = load i32, i32* %dictSize400, align 4, !tbaa !61
  store i32 %368, i32* %dictSize, align 4, !tbaa !3
  %369 = bitcast i32* %decodedSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %369) #4
  %370 = load i8*, i8** %dict, align 4, !tbaa !7
  %tobool401 = icmp ne i8* %370, null
  br i1 %tobool401, label %land.lhs.true402, label %if.end408

land.lhs.true402:                                 ; preds = %if.then398
  %371 = load i32, i32* %dictSize, align 4, !tbaa !3
  %cmp403 = icmp ugt i32 %371, 1073741824
  br i1 %cmp403, label %if.then405, label %if.end408

if.then405:                                       ; preds = %land.lhs.true402
  %372 = load i32, i32* %dictSize, align 4, !tbaa !3
  %sub406 = sub i32 %372, 65536
  %373 = load i8*, i8** %dict, align 4, !tbaa !7
  %add.ptr407 = getelementptr inbounds i8, i8* %373, i32 %sub406
  store i8* %add.ptr407, i8** %dict, align 4, !tbaa !7
  store i32 65536, i32* %dictSize, align 4, !tbaa !3
  br label %if.end408

if.end408:                                        ; preds = %if.then405, %land.lhs.true402, %if.then398
  %374 = load i8*, i8** %selectedIn, align 4, !tbaa !7
  %375 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %376 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget409 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %376, i32 0, i32 8
  %377 = load i32, i32* %tmpInTarget409, align 8, !tbaa !64
  %378 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %maxBlockSize410 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %378, i32 0, i32 4
  %379 = load i32, i32* %maxBlockSize410, align 8, !tbaa !66
  %380 = load i8*, i8** %dict, align 4, !tbaa !7
  %381 = load i32, i32* %dictSize, align 4, !tbaa !3
  %call411 = call i32 @LZ4_decompress_safe_usingDict(i8* %374, i8* %375, i32 %377, i32 %379, i8* %380, i32 %381)
  store i32 %call411, i32* %decodedSize, align 4, !tbaa !9
  %382 = load i32, i32* %decodedSize, align 4, !tbaa !9
  %cmp412 = icmp slt i32 %382, 0
  br i1 %cmp412, label %if.then414, label %if.end416

if.then414:                                       ; preds = %if.end408
  %call415 = call i32 @err0r(i32 1)
  store i32 %call415, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup440

if.end416:                                        ; preds = %if.end408
  %383 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo417 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %383, i32 0, i32 0
  %contentChecksumFlag418 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo417, i32 0, i32 2
  %384 = load i32, i32* %contentChecksumFlag418, align 8, !tbaa !65
  %tobool419 = icmp ne i32 %384, 0
  br i1 %tobool419, label %if.then420, label %if.end423

if.then420:                                       ; preds = %if.end416
  %385 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %xxh421 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %385, i32 0, i32 15
  %386 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %387 = load i32, i32* %decodedSize, align 4, !tbaa !9
  %call422 = call i32 @LZ4_XXH32_update(%struct.XXH32_state_s* %xxh421, i8* %386, i32 %387)
  br label %if.end423

if.end423:                                        ; preds = %if.then420, %if.end416
  %388 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo424 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %388, i32 0, i32 0
  %contentSize425 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo424, i32 0, i32 4
  %389 = load i64, i64* %contentSize425, align 8, !tbaa !73
  %tobool426 = icmp ne i64 %389, 0
  br i1 %tobool426, label %if.then427, label %if.end431

if.then427:                                       ; preds = %if.end423
  %390 = load i32, i32* %decodedSize, align 4, !tbaa !9
  %conv428 = zext i32 %390 to i64
  %391 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameRemainingSize429 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %391, i32 0, i32 3
  %392 = load i64, i64* %frameRemainingSize429, align 8, !tbaa !74
  %sub430 = sub i64 %392, %conv428
  store i64 %sub430, i64* %frameRemainingSize429, align 8, !tbaa !74
  br label %if.end431

if.end431:                                        ; preds = %if.then427, %if.end423
  %393 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo432 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %393, i32 0, i32 0
  %blockMode433 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo432, i32 0, i32 1
  %394 = load i32, i32* %blockMode433, align 4, !tbaa !67
  %cmp434 = icmp eq i32 %394, 0
  br i1 %cmp434, label %if.then436, label %if.end437

if.then436:                                       ; preds = %if.end431
  %395 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %396 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %397 = load i32, i32* %decodedSize, align 4, !tbaa !9
  %398 = load i8*, i8** %dstStart, align 4, !tbaa !7
  call void @LZ4F_updateDict(%struct.LZ4F_dctx_s* %395, i8* %396, i32 %397, i8* %398, i32 0)
  br label %if.end437

if.end437:                                        ; preds = %if.then436, %if.end431
  %399 = load i32, i32* %decodedSize, align 4, !tbaa !9
  %400 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %add.ptr438 = getelementptr inbounds i8, i8* %400, i32 %399
  store i8* %add.ptr438, i8** %dstPtr, align 4, !tbaa !7
  %401 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage439 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %401, i32 0, i32 2
  store i32 3, i32* %dStage439, align 4, !tbaa !57
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup440

cleanup440:                                       ; preds = %if.end437, %if.then414
  %402 = bitcast i32* %decodedSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %402) #4
  %403 = bitcast i32* %dictSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %403) #4
  %404 = bitcast i8** %dict to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %404) #4
  %cleanup.dest443 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest443, label %cleanup824 [
    i32 4, label %sw.epilog
  ]

if.end444:                                        ; preds = %if.end391
  %405 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo445 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %405, i32 0, i32 0
  %blockMode446 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo445, i32 0, i32 1
  %406 = load i32, i32* %blockMode446, align 4, !tbaa !67
  %cmp447 = icmp eq i32 %406, 0
  br i1 %cmp447, label %if.then449, label %if.end483

if.then449:                                       ; preds = %if.end444
  %407 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict450 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %407, i32 0, i32 10
  %408 = load i8*, i8** %dict450, align 8, !tbaa !60
  %409 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer451 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %409, i32 0, i32 9
  %410 = load i8*, i8** %tmpOutBuffer451, align 4, !tbaa !59
  %cmp452 = icmp eq i8* %408, %410
  br i1 %cmp452, label %if.then454, label %if.else470

if.then454:                                       ; preds = %if.then449
  %411 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize455 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %411, i32 0, i32 11
  %412 = load i32, i32* %dictSize455, align 4, !tbaa !61
  %cmp456 = icmp ugt i32 %412, 131072
  br i1 %cmp456, label %if.then458, label %if.end465

if.then458:                                       ; preds = %if.then454
  %413 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer459 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %413, i32 0, i32 9
  %414 = load i8*, i8** %tmpOutBuffer459, align 4, !tbaa !59
  %415 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict460 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %415, i32 0, i32 10
  %416 = load i8*, i8** %dict460, align 8, !tbaa !60
  %417 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize461 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %417, i32 0, i32 11
  %418 = load i32, i32* %dictSize461, align 4, !tbaa !61
  %add.ptr462 = getelementptr inbounds i8, i8* %416, i32 %418
  %add.ptr463 = getelementptr inbounds i8, i8* %add.ptr462, i32 -65536
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %414, i8* align 1 %add.ptr463, i32 65536, i1 false)
  %419 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize464 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %419, i32 0, i32 11
  store i32 65536, i32* %dictSize464, align 4, !tbaa !61
  br label %if.end465

if.end465:                                        ; preds = %if.then458, %if.then454
  %420 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer466 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %420, i32 0, i32 9
  %421 = load i8*, i8** %tmpOutBuffer466, align 4, !tbaa !59
  %422 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize467 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %422, i32 0, i32 11
  %423 = load i32, i32* %dictSize467, align 4, !tbaa !61
  %add.ptr468 = getelementptr inbounds i8, i8* %421, i32 %423
  %424 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOut469 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %424, i32 0, i32 12
  store i8* %add.ptr468, i8** %tmpOut469, align 8, !tbaa !69
  br label %if.end482

if.else470:                                       ; preds = %if.then449
  %425 = bitcast i32* %reservedDictSpace to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %425) #4
  %426 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize471 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %426, i32 0, i32 11
  %427 = load i32, i32* %dictSize471, align 4, !tbaa !61
  %cmp472 = icmp ult i32 %427, 65536
  br i1 %cmp472, label %cond.true474, label %cond.false476

cond.true474:                                     ; preds = %if.else470
  %428 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize475 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %428, i32 0, i32 11
  %429 = load i32, i32* %dictSize475, align 4, !tbaa !61
  br label %cond.end477

cond.false476:                                    ; preds = %if.else470
  br label %cond.end477

cond.end477:                                      ; preds = %cond.false476, %cond.true474
  %cond478 = phi i32 [ %429, %cond.true474 ], [ 65536, %cond.false476 ]
  store i32 %cond478, i32* %reservedDictSpace, align 4, !tbaa !3
  %430 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer479 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %430, i32 0, i32 9
  %431 = load i8*, i8** %tmpOutBuffer479, align 4, !tbaa !59
  %432 = load i32, i32* %reservedDictSpace, align 4, !tbaa !3
  %add.ptr480 = getelementptr inbounds i8, i8* %431, i32 %432
  %433 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOut481 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %433, i32 0, i32 12
  store i8* %add.ptr480, i8** %tmpOut481, align 8, !tbaa !69
  %434 = bitcast i32* %reservedDictSpace to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %434) #4
  br label %if.end482

if.end482:                                        ; preds = %cond.end477, %if.end465
  br label %if.end483

if.end483:                                        ; preds = %if.end482, %if.end444
  %435 = bitcast i8** %dict484 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %435) #4
  %436 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict485 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %436, i32 0, i32 10
  %437 = load i8*, i8** %dict485, align 8, !tbaa !60
  store i8* %437, i8** %dict484, align 4, !tbaa !7
  %438 = bitcast i32* %dictSize486 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %438) #4
  %439 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize487 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %439, i32 0, i32 11
  %440 = load i32, i32* %dictSize487, align 4, !tbaa !61
  store i32 %440, i32* %dictSize486, align 4, !tbaa !3
  %441 = bitcast i32* %decodedSize488 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %441) #4
  %442 = load i8*, i8** %dict484, align 4, !tbaa !7
  %tobool489 = icmp ne i8* %442, null
  br i1 %tobool489, label %land.lhs.true490, label %if.end496

land.lhs.true490:                                 ; preds = %if.end483
  %443 = load i32, i32* %dictSize486, align 4, !tbaa !3
  %cmp491 = icmp ugt i32 %443, 1073741824
  br i1 %cmp491, label %if.then493, label %if.end496

if.then493:                                       ; preds = %land.lhs.true490
  %444 = load i32, i32* %dictSize486, align 4, !tbaa !3
  %sub494 = sub i32 %444, 65536
  %445 = load i8*, i8** %dict484, align 4, !tbaa !7
  %add.ptr495 = getelementptr inbounds i8, i8* %445, i32 %sub494
  store i8* %add.ptr495, i8** %dict484, align 4, !tbaa !7
  store i32 65536, i32* %dictSize486, align 4, !tbaa !3
  br label %if.end496

if.end496:                                        ; preds = %if.then493, %land.lhs.true490, %if.end483
  %446 = load i8*, i8** %selectedIn, align 4, !tbaa !7
  %447 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOut497 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %447, i32 0, i32 12
  %448 = load i8*, i8** %tmpOut497, align 8, !tbaa !69
  %449 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget498 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %449, i32 0, i32 8
  %450 = load i32, i32* %tmpInTarget498, align 8, !tbaa !64
  %451 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %maxBlockSize499 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %451, i32 0, i32 4
  %452 = load i32, i32* %maxBlockSize499, align 8, !tbaa !66
  %453 = load i8*, i8** %dict484, align 4, !tbaa !7
  %454 = load i32, i32* %dictSize486, align 4, !tbaa !3
  %call500 = call i32 @LZ4_decompress_safe_usingDict(i8* %446, i8* %448, i32 %450, i32 %452, i8* %453, i32 %454)
  store i32 %call500, i32* %decodedSize488, align 4, !tbaa !9
  %455 = load i32, i32* %decodedSize488, align 4, !tbaa !9
  %cmp501 = icmp slt i32 %455, 0
  br i1 %cmp501, label %if.then503, label %if.end505

if.then503:                                       ; preds = %if.end496
  %call504 = call i32 @err0r(i32 16)
  store i32 %call504, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup525

if.end505:                                        ; preds = %if.end496
  %456 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo506 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %456, i32 0, i32 0
  %contentChecksumFlag507 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo506, i32 0, i32 2
  %457 = load i32, i32* %contentChecksumFlag507, align 8, !tbaa !65
  %tobool508 = icmp ne i32 %457, 0
  br i1 %tobool508, label %if.then509, label %if.end513

if.then509:                                       ; preds = %if.end505
  %458 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %xxh510 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %458, i32 0, i32 15
  %459 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOut511 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %459, i32 0, i32 12
  %460 = load i8*, i8** %tmpOut511, align 8, !tbaa !69
  %461 = load i32, i32* %decodedSize488, align 4, !tbaa !9
  %call512 = call i32 @LZ4_XXH32_update(%struct.XXH32_state_s* %xxh510, i8* %460, i32 %461)
  br label %if.end513

if.end513:                                        ; preds = %if.then509, %if.end505
  %462 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo514 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %462, i32 0, i32 0
  %contentSize515 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo514, i32 0, i32 4
  %463 = load i64, i64* %contentSize515, align 8, !tbaa !73
  %tobool516 = icmp ne i64 %463, 0
  br i1 %tobool516, label %if.then517, label %if.end521

if.then517:                                       ; preds = %if.end513
  %464 = load i32, i32* %decodedSize488, align 4, !tbaa !9
  %conv518 = zext i32 %464 to i64
  %465 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameRemainingSize519 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %465, i32 0, i32 3
  %466 = load i64, i64* %frameRemainingSize519, align 8, !tbaa !74
  %sub520 = sub i64 %466, %conv518
  store i64 %sub520, i64* %frameRemainingSize519, align 8, !tbaa !74
  br label %if.end521

if.end521:                                        ; preds = %if.then517, %if.end513
  %467 = load i32, i32* %decodedSize488, align 4, !tbaa !9
  %468 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutSize522 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %468, i32 0, i32 13
  store i32 %467, i32* %tmpOutSize522, align 4, !tbaa !71
  %469 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutStart523 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %469, i32 0, i32 14
  store i32 0, i32* %tmpOutStart523, align 8, !tbaa !70
  %470 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage524 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %470, i32 0, i32 2
  store i32 9, i32* %dStage524, align 4, !tbaa !57
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup525

cleanup525:                                       ; preds = %if.end521, %if.then503
  %471 = bitcast i32* %decodedSize488 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %471) #4
  %472 = bitcast i32* %dictSize486 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %472) #4
  %473 = bitcast i8** %dict484 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %473) #4
  %cleanup.dest528 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest528, label %cleanup824 [
    i32 0, label %cleanup.cont529
  ]

cleanup.cont529:                                  ; preds = %cleanup525
  br label %sw.bb530

sw.bb530:                                         ; preds = %while.body, %cleanup.cont529
  %474 = bitcast i32* %sizeToCopy531 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %474) #4
  %475 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutSize532 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %475, i32 0, i32 13
  %476 = load i32, i32* %tmpOutSize532, align 4, !tbaa !71
  %477 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutStart533 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %477, i32 0, i32 14
  %478 = load i32, i32* %tmpOutStart533, align 8, !tbaa !70
  %sub534 = sub i32 %476, %478
  %479 = load i8*, i8** %dstEnd, align 4, !tbaa !7
  %480 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast535 = ptrtoint i8* %479 to i32
  %sub.ptr.rhs.cast536 = ptrtoint i8* %480 to i32
  %sub.ptr.sub537 = sub i32 %sub.ptr.lhs.cast535, %sub.ptr.rhs.cast536
  %cmp538 = icmp ult i32 %sub534, %sub.ptr.sub537
  br i1 %cmp538, label %cond.true540, label %cond.false544

cond.true540:                                     ; preds = %sw.bb530
  %481 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutSize541 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %481, i32 0, i32 13
  %482 = load i32, i32* %tmpOutSize541, align 4, !tbaa !71
  %483 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutStart542 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %483, i32 0, i32 14
  %484 = load i32, i32* %tmpOutStart542, align 8, !tbaa !70
  %sub543 = sub i32 %482, %484
  br label %cond.end548

cond.false544:                                    ; preds = %sw.bb530
  %485 = load i8*, i8** %dstEnd, align 4, !tbaa !7
  %486 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast545 = ptrtoint i8* %485 to i32
  %sub.ptr.rhs.cast546 = ptrtoint i8* %486 to i32
  %sub.ptr.sub547 = sub i32 %sub.ptr.lhs.cast545, %sub.ptr.rhs.cast546
  br label %cond.end548

cond.end548:                                      ; preds = %cond.false544, %cond.true540
  %cond549 = phi i32 [ %sub543, %cond.true540 ], [ %sub.ptr.sub547, %cond.false544 ]
  store i32 %cond549, i32* %sizeToCopy531, align 4, !tbaa !3
  %487 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %488 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOut550 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %488, i32 0, i32 12
  %489 = load i8*, i8** %tmpOut550, align 8, !tbaa !69
  %490 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutStart551 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %490, i32 0, i32 14
  %491 = load i32, i32* %tmpOutStart551, align 8, !tbaa !70
  %add.ptr552 = getelementptr inbounds i8, i8* %489, i32 %491
  %492 = load i32, i32* %sizeToCopy531, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %487, i8* align 1 %add.ptr552, i32 %492, i1 false)
  %493 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo553 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %493, i32 0, i32 0
  %blockMode554 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo553, i32 0, i32 1
  %494 = load i32, i32* %blockMode554, align 4, !tbaa !67
  %cmp555 = icmp eq i32 %494, 0
  br i1 %cmp555, label %if.then557, label %if.end558

if.then557:                                       ; preds = %cond.end548
  %495 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %496 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %497 = load i32, i32* %sizeToCopy531, align 4, !tbaa !3
  %498 = load i8*, i8** %dstStart, align 4, !tbaa !7
  call void @LZ4F_updateDict(%struct.LZ4F_dctx_s* %495, i8* %496, i32 %497, i8* %498, i32 1)
  br label %if.end558

if.end558:                                        ; preds = %if.then557, %cond.end548
  %499 = load i32, i32* %sizeToCopy531, align 4, !tbaa !3
  %500 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutStart559 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %500, i32 0, i32 14
  %501 = load i32, i32* %tmpOutStart559, align 8, !tbaa !70
  %add560 = add i32 %501, %499
  store i32 %add560, i32* %tmpOutStart559, align 8, !tbaa !70
  %502 = load i32, i32* %sizeToCopy531, align 4, !tbaa !3
  %503 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %add.ptr561 = getelementptr inbounds i8, i8* %503, i32 %502
  store i8* %add.ptr561, i8** %dstPtr, align 4, !tbaa !7
  %504 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutStart562 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %504, i32 0, i32 14
  %505 = load i32, i32* %tmpOutStart562, align 8, !tbaa !70
  %506 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutSize563 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %506, i32 0, i32 13
  %507 = load i32, i32* %tmpOutSize563, align 4, !tbaa !71
  %cmp564 = icmp eq i32 %505, %507
  br i1 %cmp564, label %if.then566, label %if.end568

if.then566:                                       ; preds = %if.end558
  %508 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage567 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %508, i32 0, i32 2
  store i32 3, i32* %dStage567, align 4, !tbaa !57
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup569

if.end568:                                        ; preds = %if.end558
  store i32 0, i32* %doAnotherStage, align 4, !tbaa !9
  store i32 4, i32* %nextSrcSizeHint, align 4, !tbaa !3
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup569

cleanup569:                                       ; preds = %if.end568, %if.then566
  %509 = bitcast i32* %sizeToCopy531 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %509) #4
  br label %sw.epilog

sw.bb570:                                         ; preds = %while.body
  %510 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameRemainingSize571 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %510, i32 0, i32 3
  %511 = load i64, i64* %frameRemainingSize571, align 8, !tbaa !74
  %tobool572 = icmp ne i64 %511, 0
  br i1 %tobool572, label %if.then573, label %if.end575

if.then573:                                       ; preds = %sw.bb570
  %call574 = call i32 @err0r(i32 14)
  store i32 %call574, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup824

if.end575:                                        ; preds = %sw.bb570
  %512 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo576 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %512, i32 0, i32 0
  %contentChecksumFlag577 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo576, i32 0, i32 2
  %513 = load i32, i32* %contentChecksumFlag577, align 8, !tbaa !65
  %tobool578 = icmp ne i32 %513, 0
  br i1 %tobool578, label %if.end580, label %if.then579

if.then579:                                       ; preds = %if.end575
  store i32 0, i32* %nextSrcSizeHint, align 4, !tbaa !3
  %514 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  call void @LZ4F_resetDecompressionContext(%struct.LZ4F_dctx_s* %514)
  store i32 0, i32* %doAnotherStage, align 4, !tbaa !9
  br label %sw.epilog

if.end580:                                        ; preds = %if.end575
  %515 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %516 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast581 = ptrtoint i8* %515 to i32
  %sub.ptr.rhs.cast582 = ptrtoint i8* %516 to i32
  %sub.ptr.sub583 = sub i32 %sub.ptr.lhs.cast581, %sub.ptr.rhs.cast582
  %cmp584 = icmp slt i32 %sub.ptr.sub583, 4
  br i1 %cmp584, label %if.then586, label %if.else589

if.then586:                                       ; preds = %if.end580
  %517 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize587 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %517, i32 0, i32 7
  store i32 0, i32* %tmpInSize587, align 4, !tbaa !63
  %518 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage588 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %518, i32 0, i32 2
  store i32 11, i32* %dStage588, align 4, !tbaa !57
  br label %if.end591

if.else589:                                       ; preds = %if.end580
  %519 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  store i8* %519, i8** %selectedIn, align 4, !tbaa !7
  %520 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %add.ptr590 = getelementptr inbounds i8, i8* %520, i32 4
  store i8* %add.ptr590, i8** %srcPtr, align 4, !tbaa !7
  br label %if.end591

if.end591:                                        ; preds = %if.else589, %if.then586
  %521 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage592 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %521, i32 0, i32 2
  %522 = load i32, i32* %dStage592, align 4, !tbaa !57
  %cmp593 = icmp eq i32 %522, 11
  br i1 %cmp593, label %if.then595, label %if.end630

if.then595:                                       ; preds = %if.end591
  br label %sw.bb596

sw.bb596:                                         ; preds = %while.body, %if.then595
  %523 = bitcast i32* %remainingInput597 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %523) #4
  %524 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %525 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast598 = ptrtoint i8* %524 to i32
  %sub.ptr.rhs.cast599 = ptrtoint i8* %525 to i32
  %sub.ptr.sub600 = sub i32 %sub.ptr.lhs.cast598, %sub.ptr.rhs.cast599
  store i32 %sub.ptr.sub600, i32* %remainingInput597, align 4, !tbaa !3
  %526 = bitcast i32* %wantedData601 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %526) #4
  %527 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize602 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %527, i32 0, i32 7
  %528 = load i32, i32* %tmpInSize602, align 4, !tbaa !63
  %sub603 = sub i32 4, %528
  store i32 %sub603, i32* %wantedData601, align 4, !tbaa !3
  %529 = bitcast i32* %sizeToCopy604 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %529) #4
  %530 = load i32, i32* %wantedData601, align 4, !tbaa !3
  %531 = load i32, i32* %remainingInput597, align 4, !tbaa !3
  %cmp605 = icmp ult i32 %530, %531
  br i1 %cmp605, label %cond.true607, label %cond.false608

cond.true607:                                     ; preds = %sw.bb596
  %532 = load i32, i32* %wantedData601, align 4, !tbaa !3
  br label %cond.end609

cond.false608:                                    ; preds = %sw.bb596
  %533 = load i32, i32* %remainingInput597, align 4, !tbaa !3
  br label %cond.end609

cond.end609:                                      ; preds = %cond.false608, %cond.true607
  %cond610 = phi i32 [ %532, %cond.true607 ], [ %533, %cond.false608 ]
  store i32 %cond610, i32* %sizeToCopy604, align 4, !tbaa !3
  %534 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpIn611 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %534, i32 0, i32 6
  %535 = load i8*, i8** %tmpIn611, align 8, !tbaa !58
  %536 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize612 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %536, i32 0, i32 7
  %537 = load i32, i32* %tmpInSize612, align 4, !tbaa !63
  %add.ptr613 = getelementptr inbounds i8, i8* %535, i32 %537
  %538 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %539 = load i32, i32* %sizeToCopy604, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr613, i8* align 1 %538, i32 %539, i1 false)
  %540 = load i32, i32* %sizeToCopy604, align 4, !tbaa !3
  %541 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %add.ptr614 = getelementptr inbounds i8, i8* %541, i32 %540
  store i8* %add.ptr614, i8** %srcPtr, align 4, !tbaa !7
  %542 = load i32, i32* %sizeToCopy604, align 4, !tbaa !3
  %543 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize615 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %543, i32 0, i32 7
  %544 = load i32, i32* %tmpInSize615, align 4, !tbaa !63
  %add616 = add i32 %544, %542
  store i32 %add616, i32* %tmpInSize615, align 4, !tbaa !63
  %545 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize617 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %545, i32 0, i32 7
  %546 = load i32, i32* %tmpInSize617, align 4, !tbaa !63
  %cmp618 = icmp ult i32 %546, 4
  br i1 %cmp618, label %if.then620, label %if.end623

if.then620:                                       ; preds = %cond.end609
  %547 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize621 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %547, i32 0, i32 7
  %548 = load i32, i32* %tmpInSize621, align 4, !tbaa !63
  %sub622 = sub i32 4, %548
  store i32 %sub622, i32* %nextSrcSizeHint, align 4, !tbaa !3
  store i32 0, i32* %doAnotherStage, align 4, !tbaa !9
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup625

if.end623:                                        ; preds = %cond.end609
  %549 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpIn624 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %549, i32 0, i32 6
  %550 = load i8*, i8** %tmpIn624, align 8, !tbaa !58
  store i8* %550, i8** %selectedIn, align 4, !tbaa !7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup625

cleanup625:                                       ; preds = %if.end623, %if.then620
  %551 = bitcast i32* %sizeToCopy604 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %551) #4
  %552 = bitcast i32* %wantedData601 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %552) #4
  %553 = bitcast i32* %remainingInput597 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %553) #4
  %cleanup.dest628 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest628, label %unreachable [
    i32 0, label %cleanup.cont629
    i32 4, label %sw.epilog
  ]

cleanup.cont629:                                  ; preds = %cleanup625
  br label %if.end630

if.end630:                                        ; preds = %cleanup.cont629, %if.end591
  %554 = bitcast i32* %readCRC631 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %554) #4
  %555 = load i8*, i8** %selectedIn, align 4, !tbaa !7
  %call632 = call i32 @LZ4F_readLE32(i8* %555)
  store i32 %call632, i32* %readCRC631, align 4, !tbaa !9
  %556 = bitcast i32* %resultCRC to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %556) #4
  %557 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %xxh633 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %557, i32 0, i32 15
  %call634 = call i32 @LZ4_XXH32_digest(%struct.XXH32_state_s* %xxh633)
  store i32 %call634, i32* %resultCRC, align 4, !tbaa !9
  %558 = load i32, i32* %readCRC631, align 4, !tbaa !9
  %559 = load i32, i32* %resultCRC, align 4, !tbaa !9
  %cmp635 = icmp ne i32 %558, %559
  br i1 %cmp635, label %if.then637, label %if.end639

if.then637:                                       ; preds = %if.end630
  %call638 = call i32 @err0r(i32 18)
  store i32 %call638, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup640

if.end639:                                        ; preds = %if.end630
  store i32 0, i32* %nextSrcSizeHint, align 4, !tbaa !3
  %560 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  call void @LZ4F_resetDecompressionContext(%struct.LZ4F_dctx_s* %560)
  store i32 0, i32* %doAnotherStage, align 4, !tbaa !9
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup640

cleanup640:                                       ; preds = %if.end639, %if.then637
  %561 = bitcast i32* %resultCRC to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %561) #4
  %562 = bitcast i32* %readCRC631 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %562) #4
  %cleanup.dest642 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest642, label %cleanup824 [
    i32 4, label %sw.epilog
  ]

sw.bb643:                                         ; preds = %while.body
  %563 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %564 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast644 = ptrtoint i8* %563 to i32
  %sub.ptr.rhs.cast645 = ptrtoint i8* %564 to i32
  %sub.ptr.sub646 = sub i32 %sub.ptr.lhs.cast644, %sub.ptr.rhs.cast645
  %cmp647 = icmp sge i32 %sub.ptr.sub646, 4
  br i1 %cmp647, label %if.then649, label %if.else651

if.then649:                                       ; preds = %sw.bb643
  %565 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  store i8* %565, i8** %selectedIn, align 4, !tbaa !7
  %566 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %add.ptr650 = getelementptr inbounds i8, i8* %566, i32 4
  store i8* %add.ptr650, i8** %srcPtr, align 4, !tbaa !7
  br label %if.end655

if.else651:                                       ; preds = %sw.bb643
  %567 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize652 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %567, i32 0, i32 7
  store i32 4, i32* %tmpInSize652, align 4, !tbaa !63
  %568 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget653 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %568, i32 0, i32 8
  store i32 8, i32* %tmpInTarget653, align 8, !tbaa !64
  %569 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage654 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %569, i32 0, i32 2
  store i32 13, i32* %dStage654, align 4, !tbaa !57
  br label %if.end655

if.end655:                                        ; preds = %if.else651, %if.then649
  %570 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage656 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %570, i32 0, i32 2
  %571 = load i32, i32* %dStage656, align 4, !tbaa !57
  %cmp657 = icmp eq i32 %571, 13
  br i1 %cmp657, label %if.then659, label %if.end702

if.then659:                                       ; preds = %if.end655
  br label %sw.bb660

sw.bb660:                                         ; preds = %while.body, %if.then659
  %572 = bitcast i32* %sizeToCopy661 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %572) #4
  %573 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget662 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %573, i32 0, i32 8
  %574 = load i32, i32* %tmpInTarget662, align 8, !tbaa !64
  %575 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize663 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %575, i32 0, i32 7
  %576 = load i32, i32* %tmpInSize663, align 4, !tbaa !63
  %sub664 = sub i32 %574, %576
  %577 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %578 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast665 = ptrtoint i8* %577 to i32
  %sub.ptr.rhs.cast666 = ptrtoint i8* %578 to i32
  %sub.ptr.sub667 = sub i32 %sub.ptr.lhs.cast665, %sub.ptr.rhs.cast666
  %cmp668 = icmp ult i32 %sub664, %sub.ptr.sub667
  br i1 %cmp668, label %cond.true670, label %cond.false674

cond.true670:                                     ; preds = %sw.bb660
  %579 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget671 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %579, i32 0, i32 8
  %580 = load i32, i32* %tmpInTarget671, align 8, !tbaa !64
  %581 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize672 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %581, i32 0, i32 7
  %582 = load i32, i32* %tmpInSize672, align 4, !tbaa !63
  %sub673 = sub i32 %580, %582
  br label %cond.end678

cond.false674:                                    ; preds = %sw.bb660
  %583 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %584 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast675 = ptrtoint i8* %583 to i32
  %sub.ptr.rhs.cast676 = ptrtoint i8* %584 to i32
  %sub.ptr.sub677 = sub i32 %sub.ptr.lhs.cast675, %sub.ptr.rhs.cast676
  br label %cond.end678

cond.end678:                                      ; preds = %cond.false674, %cond.true670
  %cond679 = phi i32 [ %sub673, %cond.true670 ], [ %sub.ptr.sub677, %cond.false674 ]
  store i32 %cond679, i32* %sizeToCopy661, align 4, !tbaa !3
  %585 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %header680 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %585, i32 0, i32 17
  %arraydecay681 = getelementptr inbounds [19 x i8], [19 x i8]* %header680, i32 0, i32 0
  %586 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize682 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %586, i32 0, i32 7
  %587 = load i32, i32* %tmpInSize682, align 4, !tbaa !63
  %add.ptr683 = getelementptr inbounds i8, i8* %arraydecay681, i32 %587
  %588 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %589 = load i32, i32* %sizeToCopy661, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr683, i8* align 1 %588, i32 %589, i1 false)
  %590 = load i32, i32* %sizeToCopy661, align 4, !tbaa !3
  %591 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %add.ptr684 = getelementptr inbounds i8, i8* %591, i32 %590
  store i8* %add.ptr684, i8** %srcPtr, align 4, !tbaa !7
  %592 = load i32, i32* %sizeToCopy661, align 4, !tbaa !3
  %593 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize685 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %593, i32 0, i32 7
  %594 = load i32, i32* %tmpInSize685, align 4, !tbaa !63
  %add686 = add i32 %594, %592
  store i32 %add686, i32* %tmpInSize685, align 4, !tbaa !63
  %595 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize687 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %595, i32 0, i32 7
  %596 = load i32, i32* %tmpInSize687, align 4, !tbaa !63
  %597 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget688 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %597, i32 0, i32 8
  %598 = load i32, i32* %tmpInTarget688, align 8, !tbaa !64
  %cmp689 = icmp ult i32 %596, %598
  br i1 %cmp689, label %if.then691, label %if.end695

if.then691:                                       ; preds = %cond.end678
  %599 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget692 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %599, i32 0, i32 8
  %600 = load i32, i32* %tmpInTarget692, align 8, !tbaa !64
  %601 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize693 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %601, i32 0, i32 7
  %602 = load i32, i32* %tmpInSize693, align 4, !tbaa !63
  %sub694 = sub i32 %600, %602
  store i32 %sub694, i32* %nextSrcSizeHint, align 4, !tbaa !3
  store i32 0, i32* %doAnotherStage, align 4, !tbaa !9
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup699

if.end695:                                        ; preds = %cond.end678
  %603 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %header696 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %603, i32 0, i32 17
  %arraydecay697 = getelementptr inbounds [19 x i8], [19 x i8]* %header696, i32 0, i32 0
  %add.ptr698 = getelementptr inbounds i8, i8* %arraydecay697, i32 4
  store i8* %add.ptr698, i8** %selectedIn, align 4, !tbaa !7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup699

cleanup699:                                       ; preds = %if.end695, %if.then691
  %604 = bitcast i32* %sizeToCopy661 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %604) #4
  %cleanup.dest700 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest700, label %unreachable [
    i32 0, label %cleanup.cont701
    i32 4, label %sw.epilog
  ]

cleanup.cont701:                                  ; preds = %cleanup699
  br label %if.end702

if.end702:                                        ; preds = %cleanup.cont701, %if.end655
  %605 = bitcast i32* %SFrameSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %605) #4
  %606 = load i8*, i8** %selectedIn, align 4, !tbaa !7
  %call703 = call i32 @LZ4F_readLE32(i8* %606)
  store i32 %call703, i32* %SFrameSize, align 4, !tbaa !3
  %607 = load i32, i32* %SFrameSize, align 4, !tbaa !3
  %conv704 = zext i32 %607 to i64
  %608 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo705 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %608, i32 0, i32 0
  %contentSize706 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo705, i32 0, i32 4
  store i64 %conv704, i64* %contentSize706, align 8, !tbaa !73
  %609 = load i32, i32* %SFrameSize, align 4, !tbaa !3
  %610 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget707 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %610, i32 0, i32 8
  store i32 %609, i32* %tmpInTarget707, align 8, !tbaa !64
  %611 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage708 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %611, i32 0, i32 2
  store i32 14, i32* %dStage708, align 4, !tbaa !57
  store i32 4, i32* %cleanup.dest.slot, align 4
  %612 = bitcast i32* %SFrameSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %612) #4
  br label %sw.epilog

sw.bb710:                                         ; preds = %while.body
  %613 = bitcast i32* %skipSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %613) #4
  %614 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget711 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %614, i32 0, i32 8
  %615 = load i32, i32* %tmpInTarget711, align 8, !tbaa !64
  %616 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %617 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast712 = ptrtoint i8* %616 to i32
  %sub.ptr.rhs.cast713 = ptrtoint i8* %617 to i32
  %sub.ptr.sub714 = sub i32 %sub.ptr.lhs.cast712, %sub.ptr.rhs.cast713
  %cmp715 = icmp ult i32 %615, %sub.ptr.sub714
  br i1 %cmp715, label %cond.true717, label %cond.false719

cond.true717:                                     ; preds = %sw.bb710
  %618 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget718 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %618, i32 0, i32 8
  %619 = load i32, i32* %tmpInTarget718, align 8, !tbaa !64
  br label %cond.end723

cond.false719:                                    ; preds = %sw.bb710
  %620 = load i8*, i8** %srcEnd, align 4, !tbaa !7
  %621 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %sub.ptr.lhs.cast720 = ptrtoint i8* %620 to i32
  %sub.ptr.rhs.cast721 = ptrtoint i8* %621 to i32
  %sub.ptr.sub722 = sub i32 %sub.ptr.lhs.cast720, %sub.ptr.rhs.cast721
  br label %cond.end723

cond.end723:                                      ; preds = %cond.false719, %cond.true717
  %cond724 = phi i32 [ %619, %cond.true717 ], [ %sub.ptr.sub722, %cond.false719 ]
  store i32 %cond724, i32* %skipSize, align 4, !tbaa !3
  %622 = load i32, i32* %skipSize, align 4, !tbaa !3
  %623 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %add.ptr725 = getelementptr inbounds i8, i8* %623, i32 %622
  store i8* %add.ptr725, i8** %srcPtr, align 4, !tbaa !7
  %624 = load i32, i32* %skipSize, align 4, !tbaa !3
  %625 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget726 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %625, i32 0, i32 8
  %626 = load i32, i32* %tmpInTarget726, align 8, !tbaa !64
  %sub727 = sub i32 %626, %624
  store i32 %sub727, i32* %tmpInTarget726, align 8, !tbaa !64
  store i32 0, i32* %doAnotherStage, align 4, !tbaa !9
  %627 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget728 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %627, i32 0, i32 8
  %628 = load i32, i32* %tmpInTarget728, align 8, !tbaa !64
  store i32 %628, i32* %nextSrcSizeHint, align 4, !tbaa !3
  %629 = load i32, i32* %nextSrcSizeHint, align 4, !tbaa !3
  %tobool729 = icmp ne i32 %629, 0
  br i1 %tobool729, label %if.then730, label %if.end731

if.then730:                                       ; preds = %cond.end723
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup732

if.end731:                                        ; preds = %cond.end723
  %630 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  call void @LZ4F_resetDecompressionContext(%struct.LZ4F_dctx_s* %630)
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup732

cleanup732:                                       ; preds = %if.end731, %if.then730
  %631 = bitcast i32* %skipSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %631) #4
  br label %sw.epilog

sw.epilog:                                        ; preds = %while.body, %cleanup732, %if.end702, %cleanup699, %cleanup640, %cleanup625, %if.then579, %cleanup569, %cleanup440, %cleanup365, %if.then321, %cleanup.cont312, %cleanup310, %cleanup250, %cleanup171, %cleanup132, %cleanup.cont, %if.then40, %cleanup
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %632 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo733 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %632, i32 0, i32 0
  %blockMode734 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo733, i32 0, i32 1
  %633 = load i32, i32* %blockMode734, align 4, !tbaa !67
  %cmp735 = icmp eq i32 %633, 0
  br i1 %cmp735, label %land.lhs.true737, label %if.end817

land.lhs.true737:                                 ; preds = %while.end
  %634 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict738 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %634, i32 0, i32 10
  %635 = load i8*, i8** %dict738, align 8, !tbaa !60
  %636 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer739 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %636, i32 0, i32 9
  %637 = load i8*, i8** %tmpOutBuffer739, align 4, !tbaa !59
  %cmp740 = icmp ne i8* %635, %637
  br i1 %cmp740, label %land.lhs.true742, label %if.end817

land.lhs.true742:                                 ; preds = %land.lhs.true737
  %638 = load %struct.LZ4F_decompressOptions_t*, %struct.LZ4F_decompressOptions_t** %decompressOptionsPtr.addr, align 4, !tbaa !7
  %stableDst = getelementptr inbounds %struct.LZ4F_decompressOptions_t, %struct.LZ4F_decompressOptions_t* %638, i32 0, i32 0
  %639 = load i32, i32* %stableDst, align 4, !tbaa !23
  %tobool743 = icmp ne i32 %639, 0
  br i1 %tobool743, label %if.end817, label %land.lhs.true744

land.lhs.true744:                                 ; preds = %land.lhs.true742
  %640 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage745 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %640, i32 0, i32 2
  %641 = load i32, i32* %dStage745, align 4, !tbaa !57
  %sub746 = sub i32 %641, 2
  %cmp747 = icmp ult i32 %sub746, 8
  br i1 %cmp747, label %if.then749, label %if.end817

if.then749:                                       ; preds = %land.lhs.true744
  %642 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage750 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %642, i32 0, i32 2
  %643 = load i32, i32* %dStage750, align 4, !tbaa !57
  %cmp751 = icmp eq i32 %643, 9
  br i1 %cmp751, label %if.then753, label %if.else790

if.then753:                                       ; preds = %if.then749
  %644 = bitcast i32* %preserveSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %644) #4
  %645 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOut754 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %645, i32 0, i32 12
  %646 = load i8*, i8** %tmpOut754, align 8, !tbaa !69
  %647 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer755 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %647, i32 0, i32 9
  %648 = load i8*, i8** %tmpOutBuffer755, align 4, !tbaa !59
  %sub.ptr.lhs.cast756 = ptrtoint i8* %646 to i32
  %sub.ptr.rhs.cast757 = ptrtoint i8* %648 to i32
  %sub.ptr.sub758 = sub i32 %sub.ptr.lhs.cast756, %sub.ptr.rhs.cast757
  store i32 %sub.ptr.sub758, i32* %preserveSize, align 4, !tbaa !3
  %649 = bitcast i32* %copySize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %649) #4
  %650 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutSize759 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %650, i32 0, i32 13
  %651 = load i32, i32* %tmpOutSize759, align 4, !tbaa !71
  %sub760 = sub i32 65536, %651
  store i32 %sub760, i32* %copySize, align 4, !tbaa !3
  %652 = bitcast i8** %oldDictEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %652) #4
  %653 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict761 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %653, i32 0, i32 10
  %654 = load i8*, i8** %dict761, align 8, !tbaa !60
  %655 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize762 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %655, i32 0, i32 11
  %656 = load i32, i32* %dictSize762, align 4, !tbaa !61
  %add.ptr763 = getelementptr inbounds i8, i8* %654, i32 %656
  %657 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutStart764 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %657, i32 0, i32 14
  %658 = load i32, i32* %tmpOutStart764, align 8, !tbaa !70
  %idx.neg = sub i32 0, %658
  %add.ptr765 = getelementptr inbounds i8, i8* %add.ptr763, i32 %idx.neg
  store i8* %add.ptr765, i8** %oldDictEnd, align 4, !tbaa !7
  %659 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutSize766 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %659, i32 0, i32 13
  %660 = load i32, i32* %tmpOutSize766, align 4, !tbaa !71
  %cmp767 = icmp ugt i32 %660, 65536
  br i1 %cmp767, label %if.then769, label %if.end770

if.then769:                                       ; preds = %if.then753
  store i32 0, i32* %copySize, align 4, !tbaa !3
  br label %if.end770

if.end770:                                        ; preds = %if.then769, %if.then753
  %661 = load i32, i32* %copySize, align 4, !tbaa !3
  %662 = load i32, i32* %preserveSize, align 4, !tbaa !3
  %cmp771 = icmp ugt i32 %661, %662
  br i1 %cmp771, label %if.then773, label %if.end774

if.then773:                                       ; preds = %if.end770
  %663 = load i32, i32* %preserveSize, align 4, !tbaa !3
  store i32 %663, i32* %copySize, align 4, !tbaa !3
  br label %if.end774

if.end774:                                        ; preds = %if.then773, %if.end770
  %664 = load i32, i32* %copySize, align 4, !tbaa !3
  %cmp775 = icmp ugt i32 %664, 0
  br i1 %cmp775, label %if.then777, label %if.end784

if.then777:                                       ; preds = %if.end774
  %665 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer778 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %665, i32 0, i32 9
  %666 = load i8*, i8** %tmpOutBuffer778, align 4, !tbaa !59
  %667 = load i32, i32* %preserveSize, align 4, !tbaa !3
  %add.ptr779 = getelementptr inbounds i8, i8* %666, i32 %667
  %668 = load i32, i32* %copySize, align 4, !tbaa !3
  %idx.neg780 = sub i32 0, %668
  %add.ptr781 = getelementptr inbounds i8, i8* %add.ptr779, i32 %idx.neg780
  %669 = load i8*, i8** %oldDictEnd, align 4, !tbaa !7
  %670 = load i32, i32* %copySize, align 4, !tbaa !3
  %idx.neg782 = sub i32 0, %670
  %add.ptr783 = getelementptr inbounds i8, i8* %669, i32 %idx.neg782
  %671 = load i32, i32* %copySize, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr781, i8* align 1 %add.ptr783, i32 %671, i1 false)
  br label %if.end784

if.end784:                                        ; preds = %if.then777, %if.end774
  %672 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer785 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %672, i32 0, i32 9
  %673 = load i8*, i8** %tmpOutBuffer785, align 4, !tbaa !59
  %674 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict786 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %674, i32 0, i32 10
  store i8* %673, i8** %dict786, align 8, !tbaa !60
  %675 = load i32, i32* %preserveSize, align 4, !tbaa !3
  %676 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutStart787 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %676, i32 0, i32 14
  %677 = load i32, i32* %tmpOutStart787, align 8, !tbaa !70
  %add788 = add i32 %675, %677
  %678 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize789 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %678, i32 0, i32 11
  store i32 %add788, i32* %dictSize789, align 4, !tbaa !61
  %679 = bitcast i8** %oldDictEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %679) #4
  %680 = bitcast i32* %copySize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %680) #4
  %681 = bitcast i32* %preserveSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %681) #4
  br label %if.end816

if.else790:                                       ; preds = %if.then749
  %682 = bitcast i8** %oldDictEnd791 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %682) #4
  %683 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict792 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %683, i32 0, i32 10
  %684 = load i8*, i8** %dict792, align 8, !tbaa !60
  %685 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize793 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %685, i32 0, i32 11
  %686 = load i32, i32* %dictSize793, align 4, !tbaa !61
  %add.ptr794 = getelementptr inbounds i8, i8* %684, i32 %686
  store i8* %add.ptr794, i8** %oldDictEnd791, align 4, !tbaa !7
  %687 = bitcast i32* %newDictSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %687) #4
  %688 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize795 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %688, i32 0, i32 11
  %689 = load i32, i32* %dictSize795, align 4, !tbaa !61
  %cmp796 = icmp ult i32 %689, 65536
  br i1 %cmp796, label %cond.true798, label %cond.false800

cond.true798:                                     ; preds = %if.else790
  %690 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize799 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %690, i32 0, i32 11
  %691 = load i32, i32* %dictSize799, align 4, !tbaa !61
  br label %cond.end801

cond.false800:                                    ; preds = %if.else790
  br label %cond.end801

cond.end801:                                      ; preds = %cond.false800, %cond.true798
  %cond802 = phi i32 [ %691, %cond.true798 ], [ 65536, %cond.false800 ]
  store i32 %cond802, i32* %newDictSize, align 4, !tbaa !3
  %692 = load i32, i32* %newDictSize, align 4, !tbaa !3
  %cmp803 = icmp ugt i32 %692, 0
  br i1 %cmp803, label %if.then805, label %if.end809

if.then805:                                       ; preds = %cond.end801
  %693 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer806 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %693, i32 0, i32 9
  %694 = load i8*, i8** %tmpOutBuffer806, align 4, !tbaa !59
  %695 = load i8*, i8** %oldDictEnd791, align 4, !tbaa !7
  %696 = load i32, i32* %newDictSize, align 4, !tbaa !3
  %idx.neg807 = sub i32 0, %696
  %add.ptr808 = getelementptr inbounds i8, i8* %695, i32 %idx.neg807
  %697 = load i32, i32* %newDictSize, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %694, i8* align 1 %add.ptr808, i32 %697, i1 false)
  br label %if.end809

if.end809:                                        ; preds = %if.then805, %cond.end801
  %698 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer810 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %698, i32 0, i32 9
  %699 = load i8*, i8** %tmpOutBuffer810, align 4, !tbaa !59
  %700 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict811 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %700, i32 0, i32 10
  store i8* %699, i8** %dict811, align 8, !tbaa !60
  %701 = load i32, i32* %newDictSize, align 4, !tbaa !3
  %702 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize812 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %702, i32 0, i32 11
  store i32 %701, i32* %dictSize812, align 4, !tbaa !61
  %703 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer813 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %703, i32 0, i32 9
  %704 = load i8*, i8** %tmpOutBuffer813, align 4, !tbaa !59
  %705 = load i32, i32* %newDictSize, align 4, !tbaa !3
  %add.ptr814 = getelementptr inbounds i8, i8* %704, i32 %705
  %706 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOut815 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %706, i32 0, i32 12
  store i8* %add.ptr814, i8** %tmpOut815, align 8, !tbaa !69
  %707 = bitcast i32* %newDictSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %707) #4
  %708 = bitcast i8** %oldDictEnd791 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %708) #4
  br label %if.end816

if.end816:                                        ; preds = %if.end809, %if.end784
  br label %if.end817

if.end817:                                        ; preds = %if.end816, %land.lhs.true744, %land.lhs.true742, %land.lhs.true737, %while.end
  %709 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %710 = load i8*, i8** %srcStart, align 4, !tbaa !7
  %sub.ptr.lhs.cast818 = ptrtoint i8* %709 to i32
  %sub.ptr.rhs.cast819 = ptrtoint i8* %710 to i32
  %sub.ptr.sub820 = sub i32 %sub.ptr.lhs.cast818, %sub.ptr.rhs.cast819
  %711 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  store i32 %sub.ptr.sub820, i32* %711, align 4, !tbaa !3
  %712 = load i8*, i8** %dstPtr, align 4, !tbaa !7
  %713 = load i8*, i8** %dstStart, align 4, !tbaa !7
  %sub.ptr.lhs.cast821 = ptrtoint i8* %712 to i32
  %sub.ptr.rhs.cast822 = ptrtoint i8* %713 to i32
  %sub.ptr.sub823 = sub i32 %sub.ptr.lhs.cast821, %sub.ptr.rhs.cast822
  %714 = load i32*, i32** %dstSizePtr.addr, align 4, !tbaa !7
  store i32 %sub.ptr.sub823, i32* %714, align 4, !tbaa !3
  %715 = load i32, i32* %nextSrcSizeHint, align 4, !tbaa !3
  store i32 %715, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup824

cleanup824:                                       ; preds = %if.end817, %cleanup640, %if.then573, %cleanup525, %cleanup440, %cleanup387, %cleanup310, %cleanup171, %cleanup87, %cleanup55, %if.then17, %cleanup
  %716 = bitcast i32* %nextSrcSizeHint to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %716) #4
  %717 = bitcast i32* %doAnotherStage to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %717) #4
  %718 = bitcast i8** %selectedIn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %718) #4
  %719 = bitcast i8** %dstPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %719) #4
  %720 = bitcast i8** %dstEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %720) #4
  %721 = bitcast i8** %dstStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %721) #4
  %722 = bitcast i8** %srcPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %722) #4
  %723 = bitcast i8** %srcEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %723) #4
  %724 = bitcast i8** %srcStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %724) #4
  %725 = bitcast %struct.LZ4F_decompressOptions_t* %optionsNull to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %725) #4
  %726 = load i32, i32* %retval, align 4
  ret i32 %726

unreachable:                                      ; preds = %cleanup699, %cleanup625, %cleanup365, %cleanup132
  unreachable
}

; Function Attrs: nounwind
define internal i32 @LZ4F_decodeHeader(%struct.LZ4F_dctx_s* %dctx, i8* %src, i32 %srcSize) #0 {
entry:
  %retval = alloca i32, align 4
  %dctx.addr = alloca %struct.LZ4F_dctx_s*, align 4
  %src.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %blockMode = alloca i32, align 4
  %blockChecksumFlag = alloca i32, align 4
  %contentSizeFlag = alloca i32, align 4
  %contentChecksumFlag = alloca i32, align 4
  %dictIDFlag = alloca i32, align 4
  %blockSizeID = alloca i32, align 4
  %frameHeaderSize = alloca i32, align 4
  %srcPtr = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %FLG = alloca i32, align 4
  %version = alloca i32, align 4
  %BD = alloca i32, align 4
  %HC = alloca i8, align 1
  store %struct.LZ4F_dctx_s* %dctx, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !3
  %0 = bitcast i32* %blockMode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %blockChecksumFlag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %contentSizeFlag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %contentChecksumFlag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %dictIDFlag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %blockSizeID to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %frameHeaderSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i8** %srcPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load i8*, i8** %src.addr, align 4, !tbaa !7
  store i8* %8, i8** %srcPtr, align 4, !tbaa !7
  %9 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %cmp = icmp ult i32 %9, 7
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call i32 @err0r(i32 12)
  store i32 %call, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup119

if.end:                                           ; preds = %entry
  %10 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %10, i32 0, i32 0
  %11 = bitcast %struct.LZ4F_frameInfo_t* %frameInfo to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %11, i8 0, i32 32, i1 false)
  %12 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %call1 = call i32 @LZ4F_readLE32(i8* %12)
  %and = and i32 %call1, -16
  %cmp2 = icmp eq i32 %and, 407710288
  br i1 %cmp2, label %if.then3, label %if.end8

if.then3:                                         ; preds = %if.end
  %13 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo4 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %13, i32 0, i32 0
  %frameType = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo4, i32 0, i32 3
  store i32 1, i32* %frameType, align 4, !tbaa !75
  %14 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %15 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %header = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %15, i32 0, i32 17
  %arraydecay = getelementptr inbounds [19 x i8], [19 x i8]* %header, i32 0, i32 0
  %cmp5 = icmp eq i8* %14, %arraydecay
  br i1 %cmp5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.then3
  %16 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %17 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %17, i32 0, i32 7
  store i32 %16, i32* %tmpInSize, align 4, !tbaa !63
  %18 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %18, i32 0, i32 8
  store i32 8, i32* %tmpInTarget, align 8, !tbaa !64
  %19 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %19, i32 0, i32 2
  store i32 13, i32* %dStage, align 4, !tbaa !57
  %20 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  store i32 %20, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup119

if.else:                                          ; preds = %if.then3
  %21 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage7 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %21, i32 0, i32 2
  store i32 12, i32* %dStage7, align 4, !tbaa !57
  store i32 4, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup119

if.end8:                                          ; preds = %if.end
  %22 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %call9 = call i32 @LZ4F_readLE32(i8* %22)
  %cmp10 = icmp ne i32 %call9, 407708164
  br i1 %cmp10, label %if.then11, label %if.end13

if.then11:                                        ; preds = %if.end8
  %call12 = call i32 @err0r(i32 13)
  store i32 %call12, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup119

if.end13:                                         ; preds = %if.end8
  %23 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo14 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %23, i32 0, i32 0
  %frameType15 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo14, i32 0, i32 3
  store i32 0, i32* %frameType15, align 4, !tbaa !75
  %24 = bitcast i32* %FLG to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i8, i8* %25, i32 4
  %26 = load i8, i8* %arrayidx, align 1, !tbaa !11
  %conv = zext i8 %26 to i32
  store i32 %conv, i32* %FLG, align 4, !tbaa !9
  %27 = bitcast i32* %version to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load i32, i32* %FLG, align 4, !tbaa !9
  %shr = lshr i32 %28, 6
  %and16 = and i32 %shr, 3
  store i32 %and16, i32* %version, align 4, !tbaa !9
  %29 = load i32, i32* %FLG, align 4, !tbaa !9
  %shr17 = lshr i32 %29, 4
  %and18 = and i32 %shr17, 1
  store i32 %and18, i32* %blockChecksumFlag, align 4, !tbaa !9
  %30 = load i32, i32* %FLG, align 4, !tbaa !9
  %shr19 = lshr i32 %30, 5
  %and20 = and i32 %shr19, 1
  store i32 %and20, i32* %blockMode, align 4, !tbaa !9
  %31 = load i32, i32* %FLG, align 4, !tbaa !9
  %shr21 = lshr i32 %31, 3
  %and22 = and i32 %shr21, 1
  store i32 %and22, i32* %contentSizeFlag, align 4, !tbaa !9
  %32 = load i32, i32* %FLG, align 4, !tbaa !9
  %shr23 = lshr i32 %32, 2
  %and24 = and i32 %shr23, 1
  store i32 %and24, i32* %contentChecksumFlag, align 4, !tbaa !9
  %33 = load i32, i32* %FLG, align 4, !tbaa !9
  %and25 = and i32 %33, 1
  store i32 %and25, i32* %dictIDFlag, align 4, !tbaa !9
  %34 = load i32, i32* %FLG, align 4, !tbaa !9
  %shr26 = lshr i32 %34, 1
  %and27 = and i32 %shr26, 1
  %cmp28 = icmp ne i32 %and27, 0
  br i1 %cmp28, label %if.then30, label %if.end32

if.then30:                                        ; preds = %if.end13
  %call31 = call i32 @err0r(i32 8)
  store i32 %call31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end32:                                         ; preds = %if.end13
  %35 = load i32, i32* %version, align 4, !tbaa !9
  %cmp33 = icmp ne i32 %35, 1
  br i1 %cmp33, label %if.then35, label %if.end37

if.then35:                                        ; preds = %if.end32
  %call36 = call i32 @err0r(i32 6)
  store i32 %call36, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end37:                                         ; preds = %if.end32
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end37, %if.then35, %if.then30
  %36 = bitcast i32* %version to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  %37 = bitcast i32* %FLG to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup119 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  %38 = load i32, i32* %contentSizeFlag, align 4, !tbaa !9
  %tobool = icmp ne i32 %38, 0
  %39 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 8, i32 0
  %add = add i32 7, %cond
  %40 = load i32, i32* %dictIDFlag, align 4, !tbaa !9
  %tobool39 = icmp ne i32 %40, 0
  %41 = zext i1 %tobool39 to i64
  %cond40 = select i1 %tobool39, i32 4, i32 0
  %add41 = add i32 %add, %cond40
  store i32 %add41, i32* %frameHeaderSize, align 4, !tbaa !3
  %42 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %43 = load i32, i32* %frameHeaderSize, align 4, !tbaa !3
  %cmp42 = icmp ult i32 %42, %43
  br i1 %cmp42, label %if.then44, label %if.end56

if.then44:                                        ; preds = %cleanup.cont
  %44 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %45 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %header45 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %45, i32 0, i32 17
  %arraydecay46 = getelementptr inbounds [19 x i8], [19 x i8]* %header45, i32 0, i32 0
  %cmp47 = icmp ne i8* %44, %arraydecay46
  br i1 %cmp47, label %if.then49, label %if.end52

if.then49:                                        ; preds = %if.then44
  %46 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %header50 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %46, i32 0, i32 17
  %arraydecay51 = getelementptr inbounds [19 x i8], [19 x i8]* %header50, i32 0, i32 0
  %47 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %48 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %arraydecay51, i8* align 1 %47, i32 %48, i1 false)
  br label %if.end52

if.end52:                                         ; preds = %if.then49, %if.then44
  %49 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %50 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInSize53 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %50, i32 0, i32 7
  store i32 %49, i32* %tmpInSize53, align 4, !tbaa !63
  %51 = load i32, i32* %frameHeaderSize, align 4, !tbaa !3
  %52 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpInTarget54 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %52, i32 0, i32 8
  store i32 %51, i32* %tmpInTarget54, align 8, !tbaa !64
  %53 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage55 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %53, i32 0, i32 2
  store i32 1, i32* %dStage55, align 4, !tbaa !57
  %54 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  store i32 %54, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup119

if.end56:                                         ; preds = %cleanup.cont
  %55 = bitcast i32* %BD to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #4
  %56 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %arrayidx57 = getelementptr inbounds i8, i8* %56, i32 5
  %57 = load i8, i8* %arrayidx57, align 1, !tbaa !11
  %conv58 = zext i8 %57 to i32
  store i32 %conv58, i32* %BD, align 4, !tbaa !9
  %58 = load i32, i32* %BD, align 4, !tbaa !9
  %shr59 = lshr i32 %58, 4
  %and60 = and i32 %shr59, 7
  store i32 %and60, i32* %blockSizeID, align 4, !tbaa !9
  %59 = load i32, i32* %BD, align 4, !tbaa !9
  %shr61 = lshr i32 %59, 7
  %and62 = and i32 %shr61, 1
  %cmp63 = icmp ne i32 %and62, 0
  br i1 %cmp63, label %if.then65, label %if.end67

if.then65:                                        ; preds = %if.end56
  %call66 = call i32 @err0r(i32 8)
  store i32 %call66, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup80

if.end67:                                         ; preds = %if.end56
  %60 = load i32, i32* %blockSizeID, align 4, !tbaa !9
  %cmp68 = icmp ult i32 %60, 4
  br i1 %cmp68, label %if.then70, label %if.end72

if.then70:                                        ; preds = %if.end67
  %call71 = call i32 @err0r(i32 2)
  store i32 %call71, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup80

if.end72:                                         ; preds = %if.end67
  %61 = load i32, i32* %BD, align 4, !tbaa !9
  %shr73 = lshr i32 %61, 0
  %and74 = and i32 %shr73, 15
  %cmp75 = icmp ne i32 %and74, 0
  br i1 %cmp75, label %if.then77, label %if.end79

if.then77:                                        ; preds = %if.end72
  %call78 = call i32 @err0r(i32 8)
  store i32 %call78, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup80

if.end79:                                         ; preds = %if.end72
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup80

cleanup80:                                        ; preds = %if.end79, %if.then77, %if.then70, %if.then65
  %62 = bitcast i32* %BD to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #4
  %cleanup.dest81 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest81, label %cleanup119 [
    i32 0, label %cleanup.cont82
  ]

cleanup.cont82:                                   ; preds = %cleanup80
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %HC) #4
  %63 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds i8, i8* %63, i32 4
  %64 = load i32, i32* %frameHeaderSize, align 4, !tbaa !3
  %sub = sub i32 %64, 5
  %call83 = call zeroext i8 @LZ4F_headerChecksum(i8* %add.ptr, i32 %sub)
  store i8 %call83, i8* %HC, align 1, !tbaa !11
  %65 = load i8, i8* %HC, align 1, !tbaa !11
  %conv84 = zext i8 %65 to i32
  %66 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %67 = load i32, i32* %frameHeaderSize, align 4, !tbaa !3
  %sub85 = sub i32 %67, 1
  %arrayidx86 = getelementptr inbounds i8, i8* %66, i32 %sub85
  %68 = load i8, i8* %arrayidx86, align 1, !tbaa !11
  %conv87 = zext i8 %68 to i32
  %cmp88 = icmp ne i32 %conv84, %conv87
  br i1 %cmp88, label %if.then90, label %if.end92

if.then90:                                        ; preds = %cleanup.cont82
  %call91 = call i32 @err0r(i32 17)
  store i32 %call91, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup93

if.end92:                                         ; preds = %cleanup.cont82
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup93

cleanup93:                                        ; preds = %if.end92, %if.then90
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %HC) #4
  %cleanup.dest94 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest94, label %cleanup119 [
    i32 0, label %cleanup.cont95
  ]

cleanup.cont95:                                   ; preds = %cleanup93
  %69 = load i32, i32* %blockMode, align 4, !tbaa !9
  %70 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo96 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %70, i32 0, i32 0
  %blockMode97 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo96, i32 0, i32 1
  store i32 %69, i32* %blockMode97, align 4, !tbaa !67
  %71 = load i32, i32* %blockChecksumFlag, align 4, !tbaa !9
  %72 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo98 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %72, i32 0, i32 0
  %blockChecksumFlag99 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo98, i32 0, i32 6
  store i32 %71, i32* %blockChecksumFlag99, align 4, !tbaa !72
  %73 = load i32, i32* %contentChecksumFlag, align 4, !tbaa !9
  %74 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo100 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %74, i32 0, i32 0
  %contentChecksumFlag101 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo100, i32 0, i32 2
  store i32 %73, i32* %contentChecksumFlag101, align 8, !tbaa !65
  %75 = load i32, i32* %blockSizeID, align 4, !tbaa !9
  %76 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo102 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %76, i32 0, i32 0
  %blockSizeID103 = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo102, i32 0, i32 0
  store i32 %75, i32* %blockSizeID103, align 8, !tbaa !76
  %77 = load i32, i32* %blockSizeID, align 4, !tbaa !9
  %call104 = call i32 @LZ4F_getBlockSize(i32 %77)
  %78 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %maxBlockSize = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %78, i32 0, i32 4
  store i32 %call104, i32* %maxBlockSize, align 8, !tbaa !66
  %79 = load i32, i32* %contentSizeFlag, align 4, !tbaa !9
  %tobool105 = icmp ne i32 %79, 0
  br i1 %tobool105, label %if.then106, label %if.end110

if.then106:                                       ; preds = %cleanup.cont95
  %80 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %add.ptr107 = getelementptr inbounds i8, i8* %80, i32 6
  %call108 = call i64 @LZ4F_readLE64(i8* %add.ptr107)
  %81 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo109 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %81, i32 0, i32 0
  %contentSize = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo109, i32 0, i32 4
  store i64 %call108, i64* %contentSize, align 8, !tbaa !73
  %82 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameRemainingSize = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %82, i32 0, i32 3
  store i64 %call108, i64* %frameRemainingSize, align 8, !tbaa !74
  br label %if.end110

if.end110:                                        ; preds = %if.then106, %cleanup.cont95
  %83 = load i32, i32* %dictIDFlag, align 4, !tbaa !9
  %tobool111 = icmp ne i32 %83, 0
  br i1 %tobool111, label %if.then112, label %if.end117

if.then112:                                       ; preds = %if.end110
  %84 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %85 = load i32, i32* %frameHeaderSize, align 4, !tbaa !3
  %add.ptr113 = getelementptr inbounds i8, i8* %84, i32 %85
  %add.ptr114 = getelementptr inbounds i8, i8* %add.ptr113, i32 -5
  %call115 = call i32 @LZ4F_readLE32(i8* %add.ptr114)
  %86 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %frameInfo116 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %86, i32 0, i32 0
  %dictID = getelementptr inbounds %struct.LZ4F_frameInfo_t, %struct.LZ4F_frameInfo_t* %frameInfo116, i32 0, i32 5
  store i32 %call115, i32* %dictID, align 8, !tbaa !77
  br label %if.end117

if.end117:                                        ; preds = %if.then112, %if.end110
  %87 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage118 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %87, i32 0, i32 2
  store i32 2, i32* %dStage118, align 4, !tbaa !57
  %88 = load i32, i32* %frameHeaderSize, align 4, !tbaa !3
  store i32 %88, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup119

cleanup119:                                       ; preds = %if.end117, %cleanup93, %cleanup80, %if.end52, %cleanup, %if.then11, %if.else, %if.then6, %if.then
  %89 = bitcast i8** %srcPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %90 = bitcast i32* %frameHeaderSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #4
  %91 = bitcast i32* %blockSizeID to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #4
  %92 = bitcast i32* %dictIDFlag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  %93 = bitcast i32* %contentChecksumFlag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #4
  %94 = bitcast i32* %contentSizeFlag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #4
  %95 = bitcast i32* %blockChecksumFlag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #4
  %96 = bitcast i32* %blockMode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #4
  %97 = load i32, i32* %retval, align 4
  ret i32 %97
}

; Function Attrs: nounwind
define internal void @LZ4F_updateDict(%struct.LZ4F_dctx_s* %dctx, i8* %dstPtr, i32 %dstSize, i8* %dstBufferStart, i32 %withinTmp) #0 {
entry:
  %dctx.addr = alloca %struct.LZ4F_dctx_s*, align 4
  %dstPtr.addr = alloca i8*, align 4
  %dstSize.addr = alloca i32, align 4
  %dstBufferStart.addr = alloca i8*, align 4
  %withinTmp.addr = alloca i32, align 4
  %preserveSize = alloca i32, align 4
  %copySize = alloca i32, align 4
  %oldDictEnd = alloca i8*, align 4
  %preserveSize61 = alloca i32, align 4
  %preserveSize77 = alloca i32, align 4
  store %struct.LZ4F_dctx_s* %dctx, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  store i8* %dstPtr, i8** %dstPtr.addr, align 4, !tbaa !7
  store i32 %dstSize, i32* %dstSize.addr, align 4, !tbaa !3
  store i8* %dstBufferStart, i8** %dstBufferStart.addr, align 4, !tbaa !7
  store i32 %withinTmp, i32* %withinTmp.addr, align 4, !tbaa !9
  %0 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %0, i32 0, i32 11
  %1 = load i32, i32* %dictSize, align 4, !tbaa !61
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i8*, i8** %dstPtr.addr, align 4, !tbaa !7
  %3 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %3, i32 0, i32 10
  store i8* %2, i8** %dict, align 8, !tbaa !60
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict1 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %4, i32 0, i32 10
  %5 = load i8*, i8** %dict1, align 8, !tbaa !60
  %6 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize2 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %6, i32 0, i32 11
  %7 = load i32, i32* %dictSize2, align 4, !tbaa !61
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %7
  %8 = load i8*, i8** %dstPtr.addr, align 4, !tbaa !7
  %cmp3 = icmp eq i8* %add.ptr, %8
  br i1 %cmp3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.end
  %9 = load i32, i32* %dstSize.addr, align 4, !tbaa !3
  %10 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize5 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %10, i32 0, i32 11
  %11 = load i32, i32* %dictSize5, align 4, !tbaa !61
  %add = add i32 %11, %9
  store i32 %add, i32* %dictSize5, align 4, !tbaa !61
  br label %return

if.end6:                                          ; preds = %if.end
  %12 = load i8*, i8** %dstPtr.addr, align 4, !tbaa !7
  %13 = load i8*, i8** %dstBufferStart.addr, align 4, !tbaa !7
  %sub.ptr.lhs.cast = ptrtoint i8* %12 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %13 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %14 = load i32, i32* %dstSize.addr, align 4, !tbaa !3
  %add7 = add i32 %sub.ptr.sub, %14
  %cmp8 = icmp uge i32 %add7, 65536
  br i1 %cmp8, label %if.then9, label %if.end16

if.then9:                                         ; preds = %if.end6
  %15 = load i8*, i8** %dstBufferStart.addr, align 4, !tbaa !7
  %16 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict10 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %16, i32 0, i32 10
  store i8* %15, i8** %dict10, align 8, !tbaa !60
  %17 = load i8*, i8** %dstPtr.addr, align 4, !tbaa !7
  %18 = load i8*, i8** %dstBufferStart.addr, align 4, !tbaa !7
  %sub.ptr.lhs.cast11 = ptrtoint i8* %17 to i32
  %sub.ptr.rhs.cast12 = ptrtoint i8* %18 to i32
  %sub.ptr.sub13 = sub i32 %sub.ptr.lhs.cast11, %sub.ptr.rhs.cast12
  %19 = load i32, i32* %dstSize.addr, align 4, !tbaa !3
  %add14 = add i32 %sub.ptr.sub13, %19
  %20 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize15 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %20, i32 0, i32 11
  store i32 %add14, i32* %dictSize15, align 4, !tbaa !61
  br label %return

if.end16:                                         ; preds = %if.end6
  %21 = load i32, i32* %withinTmp.addr, align 4, !tbaa !9
  %tobool = icmp ne i32 %21, 0
  br i1 %tobool, label %land.lhs.true, label %if.end22

land.lhs.true:                                    ; preds = %if.end16
  %22 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict17 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %22, i32 0, i32 10
  %23 = load i8*, i8** %dict17, align 8, !tbaa !60
  %24 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %24, i32 0, i32 9
  %25 = load i8*, i8** %tmpOutBuffer, align 4, !tbaa !59
  %cmp18 = icmp eq i8* %23, %25
  br i1 %cmp18, label %if.then19, label %if.end22

if.then19:                                        ; preds = %land.lhs.true
  %26 = load i32, i32* %dstSize.addr, align 4, !tbaa !3
  %27 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize20 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %27, i32 0, i32 11
  %28 = load i32, i32* %dictSize20, align 4, !tbaa !61
  %add21 = add i32 %28, %26
  store i32 %add21, i32* %dictSize20, align 4, !tbaa !61
  br label %return

if.end22:                                         ; preds = %land.lhs.true, %if.end16
  %29 = load i32, i32* %withinTmp.addr, align 4, !tbaa !9
  %tobool23 = icmp ne i32 %29, 0
  br i1 %tobool23, label %if.then24, label %if.end52

if.then24:                                        ; preds = %if.end22
  %30 = bitcast i32* %preserveSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  %31 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOut = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %31, i32 0, i32 12
  %32 = load i8*, i8** %tmpOut, align 8, !tbaa !69
  %33 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer25 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %33, i32 0, i32 9
  %34 = load i8*, i8** %tmpOutBuffer25, align 4, !tbaa !59
  %sub.ptr.lhs.cast26 = ptrtoint i8* %32 to i32
  %sub.ptr.rhs.cast27 = ptrtoint i8* %34 to i32
  %sub.ptr.sub28 = sub i32 %sub.ptr.lhs.cast26, %sub.ptr.rhs.cast27
  store i32 %sub.ptr.sub28, i32* %preserveSize, align 4, !tbaa !3
  %35 = bitcast i32* %copySize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #4
  %36 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutSize = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %36, i32 0, i32 13
  %37 = load i32, i32* %tmpOutSize, align 4, !tbaa !71
  %sub = sub i32 65536, %37
  store i32 %sub, i32* %copySize, align 4, !tbaa !3
  %38 = bitcast i8** %oldDictEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #4
  %39 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict29 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %39, i32 0, i32 10
  %40 = load i8*, i8** %dict29, align 8, !tbaa !60
  %41 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize30 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %41, i32 0, i32 11
  %42 = load i32, i32* %dictSize30, align 4, !tbaa !61
  %add.ptr31 = getelementptr inbounds i8, i8* %40, i32 %42
  %43 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutStart = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %43, i32 0, i32 14
  %44 = load i32, i32* %tmpOutStart, align 8, !tbaa !70
  %idx.neg = sub i32 0, %44
  %add.ptr32 = getelementptr inbounds i8, i8* %add.ptr31, i32 %idx.neg
  store i8* %add.ptr32, i8** %oldDictEnd, align 4, !tbaa !7
  %45 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutSize33 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %45, i32 0, i32 13
  %46 = load i32, i32* %tmpOutSize33, align 4, !tbaa !71
  %cmp34 = icmp ugt i32 %46, 65536
  br i1 %cmp34, label %if.then35, label %if.end36

if.then35:                                        ; preds = %if.then24
  store i32 0, i32* %copySize, align 4, !tbaa !3
  br label %if.end36

if.end36:                                         ; preds = %if.then35, %if.then24
  %47 = load i32, i32* %copySize, align 4, !tbaa !3
  %48 = load i32, i32* %preserveSize, align 4, !tbaa !3
  %cmp37 = icmp ugt i32 %47, %48
  br i1 %cmp37, label %if.then38, label %if.end39

if.then38:                                        ; preds = %if.end36
  %49 = load i32, i32* %preserveSize, align 4, !tbaa !3
  store i32 %49, i32* %copySize, align 4, !tbaa !3
  br label %if.end39

if.end39:                                         ; preds = %if.then38, %if.end36
  %50 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer40 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %50, i32 0, i32 9
  %51 = load i8*, i8** %tmpOutBuffer40, align 4, !tbaa !59
  %52 = load i32, i32* %preserveSize, align 4, !tbaa !3
  %add.ptr41 = getelementptr inbounds i8, i8* %51, i32 %52
  %53 = load i32, i32* %copySize, align 4, !tbaa !3
  %idx.neg42 = sub i32 0, %53
  %add.ptr43 = getelementptr inbounds i8, i8* %add.ptr41, i32 %idx.neg42
  %54 = load i8*, i8** %oldDictEnd, align 4, !tbaa !7
  %55 = load i32, i32* %copySize, align 4, !tbaa !3
  %idx.neg44 = sub i32 0, %55
  %add.ptr45 = getelementptr inbounds i8, i8* %54, i32 %idx.neg44
  %56 = load i32, i32* %copySize, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr43, i8* align 1 %add.ptr45, i32 %56, i1 false)
  %57 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer46 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %57, i32 0, i32 9
  %58 = load i8*, i8** %tmpOutBuffer46, align 4, !tbaa !59
  %59 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict47 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %59, i32 0, i32 10
  store i8* %58, i8** %dict47, align 8, !tbaa !60
  %60 = load i32, i32* %preserveSize, align 4, !tbaa !3
  %61 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutStart48 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %61, i32 0, i32 14
  %62 = load i32, i32* %tmpOutStart48, align 8, !tbaa !70
  %add49 = add i32 %60, %62
  %63 = load i32, i32* %dstSize.addr, align 4, !tbaa !3
  %add50 = add i32 %add49, %63
  %64 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize51 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %64, i32 0, i32 11
  store i32 %add50, i32* %dictSize51, align 4, !tbaa !61
  %65 = bitcast i8** %oldDictEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #4
  %66 = bitcast i32* %copySize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #4
  %67 = bitcast i32* %preserveSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #4
  br label %return

if.end52:                                         ; preds = %if.end22
  %68 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict53 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %68, i32 0, i32 10
  %69 = load i8*, i8** %dict53, align 8, !tbaa !60
  %70 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer54 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %70, i32 0, i32 9
  %71 = load i8*, i8** %tmpOutBuffer54, align 4, !tbaa !59
  %cmp55 = icmp eq i8* %69, %71
  br i1 %cmp55, label %if.then56, label %if.end76

if.then56:                                        ; preds = %if.end52
  %72 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize57 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %72, i32 0, i32 11
  %73 = load i32, i32* %dictSize57, align 4, !tbaa !61
  %74 = load i32, i32* %dstSize.addr, align 4, !tbaa !3
  %add58 = add i32 %73, %74
  %75 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %maxBufferSize = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %75, i32 0, i32 5
  %76 = load i32, i32* %maxBufferSize, align 4, !tbaa !68
  %cmp59 = icmp ugt i32 %add58, %76
  br i1 %cmp59, label %if.then60, label %if.end70

if.then60:                                        ; preds = %if.then56
  %77 = bitcast i32* %preserveSize61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #4
  %78 = load i32, i32* %dstSize.addr, align 4, !tbaa !3
  %sub62 = sub i32 65536, %78
  store i32 %sub62, i32* %preserveSize61, align 4, !tbaa !3
  %79 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer63 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %79, i32 0, i32 9
  %80 = load i8*, i8** %tmpOutBuffer63, align 4, !tbaa !59
  %81 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict64 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %81, i32 0, i32 10
  %82 = load i8*, i8** %dict64, align 8, !tbaa !60
  %83 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize65 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %83, i32 0, i32 11
  %84 = load i32, i32* %dictSize65, align 4, !tbaa !61
  %add.ptr66 = getelementptr inbounds i8, i8* %82, i32 %84
  %85 = load i32, i32* %preserveSize61, align 4, !tbaa !3
  %idx.neg67 = sub i32 0, %85
  %add.ptr68 = getelementptr inbounds i8, i8* %add.ptr66, i32 %idx.neg67
  %86 = load i32, i32* %preserveSize61, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %80, i8* align 1 %add.ptr68, i32 %86, i1 false)
  %87 = load i32, i32* %preserveSize61, align 4, !tbaa !3
  %88 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize69 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %88, i32 0, i32 11
  store i32 %87, i32* %dictSize69, align 4, !tbaa !61
  %89 = bitcast i32* %preserveSize61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  br label %if.end70

if.end70:                                         ; preds = %if.then60, %if.then56
  %90 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer71 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %90, i32 0, i32 9
  %91 = load i8*, i8** %tmpOutBuffer71, align 4, !tbaa !59
  %92 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize72 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %92, i32 0, i32 11
  %93 = load i32, i32* %dictSize72, align 4, !tbaa !61
  %add.ptr73 = getelementptr inbounds i8, i8* %91, i32 %93
  %94 = load i8*, i8** %dstPtr.addr, align 4, !tbaa !7
  %95 = load i32, i32* %dstSize.addr, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr73, i8* align 1 %94, i32 %95, i1 false)
  %96 = load i32, i32* %dstSize.addr, align 4, !tbaa !3
  %97 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize74 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %97, i32 0, i32 11
  %98 = load i32, i32* %dictSize74, align 4, !tbaa !61
  %add75 = add i32 %98, %96
  store i32 %add75, i32* %dictSize74, align 4, !tbaa !61
  br label %return

if.end76:                                         ; preds = %if.end52
  %99 = bitcast i32* %preserveSize77 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #4
  %100 = load i32, i32* %dstSize.addr, align 4, !tbaa !3
  %sub78 = sub i32 65536, %100
  store i32 %sub78, i32* %preserveSize77, align 4, !tbaa !3
  %101 = load i32, i32* %preserveSize77, align 4, !tbaa !3
  %102 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize79 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %102, i32 0, i32 11
  %103 = load i32, i32* %dictSize79, align 4, !tbaa !61
  %cmp80 = icmp ugt i32 %101, %103
  br i1 %cmp80, label %if.then81, label %if.end83

if.then81:                                        ; preds = %if.end76
  %104 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize82 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %104, i32 0, i32 11
  %105 = load i32, i32* %dictSize82, align 4, !tbaa !61
  store i32 %105, i32* %preserveSize77, align 4, !tbaa !3
  br label %if.end83

if.end83:                                         ; preds = %if.then81, %if.end76
  %106 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer84 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %106, i32 0, i32 9
  %107 = load i8*, i8** %tmpOutBuffer84, align 4, !tbaa !59
  %108 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict85 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %108, i32 0, i32 10
  %109 = load i8*, i8** %dict85, align 8, !tbaa !60
  %110 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize86 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %110, i32 0, i32 11
  %111 = load i32, i32* %dictSize86, align 4, !tbaa !61
  %add.ptr87 = getelementptr inbounds i8, i8* %109, i32 %111
  %112 = load i32, i32* %preserveSize77, align 4, !tbaa !3
  %idx.neg88 = sub i32 0, %112
  %add.ptr89 = getelementptr inbounds i8, i8* %add.ptr87, i32 %idx.neg88
  %113 = load i32, i32* %preserveSize77, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %107, i8* align 1 %add.ptr89, i32 %113, i1 false)
  %114 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer90 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %114, i32 0, i32 9
  %115 = load i8*, i8** %tmpOutBuffer90, align 4, !tbaa !59
  %116 = load i32, i32* %preserveSize77, align 4, !tbaa !3
  %add.ptr91 = getelementptr inbounds i8, i8* %115, i32 %116
  %117 = load i8*, i8** %dstPtr.addr, align 4, !tbaa !7
  %118 = load i32, i32* %dstSize.addr, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr91, i8* align 1 %117, i32 %118, i1 false)
  %119 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %tmpOutBuffer92 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %119, i32 0, i32 9
  %120 = load i8*, i8** %tmpOutBuffer92, align 4, !tbaa !59
  %121 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict93 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %121, i32 0, i32 10
  store i8* %120, i8** %dict93, align 8, !tbaa !60
  %122 = load i32, i32* %preserveSize77, align 4, !tbaa !3
  %123 = load i32, i32* %dstSize.addr, align 4, !tbaa !3
  %add94 = add i32 %122, %123
  %124 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize95 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %124, i32 0, i32 11
  store i32 %add94, i32* %dictSize95, align 4, !tbaa !61
  %125 = bitcast i32* %preserveSize77 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #4
  br label %return

return:                                           ; preds = %if.end83, %if.end70, %if.end39, %if.then19, %if.then9, %if.then4
  ret void
}

declare i32 @LZ4_XXH32(i8*, i32, i32) #3

declare i32 @LZ4_decompress_safe_usingDict(i8*, i8*, i32, i32, i8*, i32) #3

; Function Attrs: nounwind
define hidden i32 @LZ4F_decompress_usingDict(%struct.LZ4F_dctx_s* %dctx, i8* %dstBuffer, i32* %dstSizePtr, i8* %srcBuffer, i32* %srcSizePtr, i8* %dict, i32 %dictSize, %struct.LZ4F_decompressOptions_t* %decompressOptionsPtr) #0 {
entry:
  %dctx.addr = alloca %struct.LZ4F_dctx_s*, align 4
  %dstBuffer.addr = alloca i8*, align 4
  %dstSizePtr.addr = alloca i32*, align 4
  %srcBuffer.addr = alloca i8*, align 4
  %srcSizePtr.addr = alloca i32*, align 4
  %dict.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  %decompressOptionsPtr.addr = alloca %struct.LZ4F_decompressOptions_t*, align 4
  store %struct.LZ4F_dctx_s* %dctx, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  store i8* %dstBuffer, i8** %dstBuffer.addr, align 4, !tbaa !7
  store i32* %dstSizePtr, i32** %dstSizePtr.addr, align 4, !tbaa !7
  store i8* %srcBuffer, i8** %srcBuffer.addr, align 4, !tbaa !7
  store i32* %srcSizePtr, i32** %srcSizePtr.addr, align 4, !tbaa !7
  store i8* %dict, i8** %dict.addr, align 4, !tbaa !7
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !3
  store %struct.LZ4F_decompressOptions_t* %decompressOptionsPtr, %struct.LZ4F_decompressOptions_t** %decompressOptionsPtr.addr, align 4, !tbaa !7
  %0 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dStage = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %0, i32 0, i32 2
  %1 = load i32, i32* %dStage, align 4, !tbaa !57
  %cmp = icmp ule i32 %1, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i8*, i8** %dict.addr, align 4, !tbaa !7
  %3 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dict1 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %3, i32 0, i32 10
  store i8* %2, i8** %dict1, align 8, !tbaa !60
  %4 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %5 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %dictSize2 = getelementptr inbounds %struct.LZ4F_dctx_s, %struct.LZ4F_dctx_s* %5, i32 0, i32 11
  store i32 %4, i32* %dictSize2, align 4, !tbaa !61
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = load %struct.LZ4F_dctx_s*, %struct.LZ4F_dctx_s** %dctx.addr, align 4, !tbaa !7
  %7 = load i8*, i8** %dstBuffer.addr, align 4, !tbaa !7
  %8 = load i32*, i32** %dstSizePtr.addr, align 4, !tbaa !7
  %9 = load i8*, i8** %srcBuffer.addr, align 4, !tbaa !7
  %10 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  %11 = load %struct.LZ4F_decompressOptions_t*, %struct.LZ4F_decompressOptions_t** %decompressOptionsPtr.addr, align 4, !tbaa !7
  %call = call i32 @LZ4F_decompress(%struct.LZ4F_dctx_s* %6, i8* %7, i32* %8, i8* %9, i32* %10, %struct.LZ4F_decompressOptions_t* %11)
  ret i32 %call
}

declare void @LZ4_resetStream_fast(%union.LZ4_stream_u*) #3

declare void @LZ4_attach_dictionary(%union.LZ4_stream_u*, %union.LZ4_stream_u*) #3

declare void @LZ4_resetStreamHC_fast(%union.LZ4_streamHC_u*, i32) #3

declare void @LZ4_attach_HC_dictionary(%union.LZ4_streamHC_u*, %union.LZ4_streamHC_u*) #3

; Function Attrs: nounwind
define internal i32 @LZ4F_compressBlock(i8* %ctx, i8* %src, i8* %dst, i32 %srcSize, i32 %dstCapacity, i32 %level, %struct.LZ4F_CDict_s* %cdict) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca i8*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %dstCapacity.addr = alloca i32, align 4
  %level.addr = alloca i32, align 4
  %cdict.addr = alloca %struct.LZ4F_CDict_s*, align 4
  %acceleration = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %ctx, i8** %ctx.addr, align 4, !tbaa !7
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !7
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !9
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !9
  store i32 %level, i32* %level.addr, align 4, !tbaa !9
  store %struct.LZ4F_CDict_s* %cdict, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %0 = bitcast i32* %acceleration to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %level.addr, align 4, !tbaa !9
  %cmp = icmp slt i32 %1, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %level.addr, align 4, !tbaa !9
  %sub = sub nsw i32 0, %2
  %add = add nsw i32 %sub, 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add, %cond.true ], [ 1, %cond.false ]
  store i32 %cond, i32* %acceleration, align 4, !tbaa !9
  %3 = load i8*, i8** %ctx.addr, align 4, !tbaa !7
  %4 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %5 = load i32, i32* %level.addr, align 4, !tbaa !9
  call void @LZ4F_initStream(i8* %3, %struct.LZ4F_CDict_s* %4, i32 %5, i32 1)
  %6 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %tobool = icmp ne %struct.LZ4F_CDict_s* %6, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end
  %7 = load i8*, i8** %ctx.addr, align 4, !tbaa !7
  %8 = bitcast i8* %7 to %union.LZ4_stream_u*
  %9 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %10 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %11 = load i32, i32* %srcSize.addr, align 4, !tbaa !9
  %12 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !9
  %13 = load i32, i32* %acceleration, align 4, !tbaa !9
  %call = call i32 @LZ4_compress_fast_continue(%union.LZ4_stream_u* %8, i8* %9, i8* %10, i32 %11, i32 %12, i32 %13)
  store i32 %call, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %cond.end
  %14 = load i8*, i8** %ctx.addr, align 4, !tbaa !7
  %15 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %16 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %17 = load i32, i32* %srcSize.addr, align 4, !tbaa !9
  %18 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !9
  %19 = load i32, i32* %acceleration, align 4, !tbaa !9
  %call1 = call i32 @LZ4_compress_fast_extState_fastReset(i8* %14, i8* %15, i8* %16, i32 %17, i32 %18, i32 %19)
  store i32 %call1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then
  %20 = bitcast i32* %acceleration to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #4
  %21 = load i32, i32* %retval, align 4
  ret i32 %21
}

; Function Attrs: nounwind
define internal i32 @LZ4F_compressBlock_continue(i8* %ctx, i8* %src, i8* %dst, i32 %srcSize, i32 %dstCapacity, i32 %level, %struct.LZ4F_CDict_s* %cdict) #0 {
entry:
  %ctx.addr = alloca i8*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %dstCapacity.addr = alloca i32, align 4
  %level.addr = alloca i32, align 4
  %cdict.addr = alloca %struct.LZ4F_CDict_s*, align 4
  %acceleration = alloca i32, align 4
  store i8* %ctx, i8** %ctx.addr, align 4, !tbaa !7
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !7
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !9
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !9
  store i32 %level, i32* %level.addr, align 4, !tbaa !9
  store %struct.LZ4F_CDict_s* %cdict, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %0 = bitcast i32* %acceleration to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %level.addr, align 4, !tbaa !9
  %cmp = icmp slt i32 %1, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %level.addr, align 4, !tbaa !9
  %sub = sub nsw i32 0, %2
  %add = add nsw i32 %sub, 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add, %cond.true ], [ 1, %cond.false ]
  store i32 %cond, i32* %acceleration, align 4, !tbaa !9
  %3 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %4 = load i8*, i8** %ctx.addr, align 4, !tbaa !7
  %5 = bitcast i8* %4 to %union.LZ4_stream_u*
  %6 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %7 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %8 = load i32, i32* %srcSize.addr, align 4, !tbaa !9
  %9 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !9
  %10 = load i32, i32* %acceleration, align 4, !tbaa !9
  %call = call i32 @LZ4_compress_fast_continue(%union.LZ4_stream_u* %5, i8* %6, i8* %7, i32 %8, i32 %9, i32 %10)
  %11 = bitcast i32* %acceleration to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @LZ4F_compressBlockHC(i8* %ctx, i8* %src, i8* %dst, i32 %srcSize, i32 %dstCapacity, i32 %level, %struct.LZ4F_CDict_s* %cdict) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca i8*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %dstCapacity.addr = alloca i32, align 4
  %level.addr = alloca i32, align 4
  %cdict.addr = alloca %struct.LZ4F_CDict_s*, align 4
  store i8* %ctx, i8** %ctx.addr, align 4, !tbaa !7
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !7
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !9
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !9
  store i32 %level, i32* %level.addr, align 4, !tbaa !9
  store %struct.LZ4F_CDict_s* %cdict, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %ctx.addr, align 4, !tbaa !7
  %1 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %2 = load i32, i32* %level.addr, align 4, !tbaa !9
  call void @LZ4F_initStream(i8* %0, %struct.LZ4F_CDict_s* %1, i32 %2, i32 1)
  %3 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %tobool = icmp ne %struct.LZ4F_CDict_s* %3, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load i8*, i8** %ctx.addr, align 4, !tbaa !7
  %5 = bitcast i8* %4 to %union.LZ4_streamHC_u*
  %6 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %7 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %8 = load i32, i32* %srcSize.addr, align 4, !tbaa !9
  %9 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !9
  %call = call i32 @LZ4_compress_HC_continue(%union.LZ4_streamHC_u* %5, i8* %6, i8* %7, i32 %8, i32 %9)
  store i32 %call, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %10 = load i8*, i8** %ctx.addr, align 4, !tbaa !7
  %11 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %12 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %13 = load i32, i32* %srcSize.addr, align 4, !tbaa !9
  %14 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !9
  %15 = load i32, i32* %level.addr, align 4, !tbaa !9
  %call1 = call i32 @LZ4_compress_HC_extStateHC_fastReset(i8* %10, i8* %11, i8* %12, i32 %13, i32 %14, i32 %15)
  store i32 %call1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %16 = load i32, i32* %retval, align 4
  ret i32 %16
}

; Function Attrs: nounwind
define internal i32 @LZ4F_compressBlockHC_continue(i8* %ctx, i8* %src, i8* %dst, i32 %srcSize, i32 %dstCapacity, i32 %level, %struct.LZ4F_CDict_s* %cdict) #0 {
entry:
  %ctx.addr = alloca i8*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %dstCapacity.addr = alloca i32, align 4
  %level.addr = alloca i32, align 4
  %cdict.addr = alloca %struct.LZ4F_CDict_s*, align 4
  store i8* %ctx, i8** %ctx.addr, align 4, !tbaa !7
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !7
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !9
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !9
  store i32 %level, i32* %level.addr, align 4, !tbaa !9
  store %struct.LZ4F_CDict_s* %cdict, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %0 = load i32, i32* %level.addr, align 4, !tbaa !9
  %1 = load %struct.LZ4F_CDict_s*, %struct.LZ4F_CDict_s** %cdict.addr, align 4, !tbaa !7
  %2 = load i8*, i8** %ctx.addr, align 4, !tbaa !7
  %3 = bitcast i8* %2 to %union.LZ4_streamHC_u*
  %4 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %5 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %6 = load i32, i32* %srcSize.addr, align 4, !tbaa !9
  %7 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !9
  %call = call i32 @LZ4_compress_HC_continue(%union.LZ4_streamHC_u* %3, i8* %4, i8* %5, i32 %6, i32 %7)
  ret i32 %call
}

declare i32 @LZ4_compress_fast_continue(%union.LZ4_stream_u*, i8*, i8*, i32, i32, i32) #3

declare i32 @LZ4_compress_fast_extState_fastReset(i8*, i8*, i8*, i32, i32, i32) #3

declare i32 @LZ4_compress_HC_continue(%union.LZ4_streamHC_u*, i8*, i8*, i32, i32) #3

declare i32 @LZ4_compress_HC_extStateHC_fastReset(i8*, i8*, i8*, i32, i32, i32) #3

declare i32 @LZ4_saveDict(%union.LZ4_stream_u*, i8*, i32) #3

declare i32 @LZ4_saveDictHC(%union.LZ4_streamHC_u*, i8*, i32) #3

; Function Attrs: nounwind
define internal i64 @LZ4F_readLE64(i8* %src) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %srcPtr = alloca i8*, align 4
  %value64 = alloca i64, align 8
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  %0 = bitcast i8** %srcPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %src.addr, align 4, !tbaa !7
  store i8* %1, i8** %srcPtr, align 4, !tbaa !7
  %2 = bitcast i64* %value64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %2) #4
  %3 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 0
  %4 = load i8, i8* %arrayidx, align 1, !tbaa !11
  %conv = zext i8 %4 to i64
  store i64 %conv, i64* %value64, align 8, !tbaa !13
  %5 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %arrayidx1 = getelementptr inbounds i8, i8* %5, i32 1
  %6 = load i8, i8* %arrayidx1, align 1, !tbaa !11
  %conv2 = zext i8 %6 to i64
  %shl = shl i64 %conv2, 8
  %7 = load i64, i64* %value64, align 8, !tbaa !13
  %add = add i64 %7, %shl
  store i64 %add, i64* %value64, align 8, !tbaa !13
  %8 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %arrayidx3 = getelementptr inbounds i8, i8* %8, i32 2
  %9 = load i8, i8* %arrayidx3, align 1, !tbaa !11
  %conv4 = zext i8 %9 to i64
  %shl5 = shl i64 %conv4, 16
  %10 = load i64, i64* %value64, align 8, !tbaa !13
  %add6 = add i64 %10, %shl5
  store i64 %add6, i64* %value64, align 8, !tbaa !13
  %11 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %arrayidx7 = getelementptr inbounds i8, i8* %11, i32 3
  %12 = load i8, i8* %arrayidx7, align 1, !tbaa !11
  %conv8 = zext i8 %12 to i64
  %shl9 = shl i64 %conv8, 24
  %13 = load i64, i64* %value64, align 8, !tbaa !13
  %add10 = add i64 %13, %shl9
  store i64 %add10, i64* %value64, align 8, !tbaa !13
  %14 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %arrayidx11 = getelementptr inbounds i8, i8* %14, i32 4
  %15 = load i8, i8* %arrayidx11, align 1, !tbaa !11
  %conv12 = zext i8 %15 to i64
  %shl13 = shl i64 %conv12, 32
  %16 = load i64, i64* %value64, align 8, !tbaa !13
  %add14 = add i64 %16, %shl13
  store i64 %add14, i64* %value64, align 8, !tbaa !13
  %17 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %arrayidx15 = getelementptr inbounds i8, i8* %17, i32 5
  %18 = load i8, i8* %arrayidx15, align 1, !tbaa !11
  %conv16 = zext i8 %18 to i64
  %shl17 = shl i64 %conv16, 40
  %19 = load i64, i64* %value64, align 8, !tbaa !13
  %add18 = add i64 %19, %shl17
  store i64 %add18, i64* %value64, align 8, !tbaa !13
  %20 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %arrayidx19 = getelementptr inbounds i8, i8* %20, i32 6
  %21 = load i8, i8* %arrayidx19, align 1, !tbaa !11
  %conv20 = zext i8 %21 to i64
  %shl21 = shl i64 %conv20, 48
  %22 = load i64, i64* %value64, align 8, !tbaa !13
  %add22 = add i64 %22, %shl21
  store i64 %add22, i64* %value64, align 8, !tbaa !13
  %23 = load i8*, i8** %srcPtr, align 4, !tbaa !7
  %arrayidx23 = getelementptr inbounds i8, i8* %23, i32 7
  %24 = load i8, i8* %arrayidx23, align 1, !tbaa !11
  %conv24 = zext i8 %24 to i64
  %shl25 = shl i64 %conv24, 56
  %25 = load i64, i64* %value64, align 8, !tbaa !13
  %add26 = add i64 %25, %shl25
  store i64 %add26, i64* %value64, align 8, !tbaa !13
  %26 = load i64, i64* %value64, align 8, !tbaa !13
  %27 = bitcast i64* %value64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %27) #4
  %28 = bitcast i8** %srcPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #4
  ret i64 %26
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!3 = !{!4, !4, i64 0}
!4 = !{!"long", !5, i64 0}
!5 = !{!"omnipotent char", !6, i64 0}
!6 = !{!"Simple C/C++ TBAA"}
!7 = !{!8, !8, i64 0}
!8 = !{!"any pointer", !5, i64 0}
!9 = !{!10, !10, i64 0}
!10 = !{!"int", !5, i64 0}
!11 = !{!5, !5, i64 0}
!12 = !{i64 0, i64 4, !11, i64 4, i64 4, !11, i64 8, i64 4, !11, i64 12, i64 4, !11, i64 16, i64 8, !13, i64 24, i64 4, !9, i64 28, i64 4, !11, i64 32, i64 4, !9, i64 36, i64 4, !9, i64 40, i64 4, !9, i64 44, i64 12, !11}
!13 = !{!14, !14, i64 0}
!14 = !{!"long long", !5, i64 0}
!15 = !{!16, !10, i64 36}
!16 = !{!"", !17, i64 0, !10, i64 32, !10, i64 36, !10, i64 40, !5, i64 44}
!17 = !{!"", !5, i64 0, !5, i64 4, !5, i64 8, !5, i64 12, !14, i64 16, !10, i64 24, !5, i64 28}
!18 = !{!16, !5, i64 8}
!19 = !{!16, !5, i64 28}
!20 = !{!16, !5, i64 0}
!21 = !{!16, !14, i64 16}
!22 = !{!16, !5, i64 4}
!23 = !{!24, !10, i64 0}
!24 = !{!"", !10, i64 0, !5, i64 4}
!25 = !{!26, !10, i64 32}
!26 = !{!"LZ4F_cctx_s", !16, i64 0, !10, i64 56, !10, i64 60, !8, i64 64, !4, i64 68, !4, i64 72, !8, i64 76, !8, i64 80, !4, i64 84, !14, i64 88, !27, i64 96, !8, i64 144, !28, i64 148, !28, i64 150}
!27 = !{!"XXH32_state_s", !10, i64 0, !10, i64 4, !10, i64 8, !10, i64 12, !10, i64 16, !10, i64 20, !5, i64 24, !10, i64 40, !10, i64 44}
!28 = !{!"short", !5, i64 0}
!29 = !{!28, !28, i64 0}
!30 = !{!26, !28, i64 148}
!31 = !{!26, !8, i64 144}
!32 = !{!26, !28, i64 150}
!33 = !{!26, !5, i64 0}
!34 = !{!26, !4, i64 68}
!35 = !{!26, !5, i64 4}
!36 = !{!26, !4, i64 72}
!37 = !{!26, !8, i64 76}
!38 = !{!26, !8, i64 80}
!39 = !{!26, !4, i64 84}
!40 = !{!26, !8, i64 64}
!41 = !{!16, !10, i64 32}
!42 = !{!16, !10, i64 40}
!43 = !{!26, !5, i64 28}
!44 = !{!26, !14, i64 16}
!45 = !{!26, !5, i64 8}
!46 = !{!26, !10, i64 24}
!47 = !{!26, !14, i64 88}
!48 = !{!26, !10, i64 60}
!49 = !{!26, !10, i64 36}
!50 = !{!26, !10, i64 56}
!51 = !{!52, !8, i64 0}
!52 = !{!"LZ4F_CDict_s", !8, i64 0, !8, i64 4, !8, i64 8}
!53 = !{!52, !8, i64 4}
!54 = !{!52, !8, i64 8}
!55 = !{!56, !10, i64 32}
!56 = !{!"LZ4F_dctx_s", !17, i64 0, !10, i64 32, !5, i64 36, !14, i64 40, !4, i64 48, !4, i64 52, !8, i64 56, !4, i64 60, !4, i64 64, !8, i64 68, !8, i64 72, !4, i64 76, !8, i64 80, !4, i64 84, !4, i64 88, !27, i64 92, !27, i64 140, !5, i64 188}
!57 = !{!56, !5, i64 36}
!58 = !{!56, !8, i64 56}
!59 = !{!56, !8, i64 68}
!60 = !{!56, !8, i64 72}
!61 = !{!56, !4, i64 76}
!62 = !{i64 0, i64 4, !11, i64 4, i64 4, !11, i64 8, i64 4, !11, i64 12, i64 4, !11, i64 16, i64 8, !13, i64 24, i64 4, !9, i64 28, i64 4, !11}
!63 = !{!56, !4, i64 60}
!64 = !{!56, !4, i64 64}
!65 = !{!56, !5, i64 8}
!66 = !{!56, !4, i64 48}
!67 = !{!56, !5, i64 4}
!68 = !{!56, !4, i64 52}
!69 = !{!56, !8, i64 80}
!70 = !{!56, !4, i64 88}
!71 = !{!56, !4, i64 84}
!72 = !{!56, !5, i64 28}
!73 = !{!56, !14, i64 16}
!74 = !{!56, !14, i64 40}
!75 = !{!56, !5, i64 12}
!76 = !{!56, !5, i64 0}
!77 = !{!56, !10, i64 24}
