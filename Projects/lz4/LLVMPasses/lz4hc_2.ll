; ModuleID = 'lz4hc.c'
source_filename = "lz4hc.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.cParams_t = type { i32, i32, i32 }
%union.anon = type { i32 }
%struct.LZ4HC_CCtx_internal = type { [32768 x i32], [65536 x i16], i8*, i8*, i8*, i32, i32, i32, i16, i8, i8, %struct.LZ4HC_CCtx_internal* }
%union.LZ4_streamHC_u = type { [65550 x i32] }
%struct.LZ4HC_optimal_t = type { i32, i32, i32, i32 }
%struct.LZ4HC_match_t = type { i32, i32 }
%union.unalign = type { i32 }

@LZ4HC_compress_generic_internal.clTable = internal constant [13 x %struct.cParams_t] [%struct.cParams_t { i32 0, i32 2, i32 16 }, %struct.cParams_t { i32 0, i32 2, i32 16 }, %struct.cParams_t { i32 0, i32 2, i32 16 }, %struct.cParams_t { i32 0, i32 4, i32 16 }, %struct.cParams_t { i32 0, i32 8, i32 16 }, %struct.cParams_t { i32 0, i32 16, i32 16 }, %struct.cParams_t { i32 0, i32 32, i32 16 }, %struct.cParams_t { i32 0, i32 64, i32 16 }, %struct.cParams_t { i32 0, i32 128, i32 16 }, %struct.cParams_t { i32 0, i32 256, i32 16 }, %struct.cParams_t { i32 1, i32 96, i32 64 }, %struct.cParams_t { i32 1, i32 512, i32 128 }, %struct.cParams_t { i32 1, i32 16384, i32 4096 }], align 16
@__const.LZ4_isLittleEndian.one = private unnamed_addr constant %union.anon { i32 1 }, align 4

; Function Attrs: nounwind
define i32 @LZ4_sizeofStateHC() #0 {
entry:
  ret i32 262200
}

; Function Attrs: nounwind
define hidden i32 @LZ4_compress_HC_extStateHC_fastReset(i8* %state, i8* %src, i8* %dst, i32 %srcSize, i32 %dstCapacity, i32 %compressionLevel) #0 {
entry:
  %retval = alloca i32, align 4
  %state.addr = alloca i8*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %dstCapacity.addr = alloca i32, align 4
  %compressionLevel.addr = alloca i32, align 4
  %ctx = alloca %struct.LZ4HC_CCtx_internal*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %state, i8** %state.addr, align 4, !tbaa !3
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !7
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !7
  store i32 %compressionLevel, i32* %compressionLevel.addr, align 4, !tbaa !7
  %0 = bitcast %struct.LZ4HC_CCtx_internal** %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8*, i8** %state.addr, align 4, !tbaa !3
  %2 = bitcast i8* %1 to %union.LZ4_streamHC_u*
  %internal_donotuse = bitcast %union.LZ4_streamHC_u* %2 to %struct.LZ4HC_CCtx_internal*
  store %struct.LZ4HC_CCtx_internal* %internal_donotuse, %struct.LZ4HC_CCtx_internal** %ctx, align 4, !tbaa !3
  %3 = load i8*, i8** %state.addr, align 4, !tbaa !3
  %4 = ptrtoint i8* %3 to i32
  %and = and i32 %4, 3
  %cmp = icmp ne i32 %and, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %5 = load i8*, i8** %state.addr, align 4, !tbaa !3
  %6 = bitcast i8* %5 to %union.LZ4_streamHC_u*
  %7 = load i32, i32* %compressionLevel.addr, align 4, !tbaa !7
  call void @LZ4_resetStreamHC_fast(%union.LZ4_streamHC_u* %6, i32 %7)
  %8 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx, align 4, !tbaa !3
  %9 = load i8*, i8** %src.addr, align 4, !tbaa !3
  call void @LZ4HC_init_internal(%struct.LZ4HC_CCtx_internal* %8, i8* %9)
  %10 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %11 = load i32, i32* %srcSize.addr, align 4, !tbaa !7
  %call = call i32 @LZ4_compressBound(i32 %11)
  %cmp1 = icmp slt i32 %10, %call
  br i1 %cmp1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.end
  %12 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx, align 4, !tbaa !3
  %13 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %14 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %15 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %16 = load i32, i32* %compressionLevel.addr, align 4, !tbaa !7
  %call3 = call i32 @LZ4HC_compress_generic(%struct.LZ4HC_CCtx_internal* %12, i8* %13, i8* %14, i32* %srcSize.addr, i32 %15, i32 %16, i32 1)
  store i32 %call3, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %if.end
  %17 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx, align 4, !tbaa !3
  %18 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %19 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %20 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %21 = load i32, i32* %compressionLevel.addr, align 4, !tbaa !7
  %call4 = call i32 @LZ4HC_compress_generic(%struct.LZ4HC_CCtx_internal* %17, i8* %18, i8* %19, i32* %srcSize.addr, i32 %20, i32 %21, i32 0)
  store i32 %call4, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then2, %if.then
  %22 = bitcast %struct.LZ4HC_CCtx_internal** %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #7
  %23 = load i32, i32* %retval, align 4
  ret i32 %23
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define void @LZ4_resetStreamHC_fast(%union.LZ4_streamHC_u* %LZ4_streamHCPtr, i32 %compressionLevel) #0 {
entry:
  %LZ4_streamHCPtr.addr = alloca %union.LZ4_streamHC_u*, align 4
  %compressionLevel.addr = alloca i32, align 4
  store %union.LZ4_streamHC_u* %LZ4_streamHCPtr, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  store i32 %compressionLevel, i32* %compressionLevel.addr, align 4, !tbaa !7
  %0 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %internal_donotuse = bitcast %union.LZ4_streamHC_u* %0 to %struct.LZ4HC_CCtx_internal*
  %dirty = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %internal_donotuse, i32 0, i32 10
  %1 = load i8, i8* %dirty, align 1, !tbaa !9
  %tobool = icmp ne i8 %1, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %3 = bitcast %union.LZ4_streamHC_u* %2 to i8*
  %call = call %union.LZ4_streamHC_u* @LZ4_initStreamHC(i8* %3, i32 262200)
  br label %if.end

if.else:                                          ; preds = %entry
  %4 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %internal_donotuse1 = bitcast %union.LZ4_streamHC_u* %4 to %struct.LZ4HC_CCtx_internal*
  %base = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %internal_donotuse1, i32 0, i32 3
  %5 = load i8*, i8** %base, align 4, !tbaa !9
  %6 = ptrtoint i8* %5 to i32
  %7 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %internal_donotuse2 = bitcast %union.LZ4_streamHC_u* %7 to %struct.LZ4HC_CCtx_internal*
  %end = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %internal_donotuse2, i32 0, i32 2
  %8 = load i8*, i8** %end, align 4, !tbaa !9
  %idx.neg = sub i32 0, %6
  %add.ptr = getelementptr inbounds i8, i8* %8, i32 %idx.neg
  store i8* %add.ptr, i8** %end, align 4, !tbaa !9
  %9 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %internal_donotuse3 = bitcast %union.LZ4_streamHC_u* %9 to %struct.LZ4HC_CCtx_internal*
  %base4 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %internal_donotuse3, i32 0, i32 3
  store i8* null, i8** %base4, align 4, !tbaa !9
  %10 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %internal_donotuse5 = bitcast %union.LZ4_streamHC_u* %10 to %struct.LZ4HC_CCtx_internal*
  %dictCtx = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %internal_donotuse5, i32 0, i32 11
  store %struct.LZ4HC_CCtx_internal* null, %struct.LZ4HC_CCtx_internal** %dictCtx, align 4, !tbaa !9
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %11 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %12 = load i32, i32* %compressionLevel.addr, align 4, !tbaa !7
  call void @LZ4_setCompressionLevel(%union.LZ4_streamHC_u* %11, i32 %12)
  ret void
}

; Function Attrs: nounwind
define internal void @LZ4HC_init_internal(%struct.LZ4HC_CCtx_internal* %hc4, i8* %start) #0 {
entry:
  %hc4.addr = alloca %struct.LZ4HC_CCtx_internal*, align 4
  %start.addr = alloca i8*, align 4
  %startingOffset = alloca i32, align 4
  store %struct.LZ4HC_CCtx_internal* %hc4, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  store i8* %start, i8** %start.addr, align 4, !tbaa !3
  %0 = bitcast i32* %startingOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %end = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %1, i32 0, i32 2
  %2 = load i8*, i8** %end, align 4, !tbaa !10
  %3 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %base = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %3, i32 0, i32 3
  %4 = load i8*, i8** %base, align 4, !tbaa !13
  %sub.ptr.lhs.cast = ptrtoint i8* %2 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %4 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %startingOffset, align 4, !tbaa !14
  %5 = load i32, i32* %startingOffset, align 4, !tbaa !14
  %cmp = icmp ugt i32 %5, 1073741824
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  call void @LZ4HC_clearTables(%struct.LZ4HC_CCtx_internal* %6)
  store i32 0, i32* %startingOffset, align 4, !tbaa !14
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load i32, i32* %startingOffset, align 4, !tbaa !14
  %add = add i32 %7, 65536
  store i32 %add, i32* %startingOffset, align 4, !tbaa !14
  %8 = load i32, i32* %startingOffset, align 4, !tbaa !14
  %9 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %nextToUpdate = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %9, i32 0, i32 7
  store i32 %8, i32* %nextToUpdate, align 4, !tbaa !16
  %10 = load i8*, i8** %start.addr, align 4, !tbaa !3
  %11 = load i32, i32* %startingOffset, align 4, !tbaa !14
  %idx.neg = sub i32 0, %11
  %add.ptr = getelementptr inbounds i8, i8* %10, i32 %idx.neg
  %12 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %base1 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %12, i32 0, i32 3
  store i8* %add.ptr, i8** %base1, align 4, !tbaa !13
  %13 = load i8*, i8** %start.addr, align 4, !tbaa !3
  %14 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %end2 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %14, i32 0, i32 2
  store i8* %13, i8** %end2, align 4, !tbaa !10
  %15 = load i8*, i8** %start.addr, align 4, !tbaa !3
  %16 = load i32, i32* %startingOffset, align 4, !tbaa !14
  %idx.neg3 = sub i32 0, %16
  %add.ptr4 = getelementptr inbounds i8, i8* %15, i32 %idx.neg3
  %17 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %dictBase = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %17, i32 0, i32 4
  store i8* %add.ptr4, i8** %dictBase, align 4, !tbaa !17
  %18 = load i32, i32* %startingOffset, align 4, !tbaa !14
  %19 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %dictLimit = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %19, i32 0, i32 5
  store i32 %18, i32* %dictLimit, align 4, !tbaa !18
  %20 = load i32, i32* %startingOffset, align 4, !tbaa !14
  %21 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %lowLimit = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %21, i32 0, i32 6
  store i32 %20, i32* %lowLimit, align 4, !tbaa !19
  %22 = bitcast i32* %startingOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #7
  ret void
}

declare i32 @LZ4_compressBound(i32) #2

; Function Attrs: nounwind
define internal i32 @LZ4HC_compress_generic(%struct.LZ4HC_CCtx_internal* %ctx, i8* %src, i8* %dst, i32* %srcSizePtr, i32 %dstCapacity, i32 %cLevel, i32 %limit) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.LZ4HC_CCtx_internal*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSizePtr.addr = alloca i32*, align 4
  %dstCapacity.addr = alloca i32, align 4
  %cLevel.addr = alloca i32, align 4
  %limit.addr = alloca i32, align 4
  store %struct.LZ4HC_CCtx_internal* %ctx, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32* %srcSizePtr, i32** %srcSizePtr.addr, align 4, !tbaa !3
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !7
  store i32 %cLevel, i32* %cLevel.addr, align 4, !tbaa !7
  store i32 %limit, i32* %limit.addr, align 4, !tbaa !9
  %0 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %dictCtx = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %0, i32 0, i32 11
  %1 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %dictCtx, align 4, !tbaa !20
  %cmp = icmp eq %struct.LZ4HC_CCtx_internal* %1, null
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %3 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %4 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %5 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  %6 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %7 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  %8 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %call = call i32 @LZ4HC_compress_generic_noDictCtx(%struct.LZ4HC_CCtx_internal* %2, i8* %3, i8* %4, i32* %5, i32 %6, i32 %7, i32 %8)
  store i32 %call, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %9 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %10 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %11 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %12 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  %13 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %14 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  %15 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %call1 = call i32 @LZ4HC_compress_generic_dictCtx(%struct.LZ4HC_CCtx_internal* %9, i8* %10, i8* %11, i32* %12, i32 %13, i32 %14, i32 %15)
  store i32 %call1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.else, %if.then
  %16 = load i32, i32* %retval, align 4
  ret i32 %16
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define i32 @LZ4_compress_HC_extStateHC(i8* %state, i8* %src, i8* %dst, i32 %srcSize, i32 %dstCapacity, i32 %compressionLevel) #0 {
entry:
  %retval = alloca i32, align 4
  %state.addr = alloca i8*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %dstCapacity.addr = alloca i32, align 4
  %compressionLevel.addr = alloca i32, align 4
  %ctx = alloca %union.LZ4_streamHC_u*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %state, i8** %state.addr, align 4, !tbaa !3
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !7
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !7
  store i32 %compressionLevel, i32* %compressionLevel.addr, align 4, !tbaa !7
  %0 = bitcast %union.LZ4_streamHC_u** %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8*, i8** %state.addr, align 4, !tbaa !3
  %call = call %union.LZ4_streamHC_u* @LZ4_initStreamHC(i8* %1, i32 262200)
  store %union.LZ4_streamHC_u* %call, %union.LZ4_streamHC_u** %ctx, align 4, !tbaa !3
  %2 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %ctx, align 4, !tbaa !3
  %cmp = icmp eq %union.LZ4_streamHC_u* %2, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %3 = load i8*, i8** %state.addr, align 4, !tbaa !3
  %4 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %5 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %6 = load i32, i32* %srcSize.addr, align 4, !tbaa !7
  %7 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %8 = load i32, i32* %compressionLevel.addr, align 4, !tbaa !7
  %call1 = call i32 @LZ4_compress_HC_extStateHC_fastReset(i8* %3, i8* %4, i8* %5, i32 %6, i32 %7, i32 %8)
  store i32 %call1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %9 = bitcast %union.LZ4_streamHC_u** %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #7
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: nounwind
define %union.LZ4_streamHC_u* @LZ4_initStreamHC(i8* %buffer, i32 %size) #0 {
entry:
  %retval = alloca %union.LZ4_streamHC_u*, align 4
  %buffer.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %LZ4_streamHCPtr = alloca %union.LZ4_streamHC_u*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %buffer, i8** %buffer.addr, align 4, !tbaa !3
  store i32 %size, i32* %size.addr, align 4, !tbaa !14
  %0 = bitcast %union.LZ4_streamHC_u** %LZ4_streamHCPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8*, i8** %buffer.addr, align 4, !tbaa !3
  %2 = bitcast i8* %1 to %union.LZ4_streamHC_u*
  store %union.LZ4_streamHC_u* %2, %union.LZ4_streamHC_u** %LZ4_streamHCPtr, align 4, !tbaa !3
  %3 = load i8*, i8** %buffer.addr, align 4, !tbaa !3
  %cmp = icmp eq i8* %3, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %union.LZ4_streamHC_u* null, %union.LZ4_streamHC_u** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %4 = load i32, i32* %size.addr, align 4, !tbaa !14
  %cmp1 = icmp ult i32 %4, 262200
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store %union.LZ4_streamHC_u* null, %union.LZ4_streamHC_u** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %if.end
  %5 = load i8*, i8** %buffer.addr, align 4, !tbaa !3
  %6 = ptrtoint i8* %5 to i32
  %call = call i32 @LZ4_streamHC_t_alignment()
  %sub = sub i32 %call, 1
  %and = and i32 %6, %sub
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end3
  store %union.LZ4_streamHC_u* null, %union.LZ4_streamHC_u** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %if.end3
  %7 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr, align 4, !tbaa !3
  %internal_donotuse = bitcast %union.LZ4_streamHC_u* %7 to %struct.LZ4HC_CCtx_internal*
  %end = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %internal_donotuse, i32 0, i32 2
  store i8* inttoptr (i32 -1 to i8*), i8** %end, align 4, !tbaa !9
  %8 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr, align 4, !tbaa !3
  %internal_donotuse6 = bitcast %union.LZ4_streamHC_u* %8 to %struct.LZ4HC_CCtx_internal*
  %base = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %internal_donotuse6, i32 0, i32 3
  store i8* null, i8** %base, align 4, !tbaa !9
  %9 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr, align 4, !tbaa !3
  %internal_donotuse7 = bitcast %union.LZ4_streamHC_u* %9 to %struct.LZ4HC_CCtx_internal*
  %dictCtx = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %internal_donotuse7, i32 0, i32 11
  store %struct.LZ4HC_CCtx_internal* null, %struct.LZ4HC_CCtx_internal** %dictCtx, align 4, !tbaa !9
  %10 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr, align 4, !tbaa !3
  %internal_donotuse8 = bitcast %union.LZ4_streamHC_u* %10 to %struct.LZ4HC_CCtx_internal*
  %favorDecSpeed = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %internal_donotuse8, i32 0, i32 9
  store i8 0, i8* %favorDecSpeed, align 2, !tbaa !9
  %11 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr, align 4, !tbaa !3
  %internal_donotuse9 = bitcast %union.LZ4_streamHC_u* %11 to %struct.LZ4HC_CCtx_internal*
  %dirty = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %internal_donotuse9, i32 0, i32 10
  store i8 0, i8* %dirty, align 1, !tbaa !9
  %12 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr, align 4, !tbaa !3
  call void @LZ4_setCompressionLevel(%union.LZ4_streamHC_u* %12, i32 9)
  %13 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr, align 4, !tbaa !3
  store %union.LZ4_streamHC_u* %13, %union.LZ4_streamHC_u** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end5, %if.then4, %if.then2, %if.then
  %14 = bitcast %union.LZ4_streamHC_u** %LZ4_streamHCPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #7
  %15 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %retval, align 4
  ret %union.LZ4_streamHC_u* %15
}

; Function Attrs: nounwind
define i32 @LZ4_compress_HC(i8* %src, i8* %dst, i32 %srcSize, i32 %dstCapacity, i32 %compressionLevel) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %dstCapacity.addr = alloca i32, align 4
  %compressionLevel.addr = alloca i32, align 4
  %statePtr = alloca %union.LZ4_streamHC_u*, align 4
  %cSize = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !7
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !7
  store i32 %compressionLevel, i32* %compressionLevel.addr, align 4, !tbaa !7
  %0 = bitcast %union.LZ4_streamHC_u** %statePtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i8* @malloc(i32 262200)
  %1 = bitcast i8* %call to %union.LZ4_streamHC_u*
  store %union.LZ4_streamHC_u* %1, %union.LZ4_streamHC_u** %statePtr, align 4, !tbaa !3
  %2 = bitcast i32* %cSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %statePtr, align 4, !tbaa !3
  %4 = bitcast %union.LZ4_streamHC_u* %3 to i8*
  %5 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %6 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %7 = load i32, i32* %srcSize.addr, align 4, !tbaa !7
  %8 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %9 = load i32, i32* %compressionLevel.addr, align 4, !tbaa !7
  %call1 = call i32 @LZ4_compress_HC_extStateHC(i8* %4, i8* %5, i8* %6, i32 %7, i32 %8, i32 %9)
  store i32 %call1, i32* %cSize, align 4, !tbaa !7
  %10 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %statePtr, align 4, !tbaa !3
  %11 = bitcast %union.LZ4_streamHC_u* %10 to i8*
  call void @free(i8* %11)
  %12 = load i32, i32* %cSize, align 4, !tbaa !7
  %13 = bitcast i32* %cSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  %14 = bitcast %union.LZ4_streamHC_u** %statePtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #7
  ret i32 %12
}

declare i8* @malloc(i32) #2

declare void @free(i8*) #2

; Function Attrs: nounwind
define i32 @LZ4_compress_HC_destSize(i8* %state, i8* %source, i8* %dest, i32* %sourceSizePtr, i32 %targetDestSize, i32 %cLevel) #0 {
entry:
  %retval = alloca i32, align 4
  %state.addr = alloca i8*, align 4
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %sourceSizePtr.addr = alloca i32*, align 4
  %targetDestSize.addr = alloca i32, align 4
  %cLevel.addr = alloca i32, align 4
  %ctx = alloca %union.LZ4_streamHC_u*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %state, i8** %state.addr, align 4, !tbaa !3
  store i8* %source, i8** %source.addr, align 4, !tbaa !3
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !3
  store i32* %sourceSizePtr, i32** %sourceSizePtr.addr, align 4, !tbaa !3
  store i32 %targetDestSize, i32* %targetDestSize.addr, align 4, !tbaa !7
  store i32 %cLevel, i32* %cLevel.addr, align 4, !tbaa !7
  %0 = bitcast %union.LZ4_streamHC_u** %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8*, i8** %state.addr, align 4, !tbaa !3
  %call = call %union.LZ4_streamHC_u* @LZ4_initStreamHC(i8* %1, i32 262200)
  store %union.LZ4_streamHC_u* %call, %union.LZ4_streamHC_u** %ctx, align 4, !tbaa !3
  %2 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %ctx, align 4, !tbaa !3
  %cmp = icmp eq %union.LZ4_streamHC_u* %2, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %3 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %ctx, align 4, !tbaa !3
  %internal_donotuse = bitcast %union.LZ4_streamHC_u* %3 to %struct.LZ4HC_CCtx_internal*
  %4 = load i8*, i8** %source.addr, align 4, !tbaa !3
  call void @LZ4HC_init_internal(%struct.LZ4HC_CCtx_internal* %internal_donotuse, i8* %4)
  %5 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %ctx, align 4, !tbaa !3
  %6 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  call void @LZ4_setCompressionLevel(%union.LZ4_streamHC_u* %5, i32 %6)
  %7 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %ctx, align 4, !tbaa !3
  %internal_donotuse1 = bitcast %union.LZ4_streamHC_u* %7 to %struct.LZ4HC_CCtx_internal*
  %8 = load i8*, i8** %source.addr, align 4, !tbaa !3
  %9 = load i8*, i8** %dest.addr, align 4, !tbaa !3
  %10 = load i32*, i32** %sourceSizePtr.addr, align 4, !tbaa !3
  %11 = load i32, i32* %targetDestSize.addr, align 4, !tbaa !7
  %12 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  %call2 = call i32 @LZ4HC_compress_generic(%struct.LZ4HC_CCtx_internal* %internal_donotuse1, i8* %8, i8* %9, i32* %10, i32 %11, i32 %12, i32 2)
  store i32 %call2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %13 = bitcast %union.LZ4_streamHC_u** %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  %14 = load i32, i32* %retval, align 4
  ret i32 %14
}

; Function Attrs: nounwind
define hidden void @LZ4_setCompressionLevel(%union.LZ4_streamHC_u* %LZ4_streamHCPtr, i32 %compressionLevel) #0 {
entry:
  %LZ4_streamHCPtr.addr = alloca %union.LZ4_streamHC_u*, align 4
  %compressionLevel.addr = alloca i32, align 4
  store %union.LZ4_streamHC_u* %LZ4_streamHCPtr, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  store i32 %compressionLevel, i32* %compressionLevel.addr, align 4, !tbaa !7
  %0 = load i32, i32* %compressionLevel.addr, align 4, !tbaa !7
  %cmp = icmp slt i32 %0, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 9, i32* %compressionLevel.addr, align 4, !tbaa !7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load i32, i32* %compressionLevel.addr, align 4, !tbaa !7
  %cmp1 = icmp sgt i32 %1, 12
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 12, i32* %compressionLevel.addr, align 4, !tbaa !7
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %2 = load i32, i32* %compressionLevel.addr, align 4, !tbaa !7
  %conv = trunc i32 %2 to i16
  %3 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %internal_donotuse = bitcast %union.LZ4_streamHC_u* %3 to %struct.LZ4HC_CCtx_internal*
  %compressionLevel4 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %internal_donotuse, i32 0, i32 8
  store i16 %conv, i16* %compressionLevel4, align 4, !tbaa !9
  ret void
}

; Function Attrs: nounwind
define %union.LZ4_streamHC_u* @LZ4_createStreamHC() #0 {
entry:
  %retval = alloca %union.LZ4_streamHC_u*, align 4
  %LZ4_streamHCPtr = alloca %union.LZ4_streamHC_u*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %0 = bitcast %union.LZ4_streamHC_u** %LZ4_streamHCPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i8* @malloc(i32 262200)
  %1 = bitcast i8* %call to %union.LZ4_streamHC_u*
  store %union.LZ4_streamHC_u* %1, %union.LZ4_streamHC_u** %LZ4_streamHCPtr, align 4, !tbaa !3
  %2 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr, align 4, !tbaa !3
  %cmp = icmp eq %union.LZ4_streamHC_u* %2, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %union.LZ4_streamHC_u* null, %union.LZ4_streamHC_u** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %3 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr, align 4, !tbaa !3
  %4 = bitcast %union.LZ4_streamHC_u* %3 to i8*
  %call1 = call %union.LZ4_streamHC_u* @LZ4_initStreamHC(i8* %4, i32 262200)
  %5 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr, align 4, !tbaa !3
  store %union.LZ4_streamHC_u* %5, %union.LZ4_streamHC_u** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %6 = bitcast %union.LZ4_streamHC_u** %LZ4_streamHCPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #7
  %7 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %retval, align 4
  ret %union.LZ4_streamHC_u* %7
}

; Function Attrs: nounwind
define i32 @LZ4_freeStreamHC(%union.LZ4_streamHC_u* %LZ4_streamHCPtr) #0 {
entry:
  %retval = alloca i32, align 4
  %LZ4_streamHCPtr.addr = alloca %union.LZ4_streamHC_u*, align 4
  store %union.LZ4_streamHC_u* %LZ4_streamHCPtr, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %0 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %tobool = icmp ne %union.LZ4_streamHC_u* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %2 = bitcast %union.LZ4_streamHC_u* %1 to i8*
  call void @free(i8* %2)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: nounwind
define internal i32 @LZ4_streamHC_t_alignment() #0 {
entry:
  ret i32 4
}

; Function Attrs: nounwind
define void @LZ4_resetStreamHC(%union.LZ4_streamHC_u* %LZ4_streamHCPtr, i32 %compressionLevel) #0 {
entry:
  %LZ4_streamHCPtr.addr = alloca %union.LZ4_streamHC_u*, align 4
  %compressionLevel.addr = alloca i32, align 4
  store %union.LZ4_streamHC_u* %LZ4_streamHCPtr, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  store i32 %compressionLevel, i32* %compressionLevel.addr, align 4, !tbaa !7
  %0 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %1 = bitcast %union.LZ4_streamHC_u* %0 to i8*
  %call = call %union.LZ4_streamHC_u* @LZ4_initStreamHC(i8* %1, i32 262200)
  %2 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %3 = load i32, i32* %compressionLevel.addr, align 4, !tbaa !7
  call void @LZ4_setCompressionLevel(%union.LZ4_streamHC_u* %2, i32 %3)
  ret void
}

; Function Attrs: nounwind
define hidden void @LZ4_favorDecompressionSpeed(%union.LZ4_streamHC_u* %LZ4_streamHCPtr, i32 %favor) #0 {
entry:
  %LZ4_streamHCPtr.addr = alloca %union.LZ4_streamHC_u*, align 4
  %favor.addr = alloca i32, align 4
  store %union.LZ4_streamHC_u* %LZ4_streamHCPtr, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  store i32 %favor, i32* %favor.addr, align 4, !tbaa !7
  %0 = load i32, i32* %favor.addr, align 4, !tbaa !7
  %cmp = icmp ne i32 %0, 0
  %conv = zext i1 %cmp to i32
  %conv1 = trunc i32 %conv to i8
  %1 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %internal_donotuse = bitcast %union.LZ4_streamHC_u* %1 to %struct.LZ4HC_CCtx_internal*
  %favorDecSpeed = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %internal_donotuse, i32 0, i32 9
  store i8 %conv1, i8* %favorDecSpeed, align 2, !tbaa !9
  ret void
}

; Function Attrs: nounwind
define i32 @LZ4_loadDictHC(%union.LZ4_streamHC_u* %LZ4_streamHCPtr, i8* %dictionary, i32 %dictSize) #0 {
entry:
  %LZ4_streamHCPtr.addr = alloca %union.LZ4_streamHC_u*, align 4
  %dictionary.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  %ctxPtr = alloca %struct.LZ4HC_CCtx_internal*, align 4
  %cLevel = alloca i32, align 4
  store %union.LZ4_streamHC_u* %LZ4_streamHCPtr, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  store i8* %dictionary, i8** %dictionary.addr, align 4, !tbaa !3
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !7
  %0 = bitcast %struct.LZ4HC_CCtx_internal** %ctxPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %internal_donotuse = bitcast %union.LZ4_streamHC_u* %1 to %struct.LZ4HC_CCtx_internal*
  store %struct.LZ4HC_CCtx_internal* %internal_donotuse, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %2 = load i32, i32* %dictSize.addr, align 4, !tbaa !7
  %cmp = icmp sgt i32 %2, 65536
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i32, i32* %dictSize.addr, align 4, !tbaa !7
  %sub = sub i32 %3, 65536
  %4 = load i8*, i8** %dictionary.addr, align 4, !tbaa !3
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %sub
  store i8* %add.ptr, i8** %dictionary.addr, align 4, !tbaa !3
  store i32 65536, i32* %dictSize.addr, align 4, !tbaa !7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = bitcast i32* %cLevel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %compressionLevel = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %6, i32 0, i32 8
  %7 = load i16, i16* %compressionLevel, align 4, !tbaa !21
  %conv = sext i16 %7 to i32
  store i32 %conv, i32* %cLevel, align 4, !tbaa !7
  %8 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %9 = bitcast %union.LZ4_streamHC_u* %8 to i8*
  %call = call %union.LZ4_streamHC_u* @LZ4_initStreamHC(i8* %9, i32 262200)
  %10 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %11 = load i32, i32* %cLevel, align 4, !tbaa !7
  call void @LZ4_setCompressionLevel(%union.LZ4_streamHC_u* %10, i32 %11)
  %12 = bitcast i32* %cLevel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  %13 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %14 = load i8*, i8** %dictionary.addr, align 4, !tbaa !3
  call void @LZ4HC_init_internal(%struct.LZ4HC_CCtx_internal* %13, i8* %14)
  %15 = load i8*, i8** %dictionary.addr, align 4, !tbaa !3
  %16 = load i32, i32* %dictSize.addr, align 4, !tbaa !7
  %add.ptr1 = getelementptr inbounds i8, i8* %15, i32 %16
  %17 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %end = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %17, i32 0, i32 2
  store i8* %add.ptr1, i8** %end, align 4, !tbaa !10
  %18 = load i32, i32* %dictSize.addr, align 4, !tbaa !7
  %cmp2 = icmp sge i32 %18, 4
  br i1 %cmp2, label %if.then4, label %if.end7

if.then4:                                         ; preds = %if.end
  %19 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %20 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %end5 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %20, i32 0, i32 2
  %21 = load i8*, i8** %end5, align 4, !tbaa !10
  %add.ptr6 = getelementptr inbounds i8, i8* %21, i32 -3
  call void @LZ4HC_Insert(%struct.LZ4HC_CCtx_internal* %19, i8* %add.ptr6)
  br label %if.end7

if.end7:                                          ; preds = %if.then4, %if.end
  %22 = load i32, i32* %dictSize.addr, align 4, !tbaa !7
  %23 = bitcast %struct.LZ4HC_CCtx_internal** %ctxPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #7
  ret i32 %22
}

; Function Attrs: alwaysinline nounwind
define internal void @LZ4HC_Insert(%struct.LZ4HC_CCtx_internal* %hc4, i8* %ip) #3 {
entry:
  %hc4.addr = alloca %struct.LZ4HC_CCtx_internal*, align 4
  %ip.addr = alloca i8*, align 4
  %chainTable = alloca i16*, align 4
  %hashTable = alloca i32*, align 4
  %base = alloca i8*, align 4
  %target = alloca i32, align 4
  %idx = alloca i32, align 4
  %h = alloca i32, align 4
  %delta = alloca i32, align 4
  store %struct.LZ4HC_CCtx_internal* %hc4, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  store i8* %ip, i8** %ip.addr, align 4, !tbaa !3
  %0 = bitcast i16** %chainTable to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %chainTable1 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %1, i32 0, i32 1
  %arraydecay = getelementptr inbounds [65536 x i16], [65536 x i16]* %chainTable1, i32 0, i32 0
  store i16* %arraydecay, i16** %chainTable, align 4, !tbaa !3
  %2 = bitcast i32** %hashTable to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %hashTable2 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %3, i32 0, i32 0
  %arraydecay3 = getelementptr inbounds [32768 x i32], [32768 x i32]* %hashTable2, i32 0, i32 0
  store i32* %arraydecay3, i32** %hashTable, align 4, !tbaa !3
  %4 = bitcast i8** %base to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %base4 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %5, i32 0, i32 3
  %6 = load i8*, i8** %base4, align 4, !tbaa !13
  store i8* %6, i8** %base, align 4, !tbaa !3
  %7 = bitcast i32* %target to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %9 = load i8*, i8** %base, align 4, !tbaa !3
  %sub.ptr.lhs.cast = ptrtoint i8* %8 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %9 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %target, align 4, !tbaa !7
  %10 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %nextToUpdate = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %11, i32 0, i32 7
  %12 = load i32, i32* %nextToUpdate, align 4, !tbaa !16
  store i32 %12, i32* %idx, align 4, !tbaa !7
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %13 = load i32, i32* %idx, align 4, !tbaa !7
  %14 = load i32, i32* %target, align 4, !tbaa !7
  %cmp = icmp ult i32 %13, %14
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %15 = bitcast i32* %h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  %16 = load i8*, i8** %base, align 4, !tbaa !3
  %17 = load i32, i32* %idx, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds i8, i8* %16, i32 %17
  %call = call i32 @LZ4HC_hashPtr(i8* %add.ptr)
  store i32 %call, i32* %h, align 4, !tbaa !7
  %18 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #7
  %19 = load i32, i32* %idx, align 4, !tbaa !7
  %20 = load i32*, i32** %hashTable, align 4, !tbaa !3
  %21 = load i32, i32* %h, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i32, i32* %20, i32 %21
  %22 = load i32, i32* %arrayidx, align 4, !tbaa !7
  %sub = sub i32 %19, %22
  store i32 %sub, i32* %delta, align 4, !tbaa !14
  %23 = load i32, i32* %delta, align 4, !tbaa !14
  %cmp5 = icmp ugt i32 %23, 65535
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  store i32 65535, i32* %delta, align 4, !tbaa !14
  br label %if.end

if.end:                                           ; preds = %if.then, %while.body
  %24 = load i32, i32* %delta, align 4, !tbaa !14
  %conv = trunc i32 %24 to i16
  %25 = load i16*, i16** %chainTable, align 4, !tbaa !3
  %26 = load i32, i32* %idx, align 4, !tbaa !7
  %conv6 = trunc i32 %26 to i16
  %idxprom = zext i16 %conv6 to i32
  %arrayidx7 = getelementptr inbounds i16, i16* %25, i32 %idxprom
  store i16 %conv, i16* %arrayidx7, align 2, !tbaa !22
  %27 = load i32, i32* %idx, align 4, !tbaa !7
  %28 = load i32*, i32** %hashTable, align 4, !tbaa !3
  %29 = load i32, i32* %h, align 4, !tbaa !7
  %arrayidx8 = getelementptr inbounds i32, i32* %28, i32 %29
  store i32 %27, i32* %arrayidx8, align 4, !tbaa !7
  %30 = load i32, i32* %idx, align 4, !tbaa !7
  %inc = add i32 %30, 1
  store i32 %inc, i32* %idx, align 4, !tbaa !7
  %31 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #7
  %32 = bitcast i32* %h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #7
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %33 = load i32, i32* %target, align 4, !tbaa !7
  %34 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %nextToUpdate9 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %34, i32 0, i32 7
  store i32 %33, i32* %nextToUpdate9, align 4, !tbaa !16
  %35 = bitcast i32* %idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #7
  %36 = bitcast i32* %target to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #7
  %37 = bitcast i8** %base to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #7
  %38 = bitcast i32** %hashTable to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #7
  %39 = bitcast i16** %chainTable to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #7
  ret void
}

; Function Attrs: nounwind
define hidden void @LZ4_attach_HC_dictionary(%union.LZ4_streamHC_u* %working_stream, %union.LZ4_streamHC_u* %dictionary_stream) #0 {
entry:
  %working_stream.addr = alloca %union.LZ4_streamHC_u*, align 4
  %dictionary_stream.addr = alloca %union.LZ4_streamHC_u*, align 4
  store %union.LZ4_streamHC_u* %working_stream, %union.LZ4_streamHC_u** %working_stream.addr, align 4, !tbaa !3
  store %union.LZ4_streamHC_u* %dictionary_stream, %union.LZ4_streamHC_u** %dictionary_stream.addr, align 4, !tbaa !3
  %0 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %dictionary_stream.addr, align 4, !tbaa !3
  %cmp = icmp ne %union.LZ4_streamHC_u* %0, null
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %dictionary_stream.addr, align 4, !tbaa !3
  %internal_donotuse = bitcast %union.LZ4_streamHC_u* %1 to %struct.LZ4HC_CCtx_internal*
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.LZ4HC_CCtx_internal* [ %internal_donotuse, %cond.true ], [ null, %cond.false ]
  %2 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %working_stream.addr, align 4, !tbaa !3
  %internal_donotuse1 = bitcast %union.LZ4_streamHC_u* %2 to %struct.LZ4HC_CCtx_internal*
  %dictCtx = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %internal_donotuse1, i32 0, i32 11
  store %struct.LZ4HC_CCtx_internal* %cond, %struct.LZ4HC_CCtx_internal** %dictCtx, align 4, !tbaa !9
  ret void
}

; Function Attrs: nounwind
define i32 @LZ4_compress_HC_continue(%union.LZ4_streamHC_u* %LZ4_streamHCPtr, i8* %src, i8* %dst, i32 %srcSize, i32 %dstCapacity) #0 {
entry:
  %retval = alloca i32, align 4
  %LZ4_streamHCPtr.addr = alloca %union.LZ4_streamHC_u*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %dstCapacity.addr = alloca i32, align 4
  store %union.LZ4_streamHC_u* %LZ4_streamHCPtr, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !7
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !7
  %0 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %1 = load i32, i32* %srcSize.addr, align 4, !tbaa !7
  %call = call i32 @LZ4_compressBound(i32 %1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %3 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %4 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %5 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %call1 = call i32 @LZ4_compressHC_continue_generic(%union.LZ4_streamHC_u* %2, i8* %3, i8* %4, i32* %srcSize.addr, i32 %5, i32 1)
  store i32 %call1, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %6 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %7 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %8 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %9 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %call2 = call i32 @LZ4_compressHC_continue_generic(%union.LZ4_streamHC_u* %6, i8* %7, i8* %8, i32* %srcSize.addr, i32 %9, i32 0)
  store i32 %call2, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.else, %if.then
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: nounwind
define internal i32 @LZ4_compressHC_continue_generic(%union.LZ4_streamHC_u* %LZ4_streamHCPtr, i8* %src, i8* %dst, i32* %srcSizePtr, i32 %dstCapacity, i32 %limit) #0 {
entry:
  %LZ4_streamHCPtr.addr = alloca %union.LZ4_streamHC_u*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSizePtr.addr = alloca i32*, align 4
  %dstCapacity.addr = alloca i32, align 4
  %limit.addr = alloca i32, align 4
  %ctxPtr = alloca %struct.LZ4HC_CCtx_internal*, align 4
  %dictSize = alloca i32, align 4
  %sourceEnd = alloca i8*, align 4
  %dictBegin = alloca i8*, align 4
  %dictEnd = alloca i8*, align 4
  store %union.LZ4_streamHC_u* %LZ4_streamHCPtr, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32* %srcSizePtr, i32** %srcSizePtr.addr, align 4, !tbaa !3
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !7
  store i32 %limit, i32* %limit.addr, align 4, !tbaa !9
  %0 = bitcast %struct.LZ4HC_CCtx_internal** %ctxPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %internal_donotuse = bitcast %union.LZ4_streamHC_u* %1 to %struct.LZ4HC_CCtx_internal*
  store %struct.LZ4HC_CCtx_internal* %internal_donotuse, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %2 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %base = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %2, i32 0, i32 3
  %3 = load i8*, i8** %base, align 4, !tbaa !13
  %cmp = icmp eq i8* %3, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %5 = load i8*, i8** %src.addr, align 4, !tbaa !3
  call void @LZ4HC_init_internal(%struct.LZ4HC_CCtx_internal* %4, i8* %5)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %end = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %6, i32 0, i32 2
  %7 = load i8*, i8** %end, align 4, !tbaa !10
  %8 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %base1 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %8, i32 0, i32 3
  %9 = load i8*, i8** %base1, align 4, !tbaa !13
  %sub.ptr.lhs.cast = ptrtoint i8* %7 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %9 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %cmp2 = icmp ugt i32 %sub.ptr.sub, -2147483648
  br i1 %cmp2, label %if.then3, label %if.end13

if.then3:                                         ; preds = %if.end
  %10 = bitcast i32* %dictSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %end4 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %11, i32 0, i32 2
  %12 = load i8*, i8** %end4, align 4, !tbaa !10
  %13 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %base5 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %13, i32 0, i32 3
  %14 = load i8*, i8** %base5, align 4, !tbaa !13
  %sub.ptr.lhs.cast6 = ptrtoint i8* %12 to i32
  %sub.ptr.rhs.cast7 = ptrtoint i8* %14 to i32
  %sub.ptr.sub8 = sub i32 %sub.ptr.lhs.cast6, %sub.ptr.rhs.cast7
  %15 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %dictLimit = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %15, i32 0, i32 5
  %16 = load i32, i32* %dictLimit, align 4, !tbaa !18
  %sub = sub i32 %sub.ptr.sub8, %16
  store i32 %sub, i32* %dictSize, align 4, !tbaa !14
  %17 = load i32, i32* %dictSize, align 4, !tbaa !14
  %cmp9 = icmp ugt i32 %17, 65536
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.then3
  store i32 65536, i32* %dictSize, align 4, !tbaa !14
  br label %if.end11

if.end11:                                         ; preds = %if.then10, %if.then3
  %18 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %19 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %end12 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %19, i32 0, i32 2
  %20 = load i8*, i8** %end12, align 4, !tbaa !10
  %21 = load i32, i32* %dictSize, align 4, !tbaa !14
  %idx.neg = sub i32 0, %21
  %add.ptr = getelementptr inbounds i8, i8* %20, i32 %idx.neg
  %22 = load i32, i32* %dictSize, align 4, !tbaa !14
  %call = call i32 @LZ4_loadDictHC(%union.LZ4_streamHC_u* %18, i8* %add.ptr, i32 %22)
  %23 = bitcast i32* %dictSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #7
  br label %if.end13

if.end13:                                         ; preds = %if.end11, %if.end
  %24 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %25 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %end14 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %25, i32 0, i32 2
  %26 = load i8*, i8** %end14, align 4, !tbaa !10
  %cmp15 = icmp ne i8* %24, %26
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end13
  %27 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %28 = load i8*, i8** %src.addr, align 4, !tbaa !3
  call void @LZ4HC_setExternalDict(%struct.LZ4HC_CCtx_internal* %27, i8* %28)
  br label %if.end17

if.end17:                                         ; preds = %if.then16, %if.end13
  %29 = bitcast i8** %sourceEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #7
  %30 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %31 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  %32 = load i32, i32* %31, align 4, !tbaa !7
  %add.ptr18 = getelementptr inbounds i8, i8* %30, i32 %32
  store i8* %add.ptr18, i8** %sourceEnd, align 4, !tbaa !3
  %33 = bitcast i8** %dictBegin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #7
  %34 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %dictBase = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %34, i32 0, i32 4
  %35 = load i8*, i8** %dictBase, align 4, !tbaa !17
  %36 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %lowLimit = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %36, i32 0, i32 6
  %37 = load i32, i32* %lowLimit, align 4, !tbaa !19
  %add.ptr19 = getelementptr inbounds i8, i8* %35, i32 %37
  store i8* %add.ptr19, i8** %dictBegin, align 4, !tbaa !3
  %38 = bitcast i8** %dictEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #7
  %39 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %dictBase20 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %39, i32 0, i32 4
  %40 = load i8*, i8** %dictBase20, align 4, !tbaa !17
  %41 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %dictLimit21 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %41, i32 0, i32 5
  %42 = load i32, i32* %dictLimit21, align 4, !tbaa !18
  %add.ptr22 = getelementptr inbounds i8, i8* %40, i32 %42
  store i8* %add.ptr22, i8** %dictEnd, align 4, !tbaa !3
  %43 = load i8*, i8** %sourceEnd, align 4, !tbaa !3
  %44 = load i8*, i8** %dictBegin, align 4, !tbaa !3
  %cmp23 = icmp ugt i8* %43, %44
  br i1 %cmp23, label %land.lhs.true, label %if.end42

land.lhs.true:                                    ; preds = %if.end17
  %45 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %46 = load i8*, i8** %dictEnd, align 4, !tbaa !3
  %cmp24 = icmp ult i8* %45, %46
  br i1 %cmp24, label %if.then25, label %if.end42

if.then25:                                        ; preds = %land.lhs.true
  %47 = load i8*, i8** %sourceEnd, align 4, !tbaa !3
  %48 = load i8*, i8** %dictEnd, align 4, !tbaa !3
  %cmp26 = icmp ugt i8* %47, %48
  br i1 %cmp26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %if.then25
  %49 = load i8*, i8** %dictEnd, align 4, !tbaa !3
  store i8* %49, i8** %sourceEnd, align 4, !tbaa !3
  br label %if.end28

if.end28:                                         ; preds = %if.then27, %if.then25
  %50 = load i8*, i8** %sourceEnd, align 4, !tbaa !3
  %51 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %dictBase29 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %51, i32 0, i32 4
  %52 = load i8*, i8** %dictBase29, align 4, !tbaa !17
  %sub.ptr.lhs.cast30 = ptrtoint i8* %50 to i32
  %sub.ptr.rhs.cast31 = ptrtoint i8* %52 to i32
  %sub.ptr.sub32 = sub i32 %sub.ptr.lhs.cast30, %sub.ptr.rhs.cast31
  %53 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %lowLimit33 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %53, i32 0, i32 6
  store i32 %sub.ptr.sub32, i32* %lowLimit33, align 4, !tbaa !19
  %54 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %dictLimit34 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %54, i32 0, i32 5
  %55 = load i32, i32* %dictLimit34, align 4, !tbaa !18
  %56 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %lowLimit35 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %56, i32 0, i32 6
  %57 = load i32, i32* %lowLimit35, align 4, !tbaa !19
  %sub36 = sub i32 %55, %57
  %cmp37 = icmp ult i32 %sub36, 4
  br i1 %cmp37, label %if.then38, label %if.end41

if.then38:                                        ; preds = %if.end28
  %58 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %dictLimit39 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %58, i32 0, i32 5
  %59 = load i32, i32* %dictLimit39, align 4, !tbaa !18
  %60 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %lowLimit40 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %60, i32 0, i32 6
  store i32 %59, i32* %lowLimit40, align 4, !tbaa !19
  br label %if.end41

if.end41:                                         ; preds = %if.then38, %if.end28
  br label %if.end42

if.end42:                                         ; preds = %if.end41, %land.lhs.true, %if.end17
  %61 = bitcast i8** %dictEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #7
  %62 = bitcast i8** %dictBegin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #7
  %63 = bitcast i8** %sourceEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #7
  %64 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %65 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %66 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %67 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  %68 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %69 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr, align 4, !tbaa !3
  %compressionLevel = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %69, i32 0, i32 8
  %70 = load i16, i16* %compressionLevel, align 4, !tbaa !21
  %conv = sext i16 %70 to i32
  %71 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %call43 = call i32 @LZ4HC_compress_generic(%struct.LZ4HC_CCtx_internal* %64, i8* %65, i8* %66, i32* %67, i32 %68, i32 %conv, i32 %71)
  %72 = bitcast %struct.LZ4HC_CCtx_internal** %ctxPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #7
  ret i32 %call43
}

; Function Attrs: nounwind
define i32 @LZ4_compress_HC_continue_destSize(%union.LZ4_streamHC_u* %LZ4_streamHCPtr, i8* %src, i8* %dst, i32* %srcSizePtr, i32 %targetDestSize) #0 {
entry:
  %LZ4_streamHCPtr.addr = alloca %union.LZ4_streamHC_u*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSizePtr.addr = alloca i32*, align 4
  %targetDestSize.addr = alloca i32, align 4
  store %union.LZ4_streamHC_u* %LZ4_streamHCPtr, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32* %srcSizePtr, i32** %srcSizePtr.addr, align 4, !tbaa !3
  store i32 %targetDestSize, i32* %targetDestSize.addr, align 4, !tbaa !7
  %0 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %1 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %2 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %3 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  %4 = load i32, i32* %targetDestSize.addr, align 4, !tbaa !7
  %call = call i32 @LZ4_compressHC_continue_generic(%union.LZ4_streamHC_u* %0, i8* %1, i8* %2, i32* %3, i32 %4, i32 2)
  ret i32 %call
}

; Function Attrs: nounwind
define i32 @LZ4_saveDictHC(%union.LZ4_streamHC_u* %LZ4_streamHCPtr, i8* %safeBuffer, i32 %dictSize) #0 {
entry:
  %LZ4_streamHCPtr.addr = alloca %union.LZ4_streamHC_u*, align 4
  %safeBuffer.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  %streamPtr = alloca %struct.LZ4HC_CCtx_internal*, align 4
  %prefixSize = alloca i32, align 4
  %endIndex = alloca i32, align 4
  store %union.LZ4_streamHC_u* %LZ4_streamHCPtr, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  store i8* %safeBuffer, i8** %safeBuffer.addr, align 4, !tbaa !3
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !7
  %0 = bitcast %struct.LZ4HC_CCtx_internal** %streamPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHCPtr.addr, align 4, !tbaa !3
  %internal_donotuse = bitcast %union.LZ4_streamHC_u* %1 to %struct.LZ4HC_CCtx_internal*
  store %struct.LZ4HC_CCtx_internal* %internal_donotuse, %struct.LZ4HC_CCtx_internal** %streamPtr, align 4, !tbaa !3
  %2 = bitcast i32* %prefixSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %streamPtr, align 4, !tbaa !3
  %end = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %3, i32 0, i32 2
  %4 = load i8*, i8** %end, align 4, !tbaa !10
  %5 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %streamPtr, align 4, !tbaa !3
  %base = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %5, i32 0, i32 3
  %6 = load i8*, i8** %base, align 4, !tbaa !13
  %7 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %streamPtr, align 4, !tbaa !3
  %dictLimit = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %7, i32 0, i32 5
  %8 = load i32, i32* %dictLimit, align 4, !tbaa !18
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 %8
  %sub.ptr.lhs.cast = ptrtoint i8* %4 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %add.ptr to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %prefixSize, align 4, !tbaa !7
  %9 = load i32, i32* %dictSize.addr, align 4, !tbaa !7
  %cmp = icmp sgt i32 %9, 65536
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 65536, i32* %dictSize.addr, align 4, !tbaa !7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = load i32, i32* %dictSize.addr, align 4, !tbaa !7
  %cmp1 = icmp slt i32 %10, 4
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 0, i32* %dictSize.addr, align 4, !tbaa !7
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %11 = load i32, i32* %dictSize.addr, align 4, !tbaa !7
  %12 = load i32, i32* %prefixSize, align 4, !tbaa !7
  %cmp4 = icmp sgt i32 %11, %12
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  %13 = load i32, i32* %prefixSize, align 4, !tbaa !7
  store i32 %13, i32* %dictSize.addr, align 4, !tbaa !7
  br label %if.end6

if.end6:                                          ; preds = %if.then5, %if.end3
  %14 = load i8*, i8** %safeBuffer.addr, align 4, !tbaa !3
  %15 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %streamPtr, align 4, !tbaa !3
  %end7 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %15, i32 0, i32 2
  %16 = load i8*, i8** %end7, align 4, !tbaa !10
  %17 = load i32, i32* %dictSize.addr, align 4, !tbaa !7
  %idx.neg = sub i32 0, %17
  %add.ptr8 = getelementptr inbounds i8, i8* %16, i32 %idx.neg
  %18 = load i32, i32* %dictSize.addr, align 4, !tbaa !7
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %14, i8* align 1 %add.ptr8, i32 %18, i1 false)
  %19 = bitcast i32* %endIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #7
  %20 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %streamPtr, align 4, !tbaa !3
  %end9 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %20, i32 0, i32 2
  %21 = load i8*, i8** %end9, align 4, !tbaa !10
  %22 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %streamPtr, align 4, !tbaa !3
  %base10 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %22, i32 0, i32 3
  %23 = load i8*, i8** %base10, align 4, !tbaa !13
  %sub.ptr.lhs.cast11 = ptrtoint i8* %21 to i32
  %sub.ptr.rhs.cast12 = ptrtoint i8* %23 to i32
  %sub.ptr.sub13 = sub i32 %sub.ptr.lhs.cast11, %sub.ptr.rhs.cast12
  store i32 %sub.ptr.sub13, i32* %endIndex, align 4, !tbaa !7
  %24 = load i8*, i8** %safeBuffer.addr, align 4, !tbaa !3
  %25 = load i32, i32* %dictSize.addr, align 4, !tbaa !7
  %add.ptr14 = getelementptr inbounds i8, i8* %24, i32 %25
  %26 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %streamPtr, align 4, !tbaa !3
  %end15 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %26, i32 0, i32 2
  store i8* %add.ptr14, i8** %end15, align 4, !tbaa !10
  %27 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %streamPtr, align 4, !tbaa !3
  %end16 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %27, i32 0, i32 2
  %28 = load i8*, i8** %end16, align 4, !tbaa !10
  %29 = load i32, i32* %endIndex, align 4, !tbaa !7
  %idx.neg17 = sub i32 0, %29
  %add.ptr18 = getelementptr inbounds i8, i8* %28, i32 %idx.neg17
  %30 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %streamPtr, align 4, !tbaa !3
  %base19 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %30, i32 0, i32 3
  store i8* %add.ptr18, i8** %base19, align 4, !tbaa !13
  %31 = load i32, i32* %endIndex, align 4, !tbaa !7
  %32 = load i32, i32* %dictSize.addr, align 4, !tbaa !7
  %sub = sub i32 %31, %32
  %33 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %streamPtr, align 4, !tbaa !3
  %dictLimit20 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %33, i32 0, i32 5
  store i32 %sub, i32* %dictLimit20, align 4, !tbaa !18
  %34 = load i32, i32* %endIndex, align 4, !tbaa !7
  %35 = load i32, i32* %dictSize.addr, align 4, !tbaa !7
  %sub21 = sub i32 %34, %35
  %36 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %streamPtr, align 4, !tbaa !3
  %lowLimit = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %36, i32 0, i32 6
  store i32 %sub21, i32* %lowLimit, align 4, !tbaa !19
  %37 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %streamPtr, align 4, !tbaa !3
  %nextToUpdate = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %37, i32 0, i32 7
  %38 = load i32, i32* %nextToUpdate, align 4, !tbaa !16
  %39 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %streamPtr, align 4, !tbaa !3
  %dictLimit22 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %39, i32 0, i32 5
  %40 = load i32, i32* %dictLimit22, align 4, !tbaa !18
  %cmp23 = icmp ult i32 %38, %40
  br i1 %cmp23, label %if.then24, label %if.end27

if.then24:                                        ; preds = %if.end6
  %41 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %streamPtr, align 4, !tbaa !3
  %dictLimit25 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %41, i32 0, i32 5
  %42 = load i32, i32* %dictLimit25, align 4, !tbaa !18
  %43 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %streamPtr, align 4, !tbaa !3
  %nextToUpdate26 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %43, i32 0, i32 7
  store i32 %42, i32* %nextToUpdate26, align 4, !tbaa !16
  br label %if.end27

if.end27:                                         ; preds = %if.then24, %if.end6
  %44 = bitcast i32* %endIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #7
  %45 = load i32, i32* %dictSize.addr, align 4, !tbaa !7
  %46 = bitcast i32* %prefixSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #7
  %47 = bitcast %struct.LZ4HC_CCtx_internal** %streamPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #7
  ret i32 %45
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define i32 @LZ4_compressHC(i8* %src, i8* %dst, i32 %srcSize) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %2 = load i32, i32* %srcSize.addr, align 4, !tbaa !7
  %3 = load i32, i32* %srcSize.addr, align 4, !tbaa !7
  %call = call i32 @LZ4_compressBound(i32 %3)
  %call1 = call i32 @LZ4_compress_HC(i8* %0, i8* %1, i32 %2, i32 %call, i32 0)
  ret i32 %call1
}

; Function Attrs: nounwind
define i32 @LZ4_compressHC_limitedOutput(i8* %src, i8* %dst, i32 %srcSize, i32 %maxDstSize) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %maxDstSize.addr = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !7
  store i32 %maxDstSize, i32* %maxDstSize.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %2 = load i32, i32* %srcSize.addr, align 4, !tbaa !7
  %3 = load i32, i32* %maxDstSize.addr, align 4, !tbaa !7
  %call = call i32 @LZ4_compress_HC(i8* %0, i8* %1, i32 %2, i32 %3, i32 0)
  ret i32 %call
}

; Function Attrs: nounwind
define i32 @LZ4_compressHC2(i8* %src, i8* %dst, i32 %srcSize, i32 %cLevel) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %cLevel.addr = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !7
  store i32 %cLevel, i32* %cLevel.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %2 = load i32, i32* %srcSize.addr, align 4, !tbaa !7
  %3 = load i32, i32* %srcSize.addr, align 4, !tbaa !7
  %call = call i32 @LZ4_compressBound(i32 %3)
  %4 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  %call1 = call i32 @LZ4_compress_HC(i8* %0, i8* %1, i32 %2, i32 %call, i32 %4)
  ret i32 %call1
}

; Function Attrs: nounwind
define i32 @LZ4_compressHC2_limitedOutput(i8* %src, i8* %dst, i32 %srcSize, i32 %maxDstSize, i32 %cLevel) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %maxDstSize.addr = alloca i32, align 4
  %cLevel.addr = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !7
  store i32 %maxDstSize, i32* %maxDstSize.addr, align 4, !tbaa !7
  store i32 %cLevel, i32* %cLevel.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %2 = load i32, i32* %srcSize.addr, align 4, !tbaa !7
  %3 = load i32, i32* %maxDstSize.addr, align 4, !tbaa !7
  %4 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  %call = call i32 @LZ4_compress_HC(i8* %0, i8* %1, i32 %2, i32 %3, i32 %4)
  ret i32 %call
}

; Function Attrs: nounwind
define i32 @LZ4_compressHC_withStateHC(i8* %state, i8* %src, i8* %dst, i32 %srcSize) #0 {
entry:
  %state.addr = alloca i8*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  store i8* %state, i8** %state.addr, align 4, !tbaa !3
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %state.addr, align 4, !tbaa !3
  %1 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %2 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %3 = load i32, i32* %srcSize.addr, align 4, !tbaa !7
  %4 = load i32, i32* %srcSize.addr, align 4, !tbaa !7
  %call = call i32 @LZ4_compressBound(i32 %4)
  %call1 = call i32 @LZ4_compress_HC_extStateHC(i8* %0, i8* %1, i8* %2, i32 %3, i32 %call, i32 0)
  ret i32 %call1
}

; Function Attrs: nounwind
define i32 @LZ4_compressHC_limitedOutput_withStateHC(i8* %state, i8* %src, i8* %dst, i32 %srcSize, i32 %maxDstSize) #0 {
entry:
  %state.addr = alloca i8*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %maxDstSize.addr = alloca i32, align 4
  store i8* %state, i8** %state.addr, align 4, !tbaa !3
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !7
  store i32 %maxDstSize, i32* %maxDstSize.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %state.addr, align 4, !tbaa !3
  %1 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %2 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %3 = load i32, i32* %srcSize.addr, align 4, !tbaa !7
  %4 = load i32, i32* %maxDstSize.addr, align 4, !tbaa !7
  %call = call i32 @LZ4_compress_HC_extStateHC(i8* %0, i8* %1, i8* %2, i32 %3, i32 %4, i32 0)
  ret i32 %call
}

; Function Attrs: nounwind
define i32 @LZ4_compressHC2_withStateHC(i8* %state, i8* %src, i8* %dst, i32 %srcSize, i32 %cLevel) #0 {
entry:
  %state.addr = alloca i8*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %cLevel.addr = alloca i32, align 4
  store i8* %state, i8** %state.addr, align 4, !tbaa !3
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !7
  store i32 %cLevel, i32* %cLevel.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %state.addr, align 4, !tbaa !3
  %1 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %2 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %3 = load i32, i32* %srcSize.addr, align 4, !tbaa !7
  %4 = load i32, i32* %srcSize.addr, align 4, !tbaa !7
  %call = call i32 @LZ4_compressBound(i32 %4)
  %5 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  %call1 = call i32 @LZ4_compress_HC_extStateHC(i8* %0, i8* %1, i8* %2, i32 %3, i32 %call, i32 %5)
  ret i32 %call1
}

; Function Attrs: nounwind
define i32 @LZ4_compressHC2_limitedOutput_withStateHC(i8* %state, i8* %src, i8* %dst, i32 %srcSize, i32 %maxDstSize, i32 %cLevel) #0 {
entry:
  %state.addr = alloca i8*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %maxDstSize.addr = alloca i32, align 4
  %cLevel.addr = alloca i32, align 4
  store i8* %state, i8** %state.addr, align 4, !tbaa !3
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !7
  store i32 %maxDstSize, i32* %maxDstSize.addr, align 4, !tbaa !7
  store i32 %cLevel, i32* %cLevel.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %state.addr, align 4, !tbaa !3
  %1 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %2 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %3 = load i32, i32* %srcSize.addr, align 4, !tbaa !7
  %4 = load i32, i32* %maxDstSize.addr, align 4, !tbaa !7
  %5 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  %call = call i32 @LZ4_compress_HC_extStateHC(i8* %0, i8* %1, i8* %2, i32 %3, i32 %4, i32 %5)
  ret i32 %call
}

; Function Attrs: nounwind
define i32 @LZ4_compressHC_continue(%union.LZ4_streamHC_u* %ctx, i8* %src, i8* %dst, i32 %srcSize) #0 {
entry:
  %ctx.addr = alloca %union.LZ4_streamHC_u*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  store %union.LZ4_streamHC_u* %ctx, %union.LZ4_streamHC_u** %ctx.addr, align 4, !tbaa !3
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !7
  %0 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %ctx.addr, align 4, !tbaa !3
  %1 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %2 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %3 = load i32, i32* %srcSize.addr, align 4, !tbaa !7
  %4 = load i32, i32* %srcSize.addr, align 4, !tbaa !7
  %call = call i32 @LZ4_compressBound(i32 %4)
  %call1 = call i32 @LZ4_compress_HC_continue(%union.LZ4_streamHC_u* %0, i8* %1, i8* %2, i32 %3, i32 %call)
  ret i32 %call1
}

; Function Attrs: nounwind
define i32 @LZ4_compressHC_limitedOutput_continue(%union.LZ4_streamHC_u* %ctx, i8* %src, i8* %dst, i32 %srcSize, i32 %maxDstSize) #0 {
entry:
  %ctx.addr = alloca %union.LZ4_streamHC_u*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %maxDstSize.addr = alloca i32, align 4
  store %union.LZ4_streamHC_u* %ctx, %union.LZ4_streamHC_u** %ctx.addr, align 4, !tbaa !3
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !7
  store i32 %maxDstSize, i32* %maxDstSize.addr, align 4, !tbaa !7
  %0 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %ctx.addr, align 4, !tbaa !3
  %1 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %2 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %3 = load i32, i32* %srcSize.addr, align 4, !tbaa !7
  %4 = load i32, i32* %maxDstSize.addr, align 4, !tbaa !7
  %call = call i32 @LZ4_compress_HC_continue(%union.LZ4_streamHC_u* %0, i8* %1, i8* %2, i32 %3, i32 %4)
  ret i32 %call
}

; Function Attrs: nounwind
define i32 @LZ4_sizeofStreamStateHC() #0 {
entry:
  ret i32 262200
}

; Function Attrs: nounwind
define i32 @LZ4_resetStreamStateHC(i8* %state, i8* %inputBuffer) #0 {
entry:
  %retval = alloca i32, align 4
  %state.addr = alloca i8*, align 4
  %inputBuffer.addr = alloca i8*, align 4
  %hc4 = alloca %union.LZ4_streamHC_u*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %state, i8** %state.addr, align 4, !tbaa !3
  store i8* %inputBuffer, i8** %inputBuffer.addr, align 4, !tbaa !3
  %0 = bitcast %union.LZ4_streamHC_u** %hc4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8*, i8** %state.addr, align 4, !tbaa !3
  %call = call %union.LZ4_streamHC_u* @LZ4_initStreamHC(i8* %1, i32 262200)
  store %union.LZ4_streamHC_u* %call, %union.LZ4_streamHC_u** %hc4, align 4, !tbaa !3
  %2 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %hc4, align 4, !tbaa !3
  %cmp = icmp eq %union.LZ4_streamHC_u* %2, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %3 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %hc4, align 4, !tbaa !3
  %internal_donotuse = bitcast %union.LZ4_streamHC_u* %3 to %struct.LZ4HC_CCtx_internal*
  %4 = load i8*, i8** %inputBuffer.addr, align 4, !tbaa !3
  call void @LZ4HC_init_internal(%struct.LZ4HC_CCtx_internal* %internal_donotuse, i8* %4)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %5 = bitcast %union.LZ4_streamHC_u** %hc4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #7
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: nounwind
define i8* @LZ4_createHC(i8* %inputBuffer) #0 {
entry:
  %retval = alloca i8*, align 4
  %inputBuffer.addr = alloca i8*, align 4
  %hc4 = alloca %union.LZ4_streamHC_u*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %inputBuffer, i8** %inputBuffer.addr, align 4, !tbaa !3
  %0 = bitcast %union.LZ4_streamHC_u** %hc4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call %union.LZ4_streamHC_u* @LZ4_createStreamHC()
  store %union.LZ4_streamHC_u* %call, %union.LZ4_streamHC_u** %hc4, align 4, !tbaa !3
  %1 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %hc4, align 4, !tbaa !3
  %cmp = icmp eq %union.LZ4_streamHC_u* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %2 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %hc4, align 4, !tbaa !3
  %internal_donotuse = bitcast %union.LZ4_streamHC_u* %2 to %struct.LZ4HC_CCtx_internal*
  %3 = load i8*, i8** %inputBuffer.addr, align 4, !tbaa !3
  call void @LZ4HC_init_internal(%struct.LZ4HC_CCtx_internal* %internal_donotuse, i8* %3)
  %4 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %hc4, align 4, !tbaa !3
  %5 = bitcast %union.LZ4_streamHC_u* %4 to i8*
  store i8* %5, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %6 = bitcast %union.LZ4_streamHC_u** %hc4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #7
  %7 = load i8*, i8** %retval, align 4
  ret i8* %7
}

; Function Attrs: nounwind
define i32 @LZ4_freeHC(i8* %LZ4HC_Data) #0 {
entry:
  %retval = alloca i32, align 4
  %LZ4HC_Data.addr = alloca i8*, align 4
  store i8* %LZ4HC_Data, i8** %LZ4HC_Data.addr, align 4, !tbaa !3
  %0 = load i8*, i8** %LZ4HC_Data.addr, align 4, !tbaa !3
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %LZ4HC_Data.addr, align 4, !tbaa !3
  call void @free(i8* %1)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i32, i32* %retval, align 4
  ret i32 %2
}

; Function Attrs: nounwind
define i32 @LZ4_compressHC2_continue(i8* %LZ4HC_Data, i8* %src, i8* %dst, i32 %srcSize, i32 %cLevel) #0 {
entry:
  %LZ4HC_Data.addr = alloca i8*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %cLevel.addr = alloca i32, align 4
  store i8* %LZ4HC_Data, i8** %LZ4HC_Data.addr, align 4, !tbaa !3
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !7
  store i32 %cLevel, i32* %cLevel.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %LZ4HC_Data.addr, align 4, !tbaa !3
  %1 = bitcast i8* %0 to %union.LZ4_streamHC_u*
  %internal_donotuse = bitcast %union.LZ4_streamHC_u* %1 to %struct.LZ4HC_CCtx_internal*
  %2 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %3 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %4 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  %call = call i32 @LZ4HC_compress_generic(%struct.LZ4HC_CCtx_internal* %internal_donotuse, i8* %2, i8* %3, i32* %srcSize.addr, i32 0, i32 %4, i32 0)
  ret i32 %call
}

; Function Attrs: nounwind
define i32 @LZ4_compressHC2_limitedOutput_continue(i8* %LZ4HC_Data, i8* %src, i8* %dst, i32 %srcSize, i32 %dstCapacity, i32 %cLevel) #0 {
entry:
  %LZ4HC_Data.addr = alloca i8*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %dstCapacity.addr = alloca i32, align 4
  %cLevel.addr = alloca i32, align 4
  store i8* %LZ4HC_Data, i8** %LZ4HC_Data.addr, align 4, !tbaa !3
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !7
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !7
  store i32 %cLevel, i32* %cLevel.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %LZ4HC_Data.addr, align 4, !tbaa !3
  %1 = bitcast i8* %0 to %union.LZ4_streamHC_u*
  %internal_donotuse = bitcast %union.LZ4_streamHC_u* %1 to %struct.LZ4HC_CCtx_internal*
  %2 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %3 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %4 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %5 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  %call = call i32 @LZ4HC_compress_generic(%struct.LZ4HC_CCtx_internal* %internal_donotuse, i8* %2, i8* %3, i32* %srcSize.addr, i32 %4, i32 %5, i32 1)
  ret i32 %call
}

; Function Attrs: nounwind
define i8* @LZ4_slideInputBufferHC(i8* %LZ4HC_Data) #0 {
entry:
  %LZ4HC_Data.addr = alloca i8*, align 4
  %ctx = alloca %union.LZ4_streamHC_u*, align 4
  %bufferStart = alloca i8*, align 4
  store i8* %LZ4HC_Data, i8** %LZ4HC_Data.addr, align 4, !tbaa !3
  %0 = bitcast %union.LZ4_streamHC_u** %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8*, i8** %LZ4HC_Data.addr, align 4, !tbaa !3
  %2 = bitcast i8* %1 to %union.LZ4_streamHC_u*
  store %union.LZ4_streamHC_u* %2, %union.LZ4_streamHC_u** %ctx, align 4, !tbaa !3
  %3 = bitcast i8** %bufferStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %ctx, align 4, !tbaa !3
  %internal_donotuse = bitcast %union.LZ4_streamHC_u* %4 to %struct.LZ4HC_CCtx_internal*
  %base = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %internal_donotuse, i32 0, i32 3
  %5 = load i8*, i8** %base, align 4, !tbaa !9
  %6 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %ctx, align 4, !tbaa !3
  %internal_donotuse1 = bitcast %union.LZ4_streamHC_u* %6 to %struct.LZ4HC_CCtx_internal*
  %lowLimit = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %internal_donotuse1, i32 0, i32 6
  %7 = load i32, i32* %lowLimit, align 4, !tbaa !9
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %7
  store i8* %add.ptr, i8** %bufferStart, align 4, !tbaa !3
  %8 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %ctx, align 4, !tbaa !3
  %9 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %ctx, align 4, !tbaa !3
  %internal_donotuse2 = bitcast %union.LZ4_streamHC_u* %9 to %struct.LZ4HC_CCtx_internal*
  %compressionLevel = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %internal_donotuse2, i32 0, i32 8
  %10 = load i16, i16* %compressionLevel, align 4, !tbaa !9
  %conv = sext i16 %10 to i32
  call void @LZ4_resetStreamHC_fast(%union.LZ4_streamHC_u* %8, i32 %conv)
  %11 = load i8*, i8** %bufferStart, align 4, !tbaa !3
  %12 = ptrtoint i8* %11 to i32
  %13 = inttoptr i32 %12 to i8*
  %14 = bitcast i8** %bufferStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #7
  %15 = bitcast %union.LZ4_streamHC_u** %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  ret i8* %13
}

; Function Attrs: nounwind
define internal void @LZ4HC_clearTables(%struct.LZ4HC_CCtx_internal* %hc4) #0 {
entry:
  %hc4.addr = alloca %struct.LZ4HC_CCtx_internal*, align 4
  store %struct.LZ4HC_CCtx_internal* %hc4, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %0 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %hashTable = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %0, i32 0, i32 0
  %arraydecay = getelementptr inbounds [32768 x i32], [32768 x i32]* %hashTable, i32 0, i32 0
  %1 = bitcast i32* %arraydecay to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %1, i8 0, i32 131072, i1 false)
  %2 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %chainTable = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %2, i32 0, i32 1
  %arraydecay1 = getelementptr inbounds [65536 x i16], [65536 x i16]* %chainTable, i32 0, i32 0
  %3 = bitcast i16* %arraydecay1 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 -1, i32 131072, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: nounwind
define internal i32 @LZ4HC_compress_generic_noDictCtx(%struct.LZ4HC_CCtx_internal* %ctx, i8* %src, i8* %dst, i32* %srcSizePtr, i32 %dstCapacity, i32 %cLevel, i32 %limit) #0 {
entry:
  %ctx.addr = alloca %struct.LZ4HC_CCtx_internal*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSizePtr.addr = alloca i32*, align 4
  %dstCapacity.addr = alloca i32, align 4
  %cLevel.addr = alloca i32, align 4
  %limit.addr = alloca i32, align 4
  store %struct.LZ4HC_CCtx_internal* %ctx, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32* %srcSizePtr, i32** %srcSizePtr.addr, align 4, !tbaa !3
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !7
  store i32 %cLevel, i32* %cLevel.addr, align 4, !tbaa !7
  store i32 %limit, i32* %limit.addr, align 4, !tbaa !9
  %0 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %1 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %2 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %3 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  %4 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %5 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  %6 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %call = call i32 @LZ4HC_compress_generic_internal(%struct.LZ4HC_CCtx_internal* %0, i8* %1, i8* %2, i32* %3, i32 %4, i32 %5, i32 %6, i32 0)
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @LZ4HC_compress_generic_dictCtx(%struct.LZ4HC_CCtx_internal* %ctx, i8* %src, i8* %dst, i32* %srcSizePtr, i32 %dstCapacity, i32 %cLevel, i32 %limit) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.LZ4HC_CCtx_internal*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSizePtr.addr = alloca i32*, align 4
  %dstCapacity.addr = alloca i32, align 4
  %cLevel.addr = alloca i32, align 4
  %limit.addr = alloca i32, align 4
  %position = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.LZ4HC_CCtx_internal* %ctx, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32* %srcSizePtr, i32** %srcSizePtr.addr, align 4, !tbaa !3
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !7
  store i32 %cLevel, i32* %cLevel.addr, align 4, !tbaa !7
  store i32 %limit, i32* %limit.addr, align 4, !tbaa !9
  %0 = bitcast i32* %position to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %end = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %1, i32 0, i32 2
  %2 = load i8*, i8** %end, align 4, !tbaa !10
  %3 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %base = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %3, i32 0, i32 3
  %4 = load i8*, i8** %base, align 4, !tbaa !13
  %sub.ptr.lhs.cast = ptrtoint i8* %2 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %4 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %5 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %lowLimit = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %5, i32 0, i32 6
  %6 = load i32, i32* %lowLimit, align 4, !tbaa !19
  %sub = sub i32 %sub.ptr.sub, %6
  store i32 %sub, i32* %position, align 4, !tbaa !14
  %7 = load i32, i32* %position, align 4, !tbaa !14
  %cmp = icmp uge i32 %7, 65536
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %8 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %dictCtx = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %8, i32 0, i32 11
  store %struct.LZ4HC_CCtx_internal* null, %struct.LZ4HC_CCtx_internal** %dictCtx, align 4, !tbaa !20
  %9 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %10 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %11 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %12 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  %13 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %14 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  %15 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %call = call i32 @LZ4HC_compress_generic_noDictCtx(%struct.LZ4HC_CCtx_internal* %9, i8* %10, i8* %11, i32* %12, i32 %13, i32 %14, i32 %15)
  store i32 %call, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %entry
  %16 = load i32, i32* %position, align 4, !tbaa !14
  %cmp1 = icmp eq i32 %16, 0
  br i1 %cmp1, label %land.lhs.true, label %if.else6

land.lhs.true:                                    ; preds = %if.else
  %17 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  %18 = load i32, i32* %17, align 4, !tbaa !7
  %cmp2 = icmp sgt i32 %18, 4096
  br i1 %cmp2, label %if.then3, label %if.else6

if.then3:                                         ; preds = %land.lhs.true
  %19 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %20 = bitcast %struct.LZ4HC_CCtx_internal* %19 to i8*
  %21 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %dictCtx4 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %21, i32 0, i32 11
  %22 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %dictCtx4, align 4, !tbaa !20
  %23 = bitcast %struct.LZ4HC_CCtx_internal* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %23, i32 262176, i1 false)
  %24 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %25 = load i8*, i8** %src.addr, align 4, !tbaa !3
  call void @LZ4HC_setExternalDict(%struct.LZ4HC_CCtx_internal* %24, i8* %25)
  %26 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  %conv = trunc i32 %26 to i16
  %27 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %compressionLevel = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %27, i32 0, i32 8
  store i16 %conv, i16* %compressionLevel, align 4, !tbaa !21
  %28 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %29 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %30 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %31 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  %32 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %33 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  %34 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %call5 = call i32 @LZ4HC_compress_generic_noDictCtx(%struct.LZ4HC_CCtx_internal* %28, i8* %29, i8* %30, i32* %31, i32 %32, i32 %33, i32 %34)
  store i32 %call5, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else6:                                         ; preds = %land.lhs.true, %if.else
  %35 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %36 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %37 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %38 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  %39 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %40 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  %41 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %call7 = call i32 @LZ4HC_compress_generic_internal(%struct.LZ4HC_CCtx_internal* %35, i8* %36, i8* %37, i32* %38, i32 %39, i32 %40, i32 %41, i32 1)
  store i32 %call7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else6, %if.then3, %if.then
  %42 = bitcast i32* %position to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #7
  %43 = load i32, i32* %retval, align 4
  ret i32 %43
}

; Function Attrs: alwaysinline nounwind
define internal i32 @LZ4HC_compress_generic_internal(%struct.LZ4HC_CCtx_internal* %ctx, i8* %src, i8* %dst, i32* %srcSizePtr, i32 %dstCapacity, i32 %cLevel, i32 %limit, i32 %dict) #3 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.LZ4HC_CCtx_internal*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSizePtr.addr = alloca i32*, align 4
  %dstCapacity.addr = alloca i32, align 4
  %cLevel.addr = alloca i32, align 4
  %limit.addr = alloca i32, align 4
  %dict.addr = alloca i32, align 4
  %cParam = alloca %struct.cParams_t, align 4
  %favor = alloca i32, align 4
  %result = alloca i32, align 4
  store %struct.LZ4HC_CCtx_internal* %ctx, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32* %srcSizePtr, i32** %srcSizePtr.addr, align 4, !tbaa !3
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !7
  store i32 %cLevel, i32* %cLevel.addr, align 4, !tbaa !7
  store i32 %limit, i32* %limit.addr, align 4, !tbaa !9
  store i32 %dict, i32* %dict.addr, align 4, !tbaa !9
  %0 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %cmp = icmp eq i32 %0, 2
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %cmp1 = icmp slt i32 %1, 1
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %2 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  %3 = load i32, i32* %2, align 4, !tbaa !7
  %cmp2 = icmp ugt i32 %3, 2113929216
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %4 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  %5 = load i32, i32* %4, align 4, !tbaa !7
  %6 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %end = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %6, i32 0, i32 2
  %7 = load i8*, i8** %end, align 4, !tbaa !10
  %add.ptr = getelementptr inbounds i8, i8* %7, i32 %5
  store i8* %add.ptr, i8** %end, align 4, !tbaa !10
  %8 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  %cmp5 = icmp slt i32 %8, 1
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end4
  store i32 9, i32* %cLevel.addr, align 4, !tbaa !7
  br label %if.end7

if.end7:                                          ; preds = %if.then6, %if.end4
  %9 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  %cmp8 = icmp slt i32 12, %9
  br i1 %cmp8, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end7
  br label %cond.end

cond.false:                                       ; preds = %if.end7
  %10 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 12, %cond.true ], [ %10, %cond.false ]
  store i32 %cond, i32* %cLevel.addr, align 4, !tbaa !7
  %11 = bitcast %struct.cParams_t* %cParam to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %11) #7
  %12 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds [13 x %struct.cParams_t], [13 x %struct.cParams_t]* @LZ4HC_compress_generic_internal.clTable, i32 0, i32 %12
  %13 = bitcast %struct.cParams_t* %cParam to i8*
  %14 = bitcast %struct.cParams_t* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 12, i1 false), !tbaa.struct !23
  %15 = bitcast i32* %favor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  %16 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %favorDecSpeed = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %16, i32 0, i32 9
  %17 = load i8, i8* %favorDecSpeed, align 2, !tbaa !24
  %conv = sext i8 %17 to i32
  %tobool = icmp ne i32 %conv, 0
  %18 = zext i1 %tobool to i64
  %cond9 = select i1 %tobool, i32 1, i32 0
  store i32 %cond9, i32* %favor, align 4, !tbaa !9
  %19 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #7
  %strat = getelementptr inbounds %struct.cParams_t, %struct.cParams_t* %cParam, i32 0, i32 0
  %20 = load i32, i32* %strat, align 4, !tbaa !25
  %cmp10 = icmp eq i32 %20, 0
  br i1 %cmp10, label %if.then12, label %if.else

if.then12:                                        ; preds = %cond.end
  %21 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %22 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %23 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %24 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  %25 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %nbSearches = getelementptr inbounds %struct.cParams_t, %struct.cParams_t* %cParam, i32 0, i32 1
  %26 = load i32, i32* %nbSearches, align 4, !tbaa !27
  %27 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %28 = load i32, i32* %dict.addr, align 4, !tbaa !9
  %call = call i32 @LZ4HC_compress_hashChain(%struct.LZ4HC_CCtx_internal* %21, i8* %22, i8* %23, i32* %24, i32 %25, i32 %26, i32 %27, i32 %28)
  store i32 %call, i32* %result, align 4, !tbaa !7
  br label %if.end17

if.else:                                          ; preds = %cond.end
  %29 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %30 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %31 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %32 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  %33 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %nbSearches13 = getelementptr inbounds %struct.cParams_t, %struct.cParams_t* %cParam, i32 0, i32 1
  %34 = load i32, i32* %nbSearches13, align 4, !tbaa !27
  %targetLength = getelementptr inbounds %struct.cParams_t, %struct.cParams_t* %cParam, i32 0, i32 2
  %35 = load i32, i32* %targetLength, align 4, !tbaa !28
  %36 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %37 = load i32, i32* %cLevel.addr, align 4, !tbaa !7
  %cmp14 = icmp eq i32 %37, 12
  %conv15 = zext i1 %cmp14 to i32
  %38 = load i32, i32* %dict.addr, align 4, !tbaa !9
  %39 = load i32, i32* %favor, align 4, !tbaa !9
  %call16 = call i32 @LZ4HC_compress_optimal(%struct.LZ4HC_CCtx_internal* %29, i8* %30, i8* %31, i32* %32, i32 %33, i32 %34, i32 %35, i32 %36, i32 %conv15, i32 %38, i32 %39)
  store i32 %call16, i32* %result, align 4, !tbaa !7
  br label %if.end17

if.end17:                                         ; preds = %if.else, %if.then12
  %40 = load i32, i32* %result, align 4, !tbaa !7
  %cmp18 = icmp sle i32 %40, 0
  br i1 %cmp18, label %if.then20, label %if.end21

if.then20:                                        ; preds = %if.end17
  %41 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %dirty = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %41, i32 0, i32 10
  store i8 1, i8* %dirty, align 1, !tbaa !29
  br label %if.end21

if.end21:                                         ; preds = %if.then20, %if.end17
  %42 = load i32, i32* %result, align 4, !tbaa !7
  store i32 %42, i32* %retval, align 4
  %43 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #7
  %44 = bitcast i32* %favor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #7
  %45 = bitcast %struct.cParams_t* %cParam to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %45) #7
  br label %return

return:                                           ; preds = %if.end21, %if.then3, %if.then
  %46 = load i32, i32* %retval, align 4
  ret i32 %46
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: alwaysinline nounwind
define internal i32 @LZ4HC_compress_hashChain(%struct.LZ4HC_CCtx_internal* %ctx, i8* %source, i8* %dest, i32* %srcSizePtr, i32 %maxOutputSize, i32 %maxNbAttempts, i32 %limit, i32 %dict) #3 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.LZ4HC_CCtx_internal*, align 4
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %srcSizePtr.addr = alloca i32*, align 4
  %maxOutputSize.addr = alloca i32, align 4
  %maxNbAttempts.addr = alloca i32, align 4
  %limit.addr = alloca i32, align 4
  %dict.addr = alloca i32, align 4
  %inputSize = alloca i32, align 4
  %patternAnalysis = alloca i32, align 4
  %ip = alloca i8*, align 4
  %anchor = alloca i8*, align 4
  %iend = alloca i8*, align 4
  %mflimit = alloca i8*, align 4
  %matchlimit = alloca i8*, align 4
  %optr = alloca i8*, align 4
  %op = alloca i8*, align 4
  %oend = alloca i8*, align 4
  %ml0 = alloca i32, align 4
  %ml = alloca i32, align 4
  %ml2 = alloca i32, align 4
  %ml3 = alloca i32, align 4
  %start0 = alloca i8*, align 4
  %ref0 = alloca i8*, align 4
  %ref = alloca i8*, align 4
  %start2 = alloca i8*, align 4
  %ref2 = alloca i8*, align 4
  %start3 = alloca i8*, align 4
  %ref3 = alloca i8*, align 4
  %correction = alloca i32, align 4
  %new_ml = alloca i32, align 4
  %correction120 = alloca i32, align 4
  %correction149 = alloca i32, align 4
  %lastRunSize = alloca i32, align 4
  %litLength = alloca i32, align 4
  %totalSize = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %accumulator = alloca i32, align 4
  store %struct.LZ4HC_CCtx_internal* %ctx, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  store i8* %source, i8** %source.addr, align 4, !tbaa !3
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !3
  store i32* %srcSizePtr, i32** %srcSizePtr.addr, align 4, !tbaa !3
  store i32 %maxOutputSize, i32* %maxOutputSize.addr, align 4, !tbaa !7
  store i32 %maxNbAttempts, i32* %maxNbAttempts.addr, align 4, !tbaa !7
  store i32 %limit, i32* %limit.addr, align 4, !tbaa !9
  store i32 %dict, i32* %dict.addr, align 4, !tbaa !9
  %0 = bitcast i32* %inputSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  %2 = load i32, i32* %1, align 4, !tbaa !7
  store i32 %2, i32* %inputSize, align 4, !tbaa !7
  %3 = bitcast i32* %patternAnalysis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %maxNbAttempts.addr, align 4, !tbaa !7
  %cmp = icmp ugt i32 %4, 128
  %conv = zext i1 %cmp to i32
  store i32 %conv, i32* %patternAnalysis, align 4, !tbaa !7
  %5 = bitcast i8** %ip to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load i8*, i8** %source.addr, align 4, !tbaa !3
  store i8* %6, i8** %ip, align 4, !tbaa !3
  %7 = bitcast i8** %anchor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = load i8*, i8** %ip, align 4, !tbaa !3
  store i8* %8, i8** %anchor, align 4, !tbaa !3
  %9 = bitcast i8** %iend to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load i8*, i8** %ip, align 4, !tbaa !3
  %11 = load i32, i32* %inputSize, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds i8, i8* %10, i32 %11
  store i8* %add.ptr, i8** %iend, align 4, !tbaa !3
  %12 = bitcast i8** %mflimit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  %13 = load i8*, i8** %iend, align 4, !tbaa !3
  %add.ptr1 = getelementptr inbounds i8, i8* %13, i32 -12
  store i8* %add.ptr1, i8** %mflimit, align 4, !tbaa !3
  %14 = bitcast i8** %matchlimit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %15 = load i8*, i8** %iend, align 4, !tbaa !3
  %add.ptr2 = getelementptr inbounds i8, i8* %15, i32 -5
  store i8* %add.ptr2, i8** %matchlimit, align 4, !tbaa !3
  %16 = bitcast i8** %optr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %17 = load i8*, i8** %dest.addr, align 4, !tbaa !3
  store i8* %17, i8** %optr, align 4, !tbaa !3
  %18 = bitcast i8** %op to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #7
  %19 = load i8*, i8** %dest.addr, align 4, !tbaa !3
  store i8* %19, i8** %op, align 4, !tbaa !3
  %20 = bitcast i8** %oend to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #7
  %21 = load i8*, i8** %op, align 4, !tbaa !3
  %22 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !7
  %add.ptr3 = getelementptr inbounds i8, i8* %21, i32 %22
  store i8* %add.ptr3, i8** %oend, align 4, !tbaa !3
  %23 = bitcast i32* %ml0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #7
  %24 = bitcast i32* %ml to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  %25 = bitcast i32* %ml2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #7
  %26 = bitcast i32* %ml3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #7
  %27 = bitcast i8** %start0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #7
  %28 = bitcast i8** %ref0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #7
  %29 = bitcast i8** %ref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #7
  store i8* null, i8** %ref, align 4, !tbaa !3
  %30 = bitcast i8** %start2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #7
  store i8* null, i8** %start2, align 4, !tbaa !3
  %31 = bitcast i8** %ref2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #7
  store i8* null, i8** %ref2, align 4, !tbaa !3
  %32 = bitcast i8** %start3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #7
  store i8* null, i8** %start3, align 4, !tbaa !3
  %33 = bitcast i8** %ref3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #7
  store i8* null, i8** %ref3, align 4, !tbaa !3
  %34 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  store i32 0, i32* %34, align 4, !tbaa !7
  %35 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %cmp4 = icmp eq i32 %35, 2
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %36 = load i8*, i8** %oend, align 4, !tbaa !3
  %add.ptr6 = getelementptr inbounds i8, i8* %36, i32 -5
  store i8* %add.ptr6, i8** %oend, align 4, !tbaa !3
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %37 = load i32, i32* %inputSize, align 4, !tbaa !7
  %cmp7 = icmp slt i32 %37, 13
  br i1 %cmp7, label %if.then9, label %if.end10

if.then9:                                         ; preds = %if.end
  br label %_last_literals

if.end10:                                         ; preds = %if.end
  br label %while.cond

while.cond:                                       ; preds = %if.end105, %if.end31, %if.then15, %if.end10
  %38 = load i8*, i8** %ip, align 4, !tbaa !3
  %39 = load i8*, i8** %mflimit, align 4, !tbaa !3
  %cmp11 = icmp ule i8* %38, %39
  br i1 %cmp11, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %40 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %41 = load i8*, i8** %ip, align 4, !tbaa !3
  %42 = load i8*, i8** %matchlimit, align 4, !tbaa !3
  %43 = load i32, i32* %maxNbAttempts.addr, align 4, !tbaa !7
  %44 = load i32, i32* %patternAnalysis, align 4, !tbaa !7
  %45 = load i32, i32* %dict.addr, align 4, !tbaa !9
  %call = call i32 @LZ4HC_InsertAndFindBestMatch(%struct.LZ4HC_CCtx_internal* %40, i8* %41, i8* %42, i8** %ref, i32 %43, i32 %44, i32 %45)
  store i32 %call, i32* %ml, align 4, !tbaa !7
  %46 = load i32, i32* %ml, align 4, !tbaa !7
  %cmp13 = icmp slt i32 %46, 4
  br i1 %cmp13, label %if.then15, label %if.end16

if.then15:                                        ; preds = %while.body
  %47 = load i8*, i8** %ip, align 4, !tbaa !3
  %incdec.ptr = getelementptr inbounds i8, i8* %47, i32 1
  store i8* %incdec.ptr, i8** %ip, align 4, !tbaa !3
  br label %while.cond

if.end16:                                         ; preds = %while.body
  %48 = load i8*, i8** %ip, align 4, !tbaa !3
  store i8* %48, i8** %start0, align 4, !tbaa !3
  %49 = load i8*, i8** %ref, align 4, !tbaa !3
  store i8* %49, i8** %ref0, align 4, !tbaa !3
  %50 = load i32, i32* %ml, align 4, !tbaa !7
  store i32 %50, i32* %ml0, align 4, !tbaa !7
  br label %_Search2

_Search2:                                         ; preds = %if.end136, %if.then44, %if.end16
  %51 = load i8*, i8** %ip, align 4, !tbaa !3
  %52 = load i32, i32* %ml, align 4, !tbaa !7
  %add.ptr17 = getelementptr inbounds i8, i8* %51, i32 %52
  %53 = load i8*, i8** %mflimit, align 4, !tbaa !3
  %cmp18 = icmp ule i8* %add.ptr17, %53
  br i1 %cmp18, label %if.then20, label %if.else

if.then20:                                        ; preds = %_Search2
  %54 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %55 = load i8*, i8** %ip, align 4, !tbaa !3
  %56 = load i32, i32* %ml, align 4, !tbaa !7
  %add.ptr21 = getelementptr inbounds i8, i8* %55, i32 %56
  %add.ptr22 = getelementptr inbounds i8, i8* %add.ptr21, i32 -2
  %57 = load i8*, i8** %ip, align 4, !tbaa !3
  %add.ptr23 = getelementptr inbounds i8, i8* %57, i32 0
  %58 = load i8*, i8** %matchlimit, align 4, !tbaa !3
  %59 = load i32, i32* %ml, align 4, !tbaa !7
  %60 = load i32, i32* %maxNbAttempts.addr, align 4, !tbaa !7
  %61 = load i32, i32* %patternAnalysis, align 4, !tbaa !7
  %62 = load i32, i32* %dict.addr, align 4, !tbaa !9
  %call24 = call i32 @LZ4HC_InsertAndGetWiderMatch(%struct.LZ4HC_CCtx_internal* %54, i8* %add.ptr22, i8* %add.ptr23, i8* %58, i32 %59, i8** %ref2, i8** %start2, i32 %60, i32 %61, i32 0, i32 %62, i32 0)
  store i32 %call24, i32* %ml2, align 4, !tbaa !7
  br label %if.end25

if.else:                                          ; preds = %_Search2
  %63 = load i32, i32* %ml, align 4, !tbaa !7
  store i32 %63, i32* %ml2, align 4, !tbaa !7
  br label %if.end25

if.end25:                                         ; preds = %if.else, %if.then20
  %64 = load i32, i32* %ml2, align 4, !tbaa !7
  %65 = load i32, i32* %ml, align 4, !tbaa !7
  %cmp26 = icmp eq i32 %64, %65
  br i1 %cmp26, label %if.then28, label %if.end32

if.then28:                                        ; preds = %if.end25
  %66 = load i8*, i8** %op, align 4, !tbaa !3
  store i8* %66, i8** %optr, align 4, !tbaa !3
  %67 = load i32, i32* %ml, align 4, !tbaa !7
  %68 = load i8*, i8** %ref, align 4, !tbaa !3
  %69 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %70 = load i8*, i8** %oend, align 4, !tbaa !3
  %call29 = call i32 @LZ4HC_encodeSequence(i8** %ip, i8** %op, i8** %anchor, i32 %67, i8* %68, i32 %69, i8* %70)
  %tobool = icmp ne i32 %call29, 0
  br i1 %tobool, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then28
  br label %_dest_overflow

if.end31:                                         ; preds = %if.then28
  br label %while.cond

if.end32:                                         ; preds = %if.end25
  %71 = load i8*, i8** %start0, align 4, !tbaa !3
  %72 = load i8*, i8** %ip, align 4, !tbaa !3
  %cmp33 = icmp ult i8* %71, %72
  br i1 %cmp33, label %if.then35, label %if.end41

if.then35:                                        ; preds = %if.end32
  %73 = load i8*, i8** %start2, align 4, !tbaa !3
  %74 = load i8*, i8** %ip, align 4, !tbaa !3
  %75 = load i32, i32* %ml0, align 4, !tbaa !7
  %add.ptr36 = getelementptr inbounds i8, i8* %74, i32 %75
  %cmp37 = icmp ult i8* %73, %add.ptr36
  br i1 %cmp37, label %if.then39, label %if.end40

if.then39:                                        ; preds = %if.then35
  %76 = load i8*, i8** %start0, align 4, !tbaa !3
  store i8* %76, i8** %ip, align 4, !tbaa !3
  %77 = load i8*, i8** %ref0, align 4, !tbaa !3
  store i8* %77, i8** %ref, align 4, !tbaa !3
  %78 = load i32, i32* %ml0, align 4, !tbaa !7
  store i32 %78, i32* %ml, align 4, !tbaa !7
  br label %if.end40

if.end40:                                         ; preds = %if.then39, %if.then35
  br label %if.end41

if.end41:                                         ; preds = %if.end40, %if.end32
  %79 = load i8*, i8** %start2, align 4, !tbaa !3
  %80 = load i8*, i8** %ip, align 4, !tbaa !3
  %sub.ptr.lhs.cast = ptrtoint i8* %79 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %80 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %cmp42 = icmp slt i32 %sub.ptr.sub, 3
  br i1 %cmp42, label %if.then44, label %if.end45

if.then44:                                        ; preds = %if.end41
  %81 = load i32, i32* %ml2, align 4, !tbaa !7
  store i32 %81, i32* %ml, align 4, !tbaa !7
  %82 = load i8*, i8** %start2, align 4, !tbaa !3
  store i8* %82, i8** %ip, align 4, !tbaa !3
  %83 = load i8*, i8** %ref2, align 4, !tbaa !3
  store i8* %83, i8** %ref, align 4, !tbaa !3
  br label %_Search2

if.end45:                                         ; preds = %if.end41
  br label %_Search3

_Search3:                                         ; preds = %if.end186, %if.end137, %if.end45
  %84 = load i8*, i8** %start2, align 4, !tbaa !3
  %85 = load i8*, i8** %ip, align 4, !tbaa !3
  %sub.ptr.lhs.cast46 = ptrtoint i8* %84 to i32
  %sub.ptr.rhs.cast47 = ptrtoint i8* %85 to i32
  %sub.ptr.sub48 = sub i32 %sub.ptr.lhs.cast46, %sub.ptr.rhs.cast47
  %cmp49 = icmp slt i32 %sub.ptr.sub48, 18
  br i1 %cmp49, label %if.then51, label %if.end77

if.then51:                                        ; preds = %_Search3
  %86 = bitcast i32* %correction to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #7
  %87 = bitcast i32* %new_ml to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #7
  %88 = load i32, i32* %ml, align 4, !tbaa !7
  store i32 %88, i32* %new_ml, align 4, !tbaa !7
  %89 = load i32, i32* %new_ml, align 4, !tbaa !7
  %cmp52 = icmp sgt i32 %89, 18
  br i1 %cmp52, label %if.then54, label %if.end55

if.then54:                                        ; preds = %if.then51
  store i32 18, i32* %new_ml, align 4, !tbaa !7
  br label %if.end55

if.end55:                                         ; preds = %if.then54, %if.then51
  %90 = load i8*, i8** %ip, align 4, !tbaa !3
  %91 = load i32, i32* %new_ml, align 4, !tbaa !7
  %add.ptr56 = getelementptr inbounds i8, i8* %90, i32 %91
  %92 = load i8*, i8** %start2, align 4, !tbaa !3
  %93 = load i32, i32* %ml2, align 4, !tbaa !7
  %add.ptr57 = getelementptr inbounds i8, i8* %92, i32 %93
  %add.ptr58 = getelementptr inbounds i8, i8* %add.ptr57, i32 -4
  %cmp59 = icmp ugt i8* %add.ptr56, %add.ptr58
  br i1 %cmp59, label %if.then61, label %if.end65

if.then61:                                        ; preds = %if.end55
  %94 = load i8*, i8** %start2, align 4, !tbaa !3
  %95 = load i8*, i8** %ip, align 4, !tbaa !3
  %sub.ptr.lhs.cast62 = ptrtoint i8* %94 to i32
  %sub.ptr.rhs.cast63 = ptrtoint i8* %95 to i32
  %sub.ptr.sub64 = sub i32 %sub.ptr.lhs.cast62, %sub.ptr.rhs.cast63
  %96 = load i32, i32* %ml2, align 4, !tbaa !7
  %add = add nsw i32 %sub.ptr.sub64, %96
  %sub = sub nsw i32 %add, 4
  store i32 %sub, i32* %new_ml, align 4, !tbaa !7
  br label %if.end65

if.end65:                                         ; preds = %if.then61, %if.end55
  %97 = load i32, i32* %new_ml, align 4, !tbaa !7
  %98 = load i8*, i8** %start2, align 4, !tbaa !3
  %99 = load i8*, i8** %ip, align 4, !tbaa !3
  %sub.ptr.lhs.cast66 = ptrtoint i8* %98 to i32
  %sub.ptr.rhs.cast67 = ptrtoint i8* %99 to i32
  %sub.ptr.sub68 = sub i32 %sub.ptr.lhs.cast66, %sub.ptr.rhs.cast67
  %sub69 = sub nsw i32 %97, %sub.ptr.sub68
  store i32 %sub69, i32* %correction, align 4, !tbaa !7
  %100 = load i32, i32* %correction, align 4, !tbaa !7
  %cmp70 = icmp sgt i32 %100, 0
  br i1 %cmp70, label %if.then72, label %if.end76

if.then72:                                        ; preds = %if.end65
  %101 = load i32, i32* %correction, align 4, !tbaa !7
  %102 = load i8*, i8** %start2, align 4, !tbaa !3
  %add.ptr73 = getelementptr inbounds i8, i8* %102, i32 %101
  store i8* %add.ptr73, i8** %start2, align 4, !tbaa !3
  %103 = load i32, i32* %correction, align 4, !tbaa !7
  %104 = load i8*, i8** %ref2, align 4, !tbaa !3
  %add.ptr74 = getelementptr inbounds i8, i8* %104, i32 %103
  store i8* %add.ptr74, i8** %ref2, align 4, !tbaa !3
  %105 = load i32, i32* %correction, align 4, !tbaa !7
  %106 = load i32, i32* %ml2, align 4, !tbaa !7
  %sub75 = sub nsw i32 %106, %105
  store i32 %sub75, i32* %ml2, align 4, !tbaa !7
  br label %if.end76

if.end76:                                         ; preds = %if.then72, %if.end65
  %107 = bitcast i32* %new_ml to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #7
  %108 = bitcast i32* %correction to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #7
  br label %if.end77

if.end77:                                         ; preds = %if.end76, %_Search3
  %109 = load i8*, i8** %start2, align 4, !tbaa !3
  %110 = load i32, i32* %ml2, align 4, !tbaa !7
  %add.ptr78 = getelementptr inbounds i8, i8* %109, i32 %110
  %111 = load i8*, i8** %mflimit, align 4, !tbaa !3
  %cmp79 = icmp ule i8* %add.ptr78, %111
  br i1 %cmp79, label %if.then81, label %if.else85

if.then81:                                        ; preds = %if.end77
  %112 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %113 = load i8*, i8** %start2, align 4, !tbaa !3
  %114 = load i32, i32* %ml2, align 4, !tbaa !7
  %add.ptr82 = getelementptr inbounds i8, i8* %113, i32 %114
  %add.ptr83 = getelementptr inbounds i8, i8* %add.ptr82, i32 -3
  %115 = load i8*, i8** %start2, align 4, !tbaa !3
  %116 = load i8*, i8** %matchlimit, align 4, !tbaa !3
  %117 = load i32, i32* %ml2, align 4, !tbaa !7
  %118 = load i32, i32* %maxNbAttempts.addr, align 4, !tbaa !7
  %119 = load i32, i32* %patternAnalysis, align 4, !tbaa !7
  %120 = load i32, i32* %dict.addr, align 4, !tbaa !9
  %call84 = call i32 @LZ4HC_InsertAndGetWiderMatch(%struct.LZ4HC_CCtx_internal* %112, i8* %add.ptr83, i8* %115, i8* %116, i32 %117, i8** %ref3, i8** %start3, i32 %118, i32 %119, i32 0, i32 %120, i32 0)
  store i32 %call84, i32* %ml3, align 4, !tbaa !7
  br label %if.end86

if.else85:                                        ; preds = %if.end77
  %121 = load i32, i32* %ml2, align 4, !tbaa !7
  store i32 %121, i32* %ml3, align 4, !tbaa !7
  br label %if.end86

if.end86:                                         ; preds = %if.else85, %if.then81
  %122 = load i32, i32* %ml3, align 4, !tbaa !7
  %123 = load i32, i32* %ml2, align 4, !tbaa !7
  %cmp87 = icmp eq i32 %122, %123
  br i1 %cmp87, label %if.then89, label %if.end106

if.then89:                                        ; preds = %if.end86
  %124 = load i8*, i8** %start2, align 4, !tbaa !3
  %125 = load i8*, i8** %ip, align 4, !tbaa !3
  %126 = load i32, i32* %ml, align 4, !tbaa !7
  %add.ptr90 = getelementptr inbounds i8, i8* %125, i32 %126
  %cmp91 = icmp ult i8* %124, %add.ptr90
  br i1 %cmp91, label %if.then93, label %if.end97

if.then93:                                        ; preds = %if.then89
  %127 = load i8*, i8** %start2, align 4, !tbaa !3
  %128 = load i8*, i8** %ip, align 4, !tbaa !3
  %sub.ptr.lhs.cast94 = ptrtoint i8* %127 to i32
  %sub.ptr.rhs.cast95 = ptrtoint i8* %128 to i32
  %sub.ptr.sub96 = sub i32 %sub.ptr.lhs.cast94, %sub.ptr.rhs.cast95
  store i32 %sub.ptr.sub96, i32* %ml, align 4, !tbaa !7
  br label %if.end97

if.end97:                                         ; preds = %if.then93, %if.then89
  %129 = load i8*, i8** %op, align 4, !tbaa !3
  store i8* %129, i8** %optr, align 4, !tbaa !3
  %130 = load i32, i32* %ml, align 4, !tbaa !7
  %131 = load i8*, i8** %ref, align 4, !tbaa !3
  %132 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %133 = load i8*, i8** %oend, align 4, !tbaa !3
  %call98 = call i32 @LZ4HC_encodeSequence(i8** %ip, i8** %op, i8** %anchor, i32 %130, i8* %131, i32 %132, i8* %133)
  %tobool99 = icmp ne i32 %call98, 0
  br i1 %tobool99, label %if.then100, label %if.end101

if.then100:                                       ; preds = %if.end97
  br label %_dest_overflow

if.end101:                                        ; preds = %if.end97
  %134 = load i8*, i8** %start2, align 4, !tbaa !3
  store i8* %134, i8** %ip, align 4, !tbaa !3
  %135 = load i8*, i8** %op, align 4, !tbaa !3
  store i8* %135, i8** %optr, align 4, !tbaa !3
  %136 = load i32, i32* %ml2, align 4, !tbaa !7
  %137 = load i8*, i8** %ref2, align 4, !tbaa !3
  %138 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %139 = load i8*, i8** %oend, align 4, !tbaa !3
  %call102 = call i32 @LZ4HC_encodeSequence(i8** %ip, i8** %op, i8** %anchor, i32 %136, i8* %137, i32 %138, i8* %139)
  %tobool103 = icmp ne i32 %call102, 0
  br i1 %tobool103, label %if.then104, label %if.end105

if.then104:                                       ; preds = %if.end101
  br label %_dest_overflow

if.end105:                                        ; preds = %if.end101
  br label %while.cond

if.end106:                                        ; preds = %if.end86
  %140 = load i8*, i8** %start3, align 4, !tbaa !3
  %141 = load i8*, i8** %ip, align 4, !tbaa !3
  %142 = load i32, i32* %ml, align 4, !tbaa !7
  %add.ptr107 = getelementptr inbounds i8, i8* %141, i32 %142
  %add.ptr108 = getelementptr inbounds i8, i8* %add.ptr107, i32 3
  %cmp109 = icmp ult i8* %140, %add.ptr108
  br i1 %cmp109, label %if.then111, label %if.end138

if.then111:                                       ; preds = %if.end106
  %143 = load i8*, i8** %start3, align 4, !tbaa !3
  %144 = load i8*, i8** %ip, align 4, !tbaa !3
  %145 = load i32, i32* %ml, align 4, !tbaa !7
  %add.ptr112 = getelementptr inbounds i8, i8* %144, i32 %145
  %cmp113 = icmp uge i8* %143, %add.ptr112
  br i1 %cmp113, label %if.then115, label %if.end137

if.then115:                                       ; preds = %if.then111
  %146 = load i8*, i8** %start2, align 4, !tbaa !3
  %147 = load i8*, i8** %ip, align 4, !tbaa !3
  %148 = load i32, i32* %ml, align 4, !tbaa !7
  %add.ptr116 = getelementptr inbounds i8, i8* %147, i32 %148
  %cmp117 = icmp ult i8* %146, %add.ptr116
  br i1 %cmp117, label %if.then119, label %if.end132

if.then119:                                       ; preds = %if.then115
  %149 = bitcast i32* %correction120 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %149) #7
  %150 = load i8*, i8** %ip, align 4, !tbaa !3
  %151 = load i32, i32* %ml, align 4, !tbaa !7
  %add.ptr121 = getelementptr inbounds i8, i8* %150, i32 %151
  %152 = load i8*, i8** %start2, align 4, !tbaa !3
  %sub.ptr.lhs.cast122 = ptrtoint i8* %add.ptr121 to i32
  %sub.ptr.rhs.cast123 = ptrtoint i8* %152 to i32
  %sub.ptr.sub124 = sub i32 %sub.ptr.lhs.cast122, %sub.ptr.rhs.cast123
  store i32 %sub.ptr.sub124, i32* %correction120, align 4, !tbaa !7
  %153 = load i32, i32* %correction120, align 4, !tbaa !7
  %154 = load i8*, i8** %start2, align 4, !tbaa !3
  %add.ptr125 = getelementptr inbounds i8, i8* %154, i32 %153
  store i8* %add.ptr125, i8** %start2, align 4, !tbaa !3
  %155 = load i32, i32* %correction120, align 4, !tbaa !7
  %156 = load i8*, i8** %ref2, align 4, !tbaa !3
  %add.ptr126 = getelementptr inbounds i8, i8* %156, i32 %155
  store i8* %add.ptr126, i8** %ref2, align 4, !tbaa !3
  %157 = load i32, i32* %correction120, align 4, !tbaa !7
  %158 = load i32, i32* %ml2, align 4, !tbaa !7
  %sub127 = sub nsw i32 %158, %157
  store i32 %sub127, i32* %ml2, align 4, !tbaa !7
  %159 = load i32, i32* %ml2, align 4, !tbaa !7
  %cmp128 = icmp slt i32 %159, 4
  br i1 %cmp128, label %if.then130, label %if.end131

if.then130:                                       ; preds = %if.then119
  %160 = load i8*, i8** %start3, align 4, !tbaa !3
  store i8* %160, i8** %start2, align 4, !tbaa !3
  %161 = load i8*, i8** %ref3, align 4, !tbaa !3
  store i8* %161, i8** %ref2, align 4, !tbaa !3
  %162 = load i32, i32* %ml3, align 4, !tbaa !7
  store i32 %162, i32* %ml2, align 4, !tbaa !7
  br label %if.end131

if.end131:                                        ; preds = %if.then130, %if.then119
  %163 = bitcast i32* %correction120 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #7
  br label %if.end132

if.end132:                                        ; preds = %if.end131, %if.then115
  %164 = load i8*, i8** %op, align 4, !tbaa !3
  store i8* %164, i8** %optr, align 4, !tbaa !3
  %165 = load i32, i32* %ml, align 4, !tbaa !7
  %166 = load i8*, i8** %ref, align 4, !tbaa !3
  %167 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %168 = load i8*, i8** %oend, align 4, !tbaa !3
  %call133 = call i32 @LZ4HC_encodeSequence(i8** %ip, i8** %op, i8** %anchor, i32 %165, i8* %166, i32 %167, i8* %168)
  %tobool134 = icmp ne i32 %call133, 0
  br i1 %tobool134, label %if.then135, label %if.end136

if.then135:                                       ; preds = %if.end132
  br label %_dest_overflow

if.end136:                                        ; preds = %if.end132
  %169 = load i8*, i8** %start3, align 4, !tbaa !3
  store i8* %169, i8** %ip, align 4, !tbaa !3
  %170 = load i8*, i8** %ref3, align 4, !tbaa !3
  store i8* %170, i8** %ref, align 4, !tbaa !3
  %171 = load i32, i32* %ml3, align 4, !tbaa !7
  store i32 %171, i32* %ml, align 4, !tbaa !7
  %172 = load i8*, i8** %start2, align 4, !tbaa !3
  store i8* %172, i8** %start0, align 4, !tbaa !3
  %173 = load i8*, i8** %ref2, align 4, !tbaa !3
  store i8* %173, i8** %ref0, align 4, !tbaa !3
  %174 = load i32, i32* %ml2, align 4, !tbaa !7
  store i32 %174, i32* %ml0, align 4, !tbaa !7
  br label %_Search2

if.end137:                                        ; preds = %if.then111
  %175 = load i8*, i8** %start3, align 4, !tbaa !3
  store i8* %175, i8** %start2, align 4, !tbaa !3
  %176 = load i8*, i8** %ref3, align 4, !tbaa !3
  store i8* %176, i8** %ref2, align 4, !tbaa !3
  %177 = load i32, i32* %ml3, align 4, !tbaa !7
  store i32 %177, i32* %ml2, align 4, !tbaa !7
  br label %_Search3

if.end138:                                        ; preds = %if.end106
  %178 = load i8*, i8** %start2, align 4, !tbaa !3
  %179 = load i8*, i8** %ip, align 4, !tbaa !3
  %180 = load i32, i32* %ml, align 4, !tbaa !7
  %add.ptr139 = getelementptr inbounds i8, i8* %179, i32 %180
  %cmp140 = icmp ult i8* %178, %add.ptr139
  br i1 %cmp140, label %if.then142, label %if.end182

if.then142:                                       ; preds = %if.end138
  %181 = load i8*, i8** %start2, align 4, !tbaa !3
  %182 = load i8*, i8** %ip, align 4, !tbaa !3
  %sub.ptr.lhs.cast143 = ptrtoint i8* %181 to i32
  %sub.ptr.rhs.cast144 = ptrtoint i8* %182 to i32
  %sub.ptr.sub145 = sub i32 %sub.ptr.lhs.cast143, %sub.ptr.rhs.cast144
  %cmp146 = icmp slt i32 %sub.ptr.sub145, 18
  br i1 %cmp146, label %if.then148, label %if.else177

if.then148:                                       ; preds = %if.then142
  %183 = bitcast i32* %correction149 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %183) #7
  %184 = load i32, i32* %ml, align 4, !tbaa !7
  %cmp150 = icmp sgt i32 %184, 18
  br i1 %cmp150, label %if.then152, label %if.end153

if.then152:                                       ; preds = %if.then148
  store i32 18, i32* %ml, align 4, !tbaa !7
  br label %if.end153

if.end153:                                        ; preds = %if.then152, %if.then148
  %185 = load i8*, i8** %ip, align 4, !tbaa !3
  %186 = load i32, i32* %ml, align 4, !tbaa !7
  %add.ptr154 = getelementptr inbounds i8, i8* %185, i32 %186
  %187 = load i8*, i8** %start2, align 4, !tbaa !3
  %188 = load i32, i32* %ml2, align 4, !tbaa !7
  %add.ptr155 = getelementptr inbounds i8, i8* %187, i32 %188
  %add.ptr156 = getelementptr inbounds i8, i8* %add.ptr155, i32 -4
  %cmp157 = icmp ugt i8* %add.ptr154, %add.ptr156
  br i1 %cmp157, label %if.then159, label %if.end165

if.then159:                                       ; preds = %if.end153
  %189 = load i8*, i8** %start2, align 4, !tbaa !3
  %190 = load i8*, i8** %ip, align 4, !tbaa !3
  %sub.ptr.lhs.cast160 = ptrtoint i8* %189 to i32
  %sub.ptr.rhs.cast161 = ptrtoint i8* %190 to i32
  %sub.ptr.sub162 = sub i32 %sub.ptr.lhs.cast160, %sub.ptr.rhs.cast161
  %191 = load i32, i32* %ml2, align 4, !tbaa !7
  %add163 = add nsw i32 %sub.ptr.sub162, %191
  %sub164 = sub nsw i32 %add163, 4
  store i32 %sub164, i32* %ml, align 4, !tbaa !7
  br label %if.end165

if.end165:                                        ; preds = %if.then159, %if.end153
  %192 = load i32, i32* %ml, align 4, !tbaa !7
  %193 = load i8*, i8** %start2, align 4, !tbaa !3
  %194 = load i8*, i8** %ip, align 4, !tbaa !3
  %sub.ptr.lhs.cast166 = ptrtoint i8* %193 to i32
  %sub.ptr.rhs.cast167 = ptrtoint i8* %194 to i32
  %sub.ptr.sub168 = sub i32 %sub.ptr.lhs.cast166, %sub.ptr.rhs.cast167
  %sub169 = sub nsw i32 %192, %sub.ptr.sub168
  store i32 %sub169, i32* %correction149, align 4, !tbaa !7
  %195 = load i32, i32* %correction149, align 4, !tbaa !7
  %cmp170 = icmp sgt i32 %195, 0
  br i1 %cmp170, label %if.then172, label %if.end176

if.then172:                                       ; preds = %if.end165
  %196 = load i32, i32* %correction149, align 4, !tbaa !7
  %197 = load i8*, i8** %start2, align 4, !tbaa !3
  %add.ptr173 = getelementptr inbounds i8, i8* %197, i32 %196
  store i8* %add.ptr173, i8** %start2, align 4, !tbaa !3
  %198 = load i32, i32* %correction149, align 4, !tbaa !7
  %199 = load i8*, i8** %ref2, align 4, !tbaa !3
  %add.ptr174 = getelementptr inbounds i8, i8* %199, i32 %198
  store i8* %add.ptr174, i8** %ref2, align 4, !tbaa !3
  %200 = load i32, i32* %correction149, align 4, !tbaa !7
  %201 = load i32, i32* %ml2, align 4, !tbaa !7
  %sub175 = sub nsw i32 %201, %200
  store i32 %sub175, i32* %ml2, align 4, !tbaa !7
  br label %if.end176

if.end176:                                        ; preds = %if.then172, %if.end165
  %202 = bitcast i32* %correction149 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #7
  br label %if.end181

if.else177:                                       ; preds = %if.then142
  %203 = load i8*, i8** %start2, align 4, !tbaa !3
  %204 = load i8*, i8** %ip, align 4, !tbaa !3
  %sub.ptr.lhs.cast178 = ptrtoint i8* %203 to i32
  %sub.ptr.rhs.cast179 = ptrtoint i8* %204 to i32
  %sub.ptr.sub180 = sub i32 %sub.ptr.lhs.cast178, %sub.ptr.rhs.cast179
  store i32 %sub.ptr.sub180, i32* %ml, align 4, !tbaa !7
  br label %if.end181

if.end181:                                        ; preds = %if.else177, %if.end176
  br label %if.end182

if.end182:                                        ; preds = %if.end181, %if.end138
  %205 = load i8*, i8** %op, align 4, !tbaa !3
  store i8* %205, i8** %optr, align 4, !tbaa !3
  %206 = load i32, i32* %ml, align 4, !tbaa !7
  %207 = load i8*, i8** %ref, align 4, !tbaa !3
  %208 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %209 = load i8*, i8** %oend, align 4, !tbaa !3
  %call183 = call i32 @LZ4HC_encodeSequence(i8** %ip, i8** %op, i8** %anchor, i32 %206, i8* %207, i32 %208, i8* %209)
  %tobool184 = icmp ne i32 %call183, 0
  br i1 %tobool184, label %if.then185, label %if.end186

if.then185:                                       ; preds = %if.end182
  br label %_dest_overflow

if.end186:                                        ; preds = %if.end182
  %210 = load i8*, i8** %start2, align 4, !tbaa !3
  store i8* %210, i8** %ip, align 4, !tbaa !3
  %211 = load i8*, i8** %ref2, align 4, !tbaa !3
  store i8* %211, i8** %ref, align 4, !tbaa !3
  %212 = load i32, i32* %ml2, align 4, !tbaa !7
  store i32 %212, i32* %ml, align 4, !tbaa !7
  %213 = load i8*, i8** %start3, align 4, !tbaa !3
  store i8* %213, i8** %start2, align 4, !tbaa !3
  %214 = load i8*, i8** %ref3, align 4, !tbaa !3
  store i8* %214, i8** %ref2, align 4, !tbaa !3
  %215 = load i32, i32* %ml3, align 4, !tbaa !7
  store i32 %215, i32* %ml2, align 4, !tbaa !7
  br label %_Search3

while.end:                                        ; preds = %while.cond
  br label %_last_literals

_last_literals:                                   ; preds = %if.then244, %while.end, %if.then9
  %216 = bitcast i32* %lastRunSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %216) #7
  %217 = load i8*, i8** %iend, align 4, !tbaa !3
  %218 = load i8*, i8** %anchor, align 4, !tbaa !3
  %sub.ptr.lhs.cast187 = ptrtoint i8* %217 to i32
  %sub.ptr.rhs.cast188 = ptrtoint i8* %218 to i32
  %sub.ptr.sub189 = sub i32 %sub.ptr.lhs.cast187, %sub.ptr.rhs.cast188
  store i32 %sub.ptr.sub189, i32* %lastRunSize, align 4, !tbaa !14
  %219 = bitcast i32* %litLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %219) #7
  %220 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  %add190 = add i32 %220, 255
  %sub191 = sub i32 %add190, 15
  %div = udiv i32 %sub191, 255
  store i32 %div, i32* %litLength, align 4, !tbaa !14
  %221 = bitcast i32* %totalSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %221) #7
  %222 = load i32, i32* %litLength, align 4, !tbaa !14
  %add192 = add i32 1, %222
  %223 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  %add193 = add i32 %add192, %223
  store i32 %add193, i32* %totalSize, align 4, !tbaa !14
  %224 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %cmp194 = icmp eq i32 %224, 2
  br i1 %cmp194, label %if.then196, label %if.end198

if.then196:                                       ; preds = %_last_literals
  %225 = load i8*, i8** %oend, align 4, !tbaa !3
  %add.ptr197 = getelementptr inbounds i8, i8* %225, i32 5
  store i8* %add.ptr197, i8** %oend, align 4, !tbaa !3
  br label %if.end198

if.end198:                                        ; preds = %if.then196, %_last_literals
  %226 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %tobool199 = icmp ne i32 %226, 0
  br i1 %tobool199, label %land.lhs.true, label %if.end216

land.lhs.true:                                    ; preds = %if.end198
  %227 = load i8*, i8** %op, align 4, !tbaa !3
  %228 = load i32, i32* %totalSize, align 4, !tbaa !14
  %add.ptr200 = getelementptr inbounds i8, i8* %227, i32 %228
  %229 = load i8*, i8** %oend, align 4, !tbaa !3
  %cmp201 = icmp ugt i8* %add.ptr200, %229
  br i1 %cmp201, label %if.then203, label %if.end216

if.then203:                                       ; preds = %land.lhs.true
  %230 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %cmp204 = icmp eq i32 %230, 1
  br i1 %cmp204, label %if.then206, label %if.end207

if.then206:                                       ; preds = %if.then203
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end207:                                        ; preds = %if.then203
  %231 = load i8*, i8** %oend, align 4, !tbaa !3
  %232 = load i8*, i8** %op, align 4, !tbaa !3
  %sub.ptr.lhs.cast208 = ptrtoint i8* %231 to i32
  %sub.ptr.rhs.cast209 = ptrtoint i8* %232 to i32
  %sub.ptr.sub210 = sub i32 %sub.ptr.lhs.cast208, %sub.ptr.rhs.cast209
  %sub211 = sub i32 %sub.ptr.sub210, 1
  store i32 %sub211, i32* %lastRunSize, align 4, !tbaa !14
  %233 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  %add212 = add i32 %233, 255
  %sub213 = sub i32 %add212, 15
  %div214 = udiv i32 %sub213, 255
  store i32 %div214, i32* %litLength, align 4, !tbaa !14
  %234 = load i32, i32* %litLength, align 4, !tbaa !14
  %235 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  %sub215 = sub i32 %235, %234
  store i32 %sub215, i32* %lastRunSize, align 4, !tbaa !14
  br label %if.end216

if.end216:                                        ; preds = %if.end207, %land.lhs.true, %if.end198
  %236 = load i8*, i8** %anchor, align 4, !tbaa !3
  %237 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  %add.ptr217 = getelementptr inbounds i8, i8* %236, i32 %237
  store i8* %add.ptr217, i8** %ip, align 4, !tbaa !3
  %238 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  %cmp218 = icmp uge i32 %238, 15
  br i1 %cmp218, label %if.then220, label %if.else229

if.then220:                                       ; preds = %if.end216
  %239 = bitcast i32* %accumulator to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %239) #7
  %240 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  %sub221 = sub i32 %240, 15
  store i32 %sub221, i32* %accumulator, align 4, !tbaa !14
  %241 = load i8*, i8** %op, align 4, !tbaa !3
  %incdec.ptr222 = getelementptr inbounds i8, i8* %241, i32 1
  store i8* %incdec.ptr222, i8** %op, align 4, !tbaa !3
  store i8 -16, i8* %241, align 1, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then220
  %242 = load i32, i32* %accumulator, align 4, !tbaa !14
  %cmp223 = icmp uge i32 %242, 255
  br i1 %cmp223, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %243 = load i8*, i8** %op, align 4, !tbaa !3
  %incdec.ptr225 = getelementptr inbounds i8, i8* %243, i32 1
  store i8* %incdec.ptr225, i8** %op, align 4, !tbaa !3
  store i8 -1, i8* %243, align 1, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %244 = load i32, i32* %accumulator, align 4, !tbaa !14
  %sub226 = sub i32 %244, 255
  store i32 %sub226, i32* %accumulator, align 4, !tbaa !14
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %245 = load i32, i32* %accumulator, align 4, !tbaa !14
  %conv227 = trunc i32 %245 to i8
  %246 = load i8*, i8** %op, align 4, !tbaa !3
  %incdec.ptr228 = getelementptr inbounds i8, i8* %246, i32 1
  store i8* %incdec.ptr228, i8** %op, align 4, !tbaa !3
  store i8 %conv227, i8* %246, align 1, !tbaa !9
  %247 = bitcast i32* %accumulator to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %247) #7
  br label %if.end232

if.else229:                                       ; preds = %if.end216
  %248 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  %shl = shl i32 %248, 4
  %conv230 = trunc i32 %shl to i8
  %249 = load i8*, i8** %op, align 4, !tbaa !3
  %incdec.ptr231 = getelementptr inbounds i8, i8* %249, i32 1
  store i8* %incdec.ptr231, i8** %op, align 4, !tbaa !3
  store i8 %conv230, i8* %249, align 1, !tbaa !9
  br label %if.end232

if.end232:                                        ; preds = %if.else229, %for.end
  %250 = load i8*, i8** %op, align 4, !tbaa !3
  %251 = load i8*, i8** %anchor, align 4, !tbaa !3
  %252 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %250, i8* align 1 %251, i32 %252, i1 false)
  %253 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  %254 = load i8*, i8** %op, align 4, !tbaa !3
  %add.ptr233 = getelementptr inbounds i8, i8* %254, i32 %253
  store i8* %add.ptr233, i8** %op, align 4, !tbaa !3
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end232, %if.then206
  %255 = bitcast i32* %totalSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %255) #7
  %256 = bitcast i32* %litLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %256) #7
  %257 = bitcast i32* %lastRunSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %257) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup246 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  %258 = load i8*, i8** %ip, align 4, !tbaa !3
  %259 = load i8*, i8** %source.addr, align 4, !tbaa !3
  %sub.ptr.lhs.cast236 = ptrtoint i8* %258 to i32
  %sub.ptr.rhs.cast237 = ptrtoint i8* %259 to i32
  %sub.ptr.sub238 = sub i32 %sub.ptr.lhs.cast236, %sub.ptr.rhs.cast237
  %260 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  store i32 %sub.ptr.sub238, i32* %260, align 4, !tbaa !7
  %261 = load i8*, i8** %op, align 4, !tbaa !3
  %262 = load i8*, i8** %dest.addr, align 4, !tbaa !3
  %sub.ptr.lhs.cast239 = ptrtoint i8* %261 to i32
  %sub.ptr.rhs.cast240 = ptrtoint i8* %262 to i32
  %sub.ptr.sub241 = sub i32 %sub.ptr.lhs.cast239, %sub.ptr.rhs.cast240
  store i32 %sub.ptr.sub241, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup246

_dest_overflow:                                   ; preds = %if.then185, %if.then135, %if.then104, %if.then100, %if.then30
  %263 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %cmp242 = icmp eq i32 %263, 2
  br i1 %cmp242, label %if.then244, label %if.end245

if.then244:                                       ; preds = %_dest_overflow
  %264 = load i8*, i8** %optr, align 4, !tbaa !3
  store i8* %264, i8** %op, align 4, !tbaa !3
  br label %_last_literals

if.end245:                                        ; preds = %_dest_overflow
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup246

cleanup246:                                       ; preds = %if.end245, %cleanup.cont, %cleanup
  %265 = bitcast i8** %ref3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %265) #7
  %266 = bitcast i8** %start3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %266) #7
  %267 = bitcast i8** %ref2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %267) #7
  %268 = bitcast i8** %start2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %268) #7
  %269 = bitcast i8** %ref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %269) #7
  %270 = bitcast i8** %ref0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %270) #7
  %271 = bitcast i8** %start0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %271) #7
  %272 = bitcast i32* %ml3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %272) #7
  %273 = bitcast i32* %ml2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %273) #7
  %274 = bitcast i32* %ml to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %274) #7
  %275 = bitcast i32* %ml0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %275) #7
  %276 = bitcast i8** %oend to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %276) #7
  %277 = bitcast i8** %op to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %277) #7
  %278 = bitcast i8** %optr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %278) #7
  %279 = bitcast i8** %matchlimit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %279) #7
  %280 = bitcast i8** %mflimit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %280) #7
  %281 = bitcast i8** %iend to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %281) #7
  %282 = bitcast i8** %anchor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %282) #7
  %283 = bitcast i8** %ip to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %283) #7
  %284 = bitcast i32* %patternAnalysis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %284) #7
  %285 = bitcast i32* %inputSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %285) #7
  %286 = load i32, i32* %retval, align 4
  ret i32 %286
}

; Function Attrs: nounwind
define internal i32 @LZ4HC_compress_optimal(%struct.LZ4HC_CCtx_internal* %ctx, i8* %source, i8* %dst, i32* %srcSizePtr, i32 %dstCapacity, i32 %nbSearches, i32 %sufficient_len, i32 %limit, i32 %fullUpdate, i32 %dict, i32 %favorDecSpeed) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.LZ4HC_CCtx_internal*, align 4
  %source.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSizePtr.addr = alloca i32*, align 4
  %dstCapacity.addr = alloca i32, align 4
  %nbSearches.addr = alloca i32, align 4
  %sufficient_len.addr = alloca i32, align 4
  %limit.addr = alloca i32, align 4
  %fullUpdate.addr = alloca i32, align 4
  %dict.addr = alloca i32, align 4
  %favorDecSpeed.addr = alloca i32, align 4
  %retval1 = alloca i32, align 4
  %opt = alloca %struct.LZ4HC_optimal_t*, align 4
  %ip = alloca i8*, align 4
  %anchor = alloca i8*, align 4
  %iend = alloca i8*, align 4
  %mflimit = alloca i8*, align 4
  %matchlimit = alloca i8*, align 4
  %op = alloca i8*, align 4
  %opSaved = alloca i8*, align 4
  %oend = alloca i8*, align 4
  %llen = alloca i32, align 4
  %best_mlen = alloca i32, align 4
  %best_off = alloca i32, align 4
  %cur = alloca i32, align 4
  %last_match_pos = alloca i32, align 4
  %firstMatch = alloca %struct.LZ4HC_match_t, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %firstML = alloca i32, align 4
  %matchPos = alloca i8*, align 4
  %rPos = alloca i32, align 4
  %cost = alloca i32, align 4
  %mlen33 = alloca i32, align 4
  %matchML = alloca i32, align 4
  %offset = alloca i32, align 4
  %cost39 = alloca i32, align 4
  %addLit = alloca i32, align 4
  %curPtr = alloca i8*, align 4
  %newMatch = alloca %struct.LZ4HC_match_t, align 4
  %tmp = alloca %struct.LZ4HC_match_t, align 4
  %tmp111 = alloca %struct.LZ4HC_match_t, align 4
  %baseLitlen = alloca i32, align 4
  %litlen129 = alloca i32, align 4
  %price133 = alloca i32, align 4
  %pos = alloca i32, align 4
  %matchML159 = alloca i32, align 4
  %ml = alloca i32, align 4
  %pos164 = alloca i32, align 4
  %offset166 = alloca i32, align 4
  %price168 = alloca i32, align 4
  %ll = alloca i32, align 4
  %addLit212 = alloca i32, align 4
  %candidate_pos = alloca i32, align 4
  %selected_matchLength = alloca i32, align 4
  %selected_offset = alloca i32, align 4
  %next_matchLength = alloca i32, align 4
  %next_offset = alloca i32, align 4
  %rPos263 = alloca i32, align 4
  %ml267 = alloca i32, align 4
  %offset270 = alloca i32, align 4
  %lastRunSize = alloca i32, align 4
  %litLength = alloca i32, align 4
  %totalSize = alloca i32, align 4
  %accumulator = alloca i32, align 4
  store %struct.LZ4HC_CCtx_internal* %ctx, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  store i8* %source, i8** %source.addr, align 4, !tbaa !3
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !3
  store i32* %srcSizePtr, i32** %srcSizePtr.addr, align 4, !tbaa !3
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !7
  store i32 %nbSearches, i32* %nbSearches.addr, align 4, !tbaa !7
  store i32 %sufficient_len, i32* %sufficient_len.addr, align 4, !tbaa !14
  store i32 %limit, i32* %limit.addr, align 4, !tbaa !9
  store i32 %fullUpdate, i32* %fullUpdate.addr, align 4, !tbaa !7
  store i32 %dict, i32* %dict.addr, align 4, !tbaa !9
  store i32 %favorDecSpeed, i32* %favorDecSpeed.addr, align 4, !tbaa !9
  %0 = bitcast i32* %retval1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 0, i32* %retval1, align 4, !tbaa !7
  %1 = bitcast %struct.LZ4HC_optimal_t** %opt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %call = call i8* @malloc(i32 65584)
  %2 = bitcast i8* %call to %struct.LZ4HC_optimal_t*
  store %struct.LZ4HC_optimal_t* %2, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %3 = bitcast i8** %ip to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i8*, i8** %source.addr, align 4, !tbaa !3
  store i8* %4, i8** %ip, align 4, !tbaa !3
  %5 = bitcast i8** %anchor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load i8*, i8** %ip, align 4, !tbaa !3
  store i8* %6, i8** %anchor, align 4, !tbaa !3
  %7 = bitcast i8** %iend to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = load i8*, i8** %ip, align 4, !tbaa !3
  %9 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  %10 = load i32, i32* %9, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds i8, i8* %8, i32 %10
  store i8* %add.ptr, i8** %iend, align 4, !tbaa !3
  %11 = bitcast i8** %mflimit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %12 = load i8*, i8** %iend, align 4, !tbaa !3
  %add.ptr2 = getelementptr inbounds i8, i8* %12, i32 -12
  store i8* %add.ptr2, i8** %mflimit, align 4, !tbaa !3
  %13 = bitcast i8** %matchlimit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i8*, i8** %iend, align 4, !tbaa !3
  %add.ptr3 = getelementptr inbounds i8, i8* %14, i32 -5
  store i8* %add.ptr3, i8** %matchlimit, align 4, !tbaa !3
  %15 = bitcast i8** %op to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  %16 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  store i8* %16, i8** %op, align 4, !tbaa !3
  %17 = bitcast i8** %opSaved to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #7
  %18 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  store i8* %18, i8** %opSaved, align 4, !tbaa !3
  %19 = bitcast i8** %oend to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #7
  %20 = load i8*, i8** %op, align 4, !tbaa !3
  %21 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !7
  %add.ptr4 = getelementptr inbounds i8, i8* %20, i32 %21
  store i8* %add.ptr4, i8** %oend, align 4, !tbaa !3
  %22 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %cmp = icmp eq %struct.LZ4HC_optimal_t* %22, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %_return_label

if.end:                                           ; preds = %entry
  %23 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  store i32 0, i32* %23, align 4, !tbaa !7
  %24 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %cmp5 = icmp eq i32 %24, 2
  br i1 %cmp5, label %if.then6, label %if.end8

if.then6:                                         ; preds = %if.end
  %25 = load i8*, i8** %oend, align 4, !tbaa !3
  %add.ptr7 = getelementptr inbounds i8, i8* %25, i32 -5
  store i8* %add.ptr7, i8** %oend, align 4, !tbaa !3
  br label %if.end8

if.end8:                                          ; preds = %if.then6, %if.end
  %26 = load i32, i32* %sufficient_len.addr, align 4, !tbaa !14
  %cmp9 = icmp uge i32 %26, 4096
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end8
  store i32 4095, i32* %sufficient_len.addr, align 4, !tbaa !14
  br label %if.end11

if.end11:                                         ; preds = %if.then10, %if.end8
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont300, %cleanup293, %if.end11
  %27 = load i8*, i8** %ip, align 4, !tbaa !3
  %28 = load i8*, i8** %mflimit, align 4, !tbaa !3
  %cmp12 = icmp ule i8* %27, %28
  br i1 %cmp12, label %while.body, label %while.end301

while.body:                                       ; preds = %while.cond
  %29 = bitcast i32* %llen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #7
  %30 = load i8*, i8** %ip, align 4, !tbaa !3
  %31 = load i8*, i8** %anchor, align 4, !tbaa !3
  %sub.ptr.lhs.cast = ptrtoint i8* %30 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %31 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %llen, align 4, !tbaa !7
  %32 = bitcast i32* %best_mlen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #7
  %33 = bitcast i32* %best_off to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #7
  %34 = bitcast i32* %cur to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #7
  %35 = bitcast i32* %last_match_pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #7
  store i32 0, i32* %last_match_pos, align 4, !tbaa !7
  %36 = bitcast %struct.LZ4HC_match_t* %firstMatch to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %36) #7
  %37 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %38 = load i8*, i8** %ip, align 4, !tbaa !3
  %39 = load i8*, i8** %matchlimit, align 4, !tbaa !3
  %40 = load i32, i32* %nbSearches.addr, align 4, !tbaa !7
  %41 = load i32, i32* %dict.addr, align 4, !tbaa !9
  %42 = load i32, i32* %favorDecSpeed.addr, align 4, !tbaa !9
  call void @LZ4HC_FindLongerMatch(%struct.LZ4HC_match_t* sret align 4 %firstMatch, %struct.LZ4HC_CCtx_internal* %37, i8* %38, i8* %39, i32 3, i32 %40, i32 %41, i32 %42)
  %len = getelementptr inbounds %struct.LZ4HC_match_t, %struct.LZ4HC_match_t* %firstMatch, i32 0, i32 1
  %43 = load i32, i32* %len, align 4, !tbaa !30
  %cmp13 = icmp eq i32 %43, 0
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %while.body
  %44 = load i8*, i8** %ip, align 4, !tbaa !3
  %incdec.ptr = getelementptr inbounds i8, i8* %44, i32 1
  store i8* %incdec.ptr, i8** %ip, align 4, !tbaa !3
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup293

if.end15:                                         ; preds = %while.body
  %len16 = getelementptr inbounds %struct.LZ4HC_match_t, %struct.LZ4HC_match_t* %firstMatch, i32 0, i32 1
  %45 = load i32, i32* %len16, align 4, !tbaa !30
  %46 = load i32, i32* %sufficient_len.addr, align 4, !tbaa !14
  %cmp17 = icmp ugt i32 %45, %46
  br i1 %cmp17, label %if.then18, label %if.end25

if.then18:                                        ; preds = %if.end15
  %47 = bitcast i32* %firstML to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #7
  %len19 = getelementptr inbounds %struct.LZ4HC_match_t, %struct.LZ4HC_match_t* %firstMatch, i32 0, i32 1
  %48 = load i32, i32* %len19, align 4, !tbaa !30
  store i32 %48, i32* %firstML, align 4, !tbaa !7
  %49 = bitcast i8** %matchPos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #7
  %50 = load i8*, i8** %ip, align 4, !tbaa !3
  %off = getelementptr inbounds %struct.LZ4HC_match_t, %struct.LZ4HC_match_t* %firstMatch, i32 0, i32 0
  %51 = load i32, i32* %off, align 4, !tbaa !32
  %idx.neg = sub i32 0, %51
  %add.ptr20 = getelementptr inbounds i8, i8* %50, i32 %idx.neg
  store i8* %add.ptr20, i8** %matchPos, align 4, !tbaa !3
  %52 = load i8*, i8** %op, align 4, !tbaa !3
  store i8* %52, i8** %opSaved, align 4, !tbaa !3
  %53 = load i32, i32* %firstML, align 4, !tbaa !7
  %54 = load i8*, i8** %matchPos, align 4, !tbaa !3
  %55 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %56 = load i8*, i8** %oend, align 4, !tbaa !3
  %call21 = call i32 @LZ4HC_encodeSequence(i8** %ip, i8** %op, i8** %anchor, i32 %53, i8* %54, i32 %55, i8* %56)
  %tobool = icmp ne i32 %call21, 0
  br i1 %tobool, label %if.then22, label %if.end23

if.then22:                                        ; preds = %if.then18
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end23:                                         ; preds = %if.then18
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.then22, %if.end23
  %57 = bitcast i8** %matchPos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #7
  %58 = bitcast i32* %firstML to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #7
  br label %cleanup293

if.end25:                                         ; preds = %if.end15
  %59 = bitcast i32* %rPos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #7
  store i32 0, i32* %rPos, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end25
  %60 = load i32, i32* %rPos, align 4, !tbaa !7
  %cmp26 = icmp slt i32 %60, 4
  br i1 %cmp26, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %61 = bitcast i32* %cost to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #7
  %62 = load i32, i32* %llen, align 4, !tbaa !7
  %63 = load i32, i32* %rPos, align 4, !tbaa !7
  %add = add nsw i32 %62, %63
  %call27 = call i32 @LZ4HC_literalsPrice(i32 %add)
  store i32 %call27, i32* %cost, align 4, !tbaa !7
  %64 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %65 = load i32, i32* %rPos, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %64, i32 %65
  %mlen = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx, i32 0, i32 2
  store i32 1, i32* %mlen, align 4, !tbaa !33
  %66 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %67 = load i32, i32* %rPos, align 4, !tbaa !7
  %arrayidx28 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %66, i32 %67
  %off29 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx28, i32 0, i32 1
  store i32 0, i32* %off29, align 4, !tbaa !35
  %68 = load i32, i32* %llen, align 4, !tbaa !7
  %69 = load i32, i32* %rPos, align 4, !tbaa !7
  %add30 = add nsw i32 %68, %69
  %70 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %71 = load i32, i32* %rPos, align 4, !tbaa !7
  %arrayidx31 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %70, i32 %71
  %litlen = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx31, i32 0, i32 3
  store i32 %add30, i32* %litlen, align 4, !tbaa !36
  %72 = load i32, i32* %cost, align 4, !tbaa !7
  %73 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %74 = load i32, i32* %rPos, align 4, !tbaa !7
  %arrayidx32 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %73, i32 %74
  %price = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx32, i32 0, i32 0
  store i32 %72, i32* %price, align 4, !tbaa !37
  %75 = bitcast i32* %cost to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %76 = load i32, i32* %rPos, align 4, !tbaa !7
  %inc = add nsw i32 %76, 1
  store i32 %inc, i32* %rPos, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %77 = bitcast i32* %rPos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #7
  %78 = bitcast i32* %mlen33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #7
  store i32 4, i32* %mlen33, align 4, !tbaa !7
  %79 = bitcast i32* %matchML to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #7
  %len34 = getelementptr inbounds %struct.LZ4HC_match_t, %struct.LZ4HC_match_t* %firstMatch, i32 0, i32 1
  %80 = load i32, i32* %len34, align 4, !tbaa !30
  store i32 %80, i32* %matchML, align 4, !tbaa !7
  %81 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #7
  %off35 = getelementptr inbounds %struct.LZ4HC_match_t, %struct.LZ4HC_match_t* %firstMatch, i32 0, i32 0
  %82 = load i32, i32* %off35, align 4, !tbaa !32
  store i32 %82, i32* %offset, align 4, !tbaa !7
  br label %for.cond36

for.cond36:                                       ; preds = %for.inc49, %for.end
  %83 = load i32, i32* %mlen33, align 4, !tbaa !7
  %84 = load i32, i32* %matchML, align 4, !tbaa !7
  %cmp37 = icmp sle i32 %83, %84
  br i1 %cmp37, label %for.body38, label %for.end51

for.body38:                                       ; preds = %for.cond36
  %85 = bitcast i32* %cost39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #7
  %86 = load i32, i32* %llen, align 4, !tbaa !7
  %87 = load i32, i32* %mlen33, align 4, !tbaa !7
  %call40 = call i32 @LZ4HC_sequencePrice(i32 %86, i32 %87)
  store i32 %call40, i32* %cost39, align 4, !tbaa !7
  %88 = load i32, i32* %mlen33, align 4, !tbaa !7
  %89 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %90 = load i32, i32* %mlen33, align 4, !tbaa !7
  %arrayidx41 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %89, i32 %90
  %mlen42 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx41, i32 0, i32 2
  store i32 %88, i32* %mlen42, align 4, !tbaa !33
  %91 = load i32, i32* %offset, align 4, !tbaa !7
  %92 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %93 = load i32, i32* %mlen33, align 4, !tbaa !7
  %arrayidx43 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %92, i32 %93
  %off44 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx43, i32 0, i32 1
  store i32 %91, i32* %off44, align 4, !tbaa !35
  %94 = load i32, i32* %llen, align 4, !tbaa !7
  %95 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %96 = load i32, i32* %mlen33, align 4, !tbaa !7
  %arrayidx45 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %95, i32 %96
  %litlen46 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx45, i32 0, i32 3
  store i32 %94, i32* %litlen46, align 4, !tbaa !36
  %97 = load i32, i32* %cost39, align 4, !tbaa !7
  %98 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %99 = load i32, i32* %mlen33, align 4, !tbaa !7
  %arrayidx47 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %98, i32 %99
  %price48 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx47, i32 0, i32 0
  store i32 %97, i32* %price48, align 4, !tbaa !37
  %100 = bitcast i32* %cost39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #7
  br label %for.inc49

for.inc49:                                        ; preds = %for.body38
  %101 = load i32, i32* %mlen33, align 4, !tbaa !7
  %inc50 = add nsw i32 %101, 1
  store i32 %inc50, i32* %mlen33, align 4, !tbaa !7
  br label %for.cond36

for.end51:                                        ; preds = %for.cond36
  %102 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #7
  %103 = bitcast i32* %matchML to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #7
  %104 = bitcast i32* %mlen33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #7
  %len52 = getelementptr inbounds %struct.LZ4HC_match_t, %struct.LZ4HC_match_t* %firstMatch, i32 0, i32 1
  %105 = load i32, i32* %len52, align 4, !tbaa !30
  store i32 %105, i32* %last_match_pos, align 4, !tbaa !7
  %106 = bitcast i32* %addLit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %106) #7
  store i32 1, i32* %addLit, align 4, !tbaa !7
  br label %for.cond53

for.cond53:                                       ; preds = %for.inc72, %for.end51
  %107 = load i32, i32* %addLit, align 4, !tbaa !7
  %cmp54 = icmp sle i32 %107, 3
  br i1 %cmp54, label %for.body55, label %for.end74

for.body55:                                       ; preds = %for.cond53
  %108 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %109 = load i32, i32* %last_match_pos, align 4, !tbaa !7
  %110 = load i32, i32* %addLit, align 4, !tbaa !7
  %add56 = add nsw i32 %109, %110
  %arrayidx57 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %108, i32 %add56
  %mlen58 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx57, i32 0, i32 2
  store i32 1, i32* %mlen58, align 4, !tbaa !33
  %111 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %112 = load i32, i32* %last_match_pos, align 4, !tbaa !7
  %113 = load i32, i32* %addLit, align 4, !tbaa !7
  %add59 = add nsw i32 %112, %113
  %arrayidx60 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %111, i32 %add59
  %off61 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx60, i32 0, i32 1
  store i32 0, i32* %off61, align 4, !tbaa !35
  %114 = load i32, i32* %addLit, align 4, !tbaa !7
  %115 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %116 = load i32, i32* %last_match_pos, align 4, !tbaa !7
  %117 = load i32, i32* %addLit, align 4, !tbaa !7
  %add62 = add nsw i32 %116, %117
  %arrayidx63 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %115, i32 %add62
  %litlen64 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx63, i32 0, i32 3
  store i32 %114, i32* %litlen64, align 4, !tbaa !36
  %118 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %119 = load i32, i32* %last_match_pos, align 4, !tbaa !7
  %arrayidx65 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %118, i32 %119
  %price66 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx65, i32 0, i32 0
  %120 = load i32, i32* %price66, align 4, !tbaa !37
  %121 = load i32, i32* %addLit, align 4, !tbaa !7
  %call67 = call i32 @LZ4HC_literalsPrice(i32 %121)
  %add68 = add nsw i32 %120, %call67
  %122 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %123 = load i32, i32* %last_match_pos, align 4, !tbaa !7
  %124 = load i32, i32* %addLit, align 4, !tbaa !7
  %add69 = add nsw i32 %123, %124
  %arrayidx70 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %122, i32 %add69
  %price71 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx70, i32 0, i32 0
  store i32 %add68, i32* %price71, align 4, !tbaa !37
  br label %for.inc72

for.inc72:                                        ; preds = %for.body55
  %125 = load i32, i32* %addLit, align 4, !tbaa !7
  %inc73 = add nsw i32 %125, 1
  store i32 %inc73, i32* %addLit, align 4, !tbaa !7
  br label %for.cond53

for.end74:                                        ; preds = %for.cond53
  %126 = bitcast i32* %addLit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #7
  store i32 1, i32* %cur, align 4, !tbaa !7
  br label %for.cond75

for.cond75:                                       ; preds = %for.inc237, %for.end74
  %127 = load i32, i32* %cur, align 4, !tbaa !7
  %128 = load i32, i32* %last_match_pos, align 4, !tbaa !7
  %cmp76 = icmp slt i32 %127, %128
  br i1 %cmp76, label %for.body77, label %for.end239

for.body77:                                       ; preds = %for.cond75
  %129 = bitcast i8** %curPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %129) #7
  %130 = load i8*, i8** %ip, align 4, !tbaa !3
  %131 = load i32, i32* %cur, align 4, !tbaa !7
  %add.ptr78 = getelementptr inbounds i8, i8* %130, i32 %131
  store i8* %add.ptr78, i8** %curPtr, align 4, !tbaa !3
  %132 = bitcast %struct.LZ4HC_match_t* %newMatch to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %132) #7
  %133 = load i8*, i8** %curPtr, align 4, !tbaa !3
  %134 = load i8*, i8** %mflimit, align 4, !tbaa !3
  %cmp79 = icmp ugt i8* %133, %134
  br i1 %cmp79, label %if.then80, label %if.end81

if.then80:                                        ; preds = %for.body77
  store i32 15, i32* %cleanup.dest.slot, align 4
  br label %cleanup235

if.end81:                                         ; preds = %for.body77
  %135 = load i32, i32* %fullUpdate.addr, align 4, !tbaa !7
  %tobool82 = icmp ne i32 %135, 0
  br i1 %tobool82, label %if.then83, label %if.else

if.then83:                                        ; preds = %if.end81
  %136 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %137 = load i32, i32* %cur, align 4, !tbaa !7
  %add84 = add nsw i32 %137, 1
  %arrayidx85 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %136, i32 %add84
  %price86 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx85, i32 0, i32 0
  %138 = load i32, i32* %price86, align 4, !tbaa !37
  %139 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %140 = load i32, i32* %cur, align 4, !tbaa !7
  %arrayidx87 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %139, i32 %140
  %price88 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx87, i32 0, i32 0
  %141 = load i32, i32* %price88, align 4, !tbaa !37
  %cmp89 = icmp sle i32 %138, %141
  br i1 %cmp89, label %land.lhs.true, label %if.end98

land.lhs.true:                                    ; preds = %if.then83
  %142 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %143 = load i32, i32* %cur, align 4, !tbaa !7
  %add90 = add nsw i32 %143, 4
  %arrayidx91 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %142, i32 %add90
  %price92 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx91, i32 0, i32 0
  %144 = load i32, i32* %price92, align 4, !tbaa !37
  %145 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %146 = load i32, i32* %cur, align 4, !tbaa !7
  %arrayidx93 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %145, i32 %146
  %price94 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx93, i32 0, i32 0
  %147 = load i32, i32* %price94, align 4, !tbaa !37
  %add95 = add nsw i32 %147, 3
  %cmp96 = icmp slt i32 %144, %add95
  br i1 %cmp96, label %if.then97, label %if.end98

if.then97:                                        ; preds = %land.lhs.true
  store i32 17, i32* %cleanup.dest.slot, align 4
  br label %cleanup235

if.end98:                                         ; preds = %land.lhs.true, %if.then83
  br label %if.end107

if.else:                                          ; preds = %if.end81
  %148 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %149 = load i32, i32* %cur, align 4, !tbaa !7
  %add99 = add nsw i32 %149, 1
  %arrayidx100 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %148, i32 %add99
  %price101 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx100, i32 0, i32 0
  %150 = load i32, i32* %price101, align 4, !tbaa !37
  %151 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %152 = load i32, i32* %cur, align 4, !tbaa !7
  %arrayidx102 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %151, i32 %152
  %price103 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx102, i32 0, i32 0
  %153 = load i32, i32* %price103, align 4, !tbaa !37
  %cmp104 = icmp sle i32 %150, %153
  br i1 %cmp104, label %if.then105, label %if.end106

if.then105:                                       ; preds = %if.else
  store i32 17, i32* %cleanup.dest.slot, align 4
  br label %cleanup235

if.end106:                                        ; preds = %if.else
  br label %if.end107

if.end107:                                        ; preds = %if.end106, %if.end98
  %154 = load i32, i32* %fullUpdate.addr, align 4, !tbaa !7
  %tobool108 = icmp ne i32 %154, 0
  br i1 %tobool108, label %if.then109, label %if.else110

if.then109:                                       ; preds = %if.end107
  %155 = bitcast %struct.LZ4HC_match_t* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %155) #7
  %156 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %157 = load i8*, i8** %curPtr, align 4, !tbaa !3
  %158 = load i8*, i8** %matchlimit, align 4, !tbaa !3
  %159 = load i32, i32* %nbSearches.addr, align 4, !tbaa !7
  %160 = load i32, i32* %dict.addr, align 4, !tbaa !9
  %161 = load i32, i32* %favorDecSpeed.addr, align 4, !tbaa !9
  call void @LZ4HC_FindLongerMatch(%struct.LZ4HC_match_t* sret align 4 %tmp, %struct.LZ4HC_CCtx_internal* %156, i8* %157, i8* %158, i32 3, i32 %159, i32 %160, i32 %161)
  %162 = bitcast %struct.LZ4HC_match_t* %newMatch to i8*
  %163 = bitcast %struct.LZ4HC_match_t* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %162, i8* align 4 %163, i32 8, i1 false), !tbaa.struct !38
  %164 = bitcast %struct.LZ4HC_match_t* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %164) #7
  br label %if.end112

if.else110:                                       ; preds = %if.end107
  %165 = bitcast %struct.LZ4HC_match_t* %tmp111 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %165) #7
  %166 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %167 = load i8*, i8** %curPtr, align 4, !tbaa !3
  %168 = load i8*, i8** %matchlimit, align 4, !tbaa !3
  %169 = load i32, i32* %last_match_pos, align 4, !tbaa !7
  %170 = load i32, i32* %cur, align 4, !tbaa !7
  %sub = sub nsw i32 %169, %170
  %171 = load i32, i32* %nbSearches.addr, align 4, !tbaa !7
  %172 = load i32, i32* %dict.addr, align 4, !tbaa !9
  %173 = load i32, i32* %favorDecSpeed.addr, align 4, !tbaa !9
  call void @LZ4HC_FindLongerMatch(%struct.LZ4HC_match_t* sret align 4 %tmp111, %struct.LZ4HC_CCtx_internal* %166, i8* %167, i8* %168, i32 %sub, i32 %171, i32 %172, i32 %173)
  %174 = bitcast %struct.LZ4HC_match_t* %newMatch to i8*
  %175 = bitcast %struct.LZ4HC_match_t* %tmp111 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %174, i8* align 4 %175, i32 8, i1 false), !tbaa.struct !38
  %176 = bitcast %struct.LZ4HC_match_t* %tmp111 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %176) #7
  br label %if.end112

if.end112:                                        ; preds = %if.else110, %if.then109
  %len113 = getelementptr inbounds %struct.LZ4HC_match_t, %struct.LZ4HC_match_t* %newMatch, i32 0, i32 1
  %177 = load i32, i32* %len113, align 4, !tbaa !30
  %tobool114 = icmp ne i32 %177, 0
  br i1 %tobool114, label %if.end116, label %if.then115

if.then115:                                       ; preds = %if.end112
  store i32 17, i32* %cleanup.dest.slot, align 4
  br label %cleanup235

if.end116:                                        ; preds = %if.end112
  %len117 = getelementptr inbounds %struct.LZ4HC_match_t, %struct.LZ4HC_match_t* %newMatch, i32 0, i32 1
  %178 = load i32, i32* %len117, align 4, !tbaa !30
  %179 = load i32, i32* %sufficient_len.addr, align 4, !tbaa !14
  %cmp118 = icmp ugt i32 %178, %179
  br i1 %cmp118, label %if.then122, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end116
  %len119 = getelementptr inbounds %struct.LZ4HC_match_t, %struct.LZ4HC_match_t* %newMatch, i32 0, i32 1
  %180 = load i32, i32* %len119, align 4, !tbaa !30
  %181 = load i32, i32* %cur, align 4, !tbaa !7
  %add120 = add nsw i32 %180, %181
  %cmp121 = icmp sge i32 %add120, 4096
  br i1 %cmp121, label %if.then122, label %if.end126

if.then122:                                       ; preds = %lor.lhs.false, %if.end116
  %len123 = getelementptr inbounds %struct.LZ4HC_match_t, %struct.LZ4HC_match_t* %newMatch, i32 0, i32 1
  %182 = load i32, i32* %len123, align 4, !tbaa !30
  store i32 %182, i32* %best_mlen, align 4, !tbaa !7
  %off124 = getelementptr inbounds %struct.LZ4HC_match_t, %struct.LZ4HC_match_t* %newMatch, i32 0, i32 0
  %183 = load i32, i32* %off124, align 4, !tbaa !32
  store i32 %183, i32* %best_off, align 4, !tbaa !7
  %184 = load i32, i32* %cur, align 4, !tbaa !7
  %add125 = add nsw i32 %184, 1
  store i32 %add125, i32* %last_match_pos, align 4, !tbaa !7
  store i32 18, i32* %cleanup.dest.slot, align 4
  br label %cleanup235

if.end126:                                        ; preds = %lor.lhs.false
  %185 = bitcast i32* %baseLitlen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %185) #7
  %186 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %187 = load i32, i32* %cur, align 4, !tbaa !7
  %arrayidx127 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %186, i32 %187
  %litlen128 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx127, i32 0, i32 3
  %188 = load i32, i32* %litlen128, align 4, !tbaa !36
  store i32 %188, i32* %baseLitlen, align 4, !tbaa !7
  %189 = bitcast i32* %litlen129 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %189) #7
  store i32 1, i32* %litlen129, align 4, !tbaa !7
  br label %for.cond130

for.cond130:                                      ; preds = %for.inc156, %if.end126
  %190 = load i32, i32* %litlen129, align 4, !tbaa !7
  %cmp131 = icmp slt i32 %190, 4
  br i1 %cmp131, label %for.body132, label %for.end158

for.body132:                                      ; preds = %for.cond130
  %191 = bitcast i32* %price133 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %191) #7
  %192 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %193 = load i32, i32* %cur, align 4, !tbaa !7
  %arrayidx134 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %192, i32 %193
  %price135 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx134, i32 0, i32 0
  %194 = load i32, i32* %price135, align 4, !tbaa !37
  %195 = load i32, i32* %baseLitlen, align 4, !tbaa !7
  %call136 = call i32 @LZ4HC_literalsPrice(i32 %195)
  %sub137 = sub nsw i32 %194, %call136
  %196 = load i32, i32* %baseLitlen, align 4, !tbaa !7
  %197 = load i32, i32* %litlen129, align 4, !tbaa !7
  %add138 = add nsw i32 %196, %197
  %call139 = call i32 @LZ4HC_literalsPrice(i32 %add138)
  %add140 = add nsw i32 %sub137, %call139
  store i32 %add140, i32* %price133, align 4, !tbaa !7
  %198 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %198) #7
  %199 = load i32, i32* %cur, align 4, !tbaa !7
  %200 = load i32, i32* %litlen129, align 4, !tbaa !7
  %add141 = add nsw i32 %199, %200
  store i32 %add141, i32* %pos, align 4, !tbaa !7
  %201 = load i32, i32* %price133, align 4, !tbaa !7
  %202 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %203 = load i32, i32* %pos, align 4, !tbaa !7
  %arrayidx142 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %202, i32 %203
  %price143 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx142, i32 0, i32 0
  %204 = load i32, i32* %price143, align 4, !tbaa !37
  %cmp144 = icmp slt i32 %201, %204
  br i1 %cmp144, label %if.then145, label %if.end155

if.then145:                                       ; preds = %for.body132
  %205 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %206 = load i32, i32* %pos, align 4, !tbaa !7
  %arrayidx146 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %205, i32 %206
  %mlen147 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx146, i32 0, i32 2
  store i32 1, i32* %mlen147, align 4, !tbaa !33
  %207 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %208 = load i32, i32* %pos, align 4, !tbaa !7
  %arrayidx148 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %207, i32 %208
  %off149 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx148, i32 0, i32 1
  store i32 0, i32* %off149, align 4, !tbaa !35
  %209 = load i32, i32* %baseLitlen, align 4, !tbaa !7
  %210 = load i32, i32* %litlen129, align 4, !tbaa !7
  %add150 = add nsw i32 %209, %210
  %211 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %212 = load i32, i32* %pos, align 4, !tbaa !7
  %arrayidx151 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %211, i32 %212
  %litlen152 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx151, i32 0, i32 3
  store i32 %add150, i32* %litlen152, align 4, !tbaa !36
  %213 = load i32, i32* %price133, align 4, !tbaa !7
  %214 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %215 = load i32, i32* %pos, align 4, !tbaa !7
  %arrayidx153 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %214, i32 %215
  %price154 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx153, i32 0, i32 0
  store i32 %213, i32* %price154, align 4, !tbaa !37
  br label %if.end155

if.end155:                                        ; preds = %if.then145, %for.body132
  %216 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #7
  %217 = bitcast i32* %price133 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #7
  br label %for.inc156

for.inc156:                                       ; preds = %if.end155
  %218 = load i32, i32* %litlen129, align 4, !tbaa !7
  %inc157 = add nsw i32 %218, 1
  store i32 %inc157, i32* %litlen129, align 4, !tbaa !7
  br label %for.cond130

for.end158:                                       ; preds = %for.cond130
  %219 = bitcast i32* %litlen129 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #7
  %220 = bitcast i32* %baseLitlen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #7
  %221 = bitcast i32* %matchML159 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %221) #7
  %len160 = getelementptr inbounds %struct.LZ4HC_match_t, %struct.LZ4HC_match_t* %newMatch, i32 0, i32 1
  %222 = load i32, i32* %len160, align 4, !tbaa !30
  store i32 %222, i32* %matchML159, align 4, !tbaa !7
  %223 = bitcast i32* %ml to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %223) #7
  store i32 4, i32* %ml, align 4, !tbaa !7
  br label %for.cond161

for.cond161:                                      ; preds = %for.inc209, %for.end158
  %224 = load i32, i32* %ml, align 4, !tbaa !7
  %225 = load i32, i32* %matchML159, align 4, !tbaa !7
  %cmp162 = icmp sle i32 %224, %225
  br i1 %cmp162, label %for.body163, label %for.end211

for.body163:                                      ; preds = %for.cond161
  %226 = bitcast i32* %pos164 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %226) #7
  %227 = load i32, i32* %cur, align 4, !tbaa !7
  %228 = load i32, i32* %ml, align 4, !tbaa !7
  %add165 = add nsw i32 %227, %228
  store i32 %add165, i32* %pos164, align 4, !tbaa !7
  %229 = bitcast i32* %offset166 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %229) #7
  %off167 = getelementptr inbounds %struct.LZ4HC_match_t, %struct.LZ4HC_match_t* %newMatch, i32 0, i32 0
  %230 = load i32, i32* %off167, align 4, !tbaa !32
  store i32 %230, i32* %offset166, align 4, !tbaa !7
  %231 = bitcast i32* %price168 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %231) #7
  %232 = bitcast i32* %ll to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %232) #7
  %233 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %234 = load i32, i32* %cur, align 4, !tbaa !7
  %arrayidx169 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %233, i32 %234
  %mlen170 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx169, i32 0, i32 2
  %235 = load i32, i32* %mlen170, align 4, !tbaa !33
  %cmp171 = icmp eq i32 %235, 1
  br i1 %cmp171, label %if.then172, label %if.else181

if.then172:                                       ; preds = %for.body163
  %236 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %237 = load i32, i32* %cur, align 4, !tbaa !7
  %arrayidx173 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %236, i32 %237
  %litlen174 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx173, i32 0, i32 3
  %238 = load i32, i32* %litlen174, align 4, !tbaa !36
  store i32 %238, i32* %ll, align 4, !tbaa !7
  %239 = load i32, i32* %cur, align 4, !tbaa !7
  %240 = load i32, i32* %ll, align 4, !tbaa !7
  %cmp175 = icmp sgt i32 %239, %240
  br i1 %cmp175, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then172
  %241 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %242 = load i32, i32* %cur, align 4, !tbaa !7
  %243 = load i32, i32* %ll, align 4, !tbaa !7
  %sub176 = sub nsw i32 %242, %243
  %arrayidx177 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %241, i32 %sub176
  %price178 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx177, i32 0, i32 0
  %244 = load i32, i32* %price178, align 4, !tbaa !37
  br label %cond.end

cond.false:                                       ; preds = %if.then172
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %244, %cond.true ], [ 0, %cond.false ]
  %245 = load i32, i32* %ll, align 4, !tbaa !7
  %246 = load i32, i32* %ml, align 4, !tbaa !7
  %call179 = call i32 @LZ4HC_sequencePrice(i32 %245, i32 %246)
  %add180 = add nsw i32 %cond, %call179
  store i32 %add180, i32* %price168, align 4, !tbaa !7
  br label %if.end186

if.else181:                                       ; preds = %for.body163
  store i32 0, i32* %ll, align 4, !tbaa !7
  %247 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %248 = load i32, i32* %cur, align 4, !tbaa !7
  %arrayidx182 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %247, i32 %248
  %price183 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx182, i32 0, i32 0
  %249 = load i32, i32* %price183, align 4, !tbaa !37
  %250 = load i32, i32* %ml, align 4, !tbaa !7
  %call184 = call i32 @LZ4HC_sequencePrice(i32 0, i32 %250)
  %add185 = add nsw i32 %249, %call184
  store i32 %add185, i32* %price168, align 4, !tbaa !7
  br label %if.end186

if.end186:                                        ; preds = %if.else181, %cond.end
  %251 = load i32, i32* %pos164, align 4, !tbaa !7
  %252 = load i32, i32* %last_match_pos, align 4, !tbaa !7
  %add187 = add nsw i32 %252, 3
  %cmp188 = icmp sgt i32 %251, %add187
  br i1 %cmp188, label %if.then194, label %lor.lhs.false189

lor.lhs.false189:                                 ; preds = %if.end186
  %253 = load i32, i32* %price168, align 4, !tbaa !7
  %254 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %255 = load i32, i32* %pos164, align 4, !tbaa !7
  %arrayidx190 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %254, i32 %255
  %price191 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx190, i32 0, i32 0
  %256 = load i32, i32* %price191, align 4, !tbaa !37
  %257 = load i32, i32* %favorDecSpeed.addr, align 4, !tbaa !9
  %sub192 = sub nsw i32 %256, %257
  %cmp193 = icmp sle i32 %253, %sub192
  br i1 %cmp193, label %if.then194, label %if.end208

if.then194:                                       ; preds = %lor.lhs.false189, %if.end186
  %258 = load i32, i32* %ml, align 4, !tbaa !7
  %259 = load i32, i32* %matchML159, align 4, !tbaa !7
  %cmp195 = icmp eq i32 %258, %259
  br i1 %cmp195, label %land.lhs.true196, label %if.end199

land.lhs.true196:                                 ; preds = %if.then194
  %260 = load i32, i32* %last_match_pos, align 4, !tbaa !7
  %261 = load i32, i32* %pos164, align 4, !tbaa !7
  %cmp197 = icmp slt i32 %260, %261
  br i1 %cmp197, label %if.then198, label %if.end199

if.then198:                                       ; preds = %land.lhs.true196
  %262 = load i32, i32* %pos164, align 4, !tbaa !7
  store i32 %262, i32* %last_match_pos, align 4, !tbaa !7
  br label %if.end199

if.end199:                                        ; preds = %if.then198, %land.lhs.true196, %if.then194
  %263 = load i32, i32* %ml, align 4, !tbaa !7
  %264 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %265 = load i32, i32* %pos164, align 4, !tbaa !7
  %arrayidx200 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %264, i32 %265
  %mlen201 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx200, i32 0, i32 2
  store i32 %263, i32* %mlen201, align 4, !tbaa !33
  %266 = load i32, i32* %offset166, align 4, !tbaa !7
  %267 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %268 = load i32, i32* %pos164, align 4, !tbaa !7
  %arrayidx202 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %267, i32 %268
  %off203 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx202, i32 0, i32 1
  store i32 %266, i32* %off203, align 4, !tbaa !35
  %269 = load i32, i32* %ll, align 4, !tbaa !7
  %270 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %271 = load i32, i32* %pos164, align 4, !tbaa !7
  %arrayidx204 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %270, i32 %271
  %litlen205 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx204, i32 0, i32 3
  store i32 %269, i32* %litlen205, align 4, !tbaa !36
  %272 = load i32, i32* %price168, align 4, !tbaa !7
  %273 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %274 = load i32, i32* %pos164, align 4, !tbaa !7
  %arrayidx206 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %273, i32 %274
  %price207 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx206, i32 0, i32 0
  store i32 %272, i32* %price207, align 4, !tbaa !37
  br label %if.end208

if.end208:                                        ; preds = %if.end199, %lor.lhs.false189
  %275 = bitcast i32* %ll to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %275) #7
  %276 = bitcast i32* %price168 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %276) #7
  %277 = bitcast i32* %offset166 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %277) #7
  %278 = bitcast i32* %pos164 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %278) #7
  br label %for.inc209

for.inc209:                                       ; preds = %if.end208
  %279 = load i32, i32* %ml, align 4, !tbaa !7
  %inc210 = add nsw i32 %279, 1
  store i32 %inc210, i32* %ml, align 4, !tbaa !7
  br label %for.cond161

for.end211:                                       ; preds = %for.cond161
  %280 = bitcast i32* %ml to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %280) #7
  %281 = bitcast i32* %matchML159 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %281) #7
  %282 = bitcast i32* %addLit212 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %282) #7
  store i32 1, i32* %addLit212, align 4, !tbaa !7
  br label %for.cond213

for.cond213:                                      ; preds = %for.inc232, %for.end211
  %283 = load i32, i32* %addLit212, align 4, !tbaa !7
  %cmp214 = icmp sle i32 %283, 3
  br i1 %cmp214, label %for.body215, label %for.end234

for.body215:                                      ; preds = %for.cond213
  %284 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %285 = load i32, i32* %last_match_pos, align 4, !tbaa !7
  %286 = load i32, i32* %addLit212, align 4, !tbaa !7
  %add216 = add nsw i32 %285, %286
  %arrayidx217 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %284, i32 %add216
  %mlen218 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx217, i32 0, i32 2
  store i32 1, i32* %mlen218, align 4, !tbaa !33
  %287 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %288 = load i32, i32* %last_match_pos, align 4, !tbaa !7
  %289 = load i32, i32* %addLit212, align 4, !tbaa !7
  %add219 = add nsw i32 %288, %289
  %arrayidx220 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %287, i32 %add219
  %off221 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx220, i32 0, i32 1
  store i32 0, i32* %off221, align 4, !tbaa !35
  %290 = load i32, i32* %addLit212, align 4, !tbaa !7
  %291 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %292 = load i32, i32* %last_match_pos, align 4, !tbaa !7
  %293 = load i32, i32* %addLit212, align 4, !tbaa !7
  %add222 = add nsw i32 %292, %293
  %arrayidx223 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %291, i32 %add222
  %litlen224 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx223, i32 0, i32 3
  store i32 %290, i32* %litlen224, align 4, !tbaa !36
  %294 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %295 = load i32, i32* %last_match_pos, align 4, !tbaa !7
  %arrayidx225 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %294, i32 %295
  %price226 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx225, i32 0, i32 0
  %296 = load i32, i32* %price226, align 4, !tbaa !37
  %297 = load i32, i32* %addLit212, align 4, !tbaa !7
  %call227 = call i32 @LZ4HC_literalsPrice(i32 %297)
  %add228 = add nsw i32 %296, %call227
  %298 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %299 = load i32, i32* %last_match_pos, align 4, !tbaa !7
  %300 = load i32, i32* %addLit212, align 4, !tbaa !7
  %add229 = add nsw i32 %299, %300
  %arrayidx230 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %298, i32 %add229
  %price231 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx230, i32 0, i32 0
  store i32 %add228, i32* %price231, align 4, !tbaa !37
  br label %for.inc232

for.inc232:                                       ; preds = %for.body215
  %301 = load i32, i32* %addLit212, align 4, !tbaa !7
  %inc233 = add nsw i32 %301, 1
  store i32 %inc233, i32* %addLit212, align 4, !tbaa !7
  br label %for.cond213

for.end234:                                       ; preds = %for.cond213
  %302 = bitcast i32* %addLit212 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %302) #7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup235

cleanup235:                                       ; preds = %if.then122, %for.end234, %if.then115, %if.then105, %if.then97, %if.then80
  %303 = bitcast %struct.LZ4HC_match_t* %newMatch to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %303) #7
  %304 = bitcast i8** %curPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %304) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup293 [
    i32 0, label %cleanup.cont
    i32 15, label %for.end239
    i32 17, label %for.inc237
    i32 18, label %encode
  ]

cleanup.cont:                                     ; preds = %cleanup235
  br label %for.inc237

for.inc237:                                       ; preds = %cleanup.cont, %cleanup235
  %305 = load i32, i32* %cur, align 4, !tbaa !7
  %inc238 = add nsw i32 %305, 1
  store i32 %inc238, i32* %cur, align 4, !tbaa !7
  br label %for.cond75

for.end239:                                       ; preds = %cleanup235, %for.cond75
  %306 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %307 = load i32, i32* %last_match_pos, align 4, !tbaa !7
  %arrayidx240 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %306, i32 %307
  %mlen241 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx240, i32 0, i32 2
  %308 = load i32, i32* %mlen241, align 4, !tbaa !33
  store i32 %308, i32* %best_mlen, align 4, !tbaa !7
  %309 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %310 = load i32, i32* %last_match_pos, align 4, !tbaa !7
  %arrayidx242 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %309, i32 %310
  %off243 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx242, i32 0, i32 1
  %311 = load i32, i32* %off243, align 4, !tbaa !35
  store i32 %311, i32* %best_off, align 4, !tbaa !7
  %312 = load i32, i32* %last_match_pos, align 4, !tbaa !7
  %313 = load i32, i32* %best_mlen, align 4, !tbaa !7
  %sub244 = sub nsw i32 %312, %313
  store i32 %sub244, i32* %cur, align 4, !tbaa !7
  br label %encode

encode:                                           ; preds = %for.end239, %cleanup235
  %314 = bitcast i32* %candidate_pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %314) #7
  %315 = load i32, i32* %cur, align 4, !tbaa !7
  store i32 %315, i32* %candidate_pos, align 4, !tbaa !7
  %316 = bitcast i32* %selected_matchLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %316) #7
  %317 = load i32, i32* %best_mlen, align 4, !tbaa !7
  store i32 %317, i32* %selected_matchLength, align 4, !tbaa !7
  %318 = bitcast i32* %selected_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %318) #7
  %319 = load i32, i32* %best_off, align 4, !tbaa !7
  store i32 %319, i32* %selected_offset, align 4, !tbaa !7
  br label %while.cond245

while.cond245:                                    ; preds = %cleanup.cont262, %encode
  br label %while.body246

while.body246:                                    ; preds = %while.cond245
  %320 = bitcast i32* %next_matchLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %320) #7
  %321 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %322 = load i32, i32* %candidate_pos, align 4, !tbaa !7
  %arrayidx247 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %321, i32 %322
  %mlen248 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx247, i32 0, i32 2
  %323 = load i32, i32* %mlen248, align 4, !tbaa !33
  store i32 %323, i32* %next_matchLength, align 4, !tbaa !7
  %324 = bitcast i32* %next_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %324) #7
  %325 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %326 = load i32, i32* %candidate_pos, align 4, !tbaa !7
  %arrayidx249 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %325, i32 %326
  %off250 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx249, i32 0, i32 1
  %327 = load i32, i32* %off250, align 4, !tbaa !35
  store i32 %327, i32* %next_offset, align 4, !tbaa !7
  %328 = load i32, i32* %selected_matchLength, align 4, !tbaa !7
  %329 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %330 = load i32, i32* %candidate_pos, align 4, !tbaa !7
  %arrayidx251 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %329, i32 %330
  %mlen252 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx251, i32 0, i32 2
  store i32 %328, i32* %mlen252, align 4, !tbaa !33
  %331 = load i32, i32* %selected_offset, align 4, !tbaa !7
  %332 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %333 = load i32, i32* %candidate_pos, align 4, !tbaa !7
  %arrayidx253 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %332, i32 %333
  %off254 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx253, i32 0, i32 1
  store i32 %331, i32* %off254, align 4, !tbaa !35
  %334 = load i32, i32* %next_matchLength, align 4, !tbaa !7
  store i32 %334, i32* %selected_matchLength, align 4, !tbaa !7
  %335 = load i32, i32* %next_offset, align 4, !tbaa !7
  store i32 %335, i32* %selected_offset, align 4, !tbaa !7
  %336 = load i32, i32* %next_matchLength, align 4, !tbaa !7
  %337 = load i32, i32* %candidate_pos, align 4, !tbaa !7
  %cmp255 = icmp sgt i32 %336, %337
  br i1 %cmp255, label %if.then256, label %if.end257

if.then256:                                       ; preds = %while.body246
  store i32 29, i32* %cleanup.dest.slot, align 4
  br label %cleanup259

if.end257:                                        ; preds = %while.body246
  %338 = load i32, i32* %next_matchLength, align 4, !tbaa !7
  %339 = load i32, i32* %candidate_pos, align 4, !tbaa !7
  %sub258 = sub nsw i32 %339, %338
  store i32 %sub258, i32* %candidate_pos, align 4, !tbaa !7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup259

cleanup259:                                       ; preds = %if.end257, %if.then256
  %340 = bitcast i32* %next_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %340) #7
  %341 = bitcast i32* %next_matchLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %341) #7
  %cleanup.dest261 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest261, label %unreachable [
    i32 0, label %cleanup.cont262
    i32 29, label %while.end
  ]

cleanup.cont262:                                  ; preds = %cleanup259
  br label %while.cond245

while.end:                                        ; preds = %cleanup259
  %342 = bitcast i32* %selected_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %342) #7
  %343 = bitcast i32* %selected_matchLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %343) #7
  %344 = bitcast i32* %candidate_pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %344) #7
  %345 = bitcast i32* %rPos263 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %345) #7
  store i32 0, i32* %rPos263, align 4, !tbaa !7
  br label %while.cond264

while.cond264:                                    ; preds = %cleanup.cont288, %cleanup285, %while.end
  %346 = load i32, i32* %rPos263, align 4, !tbaa !7
  %347 = load i32, i32* %last_match_pos, align 4, !tbaa !7
  %cmp265 = icmp slt i32 %346, %347
  br i1 %cmp265, label %while.body266, label %while.end289

while.body266:                                    ; preds = %while.cond264
  %348 = bitcast i32* %ml267 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %348) #7
  %349 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %350 = load i32, i32* %rPos263, align 4, !tbaa !7
  %arrayidx268 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %349, i32 %350
  %mlen269 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx268, i32 0, i32 2
  %351 = load i32, i32* %mlen269, align 4, !tbaa !33
  store i32 %351, i32* %ml267, align 4, !tbaa !7
  %352 = bitcast i32* %offset270 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %352) #7
  %353 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %354 = load i32, i32* %rPos263, align 4, !tbaa !7
  %arrayidx271 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %353, i32 %354
  %off272 = getelementptr inbounds %struct.LZ4HC_optimal_t, %struct.LZ4HC_optimal_t* %arrayidx271, i32 0, i32 1
  %355 = load i32, i32* %off272, align 4, !tbaa !35
  store i32 %355, i32* %offset270, align 4, !tbaa !7
  %356 = load i32, i32* %ml267, align 4, !tbaa !7
  %cmp273 = icmp eq i32 %356, 1
  br i1 %cmp273, label %if.then274, label %if.end277

if.then274:                                       ; preds = %while.body266
  %357 = load i8*, i8** %ip, align 4, !tbaa !3
  %incdec.ptr275 = getelementptr inbounds i8, i8* %357, i32 1
  store i8* %incdec.ptr275, i8** %ip, align 4, !tbaa !3
  %358 = load i32, i32* %rPos263, align 4, !tbaa !7
  %inc276 = add nsw i32 %358, 1
  store i32 %inc276, i32* %rPos263, align 4, !tbaa !7
  store i32 30, i32* %cleanup.dest.slot, align 4
  br label %cleanup285

if.end277:                                        ; preds = %while.body266
  %359 = load i32, i32* %ml267, align 4, !tbaa !7
  %360 = load i32, i32* %rPos263, align 4, !tbaa !7
  %add278 = add nsw i32 %360, %359
  store i32 %add278, i32* %rPos263, align 4, !tbaa !7
  %361 = load i8*, i8** %op, align 4, !tbaa !3
  store i8* %361, i8** %opSaved, align 4, !tbaa !3
  %362 = load i32, i32* %ml267, align 4, !tbaa !7
  %363 = load i8*, i8** %ip, align 4, !tbaa !3
  %364 = load i32, i32* %offset270, align 4, !tbaa !7
  %idx.neg279 = sub i32 0, %364
  %add.ptr280 = getelementptr inbounds i8, i8* %363, i32 %idx.neg279
  %365 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %366 = load i8*, i8** %oend, align 4, !tbaa !3
  %call281 = call i32 @LZ4HC_encodeSequence(i8** %ip, i8** %op, i8** %anchor, i32 %362, i8* %add.ptr280, i32 %365, i8* %366)
  %tobool282 = icmp ne i32 %call281, 0
  br i1 %tobool282, label %if.then283, label %if.end284

if.then283:                                       ; preds = %if.end277
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup285

if.end284:                                        ; preds = %if.end277
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup285

cleanup285:                                       ; preds = %if.then283, %if.end284, %if.then274
  %367 = bitcast i32* %offset270 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %367) #7
  %368 = bitcast i32* %ml267 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %368) #7
  %cleanup.dest287 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest287, label %cleanup290 [
    i32 0, label %cleanup.cont288
    i32 30, label %while.cond264
  ]

cleanup.cont288:                                  ; preds = %cleanup285
  br label %while.cond264

while.end289:                                     ; preds = %while.cond264
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup290

cleanup290:                                       ; preds = %while.end289, %cleanup285
  %369 = bitcast i32* %rPos263 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %369) #7
  %cleanup.dest291 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest291, label %cleanup293 [
    i32 0, label %cleanup.cont292
  ]

cleanup.cont292:                                  ; preds = %cleanup290
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup293

cleanup293:                                       ; preds = %cleanup.cont292, %cleanup290, %cleanup235, %cleanup, %if.then14
  %370 = bitcast %struct.LZ4HC_match_t* %firstMatch to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %370) #7
  %371 = bitcast i32* %last_match_pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %371) #7
  %372 = bitcast i32* %cur to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %372) #7
  %373 = bitcast i32* %best_off to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %373) #7
  %374 = bitcast i32* %best_mlen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %374) #7
  %375 = bitcast i32* %llen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %375) #7
  %cleanup.dest299 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest299, label %cleanup363 [
    i32 0, label %cleanup.cont300
    i32 3, label %while.cond
    i32 5, label %_dest_overflow
  ]

cleanup.cont300:                                  ; preds = %cleanup293
  br label %while.cond

while.end301:                                     ; preds = %while.cond
  br label %_last_literals

_last_literals:                                   ; preds = %if.then361, %while.end301
  %376 = bitcast i32* %lastRunSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %376) #7
  %377 = load i8*, i8** %iend, align 4, !tbaa !3
  %378 = load i8*, i8** %anchor, align 4, !tbaa !3
  %sub.ptr.lhs.cast302 = ptrtoint i8* %377 to i32
  %sub.ptr.rhs.cast303 = ptrtoint i8* %378 to i32
  %sub.ptr.sub304 = sub i32 %sub.ptr.lhs.cast302, %sub.ptr.rhs.cast303
  store i32 %sub.ptr.sub304, i32* %lastRunSize, align 4, !tbaa !14
  %379 = bitcast i32* %litLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %379) #7
  %380 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  %add305 = add i32 %380, 255
  %sub306 = sub i32 %add305, 15
  %div = udiv i32 %sub306, 255
  store i32 %div, i32* %litLength, align 4, !tbaa !14
  %381 = bitcast i32* %totalSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %381) #7
  %382 = load i32, i32* %litLength, align 4, !tbaa !14
  %add307 = add i32 1, %382
  %383 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  %add308 = add i32 %add307, %383
  store i32 %add308, i32* %totalSize, align 4, !tbaa !14
  %384 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %cmp309 = icmp eq i32 %384, 2
  br i1 %cmp309, label %if.then310, label %if.end312

if.then310:                                       ; preds = %_last_literals
  %385 = load i8*, i8** %oend, align 4, !tbaa !3
  %add.ptr311 = getelementptr inbounds i8, i8* %385, i32 5
  store i8* %add.ptr311, i8** %oend, align 4, !tbaa !3
  br label %if.end312

if.end312:                                        ; preds = %if.then310, %_last_literals
  %386 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %tobool313 = icmp ne i32 %386, 0
  br i1 %tobool313, label %land.lhs.true314, label %if.end329

land.lhs.true314:                                 ; preds = %if.end312
  %387 = load i8*, i8** %op, align 4, !tbaa !3
  %388 = load i32, i32* %totalSize, align 4, !tbaa !14
  %add.ptr315 = getelementptr inbounds i8, i8* %387, i32 %388
  %389 = load i8*, i8** %oend, align 4, !tbaa !3
  %cmp316 = icmp ugt i8* %add.ptr315, %389
  br i1 %cmp316, label %if.then317, label %if.end329

if.then317:                                       ; preds = %land.lhs.true314
  %390 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %cmp318 = icmp eq i32 %390, 1
  br i1 %cmp318, label %if.then319, label %if.end320

if.then319:                                       ; preds = %if.then317
  store i32 0, i32* %retval1, align 4, !tbaa !7
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup348

if.end320:                                        ; preds = %if.then317
  %391 = load i8*, i8** %oend, align 4, !tbaa !3
  %392 = load i8*, i8** %op, align 4, !tbaa !3
  %sub.ptr.lhs.cast321 = ptrtoint i8* %391 to i32
  %sub.ptr.rhs.cast322 = ptrtoint i8* %392 to i32
  %sub.ptr.sub323 = sub i32 %sub.ptr.lhs.cast321, %sub.ptr.rhs.cast322
  %sub324 = sub i32 %sub.ptr.sub323, 1
  store i32 %sub324, i32* %lastRunSize, align 4, !tbaa !14
  %393 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  %add325 = add i32 %393, 255
  %sub326 = sub i32 %add325, 15
  %div327 = udiv i32 %sub326, 255
  store i32 %div327, i32* %litLength, align 4, !tbaa !14
  %394 = load i32, i32* %litLength, align 4, !tbaa !14
  %395 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  %sub328 = sub i32 %395, %394
  store i32 %sub328, i32* %lastRunSize, align 4, !tbaa !14
  br label %if.end329

if.end329:                                        ; preds = %if.end320, %land.lhs.true314, %if.end312
  %396 = load i8*, i8** %anchor, align 4, !tbaa !3
  %397 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  %add.ptr330 = getelementptr inbounds i8, i8* %396, i32 %397
  store i8* %add.ptr330, i8** %ip, align 4, !tbaa !3
  %398 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  %cmp331 = icmp uge i32 %398, 15
  br i1 %cmp331, label %if.then332, label %if.else343

if.then332:                                       ; preds = %if.end329
  %399 = bitcast i32* %accumulator to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %399) #7
  %400 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  %sub333 = sub i32 %400, 15
  store i32 %sub333, i32* %accumulator, align 4, !tbaa !14
  %401 = load i8*, i8** %op, align 4, !tbaa !3
  %incdec.ptr334 = getelementptr inbounds i8, i8* %401, i32 1
  store i8* %incdec.ptr334, i8** %op, align 4, !tbaa !3
  store i8 -16, i8* %401, align 1, !tbaa !9
  br label %for.cond335

for.cond335:                                      ; preds = %for.inc339, %if.then332
  %402 = load i32, i32* %accumulator, align 4, !tbaa !14
  %cmp336 = icmp uge i32 %402, 255
  br i1 %cmp336, label %for.body337, label %for.end341

for.body337:                                      ; preds = %for.cond335
  %403 = load i8*, i8** %op, align 4, !tbaa !3
  %incdec.ptr338 = getelementptr inbounds i8, i8* %403, i32 1
  store i8* %incdec.ptr338, i8** %op, align 4, !tbaa !3
  store i8 -1, i8* %403, align 1, !tbaa !9
  br label %for.inc339

for.inc339:                                       ; preds = %for.body337
  %404 = load i32, i32* %accumulator, align 4, !tbaa !14
  %sub340 = sub i32 %404, 255
  store i32 %sub340, i32* %accumulator, align 4, !tbaa !14
  br label %for.cond335

for.end341:                                       ; preds = %for.cond335
  %405 = load i32, i32* %accumulator, align 4, !tbaa !14
  %conv = trunc i32 %405 to i8
  %406 = load i8*, i8** %op, align 4, !tbaa !3
  %incdec.ptr342 = getelementptr inbounds i8, i8* %406, i32 1
  store i8* %incdec.ptr342, i8** %op, align 4, !tbaa !3
  store i8 %conv, i8* %406, align 1, !tbaa !9
  %407 = bitcast i32* %accumulator to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %407) #7
  br label %if.end346

if.else343:                                       ; preds = %if.end329
  %408 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  %shl = shl i32 %408, 4
  %conv344 = trunc i32 %shl to i8
  %409 = load i8*, i8** %op, align 4, !tbaa !3
  %incdec.ptr345 = getelementptr inbounds i8, i8* %409, i32 1
  store i8* %incdec.ptr345, i8** %op, align 4, !tbaa !3
  store i8 %conv344, i8* %409, align 1, !tbaa !9
  br label %if.end346

if.end346:                                        ; preds = %if.else343, %for.end341
  %410 = load i8*, i8** %op, align 4, !tbaa !3
  %411 = load i8*, i8** %anchor, align 4, !tbaa !3
  %412 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %410, i8* align 1 %411, i32 %412, i1 false)
  %413 = load i32, i32* %lastRunSize, align 4, !tbaa !14
  %414 = load i8*, i8** %op, align 4, !tbaa !3
  %add.ptr347 = getelementptr inbounds i8, i8* %414, i32 %413
  store i8* %add.ptr347, i8** %op, align 4, !tbaa !3
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup348

cleanup348:                                       ; preds = %if.then319, %if.end346
  %415 = bitcast i32* %totalSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %415) #7
  %416 = bitcast i32* %litLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %416) #7
  %417 = bitcast i32* %lastRunSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %417) #7
  %cleanup.dest351 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest351, label %cleanup363 [
    i32 0, label %cleanup.cont352
    i32 2, label %_return_label
  ]

cleanup.cont352:                                  ; preds = %cleanup348
  %418 = load i8*, i8** %ip, align 4, !tbaa !3
  %419 = load i8*, i8** %source.addr, align 4, !tbaa !3
  %sub.ptr.lhs.cast353 = ptrtoint i8* %418 to i32
  %sub.ptr.rhs.cast354 = ptrtoint i8* %419 to i32
  %sub.ptr.sub355 = sub i32 %sub.ptr.lhs.cast353, %sub.ptr.rhs.cast354
  %420 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !3
  store i32 %sub.ptr.sub355, i32* %420, align 4, !tbaa !7
  %421 = load i8*, i8** %op, align 4, !tbaa !3
  %422 = load i8*, i8** %dst.addr, align 4, !tbaa !3
  %sub.ptr.lhs.cast356 = ptrtoint i8* %421 to i32
  %sub.ptr.rhs.cast357 = ptrtoint i8* %422 to i32
  %sub.ptr.sub358 = sub i32 %sub.ptr.lhs.cast356, %sub.ptr.rhs.cast357
  store i32 %sub.ptr.sub358, i32* %retval1, align 4, !tbaa !7
  br label %_return_label

_dest_overflow:                                   ; preds = %cleanup293
  %423 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %cmp359 = icmp eq i32 %423, 2
  br i1 %cmp359, label %if.then361, label %if.end362

if.then361:                                       ; preds = %_dest_overflow
  %424 = load i8*, i8** %opSaved, align 4, !tbaa !3
  store i8* %424, i8** %op, align 4, !tbaa !3
  br label %_last_literals

if.end362:                                        ; preds = %_dest_overflow
  br label %_return_label

_return_label:                                    ; preds = %if.end362, %cleanup348, %cleanup.cont352, %if.then
  %425 = load %struct.LZ4HC_optimal_t*, %struct.LZ4HC_optimal_t** %opt, align 4, !tbaa !3
  %426 = bitcast %struct.LZ4HC_optimal_t* %425 to i8*
  call void @free(i8* %426)
  %427 = load i32, i32* %retval1, align 4, !tbaa !7
  store i32 %427, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup363

cleanup363:                                       ; preds = %_return_label, %cleanup348, %cleanup293
  %428 = bitcast i8** %oend to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %428) #7
  %429 = bitcast i8** %opSaved to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %429) #7
  %430 = bitcast i8** %op to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %430) #7
  %431 = bitcast i8** %matchlimit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %431) #7
  %432 = bitcast i8** %mflimit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %432) #7
  %433 = bitcast i8** %iend to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %433) #7
  %434 = bitcast i8** %anchor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %434) #7
  %435 = bitcast i8** %ip to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %435) #7
  %436 = bitcast %struct.LZ4HC_optimal_t** %opt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %436) #7
  %437 = bitcast i32* %retval1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %437) #7
  %438 = load i32, i32* %retval, align 4
  ret i32 %438

unreachable:                                      ; preds = %cleanup259
  unreachable
}

; Function Attrs: alwaysinline nounwind
define internal i32 @LZ4HC_InsertAndFindBestMatch(%struct.LZ4HC_CCtx_internal* %hc4, i8* %ip, i8* %iLimit, i8** %matchpos, i32 %maxNbAttempts, i32 %patternAnalysis, i32 %dict) #3 {
entry:
  %hc4.addr = alloca %struct.LZ4HC_CCtx_internal*, align 4
  %ip.addr = alloca i8*, align 4
  %iLimit.addr = alloca i8*, align 4
  %matchpos.addr = alloca i8**, align 4
  %maxNbAttempts.addr = alloca i32, align 4
  %patternAnalysis.addr = alloca i32, align 4
  %dict.addr = alloca i32, align 4
  %uselessPtr = alloca i8*, align 4
  store %struct.LZ4HC_CCtx_internal* %hc4, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  store i8* %ip, i8** %ip.addr, align 4, !tbaa !3
  store i8* %iLimit, i8** %iLimit.addr, align 4, !tbaa !3
  store i8** %matchpos, i8*** %matchpos.addr, align 4, !tbaa !3
  store i32 %maxNbAttempts, i32* %maxNbAttempts.addr, align 4, !tbaa !7
  store i32 %patternAnalysis, i32* %patternAnalysis.addr, align 4, !tbaa !7
  store i32 %dict, i32* %dict.addr, align 4, !tbaa !9
  %0 = bitcast i8** %uselessPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  store i8* %1, i8** %uselessPtr, align 4, !tbaa !3
  %2 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %3 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %4 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %5 = load i8*, i8** %iLimit.addr, align 4, !tbaa !3
  %6 = load i8**, i8*** %matchpos.addr, align 4, !tbaa !3
  %7 = load i32, i32* %maxNbAttempts.addr, align 4, !tbaa !7
  %8 = load i32, i32* %patternAnalysis.addr, align 4, !tbaa !7
  %9 = load i32, i32* %dict.addr, align 4, !tbaa !9
  %call = call i32 @LZ4HC_InsertAndGetWiderMatch(%struct.LZ4HC_CCtx_internal* %2, i8* %3, i8* %4, i8* %5, i32 3, i8** %6, i8** %uselessPtr, i32 %7, i32 %8, i32 0, i32 %9, i32 0)
  %10 = bitcast i8** %uselessPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #7
  ret i32 %call
}

; Function Attrs: alwaysinline nounwind
define internal i32 @LZ4HC_InsertAndGetWiderMatch(%struct.LZ4HC_CCtx_internal* %hc4, i8* %ip, i8* %iLowLimit, i8* %iHighLimit, i32 %longest, i8** %matchpos, i8** %startpos, i32 %maxNbAttempts, i32 %patternAnalysis, i32 %chainSwap, i32 %dict, i32 %favorDecSpeed) #3 {
entry:
  %hc4.addr = alloca %struct.LZ4HC_CCtx_internal*, align 4
  %ip.addr = alloca i8*, align 4
  %iLowLimit.addr = alloca i8*, align 4
  %iHighLimit.addr = alloca i8*, align 4
  %longest.addr = alloca i32, align 4
  %matchpos.addr = alloca i8**, align 4
  %startpos.addr = alloca i8**, align 4
  %maxNbAttempts.addr = alloca i32, align 4
  %patternAnalysis.addr = alloca i32, align 4
  %chainSwap.addr = alloca i32, align 4
  %dict.addr = alloca i32, align 4
  %favorDecSpeed.addr = alloca i32, align 4
  %chainTable = alloca i16*, align 4
  %HashTable = alloca i32*, align 4
  %dictCtx = alloca %struct.LZ4HC_CCtx_internal*, align 4
  %base = alloca i8*, align 4
  %dictLimit = alloca i32, align 4
  %lowPrefixPtr = alloca i8*, align 4
  %ipIndex = alloca i32, align 4
  %lowestMatchIndex = alloca i32, align 4
  %dictBase = alloca i8*, align 4
  %lookBackLength = alloca i32, align 4
  %nbAttempts = alloca i32, align 4
  %matchChainPos = alloca i32, align 4
  %pattern = alloca i32, align 4
  %matchIndex = alloca i32, align 4
  %repeat = alloca i32, align 4
  %srcPatternLength = alloca i32, align 4
  %matchLength = alloca i32, align 4
  %matchPtr = alloca i8*, align 4
  %back = alloca i32, align 4
  %matchPtr53 = alloca i8*, align 4
  %dictStart = alloca i8*, align 4
  %back61 = alloca i32, align 4
  %vLimit = alloca i8*, align 4
  %kTrigger = alloca i32, align 4
  %distanceToNextMatch = alloca i32, align 4
  %end = alloca i32, align 4
  %step = alloca i32, align 4
  %accel = alloca i32, align 4
  %pos = alloca i32, align 4
  %candidateDist = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %distNextMatch = alloca i32, align 4
  %matchCandidateIdx = alloca i32, align 4
  %extDict = alloca i32, align 4
  %matchPtr181 = alloca i8*, align 4
  %dictStart192 = alloca i8*, align 4
  %iLimit = alloca i8*, align 4
  %forwardPatternLength = alloca i32, align 4
  %rotatedPattern = alloca i32, align 4
  %lowestMatchPtr = alloca i8*, align 4
  %backLength = alloca i32, align 4
  %currentSegmentLength = alloca i32, align 4
  %rotatedPattern231 = alloca i32, align 4
  %newMatchIndex = alloca i32, align 4
  %newMatchIndex262 = alloca i32, align 4
  %maxML = alloca i32, align 4
  %distToNextPattern = alloca i32, align 4
  %dictEndOffset = alloca i32, align 4
  %dictMatchIndex = alloca i32, align 4
  %matchPtr369 = alloca i8*, align 4
  %mlt = alloca i32, align 4
  %back376 = alloca i32, align 4
  %vLimit377 = alloca i8*, align 4
  %nextOffset = alloca i32, align 4
  store %struct.LZ4HC_CCtx_internal* %hc4, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  store i8* %ip, i8** %ip.addr, align 4, !tbaa !3
  store i8* %iLowLimit, i8** %iLowLimit.addr, align 4, !tbaa !3
  store i8* %iHighLimit, i8** %iHighLimit.addr, align 4, !tbaa !3
  store i32 %longest, i32* %longest.addr, align 4, !tbaa !7
  store i8** %matchpos, i8*** %matchpos.addr, align 4, !tbaa !3
  store i8** %startpos, i8*** %startpos.addr, align 4, !tbaa !3
  store i32 %maxNbAttempts, i32* %maxNbAttempts.addr, align 4, !tbaa !7
  store i32 %patternAnalysis, i32* %patternAnalysis.addr, align 4, !tbaa !7
  store i32 %chainSwap, i32* %chainSwap.addr, align 4, !tbaa !7
  store i32 %dict, i32* %dict.addr, align 4, !tbaa !9
  store i32 %favorDecSpeed, i32* %favorDecSpeed.addr, align 4, !tbaa !9
  %0 = bitcast i16** %chainTable to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %chainTable1 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %1, i32 0, i32 1
  %arraydecay = getelementptr inbounds [65536 x i16], [65536 x i16]* %chainTable1, i32 0, i32 0
  store i16* %arraydecay, i16** %chainTable, align 4, !tbaa !3
  %2 = bitcast i32** %HashTable to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %hashTable = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %3, i32 0, i32 0
  %arraydecay2 = getelementptr inbounds [32768 x i32], [32768 x i32]* %hashTable, i32 0, i32 0
  store i32* %arraydecay2, i32** %HashTable, align 4, !tbaa !3
  %4 = bitcast %struct.LZ4HC_CCtx_internal** %dictCtx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %dictCtx3 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %5, i32 0, i32 11
  %6 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %dictCtx3, align 4, !tbaa !20
  store %struct.LZ4HC_CCtx_internal* %6, %struct.LZ4HC_CCtx_internal** %dictCtx, align 4, !tbaa !3
  %7 = bitcast i8** %base to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %base4 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %8, i32 0, i32 3
  %9 = load i8*, i8** %base4, align 4, !tbaa !13
  store i8* %9, i8** %base, align 4, !tbaa !3
  %10 = bitcast i32* %dictLimit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %dictLimit5 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %11, i32 0, i32 5
  %12 = load i32, i32* %dictLimit5, align 4, !tbaa !18
  store i32 %12, i32* %dictLimit, align 4, !tbaa !7
  %13 = bitcast i8** %lowPrefixPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i8*, i8** %base, align 4, !tbaa !3
  %15 = load i32, i32* %dictLimit, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds i8, i8* %14, i32 %15
  store i8* %add.ptr, i8** %lowPrefixPtr, align 4, !tbaa !3
  %16 = bitcast i32* %ipIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %17 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %18 = load i8*, i8** %base, align 4, !tbaa !3
  %sub.ptr.lhs.cast = ptrtoint i8* %17 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %18 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %ipIndex, align 4, !tbaa !7
  %19 = bitcast i32* %lowestMatchIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #7
  %20 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %lowLimit = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %20, i32 0, i32 6
  %21 = load i32, i32* %lowLimit, align 4, !tbaa !19
  %add = add i32 %21, 65536
  %22 = load i32, i32* %ipIndex, align 4, !tbaa !7
  %cmp = icmp ugt i32 %add, %22
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %23 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %lowLimit6 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %23, i32 0, i32 6
  %24 = load i32, i32* %lowLimit6, align 4, !tbaa !19
  br label %cond.end

cond.false:                                       ; preds = %entry
  %25 = load i32, i32* %ipIndex, align 4, !tbaa !7
  %sub = sub i32 %25, 65535
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %24, %cond.true ], [ %sub, %cond.false ]
  store i32 %cond, i32* %lowestMatchIndex, align 4, !tbaa !7
  %26 = bitcast i8** %dictBase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #7
  %27 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %dictBase7 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %27, i32 0, i32 4
  %28 = load i8*, i8** %dictBase7, align 4, !tbaa !17
  store i8* %28, i8** %dictBase, align 4, !tbaa !3
  %29 = bitcast i32* %lookBackLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #7
  %30 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %31 = load i8*, i8** %iLowLimit.addr, align 4, !tbaa !3
  %sub.ptr.lhs.cast8 = ptrtoint i8* %30 to i32
  %sub.ptr.rhs.cast9 = ptrtoint i8* %31 to i32
  %sub.ptr.sub10 = sub i32 %sub.ptr.lhs.cast8, %sub.ptr.rhs.cast9
  store i32 %sub.ptr.sub10, i32* %lookBackLength, align 4, !tbaa !7
  %32 = bitcast i32* %nbAttempts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #7
  %33 = load i32, i32* %maxNbAttempts.addr, align 4, !tbaa !7
  store i32 %33, i32* %nbAttempts, align 4, !tbaa !7
  %34 = bitcast i32* %matchChainPos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #7
  store i32 0, i32* %matchChainPos, align 4, !tbaa !7
  %35 = bitcast i32* %pattern to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #7
  %36 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_read32(i8* %36)
  store i32 %call, i32* %pattern, align 4, !tbaa !7
  %37 = bitcast i32* %matchIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #7
  %38 = bitcast i32* %repeat to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #7
  store i32 0, i32* %repeat, align 4, !tbaa !9
  %39 = bitcast i32* %srcPatternLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #7
  store i32 0, i32* %srcPatternLength, align 4, !tbaa !14
  %40 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %41 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  call void @LZ4HC_Insert(%struct.LZ4HC_CCtx_internal* %40, i8* %41)
  %42 = load i32*, i32** %HashTable, align 4, !tbaa !3
  %43 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %call11 = call i32 @LZ4HC_hashPtr(i8* %43)
  %arrayidx = getelementptr inbounds i32, i32* %42, i32 %call11
  %44 = load i32, i32* %arrayidx, align 4, !tbaa !7
  store i32 %44, i32* %matchIndex, align 4, !tbaa !7
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont340, %cleanup338, %cond.end
  %45 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %46 = load i32, i32* %lowestMatchIndex, align 4, !tbaa !7
  %cmp12 = icmp uge i32 %45, %46
  br i1 %cmp12, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %47 = load i32, i32* %nbAttempts, align 4, !tbaa !7
  %tobool = icmp ne i32 %47, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %48 = phi i1 [ false, %while.cond ], [ %tobool, %land.rhs ]
  br i1 %48, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %49 = bitcast i32* %matchLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #7
  store i32 0, i32* %matchLength, align 4, !tbaa !7
  %50 = load i32, i32* %nbAttempts, align 4, !tbaa !7
  %dec = add nsw i32 %50, -1
  store i32 %dec, i32* %nbAttempts, align 4, !tbaa !7
  %51 = load i32, i32* %favorDecSpeed.addr, align 4, !tbaa !9
  %tobool13 = icmp ne i32 %51, 0
  br i1 %tobool13, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %while.body
  %52 = load i32, i32* %ipIndex, align 4, !tbaa !7
  %53 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %sub14 = sub i32 %52, %53
  %cmp15 = icmp ult i32 %sub14, 8
  br i1 %cmp15, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  br label %if.end99

if.else:                                          ; preds = %land.lhs.true, %while.body
  %54 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %55 = load i32, i32* %dictLimit, align 4, !tbaa !7
  %cmp16 = icmp uge i32 %54, %55
  br i1 %cmp16, label %if.then17, label %if.else52

if.then17:                                        ; preds = %if.else
  %56 = bitcast i8** %matchPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #7
  %57 = load i8*, i8** %base, align 4, !tbaa !3
  %58 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %add.ptr18 = getelementptr inbounds i8, i8* %57, i32 %58
  store i8* %add.ptr18, i8** %matchPtr, align 4, !tbaa !3
  %59 = load i8*, i8** %iLowLimit.addr, align 4, !tbaa !3
  %60 = load i32, i32* %longest.addr, align 4, !tbaa !7
  %add.ptr19 = getelementptr inbounds i8, i8* %59, i32 %60
  %add.ptr20 = getelementptr inbounds i8, i8* %add.ptr19, i32 -1
  %call21 = call zeroext i16 @LZ4_read16(i8* %add.ptr20)
  %conv = zext i16 %call21 to i32
  %61 = load i8*, i8** %matchPtr, align 4, !tbaa !3
  %62 = load i32, i32* %lookBackLength, align 4, !tbaa !7
  %idx.neg = sub i32 0, %62
  %add.ptr22 = getelementptr inbounds i8, i8* %61, i32 %idx.neg
  %63 = load i32, i32* %longest.addr, align 4, !tbaa !7
  %add.ptr23 = getelementptr inbounds i8, i8* %add.ptr22, i32 %63
  %add.ptr24 = getelementptr inbounds i8, i8* %add.ptr23, i32 -1
  %call25 = call zeroext i16 @LZ4_read16(i8* %add.ptr24)
  %conv26 = zext i16 %call25 to i32
  %cmp27 = icmp eq i32 %conv, %conv26
  br i1 %cmp27, label %if.then29, label %if.end51

if.then29:                                        ; preds = %if.then17
  %64 = load i8*, i8** %matchPtr, align 4, !tbaa !3
  %call30 = call i32 @LZ4_read32(i8* %64)
  %65 = load i32, i32* %pattern, align 4, !tbaa !7
  %cmp31 = icmp eq i32 %call30, %65
  br i1 %cmp31, label %if.then33, label %if.end50

if.then33:                                        ; preds = %if.then29
  %66 = bitcast i32* %back to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #7
  %67 = load i32, i32* %lookBackLength, align 4, !tbaa !7
  %tobool34 = icmp ne i32 %67, 0
  br i1 %tobool34, label %cond.true35, label %cond.false37

cond.true35:                                      ; preds = %if.then33
  %68 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %69 = load i8*, i8** %matchPtr, align 4, !tbaa !3
  %70 = load i8*, i8** %iLowLimit.addr, align 4, !tbaa !3
  %71 = load i8*, i8** %lowPrefixPtr, align 4, !tbaa !3
  %call36 = call i32 @LZ4HC_countBack(i8* %68, i8* %69, i8* %70, i8* %71)
  br label %cond.end38

cond.false37:                                     ; preds = %if.then33
  br label %cond.end38

cond.end38:                                       ; preds = %cond.false37, %cond.true35
  %cond39 = phi i32 [ %call36, %cond.true35 ], [ 0, %cond.false37 ]
  store i32 %cond39, i32* %back, align 4, !tbaa !7
  %72 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %add.ptr40 = getelementptr inbounds i8, i8* %72, i32 4
  %73 = load i8*, i8** %matchPtr, align 4, !tbaa !3
  %add.ptr41 = getelementptr inbounds i8, i8* %73, i32 4
  %74 = load i8*, i8** %iHighLimit.addr, align 4, !tbaa !3
  %call42 = call i32 @LZ4_count(i8* %add.ptr40, i8* %add.ptr41, i8* %74)
  %add43 = add nsw i32 4, %call42
  store i32 %add43, i32* %matchLength, align 4, !tbaa !7
  %75 = load i32, i32* %back, align 4, !tbaa !7
  %76 = load i32, i32* %matchLength, align 4, !tbaa !7
  %sub44 = sub nsw i32 %76, %75
  store i32 %sub44, i32* %matchLength, align 4, !tbaa !7
  %77 = load i32, i32* %matchLength, align 4, !tbaa !7
  %78 = load i32, i32* %longest.addr, align 4, !tbaa !7
  %cmp45 = icmp sgt i32 %77, %78
  br i1 %cmp45, label %if.then47, label %if.end

if.then47:                                        ; preds = %cond.end38
  %79 = load i32, i32* %matchLength, align 4, !tbaa !7
  store i32 %79, i32* %longest.addr, align 4, !tbaa !7
  %80 = load i8*, i8** %matchPtr, align 4, !tbaa !3
  %81 = load i32, i32* %back, align 4, !tbaa !7
  %add.ptr48 = getelementptr inbounds i8, i8* %80, i32 %81
  %82 = load i8**, i8*** %matchpos.addr, align 4, !tbaa !3
  store i8* %add.ptr48, i8** %82, align 4, !tbaa !3
  %83 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %84 = load i32, i32* %back, align 4, !tbaa !7
  %add.ptr49 = getelementptr inbounds i8, i8* %83, i32 %84
  %85 = load i8**, i8*** %startpos.addr, align 4, !tbaa !3
  store i8* %add.ptr49, i8** %85, align 4, !tbaa !3
  br label %if.end

if.end:                                           ; preds = %if.then47, %cond.end38
  %86 = bitcast i32* %back to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #7
  br label %if.end50

if.end50:                                         ; preds = %if.end, %if.then29
  br label %if.end51

if.end51:                                         ; preds = %if.end50, %if.then17
  %87 = bitcast i8** %matchPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #7
  br label %if.end98

if.else52:                                        ; preds = %if.else
  %88 = bitcast i8** %matchPtr53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #7
  %89 = load i8*, i8** %dictBase, align 4, !tbaa !3
  %90 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %add.ptr54 = getelementptr inbounds i8, i8* %89, i32 %90
  store i8* %add.ptr54, i8** %matchPtr53, align 4, !tbaa !3
  %91 = load i8*, i8** %matchPtr53, align 4, !tbaa !3
  %call55 = call i32 @LZ4_read32(i8* %91)
  %92 = load i32, i32* %pattern, align 4, !tbaa !7
  %cmp56 = icmp eq i32 %call55, %92
  br i1 %cmp56, label %if.then58, label %if.end97

if.then58:                                        ; preds = %if.else52
  %93 = bitcast i8** %dictStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %93) #7
  %94 = load i8*, i8** %dictBase, align 4, !tbaa !3
  %95 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %lowLimit59 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %95, i32 0, i32 6
  %96 = load i32, i32* %lowLimit59, align 4, !tbaa !19
  %add.ptr60 = getelementptr inbounds i8, i8* %94, i32 %96
  store i8* %add.ptr60, i8** %dictStart, align 4, !tbaa !3
  %97 = bitcast i32* %back61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %97) #7
  store i32 0, i32* %back61, align 4, !tbaa !7
  %98 = bitcast i8** %vLimit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %98) #7
  %99 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %100 = load i32, i32* %dictLimit, align 4, !tbaa !7
  %101 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %sub62 = sub i32 %100, %101
  %add.ptr63 = getelementptr inbounds i8, i8* %99, i32 %sub62
  store i8* %add.ptr63, i8** %vLimit, align 4, !tbaa !3
  %102 = load i8*, i8** %vLimit, align 4, !tbaa !3
  %103 = load i8*, i8** %iHighLimit.addr, align 4, !tbaa !3
  %cmp64 = icmp ugt i8* %102, %103
  br i1 %cmp64, label %if.then66, label %if.end67

if.then66:                                        ; preds = %if.then58
  %104 = load i8*, i8** %iHighLimit.addr, align 4, !tbaa !3
  store i8* %104, i8** %vLimit, align 4, !tbaa !3
  br label %if.end67

if.end67:                                         ; preds = %if.then66, %if.then58
  %105 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %add.ptr68 = getelementptr inbounds i8, i8* %105, i32 4
  %106 = load i8*, i8** %matchPtr53, align 4, !tbaa !3
  %add.ptr69 = getelementptr inbounds i8, i8* %106, i32 4
  %107 = load i8*, i8** %vLimit, align 4, !tbaa !3
  %call70 = call i32 @LZ4_count(i8* %add.ptr68, i8* %add.ptr69, i8* %107)
  %add71 = add nsw i32 %call70, 4
  store i32 %add71, i32* %matchLength, align 4, !tbaa !7
  %108 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %109 = load i32, i32* %matchLength, align 4, !tbaa !7
  %add.ptr72 = getelementptr inbounds i8, i8* %108, i32 %109
  %110 = load i8*, i8** %vLimit, align 4, !tbaa !3
  %cmp73 = icmp eq i8* %add.ptr72, %110
  br i1 %cmp73, label %land.lhs.true75, label %if.end82

land.lhs.true75:                                  ; preds = %if.end67
  %111 = load i8*, i8** %vLimit, align 4, !tbaa !3
  %112 = load i8*, i8** %iHighLimit.addr, align 4, !tbaa !3
  %cmp76 = icmp ult i8* %111, %112
  br i1 %cmp76, label %if.then78, label %if.end82

if.then78:                                        ; preds = %land.lhs.true75
  %113 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %114 = load i32, i32* %matchLength, align 4, !tbaa !7
  %add.ptr79 = getelementptr inbounds i8, i8* %113, i32 %114
  %115 = load i8*, i8** %lowPrefixPtr, align 4, !tbaa !3
  %116 = load i8*, i8** %iHighLimit.addr, align 4, !tbaa !3
  %call80 = call i32 @LZ4_count(i8* %add.ptr79, i8* %115, i8* %116)
  %117 = load i32, i32* %matchLength, align 4, !tbaa !7
  %add81 = add i32 %117, %call80
  store i32 %add81, i32* %matchLength, align 4, !tbaa !7
  br label %if.end82

if.end82:                                         ; preds = %if.then78, %land.lhs.true75, %if.end67
  %118 = load i32, i32* %lookBackLength, align 4, !tbaa !7
  %tobool83 = icmp ne i32 %118, 0
  br i1 %tobool83, label %cond.true84, label %cond.false86

cond.true84:                                      ; preds = %if.end82
  %119 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %120 = load i8*, i8** %matchPtr53, align 4, !tbaa !3
  %121 = load i8*, i8** %iLowLimit.addr, align 4, !tbaa !3
  %122 = load i8*, i8** %dictStart, align 4, !tbaa !3
  %call85 = call i32 @LZ4HC_countBack(i8* %119, i8* %120, i8* %121, i8* %122)
  br label %cond.end87

cond.false86:                                     ; preds = %if.end82
  br label %cond.end87

cond.end87:                                       ; preds = %cond.false86, %cond.true84
  %cond88 = phi i32 [ %call85, %cond.true84 ], [ 0, %cond.false86 ]
  store i32 %cond88, i32* %back61, align 4, !tbaa !7
  %123 = load i32, i32* %back61, align 4, !tbaa !7
  %124 = load i32, i32* %matchLength, align 4, !tbaa !7
  %sub89 = sub nsw i32 %124, %123
  store i32 %sub89, i32* %matchLength, align 4, !tbaa !7
  %125 = load i32, i32* %matchLength, align 4, !tbaa !7
  %126 = load i32, i32* %longest.addr, align 4, !tbaa !7
  %cmp90 = icmp sgt i32 %125, %126
  br i1 %cmp90, label %if.then92, label %if.end96

if.then92:                                        ; preds = %cond.end87
  %127 = load i32, i32* %matchLength, align 4, !tbaa !7
  store i32 %127, i32* %longest.addr, align 4, !tbaa !7
  %128 = load i8*, i8** %base, align 4, !tbaa !3
  %129 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %add.ptr93 = getelementptr inbounds i8, i8* %128, i32 %129
  %130 = load i32, i32* %back61, align 4, !tbaa !7
  %add.ptr94 = getelementptr inbounds i8, i8* %add.ptr93, i32 %130
  %131 = load i8**, i8*** %matchpos.addr, align 4, !tbaa !3
  store i8* %add.ptr94, i8** %131, align 4, !tbaa !3
  %132 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %133 = load i32, i32* %back61, align 4, !tbaa !7
  %add.ptr95 = getelementptr inbounds i8, i8* %132, i32 %133
  %134 = load i8**, i8*** %startpos.addr, align 4, !tbaa !3
  store i8* %add.ptr95, i8** %134, align 4, !tbaa !3
  br label %if.end96

if.end96:                                         ; preds = %if.then92, %cond.end87
  %135 = bitcast i8** %vLimit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #7
  %136 = bitcast i32* %back61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #7
  %137 = bitcast i8** %dictStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #7
  br label %if.end97

if.end97:                                         ; preds = %if.end96, %if.else52
  %138 = bitcast i8** %matchPtr53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #7
  br label %if.end98

if.end98:                                         ; preds = %if.end97, %if.end51
  br label %if.end99

if.end99:                                         ; preds = %if.end98, %if.then
  %139 = load i32, i32* %chainSwap.addr, align 4, !tbaa !7
  %tobool100 = icmp ne i32 %139, 0
  br i1 %tobool100, label %land.lhs.true101, label %if.end137

land.lhs.true101:                                 ; preds = %if.end99
  %140 = load i32, i32* %matchLength, align 4, !tbaa !7
  %141 = load i32, i32* %longest.addr, align 4, !tbaa !7
  %cmp102 = icmp eq i32 %140, %141
  br i1 %cmp102, label %if.then104, label %if.end137

if.then104:                                       ; preds = %land.lhs.true101
  %142 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %143 = load i32, i32* %longest.addr, align 4, !tbaa !7
  %add105 = add i32 %142, %143
  %144 = load i32, i32* %ipIndex, align 4, !tbaa !7
  %cmp106 = icmp ule i32 %add105, %144
  br i1 %cmp106, label %if.then108, label %if.end136

if.then108:                                       ; preds = %if.then104
  %145 = bitcast i32* %kTrigger to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %145) #7
  store i32 4, i32* %kTrigger, align 4, !tbaa !7
  %146 = bitcast i32* %distanceToNextMatch to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %146) #7
  store i32 1, i32* %distanceToNextMatch, align 4, !tbaa !7
  %147 = bitcast i32* %end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %147) #7
  %148 = load i32, i32* %longest.addr, align 4, !tbaa !7
  %sub109 = sub nsw i32 %148, 4
  %add110 = add nsw i32 %sub109, 1
  store i32 %add110, i32* %end, align 4, !tbaa !7
  %149 = bitcast i32* %step to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %149) #7
  store i32 1, i32* %step, align 4, !tbaa !7
  %150 = bitcast i32* %accel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %150) #7
  store i32 16, i32* %accel, align 4, !tbaa !7
  %151 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %151) #7
  store i32 0, i32* %pos, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then108
  %152 = load i32, i32* %pos, align 4, !tbaa !7
  %153 = load i32, i32* %end, align 4, !tbaa !7
  %cmp111 = icmp slt i32 %152, %153
  br i1 %cmp111, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %154 = bitcast i32* %candidateDist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %154) #7
  %155 = load i16*, i16** %chainTable, align 4, !tbaa !3
  %156 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %157 = load i32, i32* %pos, align 4, !tbaa !7
  %add113 = add i32 %156, %157
  %conv114 = trunc i32 %add113 to i16
  %idxprom = zext i16 %conv114 to i32
  %arrayidx115 = getelementptr inbounds i16, i16* %155, i32 %idxprom
  %158 = load i16, i16* %arrayidx115, align 2, !tbaa !22
  %conv116 = zext i16 %158 to i32
  store i32 %conv116, i32* %candidateDist, align 4, !tbaa !7
  %159 = load i32, i32* %accel, align 4, !tbaa !7
  %inc = add nsw i32 %159, 1
  store i32 %inc, i32* %accel, align 4, !tbaa !7
  %shr = ashr i32 %159, 4
  store i32 %shr, i32* %step, align 4, !tbaa !7
  %160 = load i32, i32* %candidateDist, align 4, !tbaa !7
  %161 = load i32, i32* %distanceToNextMatch, align 4, !tbaa !7
  %cmp117 = icmp ugt i32 %160, %161
  br i1 %cmp117, label %if.then119, label %if.end120

if.then119:                                       ; preds = %for.body
  %162 = load i32, i32* %candidateDist, align 4, !tbaa !7
  store i32 %162, i32* %distanceToNextMatch, align 4, !tbaa !7
  %163 = load i32, i32* %pos, align 4, !tbaa !7
  store i32 %163, i32* %matchChainPos, align 4, !tbaa !7
  store i32 16, i32* %accel, align 4, !tbaa !7
  br label %if.end120

if.end120:                                        ; preds = %if.then119, %for.body
  %164 = bitcast i32* %candidateDist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #7
  br label %for.inc

for.inc:                                          ; preds = %if.end120
  %165 = load i32, i32* %step, align 4, !tbaa !7
  %166 = load i32, i32* %pos, align 4, !tbaa !7
  %add121 = add nsw i32 %166, %165
  store i32 %add121, i32* %pos, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %167 = load i32, i32* %distanceToNextMatch, align 4, !tbaa !7
  %cmp122 = icmp ugt i32 %167, 1
  br i1 %cmp122, label %if.then124, label %if.end130

if.then124:                                       ; preds = %for.end
  %168 = load i32, i32* %distanceToNextMatch, align 4, !tbaa !7
  %169 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %cmp125 = icmp ugt i32 %168, %169
  br i1 %cmp125, label %if.then127, label %if.end128

if.then127:                                       ; preds = %if.then124
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end128:                                        ; preds = %if.then124
  %170 = load i32, i32* %distanceToNextMatch, align 4, !tbaa !7
  %171 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %sub129 = sub i32 %171, %170
  store i32 %sub129, i32* %matchIndex, align 4, !tbaa !7
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end130:                                        ; preds = %for.end
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end130, %if.end128, %if.then127
  %172 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #7
  %173 = bitcast i32* %accel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #7
  %174 = bitcast i32* %step to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #7
  %175 = bitcast i32* %end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #7
  %176 = bitcast i32* %distanceToNextMatch to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #7
  %177 = bitcast i32* %kTrigger to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup338 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end136

if.end136:                                        ; preds = %cleanup.cont, %if.then104
  br label %if.end137

if.end137:                                        ; preds = %if.end136, %land.lhs.true101, %if.end99
  %178 = bitcast i32* %distNextMatch to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %178) #7
  %179 = load i16*, i16** %chainTable, align 4, !tbaa !3
  %180 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %conv138 = trunc i32 %180 to i16
  %idxprom139 = zext i16 %conv138 to i32
  %arrayidx140 = getelementptr inbounds i16, i16* %179, i32 %idxprom139
  %181 = load i16, i16* %arrayidx140, align 2, !tbaa !22
  %conv141 = zext i16 %181 to i32
  store i32 %conv141, i32* %distNextMatch, align 4, !tbaa !7
  %182 = load i32, i32* %patternAnalysis.addr, align 4, !tbaa !7
  %tobool142 = icmp ne i32 %182, 0
  br i1 %tobool142, label %land.lhs.true143, label %if.end328

land.lhs.true143:                                 ; preds = %if.end137
  %183 = load i32, i32* %distNextMatch, align 4, !tbaa !7
  %cmp144 = icmp eq i32 %183, 1
  br i1 %cmp144, label %land.lhs.true146, label %if.end328

land.lhs.true146:                                 ; preds = %land.lhs.true143
  %184 = load i32, i32* %matchChainPos, align 4, !tbaa !7
  %cmp147 = icmp eq i32 %184, 0
  br i1 %cmp147, label %if.then149, label %if.end328

if.then149:                                       ; preds = %land.lhs.true146
  %185 = bitcast i32* %matchCandidateIdx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %185) #7
  %186 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %sub150 = sub i32 %186, 1
  store i32 %sub150, i32* %matchCandidateIdx, align 4, !tbaa !7
  %187 = load i32, i32* %repeat, align 4, !tbaa !9
  %cmp151 = icmp eq i32 %187, 0
  br i1 %cmp151, label %if.then153, label %if.end169

if.then153:                                       ; preds = %if.then149
  %188 = load i32, i32* %pattern, align 4, !tbaa !7
  %and = and i32 %188, 65535
  %189 = load i32, i32* %pattern, align 4, !tbaa !7
  %shr154 = lshr i32 %189, 16
  %cmp155 = icmp eq i32 %and, %shr154
  %conv156 = zext i1 %cmp155 to i32
  %190 = load i32, i32* %pattern, align 4, !tbaa !7
  %and157 = and i32 %190, 255
  %191 = load i32, i32* %pattern, align 4, !tbaa !7
  %shr158 = lshr i32 %191, 24
  %cmp159 = icmp eq i32 %and157, %shr158
  %conv160 = zext i1 %cmp159 to i32
  %and161 = and i32 %conv156, %conv160
  %tobool162 = icmp ne i32 %and161, 0
  br i1 %tobool162, label %if.then163, label %if.else167

if.then163:                                       ; preds = %if.then153
  store i32 2, i32* %repeat, align 4, !tbaa !9
  %192 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %add.ptr164 = getelementptr inbounds i8, i8* %192, i32 4
  %193 = load i8*, i8** %iHighLimit.addr, align 4, !tbaa !3
  %194 = load i32, i32* %pattern, align 4, !tbaa !7
  %call165 = call i32 @LZ4HC_countPattern(i8* %add.ptr164, i8* %193, i32 %194)
  %add166 = add i32 %call165, 4
  store i32 %add166, i32* %srcPatternLength, align 4, !tbaa !14
  br label %if.end168

if.else167:                                       ; preds = %if.then153
  store i32 1, i32* %repeat, align 4, !tbaa !9
  br label %if.end168

if.end168:                                        ; preds = %if.else167, %if.then163
  br label %if.end169

if.end169:                                        ; preds = %if.end168, %if.then149
  %195 = load i32, i32* %repeat, align 4, !tbaa !9
  %cmp170 = icmp eq i32 %195, 2
  br i1 %cmp170, label %land.lhs.true172, label %if.end324

land.lhs.true172:                                 ; preds = %if.end169
  %196 = load i32, i32* %matchCandidateIdx, align 4, !tbaa !7
  %197 = load i32, i32* %lowestMatchIndex, align 4, !tbaa !7
  %cmp173 = icmp uge i32 %196, %197
  br i1 %cmp173, label %land.lhs.true175, label %if.end324

land.lhs.true175:                                 ; preds = %land.lhs.true172
  %198 = load i32, i32* %dictLimit, align 4, !tbaa !7
  %199 = load i32, i32* %matchCandidateIdx, align 4, !tbaa !7
  %call176 = call i32 @LZ4HC_protectDictEnd(i32 %198, i32 %199)
  %tobool177 = icmp ne i32 %call176, 0
  br i1 %tobool177, label %if.then178, label %if.end324

if.then178:                                       ; preds = %land.lhs.true175
  %200 = bitcast i32* %extDict to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %200) #7
  %201 = load i32, i32* %matchCandidateIdx, align 4, !tbaa !7
  %202 = load i32, i32* %dictLimit, align 4, !tbaa !7
  %cmp179 = icmp ult i32 %201, %202
  %conv180 = zext i1 %cmp179 to i32
  store i32 %conv180, i32* %extDict, align 4, !tbaa !7
  %203 = bitcast i8** %matchPtr181 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %203) #7
  %204 = load i32, i32* %extDict, align 4, !tbaa !7
  %tobool182 = icmp ne i32 %204, 0
  br i1 %tobool182, label %cond.true183, label %cond.false184

cond.true183:                                     ; preds = %if.then178
  %205 = load i8*, i8** %dictBase, align 4, !tbaa !3
  br label %cond.end185

cond.false184:                                    ; preds = %if.then178
  %206 = load i8*, i8** %base, align 4, !tbaa !3
  br label %cond.end185

cond.end185:                                      ; preds = %cond.false184, %cond.true183
  %cond186 = phi i8* [ %205, %cond.true183 ], [ %206, %cond.false184 ]
  %207 = load i32, i32* %matchCandidateIdx, align 4, !tbaa !7
  %add.ptr187 = getelementptr inbounds i8, i8* %cond186, i32 %207
  store i8* %add.ptr187, i8** %matchPtr181, align 4, !tbaa !3
  %208 = load i8*, i8** %matchPtr181, align 4, !tbaa !3
  %call188 = call i32 @LZ4_read32(i8* %208)
  %209 = load i32, i32* %pattern, align 4, !tbaa !7
  %cmp189 = icmp eq i32 %call188, %209
  br i1 %cmp189, label %if.then191, label %if.end319

if.then191:                                       ; preds = %cond.end185
  %210 = bitcast i8** %dictStart192 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %210) #7
  %211 = load i8*, i8** %dictBase, align 4, !tbaa !3
  %212 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %lowLimit193 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %212, i32 0, i32 6
  %213 = load i32, i32* %lowLimit193, align 4, !tbaa !19
  %add.ptr194 = getelementptr inbounds i8, i8* %211, i32 %213
  store i8* %add.ptr194, i8** %dictStart192, align 4, !tbaa !3
  %214 = bitcast i8** %iLimit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %214) #7
  %215 = load i32, i32* %extDict, align 4, !tbaa !7
  %tobool195 = icmp ne i32 %215, 0
  br i1 %tobool195, label %cond.true196, label %cond.false198

cond.true196:                                     ; preds = %if.then191
  %216 = load i8*, i8** %dictBase, align 4, !tbaa !3
  %217 = load i32, i32* %dictLimit, align 4, !tbaa !7
  %add.ptr197 = getelementptr inbounds i8, i8* %216, i32 %217
  br label %cond.end199

cond.false198:                                    ; preds = %if.then191
  %218 = load i8*, i8** %iHighLimit.addr, align 4, !tbaa !3
  br label %cond.end199

cond.end199:                                      ; preds = %cond.false198, %cond.true196
  %cond200 = phi i8* [ %add.ptr197, %cond.true196 ], [ %218, %cond.false198 ]
  store i8* %cond200, i8** %iLimit, align 4, !tbaa !3
  %219 = bitcast i32* %forwardPatternLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %219) #7
  %220 = load i8*, i8** %matchPtr181, align 4, !tbaa !3
  %add.ptr201 = getelementptr inbounds i8, i8* %220, i32 4
  %221 = load i8*, i8** %iLimit, align 4, !tbaa !3
  %222 = load i32, i32* %pattern, align 4, !tbaa !7
  %call202 = call i32 @LZ4HC_countPattern(i8* %add.ptr201, i8* %221, i32 %222)
  %add203 = add i32 %call202, 4
  store i32 %add203, i32* %forwardPatternLength, align 4, !tbaa !14
  %223 = load i32, i32* %extDict, align 4, !tbaa !7
  %tobool204 = icmp ne i32 %223, 0
  br i1 %tobool204, label %land.lhs.true205, label %if.end213

land.lhs.true205:                                 ; preds = %cond.end199
  %224 = load i8*, i8** %matchPtr181, align 4, !tbaa !3
  %225 = load i32, i32* %forwardPatternLength, align 4, !tbaa !14
  %add.ptr206 = getelementptr inbounds i8, i8* %224, i32 %225
  %226 = load i8*, i8** %iLimit, align 4, !tbaa !3
  %cmp207 = icmp eq i8* %add.ptr206, %226
  br i1 %cmp207, label %if.then209, label %if.end213

if.then209:                                       ; preds = %land.lhs.true205
  %227 = bitcast i32* %rotatedPattern to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %227) #7
  %228 = load i32, i32* %forwardPatternLength, align 4, !tbaa !14
  %229 = load i32, i32* %pattern, align 4, !tbaa !7
  %call210 = call i32 @LZ4HC_rotatePattern(i32 %228, i32 %229)
  store i32 %call210, i32* %rotatedPattern, align 4, !tbaa !7
  %230 = load i8*, i8** %lowPrefixPtr, align 4, !tbaa !3
  %231 = load i8*, i8** %iHighLimit.addr, align 4, !tbaa !3
  %232 = load i32, i32* %rotatedPattern, align 4, !tbaa !7
  %call211 = call i32 @LZ4HC_countPattern(i8* %230, i8* %231, i32 %232)
  %233 = load i32, i32* %forwardPatternLength, align 4, !tbaa !14
  %add212 = add i32 %233, %call211
  store i32 %add212, i32* %forwardPatternLength, align 4, !tbaa !14
  %234 = bitcast i32* %rotatedPattern to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %234) #7
  br label %if.end213

if.end213:                                        ; preds = %if.then209, %land.lhs.true205, %cond.end199
  %235 = bitcast i8** %lowestMatchPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %235) #7
  %236 = load i32, i32* %extDict, align 4, !tbaa !7
  %tobool214 = icmp ne i32 %236, 0
  br i1 %tobool214, label %cond.true215, label %cond.false216

cond.true215:                                     ; preds = %if.end213
  %237 = load i8*, i8** %dictStart192, align 4, !tbaa !3
  br label %cond.end217

cond.false216:                                    ; preds = %if.end213
  %238 = load i8*, i8** %lowPrefixPtr, align 4, !tbaa !3
  br label %cond.end217

cond.end217:                                      ; preds = %cond.false216, %cond.true215
  %cond218 = phi i8* [ %237, %cond.true215 ], [ %238, %cond.false216 ]
  store i8* %cond218, i8** %lowestMatchPtr, align 4, !tbaa !3
  %239 = bitcast i32* %backLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %239) #7
  %240 = load i8*, i8** %matchPtr181, align 4, !tbaa !3
  %241 = load i8*, i8** %lowestMatchPtr, align 4, !tbaa !3
  %242 = load i32, i32* %pattern, align 4, !tbaa !7
  %call219 = call i32 @LZ4HC_reverseCountPattern(i8* %240, i8* %241, i32 %242)
  store i32 %call219, i32* %backLength, align 4, !tbaa !14
  %243 = bitcast i32* %currentSegmentLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %243) #7
  %244 = load i32, i32* %extDict, align 4, !tbaa !7
  %tobool220 = icmp ne i32 %244, 0
  br i1 %tobool220, label %if.end237, label %land.lhs.true221

land.lhs.true221:                                 ; preds = %cond.end217
  %245 = load i8*, i8** %matchPtr181, align 4, !tbaa !3
  %246 = load i32, i32* %backLength, align 4, !tbaa !14
  %idx.neg222 = sub i32 0, %246
  %add.ptr223 = getelementptr inbounds i8, i8* %245, i32 %idx.neg222
  %247 = load i8*, i8** %lowPrefixPtr, align 4, !tbaa !3
  %cmp224 = icmp eq i8* %add.ptr223, %247
  br i1 %cmp224, label %land.lhs.true226, label %if.end237

land.lhs.true226:                                 ; preds = %land.lhs.true221
  %248 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %hc4.addr, align 4, !tbaa !3
  %lowLimit227 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %248, i32 0, i32 6
  %249 = load i32, i32* %lowLimit227, align 4, !tbaa !19
  %250 = load i32, i32* %dictLimit, align 4, !tbaa !7
  %cmp228 = icmp ult i32 %249, %250
  br i1 %cmp228, label %if.then230, label %if.end237

if.then230:                                       ; preds = %land.lhs.true226
  %251 = bitcast i32* %rotatedPattern231 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %251) #7
  %252 = load i32, i32* %backLength, align 4, !tbaa !14
  %sub232 = sub nsw i32 0, %252
  %253 = load i32, i32* %pattern, align 4, !tbaa !7
  %call233 = call i32 @LZ4HC_rotatePattern(i32 %sub232, i32 %253)
  store i32 %call233, i32* %rotatedPattern231, align 4, !tbaa !7
  %254 = load i8*, i8** %dictBase, align 4, !tbaa !3
  %255 = load i32, i32* %dictLimit, align 4, !tbaa !7
  %add.ptr234 = getelementptr inbounds i8, i8* %254, i32 %255
  %256 = load i8*, i8** %dictStart192, align 4, !tbaa !3
  %257 = load i32, i32* %rotatedPattern231, align 4, !tbaa !7
  %call235 = call i32 @LZ4HC_reverseCountPattern(i8* %add.ptr234, i8* %256, i32 %257)
  %258 = load i32, i32* %backLength, align 4, !tbaa !14
  %add236 = add i32 %258, %call235
  store i32 %add236, i32* %backLength, align 4, !tbaa !14
  %259 = bitcast i32* %rotatedPattern231 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %259) #7
  br label %if.end237

if.end237:                                        ; preds = %if.then230, %land.lhs.true226, %land.lhs.true221, %cond.end217
  %260 = load i32, i32* %matchCandidateIdx, align 4, !tbaa !7
  %261 = load i32, i32* %matchCandidateIdx, align 4, !tbaa !7
  %262 = load i32, i32* %backLength, align 4, !tbaa !14
  %sub238 = sub i32 %261, %262
  %263 = load i32, i32* %lowestMatchIndex, align 4, !tbaa !7
  %cmp239 = icmp ugt i32 %sub238, %263
  br i1 %cmp239, label %cond.true241, label %cond.false243

cond.true241:                                     ; preds = %if.end237
  %264 = load i32, i32* %matchCandidateIdx, align 4, !tbaa !7
  %265 = load i32, i32* %backLength, align 4, !tbaa !14
  %sub242 = sub i32 %264, %265
  br label %cond.end244

cond.false243:                                    ; preds = %if.end237
  %266 = load i32, i32* %lowestMatchIndex, align 4, !tbaa !7
  br label %cond.end244

cond.end244:                                      ; preds = %cond.false243, %cond.true241
  %cond245 = phi i32 [ %sub242, %cond.true241 ], [ %266, %cond.false243 ]
  %sub246 = sub i32 %260, %cond245
  store i32 %sub246, i32* %backLength, align 4, !tbaa !14
  %267 = load i32, i32* %backLength, align 4, !tbaa !14
  %268 = load i32, i32* %forwardPatternLength, align 4, !tbaa !14
  %add247 = add i32 %267, %268
  store i32 %add247, i32* %currentSegmentLength, align 4, !tbaa !14
  %269 = load i32, i32* %currentSegmentLength, align 4, !tbaa !14
  %270 = load i32, i32* %srcPatternLength, align 4, !tbaa !14
  %cmp248 = icmp uge i32 %269, %270
  br i1 %cmp248, label %land.lhs.true250, label %if.else261

land.lhs.true250:                                 ; preds = %cond.end244
  %271 = load i32, i32* %forwardPatternLength, align 4, !tbaa !14
  %272 = load i32, i32* %srcPatternLength, align 4, !tbaa !14
  %cmp251 = icmp ule i32 %271, %272
  br i1 %cmp251, label %if.then253, label %if.else261

if.then253:                                       ; preds = %land.lhs.true250
  %273 = bitcast i32* %newMatchIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %273) #7
  %274 = load i32, i32* %matchCandidateIdx, align 4, !tbaa !7
  %275 = load i32, i32* %forwardPatternLength, align 4, !tbaa !14
  %add254 = add i32 %274, %275
  %276 = load i32, i32* %srcPatternLength, align 4, !tbaa !14
  %sub255 = sub i32 %add254, %276
  store i32 %sub255, i32* %newMatchIndex, align 4, !tbaa !7
  %277 = load i32, i32* %dictLimit, align 4, !tbaa !7
  %278 = load i32, i32* %newMatchIndex, align 4, !tbaa !7
  %call256 = call i32 @LZ4HC_protectDictEnd(i32 %277, i32 %278)
  %tobool257 = icmp ne i32 %call256, 0
  br i1 %tobool257, label %if.then258, label %if.else259

if.then258:                                       ; preds = %if.then253
  %279 = load i32, i32* %newMatchIndex, align 4, !tbaa !7
  store i32 %279, i32* %matchIndex, align 4, !tbaa !7
  br label %if.end260

if.else259:                                       ; preds = %if.then253
  %280 = load i32, i32* %dictLimit, align 4, !tbaa !7
  store i32 %280, i32* %matchIndex, align 4, !tbaa !7
  br label %if.end260

if.end260:                                        ; preds = %if.else259, %if.then258
  %281 = bitcast i32* %newMatchIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %281) #7
  br label %if.end310

if.else261:                                       ; preds = %land.lhs.true250, %cond.end244
  %282 = bitcast i32* %newMatchIndex262 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %282) #7
  %283 = load i32, i32* %matchCandidateIdx, align 4, !tbaa !7
  %284 = load i32, i32* %backLength, align 4, !tbaa !14
  %sub263 = sub i32 %283, %284
  store i32 %sub263, i32* %newMatchIndex262, align 4, !tbaa !7
  %285 = load i32, i32* %dictLimit, align 4, !tbaa !7
  %286 = load i32, i32* %newMatchIndex262, align 4, !tbaa !7
  %call264 = call i32 @LZ4HC_protectDictEnd(i32 %285, i32 %286)
  %tobool265 = icmp ne i32 %call264, 0
  br i1 %tobool265, label %if.else267, label %if.then266

if.then266:                                       ; preds = %if.else261
  %287 = load i32, i32* %dictLimit, align 4, !tbaa !7
  store i32 %287, i32* %matchIndex, align 4, !tbaa !7
  br label %if.end306

if.else267:                                       ; preds = %if.else261
  %288 = load i32, i32* %newMatchIndex262, align 4, !tbaa !7
  store i32 %288, i32* %matchIndex, align 4, !tbaa !7
  %289 = load i32, i32* %lookBackLength, align 4, !tbaa !7
  %cmp268 = icmp eq i32 %289, 0
  br i1 %cmp268, label %if.then270, label %if.end305

if.then270:                                       ; preds = %if.else267
  %290 = bitcast i32* %maxML to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %290) #7
  %291 = load i32, i32* %currentSegmentLength, align 4, !tbaa !14
  %292 = load i32, i32* %srcPatternLength, align 4, !tbaa !14
  %cmp271 = icmp ult i32 %291, %292
  br i1 %cmp271, label %cond.true273, label %cond.false274

cond.true273:                                     ; preds = %if.then270
  %293 = load i32, i32* %currentSegmentLength, align 4, !tbaa !14
  br label %cond.end275

cond.false274:                                    ; preds = %if.then270
  %294 = load i32, i32* %srcPatternLength, align 4, !tbaa !14
  br label %cond.end275

cond.end275:                                      ; preds = %cond.false274, %cond.true273
  %cond276 = phi i32 [ %293, %cond.true273 ], [ %294, %cond.false274 ]
  store i32 %cond276, i32* %maxML, align 4, !tbaa !14
  %295 = load i32, i32* %longest.addr, align 4, !tbaa !7
  %296 = load i32, i32* %maxML, align 4, !tbaa !14
  %cmp277 = icmp ult i32 %295, %296
  br i1 %cmp277, label %if.then279, label %if.end289

if.then279:                                       ; preds = %cond.end275
  %297 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %298 = load i8*, i8** %base, align 4, !tbaa !3
  %sub.ptr.lhs.cast280 = ptrtoint i8* %297 to i32
  %sub.ptr.rhs.cast281 = ptrtoint i8* %298 to i32
  %sub.ptr.sub282 = sub i32 %sub.ptr.lhs.cast280, %sub.ptr.rhs.cast281
  %299 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %sub283 = sub i32 %sub.ptr.sub282, %299
  %cmp284 = icmp ugt i32 %sub283, 65535
  br i1 %cmp284, label %if.then286, label %if.end287

if.then286:                                       ; preds = %if.then279
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup302

if.end287:                                        ; preds = %if.then279
  %300 = load i32, i32* %maxML, align 4, !tbaa !14
  store i32 %300, i32* %longest.addr, align 4, !tbaa !7
  %301 = load i8*, i8** %base, align 4, !tbaa !3
  %302 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %add.ptr288 = getelementptr inbounds i8, i8* %301, i32 %302
  %303 = load i8**, i8*** %matchpos.addr, align 4, !tbaa !3
  store i8* %add.ptr288, i8** %303, align 4, !tbaa !3
  %304 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %305 = load i8**, i8*** %startpos.addr, align 4, !tbaa !3
  store i8* %304, i8** %305, align 4, !tbaa !3
  br label %if.end289

if.end289:                                        ; preds = %if.end287, %cond.end275
  %306 = bitcast i32* %distToNextPattern to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %306) #7
  %307 = load i16*, i16** %chainTable, align 4, !tbaa !3
  %308 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %conv290 = trunc i32 %308 to i16
  %idxprom291 = zext i16 %conv290 to i32
  %arrayidx292 = getelementptr inbounds i16, i16* %307, i32 %idxprom291
  %309 = load i16, i16* %arrayidx292, align 2, !tbaa !22
  %conv293 = zext i16 %309 to i32
  store i32 %conv293, i32* %distToNextPattern, align 4, !tbaa !7
  %310 = load i32, i32* %distToNextPattern, align 4, !tbaa !7
  %311 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %cmp294 = icmp ugt i32 %310, %311
  br i1 %cmp294, label %if.then296, label %if.end297

if.then296:                                       ; preds = %if.end289
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup299

if.end297:                                        ; preds = %if.end289
  %312 = load i32, i32* %distToNextPattern, align 4, !tbaa !7
  %313 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %sub298 = sub i32 %313, %312
  store i32 %sub298, i32* %matchIndex, align 4, !tbaa !7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup299

cleanup299:                                       ; preds = %if.end297, %if.then296
  %314 = bitcast i32* %distToNextPattern to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %314) #7
  %cleanup.dest300 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest300, label %cleanup302 [
    i32 0, label %cleanup.cont301
  ]

cleanup.cont301:                                  ; preds = %cleanup299
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup302

cleanup302:                                       ; preds = %cleanup.cont301, %cleanup299, %if.then286
  %315 = bitcast i32* %maxML to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %315) #7
  %cleanup.dest303 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest303, label %cleanup307 [
    i32 0, label %cleanup.cont304
  ]

cleanup.cont304:                                  ; preds = %cleanup302
  br label %if.end305

if.end305:                                        ; preds = %cleanup.cont304, %if.else267
  br label %if.end306

if.end306:                                        ; preds = %if.end305, %if.then266
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup307

cleanup307:                                       ; preds = %if.end306, %cleanup302
  %316 = bitcast i32* %newMatchIndex262 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %316) #7
  %cleanup.dest308 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest308, label %cleanup311 [
    i32 0, label %cleanup.cont309
  ]

cleanup.cont309:                                  ; preds = %cleanup307
  br label %if.end310

if.end310:                                        ; preds = %cleanup.cont309, %if.end260
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup311

cleanup311:                                       ; preds = %if.end310, %cleanup307
  %317 = bitcast i32* %currentSegmentLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %317) #7
  %318 = bitcast i32* %backLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %318) #7
  %319 = bitcast i8** %lowestMatchPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %319) #7
  %cleanup.dest314 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest314, label %cleanup316 [
    i32 0, label %cleanup.cont315
  ]

cleanup.cont315:                                  ; preds = %cleanup311
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup316

cleanup316:                                       ; preds = %cleanup.cont315, %cleanup311
  %320 = bitcast i32* %forwardPatternLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %320) #7
  %321 = bitcast i8** %iLimit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %321) #7
  %322 = bitcast i8** %dictStart192 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %322) #7
  br label %cleanup320

if.end319:                                        ; preds = %cond.end185
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup320

cleanup320:                                       ; preds = %if.end319, %cleanup316
  %323 = bitcast i8** %matchPtr181 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %323) #7
  %324 = bitcast i32* %extDict to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %324) #7
  %cleanup.dest322 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest322, label %cleanup325 [
    i32 0, label %cleanup.cont323
  ]

cleanup.cont323:                                  ; preds = %cleanup320
  br label %if.end324

if.end324:                                        ; preds = %cleanup.cont323, %land.lhs.true175, %land.lhs.true172, %if.end169
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup325

cleanup325:                                       ; preds = %if.end324, %cleanup320
  %325 = bitcast i32* %matchCandidateIdx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %325) #7
  %cleanup.dest326 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest326, label %cleanup329 [
    i32 0, label %cleanup.cont327
  ]

cleanup.cont327:                                  ; preds = %cleanup325
  br label %if.end328

if.end328:                                        ; preds = %cleanup.cont327, %land.lhs.true146, %land.lhs.true143, %if.end137
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup329

cleanup329:                                       ; preds = %if.end328, %cleanup325
  %326 = bitcast i32* %distNextMatch to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %326) #7
  %cleanup.dest330 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest330, label %cleanup338 [
    i32 0, label %cleanup.cont331
  ]

cleanup.cont331:                                  ; preds = %cleanup329
  %327 = load i16*, i16** %chainTable, align 4, !tbaa !3
  %328 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %329 = load i32, i32* %matchChainPos, align 4, !tbaa !7
  %add332 = add i32 %328, %329
  %conv333 = trunc i32 %add332 to i16
  %idxprom334 = zext i16 %conv333 to i32
  %arrayidx335 = getelementptr inbounds i16, i16* %327, i32 %idxprom334
  %330 = load i16, i16* %arrayidx335, align 2, !tbaa !22
  %conv336 = zext i16 %330 to i32
  %331 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %sub337 = sub i32 %331, %conv336
  store i32 %sub337, i32* %matchIndex, align 4, !tbaa !7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup338

cleanup338:                                       ; preds = %cleanup.cont331, %cleanup329, %cleanup
  %332 = bitcast i32* %matchLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %332) #7
  %cleanup.dest339 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest339, label %unreachable [
    i32 0, label %cleanup.cont340
    i32 3, label %while.end
    i32 2, label %while.cond
  ]

cleanup.cont340:                                  ; preds = %cleanup338
  br label %while.cond

while.end:                                        ; preds = %cleanup338, %land.end
  %333 = load i32, i32* %dict.addr, align 4, !tbaa !9
  %cmp341 = icmp eq i32 %333, 1
  br i1 %cmp341, label %land.lhs.true343, label %if.end414

land.lhs.true343:                                 ; preds = %while.end
  %334 = load i32, i32* %nbAttempts, align 4, !tbaa !7
  %tobool344 = icmp ne i32 %334, 0
  br i1 %tobool344, label %land.lhs.true345, label %if.end414

land.lhs.true345:                                 ; preds = %land.lhs.true343
  %335 = load i32, i32* %ipIndex, align 4, !tbaa !7
  %336 = load i32, i32* %lowestMatchIndex, align 4, !tbaa !7
  %sub346 = sub i32 %335, %336
  %cmp347 = icmp ult i32 %sub346, 65535
  br i1 %cmp347, label %if.then349, label %if.end414

if.then349:                                       ; preds = %land.lhs.true345
  %337 = bitcast i32* %dictEndOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %337) #7
  %338 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %dictCtx, align 4, !tbaa !3
  %end350 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %338, i32 0, i32 2
  %339 = load i8*, i8** %end350, align 4, !tbaa !10
  %340 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %dictCtx, align 4, !tbaa !3
  %base351 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %340, i32 0, i32 3
  %341 = load i8*, i8** %base351, align 4, !tbaa !13
  %sub.ptr.lhs.cast352 = ptrtoint i8* %339 to i32
  %sub.ptr.rhs.cast353 = ptrtoint i8* %341 to i32
  %sub.ptr.sub354 = sub i32 %sub.ptr.lhs.cast352, %sub.ptr.rhs.cast353
  store i32 %sub.ptr.sub354, i32* %dictEndOffset, align 4, !tbaa !14
  %342 = bitcast i32* %dictMatchIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %342) #7
  %343 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %dictCtx, align 4, !tbaa !3
  %hashTable355 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %343, i32 0, i32 0
  %344 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %call356 = call i32 @LZ4HC_hashPtr(i8* %344)
  %arrayidx357 = getelementptr inbounds [32768 x i32], [32768 x i32]* %hashTable355, i32 0, i32 %call356
  %345 = load i32, i32* %arrayidx357, align 4, !tbaa !7
  store i32 %345, i32* %dictMatchIndex, align 4, !tbaa !7
  %346 = load i32, i32* %dictMatchIndex, align 4, !tbaa !7
  %347 = load i32, i32* %lowestMatchIndex, align 4, !tbaa !7
  %add358 = add i32 %346, %347
  %348 = load i32, i32* %dictEndOffset, align 4, !tbaa !14
  %sub359 = sub i32 %add358, %348
  store i32 %sub359, i32* %matchIndex, align 4, !tbaa !7
  br label %while.cond360

while.cond360:                                    ; preds = %if.end405, %if.then349
  %349 = load i32, i32* %ipIndex, align 4, !tbaa !7
  %350 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %sub361 = sub i32 %349, %350
  %cmp362 = icmp ule i32 %sub361, 65535
  br i1 %cmp362, label %land.rhs364, label %land.end367

land.rhs364:                                      ; preds = %while.cond360
  %351 = load i32, i32* %nbAttempts, align 4, !tbaa !7
  %dec365 = add nsw i32 %351, -1
  store i32 %dec365, i32* %nbAttempts, align 4, !tbaa !7
  %tobool366 = icmp ne i32 %351, 0
  br label %land.end367

land.end367:                                      ; preds = %land.rhs364, %while.cond360
  %352 = phi i1 [ false, %while.cond360 ], [ %tobool366, %land.rhs364 ]
  br i1 %352, label %while.body368, label %while.end413

while.body368:                                    ; preds = %land.end367
  %353 = bitcast i8** %matchPtr369 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %353) #7
  %354 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %dictCtx, align 4, !tbaa !3
  %base370 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %354, i32 0, i32 3
  %355 = load i8*, i8** %base370, align 4, !tbaa !13
  %356 = load i32, i32* %dictMatchIndex, align 4, !tbaa !7
  %add.ptr371 = getelementptr inbounds i8, i8* %355, i32 %356
  store i8* %add.ptr371, i8** %matchPtr369, align 4, !tbaa !3
  %357 = load i8*, i8** %matchPtr369, align 4, !tbaa !3
  %call372 = call i32 @LZ4_read32(i8* %357)
  %358 = load i32, i32* %pattern, align 4, !tbaa !7
  %cmp373 = icmp eq i32 %call372, %358
  br i1 %cmp373, label %if.then375, label %if.end405

if.then375:                                       ; preds = %while.body368
  %359 = bitcast i32* %mlt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %359) #7
  %360 = bitcast i32* %back376 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %360) #7
  store i32 0, i32* %back376, align 4, !tbaa !7
  %361 = bitcast i8** %vLimit377 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %361) #7
  %362 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %363 = load i32, i32* %dictEndOffset, align 4, !tbaa !14
  %364 = load i32, i32* %dictMatchIndex, align 4, !tbaa !7
  %sub378 = sub i32 %363, %364
  %add.ptr379 = getelementptr inbounds i8, i8* %362, i32 %sub378
  store i8* %add.ptr379, i8** %vLimit377, align 4, !tbaa !3
  %365 = load i8*, i8** %vLimit377, align 4, !tbaa !3
  %366 = load i8*, i8** %iHighLimit.addr, align 4, !tbaa !3
  %cmp380 = icmp ugt i8* %365, %366
  br i1 %cmp380, label %if.then382, label %if.end383

if.then382:                                       ; preds = %if.then375
  %367 = load i8*, i8** %iHighLimit.addr, align 4, !tbaa !3
  store i8* %367, i8** %vLimit377, align 4, !tbaa !3
  br label %if.end383

if.end383:                                        ; preds = %if.then382, %if.then375
  %368 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %add.ptr384 = getelementptr inbounds i8, i8* %368, i32 4
  %369 = load i8*, i8** %matchPtr369, align 4, !tbaa !3
  %add.ptr385 = getelementptr inbounds i8, i8* %369, i32 4
  %370 = load i8*, i8** %vLimit377, align 4, !tbaa !3
  %call386 = call i32 @LZ4_count(i8* %add.ptr384, i8* %add.ptr385, i8* %370)
  %add387 = add nsw i32 %call386, 4
  store i32 %add387, i32* %mlt, align 4, !tbaa !7
  %371 = load i32, i32* %lookBackLength, align 4, !tbaa !7
  %tobool388 = icmp ne i32 %371, 0
  br i1 %tobool388, label %cond.true389, label %cond.false394

cond.true389:                                     ; preds = %if.end383
  %372 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %373 = load i8*, i8** %matchPtr369, align 4, !tbaa !3
  %374 = load i8*, i8** %iLowLimit.addr, align 4, !tbaa !3
  %375 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %dictCtx, align 4, !tbaa !3
  %base390 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %375, i32 0, i32 3
  %376 = load i8*, i8** %base390, align 4, !tbaa !13
  %377 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %dictCtx, align 4, !tbaa !3
  %dictLimit391 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %377, i32 0, i32 5
  %378 = load i32, i32* %dictLimit391, align 4, !tbaa !18
  %add.ptr392 = getelementptr inbounds i8, i8* %376, i32 %378
  %call393 = call i32 @LZ4HC_countBack(i8* %372, i8* %373, i8* %374, i8* %add.ptr392)
  br label %cond.end395

cond.false394:                                    ; preds = %if.end383
  br label %cond.end395

cond.end395:                                      ; preds = %cond.false394, %cond.true389
  %cond396 = phi i32 [ %call393, %cond.true389 ], [ 0, %cond.false394 ]
  store i32 %cond396, i32* %back376, align 4, !tbaa !7
  %379 = load i32, i32* %back376, align 4, !tbaa !7
  %380 = load i32, i32* %mlt, align 4, !tbaa !7
  %sub397 = sub nsw i32 %380, %379
  store i32 %sub397, i32* %mlt, align 4, !tbaa !7
  %381 = load i32, i32* %mlt, align 4, !tbaa !7
  %382 = load i32, i32* %longest.addr, align 4, !tbaa !7
  %cmp398 = icmp sgt i32 %381, %382
  br i1 %cmp398, label %if.then400, label %if.end404

if.then400:                                       ; preds = %cond.end395
  %383 = load i32, i32* %mlt, align 4, !tbaa !7
  store i32 %383, i32* %longest.addr, align 4, !tbaa !7
  %384 = load i8*, i8** %base, align 4, !tbaa !3
  %385 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %add.ptr401 = getelementptr inbounds i8, i8* %384, i32 %385
  %386 = load i32, i32* %back376, align 4, !tbaa !7
  %add.ptr402 = getelementptr inbounds i8, i8* %add.ptr401, i32 %386
  %387 = load i8**, i8*** %matchpos.addr, align 4, !tbaa !3
  store i8* %add.ptr402, i8** %387, align 4, !tbaa !3
  %388 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %389 = load i32, i32* %back376, align 4, !tbaa !7
  %add.ptr403 = getelementptr inbounds i8, i8* %388, i32 %389
  %390 = load i8**, i8*** %startpos.addr, align 4, !tbaa !3
  store i8* %add.ptr403, i8** %390, align 4, !tbaa !3
  br label %if.end404

if.end404:                                        ; preds = %if.then400, %cond.end395
  %391 = bitcast i8** %vLimit377 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %391) #7
  %392 = bitcast i32* %back376 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %392) #7
  %393 = bitcast i32* %mlt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %393) #7
  br label %if.end405

if.end405:                                        ; preds = %if.end404, %while.body368
  %394 = bitcast i32* %nextOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %394) #7
  %395 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %dictCtx, align 4, !tbaa !3
  %chainTable406 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %395, i32 0, i32 1
  %396 = load i32, i32* %dictMatchIndex, align 4, !tbaa !7
  %conv407 = trunc i32 %396 to i16
  %idxprom408 = zext i16 %conv407 to i32
  %arrayidx409 = getelementptr inbounds [65536 x i16], [65536 x i16]* %chainTable406, i32 0, i32 %idxprom408
  %397 = load i16, i16* %arrayidx409, align 2, !tbaa !22
  %conv410 = zext i16 %397 to i32
  store i32 %conv410, i32* %nextOffset, align 4, !tbaa !7
  %398 = load i32, i32* %nextOffset, align 4, !tbaa !7
  %399 = load i32, i32* %dictMatchIndex, align 4, !tbaa !7
  %sub411 = sub i32 %399, %398
  store i32 %sub411, i32* %dictMatchIndex, align 4, !tbaa !7
  %400 = load i32, i32* %nextOffset, align 4, !tbaa !7
  %401 = load i32, i32* %matchIndex, align 4, !tbaa !7
  %sub412 = sub i32 %401, %400
  store i32 %sub412, i32* %matchIndex, align 4, !tbaa !7
  %402 = bitcast i32* %nextOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %402) #7
  %403 = bitcast i8** %matchPtr369 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %403) #7
  br label %while.cond360

while.end413:                                     ; preds = %land.end367
  %404 = bitcast i32* %dictMatchIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %404) #7
  %405 = bitcast i32* %dictEndOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %405) #7
  br label %if.end414

if.end414:                                        ; preds = %while.end413, %land.lhs.true345, %land.lhs.true343, %while.end
  %406 = load i32, i32* %longest.addr, align 4, !tbaa !7
  store i32 1, i32* %cleanup.dest.slot, align 4
  %407 = bitcast i32* %srcPatternLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %407) #7
  %408 = bitcast i32* %repeat to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %408) #7
  %409 = bitcast i32* %matchIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %409) #7
  %410 = bitcast i32* %pattern to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %410) #7
  %411 = bitcast i32* %matchChainPos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %411) #7
  %412 = bitcast i32* %nbAttempts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %412) #7
  %413 = bitcast i32* %lookBackLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %413) #7
  %414 = bitcast i8** %dictBase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %414) #7
  %415 = bitcast i32* %lowestMatchIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %415) #7
  %416 = bitcast i32* %ipIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %416) #7
  %417 = bitcast i8** %lowPrefixPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %417) #7
  %418 = bitcast i32* %dictLimit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %418) #7
  %419 = bitcast i8** %base to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %419) #7
  %420 = bitcast %struct.LZ4HC_CCtx_internal** %dictCtx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %420) #7
  %421 = bitcast i32** %HashTable to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %421) #7
  %422 = bitcast i16** %chainTable to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %422) #7
  ret i32 %406

unreachable:                                      ; preds = %cleanup338
  unreachable
}

; Function Attrs: alwaysinline nounwind
define internal i32 @LZ4HC_encodeSequence(i8** %ip, i8** %op, i8** %anchor, i32 %matchLength, i8* %match, i32 %limit, i8* %oend) #3 {
entry:
  %retval = alloca i32, align 4
  %ip.addr = alloca i8**, align 4
  %op.addr = alloca i8**, align 4
  %anchor.addr = alloca i8**, align 4
  %matchLength.addr = alloca i32, align 4
  %match.addr = alloca i8*, align 4
  %limit.addr = alloca i32, align 4
  %oend.addr = alloca i8*, align 4
  %length = alloca i32, align 4
  %token = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %len = alloca i32, align 4
  store i8** %ip, i8*** %ip.addr, align 4, !tbaa !3
  store i8** %op, i8*** %op.addr, align 4, !tbaa !3
  store i8** %anchor, i8*** %anchor.addr, align 4, !tbaa !3
  store i32 %matchLength, i32* %matchLength.addr, align 4, !tbaa !7
  store i8* %match, i8** %match.addr, align 4, !tbaa !3
  store i32 %limit, i32* %limit.addr, align 4, !tbaa !9
  store i8* %oend, i8** %oend.addr, align 4, !tbaa !3
  %0 = bitcast i32* %length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast i8** %token to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i8**, i8*** %op.addr, align 4, !tbaa !3
  %3 = load i8*, i8** %2, align 4, !tbaa !3
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 1
  store i8* %incdec.ptr, i8** %2, align 4, !tbaa !3
  store i8* %3, i8** %token, align 4, !tbaa !3
  %4 = load i8**, i8*** %ip.addr, align 4, !tbaa !3
  %5 = load i8*, i8** %4, align 4, !tbaa !3
  %6 = load i8**, i8*** %anchor.addr, align 4, !tbaa !3
  %7 = load i8*, i8** %6, align 4, !tbaa !3
  %sub.ptr.lhs.cast = ptrtoint i8* %5 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %7 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %length, align 4, !tbaa !14
  %8 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %tobool = icmp ne i32 %8, 0
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %9 = load i8**, i8*** %op.addr, align 4, !tbaa !3
  %10 = load i8*, i8** %9, align 4, !tbaa !3
  %11 = load i32, i32* %length, align 4, !tbaa !14
  %div = udiv i32 %11, 255
  %add.ptr = getelementptr inbounds i8, i8* %10, i32 %div
  %12 = load i32, i32* %length, align 4, !tbaa !14
  %add.ptr1 = getelementptr inbounds i8, i8* %add.ptr, i32 %12
  %add.ptr2 = getelementptr inbounds i8, i8* %add.ptr1, i32 8
  %13 = load i8*, i8** %oend.addr, align 4, !tbaa !3
  %cmp = icmp ugt i8* %add.ptr2, %13
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %land.lhs.true, %entry
  %14 = load i32, i32* %length, align 4, !tbaa !14
  %cmp3 = icmp uge i32 %14, 15
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.end
  %15 = bitcast i32* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #7
  %16 = load i32, i32* %length, align 4, !tbaa !14
  %sub = sub i32 %16, 15
  store i32 %sub, i32* %len, align 4, !tbaa !14
  %17 = load i8*, i8** %token, align 4, !tbaa !3
  store i8 -16, i8* %17, align 1, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then4
  %18 = load i32, i32* %len, align 4, !tbaa !14
  %cmp5 = icmp uge i32 %18, 255
  br i1 %cmp5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %19 = load i8**, i8*** %op.addr, align 4, !tbaa !3
  %20 = load i8*, i8** %19, align 4, !tbaa !3
  %incdec.ptr6 = getelementptr inbounds i8, i8* %20, i32 1
  store i8* %incdec.ptr6, i8** %19, align 4, !tbaa !3
  store i8 -1, i8* %20, align 1, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %len, align 4, !tbaa !14
  %sub7 = sub i32 %21, 255
  store i32 %sub7, i32* %len, align 4, !tbaa !14
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %22 = load i32, i32* %len, align 4, !tbaa !14
  %conv = trunc i32 %22 to i8
  %23 = load i8**, i8*** %op.addr, align 4, !tbaa !3
  %24 = load i8*, i8** %23, align 4, !tbaa !3
  %incdec.ptr8 = getelementptr inbounds i8, i8* %24, i32 1
  store i8* %incdec.ptr8, i8** %23, align 4, !tbaa !3
  store i8 %conv, i8* %24, align 1, !tbaa !9
  %25 = bitcast i32* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #7
  br label %if.end10

if.else:                                          ; preds = %if.end
  %26 = load i32, i32* %length, align 4, !tbaa !14
  %shl = shl i32 %26, 4
  %conv9 = trunc i32 %shl to i8
  %27 = load i8*, i8** %token, align 4, !tbaa !3
  store i8 %conv9, i8* %27, align 1, !tbaa !9
  br label %if.end10

if.end10:                                         ; preds = %if.else, %for.end
  %28 = load i8**, i8*** %op.addr, align 4, !tbaa !3
  %29 = load i8*, i8** %28, align 4, !tbaa !3
  %30 = load i8**, i8*** %anchor.addr, align 4, !tbaa !3
  %31 = load i8*, i8** %30, align 4, !tbaa !3
  %32 = load i8**, i8*** %op.addr, align 4, !tbaa !3
  %33 = load i8*, i8** %32, align 4, !tbaa !3
  %34 = load i32, i32* %length, align 4, !tbaa !14
  %add.ptr11 = getelementptr inbounds i8, i8* %33, i32 %34
  call void @LZ4_wildCopy8(i8* %29, i8* %31, i8* %add.ptr11)
  %35 = load i32, i32* %length, align 4, !tbaa !14
  %36 = load i8**, i8*** %op.addr, align 4, !tbaa !3
  %37 = load i8*, i8** %36, align 4, !tbaa !3
  %add.ptr12 = getelementptr inbounds i8, i8* %37, i32 %35
  store i8* %add.ptr12, i8** %36, align 4, !tbaa !3
  %38 = load i8**, i8*** %op.addr, align 4, !tbaa !3
  %39 = load i8*, i8** %38, align 4, !tbaa !3
  %40 = load i8**, i8*** %ip.addr, align 4, !tbaa !3
  %41 = load i8*, i8** %40, align 4, !tbaa !3
  %42 = load i8*, i8** %match.addr, align 4, !tbaa !3
  %sub.ptr.lhs.cast13 = ptrtoint i8* %41 to i32
  %sub.ptr.rhs.cast14 = ptrtoint i8* %42 to i32
  %sub.ptr.sub15 = sub i32 %sub.ptr.lhs.cast13, %sub.ptr.rhs.cast14
  %conv16 = trunc i32 %sub.ptr.sub15 to i16
  call void @LZ4_writeLE16(i8* %39, i16 zeroext %conv16)
  %43 = load i8**, i8*** %op.addr, align 4, !tbaa !3
  %44 = load i8*, i8** %43, align 4, !tbaa !3
  %add.ptr17 = getelementptr inbounds i8, i8* %44, i32 2
  store i8* %add.ptr17, i8** %43, align 4, !tbaa !3
  %45 = load i32, i32* %matchLength.addr, align 4, !tbaa !7
  %sub18 = sub i32 %45, 4
  store i32 %sub18, i32* %length, align 4, !tbaa !14
  %46 = load i32, i32* %limit.addr, align 4, !tbaa !9
  %tobool19 = icmp ne i32 %46, 0
  br i1 %tobool19, label %land.lhs.true20, label %if.end27

land.lhs.true20:                                  ; preds = %if.end10
  %47 = load i8**, i8*** %op.addr, align 4, !tbaa !3
  %48 = load i8*, i8** %47, align 4, !tbaa !3
  %49 = load i32, i32* %length, align 4, !tbaa !14
  %div21 = udiv i32 %49, 255
  %add.ptr22 = getelementptr inbounds i8, i8* %48, i32 %div21
  %add.ptr23 = getelementptr inbounds i8, i8* %add.ptr22, i32 6
  %50 = load i8*, i8** %oend.addr, align 4, !tbaa !3
  %cmp24 = icmp ugt i8* %add.ptr23, %50
  br i1 %cmp24, label %if.then26, label %if.end27

if.then26:                                        ; preds = %land.lhs.true20
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end27:                                         ; preds = %land.lhs.true20, %if.end10
  %51 = load i32, i32* %length, align 4, !tbaa !14
  %cmp28 = icmp uge i32 %51, 15
  br i1 %cmp28, label %if.then30, label %if.else51

if.then30:                                        ; preds = %if.end27
  %52 = load i8*, i8** %token, align 4, !tbaa !3
  %53 = load i8, i8* %52, align 1, !tbaa !9
  %conv31 = zext i8 %53 to i32
  %add = add i32 %conv31, 15
  %conv32 = trunc i32 %add to i8
  store i8 %conv32, i8* %52, align 1, !tbaa !9
  %54 = load i32, i32* %length, align 4, !tbaa !14
  %sub33 = sub i32 %54, 15
  store i32 %sub33, i32* %length, align 4, !tbaa !14
  br label %for.cond34

for.cond34:                                       ; preds = %for.inc40, %if.then30
  %55 = load i32, i32* %length, align 4, !tbaa !14
  %cmp35 = icmp uge i32 %55, 510
  br i1 %cmp35, label %for.body37, label %for.end42

for.body37:                                       ; preds = %for.cond34
  %56 = load i8**, i8*** %op.addr, align 4, !tbaa !3
  %57 = load i8*, i8** %56, align 4, !tbaa !3
  %incdec.ptr38 = getelementptr inbounds i8, i8* %57, i32 1
  store i8* %incdec.ptr38, i8** %56, align 4, !tbaa !3
  store i8 -1, i8* %57, align 1, !tbaa !9
  %58 = load i8**, i8*** %op.addr, align 4, !tbaa !3
  %59 = load i8*, i8** %58, align 4, !tbaa !3
  %incdec.ptr39 = getelementptr inbounds i8, i8* %59, i32 1
  store i8* %incdec.ptr39, i8** %58, align 4, !tbaa !3
  store i8 -1, i8* %59, align 1, !tbaa !9
  br label %for.inc40

for.inc40:                                        ; preds = %for.body37
  %60 = load i32, i32* %length, align 4, !tbaa !14
  %sub41 = sub i32 %60, 510
  store i32 %sub41, i32* %length, align 4, !tbaa !14
  br label %for.cond34

for.end42:                                        ; preds = %for.cond34
  %61 = load i32, i32* %length, align 4, !tbaa !14
  %cmp43 = icmp uge i32 %61, 255
  br i1 %cmp43, label %if.then45, label %if.end48

if.then45:                                        ; preds = %for.end42
  %62 = load i32, i32* %length, align 4, !tbaa !14
  %sub46 = sub i32 %62, 255
  store i32 %sub46, i32* %length, align 4, !tbaa !14
  %63 = load i8**, i8*** %op.addr, align 4, !tbaa !3
  %64 = load i8*, i8** %63, align 4, !tbaa !3
  %incdec.ptr47 = getelementptr inbounds i8, i8* %64, i32 1
  store i8* %incdec.ptr47, i8** %63, align 4, !tbaa !3
  store i8 -1, i8* %64, align 1, !tbaa !9
  br label %if.end48

if.end48:                                         ; preds = %if.then45, %for.end42
  %65 = load i32, i32* %length, align 4, !tbaa !14
  %conv49 = trunc i32 %65 to i8
  %66 = load i8**, i8*** %op.addr, align 4, !tbaa !3
  %67 = load i8*, i8** %66, align 4, !tbaa !3
  %incdec.ptr50 = getelementptr inbounds i8, i8* %67, i32 1
  store i8* %incdec.ptr50, i8** %66, align 4, !tbaa !3
  store i8 %conv49, i8* %67, align 1, !tbaa !9
  br label %if.end57

if.else51:                                        ; preds = %if.end27
  %68 = load i32, i32* %length, align 4, !tbaa !14
  %conv52 = trunc i32 %68 to i8
  %conv53 = zext i8 %conv52 to i32
  %69 = load i8*, i8** %token, align 4, !tbaa !3
  %70 = load i8, i8* %69, align 1, !tbaa !9
  %conv54 = zext i8 %70 to i32
  %add55 = add nsw i32 %conv54, %conv53
  %conv56 = trunc i32 %add55 to i8
  store i8 %conv56, i8* %69, align 1, !tbaa !9
  br label %if.end57

if.end57:                                         ; preds = %if.else51, %if.end48
  %71 = load i32, i32* %matchLength.addr, align 4, !tbaa !7
  %72 = load i8**, i8*** %ip.addr, align 4, !tbaa !3
  %73 = load i8*, i8** %72, align 4, !tbaa !3
  %add.ptr58 = getelementptr inbounds i8, i8* %73, i32 %71
  store i8* %add.ptr58, i8** %72, align 4, !tbaa !3
  %74 = load i8**, i8*** %ip.addr, align 4, !tbaa !3
  %75 = load i8*, i8** %74, align 4, !tbaa !3
  %76 = load i8**, i8*** %anchor.addr, align 4, !tbaa !3
  store i8* %75, i8** %76, align 4, !tbaa !3
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end57, %if.then26, %if.then
  %77 = bitcast i8** %token to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #7
  %78 = bitcast i32* %length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #7
  %79 = load i32, i32* %retval, align 4
  ret i32 %79
}

; Function Attrs: nounwind
define internal i32 @LZ4_read32(i8* %ptr) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !3
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  %1 = bitcast i8* %0 to %union.unalign*
  %u32 = bitcast %union.unalign* %1 to i32*
  %2 = load i32, i32* %u32, align 1, !tbaa !9
  ret i32 %2
}

; Function Attrs: nounwind
define internal i32 @LZ4HC_hashPtr(i8* %ptr) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !3
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_read32(i8* %0)
  %mul = mul i32 %call, -1640531535
  %shr = lshr i32 %mul, 17
  ret i32 %shr
}

; Function Attrs: nounwind
define internal zeroext i16 @LZ4_read16(i8* %ptr) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !3
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  %1 = bitcast i8* %0 to %union.unalign*
  %u16 = bitcast %union.unalign* %1 to i16*
  %2 = load i16, i16* %u16, align 1, !tbaa !9
  ret i16 %2
}

; Function Attrs: alwaysinline nounwind
define internal i32 @LZ4HC_countBack(i8* %ip, i8* %match, i8* %iMin, i8* %mMin) #3 {
entry:
  %ip.addr = alloca i8*, align 4
  %match.addr = alloca i8*, align 4
  %iMin.addr = alloca i8*, align 4
  %mMin.addr = alloca i8*, align 4
  %back = alloca i32, align 4
  %min = alloca i32, align 4
  store i8* %ip, i8** %ip.addr, align 4, !tbaa !3
  store i8* %match, i8** %match.addr, align 4, !tbaa !3
  store i8* %iMin, i8** %iMin.addr, align 4, !tbaa !3
  store i8* %mMin, i8** %mMin.addr, align 4, !tbaa !3
  %0 = bitcast i32* %back to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 0, i32* %back, align 4, !tbaa !7
  %1 = bitcast i32* %min to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i8*, i8** %iMin.addr, align 4, !tbaa !3
  %3 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %sub.ptr.lhs.cast = ptrtoint i8* %2 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %4 = load i8*, i8** %mMin.addr, align 4, !tbaa !3
  %5 = load i8*, i8** %match.addr, align 4, !tbaa !3
  %sub.ptr.lhs.cast1 = ptrtoint i8* %4 to i32
  %sub.ptr.rhs.cast2 = ptrtoint i8* %5 to i32
  %sub.ptr.sub3 = sub i32 %sub.ptr.lhs.cast1, %sub.ptr.rhs.cast2
  %cmp = icmp sgt i32 %sub.ptr.sub, %sub.ptr.sub3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %6 = load i8*, i8** %iMin.addr, align 4, !tbaa !3
  %7 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %sub.ptr.lhs.cast4 = ptrtoint i8* %6 to i32
  %sub.ptr.rhs.cast5 = ptrtoint i8* %7 to i32
  %sub.ptr.sub6 = sub i32 %sub.ptr.lhs.cast4, %sub.ptr.rhs.cast5
  br label %cond.end

cond.false:                                       ; preds = %entry
  %8 = load i8*, i8** %mMin.addr, align 4, !tbaa !3
  %9 = load i8*, i8** %match.addr, align 4, !tbaa !3
  %sub.ptr.lhs.cast7 = ptrtoint i8* %8 to i32
  %sub.ptr.rhs.cast8 = ptrtoint i8* %9 to i32
  %sub.ptr.sub9 = sub i32 %sub.ptr.lhs.cast7, %sub.ptr.rhs.cast8
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub.ptr.sub6, %cond.true ], [ %sub.ptr.sub9, %cond.false ]
  store i32 %cond, i32* %min, align 4, !tbaa !7
  br label %while.cond

while.cond:                                       ; preds = %while.body, %cond.end
  %10 = load i32, i32* %back, align 4, !tbaa !7
  %11 = load i32, i32* %min, align 4, !tbaa !7
  %cmp10 = icmp sgt i32 %10, %11
  br i1 %cmp10, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %12 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %13 = load i32, i32* %back, align 4, !tbaa !7
  %sub = sub nsw i32 %13, 1
  %arrayidx = getelementptr inbounds i8, i8* %12, i32 %sub
  %14 = load i8, i8* %arrayidx, align 1, !tbaa !9
  %conv = zext i8 %14 to i32
  %15 = load i8*, i8** %match.addr, align 4, !tbaa !3
  %16 = load i32, i32* %back, align 4, !tbaa !7
  %sub11 = sub nsw i32 %16, 1
  %arrayidx12 = getelementptr inbounds i8, i8* %15, i32 %sub11
  %17 = load i8, i8* %arrayidx12, align 1, !tbaa !9
  %conv13 = zext i8 %17 to i32
  %cmp14 = icmp eq i32 %conv, %conv13
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %18 = phi i1 [ false, %while.cond ], [ %cmp14, %land.rhs ]
  br i1 %18, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %19 = load i32, i32* %back, align 4, !tbaa !7
  %dec = add nsw i32 %19, -1
  store i32 %dec, i32* %back, align 4, !tbaa !7
  br label %while.cond

while.end:                                        ; preds = %land.end
  %20 = load i32, i32* %back, align 4, !tbaa !7
  %21 = bitcast i32* %min to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #7
  %22 = bitcast i32* %back to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #7
  ret i32 %20
}

; Function Attrs: alwaysinline nounwind
define internal i32 @LZ4_count(i8* %pIn, i8* %pMatch, i8* %pInLimit) #3 {
entry:
  %retval = alloca i32, align 4
  %pIn.addr = alloca i8*, align 4
  %pMatch.addr = alloca i8*, align 4
  %pInLimit.addr = alloca i8*, align 4
  %pStart = alloca i8*, align 4
  %diff = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %diff17 = alloca i32, align 4
  store i8* %pIn, i8** %pIn.addr, align 4, !tbaa !3
  store i8* %pMatch, i8** %pMatch.addr, align 4, !tbaa !3
  store i8* %pInLimit, i8** %pInLimit.addr, align 4, !tbaa !3
  %0 = bitcast i8** %pStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8*, i8** %pIn.addr, align 4, !tbaa !3
  store i8* %1, i8** %pStart, align 4, !tbaa !3
  %2 = load i8*, i8** %pIn.addr, align 4, !tbaa !3
  %3 = load i8*, i8** %pInLimit.addr, align 4, !tbaa !3
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 -3
  %cmp = icmp ult i8* %2, %add.ptr
  %conv = zext i1 %cmp to i32
  %cmp1 = icmp ne i32 %conv, 0
  %conv2 = zext i1 %cmp1 to i32
  %expval = call i32 @llvm.expect.i32(i32 %conv2, i32 1)
  %tobool = icmp ne i32 %expval, 0
  br i1 %tobool, label %if.then, label %if.end9

if.then:                                          ; preds = %entry
  %4 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load i8*, i8** %pMatch.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_read_ARCH(i8* %5)
  %6 = load i8*, i8** %pIn.addr, align 4, !tbaa !3
  %call3 = call i32 @LZ4_read_ARCH(i8* %6)
  %xor = xor i32 %call, %call3
  store i32 %xor, i32* %diff, align 4, !tbaa !14
  %7 = load i32, i32* %diff, align 4, !tbaa !14
  %tobool4 = icmp ne i32 %7, 0
  br i1 %tobool4, label %if.else, label %if.then5

if.then5:                                         ; preds = %if.then
  %8 = load i8*, i8** %pIn.addr, align 4, !tbaa !3
  %add.ptr6 = getelementptr inbounds i8, i8* %8, i32 4
  store i8* %add.ptr6, i8** %pIn.addr, align 4, !tbaa !3
  %9 = load i8*, i8** %pMatch.addr, align 4, !tbaa !3
  %add.ptr7 = getelementptr inbounds i8, i8* %9, i32 4
  store i8* %add.ptr7, i8** %pMatch.addr, align 4, !tbaa !3
  br label %if.end

if.else:                                          ; preds = %if.then
  %10 = load i32, i32* %diff, align 4, !tbaa !14
  %call8 = call i32 @LZ4_NbCommonBytes(i32 %10)
  store i32 %call8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then5
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.else
  %11 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup55 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end9

if.end9:                                          ; preds = %cleanup.cont, %entry
  br label %while.cond

while.cond:                                       ; preds = %cleanup28, %if.end9
  %12 = load i8*, i8** %pIn.addr, align 4, !tbaa !3
  %13 = load i8*, i8** %pInLimit.addr, align 4, !tbaa !3
  %add.ptr10 = getelementptr inbounds i8, i8* %13, i32 -3
  %cmp11 = icmp ult i8* %12, %add.ptr10
  %conv12 = zext i1 %cmp11 to i32
  %cmp13 = icmp ne i32 %conv12, 0
  %conv14 = zext i1 %cmp13 to i32
  %expval15 = call i32 @llvm.expect.i32(i32 %conv14, i32 1)
  %tobool16 = icmp ne i32 %expval15, 0
  br i1 %tobool16, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %14 = bitcast i32* %diff17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %15 = load i8*, i8** %pMatch.addr, align 4, !tbaa !3
  %call18 = call i32 @LZ4_read_ARCH(i8* %15)
  %16 = load i8*, i8** %pIn.addr, align 4, !tbaa !3
  %call19 = call i32 @LZ4_read_ARCH(i8* %16)
  %xor20 = xor i32 %call18, %call19
  store i32 %xor20, i32* %diff17, align 4, !tbaa !14
  %17 = load i32, i32* %diff17, align 4, !tbaa !14
  %tobool21 = icmp ne i32 %17, 0
  br i1 %tobool21, label %if.end25, label %if.then22

if.then22:                                        ; preds = %while.body
  %18 = load i8*, i8** %pIn.addr, align 4, !tbaa !3
  %add.ptr23 = getelementptr inbounds i8, i8* %18, i32 4
  store i8* %add.ptr23, i8** %pIn.addr, align 4, !tbaa !3
  %19 = load i8*, i8** %pMatch.addr, align 4, !tbaa !3
  %add.ptr24 = getelementptr inbounds i8, i8* %19, i32 4
  store i8* %add.ptr24, i8** %pMatch.addr, align 4, !tbaa !3
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup28

if.end25:                                         ; preds = %while.body
  %20 = load i32, i32* %diff17, align 4, !tbaa !14
  %call26 = call i32 @LZ4_NbCommonBytes(i32 %20)
  %21 = load i8*, i8** %pIn.addr, align 4, !tbaa !3
  %add.ptr27 = getelementptr inbounds i8, i8* %21, i32 %call26
  store i8* %add.ptr27, i8** %pIn.addr, align 4, !tbaa !3
  %22 = load i8*, i8** %pIn.addr, align 4, !tbaa !3
  %23 = load i8*, i8** %pStart, align 4, !tbaa !3
  %sub.ptr.lhs.cast = ptrtoint i8* %22 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %23 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup28

cleanup28:                                        ; preds = %if.end25, %if.then22
  %24 = bitcast i32* %diff17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #7
  %cleanup.dest29 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest29, label %cleanup55 [
    i32 2, label %while.cond
  ]

while.end:                                        ; preds = %while.cond
  %25 = load i8*, i8** %pIn.addr, align 4, !tbaa !3
  %26 = load i8*, i8** %pInLimit.addr, align 4, !tbaa !3
  %add.ptr30 = getelementptr inbounds i8, i8* %26, i32 -1
  %cmp31 = icmp ult i8* %25, %add.ptr30
  br i1 %cmp31, label %land.lhs.true, label %if.end42

land.lhs.true:                                    ; preds = %while.end
  %27 = load i8*, i8** %pMatch.addr, align 4, !tbaa !3
  %call33 = call zeroext i16 @LZ4_read16(i8* %27)
  %conv34 = zext i16 %call33 to i32
  %28 = load i8*, i8** %pIn.addr, align 4, !tbaa !3
  %call35 = call zeroext i16 @LZ4_read16(i8* %28)
  %conv36 = zext i16 %call35 to i32
  %cmp37 = icmp eq i32 %conv34, %conv36
  br i1 %cmp37, label %if.then39, label %if.end42

if.then39:                                        ; preds = %land.lhs.true
  %29 = load i8*, i8** %pIn.addr, align 4, !tbaa !3
  %add.ptr40 = getelementptr inbounds i8, i8* %29, i32 2
  store i8* %add.ptr40, i8** %pIn.addr, align 4, !tbaa !3
  %30 = load i8*, i8** %pMatch.addr, align 4, !tbaa !3
  %add.ptr41 = getelementptr inbounds i8, i8* %30, i32 2
  store i8* %add.ptr41, i8** %pMatch.addr, align 4, !tbaa !3
  br label %if.end42

if.end42:                                         ; preds = %if.then39, %land.lhs.true, %while.end
  %31 = load i8*, i8** %pIn.addr, align 4, !tbaa !3
  %32 = load i8*, i8** %pInLimit.addr, align 4, !tbaa !3
  %cmp43 = icmp ult i8* %31, %32
  br i1 %cmp43, label %land.lhs.true45, label %if.end51

land.lhs.true45:                                  ; preds = %if.end42
  %33 = load i8*, i8** %pMatch.addr, align 4, !tbaa !3
  %34 = load i8, i8* %33, align 1, !tbaa !9
  %conv46 = zext i8 %34 to i32
  %35 = load i8*, i8** %pIn.addr, align 4, !tbaa !3
  %36 = load i8, i8* %35, align 1, !tbaa !9
  %conv47 = zext i8 %36 to i32
  %cmp48 = icmp eq i32 %conv46, %conv47
  br i1 %cmp48, label %if.then50, label %if.end51

if.then50:                                        ; preds = %land.lhs.true45
  %37 = load i8*, i8** %pIn.addr, align 4, !tbaa !3
  %incdec.ptr = getelementptr inbounds i8, i8* %37, i32 1
  store i8* %incdec.ptr, i8** %pIn.addr, align 4, !tbaa !3
  br label %if.end51

if.end51:                                         ; preds = %if.then50, %land.lhs.true45, %if.end42
  %38 = load i8*, i8** %pIn.addr, align 4, !tbaa !3
  %39 = load i8*, i8** %pStart, align 4, !tbaa !3
  %sub.ptr.lhs.cast52 = ptrtoint i8* %38 to i32
  %sub.ptr.rhs.cast53 = ptrtoint i8* %39 to i32
  %sub.ptr.sub54 = sub i32 %sub.ptr.lhs.cast52, %sub.ptr.rhs.cast53
  store i32 %sub.ptr.sub54, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup55

cleanup55:                                        ; preds = %if.end51, %cleanup28, %cleanup
  %40 = bitcast i8** %pStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #7
  %41 = load i32, i32* %retval, align 4
  ret i32 %41
}

; Function Attrs: nounwind
define internal i32 @LZ4HC_countPattern(i8* %ip, i8* %iEnd, i32 %pattern32) #0 {
entry:
  %retval = alloca i32, align 4
  %ip.addr = alloca i8*, align 4
  %iEnd.addr = alloca i8*, align 4
  %pattern32.addr = alloca i32, align 4
  %iStart = alloca i8*, align 4
  %pattern = alloca i32, align 4
  %diff = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %patternByte = alloca i32, align 4
  %bitOffset = alloca i32, align 4
  %byte = alloca i8, align 1
  store i8* %ip, i8** %ip.addr, align 4, !tbaa !3
  store i8* %iEnd, i8** %iEnd.addr, align 4, !tbaa !3
  store i32 %pattern32, i32* %pattern32.addr, align 4, !tbaa !7
  %0 = bitcast i8** %iStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  store i8* %1, i8** %iStart, align 4, !tbaa !3
  %2 = bitcast i32* %pattern to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load i32, i32* %pattern32.addr, align 4, !tbaa !7
  store i32 %3, i32* %pattern, align 4, !tbaa !14
  br label %while.cond

while.cond:                                       ; preds = %cleanup, %entry
  %4 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %5 = load i8*, i8** %iEnd.addr, align 4, !tbaa !3
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 -3
  %cmp = icmp ult i8* %4, %add.ptr
  %conv = zext i1 %cmp to i32
  %cmp1 = icmp ne i32 %conv, 0
  %conv2 = zext i1 %cmp1 to i32
  %expval = call i32 @llvm.expect.i32(i32 %conv2, i32 1)
  %tobool = icmp ne i32 %expval, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %6 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_read_ARCH(i8* %7)
  %8 = load i32, i32* %pattern, align 4, !tbaa !14
  %xor = xor i32 %call, %8
  store i32 %xor, i32* %diff, align 4, !tbaa !14
  %9 = load i32, i32* %diff, align 4, !tbaa !14
  %tobool3 = icmp ne i32 %9, 0
  br i1 %tobool3, label %if.end, label %if.then

if.then:                                          ; preds = %while.body
  %10 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %add.ptr4 = getelementptr inbounds i8, i8* %10, i32 4
  store i8* %add.ptr4, i8** %ip.addr, align 4, !tbaa !3
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %while.body
  %11 = load i32, i32* %diff, align 4, !tbaa !14
  %call5 = call i32 @LZ4_NbCommonBytes(i32 %11)
  %12 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %add.ptr6 = getelementptr inbounds i8, i8* %12, i32 %call5
  store i8* %add.ptr6, i8** %ip.addr, align 4, !tbaa !3
  %13 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %14 = load i8*, i8** %iStart, align 4, !tbaa !3
  %sub.ptr.lhs.cast = ptrtoint i8* %13 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %14 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %15 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup40 [
    i32 2, label %while.cond
  ]

while.end:                                        ; preds = %while.cond
  %call7 = call i32 @LZ4_isLittleEndian()
  %tobool8 = icmp ne i32 %call7, 0
  br i1 %tobool8, label %if.then9, label %if.else

if.then9:                                         ; preds = %while.end
  %16 = bitcast i32* %patternByte to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %17 = load i32, i32* %pattern, align 4, !tbaa !14
  store i32 %17, i32* %patternByte, align 4, !tbaa !14
  br label %while.cond10

while.cond10:                                     ; preds = %while.body18, %if.then9
  %18 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %19 = load i8*, i8** %iEnd.addr, align 4, !tbaa !3
  %cmp11 = icmp ult i8* %18, %19
  br i1 %cmp11, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond10
  %20 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %21 = load i8, i8* %20, align 1, !tbaa !9
  %conv13 = zext i8 %21 to i32
  %22 = load i32, i32* %patternByte, align 4, !tbaa !14
  %conv14 = trunc i32 %22 to i8
  %conv15 = zext i8 %conv14 to i32
  %cmp16 = icmp eq i32 %conv13, %conv15
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond10
  %23 = phi i1 [ false, %while.cond10 ], [ %cmp16, %land.rhs ]
  br i1 %23, label %while.body18, label %while.end19

while.body18:                                     ; preds = %land.end
  %24 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %incdec.ptr = getelementptr inbounds i8, i8* %24, i32 1
  store i8* %incdec.ptr, i8** %ip.addr, align 4, !tbaa !3
  %25 = load i32, i32* %patternByte, align 4, !tbaa !14
  %shr = lshr i32 %25, 8
  store i32 %shr, i32* %patternByte, align 4, !tbaa !14
  br label %while.cond10

while.end19:                                      ; preds = %land.end
  %26 = bitcast i32* %patternByte to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #7
  br label %if.end36

if.else:                                          ; preds = %while.end
  %27 = bitcast i32* %bitOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #7
  store i32 24, i32* %bitOffset, align 4, !tbaa !7
  br label %while.cond20

while.cond20:                                     ; preds = %cleanup.cont, %if.else
  %28 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %29 = load i8*, i8** %iEnd.addr, align 4, !tbaa !3
  %cmp21 = icmp ult i8* %28, %29
  br i1 %cmp21, label %while.body23, label %while.end35

while.body23:                                     ; preds = %while.cond20
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %byte) #7
  %30 = load i32, i32* %pattern, align 4, !tbaa !14
  %31 = load i32, i32* %bitOffset, align 4, !tbaa !7
  %shr24 = lshr i32 %30, %31
  %conv25 = trunc i32 %shr24 to i8
  store i8 %conv25, i8* %byte, align 1, !tbaa !9
  %32 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %33 = load i8, i8* %32, align 1, !tbaa !9
  %conv26 = zext i8 %33 to i32
  %34 = load i8, i8* %byte, align 1, !tbaa !9
  %conv27 = zext i8 %34 to i32
  %cmp28 = icmp ne i32 %conv26, %conv27
  br i1 %cmp28, label %if.then30, label %if.end31

if.then30:                                        ; preds = %while.body23
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup33

if.end31:                                         ; preds = %while.body23
  %35 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %incdec.ptr32 = getelementptr inbounds i8, i8* %35, i32 1
  store i8* %incdec.ptr32, i8** %ip.addr, align 4, !tbaa !3
  %36 = load i32, i32* %bitOffset, align 4, !tbaa !7
  %sub = sub i32 %36, 8
  store i32 %sub, i32* %bitOffset, align 4, !tbaa !7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup33

cleanup33:                                        ; preds = %if.end31, %if.then30
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %byte) #7
  %cleanup.dest34 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest34, label %unreachable [
    i32 0, label %cleanup.cont
    i32 7, label %while.end35
  ]

cleanup.cont:                                     ; preds = %cleanup33
  br label %while.cond20

while.end35:                                      ; preds = %cleanup33, %while.cond20
  %37 = bitcast i32* %bitOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #7
  br label %if.end36

if.end36:                                         ; preds = %while.end35, %while.end19
  %38 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %39 = load i8*, i8** %iStart, align 4, !tbaa !3
  %sub.ptr.lhs.cast37 = ptrtoint i8* %38 to i32
  %sub.ptr.rhs.cast38 = ptrtoint i8* %39 to i32
  %sub.ptr.sub39 = sub i32 %sub.ptr.lhs.cast37, %sub.ptr.rhs.cast38
  store i32 %sub.ptr.sub39, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup40

cleanup40:                                        ; preds = %if.end36, %cleanup
  %40 = bitcast i32* %pattern to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #7
  %41 = bitcast i8** %iStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #7
  %42 = load i32, i32* %retval, align 4
  ret i32 %42

unreachable:                                      ; preds = %cleanup33
  unreachable
}

; Function Attrs: nounwind
define internal i32 @LZ4HC_protectDictEnd(i32 %dictLimit, i32 %matchIndex) #0 {
entry:
  %dictLimit.addr = alloca i32, align 4
  %matchIndex.addr = alloca i32, align 4
  store i32 %dictLimit, i32* %dictLimit.addr, align 4, !tbaa !7
  store i32 %matchIndex, i32* %matchIndex.addr, align 4, !tbaa !7
  %0 = load i32, i32* %dictLimit.addr, align 4, !tbaa !7
  %sub = sub i32 %0, 1
  %1 = load i32, i32* %matchIndex.addr, align 4, !tbaa !7
  %sub1 = sub i32 %sub, %1
  %cmp = icmp uge i32 %sub1, 3
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: nounwind
define internal i32 @LZ4HC_rotatePattern(i32 %rotate, i32 %pattern) #0 {
entry:
  %retval = alloca i32, align 4
  %rotate.addr = alloca i32, align 4
  %pattern.addr = alloca i32, align 4
  %bitsToRotate = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i32 %rotate, i32* %rotate.addr, align 4, !tbaa !14
  store i32 %pattern, i32* %pattern.addr, align 4, !tbaa !7
  %0 = bitcast i32* %bitsToRotate to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %rotate.addr, align 4, !tbaa !14
  %and = and i32 %1, 3
  %shl = shl i32 %and, 3
  store i32 %shl, i32* %bitsToRotate, align 4, !tbaa !14
  %2 = load i32, i32* %bitsToRotate, align 4, !tbaa !14
  %cmp = icmp eq i32 %2, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i32, i32* %pattern.addr, align 4, !tbaa !7
  store i32 %3, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %4 = load i32, i32* %pattern.addr, align 4, !tbaa !7
  %5 = load i32, i32* %bitsToRotate, align 4, !tbaa !14
  %shl1 = shl i32 %4, %5
  %6 = load i32, i32* %pattern.addr, align 4, !tbaa !7
  %7 = load i32, i32* %bitsToRotate, align 4, !tbaa !14
  %sub = sub nsw i32 32, %7
  %shr = lshr i32 %6, %sub
  %or = or i32 %shl1, %shr
  store i32 %or, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %8 = bitcast i32* %bitsToRotate to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #7
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

; Function Attrs: nounwind
define internal i32 @LZ4HC_reverseCountPattern(i8* %ip, i8* %iLow, i32 %pattern) #0 {
entry:
  %ip.addr = alloca i8*, align 4
  %iLow.addr = alloca i8*, align 4
  %pattern.addr = alloca i32, align 4
  %iStart = alloca i8*, align 4
  %bytePtr = alloca i8*, align 4
  store i8* %ip, i8** %ip.addr, align 4, !tbaa !3
  store i8* %iLow, i8** %iLow.addr, align 4, !tbaa !3
  store i32 %pattern, i32* %pattern.addr, align 4, !tbaa !7
  %0 = bitcast i8** %iStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  store i8* %1, i8** %iStart, align 4, !tbaa !3
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %2 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %3 = load i8*, i8** %iLow.addr, align 4, !tbaa !3
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 4
  %cmp = icmp uge i8* %2, %add.ptr
  %conv = zext i1 %cmp to i32
  %cmp1 = icmp ne i32 %conv, 0
  %conv2 = zext i1 %cmp1 to i32
  %expval = call i32 @llvm.expect.i32(i32 %conv2, i32 1)
  %tobool = icmp ne i32 %expval, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %add.ptr3 = getelementptr inbounds i8, i8* %4, i32 -4
  %call = call i32 @LZ4_read32(i8* %add.ptr3)
  %5 = load i32, i32* %pattern.addr, align 4, !tbaa !7
  %cmp4 = icmp ne i32 %call, %5
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  br label %while.end

if.end:                                           ; preds = %while.body
  %6 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %add.ptr6 = getelementptr inbounds i8, i8* %6, i32 -4
  store i8* %add.ptr6, i8** %ip.addr, align 4, !tbaa !3
  br label %while.cond

while.end:                                        ; preds = %if.then, %while.cond
  %7 = bitcast i8** %bytePtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = bitcast i32* %pattern.addr to i8*
  %add.ptr7 = getelementptr inbounds i8, i8* %8, i32 3
  store i8* %add.ptr7, i8** %bytePtr, align 4, !tbaa !3
  br label %while.cond8

while.cond8:                                      ; preds = %if.end21, %while.end
  %9 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %10 = load i8*, i8** %iLow.addr, align 4, !tbaa !3
  %cmp9 = icmp ugt i8* %9, %10
  %conv10 = zext i1 %cmp9 to i32
  %cmp11 = icmp ne i32 %conv10, 0
  %conv12 = zext i1 %cmp11 to i32
  %expval13 = call i32 @llvm.expect.i32(i32 %conv12, i32 1)
  %tobool14 = icmp ne i32 %expval13, 0
  br i1 %tobool14, label %while.body15, label %while.end23

while.body15:                                     ; preds = %while.cond8
  %11 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %arrayidx = getelementptr inbounds i8, i8* %11, i32 -1
  %12 = load i8, i8* %arrayidx, align 1, !tbaa !9
  %conv16 = zext i8 %12 to i32
  %13 = load i8*, i8** %bytePtr, align 4, !tbaa !3
  %14 = load i8, i8* %13, align 1, !tbaa !9
  %conv17 = zext i8 %14 to i32
  %cmp18 = icmp ne i32 %conv16, %conv17
  br i1 %cmp18, label %if.then20, label %if.end21

if.then20:                                        ; preds = %while.body15
  br label %while.end23

if.end21:                                         ; preds = %while.body15
  %15 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %incdec.ptr = getelementptr inbounds i8, i8* %15, i32 -1
  store i8* %incdec.ptr, i8** %ip.addr, align 4, !tbaa !3
  %16 = load i8*, i8** %bytePtr, align 4, !tbaa !3
  %incdec.ptr22 = getelementptr inbounds i8, i8* %16, i32 -1
  store i8* %incdec.ptr22, i8** %bytePtr, align 4, !tbaa !3
  br label %while.cond8

while.end23:                                      ; preds = %if.then20, %while.cond8
  %17 = bitcast i8** %bytePtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  %18 = load i8*, i8** %iStart, align 4, !tbaa !3
  %19 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %sub.ptr.lhs.cast = ptrtoint i8* %18 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %19 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %20 = bitcast i8** %iStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #7
  ret i32 %sub.ptr.sub
}

; Function Attrs: nounwind readnone willreturn
declare i32 @llvm.expect.i32(i32, i32) #5

; Function Attrs: nounwind
define internal i32 @LZ4_read_ARCH(i8* %ptr) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !3
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  %1 = bitcast i8* %0 to %union.unalign*
  %uArch = bitcast %union.unalign* %1 to i32*
  %2 = load i32, i32* %uArch, align 1, !tbaa !9
  ret i32 %2
}

; Function Attrs: nounwind
define internal i32 @LZ4_NbCommonBytes(i32 %val) #0 {
entry:
  %retval = alloca i32, align 4
  %val.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !14
  %call = call i32 @LZ4_isLittleEndian()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %0 = load i32, i32* %val.addr, align 4, !tbaa !14
  %1 = call i32 @llvm.cttz.i32(i32 %0, i1 false)
  %shr = lshr i32 %1, 3
  store i32 %shr, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %2 = load i32, i32* %val.addr, align 4, !tbaa !14
  %3 = call i32 @llvm.ctlz.i32(i32 %2, i1 false)
  %shr1 = lshr i32 %3, 3
  store i32 %shr1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.else, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: nounwind
define internal i32 @LZ4_isLittleEndian() #0 {
entry:
  %one = alloca %union.anon, align 4
  %0 = bitcast %union.anon* %one to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast %union.anon* %one to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 bitcast (%union.anon* @__const.LZ4_isLittleEndian.one to i8*), i32 4, i1 false)
  %c = bitcast %union.anon* %one to [4 x i8]*
  %arrayidx = getelementptr inbounds [4 x i8], [4 x i8]* %c, i32 0, i32 0
  %2 = load i8, i8* %arrayidx, align 4, !tbaa !9
  %conv = zext i8 %2 to i32
  %3 = bitcast %union.anon* %one to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #7
  ret i32 %conv
}

; Function Attrs: nounwind readnone speculatable willreturn
declare i32 @llvm.cttz.i32(i32, i1 immarg) #6

; Function Attrs: nounwind readnone speculatable willreturn
declare i32 @llvm.ctlz.i32(i32, i1 immarg) #6

; Function Attrs: nounwind
define internal void @LZ4_wildCopy8(i8* %dstPtr, i8* %srcPtr, i8* %dstEnd) #0 {
entry:
  %dstPtr.addr = alloca i8*, align 4
  %srcPtr.addr = alloca i8*, align 4
  %dstEnd.addr = alloca i8*, align 4
  %d = alloca i8*, align 4
  %s = alloca i8*, align 4
  %e = alloca i8*, align 4
  store i8* %dstPtr, i8** %dstPtr.addr, align 4, !tbaa !3
  store i8* %srcPtr, i8** %srcPtr.addr, align 4, !tbaa !3
  store i8* %dstEnd, i8** %dstEnd.addr, align 4, !tbaa !3
  %0 = bitcast i8** %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8*, i8** %dstPtr.addr, align 4, !tbaa !3
  store i8* %1, i8** %d, align 4, !tbaa !3
  %2 = bitcast i8** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load i8*, i8** %srcPtr.addr, align 4, !tbaa !3
  store i8* %3, i8** %s, align 4, !tbaa !3
  %4 = bitcast i8** %e to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load i8*, i8** %dstEnd.addr, align 4, !tbaa !3
  store i8* %5, i8** %e, align 4, !tbaa !3
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  %6 = load i8*, i8** %d, align 4, !tbaa !3
  %7 = load i8*, i8** %s, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %6, i8* align 1 %7, i32 8, i1 false)
  %8 = load i8*, i8** %d, align 4, !tbaa !3
  %add.ptr = getelementptr inbounds i8, i8* %8, i32 8
  store i8* %add.ptr, i8** %d, align 4, !tbaa !3
  %9 = load i8*, i8** %s, align 4, !tbaa !3
  %add.ptr1 = getelementptr inbounds i8, i8* %9, i32 8
  store i8* %add.ptr1, i8** %s, align 4, !tbaa !3
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %10 = load i8*, i8** %d, align 4, !tbaa !3
  %11 = load i8*, i8** %e, align 4, !tbaa !3
  %cmp = icmp ult i8* %10, %11
  br i1 %cmp, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %12 = bitcast i8** %e to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  %13 = bitcast i8** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  %14 = bitcast i8** %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #7
  ret void
}

; Function Attrs: nounwind
define internal void @LZ4_writeLE16(i8* %memPtr, i16 zeroext %value) #0 {
entry:
  %memPtr.addr = alloca i8*, align 4
  %value.addr = alloca i16, align 2
  %p = alloca i8*, align 4
  store i8* %memPtr, i8** %memPtr.addr, align 4, !tbaa !3
  store i16 %value, i16* %value.addr, align 2, !tbaa !22
  %call = call i32 @LZ4_isLittleEndian()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %0 = load i8*, i8** %memPtr.addr, align 4, !tbaa !3
  %1 = load i16, i16* %value.addr, align 2, !tbaa !22
  call void @LZ4_write16(i8* %0, i16 zeroext %1)
  br label %if.end

if.else:                                          ; preds = %entry
  %2 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load i8*, i8** %memPtr.addr, align 4, !tbaa !3
  store i8* %3, i8** %p, align 4, !tbaa !3
  %4 = load i16, i16* %value.addr, align 2, !tbaa !22
  %conv = trunc i16 %4 to i8
  %5 = load i8*, i8** %p, align 4, !tbaa !3
  %arrayidx = getelementptr inbounds i8, i8* %5, i32 0
  store i8 %conv, i8* %arrayidx, align 1, !tbaa !9
  %6 = load i16, i16* %value.addr, align 2, !tbaa !22
  %conv1 = zext i16 %6 to i32
  %shr = ashr i32 %conv1, 8
  %conv2 = trunc i32 %shr to i8
  %7 = load i8*, i8** %p, align 4, !tbaa !3
  %arrayidx3 = getelementptr inbounds i8, i8* %7, i32 1
  store i8 %conv2, i8* %arrayidx3, align 1, !tbaa !9
  %8 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #7
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @LZ4_write16(i8* %memPtr, i16 zeroext %value) #0 {
entry:
  %memPtr.addr = alloca i8*, align 4
  %value.addr = alloca i16, align 2
  store i8* %memPtr, i8** %memPtr.addr, align 4, !tbaa !3
  store i16 %value, i16* %value.addr, align 2, !tbaa !22
  %0 = load i16, i16* %value.addr, align 2, !tbaa !22
  %1 = load i8*, i8** %memPtr.addr, align 4, !tbaa !3
  %2 = bitcast i8* %1 to %union.unalign*
  %u16 = bitcast %union.unalign* %2 to i16*
  store i16 %0, i16* %u16, align 1, !tbaa !9
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @LZ4HC_FindLongerMatch(%struct.LZ4HC_match_t* noalias sret align 4 %agg.result, %struct.LZ4HC_CCtx_internal* %ctx, i8* %ip, i8* %iHighLimit, i32 %minLen, i32 %nbSearches, i32 %dict, i32 %favorDecSpeed) #3 {
entry:
  %ctx.addr = alloca %struct.LZ4HC_CCtx_internal*, align 4
  %ip.addr = alloca i8*, align 4
  %iHighLimit.addr = alloca i8*, align 4
  %minLen.addr = alloca i32, align 4
  %nbSearches.addr = alloca i32, align 4
  %dict.addr = alloca i32, align 4
  %favorDecSpeed.addr = alloca i32, align 4
  %matchPtr = alloca i8*, align 4
  %matchLength = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.LZ4HC_CCtx_internal* %ctx, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  store i8* %ip, i8** %ip.addr, align 4, !tbaa !3
  store i8* %iHighLimit, i8** %iHighLimit.addr, align 4, !tbaa !3
  store i32 %minLen, i32* %minLen.addr, align 4, !tbaa !7
  store i32 %nbSearches, i32* %nbSearches.addr, align 4, !tbaa !7
  store i32 %dict, i32* %dict.addr, align 4, !tbaa !9
  store i32 %favorDecSpeed, i32* %favorDecSpeed.addr, align 4, !tbaa !9
  %0 = bitcast %struct.LZ4HC_match_t* %agg.result to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %0, i8 0, i32 8, i1 false)
  %1 = bitcast i8** %matchPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  store i8* null, i8** %matchPtr, align 4, !tbaa !3
  %2 = bitcast i32* %matchLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctx.addr, align 4, !tbaa !3
  %4 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %5 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %6 = load i8*, i8** %iHighLimit.addr, align 4, !tbaa !3
  %7 = load i32, i32* %minLen.addr, align 4, !tbaa !7
  %8 = load i32, i32* %nbSearches.addr, align 4, !tbaa !7
  %9 = load i32, i32* %dict.addr, align 4, !tbaa !9
  %10 = load i32, i32* %favorDecSpeed.addr, align 4, !tbaa !9
  %call = call i32 @LZ4HC_InsertAndGetWiderMatch(%struct.LZ4HC_CCtx_internal* %3, i8* %4, i8* %5, i8* %6, i32 %7, i8** %matchPtr, i8** %ip.addr, i32 %8, i32 1, i32 1, i32 %9, i32 %10)
  store i32 %call, i32* %matchLength, align 4, !tbaa !7
  %11 = load i32, i32* %matchLength, align 4, !tbaa !7
  %12 = load i32, i32* %minLen.addr, align 4, !tbaa !7
  %cmp = icmp sle i32 %11, %12
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %13 = load i32, i32* %favorDecSpeed.addr, align 4, !tbaa !9
  %tobool = icmp ne i32 %13, 0
  br i1 %tobool, label %if.then1, label %if.end8

if.then1:                                         ; preds = %if.end
  %14 = load i32, i32* %matchLength, align 4, !tbaa !7
  %cmp2 = icmp sgt i32 %14, 18
  %conv = zext i1 %cmp2 to i32
  %15 = load i32, i32* %matchLength, align 4, !tbaa !7
  %cmp3 = icmp sle i32 %15, 36
  %conv4 = zext i1 %cmp3 to i32
  %and = and i32 %conv, %conv4
  %tobool5 = icmp ne i32 %and, 0
  br i1 %tobool5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.then1
  store i32 18, i32* %matchLength, align 4, !tbaa !7
  br label %if.end7

if.end7:                                          ; preds = %if.then6, %if.then1
  br label %if.end8

if.end8:                                          ; preds = %if.end7, %if.end
  %16 = load i32, i32* %matchLength, align 4, !tbaa !7
  %len = getelementptr inbounds %struct.LZ4HC_match_t, %struct.LZ4HC_match_t* %agg.result, i32 0, i32 1
  store i32 %16, i32* %len, align 4, !tbaa !30
  %17 = load i8*, i8** %ip.addr, align 4, !tbaa !3
  %18 = load i8*, i8** %matchPtr, align 4, !tbaa !3
  %sub.ptr.lhs.cast = ptrtoint i8* %17 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %18 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %off = getelementptr inbounds %struct.LZ4HC_match_t, %struct.LZ4HC_match_t* %agg.result, i32 0, i32 0
  store i32 %sub.ptr.sub, i32* %off, align 4, !tbaa !32
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end8, %if.then
  %19 = bitcast i32* %matchLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #7
  %20 = bitcast i8** %matchPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #7
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal i32 @LZ4HC_literalsPrice(i32 %litlen) #3 {
entry:
  %litlen.addr = alloca i32, align 4
  %price = alloca i32, align 4
  store i32 %litlen, i32* %litlen.addr, align 4, !tbaa !7
  %0 = bitcast i32* %price to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %litlen.addr, align 4, !tbaa !7
  store i32 %1, i32* %price, align 4, !tbaa !7
  %2 = load i32, i32* %litlen.addr, align 4, !tbaa !7
  %cmp = icmp sge i32 %2, 15
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i32, i32* %litlen.addr, align 4, !tbaa !7
  %sub = sub nsw i32 %3, 15
  %div = sdiv i32 %sub, 255
  %add = add nsw i32 1, %div
  %4 = load i32, i32* %price, align 4, !tbaa !7
  %add1 = add nsw i32 %4, %add
  store i32 %add1, i32* %price, align 4, !tbaa !7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load i32, i32* %price, align 4, !tbaa !7
  %6 = bitcast i32* %price to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #7
  ret i32 %5
}

; Function Attrs: alwaysinline nounwind
define internal i32 @LZ4HC_sequencePrice(i32 %litlen, i32 %mlen) #3 {
entry:
  %litlen.addr = alloca i32, align 4
  %mlen.addr = alloca i32, align 4
  %price = alloca i32, align 4
  store i32 %litlen, i32* %litlen.addr, align 4, !tbaa !7
  store i32 %mlen, i32* %mlen.addr, align 4, !tbaa !7
  %0 = bitcast i32* %price to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 3, i32* %price, align 4, !tbaa !7
  %1 = load i32, i32* %litlen.addr, align 4, !tbaa !7
  %call = call i32 @LZ4HC_literalsPrice(i32 %1)
  %2 = load i32, i32* %price, align 4, !tbaa !7
  %add = add nsw i32 %2, %call
  store i32 %add, i32* %price, align 4, !tbaa !7
  %3 = load i32, i32* %mlen.addr, align 4, !tbaa !7
  %cmp = icmp sge i32 %3, 19
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load i32, i32* %mlen.addr, align 4, !tbaa !7
  %sub = sub nsw i32 %4, 19
  %div = sdiv i32 %sub, 255
  %add1 = add nsw i32 1, %div
  %5 = load i32, i32* %price, align 4, !tbaa !7
  %add2 = add nsw i32 %5, %add1
  store i32 %add2, i32* %price, align 4, !tbaa !7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = load i32, i32* %price, align 4, !tbaa !7
  %7 = bitcast i32* %price to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret i32 %6
}

; Function Attrs: nounwind
define internal void @LZ4HC_setExternalDict(%struct.LZ4HC_CCtx_internal* %ctxPtr, i8* %newBlock) #0 {
entry:
  %ctxPtr.addr = alloca %struct.LZ4HC_CCtx_internal*, align 4
  %newBlock.addr = alloca i8*, align 4
  store %struct.LZ4HC_CCtx_internal* %ctxPtr, %struct.LZ4HC_CCtx_internal** %ctxPtr.addr, align 4, !tbaa !3
  store i8* %newBlock, i8** %newBlock.addr, align 4, !tbaa !3
  %0 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr.addr, align 4, !tbaa !3
  %end = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %0, i32 0, i32 2
  %1 = load i8*, i8** %end, align 4, !tbaa !10
  %2 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr.addr, align 4, !tbaa !3
  %base = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %2, i32 0, i32 3
  %3 = load i8*, i8** %base, align 4, !tbaa !13
  %4 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr.addr, align 4, !tbaa !3
  %dictLimit = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %4, i32 0, i32 5
  %5 = load i32, i32* %dictLimit, align 4, !tbaa !18
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %5
  %add.ptr1 = getelementptr inbounds i8, i8* %add.ptr, i32 4
  %cmp = icmp uge i8* %1, %add.ptr1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr.addr, align 4, !tbaa !3
  %7 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr.addr, align 4, !tbaa !3
  %end2 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %7, i32 0, i32 2
  %8 = load i8*, i8** %end2, align 4, !tbaa !10
  %add.ptr3 = getelementptr inbounds i8, i8* %8, i32 -3
  call void @LZ4HC_Insert(%struct.LZ4HC_CCtx_internal* %6, i8* %add.ptr3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr.addr, align 4, !tbaa !3
  %dictLimit4 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %9, i32 0, i32 5
  %10 = load i32, i32* %dictLimit4, align 4, !tbaa !18
  %11 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr.addr, align 4, !tbaa !3
  %lowLimit = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %11, i32 0, i32 6
  store i32 %10, i32* %lowLimit, align 4, !tbaa !19
  %12 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr.addr, align 4, !tbaa !3
  %end5 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %12, i32 0, i32 2
  %13 = load i8*, i8** %end5, align 4, !tbaa !10
  %14 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr.addr, align 4, !tbaa !3
  %base6 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %14, i32 0, i32 3
  %15 = load i8*, i8** %base6, align 4, !tbaa !13
  %sub.ptr.lhs.cast = ptrtoint i8* %13 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %15 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %16 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr.addr, align 4, !tbaa !3
  %dictLimit7 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %16, i32 0, i32 5
  store i32 %sub.ptr.sub, i32* %dictLimit7, align 4, !tbaa !18
  %17 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr.addr, align 4, !tbaa !3
  %base8 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %17, i32 0, i32 3
  %18 = load i8*, i8** %base8, align 4, !tbaa !13
  %19 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr.addr, align 4, !tbaa !3
  %dictBase = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %19, i32 0, i32 4
  store i8* %18, i8** %dictBase, align 4, !tbaa !17
  %20 = load i8*, i8** %newBlock.addr, align 4, !tbaa !3
  %21 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr.addr, align 4, !tbaa !3
  %dictLimit9 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %21, i32 0, i32 5
  %22 = load i32, i32* %dictLimit9, align 4, !tbaa !18
  %idx.neg = sub i32 0, %22
  %add.ptr10 = getelementptr inbounds i8, i8* %20, i32 %idx.neg
  %23 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr.addr, align 4, !tbaa !3
  %base11 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %23, i32 0, i32 3
  store i8* %add.ptr10, i8** %base11, align 4, !tbaa !13
  %24 = load i8*, i8** %newBlock.addr, align 4, !tbaa !3
  %25 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr.addr, align 4, !tbaa !3
  %end12 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %25, i32 0, i32 2
  store i8* %24, i8** %end12, align 4, !tbaa !10
  %26 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr.addr, align 4, !tbaa !3
  %dictLimit13 = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %26, i32 0, i32 5
  %27 = load i32, i32* %dictLimit13, align 4, !tbaa !18
  %28 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr.addr, align 4, !tbaa !3
  %nextToUpdate = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %28, i32 0, i32 7
  store i32 %27, i32* %nextToUpdate, align 4, !tbaa !16
  %29 = load %struct.LZ4HC_CCtx_internal*, %struct.LZ4HC_CCtx_internal** %ctxPtr.addr, align 4, !tbaa !3
  %dictCtx = getelementptr inbounds %struct.LZ4HC_CCtx_internal, %struct.LZ4HC_CCtx_internal* %29, i32 0, i32 11
  store %struct.LZ4HC_CCtx_internal* null, %struct.LZ4HC_CCtx_internal** %dictCtx, align 4, !tbaa !20
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { alwaysinline nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { nounwind readnone willreturn }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!3 = !{!4, !4, i64 0}
!4 = !{!"any pointer", !5, i64 0}
!5 = !{!"omnipotent char", !6, i64 0}
!6 = !{!"Simple C/C++ TBAA"}
!7 = !{!8, !8, i64 0}
!8 = !{!"int", !5, i64 0}
!9 = !{!5, !5, i64 0}
!10 = !{!11, !4, i64 262144}
!11 = !{!"LZ4HC_CCtx_internal", !5, i64 0, !5, i64 131072, !4, i64 262144, !4, i64 262148, !4, i64 262152, !8, i64 262156, !8, i64 262160, !8, i64 262164, !12, i64 262168, !5, i64 262170, !5, i64 262171, !4, i64 262172}
!12 = !{!"short", !5, i64 0}
!13 = !{!11, !4, i64 262148}
!14 = !{!15, !15, i64 0}
!15 = !{!"long", !5, i64 0}
!16 = !{!11, !8, i64 262164}
!17 = !{!11, !4, i64 262152}
!18 = !{!11, !8, i64 262156}
!19 = !{!11, !8, i64 262160}
!20 = !{!11, !4, i64 262172}
!21 = !{!11, !12, i64 262168}
!22 = !{!12, !12, i64 0}
!23 = !{i64 0, i64 4, !9, i64 4, i64 4, !7, i64 8, i64 4, !7}
!24 = !{!11, !5, i64 262170}
!25 = !{!26, !5, i64 0}
!26 = !{!"", !5, i64 0, !8, i64 4, !8, i64 8}
!27 = !{!26, !8, i64 4}
!28 = !{!26, !8, i64 8}
!29 = !{!11, !5, i64 262171}
!30 = !{!31, !8, i64 4}
!31 = !{!"", !8, i64 0, !8, i64 4}
!32 = !{!31, !8, i64 0}
!33 = !{!34, !8, i64 8}
!34 = !{!"", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12}
!35 = !{!34, !8, i64 4}
!36 = !{!34, !8, i64 12}
!37 = !{!34, !8, i64 0}
!38 = !{i64 0, i64 4, !7, i64 4, i64 4, !7}
