; ModuleID = 'bench.c'
source_filename = "bench.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct._IO_FILE = type opaque
%struct.stat = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i64, i32, i32, %struct.timespec, %struct.timespec, %struct.timespec, i64 }
%struct.timespec = type { i32, i32 }
%struct.blockParam_t = type { i8*, i32, i8*, i32, i32, i8*, i32 }
%struct.compressionParameters = type { i32, i8*, i32, %union.LZ4_stream_u*, %union.LZ4_stream_u*, %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u*, void (%struct.compressionParameters*)*, void (%struct.compressionParameters*)*, i32 (%struct.compressionParameters*, i8*, i8*, i32, i32)*, void (%struct.compressionParameters*)* }
%union.LZ4_stream_u = type { [2052 x i64] }
%union.LZ4_streamHC_u = type { [65550 x i32] }

@g_additionalParam = hidden global i32 0, align 4
@g_benchSeparately = hidden global i32 0, align 4
@g_displayLevel = internal global i32 2, align 4
@g_nbSeconds = internal global i32 3, align 4
@stderr = external constant %struct._IO_FILE*, align 4
@.str = private unnamed_addr constant [56 x i8] c"- test >= %u seconds per compression / decompression -\0A\00", align 1
@g_blockSize = internal global i32 0, align 4
@.str.1 = private unnamed_addr constant [35 x i8] c"Benchmarking levels from %d to %d\0A\00", align 1
@g_compressibilityDefault = internal global i32 50, align 4
@.str.2 = private unnamed_addr constant [12 x i8] c"Error %i : \00", align 1
@.str.3 = private unnamed_addr constant [50 x i8] c"Dictionary error : could not stat dictionary file\00", align 1
@.str.4 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.5 = private unnamed_addr constant [3 x i8] c"rb\00", align 1
@.str.6 = private unnamed_addr constant [50 x i8] c"Dictionary error : could not open dictionary file\00", align 1
@.str.7 = private unnamed_addr constant [50 x i8] c"Dictionary error : could not seek dictionary file\00", align 1
@.str.8 = private unnamed_addr constant [37 x i8] c"Allocation error : not enough memory\00", align 1
@.str.9 = private unnamed_addr constant [50 x i8] c"Dictionary error : could not read dictionary file\00", align 1
@.str.10 = private unnamed_addr constant [32 x i8] c"not enough memory for fileSizes\00", align 1
@.str.11 = private unnamed_addr constant [18 x i8] c"not enough memory\00", align 1
@.str.12 = private unnamed_addr constant [65 x i8] c"File(s) bigger than LZ4's max input size; testing %u MB only...\0A\00", align 1
@.str.13 = private unnamed_addr constant [42 x i8] c"Not enough memory; testing %u MB only...\0A\00", align 1
@.str.14 = private unnamed_addr constant [10 x i8] c" %u files\00", align 1
@.str.15 = private unnamed_addr constant [33 x i8] c"Ignoring %s directory...       \0A\00", align 1
@.str.16 = private unnamed_addr constant [27 x i8] c"impossible to open file %s\00", align 1
@g_time = internal global i32 0, align 4
@.str.17 = private unnamed_addr constant [22 x i8] c"Loading %s...       \0D\00", align 1
@stdout = external constant %struct._IO_FILE*, align 4
@.str.18 = private unnamed_addr constant [18 x i8] c"could not read %s\00", align 1
@.str.19 = private unnamed_addr constant [17 x i8] c"no data to bench\00", align 1
@.str.20 = private unnamed_addr constant [55 x i8] c"bench %s %s: input %u bytes, %u seconds, %u KB blocks\0A\00", align 1
@.str.21 = private unnamed_addr constant [6 x i8] c"1.9.2\00", align 1
@.str.22 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@.str.23 = private unnamed_addr constant [37 x i8] c"allocation error : not enough memory\00", align 1
@.str.24 = private unnamed_addr constant [3 x i8] c" |\00", align 1
@.str.25 = private unnamed_addr constant [3 x i8] c" /\00", align 1
@.str.26 = private unnamed_addr constant [3 x i8] c" =\00", align 1
@.str.27 = private unnamed_addr constant [2 x i8] c"\\\00", align 1
@__const.BMK_benchMem.marks = private unnamed_addr constant [4 x i8*] [i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.24, i32 0, i32 0), i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.25, i32 0, i32 0), i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.26, i32 0, i32 0), i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.27, i32 0, i32 0)], align 16
@.str.28 = private unnamed_addr constant [7 x i8] c"\0D%79s\0D\00", align 1
@.str.29 = private unnamed_addr constant [23 x i8] c"\0Dcooling down ...    \0D\00", align 1
@.str.30 = private unnamed_addr constant [23 x i8] c"%2s-%-17.17s :%10u ->\0D\00", align 1
@.str.31 = private unnamed_addr constant [23 x i8] c"LZ4 compression failed\00", align 1
@.str.32 = private unnamed_addr constant [13 x i8] c"fastestC > 0\00", align 1
@.str.33 = private unnamed_addr constant [8 x i8] c"bench.c\00", align 1
@__func__.BMK_benchMem = private unnamed_addr constant [13 x i8] c"BMK_benchMem\00", align 1
@.str.34 = private unnamed_addr constant [30 x i8] c"nbCompressionLoops < 40000000\00", align 1
@.str.35 = private unnamed_addr constant [46 x i8] c"%2s-%-17.17s :%10u ->%10u (%5.3f),%6.1f MB/s\0D\00", align 1
@.str.36 = private unnamed_addr constant [53 x i8] c"LZ4_decompress_safe_usingDict() failed on block %u \0A\00", align 1
@.str.37 = private unnamed_addr constant [13 x i8] c"fastestD > 0\00", align 1
@.str.38 = private unnamed_addr constant [25 x i8] c"nbDecodeLoops < 40000000\00", align 1
@.str.39 = private unnamed_addr constant [58 x i8] c"%2s-%-17.17s :%10u ->%10u (%5.3f),%6.1f MB/s ,%6.1f MB/s\0D\00", align 1
@.str.40 = private unnamed_addr constant [56 x i8] c"\0A!!! WARNING !!! %17s : Invalid Checksum : %x != %x   \0A\00", align 1
@.str.41 = private unnamed_addr constant [26 x i8] c"Decoding error at pos %u \00", align 1
@.str.42 = private unnamed_addr constant [29 x i8] c"(block %u, sub %u, pos %u) \0A\00", align 1
@.str.43 = private unnamed_addr constant [24 x i8] c"no difference detected\0A\00", align 1
@.str.44 = private unnamed_addr constant [56 x i8] c"-%-3i%11i (%5.3f) %6.2f MB/s %6.1f MB/s  %s (param=%d)\0A\00", align 1
@.str.45 = private unnamed_addr constant [45 x i8] c"-%-3i%11i (%5.3f) %6.2f MB/s %6.1f MB/s  %s\0A\00", align 1
@.str.46 = private unnamed_addr constant [6 x i8] c"%2i#\0A\00", align 1
@.str.47 = private unnamed_addr constant [16 x i8] c"Synthetic %2u%%\00", align 1

; Function Attrs: nounwind
define hidden void @BMK_setNotificationLevel(i32 %level) #0 {
entry:
  %level.addr = alloca i32, align 4
  store i32 %level, i32* %level.addr, align 4, !tbaa !2
  %0 = load i32, i32* %level.addr, align 4, !tbaa !2
  store i32 %0, i32* @g_displayLevel, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden void @BMK_setAdditionalParam(i32 %additionalParam) #0 {
entry:
  %additionalParam.addr = alloca i32, align 4
  store i32 %additionalParam, i32* %additionalParam.addr, align 4, !tbaa !2
  %0 = load i32, i32* %additionalParam.addr, align 4, !tbaa !2
  store i32 %0, i32* @g_additionalParam, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden void @BMK_setNbSeconds(i32 %nbSeconds) #0 {
entry:
  %nbSeconds.addr = alloca i32, align 4
  store i32 %nbSeconds, i32* %nbSeconds.addr, align 4, !tbaa !2
  %0 = load i32, i32* %nbSeconds.addr, align 4, !tbaa !2
  store i32 %0, i32* @g_nbSeconds, align 4, !tbaa !2
  %1 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp = icmp uge i32 %1, 3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %3 = load i32, i32* @g_nbSeconds, align 4, !tbaa !2
  %call = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %2, i8* getelementptr inbounds ([56 x i8], [56 x i8]* @.str, i32 0, i32 0), i32 %3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #1

; Function Attrs: nounwind
define hidden void @BMK_setBlockSize(i32 %blockSize) #0 {
entry:
  %blockSize.addr = alloca i32, align 4
  store i32 %blockSize, i32* %blockSize.addr, align 4, !tbaa !8
  %0 = load i32, i32* %blockSize.addr, align 4, !tbaa !8
  store i32 %0, i32* @g_blockSize, align 4, !tbaa !8
  ret void
}

; Function Attrs: nounwind
define hidden void @BMK_setBenchSeparately(i32 %separate) #0 {
entry:
  %separate.addr = alloca i32, align 4
  store i32 %separate, i32* %separate.addr, align 4, !tbaa !2
  %0 = load i32, i32* %separate.addr, align 4, !tbaa !2
  %cmp = icmp ne i32 %0, 0
  %conv = zext i1 %cmp to i32
  store i32 %conv, i32* @g_benchSeparately, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden i32 @BMK_benchFilesSeparately(i8** %fileNamesTable, i32 %nbFiles, i32 %cLevel, i32 %cLevelLast, i8* %dictBuf, i32 %dictSize) #0 {
entry:
  %fileNamesTable.addr = alloca i8**, align 4
  %nbFiles.addr = alloca i32, align 4
  %cLevel.addr = alloca i32, align 4
  %cLevelLast.addr = alloca i32, align 4
  %dictBuf.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  %fileNb = alloca i32, align 4
  store i8** %fileNamesTable, i8*** %fileNamesTable.addr, align 4, !tbaa !6
  store i32 %nbFiles, i32* %nbFiles.addr, align 4, !tbaa !2
  store i32 %cLevel, i32* %cLevel.addr, align 4, !tbaa !2
  store i32 %cLevelLast, i32* %cLevelLast.addr, align 4, !tbaa !2
  store i8* %dictBuf, i8** %dictBuf.addr, align 4, !tbaa !6
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !2
  %0 = bitcast i32* %fileNb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %cmp = icmp sgt i32 %1, 12
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 12, i32* %cLevel.addr, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load i32, i32* %cLevelLast.addr, align 4, !tbaa !2
  %cmp1 = icmp sgt i32 %2, 12
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 12, i32* %cLevelLast.addr, align 4, !tbaa !2
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %3 = load i32, i32* %cLevelLast.addr, align 4, !tbaa !2
  %4 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %cmp4 = icmp slt i32 %3, %4
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  %5 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  store i32 %5, i32* %cLevelLast.addr, align 4, !tbaa !2
  br label %if.end6

if.end6:                                          ; preds = %if.then5, %if.end3
  %6 = load i32, i32* %cLevelLast.addr, align 4, !tbaa !2
  %7 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %cmp7 = icmp sgt i32 %6, %7
  br i1 %cmp7, label %if.then8, label %if.end12

if.then8:                                         ; preds = %if.end6
  %8 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp9 = icmp uge i32 %8, 2
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.then8
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %10 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %11 = load i32, i32* %cLevelLast.addr, align 4, !tbaa !2
  %call = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %9, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.1, i32 0, i32 0), i32 %10, i32 %11)
  br label %if.end11

if.end11:                                         ; preds = %if.then10, %if.then8
  br label %if.end12

if.end12:                                         ; preds = %if.end11, %if.end6
  store i32 0, i32* %fileNb, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end12
  %12 = load i32, i32* %fileNb, align 4, !tbaa !2
  %13 = load i32, i32* %nbFiles.addr, align 4, !tbaa !2
  %cmp13 = icmp ult i32 %12, %13
  br i1 %cmp13, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load i8**, i8*** %fileNamesTable.addr, align 4, !tbaa !6
  %15 = load i32, i32* %fileNb, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8*, i8** %14, i32 %15
  %16 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %17 = load i32, i32* %cLevelLast.addr, align 4, !tbaa !2
  %18 = load i8*, i8** %dictBuf.addr, align 4, !tbaa !6
  %19 = load i32, i32* %dictSize.addr, align 4, !tbaa !2
  call void @BMK_benchFileTable(i8** %add.ptr, i32 1, i32 %16, i32 %17, i8* %18, i32 %19)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %20 = load i32, i32* %fileNb, align 4, !tbaa !2
  %inc = add i32 %20, 1
  store i32 %inc, i32* %fileNb, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %21 = bitcast i32* %fileNb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #5
  ret i32 0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define internal void @BMK_benchFileTable(i8** %fileNamesTable, i32 %nbFiles, i32 %cLevel, i32 %cLevelLast, i8* %dictBuf, i32 %dictSize) #0 {
entry:
  %fileNamesTable.addr = alloca i8**, align 4
  %nbFiles.addr = alloca i32, align 4
  %cLevel.addr = alloca i32, align 4
  %cLevelLast.addr = alloca i32, align 4
  %dictBuf.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  %srcBuffer = alloca i8*, align 4
  %benchedSize = alloca i32, align 4
  %fileSizes = alloca i32*, align 4
  %totalSizeToLoad = alloca i64, align 8
  %mfName = alloca [20 x i8], align 16
  %displayName = alloca i8*, align 4
  store i8** %fileNamesTable, i8*** %fileNamesTable.addr, align 4, !tbaa !6
  store i32 %nbFiles, i32* %nbFiles.addr, align 4, !tbaa !2
  store i32 %cLevel, i32* %cLevel.addr, align 4, !tbaa !2
  store i32 %cLevelLast, i32* %cLevelLast.addr, align 4, !tbaa !2
  store i8* %dictBuf, i8** %dictBuf.addr, align 4, !tbaa !6
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !2
  %0 = bitcast i8** %srcBuffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %benchedSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32** %fileSizes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i32, i32* %nbFiles.addr, align 4, !tbaa !2
  %mul = mul i32 %3, 4
  %call = call i8* @malloc(i32 %mul)
  %4 = bitcast i8* %call to i32*
  store i32* %4, i32** %fileSizes, align 4, !tbaa !6
  %5 = bitcast i64* %totalSizeToLoad to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %5) #5
  %6 = load i8**, i8*** %fileNamesTable.addr, align 4, !tbaa !6
  %7 = load i32, i32* %nbFiles.addr, align 4, !tbaa !2
  %call1 = call i64 @UTIL_getTotalFileSize(i8** %6, i32 %7)
  store i64 %call1, i64* %totalSizeToLoad, align 8, !tbaa !10
  %8 = bitcast [20 x i8]* %mfName to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %8) #5
  %9 = bitcast [20 x i8]* %mfName to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %9, i8 0, i32 20, i1 false)
  %10 = load i32*, i32** %fileSizes, align 4, !tbaa !6
  %tobool = icmp ne i32* %10, null
  br i1 %tobool, label %if.end12, label %if.then

if.then:                                          ; preds = %entry
  %11 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp = icmp uge i32 %11, 1
  br i1 %cmp, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %12 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call3 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %12, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0), i32 12)
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  %13 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp4 = icmp uge i32 %13, 1
  br i1 %cmp4, label %if.then5, label %if.end7

if.then5:                                         ; preds = %if.end
  %14 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call6 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %14, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.10, i32 0, i32 0))
  br label %if.end7

if.end7:                                          ; preds = %if.then5, %if.end
  %15 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp8 = icmp uge i32 %15, 1
  br i1 %cmp8, label %if.then9, label %if.end11

if.then9:                                         ; preds = %if.end7
  %16 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call10 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %16, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end11

if.end11:                                         ; preds = %if.then9, %if.end7
  call void @exit(i32 12) #6
  unreachable

if.end12:                                         ; preds = %entry
  %17 = load i64, i64* %totalSizeToLoad, align 8, !tbaa !10
  %mul13 = mul i64 %17, 3
  %call14 = call i32 @BMK_findMaxMem(i64 %mul13)
  %div = udiv i32 %call14, 3
  store i32 %div, i32* %benchedSize, align 4, !tbaa !8
  %18 = load i32, i32* %benchedSize, align 4, !tbaa !8
  %cmp15 = icmp eq i32 %18, 0
  br i1 %cmp15, label %if.then16, label %if.end29

if.then16:                                        ; preds = %if.end12
  %19 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp17 = icmp uge i32 %19, 1
  br i1 %cmp17, label %if.then18, label %if.end20

if.then18:                                        ; preds = %if.then16
  %20 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call19 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %20, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0), i32 12)
  br label %if.end20

if.end20:                                         ; preds = %if.then18, %if.then16
  %21 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp21 = icmp uge i32 %21, 1
  br i1 %cmp21, label %if.then22, label %if.end24

if.then22:                                        ; preds = %if.end20
  %22 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call23 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %22, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.11, i32 0, i32 0))
  br label %if.end24

if.end24:                                         ; preds = %if.then22, %if.end20
  %23 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp25 = icmp uge i32 %23, 1
  br i1 %cmp25, label %if.then26, label %if.end28

if.then26:                                        ; preds = %if.end24
  %24 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call27 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %24, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end28

if.end28:                                         ; preds = %if.then26, %if.end24
  call void @exit(i32 12) #6
  unreachable

if.end29:                                         ; preds = %if.end12
  %25 = load i32, i32* %benchedSize, align 4, !tbaa !8
  %conv = zext i32 %25 to i64
  %26 = load i64, i64* %totalSizeToLoad, align 8, !tbaa !10
  %cmp30 = icmp ugt i64 %conv, %26
  br i1 %cmp30, label %if.then32, label %if.end34

if.then32:                                        ; preds = %if.end29
  %27 = load i64, i64* %totalSizeToLoad, align 8, !tbaa !10
  %conv33 = trunc i64 %27 to i32
  store i32 %conv33, i32* %benchedSize, align 4, !tbaa !8
  br label %if.end34

if.end34:                                         ; preds = %if.then32, %if.end29
  %28 = load i32, i32* %benchedSize, align 4, !tbaa !8
  %cmp35 = icmp ugt i32 %28, 2113929216
  br i1 %cmp35, label %if.then37, label %if.else

if.then37:                                        ; preds = %if.end34
  store i32 2113929216, i32* %benchedSize, align 4, !tbaa !8
  %29 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %30 = load i32, i32* %benchedSize, align 4, !tbaa !8
  %shr = lshr i32 %30, 20
  %call38 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %29, i8* getelementptr inbounds ([65 x i8], [65 x i8]* @.str.12, i32 0, i32 0), i32 %shr)
  br label %if.end46

if.else:                                          ; preds = %if.end34
  %31 = load i32, i32* %benchedSize, align 4, !tbaa !8
  %conv39 = zext i32 %31 to i64
  %32 = load i64, i64* %totalSizeToLoad, align 8, !tbaa !10
  %cmp40 = icmp ult i64 %conv39, %32
  br i1 %cmp40, label %if.then42, label %if.end45

if.then42:                                        ; preds = %if.else
  %33 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %34 = load i32, i32* %benchedSize, align 4, !tbaa !8
  %shr43 = lshr i32 %34, 20
  %call44 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %33, i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.13, i32 0, i32 0), i32 %shr43)
  br label %if.end45

if.end45:                                         ; preds = %if.then42, %if.else
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %if.then37
  %35 = load i32, i32* %benchedSize, align 4, !tbaa !8
  %36 = load i32, i32* %benchedSize, align 4, !tbaa !8
  %tobool47 = icmp ne i32 %36, 0
  %lnot = xor i1 %tobool47, true
  %lnot.ext = zext i1 %lnot to i32
  %add = add i32 %35, %lnot.ext
  %call48 = call i8* @malloc(i32 %add)
  store i8* %call48, i8** %srcBuffer, align 4, !tbaa !6
  %37 = load i8*, i8** %srcBuffer, align 4, !tbaa !6
  %tobool49 = icmp ne i8* %37, null
  br i1 %tobool49, label %if.end66, label %if.then50

if.then50:                                        ; preds = %if.end46
  %38 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp51 = icmp uge i32 %38, 1
  br i1 %cmp51, label %if.then53, label %if.end55

if.then53:                                        ; preds = %if.then50
  %39 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call54 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %39, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0), i32 12)
  br label %if.end55

if.end55:                                         ; preds = %if.then53, %if.then50
  %40 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp56 = icmp uge i32 %40, 1
  br i1 %cmp56, label %if.then58, label %if.end60

if.then58:                                        ; preds = %if.end55
  %41 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call59 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %41, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.11, i32 0, i32 0))
  br label %if.end60

if.end60:                                         ; preds = %if.then58, %if.end55
  %42 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp61 = icmp uge i32 %42, 1
  br i1 %cmp61, label %if.then63, label %if.end65

if.then63:                                        ; preds = %if.end60
  %43 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call64 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %43, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end65

if.end65:                                         ; preds = %if.then63, %if.end60
  call void @exit(i32 12) #6
  unreachable

if.end66:                                         ; preds = %if.end46
  %44 = load i8*, i8** %srcBuffer, align 4, !tbaa !6
  %45 = load i32, i32* %benchedSize, align 4, !tbaa !8
  %46 = load i32*, i32** %fileSizes, align 4, !tbaa !6
  %47 = load i8**, i8*** %fileNamesTable.addr, align 4, !tbaa !6
  %48 = load i32, i32* %nbFiles.addr, align 4, !tbaa !2
  call void @BMK_loadFiles(i8* %44, i32 %45, i32* %46, i8** %47, i32 %48)
  %arraydecay = getelementptr inbounds [20 x i8], [20 x i8]* %mfName, i32 0, i32 0
  %49 = load i32, i32* %nbFiles.addr, align 4, !tbaa !2
  %call67 = call i32 (i8*, i32, i8*, ...) @snprintf(i8* %arraydecay, i32 20, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.14, i32 0, i32 0), i32 %49)
  %50 = bitcast i8** %displayName to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #5
  %51 = load i32, i32* %nbFiles.addr, align 4, !tbaa !2
  %cmp68 = icmp ugt i32 %51, 1
  br i1 %cmp68, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end66
  %arraydecay70 = getelementptr inbounds [20 x i8], [20 x i8]* %mfName, i32 0, i32 0
  br label %cond.end

cond.false:                                       ; preds = %if.end66
  %52 = load i8**, i8*** %fileNamesTable.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8*, i8** %52, i32 0
  %53 = load i8*, i8** %arrayidx, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %arraydecay70, %cond.true ], [ %53, %cond.false ]
  store i8* %cond, i8** %displayName, align 4, !tbaa !6
  %54 = load i8*, i8** %srcBuffer, align 4, !tbaa !6
  %55 = load i32, i32* %benchedSize, align 4, !tbaa !8
  %56 = load i8*, i8** %displayName, align 4, !tbaa !6
  %57 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %58 = load i32, i32* %cLevelLast.addr, align 4, !tbaa !2
  %59 = load i32*, i32** %fileSizes, align 4, !tbaa !6
  %60 = load i32, i32* %nbFiles.addr, align 4, !tbaa !2
  %61 = load i8*, i8** %dictBuf.addr, align 4, !tbaa !6
  %62 = load i32, i32* %dictSize.addr, align 4, !tbaa !2
  call void @BMK_benchCLevel(i8* %54, i32 %55, i8* %56, i32 %57, i32 %58, i32* %59, i32 %60, i8* %61, i32 %62)
  %63 = bitcast i8** %displayName to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #5
  %64 = load i8*, i8** %srcBuffer, align 4, !tbaa !6
  call void @free(i8* %64)
  %65 = load i32*, i32** %fileSizes, align 4, !tbaa !6
  %66 = bitcast i32* %65 to i8*
  call void @free(i8* %66)
  %67 = bitcast [20 x i8]* %mfName to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %67) #5
  %68 = bitcast i64* %totalSizeToLoad to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %68) #5
  %69 = bitcast i32** %fileSizes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #5
  %70 = bitcast i32* %benchedSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #5
  %71 = bitcast i8** %srcBuffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #5
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define hidden i32 @BMK_benchFiles(i8** %fileNamesTable, i32 %nbFiles, i32 %cLevel, i32 %cLevelLast, i8* %dictFileName) #0 {
entry:
  %fileNamesTable.addr = alloca i8**, align 4
  %nbFiles.addr = alloca i32, align 4
  %cLevel.addr = alloca i32, align 4
  %cLevelLast.addr = alloca i32, align 4
  %dictFileName.addr = alloca i8*, align 4
  %compressibility = alloca double, align 8
  %dictBuf = alloca i8*, align 4
  %dictSize = alloca i32, align 4
  %dictFile = alloca %struct._IO_FILE*, align 4
  %dictFileSize = alloca i64, align 8
  store i8** %fileNamesTable, i8*** %fileNamesTable.addr, align 4, !tbaa !6
  store i32 %nbFiles, i32* %nbFiles.addr, align 4, !tbaa !2
  store i32 %cLevel, i32* %cLevel.addr, align 4, !tbaa !2
  store i32 %cLevelLast, i32* %cLevelLast.addr, align 4, !tbaa !2
  store i8* %dictFileName, i8** %dictFileName.addr, align 4, !tbaa !6
  %0 = bitcast double* %compressibility to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #5
  %1 = load i32, i32* @g_compressibilityDefault, align 4, !tbaa !2
  %conv = uitofp i32 %1 to double
  %div = fdiv double %conv, 1.000000e+02
  store double %div, double* %compressibility, align 8, !tbaa !12
  %2 = bitcast i8** %dictBuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i8* null, i8** %dictBuf, align 4, !tbaa !6
  %3 = bitcast i32* %dictSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  store i32 0, i32* %dictSize, align 4, !tbaa !2
  %4 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %cmp = icmp sgt i32 %4, 12
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 12, i32* %cLevel.addr, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load i32, i32* %cLevelLast.addr, align 4, !tbaa !2
  %cmp2 = icmp sgt i32 %5, 12
  br i1 %cmp2, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  store i32 12, i32* %cLevelLast.addr, align 4, !tbaa !2
  br label %if.end5

if.end5:                                          ; preds = %if.then4, %if.end
  %6 = load i32, i32* %cLevelLast.addr, align 4, !tbaa !2
  %7 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %cmp6 = icmp slt i32 %6, %7
  br i1 %cmp6, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end5
  %8 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  store i32 %8, i32* %cLevelLast.addr, align 4, !tbaa !2
  br label %if.end9

if.end9:                                          ; preds = %if.then8, %if.end5
  %9 = load i32, i32* %cLevelLast.addr, align 4, !tbaa !2
  %10 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %cmp10 = icmp sgt i32 %9, %10
  br i1 %cmp10, label %if.then12, label %if.end17

if.then12:                                        ; preds = %if.end9
  %11 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp13 = icmp uge i32 %11, 2
  br i1 %cmp13, label %if.then15, label %if.end16

if.then15:                                        ; preds = %if.then12
  %12 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %13 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %14 = load i32, i32* %cLevelLast.addr, align 4, !tbaa !2
  %call = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %12, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.1, i32 0, i32 0), i32 %13, i32 %14)
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %if.then12
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %if.end9
  %15 = load i8*, i8** %dictFileName.addr, align 4, !tbaa !6
  %tobool = icmp ne i8* %15, null
  br i1 %tobool, label %if.then18, label %if.end122

if.then18:                                        ; preds = %if.end17
  %16 = bitcast %struct._IO_FILE** %dictFile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #5
  store %struct._IO_FILE* null, %struct._IO_FILE** %dictFile, align 4, !tbaa !6
  %17 = bitcast i64* %dictFileSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %17) #5
  %18 = load i8*, i8** %dictFileName.addr, align 4, !tbaa !6
  %call19 = call i64 @UTIL_getFileSize(i8* %18)
  store i64 %call19, i64* %dictFileSize, align 8, !tbaa !10
  %19 = load i64, i64* %dictFileSize, align 8, !tbaa !10
  %tobool20 = icmp ne i64 %19, 0
  br i1 %tobool20, label %if.end37, label %if.then21

if.then21:                                        ; preds = %if.then18
  %20 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp22 = icmp uge i32 %20, 1
  br i1 %cmp22, label %if.then24, label %if.end26

if.then24:                                        ; preds = %if.then21
  %21 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call25 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %21, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0), i32 25)
  br label %if.end26

if.end26:                                         ; preds = %if.then24, %if.then21
  %22 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp27 = icmp uge i32 %22, 1
  br i1 %cmp27, label %if.then29, label %if.end31

if.then29:                                        ; preds = %if.end26
  %23 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call30 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %23, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @.str.3, i32 0, i32 0))
  br label %if.end31

if.end31:                                         ; preds = %if.then29, %if.end26
  %24 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp32 = icmp uge i32 %24, 1
  br i1 %cmp32, label %if.then34, label %if.end36

if.then34:                                        ; preds = %if.end31
  %25 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call35 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %25, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end36

if.end36:                                         ; preds = %if.then34, %if.end31
  call void @exit(i32 25) #6
  unreachable

if.end37:                                         ; preds = %if.then18
  %26 = load i8*, i8** %dictFileName.addr, align 4, !tbaa !6
  %call38 = call %struct._IO_FILE* @fopen(i8* %26, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i32 0, i32 0))
  store %struct._IO_FILE* %call38, %struct._IO_FILE** %dictFile, align 4, !tbaa !6
  %27 = load %struct._IO_FILE*, %struct._IO_FILE** %dictFile, align 4, !tbaa !6
  %tobool39 = icmp ne %struct._IO_FILE* %27, null
  br i1 %tobool39, label %if.end56, label %if.then40

if.then40:                                        ; preds = %if.end37
  %28 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp41 = icmp uge i32 %28, 1
  br i1 %cmp41, label %if.then43, label %if.end45

if.then43:                                        ; preds = %if.then40
  %29 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call44 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %29, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0), i32 25)
  br label %if.end45

if.end45:                                         ; preds = %if.then43, %if.then40
  %30 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp46 = icmp uge i32 %30, 1
  br i1 %cmp46, label %if.then48, label %if.end50

if.then48:                                        ; preds = %if.end45
  %31 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call49 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %31, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @.str.6, i32 0, i32 0))
  br label %if.end50

if.end50:                                         ; preds = %if.then48, %if.end45
  %32 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp51 = icmp uge i32 %32, 1
  br i1 %cmp51, label %if.then53, label %if.end55

if.then53:                                        ; preds = %if.end50
  %33 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call54 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %33, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end55

if.end55:                                         ; preds = %if.then53, %if.end50
  call void @exit(i32 25) #6
  unreachable

if.end56:                                         ; preds = %if.end37
  %34 = load i64, i64* %dictFileSize, align 8, !tbaa !10
  %cmp57 = icmp ugt i64 %34, 65536
  br i1 %cmp57, label %if.then59, label %if.else

if.then59:                                        ; preds = %if.end56
  store i32 65536, i32* %dictSize, align 4, !tbaa !2
  %35 = load %struct._IO_FILE*, %struct._IO_FILE** %dictFile, align 4, !tbaa !6
  %36 = load i64, i64* %dictFileSize, align 8, !tbaa !10
  %37 = load i32, i32* %dictSize, align 4, !tbaa !2
  %conv60 = sext i32 %37 to i64
  %sub = sub i64 %36, %conv60
  %call61 = call i32 @fseeko(%struct._IO_FILE* %35, i64 %sub, i32 0)
  %tobool62 = icmp ne i32 %call61, 0
  br i1 %tobool62, label %if.then63, label %if.end79

if.then63:                                        ; preds = %if.then59
  %38 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp64 = icmp uge i32 %38, 1
  br i1 %cmp64, label %if.then66, label %if.end68

if.then66:                                        ; preds = %if.then63
  %39 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call67 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %39, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0), i32 25)
  br label %if.end68

if.end68:                                         ; preds = %if.then66, %if.then63
  %40 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp69 = icmp uge i32 %40, 1
  br i1 %cmp69, label %if.then71, label %if.end73

if.then71:                                        ; preds = %if.end68
  %41 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call72 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %41, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @.str.7, i32 0, i32 0))
  br label %if.end73

if.end73:                                         ; preds = %if.then71, %if.end68
  %42 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp74 = icmp uge i32 %42, 1
  br i1 %cmp74, label %if.then76, label %if.end78

if.then76:                                        ; preds = %if.end73
  %43 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call77 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %43, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end78

if.end78:                                         ; preds = %if.then76, %if.end73
  call void @exit(i32 25) #6
  unreachable

if.end79:                                         ; preds = %if.then59
  br label %if.end81

if.else:                                          ; preds = %if.end56
  %44 = load i64, i64* %dictFileSize, align 8, !tbaa !10
  %conv80 = trunc i64 %44 to i32
  store i32 %conv80, i32* %dictSize, align 4, !tbaa !2
  br label %if.end81

if.end81:                                         ; preds = %if.else, %if.end79
  %45 = load i32, i32* %dictSize, align 4, !tbaa !2
  %call82 = call i8* @malloc(i32 %45)
  store i8* %call82, i8** %dictBuf, align 4, !tbaa !6
  %46 = load i8*, i8** %dictBuf, align 4, !tbaa !6
  %tobool83 = icmp ne i8* %46, null
  br i1 %tobool83, label %if.end100, label %if.then84

if.then84:                                        ; preds = %if.end81
  %47 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp85 = icmp uge i32 %47, 1
  br i1 %cmp85, label %if.then87, label %if.end89

if.then87:                                        ; preds = %if.then84
  %48 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call88 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %48, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0), i32 25)
  br label %if.end89

if.end89:                                         ; preds = %if.then87, %if.then84
  %49 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp90 = icmp uge i32 %49, 1
  br i1 %cmp90, label %if.then92, label %if.end94

if.then92:                                        ; preds = %if.end89
  %50 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call93 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %50, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.8, i32 0, i32 0))
  br label %if.end94

if.end94:                                         ; preds = %if.then92, %if.end89
  %51 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp95 = icmp uge i32 %51, 1
  br i1 %cmp95, label %if.then97, label %if.end99

if.then97:                                        ; preds = %if.end94
  %52 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call98 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %52, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end99

if.end99:                                         ; preds = %if.then97, %if.end94
  call void @exit(i32 25) #6
  unreachable

if.end100:                                        ; preds = %if.end81
  %53 = load i8*, i8** %dictBuf, align 4, !tbaa !6
  %54 = load i32, i32* %dictSize, align 4, !tbaa !2
  %55 = load %struct._IO_FILE*, %struct._IO_FILE** %dictFile, align 4, !tbaa !6
  %call101 = call i32 @fread(i8* %53, i32 1, i32 %54, %struct._IO_FILE* %55)
  %56 = load i32, i32* %dictSize, align 4, !tbaa !2
  %cmp102 = icmp ne i32 %call101, %56
  br i1 %cmp102, label %if.then104, label %if.end120

if.then104:                                       ; preds = %if.end100
  %57 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp105 = icmp uge i32 %57, 1
  br i1 %cmp105, label %if.then107, label %if.end109

if.then107:                                       ; preds = %if.then104
  %58 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call108 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %58, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0), i32 25)
  br label %if.end109

if.end109:                                        ; preds = %if.then107, %if.then104
  %59 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp110 = icmp uge i32 %59, 1
  br i1 %cmp110, label %if.then112, label %if.end114

if.then112:                                       ; preds = %if.end109
  %60 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call113 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %60, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @.str.9, i32 0, i32 0))
  br label %if.end114

if.end114:                                        ; preds = %if.then112, %if.end109
  %61 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp115 = icmp uge i32 %61, 1
  br i1 %cmp115, label %if.then117, label %if.end119

if.then117:                                       ; preds = %if.end114
  %62 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call118 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %62, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end119

if.end119:                                        ; preds = %if.then117, %if.end114
  call void @exit(i32 25) #6
  unreachable

if.end120:                                        ; preds = %if.end100
  %63 = load %struct._IO_FILE*, %struct._IO_FILE** %dictFile, align 4, !tbaa !6
  %call121 = call i32 @fclose(%struct._IO_FILE* %63)
  %64 = bitcast i64* %dictFileSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %64) #5
  %65 = bitcast %struct._IO_FILE** %dictFile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #5
  br label %if.end122

if.end122:                                        ; preds = %if.end120, %if.end17
  %66 = load i32, i32* %nbFiles.addr, align 4, !tbaa !2
  %cmp123 = icmp eq i32 %66, 0
  br i1 %cmp123, label %if.then125, label %if.else126

if.then125:                                       ; preds = %if.end122
  %67 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %68 = load i32, i32* %cLevelLast.addr, align 4, !tbaa !2
  %69 = load double, double* %compressibility, align 8, !tbaa !12
  %70 = load i8*, i8** %dictBuf, align 4, !tbaa !6
  %71 = load i32, i32* %dictSize, align 4, !tbaa !2
  call void @BMK_syntheticTest(i32 %67, i32 %68, double %69, i8* %70, i32 %71)
  br label %if.end132

if.else126:                                       ; preds = %if.end122
  %72 = load i32, i32* @g_benchSeparately, align 4, !tbaa !2
  %tobool127 = icmp ne i32 %72, 0
  br i1 %tobool127, label %if.then128, label %if.else130

if.then128:                                       ; preds = %if.else126
  %73 = load i8**, i8*** %fileNamesTable.addr, align 4, !tbaa !6
  %74 = load i32, i32* %nbFiles.addr, align 4, !tbaa !2
  %75 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %76 = load i32, i32* %cLevelLast.addr, align 4, !tbaa !2
  %77 = load i8*, i8** %dictBuf, align 4, !tbaa !6
  %78 = load i32, i32* %dictSize, align 4, !tbaa !2
  %call129 = call i32 @BMK_benchFilesSeparately(i8** %73, i32 %74, i32 %75, i32 %76, i8* %77, i32 %78)
  br label %if.end131

if.else130:                                       ; preds = %if.else126
  %79 = load i8**, i8*** %fileNamesTable.addr, align 4, !tbaa !6
  %80 = load i32, i32* %nbFiles.addr, align 4, !tbaa !2
  %81 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %82 = load i32, i32* %cLevelLast.addr, align 4, !tbaa !2
  %83 = load i8*, i8** %dictBuf, align 4, !tbaa !6
  %84 = load i32, i32* %dictSize, align 4, !tbaa !2
  call void @BMK_benchFileTable(i8** %79, i32 %80, i32 %81, i32 %82, i8* %83, i32 %84)
  br label %if.end131

if.end131:                                        ; preds = %if.else130, %if.then128
  br label %if.end132

if.end132:                                        ; preds = %if.end131, %if.then125
  %85 = load i8*, i8** %dictBuf, align 4, !tbaa !6
  call void @free(i8* %85)
  %86 = bitcast i32* %dictSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #5
  %87 = bitcast i8** %dictBuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #5
  %88 = bitcast double* %compressibility to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %88) #5
  ret i32 0
}

; Function Attrs: nounwind
define internal i64 @UTIL_getFileSize(i8* %infilename) #0 {
entry:
  %retval = alloca i64, align 8
  %infilename.addr = alloca i8*, align 4
  %r = alloca i32, align 4
  %statbuf = alloca %struct.stat, align 8
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %infilename, i8** %infilename.addr, align 4, !tbaa !6
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast %struct.stat* %statbuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 88, i8* %1) #5
  %2 = load i8*, i8** %infilename.addr, align 4, !tbaa !6
  %call = call i32 @stat(i8* %2, %struct.stat* %statbuf)
  store i32 %call, i32* %r, align 4, !tbaa !2
  %3 = load i32, i32* %r, align 4, !tbaa !2
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %st_mode = getelementptr inbounds %struct.stat, %struct.stat* %statbuf, i32 0, i32 3
  %4 = load i32, i32* %st_mode, align 4, !tbaa !14
  %and = and i32 %4, 61440
  %cmp = icmp eq i32 %and, 32768
  br i1 %cmp, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i64 0, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  %st_size = getelementptr inbounds %struct.stat, %struct.stat* %statbuf, i32 0, i32 9
  %5 = load i64, i64* %st_size, align 8, !tbaa !17
  store i64 %5, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %6 = bitcast %struct.stat* %statbuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 88, i8* %6) #5
  %7 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #5
  %8 = load i64, i64* %retval, align 8
  ret i64 %8
}

; Function Attrs: noreturn
declare void @exit(i32) #3

declare %struct._IO_FILE* @fopen(i8*, i8*) #1

declare i32 @fseeko(%struct._IO_FILE*, i64, i32) #1

declare i8* @malloc(i32) #1

declare i32 @fread(i8*, i32, i32, %struct._IO_FILE*) #1

declare i32 @fclose(%struct._IO_FILE*) #1

; Function Attrs: nounwind
define internal void @BMK_syntheticTest(i32 %cLevel, i32 %cLevelLast, double %compressibility, i8* %dictBuf, i32 %dictSize) #0 {
entry:
  %cLevel.addr = alloca i32, align 4
  %cLevelLast.addr = alloca i32, align 4
  %compressibility.addr = alloca double, align 8
  %dictBuf.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  %name = alloca [20 x i8], align 16
  %benchedSize = alloca i32, align 4
  %srcBuffer = alloca i8*, align 4
  store i32 %cLevel, i32* %cLevel.addr, align 4, !tbaa !2
  store i32 %cLevelLast, i32* %cLevelLast.addr, align 4, !tbaa !2
  store double %compressibility, double* %compressibility.addr, align 8, !tbaa !12
  store i8* %dictBuf, i8** %dictBuf.addr, align 4, !tbaa !6
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !2
  %0 = bitcast [20 x i8]* %name to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %0) #5
  %1 = bitcast [20 x i8]* %name to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %1, i8 0, i32 20, i1 false)
  %2 = bitcast i32* %benchedSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i32 10000000, i32* %benchedSize, align 4, !tbaa !8
  %3 = bitcast i8** %srcBuffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load i32, i32* %benchedSize, align 4, !tbaa !8
  %call = call i8* @malloc(i32 %4)
  store i8* %call, i8** %srcBuffer, align 4, !tbaa !6
  %5 = load i8*, i8** %srcBuffer, align 4, !tbaa !6
  %tobool = icmp ne i8* %5, null
  br i1 %tobool, label %if.end11, label %if.then

if.then:                                          ; preds = %entry
  %6 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp = icmp uge i32 %6, 1
  br i1 %cmp, label %if.then1, label %if.end

if.then1:                                         ; preds = %if.then
  %7 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call2 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %7, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0), i32 21)
  br label %if.end

if.end:                                           ; preds = %if.then1, %if.then
  %8 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp3 = icmp uge i32 %8, 1
  br i1 %cmp3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.end
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call5 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %9, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.11, i32 0, i32 0))
  br label %if.end6

if.end6:                                          ; preds = %if.then4, %if.end
  %10 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp7 = icmp uge i32 %10, 1
  br i1 %cmp7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %if.end6
  %11 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call9 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %11, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end10

if.end10:                                         ; preds = %if.then8, %if.end6
  call void @exit(i32 21) #6
  unreachable

if.end11:                                         ; preds = %entry
  %12 = load i8*, i8** %srcBuffer, align 4, !tbaa !6
  %13 = load i32, i32* %benchedSize, align 4, !tbaa !8
  %14 = load double, double* %compressibility.addr, align 8, !tbaa !12
  call void @RDG_genBuffer(i8* %12, i32 %13, double %14, double 0.000000e+00, i32 0)
  %arraydecay = getelementptr inbounds [20 x i8], [20 x i8]* %name, i32 0, i32 0
  %15 = load double, double* %compressibility.addr, align 8, !tbaa !12
  %mul = fmul double %15, 1.000000e+02
  %conv = fptoui double %mul to i32
  %call12 = call i32 (i8*, i32, i8*, ...) @snprintf(i8* %arraydecay, i32 20, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.47, i32 0, i32 0), i32 %conv)
  %16 = load i8*, i8** %srcBuffer, align 4, !tbaa !6
  %17 = load i32, i32* %benchedSize, align 4, !tbaa !8
  %arraydecay13 = getelementptr inbounds [20 x i8], [20 x i8]* %name, i32 0, i32 0
  %18 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %19 = load i32, i32* %cLevelLast.addr, align 4, !tbaa !2
  %20 = load i8*, i8** %dictBuf.addr, align 4, !tbaa !6
  %21 = load i32, i32* %dictSize.addr, align 4, !tbaa !2
  call void @BMK_benchCLevel(i8* %16, i32 %17, i8* %arraydecay13, i32 %18, i32 %19, i32* %benchedSize, i32 1, i8* %20, i32 %21)
  %22 = load i8*, i8** %srcBuffer, align 4, !tbaa !6
  call void @free(i8* %22)
  %23 = bitcast i8** %srcBuffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #5
  %24 = bitcast i32* %benchedSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #5
  %25 = bitcast [20 x i8]* %name to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %25) #5
  ret void
}

declare void @free(i8*) #1

; Function Attrs: nounwind
define internal i64 @UTIL_getTotalFileSize(i8** %fileNamesTable, i32 %nbFiles) #0 {
entry:
  %fileNamesTable.addr = alloca i8**, align 4
  %nbFiles.addr = alloca i32, align 4
  %total = alloca i64, align 8
  %n = alloca i32, align 4
  store i8** %fileNamesTable, i8*** %fileNamesTable.addr, align 4, !tbaa !6
  store i32 %nbFiles, i32* %nbFiles.addr, align 4, !tbaa !2
  %0 = bitcast i64* %total to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #5
  store i64 0, i64* %total, align 8, !tbaa !10
  %1 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 0, i32* %n, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %n, align 4, !tbaa !2
  %3 = load i32, i32* %nbFiles.addr, align 4, !tbaa !2
  %cmp = icmp ult i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8**, i8*** %fileNamesTable.addr, align 4, !tbaa !6
  %5 = load i32, i32* %n, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8*, i8** %4, i32 %5
  %6 = load i8*, i8** %arrayidx, align 4, !tbaa !6
  %call = call i64 @UTIL_getFileSize(i8* %6)
  %7 = load i64, i64* %total, align 8, !tbaa !10
  %add = add i64 %7, %call
  store i64 %add, i64* %total, align 8, !tbaa !10
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %n, align 4, !tbaa !2
  %inc = add i32 %8, 1
  store i32 %inc, i32* %n, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = load i64, i64* %total, align 8, !tbaa !10
  %10 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #5
  %11 = bitcast i64* %total to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %11) #5
  ret i64 %9
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: nounwind
define internal i32 @BMK_findMaxMem(i64 %requiredMem) #0 {
entry:
  %requiredMem.addr = alloca i64, align 8
  %step = alloca i32, align 4
  %testmem = alloca i8*, align 4
  store i64 %requiredMem, i64* %requiredMem.addr, align 8, !tbaa !10
  %0 = bitcast i32* %step to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 67108864, i32* %step, align 4, !tbaa !8
  %1 = bitcast i8** %testmem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i8* null, i8** %testmem, align 4, !tbaa !6
  %2 = load i64, i64* %requiredMem.addr, align 8, !tbaa !10
  %shr = lshr i64 %2, 26
  %add = add i64 %shr, 1
  %shl = shl i64 %add, 26
  store i64 %shl, i64* %requiredMem.addr, align 8, !tbaa !10
  %3 = load i32, i32* %step, align 4, !tbaa !8
  %mul = mul i32 2, %3
  %conv = zext i32 %mul to i64
  %4 = load i64, i64* %requiredMem.addr, align 8, !tbaa !10
  %add1 = add i64 %4, %conv
  store i64 %add1, i64* %requiredMem.addr, align 8, !tbaa !10
  %5 = load i64, i64* %requiredMem.addr, align 8, !tbaa !10
  %cmp = icmp ugt i64 %5, 2080374784
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i64 2080374784, i64* %requiredMem.addr, align 8, !tbaa !10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  br label %while.cond

while.cond:                                       ; preds = %if.end9, %if.end
  %6 = load i8*, i8** %testmem, align 4, !tbaa !6
  %tobool = icmp ne i8* %6, null
  %lnot = xor i1 %tobool, true
  br i1 %lnot, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load i64, i64* %requiredMem.addr, align 8, !tbaa !10
  %8 = load i32, i32* %step, align 4, !tbaa !8
  %conv3 = zext i32 %8 to i64
  %cmp4 = icmp ugt i64 %7, %conv3
  br i1 %cmp4, label %if.then6, label %if.else

if.then6:                                         ; preds = %while.body
  %9 = load i32, i32* %step, align 4, !tbaa !8
  %conv7 = zext i32 %9 to i64
  %10 = load i64, i64* %requiredMem.addr, align 8, !tbaa !10
  %sub = sub i64 %10, %conv7
  store i64 %sub, i64* %requiredMem.addr, align 8, !tbaa !10
  br label %if.end9

if.else:                                          ; preds = %while.body
  %11 = load i64, i64* %requiredMem.addr, align 8, !tbaa !10
  %shr8 = lshr i64 %11, 1
  store i64 %shr8, i64* %requiredMem.addr, align 8, !tbaa !10
  br label %if.end9

if.end9:                                          ; preds = %if.else, %if.then6
  %12 = load i64, i64* %requiredMem.addr, align 8, !tbaa !10
  %conv10 = trunc i64 %12 to i32
  %call = call i8* @malloc(i32 %conv10)
  store i8* %call, i8** %testmem, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %13 = load i8*, i8** %testmem, align 4, !tbaa !6
  call void @free(i8* %13)
  %14 = load i64, i64* %requiredMem.addr, align 8, !tbaa !10
  %15 = load i32, i32* %step, align 4, !tbaa !8
  %conv11 = zext i32 %15 to i64
  %cmp12 = icmp ugt i64 %14, %conv11
  br i1 %cmp12, label %if.then14, label %if.else17

if.then14:                                        ; preds = %while.end
  %16 = load i32, i32* %step, align 4, !tbaa !8
  %conv15 = zext i32 %16 to i64
  %17 = load i64, i64* %requiredMem.addr, align 8, !tbaa !10
  %sub16 = sub i64 %17, %conv15
  store i64 %sub16, i64* %requiredMem.addr, align 8, !tbaa !10
  br label %if.end19

if.else17:                                        ; preds = %while.end
  %18 = load i64, i64* %requiredMem.addr, align 8, !tbaa !10
  %shr18 = lshr i64 %18, 1
  store i64 %shr18, i64* %requiredMem.addr, align 8, !tbaa !10
  br label %if.end19

if.end19:                                         ; preds = %if.else17, %if.then14
  %19 = load i64, i64* %requiredMem.addr, align 8, !tbaa !10
  %conv20 = trunc i64 %19 to i32
  %20 = bitcast i8** %testmem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  %21 = bitcast i32* %step to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #5
  ret i32 %conv20
}

; Function Attrs: nounwind
define internal void @BMK_loadFiles(i8* %buffer, i32 %bufferSize, i32* %fileSizes, i8** %fileNamesTable, i32 %nbFiles) #0 {
entry:
  %buffer.addr = alloca i8*, align 4
  %bufferSize.addr = alloca i32, align 4
  %fileSizes.addr = alloca i32*, align 4
  %fileNamesTable.addr = alloca i8**, align 4
  %nbFiles.addr = alloca i32, align 4
  %pos = alloca i32, align 4
  %totalSize = alloca i32, align 4
  %n = alloca i32, align 4
  %f = alloca %struct._IO_FILE*, align 4
  %fileSize = alloca i64, align 8
  %cleanup.dest.slot = alloca i32, align 4
  %readSize = alloca i32, align 4
  store i8* %buffer, i8** %buffer.addr, align 4, !tbaa !6
  store i32 %bufferSize, i32* %bufferSize.addr, align 4, !tbaa !8
  store i32* %fileSizes, i32** %fileSizes.addr, align 4, !tbaa !6
  store i8** %fileNamesTable, i8*** %fileNamesTable.addr, align 4, !tbaa !6
  store i32 %nbFiles, i32* %nbFiles.addr, align 4, !tbaa !2
  %0 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %pos, align 4, !tbaa !8
  %1 = bitcast i32* %totalSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 0, i32* %totalSize, align 4, !tbaa !8
  %2 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i32 0, i32* %n, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %n, align 4, !tbaa !2
  %4 = load i32, i32* %nbFiles.addr, align 4, !tbaa !2
  %cmp = icmp ult i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast %struct._IO_FILE** %f to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = bitcast i64* %fileSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #5
  %7 = load i8**, i8*** %fileNamesTable.addr, align 4, !tbaa !6
  %8 = load i32, i32* %n, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8*, i8** %7, i32 %8
  %9 = load i8*, i8** %arrayidx, align 4, !tbaa !6
  %call = call i64 @UTIL_getFileSize(i8* %9)
  store i64 %call, i64* %fileSize, align 8, !tbaa !10
  %10 = load i8**, i8*** %fileNamesTable.addr, align 4, !tbaa !6
  %11 = load i32, i32* %n, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8*, i8** %10, i32 %11
  %12 = load i8*, i8** %arrayidx1, align 4, !tbaa !6
  %call2 = call i32 @UTIL_isDirectory(i8* %12)
  %tobool = icmp ne i32 %call2, 0
  br i1 %tobool, label %if.then, label %if.end8

if.then:                                          ; preds = %for.body
  %13 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp3 = icmp uge i32 %13, 2
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.then
  %14 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %15 = load i8**, i8*** %fileNamesTable.addr, align 4, !tbaa !6
  %16 = load i32, i32* %n, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8*, i8** %15, i32 %16
  %17 = load i8*, i8** %arrayidx5, align 4, !tbaa !6
  %call6 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %14, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.15, i32 0, i32 0), i8* %17)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.then
  %18 = load i32*, i32** %fileSizes.addr, align 4, !tbaa !6
  %19 = load i32, i32* %n, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i32, i32* %18, i32 %19
  store i32 0, i32* %arrayidx7, align 4, !tbaa !8
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %for.body
  %20 = load i8**, i8*** %fileNamesTable.addr, align 4, !tbaa !6
  %21 = load i32, i32* %n, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i8*, i8** %20, i32 %21
  %22 = load i8*, i8** %arrayidx9, align 4, !tbaa !6
  %call10 = call %struct._IO_FILE* @fopen(i8* %22, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.5, i32 0, i32 0))
  store %struct._IO_FILE* %call10, %struct._IO_FILE** %f, align 4, !tbaa !6
  %23 = load %struct._IO_FILE*, %struct._IO_FILE** %f, align 4, !tbaa !6
  %cmp11 = icmp eq %struct._IO_FILE* %23, null
  br i1 %cmp11, label %if.then12, label %if.end26

if.then12:                                        ; preds = %if.end8
  %24 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp13 = icmp uge i32 %24, 1
  br i1 %cmp13, label %if.then14, label %if.end16

if.then14:                                        ; preds = %if.then12
  %25 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call15 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %25, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0), i32 10)
  br label %if.end16

if.end16:                                         ; preds = %if.then14, %if.then12
  %26 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp17 = icmp uge i32 %26, 1
  br i1 %cmp17, label %if.then18, label %if.end21

if.then18:                                        ; preds = %if.end16
  %27 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %28 = load i8**, i8*** %fileNamesTable.addr, align 4, !tbaa !6
  %29 = load i32, i32* %n, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i8*, i8** %28, i32 %29
  %30 = load i8*, i8** %arrayidx19, align 4, !tbaa !6
  %call20 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %27, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.16, i32 0, i32 0), i8* %30)
  br label %if.end21

if.end21:                                         ; preds = %if.then18, %if.end16
  %31 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp22 = icmp uge i32 %31, 1
  br i1 %cmp22, label %if.then23, label %if.end25

if.then23:                                        ; preds = %if.end21
  %32 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call24 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %32, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end25

if.end25:                                         ; preds = %if.then23, %if.end21
  call void @exit(i32 10) #6
  unreachable

if.end26:                                         ; preds = %if.end8
  %33 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp27 = icmp uge i32 %33, 2
  br i1 %cmp27, label %if.then28, label %if.end41

if.then28:                                        ; preds = %if.end26
  %call29 = call i32 @clock()
  %34 = load i32, i32* @g_time, align 4, !tbaa !8
  %sub = sub nsw i32 %call29, %34
  %cmp30 = icmp sgt i32 %sub, 150000
  br i1 %cmp30, label %if.then32, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then28
  %35 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp31 = icmp uge i32 %35, 4
  br i1 %cmp31, label %if.then32, label %if.end40

if.then32:                                        ; preds = %lor.lhs.false, %if.then28
  %call33 = call i32 @clock()
  store i32 %call33, i32* @g_time, align 4, !tbaa !8
  %36 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %37 = load i8**, i8*** %fileNamesTable.addr, align 4, !tbaa !6
  %38 = load i32, i32* %n, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i8*, i8** %37, i32 %38
  %39 = load i8*, i8** %arrayidx34, align 4, !tbaa !6
  %call35 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %36, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.17, i32 0, i32 0), i8* %39)
  %40 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp36 = icmp uge i32 %40, 4
  br i1 %cmp36, label %if.then37, label %if.end39

if.then37:                                        ; preds = %if.then32
  %41 = load %struct._IO_FILE*, %struct._IO_FILE** @stdout, align 4, !tbaa !6
  %call38 = call i32 @fflush(%struct._IO_FILE* %41)
  br label %if.end39

if.end39:                                         ; preds = %if.then37, %if.then32
  br label %if.end40

if.end40:                                         ; preds = %if.end39, %lor.lhs.false
  br label %if.end41

if.end41:                                         ; preds = %if.end40, %if.end26
  %42 = load i64, i64* %fileSize, align 8, !tbaa !10
  %43 = load i32, i32* %bufferSize.addr, align 4, !tbaa !8
  %44 = load i32, i32* %pos, align 4, !tbaa !8
  %sub42 = sub i32 %43, %44
  %conv = zext i32 %sub42 to i64
  %cmp43 = icmp ugt i64 %42, %conv
  br i1 %cmp43, label %if.then45, label %if.end48

if.then45:                                        ; preds = %if.end41
  %45 = load i32, i32* %bufferSize.addr, align 4, !tbaa !8
  %46 = load i32, i32* %pos, align 4, !tbaa !8
  %sub46 = sub i32 %45, %46
  %conv47 = zext i32 %sub46 to i64
  store i64 %conv47, i64* %fileSize, align 8, !tbaa !10
  %47 = load i32, i32* %n, align 4, !tbaa !2
  store i32 %47, i32* %nbFiles.addr, align 4, !tbaa !2
  br label %if.end48

if.end48:                                         ; preds = %if.then45, %if.end41
  %48 = bitcast i32* %readSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #5
  %49 = load i8*, i8** %buffer.addr, align 4, !tbaa !6
  %50 = load i32, i32* %pos, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* %49, i32 %50
  %51 = load i64, i64* %fileSize, align 8, !tbaa !10
  %conv49 = trunc i64 %51 to i32
  %52 = load %struct._IO_FILE*, %struct._IO_FILE** %f, align 4, !tbaa !6
  %call50 = call i32 @fread(i8* %add.ptr, i32 1, i32 %conv49, %struct._IO_FILE* %52)
  store i32 %call50, i32* %readSize, align 4, !tbaa !8
  %53 = load i32, i32* %readSize, align 4, !tbaa !8
  %54 = load i64, i64* %fileSize, align 8, !tbaa !10
  %conv51 = trunc i64 %54 to i32
  %cmp52 = icmp ne i32 %53, %conv51
  br i1 %cmp52, label %if.then54, label %if.end71

if.then54:                                        ; preds = %if.end48
  %55 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp55 = icmp uge i32 %55, 1
  br i1 %cmp55, label %if.then57, label %if.end59

if.then57:                                        ; preds = %if.then54
  %56 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call58 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %56, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0), i32 11)
  br label %if.end59

if.end59:                                         ; preds = %if.then57, %if.then54
  %57 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp60 = icmp uge i32 %57, 1
  br i1 %cmp60, label %if.then62, label %if.end65

if.then62:                                        ; preds = %if.end59
  %58 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %59 = load i8**, i8*** %fileNamesTable.addr, align 4, !tbaa !6
  %60 = load i32, i32* %n, align 4, !tbaa !2
  %arrayidx63 = getelementptr inbounds i8*, i8** %59, i32 %60
  %61 = load i8*, i8** %arrayidx63, align 4, !tbaa !6
  %call64 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %58, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.18, i32 0, i32 0), i8* %61)
  br label %if.end65

if.end65:                                         ; preds = %if.then62, %if.end59
  %62 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp66 = icmp uge i32 %62, 1
  br i1 %cmp66, label %if.then68, label %if.end70

if.then68:                                        ; preds = %if.end65
  %63 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call69 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %63, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end70

if.end70:                                         ; preds = %if.then68, %if.end65
  call void @exit(i32 11) #6
  unreachable

if.end71:                                         ; preds = %if.end48
  %64 = load i32, i32* %readSize, align 4, !tbaa !8
  %65 = load i32, i32* %pos, align 4, !tbaa !8
  %add = add i32 %65, %64
  store i32 %add, i32* %pos, align 4, !tbaa !8
  %66 = bitcast i32* %readSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #5
  %67 = load i64, i64* %fileSize, align 8, !tbaa !10
  %conv72 = trunc i64 %67 to i32
  %68 = load i32*, i32** %fileSizes.addr, align 4, !tbaa !6
  %69 = load i32, i32* %n, align 4, !tbaa !2
  %arrayidx73 = getelementptr inbounds i32, i32* %68, i32 %69
  store i32 %conv72, i32* %arrayidx73, align 4, !tbaa !8
  %70 = load i64, i64* %fileSize, align 8, !tbaa !10
  %conv74 = trunc i64 %70 to i32
  %71 = load i32, i32* %totalSize, align 4, !tbaa !8
  %add75 = add i32 %71, %conv74
  store i32 %add75, i32* %totalSize, align 4, !tbaa !8
  %72 = load %struct._IO_FILE*, %struct._IO_FILE** %f, align 4, !tbaa !6
  %call76 = call i32 @fclose(%struct._IO_FILE* %72)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end71, %if.end
  %73 = bitcast i64* %fileSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %73) #5
  %74 = bitcast %struct._IO_FILE** %f to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 4, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %75 = load i32, i32* %n, align 4, !tbaa !2
  %inc = add i32 %75, 1
  store i32 %inc, i32* %n, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %76 = load i32, i32* %totalSize, align 4, !tbaa !8
  %cmp78 = icmp eq i32 %76, 0
  br i1 %cmp78, label %if.then80, label %if.end96

if.then80:                                        ; preds = %for.end
  %77 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp81 = icmp uge i32 %77, 1
  br i1 %cmp81, label %if.then83, label %if.end85

if.then83:                                        ; preds = %if.then80
  %78 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call84 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %78, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0), i32 12)
  br label %if.end85

if.end85:                                         ; preds = %if.then83, %if.then80
  %79 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp86 = icmp uge i32 %79, 1
  br i1 %cmp86, label %if.then88, label %if.end90

if.then88:                                        ; preds = %if.end85
  %80 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call89 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %80, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.19, i32 0, i32 0))
  br label %if.end90

if.end90:                                         ; preds = %if.then88, %if.end85
  %81 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp91 = icmp uge i32 %81, 1
  br i1 %cmp91, label %if.then93, label %if.end95

if.then93:                                        ; preds = %if.end90
  %82 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call94 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %82, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end95

if.end95:                                         ; preds = %if.then93, %if.end90
  call void @exit(i32 12) #6
  unreachable

if.end96:                                         ; preds = %for.end
  %83 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #5
  %84 = bitcast i32* %totalSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #5
  %85 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #5
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

declare i32 @snprintf(i8*, i32, i8*, ...) #1

; Function Attrs: nounwind
define internal void @BMK_benchCLevel(i8* %srcBuffer, i32 %benchedSize, i8* %displayName, i32 %cLevel, i32 %cLevelLast, i32* %fileSizes, i32 %nbFiles, i8* %dictBuf, i32 %dictSize) #0 {
entry:
  %srcBuffer.addr = alloca i8*, align 4
  %benchedSize.addr = alloca i32, align 4
  %displayName.addr = alloca i8*, align 4
  %cLevel.addr = alloca i32, align 4
  %cLevelLast.addr = alloca i32, align 4
  %fileSizes.addr = alloca i32*, align 4
  %nbFiles.addr = alloca i32, align 4
  %dictBuf.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  %l = alloca i32, align 4
  %pch = alloca i8*, align 4
  store i8* %srcBuffer, i8** %srcBuffer.addr, align 4, !tbaa !6
  store i32 %benchedSize, i32* %benchedSize.addr, align 4, !tbaa !8
  store i8* %displayName, i8** %displayName.addr, align 4, !tbaa !6
  store i32 %cLevel, i32* %cLevel.addr, align 4, !tbaa !2
  store i32 %cLevelLast, i32* %cLevelLast.addr, align 4, !tbaa !2
  store i32* %fileSizes, i32** %fileSizes.addr, align 4, !tbaa !6
  store i32 %nbFiles, i32* %nbFiles.addr, align 4, !tbaa !2
  store i8* %dictBuf, i8** %dictBuf.addr, align 4, !tbaa !6
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !2
  %0 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i8** %pch to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load i8*, i8** %displayName.addr, align 4, !tbaa !6
  %call = call i8* @strrchr(i8* %2, i32 92)
  store i8* %call, i8** %pch, align 4, !tbaa !6
  %3 = load i8*, i8** %pch, align 4, !tbaa !6
  %tobool = icmp ne i8* %3, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %4 = load i8*, i8** %displayName.addr, align 4, !tbaa !6
  %call1 = call i8* @strrchr(i8* %4, i32 47)
  store i8* %call1, i8** %pch, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load i8*, i8** %pch, align 4, !tbaa !6
  %tobool2 = icmp ne i8* %5, null
  br i1 %tobool2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  %6 = load i8*, i8** %pch, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 1
  store i8* %add.ptr, i8** %displayName.addr, align 4, !tbaa !6
  br label %if.end4

if.end4:                                          ; preds = %if.then3, %if.end
  %call5 = call i32 @setpriority(i32 0, i32 0, i32 -20)
  %7 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp = icmp eq i32 %7, 1
  br i1 %cmp, label %land.lhs.true, label %if.end9

land.lhs.true:                                    ; preds = %if.end4
  %8 = load i32, i32* @g_additionalParam, align 4, !tbaa !2
  %tobool6 = icmp ne i32 %8, 0
  br i1 %tobool6, label %if.end9, label %if.then7

if.then7:                                         ; preds = %land.lhs.true
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %10 = load i32, i32* %benchedSize.addr, align 4, !tbaa !8
  %11 = load i32, i32* @g_nbSeconds, align 4, !tbaa !2
  %12 = load i32, i32* @g_blockSize, align 4, !tbaa !8
  %shr = lshr i32 %12, 10
  %call8 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %9, i8* getelementptr inbounds ([55 x i8], [55 x i8]* @.str.20, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.21, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @.str.22, i32 0, i32 0), i32 %10, i32 %11, i32 %shr)
  br label %if.end9

if.end9:                                          ; preds = %if.then7, %land.lhs.true, %if.end4
  %13 = load i32, i32* %cLevelLast.addr, align 4, !tbaa !2
  %14 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %cmp10 = icmp slt i32 %13, %14
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end9
  %15 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  store i32 %15, i32* %cLevelLast.addr, align 4, !tbaa !2
  br label %if.end12

if.end12:                                         ; preds = %if.then11, %if.end9
  %16 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  store i32 %16, i32* %l, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end12
  %17 = load i32, i32* %l, align 4, !tbaa !2
  %18 = load i32, i32* %cLevelLast.addr, align 4, !tbaa !2
  %cmp13 = icmp sle i32 %17, %18
  br i1 %cmp13, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %19 = load i8*, i8** %srcBuffer.addr, align 4, !tbaa !6
  %20 = load i32, i32* %benchedSize.addr, align 4, !tbaa !8
  %21 = load i8*, i8** %displayName.addr, align 4, !tbaa !6
  %22 = load i32, i32* %l, align 4, !tbaa !2
  %23 = load i32*, i32** %fileSizes.addr, align 4, !tbaa !6
  %24 = load i32, i32* %nbFiles.addr, align 4, !tbaa !2
  %25 = load i8*, i8** %dictBuf.addr, align 4, !tbaa !6
  %26 = load i32, i32* %dictSize.addr, align 4, !tbaa !2
  %call14 = call i32 @BMK_benchMem(i8* %19, i32 %20, i8* %21, i32 %22, i32* %23, i32 %24, i8* %25, i32 %26)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %27 = load i32, i32* %l, align 4, !tbaa !2
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %l, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %28 = bitcast i8** %pch to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  %29 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #5
  ret void
}

; Function Attrs: nounwind
define internal i32 @UTIL_isDirectory(i8* %infilename) #0 {
entry:
  %retval = alloca i32, align 4
  %infilename.addr = alloca i8*, align 4
  %r = alloca i32, align 4
  %statbuf = alloca %struct.stat, align 8
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %infilename, i8** %infilename.addr, align 4, !tbaa !6
  %0 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast %struct.stat* %statbuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 88, i8* %1) #5
  %2 = load i8*, i8** %infilename.addr, align 4, !tbaa !6
  %call = call i32 @stat(i8* %2, %struct.stat* %statbuf)
  store i32 %call, i32* %r, align 4, !tbaa !2
  %3 = load i32, i32* %r, align 4, !tbaa !2
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.end, label %land.lhs.true

land.lhs.true:                                    ; preds = %entry
  %st_mode = getelementptr inbounds %struct.stat, %struct.stat* %statbuf, i32 0, i32 3
  %4 = load i32, i32* %st_mode, align 4, !tbaa !14
  %and = and i32 %4, 61440
  %cmp = icmp eq i32 %and, 16384
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %land.lhs.true, %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %5 = bitcast %struct.stat* %statbuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 88, i8* %5) #5
  %6 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #5
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

declare i32 @clock() #1

declare i32 @fflush(%struct._IO_FILE*) #1

declare i32 @stat(i8*, %struct.stat*) #1

declare i8* @strrchr(i8*, i32) #1

declare i32 @setpriority(i32, i32, i32) #1

; Function Attrs: nounwind
define internal i32 @BMK_benchMem(i8* %srcBuffer, i32 %srcSize, i8* %displayName, i32 %cLevel, i32* %fileSizes, i32 %nbFiles, i8* %dictBuf, i32 %dictSize) #0 {
entry:
  %srcBuffer.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %displayName.addr = alloca i8*, align 4
  %cLevel.addr = alloca i32, align 4
  %fileSizes.addr = alloca i32*, align 4
  %nbFiles.addr = alloca i32, align 4
  %dictBuf.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  %blockSize = alloca i32, align 4
  %maxNbBlocks = alloca i32, align 4
  %blockTable = alloca %struct.blockParam_t*, align 4
  %maxCompressedSize = alloca i32, align 4
  %compressedBuffer = alloca i8*, align 4
  %resultBuffer = alloca i8*, align 4
  %nbBlocks = alloca i32, align 4
  %compP = alloca %struct.compressionParameters, align 4
  %srcPtr = alloca i8*, align 4
  %cPtr = alloca i8*, align 4
  %resPtr = alloca i8*, align 4
  %fileNb = alloca i32, align 4
  %remaining = alloca i32, align 4
  %nbBlocksforThisFile = alloca i32, align 4
  %blockEnd = alloca i32, align 4
  %thisBlockSize = alloca i32, align 4
  %fastestC = alloca i64, align 8
  %fastestD = alloca i64, align 8
  %crcOrig = alloca i64, align 8
  %coolTime = alloca i32, align 4
  %maxTime = alloca i64, align 8
  %nbCompressionLoops = alloca i32, align 4
  %nbDecodeLoops = alloca i32, align 4
  %totalCTime = alloca i64, align 8
  %totalDTime = alloca i64, align 8
  %cCompleted = alloca i32, align 4
  %dCompleted = alloca i32, align 4
  %marks = alloca [4 x i8*], align 16
  %markNb = alloca i32, align 4
  %cSize = alloca i32, align 4
  %ratio = alloca double, align 8
  %t = alloca %struct.timespec, align 4
  %clockStart = alloca i32, align 4
  %nbLoops = alloca i32, align 4
  %blockNb = alloca i32, align 4
  %rSize = alloca i32, align 4
  %clockSpan = alloca i64, align 8
  %blockNb181 = alloca i32, align 4
  %t213 = alloca %struct.timespec, align 4
  %clockStart219 = alloca i32, align 4
  %nbLoops221 = alloca i32, align 4
  %blockNb226 = alloca i32, align 4
  %regenSize = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %clockSpan252 = alloca i64, align 8
  %crcCheck = alloca i64, align 8
  %u = alloca i32, align 4
  %segNb = alloca i32, align 4
  %bNb = alloca i32, align 4
  %pos = alloca i32, align 4
  %bacc = alloca i32, align 4
  %cSpeed = alloca double, align 8
  %dSpeed = alloca double, align 8
  store i8* %srcBuffer, i8** %srcBuffer.addr, align 4, !tbaa !6
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !8
  store i8* %displayName, i8** %displayName.addr, align 4, !tbaa !6
  store i32 %cLevel, i32* %cLevel.addr, align 4, !tbaa !2
  store i32* %fileSizes, i32** %fileSizes.addr, align 4, !tbaa !6
  store i32 %nbFiles, i32* %nbFiles.addr, align 4, !tbaa !2
  store i8* %dictBuf, i8** %dictBuf.addr, align 4, !tbaa !6
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !2
  %0 = bitcast i32* %blockSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* @g_blockSize, align 4, !tbaa !8
  %cmp = icmp uge i32 %1, 32
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* @g_blockSize, align 4, !tbaa !8
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %2, %cond.true ], [ %3, %cond.false ]
  %4 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %4, 0
  %lnot = xor i1 %tobool, true
  %lnot.ext = zext i1 %lnot to i32
  %add = add i32 %cond, %lnot.ext
  store i32 %add, i32* %blockSize, align 4, !tbaa !8
  %5 = bitcast i32* %maxNbBlocks to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  %7 = load i32, i32* %blockSize, align 4, !tbaa !8
  %sub = sub i32 %7, 1
  %add1 = add i32 %6, %sub
  %8 = load i32, i32* %blockSize, align 4, !tbaa !8
  %div = udiv i32 %add1, %8
  %9 = load i32, i32* %nbFiles.addr, align 4, !tbaa !2
  %add2 = add i32 %div, %9
  store i32 %add2, i32* %maxNbBlocks, align 4, !tbaa !2
  %10 = bitcast %struct.blockParam_t** %blockTable to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load i32, i32* %maxNbBlocks, align 4, !tbaa !2
  %mul = mul i32 %11, 28
  %call = call i8* @malloc(i32 %mul)
  %12 = bitcast i8* %call to %struct.blockParam_t*
  store %struct.blockParam_t* %12, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %13 = bitcast i32* %maxCompressedSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  %14 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  %call3 = call i32 @LZ4_compressBound(i32 %14)
  %15 = load i32, i32* %maxNbBlocks, align 4, !tbaa !2
  %mul4 = mul i32 %15, 1024
  %add5 = add i32 %call3, %mul4
  store i32 %add5, i32* %maxCompressedSize, align 4, !tbaa !8
  %16 = bitcast i8** %compressedBuffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #5
  %17 = load i32, i32* %maxCompressedSize, align 4, !tbaa !8
  %call6 = call i8* @malloc(i32 %17)
  store i8* %call6, i8** %compressedBuffer, align 4, !tbaa !6
  %18 = bitcast i8** %resultBuffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #5
  %19 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  %call7 = call i8* @malloc(i32 %19)
  store i8* %call7, i8** %resultBuffer, align 4, !tbaa !6
  %20 = bitcast i32* %nbBlocks to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #5
  %21 = bitcast %struct.compressionParameters* %compP to i8*
  call void @llvm.lifetime.start.p0i8(i64 44, i8* %21) #5
  %22 = load i8*, i8** %compressedBuffer, align 4, !tbaa !6
  %tobool8 = icmp ne i8* %22, null
  br i1 %tobool8, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %cond.end
  %23 = load i8*, i8** %resultBuffer, align 4, !tbaa !6
  %tobool9 = icmp ne i8* %23, null
  br i1 %tobool9, label %lor.lhs.false10, label %if.then

lor.lhs.false10:                                  ; preds = %lor.lhs.false
  %24 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %tobool11 = icmp ne %struct.blockParam_t* %24, null
  br i1 %tobool11, label %if.end23, label %if.then

if.then:                                          ; preds = %lor.lhs.false10, %lor.lhs.false, %cond.end
  %25 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp12 = icmp uge i32 %25, 1
  br i1 %cmp12, label %if.then13, label %if.end

if.then13:                                        ; preds = %if.then
  %26 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call14 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %26, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0), i32 31)
  br label %if.end

if.end:                                           ; preds = %if.then13, %if.then
  %27 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp15 = icmp uge i32 %27, 1
  br i1 %cmp15, label %if.then16, label %if.end18

if.then16:                                        ; preds = %if.end
  %28 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call17 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %28, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.23, i32 0, i32 0))
  br label %if.end18

if.end18:                                         ; preds = %if.then16, %if.end
  %29 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp19 = icmp uge i32 %29, 1
  br i1 %cmp19, label %if.then20, label %if.end22

if.then20:                                        ; preds = %if.end18
  %30 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call21 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %30, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end22

if.end22:                                         ; preds = %if.then20, %if.end18
  call void @exit(i32 31) #6
  unreachable

if.end23:                                         ; preds = %lor.lhs.false10
  %31 = load i8*, i8** %displayName.addr, align 4, !tbaa !6
  %call24 = call i32 @strlen(i8* %31)
  %cmp25 = icmp ugt i32 %call24, 17
  br i1 %cmp25, label %if.then26, label %if.end29

if.then26:                                        ; preds = %if.end23
  %32 = load i8*, i8** %displayName.addr, align 4, !tbaa !6
  %call27 = call i32 @strlen(i8* %32)
  %sub28 = sub i32 %call27, 17
  %33 = load i8*, i8** %displayName.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %33, i32 %sub28
  store i8* %add.ptr, i8** %displayName.addr, align 4, !tbaa !6
  br label %if.end29

if.end29:                                         ; preds = %if.then26, %if.end23
  %34 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %35 = load i8*, i8** %dictBuf.addr, align 4, !tbaa !6
  %36 = load i32, i32* %dictSize.addr, align 4, !tbaa !2
  call void @LZ4_buildCompressionParameters(%struct.compressionParameters* %compP, i32 %34, i8* %35, i32 %36)
  %initFunction = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %compP, i32 0, i32 7
  %37 = load void (%struct.compressionParameters*)*, void (%struct.compressionParameters*)** %initFunction, align 4, !tbaa !18
  call void %37(%struct.compressionParameters* %compP)
  %38 = bitcast i8** %srcPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #5
  %39 = load i8*, i8** %srcBuffer.addr, align 4, !tbaa !6
  store i8* %39, i8** %srcPtr, align 4, !tbaa !6
  %40 = bitcast i8** %cPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #5
  %41 = load i8*, i8** %compressedBuffer, align 4, !tbaa !6
  store i8* %41, i8** %cPtr, align 4, !tbaa !6
  %42 = bitcast i8** %resPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #5
  %43 = load i8*, i8** %resultBuffer, align 4, !tbaa !6
  store i8* %43, i8** %resPtr, align 4, !tbaa !6
  %44 = bitcast i32* %fileNb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #5
  store i32 0, i32* %nbBlocks, align 4, !tbaa !2
  store i32 0, i32* %fileNb, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc59, %if.end29
  %45 = load i32, i32* %fileNb, align 4, !tbaa !2
  %46 = load i32, i32* %nbFiles.addr, align 4, !tbaa !2
  %cmp30 = icmp ult i32 %45, %46
  br i1 %cmp30, label %for.body, label %for.end61

for.body:                                         ; preds = %for.cond
  %47 = bitcast i32* %remaining to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #5
  %48 = load i32*, i32** %fileSizes.addr, align 4, !tbaa !6
  %49 = load i32, i32* %fileNb, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %48, i32 %49
  %50 = load i32, i32* %arrayidx, align 4, !tbaa !8
  store i32 %50, i32* %remaining, align 4, !tbaa !8
  %51 = bitcast i32* %nbBlocksforThisFile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #5
  %52 = load i32, i32* %remaining, align 4, !tbaa !8
  %53 = load i32, i32* %blockSize, align 4, !tbaa !8
  %sub31 = sub i32 %53, 1
  %add32 = add i32 %52, %sub31
  %54 = load i32, i32* %blockSize, align 4, !tbaa !8
  %div33 = udiv i32 %add32, %54
  store i32 %div33, i32* %nbBlocksforThisFile, align 4, !tbaa !2
  %55 = bitcast i32* %blockEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #5
  %56 = load i32, i32* %nbBlocks, align 4, !tbaa !2
  %57 = load i32, i32* %nbBlocksforThisFile, align 4, !tbaa !2
  %add34 = add i32 %56, %57
  store i32 %add34, i32* %blockEnd, align 4, !tbaa !2
  br label %for.cond35

for.cond35:                                       ; preds = %for.inc, %for.body
  %58 = load i32, i32* %nbBlocks, align 4, !tbaa !2
  %59 = load i32, i32* %blockEnd, align 4, !tbaa !2
  %cmp36 = icmp ult i32 %58, %59
  br i1 %cmp36, label %for.body37, label %for.end

for.body37:                                       ; preds = %for.cond35
  %60 = bitcast i32* %thisBlockSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #5
  %61 = load i32, i32* %remaining, align 4, !tbaa !8
  %62 = load i32, i32* %blockSize, align 4, !tbaa !8
  %cmp38 = icmp ult i32 %61, %62
  br i1 %cmp38, label %cond.true39, label %cond.false40

cond.true39:                                      ; preds = %for.body37
  %63 = load i32, i32* %remaining, align 4, !tbaa !8
  br label %cond.end41

cond.false40:                                     ; preds = %for.body37
  %64 = load i32, i32* %blockSize, align 4, !tbaa !8
  br label %cond.end41

cond.end41:                                       ; preds = %cond.false40, %cond.true39
  %cond42 = phi i32 [ %63, %cond.true39 ], [ %64, %cond.false40 ]
  store i32 %cond42, i32* %thisBlockSize, align 4, !tbaa !8
  %65 = load i8*, i8** %srcPtr, align 4, !tbaa !6
  %66 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %67 = load i32, i32* %nbBlocks, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %66, i32 %67
  %srcPtr44 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %arrayidx43, i32 0, i32 0
  store i8* %65, i8** %srcPtr44, align 4, !tbaa !20
  %68 = load i8*, i8** %cPtr, align 4, !tbaa !6
  %69 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %70 = load i32, i32* %nbBlocks, align 4, !tbaa !2
  %arrayidx45 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %69, i32 %70
  %cPtr46 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %arrayidx45, i32 0, i32 2
  store i8* %68, i8** %cPtr46, align 4, !tbaa !22
  %71 = load i8*, i8** %resPtr, align 4, !tbaa !6
  %72 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %73 = load i32, i32* %nbBlocks, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %72, i32 %73
  %resPtr48 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %arrayidx47, i32 0, i32 5
  store i8* %71, i8** %resPtr48, align 4, !tbaa !23
  %74 = load i32, i32* %thisBlockSize, align 4, !tbaa !8
  %75 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %76 = load i32, i32* %nbBlocks, align 4, !tbaa !2
  %arrayidx49 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %75, i32 %76
  %srcSize50 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %arrayidx49, i32 0, i32 1
  store i32 %74, i32* %srcSize50, align 4, !tbaa !24
  %77 = load i32, i32* %thisBlockSize, align 4, !tbaa !8
  %call51 = call i32 @LZ4_compressBound(i32 %77)
  %78 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %79 = load i32, i32* %nbBlocks, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %78, i32 %79
  %cRoom = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %arrayidx52, i32 0, i32 3
  store i32 %call51, i32* %cRoom, align 4, !tbaa !25
  %80 = load i32, i32* %thisBlockSize, align 4, !tbaa !8
  %81 = load i8*, i8** %srcPtr, align 4, !tbaa !6
  %add.ptr53 = getelementptr inbounds i8, i8* %81, i32 %80
  store i8* %add.ptr53, i8** %srcPtr, align 4, !tbaa !6
  %82 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %83 = load i32, i32* %nbBlocks, align 4, !tbaa !2
  %arrayidx54 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %82, i32 %83
  %cRoom55 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %arrayidx54, i32 0, i32 3
  %84 = load i32, i32* %cRoom55, align 4, !tbaa !25
  %85 = load i8*, i8** %cPtr, align 4, !tbaa !6
  %add.ptr56 = getelementptr inbounds i8, i8* %85, i32 %84
  store i8* %add.ptr56, i8** %cPtr, align 4, !tbaa !6
  %86 = load i32, i32* %thisBlockSize, align 4, !tbaa !8
  %87 = load i8*, i8** %resPtr, align 4, !tbaa !6
  %add.ptr57 = getelementptr inbounds i8, i8* %87, i32 %86
  store i8* %add.ptr57, i8** %resPtr, align 4, !tbaa !6
  %88 = load i32, i32* %thisBlockSize, align 4, !tbaa !8
  %89 = load i32, i32* %remaining, align 4, !tbaa !8
  %sub58 = sub i32 %89, %88
  store i32 %sub58, i32* %remaining, align 4, !tbaa !8
  %90 = bitcast i32* %thisBlockSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #5
  br label %for.inc

for.inc:                                          ; preds = %cond.end41
  %91 = load i32, i32* %nbBlocks, align 4, !tbaa !2
  %inc = add i32 %91, 1
  store i32 %inc, i32* %nbBlocks, align 4, !tbaa !2
  br label %for.cond35

for.end:                                          ; preds = %for.cond35
  %92 = bitcast i32* %blockEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #5
  %93 = bitcast i32* %nbBlocksforThisFile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #5
  %94 = bitcast i32* %remaining to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #5
  br label %for.inc59

for.inc59:                                        ; preds = %for.end
  %95 = load i32, i32* %fileNb, align 4, !tbaa !2
  %inc60 = add i32 %95, 1
  store i32 %inc60, i32* %fileNb, align 4, !tbaa !2
  br label %for.cond

for.end61:                                        ; preds = %for.cond
  %96 = bitcast i32* %fileNb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #5
  %97 = bitcast i8** %resPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #5
  %98 = bitcast i8** %cPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #5
  %99 = bitcast i8** %srcPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #5
  %100 = load i8*, i8** %compressedBuffer, align 4, !tbaa !6
  %101 = load i32, i32* %maxCompressedSize, align 4, !tbaa !8
  call void @RDG_genBuffer(i8* %100, i32 %101, double 1.000000e-01, double 5.000000e-01, i32 1)
  %102 = bitcast i64* %fastestC to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %102) #5
  store i64 -1, i64* %fastestC, align 8, !tbaa !10
  %103 = bitcast i64* %fastestD to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %103) #5
  store i64 -1, i64* %fastestD, align 8, !tbaa !10
  %104 = bitcast i64* %crcOrig to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %104) #5
  %105 = load i8*, i8** %srcBuffer.addr, align 4, !tbaa !6
  %106 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  %call62 = call i64 @LZ4_XXH64(i8* %105, i32 %106, i64 0)
  store i64 %call62, i64* %crcOrig, align 8, !tbaa !10
  %107 = bitcast i32* %coolTime to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %107) #5
  %108 = bitcast i64* %maxTime to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %108) #5
  %109 = load i32, i32* @g_nbSeconds, align 4, !tbaa !2
  %mul63 = mul i32 %109, 1
  %conv = zext i32 %mul63 to i64
  %mul64 = mul i64 %conv, 1000000000
  %add65 = add i64 %mul64, 100
  store i64 %add65, i64* %maxTime, align 8, !tbaa !10
  %110 = bitcast i32* %nbCompressionLoops to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %110) #5
  %111 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  %add66 = add i32 %111, 1
  %div67 = udiv i32 5242880, %add66
  %add68 = add i32 %div67, 1
  store i32 %add68, i32* %nbCompressionLoops, align 4, !tbaa !2
  %112 = bitcast i32* %nbDecodeLoops to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %112) #5
  %113 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  %add69 = add i32 %113, 1
  %div70 = udiv i32 209715200, %add69
  %add71 = add i32 %div70, 1
  store i32 %add71, i32* %nbDecodeLoops, align 4, !tbaa !2
  %114 = bitcast i64* %totalCTime to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %114) #5
  store i64 0, i64* %totalCTime, align 8, !tbaa !10
  %115 = bitcast i64* %totalDTime to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %115) #5
  store i64 0, i64* %totalDTime, align 8, !tbaa !10
  %116 = bitcast i32* %cCompleted to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #5
  store i32 0, i32* %cCompleted, align 4, !tbaa !2
  %117 = bitcast i32* %dCompleted to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %117) #5
  store i32 0, i32* %dCompleted, align 4, !tbaa !2
  %118 = bitcast [4 x i8*]* %marks to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %118) #5
  %119 = bitcast [4 x i8*]* %marks to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %119, i8* align 16 bitcast ([4 x i8*]* @__const.BMK_benchMem.marks to i8*), i32 16, i1 false)
  %120 = bitcast i32* %markNb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %120) #5
  store i32 0, i32* %markNb, align 4, !tbaa !2
  %121 = bitcast i32* %cSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %121) #5
  store i32 0, i32* %cSize, align 4, !tbaa !8
  %122 = bitcast double* %ratio to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %122) #5
  store double 0.000000e+00, double* %ratio, align 8, !tbaa !12
  %call72 = call i32 @UTIL_getTime()
  store i32 %call72, i32* %coolTime, align 4, !tbaa !8
  %123 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp73 = icmp uge i32 %123, 2
  br i1 %cmp73, label %if.then75, label %if.end77

if.then75:                                        ; preds = %for.end61
  %124 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call76 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %124, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.28, i32 0, i32 0), i8* getelementptr inbounds ([1 x i8], [1 x i8]* @.str.22, i32 0, i32 0))
  br label %if.end77

if.end77:                                         ; preds = %if.then75, %for.end61
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont359, %if.end77
  %125 = load i32, i32* %cCompleted, align 4, !tbaa !2
  %tobool78 = icmp ne i32 %125, 0
  br i1 %tobool78, label %lor.rhs, label %lor.end

lor.rhs:                                          ; preds = %while.cond
  %126 = load i32, i32* %dCompleted, align 4, !tbaa !2
  %tobool79 = icmp ne i32 %126, 0
  %lnot80 = xor i1 %tobool79, true
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %while.cond
  %127 = phi i1 [ true, %while.cond ], [ %lnot80, %lor.rhs ]
  br i1 %127, label %while.body, label %while.end

while.body:                                       ; preds = %lor.end
  %128 = load i32, i32* %coolTime, align 4, !tbaa !8
  %call82 = call i64 @UTIL_clockSpanMicro(i32 %128)
  %cmp83 = icmp ugt i64 %call82, 70000000
  br i1 %cmp83, label %if.then85, label %if.end93

if.then85:                                        ; preds = %while.body
  %129 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp86 = icmp uge i32 %129, 2
  br i1 %cmp86, label %if.then88, label %if.end90

if.then88:                                        ; preds = %if.then85
  %130 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call89 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %130, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.29, i32 0, i32 0))
  br label %if.end90

if.end90:                                         ; preds = %if.then88, %if.then85
  %call91 = call i32 @sleep(i32 10)
  %call92 = call i32 @UTIL_getTime()
  store i32 %call92, i32* %coolTime, align 4, !tbaa !8
  br label %if.end93

if.end93:                                         ; preds = %if.end90, %while.body
  %131 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp94 = icmp uge i32 %131, 2
  br i1 %cmp94, label %if.then96, label %if.end99

if.then96:                                        ; preds = %if.end93
  %132 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %133 = load i32, i32* %markNb, align 4, !tbaa !2
  %arrayidx97 = getelementptr inbounds [4 x i8*], [4 x i8*]* %marks, i32 0, i32 %133
  %134 = load i8*, i8** %arrayidx97, align 4, !tbaa !6
  %135 = load i8*, i8** %displayName.addr, align 4, !tbaa !6
  %136 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  %call98 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %132, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.30, i32 0, i32 0), i8* %134, i8* %135, i32 %136)
  br label %if.end99

if.end99:                                         ; preds = %if.then96, %if.end93
  %137 = load i32, i32* %cCompleted, align 4, !tbaa !2
  %tobool100 = icmp ne i32 %137, 0
  br i1 %tobool100, label %if.end102, label %if.then101

if.then101:                                       ; preds = %if.end99
  %138 = load i8*, i8** %compressedBuffer, align 4, !tbaa !6
  %139 = load i32, i32* %maxCompressedSize, align 4, !tbaa !8
  call void @llvm.memset.p0i8.i32(i8* align 1 %138, i8 -27, i32 %139, i1 false)
  br label %if.end102

if.end102:                                        ; preds = %if.then101, %if.end99
  %140 = bitcast %struct.timespec* %t to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %140) #5
  %tv_sec = getelementptr inbounds %struct.timespec, %struct.timespec* %t, i32 0, i32 0
  store i32 0, i32* %tv_sec, align 4, !tbaa !26
  %tv_nsec = getelementptr inbounds %struct.timespec, %struct.timespec* %t, i32 0, i32 1
  store i32 1000000, i32* %tv_nsec, align 4, !tbaa !27
  %call103 = call i32 @nanosleep(%struct.timespec* %t, %struct.timespec* null)
  %141 = bitcast %struct.timespec* %t to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %141) #5
  call void @UTIL_waitForNextTick()
  %142 = load i32, i32* %cCompleted, align 4, !tbaa !2
  %tobool104 = icmp ne i32 %142, 0
  br i1 %tobool104, label %if.end180, label %if.then105

if.then105:                                       ; preds = %if.end102
  %143 = bitcast i32* %clockStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %143) #5
  %call106 = call i32 @UTIL_getTime()
  store i32 %call106, i32* %clockStart, align 4, !tbaa !8
  %144 = bitcast i32* %nbLoops to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %144) #5
  store i32 0, i32* %nbLoops, align 4, !tbaa !2
  br label %for.cond107

for.cond107:                                      ; preds = %for.inc148, %if.then105
  %145 = load i32, i32* %nbLoops, align 4, !tbaa !2
  %146 = load i32, i32* %nbCompressionLoops, align 4, !tbaa !2
  %cmp108 = icmp ult i32 %145, %146
  br i1 %cmp108, label %for.body110, label %for.end150

for.body110:                                      ; preds = %for.cond107
  %147 = bitcast i32* %blockNb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %147) #5
  %resetFunction = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %compP, i32 0, i32 8
  %148 = load void (%struct.compressionParameters*)*, void (%struct.compressionParameters*)** %resetFunction, align 4, !tbaa !28
  call void %148(%struct.compressionParameters* %compP)
  store i32 0, i32* %blockNb, align 4, !tbaa !2
  br label %for.cond111

for.cond111:                                      ; preds = %for.inc145, %for.body110
  %149 = load i32, i32* %blockNb, align 4, !tbaa !2
  %150 = load i32, i32* %nbBlocks, align 4, !tbaa !2
  %cmp112 = icmp ult i32 %149, %150
  br i1 %cmp112, label %for.body114, label %for.end147

for.body114:                                      ; preds = %for.cond111
  %151 = bitcast i32* %rSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %151) #5
  %blockFunction = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %compP, i32 0, i32 9
  %152 = load i32 (%struct.compressionParameters*, i8*, i8*, i32, i32)*, i32 (%struct.compressionParameters*, i8*, i8*, i32, i32)** %blockFunction, align 4, !tbaa !29
  %153 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %154 = load i32, i32* %blockNb, align 4, !tbaa !2
  %arrayidx115 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %153, i32 %154
  %srcPtr116 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %arrayidx115, i32 0, i32 0
  %155 = load i8*, i8** %srcPtr116, align 4, !tbaa !20
  %156 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %157 = load i32, i32* %blockNb, align 4, !tbaa !2
  %arrayidx117 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %156, i32 %157
  %cPtr118 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %arrayidx117, i32 0, i32 2
  %158 = load i8*, i8** %cPtr118, align 4, !tbaa !22
  %159 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %160 = load i32, i32* %blockNb, align 4, !tbaa !2
  %arrayidx119 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %159, i32 %160
  %srcSize120 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %arrayidx119, i32 0, i32 1
  %161 = load i32, i32* %srcSize120, align 4, !tbaa !24
  %162 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %163 = load i32, i32* %blockNb, align 4, !tbaa !2
  %arrayidx121 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %162, i32 %163
  %cRoom122 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %arrayidx121, i32 0, i32 3
  %164 = load i32, i32* %cRoom122, align 4, !tbaa !25
  %call123 = call i32 %152(%struct.compressionParameters* %compP, i8* %155, i8* %158, i32 %161, i32 %164)
  store i32 %call123, i32* %rSize, align 4, !tbaa !8
  %165 = load i32, i32* %rSize, align 4, !tbaa !8
  %cmp124 = icmp eq i32 %165, 0
  br i1 %cmp124, label %if.then126, label %if.end142

if.then126:                                       ; preds = %for.body114
  %166 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp127 = icmp uge i32 %166, 1
  br i1 %cmp127, label %if.then129, label %if.end131

if.then129:                                       ; preds = %if.then126
  %167 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call130 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %167, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0), i32 1)
  br label %if.end131

if.end131:                                        ; preds = %if.then129, %if.then126
  %168 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp132 = icmp uge i32 %168, 1
  br i1 %cmp132, label %if.then134, label %if.end136

if.then134:                                       ; preds = %if.end131
  %169 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call135 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %169, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.31, i32 0, i32 0))
  br label %if.end136

if.end136:                                        ; preds = %if.then134, %if.end131
  %170 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp137 = icmp uge i32 %170, 1
  br i1 %cmp137, label %if.then139, label %if.end141

if.then139:                                       ; preds = %if.end136
  %171 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call140 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %171, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end141

if.end141:                                        ; preds = %if.then139, %if.end136
  call void @exit(i32 1) #6
  unreachable

if.end142:                                        ; preds = %for.body114
  %172 = load i32, i32* %rSize, align 4, !tbaa !8
  %173 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %174 = load i32, i32* %blockNb, align 4, !tbaa !2
  %arrayidx143 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %173, i32 %174
  %cSize144 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %arrayidx143, i32 0, i32 4
  store i32 %172, i32* %cSize144, align 4, !tbaa !30
  %175 = bitcast i32* %rSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #5
  br label %for.inc145

for.inc145:                                       ; preds = %if.end142
  %176 = load i32, i32* %blockNb, align 4, !tbaa !2
  %inc146 = add i32 %176, 1
  store i32 %inc146, i32* %blockNb, align 4, !tbaa !2
  br label %for.cond111

for.end147:                                       ; preds = %for.cond111
  %177 = bitcast i32* %blockNb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #5
  br label %for.inc148

for.inc148:                                       ; preds = %for.end147
  %178 = load i32, i32* %nbLoops, align 4, !tbaa !2
  %inc149 = add i32 %178, 1
  store i32 %inc149, i32* %nbLoops, align 4, !tbaa !2
  br label %for.cond107

for.end150:                                       ; preds = %for.cond107
  %179 = bitcast i64* %clockSpan to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %179) #5
  %180 = load i32, i32* %clockStart, align 4, !tbaa !8
  %call151 = call i64 @UTIL_clockSpanNano(i32 %180)
  store i64 %call151, i64* %clockSpan, align 8, !tbaa !10
  %181 = load i64, i64* %clockSpan, align 8, !tbaa !10
  %cmp152 = icmp ugt i64 %181, 0
  br i1 %cmp152, label %if.then154, label %if.else

if.then154:                                       ; preds = %for.end150
  %182 = load i64, i64* %clockSpan, align 8, !tbaa !10
  %183 = load i64, i64* %fastestC, align 8, !tbaa !10
  %184 = load i32, i32* %nbCompressionLoops, align 4, !tbaa !2
  %conv155 = zext i32 %184 to i64
  %mul156 = mul i64 %183, %conv155
  %cmp157 = icmp ult i64 %182, %mul156
  br i1 %cmp157, label %if.then159, label %if.end162

if.then159:                                       ; preds = %if.then154
  %185 = load i64, i64* %clockSpan, align 8, !tbaa !10
  %186 = load i32, i32* %nbCompressionLoops, align 4, !tbaa !2
  %conv160 = zext i32 %186 to i64
  %div161 = udiv i64 %185, %conv160
  store i64 %div161, i64* %fastestC, align 8, !tbaa !10
  br label %if.end162

if.end162:                                        ; preds = %if.then159, %if.then154
  %187 = load i64, i64* %fastestC, align 8, !tbaa !10
  %cmp163 = icmp ugt i64 %187, 0
  br i1 %cmp163, label %lor.end166, label %lor.rhs165

lor.rhs165:                                       ; preds = %if.end162
  call void @__assert_fail(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.32, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.33, i32 0, i32 0), i32 418, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @__func__.BMK_benchMem, i32 0, i32 0)) #6
  unreachable

188:                                              ; No predecessors!
  br label %lor.end166

lor.end166:                                       ; preds = %188, %if.end162
  %189 = phi i1 [ true, %if.end162 ], [ false, %188 ]
  %lor.ext = zext i1 %189 to i32
  %190 = load i64, i64* %fastestC, align 8, !tbaa !10
  %div167 = udiv i64 1000000000, %190
  %conv168 = trunc i64 %div167 to i32
  %add169 = add i32 %conv168, 1
  store i32 %add169, i32* %nbCompressionLoops, align 4, !tbaa !2
  br label %if.end176

if.else:                                          ; preds = %for.end150
  %191 = load i32, i32* %nbCompressionLoops, align 4, !tbaa !2
  %cmp170 = icmp ult i32 %191, 40000000
  br i1 %cmp170, label %lor.end173, label %lor.rhs172

lor.rhs172:                                       ; preds = %if.else
  call void @__assert_fail(i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.34, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.33, i32 0, i32 0), i32 421, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @__func__.BMK_benchMem, i32 0, i32 0)) #6
  unreachable

192:                                              ; No predecessors!
  br label %lor.end173

lor.end173:                                       ; preds = %192, %if.else
  %193 = phi i1 [ true, %if.else ], [ false, %192 ]
  %lor.ext174 = zext i1 %193 to i32
  %194 = load i32, i32* %nbCompressionLoops, align 4, !tbaa !2
  %mul175 = mul i32 %194, 100
  store i32 %mul175, i32* %nbCompressionLoops, align 4, !tbaa !2
  br label %if.end176

if.end176:                                        ; preds = %lor.end173, %lor.end166
  %195 = load i64, i64* %clockSpan, align 8, !tbaa !10
  %196 = load i64, i64* %totalCTime, align 8, !tbaa !10
  %add177 = add i64 %196, %195
  store i64 %add177, i64* %totalCTime, align 8, !tbaa !10
  %197 = load i64, i64* %totalCTime, align 8, !tbaa !10
  %198 = load i64, i64* %maxTime, align 8, !tbaa !10
  %cmp178 = icmp ugt i64 %197, %198
  %conv179 = zext i1 %cmp178 to i32
  store i32 %conv179, i32* %cCompleted, align 4, !tbaa !2
  %199 = bitcast i64* %clockSpan to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %199) #5
  %200 = bitcast i32* %nbLoops to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #5
  %201 = bitcast i32* %clockStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #5
  br label %if.end180

if.end180:                                        ; preds = %if.end176, %if.end102
  store i32 0, i32* %cSize, align 4, !tbaa !8
  %202 = bitcast i32* %blockNb181 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %202) #5
  store i32 0, i32* %blockNb181, align 4, !tbaa !2
  br label %for.cond182

for.cond182:                                      ; preds = %for.inc189, %if.end180
  %203 = load i32, i32* %blockNb181, align 4, !tbaa !2
  %204 = load i32, i32* %nbBlocks, align 4, !tbaa !2
  %cmp183 = icmp ult i32 %203, %204
  br i1 %cmp183, label %for.body185, label %for.end191

for.body185:                                      ; preds = %for.cond182
  %205 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %206 = load i32, i32* %blockNb181, align 4, !tbaa !2
  %arrayidx186 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %205, i32 %206
  %cSize187 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %arrayidx186, i32 0, i32 4
  %207 = load i32, i32* %cSize187, align 4, !tbaa !30
  %208 = load i32, i32* %cSize, align 4, !tbaa !8
  %add188 = add i32 %208, %207
  store i32 %add188, i32* %cSize, align 4, !tbaa !8
  br label %for.inc189

for.inc189:                                       ; preds = %for.body185
  %209 = load i32, i32* %blockNb181, align 4, !tbaa !2
  %inc190 = add i32 %209, 1
  store i32 %inc190, i32* %blockNb181, align 4, !tbaa !2
  br label %for.cond182

for.end191:                                       ; preds = %for.cond182
  %210 = bitcast i32* %blockNb181 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #5
  %211 = load i32, i32* %cSize, align 4, !tbaa !8
  %tobool192 = icmp ne i32 %211, 0
  %lnot193 = xor i1 %tobool192, true
  %lnot.ext194 = zext i1 %lnot193 to i32
  %212 = load i32, i32* %cSize, align 4, !tbaa !8
  %add195 = add i32 %212, %lnot.ext194
  store i32 %add195, i32* %cSize, align 4, !tbaa !8
  %213 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  %conv196 = uitofp i32 %213 to double
  %214 = load i32, i32* %cSize, align 4, !tbaa !8
  %conv197 = uitofp i32 %214 to double
  %div198 = fdiv double %conv196, %conv197
  store double %div198, double* %ratio, align 8, !tbaa !12
  %215 = load i32, i32* %markNb, align 4, !tbaa !2
  %add199 = add i32 %215, 1
  %rem = urem i32 %add199, 4
  store i32 %rem, i32* %markNb, align 4, !tbaa !2
  %216 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp200 = icmp uge i32 %216, 2
  br i1 %cmp200, label %if.then202, label %if.end209

if.then202:                                       ; preds = %for.end191
  %217 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %218 = load i32, i32* %markNb, align 4, !tbaa !2
  %arrayidx203 = getelementptr inbounds [4 x i8*], [4 x i8*]* %marks, i32 0, i32 %218
  %219 = load i8*, i8** %arrayidx203, align 4, !tbaa !6
  %220 = load i8*, i8** %displayName.addr, align 4, !tbaa !6
  %221 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  %222 = load i32, i32* %cSize, align 4, !tbaa !8
  %223 = load double, double* %ratio, align 8, !tbaa !12
  %224 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  %conv204 = uitofp i32 %224 to double
  %225 = load i64, i64* %fastestC, align 8, !tbaa !10
  %conv205 = uitofp i64 %225 to double
  %div206 = fdiv double %conv204, %conv205
  %mul207 = fmul double %div206, 1.000000e+03
  %call208 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %217, i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.35, i32 0, i32 0), i8* %219, i8* %220, i32 %221, i32 %222, double %223, double %mul207)
  br label %if.end209

if.end209:                                        ; preds = %if.then202, %for.end191
  %226 = load i64, i64* %fastestD, align 8, !tbaa !10
  %227 = load i64, i64* %crcOrig, align 8, !tbaa !10
  %228 = load i32, i32* %dCompleted, align 4, !tbaa !2
  %tobool210 = icmp ne i32 %228, 0
  br i1 %tobool210, label %if.end212, label %if.then211

if.then211:                                       ; preds = %if.end209
  %229 = load i8*, i8** %resultBuffer, align 4, !tbaa !6
  %230 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  call void @llvm.memset.p0i8.i32(i8* align 1 %229, i8 -42, i32 %230, i1 false)
  br label %if.end212

if.end212:                                        ; preds = %if.then211, %if.end209
  %231 = bitcast %struct.timespec* %t213 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %231) #5
  %tv_sec214 = getelementptr inbounds %struct.timespec, %struct.timespec* %t213, i32 0, i32 0
  store i32 0, i32* %tv_sec214, align 4, !tbaa !26
  %tv_nsec215 = getelementptr inbounds %struct.timespec, %struct.timespec* %t213, i32 0, i32 1
  store i32 5000000, i32* %tv_nsec215, align 4, !tbaa !27
  %call216 = call i32 @nanosleep(%struct.timespec* %t213, %struct.timespec* null)
  %232 = bitcast %struct.timespec* %t213 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %232) #5
  call void @UTIL_waitForNextTick()
  %233 = load i32, i32* %dCompleted, align 4, !tbaa !2
  %tobool217 = icmp ne i32 %233, 0
  br i1 %tobool217, label %if.end285, label %if.then218

if.then218:                                       ; preds = %if.end212
  %234 = bitcast i32* %clockStart219 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %234) #5
  %call220 = call i32 @UTIL_getTime()
  store i32 %call220, i32* %clockStart219, align 4, !tbaa !8
  %235 = bitcast i32* %nbLoops221 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %235) #5
  store i32 0, i32* %nbLoops221, align 4, !tbaa !2
  br label %for.cond222

for.cond222:                                      ; preds = %for.inc249, %if.then218
  %236 = load i32, i32* %nbLoops221, align 4, !tbaa !2
  %237 = load i32, i32* %nbDecodeLoops, align 4, !tbaa !2
  %cmp223 = icmp ult i32 %236, %237
  br i1 %cmp223, label %for.body225, label %for.end251

for.body225:                                      ; preds = %for.cond222
  %238 = bitcast i32* %blockNb226 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %238) #5
  store i32 0, i32* %blockNb226, align 4, !tbaa !2
  br label %for.cond227

for.cond227:                                      ; preds = %for.inc246, %for.body225
  %239 = load i32, i32* %blockNb226, align 4, !tbaa !2
  %240 = load i32, i32* %nbBlocks, align 4, !tbaa !2
  %cmp228 = icmp ult i32 %239, %240
  br i1 %cmp228, label %for.body230, label %for.end248

for.body230:                                      ; preds = %for.cond227
  %241 = bitcast i32* %regenSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %241) #5
  %242 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %243 = load i32, i32* %blockNb226, align 4, !tbaa !2
  %arrayidx231 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %242, i32 %243
  %cPtr232 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %arrayidx231, i32 0, i32 2
  %244 = load i8*, i8** %cPtr232, align 4, !tbaa !22
  %245 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %246 = load i32, i32* %blockNb226, align 4, !tbaa !2
  %arrayidx233 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %245, i32 %246
  %resPtr234 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %arrayidx233, i32 0, i32 5
  %247 = load i8*, i8** %resPtr234, align 4, !tbaa !23
  %248 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %249 = load i32, i32* %blockNb226, align 4, !tbaa !2
  %arrayidx235 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %248, i32 %249
  %cSize236 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %arrayidx235, i32 0, i32 4
  %250 = load i32, i32* %cSize236, align 4, !tbaa !30
  %251 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %252 = load i32, i32* %blockNb226, align 4, !tbaa !2
  %arrayidx237 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %251, i32 %252
  %srcSize238 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %arrayidx237, i32 0, i32 1
  %253 = load i32, i32* %srcSize238, align 4, !tbaa !24
  %254 = load i8*, i8** %dictBuf.addr, align 4, !tbaa !6
  %255 = load i32, i32* %dictSize.addr, align 4, !tbaa !2
  %call239 = call i32 @LZ4_decompress_safe_usingDict(i8* %244, i8* %247, i32 %250, i32 %253, i8* %254, i32 %255)
  store i32 %call239, i32* %regenSize, align 4, !tbaa !2
  %256 = load i32, i32* %regenSize, align 4, !tbaa !2
  %cmp240 = icmp slt i32 %256, 0
  br i1 %cmp240, label %if.then242, label %if.end244

if.then242:                                       ; preds = %for.body230
  %257 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %258 = load i32, i32* %blockNb226, align 4, !tbaa !2
  %call243 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %257, i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.36, i32 0, i32 0), i32 %258)
  store i32 22, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end244:                                        ; preds = %for.body230
  %259 = load i32, i32* %regenSize, align 4, !tbaa !2
  %260 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %261 = load i32, i32* %blockNb226, align 4, !tbaa !2
  %arrayidx245 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %260, i32 %261
  %resSize = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %arrayidx245, i32 0, i32 6
  store i32 %259, i32* %resSize, align 4, !tbaa !31
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end244, %if.then242
  %262 = bitcast i32* %regenSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 22, label %for.end248
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc246

for.inc246:                                       ; preds = %cleanup.cont
  %263 = load i32, i32* %blockNb226, align 4, !tbaa !2
  %inc247 = add i32 %263, 1
  store i32 %inc247, i32* %blockNb226, align 4, !tbaa !2
  br label %for.cond227

for.end248:                                       ; preds = %cleanup, %for.cond227
  %264 = bitcast i32* %blockNb226 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %264) #5
  br label %for.inc249

for.inc249:                                       ; preds = %for.end248
  %265 = load i32, i32* %nbLoops221, align 4, !tbaa !2
  %inc250 = add i32 %265, 1
  store i32 %inc250, i32* %nbLoops221, align 4, !tbaa !2
  br label %for.cond222

for.end251:                                       ; preds = %for.cond222
  %266 = bitcast i64* %clockSpan252 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %266) #5
  %267 = load i32, i32* %clockStart219, align 4, !tbaa !8
  %call253 = call i64 @UTIL_clockSpanNano(i32 %267)
  store i64 %call253, i64* %clockSpan252, align 8, !tbaa !10
  %268 = load i64, i64* %clockSpan252, align 8, !tbaa !10
  %cmp254 = icmp ugt i64 %268, 0
  br i1 %cmp254, label %if.then256, label %if.else273

if.then256:                                       ; preds = %for.end251
  %269 = load i64, i64* %clockSpan252, align 8, !tbaa !10
  %270 = load i64, i64* %fastestD, align 8, !tbaa !10
  %271 = load i32, i32* %nbDecodeLoops, align 4, !tbaa !2
  %conv257 = zext i32 %271 to i64
  %mul258 = mul i64 %270, %conv257
  %cmp259 = icmp ult i64 %269, %mul258
  br i1 %cmp259, label %if.then261, label %if.end264

if.then261:                                       ; preds = %if.then256
  %272 = load i64, i64* %clockSpan252, align 8, !tbaa !10
  %273 = load i32, i32* %nbDecodeLoops, align 4, !tbaa !2
  %conv262 = zext i32 %273 to i64
  %div263 = udiv i64 %272, %conv262
  store i64 %div263, i64* %fastestD, align 8, !tbaa !10
  br label %if.end264

if.end264:                                        ; preds = %if.then261, %if.then256
  %274 = load i64, i64* %fastestD, align 8, !tbaa !10
  %cmp265 = icmp ugt i64 %274, 0
  br i1 %cmp265, label %lor.end268, label %lor.rhs267

lor.rhs267:                                       ; preds = %if.end264
  call void @__assert_fail(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.37, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.33, i32 0, i32 0), i32 465, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @__func__.BMK_benchMem, i32 0, i32 0)) #6
  unreachable

275:                                              ; No predecessors!
  br label %lor.end268

lor.end268:                                       ; preds = %275, %if.end264
  %276 = phi i1 [ true, %if.end264 ], [ false, %275 ]
  %lor.ext269 = zext i1 %276 to i32
  %277 = load i64, i64* %fastestD, align 8, !tbaa !10
  %div270 = udiv i64 1000000000, %277
  %conv271 = trunc i64 %div270 to i32
  %add272 = add i32 %conv271, 1
  store i32 %add272, i32* %nbDecodeLoops, align 4, !tbaa !2
  br label %if.end280

if.else273:                                       ; preds = %for.end251
  %278 = load i32, i32* %nbDecodeLoops, align 4, !tbaa !2
  %cmp274 = icmp ult i32 %278, 40000000
  br i1 %cmp274, label %lor.end277, label %lor.rhs276

lor.rhs276:                                       ; preds = %if.else273
  call void @__assert_fail(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.38, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.33, i32 0, i32 0), i32 468, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @__func__.BMK_benchMem, i32 0, i32 0)) #6
  unreachable

279:                                              ; No predecessors!
  br label %lor.end277

lor.end277:                                       ; preds = %279, %if.else273
  %280 = phi i1 [ true, %if.else273 ], [ false, %279 ]
  %lor.ext278 = zext i1 %280 to i32
  %281 = load i32, i32* %nbDecodeLoops, align 4, !tbaa !2
  %mul279 = mul i32 %281, 100
  store i32 %mul279, i32* %nbDecodeLoops, align 4, !tbaa !2
  br label %if.end280

if.end280:                                        ; preds = %lor.end277, %lor.end268
  %282 = load i64, i64* %clockSpan252, align 8, !tbaa !10
  %283 = load i64, i64* %totalDTime, align 8, !tbaa !10
  %add281 = add i64 %283, %282
  store i64 %add281, i64* %totalDTime, align 8, !tbaa !10
  %284 = load i64, i64* %totalDTime, align 8, !tbaa !10
  %285 = load i64, i64* %maxTime, align 8, !tbaa !10
  %mul282 = mul i64 1, %285
  %cmp283 = icmp ugt i64 %284, %mul282
  %conv284 = zext i1 %cmp283 to i32
  store i32 %conv284, i32* %dCompleted, align 4, !tbaa !2
  %286 = bitcast i64* %clockSpan252 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %286) #5
  %287 = bitcast i32* %nbLoops221 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %287) #5
  %288 = bitcast i32* %clockStart219 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %288) #5
  br label %if.end285

if.end285:                                        ; preds = %if.end280, %if.end212
  %289 = load i32, i32* %markNb, align 4, !tbaa !2
  %add286 = add i32 %289, 1
  %rem287 = urem i32 %add286, 4
  store i32 %rem287, i32* %markNb, align 4, !tbaa !2
  %290 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp288 = icmp uge i32 %290, 2
  br i1 %cmp288, label %if.then290, label %if.end301

if.then290:                                       ; preds = %if.end285
  %291 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %292 = load i32, i32* %markNb, align 4, !tbaa !2
  %arrayidx291 = getelementptr inbounds [4 x i8*], [4 x i8*]* %marks, i32 0, i32 %292
  %293 = load i8*, i8** %arrayidx291, align 4, !tbaa !6
  %294 = load i8*, i8** %displayName.addr, align 4, !tbaa !6
  %295 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  %296 = load i32, i32* %cSize, align 4, !tbaa !8
  %297 = load double, double* %ratio, align 8, !tbaa !12
  %298 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  %conv292 = uitofp i32 %298 to double
  %299 = load i64, i64* %fastestC, align 8, !tbaa !10
  %conv293 = uitofp i64 %299 to double
  %div294 = fdiv double %conv292, %conv293
  %mul295 = fmul double %div294, 1.000000e+03
  %300 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  %conv296 = uitofp i32 %300 to double
  %301 = load i64, i64* %fastestD, align 8, !tbaa !10
  %conv297 = uitofp i64 %301 to double
  %div298 = fdiv double %conv296, %conv297
  %mul299 = fmul double %div298, 1.000000e+03
  %call300 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %291, i8* getelementptr inbounds ([58 x i8], [58 x i8]* @.str.39, i32 0, i32 0), i8* %293, i8* %294, i32 %295, i32 %296, double %297, double %mul295, double %mul299)
  br label %if.end301

if.end301:                                        ; preds = %if.then290, %if.end285
  %302 = bitcast i64* %crcCheck to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %302) #5
  %303 = load i8*, i8** %resultBuffer, align 4, !tbaa !6
  %304 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  %call302 = call i64 @LZ4_XXH64(i8* %303, i32 %304, i64 0)
  store i64 %call302, i64* %crcCheck, align 8, !tbaa !10
  %305 = load i64, i64* %crcOrig, align 8, !tbaa !10
  %306 = load i64, i64* %crcCheck, align 8, !tbaa !10
  %cmp303 = icmp ne i64 %305, %306
  br i1 %cmp303, label %if.then305, label %if.end356

if.then305:                                       ; preds = %if.end301
  %307 = bitcast i32* %u to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %307) #5
  %308 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %309 = load i8*, i8** %displayName.addr, align 4, !tbaa !6
  %310 = load i64, i64* %crcOrig, align 8, !tbaa !10
  %conv306 = trunc i64 %310 to i32
  %311 = load i64, i64* %crcCheck, align 8, !tbaa !10
  %conv307 = trunc i64 %311 to i32
  %call308 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %308, i8* getelementptr inbounds ([56 x i8], [56 x i8]* @.str.40, i32 0, i32 0), i8* %309, i32 %conv306, i32 %conv307)
  store i32 0, i32* %u, align 4, !tbaa !8
  br label %for.cond309

for.cond309:                                      ; preds = %for.inc352, %if.then305
  %312 = load i32, i32* %u, align 4, !tbaa !8
  %313 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  %cmp310 = icmp ult i32 %312, %313
  br i1 %cmp310, label %for.body312, label %for.end354

for.body312:                                      ; preds = %for.cond309
  %314 = load i8*, i8** %srcBuffer.addr, align 4, !tbaa !6
  %315 = load i32, i32* %u, align 4, !tbaa !8
  %arrayidx313 = getelementptr inbounds i8, i8* %314, i32 %315
  %316 = load i8, i8* %arrayidx313, align 1, !tbaa !32
  %conv314 = zext i8 %316 to i32
  %317 = load i8*, i8** %resultBuffer, align 4, !tbaa !6
  %318 = load i32, i32* %u, align 4, !tbaa !8
  %arrayidx315 = getelementptr inbounds i8, i8* %317, i32 %318
  %319 = load i8, i8* %arrayidx315, align 1, !tbaa !32
  %conv316 = zext i8 %319 to i32
  %cmp317 = icmp ne i32 %conv314, %conv316
  br i1 %cmp317, label %if.then319, label %if.end345

if.then319:                                       ; preds = %for.body312
  %320 = bitcast i32* %segNb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %320) #5
  %321 = bitcast i32* %bNb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %321) #5
  %322 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %322) #5
  %323 = bitcast i32* %bacc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %323) #5
  store i32 0, i32* %bacc, align 4, !tbaa !8
  %324 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %325 = load i32, i32* %u, align 4, !tbaa !8
  %call320 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %324, i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.41, i32 0, i32 0), i32 %325)
  store i32 0, i32* %segNb, align 4, !tbaa !2
  br label %for.cond321

for.cond321:                                      ; preds = %for.inc335, %if.then319
  %326 = load i32, i32* %segNb, align 4, !tbaa !2
  %327 = load i32, i32* %nbBlocks, align 4, !tbaa !2
  %cmp322 = icmp ult i32 %326, %327
  br i1 %cmp322, label %for.body324, label %for.end337

for.body324:                                      ; preds = %for.cond321
  %328 = load i32, i32* %bacc, align 4, !tbaa !8
  %329 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %330 = load i32, i32* %segNb, align 4, !tbaa !2
  %arrayidx325 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %329, i32 %330
  %srcSize326 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %arrayidx325, i32 0, i32 1
  %331 = load i32, i32* %srcSize326, align 4, !tbaa !24
  %add327 = add i32 %328, %331
  %332 = load i32, i32* %u, align 4, !tbaa !8
  %cmp328 = icmp ugt i32 %add327, %332
  br i1 %cmp328, label %if.then330, label %if.end331

if.then330:                                       ; preds = %for.body324
  br label %for.end337

if.end331:                                        ; preds = %for.body324
  %333 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %334 = load i32, i32* %segNb, align 4, !tbaa !2
  %arrayidx332 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %333, i32 %334
  %srcSize333 = getelementptr inbounds %struct.blockParam_t, %struct.blockParam_t* %arrayidx332, i32 0, i32 1
  %335 = load i32, i32* %srcSize333, align 4, !tbaa !24
  %336 = load i32, i32* %bacc, align 4, !tbaa !8
  %add334 = add i32 %336, %335
  store i32 %add334, i32* %bacc, align 4, !tbaa !8
  br label %for.inc335

for.inc335:                                       ; preds = %if.end331
  %337 = load i32, i32* %segNb, align 4, !tbaa !2
  %inc336 = add i32 %337, 1
  store i32 %inc336, i32* %segNb, align 4, !tbaa !2
  br label %for.cond321

for.end337:                                       ; preds = %if.then330, %for.cond321
  %338 = load i32, i32* %u, align 4, !tbaa !8
  %339 = load i32, i32* %bacc, align 4, !tbaa !8
  %sub338 = sub i32 %338, %339
  store i32 %sub338, i32* %pos, align 4, !tbaa !2
  %340 = load i32, i32* %pos, align 4, !tbaa !2
  %div339 = udiv i32 %340, 131072
  store i32 %div339, i32* %bNb, align 4, !tbaa !2
  %341 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %342 = load i32, i32* %segNb, align 4, !tbaa !2
  %343 = load i32, i32* %bNb, align 4, !tbaa !2
  %344 = load i32, i32* %pos, align 4, !tbaa !2
  %call340 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %341, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.42, i32 0, i32 0), i32 %342, i32 %343, i32 %344)
  store i32 25, i32* %cleanup.dest.slot, align 4
  %345 = bitcast i32* %bacc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %345) #5
  %346 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %346) #5
  %347 = bitcast i32* %bNb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %347) #5
  %348 = bitcast i32* %segNb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %348) #5
  br label %for.end354

if.end345:                                        ; preds = %for.body312
  %349 = load i32, i32* %u, align 4, !tbaa !8
  %350 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  %sub346 = sub i32 %350, 1
  %cmp347 = icmp eq i32 %349, %sub346
  br i1 %cmp347, label %if.then349, label %if.end351

if.then349:                                       ; preds = %if.end345
  %351 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %call350 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %351, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.43, i32 0, i32 0))
  br label %if.end351

if.end351:                                        ; preds = %if.then349, %if.end345
  br label %for.inc352

for.inc352:                                       ; preds = %if.end351
  %352 = load i32, i32* %u, align 4, !tbaa !8
  %inc353 = add i32 %352, 1
  store i32 %inc353, i32* %u, align 4, !tbaa !8
  br label %for.cond309

for.end354:                                       ; preds = %for.end337, %for.cond309
  store i32 9, i32* %cleanup.dest.slot, align 4
  %353 = bitcast i32* %u to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %353) #5
  br label %cleanup357

if.end356:                                        ; preds = %if.end301
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup357

cleanup357:                                       ; preds = %if.end356, %for.end354
  %354 = bitcast i64* %crcCheck to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %354) #5
  %cleanup.dest358 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest358, label %unreachable [
    i32 0, label %cleanup.cont359
    i32 9, label %while.end
  ]

cleanup.cont359:                                  ; preds = %cleanup357
  br label %while.cond

while.end:                                        ; preds = %cleanup357, %lor.end
  %355 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp360 = icmp eq i32 %355, 1
  br i1 %cmp360, label %if.then362, label %if.end377

if.then362:                                       ; preds = %while.end
  %356 = bitcast double* %cSpeed to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %356) #5
  %357 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  %conv363 = uitofp i32 %357 to double
  %358 = load i64, i64* %fastestC, align 8, !tbaa !10
  %conv364 = uitofp i64 %358 to double
  %div365 = fdiv double %conv363, %conv364
  %mul366 = fmul double %div365, 1.000000e+03
  store double %mul366, double* %cSpeed, align 8, !tbaa !12
  %359 = bitcast double* %dSpeed to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %359) #5
  %360 = load i32, i32* %srcSize.addr, align 4, !tbaa !8
  %conv367 = uitofp i32 %360 to double
  %361 = load i64, i64* %fastestD, align 8, !tbaa !10
  %conv368 = uitofp i64 %361 to double
  %div369 = fdiv double %conv367, %conv368
  %mul370 = fmul double %div369, 1.000000e+03
  store double %mul370, double* %dSpeed, align 8, !tbaa !12
  %362 = load i32, i32* @g_additionalParam, align 4, !tbaa !2
  %tobool371 = icmp ne i32 %362, 0
  br i1 %tobool371, label %if.then372, label %if.else374

if.then372:                                       ; preds = %if.then362
  %363 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %364 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %365 = load i32, i32* %cSize, align 4, !tbaa !8
  %366 = load double, double* %ratio, align 8, !tbaa !12
  %367 = load double, double* %cSpeed, align 8, !tbaa !12
  %368 = load double, double* %dSpeed, align 8, !tbaa !12
  %369 = load i8*, i8** %displayName.addr, align 4, !tbaa !6
  %370 = load i32, i32* @g_additionalParam, align 4, !tbaa !2
  %call373 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %363, i8* getelementptr inbounds ([56 x i8], [56 x i8]* @.str.44, i32 0, i32 0), i32 %364, i32 %365, double %366, double %367, double %368, i8* %369, i32 %370)
  br label %if.end376

if.else374:                                       ; preds = %if.then362
  %371 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %372 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %373 = load i32, i32* %cSize, align 4, !tbaa !8
  %374 = load double, double* %ratio, align 8, !tbaa !12
  %375 = load double, double* %cSpeed, align 8, !tbaa !12
  %376 = load double, double* %dSpeed, align 8, !tbaa !12
  %377 = load i8*, i8** %displayName.addr, align 4, !tbaa !6
  %call375 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %371, i8* getelementptr inbounds ([45 x i8], [45 x i8]* @.str.45, i32 0, i32 0), i32 %372, i32 %373, double %374, double %375, double %376, i8* %377)
  br label %if.end376

if.end376:                                        ; preds = %if.else374, %if.then372
  %378 = bitcast double* %dSpeed to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %378) #5
  %379 = bitcast double* %cSpeed to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %379) #5
  br label %if.end377

if.end377:                                        ; preds = %if.end376, %while.end
  %380 = load i32, i32* @g_displayLevel, align 4, !tbaa !2
  %cmp378 = icmp uge i32 %380, 2
  br i1 %cmp378, label %if.then380, label %if.end382

if.then380:                                       ; preds = %if.end377
  %381 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !6
  %382 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %call381 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %381, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.46, i32 0, i32 0), i32 %382)
  br label %if.end382

if.end382:                                        ; preds = %if.then380, %if.end377
  %383 = bitcast double* %ratio to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %383) #5
  %384 = bitcast i32* %cSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %384) #5
  %385 = bitcast i32* %markNb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %385) #5
  %386 = bitcast [4 x i8*]* %marks to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %386) #5
  %387 = bitcast i32* %dCompleted to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %387) #5
  %388 = bitcast i32* %cCompleted to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %388) #5
  %389 = bitcast i64* %totalDTime to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %389) #5
  %390 = bitcast i64* %totalCTime to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %390) #5
  %391 = bitcast i32* %nbDecodeLoops to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %391) #5
  %392 = bitcast i32* %nbCompressionLoops to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %392) #5
  %393 = bitcast i64* %maxTime to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %393) #5
  %394 = bitcast i32* %coolTime to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %394) #5
  %395 = bitcast i64* %crcOrig to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %395) #5
  %396 = bitcast i64* %fastestD to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %396) #5
  %397 = bitcast i64* %fastestC to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %397) #5
  %cleanupFunction = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %compP, i32 0, i32 10
  %398 = load void (%struct.compressionParameters*)*, void (%struct.compressionParameters*)** %cleanupFunction, align 4, !tbaa !33
  call void %398(%struct.compressionParameters* %compP)
  %399 = load %struct.blockParam_t*, %struct.blockParam_t** %blockTable, align 4, !tbaa !6
  %400 = bitcast %struct.blockParam_t* %399 to i8*
  call void @free(i8* %400)
  %401 = load i8*, i8** %compressedBuffer, align 4, !tbaa !6
  call void @free(i8* %401)
  %402 = load i8*, i8** %resultBuffer, align 4, !tbaa !6
  call void @free(i8* %402)
  store i32 1, i32* %cleanup.dest.slot, align 4
  %403 = bitcast %struct.compressionParameters* %compP to i8*
  call void @llvm.lifetime.end.p0i8(i64 44, i8* %403) #5
  %404 = bitcast i32* %nbBlocks to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %404) #5
  %405 = bitcast i8** %resultBuffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %405) #5
  %406 = bitcast i8** %compressedBuffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %406) #5
  %407 = bitcast i32* %maxCompressedSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %407) #5
  %408 = bitcast %struct.blockParam_t** %blockTable to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %408) #5
  %409 = bitcast i32* %maxNbBlocks to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %409) #5
  %410 = bitcast i32* %blockSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %410) #5
  ret i32 0

unreachable:                                      ; preds = %cleanup357, %cleanup
  unreachable
}

declare i32 @LZ4_compressBound(i32) #1

declare i32 @strlen(i8*) #1

; Function Attrs: nounwind
define internal void @LZ4_buildCompressionParameters(%struct.compressionParameters* %pParams, i32 %cLevel, i8* %dictBuf, i32 %dictSize) #0 {
entry:
  %pParams.addr = alloca %struct.compressionParameters*, align 4
  %cLevel.addr = alloca i32, align 4
  %dictBuf.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  store %struct.compressionParameters* %pParams, %struct.compressionParameters** %pParams.addr, align 4, !tbaa !6
  store i32 %cLevel, i32* %cLevel.addr, align 4, !tbaa !2
  store i8* %dictBuf, i8** %dictBuf.addr, align 4, !tbaa !6
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !2
  %0 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %1 = load %struct.compressionParameters*, %struct.compressionParameters** %pParams.addr, align 4, !tbaa !6
  %cLevel1 = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %1, i32 0, i32 0
  store i32 %0, i32* %cLevel1, align 4, !tbaa !34
  %2 = load i8*, i8** %dictBuf.addr, align 4, !tbaa !6
  %3 = load %struct.compressionParameters*, %struct.compressionParameters** %pParams.addr, align 4, !tbaa !6
  %dictBuf2 = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %3, i32 0, i32 1
  store i8* %2, i8** %dictBuf2, align 4, !tbaa !35
  %4 = load i32, i32* %dictSize.addr, align 4, !tbaa !2
  %5 = load %struct.compressionParameters*, %struct.compressionParameters** %pParams.addr, align 4, !tbaa !6
  %dictSize3 = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %5, i32 0, i32 2
  store i32 %4, i32* %dictSize3, align 4, !tbaa !36
  %6 = load i32, i32* %dictSize.addr, align 4, !tbaa !2
  %tobool = icmp ne i32 %6, 0
  br i1 %tobool, label %if.then, label %if.else9

if.then:                                          ; preds = %entry
  %7 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %cmp = icmp slt i32 %7, 3
  br i1 %cmp, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.then
  %8 = load %struct.compressionParameters*, %struct.compressionParameters** %pParams.addr, align 4, !tbaa !6
  %initFunction = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %8, i32 0, i32 7
  store void (%struct.compressionParameters*)* @LZ4_compressInitStream, void (%struct.compressionParameters*)** %initFunction, align 4, !tbaa !18
  %9 = load %struct.compressionParameters*, %struct.compressionParameters** %pParams.addr, align 4, !tbaa !6
  %resetFunction = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %9, i32 0, i32 8
  store void (%struct.compressionParameters*)* @LZ4_compressResetStream, void (%struct.compressionParameters*)** %resetFunction, align 4, !tbaa !28
  %10 = load %struct.compressionParameters*, %struct.compressionParameters** %pParams.addr, align 4, !tbaa !6
  %blockFunction = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %10, i32 0, i32 9
  store i32 (%struct.compressionParameters*, i8*, i8*, i32, i32)* @LZ4_compressBlockStream, i32 (%struct.compressionParameters*, i8*, i8*, i32, i32)** %blockFunction, align 4, !tbaa !29
  %11 = load %struct.compressionParameters*, %struct.compressionParameters** %pParams.addr, align 4, !tbaa !6
  %cleanupFunction = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %11, i32 0, i32 10
  store void (%struct.compressionParameters*)* @LZ4_compressCleanupStream, void (%struct.compressionParameters*)** %cleanupFunction, align 4, !tbaa !33
  br label %if.end

if.else:                                          ; preds = %if.then
  %12 = load %struct.compressionParameters*, %struct.compressionParameters** %pParams.addr, align 4, !tbaa !6
  %initFunction5 = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %12, i32 0, i32 7
  store void (%struct.compressionParameters*)* @LZ4_compressInitStreamHC, void (%struct.compressionParameters*)** %initFunction5, align 4, !tbaa !18
  %13 = load %struct.compressionParameters*, %struct.compressionParameters** %pParams.addr, align 4, !tbaa !6
  %resetFunction6 = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %13, i32 0, i32 8
  store void (%struct.compressionParameters*)* @LZ4_compressResetStreamHC, void (%struct.compressionParameters*)** %resetFunction6, align 4, !tbaa !28
  %14 = load %struct.compressionParameters*, %struct.compressionParameters** %pParams.addr, align 4, !tbaa !6
  %blockFunction7 = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %14, i32 0, i32 9
  store i32 (%struct.compressionParameters*, i8*, i8*, i32, i32)* @LZ4_compressBlockStreamHC, i32 (%struct.compressionParameters*, i8*, i8*, i32, i32)** %blockFunction7, align 4, !tbaa !29
  %15 = load %struct.compressionParameters*, %struct.compressionParameters** %pParams.addr, align 4, !tbaa !6
  %cleanupFunction8 = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %15, i32 0, i32 10
  store void (%struct.compressionParameters*)* @LZ4_compressCleanupStreamHC, void (%struct.compressionParameters*)** %cleanupFunction8, align 4, !tbaa !33
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then4
  br label %if.end19

if.else9:                                         ; preds = %entry
  %16 = load %struct.compressionParameters*, %struct.compressionParameters** %pParams.addr, align 4, !tbaa !6
  %initFunction10 = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %16, i32 0, i32 7
  store void (%struct.compressionParameters*)* @LZ4_compressInitNoStream, void (%struct.compressionParameters*)** %initFunction10, align 4, !tbaa !18
  %17 = load %struct.compressionParameters*, %struct.compressionParameters** %pParams.addr, align 4, !tbaa !6
  %resetFunction11 = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %17, i32 0, i32 8
  store void (%struct.compressionParameters*)* @LZ4_compressResetNoStream, void (%struct.compressionParameters*)** %resetFunction11, align 4, !tbaa !28
  %18 = load %struct.compressionParameters*, %struct.compressionParameters** %pParams.addr, align 4, !tbaa !6
  %cleanupFunction12 = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %18, i32 0, i32 10
  store void (%struct.compressionParameters*)* @LZ4_compressCleanupNoStream, void (%struct.compressionParameters*)** %cleanupFunction12, align 4, !tbaa !33
  %19 = load i32, i32* %cLevel.addr, align 4, !tbaa !2
  %cmp13 = icmp slt i32 %19, 3
  br i1 %cmp13, label %if.then14, label %if.else16

if.then14:                                        ; preds = %if.else9
  %20 = load %struct.compressionParameters*, %struct.compressionParameters** %pParams.addr, align 4, !tbaa !6
  %blockFunction15 = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %20, i32 0, i32 9
  store i32 (%struct.compressionParameters*, i8*, i8*, i32, i32)* @LZ4_compressBlockNoStream, i32 (%struct.compressionParameters*, i8*, i8*, i32, i32)** %blockFunction15, align 4, !tbaa !29
  br label %if.end18

if.else16:                                        ; preds = %if.else9
  %21 = load %struct.compressionParameters*, %struct.compressionParameters** %pParams.addr, align 4, !tbaa !6
  %blockFunction17 = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %21, i32 0, i32 9
  store i32 (%struct.compressionParameters*, i8*, i8*, i32, i32)* @LZ4_compressBlockNoStreamHC, i32 (%struct.compressionParameters*, i8*, i8*, i32, i32)** %blockFunction17, align 4, !tbaa !29
  br label %if.end18

if.end18:                                         ; preds = %if.else16, %if.then14
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %if.end
  ret void
}

declare void @RDG_genBuffer(i8*, i32, double, double, i32) #1

declare i64 @LZ4_XXH64(i8*, i32, i64) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: nounwind
define internal i32 @UTIL_getTime() #0 {
entry:
  %call = call i32 @clock()
  ret i32 %call
}

; Function Attrs: nounwind
define internal i64 @UTIL_clockSpanMicro(i32 %clockStart) #0 {
entry:
  %clockStart.addr = alloca i32, align 4
  %clockEnd = alloca i32, align 4
  store i32 %clockStart, i32* %clockStart.addr, align 4, !tbaa !8
  %0 = bitcast i32* %clockEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %call = call i32 @UTIL_getTime()
  store i32 %call, i32* %clockEnd, align 4, !tbaa !8
  %1 = load i32, i32* %clockStart.addr, align 4, !tbaa !8
  %2 = load i32, i32* %clockEnd, align 4, !tbaa !8
  %call1 = call i64 @UTIL_getSpanTimeMicro(i32 %1, i32 %2)
  %3 = bitcast i32* %clockEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #5
  ret i64 %call1
}

declare i32 @sleep(i32) #1

declare i32 @nanosleep(%struct.timespec*, %struct.timespec*) #1

; Function Attrs: nounwind
define internal void @UTIL_waitForNextTick() #0 {
entry:
  %clockStart = alloca i32, align 4
  %clockEnd = alloca i32, align 4
  %0 = bitcast i32* %clockStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %call = call i32 @UTIL_getTime()
  store i32 %call, i32* %clockStart, align 4, !tbaa !8
  %1 = bitcast i32* %clockEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  %call1 = call i32 @UTIL_getTime()
  store i32 %call1, i32* %clockEnd, align 4, !tbaa !8
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %2 = load i32, i32* %clockStart, align 4, !tbaa !8
  %3 = load i32, i32* %clockEnd, align 4, !tbaa !8
  %call2 = call i64 @UTIL_getSpanTimeNano(i32 %2, i32 %3)
  %cmp = icmp eq i64 %call2, 0
  br i1 %cmp, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %4 = bitcast i32* %clockEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #5
  %5 = bitcast i32* %clockStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #5
  ret void
}

; Function Attrs: nounwind
define internal i64 @UTIL_clockSpanNano(i32 %clockStart) #0 {
entry:
  %clockStart.addr = alloca i32, align 4
  %clockEnd = alloca i32, align 4
  store i32 %clockStart, i32* %clockStart.addr, align 4, !tbaa !8
  %0 = bitcast i32* %clockEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %call = call i32 @UTIL_getTime()
  store i32 %call, i32* %clockEnd, align 4, !tbaa !8
  %1 = load i32, i32* %clockStart.addr, align 4, !tbaa !8
  %2 = load i32, i32* %clockEnd, align 4, !tbaa !8
  %call1 = call i64 @UTIL_getSpanTimeNano(i32 %1, i32 %2)
  %3 = bitcast i32* %clockEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #5
  ret i64 %call1
}

; Function Attrs: noreturn
declare void @__assert_fail(i8*, i8*, i32, i8*) #3

declare i32 @LZ4_decompress_safe_usingDict(i8*, i8*, i32, i32, i8*, i32) #1

; Function Attrs: nounwind
define internal void @LZ4_compressInitStream(%struct.compressionParameters* %pThis) #0 {
entry:
  %pThis.addr = alloca %struct.compressionParameters*, align 4
  store %struct.compressionParameters* %pThis, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %call = call %union.LZ4_stream_u* @LZ4_createStream()
  %0 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_stream = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %0, i32 0, i32 3
  store %union.LZ4_stream_u* %call, %union.LZ4_stream_u** %LZ4_stream, align 4, !tbaa !37
  %call1 = call %union.LZ4_stream_u* @LZ4_createStream()
  %1 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_dictStream = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %1, i32 0, i32 4
  store %union.LZ4_stream_u* %call1, %union.LZ4_stream_u** %LZ4_dictStream, align 4, !tbaa !38
  %2 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_streamHC = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %2, i32 0, i32 5
  store %union.LZ4_streamHC_u* null, %union.LZ4_streamHC_u** %LZ4_streamHC, align 4, !tbaa !39
  %3 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_dictStreamHC = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %3, i32 0, i32 6
  store %union.LZ4_streamHC_u* null, %union.LZ4_streamHC_u** %LZ4_dictStreamHC, align 4, !tbaa !40
  %4 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_dictStream2 = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %4, i32 0, i32 4
  %5 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %LZ4_dictStream2, align 4, !tbaa !38
  %6 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %dictBuf = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %6, i32 0, i32 1
  %7 = load i8*, i8** %dictBuf, align 4, !tbaa !35
  %8 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %dictSize = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %8, i32 0, i32 2
  %9 = load i32, i32* %dictSize, align 4, !tbaa !36
  %call3 = call i32 @LZ4_loadDict(%union.LZ4_stream_u* %5, i8* %7, i32 %9)
  ret void
}

; Function Attrs: nounwind
define internal void @LZ4_compressResetStream(%struct.compressionParameters* %pThis) #0 {
entry:
  %pThis.addr = alloca %struct.compressionParameters*, align 4
  store %struct.compressionParameters* %pThis, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %0 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_stream = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %0, i32 0, i32 3
  %1 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %LZ4_stream, align 4, !tbaa !37
  call void @LZ4_resetStream_fast(%union.LZ4_stream_u* %1)
  %2 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_stream1 = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %2, i32 0, i32 3
  %3 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %LZ4_stream1, align 4, !tbaa !37
  %4 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_dictStream = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %4, i32 0, i32 4
  %5 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %LZ4_dictStream, align 4, !tbaa !38
  call void @LZ4_attach_dictionary(%union.LZ4_stream_u* %3, %union.LZ4_stream_u* %5)
  ret void
}

; Function Attrs: nounwind
define internal i32 @LZ4_compressBlockStream(%struct.compressionParameters* %pThis, i8* %src, i8* %dst, i32 %srcSize, i32 %dstSize) #0 {
entry:
  %pThis.addr = alloca %struct.compressionParameters*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %dstSize.addr = alloca i32, align 4
  %acceleration = alloca i32, align 4
  store %struct.compressionParameters* %pThis, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  store i8* %src, i8** %src.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !6
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !2
  store i32 %dstSize, i32* %dstSize.addr, align 4, !tbaa !2
  %0 = bitcast i32* %acceleration to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %cLevel = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %1, i32 0, i32 0
  %2 = load i32, i32* %cLevel, align 4, !tbaa !34
  %cmp = icmp slt i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %3 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %cLevel1 = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %3, i32 0, i32 0
  %4 = load i32, i32* %cLevel1, align 4, !tbaa !34
  %sub = sub nsw i32 0, %4
  %add = add nsw i32 %sub, 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add, %cond.true ], [ 1, %cond.false ]
  store i32 %cond, i32* %acceleration, align 4, !tbaa !2
  %5 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_stream = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %5, i32 0, i32 3
  %6 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %LZ4_stream, align 4, !tbaa !37
  %7 = load i8*, i8** %src.addr, align 4, !tbaa !6
  %8 = load i8*, i8** %dst.addr, align 4, !tbaa !6
  %9 = load i32, i32* %srcSize.addr, align 4, !tbaa !2
  %10 = load i32, i32* %dstSize.addr, align 4, !tbaa !2
  %11 = load i32, i32* %acceleration, align 4, !tbaa !2
  %call = call i32 @LZ4_compress_fast_continue(%union.LZ4_stream_u* %6, i8* %7, i8* %8, i32 %9, i32 %10, i32 %11)
  %12 = bitcast i32* %acceleration to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #5
  ret i32 %call
}

; Function Attrs: nounwind
define internal void @LZ4_compressCleanupStream(%struct.compressionParameters* %pThis) #0 {
entry:
  %pThis.addr = alloca %struct.compressionParameters*, align 4
  store %struct.compressionParameters* %pThis, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %0 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_stream = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %0, i32 0, i32 3
  %1 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %LZ4_stream, align 4, !tbaa !37
  %call = call i32 @LZ4_freeStream(%union.LZ4_stream_u* %1)
  %2 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_dictStream = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %2, i32 0, i32 4
  %3 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %LZ4_dictStream, align 4, !tbaa !38
  %call1 = call i32 @LZ4_freeStream(%union.LZ4_stream_u* %3)
  ret void
}

; Function Attrs: nounwind
define internal void @LZ4_compressInitStreamHC(%struct.compressionParameters* %pThis) #0 {
entry:
  %pThis.addr = alloca %struct.compressionParameters*, align 4
  store %struct.compressionParameters* %pThis, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %0 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_stream = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %0, i32 0, i32 3
  store %union.LZ4_stream_u* null, %union.LZ4_stream_u** %LZ4_stream, align 4, !tbaa !37
  %1 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_dictStream = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %1, i32 0, i32 4
  store %union.LZ4_stream_u* null, %union.LZ4_stream_u** %LZ4_dictStream, align 4, !tbaa !38
  %call = call %union.LZ4_streamHC_u* @LZ4_createStreamHC()
  %2 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_streamHC = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %2, i32 0, i32 5
  store %union.LZ4_streamHC_u* %call, %union.LZ4_streamHC_u** %LZ4_streamHC, align 4, !tbaa !39
  %call1 = call %union.LZ4_streamHC_u* @LZ4_createStreamHC()
  %3 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_dictStreamHC = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %3, i32 0, i32 6
  store %union.LZ4_streamHC_u* %call1, %union.LZ4_streamHC_u** %LZ4_dictStreamHC, align 4, !tbaa !40
  %4 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_dictStreamHC2 = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %4, i32 0, i32 6
  %5 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_dictStreamHC2, align 4, !tbaa !40
  %6 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %dictBuf = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %6, i32 0, i32 1
  %7 = load i8*, i8** %dictBuf, align 4, !tbaa !35
  %8 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %dictSize = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %8, i32 0, i32 2
  %9 = load i32, i32* %dictSize, align 4, !tbaa !36
  %call3 = call i32 @LZ4_loadDictHC(%union.LZ4_streamHC_u* %5, i8* %7, i32 %9)
  ret void
}

; Function Attrs: nounwind
define internal void @LZ4_compressResetStreamHC(%struct.compressionParameters* %pThis) #0 {
entry:
  %pThis.addr = alloca %struct.compressionParameters*, align 4
  store %struct.compressionParameters* %pThis, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %0 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_streamHC = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %0, i32 0, i32 5
  %1 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHC, align 4, !tbaa !39
  %2 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %cLevel = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %2, i32 0, i32 0
  %3 = load i32, i32* %cLevel, align 4, !tbaa !34
  call void @LZ4_resetStreamHC_fast(%union.LZ4_streamHC_u* %1, i32 %3)
  %4 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_streamHC1 = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %4, i32 0, i32 5
  %5 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHC1, align 4, !tbaa !39
  %6 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_dictStreamHC = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %6, i32 0, i32 6
  %7 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_dictStreamHC, align 4, !tbaa !40
  call void @LZ4_attach_HC_dictionary(%union.LZ4_streamHC_u* %5, %union.LZ4_streamHC_u* %7)
  ret void
}

; Function Attrs: nounwind
define internal i32 @LZ4_compressBlockStreamHC(%struct.compressionParameters* %pThis, i8* %src, i8* %dst, i32 %srcSize, i32 %dstSize) #0 {
entry:
  %pThis.addr = alloca %struct.compressionParameters*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %dstSize.addr = alloca i32, align 4
  store %struct.compressionParameters* %pThis, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  store i8* %src, i8** %src.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !6
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !2
  store i32 %dstSize, i32* %dstSize.addr, align 4, !tbaa !2
  %0 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_streamHC = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %0, i32 0, i32 5
  %1 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHC, align 4, !tbaa !39
  %2 = load i8*, i8** %src.addr, align 4, !tbaa !6
  %3 = load i8*, i8** %dst.addr, align 4, !tbaa !6
  %4 = load i32, i32* %srcSize.addr, align 4, !tbaa !2
  %5 = load i32, i32* %dstSize.addr, align 4, !tbaa !2
  %call = call i32 @LZ4_compress_HC_continue(%union.LZ4_streamHC_u* %1, i8* %2, i8* %3, i32 %4, i32 %5)
  ret i32 %call
}

; Function Attrs: nounwind
define internal void @LZ4_compressCleanupStreamHC(%struct.compressionParameters* %pThis) #0 {
entry:
  %pThis.addr = alloca %struct.compressionParameters*, align 4
  store %struct.compressionParameters* %pThis, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %0 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_streamHC = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %0, i32 0, i32 5
  %1 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_streamHC, align 4, !tbaa !39
  %call = call i32 @LZ4_freeStreamHC(%union.LZ4_streamHC_u* %1)
  %2 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_dictStreamHC = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %2, i32 0, i32 6
  %3 = load %union.LZ4_streamHC_u*, %union.LZ4_streamHC_u** %LZ4_dictStreamHC, align 4, !tbaa !40
  %call1 = call i32 @LZ4_freeStreamHC(%union.LZ4_streamHC_u* %3)
  ret void
}

; Function Attrs: nounwind
define internal void @LZ4_compressInitNoStream(%struct.compressionParameters* %pThis) #0 {
entry:
  %pThis.addr = alloca %struct.compressionParameters*, align 4
  store %struct.compressionParameters* %pThis, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %0 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_stream = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %0, i32 0, i32 3
  store %union.LZ4_stream_u* null, %union.LZ4_stream_u** %LZ4_stream, align 4, !tbaa !37
  %1 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_dictStream = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %1, i32 0, i32 4
  store %union.LZ4_stream_u* null, %union.LZ4_stream_u** %LZ4_dictStream, align 4, !tbaa !38
  %2 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_streamHC = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %2, i32 0, i32 5
  store %union.LZ4_streamHC_u* null, %union.LZ4_streamHC_u** %LZ4_streamHC, align 4, !tbaa !39
  %3 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %LZ4_dictStreamHC = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %3, i32 0, i32 6
  store %union.LZ4_streamHC_u* null, %union.LZ4_streamHC_u** %LZ4_dictStreamHC, align 4, !tbaa !40
  ret void
}

; Function Attrs: nounwind
define internal void @LZ4_compressResetNoStream(%struct.compressionParameters* %pThis) #0 {
entry:
  %pThis.addr = alloca %struct.compressionParameters*, align 4
  store %struct.compressionParameters* %pThis, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %0 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
define internal void @LZ4_compressCleanupNoStream(%struct.compressionParameters* %pThis) #0 {
entry:
  %pThis.addr = alloca %struct.compressionParameters*, align 4
  store %struct.compressionParameters* %pThis, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %0 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
define internal i32 @LZ4_compressBlockNoStream(%struct.compressionParameters* %pThis, i8* %src, i8* %dst, i32 %srcSize, i32 %dstSize) #0 {
entry:
  %pThis.addr = alloca %struct.compressionParameters*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %dstSize.addr = alloca i32, align 4
  %acceleration = alloca i32, align 4
  store %struct.compressionParameters* %pThis, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  store i8* %src, i8** %src.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !6
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !2
  store i32 %dstSize, i32* %dstSize.addr, align 4, !tbaa !2
  %0 = bitcast i32* %acceleration to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %cLevel = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %1, i32 0, i32 0
  %2 = load i32, i32* %cLevel, align 4, !tbaa !34
  %cmp = icmp slt i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %3 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %cLevel1 = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %3, i32 0, i32 0
  %4 = load i32, i32* %cLevel1, align 4, !tbaa !34
  %sub = sub nsw i32 0, %4
  %add = add nsw i32 %sub, 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add, %cond.true ], [ 1, %cond.false ]
  store i32 %cond, i32* %acceleration, align 4, !tbaa !2
  %5 = load i8*, i8** %src.addr, align 4, !tbaa !6
  %6 = load i8*, i8** %dst.addr, align 4, !tbaa !6
  %7 = load i32, i32* %srcSize.addr, align 4, !tbaa !2
  %8 = load i32, i32* %dstSize.addr, align 4, !tbaa !2
  %9 = load i32, i32* %acceleration, align 4, !tbaa !2
  %call = call i32 @LZ4_compress_fast(i8* %5, i8* %6, i32 %7, i32 %8, i32 %9)
  %10 = bitcast i32* %acceleration to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #5
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @LZ4_compressBlockNoStreamHC(%struct.compressionParameters* %pThis, i8* %src, i8* %dst, i32 %srcSize, i32 %dstSize) #0 {
entry:
  %pThis.addr = alloca %struct.compressionParameters*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %dstSize.addr = alloca i32, align 4
  store %struct.compressionParameters* %pThis, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  store i8* %src, i8** %src.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !6
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !2
  store i32 %dstSize, i32* %dstSize.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %src.addr, align 4, !tbaa !6
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !6
  %2 = load i32, i32* %srcSize.addr, align 4, !tbaa !2
  %3 = load i32, i32* %dstSize.addr, align 4, !tbaa !2
  %4 = load %struct.compressionParameters*, %struct.compressionParameters** %pThis.addr, align 4, !tbaa !6
  %cLevel = getelementptr inbounds %struct.compressionParameters, %struct.compressionParameters* %4, i32 0, i32 0
  %5 = load i32, i32* %cLevel, align 4, !tbaa !34
  %call = call i32 @LZ4_compress_HC(i8* %0, i8* %1, i32 %2, i32 %3, i32 %5)
  ret i32 %call
}

declare %union.LZ4_stream_u* @LZ4_createStream() #1

declare i32 @LZ4_loadDict(%union.LZ4_stream_u*, i8*, i32) #1

declare void @LZ4_resetStream_fast(%union.LZ4_stream_u*) #1

declare void @LZ4_attach_dictionary(%union.LZ4_stream_u*, %union.LZ4_stream_u*) #1

declare i32 @LZ4_compress_fast_continue(%union.LZ4_stream_u*, i8*, i8*, i32, i32, i32) #1

declare i32 @LZ4_freeStream(%union.LZ4_stream_u*) #1

declare %union.LZ4_streamHC_u* @LZ4_createStreamHC() #1

declare i32 @LZ4_loadDictHC(%union.LZ4_streamHC_u*, i8*, i32) #1

declare void @LZ4_resetStreamHC_fast(%union.LZ4_streamHC_u*, i32) #1

declare void @LZ4_attach_HC_dictionary(%union.LZ4_streamHC_u*, %union.LZ4_streamHC_u*) #1

declare i32 @LZ4_compress_HC_continue(%union.LZ4_streamHC_u*, i8*, i8*, i32, i32) #1

declare i32 @LZ4_freeStreamHC(%union.LZ4_streamHC_u*) #1

declare i32 @LZ4_compress_fast(i8*, i8*, i32, i32, i32) #1

declare i32 @LZ4_compress_HC(i8*, i8*, i32, i32, i32) #1

; Function Attrs: nounwind
define internal i64 @UTIL_getSpanTimeMicro(i32 %clockStart, i32 %clockEnd) #0 {
entry:
  %clockStart.addr = alloca i32, align 4
  %clockEnd.addr = alloca i32, align 4
  store i32 %clockStart, i32* %clockStart.addr, align 4, !tbaa !8
  store i32 %clockEnd, i32* %clockEnd.addr, align 4, !tbaa !8
  %0 = load i32, i32* %clockEnd.addr, align 4, !tbaa !8
  %1 = load i32, i32* %clockStart.addr, align 4, !tbaa !8
  %sub = sub nsw i32 %0, %1
  %conv = sext i32 %sub to i64
  %mul = mul i64 1000000, %conv
  %div = udiv i64 %mul, 1000000
  ret i64 %div
}

; Function Attrs: nounwind
define internal i64 @UTIL_getSpanTimeNano(i32 %clockStart, i32 %clockEnd) #0 {
entry:
  %clockStart.addr = alloca i32, align 4
  %clockEnd.addr = alloca i32, align 4
  store i32 %clockStart, i32* %clockStart.addr, align 4, !tbaa !8
  store i32 %clockEnd, i32* %clockEnd.addr, align 4, !tbaa !8
  %0 = load i32, i32* %clockEnd.addr, align 4, !tbaa !8
  %1 = load i32, i32* %clockStart.addr, align 4, !tbaa !8
  %sub = sub nsw i32 %0, %1
  %conv = sext i32 %sub to i64
  %mul = mul i64 1000000000, %conv
  %div = udiv i64 %mul, 1000000
  ret i64 %div
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { nounwind }
attributes #6 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"int", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"any pointer", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"long", !4, i64 0}
!10 = !{!11, !11, i64 0}
!11 = !{!"long long", !4, i64 0}
!12 = !{!13, !13, i64 0}
!13 = !{!"double", !4, i64 0}
!14 = !{!15, !3, i64 12}
!15 = !{!"stat", !3, i64 0, !3, i64 4, !9, i64 8, !3, i64 12, !9, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !11, i64 40, !9, i64 48, !3, i64 52, !16, i64 56, !16, i64 64, !16, i64 72, !11, i64 80}
!16 = !{!"timespec", !9, i64 0, !9, i64 4}
!17 = !{!15, !11, i64 40}
!18 = !{!19, !7, i64 28}
!19 = !{!"compressionParameters", !3, i64 0, !7, i64 4, !3, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40}
!20 = !{!21, !7, i64 0}
!21 = !{!"", !7, i64 0, !9, i64 4, !7, i64 8, !9, i64 12, !9, i64 16, !7, i64 20, !9, i64 24}
!22 = !{!21, !7, i64 8}
!23 = !{!21, !7, i64 20}
!24 = !{!21, !9, i64 4}
!25 = !{!21, !9, i64 12}
!26 = !{!16, !9, i64 0}
!27 = !{!16, !9, i64 4}
!28 = !{!19, !7, i64 32}
!29 = !{!19, !7, i64 36}
!30 = !{!21, !9, i64 16}
!31 = !{!21, !9, i64 24}
!32 = !{!4, !4, i64 0}
!33 = !{!19, !7, i64 40}
!34 = !{!19, !3, i64 0}
!35 = !{!19, !7, i64 4}
!36 = !{!19, !3, i64 8}
!37 = !{!19, !7, i64 12}
!38 = !{!19, !7, i64 16}
!39 = !{!19, !7, i64 20}
!40 = !{!19, !7, i64 24}
