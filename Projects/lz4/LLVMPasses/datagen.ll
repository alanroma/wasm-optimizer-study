; ModuleID = 'datagen.c'
source_filename = "datagen.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct._IO_FILE = type opaque

@stdout = external constant %struct._IO_FILE*, align 4
@.str = private unnamed_addr constant [9 x i8] c"u<LTSIZE\00", align 1
@.str.1 = private unnamed_addr constant [10 x i8] c"datagen.c\00", align 1
@__func__.RDG_fillLiteralDistrib = private unnamed_addr constant [23 x i8] c"RDG_fillLiteralDistrib\00", align 1

; Function Attrs: nounwind
define hidden void @RDG_genBlock(i8* %buffer, i32 %buffSize, i32 %prefixSize, double %matchProba, i8* %lt, i32* %seedPtr) #0 {
entry:
  %buffer.addr = alloca i8*, align 4
  %buffSize.addr = alloca i32, align 4
  %prefixSize.addr = alloca i32, align 4
  %matchProba.addr = alloca double, align 8
  %lt.addr = alloca i8*, align 4
  %seedPtr.addr = alloca i32*, align 4
  %buffPtr = alloca i8*, align 4
  %matchProba32 = alloca i32, align 4
  %pos = alloca i32, align 4
  %seed = alloca i32*, align 4
  %size0 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %match = alloca i32, align 4
  %d = alloca i32, align 4
  %length = alloca i32, align 4
  %offset = alloca i32, align 4
  %d60 = alloca i32, align 4
  %length61 = alloca i32, align 4
  store i8* %buffer, i8** %buffer.addr, align 4, !tbaa !2
  store i32 %buffSize, i32* %buffSize.addr, align 4, !tbaa !6
  store i32 %prefixSize, i32* %prefixSize.addr, align 4, !tbaa !6
  store double %matchProba, double* %matchProba.addr, align 8, !tbaa !8
  store i8* %lt, i8** %lt.addr, align 4, !tbaa !2
  store i32* %seedPtr, i32** %seedPtr.addr, align 4, !tbaa !2
  %0 = bitcast i8** %buffPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  store i8* %1, i8** %buffPtr, align 4, !tbaa !2
  %2 = bitcast i32* %matchProba32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load double, double* %matchProba.addr, align 8, !tbaa !8
  %mul = fmul double 3.276800e+04, %3
  %conv = fptoui double %mul to i32
  store i32 %conv, i32* %matchProba32, align 4, !tbaa !10
  %4 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load i32, i32* %prefixSize.addr, align 4, !tbaa !6
  store i32 %5, i32* %pos, align 4, !tbaa !6
  %6 = bitcast i32** %seed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load i32*, i32** %seedPtr.addr, align 4, !tbaa !2
  store i32* %7, i32** %seed, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %entry
  %8 = load double, double* %matchProba.addr, align 8, !tbaa !8
  %cmp = fcmp oge double %8, 1.000000e+00
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = bitcast i32* %size0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load i32*, i32** %seed, align 4, !tbaa !2
  %call = call i32 @RDG_rand(i32* %10)
  %and = and i32 %call, 3
  store i32 %and, i32* %size0, align 4, !tbaa !6
  %11 = load i32, i32* %size0, align 4, !tbaa !6
  %mul2 = mul i32 %11, 2
  %add = add i32 16, %mul2
  %shl = shl i32 1, %add
  store i32 %shl, i32* %size0, align 4, !tbaa !6
  %12 = load i32*, i32** %seed, align 4, !tbaa !2
  %call3 = call i32 @RDG_rand(i32* %12)
  %13 = load i32, i32* %size0, align 4, !tbaa !6
  %sub = sub i32 %13, 1
  %and4 = and i32 %call3, %sub
  %14 = load i32, i32* %size0, align 4, !tbaa !6
  %add5 = add i32 %14, %and4
  store i32 %add5, i32* %size0, align 4, !tbaa !6
  %15 = load i32, i32* %buffSize.addr, align 4, !tbaa !6
  %16 = load i32, i32* %pos, align 4, !tbaa !6
  %17 = load i32, i32* %size0, align 4, !tbaa !6
  %add6 = add i32 %16, %17
  %cmp7 = icmp ult i32 %15, %add6
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %18 = load i8*, i8** %buffPtr, align 4, !tbaa !2
  %19 = load i32, i32* %pos, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %18, i32 %19
  %20 = load i32, i32* %buffSize.addr, align 4, !tbaa !6
  %21 = load i32, i32* %pos, align 4, !tbaa !6
  %sub9 = sub i32 %20, %21
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr, i8 0, i32 %sub9, i1 false)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %while.body
  %22 = load i8*, i8** %buffPtr, align 4, !tbaa !2
  %23 = load i32, i32* %pos, align 4, !tbaa !6
  %add.ptr10 = getelementptr inbounds i8, i8* %22, i32 %23
  %24 = load i32, i32* %size0, align 4, !tbaa !6
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr10, i8 0, i32 %24, i1 false)
  %25 = load i32, i32* %size0, align 4, !tbaa !6
  %26 = load i32, i32* %pos, align 4, !tbaa !6
  %add11 = add i32 %26, %25
  store i32 %add11, i32* %pos, align 4, !tbaa !6
  %27 = load i32*, i32** %seed, align 4, !tbaa !2
  %28 = load i8*, i8** %lt.addr, align 4, !tbaa !2
  %call12 = call zeroext i8 @RDG_genChar(i32* %27, i8* %28)
  %29 = load i8*, i8** %buffPtr, align 4, !tbaa !2
  %30 = load i32, i32* %pos, align 4, !tbaa !6
  %sub13 = sub i32 %30, 1
  %arrayidx = getelementptr inbounds i8, i8* %29, i32 %sub13
  store i8 %call12, i8* %arrayidx, align 1, !tbaa !12
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %31 = bitcast i32* %size0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup90 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %32 = load i32, i32* %pos, align 4, !tbaa !6
  %cmp14 = icmp eq i32 %32, 0
  br i1 %cmp14, label %if.then16, label %if.end19

if.then16:                                        ; preds = %while.end
  %33 = load i32*, i32** %seed, align 4, !tbaa !2
  %34 = load i8*, i8** %lt.addr, align 4, !tbaa !2
  %call17 = call zeroext i8 @RDG_genChar(i32* %33, i8* %34)
  %35 = load i8*, i8** %buffPtr, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds i8, i8* %35, i32 0
  store i8 %call17, i8* %arrayidx18, align 1, !tbaa !12
  store i32 1, i32* %pos, align 4, !tbaa !6
  br label %if.end19

if.end19:                                         ; preds = %if.then16, %while.end
  br label %while.cond20

while.cond20:                                     ; preds = %if.end88, %if.end19
  %36 = load i32, i32* %pos, align 4, !tbaa !6
  %37 = load i32, i32* %buffSize.addr, align 4, !tbaa !6
  %cmp21 = icmp ult i32 %36, %37
  br i1 %cmp21, label %while.body23, label %while.end89

while.body23:                                     ; preds = %while.cond20
  %38 = load i32*, i32** %seed, align 4, !tbaa !2
  %call24 = call i32 @RDG_rand(i32* %38)
  %shr = lshr i32 %call24, 3
  %and25 = and i32 %shr, 32767
  %39 = load i32, i32* %matchProba32, align 4, !tbaa !10
  %cmp26 = icmp ult i32 %and25, %39
  br i1 %cmp26, label %if.then28, label %if.else

if.then28:                                        ; preds = %while.body23
  %40 = bitcast i32* %match to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #5
  %41 = bitcast i32* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #5
  %42 = bitcast i32* %length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #5
  %43 = load i32*, i32** %seed, align 4, !tbaa !2
  %call29 = call i32 @RDG_rand(i32* %43)
  %shr30 = lshr i32 %call29, 7
  %and31 = and i32 %shr30, 7
  %tobool = icmp ne i32 %and31, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then28
  %44 = load i32*, i32** %seed, align 4, !tbaa !2
  %call32 = call i32 @RDG_rand(i32* %44)
  %and33 = and i32 %call32, 15
  br label %cond.end

cond.false:                                       ; preds = %if.then28
  %45 = load i32*, i32** %seed, align 4, !tbaa !2
  %call34 = call i32 @RDG_rand(i32* %45)
  %and35 = and i32 %call34, 511
  %add36 = add i32 %and35, 15
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %and33, %cond.true ], [ %add36, %cond.false ]
  %add37 = add i32 %cond, 4
  store i32 %add37, i32* %length, align 4, !tbaa !10
  %46 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #5
  %47 = load i32*, i32** %seed, align 4, !tbaa !2
  %call38 = call i32 @RDG_rand(i32* %47)
  %shr39 = lshr i32 %call38, 3
  %and40 = and i32 %shr39, 32767
  %add41 = add i32 %and40, 1
  store i32 %add41, i32* %offset, align 4, !tbaa !10
  %48 = load i32, i32* %offset, align 4, !tbaa !10
  %49 = load i32, i32* %pos, align 4, !tbaa !6
  %cmp42 = icmp ugt i32 %48, %49
  br i1 %cmp42, label %if.then44, label %if.end45

if.then44:                                        ; preds = %cond.end
  %50 = load i32, i32* %pos, align 4, !tbaa !6
  store i32 %50, i32* %offset, align 4, !tbaa !10
  br label %if.end45

if.end45:                                         ; preds = %if.then44, %cond.end
  %51 = load i32, i32* %pos, align 4, !tbaa !6
  %52 = load i32, i32* %offset, align 4, !tbaa !10
  %sub46 = sub i32 %51, %52
  store i32 %sub46, i32* %match, align 4, !tbaa !6
  %53 = load i32, i32* %pos, align 4, !tbaa !6
  %54 = load i32, i32* %length, align 4, !tbaa !10
  %add47 = add i32 %53, %54
  store i32 %add47, i32* %d, align 4, !tbaa !6
  %55 = load i32, i32* %d, align 4, !tbaa !6
  %56 = load i32, i32* %buffSize.addr, align 4, !tbaa !6
  %cmp48 = icmp ugt i32 %55, %56
  br i1 %cmp48, label %if.then50, label %if.end51

if.then50:                                        ; preds = %if.end45
  %57 = load i32, i32* %buffSize.addr, align 4, !tbaa !6
  store i32 %57, i32* %d, align 4, !tbaa !6
  br label %if.end51

if.end51:                                         ; preds = %if.then50, %if.end45
  br label %while.cond52

while.cond52:                                     ; preds = %while.body55, %if.end51
  %58 = load i32, i32* %pos, align 4, !tbaa !6
  %59 = load i32, i32* %d, align 4, !tbaa !6
  %cmp53 = icmp ult i32 %58, %59
  br i1 %cmp53, label %while.body55, label %while.end59

while.body55:                                     ; preds = %while.cond52
  %60 = load i8*, i8** %buffPtr, align 4, !tbaa !2
  %61 = load i32, i32* %match, align 4, !tbaa !6
  %inc = add i32 %61, 1
  store i32 %inc, i32* %match, align 4, !tbaa !6
  %arrayidx56 = getelementptr inbounds i8, i8* %60, i32 %61
  %62 = load i8, i8* %arrayidx56, align 1, !tbaa !12
  %63 = load i8*, i8** %buffPtr, align 4, !tbaa !2
  %64 = load i32, i32* %pos, align 4, !tbaa !6
  %inc57 = add i32 %64, 1
  store i32 %inc57, i32* %pos, align 4, !tbaa !6
  %arrayidx58 = getelementptr inbounds i8, i8* %63, i32 %64
  store i8 %62, i8* %arrayidx58, align 1, !tbaa !12
  br label %while.cond52

while.end59:                                      ; preds = %while.cond52
  %65 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #5
  %66 = bitcast i32* %length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #5
  %67 = bitcast i32* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #5
  %68 = bitcast i32* %match to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #5
  br label %if.end88

if.else:                                          ; preds = %while.body23
  %69 = bitcast i32* %d60 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #5
  %70 = bitcast i32* %length61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #5
  %71 = load i32*, i32** %seed, align 4, !tbaa !2
  %call62 = call i32 @RDG_rand(i32* %71)
  %shr63 = lshr i32 %call62, 7
  %and64 = and i32 %shr63, 7
  %tobool65 = icmp ne i32 %and64, 0
  br i1 %tobool65, label %cond.true66, label %cond.false69

cond.true66:                                      ; preds = %if.else
  %72 = load i32*, i32** %seed, align 4, !tbaa !2
  %call67 = call i32 @RDG_rand(i32* %72)
  %and68 = and i32 %call67, 15
  br label %cond.end73

cond.false69:                                     ; preds = %if.else
  %73 = load i32*, i32** %seed, align 4, !tbaa !2
  %call70 = call i32 @RDG_rand(i32* %73)
  %and71 = and i32 %call70, 511
  %add72 = add i32 %and71, 15
  br label %cond.end73

cond.end73:                                       ; preds = %cond.false69, %cond.true66
  %cond74 = phi i32 [ %and68, %cond.true66 ], [ %add72, %cond.false69 ]
  store i32 %cond74, i32* %length61, align 4, !tbaa !6
  %74 = load i32, i32* %pos, align 4, !tbaa !6
  %75 = load i32, i32* %length61, align 4, !tbaa !6
  %add75 = add i32 %74, %75
  store i32 %add75, i32* %d60, align 4, !tbaa !6
  %76 = load i32, i32* %d60, align 4, !tbaa !6
  %77 = load i32, i32* %buffSize.addr, align 4, !tbaa !6
  %cmp76 = icmp ugt i32 %76, %77
  br i1 %cmp76, label %if.then78, label %if.end79

if.then78:                                        ; preds = %cond.end73
  %78 = load i32, i32* %buffSize.addr, align 4, !tbaa !6
  store i32 %78, i32* %d60, align 4, !tbaa !6
  br label %if.end79

if.end79:                                         ; preds = %if.then78, %cond.end73
  br label %while.cond80

while.cond80:                                     ; preds = %while.body83, %if.end79
  %79 = load i32, i32* %pos, align 4, !tbaa !6
  %80 = load i32, i32* %d60, align 4, !tbaa !6
  %cmp81 = icmp ult i32 %79, %80
  br i1 %cmp81, label %while.body83, label %while.end87

while.body83:                                     ; preds = %while.cond80
  %81 = load i32*, i32** %seed, align 4, !tbaa !2
  %82 = load i8*, i8** %lt.addr, align 4, !tbaa !2
  %call84 = call zeroext i8 @RDG_genChar(i32* %81, i8* %82)
  %83 = load i8*, i8** %buffPtr, align 4, !tbaa !2
  %84 = load i32, i32* %pos, align 4, !tbaa !6
  %inc85 = add i32 %84, 1
  store i32 %inc85, i32* %pos, align 4, !tbaa !6
  %arrayidx86 = getelementptr inbounds i8, i8* %83, i32 %84
  store i8 %call84, i8* %arrayidx86, align 1, !tbaa !12
  br label %while.cond80

while.end87:                                      ; preds = %while.cond80
  %85 = bitcast i32* %length61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #5
  %86 = bitcast i32* %d60 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #5
  br label %if.end88

if.end88:                                         ; preds = %while.end87, %while.end59
  br label %while.cond20

while.end89:                                      ; preds = %while.cond20
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup90

cleanup90:                                        ; preds = %while.end89, %cleanup
  %87 = bitcast i32** %seed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #5
  %88 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #5
  %89 = bitcast i32* %matchProba32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #5
  %90 = bitcast i8** %buffPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #5
  %cleanup.dest94 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest94, label %unreachable [
    i32 0, label %cleanup.cont95
    i32 1, label %cleanup.cont95
  ]

cleanup.cont95:                                   ; preds = %cleanup90, %cleanup90
  ret void

unreachable:                                      ; preds = %cleanup90
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal i32 @RDG_rand(i32* %src) #0 {
entry:
  %src.addr = alloca i32*, align 4
  %rand32 = alloca i32, align 4
  store i32* %src, i32** %src.addr, align 4, !tbaa !2
  %0 = bitcast i32* %rand32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32*, i32** %src.addr, align 4, !tbaa !2
  %2 = load i32, i32* %1, align 4, !tbaa !10
  store i32 %2, i32* %rand32, align 4, !tbaa !10
  %3 = load i32, i32* %rand32, align 4, !tbaa !10
  %mul = mul i32 %3, -1640531535
  store i32 %mul, i32* %rand32, align 4, !tbaa !10
  %4 = load i32, i32* %rand32, align 4, !tbaa !10
  %xor = xor i32 %4, -2048144777
  store i32 %xor, i32* %rand32, align 4, !tbaa !10
  %5 = load i32, i32* %rand32, align 4, !tbaa !10
  %shl = shl i32 %5, 13
  %6 = load i32, i32* %rand32, align 4, !tbaa !10
  %shr = lshr i32 %6, 19
  %or = or i32 %shl, %shr
  store i32 %or, i32* %rand32, align 4, !tbaa !10
  %7 = load i32, i32* %rand32, align 4, !tbaa !10
  %8 = load i32*, i32** %src.addr, align 4, !tbaa !2
  store i32 %7, i32* %8, align 4, !tbaa !10
  %9 = load i32, i32* %rand32, align 4, !tbaa !10
  %10 = bitcast i32* %rand32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #5
  ret i32 %9
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: nounwind
define internal zeroext i8 @RDG_genChar(i32* %seed, i8* %lt) #0 {
entry:
  %seed.addr = alloca i32*, align 4
  %lt.addr = alloca i8*, align 4
  %id = alloca i32, align 4
  store i32* %seed, i32** %seed.addr, align 4, !tbaa !2
  store i8* %lt, i8** %lt.addr, align 4, !tbaa !2
  %0 = bitcast i32* %id to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32*, i32** %seed.addr, align 4, !tbaa !2
  %call = call i32 @RDG_rand(i32* %1)
  %and = and i32 %call, 8191
  store i32 %and, i32* %id, align 4, !tbaa !10
  %2 = load i8*, i8** %lt.addr, align 4, !tbaa !2
  %3 = load i32, i32* %id, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1, !tbaa !12
  %5 = bitcast i32* %id to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #5
  ret i8 %4
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @RDG_genBuffer(i8* %buffer, i32 %size, double %matchProba, double %litProba, i32 %seed) #0 {
entry:
  %buffer.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %matchProba.addr = alloca double, align 8
  %litProba.addr = alloca double, align 8
  %seed.addr = alloca i32, align 4
  %lt = alloca [8192 x i8], align 16
  store i8* %buffer, i8** %buffer.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  store double %matchProba, double* %matchProba.addr, align 8, !tbaa !8
  store double %litProba, double* %litProba.addr, align 8, !tbaa !8
  store i32 %seed, i32* %seed.addr, align 4, !tbaa !10
  %0 = bitcast [8192 x i8]* %lt to i8*
  call void @llvm.lifetime.start.p0i8(i64 8192, i8* %0) #5
  %1 = load double, double* %litProba.addr, align 8, !tbaa !8
  %cmp = fcmp oeq double %1, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load double, double* %matchProba.addr, align 8, !tbaa !8
  %div = fdiv double %2, 4.500000e+00
  store double %div, double* %litProba.addr, align 8, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %arraydecay = getelementptr inbounds [8192 x i8], [8192 x i8]* %lt, i32 0, i32 0
  %3 = load double, double* %litProba.addr, align 8, !tbaa !8
  call void @RDG_fillLiteralDistrib(i8* %arraydecay, double %3)
  %4 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %5 = load i32, i32* %size.addr, align 4, !tbaa !6
  %6 = load double, double* %matchProba.addr, align 8, !tbaa !8
  %arraydecay1 = getelementptr inbounds [8192 x i8], [8192 x i8]* %lt, i32 0, i32 0
  call void @RDG_genBlock(i8* %4, i32 %5, i32 0, double %6, i8* %arraydecay1, i32* %seed.addr)
  %7 = bitcast [8192 x i8]* %lt to i8*
  call void @llvm.lifetime.end.p0i8(i64 8192, i8* %7) #5
  ret void
}

; Function Attrs: nounwind
define internal void @RDG_fillLiteralDistrib(i8* %lt, double %ld) #0 {
entry:
  %lt.addr = alloca i8*, align 4
  %ld.addr = alloca double, align 8
  %firstChar = alloca i8, align 1
  %lastChar = alloca i8, align 1
  %character = alloca i8, align 1
  %u = alloca i32, align 4
  %weight = alloca i32, align 4
  %end = alloca i32, align 4
  store i8* %lt, i8** %lt.addr, align 4, !tbaa !2
  store double %ld, double* %ld.addr, align 8, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %firstChar) #5
  %0 = load double, double* %ld.addr, align 8, !tbaa !8
  %cmp = fcmp ole double %0, 0.000000e+00
  %1 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 0, i32 40
  %conv = trunc i32 %cond to i8
  store i8 %conv, i8* %firstChar, align 1, !tbaa !12
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %lastChar) #5
  %2 = load double, double* %ld.addr, align 8, !tbaa !8
  %cmp1 = fcmp ole double %2, 0.000000e+00
  %3 = zext i1 %cmp1 to i64
  %cond3 = select i1 %cmp1, i32 255, i32 125
  %conv4 = trunc i32 %cond3 to i8
  store i8 %conv4, i8* %lastChar, align 1, !tbaa !12
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %character) #5
  %4 = load double, double* %ld.addr, align 8, !tbaa !8
  %cmp5 = fcmp ole double %4, 0.000000e+00
  %5 = zext i1 %cmp5 to i64
  %cond7 = select i1 %cmp5, i32 0, i32 48
  %conv8 = trunc i32 %cond7 to i8
  store i8 %conv8, i8* %character, align 1, !tbaa !12
  %6 = bitcast i32* %u to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  store i32 0, i32* %u, align 4, !tbaa !10
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %7 = load i32, i32* %u, align 4, !tbaa !10
  %cmp9 = icmp ult i32 %7, 8192
  br i1 %cmp9, label %while.body, label %while.end29

while.body:                                       ; preds = %while.cond
  %8 = bitcast i32* %weight to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load i32, i32* %u, align 4, !tbaa !10
  %sub = sub i32 8192, %9
  %conv11 = uitofp i32 %sub to double
  %10 = load double, double* %ld.addr, align 8, !tbaa !8
  %mul = fmul double %conv11, %10
  %conv12 = fptoui double %mul to i32
  %add = add i32 %conv12, 1
  store i32 %add, i32* %weight, align 4, !tbaa !10
  %11 = bitcast i32* %end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %12 = load i32, i32* %u, align 4, !tbaa !10
  %13 = load i32, i32* %weight, align 4, !tbaa !10
  %add13 = add i32 %12, %13
  %cmp14 = icmp ult i32 %add13, 8192
  br i1 %cmp14, label %cond.true, label %cond.false

cond.true:                                        ; preds = %while.body
  %14 = load i32, i32* %u, align 4, !tbaa !10
  %15 = load i32, i32* %weight, align 4, !tbaa !10
  %add16 = add i32 %14, %15
  br label %cond.end

cond.false:                                       ; preds = %while.body
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond17 = phi i32 [ %add16, %cond.true ], [ 8192, %cond.false ]
  store i32 %cond17, i32* %end, align 4, !tbaa !10
  br label %while.cond18

while.cond18:                                     ; preds = %lor.end, %cond.end
  %16 = load i32, i32* %u, align 4, !tbaa !10
  %17 = load i32, i32* %end, align 4, !tbaa !10
  %cmp19 = icmp ult i32 %16, %17
  br i1 %cmp19, label %while.body21, label %while.end

while.body21:                                     ; preds = %while.cond18
  %18 = load i32, i32* %u, align 4, !tbaa !10
  %cmp22 = icmp ult i32 %18, 8192
  br i1 %cmp22, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %while.body21
  call void @__assert_fail(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.1, i32 0, i32 0), i32 83, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @__func__.RDG_fillLiteralDistrib, i32 0, i32 0)) #6
  unreachable

19:                                               ; No predecessors!
  br label %lor.end

lor.end:                                          ; preds = %19, %while.body21
  %20 = phi i1 [ true, %while.body21 ], [ false, %19 ]
  %lor.ext = zext i1 %20 to i32
  %21 = load i8, i8* %character, align 1, !tbaa !12
  %22 = load i8*, i8** %lt.addr, align 4, !tbaa !2
  %23 = load i32, i32* %u, align 4, !tbaa !10
  %inc = add i32 %23, 1
  store i32 %inc, i32* %u, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds i8, i8* %22, i32 %23
  store i8 %21, i8* %arrayidx, align 1, !tbaa !12
  br label %while.cond18

while.end:                                        ; preds = %while.cond18
  %24 = load i8, i8* %character, align 1, !tbaa !12
  %inc24 = add i8 %24, 1
  store i8 %inc24, i8* %character, align 1, !tbaa !12
  %25 = load i8, i8* %character, align 1, !tbaa !12
  %conv25 = zext i8 %25 to i32
  %26 = load i8, i8* %lastChar, align 1, !tbaa !12
  %conv26 = zext i8 %26 to i32
  %cmp27 = icmp sgt i32 %conv25, %conv26
  br i1 %cmp27, label %if.then, label %if.end

if.then:                                          ; preds = %while.end
  %27 = load i8, i8* %firstChar, align 1, !tbaa !12
  store i8 %27, i8* %character, align 1, !tbaa !12
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end
  %28 = bitcast i32* %end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  %29 = bitcast i32* %weight to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #5
  br label %while.cond

while.end29:                                      ; preds = %while.cond
  %30 = bitcast i32* %u to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %character) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %lastChar) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %firstChar) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @RDG_genOut(i64 %size, double %matchProba, double %litProba, i32 %seed) #0 {
entry:
  %size.addr = alloca i64, align 8
  %matchProba.addr = alloca double, align 8
  %litProba.addr = alloca double, align 8
  %seed.addr = alloca i32, align 4
  %buff = alloca [163840 x i8], align 16
  %total = alloca i64, align 8
  %genBlockSize = alloca i32, align 4
  %lt = alloca [8192 x i8], align 16
  store i64 %size, i64* %size.addr, align 8, !tbaa !13
  store double %matchProba, double* %matchProba.addr, align 8, !tbaa !8
  store double %litProba, double* %litProba.addr, align 8, !tbaa !8
  store i32 %seed, i32* %seed.addr, align 4, !tbaa !10
  %0 = bitcast [163840 x i8]* %buff to i8*
  call void @llvm.lifetime.start.p0i8(i64 163840, i8* %0) #5
  %1 = bitcast i64* %total to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #5
  store i64 0, i64* %total, align 8, !tbaa !13
  %2 = bitcast i32* %genBlockSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i32 131072, i32* %genBlockSize, align 4, !tbaa !6
  %3 = bitcast [8192 x i8]* %lt to i8*
  call void @llvm.lifetime.start.p0i8(i64 8192, i8* %3) #5
  %4 = load double, double* %litProba.addr, align 8, !tbaa !8
  %cmp = fcmp oeq double %4, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load double, double* %matchProba.addr, align 8, !tbaa !8
  %div = fdiv double %5, 4.500000e+00
  store double %div, double* %litProba.addr, align 8, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %arraydecay = getelementptr inbounds [8192 x i8], [8192 x i8]* %lt, i32 0, i32 0
  %6 = load double, double* %litProba.addr, align 8, !tbaa !8
  call void @RDG_fillLiteralDistrib(i8* %arraydecay, double %6)
  %arraydecay1 = getelementptr inbounds [163840 x i8], [163840 x i8]* %buff, i32 0, i32 0
  %7 = load double, double* %matchProba.addr, align 8, !tbaa !8
  %arraydecay2 = getelementptr inbounds [8192 x i8], [8192 x i8]* %lt, i32 0, i32 0
  call void @RDG_genBlock(i8* %arraydecay1, i32 32768, i32 0, double %7, i8* %arraydecay2, i32* %seed.addr)
  br label %while.cond

while.cond:                                       ; preds = %if.end9, %if.end
  %8 = load i64, i64* %total, align 8, !tbaa !13
  %9 = load i64, i64* %size.addr, align 8, !tbaa !13
  %cmp3 = icmp ult i64 %8, %9
  br i1 %cmp3, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %arraydecay4 = getelementptr inbounds [163840 x i8], [163840 x i8]* %buff, i32 0, i32 0
  %10 = load double, double* %matchProba.addr, align 8, !tbaa !8
  %arraydecay5 = getelementptr inbounds [8192 x i8], [8192 x i8]* %lt, i32 0, i32 0
  call void @RDG_genBlock(i8* %arraydecay4, i32 163840, i32 32768, double %10, i8* %arraydecay5, i32* %seed.addr)
  %11 = load i64, i64* %size.addr, align 8, !tbaa !13
  %12 = load i64, i64* %total, align 8, !tbaa !13
  %sub = sub i64 %11, %12
  %cmp6 = icmp ult i64 %sub, 131072
  br i1 %cmp6, label %if.then7, label %if.end9

if.then7:                                         ; preds = %while.body
  %13 = load i64, i64* %size.addr, align 8, !tbaa !13
  %14 = load i64, i64* %total, align 8, !tbaa !13
  %sub8 = sub i64 %13, %14
  %conv = trunc i64 %sub8 to i32
  store i32 %conv, i32* %genBlockSize, align 4, !tbaa !6
  br label %if.end9

if.end9:                                          ; preds = %if.then7, %while.body
  %15 = load i32, i32* %genBlockSize, align 4, !tbaa !6
  %conv10 = zext i32 %15 to i64
  %16 = load i64, i64* %total, align 8, !tbaa !13
  %add = add i64 %16, %conv10
  store i64 %add, i64* %total, align 8, !tbaa !13
  %arraydecay11 = getelementptr inbounds [163840 x i8], [163840 x i8]* %buff, i32 0, i32 0
  %17 = load i32, i32* %genBlockSize, align 4, !tbaa !6
  %18 = load %struct._IO_FILE*, %struct._IO_FILE** @stdout, align 4, !tbaa !2
  %call = call i32 @fwrite(i8* %arraydecay11, i32 1, i32 %17, %struct._IO_FILE* %18)
  %arraydecay12 = getelementptr inbounds [163840 x i8], [163840 x i8]* %buff, i32 0, i32 0
  %arraydecay13 = getelementptr inbounds [163840 x i8], [163840 x i8]* %buff, i32 0, i32 0
  %add.ptr = getelementptr inbounds i8, i8* %arraydecay13, i32 131072
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay12, i8* align 1 %add.ptr, i32 32768, i1 false)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %19 = bitcast [8192 x i8]* %lt to i8*
  call void @llvm.lifetime.end.p0i8(i64 8192, i8* %19) #5
  %20 = bitcast i32* %genBlockSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  %21 = bitcast i64* %total to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %21) #5
  %22 = bitcast [163840 x i8]* %buff to i8*
  call void @llvm.lifetime.end.p0i8(i64 163840, i8* %22) #5
  ret void
}

declare i32 @fwrite(i8*, i32, i32, %struct._IO_FILE*) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: noreturn
declare void @__assert_fail(i8*, i8*, i32, i8*) #4

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }
attributes #6 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"long", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"double", !4, i64 0}
!10 = !{!11, !11, i64 0}
!11 = !{!"int", !4, i64 0}
!12 = !{!4, !4, i64 0}
!13 = !{!14, !14, i64 0}
!14 = !{!"long long", !4, i64 0}
