; ModuleID = 'xxhash.c'
source_filename = "xxhash.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%union.anon = type { i32 }
%struct.XXH32_state_s = type { i32, i32, i32, i32, i32, i32, [4 x i32], i32, i32 }
%struct.XXH32_canonical_t = type { [4 x i8] }
%struct.XXH64_state_s = type { i64, i64, i64, i64, i64, [4 x i64], i32, [2 x i32] }
%struct.XXH64_canonical_t = type { [8 x i8] }

@__const.XXH_isLittleEndian.one = private unnamed_addr constant %union.anon { i32 1 }, align 4
@.str = private unnamed_addr constant [2 x i8] c"0\00", align 1
@.str.1 = private unnamed_addr constant [9 x i8] c"xxhash.c\00", align 1
@__func__.XXH32_finalize = private unnamed_addr constant [15 x i8] c"XXH32_finalize\00", align 1
@__func__.XXH64_finalize = private unnamed_addr constant [15 x i8] c"XXH64_finalize\00", align 1

; Function Attrs: nounwind
define hidden i32 @LZ4_XXH_versionNumber() #0 {
entry:
  ret i32 605
}

; Function Attrs: nounwind
define hidden i32 @LZ4_XXH32(i8* %input, i32 %len, i32 %seed) #0 {
entry:
  %retval = alloca i32, align 4
  %input.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %seed.addr = alloca i32, align 4
  %endian_detected = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %input, i8** %input.addr, align 4, !tbaa !3
  store i32 %len, i32* %len.addr, align 4, !tbaa !7
  store i32 %seed, i32* %seed.addr, align 4, !tbaa !9
  %0 = bitcast i32* %endian_detected to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call i32 @XXH_isLittleEndian()
  store i32 %call, i32* %endian_detected, align 4, !tbaa !11
  %1 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %2 = ptrtoint i8* %1 to i32
  %and = and i32 %2, 3
  %cmp = icmp eq i32 %and, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i32, i32* %endian_detected, align 4, !tbaa !11
  %cmp1 = icmp eq i32 %3, 1
  br i1 %cmp1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %4 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %5 = load i32, i32* %len.addr, align 4, !tbaa !7
  %6 = load i32, i32* %seed.addr, align 4, !tbaa !9
  %call3 = call i32 @XXH32_endian_align(i8* %4, i32 %5, i32 %6, i32 1, i32 0)
  store i32 %call3, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %if.then
  %7 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %8 = load i32, i32* %len.addr, align 4, !tbaa !7
  %9 = load i32, i32* %seed.addr, align 4, !tbaa !9
  %call4 = call i32 @XXH32_endian_align(i8* %7, i32 %8, i32 %9, i32 0, i32 0)
  store i32 %call4, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %10 = load i32, i32* %endian_detected, align 4, !tbaa !11
  %cmp5 = icmp eq i32 %10, 1
  br i1 %cmp5, label %if.then6, label %if.else8

if.then6:                                         ; preds = %if.end
  %11 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %12 = load i32, i32* %len.addr, align 4, !tbaa !7
  %13 = load i32, i32* %seed.addr, align 4, !tbaa !9
  %call7 = call i32 @XXH32_endian_align(i8* %11, i32 %12, i32 %13, i32 1, i32 1)
  store i32 %call7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else8:                                         ; preds = %if.end
  %14 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %15 = load i32, i32* %len.addr, align 4, !tbaa !7
  %16 = load i32, i32* %seed.addr, align 4, !tbaa !9
  %call9 = call i32 @XXH32_endian_align(i8* %14, i32 %15, i32 %16, i32 0, i32 1)
  store i32 %call9, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else8, %if.then6, %if.else, %if.then2
  %17 = bitcast i32* %endian_detected to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  %18 = load i32, i32* %retval, align 4
  ret i32 %18
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal i32 @XXH_isLittleEndian() #0 {
entry:
  %one = alloca %union.anon, align 4
  %0 = bitcast %union.anon* %one to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast %union.anon* %one to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 bitcast (%union.anon* @__const.XXH_isLittleEndian.one to i8*), i32 4, i1 false)
  %c = bitcast %union.anon* %one to [4 x i8]*
  %arrayidx = getelementptr inbounds [4 x i8], [4 x i8]* %c, i32 0, i32 0
  %2 = load i8, i8* %arrayidx, align 4, !tbaa !11
  %conv = zext i8 %2 to i32
  %3 = bitcast %union.anon* %one to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #6
  ret i32 %conv
}

; Function Attrs: alwaysinline nounwind
define internal i32 @XXH32_endian_align(i8* %input, i32 %len, i32 %seed, i32 %endian, i32 %align) #2 {
entry:
  %input.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %seed.addr = alloca i32, align 4
  %endian.addr = alloca i32, align 4
  %align.addr = alloca i32, align 4
  %p = alloca i8*, align 4
  %bEnd = alloca i8*, align 4
  %h32 = alloca i32, align 4
  %limit = alloca i8*, align 4
  %v1 = alloca i32, align 4
  %v2 = alloca i32, align 4
  %v3 = alloca i32, align 4
  %v4 = alloca i32, align 4
  store i8* %input, i8** %input.addr, align 4, !tbaa !3
  store i32 %len, i32* %len.addr, align 4, !tbaa !7
  store i32 %seed, i32* %seed.addr, align 4, !tbaa !9
  store i32 %endian, i32* %endian.addr, align 4, !tbaa !11
  store i32 %align, i32* %align.addr, align 4, !tbaa !11
  %0 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8*, i8** %input.addr, align 4, !tbaa !3
  store i8* %1, i8** %p, align 4, !tbaa !3
  %2 = bitcast i8** %bEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i8*, i8** %p, align 4, !tbaa !3
  %4 = load i32, i32* %len.addr, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %4
  store i8* %add.ptr, i8** %bEnd, align 4, !tbaa !3
  %5 = bitcast i32* %h32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  %6 = load i32, i32* %len.addr, align 4, !tbaa !7
  %cmp = icmp uge i32 %6, 16
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %7 = bitcast i8** %limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load i8*, i8** %bEnd, align 4, !tbaa !3
  %add.ptr1 = getelementptr inbounds i8, i8* %8, i32 -15
  store i8* %add.ptr1, i8** %limit, align 4, !tbaa !3
  %9 = bitcast i32* %v1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  %10 = load i32, i32* %seed.addr, align 4, !tbaa !9
  %add = add i32 %10, -1640531535
  %add2 = add i32 %add, -2048144777
  store i32 %add2, i32* %v1, align 4, !tbaa !9
  %11 = bitcast i32* %v2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  %12 = load i32, i32* %seed.addr, align 4, !tbaa !9
  %add3 = add i32 %12, -2048144777
  store i32 %add3, i32* %v2, align 4, !tbaa !9
  %13 = bitcast i32* %v3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load i32, i32* %seed.addr, align 4, !tbaa !9
  %add4 = add i32 %14, 0
  store i32 %add4, i32* %v3, align 4, !tbaa !9
  %15 = bitcast i32* %v4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  %16 = load i32, i32* %seed.addr, align 4, !tbaa !9
  %sub = sub i32 %16, -1640531535
  store i32 %sub, i32* %v4, align 4, !tbaa !9
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then
  %17 = load i32, i32* %v1, align 4, !tbaa !9
  %18 = load i8*, i8** %p, align 4, !tbaa !3
  %19 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %20 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call = call i32 @XXH_readLE32_align(i8* %18, i32 %19, i32 %20)
  %call5 = call i32 @XXH32_round(i32 %17, i32 %call)
  store i32 %call5, i32* %v1, align 4, !tbaa !9
  %21 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr6 = getelementptr inbounds i8, i8* %21, i32 4
  store i8* %add.ptr6, i8** %p, align 4, !tbaa !3
  %22 = load i32, i32* %v2, align 4, !tbaa !9
  %23 = load i8*, i8** %p, align 4, !tbaa !3
  %24 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %25 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call7 = call i32 @XXH_readLE32_align(i8* %23, i32 %24, i32 %25)
  %call8 = call i32 @XXH32_round(i32 %22, i32 %call7)
  store i32 %call8, i32* %v2, align 4, !tbaa !9
  %26 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr9 = getelementptr inbounds i8, i8* %26, i32 4
  store i8* %add.ptr9, i8** %p, align 4, !tbaa !3
  %27 = load i32, i32* %v3, align 4, !tbaa !9
  %28 = load i8*, i8** %p, align 4, !tbaa !3
  %29 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %30 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call10 = call i32 @XXH_readLE32_align(i8* %28, i32 %29, i32 %30)
  %call11 = call i32 @XXH32_round(i32 %27, i32 %call10)
  store i32 %call11, i32* %v3, align 4, !tbaa !9
  %31 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr12 = getelementptr inbounds i8, i8* %31, i32 4
  store i8* %add.ptr12, i8** %p, align 4, !tbaa !3
  %32 = load i32, i32* %v4, align 4, !tbaa !9
  %33 = load i8*, i8** %p, align 4, !tbaa !3
  %34 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %35 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call13 = call i32 @XXH_readLE32_align(i8* %33, i32 %34, i32 %35)
  %call14 = call i32 @XXH32_round(i32 %32, i32 %call13)
  store i32 %call14, i32* %v4, align 4, !tbaa !9
  %36 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr15 = getelementptr inbounds i8, i8* %36, i32 4
  store i8* %add.ptr15, i8** %p, align 4, !tbaa !3
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %37 = load i8*, i8** %p, align 4, !tbaa !3
  %38 = load i8*, i8** %limit, align 4, !tbaa !3
  %cmp16 = icmp ult i8* %37, %38
  br i1 %cmp16, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %39 = load i32, i32* %v1, align 4, !tbaa !9
  %shl = shl i32 %39, 1
  %40 = load i32, i32* %v1, align 4, !tbaa !9
  %shr = lshr i32 %40, 31
  %or = or i32 %shl, %shr
  %41 = load i32, i32* %v2, align 4, !tbaa !9
  %shl17 = shl i32 %41, 7
  %42 = load i32, i32* %v2, align 4, !tbaa !9
  %shr18 = lshr i32 %42, 25
  %or19 = or i32 %shl17, %shr18
  %add20 = add i32 %or, %or19
  %43 = load i32, i32* %v3, align 4, !tbaa !9
  %shl21 = shl i32 %43, 12
  %44 = load i32, i32* %v3, align 4, !tbaa !9
  %shr22 = lshr i32 %44, 20
  %or23 = or i32 %shl21, %shr22
  %add24 = add i32 %add20, %or23
  %45 = load i32, i32* %v4, align 4, !tbaa !9
  %shl25 = shl i32 %45, 18
  %46 = load i32, i32* %v4, align 4, !tbaa !9
  %shr26 = lshr i32 %46, 14
  %or27 = or i32 %shl25, %shr26
  %add28 = add i32 %add24, %or27
  store i32 %add28, i32* %h32, align 4, !tbaa !9
  %47 = bitcast i32* %v4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #6
  %48 = bitcast i32* %v3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #6
  %49 = bitcast i32* %v2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #6
  %50 = bitcast i32* %v1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #6
  %51 = bitcast i8** %limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #6
  br label %if.end

if.else:                                          ; preds = %entry
  %52 = load i32, i32* %seed.addr, align 4, !tbaa !9
  %add29 = add i32 %52, 374761393
  store i32 %add29, i32* %h32, align 4, !tbaa !9
  br label %if.end

if.end:                                           ; preds = %if.else, %do.end
  %53 = load i32, i32* %len.addr, align 4, !tbaa !7
  %54 = load i32, i32* %h32, align 4, !tbaa !9
  %add30 = add i32 %54, %53
  store i32 %add30, i32* %h32, align 4, !tbaa !9
  %55 = load i32, i32* %h32, align 4, !tbaa !9
  %56 = load i8*, i8** %p, align 4, !tbaa !3
  %57 = load i32, i32* %len.addr, align 4, !tbaa !7
  %and = and i32 %57, 15
  %58 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %59 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call31 = call i32 @XXH32_finalize(i32 %55, i8* %56, i32 %and, i32 %58, i32 %59)
  %60 = bitcast i32* %h32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  %61 = bitcast i8** %bEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #6
  %62 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #6
  ret i32 %call31
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden %struct.XXH32_state_s* @LZ4_XXH32_createState() #0 {
entry:
  %call = call i8* @XXH_malloc(i32 48)
  %0 = bitcast i8* %call to %struct.XXH32_state_s*
  ret %struct.XXH32_state_s* %0
}

; Function Attrs: nounwind
define internal i8* @XXH_malloc(i32 %s) #0 {
entry:
  %s.addr = alloca i32, align 4
  store i32 %s, i32* %s.addr, align 4, !tbaa !7
  %0 = load i32, i32* %s.addr, align 4, !tbaa !7
  %call = call i8* @malloc(i32 %0)
  ret i8* %call
}

; Function Attrs: nounwind
define hidden i32 @LZ4_XXH32_freeState(%struct.XXH32_state_s* %statePtr) #0 {
entry:
  %statePtr.addr = alloca %struct.XXH32_state_s*, align 4
  store %struct.XXH32_state_s* %statePtr, %struct.XXH32_state_s** %statePtr.addr, align 4, !tbaa !3
  %0 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %statePtr.addr, align 4, !tbaa !3
  %1 = bitcast %struct.XXH32_state_s* %0 to i8*
  call void @XXH_free(i8* %1)
  ret i32 0
}

; Function Attrs: nounwind
define internal void @XXH_free(i8* %p) #0 {
entry:
  %p.addr = alloca i8*, align 4
  store i8* %p, i8** %p.addr, align 4, !tbaa !3
  %0 = load i8*, i8** %p.addr, align 4, !tbaa !3
  call void @free(i8* %0)
  ret void
}

; Function Attrs: nounwind
define hidden void @LZ4_XXH32_copyState(%struct.XXH32_state_s* %dstState, %struct.XXH32_state_s* %srcState) #0 {
entry:
  %dstState.addr = alloca %struct.XXH32_state_s*, align 4
  %srcState.addr = alloca %struct.XXH32_state_s*, align 4
  store %struct.XXH32_state_s* %dstState, %struct.XXH32_state_s** %dstState.addr, align 4, !tbaa !3
  store %struct.XXH32_state_s* %srcState, %struct.XXH32_state_s** %srcState.addr, align 4, !tbaa !3
  %0 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %dstState.addr, align 4, !tbaa !3
  %1 = bitcast %struct.XXH32_state_s* %0 to i8*
  %2 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %srcState.addr, align 4, !tbaa !3
  %3 = bitcast %struct.XXH32_state_s* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %3, i32 48, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define hidden i32 @LZ4_XXH32_reset(%struct.XXH32_state_s* %statePtr, i32 %seed) #0 {
entry:
  %statePtr.addr = alloca %struct.XXH32_state_s*, align 4
  %seed.addr = alloca i32, align 4
  %state = alloca %struct.XXH32_state_s, align 4
  store %struct.XXH32_state_s* %statePtr, %struct.XXH32_state_s** %statePtr.addr, align 4, !tbaa !3
  store i32 %seed, i32* %seed.addr, align 4, !tbaa !9
  %0 = bitcast %struct.XXH32_state_s* %state to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %0) #6
  %1 = bitcast %struct.XXH32_state_s* %state to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %1, i8 0, i32 48, i1 false)
  %2 = load i32, i32* %seed.addr, align 4, !tbaa !9
  %add = add i32 %2, -1640531535
  %add1 = add i32 %add, -2048144777
  %v1 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %state, i32 0, i32 2
  store i32 %add1, i32* %v1, align 4, !tbaa !12
  %3 = load i32, i32* %seed.addr, align 4, !tbaa !9
  %add2 = add i32 %3, -2048144777
  %v2 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %state, i32 0, i32 3
  store i32 %add2, i32* %v2, align 4, !tbaa !14
  %4 = load i32, i32* %seed.addr, align 4, !tbaa !9
  %add3 = add i32 %4, 0
  %v3 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %state, i32 0, i32 4
  store i32 %add3, i32* %v3, align 4, !tbaa !15
  %5 = load i32, i32* %seed.addr, align 4, !tbaa !9
  %sub = sub i32 %5, -1640531535
  %v4 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %state, i32 0, i32 5
  store i32 %sub, i32* %v4, align 4, !tbaa !16
  %6 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %statePtr.addr, align 4, !tbaa !3
  %7 = bitcast %struct.XXH32_state_s* %6 to i8*
  %8 = bitcast %struct.XXH32_state_s* %state to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 44, i1 false)
  %9 = bitcast %struct.XXH32_state_s* %state to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %9) #6
  ret i32 0
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: nounwind
define hidden i32 @LZ4_XXH32_update(%struct.XXH32_state_s* %state_in, i8* %input, i32 %len) #0 {
entry:
  %retval = alloca i32, align 4
  %state_in.addr = alloca %struct.XXH32_state_s*, align 4
  %input.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %endian_detected = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.XXH32_state_s* %state_in, %struct.XXH32_state_s** %state_in.addr, align 4, !tbaa !3
  store i8* %input, i8** %input.addr, align 4, !tbaa !3
  store i32 %len, i32* %len.addr, align 4, !tbaa !7
  %0 = bitcast i32* %endian_detected to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call i32 @XXH_isLittleEndian()
  store i32 %call, i32* %endian_detected, align 4, !tbaa !11
  %1 = load i32, i32* %endian_detected, align 4, !tbaa !11
  %cmp = icmp eq i32 %1, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state_in.addr, align 4, !tbaa !3
  %3 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %4 = load i32, i32* %len.addr, align 4, !tbaa !7
  %call1 = call i32 @XXH32_update_endian(%struct.XXH32_state_s* %2, i8* %3, i32 %4, i32 1)
  store i32 %call1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %entry
  %5 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state_in.addr, align 4, !tbaa !3
  %6 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %7 = load i32, i32* %len.addr, align 4, !tbaa !7
  %call2 = call i32 @XXH32_update_endian(%struct.XXH32_state_s* %5, i8* %6, i32 %7, i32 0)
  store i32 %call2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then
  %8 = bitcast i32* %endian_detected to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

; Function Attrs: alwaysinline nounwind
define internal i32 @XXH32_update_endian(%struct.XXH32_state_s* %state, i8* %input, i32 %len, i32 %endian) #2 {
entry:
  %retval = alloca i32, align 4
  %state.addr = alloca %struct.XXH32_state_s*, align 4
  %input.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %endian.addr = alloca i32, align 4
  %p = alloca i8*, align 4
  %bEnd = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %p32 = alloca i32*, align 4
  %limit = alloca i8*, align 4
  %v149 = alloca i32, align 4
  %v251 = alloca i32, align 4
  %v353 = alloca i32, align 4
  %v455 = alloca i32, align 4
  store %struct.XXH32_state_s* %state, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  store i8* %input, i8** %input.addr, align 4, !tbaa !3
  store i32 %len, i32* %len.addr, align 4, !tbaa !7
  store i32 %endian, i32* %endian.addr, align 4, !tbaa !11
  %0 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %cmp = icmp eq i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i8*, i8** %input.addr, align 4, !tbaa !3
  store i8* %2, i8** %p, align 4, !tbaa !3
  %3 = bitcast i8** %bEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i8*, i8** %p, align 4, !tbaa !3
  %5 = load i32, i32* %len.addr, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %5
  store i8* %add.ptr, i8** %bEnd, align 4, !tbaa !3
  %6 = load i32, i32* %len.addr, align 4, !tbaa !7
  %7 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %total_len_32 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %7, i32 0, i32 0
  %8 = load i32, i32* %total_len_32, align 4, !tbaa !17
  %add = add i32 %8, %6
  store i32 %add, i32* %total_len_32, align 4, !tbaa !17
  %9 = load i32, i32* %len.addr, align 4, !tbaa !7
  %cmp1 = icmp uge i32 %9, 16
  %conv = zext i1 %cmp1 to i32
  %10 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %total_len_322 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %10, i32 0, i32 0
  %11 = load i32, i32* %total_len_322, align 4, !tbaa !17
  %cmp3 = icmp uge i32 %11, 16
  %conv4 = zext i1 %cmp3 to i32
  %or = or i32 %conv, %conv4
  %12 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %large_len = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %12, i32 0, i32 1
  %13 = load i32, i32* %large_len, align 4, !tbaa !18
  %or5 = or i32 %13, %or
  store i32 %or5, i32* %large_len, align 4, !tbaa !18
  %14 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %memsize = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %14, i32 0, i32 7
  %15 = load i32, i32* %memsize, align 4, !tbaa !19
  %16 = load i32, i32* %len.addr, align 4, !tbaa !7
  %add6 = add i32 %15, %16
  %cmp7 = icmp ult i32 %add6, 16
  br i1 %cmp7, label %if.then9, label %if.end14

if.then9:                                         ; preds = %if.end
  %17 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %mem32 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %17, i32 0, i32 6
  %arraydecay = getelementptr inbounds [4 x i32], [4 x i32]* %mem32, i32 0, i32 0
  %18 = bitcast i32* %arraydecay to i8*
  %19 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %memsize10 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %19, i32 0, i32 7
  %20 = load i32, i32* %memsize10, align 4, !tbaa !19
  %add.ptr11 = getelementptr inbounds i8, i8* %18, i32 %20
  %21 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %22 = load i32, i32* %len.addr, align 4, !tbaa !7
  %call = call i8* @XXH_memcpy(i8* %add.ptr11, i8* %21, i32 %22)
  %23 = load i32, i32* %len.addr, align 4, !tbaa !7
  %24 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %memsize12 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %24, i32 0, i32 7
  %25 = load i32, i32* %memsize12, align 4, !tbaa !19
  %add13 = add i32 %25, %23
  store i32 %add13, i32* %memsize12, align 4, !tbaa !19
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %if.end
  %26 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %memsize15 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %26, i32 0, i32 7
  %27 = load i32, i32* %memsize15, align 4, !tbaa !19
  %tobool = icmp ne i32 %27, 0
  br i1 %tobool, label %if.then16, label %if.end43

if.then16:                                        ; preds = %if.end14
  %28 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %mem3217 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %28, i32 0, i32 6
  %arraydecay18 = getelementptr inbounds [4 x i32], [4 x i32]* %mem3217, i32 0, i32 0
  %29 = bitcast i32* %arraydecay18 to i8*
  %30 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %memsize19 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %30, i32 0, i32 7
  %31 = load i32, i32* %memsize19, align 4, !tbaa !19
  %add.ptr20 = getelementptr inbounds i8, i8* %29, i32 %31
  %32 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %33 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %memsize21 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %33, i32 0, i32 7
  %34 = load i32, i32* %memsize21, align 4, !tbaa !19
  %sub = sub i32 16, %34
  %call22 = call i8* @XXH_memcpy(i8* %add.ptr20, i8* %32, i32 %sub)
  %35 = bitcast i32** %p32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #6
  %36 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %mem3223 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %36, i32 0, i32 6
  %arraydecay24 = getelementptr inbounds [4 x i32], [4 x i32]* %mem3223, i32 0, i32 0
  store i32* %arraydecay24, i32** %p32, align 4, !tbaa !3
  %37 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v1 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %37, i32 0, i32 2
  %38 = load i32, i32* %v1, align 4, !tbaa !12
  %39 = load i32*, i32** %p32, align 4, !tbaa !3
  %40 = bitcast i32* %39 to i8*
  %41 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call25 = call i32 @XXH_readLE32(i8* %40, i32 %41)
  %call26 = call i32 @XXH32_round(i32 %38, i32 %call25)
  %42 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v127 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %42, i32 0, i32 2
  store i32 %call26, i32* %v127, align 4, !tbaa !12
  %43 = load i32*, i32** %p32, align 4, !tbaa !3
  %incdec.ptr = getelementptr inbounds i32, i32* %43, i32 1
  store i32* %incdec.ptr, i32** %p32, align 4, !tbaa !3
  %44 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v2 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %44, i32 0, i32 3
  %45 = load i32, i32* %v2, align 4, !tbaa !14
  %46 = load i32*, i32** %p32, align 4, !tbaa !3
  %47 = bitcast i32* %46 to i8*
  %48 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call28 = call i32 @XXH_readLE32(i8* %47, i32 %48)
  %call29 = call i32 @XXH32_round(i32 %45, i32 %call28)
  %49 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v230 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %49, i32 0, i32 3
  store i32 %call29, i32* %v230, align 4, !tbaa !14
  %50 = load i32*, i32** %p32, align 4, !tbaa !3
  %incdec.ptr31 = getelementptr inbounds i32, i32* %50, i32 1
  store i32* %incdec.ptr31, i32** %p32, align 4, !tbaa !3
  %51 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v3 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %51, i32 0, i32 4
  %52 = load i32, i32* %v3, align 4, !tbaa !15
  %53 = load i32*, i32** %p32, align 4, !tbaa !3
  %54 = bitcast i32* %53 to i8*
  %55 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call32 = call i32 @XXH_readLE32(i8* %54, i32 %55)
  %call33 = call i32 @XXH32_round(i32 %52, i32 %call32)
  %56 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v334 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %56, i32 0, i32 4
  store i32 %call33, i32* %v334, align 4, !tbaa !15
  %57 = load i32*, i32** %p32, align 4, !tbaa !3
  %incdec.ptr35 = getelementptr inbounds i32, i32* %57, i32 1
  store i32* %incdec.ptr35, i32** %p32, align 4, !tbaa !3
  %58 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v4 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %58, i32 0, i32 5
  %59 = load i32, i32* %v4, align 4, !tbaa !16
  %60 = load i32*, i32** %p32, align 4, !tbaa !3
  %61 = bitcast i32* %60 to i8*
  %62 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call36 = call i32 @XXH_readLE32(i8* %61, i32 %62)
  %call37 = call i32 @XXH32_round(i32 %59, i32 %call36)
  %63 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v438 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %63, i32 0, i32 5
  store i32 %call37, i32* %v438, align 4, !tbaa !16
  %64 = bitcast i32** %p32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #6
  %65 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %memsize39 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %65, i32 0, i32 7
  %66 = load i32, i32* %memsize39, align 4, !tbaa !19
  %sub40 = sub i32 16, %66
  %67 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr41 = getelementptr inbounds i8, i8* %67, i32 %sub40
  store i8* %add.ptr41, i8** %p, align 4, !tbaa !3
  %68 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %memsize42 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %68, i32 0, i32 7
  store i32 0, i32* %memsize42, align 4, !tbaa !19
  br label %if.end43

if.end43:                                         ; preds = %if.then16, %if.end14
  %69 = load i8*, i8** %p, align 4, !tbaa !3
  %70 = load i8*, i8** %bEnd, align 4, !tbaa !3
  %add.ptr44 = getelementptr inbounds i8, i8* %70, i32 -16
  %cmp45 = icmp ule i8* %69, %add.ptr44
  br i1 %cmp45, label %if.then47, label %if.end75

if.then47:                                        ; preds = %if.end43
  %71 = bitcast i8** %limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #6
  %72 = load i8*, i8** %bEnd, align 4, !tbaa !3
  %add.ptr48 = getelementptr inbounds i8, i8* %72, i32 -16
  store i8* %add.ptr48, i8** %limit, align 4, !tbaa !3
  %73 = bitcast i32* %v149 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #6
  %74 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v150 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %74, i32 0, i32 2
  %75 = load i32, i32* %v150, align 4, !tbaa !12
  store i32 %75, i32* %v149, align 4, !tbaa !9
  %76 = bitcast i32* %v251 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #6
  %77 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v252 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %77, i32 0, i32 3
  %78 = load i32, i32* %v252, align 4, !tbaa !14
  store i32 %78, i32* %v251, align 4, !tbaa !9
  %79 = bitcast i32* %v353 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #6
  %80 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v354 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %80, i32 0, i32 4
  %81 = load i32, i32* %v354, align 4, !tbaa !15
  store i32 %81, i32* %v353, align 4, !tbaa !9
  %82 = bitcast i32* %v455 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #6
  %83 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v456 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %83, i32 0, i32 5
  %84 = load i32, i32* %v456, align 4, !tbaa !16
  store i32 %84, i32* %v455, align 4, !tbaa !9
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then47
  %85 = load i32, i32* %v149, align 4, !tbaa !9
  %86 = load i8*, i8** %p, align 4, !tbaa !3
  %87 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call57 = call i32 @XXH_readLE32(i8* %86, i32 %87)
  %call58 = call i32 @XXH32_round(i32 %85, i32 %call57)
  store i32 %call58, i32* %v149, align 4, !tbaa !9
  %88 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr59 = getelementptr inbounds i8, i8* %88, i32 4
  store i8* %add.ptr59, i8** %p, align 4, !tbaa !3
  %89 = load i32, i32* %v251, align 4, !tbaa !9
  %90 = load i8*, i8** %p, align 4, !tbaa !3
  %91 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call60 = call i32 @XXH_readLE32(i8* %90, i32 %91)
  %call61 = call i32 @XXH32_round(i32 %89, i32 %call60)
  store i32 %call61, i32* %v251, align 4, !tbaa !9
  %92 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr62 = getelementptr inbounds i8, i8* %92, i32 4
  store i8* %add.ptr62, i8** %p, align 4, !tbaa !3
  %93 = load i32, i32* %v353, align 4, !tbaa !9
  %94 = load i8*, i8** %p, align 4, !tbaa !3
  %95 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call63 = call i32 @XXH_readLE32(i8* %94, i32 %95)
  %call64 = call i32 @XXH32_round(i32 %93, i32 %call63)
  store i32 %call64, i32* %v353, align 4, !tbaa !9
  %96 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr65 = getelementptr inbounds i8, i8* %96, i32 4
  store i8* %add.ptr65, i8** %p, align 4, !tbaa !3
  %97 = load i32, i32* %v455, align 4, !tbaa !9
  %98 = load i8*, i8** %p, align 4, !tbaa !3
  %99 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call66 = call i32 @XXH_readLE32(i8* %98, i32 %99)
  %call67 = call i32 @XXH32_round(i32 %97, i32 %call66)
  store i32 %call67, i32* %v455, align 4, !tbaa !9
  %100 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr68 = getelementptr inbounds i8, i8* %100, i32 4
  store i8* %add.ptr68, i8** %p, align 4, !tbaa !3
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %101 = load i8*, i8** %p, align 4, !tbaa !3
  %102 = load i8*, i8** %limit, align 4, !tbaa !3
  %cmp69 = icmp ule i8* %101, %102
  br i1 %cmp69, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %103 = load i32, i32* %v149, align 4, !tbaa !9
  %104 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v171 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %104, i32 0, i32 2
  store i32 %103, i32* %v171, align 4, !tbaa !12
  %105 = load i32, i32* %v251, align 4, !tbaa !9
  %106 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v272 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %106, i32 0, i32 3
  store i32 %105, i32* %v272, align 4, !tbaa !14
  %107 = load i32, i32* %v353, align 4, !tbaa !9
  %108 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v373 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %108, i32 0, i32 4
  store i32 %107, i32* %v373, align 4, !tbaa !15
  %109 = load i32, i32* %v455, align 4, !tbaa !9
  %110 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v474 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %110, i32 0, i32 5
  store i32 %109, i32* %v474, align 4, !tbaa !16
  %111 = bitcast i32* %v455 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #6
  %112 = bitcast i32* %v353 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #6
  %113 = bitcast i32* %v251 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #6
  %114 = bitcast i32* %v149 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #6
  %115 = bitcast i8** %limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #6
  br label %if.end75

if.end75:                                         ; preds = %do.end, %if.end43
  %116 = load i8*, i8** %p, align 4, !tbaa !3
  %117 = load i8*, i8** %bEnd, align 4, !tbaa !3
  %cmp76 = icmp ult i8* %116, %117
  br i1 %cmp76, label %if.then78, label %if.end86

if.then78:                                        ; preds = %if.end75
  %118 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %mem3279 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %118, i32 0, i32 6
  %arraydecay80 = getelementptr inbounds [4 x i32], [4 x i32]* %mem3279, i32 0, i32 0
  %119 = bitcast i32* %arraydecay80 to i8*
  %120 = load i8*, i8** %p, align 4, !tbaa !3
  %121 = load i8*, i8** %bEnd, align 4, !tbaa !3
  %122 = load i8*, i8** %p, align 4, !tbaa !3
  %sub.ptr.lhs.cast = ptrtoint i8* %121 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %122 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %call81 = call i8* @XXH_memcpy(i8* %119, i8* %120, i32 %sub.ptr.sub)
  %123 = load i8*, i8** %bEnd, align 4, !tbaa !3
  %124 = load i8*, i8** %p, align 4, !tbaa !3
  %sub.ptr.lhs.cast82 = ptrtoint i8* %123 to i32
  %sub.ptr.rhs.cast83 = ptrtoint i8* %124 to i32
  %sub.ptr.sub84 = sub i32 %sub.ptr.lhs.cast82, %sub.ptr.rhs.cast83
  %125 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %memsize85 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %125, i32 0, i32 7
  store i32 %sub.ptr.sub84, i32* %memsize85, align 4, !tbaa !19
  br label %if.end86

if.end86:                                         ; preds = %if.then78, %if.end75
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end86, %if.then9
  %126 = bitcast i8** %bEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #6
  %127 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
  ]

cleanup.cont:                                     ; preds = %cleanup
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %cleanup.cont, %cleanup, %if.then
  %128 = load i32, i32* %retval, align 4
  ret i32 %128

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden i32 @LZ4_XXH32_digest(%struct.XXH32_state_s* %state_in) #0 {
entry:
  %retval = alloca i32, align 4
  %state_in.addr = alloca %struct.XXH32_state_s*, align 4
  %endian_detected = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.XXH32_state_s* %state_in, %struct.XXH32_state_s** %state_in.addr, align 4, !tbaa !3
  %0 = bitcast i32* %endian_detected to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call i32 @XXH_isLittleEndian()
  store i32 %call, i32* %endian_detected, align 4, !tbaa !11
  %1 = load i32, i32* %endian_detected, align 4, !tbaa !11
  %cmp = icmp eq i32 %1, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state_in.addr, align 4, !tbaa !3
  %call1 = call i32 @XXH32_digest_endian(%struct.XXH32_state_s* %2, i32 1)
  store i32 %call1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %entry
  %3 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state_in.addr, align 4, !tbaa !3
  %call2 = call i32 @XXH32_digest_endian(%struct.XXH32_state_s* %3, i32 0)
  store i32 %call2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then
  %4 = bitcast i32* %endian_detected to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: alwaysinline nounwind
define internal i32 @XXH32_digest_endian(%struct.XXH32_state_s* %state, i32 %endian) #2 {
entry:
  %state.addr = alloca %struct.XXH32_state_s*, align 4
  %endian.addr = alloca i32, align 4
  %h32 = alloca i32, align 4
  store %struct.XXH32_state_s* %state, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  store i32 %endian, i32* %endian.addr, align 4, !tbaa !11
  %0 = bitcast i32* %h32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %large_len = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %1, i32 0, i32 1
  %2 = load i32, i32* %large_len, align 4, !tbaa !18
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v1 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %3, i32 0, i32 2
  %4 = load i32, i32* %v1, align 4, !tbaa !12
  %shl = shl i32 %4, 1
  %5 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v11 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %5, i32 0, i32 2
  %6 = load i32, i32* %v11, align 4, !tbaa !12
  %shr = lshr i32 %6, 31
  %or = or i32 %shl, %shr
  %7 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v2 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %7, i32 0, i32 3
  %8 = load i32, i32* %v2, align 4, !tbaa !14
  %shl2 = shl i32 %8, 7
  %9 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v23 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %9, i32 0, i32 3
  %10 = load i32, i32* %v23, align 4, !tbaa !14
  %shr4 = lshr i32 %10, 25
  %or5 = or i32 %shl2, %shr4
  %add = add i32 %or, %or5
  %11 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v3 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %11, i32 0, i32 4
  %12 = load i32, i32* %v3, align 4, !tbaa !15
  %shl6 = shl i32 %12, 12
  %13 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v37 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %13, i32 0, i32 4
  %14 = load i32, i32* %v37, align 4, !tbaa !15
  %shr8 = lshr i32 %14, 20
  %or9 = or i32 %shl6, %shr8
  %add10 = add i32 %add, %or9
  %15 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v4 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %15, i32 0, i32 5
  %16 = load i32, i32* %v4, align 4, !tbaa !16
  %shl11 = shl i32 %16, 18
  %17 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v412 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %17, i32 0, i32 5
  %18 = load i32, i32* %v412, align 4, !tbaa !16
  %shr13 = lshr i32 %18, 14
  %or14 = or i32 %shl11, %shr13
  %add15 = add i32 %add10, %or14
  store i32 %add15, i32* %h32, align 4, !tbaa !9
  br label %if.end

if.else:                                          ; preds = %entry
  %19 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %v316 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %19, i32 0, i32 4
  %20 = load i32, i32* %v316, align 4, !tbaa !15
  %add17 = add i32 %20, 374761393
  store i32 %add17, i32* %h32, align 4, !tbaa !9
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %21 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %total_len_32 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %21, i32 0, i32 0
  %22 = load i32, i32* %total_len_32, align 4, !tbaa !17
  %23 = load i32, i32* %h32, align 4, !tbaa !9
  %add18 = add i32 %23, %22
  store i32 %add18, i32* %h32, align 4, !tbaa !9
  %24 = load i32, i32* %h32, align 4, !tbaa !9
  %25 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %mem32 = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %25, i32 0, i32 6
  %arraydecay = getelementptr inbounds [4 x i32], [4 x i32]* %mem32, i32 0, i32 0
  %26 = bitcast i32* %arraydecay to i8*
  %27 = load %struct.XXH32_state_s*, %struct.XXH32_state_s** %state.addr, align 4, !tbaa !3
  %memsize = getelementptr inbounds %struct.XXH32_state_s, %struct.XXH32_state_s* %27, i32 0, i32 7
  %28 = load i32, i32* %memsize, align 4, !tbaa !19
  %29 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call = call i32 @XXH32_finalize(i32 %24, i8* %26, i32 %28, i32 %29, i32 0)
  %30 = bitcast i32* %h32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #6
  ret i32 %call
}

; Function Attrs: nounwind
define hidden void @LZ4_XXH32_canonicalFromHash(%struct.XXH32_canonical_t* %dst, i32 %hash) #0 {
entry:
  %dst.addr = alloca %struct.XXH32_canonical_t*, align 4
  %hash.addr = alloca i32, align 4
  store %struct.XXH32_canonical_t* %dst, %struct.XXH32_canonical_t** %dst.addr, align 4, !tbaa !3
  store i32 %hash, i32* %hash.addr, align 4, !tbaa !9
  %call = call i32 @XXH_isLittleEndian()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load i32, i32* %hash.addr, align 4, !tbaa !9
  %call1 = call i32 @XXH_swap32(i32 %0)
  store i32 %call1, i32* %hash.addr, align 4, !tbaa !9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load %struct.XXH32_canonical_t*, %struct.XXH32_canonical_t** %dst.addr, align 4, !tbaa !3
  %2 = bitcast %struct.XXH32_canonical_t* %1 to i8*
  %3 = bitcast i32* %hash.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %2, i8* align 4 %3, i32 4, i1 false)
  ret void
}

; Function Attrs: nounwind
define internal i32 @XXH_swap32(i32 %x) #0 {
entry:
  %x.addr = alloca i32, align 4
  store i32 %x, i32* %x.addr, align 4, !tbaa !9
  %0 = load i32, i32* %x.addr, align 4, !tbaa !9
  %shl = shl i32 %0, 24
  %and = and i32 %shl, -16777216
  %1 = load i32, i32* %x.addr, align 4, !tbaa !9
  %shl1 = shl i32 %1, 8
  %and2 = and i32 %shl1, 16711680
  %or = or i32 %and, %and2
  %2 = load i32, i32* %x.addr, align 4, !tbaa !9
  %shr = lshr i32 %2, 8
  %and3 = and i32 %shr, 65280
  %or4 = or i32 %or, %and3
  %3 = load i32, i32* %x.addr, align 4, !tbaa !9
  %shr5 = lshr i32 %3, 24
  %and6 = and i32 %shr5, 255
  %or7 = or i32 %or4, %and6
  ret i32 %or7
}

; Function Attrs: nounwind
define hidden i32 @LZ4_XXH32_hashFromCanonical(%struct.XXH32_canonical_t* %src) #0 {
entry:
  %src.addr = alloca %struct.XXH32_canonical_t*, align 4
  store %struct.XXH32_canonical_t* %src, %struct.XXH32_canonical_t** %src.addr, align 4, !tbaa !3
  %0 = load %struct.XXH32_canonical_t*, %struct.XXH32_canonical_t** %src.addr, align 4, !tbaa !3
  %1 = bitcast %struct.XXH32_canonical_t* %0 to i8*
  %call = call i32 @XXH_readBE32(i8* %1)
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @XXH_readBE32(i8* %ptr) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !3
  %call = call i32 @XXH_isLittleEndian()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  %call1 = call i32 @XXH_read32(i8* %0)
  %call2 = call i32 @XXH_swap32(i32 %call1)
  br label %cond.end

cond.false:                                       ; preds = %entry
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  %call3 = call i32 @XXH_read32(i8* %1)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call2, %cond.true ], [ %call3, %cond.false ]
  ret i32 %cond
}

; Function Attrs: nounwind
define hidden i64 @LZ4_XXH64(i8* %input, i32 %len, i64 %seed) #0 {
entry:
  %retval = alloca i64, align 8
  %input.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %seed.addr = alloca i64, align 8
  %endian_detected = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %input, i8** %input.addr, align 4, !tbaa !3
  store i32 %len, i32* %len.addr, align 4, !tbaa !7
  store i64 %seed, i64* %seed.addr, align 8, !tbaa !20
  %0 = bitcast i32* %endian_detected to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call i32 @XXH_isLittleEndian()
  store i32 %call, i32* %endian_detected, align 4, !tbaa !11
  %1 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %2 = ptrtoint i8* %1 to i32
  %and = and i32 %2, 7
  %cmp = icmp eq i32 %and, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i32, i32* %endian_detected, align 4, !tbaa !11
  %cmp1 = icmp eq i32 %3, 1
  br i1 %cmp1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %4 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %5 = load i32, i32* %len.addr, align 4, !tbaa !7
  %6 = load i64, i64* %seed.addr, align 8, !tbaa !20
  %call3 = call i64 @XXH64_endian_align(i8* %4, i32 %5, i64 %6, i32 1, i32 0)
  store i64 %call3, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %if.then
  %7 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %8 = load i32, i32* %len.addr, align 4, !tbaa !7
  %9 = load i64, i64* %seed.addr, align 8, !tbaa !20
  %call4 = call i64 @XXH64_endian_align(i8* %7, i32 %8, i64 %9, i32 0, i32 0)
  store i64 %call4, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %10 = load i32, i32* %endian_detected, align 4, !tbaa !11
  %cmp5 = icmp eq i32 %10, 1
  br i1 %cmp5, label %if.then6, label %if.else8

if.then6:                                         ; preds = %if.end
  %11 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %12 = load i32, i32* %len.addr, align 4, !tbaa !7
  %13 = load i64, i64* %seed.addr, align 8, !tbaa !20
  %call7 = call i64 @XXH64_endian_align(i8* %11, i32 %12, i64 %13, i32 1, i32 1)
  store i64 %call7, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else8:                                         ; preds = %if.end
  %14 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %15 = load i32, i32* %len.addr, align 4, !tbaa !7
  %16 = load i64, i64* %seed.addr, align 8, !tbaa !20
  %call9 = call i64 @XXH64_endian_align(i8* %14, i32 %15, i64 %16, i32 0, i32 1)
  store i64 %call9, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else8, %if.then6, %if.else, %if.then2
  %17 = bitcast i32* %endian_detected to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  %18 = load i64, i64* %retval, align 8
  ret i64 %18
}

; Function Attrs: alwaysinline nounwind
define internal i64 @XXH64_endian_align(i8* %input, i32 %len, i64 %seed, i32 %endian, i32 %align) #2 {
entry:
  %input.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %seed.addr = alloca i64, align 8
  %endian.addr = alloca i32, align 4
  %align.addr = alloca i32, align 4
  %p = alloca i8*, align 4
  %bEnd = alloca i8*, align 4
  %h64 = alloca i64, align 8
  %limit = alloca i8*, align 4
  %v1 = alloca i64, align 8
  %v2 = alloca i64, align 8
  %v3 = alloca i64, align 8
  %v4 = alloca i64, align 8
  store i8* %input, i8** %input.addr, align 4, !tbaa !3
  store i32 %len, i32* %len.addr, align 4, !tbaa !7
  store i64 %seed, i64* %seed.addr, align 8, !tbaa !20
  store i32 %endian, i32* %endian.addr, align 4, !tbaa !11
  store i32 %align, i32* %align.addr, align 4, !tbaa !11
  %0 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8*, i8** %input.addr, align 4, !tbaa !3
  store i8* %1, i8** %p, align 4, !tbaa !3
  %2 = bitcast i8** %bEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load i8*, i8** %p, align 4, !tbaa !3
  %4 = load i32, i32* %len.addr, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %4
  store i8* %add.ptr, i8** %bEnd, align 4, !tbaa !3
  %5 = bitcast i64* %h64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %5) #6
  %6 = load i32, i32* %len.addr, align 4, !tbaa !7
  %cmp = icmp uge i32 %6, 32
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %7 = bitcast i8** %limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %8 = load i8*, i8** %bEnd, align 4, !tbaa !3
  %add.ptr1 = getelementptr inbounds i8, i8* %8, i32 -32
  store i8* %add.ptr1, i8** %limit, align 4, !tbaa !3
  %9 = bitcast i64* %v1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %9) #6
  %10 = load i64, i64* %seed.addr, align 8, !tbaa !20
  %add = add i64 %10, -7046029288634856825
  %add2 = add i64 %add, -4417276706812531889
  store i64 %add2, i64* %v1, align 8, !tbaa !20
  %11 = bitcast i64* %v2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %11) #6
  %12 = load i64, i64* %seed.addr, align 8, !tbaa !20
  %add3 = add i64 %12, -4417276706812531889
  store i64 %add3, i64* %v2, align 8, !tbaa !20
  %13 = bitcast i64* %v3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %13) #6
  %14 = load i64, i64* %seed.addr, align 8, !tbaa !20
  %add4 = add i64 %14, 0
  store i64 %add4, i64* %v3, align 8, !tbaa !20
  %15 = bitcast i64* %v4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %15) #6
  %16 = load i64, i64* %seed.addr, align 8, !tbaa !20
  %sub = sub i64 %16, -7046029288634856825
  store i64 %sub, i64* %v4, align 8, !tbaa !20
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then
  %17 = load i64, i64* %v1, align 8, !tbaa !20
  %18 = load i8*, i8** %p, align 4, !tbaa !3
  %19 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %20 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call = call i64 @XXH_readLE64_align(i8* %18, i32 %19, i32 %20)
  %call5 = call i64 @XXH64_round(i64 %17, i64 %call)
  store i64 %call5, i64* %v1, align 8, !tbaa !20
  %21 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr6 = getelementptr inbounds i8, i8* %21, i32 8
  store i8* %add.ptr6, i8** %p, align 4, !tbaa !3
  %22 = load i64, i64* %v2, align 8, !tbaa !20
  %23 = load i8*, i8** %p, align 4, !tbaa !3
  %24 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %25 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call7 = call i64 @XXH_readLE64_align(i8* %23, i32 %24, i32 %25)
  %call8 = call i64 @XXH64_round(i64 %22, i64 %call7)
  store i64 %call8, i64* %v2, align 8, !tbaa !20
  %26 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr9 = getelementptr inbounds i8, i8* %26, i32 8
  store i8* %add.ptr9, i8** %p, align 4, !tbaa !3
  %27 = load i64, i64* %v3, align 8, !tbaa !20
  %28 = load i8*, i8** %p, align 4, !tbaa !3
  %29 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %30 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call10 = call i64 @XXH_readLE64_align(i8* %28, i32 %29, i32 %30)
  %call11 = call i64 @XXH64_round(i64 %27, i64 %call10)
  store i64 %call11, i64* %v3, align 8, !tbaa !20
  %31 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr12 = getelementptr inbounds i8, i8* %31, i32 8
  store i8* %add.ptr12, i8** %p, align 4, !tbaa !3
  %32 = load i64, i64* %v4, align 8, !tbaa !20
  %33 = load i8*, i8** %p, align 4, !tbaa !3
  %34 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %35 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call13 = call i64 @XXH_readLE64_align(i8* %33, i32 %34, i32 %35)
  %call14 = call i64 @XXH64_round(i64 %32, i64 %call13)
  store i64 %call14, i64* %v4, align 8, !tbaa !20
  %36 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr15 = getelementptr inbounds i8, i8* %36, i32 8
  store i8* %add.ptr15, i8** %p, align 4, !tbaa !3
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %37 = load i8*, i8** %p, align 4, !tbaa !3
  %38 = load i8*, i8** %limit, align 4, !tbaa !3
  %cmp16 = icmp ule i8* %37, %38
  br i1 %cmp16, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %39 = load i64, i64* %v1, align 8, !tbaa !20
  %shl = shl i64 %39, 1
  %40 = load i64, i64* %v1, align 8, !tbaa !20
  %shr = lshr i64 %40, 63
  %or = or i64 %shl, %shr
  %41 = load i64, i64* %v2, align 8, !tbaa !20
  %shl17 = shl i64 %41, 7
  %42 = load i64, i64* %v2, align 8, !tbaa !20
  %shr18 = lshr i64 %42, 57
  %or19 = or i64 %shl17, %shr18
  %add20 = add i64 %or, %or19
  %43 = load i64, i64* %v3, align 8, !tbaa !20
  %shl21 = shl i64 %43, 12
  %44 = load i64, i64* %v3, align 8, !tbaa !20
  %shr22 = lshr i64 %44, 52
  %or23 = or i64 %shl21, %shr22
  %add24 = add i64 %add20, %or23
  %45 = load i64, i64* %v4, align 8, !tbaa !20
  %shl25 = shl i64 %45, 18
  %46 = load i64, i64* %v4, align 8, !tbaa !20
  %shr26 = lshr i64 %46, 46
  %or27 = or i64 %shl25, %shr26
  %add28 = add i64 %add24, %or27
  store i64 %add28, i64* %h64, align 8, !tbaa !20
  %47 = load i64, i64* %h64, align 8, !tbaa !20
  %48 = load i64, i64* %v1, align 8, !tbaa !20
  %call29 = call i64 @XXH64_mergeRound(i64 %47, i64 %48)
  store i64 %call29, i64* %h64, align 8, !tbaa !20
  %49 = load i64, i64* %h64, align 8, !tbaa !20
  %50 = load i64, i64* %v2, align 8, !tbaa !20
  %call30 = call i64 @XXH64_mergeRound(i64 %49, i64 %50)
  store i64 %call30, i64* %h64, align 8, !tbaa !20
  %51 = load i64, i64* %h64, align 8, !tbaa !20
  %52 = load i64, i64* %v3, align 8, !tbaa !20
  %call31 = call i64 @XXH64_mergeRound(i64 %51, i64 %52)
  store i64 %call31, i64* %h64, align 8, !tbaa !20
  %53 = load i64, i64* %h64, align 8, !tbaa !20
  %54 = load i64, i64* %v4, align 8, !tbaa !20
  %call32 = call i64 @XXH64_mergeRound(i64 %53, i64 %54)
  store i64 %call32, i64* %h64, align 8, !tbaa !20
  %55 = bitcast i64* %v4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %55) #6
  %56 = bitcast i64* %v3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %56) #6
  %57 = bitcast i64* %v2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %57) #6
  %58 = bitcast i64* %v1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %58) #6
  %59 = bitcast i8** %limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #6
  br label %if.end

if.else:                                          ; preds = %entry
  %60 = load i64, i64* %seed.addr, align 8, !tbaa !20
  %add33 = add i64 %60, 2870177450012600261
  store i64 %add33, i64* %h64, align 8, !tbaa !20
  br label %if.end

if.end:                                           ; preds = %if.else, %do.end
  %61 = load i32, i32* %len.addr, align 4, !tbaa !7
  %conv = zext i32 %61 to i64
  %62 = load i64, i64* %h64, align 8, !tbaa !20
  %add34 = add i64 %62, %conv
  store i64 %add34, i64* %h64, align 8, !tbaa !20
  %63 = load i64, i64* %h64, align 8, !tbaa !20
  %64 = load i8*, i8** %p, align 4, !tbaa !3
  %65 = load i32, i32* %len.addr, align 4, !tbaa !7
  %66 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %67 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call35 = call i64 @XXH64_finalize(i64 %63, i8* %64, i32 %65, i32 %66, i32 %67)
  %68 = bitcast i64* %h64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %68) #6
  %69 = bitcast i8** %bEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #6
  %70 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #6
  ret i64 %call35
}

; Function Attrs: nounwind
define hidden %struct.XXH64_state_s* @LZ4_XXH64_createState() #0 {
entry:
  %call = call i8* @XXH_malloc(i32 88)
  %0 = bitcast i8* %call to %struct.XXH64_state_s*
  ret %struct.XXH64_state_s* %0
}

; Function Attrs: nounwind
define hidden i32 @LZ4_XXH64_freeState(%struct.XXH64_state_s* %statePtr) #0 {
entry:
  %statePtr.addr = alloca %struct.XXH64_state_s*, align 4
  store %struct.XXH64_state_s* %statePtr, %struct.XXH64_state_s** %statePtr.addr, align 4, !tbaa !3
  %0 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %statePtr.addr, align 4, !tbaa !3
  %1 = bitcast %struct.XXH64_state_s* %0 to i8*
  call void @XXH_free(i8* %1)
  ret i32 0
}

; Function Attrs: nounwind
define hidden void @LZ4_XXH64_copyState(%struct.XXH64_state_s* %dstState, %struct.XXH64_state_s* %srcState) #0 {
entry:
  %dstState.addr = alloca %struct.XXH64_state_s*, align 4
  %srcState.addr = alloca %struct.XXH64_state_s*, align 4
  store %struct.XXH64_state_s* %dstState, %struct.XXH64_state_s** %dstState.addr, align 4, !tbaa !3
  store %struct.XXH64_state_s* %srcState, %struct.XXH64_state_s** %srcState.addr, align 4, !tbaa !3
  %0 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %dstState.addr, align 4, !tbaa !3
  %1 = bitcast %struct.XXH64_state_s* %0 to i8*
  %2 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %srcState.addr, align 4, !tbaa !3
  %3 = bitcast %struct.XXH64_state_s* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %1, i8* align 8 %3, i32 88, i1 false)
  ret void
}

; Function Attrs: nounwind
define hidden i32 @LZ4_XXH64_reset(%struct.XXH64_state_s* %statePtr, i64 %seed) #0 {
entry:
  %statePtr.addr = alloca %struct.XXH64_state_s*, align 4
  %seed.addr = alloca i64, align 8
  %state = alloca %struct.XXH64_state_s, align 8
  store %struct.XXH64_state_s* %statePtr, %struct.XXH64_state_s** %statePtr.addr, align 4, !tbaa !3
  store i64 %seed, i64* %seed.addr, align 8, !tbaa !20
  %0 = bitcast %struct.XXH64_state_s* %state to i8*
  call void @llvm.lifetime.start.p0i8(i64 88, i8* %0) #6
  %1 = bitcast %struct.XXH64_state_s* %state to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %1, i8 0, i32 88, i1 false)
  %2 = load i64, i64* %seed.addr, align 8, !tbaa !20
  %add = add i64 %2, -7046029288634856825
  %add1 = add i64 %add, -4417276706812531889
  %v1 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %state, i32 0, i32 1
  store i64 %add1, i64* %v1, align 8, !tbaa !22
  %3 = load i64, i64* %seed.addr, align 8, !tbaa !20
  %add2 = add i64 %3, -4417276706812531889
  %v2 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %state, i32 0, i32 2
  store i64 %add2, i64* %v2, align 8, !tbaa !24
  %4 = load i64, i64* %seed.addr, align 8, !tbaa !20
  %add3 = add i64 %4, 0
  %v3 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %state, i32 0, i32 3
  store i64 %add3, i64* %v3, align 8, !tbaa !25
  %5 = load i64, i64* %seed.addr, align 8, !tbaa !20
  %sub = sub i64 %5, -7046029288634856825
  %v4 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %state, i32 0, i32 4
  store i64 %sub, i64* %v4, align 8, !tbaa !26
  %6 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %statePtr.addr, align 4, !tbaa !3
  %7 = bitcast %struct.XXH64_state_s* %6 to i8*
  %8 = bitcast %struct.XXH64_state_s* %state to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %7, i8* align 8 %8, i32 80, i1 false)
  %9 = bitcast %struct.XXH64_state_s* %state to i8*
  call void @llvm.lifetime.end.p0i8(i64 88, i8* %9) #6
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @LZ4_XXH64_update(%struct.XXH64_state_s* %state_in, i8* %input, i32 %len) #0 {
entry:
  %retval = alloca i32, align 4
  %state_in.addr = alloca %struct.XXH64_state_s*, align 4
  %input.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %endian_detected = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.XXH64_state_s* %state_in, %struct.XXH64_state_s** %state_in.addr, align 4, !tbaa !3
  store i8* %input, i8** %input.addr, align 4, !tbaa !3
  store i32 %len, i32* %len.addr, align 4, !tbaa !7
  %0 = bitcast i32* %endian_detected to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call i32 @XXH_isLittleEndian()
  store i32 %call, i32* %endian_detected, align 4, !tbaa !11
  %1 = load i32, i32* %endian_detected, align 4, !tbaa !11
  %cmp = icmp eq i32 %1, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state_in.addr, align 4, !tbaa !3
  %3 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %4 = load i32, i32* %len.addr, align 4, !tbaa !7
  %call1 = call i32 @XXH64_update_endian(%struct.XXH64_state_s* %2, i8* %3, i32 %4, i32 1)
  store i32 %call1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %entry
  %5 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state_in.addr, align 4, !tbaa !3
  %6 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %7 = load i32, i32* %len.addr, align 4, !tbaa !7
  %call2 = call i32 @XXH64_update_endian(%struct.XXH64_state_s* %5, i8* %6, i32 %7, i32 0)
  store i32 %call2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then
  %8 = bitcast i32* %endian_detected to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #6
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

; Function Attrs: alwaysinline nounwind
define internal i32 @XXH64_update_endian(%struct.XXH64_state_s* %state, i8* %input, i32 %len, i32 %endian) #2 {
entry:
  %retval = alloca i32, align 4
  %state.addr = alloca %struct.XXH64_state_s*, align 4
  %input.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %endian.addr = alloca i32, align 4
  %p = alloca i8*, align 4
  %bEnd = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %limit = alloca i8*, align 4
  %v152 = alloca i64, align 8
  %v254 = alloca i64, align 8
  %v356 = alloca i64, align 8
  %v458 = alloca i64, align 8
  store %struct.XXH64_state_s* %state, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  store i8* %input, i8** %input.addr, align 4, !tbaa !3
  store i32 %len, i32* %len.addr, align 4, !tbaa !7
  store i32 %endian, i32* %endian.addr, align 4, !tbaa !11
  %0 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %cmp = icmp eq i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i8*, i8** %input.addr, align 4, !tbaa !3
  store i8* %2, i8** %p, align 4, !tbaa !3
  %3 = bitcast i8** %bEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i8*, i8** %p, align 4, !tbaa !3
  %5 = load i32, i32* %len.addr, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %5
  store i8* %add.ptr, i8** %bEnd, align 4, !tbaa !3
  %6 = load i32, i32* %len.addr, align 4, !tbaa !7
  %conv = zext i32 %6 to i64
  %7 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %total_len = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %7, i32 0, i32 0
  %8 = load i64, i64* %total_len, align 8, !tbaa !27
  %add = add i64 %8, %conv
  store i64 %add, i64* %total_len, align 8, !tbaa !27
  %9 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %memsize = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %9, i32 0, i32 6
  %10 = load i32, i32* %memsize, align 8, !tbaa !28
  %11 = load i32, i32* %len.addr, align 4, !tbaa !7
  %add1 = add i32 %10, %11
  %cmp2 = icmp ult i32 %add1, 32
  br i1 %cmp2, label %if.then4, label %if.end9

if.then4:                                         ; preds = %if.end
  %12 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %mem64 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %12, i32 0, i32 5
  %arraydecay = getelementptr inbounds [4 x i64], [4 x i64]* %mem64, i32 0, i32 0
  %13 = bitcast i64* %arraydecay to i8*
  %14 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %memsize5 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %14, i32 0, i32 6
  %15 = load i32, i32* %memsize5, align 8, !tbaa !28
  %add.ptr6 = getelementptr inbounds i8, i8* %13, i32 %15
  %16 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %17 = load i32, i32* %len.addr, align 4, !tbaa !7
  %call = call i8* @XXH_memcpy(i8* %add.ptr6, i8* %16, i32 %17)
  %18 = load i32, i32* %len.addr, align 4, !tbaa !7
  %19 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %memsize7 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %19, i32 0, i32 6
  %20 = load i32, i32* %memsize7, align 8, !tbaa !28
  %add8 = add i32 %20, %18
  store i32 %add8, i32* %memsize7, align 8, !tbaa !28
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end9:                                          ; preds = %if.end
  %21 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %memsize10 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %21, i32 0, i32 6
  %22 = load i32, i32* %memsize10, align 8, !tbaa !28
  %tobool = icmp ne i32 %22, 0
  br i1 %tobool, label %if.then11, label %if.end46

if.then11:                                        ; preds = %if.end9
  %23 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %mem6412 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %23, i32 0, i32 5
  %arraydecay13 = getelementptr inbounds [4 x i64], [4 x i64]* %mem6412, i32 0, i32 0
  %24 = bitcast i64* %arraydecay13 to i8*
  %25 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %memsize14 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %25, i32 0, i32 6
  %26 = load i32, i32* %memsize14, align 8, !tbaa !28
  %add.ptr15 = getelementptr inbounds i8, i8* %24, i32 %26
  %27 = load i8*, i8** %input.addr, align 4, !tbaa !3
  %28 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %memsize16 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %28, i32 0, i32 6
  %29 = load i32, i32* %memsize16, align 8, !tbaa !28
  %sub = sub i32 32, %29
  %call17 = call i8* @XXH_memcpy(i8* %add.ptr15, i8* %27, i32 %sub)
  %30 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v1 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %30, i32 0, i32 1
  %31 = load i64, i64* %v1, align 8, !tbaa !22
  %32 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %mem6418 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %32, i32 0, i32 5
  %arraydecay19 = getelementptr inbounds [4 x i64], [4 x i64]* %mem6418, i32 0, i32 0
  %add.ptr20 = getelementptr inbounds i64, i64* %arraydecay19, i32 0
  %33 = bitcast i64* %add.ptr20 to i8*
  %34 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call21 = call i64 @XXH_readLE64(i8* %33, i32 %34)
  %call22 = call i64 @XXH64_round(i64 %31, i64 %call21)
  %35 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v123 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %35, i32 0, i32 1
  store i64 %call22, i64* %v123, align 8, !tbaa !22
  %36 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v2 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %36, i32 0, i32 2
  %37 = load i64, i64* %v2, align 8, !tbaa !24
  %38 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %mem6424 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %38, i32 0, i32 5
  %arraydecay25 = getelementptr inbounds [4 x i64], [4 x i64]* %mem6424, i32 0, i32 0
  %add.ptr26 = getelementptr inbounds i64, i64* %arraydecay25, i32 1
  %39 = bitcast i64* %add.ptr26 to i8*
  %40 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call27 = call i64 @XXH_readLE64(i8* %39, i32 %40)
  %call28 = call i64 @XXH64_round(i64 %37, i64 %call27)
  %41 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v229 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %41, i32 0, i32 2
  store i64 %call28, i64* %v229, align 8, !tbaa !24
  %42 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v3 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %42, i32 0, i32 3
  %43 = load i64, i64* %v3, align 8, !tbaa !25
  %44 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %mem6430 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %44, i32 0, i32 5
  %arraydecay31 = getelementptr inbounds [4 x i64], [4 x i64]* %mem6430, i32 0, i32 0
  %add.ptr32 = getelementptr inbounds i64, i64* %arraydecay31, i32 2
  %45 = bitcast i64* %add.ptr32 to i8*
  %46 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call33 = call i64 @XXH_readLE64(i8* %45, i32 %46)
  %call34 = call i64 @XXH64_round(i64 %43, i64 %call33)
  %47 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v335 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %47, i32 0, i32 3
  store i64 %call34, i64* %v335, align 8, !tbaa !25
  %48 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v4 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %48, i32 0, i32 4
  %49 = load i64, i64* %v4, align 8, !tbaa !26
  %50 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %mem6436 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %50, i32 0, i32 5
  %arraydecay37 = getelementptr inbounds [4 x i64], [4 x i64]* %mem6436, i32 0, i32 0
  %add.ptr38 = getelementptr inbounds i64, i64* %arraydecay37, i32 3
  %51 = bitcast i64* %add.ptr38 to i8*
  %52 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call39 = call i64 @XXH_readLE64(i8* %51, i32 %52)
  %call40 = call i64 @XXH64_round(i64 %49, i64 %call39)
  %53 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v441 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %53, i32 0, i32 4
  store i64 %call40, i64* %v441, align 8, !tbaa !26
  %54 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %memsize42 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %54, i32 0, i32 6
  %55 = load i32, i32* %memsize42, align 8, !tbaa !28
  %sub43 = sub i32 32, %55
  %56 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr44 = getelementptr inbounds i8, i8* %56, i32 %sub43
  store i8* %add.ptr44, i8** %p, align 4, !tbaa !3
  %57 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %memsize45 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %57, i32 0, i32 6
  store i32 0, i32* %memsize45, align 8, !tbaa !28
  br label %if.end46

if.end46:                                         ; preds = %if.then11, %if.end9
  %58 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr47 = getelementptr inbounds i8, i8* %58, i32 32
  %59 = load i8*, i8** %bEnd, align 4, !tbaa !3
  %cmp48 = icmp ule i8* %add.ptr47, %59
  br i1 %cmp48, label %if.then50, label %if.end78

if.then50:                                        ; preds = %if.end46
  %60 = bitcast i8** %limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #6
  %61 = load i8*, i8** %bEnd, align 4, !tbaa !3
  %add.ptr51 = getelementptr inbounds i8, i8* %61, i32 -32
  store i8* %add.ptr51, i8** %limit, align 4, !tbaa !3
  %62 = bitcast i64* %v152 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %62) #6
  %63 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v153 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %63, i32 0, i32 1
  %64 = load i64, i64* %v153, align 8, !tbaa !22
  store i64 %64, i64* %v152, align 8, !tbaa !20
  %65 = bitcast i64* %v254 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %65) #6
  %66 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v255 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %66, i32 0, i32 2
  %67 = load i64, i64* %v255, align 8, !tbaa !24
  store i64 %67, i64* %v254, align 8, !tbaa !20
  %68 = bitcast i64* %v356 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %68) #6
  %69 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v357 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %69, i32 0, i32 3
  %70 = load i64, i64* %v357, align 8, !tbaa !25
  store i64 %70, i64* %v356, align 8, !tbaa !20
  %71 = bitcast i64* %v458 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %71) #6
  %72 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v459 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %72, i32 0, i32 4
  %73 = load i64, i64* %v459, align 8, !tbaa !26
  store i64 %73, i64* %v458, align 8, !tbaa !20
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then50
  %74 = load i64, i64* %v152, align 8, !tbaa !20
  %75 = load i8*, i8** %p, align 4, !tbaa !3
  %76 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call60 = call i64 @XXH_readLE64(i8* %75, i32 %76)
  %call61 = call i64 @XXH64_round(i64 %74, i64 %call60)
  store i64 %call61, i64* %v152, align 8, !tbaa !20
  %77 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr62 = getelementptr inbounds i8, i8* %77, i32 8
  store i8* %add.ptr62, i8** %p, align 4, !tbaa !3
  %78 = load i64, i64* %v254, align 8, !tbaa !20
  %79 = load i8*, i8** %p, align 4, !tbaa !3
  %80 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call63 = call i64 @XXH_readLE64(i8* %79, i32 %80)
  %call64 = call i64 @XXH64_round(i64 %78, i64 %call63)
  store i64 %call64, i64* %v254, align 8, !tbaa !20
  %81 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr65 = getelementptr inbounds i8, i8* %81, i32 8
  store i8* %add.ptr65, i8** %p, align 4, !tbaa !3
  %82 = load i64, i64* %v356, align 8, !tbaa !20
  %83 = load i8*, i8** %p, align 4, !tbaa !3
  %84 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call66 = call i64 @XXH_readLE64(i8* %83, i32 %84)
  %call67 = call i64 @XXH64_round(i64 %82, i64 %call66)
  store i64 %call67, i64* %v356, align 8, !tbaa !20
  %85 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr68 = getelementptr inbounds i8, i8* %85, i32 8
  store i8* %add.ptr68, i8** %p, align 4, !tbaa !3
  %86 = load i64, i64* %v458, align 8, !tbaa !20
  %87 = load i8*, i8** %p, align 4, !tbaa !3
  %88 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call69 = call i64 @XXH_readLE64(i8* %87, i32 %88)
  %call70 = call i64 @XXH64_round(i64 %86, i64 %call69)
  store i64 %call70, i64* %v458, align 8, !tbaa !20
  %89 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr71 = getelementptr inbounds i8, i8* %89, i32 8
  store i8* %add.ptr71, i8** %p, align 4, !tbaa !3
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %90 = load i8*, i8** %p, align 4, !tbaa !3
  %91 = load i8*, i8** %limit, align 4, !tbaa !3
  %cmp72 = icmp ule i8* %90, %91
  br i1 %cmp72, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %92 = load i64, i64* %v152, align 8, !tbaa !20
  %93 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v174 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %93, i32 0, i32 1
  store i64 %92, i64* %v174, align 8, !tbaa !22
  %94 = load i64, i64* %v254, align 8, !tbaa !20
  %95 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v275 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %95, i32 0, i32 2
  store i64 %94, i64* %v275, align 8, !tbaa !24
  %96 = load i64, i64* %v356, align 8, !tbaa !20
  %97 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v376 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %97, i32 0, i32 3
  store i64 %96, i64* %v376, align 8, !tbaa !25
  %98 = load i64, i64* %v458, align 8, !tbaa !20
  %99 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v477 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %99, i32 0, i32 4
  store i64 %98, i64* %v477, align 8, !tbaa !26
  %100 = bitcast i64* %v458 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %100) #6
  %101 = bitcast i64* %v356 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %101) #6
  %102 = bitcast i64* %v254 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %102) #6
  %103 = bitcast i64* %v152 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %103) #6
  %104 = bitcast i8** %limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #6
  br label %if.end78

if.end78:                                         ; preds = %do.end, %if.end46
  %105 = load i8*, i8** %p, align 4, !tbaa !3
  %106 = load i8*, i8** %bEnd, align 4, !tbaa !3
  %cmp79 = icmp ult i8* %105, %106
  br i1 %cmp79, label %if.then81, label %if.end89

if.then81:                                        ; preds = %if.end78
  %107 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %mem6482 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %107, i32 0, i32 5
  %arraydecay83 = getelementptr inbounds [4 x i64], [4 x i64]* %mem6482, i32 0, i32 0
  %108 = bitcast i64* %arraydecay83 to i8*
  %109 = load i8*, i8** %p, align 4, !tbaa !3
  %110 = load i8*, i8** %bEnd, align 4, !tbaa !3
  %111 = load i8*, i8** %p, align 4, !tbaa !3
  %sub.ptr.lhs.cast = ptrtoint i8* %110 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %111 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %call84 = call i8* @XXH_memcpy(i8* %108, i8* %109, i32 %sub.ptr.sub)
  %112 = load i8*, i8** %bEnd, align 4, !tbaa !3
  %113 = load i8*, i8** %p, align 4, !tbaa !3
  %sub.ptr.lhs.cast85 = ptrtoint i8* %112 to i32
  %sub.ptr.rhs.cast86 = ptrtoint i8* %113 to i32
  %sub.ptr.sub87 = sub i32 %sub.ptr.lhs.cast85, %sub.ptr.rhs.cast86
  %114 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %memsize88 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %114, i32 0, i32 6
  store i32 %sub.ptr.sub87, i32* %memsize88, align 8, !tbaa !28
  br label %if.end89

if.end89:                                         ; preds = %if.then81, %if.end78
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end89, %if.then4
  %115 = bitcast i8** %bEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #6
  %116 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
  ]

cleanup.cont:                                     ; preds = %cleanup
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %cleanup.cont, %cleanup, %if.then
  %117 = load i32, i32* %retval, align 4
  ret i32 %117

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden i64 @LZ4_XXH64_digest(%struct.XXH64_state_s* %state_in) #0 {
entry:
  %retval = alloca i64, align 8
  %state_in.addr = alloca %struct.XXH64_state_s*, align 4
  %endian_detected = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.XXH64_state_s* %state_in, %struct.XXH64_state_s** %state_in.addr, align 4, !tbaa !3
  %0 = bitcast i32* %endian_detected to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call i32 @XXH_isLittleEndian()
  store i32 %call, i32* %endian_detected, align 4, !tbaa !11
  %1 = load i32, i32* %endian_detected, align 4, !tbaa !11
  %cmp = icmp eq i32 %1, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state_in.addr, align 4, !tbaa !3
  %call1 = call i64 @XXH64_digest_endian(%struct.XXH64_state_s* %2, i32 1)
  store i64 %call1, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %entry
  %3 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state_in.addr, align 4, !tbaa !3
  %call2 = call i64 @XXH64_digest_endian(%struct.XXH64_state_s* %3, i32 0)
  store i64 %call2, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then
  %4 = bitcast i32* %endian_detected to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  %5 = load i64, i64* %retval, align 8
  ret i64 %5
}

; Function Attrs: alwaysinline nounwind
define internal i64 @XXH64_digest_endian(%struct.XXH64_state_s* %state, i32 %endian) #2 {
entry:
  %state.addr = alloca %struct.XXH64_state_s*, align 4
  %endian.addr = alloca i32, align 4
  %h64 = alloca i64, align 8
  %v1 = alloca i64, align 8
  %v2 = alloca i64, align 8
  %v3 = alloca i64, align 8
  %v4 = alloca i64, align 8
  store %struct.XXH64_state_s* %state, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  store i32 %endian, i32* %endian.addr, align 4, !tbaa !11
  %0 = bitcast i64* %h64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #6
  %1 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %total_len = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %1, i32 0, i32 0
  %2 = load i64, i64* %total_len, align 8, !tbaa !27
  %cmp = icmp uge i64 %2, 32
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i64* %v1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #6
  %4 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v11 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %4, i32 0, i32 1
  %5 = load i64, i64* %v11, align 8, !tbaa !22
  store i64 %5, i64* %v1, align 8, !tbaa !20
  %6 = bitcast i64* %v2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #6
  %7 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v22 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %7, i32 0, i32 2
  %8 = load i64, i64* %v22, align 8, !tbaa !24
  store i64 %8, i64* %v2, align 8, !tbaa !20
  %9 = bitcast i64* %v3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %9) #6
  %10 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v33 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %10, i32 0, i32 3
  %11 = load i64, i64* %v33, align 8, !tbaa !25
  store i64 %11, i64* %v3, align 8, !tbaa !20
  %12 = bitcast i64* %v4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %12) #6
  %13 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v44 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %13, i32 0, i32 4
  %14 = load i64, i64* %v44, align 8, !tbaa !26
  store i64 %14, i64* %v4, align 8, !tbaa !20
  %15 = load i64, i64* %v1, align 8, !tbaa !20
  %shl = shl i64 %15, 1
  %16 = load i64, i64* %v1, align 8, !tbaa !20
  %shr = lshr i64 %16, 63
  %or = or i64 %shl, %shr
  %17 = load i64, i64* %v2, align 8, !tbaa !20
  %shl5 = shl i64 %17, 7
  %18 = load i64, i64* %v2, align 8, !tbaa !20
  %shr6 = lshr i64 %18, 57
  %or7 = or i64 %shl5, %shr6
  %add = add i64 %or, %or7
  %19 = load i64, i64* %v3, align 8, !tbaa !20
  %shl8 = shl i64 %19, 12
  %20 = load i64, i64* %v3, align 8, !tbaa !20
  %shr9 = lshr i64 %20, 52
  %or10 = or i64 %shl8, %shr9
  %add11 = add i64 %add, %or10
  %21 = load i64, i64* %v4, align 8, !tbaa !20
  %shl12 = shl i64 %21, 18
  %22 = load i64, i64* %v4, align 8, !tbaa !20
  %shr13 = lshr i64 %22, 46
  %or14 = or i64 %shl12, %shr13
  %add15 = add i64 %add11, %or14
  store i64 %add15, i64* %h64, align 8, !tbaa !20
  %23 = load i64, i64* %h64, align 8, !tbaa !20
  %24 = load i64, i64* %v1, align 8, !tbaa !20
  %call = call i64 @XXH64_mergeRound(i64 %23, i64 %24)
  store i64 %call, i64* %h64, align 8, !tbaa !20
  %25 = load i64, i64* %h64, align 8, !tbaa !20
  %26 = load i64, i64* %v2, align 8, !tbaa !20
  %call16 = call i64 @XXH64_mergeRound(i64 %25, i64 %26)
  store i64 %call16, i64* %h64, align 8, !tbaa !20
  %27 = load i64, i64* %h64, align 8, !tbaa !20
  %28 = load i64, i64* %v3, align 8, !tbaa !20
  %call17 = call i64 @XXH64_mergeRound(i64 %27, i64 %28)
  store i64 %call17, i64* %h64, align 8, !tbaa !20
  %29 = load i64, i64* %h64, align 8, !tbaa !20
  %30 = load i64, i64* %v4, align 8, !tbaa !20
  %call18 = call i64 @XXH64_mergeRound(i64 %29, i64 %30)
  store i64 %call18, i64* %h64, align 8, !tbaa !20
  %31 = bitcast i64* %v4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %31) #6
  %32 = bitcast i64* %v3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %32) #6
  %33 = bitcast i64* %v2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %33) #6
  %34 = bitcast i64* %v1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %34) #6
  br label %if.end

if.else:                                          ; preds = %entry
  %35 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %v319 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %35, i32 0, i32 3
  %36 = load i64, i64* %v319, align 8, !tbaa !25
  %add20 = add i64 %36, 2870177450012600261
  store i64 %add20, i64* %h64, align 8, !tbaa !20
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %37 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %total_len21 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %37, i32 0, i32 0
  %38 = load i64, i64* %total_len21, align 8, !tbaa !27
  %39 = load i64, i64* %h64, align 8, !tbaa !20
  %add22 = add i64 %39, %38
  store i64 %add22, i64* %h64, align 8, !tbaa !20
  %40 = load i64, i64* %h64, align 8, !tbaa !20
  %41 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %mem64 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %41, i32 0, i32 5
  %arraydecay = getelementptr inbounds [4 x i64], [4 x i64]* %mem64, i32 0, i32 0
  %42 = bitcast i64* %arraydecay to i8*
  %43 = load %struct.XXH64_state_s*, %struct.XXH64_state_s** %state.addr, align 4, !tbaa !3
  %total_len23 = getelementptr inbounds %struct.XXH64_state_s, %struct.XXH64_state_s* %43, i32 0, i32 0
  %44 = load i64, i64* %total_len23, align 8, !tbaa !27
  %conv = trunc i64 %44 to i32
  %45 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call24 = call i64 @XXH64_finalize(i64 %40, i8* %42, i32 %conv, i32 %45, i32 0)
  %46 = bitcast i64* %h64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %46) #6
  ret i64 %call24
}

; Function Attrs: nounwind
define hidden void @LZ4_XXH64_canonicalFromHash(%struct.XXH64_canonical_t* %dst, i64 %hash) #0 {
entry:
  %dst.addr = alloca %struct.XXH64_canonical_t*, align 4
  %hash.addr = alloca i64, align 8
  store %struct.XXH64_canonical_t* %dst, %struct.XXH64_canonical_t** %dst.addr, align 4, !tbaa !3
  store i64 %hash, i64* %hash.addr, align 8, !tbaa !20
  %call = call i32 @XXH_isLittleEndian()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load i64, i64* %hash.addr, align 8, !tbaa !20
  %call1 = call i64 @XXH_swap64(i64 %0)
  store i64 %call1, i64* %hash.addr, align 8, !tbaa !20
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load %struct.XXH64_canonical_t*, %struct.XXH64_canonical_t** %dst.addr, align 4, !tbaa !3
  %2 = bitcast %struct.XXH64_canonical_t* %1 to i8*
  %3 = bitcast i64* %hash.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %2, i8* align 8 %3, i32 8, i1 false)
  ret void
}

; Function Attrs: nounwind
define internal i64 @XXH_swap64(i64 %x) #0 {
entry:
  %x.addr = alloca i64, align 8
  store i64 %x, i64* %x.addr, align 8, !tbaa !20
  %0 = load i64, i64* %x.addr, align 8, !tbaa !20
  %shl = shl i64 %0, 56
  %and = and i64 %shl, -72057594037927936
  %1 = load i64, i64* %x.addr, align 8, !tbaa !20
  %shl1 = shl i64 %1, 40
  %and2 = and i64 %shl1, 71776119061217280
  %or = or i64 %and, %and2
  %2 = load i64, i64* %x.addr, align 8, !tbaa !20
  %shl3 = shl i64 %2, 24
  %and4 = and i64 %shl3, 280375465082880
  %or5 = or i64 %or, %and4
  %3 = load i64, i64* %x.addr, align 8, !tbaa !20
  %shl6 = shl i64 %3, 8
  %and7 = and i64 %shl6, 1095216660480
  %or8 = or i64 %or5, %and7
  %4 = load i64, i64* %x.addr, align 8, !tbaa !20
  %shr = lshr i64 %4, 8
  %and9 = and i64 %shr, 4278190080
  %or10 = or i64 %or8, %and9
  %5 = load i64, i64* %x.addr, align 8, !tbaa !20
  %shr11 = lshr i64 %5, 24
  %and12 = and i64 %shr11, 16711680
  %or13 = or i64 %or10, %and12
  %6 = load i64, i64* %x.addr, align 8, !tbaa !20
  %shr14 = lshr i64 %6, 40
  %and15 = and i64 %shr14, 65280
  %or16 = or i64 %or13, %and15
  %7 = load i64, i64* %x.addr, align 8, !tbaa !20
  %shr17 = lshr i64 %7, 56
  %and18 = and i64 %shr17, 255
  %or19 = or i64 %or16, %and18
  ret i64 %or19
}

; Function Attrs: nounwind
define hidden i64 @LZ4_XXH64_hashFromCanonical(%struct.XXH64_canonical_t* %src) #0 {
entry:
  %src.addr = alloca %struct.XXH64_canonical_t*, align 4
  store %struct.XXH64_canonical_t* %src, %struct.XXH64_canonical_t** %src.addr, align 4, !tbaa !3
  %0 = load %struct.XXH64_canonical_t*, %struct.XXH64_canonical_t** %src.addr, align 4, !tbaa !3
  %1 = bitcast %struct.XXH64_canonical_t* %0 to i8*
  %call = call i64 @XXH_readBE64(i8* %1)
  ret i64 %call
}

; Function Attrs: nounwind
define internal i64 @XXH_readBE64(i8* %ptr) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !3
  %call = call i32 @XXH_isLittleEndian()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  %call1 = call i64 @XXH_read64(i8* %0)
  %call2 = call i64 @XXH_swap64(i64 %call1)
  br label %cond.end

cond.false:                                       ; preds = %entry
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  %call3 = call i64 @XXH_read64(i8* %1)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i64 [ %call2, %cond.true ], [ %call3, %cond.false ]
  ret i64 %cond
}

; Function Attrs: nounwind
define internal i32 @XXH32_round(i32 %seed, i32 %input) #0 {
entry:
  %seed.addr = alloca i32, align 4
  %input.addr = alloca i32, align 4
  store i32 %seed, i32* %seed.addr, align 4, !tbaa !9
  store i32 %input, i32* %input.addr, align 4, !tbaa !9
  %0 = load i32, i32* %input.addr, align 4, !tbaa !9
  %mul = mul i32 %0, -2048144777
  %1 = load i32, i32* %seed.addr, align 4, !tbaa !9
  %add = add i32 %1, %mul
  store i32 %add, i32* %seed.addr, align 4, !tbaa !9
  %2 = load i32, i32* %seed.addr, align 4, !tbaa !9
  %shl = shl i32 %2, 13
  %3 = load i32, i32* %seed.addr, align 4, !tbaa !9
  %shr = lshr i32 %3, 19
  %or = or i32 %shl, %shr
  store i32 %or, i32* %seed.addr, align 4, !tbaa !9
  %4 = load i32, i32* %seed.addr, align 4, !tbaa !9
  %mul1 = mul i32 %4, -1640531535
  store i32 %mul1, i32* %seed.addr, align 4, !tbaa !9
  %5 = load i32, i32* %seed.addr, align 4, !tbaa !9
  ret i32 %5
}

; Function Attrs: alwaysinline nounwind
define internal i32 @XXH_readLE32_align(i8* %ptr, i32 %endian, i32 %align) #2 {
entry:
  %retval = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  %endian.addr = alloca i32, align 4
  %align.addr = alloca i32, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !3
  store i32 %endian, i32* %endian.addr, align 4, !tbaa !11
  store i32 %align, i32* %align.addr, align 4, !tbaa !11
  %0 = load i32, i32* %align.addr, align 4, !tbaa !11
  %cmp = icmp eq i32 %0, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %cmp1 = icmp eq i32 %1, 1
  br i1 %cmp1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %2 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  %call = call i32 @XXH_read32(i8* %2)
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %3 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  %call2 = call i32 @XXH_read32(i8* %3)
  %call3 = call i32 @XXH_swap32(i32 %call2)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call, %cond.true ], [ %call3, %cond.false ]
  store i32 %cond, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %cmp4 = icmp eq i32 %4, 1
  br i1 %cmp4, label %cond.true5, label %cond.false6

cond.true5:                                       ; preds = %if.else
  %5 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  %6 = bitcast i8* %5 to i32*
  %7 = load i32, i32* %6, align 4, !tbaa !9
  br label %cond.end8

cond.false6:                                      ; preds = %if.else
  %8 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  %9 = bitcast i8* %8 to i32*
  %10 = load i32, i32* %9, align 4, !tbaa !9
  %call7 = call i32 @XXH_swap32(i32 %10)
  br label %cond.end8

cond.end8:                                        ; preds = %cond.false6, %cond.true5
  %cond9 = phi i32 [ %7, %cond.true5 ], [ %call7, %cond.false6 ]
  store i32 %cond9, i32* %retval, align 4
  br label %return

return:                                           ; preds = %cond.end8, %cond.end
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

; Function Attrs: nounwind
define internal i32 @XXH32_finalize(i32 %h32, i8* %ptr, i32 %len, i32 %endian, i32 %align) #0 {
entry:
  %retval = alloca i32, align 4
  %h32.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %endian.addr = alloca i32, align 4
  %align.addr = alloca i32, align 4
  %p = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i32 %h32, i32* %h32.addr, align 4, !tbaa !9
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !3
  store i32 %len, i32* %len.addr, align 4, !tbaa !7
  store i32 %endian, i32* %endian.addr, align 4, !tbaa !11
  store i32 %align, i32* %align.addr, align 4, !tbaa !11
  %0 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  store i8* %1, i8** %p, align 4, !tbaa !3
  %2 = load i32, i32* %len.addr, align 4, !tbaa !7
  %and = and i32 %2, 15
  switch i32 %and, label %sw.epilog [
    i32 12, label %sw.bb
    i32 8, label %sw.bb2
    i32 4, label %sw.bb11
    i32 13, label %sw.bb21
    i32 9, label %sw.bb30
    i32 5, label %sw.bb39
    i32 14, label %sw.bb55
    i32 10, label %sw.bb64
    i32 6, label %sw.bb73
    i32 15, label %sw.bb99
    i32 11, label %sw.bb108
    i32 7, label %sw.bb117
    i32 3, label %sw.bb126
    i32 2, label %sw.bb135
    i32 1, label %sw.bb144
    i32 0, label %sw.bb153
  ]

sw.bb:                                            ; preds = %entry
  %3 = load i8*, i8** %p, align 4, !tbaa !3
  %4 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %5 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call = call i32 @XXH_readLE32_align(i8* %3, i32 %4, i32 %5)
  %mul = mul i32 %call, -1028477379
  %6 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %add = add i32 %6, %mul
  store i32 %add, i32* %h32.addr, align 4, !tbaa !9
  %7 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr = getelementptr inbounds i8, i8* %7, i32 4
  store i8* %add.ptr, i8** %p, align 4, !tbaa !3
  %8 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shl = shl i32 %8, 17
  %9 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr = lshr i32 %9, 15
  %or = or i32 %shl, %shr
  %mul1 = mul i32 %or, 668265263
  store i32 %mul1, i32* %h32.addr, align 4, !tbaa !9
  br label %sw.bb2

sw.bb2:                                           ; preds = %entry, %sw.bb
  %10 = load i8*, i8** %p, align 4, !tbaa !3
  %11 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %12 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call3 = call i32 @XXH_readLE32_align(i8* %10, i32 %11, i32 %12)
  %mul4 = mul i32 %call3, -1028477379
  %13 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %add5 = add i32 %13, %mul4
  store i32 %add5, i32* %h32.addr, align 4, !tbaa !9
  %14 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr6 = getelementptr inbounds i8, i8* %14, i32 4
  store i8* %add.ptr6, i8** %p, align 4, !tbaa !3
  %15 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shl7 = shl i32 %15, 17
  %16 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr8 = lshr i32 %16, 15
  %or9 = or i32 %shl7, %shr8
  %mul10 = mul i32 %or9, 668265263
  store i32 %mul10, i32* %h32.addr, align 4, !tbaa !9
  br label %sw.bb11

sw.bb11:                                          ; preds = %entry, %sw.bb2
  %17 = load i8*, i8** %p, align 4, !tbaa !3
  %18 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %19 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call12 = call i32 @XXH_readLE32_align(i8* %17, i32 %18, i32 %19)
  %mul13 = mul i32 %call12, -1028477379
  %20 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %add14 = add i32 %20, %mul13
  store i32 %add14, i32* %h32.addr, align 4, !tbaa !9
  %21 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr15 = getelementptr inbounds i8, i8* %21, i32 4
  store i8* %add.ptr15, i8** %p, align 4, !tbaa !3
  %22 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shl16 = shl i32 %22, 17
  %23 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr17 = lshr i32 %23, 15
  %or18 = or i32 %shl16, %shr17
  %mul19 = mul i32 %or18, 668265263
  store i32 %mul19, i32* %h32.addr, align 4, !tbaa !9
  %24 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %call20 = call i32 @XXH32_avalanche(i32 %24)
  store i32 %call20, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.bb21:                                          ; preds = %entry
  %25 = load i8*, i8** %p, align 4, !tbaa !3
  %26 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %27 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call22 = call i32 @XXH_readLE32_align(i8* %25, i32 %26, i32 %27)
  %mul23 = mul i32 %call22, -1028477379
  %28 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %add24 = add i32 %28, %mul23
  store i32 %add24, i32* %h32.addr, align 4, !tbaa !9
  %29 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr25 = getelementptr inbounds i8, i8* %29, i32 4
  store i8* %add.ptr25, i8** %p, align 4, !tbaa !3
  %30 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shl26 = shl i32 %30, 17
  %31 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr27 = lshr i32 %31, 15
  %or28 = or i32 %shl26, %shr27
  %mul29 = mul i32 %or28, 668265263
  store i32 %mul29, i32* %h32.addr, align 4, !tbaa !9
  br label %sw.bb30

sw.bb30:                                          ; preds = %entry, %sw.bb21
  %32 = load i8*, i8** %p, align 4, !tbaa !3
  %33 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %34 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call31 = call i32 @XXH_readLE32_align(i8* %32, i32 %33, i32 %34)
  %mul32 = mul i32 %call31, -1028477379
  %35 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %add33 = add i32 %35, %mul32
  store i32 %add33, i32* %h32.addr, align 4, !tbaa !9
  %36 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr34 = getelementptr inbounds i8, i8* %36, i32 4
  store i8* %add.ptr34, i8** %p, align 4, !tbaa !3
  %37 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shl35 = shl i32 %37, 17
  %38 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr36 = lshr i32 %38, 15
  %or37 = or i32 %shl35, %shr36
  %mul38 = mul i32 %or37, 668265263
  store i32 %mul38, i32* %h32.addr, align 4, !tbaa !9
  br label %sw.bb39

sw.bb39:                                          ; preds = %entry, %sw.bb30
  %39 = load i8*, i8** %p, align 4, !tbaa !3
  %40 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %41 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call40 = call i32 @XXH_readLE32_align(i8* %39, i32 %40, i32 %41)
  %mul41 = mul i32 %call40, -1028477379
  %42 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %add42 = add i32 %42, %mul41
  store i32 %add42, i32* %h32.addr, align 4, !tbaa !9
  %43 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr43 = getelementptr inbounds i8, i8* %43, i32 4
  store i8* %add.ptr43, i8** %p, align 4, !tbaa !3
  %44 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shl44 = shl i32 %44, 17
  %45 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr45 = lshr i32 %45, 15
  %or46 = or i32 %shl44, %shr45
  %mul47 = mul i32 %or46, 668265263
  store i32 %mul47, i32* %h32.addr, align 4, !tbaa !9
  %46 = load i8*, i8** %p, align 4, !tbaa !3
  %incdec.ptr = getelementptr inbounds i8, i8* %46, i32 1
  store i8* %incdec.ptr, i8** %p, align 4, !tbaa !3
  %47 = load i8, i8* %46, align 1, !tbaa !11
  %conv = zext i8 %47 to i32
  %mul48 = mul i32 %conv, 374761393
  %48 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %add49 = add i32 %48, %mul48
  store i32 %add49, i32* %h32.addr, align 4, !tbaa !9
  %49 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shl50 = shl i32 %49, 11
  %50 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr51 = lshr i32 %50, 21
  %or52 = or i32 %shl50, %shr51
  %mul53 = mul i32 %or52, -1640531535
  store i32 %mul53, i32* %h32.addr, align 4, !tbaa !9
  %51 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %call54 = call i32 @XXH32_avalanche(i32 %51)
  store i32 %call54, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.bb55:                                          ; preds = %entry
  %52 = load i8*, i8** %p, align 4, !tbaa !3
  %53 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %54 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call56 = call i32 @XXH_readLE32_align(i8* %52, i32 %53, i32 %54)
  %mul57 = mul i32 %call56, -1028477379
  %55 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %add58 = add i32 %55, %mul57
  store i32 %add58, i32* %h32.addr, align 4, !tbaa !9
  %56 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr59 = getelementptr inbounds i8, i8* %56, i32 4
  store i8* %add.ptr59, i8** %p, align 4, !tbaa !3
  %57 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shl60 = shl i32 %57, 17
  %58 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr61 = lshr i32 %58, 15
  %or62 = or i32 %shl60, %shr61
  %mul63 = mul i32 %or62, 668265263
  store i32 %mul63, i32* %h32.addr, align 4, !tbaa !9
  br label %sw.bb64

sw.bb64:                                          ; preds = %entry, %sw.bb55
  %59 = load i8*, i8** %p, align 4, !tbaa !3
  %60 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %61 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call65 = call i32 @XXH_readLE32_align(i8* %59, i32 %60, i32 %61)
  %mul66 = mul i32 %call65, -1028477379
  %62 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %add67 = add i32 %62, %mul66
  store i32 %add67, i32* %h32.addr, align 4, !tbaa !9
  %63 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr68 = getelementptr inbounds i8, i8* %63, i32 4
  store i8* %add.ptr68, i8** %p, align 4, !tbaa !3
  %64 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shl69 = shl i32 %64, 17
  %65 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr70 = lshr i32 %65, 15
  %or71 = or i32 %shl69, %shr70
  %mul72 = mul i32 %or71, 668265263
  store i32 %mul72, i32* %h32.addr, align 4, !tbaa !9
  br label %sw.bb73

sw.bb73:                                          ; preds = %entry, %sw.bb64
  %66 = load i8*, i8** %p, align 4, !tbaa !3
  %67 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %68 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call74 = call i32 @XXH_readLE32_align(i8* %66, i32 %67, i32 %68)
  %mul75 = mul i32 %call74, -1028477379
  %69 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %add76 = add i32 %69, %mul75
  store i32 %add76, i32* %h32.addr, align 4, !tbaa !9
  %70 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr77 = getelementptr inbounds i8, i8* %70, i32 4
  store i8* %add.ptr77, i8** %p, align 4, !tbaa !3
  %71 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shl78 = shl i32 %71, 17
  %72 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr79 = lshr i32 %72, 15
  %or80 = or i32 %shl78, %shr79
  %mul81 = mul i32 %or80, 668265263
  store i32 %mul81, i32* %h32.addr, align 4, !tbaa !9
  %73 = load i8*, i8** %p, align 4, !tbaa !3
  %incdec.ptr82 = getelementptr inbounds i8, i8* %73, i32 1
  store i8* %incdec.ptr82, i8** %p, align 4, !tbaa !3
  %74 = load i8, i8* %73, align 1, !tbaa !11
  %conv83 = zext i8 %74 to i32
  %mul84 = mul i32 %conv83, 374761393
  %75 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %add85 = add i32 %75, %mul84
  store i32 %add85, i32* %h32.addr, align 4, !tbaa !9
  %76 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shl86 = shl i32 %76, 11
  %77 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr87 = lshr i32 %77, 21
  %or88 = or i32 %shl86, %shr87
  %mul89 = mul i32 %or88, -1640531535
  store i32 %mul89, i32* %h32.addr, align 4, !tbaa !9
  %78 = load i8*, i8** %p, align 4, !tbaa !3
  %incdec.ptr90 = getelementptr inbounds i8, i8* %78, i32 1
  store i8* %incdec.ptr90, i8** %p, align 4, !tbaa !3
  %79 = load i8, i8* %78, align 1, !tbaa !11
  %conv91 = zext i8 %79 to i32
  %mul92 = mul i32 %conv91, 374761393
  %80 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %add93 = add i32 %80, %mul92
  store i32 %add93, i32* %h32.addr, align 4, !tbaa !9
  %81 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shl94 = shl i32 %81, 11
  %82 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr95 = lshr i32 %82, 21
  %or96 = or i32 %shl94, %shr95
  %mul97 = mul i32 %or96, -1640531535
  store i32 %mul97, i32* %h32.addr, align 4, !tbaa !9
  %83 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %call98 = call i32 @XXH32_avalanche(i32 %83)
  store i32 %call98, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.bb99:                                          ; preds = %entry
  %84 = load i8*, i8** %p, align 4, !tbaa !3
  %85 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %86 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call100 = call i32 @XXH_readLE32_align(i8* %84, i32 %85, i32 %86)
  %mul101 = mul i32 %call100, -1028477379
  %87 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %add102 = add i32 %87, %mul101
  store i32 %add102, i32* %h32.addr, align 4, !tbaa !9
  %88 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr103 = getelementptr inbounds i8, i8* %88, i32 4
  store i8* %add.ptr103, i8** %p, align 4, !tbaa !3
  %89 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shl104 = shl i32 %89, 17
  %90 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr105 = lshr i32 %90, 15
  %or106 = or i32 %shl104, %shr105
  %mul107 = mul i32 %or106, 668265263
  store i32 %mul107, i32* %h32.addr, align 4, !tbaa !9
  br label %sw.bb108

sw.bb108:                                         ; preds = %entry, %sw.bb99
  %91 = load i8*, i8** %p, align 4, !tbaa !3
  %92 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %93 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call109 = call i32 @XXH_readLE32_align(i8* %91, i32 %92, i32 %93)
  %mul110 = mul i32 %call109, -1028477379
  %94 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %add111 = add i32 %94, %mul110
  store i32 %add111, i32* %h32.addr, align 4, !tbaa !9
  %95 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr112 = getelementptr inbounds i8, i8* %95, i32 4
  store i8* %add.ptr112, i8** %p, align 4, !tbaa !3
  %96 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shl113 = shl i32 %96, 17
  %97 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr114 = lshr i32 %97, 15
  %or115 = or i32 %shl113, %shr114
  %mul116 = mul i32 %or115, 668265263
  store i32 %mul116, i32* %h32.addr, align 4, !tbaa !9
  br label %sw.bb117

sw.bb117:                                         ; preds = %entry, %sw.bb108
  %98 = load i8*, i8** %p, align 4, !tbaa !3
  %99 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %100 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call118 = call i32 @XXH_readLE32_align(i8* %98, i32 %99, i32 %100)
  %mul119 = mul i32 %call118, -1028477379
  %101 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %add120 = add i32 %101, %mul119
  store i32 %add120, i32* %h32.addr, align 4, !tbaa !9
  %102 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr121 = getelementptr inbounds i8, i8* %102, i32 4
  store i8* %add.ptr121, i8** %p, align 4, !tbaa !3
  %103 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shl122 = shl i32 %103, 17
  %104 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr123 = lshr i32 %104, 15
  %or124 = or i32 %shl122, %shr123
  %mul125 = mul i32 %or124, 668265263
  store i32 %mul125, i32* %h32.addr, align 4, !tbaa !9
  br label %sw.bb126

sw.bb126:                                         ; preds = %entry, %sw.bb117
  %105 = load i8*, i8** %p, align 4, !tbaa !3
  %incdec.ptr127 = getelementptr inbounds i8, i8* %105, i32 1
  store i8* %incdec.ptr127, i8** %p, align 4, !tbaa !3
  %106 = load i8, i8* %105, align 1, !tbaa !11
  %conv128 = zext i8 %106 to i32
  %mul129 = mul i32 %conv128, 374761393
  %107 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %add130 = add i32 %107, %mul129
  store i32 %add130, i32* %h32.addr, align 4, !tbaa !9
  %108 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shl131 = shl i32 %108, 11
  %109 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr132 = lshr i32 %109, 21
  %or133 = or i32 %shl131, %shr132
  %mul134 = mul i32 %or133, -1640531535
  store i32 %mul134, i32* %h32.addr, align 4, !tbaa !9
  br label %sw.bb135

sw.bb135:                                         ; preds = %entry, %sw.bb126
  %110 = load i8*, i8** %p, align 4, !tbaa !3
  %incdec.ptr136 = getelementptr inbounds i8, i8* %110, i32 1
  store i8* %incdec.ptr136, i8** %p, align 4, !tbaa !3
  %111 = load i8, i8* %110, align 1, !tbaa !11
  %conv137 = zext i8 %111 to i32
  %mul138 = mul i32 %conv137, 374761393
  %112 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %add139 = add i32 %112, %mul138
  store i32 %add139, i32* %h32.addr, align 4, !tbaa !9
  %113 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shl140 = shl i32 %113, 11
  %114 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr141 = lshr i32 %114, 21
  %or142 = or i32 %shl140, %shr141
  %mul143 = mul i32 %or142, -1640531535
  store i32 %mul143, i32* %h32.addr, align 4, !tbaa !9
  br label %sw.bb144

sw.bb144:                                         ; preds = %entry, %sw.bb135
  %115 = load i8*, i8** %p, align 4, !tbaa !3
  %incdec.ptr145 = getelementptr inbounds i8, i8* %115, i32 1
  store i8* %incdec.ptr145, i8** %p, align 4, !tbaa !3
  %116 = load i8, i8* %115, align 1, !tbaa !11
  %conv146 = zext i8 %116 to i32
  %mul147 = mul i32 %conv146, 374761393
  %117 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %add148 = add i32 %117, %mul147
  store i32 %add148, i32* %h32.addr, align 4, !tbaa !9
  %118 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shl149 = shl i32 %118, 11
  %119 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr150 = lshr i32 %119, 21
  %or151 = or i32 %shl149, %shr150
  %mul152 = mul i32 %or151, -1640531535
  store i32 %mul152, i32* %h32.addr, align 4, !tbaa !9
  br label %sw.bb153

sw.bb153:                                         ; preds = %entry, %sw.bb144
  %120 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %call154 = call i32 @XXH32_avalanche(i32 %120)
  store i32 %call154, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.epilog:                                        ; preds = %entry
  call void @__assert_fail(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.1, i32 0, i32 0), i32 346, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @__func__.XXH32_finalize, i32 0, i32 0)) #7
  unreachable

cleanup:                                          ; preds = %sw.bb153, %sw.bb73, %sw.bb39, %sw.bb11
  %121 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #6
  %122 = load i32, i32* %retval, align 4
  ret i32 %122
}

; Function Attrs: nounwind
define internal i32 @XXH_read32(i8* %memPtr) #0 {
entry:
  %memPtr.addr = alloca i8*, align 4
  %val = alloca i32, align 4
  store i8* %memPtr, i8** %memPtr.addr, align 4, !tbaa !3
  %0 = bitcast i32* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %val to i8*
  %2 = load i8*, i8** %memPtr.addr, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 1 %2, i32 4, i1 false)
  %3 = load i32, i32* %val, align 4, !tbaa !9
  %4 = bitcast i32* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #6
  ret i32 %3
}

; Function Attrs: nounwind
define internal i32 @XXH32_avalanche(i32 %h32) #0 {
entry:
  %h32.addr = alloca i32, align 4
  store i32 %h32, i32* %h32.addr, align 4, !tbaa !9
  %0 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr = lshr i32 %0, 15
  %1 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %xor = xor i32 %1, %shr
  store i32 %xor, i32* %h32.addr, align 4, !tbaa !9
  %2 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %mul = mul i32 %2, -2048144777
  store i32 %mul, i32* %h32.addr, align 4, !tbaa !9
  %3 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr1 = lshr i32 %3, 13
  %4 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %xor2 = xor i32 %4, %shr1
  store i32 %xor2, i32* %h32.addr, align 4, !tbaa !9
  %5 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %mul3 = mul i32 %5, -1028477379
  store i32 %mul3, i32* %h32.addr, align 4, !tbaa !9
  %6 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %shr4 = lshr i32 %6, 16
  %7 = load i32, i32* %h32.addr, align 4, !tbaa !9
  %xor5 = xor i32 %7, %shr4
  store i32 %xor5, i32* %h32.addr, align 4, !tbaa !9
  %8 = load i32, i32* %h32.addr, align 4, !tbaa !9
  ret i32 %8
}

; Function Attrs: noreturn
declare void @__assert_fail(i8*, i8*, i32, i8*) #4

declare i8* @malloc(i32) #5

declare void @free(i8*) #5

; Function Attrs: nounwind
define internal i8* @XXH_memcpy(i8* %dest, i8* %src, i32 %size) #0 {
entry:
  %dest.addr = alloca i8*, align 4
  %src.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !3
  store i8* %src, i8** %src.addr, align 4, !tbaa !3
  store i32 %size, i32* %size.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %dest.addr, align 4, !tbaa !3
  %1 = load i8*, i8** %src.addr, align 4, !tbaa !3
  %2 = load i32, i32* %size.addr, align 4, !tbaa !7
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 1 %1, i32 %2, i1 false)
  ret i8* %0
}

; Function Attrs: alwaysinline nounwind
define internal i32 @XXH_readLE32(i8* %ptr, i32 %endian) #2 {
entry:
  %ptr.addr = alloca i8*, align 4
  %endian.addr = alloca i32, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !3
  store i32 %endian, i32* %endian.addr, align 4, !tbaa !11
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  %1 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call = call i32 @XXH_readLE32_align(i8* %0, i32 %1, i32 1)
  ret i32 %call
}

; Function Attrs: nounwind
define internal i64 @XXH64_round(i64 %acc, i64 %input) #0 {
entry:
  %acc.addr = alloca i64, align 8
  %input.addr = alloca i64, align 8
  store i64 %acc, i64* %acc.addr, align 8, !tbaa !20
  store i64 %input, i64* %input.addr, align 8, !tbaa !20
  %0 = load i64, i64* %input.addr, align 8, !tbaa !20
  %mul = mul i64 %0, -4417276706812531889
  %1 = load i64, i64* %acc.addr, align 8, !tbaa !20
  %add = add i64 %1, %mul
  store i64 %add, i64* %acc.addr, align 8, !tbaa !20
  %2 = load i64, i64* %acc.addr, align 8, !tbaa !20
  %shl = shl i64 %2, 31
  %3 = load i64, i64* %acc.addr, align 8, !tbaa !20
  %shr = lshr i64 %3, 33
  %or = or i64 %shl, %shr
  store i64 %or, i64* %acc.addr, align 8, !tbaa !20
  %4 = load i64, i64* %acc.addr, align 8, !tbaa !20
  %mul1 = mul i64 %4, -7046029288634856825
  store i64 %mul1, i64* %acc.addr, align 8, !tbaa !20
  %5 = load i64, i64* %acc.addr, align 8, !tbaa !20
  ret i64 %5
}

; Function Attrs: alwaysinline nounwind
define internal i64 @XXH_readLE64_align(i8* %ptr, i32 %endian, i32 %align) #2 {
entry:
  %retval = alloca i64, align 8
  %ptr.addr = alloca i8*, align 4
  %endian.addr = alloca i32, align 4
  %align.addr = alloca i32, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !3
  store i32 %endian, i32* %endian.addr, align 4, !tbaa !11
  store i32 %align, i32* %align.addr, align 4, !tbaa !11
  %0 = load i32, i32* %align.addr, align 4, !tbaa !11
  %cmp = icmp eq i32 %0, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %cmp1 = icmp eq i32 %1, 1
  br i1 %cmp1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %2 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  %call = call i64 @XXH_read64(i8* %2)
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %3 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  %call2 = call i64 @XXH_read64(i8* %3)
  %call3 = call i64 @XXH_swap64(i64 %call2)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i64 [ %call, %cond.true ], [ %call3, %cond.false ]
  store i64 %cond, i64* %retval, align 8
  br label %return

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %cmp4 = icmp eq i32 %4, 1
  br i1 %cmp4, label %cond.true5, label %cond.false6

cond.true5:                                       ; preds = %if.else
  %5 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  %6 = bitcast i8* %5 to i64*
  %7 = load i64, i64* %6, align 8, !tbaa !20
  br label %cond.end8

cond.false6:                                      ; preds = %if.else
  %8 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  %9 = bitcast i8* %8 to i64*
  %10 = load i64, i64* %9, align 8, !tbaa !20
  %call7 = call i64 @XXH_swap64(i64 %10)
  br label %cond.end8

cond.end8:                                        ; preds = %cond.false6, %cond.true5
  %cond9 = phi i64 [ %7, %cond.true5 ], [ %call7, %cond.false6 ]
  store i64 %cond9, i64* %retval, align 8
  br label %return

return:                                           ; preds = %cond.end8, %cond.end
  %11 = load i64, i64* %retval, align 8
  ret i64 %11
}

; Function Attrs: nounwind
define internal i64 @XXH64_mergeRound(i64 %acc, i64 %val) #0 {
entry:
  %acc.addr = alloca i64, align 8
  %val.addr = alloca i64, align 8
  store i64 %acc, i64* %acc.addr, align 8, !tbaa !20
  store i64 %val, i64* %val.addr, align 8, !tbaa !20
  %0 = load i64, i64* %val.addr, align 8, !tbaa !20
  %call = call i64 @XXH64_round(i64 0, i64 %0)
  store i64 %call, i64* %val.addr, align 8, !tbaa !20
  %1 = load i64, i64* %val.addr, align 8, !tbaa !20
  %2 = load i64, i64* %acc.addr, align 8, !tbaa !20
  %xor = xor i64 %2, %1
  store i64 %xor, i64* %acc.addr, align 8, !tbaa !20
  %3 = load i64, i64* %acc.addr, align 8, !tbaa !20
  %mul = mul i64 %3, -7046029288634856825
  %add = add i64 %mul, -8796714831421723037
  store i64 %add, i64* %acc.addr, align 8, !tbaa !20
  %4 = load i64, i64* %acc.addr, align 8, !tbaa !20
  ret i64 %4
}

; Function Attrs: nounwind
define internal i64 @XXH64_finalize(i64 %h64, i8* %ptr, i32 %len, i32 %endian, i32 %align) #0 {
entry:
  %retval = alloca i64, align 8
  %h64.addr = alloca i64, align 8
  %ptr.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %endian.addr = alloca i32, align 4
  %align.addr = alloca i32, align 4
  %p = alloca i8*, align 4
  %k1 = alloca i64, align 8
  %k13 = alloca i64, align 8
  %k114 = alloca i64, align 8
  %cleanup.dest.slot = alloca i32, align 4
  %k126 = alloca i64, align 8
  %k137 = alloca i64, align 8
  %k148 = alloca i64, align 8
  %k170 = alloca i64, align 8
  %k181 = alloca i64, align 8
  %k192 = alloca i64, align 8
  %k1111 = alloca i64, align 8
  %k1122 = alloca i64, align 8
  %k1133 = alloca i64, align 8
  %k1164 = alloca i64, align 8
  %k1175 = alloca i64, align 8
  %k1186 = alloca i64, align 8
  %k1214 = alloca i64, align 8
  %k1225 = alloca i64, align 8
  %k1236 = alloca i64, align 8
  %k1275 = alloca i64, align 8
  %k1286 = alloca i64, align 8
  %k1297 = alloca i64, align 8
  %k1333 = alloca i64, align 8
  %k1344 = alloca i64, align 8
  %k1355 = alloca i64, align 8
  store i64 %h64, i64* %h64.addr, align 8, !tbaa !20
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !3
  store i32 %len, i32* %len.addr, align 4, !tbaa !7
  store i32 %endian, i32* %endian.addr, align 4, !tbaa !11
  store i32 %align, i32* %align.addr, align 4, !tbaa !11
  %0 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  store i8* %1, i8** %p, align 4, !tbaa !3
  %2 = load i32, i32* %len.addr, align 4, !tbaa !7
  %and = and i32 %2, 31
  switch i32 %and, label %sw.epilog [
    i32 24, label %sw.bb
    i32 16, label %sw.bb2
    i32 8, label %sw.bb13
    i32 28, label %sw.bb25
    i32 20, label %sw.bb36
    i32 12, label %sw.bb47
    i32 4, label %sw.bb58
    i32 25, label %sw.bb69
    i32 17, label %sw.bb80
    i32 9, label %sw.bb91
    i32 29, label %sw.bb110
    i32 21, label %sw.bb121
    i32 13, label %sw.bb132
    i32 5, label %sw.bb143
    i32 26, label %sw.bb163
    i32 18, label %sw.bb174
    i32 10, label %sw.bb185
    i32 30, label %sw.bb213
    i32 22, label %sw.bb224
    i32 14, label %sw.bb235
    i32 6, label %sw.bb246
    i32 27, label %sw.bb274
    i32 19, label %sw.bb285
    i32 11, label %sw.bb296
    i32 31, label %sw.bb332
    i32 23, label %sw.bb343
    i32 15, label %sw.bb354
    i32 7, label %sw.bb365
    i32 3, label %sw.bb376
    i32 2, label %sw.bb385
    i32 1, label %sw.bb394
    i32 0, label %sw.bb403
  ]

sw.bb:                                            ; preds = %entry
  %3 = bitcast i64* %k1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #6
  %4 = load i8*, i8** %p, align 4, !tbaa !3
  %5 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %6 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call = call i64 @XXH_readLE64_align(i8* %4, i32 %5, i32 %6)
  %call1 = call i64 @XXH64_round(i64 0, i64 %call)
  store i64 %call1, i64* %k1, align 8, !tbaa !20
  %7 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr = getelementptr inbounds i8, i8* %7, i32 8
  store i8* %add.ptr, i8** %p, align 4, !tbaa !3
  %8 = load i64, i64* %k1, align 8, !tbaa !20
  %9 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor = xor i64 %9, %8
  store i64 %xor, i64* %h64.addr, align 8, !tbaa !20
  %10 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl = shl i64 %10, 27
  %11 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr = lshr i64 %11, 37
  %or = or i64 %shl, %shr
  %mul = mul i64 %or, -7046029288634856825
  %add = add i64 %mul, -8796714831421723037
  store i64 %add, i64* %h64.addr, align 8, !tbaa !20
  %12 = bitcast i64* %k1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %12) #6
  br label %sw.bb2

sw.bb2:                                           ; preds = %entry, %sw.bb
  %13 = bitcast i64* %k13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %13) #6
  %14 = load i8*, i8** %p, align 4, !tbaa !3
  %15 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %16 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call4 = call i64 @XXH_readLE64_align(i8* %14, i32 %15, i32 %16)
  %call5 = call i64 @XXH64_round(i64 0, i64 %call4)
  store i64 %call5, i64* %k13, align 8, !tbaa !20
  %17 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr6 = getelementptr inbounds i8, i8* %17, i32 8
  store i8* %add.ptr6, i8** %p, align 4, !tbaa !3
  %18 = load i64, i64* %k13, align 8, !tbaa !20
  %19 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor7 = xor i64 %19, %18
  store i64 %xor7, i64* %h64.addr, align 8, !tbaa !20
  %20 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl8 = shl i64 %20, 27
  %21 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr9 = lshr i64 %21, 37
  %or10 = or i64 %shl8, %shr9
  %mul11 = mul i64 %or10, -7046029288634856825
  %add12 = add i64 %mul11, -8796714831421723037
  store i64 %add12, i64* %h64.addr, align 8, !tbaa !20
  %22 = bitcast i64* %k13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %22) #6
  br label %sw.bb13

sw.bb13:                                          ; preds = %entry, %sw.bb2
  %23 = bitcast i64* %k114 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %23) #6
  %24 = load i8*, i8** %p, align 4, !tbaa !3
  %25 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %26 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call15 = call i64 @XXH_readLE64_align(i8* %24, i32 %25, i32 %26)
  %call16 = call i64 @XXH64_round(i64 0, i64 %call15)
  store i64 %call16, i64* %k114, align 8, !tbaa !20
  %27 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr17 = getelementptr inbounds i8, i8* %27, i32 8
  store i8* %add.ptr17, i8** %p, align 4, !tbaa !3
  %28 = load i64, i64* %k114, align 8, !tbaa !20
  %29 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor18 = xor i64 %29, %28
  store i64 %xor18, i64* %h64.addr, align 8, !tbaa !20
  %30 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl19 = shl i64 %30, 27
  %31 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr20 = lshr i64 %31, 37
  %or21 = or i64 %shl19, %shr20
  %mul22 = mul i64 %or21, -7046029288634856825
  %add23 = add i64 %mul22, -8796714831421723037
  store i64 %add23, i64* %h64.addr, align 8, !tbaa !20
  %32 = bitcast i64* %k114 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %32) #6
  %33 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %call24 = call i64 @XXH64_avalanche(i64 %33)
  store i64 %call24, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.bb25:                                          ; preds = %entry
  %34 = bitcast i64* %k126 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %34) #6
  %35 = load i8*, i8** %p, align 4, !tbaa !3
  %36 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %37 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call27 = call i64 @XXH_readLE64_align(i8* %35, i32 %36, i32 %37)
  %call28 = call i64 @XXH64_round(i64 0, i64 %call27)
  store i64 %call28, i64* %k126, align 8, !tbaa !20
  %38 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr29 = getelementptr inbounds i8, i8* %38, i32 8
  store i8* %add.ptr29, i8** %p, align 4, !tbaa !3
  %39 = load i64, i64* %k126, align 8, !tbaa !20
  %40 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor30 = xor i64 %40, %39
  store i64 %xor30, i64* %h64.addr, align 8, !tbaa !20
  %41 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl31 = shl i64 %41, 27
  %42 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr32 = lshr i64 %42, 37
  %or33 = or i64 %shl31, %shr32
  %mul34 = mul i64 %or33, -7046029288634856825
  %add35 = add i64 %mul34, -8796714831421723037
  store i64 %add35, i64* %h64.addr, align 8, !tbaa !20
  %43 = bitcast i64* %k126 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %43) #6
  br label %sw.bb36

sw.bb36:                                          ; preds = %entry, %sw.bb25
  %44 = bitcast i64* %k137 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %44) #6
  %45 = load i8*, i8** %p, align 4, !tbaa !3
  %46 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %47 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call38 = call i64 @XXH_readLE64_align(i8* %45, i32 %46, i32 %47)
  %call39 = call i64 @XXH64_round(i64 0, i64 %call38)
  store i64 %call39, i64* %k137, align 8, !tbaa !20
  %48 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr40 = getelementptr inbounds i8, i8* %48, i32 8
  store i8* %add.ptr40, i8** %p, align 4, !tbaa !3
  %49 = load i64, i64* %k137, align 8, !tbaa !20
  %50 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor41 = xor i64 %50, %49
  store i64 %xor41, i64* %h64.addr, align 8, !tbaa !20
  %51 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl42 = shl i64 %51, 27
  %52 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr43 = lshr i64 %52, 37
  %or44 = or i64 %shl42, %shr43
  %mul45 = mul i64 %or44, -7046029288634856825
  %add46 = add i64 %mul45, -8796714831421723037
  store i64 %add46, i64* %h64.addr, align 8, !tbaa !20
  %53 = bitcast i64* %k137 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %53) #6
  br label %sw.bb47

sw.bb47:                                          ; preds = %entry, %sw.bb36
  %54 = bitcast i64* %k148 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %54) #6
  %55 = load i8*, i8** %p, align 4, !tbaa !3
  %56 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %57 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call49 = call i64 @XXH_readLE64_align(i8* %55, i32 %56, i32 %57)
  %call50 = call i64 @XXH64_round(i64 0, i64 %call49)
  store i64 %call50, i64* %k148, align 8, !tbaa !20
  %58 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr51 = getelementptr inbounds i8, i8* %58, i32 8
  store i8* %add.ptr51, i8** %p, align 4, !tbaa !3
  %59 = load i64, i64* %k148, align 8, !tbaa !20
  %60 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor52 = xor i64 %60, %59
  store i64 %xor52, i64* %h64.addr, align 8, !tbaa !20
  %61 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl53 = shl i64 %61, 27
  %62 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr54 = lshr i64 %62, 37
  %or55 = or i64 %shl53, %shr54
  %mul56 = mul i64 %or55, -7046029288634856825
  %add57 = add i64 %mul56, -8796714831421723037
  store i64 %add57, i64* %h64.addr, align 8, !tbaa !20
  %63 = bitcast i64* %k148 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %63) #6
  br label %sw.bb58

sw.bb58:                                          ; preds = %entry, %sw.bb47
  %64 = load i8*, i8** %p, align 4, !tbaa !3
  %65 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %66 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call59 = call i32 @XXH_readLE32_align(i8* %64, i32 %65, i32 %66)
  %conv = zext i32 %call59 to i64
  %mul60 = mul i64 %conv, -7046029288634856825
  %67 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor61 = xor i64 %67, %mul60
  store i64 %xor61, i64* %h64.addr, align 8, !tbaa !20
  %68 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr62 = getelementptr inbounds i8, i8* %68, i32 4
  store i8* %add.ptr62, i8** %p, align 4, !tbaa !3
  %69 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl63 = shl i64 %69, 23
  %70 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr64 = lshr i64 %70, 41
  %or65 = or i64 %shl63, %shr64
  %mul66 = mul i64 %or65, -4417276706812531889
  %add67 = add i64 %mul66, 1609587929392839161
  store i64 %add67, i64* %h64.addr, align 8, !tbaa !20
  %71 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %call68 = call i64 @XXH64_avalanche(i64 %71)
  store i64 %call68, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.bb69:                                          ; preds = %entry
  %72 = bitcast i64* %k170 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %72) #6
  %73 = load i8*, i8** %p, align 4, !tbaa !3
  %74 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %75 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call71 = call i64 @XXH_readLE64_align(i8* %73, i32 %74, i32 %75)
  %call72 = call i64 @XXH64_round(i64 0, i64 %call71)
  store i64 %call72, i64* %k170, align 8, !tbaa !20
  %76 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr73 = getelementptr inbounds i8, i8* %76, i32 8
  store i8* %add.ptr73, i8** %p, align 4, !tbaa !3
  %77 = load i64, i64* %k170, align 8, !tbaa !20
  %78 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor74 = xor i64 %78, %77
  store i64 %xor74, i64* %h64.addr, align 8, !tbaa !20
  %79 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl75 = shl i64 %79, 27
  %80 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr76 = lshr i64 %80, 37
  %or77 = or i64 %shl75, %shr76
  %mul78 = mul i64 %or77, -7046029288634856825
  %add79 = add i64 %mul78, -8796714831421723037
  store i64 %add79, i64* %h64.addr, align 8, !tbaa !20
  %81 = bitcast i64* %k170 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %81) #6
  br label %sw.bb80

sw.bb80:                                          ; preds = %entry, %sw.bb69
  %82 = bitcast i64* %k181 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %82) #6
  %83 = load i8*, i8** %p, align 4, !tbaa !3
  %84 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %85 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call82 = call i64 @XXH_readLE64_align(i8* %83, i32 %84, i32 %85)
  %call83 = call i64 @XXH64_round(i64 0, i64 %call82)
  store i64 %call83, i64* %k181, align 8, !tbaa !20
  %86 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr84 = getelementptr inbounds i8, i8* %86, i32 8
  store i8* %add.ptr84, i8** %p, align 4, !tbaa !3
  %87 = load i64, i64* %k181, align 8, !tbaa !20
  %88 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor85 = xor i64 %88, %87
  store i64 %xor85, i64* %h64.addr, align 8, !tbaa !20
  %89 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl86 = shl i64 %89, 27
  %90 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr87 = lshr i64 %90, 37
  %or88 = or i64 %shl86, %shr87
  %mul89 = mul i64 %or88, -7046029288634856825
  %add90 = add i64 %mul89, -8796714831421723037
  store i64 %add90, i64* %h64.addr, align 8, !tbaa !20
  %91 = bitcast i64* %k181 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %91) #6
  br label %sw.bb91

sw.bb91:                                          ; preds = %entry, %sw.bb80
  %92 = bitcast i64* %k192 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %92) #6
  %93 = load i8*, i8** %p, align 4, !tbaa !3
  %94 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %95 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call93 = call i64 @XXH_readLE64_align(i8* %93, i32 %94, i32 %95)
  %call94 = call i64 @XXH64_round(i64 0, i64 %call93)
  store i64 %call94, i64* %k192, align 8, !tbaa !20
  %96 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr95 = getelementptr inbounds i8, i8* %96, i32 8
  store i8* %add.ptr95, i8** %p, align 4, !tbaa !3
  %97 = load i64, i64* %k192, align 8, !tbaa !20
  %98 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor96 = xor i64 %98, %97
  store i64 %xor96, i64* %h64.addr, align 8, !tbaa !20
  %99 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl97 = shl i64 %99, 27
  %100 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr98 = lshr i64 %100, 37
  %or99 = or i64 %shl97, %shr98
  %mul100 = mul i64 %or99, -7046029288634856825
  %add101 = add i64 %mul100, -8796714831421723037
  store i64 %add101, i64* %h64.addr, align 8, !tbaa !20
  %101 = bitcast i64* %k192 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %101) #6
  %102 = load i8*, i8** %p, align 4, !tbaa !3
  %incdec.ptr = getelementptr inbounds i8, i8* %102, i32 1
  store i8* %incdec.ptr, i8** %p, align 4, !tbaa !3
  %103 = load i8, i8* %102, align 1, !tbaa !11
  %conv102 = zext i8 %103 to i64
  %mul103 = mul i64 %conv102, 2870177450012600261
  %104 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor104 = xor i64 %104, %mul103
  store i64 %xor104, i64* %h64.addr, align 8, !tbaa !20
  %105 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl105 = shl i64 %105, 11
  %106 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr106 = lshr i64 %106, 53
  %or107 = or i64 %shl105, %shr106
  %mul108 = mul i64 %or107, -7046029288634856825
  store i64 %mul108, i64* %h64.addr, align 8, !tbaa !20
  %107 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %call109 = call i64 @XXH64_avalanche(i64 %107)
  store i64 %call109, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.bb110:                                         ; preds = %entry
  %108 = bitcast i64* %k1111 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %108) #6
  %109 = load i8*, i8** %p, align 4, !tbaa !3
  %110 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %111 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call112 = call i64 @XXH_readLE64_align(i8* %109, i32 %110, i32 %111)
  %call113 = call i64 @XXH64_round(i64 0, i64 %call112)
  store i64 %call113, i64* %k1111, align 8, !tbaa !20
  %112 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr114 = getelementptr inbounds i8, i8* %112, i32 8
  store i8* %add.ptr114, i8** %p, align 4, !tbaa !3
  %113 = load i64, i64* %k1111, align 8, !tbaa !20
  %114 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor115 = xor i64 %114, %113
  store i64 %xor115, i64* %h64.addr, align 8, !tbaa !20
  %115 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl116 = shl i64 %115, 27
  %116 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr117 = lshr i64 %116, 37
  %or118 = or i64 %shl116, %shr117
  %mul119 = mul i64 %or118, -7046029288634856825
  %add120 = add i64 %mul119, -8796714831421723037
  store i64 %add120, i64* %h64.addr, align 8, !tbaa !20
  %117 = bitcast i64* %k1111 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %117) #6
  br label %sw.bb121

sw.bb121:                                         ; preds = %entry, %sw.bb110
  %118 = bitcast i64* %k1122 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %118) #6
  %119 = load i8*, i8** %p, align 4, !tbaa !3
  %120 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %121 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call123 = call i64 @XXH_readLE64_align(i8* %119, i32 %120, i32 %121)
  %call124 = call i64 @XXH64_round(i64 0, i64 %call123)
  store i64 %call124, i64* %k1122, align 8, !tbaa !20
  %122 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr125 = getelementptr inbounds i8, i8* %122, i32 8
  store i8* %add.ptr125, i8** %p, align 4, !tbaa !3
  %123 = load i64, i64* %k1122, align 8, !tbaa !20
  %124 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor126 = xor i64 %124, %123
  store i64 %xor126, i64* %h64.addr, align 8, !tbaa !20
  %125 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl127 = shl i64 %125, 27
  %126 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr128 = lshr i64 %126, 37
  %or129 = or i64 %shl127, %shr128
  %mul130 = mul i64 %or129, -7046029288634856825
  %add131 = add i64 %mul130, -8796714831421723037
  store i64 %add131, i64* %h64.addr, align 8, !tbaa !20
  %127 = bitcast i64* %k1122 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %127) #6
  br label %sw.bb132

sw.bb132:                                         ; preds = %entry, %sw.bb121
  %128 = bitcast i64* %k1133 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %128) #6
  %129 = load i8*, i8** %p, align 4, !tbaa !3
  %130 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %131 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call134 = call i64 @XXH_readLE64_align(i8* %129, i32 %130, i32 %131)
  %call135 = call i64 @XXH64_round(i64 0, i64 %call134)
  store i64 %call135, i64* %k1133, align 8, !tbaa !20
  %132 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr136 = getelementptr inbounds i8, i8* %132, i32 8
  store i8* %add.ptr136, i8** %p, align 4, !tbaa !3
  %133 = load i64, i64* %k1133, align 8, !tbaa !20
  %134 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor137 = xor i64 %134, %133
  store i64 %xor137, i64* %h64.addr, align 8, !tbaa !20
  %135 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl138 = shl i64 %135, 27
  %136 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr139 = lshr i64 %136, 37
  %or140 = or i64 %shl138, %shr139
  %mul141 = mul i64 %or140, -7046029288634856825
  %add142 = add i64 %mul141, -8796714831421723037
  store i64 %add142, i64* %h64.addr, align 8, !tbaa !20
  %137 = bitcast i64* %k1133 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %137) #6
  br label %sw.bb143

sw.bb143:                                         ; preds = %entry, %sw.bb132
  %138 = load i8*, i8** %p, align 4, !tbaa !3
  %139 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %140 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call144 = call i32 @XXH_readLE32_align(i8* %138, i32 %139, i32 %140)
  %conv145 = zext i32 %call144 to i64
  %mul146 = mul i64 %conv145, -7046029288634856825
  %141 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor147 = xor i64 %141, %mul146
  store i64 %xor147, i64* %h64.addr, align 8, !tbaa !20
  %142 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr148 = getelementptr inbounds i8, i8* %142, i32 4
  store i8* %add.ptr148, i8** %p, align 4, !tbaa !3
  %143 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl149 = shl i64 %143, 23
  %144 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr150 = lshr i64 %144, 41
  %or151 = or i64 %shl149, %shr150
  %mul152 = mul i64 %or151, -4417276706812531889
  %add153 = add i64 %mul152, 1609587929392839161
  store i64 %add153, i64* %h64.addr, align 8, !tbaa !20
  %145 = load i8*, i8** %p, align 4, !tbaa !3
  %incdec.ptr154 = getelementptr inbounds i8, i8* %145, i32 1
  store i8* %incdec.ptr154, i8** %p, align 4, !tbaa !3
  %146 = load i8, i8* %145, align 1, !tbaa !11
  %conv155 = zext i8 %146 to i64
  %mul156 = mul i64 %conv155, 2870177450012600261
  %147 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor157 = xor i64 %147, %mul156
  store i64 %xor157, i64* %h64.addr, align 8, !tbaa !20
  %148 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl158 = shl i64 %148, 11
  %149 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr159 = lshr i64 %149, 53
  %or160 = or i64 %shl158, %shr159
  %mul161 = mul i64 %or160, -7046029288634856825
  store i64 %mul161, i64* %h64.addr, align 8, !tbaa !20
  %150 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %call162 = call i64 @XXH64_avalanche(i64 %150)
  store i64 %call162, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.bb163:                                         ; preds = %entry
  %151 = bitcast i64* %k1164 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %151) #6
  %152 = load i8*, i8** %p, align 4, !tbaa !3
  %153 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %154 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call165 = call i64 @XXH_readLE64_align(i8* %152, i32 %153, i32 %154)
  %call166 = call i64 @XXH64_round(i64 0, i64 %call165)
  store i64 %call166, i64* %k1164, align 8, !tbaa !20
  %155 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr167 = getelementptr inbounds i8, i8* %155, i32 8
  store i8* %add.ptr167, i8** %p, align 4, !tbaa !3
  %156 = load i64, i64* %k1164, align 8, !tbaa !20
  %157 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor168 = xor i64 %157, %156
  store i64 %xor168, i64* %h64.addr, align 8, !tbaa !20
  %158 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl169 = shl i64 %158, 27
  %159 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr170 = lshr i64 %159, 37
  %or171 = or i64 %shl169, %shr170
  %mul172 = mul i64 %or171, -7046029288634856825
  %add173 = add i64 %mul172, -8796714831421723037
  store i64 %add173, i64* %h64.addr, align 8, !tbaa !20
  %160 = bitcast i64* %k1164 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %160) #6
  br label %sw.bb174

sw.bb174:                                         ; preds = %entry, %sw.bb163
  %161 = bitcast i64* %k1175 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %161) #6
  %162 = load i8*, i8** %p, align 4, !tbaa !3
  %163 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %164 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call176 = call i64 @XXH_readLE64_align(i8* %162, i32 %163, i32 %164)
  %call177 = call i64 @XXH64_round(i64 0, i64 %call176)
  store i64 %call177, i64* %k1175, align 8, !tbaa !20
  %165 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr178 = getelementptr inbounds i8, i8* %165, i32 8
  store i8* %add.ptr178, i8** %p, align 4, !tbaa !3
  %166 = load i64, i64* %k1175, align 8, !tbaa !20
  %167 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor179 = xor i64 %167, %166
  store i64 %xor179, i64* %h64.addr, align 8, !tbaa !20
  %168 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl180 = shl i64 %168, 27
  %169 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr181 = lshr i64 %169, 37
  %or182 = or i64 %shl180, %shr181
  %mul183 = mul i64 %or182, -7046029288634856825
  %add184 = add i64 %mul183, -8796714831421723037
  store i64 %add184, i64* %h64.addr, align 8, !tbaa !20
  %170 = bitcast i64* %k1175 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %170) #6
  br label %sw.bb185

sw.bb185:                                         ; preds = %entry, %sw.bb174
  %171 = bitcast i64* %k1186 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %171) #6
  %172 = load i8*, i8** %p, align 4, !tbaa !3
  %173 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %174 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call187 = call i64 @XXH_readLE64_align(i8* %172, i32 %173, i32 %174)
  %call188 = call i64 @XXH64_round(i64 0, i64 %call187)
  store i64 %call188, i64* %k1186, align 8, !tbaa !20
  %175 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr189 = getelementptr inbounds i8, i8* %175, i32 8
  store i8* %add.ptr189, i8** %p, align 4, !tbaa !3
  %176 = load i64, i64* %k1186, align 8, !tbaa !20
  %177 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor190 = xor i64 %177, %176
  store i64 %xor190, i64* %h64.addr, align 8, !tbaa !20
  %178 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl191 = shl i64 %178, 27
  %179 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr192 = lshr i64 %179, 37
  %or193 = or i64 %shl191, %shr192
  %mul194 = mul i64 %or193, -7046029288634856825
  %add195 = add i64 %mul194, -8796714831421723037
  store i64 %add195, i64* %h64.addr, align 8, !tbaa !20
  %180 = bitcast i64* %k1186 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %180) #6
  %181 = load i8*, i8** %p, align 4, !tbaa !3
  %incdec.ptr196 = getelementptr inbounds i8, i8* %181, i32 1
  store i8* %incdec.ptr196, i8** %p, align 4, !tbaa !3
  %182 = load i8, i8* %181, align 1, !tbaa !11
  %conv197 = zext i8 %182 to i64
  %mul198 = mul i64 %conv197, 2870177450012600261
  %183 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor199 = xor i64 %183, %mul198
  store i64 %xor199, i64* %h64.addr, align 8, !tbaa !20
  %184 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl200 = shl i64 %184, 11
  %185 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr201 = lshr i64 %185, 53
  %or202 = or i64 %shl200, %shr201
  %mul203 = mul i64 %or202, -7046029288634856825
  store i64 %mul203, i64* %h64.addr, align 8, !tbaa !20
  %186 = load i8*, i8** %p, align 4, !tbaa !3
  %incdec.ptr204 = getelementptr inbounds i8, i8* %186, i32 1
  store i8* %incdec.ptr204, i8** %p, align 4, !tbaa !3
  %187 = load i8, i8* %186, align 1, !tbaa !11
  %conv205 = zext i8 %187 to i64
  %mul206 = mul i64 %conv205, 2870177450012600261
  %188 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor207 = xor i64 %188, %mul206
  store i64 %xor207, i64* %h64.addr, align 8, !tbaa !20
  %189 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl208 = shl i64 %189, 11
  %190 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr209 = lshr i64 %190, 53
  %or210 = or i64 %shl208, %shr209
  %mul211 = mul i64 %or210, -7046029288634856825
  store i64 %mul211, i64* %h64.addr, align 8, !tbaa !20
  %191 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %call212 = call i64 @XXH64_avalanche(i64 %191)
  store i64 %call212, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.bb213:                                         ; preds = %entry
  %192 = bitcast i64* %k1214 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %192) #6
  %193 = load i8*, i8** %p, align 4, !tbaa !3
  %194 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %195 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call215 = call i64 @XXH_readLE64_align(i8* %193, i32 %194, i32 %195)
  %call216 = call i64 @XXH64_round(i64 0, i64 %call215)
  store i64 %call216, i64* %k1214, align 8, !tbaa !20
  %196 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr217 = getelementptr inbounds i8, i8* %196, i32 8
  store i8* %add.ptr217, i8** %p, align 4, !tbaa !3
  %197 = load i64, i64* %k1214, align 8, !tbaa !20
  %198 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor218 = xor i64 %198, %197
  store i64 %xor218, i64* %h64.addr, align 8, !tbaa !20
  %199 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl219 = shl i64 %199, 27
  %200 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr220 = lshr i64 %200, 37
  %or221 = or i64 %shl219, %shr220
  %mul222 = mul i64 %or221, -7046029288634856825
  %add223 = add i64 %mul222, -8796714831421723037
  store i64 %add223, i64* %h64.addr, align 8, !tbaa !20
  %201 = bitcast i64* %k1214 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %201) #6
  br label %sw.bb224

sw.bb224:                                         ; preds = %entry, %sw.bb213
  %202 = bitcast i64* %k1225 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %202) #6
  %203 = load i8*, i8** %p, align 4, !tbaa !3
  %204 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %205 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call226 = call i64 @XXH_readLE64_align(i8* %203, i32 %204, i32 %205)
  %call227 = call i64 @XXH64_round(i64 0, i64 %call226)
  store i64 %call227, i64* %k1225, align 8, !tbaa !20
  %206 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr228 = getelementptr inbounds i8, i8* %206, i32 8
  store i8* %add.ptr228, i8** %p, align 4, !tbaa !3
  %207 = load i64, i64* %k1225, align 8, !tbaa !20
  %208 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor229 = xor i64 %208, %207
  store i64 %xor229, i64* %h64.addr, align 8, !tbaa !20
  %209 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl230 = shl i64 %209, 27
  %210 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr231 = lshr i64 %210, 37
  %or232 = or i64 %shl230, %shr231
  %mul233 = mul i64 %or232, -7046029288634856825
  %add234 = add i64 %mul233, -8796714831421723037
  store i64 %add234, i64* %h64.addr, align 8, !tbaa !20
  %211 = bitcast i64* %k1225 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %211) #6
  br label %sw.bb235

sw.bb235:                                         ; preds = %entry, %sw.bb224
  %212 = bitcast i64* %k1236 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %212) #6
  %213 = load i8*, i8** %p, align 4, !tbaa !3
  %214 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %215 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call237 = call i64 @XXH_readLE64_align(i8* %213, i32 %214, i32 %215)
  %call238 = call i64 @XXH64_round(i64 0, i64 %call237)
  store i64 %call238, i64* %k1236, align 8, !tbaa !20
  %216 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr239 = getelementptr inbounds i8, i8* %216, i32 8
  store i8* %add.ptr239, i8** %p, align 4, !tbaa !3
  %217 = load i64, i64* %k1236, align 8, !tbaa !20
  %218 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor240 = xor i64 %218, %217
  store i64 %xor240, i64* %h64.addr, align 8, !tbaa !20
  %219 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl241 = shl i64 %219, 27
  %220 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr242 = lshr i64 %220, 37
  %or243 = or i64 %shl241, %shr242
  %mul244 = mul i64 %or243, -7046029288634856825
  %add245 = add i64 %mul244, -8796714831421723037
  store i64 %add245, i64* %h64.addr, align 8, !tbaa !20
  %221 = bitcast i64* %k1236 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %221) #6
  br label %sw.bb246

sw.bb246:                                         ; preds = %entry, %sw.bb235
  %222 = load i8*, i8** %p, align 4, !tbaa !3
  %223 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %224 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call247 = call i32 @XXH_readLE32_align(i8* %222, i32 %223, i32 %224)
  %conv248 = zext i32 %call247 to i64
  %mul249 = mul i64 %conv248, -7046029288634856825
  %225 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor250 = xor i64 %225, %mul249
  store i64 %xor250, i64* %h64.addr, align 8, !tbaa !20
  %226 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr251 = getelementptr inbounds i8, i8* %226, i32 4
  store i8* %add.ptr251, i8** %p, align 4, !tbaa !3
  %227 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl252 = shl i64 %227, 23
  %228 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr253 = lshr i64 %228, 41
  %or254 = or i64 %shl252, %shr253
  %mul255 = mul i64 %or254, -4417276706812531889
  %add256 = add i64 %mul255, 1609587929392839161
  store i64 %add256, i64* %h64.addr, align 8, !tbaa !20
  %229 = load i8*, i8** %p, align 4, !tbaa !3
  %incdec.ptr257 = getelementptr inbounds i8, i8* %229, i32 1
  store i8* %incdec.ptr257, i8** %p, align 4, !tbaa !3
  %230 = load i8, i8* %229, align 1, !tbaa !11
  %conv258 = zext i8 %230 to i64
  %mul259 = mul i64 %conv258, 2870177450012600261
  %231 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor260 = xor i64 %231, %mul259
  store i64 %xor260, i64* %h64.addr, align 8, !tbaa !20
  %232 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl261 = shl i64 %232, 11
  %233 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr262 = lshr i64 %233, 53
  %or263 = or i64 %shl261, %shr262
  %mul264 = mul i64 %or263, -7046029288634856825
  store i64 %mul264, i64* %h64.addr, align 8, !tbaa !20
  %234 = load i8*, i8** %p, align 4, !tbaa !3
  %incdec.ptr265 = getelementptr inbounds i8, i8* %234, i32 1
  store i8* %incdec.ptr265, i8** %p, align 4, !tbaa !3
  %235 = load i8, i8* %234, align 1, !tbaa !11
  %conv266 = zext i8 %235 to i64
  %mul267 = mul i64 %conv266, 2870177450012600261
  %236 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor268 = xor i64 %236, %mul267
  store i64 %xor268, i64* %h64.addr, align 8, !tbaa !20
  %237 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl269 = shl i64 %237, 11
  %238 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr270 = lshr i64 %238, 53
  %or271 = or i64 %shl269, %shr270
  %mul272 = mul i64 %or271, -7046029288634856825
  store i64 %mul272, i64* %h64.addr, align 8, !tbaa !20
  %239 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %call273 = call i64 @XXH64_avalanche(i64 %239)
  store i64 %call273, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.bb274:                                         ; preds = %entry
  %240 = bitcast i64* %k1275 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %240) #6
  %241 = load i8*, i8** %p, align 4, !tbaa !3
  %242 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %243 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call276 = call i64 @XXH_readLE64_align(i8* %241, i32 %242, i32 %243)
  %call277 = call i64 @XXH64_round(i64 0, i64 %call276)
  store i64 %call277, i64* %k1275, align 8, !tbaa !20
  %244 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr278 = getelementptr inbounds i8, i8* %244, i32 8
  store i8* %add.ptr278, i8** %p, align 4, !tbaa !3
  %245 = load i64, i64* %k1275, align 8, !tbaa !20
  %246 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor279 = xor i64 %246, %245
  store i64 %xor279, i64* %h64.addr, align 8, !tbaa !20
  %247 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl280 = shl i64 %247, 27
  %248 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr281 = lshr i64 %248, 37
  %or282 = or i64 %shl280, %shr281
  %mul283 = mul i64 %or282, -7046029288634856825
  %add284 = add i64 %mul283, -8796714831421723037
  store i64 %add284, i64* %h64.addr, align 8, !tbaa !20
  %249 = bitcast i64* %k1275 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %249) #6
  br label %sw.bb285

sw.bb285:                                         ; preds = %entry, %sw.bb274
  %250 = bitcast i64* %k1286 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %250) #6
  %251 = load i8*, i8** %p, align 4, !tbaa !3
  %252 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %253 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call287 = call i64 @XXH_readLE64_align(i8* %251, i32 %252, i32 %253)
  %call288 = call i64 @XXH64_round(i64 0, i64 %call287)
  store i64 %call288, i64* %k1286, align 8, !tbaa !20
  %254 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr289 = getelementptr inbounds i8, i8* %254, i32 8
  store i8* %add.ptr289, i8** %p, align 4, !tbaa !3
  %255 = load i64, i64* %k1286, align 8, !tbaa !20
  %256 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor290 = xor i64 %256, %255
  store i64 %xor290, i64* %h64.addr, align 8, !tbaa !20
  %257 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl291 = shl i64 %257, 27
  %258 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr292 = lshr i64 %258, 37
  %or293 = or i64 %shl291, %shr292
  %mul294 = mul i64 %or293, -7046029288634856825
  %add295 = add i64 %mul294, -8796714831421723037
  store i64 %add295, i64* %h64.addr, align 8, !tbaa !20
  %259 = bitcast i64* %k1286 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %259) #6
  br label %sw.bb296

sw.bb296:                                         ; preds = %entry, %sw.bb285
  %260 = bitcast i64* %k1297 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %260) #6
  %261 = load i8*, i8** %p, align 4, !tbaa !3
  %262 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %263 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call298 = call i64 @XXH_readLE64_align(i8* %261, i32 %262, i32 %263)
  %call299 = call i64 @XXH64_round(i64 0, i64 %call298)
  store i64 %call299, i64* %k1297, align 8, !tbaa !20
  %264 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr300 = getelementptr inbounds i8, i8* %264, i32 8
  store i8* %add.ptr300, i8** %p, align 4, !tbaa !3
  %265 = load i64, i64* %k1297, align 8, !tbaa !20
  %266 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor301 = xor i64 %266, %265
  store i64 %xor301, i64* %h64.addr, align 8, !tbaa !20
  %267 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl302 = shl i64 %267, 27
  %268 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr303 = lshr i64 %268, 37
  %or304 = or i64 %shl302, %shr303
  %mul305 = mul i64 %or304, -7046029288634856825
  %add306 = add i64 %mul305, -8796714831421723037
  store i64 %add306, i64* %h64.addr, align 8, !tbaa !20
  %269 = bitcast i64* %k1297 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %269) #6
  %270 = load i8*, i8** %p, align 4, !tbaa !3
  %incdec.ptr307 = getelementptr inbounds i8, i8* %270, i32 1
  store i8* %incdec.ptr307, i8** %p, align 4, !tbaa !3
  %271 = load i8, i8* %270, align 1, !tbaa !11
  %conv308 = zext i8 %271 to i64
  %mul309 = mul i64 %conv308, 2870177450012600261
  %272 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor310 = xor i64 %272, %mul309
  store i64 %xor310, i64* %h64.addr, align 8, !tbaa !20
  %273 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl311 = shl i64 %273, 11
  %274 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr312 = lshr i64 %274, 53
  %or313 = or i64 %shl311, %shr312
  %mul314 = mul i64 %or313, -7046029288634856825
  store i64 %mul314, i64* %h64.addr, align 8, !tbaa !20
  %275 = load i8*, i8** %p, align 4, !tbaa !3
  %incdec.ptr315 = getelementptr inbounds i8, i8* %275, i32 1
  store i8* %incdec.ptr315, i8** %p, align 4, !tbaa !3
  %276 = load i8, i8* %275, align 1, !tbaa !11
  %conv316 = zext i8 %276 to i64
  %mul317 = mul i64 %conv316, 2870177450012600261
  %277 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor318 = xor i64 %277, %mul317
  store i64 %xor318, i64* %h64.addr, align 8, !tbaa !20
  %278 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl319 = shl i64 %278, 11
  %279 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr320 = lshr i64 %279, 53
  %or321 = or i64 %shl319, %shr320
  %mul322 = mul i64 %or321, -7046029288634856825
  store i64 %mul322, i64* %h64.addr, align 8, !tbaa !20
  %280 = load i8*, i8** %p, align 4, !tbaa !3
  %incdec.ptr323 = getelementptr inbounds i8, i8* %280, i32 1
  store i8* %incdec.ptr323, i8** %p, align 4, !tbaa !3
  %281 = load i8, i8* %280, align 1, !tbaa !11
  %conv324 = zext i8 %281 to i64
  %mul325 = mul i64 %conv324, 2870177450012600261
  %282 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor326 = xor i64 %282, %mul325
  store i64 %xor326, i64* %h64.addr, align 8, !tbaa !20
  %283 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl327 = shl i64 %283, 11
  %284 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr328 = lshr i64 %284, 53
  %or329 = or i64 %shl327, %shr328
  %mul330 = mul i64 %or329, -7046029288634856825
  store i64 %mul330, i64* %h64.addr, align 8, !tbaa !20
  %285 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %call331 = call i64 @XXH64_avalanche(i64 %285)
  store i64 %call331, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.bb332:                                         ; preds = %entry
  %286 = bitcast i64* %k1333 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %286) #6
  %287 = load i8*, i8** %p, align 4, !tbaa !3
  %288 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %289 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call334 = call i64 @XXH_readLE64_align(i8* %287, i32 %288, i32 %289)
  %call335 = call i64 @XXH64_round(i64 0, i64 %call334)
  store i64 %call335, i64* %k1333, align 8, !tbaa !20
  %290 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr336 = getelementptr inbounds i8, i8* %290, i32 8
  store i8* %add.ptr336, i8** %p, align 4, !tbaa !3
  %291 = load i64, i64* %k1333, align 8, !tbaa !20
  %292 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor337 = xor i64 %292, %291
  store i64 %xor337, i64* %h64.addr, align 8, !tbaa !20
  %293 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl338 = shl i64 %293, 27
  %294 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr339 = lshr i64 %294, 37
  %or340 = or i64 %shl338, %shr339
  %mul341 = mul i64 %or340, -7046029288634856825
  %add342 = add i64 %mul341, -8796714831421723037
  store i64 %add342, i64* %h64.addr, align 8, !tbaa !20
  %295 = bitcast i64* %k1333 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %295) #6
  br label %sw.bb343

sw.bb343:                                         ; preds = %entry, %sw.bb332
  %296 = bitcast i64* %k1344 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %296) #6
  %297 = load i8*, i8** %p, align 4, !tbaa !3
  %298 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %299 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call345 = call i64 @XXH_readLE64_align(i8* %297, i32 %298, i32 %299)
  %call346 = call i64 @XXH64_round(i64 0, i64 %call345)
  store i64 %call346, i64* %k1344, align 8, !tbaa !20
  %300 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr347 = getelementptr inbounds i8, i8* %300, i32 8
  store i8* %add.ptr347, i8** %p, align 4, !tbaa !3
  %301 = load i64, i64* %k1344, align 8, !tbaa !20
  %302 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor348 = xor i64 %302, %301
  store i64 %xor348, i64* %h64.addr, align 8, !tbaa !20
  %303 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl349 = shl i64 %303, 27
  %304 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr350 = lshr i64 %304, 37
  %or351 = or i64 %shl349, %shr350
  %mul352 = mul i64 %or351, -7046029288634856825
  %add353 = add i64 %mul352, -8796714831421723037
  store i64 %add353, i64* %h64.addr, align 8, !tbaa !20
  %305 = bitcast i64* %k1344 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %305) #6
  br label %sw.bb354

sw.bb354:                                         ; preds = %entry, %sw.bb343
  %306 = bitcast i64* %k1355 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %306) #6
  %307 = load i8*, i8** %p, align 4, !tbaa !3
  %308 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %309 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call356 = call i64 @XXH_readLE64_align(i8* %307, i32 %308, i32 %309)
  %call357 = call i64 @XXH64_round(i64 0, i64 %call356)
  store i64 %call357, i64* %k1355, align 8, !tbaa !20
  %310 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr358 = getelementptr inbounds i8, i8* %310, i32 8
  store i8* %add.ptr358, i8** %p, align 4, !tbaa !3
  %311 = load i64, i64* %k1355, align 8, !tbaa !20
  %312 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor359 = xor i64 %312, %311
  store i64 %xor359, i64* %h64.addr, align 8, !tbaa !20
  %313 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl360 = shl i64 %313, 27
  %314 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr361 = lshr i64 %314, 37
  %or362 = or i64 %shl360, %shr361
  %mul363 = mul i64 %or362, -7046029288634856825
  %add364 = add i64 %mul363, -8796714831421723037
  store i64 %add364, i64* %h64.addr, align 8, !tbaa !20
  %315 = bitcast i64* %k1355 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %315) #6
  br label %sw.bb365

sw.bb365:                                         ; preds = %entry, %sw.bb354
  %316 = load i8*, i8** %p, align 4, !tbaa !3
  %317 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %318 = load i32, i32* %align.addr, align 4, !tbaa !11
  %call366 = call i32 @XXH_readLE32_align(i8* %316, i32 %317, i32 %318)
  %conv367 = zext i32 %call366 to i64
  %mul368 = mul i64 %conv367, -7046029288634856825
  %319 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor369 = xor i64 %319, %mul368
  store i64 %xor369, i64* %h64.addr, align 8, !tbaa !20
  %320 = load i8*, i8** %p, align 4, !tbaa !3
  %add.ptr370 = getelementptr inbounds i8, i8* %320, i32 4
  store i8* %add.ptr370, i8** %p, align 4, !tbaa !3
  %321 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl371 = shl i64 %321, 23
  %322 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr372 = lshr i64 %322, 41
  %or373 = or i64 %shl371, %shr372
  %mul374 = mul i64 %or373, -4417276706812531889
  %add375 = add i64 %mul374, 1609587929392839161
  store i64 %add375, i64* %h64.addr, align 8, !tbaa !20
  br label %sw.bb376

sw.bb376:                                         ; preds = %entry, %sw.bb365
  %323 = load i8*, i8** %p, align 4, !tbaa !3
  %incdec.ptr377 = getelementptr inbounds i8, i8* %323, i32 1
  store i8* %incdec.ptr377, i8** %p, align 4, !tbaa !3
  %324 = load i8, i8* %323, align 1, !tbaa !11
  %conv378 = zext i8 %324 to i64
  %mul379 = mul i64 %conv378, 2870177450012600261
  %325 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor380 = xor i64 %325, %mul379
  store i64 %xor380, i64* %h64.addr, align 8, !tbaa !20
  %326 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl381 = shl i64 %326, 11
  %327 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr382 = lshr i64 %327, 53
  %or383 = or i64 %shl381, %shr382
  %mul384 = mul i64 %or383, -7046029288634856825
  store i64 %mul384, i64* %h64.addr, align 8, !tbaa !20
  br label %sw.bb385

sw.bb385:                                         ; preds = %entry, %sw.bb376
  %328 = load i8*, i8** %p, align 4, !tbaa !3
  %incdec.ptr386 = getelementptr inbounds i8, i8* %328, i32 1
  store i8* %incdec.ptr386, i8** %p, align 4, !tbaa !3
  %329 = load i8, i8* %328, align 1, !tbaa !11
  %conv387 = zext i8 %329 to i64
  %mul388 = mul i64 %conv387, 2870177450012600261
  %330 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor389 = xor i64 %330, %mul388
  store i64 %xor389, i64* %h64.addr, align 8, !tbaa !20
  %331 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl390 = shl i64 %331, 11
  %332 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr391 = lshr i64 %332, 53
  %or392 = or i64 %shl390, %shr391
  %mul393 = mul i64 %or392, -7046029288634856825
  store i64 %mul393, i64* %h64.addr, align 8, !tbaa !20
  br label %sw.bb394

sw.bb394:                                         ; preds = %entry, %sw.bb385
  %333 = load i8*, i8** %p, align 4, !tbaa !3
  %incdec.ptr395 = getelementptr inbounds i8, i8* %333, i32 1
  store i8* %incdec.ptr395, i8** %p, align 4, !tbaa !3
  %334 = load i8, i8* %333, align 1, !tbaa !11
  %conv396 = zext i8 %334 to i64
  %mul397 = mul i64 %conv396, 2870177450012600261
  %335 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor398 = xor i64 %335, %mul397
  store i64 %xor398, i64* %h64.addr, align 8, !tbaa !20
  %336 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shl399 = shl i64 %336, 11
  %337 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr400 = lshr i64 %337, 53
  %or401 = or i64 %shl399, %shr400
  %mul402 = mul i64 %or401, -7046029288634856825
  store i64 %mul402, i64* %h64.addr, align 8, !tbaa !20
  br label %sw.bb403

sw.bb403:                                         ; preds = %entry, %sw.bb394
  %338 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %call404 = call i64 @XXH64_avalanche(i64 %338)
  store i64 %call404, i64* %retval, align 8
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.epilog:                                        ; preds = %entry
  call void @__assert_fail(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.1, i32 0, i32 0), i32 806, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @__func__.XXH64_finalize, i32 0, i32 0)) #7
  unreachable

cleanup:                                          ; preds = %sw.bb403, %sw.bb296, %sw.bb246, %sw.bb185, %sw.bb143, %sw.bb91, %sw.bb58, %sw.bb13
  %339 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %339) #6
  %340 = load i64, i64* %retval, align 8
  ret i64 %340
}

; Function Attrs: nounwind
define internal i64 @XXH_read64(i8* %memPtr) #0 {
entry:
  %memPtr.addr = alloca i8*, align 4
  %val = alloca i64, align 8
  store i8* %memPtr, i8** %memPtr.addr, align 4, !tbaa !3
  %0 = bitcast i64* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #6
  %1 = bitcast i64* %val to i8*
  %2 = load i8*, i8** %memPtr.addr, align 4, !tbaa !3
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %1, i8* align 1 %2, i32 8, i1 false)
  %3 = load i64, i64* %val, align 8, !tbaa !20
  %4 = bitcast i64* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %4) #6
  ret i64 %3
}

; Function Attrs: nounwind
define internal i64 @XXH64_avalanche(i64 %h64) #0 {
entry:
  %h64.addr = alloca i64, align 8
  store i64 %h64, i64* %h64.addr, align 8, !tbaa !20
  %0 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr = lshr i64 %0, 33
  %1 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor = xor i64 %1, %shr
  store i64 %xor, i64* %h64.addr, align 8, !tbaa !20
  %2 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %mul = mul i64 %2, -4417276706812531889
  store i64 %mul, i64* %h64.addr, align 8, !tbaa !20
  %3 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr1 = lshr i64 %3, 29
  %4 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor2 = xor i64 %4, %shr1
  store i64 %xor2, i64* %h64.addr, align 8, !tbaa !20
  %5 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %mul3 = mul i64 %5, 1609587929392839161
  store i64 %mul3, i64* %h64.addr, align 8, !tbaa !20
  %6 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %shr4 = lshr i64 %6, 32
  %7 = load i64, i64* %h64.addr, align 8, !tbaa !20
  %xor5 = xor i64 %7, %shr4
  store i64 %xor5, i64* %h64.addr, align 8, !tbaa !20
  %8 = load i64, i64* %h64.addr, align 8, !tbaa !20
  ret i64 %8
}

; Function Attrs: alwaysinline nounwind
define internal i64 @XXH_readLE64(i8* %ptr, i32 %endian) #2 {
entry:
  %ptr.addr = alloca i8*, align 4
  %endian.addr = alloca i32, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !3
  store i32 %endian, i32* %endian.addr, align 4, !tbaa !11
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !3
  %1 = load i32, i32* %endian.addr, align 4, !tbaa !11
  %call = call i64 @XXH_readLE64_align(i8* %0, i32 %1, i32 1)
  ret i64 %call
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { alwaysinline nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { noreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!3 = !{!4, !4, i64 0}
!4 = !{!"any pointer", !5, i64 0}
!5 = !{!"omnipotent char", !6, i64 0}
!6 = !{!"Simple C/C++ TBAA"}
!7 = !{!8, !8, i64 0}
!8 = !{!"long", !5, i64 0}
!9 = !{!10, !10, i64 0}
!10 = !{!"int", !5, i64 0}
!11 = !{!5, !5, i64 0}
!12 = !{!13, !10, i64 8}
!13 = !{!"XXH32_state_s", !10, i64 0, !10, i64 4, !10, i64 8, !10, i64 12, !10, i64 16, !10, i64 20, !5, i64 24, !10, i64 40, !10, i64 44}
!14 = !{!13, !10, i64 12}
!15 = !{!13, !10, i64 16}
!16 = !{!13, !10, i64 20}
!17 = !{!13, !10, i64 0}
!18 = !{!13, !10, i64 4}
!19 = !{!13, !10, i64 40}
!20 = !{!21, !21, i64 0}
!21 = !{!"long long", !5, i64 0}
!22 = !{!23, !21, i64 8}
!23 = !{!"XXH64_state_s", !21, i64 0, !21, i64 8, !21, i64 16, !21, i64 24, !21, i64 32, !5, i64 40, !10, i64 72, !5, i64 76}
!24 = !{!23, !21, i64 16}
!25 = !{!23, !21, i64 24}
!26 = !{!23, !21, i64 32}
!27 = !{!23, !21, i64 0}
!28 = !{!23, !10, i64 72}
