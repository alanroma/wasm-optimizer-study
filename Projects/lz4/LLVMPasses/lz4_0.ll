; ModuleID = 'lz4.c'
source_filename = "lz4.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%union.anon = type { i32 }
%struct.LZ4_stream_t_internal = type { [4096 x i32], i32, i16, i16, i8*, %struct.LZ4_stream_t_internal*, i32 }
%union.LZ4_stream_u = type { [2052 x i64] }
%union.LZ4_streamDecode_u = type { [4 x i64] }
%struct.LZ4_streamDecode_t_internal = type { i8*, i32, i8*, i32 }
%union.unalign = type { i32 }

@.str = private unnamed_addr constant [6 x i8] c"1.9.2\00", align 1
@__const.LZ4_isLittleEndian.one = private unnamed_addr constant %union.anon { i32 1 }, align 4
@inc32table = internal constant [8 x i32] [i32 0, i32 1, i32 2, i32 1, i32 0, i32 4, i32 4, i32 4], align 16
@dec64table = internal constant [8 x i32] [i32 0, i32 0, i32 0, i32 -1, i32 -4, i32 1, i32 2, i32 3], align 16

; Function Attrs: nounwind
define i32 @LZ4_versionNumber() #0 {
entry:
  ret i32 10902
}

; Function Attrs: nounwind
define i8* @LZ4_versionString() #0 {
entry:
  ret i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: nounwind
define i32 @LZ4_compressBound(i32 %isize) #0 {
entry:
  %isize.addr = alloca i32, align 4
  store i32 %isize, i32* %isize.addr, align 4, !tbaa !3
  %0 = load i32, i32* %isize.addr, align 4, !tbaa !3
  %cmp = icmp ugt i32 %0, 2113929216
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %1 = load i32, i32* %isize.addr, align 4, !tbaa !3
  %2 = load i32, i32* %isize.addr, align 4, !tbaa !3
  %div = sdiv i32 %2, 255
  %add = add nsw i32 %1, %div
  %add1 = add nsw i32 %add, 16
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %add1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: nounwind
define i32 @LZ4_sizeofState() #0 {
entry:
  ret i32 16416
}

; Function Attrs: nounwind
define i32 @LZ4_compress_fast_extState(i8* %state, i8* %source, i8* %dest, i32 %inputSize, i32 %maxOutputSize, i32 %acceleration) #0 {
entry:
  %retval = alloca i32, align 4
  %state.addr = alloca i8*, align 4
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %inputSize.addr = alloca i32, align 4
  %maxOutputSize.addr = alloca i32, align 4
  %acceleration.addr = alloca i32, align 4
  %ctx = alloca %struct.LZ4_stream_t_internal*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %tableType = alloca i32, align 4
  %tableType14 = alloca i32, align 4
  store i8* %state, i8** %state.addr, align 4, !tbaa !7
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %inputSize, i32* %inputSize.addr, align 4, !tbaa !3
  store i32 %maxOutputSize, i32* %maxOutputSize.addr, align 4, !tbaa !3
  store i32 %acceleration, i32* %acceleration.addr, align 4, !tbaa !3
  %0 = bitcast %struct.LZ4_stream_t_internal** %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8*, i8** %state.addr, align 4, !tbaa !7
  %call = call %union.LZ4_stream_u* @LZ4_initStream(i8* %1, i32 16416)
  %internal_donotuse = bitcast %union.LZ4_stream_u* %call to %struct.LZ4_stream_t_internal*
  store %struct.LZ4_stream_t_internal* %internal_donotuse, %struct.LZ4_stream_t_internal** %ctx, align 4, !tbaa !7
  %2 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %cmp = icmp slt i32 %2, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %acceleration.addr, align 4, !tbaa !3
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %3 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %4 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %call1 = call i32 @LZ4_compressBound(i32 %4)
  %cmp2 = icmp sge i32 %3, %call1
  br i1 %cmp2, label %if.then3, label %if.else9

if.then3:                                         ; preds = %if.end
  %5 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %cmp4 = icmp slt i32 %5, 65547
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.then3
  %6 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %ctx, align 4, !tbaa !7
  %7 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %8 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %9 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %10 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %call6 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %6, i8* %7, i8* %8, i32 %9, i32* null, i32 0, i32 0, i32 3, i32 0, i32 0, i32 %10)
  store i32 %call6, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %if.then3
  %11 = bitcast i32* %tableType to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %12 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %13 = ptrtoint i8* %12 to i32
  %cmp7 = icmp ugt i32 %13, 65535
  %14 = zext i1 %cmp7 to i64
  %cond = select i1 %cmp7, i32 1, i32 2
  store i32 %cond, i32* %tableType, align 4, !tbaa !9
  %15 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %ctx, align 4, !tbaa !7
  %16 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %17 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %18 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %19 = load i32, i32* %tableType, align 4, !tbaa !9
  %20 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %call8 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %15, i8* %16, i8* %17, i32 %18, i32* null, i32 0, i32 0, i32 %19, i32 0, i32 0, i32 %20)
  store i32 %call8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %21 = bitcast i32* %tableType to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #7
  br label %cleanup

if.else9:                                         ; preds = %if.end
  %22 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %cmp10 = icmp slt i32 %22, 65547
  br i1 %cmp10, label %if.then11, label %if.else13

if.then11:                                        ; preds = %if.else9
  %23 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %ctx, align 4, !tbaa !7
  %24 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %25 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %26 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %27 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %28 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %call12 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %23, i8* %24, i8* %25, i32 %26, i32* null, i32 %27, i32 1, i32 3, i32 0, i32 0, i32 %28)
  store i32 %call12, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else13:                                        ; preds = %if.else9
  %29 = bitcast i32* %tableType14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #7
  %30 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %31 = ptrtoint i8* %30 to i32
  %cmp15 = icmp ugt i32 %31, 65535
  %32 = zext i1 %cmp15 to i64
  %cond16 = select i1 %cmp15, i32 1, i32 2
  store i32 %cond16, i32* %tableType14, align 4, !tbaa !9
  %33 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %ctx, align 4, !tbaa !7
  %34 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %35 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %36 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %37 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %38 = load i32, i32* %tableType14, align 4, !tbaa !9
  %39 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %call17 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %33, i8* %34, i8* %35, i32 %36, i32* null, i32 %37, i32 1, i32 %38, i32 0, i32 0, i32 %39)
  store i32 %call17, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %40 = bitcast i32* %tableType14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #7
  br label %cleanup

cleanup:                                          ; preds = %if.else13, %if.then11, %if.else, %if.then5
  %41 = bitcast %struct.LZ4_stream_t_internal** %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #7
  %42 = load i32, i32* %retval, align 4
  ret i32 %42
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define %union.LZ4_stream_u* @LZ4_initStream(i8* %buffer, i32 %size) #0 {
entry:
  %retval = alloca %union.LZ4_stream_u*, align 4
  %buffer.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  store i8* %buffer, i8** %buffer.addr, align 4, !tbaa !7
  store i32 %size, i32* %size.addr, align 4, !tbaa !10
  %0 = load i8*, i8** %buffer.addr, align 4, !tbaa !7
  %cmp = icmp eq i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %union.LZ4_stream_u* null, %union.LZ4_stream_u** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !10
  %cmp1 = icmp ult i32 %1, 16416
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store %union.LZ4_stream_u* null, %union.LZ4_stream_u** %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %2 = load i8*, i8** %buffer.addr, align 4, !tbaa !7
  %3 = ptrtoint i8* %2 to i32
  %call = call i32 @LZ4_stream_t_alignment()
  %sub = sub i32 %call, 1
  %and = and i32 %3, %sub
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end3
  store %union.LZ4_stream_u* null, %union.LZ4_stream_u** %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end3
  %4 = load i8*, i8** %buffer.addr, align 4, !tbaa !7
  call void @llvm.memset.p0i8.i32(i8* align 1 %4, i8 0, i32 16416, i1 false)
  %5 = load i8*, i8** %buffer.addr, align 4, !tbaa !7
  %6 = bitcast i8* %5 to %union.LZ4_stream_u*
  store %union.LZ4_stream_u* %6, %union.LZ4_stream_u** %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4, %if.then2, %if.then
  %7 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %retval, align 4
  ret %union.LZ4_stream_u* %7
}

; Function Attrs: alwaysinline nounwind
define internal i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %cctx, i8* %source, i8* %dest, i32 %inputSize, i32* %inputConsumed, i32 %maxOutputSize, i32 %outputDirective, i32 %tableType, i32 %dictDirective, i32 %dictIssue, i32 %acceleration) #2 {
entry:
  %retval = alloca i32, align 4
  %cctx.addr = alloca %struct.LZ4_stream_t_internal*, align 4
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %inputSize.addr = alloca i32, align 4
  %inputConsumed.addr = alloca i32*, align 4
  %maxOutputSize.addr = alloca i32, align 4
  %outputDirective.addr = alloca i32, align 4
  %tableType.addr = alloca i32, align 4
  %dictDirective.addr = alloca i32, align 4
  %dictIssue.addr = alloca i32, align 4
  %acceleration.addr = alloca i32, align 4
  %result = alloca i32, align 4
  %ip = alloca i8*, align 4
  %startIndex = alloca i32, align 4
  %base = alloca i8*, align 4
  %lowLimit = alloca i8*, align 4
  %dictCtx = alloca %struct.LZ4_stream_t_internal*, align 4
  %dictionary = alloca i8*, align 4
  %dictSize = alloca i32, align 4
  %dictDelta = alloca i32, align 4
  %maybe_extMem = alloca i32, align 4
  %prefixIdxLimit = alloca i32, align 4
  %dictEnd = alloca i8*, align 4
  %anchor = alloca i8*, align 4
  %iend = alloca i8*, align 4
  %mflimitPlusOne = alloca i8*, align 4
  %matchlimit = alloca i8*, align 4
  %dictBase = alloca i8*, align 4
  %op = alloca i8*, align 4
  %olimit = alloca i8*, align 4
  %offset = alloca i32, align 4
  %forwardH = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %match = alloca i8*, align 4
  %token = alloca i8*, align 4
  %filledIp = alloca i8*, align 4
  %forwardIp = alloca i8*, align 4
  %step = alloca i32, align 4
  %searchMatchNb = alloca i32, align 4
  %h = alloca i32, align 4
  %forwardIp113 = alloca i8*, align 4
  %step114 = alloca i32, align 4
  %searchMatchNb115 = alloca i32, align 4
  %h118 = alloca i32, align 4
  %current = alloca i32, align 4
  %matchIndex = alloca i32, align 4
  %litLength = alloca i32, align 4
  %len = alloca i32, align 4
  %matchCode = alloca i32, align 4
  %limit = alloca i8*, align 4
  %more = alloca i32, align 4
  %newMatchCode = alloca i32, align 4
  %ptr = alloca i8*, align 4
  %h370 = alloca i32, align 4
  %h436 = alloca i32, align 4
  %current438 = alloca i32, align 4
  %matchIndex442 = alloca i32, align 4
  %lastRun = alloca i32, align 4
  %accumulator = alloca i32, align 4
  store %struct.LZ4_stream_t_internal* %cctx, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %inputSize, i32* %inputSize.addr, align 4, !tbaa !3
  store i32* %inputConsumed, i32** %inputConsumed.addr, align 4, !tbaa !7
  store i32 %maxOutputSize, i32* %maxOutputSize.addr, align 4, !tbaa !3
  store i32 %outputDirective, i32* %outputDirective.addr, align 4, !tbaa !9
  store i32 %tableType, i32* %tableType.addr, align 4, !tbaa !9
  store i32 %dictDirective, i32* %dictDirective.addr, align 4, !tbaa !9
  store i32 %dictIssue, i32* %dictIssue.addr, align 4, !tbaa !9
  store i32 %acceleration, i32* %acceleration.addr, align 4, !tbaa !3
  %0 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast i8** %ip to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i8*, i8** %source.addr, align 4, !tbaa !7
  store i8* %2, i8** %ip, align 4, !tbaa !7
  %3 = bitcast i32* %startIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %currentOffset = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %4, i32 0, i32 1
  %5 = load i32, i32* %currentOffset, align 4, !tbaa !12
  store i32 %5, i32* %startIndex, align 4, !tbaa !3
  %6 = bitcast i8** %base to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %8 = load i32, i32* %startIndex, align 4, !tbaa !3
  %idx.neg = sub i32 0, %8
  %add.ptr = getelementptr inbounds i8, i8* %7, i32 %idx.neg
  store i8* %add.ptr, i8** %base, align 4, !tbaa !7
  %9 = bitcast i8** %lowLimit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = bitcast %struct.LZ4_stream_t_internal** %dictCtx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #7
  %11 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %dictCtx1 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %11, i32 0, i32 5
  %12 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dictCtx1, align 4, !tbaa !15
  store %struct.LZ4_stream_t_internal* %12, %struct.LZ4_stream_t_internal** %dictCtx, align 4, !tbaa !7
  %13 = bitcast i8** %dictionary to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i32, i32* %dictDirective.addr, align 4, !tbaa !9
  %cmp = icmp eq i32 %14, 3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %15 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dictCtx, align 4, !tbaa !7
  %dictionary2 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %15, i32 0, i32 4
  %16 = load i8*, i8** %dictionary2, align 4, !tbaa !16
  br label %cond.end

cond.false:                                       ; preds = %entry
  %17 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %dictionary3 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %17, i32 0, i32 4
  %18 = load i8*, i8** %dictionary3, align 4, !tbaa !16
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %16, %cond.true ], [ %18, %cond.false ]
  store i8* %cond, i8** %dictionary, align 4, !tbaa !7
  %19 = bitcast i32* %dictSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #7
  %20 = load i32, i32* %dictDirective.addr, align 4, !tbaa !9
  %cmp4 = icmp eq i32 %20, 3
  br i1 %cmp4, label %cond.true5, label %cond.false7

cond.true5:                                       ; preds = %cond.end
  %21 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dictCtx, align 4, !tbaa !7
  %dictSize6 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %21, i32 0, i32 6
  %22 = load i32, i32* %dictSize6, align 4, !tbaa !17
  br label %cond.end9

cond.false7:                                      ; preds = %cond.end
  %23 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %dictSize8 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %23, i32 0, i32 6
  %24 = load i32, i32* %dictSize8, align 4, !tbaa !17
  br label %cond.end9

cond.end9:                                        ; preds = %cond.false7, %cond.true5
  %cond10 = phi i32 [ %22, %cond.true5 ], [ %24, %cond.false7 ]
  store i32 %cond10, i32* %dictSize, align 4, !tbaa !3
  %25 = bitcast i32* %dictDelta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #7
  %26 = load i32, i32* %dictDirective.addr, align 4, !tbaa !9
  %cmp11 = icmp eq i32 %26, 3
  br i1 %cmp11, label %cond.true12, label %cond.false14

cond.true12:                                      ; preds = %cond.end9
  %27 = load i32, i32* %startIndex, align 4, !tbaa !3
  %28 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dictCtx, align 4, !tbaa !7
  %currentOffset13 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %28, i32 0, i32 1
  %29 = load i32, i32* %currentOffset13, align 4, !tbaa !12
  %sub = sub i32 %27, %29
  br label %cond.end15

cond.false14:                                     ; preds = %cond.end9
  br label %cond.end15

cond.end15:                                       ; preds = %cond.false14, %cond.true12
  %cond16 = phi i32 [ %sub, %cond.true12 ], [ 0, %cond.false14 ]
  store i32 %cond16, i32* %dictDelta, align 4, !tbaa !3
  %30 = bitcast i32* %maybe_extMem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #7
  %31 = load i32, i32* %dictDirective.addr, align 4, !tbaa !9
  %cmp17 = icmp eq i32 %31, 2
  br i1 %cmp17, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %cond.end15
  %32 = load i32, i32* %dictDirective.addr, align 4, !tbaa !9
  %cmp18 = icmp eq i32 %32, 3
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %cond.end15
  %33 = phi i1 [ true, %cond.end15 ], [ %cmp18, %lor.rhs ]
  %lor.ext = zext i1 %33 to i32
  store i32 %lor.ext, i32* %maybe_extMem, align 4, !tbaa !3
  %34 = bitcast i32* %prefixIdxLimit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #7
  %35 = load i32, i32* %startIndex, align 4, !tbaa !3
  %36 = load i32, i32* %dictSize, align 4, !tbaa !3
  %sub19 = sub i32 %35, %36
  store i32 %sub19, i32* %prefixIdxLimit, align 4, !tbaa !3
  %37 = bitcast i8** %dictEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #7
  %38 = load i8*, i8** %dictionary, align 4, !tbaa !7
  %tobool = icmp ne i8* %38, null
  br i1 %tobool, label %cond.true20, label %cond.false22

cond.true20:                                      ; preds = %lor.end
  %39 = load i8*, i8** %dictionary, align 4, !tbaa !7
  %40 = load i32, i32* %dictSize, align 4, !tbaa !3
  %add.ptr21 = getelementptr inbounds i8, i8* %39, i32 %40
  br label %cond.end23

cond.false22:                                     ; preds = %lor.end
  %41 = load i8*, i8** %dictionary, align 4, !tbaa !7
  br label %cond.end23

cond.end23:                                       ; preds = %cond.false22, %cond.true20
  %cond24 = phi i8* [ %add.ptr21, %cond.true20 ], [ %41, %cond.false22 ]
  store i8* %cond24, i8** %dictEnd, align 4, !tbaa !7
  %42 = bitcast i8** %anchor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #7
  %43 = load i8*, i8** %source.addr, align 4, !tbaa !7
  store i8* %43, i8** %anchor, align 4, !tbaa !7
  %44 = bitcast i8** %iend to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #7
  %45 = load i8*, i8** %ip, align 4, !tbaa !7
  %46 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %add.ptr25 = getelementptr inbounds i8, i8* %45, i32 %46
  store i8* %add.ptr25, i8** %iend, align 4, !tbaa !7
  %47 = bitcast i8** %mflimitPlusOne to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #7
  %48 = load i8*, i8** %iend, align 4, !tbaa !7
  %add.ptr26 = getelementptr inbounds i8, i8* %48, i32 -12
  %add.ptr27 = getelementptr inbounds i8, i8* %add.ptr26, i32 1
  store i8* %add.ptr27, i8** %mflimitPlusOne, align 4, !tbaa !7
  %49 = bitcast i8** %matchlimit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #7
  %50 = load i8*, i8** %iend, align 4, !tbaa !7
  %add.ptr28 = getelementptr inbounds i8, i8* %50, i32 -5
  store i8* %add.ptr28, i8** %matchlimit, align 4, !tbaa !7
  %51 = bitcast i8** %dictBase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #7
  %52 = load i8*, i8** %dictionary, align 4, !tbaa !7
  %tobool29 = icmp ne i8* %52, null
  br i1 %tobool29, label %cond.false31, label %cond.true30

cond.true30:                                      ; preds = %cond.end23
  br label %cond.end44

cond.false31:                                     ; preds = %cond.end23
  %53 = load i32, i32* %dictDirective.addr, align 4, !tbaa !9
  %cmp32 = icmp eq i32 %53, 3
  br i1 %cmp32, label %cond.true33, label %cond.false38

cond.true33:                                      ; preds = %cond.false31
  %54 = load i8*, i8** %dictionary, align 4, !tbaa !7
  %55 = load i32, i32* %dictSize, align 4, !tbaa !3
  %add.ptr34 = getelementptr inbounds i8, i8* %54, i32 %55
  %56 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dictCtx, align 4, !tbaa !7
  %currentOffset35 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %56, i32 0, i32 1
  %57 = load i32, i32* %currentOffset35, align 4, !tbaa !12
  %idx.neg36 = sub i32 0, %57
  %add.ptr37 = getelementptr inbounds i8, i8* %add.ptr34, i32 %idx.neg36
  br label %cond.end42

cond.false38:                                     ; preds = %cond.false31
  %58 = load i8*, i8** %dictionary, align 4, !tbaa !7
  %59 = load i32, i32* %dictSize, align 4, !tbaa !3
  %add.ptr39 = getelementptr inbounds i8, i8* %58, i32 %59
  %60 = load i32, i32* %startIndex, align 4, !tbaa !3
  %idx.neg40 = sub i32 0, %60
  %add.ptr41 = getelementptr inbounds i8, i8* %add.ptr39, i32 %idx.neg40
  br label %cond.end42

cond.end42:                                       ; preds = %cond.false38, %cond.true33
  %cond43 = phi i8* [ %add.ptr37, %cond.true33 ], [ %add.ptr41, %cond.false38 ]
  br label %cond.end44

cond.end44:                                       ; preds = %cond.end42, %cond.true30
  %cond45 = phi i8* [ null, %cond.true30 ], [ %cond43, %cond.end42 ]
  store i8* %cond45, i8** %dictBase, align 4, !tbaa !7
  %61 = bitcast i8** %op to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #7
  %62 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  store i8* %62, i8** %op, align 4, !tbaa !7
  %63 = bitcast i8** %olimit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #7
  %64 = load i8*, i8** %op, align 4, !tbaa !7
  %65 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %add.ptr46 = getelementptr inbounds i8, i8* %64, i32 %65
  store i8* %add.ptr46, i8** %olimit, align 4, !tbaa !7
  %66 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #7
  store i32 0, i32* %offset, align 4, !tbaa !3
  %67 = bitcast i32* %forwardH to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #7
  %68 = load i32, i32* %outputDirective.addr, align 4, !tbaa !9
  %cmp47 = icmp eq i32 %68, 2
  br i1 %cmp47, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %cond.end44
  %69 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %cmp48 = icmp slt i32 %69, 1
  br i1 %cmp48, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup579

if.end:                                           ; preds = %land.lhs.true, %cond.end44
  %70 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %cmp49 = icmp ugt i32 %70, 2113929216
  br i1 %cmp49, label %if.then50, label %if.end51

if.then50:                                        ; preds = %if.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup579

if.end51:                                         ; preds = %if.end
  %71 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %cmp52 = icmp eq i32 %71, 3
  br i1 %cmp52, label %land.lhs.true53, label %if.end56

land.lhs.true53:                                  ; preds = %if.end51
  %72 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %cmp54 = icmp sge i32 %72, 65547
  br i1 %cmp54, label %if.then55, label %if.end56

if.then55:                                        ; preds = %land.lhs.true53
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup579

if.end56:                                         ; preds = %land.lhs.true53, %if.end51
  %73 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %cmp57 = icmp eq i32 %73, 1
  br i1 %cmp57, label %if.then58, label %if.end59

if.then58:                                        ; preds = %if.end56
  br label %if.end59

if.end59:                                         ; preds = %if.then58, %if.end56
  %74 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %75 = load i32, i32* %dictDirective.addr, align 4, !tbaa !9
  %cmp60 = icmp eq i32 %75, 1
  br i1 %cmp60, label %cond.true61, label %cond.false62

cond.true61:                                      ; preds = %if.end59
  %76 = load i32, i32* %dictSize, align 4, !tbaa !3
  br label %cond.end63

cond.false62:                                     ; preds = %if.end59
  br label %cond.end63

cond.end63:                                       ; preds = %cond.false62, %cond.true61
  %cond64 = phi i32 [ %76, %cond.true61 ], [ 0, %cond.false62 ]
  %idx.neg65 = sub i32 0, %cond64
  %add.ptr66 = getelementptr inbounds i8, i8* %74, i32 %idx.neg65
  store i8* %add.ptr66, i8** %lowLimit, align 4, !tbaa !7
  %77 = load i32, i32* %dictDirective.addr, align 4, !tbaa !9
  %cmp67 = icmp eq i32 %77, 3
  br i1 %cmp67, label %if.then68, label %if.else

if.then68:                                        ; preds = %cond.end63
  %78 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %dictCtx69 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %78, i32 0, i32 5
  store %struct.LZ4_stream_t_internal* null, %struct.LZ4_stream_t_internal** %dictCtx69, align 4, !tbaa !15
  %79 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %80 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %dictSize70 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %80, i32 0, i32 6
  store i32 %79, i32* %dictSize70, align 4, !tbaa !17
  br label %if.end72

if.else:                                          ; preds = %cond.end63
  %81 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %82 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %dictSize71 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %82, i32 0, i32 6
  %83 = load i32, i32* %dictSize71, align 4, !tbaa !17
  %add = add i32 %83, %81
  store i32 %add, i32* %dictSize71, align 4, !tbaa !17
  br label %if.end72

if.end72:                                         ; preds = %if.else, %if.then68
  %84 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %85 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %currentOffset73 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %85, i32 0, i32 1
  %86 = load i32, i32* %currentOffset73, align 4, !tbaa !12
  %add74 = add i32 %86, %84
  store i32 %add74, i32* %currentOffset73, align 4, !tbaa !12
  %87 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %conv = trunc i32 %87 to i16
  %88 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %tableType75 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %88, i32 0, i32 3
  store i16 %conv, i16* %tableType75, align 2, !tbaa !18
  %89 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %cmp76 = icmp slt i32 %89, 13
  br i1 %cmp76, label %if.then78, label %if.end79

if.then78:                                        ; preds = %if.end72
  br label %_last_literals

if.end79:                                         ; preds = %if.end72
  %90 = load i8*, i8** %ip, align 4, !tbaa !7
  %91 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %hashTable = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %91, i32 0, i32 0
  %arraydecay = getelementptr inbounds [4096 x i32], [4096 x i32]* %hashTable, i32 0, i32 0
  %92 = bitcast i32* %arraydecay to i8*
  %93 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %94 = load i8*, i8** %base, align 4, !tbaa !7
  call void @LZ4_putPosition(i8* %90, i8* %92, i32 %93, i8* %94)
  %95 = load i8*, i8** %ip, align 4, !tbaa !7
  %incdec.ptr = getelementptr inbounds i8, i8* %95, i32 1
  store i8* %incdec.ptr, i8** %ip, align 4, !tbaa !7
  %96 = load i8*, i8** %ip, align 4, !tbaa !7
  %97 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %call = call i32 @LZ4_hashPosition(i8* %96, i32 %97)
  store i32 %call, i32* %forwardH, align 4, !tbaa !3
  br label %for.cond

for.cond:                                         ; preds = %cleanup.cont515, %if.end79
  %98 = bitcast i8** %match to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %98) #7
  %99 = bitcast i8** %token to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #7
  %100 = bitcast i8** %filledIp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %100) #7
  %101 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %cmp80 = icmp eq i32 %101, 1
  br i1 %cmp80, label %if.then82, label %if.else112

if.then82:                                        ; preds = %for.cond
  %102 = bitcast i8** %forwardIp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %102) #7
  %103 = load i8*, i8** %ip, align 4, !tbaa !7
  store i8* %103, i8** %forwardIp, align 4, !tbaa !7
  %104 = bitcast i32* %step to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %104) #7
  store i32 1, i32* %step, align 4, !tbaa !3
  %105 = bitcast i32* %searchMatchNb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %105) #7
  %106 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %shl = shl i32 %106, 6
  store i32 %shl, i32* %searchMatchNb, align 4, !tbaa !3
  br label %do.body

do.body:                                          ; preds = %lor.end105, %if.then82
  %107 = bitcast i32* %h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %107) #7
  %108 = load i32, i32* %forwardH, align 4, !tbaa !3
  store i32 %108, i32* %h, align 4, !tbaa !3
  %109 = load i8*, i8** %forwardIp, align 4, !tbaa !7
  store i8* %109, i8** %ip, align 4, !tbaa !7
  %110 = load i32, i32* %step, align 4, !tbaa !3
  %111 = load i8*, i8** %forwardIp, align 4, !tbaa !7
  %add.ptr83 = getelementptr inbounds i8, i8* %111, i32 %110
  store i8* %add.ptr83, i8** %forwardIp, align 4, !tbaa !7
  %112 = load i32, i32* %searchMatchNb, align 4, !tbaa !3
  %inc = add nsw i32 %112, 1
  store i32 %inc, i32* %searchMatchNb, align 4, !tbaa !3
  %shr = ashr i32 %112, 6
  store i32 %shr, i32* %step, align 4, !tbaa !3
  %113 = load i8*, i8** %forwardIp, align 4, !tbaa !7
  %114 = load i8*, i8** %mflimitPlusOne, align 4, !tbaa !7
  %cmp84 = icmp ugt i8* %113, %114
  %conv85 = zext i1 %cmp84 to i32
  %cmp86 = icmp ne i32 %conv85, 0
  %conv87 = zext i1 %cmp86 to i32
  %expval = call i32 @llvm.expect.i32(i32 %conv87, i32 0)
  %tobool88 = icmp ne i32 %expval, 0
  br i1 %tobool88, label %if.then89, label %if.end90

if.then89:                                        ; preds = %do.body
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end90:                                         ; preds = %do.body
  %115 = load i32, i32* %h, align 4, !tbaa !3
  %116 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %hashTable91 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %116, i32 0, i32 0
  %arraydecay92 = getelementptr inbounds [4096 x i32], [4096 x i32]* %hashTable91, i32 0, i32 0
  %117 = bitcast i32* %arraydecay92 to i8*
  %118 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %119 = load i8*, i8** %base, align 4, !tbaa !7
  %call93 = call i8* @LZ4_getPositionOnHash(i32 %115, i8* %117, i32 %118, i8* %119)
  store i8* %call93, i8** %match, align 4, !tbaa !7
  %120 = load i8*, i8** %forwardIp, align 4, !tbaa !7
  %121 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %call94 = call i32 @LZ4_hashPosition(i8* %120, i32 %121)
  store i32 %call94, i32* %forwardH, align 4, !tbaa !3
  %122 = load i8*, i8** %ip, align 4, !tbaa !7
  %123 = load i32, i32* %h, align 4, !tbaa !3
  %124 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %hashTable95 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %124, i32 0, i32 0
  %arraydecay96 = getelementptr inbounds [4096 x i32], [4096 x i32]* %hashTable95, i32 0, i32 0
  %125 = bitcast i32* %arraydecay96 to i8*
  %126 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %127 = load i8*, i8** %base, align 4, !tbaa !7
  call void @LZ4_putPositionOnHash(i8* %122, i32 %123, i8* %125, i32 %126, i8* %127)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.then89, %if.end90
  %128 = bitcast i32* %h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup107 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %do.cond

do.cond:                                          ; preds = %cleanup.cont
  %129 = load i8*, i8** %match, align 4, !tbaa !7
  %add.ptr97 = getelementptr inbounds i8, i8* %129, i32 65535
  %130 = load i8*, i8** %ip, align 4, !tbaa !7
  %cmp98 = icmp ult i8* %add.ptr97, %130
  br i1 %cmp98, label %lor.end105, label %lor.rhs100

lor.rhs100:                                       ; preds = %do.cond
  %131 = load i8*, i8** %match, align 4, !tbaa !7
  %call101 = call i32 @LZ4_read32(i8* %131)
  %132 = load i8*, i8** %ip, align 4, !tbaa !7
  %call102 = call i32 @LZ4_read32(i8* %132)
  %cmp103 = icmp ne i32 %call101, %call102
  br label %lor.end105

lor.end105:                                       ; preds = %lor.rhs100, %do.cond
  %133 = phi i1 [ true, %do.cond ], [ %cmp103, %lor.rhs100 ]
  br i1 %133, label %do.body, label %do.end

do.end:                                           ; preds = %lor.end105
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup107

cleanup107:                                       ; preds = %do.end, %cleanup
  %134 = bitcast i32* %searchMatchNb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #7
  %135 = bitcast i32* %step to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #7
  %136 = bitcast i8** %forwardIp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #7
  %cleanup.dest110 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest110, label %cleanup511 [
    i32 0, label %cleanup.cont111
  ]

cleanup.cont111:                                  ; preds = %cleanup107
  br label %if.end202

if.else112:                                       ; preds = %for.cond
  %137 = bitcast i8** %forwardIp113 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %137) #7
  %138 = load i8*, i8** %ip, align 4, !tbaa !7
  store i8* %138, i8** %forwardIp113, align 4, !tbaa !7
  %139 = bitcast i32* %step114 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %139) #7
  store i32 1, i32* %step114, align 4, !tbaa !3
  %140 = bitcast i32* %searchMatchNb115 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %140) #7
  %141 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %shl116 = shl i32 %141, 6
  store i32 %shl116, i32* %searchMatchNb115, align 4, !tbaa !3
  br label %do.body117

do.body117:                                       ; preds = %do.cond195, %if.else112
  %142 = bitcast i32* %h118 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %142) #7
  %143 = load i32, i32* %forwardH, align 4, !tbaa !3
  store i32 %143, i32* %h118, align 4, !tbaa !3
  %144 = bitcast i32* %current to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %144) #7
  %145 = load i8*, i8** %forwardIp113, align 4, !tbaa !7
  %146 = load i8*, i8** %base, align 4, !tbaa !7
  %sub.ptr.lhs.cast = ptrtoint i8* %145 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %146 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %current, align 4, !tbaa !3
  %147 = bitcast i32* %matchIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %147) #7
  %148 = load i32, i32* %h118, align 4, !tbaa !3
  %149 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %hashTable119 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %149, i32 0, i32 0
  %arraydecay120 = getelementptr inbounds [4096 x i32], [4096 x i32]* %hashTable119, i32 0, i32 0
  %150 = bitcast i32* %arraydecay120 to i8*
  %151 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %call121 = call i32 @LZ4_getIndexOnHash(i32 %148, i8* %150, i32 %151)
  store i32 %call121, i32* %matchIndex, align 4, !tbaa !3
  %152 = load i8*, i8** %forwardIp113, align 4, !tbaa !7
  store i8* %152, i8** %ip, align 4, !tbaa !7
  %153 = load i32, i32* %step114, align 4, !tbaa !3
  %154 = load i8*, i8** %forwardIp113, align 4, !tbaa !7
  %add.ptr122 = getelementptr inbounds i8, i8* %154, i32 %153
  store i8* %add.ptr122, i8** %forwardIp113, align 4, !tbaa !7
  %155 = load i32, i32* %searchMatchNb115, align 4, !tbaa !3
  %inc123 = add nsw i32 %155, 1
  store i32 %inc123, i32* %searchMatchNb115, align 4, !tbaa !3
  %shr124 = ashr i32 %155, 6
  store i32 %shr124, i32* %step114, align 4, !tbaa !3
  %156 = load i8*, i8** %forwardIp113, align 4, !tbaa !7
  %157 = load i8*, i8** %mflimitPlusOne, align 4, !tbaa !7
  %cmp125 = icmp ugt i8* %156, %157
  %conv126 = zext i1 %cmp125 to i32
  %cmp127 = icmp ne i32 %conv126, 0
  %conv128 = zext i1 %cmp127 to i32
  %expval129 = call i32 @llvm.expect.i32(i32 %conv128, i32 0)
  %tobool130 = icmp ne i32 %expval129, 0
  br i1 %tobool130, label %if.then131, label %if.end132

if.then131:                                       ; preds = %do.body117
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup190

if.end132:                                        ; preds = %do.body117
  %158 = load i32, i32* %dictDirective.addr, align 4, !tbaa !9
  %cmp133 = icmp eq i32 %158, 3
  br i1 %cmp133, label %if.then135, label %if.else147

if.then135:                                       ; preds = %if.end132
  %159 = load i32, i32* %matchIndex, align 4, !tbaa !3
  %160 = load i32, i32* %startIndex, align 4, !tbaa !3
  %cmp136 = icmp ult i32 %159, %160
  br i1 %cmp136, label %if.then138, label %if.else144

if.then138:                                       ; preds = %if.then135
  %161 = load i32, i32* %h118, align 4, !tbaa !3
  %162 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dictCtx, align 4, !tbaa !7
  %hashTable139 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %162, i32 0, i32 0
  %arraydecay140 = getelementptr inbounds [4096 x i32], [4096 x i32]* %hashTable139, i32 0, i32 0
  %163 = bitcast i32* %arraydecay140 to i8*
  %call141 = call i32 @LZ4_getIndexOnHash(i32 %161, i8* %163, i32 2)
  store i32 %call141, i32* %matchIndex, align 4, !tbaa !3
  %164 = load i8*, i8** %dictBase, align 4, !tbaa !7
  %165 = load i32, i32* %matchIndex, align 4, !tbaa !3
  %add.ptr142 = getelementptr inbounds i8, i8* %164, i32 %165
  store i8* %add.ptr142, i8** %match, align 4, !tbaa !7
  %166 = load i32, i32* %dictDelta, align 4, !tbaa !3
  %167 = load i32, i32* %matchIndex, align 4, !tbaa !3
  %add143 = add i32 %167, %166
  store i32 %add143, i32* %matchIndex, align 4, !tbaa !3
  %168 = load i8*, i8** %dictionary, align 4, !tbaa !7
  store i8* %168, i8** %lowLimit, align 4, !tbaa !7
  br label %if.end146

if.else144:                                       ; preds = %if.then135
  %169 = load i8*, i8** %base, align 4, !tbaa !7
  %170 = load i32, i32* %matchIndex, align 4, !tbaa !3
  %add.ptr145 = getelementptr inbounds i8, i8* %169, i32 %170
  store i8* %add.ptr145, i8** %match, align 4, !tbaa !7
  %171 = load i8*, i8** %source.addr, align 4, !tbaa !7
  store i8* %171, i8** %lowLimit, align 4, !tbaa !7
  br label %if.end146

if.end146:                                        ; preds = %if.else144, %if.then138
  br label %if.end161

if.else147:                                       ; preds = %if.end132
  %172 = load i32, i32* %dictDirective.addr, align 4, !tbaa !9
  %cmp148 = icmp eq i32 %172, 2
  br i1 %cmp148, label %if.then150, label %if.else158

if.then150:                                       ; preds = %if.else147
  %173 = load i32, i32* %matchIndex, align 4, !tbaa !3
  %174 = load i32, i32* %startIndex, align 4, !tbaa !3
  %cmp151 = icmp ult i32 %173, %174
  br i1 %cmp151, label %if.then153, label %if.else155

if.then153:                                       ; preds = %if.then150
  %175 = load i8*, i8** %dictBase, align 4, !tbaa !7
  %176 = load i32, i32* %matchIndex, align 4, !tbaa !3
  %add.ptr154 = getelementptr inbounds i8, i8* %175, i32 %176
  store i8* %add.ptr154, i8** %match, align 4, !tbaa !7
  %177 = load i8*, i8** %dictionary, align 4, !tbaa !7
  store i8* %177, i8** %lowLimit, align 4, !tbaa !7
  br label %if.end157

if.else155:                                       ; preds = %if.then150
  %178 = load i8*, i8** %base, align 4, !tbaa !7
  %179 = load i32, i32* %matchIndex, align 4, !tbaa !3
  %add.ptr156 = getelementptr inbounds i8, i8* %178, i32 %179
  store i8* %add.ptr156, i8** %match, align 4, !tbaa !7
  %180 = load i8*, i8** %source.addr, align 4, !tbaa !7
  store i8* %180, i8** %lowLimit, align 4, !tbaa !7
  br label %if.end157

if.end157:                                        ; preds = %if.else155, %if.then153
  br label %if.end160

if.else158:                                       ; preds = %if.else147
  %181 = load i8*, i8** %base, align 4, !tbaa !7
  %182 = load i32, i32* %matchIndex, align 4, !tbaa !3
  %add.ptr159 = getelementptr inbounds i8, i8* %181, i32 %182
  store i8* %add.ptr159, i8** %match, align 4, !tbaa !7
  br label %if.end160

if.end160:                                        ; preds = %if.else158, %if.end157
  br label %if.end161

if.end161:                                        ; preds = %if.end160, %if.end146
  %183 = load i8*, i8** %forwardIp113, align 4, !tbaa !7
  %184 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %call162 = call i32 @LZ4_hashPosition(i8* %183, i32 %184)
  store i32 %call162, i32* %forwardH, align 4, !tbaa !3
  %185 = load i32, i32* %current, align 4, !tbaa !3
  %186 = load i32, i32* %h118, align 4, !tbaa !3
  %187 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %hashTable163 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %187, i32 0, i32 0
  %arraydecay164 = getelementptr inbounds [4096 x i32], [4096 x i32]* %hashTable163, i32 0, i32 0
  %188 = bitcast i32* %arraydecay164 to i8*
  %189 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  call void @LZ4_putIndexOnHash(i32 %185, i32 %186, i8* %188, i32 %189)
  %190 = load i32, i32* %dictIssue.addr, align 4, !tbaa !9
  %cmp165 = icmp eq i32 %190, 1
  br i1 %cmp165, label %land.lhs.true167, label %if.end171

land.lhs.true167:                                 ; preds = %if.end161
  %191 = load i32, i32* %matchIndex, align 4, !tbaa !3
  %192 = load i32, i32* %prefixIdxLimit, align 4, !tbaa !3
  %cmp168 = icmp ult i32 %191, %192
  br i1 %cmp168, label %if.then170, label %if.end171

if.then170:                                       ; preds = %land.lhs.true167
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup190

if.end171:                                        ; preds = %land.lhs.true167, %if.end161
  %193 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %cmp172 = icmp ne i32 %193, 3
  br i1 %cmp172, label %land.lhs.true174, label %if.end179

land.lhs.true174:                                 ; preds = %if.end171
  %194 = load i32, i32* %matchIndex, align 4, !tbaa !3
  %add175 = add i32 %194, 65535
  %195 = load i32, i32* %current, align 4, !tbaa !3
  %cmp176 = icmp ult i32 %add175, %195
  br i1 %cmp176, label %if.then178, label %if.end179

if.then178:                                       ; preds = %land.lhs.true174
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup190

if.end179:                                        ; preds = %land.lhs.true174, %if.end171
  %196 = load i8*, i8** %match, align 4, !tbaa !7
  %call180 = call i32 @LZ4_read32(i8* %196)
  %197 = load i8*, i8** %ip, align 4, !tbaa !7
  %call181 = call i32 @LZ4_read32(i8* %197)
  %cmp182 = icmp eq i32 %call180, %call181
  br i1 %cmp182, label %if.then184, label %if.end189

if.then184:                                       ; preds = %if.end179
  %198 = load i32, i32* %maybe_extMem, align 4, !tbaa !3
  %tobool185 = icmp ne i32 %198, 0
  br i1 %tobool185, label %if.then186, label %if.end188

if.then186:                                       ; preds = %if.then184
  %199 = load i32, i32* %current, align 4, !tbaa !3
  %200 = load i32, i32* %matchIndex, align 4, !tbaa !3
  %sub187 = sub i32 %199, %200
  store i32 %sub187, i32* %offset, align 4, !tbaa !3
  br label %if.end188

if.end188:                                        ; preds = %if.then186, %if.then184
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup190

if.end189:                                        ; preds = %if.end179
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup190

cleanup190:                                       ; preds = %if.then131, %if.end189, %if.end188, %if.then178, %if.then170
  %201 = bitcast i32* %matchIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #7
  %202 = bitcast i32* %current to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #7
  %203 = bitcast i32* %h118 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #7
  %cleanup.dest193 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest193, label %cleanup197 [
    i32 0, label %cleanup.cont194
    i32 8, label %do.cond195
    i32 7, label %do.end196
  ]

cleanup.cont194:                                  ; preds = %cleanup190
  br label %do.cond195

do.cond195:                                       ; preds = %cleanup.cont194, %cleanup190
  br i1 true, label %do.body117, label %do.end196

do.end196:                                        ; preds = %do.cond195, %cleanup190
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup197

cleanup197:                                       ; preds = %do.end196, %cleanup190
  %204 = bitcast i32* %searchMatchNb115 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #7
  %205 = bitcast i32* %step114 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #7
  %206 = bitcast i8** %forwardIp113 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #7
  %cleanup.dest200 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest200, label %cleanup511 [
    i32 0, label %cleanup.cont201
  ]

cleanup.cont201:                                  ; preds = %cleanup197
  br label %if.end202

if.end202:                                        ; preds = %cleanup.cont201, %cleanup.cont111
  %207 = load i8*, i8** %ip, align 4, !tbaa !7
  store i8* %207, i8** %filledIp, align 4, !tbaa !7
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end202
  %208 = load i8*, i8** %ip, align 4, !tbaa !7
  %209 = load i8*, i8** %anchor, align 4, !tbaa !7
  %cmp203 = icmp ugt i8* %208, %209
  %conv204 = zext i1 %cmp203 to i32
  %210 = load i8*, i8** %match, align 4, !tbaa !7
  %211 = load i8*, i8** %lowLimit, align 4, !tbaa !7
  %cmp205 = icmp ugt i8* %210, %211
  %conv206 = zext i1 %cmp205 to i32
  %and = and i32 %conv204, %conv206
  %tobool207 = icmp ne i32 %and, 0
  br i1 %tobool207, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %212 = load i8*, i8** %ip, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i8, i8* %212, i32 -1
  %213 = load i8, i8* %arrayidx, align 1, !tbaa !9
  %conv208 = zext i8 %213 to i32
  %214 = load i8*, i8** %match, align 4, !tbaa !7
  %arrayidx209 = getelementptr inbounds i8, i8* %214, i32 -1
  %215 = load i8, i8* %arrayidx209, align 1, !tbaa !9
  %conv210 = zext i8 %215 to i32
  %cmp211 = icmp eq i32 %conv208, %conv210
  %conv212 = zext i1 %cmp211 to i32
  %cmp213 = icmp ne i32 %conv212, 0
  %conv214 = zext i1 %cmp213 to i32
  %expval215 = call i32 @llvm.expect.i32(i32 %conv214, i32 0)
  %tobool216 = icmp ne i32 %expval215, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %216 = phi i1 [ false, %while.cond ], [ %tobool216, %land.rhs ]
  br i1 %216, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %217 = load i8*, i8** %ip, align 4, !tbaa !7
  %incdec.ptr217 = getelementptr inbounds i8, i8* %217, i32 -1
  store i8* %incdec.ptr217, i8** %ip, align 4, !tbaa !7
  %218 = load i8*, i8** %match, align 4, !tbaa !7
  %incdec.ptr218 = getelementptr inbounds i8, i8* %218, i32 -1
  store i8* %incdec.ptr218, i8** %match, align 4, !tbaa !7
  br label %while.cond

while.end:                                        ; preds = %land.end
  %219 = bitcast i32* %litLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %219) #7
  %220 = load i8*, i8** %ip, align 4, !tbaa !7
  %221 = load i8*, i8** %anchor, align 4, !tbaa !7
  %sub.ptr.lhs.cast219 = ptrtoint i8* %220 to i32
  %sub.ptr.rhs.cast220 = ptrtoint i8* %221 to i32
  %sub.ptr.sub221 = sub i32 %sub.ptr.lhs.cast219, %sub.ptr.rhs.cast220
  store i32 %sub.ptr.sub221, i32* %litLength, align 4, !tbaa !3
  %222 = load i8*, i8** %op, align 4, !tbaa !7
  %incdec.ptr222 = getelementptr inbounds i8, i8* %222, i32 1
  store i8* %incdec.ptr222, i8** %op, align 4, !tbaa !7
  store i8* %222, i8** %token, align 4, !tbaa !7
  %223 = load i32, i32* %outputDirective.addr, align 4, !tbaa !9
  %cmp223 = icmp eq i32 %223, 1
  br i1 %cmp223, label %land.lhs.true225, label %if.end236

land.lhs.true225:                                 ; preds = %while.end
  %224 = load i8*, i8** %op, align 4, !tbaa !7
  %225 = load i32, i32* %litLength, align 4, !tbaa !3
  %add.ptr226 = getelementptr inbounds i8, i8* %224, i32 %225
  %add.ptr227 = getelementptr inbounds i8, i8* %add.ptr226, i32 8
  %226 = load i32, i32* %litLength, align 4, !tbaa !3
  %div = udiv i32 %226, 255
  %add.ptr228 = getelementptr inbounds i8, i8* %add.ptr227, i32 %div
  %227 = load i8*, i8** %olimit, align 4, !tbaa !7
  %cmp229 = icmp ugt i8* %add.ptr228, %227
  %conv230 = zext i1 %cmp229 to i32
  %cmp231 = icmp ne i32 %conv230, 0
  %conv232 = zext i1 %cmp231 to i32
  %expval233 = call i32 @llvm.expect.i32(i32 %conv232, i32 0)
  %tobool234 = icmp ne i32 %expval233, 0
  br i1 %tobool234, label %if.then235, label %if.end236

if.then235:                                       ; preds = %land.lhs.true225
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup274

if.end236:                                        ; preds = %land.lhs.true225, %while.end
  %228 = load i32, i32* %outputDirective.addr, align 4, !tbaa !9
  %cmp237 = icmp eq i32 %228, 2
  br i1 %cmp237, label %land.lhs.true239, label %if.end256

land.lhs.true239:                                 ; preds = %if.end236
  %229 = load i8*, i8** %op, align 4, !tbaa !7
  %230 = load i32, i32* %litLength, align 4, !tbaa !3
  %add240 = add i32 %230, 240
  %div241 = udiv i32 %add240, 255
  %add.ptr242 = getelementptr inbounds i8, i8* %229, i32 %div241
  %231 = load i32, i32* %litLength, align 4, !tbaa !3
  %add.ptr243 = getelementptr inbounds i8, i8* %add.ptr242, i32 %231
  %add.ptr244 = getelementptr inbounds i8, i8* %add.ptr243, i32 2
  %add.ptr245 = getelementptr inbounds i8, i8* %add.ptr244, i32 1
  %add.ptr246 = getelementptr inbounds i8, i8* %add.ptr245, i32 12
  %add.ptr247 = getelementptr inbounds i8, i8* %add.ptr246, i32 -4
  %232 = load i8*, i8** %olimit, align 4, !tbaa !7
  %cmp248 = icmp ugt i8* %add.ptr247, %232
  %conv249 = zext i1 %cmp248 to i32
  %cmp250 = icmp ne i32 %conv249, 0
  %conv251 = zext i1 %cmp250 to i32
  %expval252 = call i32 @llvm.expect.i32(i32 %conv251, i32 0)
  %tobool253 = icmp ne i32 %expval252, 0
  br i1 %tobool253, label %if.then254, label %if.end256

if.then254:                                       ; preds = %land.lhs.true239
  %233 = load i8*, i8** %op, align 4, !tbaa !7
  %incdec.ptr255 = getelementptr inbounds i8, i8* %233, i32 -1
  store i8* %incdec.ptr255, i8** %op, align 4, !tbaa !7
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup274

if.end256:                                        ; preds = %land.lhs.true239, %if.end236
  %234 = load i32, i32* %litLength, align 4, !tbaa !3
  %cmp257 = icmp uge i32 %234, 15
  br i1 %cmp257, label %if.then259, label %if.else268

if.then259:                                       ; preds = %if.end256
  %235 = bitcast i32* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %235) #7
  %236 = load i32, i32* %litLength, align 4, !tbaa !3
  %sub260 = sub i32 %236, 15
  store i32 %sub260, i32* %len, align 4, !tbaa !3
  %237 = load i8*, i8** %token, align 4, !tbaa !7
  store i8 -16, i8* %237, align 1, !tbaa !9
  br label %for.cond261

for.cond261:                                      ; preds = %for.inc, %if.then259
  %238 = load i32, i32* %len, align 4, !tbaa !3
  %cmp262 = icmp sge i32 %238, 255
  br i1 %cmp262, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond261
  %239 = load i8*, i8** %op, align 4, !tbaa !7
  %incdec.ptr264 = getelementptr inbounds i8, i8* %239, i32 1
  store i8* %incdec.ptr264, i8** %op, align 4, !tbaa !7
  store i8 -1, i8* %239, align 1, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %240 = load i32, i32* %len, align 4, !tbaa !3
  %sub265 = sub nsw i32 %240, 255
  store i32 %sub265, i32* %len, align 4, !tbaa !3
  br label %for.cond261

for.end:                                          ; preds = %for.cond261
  %241 = load i32, i32* %len, align 4, !tbaa !3
  %conv266 = trunc i32 %241 to i8
  %242 = load i8*, i8** %op, align 4, !tbaa !7
  %incdec.ptr267 = getelementptr inbounds i8, i8* %242, i32 1
  store i8* %incdec.ptr267, i8** %op, align 4, !tbaa !7
  store i8 %conv266, i8* %242, align 1, !tbaa !9
  %243 = bitcast i32* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %243) #7
  br label %if.end271

if.else268:                                       ; preds = %if.end256
  %244 = load i32, i32* %litLength, align 4, !tbaa !3
  %shl269 = shl i32 %244, 4
  %conv270 = trunc i32 %shl269 to i8
  %245 = load i8*, i8** %token, align 4, !tbaa !7
  store i8 %conv270, i8* %245, align 1, !tbaa !9
  br label %if.end271

if.end271:                                        ; preds = %if.else268, %for.end
  %246 = load i8*, i8** %op, align 4, !tbaa !7
  %247 = load i8*, i8** %anchor, align 4, !tbaa !7
  %248 = load i8*, i8** %op, align 4, !tbaa !7
  %249 = load i32, i32* %litLength, align 4, !tbaa !3
  %add.ptr272 = getelementptr inbounds i8, i8* %248, i32 %249
  call void @LZ4_wildCopy8(i8* %246, i8* %247, i8* %add.ptr272)
  %250 = load i32, i32* %litLength, align 4, !tbaa !3
  %251 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr273 = getelementptr inbounds i8, i8* %251, i32 %250
  store i8* %add.ptr273, i8** %op, align 4, !tbaa !7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup274

cleanup274:                                       ; preds = %if.then254, %if.end271, %if.then235
  %252 = bitcast i32* %litLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %252) #7
  %cleanup.dest275 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest275, label %cleanup511 [
    i32 0, label %cleanup.cont276
  ]

cleanup.cont276:                                  ; preds = %cleanup274
  br label %_next_match

_next_match:                                      ; preds = %cleanup503, %if.then432, %cleanup.cont276
  %253 = load i32, i32* %outputDirective.addr, align 4, !tbaa !9
  %cmp277 = icmp eq i32 %253, 2
  br i1 %cmp277, label %land.lhs.true279, label %if.end287

land.lhs.true279:                                 ; preds = %_next_match
  %254 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr280 = getelementptr inbounds i8, i8* %254, i32 2
  %add.ptr281 = getelementptr inbounds i8, i8* %add.ptr280, i32 1
  %add.ptr282 = getelementptr inbounds i8, i8* %add.ptr281, i32 12
  %add.ptr283 = getelementptr inbounds i8, i8* %add.ptr282, i32 -4
  %255 = load i8*, i8** %olimit, align 4, !tbaa !7
  %cmp284 = icmp ugt i8* %add.ptr283, %255
  br i1 %cmp284, label %if.then286, label %if.end287

if.then286:                                       ; preds = %land.lhs.true279
  %256 = load i8*, i8** %token, align 4, !tbaa !7
  store i8* %256, i8** %op, align 4, !tbaa !7
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup511

if.end287:                                        ; preds = %land.lhs.true279, %_next_match
  %257 = load i32, i32* %maybe_extMem, align 4, !tbaa !3
  %tobool288 = icmp ne i32 %257, 0
  br i1 %tobool288, label %if.then289, label %if.else292

if.then289:                                       ; preds = %if.end287
  %258 = load i8*, i8** %op, align 4, !tbaa !7
  %259 = load i32, i32* %offset, align 4, !tbaa !3
  %conv290 = trunc i32 %259 to i16
  call void @LZ4_writeLE16(i8* %258, i16 zeroext %conv290)
  %260 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr291 = getelementptr inbounds i8, i8* %260, i32 2
  store i8* %add.ptr291, i8** %op, align 4, !tbaa !7
  br label %if.end298

if.else292:                                       ; preds = %if.end287
  %261 = load i8*, i8** %op, align 4, !tbaa !7
  %262 = load i8*, i8** %ip, align 4, !tbaa !7
  %263 = load i8*, i8** %match, align 4, !tbaa !7
  %sub.ptr.lhs.cast293 = ptrtoint i8* %262 to i32
  %sub.ptr.rhs.cast294 = ptrtoint i8* %263 to i32
  %sub.ptr.sub295 = sub i32 %sub.ptr.lhs.cast293, %sub.ptr.rhs.cast294
  %conv296 = trunc i32 %sub.ptr.sub295 to i16
  call void @LZ4_writeLE16(i8* %261, i16 zeroext %conv296)
  %264 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr297 = getelementptr inbounds i8, i8* %264, i32 2
  store i8* %add.ptr297, i8** %op, align 4, !tbaa !7
  br label %if.end298

if.end298:                                        ; preds = %if.else292, %if.then289
  %265 = bitcast i32* %matchCode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %265) #7
  %266 = load i32, i32* %dictDirective.addr, align 4, !tbaa !9
  %cmp299 = icmp eq i32 %266, 2
  br i1 %cmp299, label %land.lhs.true303, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end298
  %267 = load i32, i32* %dictDirective.addr, align 4, !tbaa !9
  %cmp301 = icmp eq i32 %267, 3
  br i1 %cmp301, label %land.lhs.true303, label %if.else327

land.lhs.true303:                                 ; preds = %lor.lhs.false, %if.end298
  %268 = load i8*, i8** %lowLimit, align 4, !tbaa !7
  %269 = load i8*, i8** %dictionary, align 4, !tbaa !7
  %cmp304 = icmp eq i8* %268, %269
  br i1 %cmp304, label %if.then306, label %if.else327

if.then306:                                       ; preds = %land.lhs.true303
  %270 = bitcast i8** %limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %270) #7
  %271 = load i8*, i8** %ip, align 4, !tbaa !7
  %272 = load i8*, i8** %dictEnd, align 4, !tbaa !7
  %273 = load i8*, i8** %match, align 4, !tbaa !7
  %sub.ptr.lhs.cast307 = ptrtoint i8* %272 to i32
  %sub.ptr.rhs.cast308 = ptrtoint i8* %273 to i32
  %sub.ptr.sub309 = sub i32 %sub.ptr.lhs.cast307, %sub.ptr.rhs.cast308
  %add.ptr310 = getelementptr inbounds i8, i8* %271, i32 %sub.ptr.sub309
  store i8* %add.ptr310, i8** %limit, align 4, !tbaa !7
  %274 = load i8*, i8** %limit, align 4, !tbaa !7
  %275 = load i8*, i8** %matchlimit, align 4, !tbaa !7
  %cmp311 = icmp ugt i8* %274, %275
  br i1 %cmp311, label %if.then313, label %if.end314

if.then313:                                       ; preds = %if.then306
  %276 = load i8*, i8** %matchlimit, align 4, !tbaa !7
  store i8* %276, i8** %limit, align 4, !tbaa !7
  br label %if.end314

if.end314:                                        ; preds = %if.then313, %if.then306
  %277 = load i8*, i8** %ip, align 4, !tbaa !7
  %add.ptr315 = getelementptr inbounds i8, i8* %277, i32 4
  %278 = load i8*, i8** %match, align 4, !tbaa !7
  %add.ptr316 = getelementptr inbounds i8, i8* %278, i32 4
  %279 = load i8*, i8** %limit, align 4, !tbaa !7
  %call317 = call i32 @LZ4_count(i8* %add.ptr315, i8* %add.ptr316, i8* %279)
  store i32 %call317, i32* %matchCode, align 4, !tbaa !3
  %280 = load i32, i32* %matchCode, align 4, !tbaa !3
  %add318 = add i32 %280, 4
  %281 = load i8*, i8** %ip, align 4, !tbaa !7
  %add.ptr319 = getelementptr inbounds i8, i8* %281, i32 %add318
  store i8* %add.ptr319, i8** %ip, align 4, !tbaa !7
  %282 = load i8*, i8** %ip, align 4, !tbaa !7
  %283 = load i8*, i8** %limit, align 4, !tbaa !7
  %cmp320 = icmp eq i8* %282, %283
  br i1 %cmp320, label %if.then322, label %if.end326

if.then322:                                       ; preds = %if.end314
  %284 = bitcast i32* %more to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %284) #7
  %285 = load i8*, i8** %limit, align 4, !tbaa !7
  %286 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %287 = load i8*, i8** %matchlimit, align 4, !tbaa !7
  %call323 = call i32 @LZ4_count(i8* %285, i8* %286, i8* %287)
  store i32 %call323, i32* %more, align 4, !tbaa !3
  %288 = load i32, i32* %more, align 4, !tbaa !3
  %289 = load i32, i32* %matchCode, align 4, !tbaa !3
  %add324 = add i32 %289, %288
  store i32 %add324, i32* %matchCode, align 4, !tbaa !3
  %290 = load i32, i32* %more, align 4, !tbaa !3
  %291 = load i8*, i8** %ip, align 4, !tbaa !7
  %add.ptr325 = getelementptr inbounds i8, i8* %291, i32 %290
  store i8* %add.ptr325, i8** %ip, align 4, !tbaa !7
  %292 = bitcast i32* %more to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %292) #7
  br label %if.end326

if.end326:                                        ; preds = %if.then322, %if.end314
  %293 = bitcast i8** %limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %293) #7
  br label %if.end333

if.else327:                                       ; preds = %land.lhs.true303, %lor.lhs.false
  %294 = load i8*, i8** %ip, align 4, !tbaa !7
  %add.ptr328 = getelementptr inbounds i8, i8* %294, i32 4
  %295 = load i8*, i8** %match, align 4, !tbaa !7
  %add.ptr329 = getelementptr inbounds i8, i8* %295, i32 4
  %296 = load i8*, i8** %matchlimit, align 4, !tbaa !7
  %call330 = call i32 @LZ4_count(i8* %add.ptr328, i8* %add.ptr329, i8* %296)
  store i32 %call330, i32* %matchCode, align 4, !tbaa !3
  %297 = load i32, i32* %matchCode, align 4, !tbaa !3
  %add331 = add i32 %297, 4
  %298 = load i8*, i8** %ip, align 4, !tbaa !7
  %add.ptr332 = getelementptr inbounds i8, i8* %298, i32 %add331
  store i8* %add.ptr332, i8** %ip, align 4, !tbaa !7
  br label %if.end333

if.end333:                                        ; preds = %if.else327, %if.end326
  %299 = load i32, i32* %outputDirective.addr, align 4, !tbaa !9
  %tobool334 = icmp ne i32 %299, 0
  br i1 %tobool334, label %land.lhs.true335, label %if.end380

land.lhs.true335:                                 ; preds = %if.end333
  %300 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr336 = getelementptr inbounds i8, i8* %300, i32 6
  %301 = load i32, i32* %matchCode, align 4, !tbaa !3
  %add337 = add i32 %301, 240
  %div338 = udiv i32 %add337, 255
  %add.ptr339 = getelementptr inbounds i8, i8* %add.ptr336, i32 %div338
  %302 = load i8*, i8** %olimit, align 4, !tbaa !7
  %cmp340 = icmp ugt i8* %add.ptr339, %302
  %conv341 = zext i1 %cmp340 to i32
  %cmp342 = icmp ne i32 %conv341, 0
  %conv343 = zext i1 %cmp342 to i32
  %expval344 = call i32 @llvm.expect.i32(i32 %conv343, i32 0)
  %tobool345 = icmp ne i32 %expval344, 0
  br i1 %tobool345, label %if.then346, label %if.end380

if.then346:                                       ; preds = %land.lhs.true335
  %303 = load i32, i32* %outputDirective.addr, align 4, !tbaa !9
  %cmp347 = icmp eq i32 %303, 2
  br i1 %cmp347, label %if.then349, label %if.else378

if.then349:                                       ; preds = %if.then346
  %304 = bitcast i32* %newMatchCode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %304) #7
  %305 = load i8*, i8** %olimit, align 4, !tbaa !7
  %306 = load i8*, i8** %op, align 4, !tbaa !7
  %sub.ptr.lhs.cast350 = ptrtoint i8* %305 to i32
  %sub.ptr.rhs.cast351 = ptrtoint i8* %306 to i32
  %sub.ptr.sub352 = sub i32 %sub.ptr.lhs.cast350, %sub.ptr.rhs.cast351
  %sub353 = sub i32 %sub.ptr.sub352, 1
  %sub354 = sub i32 %sub353, 5
  %mul = mul i32 %sub354, 255
  %add355 = add i32 14, %mul
  store i32 %add355, i32* %newMatchCode, align 4, !tbaa !3
  %307 = load i32, i32* %matchCode, align 4, !tbaa !3
  %308 = load i32, i32* %newMatchCode, align 4, !tbaa !3
  %sub356 = sub i32 %307, %308
  %309 = load i8*, i8** %ip, align 4, !tbaa !7
  %idx.neg357 = sub i32 0, %sub356
  %add.ptr358 = getelementptr inbounds i8, i8* %309, i32 %idx.neg357
  store i8* %add.ptr358, i8** %ip, align 4, !tbaa !7
  %310 = load i32, i32* %newMatchCode, align 4, !tbaa !3
  store i32 %310, i32* %matchCode, align 4, !tbaa !3
  %311 = load i8*, i8** %ip, align 4, !tbaa !7
  %312 = load i8*, i8** %filledIp, align 4, !tbaa !7
  %cmp359 = icmp ule i8* %311, %312
  %conv360 = zext i1 %cmp359 to i32
  %cmp361 = icmp ne i32 %conv360, 0
  %conv362 = zext i1 %cmp361 to i32
  %expval363 = call i32 @llvm.expect.i32(i32 %conv362, i32 0)
  %tobool364 = icmp ne i32 %expval363, 0
  br i1 %tobool364, label %if.then365, label %if.end377

if.then365:                                       ; preds = %if.then349
  %313 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %313) #7
  %314 = load i8*, i8** %ip, align 4, !tbaa !7
  store i8* %314, i8** %ptr, align 4, !tbaa !7
  br label %for.cond366

for.cond366:                                      ; preds = %for.inc374, %if.then365
  %315 = load i8*, i8** %ptr, align 4, !tbaa !7
  %316 = load i8*, i8** %filledIp, align 4, !tbaa !7
  %cmp367 = icmp ule i8* %315, %316
  br i1 %cmp367, label %for.body369, label %for.end376

for.body369:                                      ; preds = %for.cond366
  %317 = bitcast i32* %h370 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %317) #7
  %318 = load i8*, i8** %ptr, align 4, !tbaa !7
  %319 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %call371 = call i32 @LZ4_hashPosition(i8* %318, i32 %319)
  store i32 %call371, i32* %h370, align 4, !tbaa !3
  %320 = load i32, i32* %h370, align 4, !tbaa !3
  %321 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %hashTable372 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %321, i32 0, i32 0
  %arraydecay373 = getelementptr inbounds [4096 x i32], [4096 x i32]* %hashTable372, i32 0, i32 0
  %322 = bitcast i32* %arraydecay373 to i8*
  %323 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  call void @LZ4_clearHash(i32 %320, i8* %322, i32 %323)
  %324 = bitcast i32* %h370 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %324) #7
  br label %for.inc374

for.inc374:                                       ; preds = %for.body369
  %325 = load i8*, i8** %ptr, align 4, !tbaa !7
  %incdec.ptr375 = getelementptr inbounds i8, i8* %325, i32 1
  store i8* %incdec.ptr375, i8** %ptr, align 4, !tbaa !7
  br label %for.cond366

for.end376:                                       ; preds = %for.cond366
  %326 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %326) #7
  br label %if.end377

if.end377:                                        ; preds = %for.end376, %if.then349
  %327 = bitcast i32* %newMatchCode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %327) #7
  br label %if.end379

if.else378:                                       ; preds = %if.then346
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup406

if.end379:                                        ; preds = %if.end377
  br label %if.end380

if.end380:                                        ; preds = %if.end379, %land.lhs.true335, %if.end333
  %328 = load i32, i32* %matchCode, align 4, !tbaa !3
  %cmp381 = icmp uge i32 %328, 15
  br i1 %cmp381, label %if.then383, label %if.else399

if.then383:                                       ; preds = %if.end380
  %329 = load i8*, i8** %token, align 4, !tbaa !7
  %330 = load i8, i8* %329, align 1, !tbaa !9
  %conv384 = zext i8 %330 to i32
  %add385 = add i32 %conv384, 15
  %conv386 = trunc i32 %add385 to i8
  store i8 %conv386, i8* %329, align 1, !tbaa !9
  %331 = load i32, i32* %matchCode, align 4, !tbaa !3
  %sub387 = sub i32 %331, 15
  store i32 %sub387, i32* %matchCode, align 4, !tbaa !3
  %332 = load i8*, i8** %op, align 4, !tbaa !7
  call void @LZ4_write32(i8* %332, i32 -1)
  br label %while.cond388

while.cond388:                                    ; preds = %while.body391, %if.then383
  %333 = load i32, i32* %matchCode, align 4, !tbaa !3
  %cmp389 = icmp uge i32 %333, 1020
  br i1 %cmp389, label %while.body391, label %while.end394

while.body391:                                    ; preds = %while.cond388
  %334 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr392 = getelementptr inbounds i8, i8* %334, i32 4
  store i8* %add.ptr392, i8** %op, align 4, !tbaa !7
  %335 = load i8*, i8** %op, align 4, !tbaa !7
  call void @LZ4_write32(i8* %335, i32 -1)
  %336 = load i32, i32* %matchCode, align 4, !tbaa !3
  %sub393 = sub i32 %336, 1020
  store i32 %sub393, i32* %matchCode, align 4, !tbaa !3
  br label %while.cond388

while.end394:                                     ; preds = %while.cond388
  %337 = load i32, i32* %matchCode, align 4, !tbaa !3
  %div395 = udiv i32 %337, 255
  %338 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr396 = getelementptr inbounds i8, i8* %338, i32 %div395
  store i8* %add.ptr396, i8** %op, align 4, !tbaa !7
  %339 = load i32, i32* %matchCode, align 4, !tbaa !3
  %rem = urem i32 %339, 255
  %conv397 = trunc i32 %rem to i8
  %340 = load i8*, i8** %op, align 4, !tbaa !7
  %incdec.ptr398 = getelementptr inbounds i8, i8* %340, i32 1
  store i8* %incdec.ptr398, i8** %op, align 4, !tbaa !7
  store i8 %conv397, i8* %340, align 1, !tbaa !9
  br label %if.end405

if.else399:                                       ; preds = %if.end380
  %341 = load i32, i32* %matchCode, align 4, !tbaa !3
  %conv400 = trunc i32 %341 to i8
  %conv401 = zext i8 %conv400 to i32
  %342 = load i8*, i8** %token, align 4, !tbaa !7
  %343 = load i8, i8* %342, align 1, !tbaa !9
  %conv402 = zext i8 %343 to i32
  %add403 = add nsw i32 %conv402, %conv401
  %conv404 = trunc i32 %add403 to i8
  store i8 %conv404, i8* %342, align 1, !tbaa !9
  br label %if.end405

if.end405:                                        ; preds = %if.else399, %while.end394
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup406

cleanup406:                                       ; preds = %if.end405, %if.else378
  %344 = bitcast i32* %matchCode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %344) #7
  %cleanup.dest407 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest407, label %cleanup511 [
    i32 0, label %cleanup.cont408
  ]

cleanup.cont408:                                  ; preds = %cleanup406
  %345 = load i8*, i8** %ip, align 4, !tbaa !7
  store i8* %345, i8** %anchor, align 4, !tbaa !7
  %346 = load i8*, i8** %ip, align 4, !tbaa !7
  %347 = load i8*, i8** %mflimitPlusOne, align 4, !tbaa !7
  %cmp409 = icmp uge i8* %346, %347
  br i1 %cmp409, label %if.then411, label %if.end412

if.then411:                                       ; preds = %cleanup.cont408
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup511

if.end412:                                        ; preds = %cleanup.cont408
  %348 = load i8*, i8** %ip, align 4, !tbaa !7
  %add.ptr413 = getelementptr inbounds i8, i8* %348, i32 -2
  %349 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %hashTable414 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %349, i32 0, i32 0
  %arraydecay415 = getelementptr inbounds [4096 x i32], [4096 x i32]* %hashTable414, i32 0, i32 0
  %350 = bitcast i32* %arraydecay415 to i8*
  %351 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %352 = load i8*, i8** %base, align 4, !tbaa !7
  call void @LZ4_putPosition(i8* %add.ptr413, i8* %350, i32 %351, i8* %352)
  %353 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %cmp416 = icmp eq i32 %353, 1
  br i1 %cmp416, label %if.then418, label %if.else435

if.then418:                                       ; preds = %if.end412
  %354 = load i8*, i8** %ip, align 4, !tbaa !7
  %355 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %hashTable419 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %355, i32 0, i32 0
  %arraydecay420 = getelementptr inbounds [4096 x i32], [4096 x i32]* %hashTable419, i32 0, i32 0
  %356 = bitcast i32* %arraydecay420 to i8*
  %357 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %358 = load i8*, i8** %base, align 4, !tbaa !7
  %call421 = call i8* @LZ4_getPosition(i8* %354, i8* %356, i32 %357, i8* %358)
  store i8* %call421, i8** %match, align 4, !tbaa !7
  %359 = load i8*, i8** %ip, align 4, !tbaa !7
  %360 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %hashTable422 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %360, i32 0, i32 0
  %arraydecay423 = getelementptr inbounds [4096 x i32], [4096 x i32]* %hashTable422, i32 0, i32 0
  %361 = bitcast i32* %arraydecay423 to i8*
  %362 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %363 = load i8*, i8** %base, align 4, !tbaa !7
  call void @LZ4_putPosition(i8* %359, i8* %361, i32 %362, i8* %363)
  %364 = load i8*, i8** %match, align 4, !tbaa !7
  %add.ptr424 = getelementptr inbounds i8, i8* %364, i32 65535
  %365 = load i8*, i8** %ip, align 4, !tbaa !7
  %cmp425 = icmp uge i8* %add.ptr424, %365
  br i1 %cmp425, label %land.lhs.true427, label %if.end434

land.lhs.true427:                                 ; preds = %if.then418
  %366 = load i8*, i8** %match, align 4, !tbaa !7
  %call428 = call i32 @LZ4_read32(i8* %366)
  %367 = load i8*, i8** %ip, align 4, !tbaa !7
  %call429 = call i32 @LZ4_read32(i8* %367)
  %cmp430 = icmp eq i32 %call428, %call429
  br i1 %cmp430, label %if.then432, label %if.end434

if.then432:                                       ; preds = %land.lhs.true427
  %368 = load i8*, i8** %op, align 4, !tbaa !7
  %incdec.ptr433 = getelementptr inbounds i8, i8* %368, i32 1
  store i8* %incdec.ptr433, i8** %op, align 4, !tbaa !7
  store i8* %368, i8** %token, align 4, !tbaa !7
  %369 = load i8*, i8** %token, align 4, !tbaa !7
  store i8 0, i8* %369, align 1, !tbaa !9
  br label %_next_match

if.end434:                                        ; preds = %land.lhs.true427, %if.then418
  br label %if.end508

if.else435:                                       ; preds = %if.end412
  %370 = bitcast i32* %h436 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %370) #7
  %371 = load i8*, i8** %ip, align 4, !tbaa !7
  %372 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %call437 = call i32 @LZ4_hashPosition(i8* %371, i32 %372)
  store i32 %call437, i32* %h436, align 4, !tbaa !3
  %373 = bitcast i32* %current438 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %373) #7
  %374 = load i8*, i8** %ip, align 4, !tbaa !7
  %375 = load i8*, i8** %base, align 4, !tbaa !7
  %sub.ptr.lhs.cast439 = ptrtoint i8* %374 to i32
  %sub.ptr.rhs.cast440 = ptrtoint i8* %375 to i32
  %sub.ptr.sub441 = sub i32 %sub.ptr.lhs.cast439, %sub.ptr.rhs.cast440
  store i32 %sub.ptr.sub441, i32* %current438, align 4, !tbaa !3
  %376 = bitcast i32* %matchIndex442 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %376) #7
  %377 = load i32, i32* %h436, align 4, !tbaa !3
  %378 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %hashTable443 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %378, i32 0, i32 0
  %arraydecay444 = getelementptr inbounds [4096 x i32], [4096 x i32]* %hashTable443, i32 0, i32 0
  %379 = bitcast i32* %arraydecay444 to i8*
  %380 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %call445 = call i32 @LZ4_getIndexOnHash(i32 %377, i8* %379, i32 %380)
  store i32 %call445, i32* %matchIndex442, align 4, !tbaa !3
  %381 = load i32, i32* %dictDirective.addr, align 4, !tbaa !9
  %cmp446 = icmp eq i32 %381, 3
  br i1 %cmp446, label %if.then448, label %if.else460

if.then448:                                       ; preds = %if.else435
  %382 = load i32, i32* %matchIndex442, align 4, !tbaa !3
  %383 = load i32, i32* %startIndex, align 4, !tbaa !3
  %cmp449 = icmp ult i32 %382, %383
  br i1 %cmp449, label %if.then451, label %if.else457

if.then451:                                       ; preds = %if.then448
  %384 = load i32, i32* %h436, align 4, !tbaa !3
  %385 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dictCtx, align 4, !tbaa !7
  %hashTable452 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %385, i32 0, i32 0
  %arraydecay453 = getelementptr inbounds [4096 x i32], [4096 x i32]* %hashTable452, i32 0, i32 0
  %386 = bitcast i32* %arraydecay453 to i8*
  %call454 = call i32 @LZ4_getIndexOnHash(i32 %384, i8* %386, i32 2)
  store i32 %call454, i32* %matchIndex442, align 4, !tbaa !3
  %387 = load i8*, i8** %dictBase, align 4, !tbaa !7
  %388 = load i32, i32* %matchIndex442, align 4, !tbaa !3
  %add.ptr455 = getelementptr inbounds i8, i8* %387, i32 %388
  store i8* %add.ptr455, i8** %match, align 4, !tbaa !7
  %389 = load i8*, i8** %dictionary, align 4, !tbaa !7
  store i8* %389, i8** %lowLimit, align 4, !tbaa !7
  %390 = load i32, i32* %dictDelta, align 4, !tbaa !3
  %391 = load i32, i32* %matchIndex442, align 4, !tbaa !3
  %add456 = add i32 %391, %390
  store i32 %add456, i32* %matchIndex442, align 4, !tbaa !3
  br label %if.end459

if.else457:                                       ; preds = %if.then448
  %392 = load i8*, i8** %base, align 4, !tbaa !7
  %393 = load i32, i32* %matchIndex442, align 4, !tbaa !3
  %add.ptr458 = getelementptr inbounds i8, i8* %392, i32 %393
  store i8* %add.ptr458, i8** %match, align 4, !tbaa !7
  %394 = load i8*, i8** %source.addr, align 4, !tbaa !7
  store i8* %394, i8** %lowLimit, align 4, !tbaa !7
  br label %if.end459

if.end459:                                        ; preds = %if.else457, %if.then451
  br label %if.end474

if.else460:                                       ; preds = %if.else435
  %395 = load i32, i32* %dictDirective.addr, align 4, !tbaa !9
  %cmp461 = icmp eq i32 %395, 2
  br i1 %cmp461, label %if.then463, label %if.else471

if.then463:                                       ; preds = %if.else460
  %396 = load i32, i32* %matchIndex442, align 4, !tbaa !3
  %397 = load i32, i32* %startIndex, align 4, !tbaa !3
  %cmp464 = icmp ult i32 %396, %397
  br i1 %cmp464, label %if.then466, label %if.else468

if.then466:                                       ; preds = %if.then463
  %398 = load i8*, i8** %dictBase, align 4, !tbaa !7
  %399 = load i32, i32* %matchIndex442, align 4, !tbaa !3
  %add.ptr467 = getelementptr inbounds i8, i8* %398, i32 %399
  store i8* %add.ptr467, i8** %match, align 4, !tbaa !7
  %400 = load i8*, i8** %dictionary, align 4, !tbaa !7
  store i8* %400, i8** %lowLimit, align 4, !tbaa !7
  br label %if.end470

if.else468:                                       ; preds = %if.then463
  %401 = load i8*, i8** %base, align 4, !tbaa !7
  %402 = load i32, i32* %matchIndex442, align 4, !tbaa !3
  %add.ptr469 = getelementptr inbounds i8, i8* %401, i32 %402
  store i8* %add.ptr469, i8** %match, align 4, !tbaa !7
  %403 = load i8*, i8** %source.addr, align 4, !tbaa !7
  store i8* %403, i8** %lowLimit, align 4, !tbaa !7
  br label %if.end470

if.end470:                                        ; preds = %if.else468, %if.then466
  br label %if.end473

if.else471:                                       ; preds = %if.else460
  %404 = load i8*, i8** %base, align 4, !tbaa !7
  %405 = load i32, i32* %matchIndex442, align 4, !tbaa !3
  %add.ptr472 = getelementptr inbounds i8, i8* %404, i32 %405
  store i8* %add.ptr472, i8** %match, align 4, !tbaa !7
  br label %if.end473

if.end473:                                        ; preds = %if.else471, %if.end470
  br label %if.end474

if.end474:                                        ; preds = %if.end473, %if.end459
  %406 = load i32, i32* %current438, align 4, !tbaa !3
  %407 = load i32, i32* %h436, align 4, !tbaa !3
  %408 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %hashTable475 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %408, i32 0, i32 0
  %arraydecay476 = getelementptr inbounds [4096 x i32], [4096 x i32]* %hashTable475, i32 0, i32 0
  %409 = bitcast i32* %arraydecay476 to i8*
  %410 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  call void @LZ4_putIndexOnHash(i32 %406, i32 %407, i8* %409, i32 %410)
  %411 = load i32, i32* %dictIssue.addr, align 4, !tbaa !9
  %cmp477 = icmp eq i32 %411, 1
  br i1 %cmp477, label %cond.true479, label %cond.false482

cond.true479:                                     ; preds = %if.end474
  %412 = load i32, i32* %matchIndex442, align 4, !tbaa !3
  %413 = load i32, i32* %prefixIdxLimit, align 4, !tbaa !3
  %cmp480 = icmp uge i32 %412, %413
  br i1 %cmp480, label %land.lhs.true483, label %if.end502

cond.false482:                                    ; preds = %if.end474
  br i1 true, label %land.lhs.true483, label %if.end502

land.lhs.true483:                                 ; preds = %cond.false482, %cond.true479
  %414 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %cmp484 = icmp eq i32 %414, 3
  br i1 %cmp484, label %cond.true486, label %cond.false487

cond.true486:                                     ; preds = %land.lhs.true483
  br i1 true, label %land.lhs.true491, label %if.end502

cond.false487:                                    ; preds = %land.lhs.true483
  %415 = load i32, i32* %matchIndex442, align 4, !tbaa !3
  %add488 = add i32 %415, 65535
  %416 = load i32, i32* %current438, align 4, !tbaa !3
  %cmp489 = icmp uge i32 %add488, %416
  br i1 %cmp489, label %land.lhs.true491, label %if.end502

land.lhs.true491:                                 ; preds = %cond.false487, %cond.true486
  %417 = load i8*, i8** %match, align 4, !tbaa !7
  %call492 = call i32 @LZ4_read32(i8* %417)
  %418 = load i8*, i8** %ip, align 4, !tbaa !7
  %call493 = call i32 @LZ4_read32(i8* %418)
  %cmp494 = icmp eq i32 %call492, %call493
  br i1 %cmp494, label %if.then496, label %if.end502

if.then496:                                       ; preds = %land.lhs.true491
  %419 = load i8*, i8** %op, align 4, !tbaa !7
  %incdec.ptr497 = getelementptr inbounds i8, i8* %419, i32 1
  store i8* %incdec.ptr497, i8** %op, align 4, !tbaa !7
  store i8* %419, i8** %token, align 4, !tbaa !7
  %420 = load i8*, i8** %token, align 4, !tbaa !7
  store i8 0, i8* %420, align 1, !tbaa !9
  %421 = load i32, i32* %maybe_extMem, align 4, !tbaa !3
  %tobool498 = icmp ne i32 %421, 0
  br i1 %tobool498, label %if.then499, label %if.end501

if.then499:                                       ; preds = %if.then496
  %422 = load i32, i32* %current438, align 4, !tbaa !3
  %423 = load i32, i32* %matchIndex442, align 4, !tbaa !3
  %sub500 = sub i32 %422, %423
  store i32 %sub500, i32* %offset, align 4, !tbaa !3
  br label %if.end501

if.end501:                                        ; preds = %if.then499, %if.then496
  store i32 14, i32* %cleanup.dest.slot, align 4
  br label %cleanup503

if.end502:                                        ; preds = %land.lhs.true491, %cond.false487, %cond.true486, %cond.false482, %cond.true479
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup503

cleanup503:                                       ; preds = %if.end502, %if.end501
  %424 = bitcast i32* %matchIndex442 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %424) #7
  %425 = bitcast i32* %current438 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %425) #7
  %426 = bitcast i32* %h436 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %426) #7
  %cleanup.dest506 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest506, label %unreachable [
    i32 0, label %cleanup.cont507
    i32 14, label %_next_match
  ]

cleanup.cont507:                                  ; preds = %cleanup503
  br label %if.end508

if.end508:                                        ; preds = %cleanup.cont507, %if.end434
  %427 = load i8*, i8** %ip, align 4, !tbaa !7
  %incdec.ptr509 = getelementptr inbounds i8, i8* %427, i32 1
  store i8* %incdec.ptr509, i8** %ip, align 4, !tbaa !7
  %428 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %call510 = call i32 @LZ4_hashPosition(i8* %incdec.ptr509, i32 %428)
  store i32 %call510, i32* %forwardH, align 4, !tbaa !3
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup511

cleanup511:                                       ; preds = %if.then286, %if.end508, %if.then411, %cleanup406, %cleanup274, %cleanup197, %cleanup107
  %429 = bitcast i8** %filledIp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %429) #7
  %430 = bitcast i8** %token to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %430) #7
  %431 = bitcast i8** %match to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %431) #7
  %cleanup.dest514 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest514, label %cleanup579 [
    i32 0, label %cleanup.cont515
    i32 3, label %for.end516
    i32 2, label %_last_literals
  ]

cleanup.cont515:                                  ; preds = %cleanup511
  br label %for.cond

for.end516:                                       ; preds = %cleanup511
  br label %_last_literals

_last_literals:                                   ; preds = %for.end516, %cleanup511, %if.then78
  %432 = bitcast i32* %lastRun to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %432) #7
  %433 = load i8*, i8** %iend, align 4, !tbaa !7
  %434 = load i8*, i8** %anchor, align 4, !tbaa !7
  %sub.ptr.lhs.cast517 = ptrtoint i8* %433 to i32
  %sub.ptr.rhs.cast518 = ptrtoint i8* %434 to i32
  %sub.ptr.sub519 = sub i32 %sub.ptr.lhs.cast517, %sub.ptr.rhs.cast518
  store i32 %sub.ptr.sub519, i32* %lastRun, align 4, !tbaa !10
  %435 = load i32, i32* %outputDirective.addr, align 4, !tbaa !9
  %tobool520 = icmp ne i32 %435, 0
  br i1 %tobool520, label %land.lhs.true521, label %if.end543

land.lhs.true521:                                 ; preds = %_last_literals
  %436 = load i8*, i8** %op, align 4, !tbaa !7
  %437 = load i32, i32* %lastRun, align 4, !tbaa !10
  %add.ptr522 = getelementptr inbounds i8, i8* %436, i32 %437
  %add.ptr523 = getelementptr inbounds i8, i8* %add.ptr522, i32 1
  %438 = load i32, i32* %lastRun, align 4, !tbaa !10
  %add524 = add i32 %438, 255
  %sub525 = sub i32 %add524, 15
  %div526 = udiv i32 %sub525, 255
  %add.ptr527 = getelementptr inbounds i8, i8* %add.ptr523, i32 %div526
  %439 = load i8*, i8** %olimit, align 4, !tbaa !7
  %cmp528 = icmp ugt i8* %add.ptr527, %439
  br i1 %cmp528, label %if.then530, label %if.end543

if.then530:                                       ; preds = %land.lhs.true521
  %440 = load i32, i32* %outputDirective.addr, align 4, !tbaa !9
  %cmp531 = icmp eq i32 %440, 2
  br i1 %cmp531, label %if.then533, label %if.else541

if.then533:                                       ; preds = %if.then530
  %441 = load i8*, i8** %olimit, align 4, !tbaa !7
  %442 = load i8*, i8** %op, align 4, !tbaa !7
  %sub.ptr.lhs.cast534 = ptrtoint i8* %441 to i32
  %sub.ptr.rhs.cast535 = ptrtoint i8* %442 to i32
  %sub.ptr.sub536 = sub i32 %sub.ptr.lhs.cast534, %sub.ptr.rhs.cast535
  %sub537 = sub i32 %sub.ptr.sub536, 1
  store i32 %sub537, i32* %lastRun, align 4, !tbaa !10
  %443 = load i32, i32* %lastRun, align 4, !tbaa !10
  %add538 = add i32 %443, 240
  %div539 = udiv i32 %add538, 255
  %444 = load i32, i32* %lastRun, align 4, !tbaa !10
  %sub540 = sub i32 %444, %div539
  store i32 %sub540, i32* %lastRun, align 4, !tbaa !10
  br label %if.end542

if.else541:                                       ; preds = %if.then530
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup566

if.end542:                                        ; preds = %if.then533
  br label %if.end543

if.end543:                                        ; preds = %if.end542, %land.lhs.true521, %_last_literals
  %445 = load i32, i32* %lastRun, align 4, !tbaa !10
  %cmp544 = icmp uge i32 %445, 15
  br i1 %cmp544, label %if.then546, label %if.else559

if.then546:                                       ; preds = %if.end543
  %446 = bitcast i32* %accumulator to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %446) #7
  %447 = load i32, i32* %lastRun, align 4, !tbaa !10
  %sub547 = sub i32 %447, 15
  store i32 %sub547, i32* %accumulator, align 4, !tbaa !10
  %448 = load i8*, i8** %op, align 4, !tbaa !7
  %incdec.ptr548 = getelementptr inbounds i8, i8* %448, i32 1
  store i8* %incdec.ptr548, i8** %op, align 4, !tbaa !7
  store i8 -16, i8* %448, align 1, !tbaa !9
  br label %for.cond549

for.cond549:                                      ; preds = %for.inc554, %if.then546
  %449 = load i32, i32* %accumulator, align 4, !tbaa !10
  %cmp550 = icmp uge i32 %449, 255
  br i1 %cmp550, label %for.body552, label %for.end556

for.body552:                                      ; preds = %for.cond549
  %450 = load i8*, i8** %op, align 4, !tbaa !7
  %incdec.ptr553 = getelementptr inbounds i8, i8* %450, i32 1
  store i8* %incdec.ptr553, i8** %op, align 4, !tbaa !7
  store i8 -1, i8* %450, align 1, !tbaa !9
  br label %for.inc554

for.inc554:                                       ; preds = %for.body552
  %451 = load i32, i32* %accumulator, align 4, !tbaa !10
  %sub555 = sub i32 %451, 255
  store i32 %sub555, i32* %accumulator, align 4, !tbaa !10
  br label %for.cond549

for.end556:                                       ; preds = %for.cond549
  %452 = load i32, i32* %accumulator, align 4, !tbaa !10
  %conv557 = trunc i32 %452 to i8
  %453 = load i8*, i8** %op, align 4, !tbaa !7
  %incdec.ptr558 = getelementptr inbounds i8, i8* %453, i32 1
  store i8* %incdec.ptr558, i8** %op, align 4, !tbaa !7
  store i8 %conv557, i8* %453, align 1, !tbaa !9
  %454 = bitcast i32* %accumulator to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %454) #7
  br label %if.end563

if.else559:                                       ; preds = %if.end543
  %455 = load i32, i32* %lastRun, align 4, !tbaa !10
  %shl560 = shl i32 %455, 4
  %conv561 = trunc i32 %shl560 to i8
  %456 = load i8*, i8** %op, align 4, !tbaa !7
  %incdec.ptr562 = getelementptr inbounds i8, i8* %456, i32 1
  store i8* %incdec.ptr562, i8** %op, align 4, !tbaa !7
  store i8 %conv561, i8* %456, align 1, !tbaa !9
  br label %if.end563

if.end563:                                        ; preds = %if.else559, %for.end556
  %457 = load i8*, i8** %op, align 4, !tbaa !7
  %458 = load i8*, i8** %anchor, align 4, !tbaa !7
  %459 = load i32, i32* %lastRun, align 4, !tbaa !10
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %457, i8* align 1 %458, i32 %459, i1 false)
  %460 = load i8*, i8** %anchor, align 4, !tbaa !7
  %461 = load i32, i32* %lastRun, align 4, !tbaa !10
  %add.ptr564 = getelementptr inbounds i8, i8* %460, i32 %461
  store i8* %add.ptr564, i8** %ip, align 4, !tbaa !7
  %462 = load i32, i32* %lastRun, align 4, !tbaa !10
  %463 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr565 = getelementptr inbounds i8, i8* %463, i32 %462
  store i8* %add.ptr565, i8** %op, align 4, !tbaa !7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup566

cleanup566:                                       ; preds = %if.end563, %if.else541
  %464 = bitcast i32* %lastRun to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %464) #7
  %cleanup.dest567 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest567, label %cleanup579 [
    i32 0, label %cleanup.cont568
  ]

cleanup.cont568:                                  ; preds = %cleanup566
  %465 = load i32, i32* %outputDirective.addr, align 4, !tbaa !9
  %cmp569 = icmp eq i32 %465, 2
  br i1 %cmp569, label %if.then571, label %if.end575

if.then571:                                       ; preds = %cleanup.cont568
  %466 = load i8*, i8** %ip, align 4, !tbaa !7
  %467 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %sub.ptr.lhs.cast572 = ptrtoint i8* %466 to i32
  %sub.ptr.rhs.cast573 = ptrtoint i8* %467 to i32
  %sub.ptr.sub574 = sub i32 %sub.ptr.lhs.cast572, %sub.ptr.rhs.cast573
  %468 = load i32*, i32** %inputConsumed.addr, align 4, !tbaa !7
  store i32 %sub.ptr.sub574, i32* %468, align 4, !tbaa !3
  br label %if.end575

if.end575:                                        ; preds = %if.then571, %cleanup.cont568
  %469 = load i8*, i8** %op, align 4, !tbaa !7
  %470 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %sub.ptr.lhs.cast576 = ptrtoint i8* %469 to i32
  %sub.ptr.rhs.cast577 = ptrtoint i8* %470 to i32
  %sub.ptr.sub578 = sub i32 %sub.ptr.lhs.cast576, %sub.ptr.rhs.cast577
  store i32 %sub.ptr.sub578, i32* %result, align 4, !tbaa !3
  %471 = load i32, i32* %result, align 4, !tbaa !3
  store i32 %471, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup579

cleanup579:                                       ; preds = %if.end575, %cleanup566, %cleanup511, %if.then55, %if.then50, %if.then
  %472 = bitcast i32* %forwardH to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %472) #7
  %473 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %473) #7
  %474 = bitcast i8** %olimit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %474) #7
  %475 = bitcast i8** %op to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %475) #7
  %476 = bitcast i8** %dictBase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %476) #7
  %477 = bitcast i8** %matchlimit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %477) #7
  %478 = bitcast i8** %mflimitPlusOne to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %478) #7
  %479 = bitcast i8** %iend to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %479) #7
  %480 = bitcast i8** %anchor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %480) #7
  %481 = bitcast i8** %dictEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %481) #7
  %482 = bitcast i32* %prefixIdxLimit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %482) #7
  %483 = bitcast i32* %maybe_extMem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %483) #7
  %484 = bitcast i32* %dictDelta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %484) #7
  %485 = bitcast i32* %dictSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %485) #7
  %486 = bitcast i8** %dictionary to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %486) #7
  %487 = bitcast %struct.LZ4_stream_t_internal** %dictCtx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %487) #7
  %488 = bitcast i8** %lowLimit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %488) #7
  %489 = bitcast i8** %base to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %489) #7
  %490 = bitcast i32* %startIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %490) #7
  %491 = bitcast i8** %ip to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %491) #7
  %492 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %492) #7
  %493 = load i32, i32* %retval, align 4
  ret i32 %493

unreachable:                                      ; preds = %cleanup503
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden i32 @LZ4_compress_fast_extState_fastReset(i8* %state, i8* %src, i8* %dst, i32 %srcSize, i32 %dstCapacity, i32 %acceleration) #0 {
entry:
  %retval = alloca i32, align 4
  %state.addr = alloca i8*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %dstCapacity.addr = alloca i32, align 4
  %acceleration.addr = alloca i32, align 4
  %ctx = alloca %struct.LZ4_stream_t_internal*, align 4
  %tableType = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %tableType9 = alloca i32, align 4
  %tableType16 = alloca i32, align 4
  %tableType25 = alloca i32, align 4
  store i8* %state, i8** %state.addr, align 4, !tbaa !7
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !7
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !3
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !3
  store i32 %acceleration, i32* %acceleration.addr, align 4, !tbaa !3
  %0 = bitcast %struct.LZ4_stream_t_internal** %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8*, i8** %state.addr, align 4, !tbaa !7
  %2 = bitcast i8* %1 to %union.LZ4_stream_u*
  %internal_donotuse = bitcast %union.LZ4_stream_u* %2 to %struct.LZ4_stream_t_internal*
  store %struct.LZ4_stream_t_internal* %internal_donotuse, %struct.LZ4_stream_t_internal** %ctx, align 4, !tbaa !7
  %3 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %cmp = icmp slt i32 %3, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %acceleration.addr, align 4, !tbaa !3
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %5 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_compressBound(i32 %5)
  %cmp1 = icmp sge i32 %4, %call
  br i1 %cmp1, label %if.then2, label %if.else13

if.then2:                                         ; preds = %if.end
  %6 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %cmp3 = icmp slt i32 %6, 65547
  br i1 %cmp3, label %if.then4, label %if.else8

if.then4:                                         ; preds = %if.then2
  %7 = bitcast i32* %tableType to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  store i32 3, i32* %tableType, align 4, !tbaa !9
  %8 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %ctx, align 4, !tbaa !7
  %9 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  call void @LZ4_prepareTable(%struct.LZ4_stream_t_internal* %8, i32 %9, i32 3)
  %10 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %ctx, align 4, !tbaa !7
  %currentOffset = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %10, i32 0, i32 1
  %11 = load i32, i32* %currentOffset, align 4, !tbaa !12
  %tobool = icmp ne i32 %11, 0
  br i1 %tobool, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.then4
  %12 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %ctx, align 4, !tbaa !7
  %13 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %14 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %15 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %16 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %call6 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %12, i8* %13, i8* %14, i32 %15, i32* null, i32 0, i32 0, i32 3, i32 0, i32 1, i32 %16)
  store i32 %call6, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %if.then4
  %17 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %ctx, align 4, !tbaa !7
  %18 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %19 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %20 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %21 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %call7 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %17, i8* %18, i8* %19, i32 %20, i32* null, i32 0, i32 0, i32 3, i32 0, i32 0, i32 %21)
  store i32 %call7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then5
  %22 = bitcast i32* %tableType to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #7
  br label %cleanup30

if.else8:                                         ; preds = %if.then2
  %23 = bitcast i32* %tableType9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #7
  %24 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %25 = ptrtoint i8* %24 to i32
  %cmp10 = icmp ugt i32 %25, 65535
  %26 = zext i1 %cmp10 to i64
  %cond = select i1 %cmp10, i32 1, i32 2
  store i32 %cond, i32* %tableType9, align 4, !tbaa !9
  %27 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %ctx, align 4, !tbaa !7
  %28 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %29 = load i32, i32* %tableType9, align 4, !tbaa !9
  call void @LZ4_prepareTable(%struct.LZ4_stream_t_internal* %27, i32 %28, i32 %29)
  %30 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %ctx, align 4, !tbaa !7
  %31 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %32 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %33 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %34 = load i32, i32* %tableType9, align 4, !tbaa !9
  %35 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %call11 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %30, i8* %31, i8* %32, i32 %33, i32* null, i32 0, i32 0, i32 %34, i32 0, i32 0, i32 %35)
  store i32 %call11, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %36 = bitcast i32* %tableType9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #7
  br label %cleanup30

if.else13:                                        ; preds = %if.end
  %37 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %cmp14 = icmp slt i32 %37, 65547
  br i1 %cmp14, label %if.then15, label %if.else24

if.then15:                                        ; preds = %if.else13
  %38 = bitcast i32* %tableType16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #7
  store i32 3, i32* %tableType16, align 4, !tbaa !9
  %39 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %ctx, align 4, !tbaa !7
  %40 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  call void @LZ4_prepareTable(%struct.LZ4_stream_t_internal* %39, i32 %40, i32 3)
  %41 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %ctx, align 4, !tbaa !7
  %currentOffset17 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %41, i32 0, i32 1
  %42 = load i32, i32* %currentOffset17, align 4, !tbaa !12
  %tobool18 = icmp ne i32 %42, 0
  br i1 %tobool18, label %if.then19, label %if.else21

if.then19:                                        ; preds = %if.then15
  %43 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %ctx, align 4, !tbaa !7
  %44 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %45 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %46 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %47 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %48 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %call20 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %43, i8* %44, i8* %45, i32 %46, i32* null, i32 %47, i32 1, i32 3, i32 0, i32 1, i32 %48)
  store i32 %call20, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

if.else21:                                        ; preds = %if.then15
  %49 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %ctx, align 4, !tbaa !7
  %50 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %51 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %52 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %53 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %54 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %call22 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %49, i8* %50, i8* %51, i32 %52, i32* null, i32 %53, i32 1, i32 3, i32 0, i32 0, i32 %54)
  store i32 %call22, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

cleanup23:                                        ; preds = %if.else21, %if.then19
  %55 = bitcast i32* %tableType16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #7
  br label %cleanup30

if.else24:                                        ; preds = %if.else13
  %56 = bitcast i32* %tableType25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #7
  %57 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %58 = ptrtoint i8* %57 to i32
  %cmp26 = icmp ugt i32 %58, 65535
  %59 = zext i1 %cmp26 to i64
  %cond27 = select i1 %cmp26, i32 1, i32 2
  store i32 %cond27, i32* %tableType25, align 4, !tbaa !9
  %60 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %ctx, align 4, !tbaa !7
  %61 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %62 = load i32, i32* %tableType25, align 4, !tbaa !9
  call void @LZ4_prepareTable(%struct.LZ4_stream_t_internal* %60, i32 %61, i32 %62)
  %63 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %ctx, align 4, !tbaa !7
  %64 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %65 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %66 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %67 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %68 = load i32, i32* %tableType25, align 4, !tbaa !9
  %69 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %call28 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %63, i8* %64, i8* %65, i32 %66, i32* null, i32 %67, i32 1, i32 %68, i32 0, i32 0, i32 %69)
  store i32 %call28, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %70 = bitcast i32* %tableType25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #7
  br label %cleanup30

cleanup30:                                        ; preds = %if.else24, %cleanup23, %if.else8, %cleanup
  %71 = bitcast %struct.LZ4_stream_t_internal** %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #7
  %72 = load i32, i32* %retval, align 4
  ret i32 %72
}

; Function Attrs: alwaysinline nounwind
define internal void @LZ4_prepareTable(%struct.LZ4_stream_t_internal* %cctx, i32 %inputSize, i32 %tableType) #2 {
entry:
  %cctx.addr = alloca %struct.LZ4_stream_t_internal*, align 4
  %inputSize.addr = alloca i32, align 4
  %tableType.addr = alloca i32, align 4
  store %struct.LZ4_stream_t_internal* %cctx, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  store i32 %inputSize, i32* %inputSize.addr, align 4, !tbaa !3
  store i32 %tableType, i32* %tableType.addr, align 4, !tbaa !9
  %0 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %dirty = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %0, i32 0, i32 2
  %1 = load i16, i16* %dirty, align 4, !tbaa !19
  %tobool = icmp ne i16 %1, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %3 = bitcast %struct.LZ4_stream_t_internal* %2 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 16404, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  %4 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %tableType1 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %4, i32 0, i32 3
  %5 = load i16, i16* %tableType1, align 2, !tbaa !18
  %conv = zext i16 %5 to i32
  %cmp = icmp ne i32 %conv, 0
  br i1 %cmp, label %if.then3, label %if.end29

if.then3:                                         ; preds = %if.end
  %6 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %tableType4 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %6, i32 0, i32 3
  %7 = load i16, i16* %tableType4, align 2, !tbaa !18
  %conv5 = zext i16 %7 to i32
  %8 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %cmp6 = icmp ne i32 %conv5, %8
  br i1 %cmp6, label %if.then25, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then3
  %9 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %cmp8 = icmp eq i32 %9, 3
  br i1 %cmp8, label %land.lhs.true, label %lor.lhs.false12

land.lhs.true:                                    ; preds = %lor.lhs.false
  %10 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %currentOffset = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %10, i32 0, i32 1
  %11 = load i32, i32* %currentOffset, align 4, !tbaa !12
  %12 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %add = add i32 %11, %12
  %cmp10 = icmp uge i32 %add, 65535
  br i1 %cmp10, label %if.then25, label %lor.lhs.false12

lor.lhs.false12:                                  ; preds = %land.lhs.true, %lor.lhs.false
  %13 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %cmp13 = icmp eq i32 %13, 2
  br i1 %cmp13, label %land.lhs.true15, label %lor.lhs.false19

land.lhs.true15:                                  ; preds = %lor.lhs.false12
  %14 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %currentOffset16 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %14, i32 0, i32 1
  %15 = load i32, i32* %currentOffset16, align 4, !tbaa !12
  %cmp17 = icmp ugt i32 %15, 1073741824
  br i1 %cmp17, label %if.then25, label %lor.lhs.false19

lor.lhs.false19:                                  ; preds = %land.lhs.true15, %lor.lhs.false12
  %16 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %cmp20 = icmp eq i32 %16, 1
  br i1 %cmp20, label %if.then25, label %lor.lhs.false22

lor.lhs.false22:                                  ; preds = %lor.lhs.false19
  %17 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %cmp23 = icmp sge i32 %17, 4096
  br i1 %cmp23, label %if.then25, label %if.else

if.then25:                                        ; preds = %lor.lhs.false22, %lor.lhs.false19, %land.lhs.true15, %land.lhs.true, %if.then3
  %18 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %hashTable = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %18, i32 0, i32 0
  %arraydecay = getelementptr inbounds [4096 x i32], [4096 x i32]* %hashTable, i32 0, i32 0
  %19 = bitcast i32* %arraydecay to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %19, i8 0, i32 16384, i1 false)
  %20 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %currentOffset26 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %20, i32 0, i32 1
  store i32 0, i32* %currentOffset26, align 4, !tbaa !12
  %21 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %tableType27 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %21, i32 0, i32 3
  store i16 0, i16* %tableType27, align 2, !tbaa !18
  br label %if.end28

if.else:                                          ; preds = %lor.lhs.false22
  br label %if.end28

if.end28:                                         ; preds = %if.else, %if.then25
  br label %if.end29

if.end29:                                         ; preds = %if.end28, %if.end
  %22 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %currentOffset30 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %22, i32 0, i32 1
  %23 = load i32, i32* %currentOffset30, align 4, !tbaa !12
  %cmp31 = icmp ne i32 %23, 0
  br i1 %cmp31, label %land.lhs.true33, label %if.end39

land.lhs.true33:                                  ; preds = %if.end29
  %24 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %cmp34 = icmp eq i32 %24, 2
  br i1 %cmp34, label %if.then36, label %if.end39

if.then36:                                        ; preds = %land.lhs.true33
  %25 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %currentOffset37 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %25, i32 0, i32 1
  %26 = load i32, i32* %currentOffset37, align 4, !tbaa !12
  %add38 = add i32 %26, 65536
  store i32 %add38, i32* %currentOffset37, align 4, !tbaa !12
  br label %if.end39

if.end39:                                         ; preds = %if.then36, %land.lhs.true33, %if.end29
  %27 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %dictCtx = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %27, i32 0, i32 5
  store %struct.LZ4_stream_t_internal* null, %struct.LZ4_stream_t_internal** %dictCtx, align 4, !tbaa !15
  %28 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %dictionary = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %28, i32 0, i32 4
  store i8* null, i8** %dictionary, align 4, !tbaa !16
  %29 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %cctx.addr, align 4, !tbaa !7
  %dictSize = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %29, i32 0, i32 6
  store i32 0, i32* %dictSize, align 4, !tbaa !17
  br label %return

return:                                           ; preds = %if.end39, %if.then
  ret void
}

; Function Attrs: nounwind
define i32 @LZ4_compress_fast(i8* %source, i8* %dest, i32 %inputSize, i32 %maxOutputSize, i32 %acceleration) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %inputSize.addr = alloca i32, align 4
  %maxOutputSize.addr = alloca i32, align 4
  %acceleration.addr = alloca i32, align 4
  %result = alloca i32, align 4
  %ctx = alloca %union.LZ4_stream_u, align 8
  %ctxPtr = alloca %union.LZ4_stream_u*, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %inputSize, i32* %inputSize.addr, align 4, !tbaa !3
  store i32 %maxOutputSize, i32* %maxOutputSize.addr, align 4, !tbaa !3
  store i32 %acceleration, i32* %acceleration.addr, align 4, !tbaa !3
  %0 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast %union.LZ4_stream_u* %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 16416, i8* %1) #7
  %2 = bitcast %union.LZ4_stream_u** %ctxPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  store %union.LZ4_stream_u* %ctx, %union.LZ4_stream_u** %ctxPtr, align 4, !tbaa !7
  %3 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %ctxPtr, align 4, !tbaa !7
  %4 = bitcast %union.LZ4_stream_u* %3 to i8*
  %5 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %6 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %7 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %8 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %9 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_compress_fast_extState(i8* %4, i8* %5, i8* %6, i32 %7, i32 %8, i32 %9)
  store i32 %call, i32* %result, align 4, !tbaa !3
  %10 = load i32, i32* %result, align 4, !tbaa !3
  %11 = bitcast %union.LZ4_stream_u** %ctxPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #7
  %12 = bitcast %union.LZ4_stream_u* %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 16416, i8* %12) #7
  %13 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  ret i32 %10
}

; Function Attrs: nounwind
define i32 @LZ4_compress_default(i8* %src, i8* %dst, i32 %srcSize, i32 %maxOutputSize) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %maxOutputSize.addr = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !7
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !3
  store i32 %maxOutputSize, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %0 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %2 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %3 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_compress_fast(i8* %0, i8* %1, i32 %2, i32 %3, i32 1)
  ret i32 %call
}

; Function Attrs: nounwind
define hidden i32 @LZ4_compress_fast_force(i8* %src, i8* %dst, i32 %srcSize, i32 %dstCapacity, i32 %acceleration) #0 {
entry:
  %retval = alloca i32, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %dstCapacity.addr = alloca i32, align 4
  %acceleration.addr = alloca i32, align 4
  %ctx = alloca %union.LZ4_stream_u, align 8
  %cleanup.dest.slot = alloca i32, align 4
  %addrMode = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !7
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !3
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !3
  store i32 %acceleration, i32* %acceleration.addr, align 4, !tbaa !3
  %0 = bitcast %union.LZ4_stream_u* %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 16416, i8* %0) #7
  %1 = bitcast %union.LZ4_stream_u* %ctx to i8*
  %call = call %union.LZ4_stream_u* @LZ4_initStream(i8* %1, i32 16416)
  %2 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %cmp = icmp slt i32 %2, 65547
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %internal_donotuse = bitcast %union.LZ4_stream_u* %ctx to %struct.LZ4_stream_t_internal*
  %3 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %4 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %5 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %6 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %7 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %call1 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %internal_donotuse, i8* %3, i8* %4, i32 %5, i32* null, i32 %6, i32 1, i32 3, i32 0, i32 0, i32 %7)
  store i32 %call1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %entry
  %8 = bitcast i32* %addrMode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  store i32 1, i32* %addrMode, align 4, !tbaa !9
  %internal_donotuse2 = bitcast %union.LZ4_stream_u* %ctx to %struct.LZ4_stream_t_internal*
  %9 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %10 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %11 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %12 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %13 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %call3 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %internal_donotuse2, i8* %9, i8* %10, i32 %11, i32* null, i32 %12, i32 1, i32 1, i32 0, i32 0, i32 %13)
  store i32 %call3, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %14 = bitcast i32* %addrMode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #7
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then
  %15 = bitcast %union.LZ4_stream_u* %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 16416, i8* %15) #7
  %16 = load i32, i32* %retval, align 4
  ret i32 %16
}

; Function Attrs: nounwind
define i32 @LZ4_compress_destSize(i8* %src, i8* %dst, i32* %srcSizePtr, i32 %targetDstSize) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSizePtr.addr = alloca i32*, align 4
  %targetDstSize.addr = alloca i32, align 4
  %ctxBody = alloca %union.LZ4_stream_u, align 8
  %ctx = alloca %union.LZ4_stream_u*, align 4
  %result = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !7
  store i32* %srcSizePtr, i32** %srcSizePtr.addr, align 4, !tbaa !7
  store i32 %targetDstSize, i32* %targetDstSize.addr, align 4, !tbaa !3
  %0 = bitcast %union.LZ4_stream_u* %ctxBody to i8*
  call void @llvm.lifetime.start.p0i8(i64 16416, i8* %0) #7
  %1 = bitcast %union.LZ4_stream_u** %ctx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  store %union.LZ4_stream_u* %ctxBody, %union.LZ4_stream_u** %ctx, align 4, !tbaa !7
  %2 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %ctx, align 4, !tbaa !7
  %4 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %5 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %6 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  %7 = load i32, i32* %targetDstSize.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_compress_destSize_extState(%union.LZ4_stream_u* %3, i8* %4, i8* %5, i32* %6, i32 %7)
  store i32 %call, i32* %result, align 4, !tbaa !3
  %8 = load i32, i32* %result, align 4, !tbaa !3
  %9 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #7
  %10 = bitcast %union.LZ4_stream_u** %ctx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #7
  %11 = bitcast %union.LZ4_stream_u* %ctxBody to i8*
  call void @llvm.lifetime.end.p0i8(i64 16416, i8* %11) #7
  ret i32 %8
}

; Function Attrs: nounwind
define internal i32 @LZ4_compress_destSize_extState(%union.LZ4_stream_u* %state, i8* %src, i8* %dst, i32* %srcSizePtr, i32 %targetDstSize) #0 {
entry:
  %retval = alloca i32, align 4
  %state.addr = alloca %union.LZ4_stream_u*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSizePtr.addr = alloca i32*, align 4
  %targetDstSize.addr = alloca i32, align 4
  %s = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %addrMode = alloca i32, align 4
  store %union.LZ4_stream_u* %state, %union.LZ4_stream_u** %state.addr, align 4, !tbaa !7
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !7
  store i32* %srcSizePtr, i32** %srcSizePtr.addr, align 4, !tbaa !7
  store i32 %targetDstSize, i32* %targetDstSize.addr, align 4, !tbaa !3
  %0 = bitcast i8** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %state.addr, align 4, !tbaa !7
  %2 = bitcast %union.LZ4_stream_u* %1 to i8*
  %call = call %union.LZ4_stream_u* @LZ4_initStream(i8* %2, i32 16416)
  %3 = bitcast %union.LZ4_stream_u* %call to i8*
  store i8* %3, i8** %s, align 4, !tbaa !7
  %4 = load i8*, i8** %s, align 4, !tbaa !7
  %5 = load i32, i32* %targetDstSize.addr, align 4, !tbaa !3
  %6 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  %7 = load i32, i32* %6, align 4, !tbaa !3
  %call1 = call i32 @LZ4_compressBound(i32 %7)
  %cmp = icmp sge i32 %5, %call1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %8 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %state.addr, align 4, !tbaa !7
  %9 = bitcast %union.LZ4_stream_u* %8 to i8*
  %10 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %11 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %12 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  %13 = load i32, i32* %12, align 4, !tbaa !3
  %14 = load i32, i32* %targetDstSize.addr, align 4, !tbaa !3
  %call2 = call i32 @LZ4_compress_fast_extState(i8* %9, i8* %10, i8* %11, i32 %13, i32 %14, i32 1)
  store i32 %call2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %entry
  %15 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  %16 = load i32, i32* %15, align 4, !tbaa !3
  %cmp3 = icmp slt i32 %16, 65547
  br i1 %cmp3, label %if.then4, label %if.else6

if.then4:                                         ; preds = %if.else
  %17 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %state.addr, align 4, !tbaa !7
  %internal_donotuse = bitcast %union.LZ4_stream_u* %17 to %struct.LZ4_stream_t_internal*
  %18 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %19 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %20 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  %21 = load i32, i32* %20, align 4, !tbaa !3
  %22 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  %23 = load i32, i32* %targetDstSize.addr, align 4, !tbaa !3
  %call5 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %internal_donotuse, i8* %18, i8* %19, i32 %21, i32* %22, i32 %23, i32 2, i32 3, i32 0, i32 0, i32 1)
  store i32 %call5, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else6:                                         ; preds = %if.else
  %24 = bitcast i32* %addrMode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #7
  %25 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %26 = ptrtoint i8* %25 to i32
  %cmp7 = icmp ugt i32 %26, 65535
  %27 = zext i1 %cmp7 to i64
  %cond = select i1 %cmp7, i32 1, i32 2
  store i32 %cond, i32* %addrMode, align 4, !tbaa !9
  %28 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %state.addr, align 4, !tbaa !7
  %internal_donotuse8 = bitcast %union.LZ4_stream_u* %28 to %struct.LZ4_stream_t_internal*
  %29 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %30 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %31 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  %32 = load i32, i32* %31, align 4, !tbaa !3
  %33 = load i32*, i32** %srcSizePtr.addr, align 4, !tbaa !7
  %34 = load i32, i32* %targetDstSize.addr, align 4, !tbaa !3
  %35 = load i32, i32* %addrMode, align 4, !tbaa !9
  %call9 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %internal_donotuse8, i8* %29, i8* %30, i32 %32, i32* %33, i32 %34, i32 2, i32 %35, i32 0, i32 0, i32 1)
  store i32 %call9, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %36 = bitcast i32* %addrMode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #7
  br label %cleanup

cleanup:                                          ; preds = %if.else6, %if.then4, %if.then
  %37 = bitcast i8** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #7
  %38 = load i32, i32* %retval, align 4
  ret i32 %38
}

; Function Attrs: nounwind
define %union.LZ4_stream_u* @LZ4_createStream() #0 {
entry:
  %retval = alloca %union.LZ4_stream_u*, align 4
  %lz4s = alloca %union.LZ4_stream_u*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %0 = bitcast %union.LZ4_stream_u** %lz4s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i8* @malloc(i32 16416)
  %1 = bitcast i8* %call to %union.LZ4_stream_u*
  store %union.LZ4_stream_u* %1, %union.LZ4_stream_u** %lz4s, align 4, !tbaa !7
  %2 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %lz4s, align 4, !tbaa !7
  %cmp = icmp eq %union.LZ4_stream_u* %2, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %union.LZ4_stream_u* null, %union.LZ4_stream_u** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %3 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %lz4s, align 4, !tbaa !7
  %4 = bitcast %union.LZ4_stream_u* %3 to i8*
  %call1 = call %union.LZ4_stream_u* @LZ4_initStream(i8* %4, i32 16416)
  %5 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %lz4s, align 4, !tbaa !7
  store %union.LZ4_stream_u* %5, %union.LZ4_stream_u** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %6 = bitcast %union.LZ4_stream_u** %lz4s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #7
  %7 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %retval, align 4
  ret %union.LZ4_stream_u* %7
}

declare i8* @malloc(i32) #3

; Function Attrs: nounwind
define internal i32 @LZ4_stream_t_alignment() #0 {
entry:
  ret i32 8
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: nounwind
define void @LZ4_resetStream(%union.LZ4_stream_u* %LZ4_stream) #0 {
entry:
  %LZ4_stream.addr = alloca %union.LZ4_stream_u*, align 4
  store %union.LZ4_stream_u* %LZ4_stream, %union.LZ4_stream_u** %LZ4_stream.addr, align 4, !tbaa !7
  %0 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %LZ4_stream.addr, align 4, !tbaa !7
  %1 = bitcast %union.LZ4_stream_u* %0 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %1, i8 0, i32 16416, i1 false)
  ret void
}

; Function Attrs: nounwind
define void @LZ4_resetStream_fast(%union.LZ4_stream_u* %ctx) #0 {
entry:
  %ctx.addr = alloca %union.LZ4_stream_u*, align 4
  store %union.LZ4_stream_u* %ctx, %union.LZ4_stream_u** %ctx.addr, align 4, !tbaa !7
  %0 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %ctx.addr, align 4, !tbaa !7
  %internal_donotuse = bitcast %union.LZ4_stream_u* %0 to %struct.LZ4_stream_t_internal*
  call void @LZ4_prepareTable(%struct.LZ4_stream_t_internal* %internal_donotuse, i32 0, i32 2)
  ret void
}

; Function Attrs: nounwind
define i32 @LZ4_freeStream(%union.LZ4_stream_u* %LZ4_stream) #0 {
entry:
  %retval = alloca i32, align 4
  %LZ4_stream.addr = alloca %union.LZ4_stream_u*, align 4
  store %union.LZ4_stream_u* %LZ4_stream, %union.LZ4_stream_u** %LZ4_stream.addr, align 4, !tbaa !7
  %0 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %LZ4_stream.addr, align 4, !tbaa !7
  %tobool = icmp ne %union.LZ4_stream_u* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %LZ4_stream.addr, align 4, !tbaa !7
  %2 = bitcast %union.LZ4_stream_u* %1 to i8*
  call void @free(i8* %2)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

declare void @free(i8*) #3

; Function Attrs: nounwind
define i32 @LZ4_loadDict(%union.LZ4_stream_u* %LZ4_dict, i8* %dictionary, i32 %dictSize) #0 {
entry:
  %retval = alloca i32, align 4
  %LZ4_dict.addr = alloca %union.LZ4_stream_u*, align 4
  %dictionary.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  %dict = alloca %struct.LZ4_stream_t_internal*, align 4
  %tableType = alloca i32, align 4
  %p = alloca i8*, align 4
  %dictEnd = alloca i8*, align 4
  %base = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %union.LZ4_stream_u* %LZ4_dict, %union.LZ4_stream_u** %LZ4_dict.addr, align 4, !tbaa !7
  store i8* %dictionary, i8** %dictionary.addr, align 4, !tbaa !7
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !3
  %0 = bitcast %struct.LZ4_stream_t_internal** %dict to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %LZ4_dict.addr, align 4, !tbaa !7
  %internal_donotuse = bitcast %union.LZ4_stream_u* %1 to %struct.LZ4_stream_t_internal*
  store %struct.LZ4_stream_t_internal* %internal_donotuse, %struct.LZ4_stream_t_internal** %dict, align 4, !tbaa !7
  %2 = bitcast i32* %tableType to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  store i32 2, i32* %tableType, align 4, !tbaa !9
  %3 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i8*, i8** %dictionary.addr, align 4, !tbaa !7
  store i8* %4, i8** %p, align 4, !tbaa !7
  %5 = bitcast i8** %dictEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #7
  %6 = load i8*, i8** %p, align 4, !tbaa !7
  %7 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 %7
  store i8* %add.ptr, i8** %dictEnd, align 4, !tbaa !7
  %8 = bitcast i8** %base to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %LZ4_dict.addr, align 4, !tbaa !7
  call void @LZ4_resetStream(%union.LZ4_stream_u* %9)
  %10 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dict, align 4, !tbaa !7
  %currentOffset = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %10, i32 0, i32 1
  %11 = load i32, i32* %currentOffset, align 4, !tbaa !12
  %add = add i32 %11, 65536
  store i32 %add, i32* %currentOffset, align 4, !tbaa !12
  %12 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %cmp = icmp slt i32 %12, 4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %13 = load i8*, i8** %dictEnd, align 4, !tbaa !7
  %14 = load i8*, i8** %p, align 4, !tbaa !7
  %sub.ptr.lhs.cast = ptrtoint i8* %13 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %14 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %cmp1 = icmp sgt i32 %sub.ptr.sub, 65536
  br i1 %cmp1, label %if.then2, label %if.end4

if.then2:                                         ; preds = %if.end
  %15 = load i8*, i8** %dictEnd, align 4, !tbaa !7
  %add.ptr3 = getelementptr inbounds i8, i8* %15, i32 -65536
  store i8* %add.ptr3, i8** %p, align 4, !tbaa !7
  br label %if.end4

if.end4:                                          ; preds = %if.then2, %if.end
  %16 = load i8*, i8** %dictEnd, align 4, !tbaa !7
  %17 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dict, align 4, !tbaa !7
  %currentOffset5 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %17, i32 0, i32 1
  %18 = load i32, i32* %currentOffset5, align 4, !tbaa !12
  %idx.neg = sub i32 0, %18
  %add.ptr6 = getelementptr inbounds i8, i8* %16, i32 %idx.neg
  store i8* %add.ptr6, i8** %base, align 4, !tbaa !7
  %19 = load i8*, i8** %p, align 4, !tbaa !7
  %20 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dict, align 4, !tbaa !7
  %dictionary7 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %20, i32 0, i32 4
  store i8* %19, i8** %dictionary7, align 4, !tbaa !16
  %21 = load i8*, i8** %dictEnd, align 4, !tbaa !7
  %22 = load i8*, i8** %p, align 4, !tbaa !7
  %sub.ptr.lhs.cast8 = ptrtoint i8* %21 to i32
  %sub.ptr.rhs.cast9 = ptrtoint i8* %22 to i32
  %sub.ptr.sub10 = sub i32 %sub.ptr.lhs.cast8, %sub.ptr.rhs.cast9
  %23 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dict, align 4, !tbaa !7
  %dictSize11 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %23, i32 0, i32 6
  store i32 %sub.ptr.sub10, i32* %dictSize11, align 4, !tbaa !17
  %24 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dict, align 4, !tbaa !7
  %tableType12 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %24, i32 0, i32 3
  store i16 2, i16* %tableType12, align 2, !tbaa !18
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end4
  %25 = load i8*, i8** %p, align 4, !tbaa !7
  %26 = load i8*, i8** %dictEnd, align 4, !tbaa !7
  %add.ptr13 = getelementptr inbounds i8, i8* %26, i32 -4
  %cmp14 = icmp ule i8* %25, %add.ptr13
  br i1 %cmp14, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %27 = load i8*, i8** %p, align 4, !tbaa !7
  %28 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dict, align 4, !tbaa !7
  %hashTable = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %28, i32 0, i32 0
  %arraydecay = getelementptr inbounds [4096 x i32], [4096 x i32]* %hashTable, i32 0, i32 0
  %29 = bitcast i32* %arraydecay to i8*
  %30 = load i8*, i8** %base, align 4, !tbaa !7
  call void @LZ4_putPosition(i8* %27, i8* %29, i32 2, i8* %30)
  %31 = load i8*, i8** %p, align 4, !tbaa !7
  %add.ptr15 = getelementptr inbounds i8, i8* %31, i32 3
  store i8* %add.ptr15, i8** %p, align 4, !tbaa !7
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %32 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dict, align 4, !tbaa !7
  %dictSize16 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %32, i32 0, i32 6
  %33 = load i32, i32* %dictSize16, align 4, !tbaa !17
  store i32 %33, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %while.end, %if.then
  %34 = bitcast i8** %base to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #7
  %35 = bitcast i8** %dictEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #7
  %36 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #7
  %37 = bitcast i32* %tableType to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #7
  %38 = bitcast %struct.LZ4_stream_t_internal** %dict to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #7
  %39 = load i32, i32* %retval, align 4
  ret i32 %39
}

; Function Attrs: alwaysinline nounwind
define internal void @LZ4_putPosition(i8* %p, i8* %tableBase, i32 %tableType, i8* %srcBase) #2 {
entry:
  %p.addr = alloca i8*, align 4
  %tableBase.addr = alloca i8*, align 4
  %tableType.addr = alloca i32, align 4
  %srcBase.addr = alloca i8*, align 4
  %h = alloca i32, align 4
  store i8* %p, i8** %p.addr, align 4, !tbaa !7
  store i8* %tableBase, i8** %tableBase.addr, align 4, !tbaa !7
  store i32 %tableType, i32* %tableType.addr, align 4, !tbaa !9
  store i8* %srcBase, i8** %srcBase.addr, align 4, !tbaa !7
  %0 = bitcast i32* %h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8*, i8** %p.addr, align 4, !tbaa !7
  %2 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %call = call i32 @LZ4_hashPosition(i8* %1, i32 %2)
  store i32 %call, i32* %h, align 4, !tbaa !3
  %3 = load i8*, i8** %p.addr, align 4, !tbaa !7
  %4 = load i32, i32* %h, align 4, !tbaa !3
  %5 = load i8*, i8** %tableBase.addr, align 4, !tbaa !7
  %6 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %7 = load i8*, i8** %srcBase.addr, align 4, !tbaa !7
  call void @LZ4_putPositionOnHash(i8* %3, i32 %4, i8* %5, i32 %6, i8* %7)
  %8 = bitcast i32* %h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #7
  ret void
}

; Function Attrs: nounwind
define hidden void @LZ4_attach_dictionary(%union.LZ4_stream_u* %workingStream, %union.LZ4_stream_u* %dictionaryStream) #0 {
entry:
  %workingStream.addr = alloca %union.LZ4_stream_u*, align 4
  %dictionaryStream.addr = alloca %union.LZ4_stream_u*, align 4
  %dictCtx = alloca %struct.LZ4_stream_t_internal*, align 4
  store %union.LZ4_stream_u* %workingStream, %union.LZ4_stream_u** %workingStream.addr, align 4, !tbaa !7
  store %union.LZ4_stream_u* %dictionaryStream, %union.LZ4_stream_u** %dictionaryStream.addr, align 4, !tbaa !7
  %0 = bitcast %struct.LZ4_stream_t_internal** %dictCtx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %dictionaryStream.addr, align 4, !tbaa !7
  %cmp = icmp eq %union.LZ4_stream_u* %1, null
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %2 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %dictionaryStream.addr, align 4, !tbaa !7
  %internal_donotuse = bitcast %union.LZ4_stream_u* %2 to %struct.LZ4_stream_t_internal*
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.LZ4_stream_t_internal* [ null, %cond.true ], [ %internal_donotuse, %cond.false ]
  store %struct.LZ4_stream_t_internal* %cond, %struct.LZ4_stream_t_internal** %dictCtx, align 4, !tbaa !7
  %3 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %workingStream.addr, align 4, !tbaa !7
  call void @LZ4_resetStream_fast(%union.LZ4_stream_u* %3)
  %4 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dictCtx, align 4, !tbaa !7
  %cmp1 = icmp ne %struct.LZ4_stream_t_internal* %4, null
  br i1 %cmp1, label %if.then, label %if.end10

if.then:                                          ; preds = %cond.end
  %5 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %workingStream.addr, align 4, !tbaa !7
  %internal_donotuse2 = bitcast %union.LZ4_stream_u* %5 to %struct.LZ4_stream_t_internal*
  %currentOffset = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %internal_donotuse2, i32 0, i32 1
  %6 = load i32, i32* %currentOffset, align 8, !tbaa !9
  %cmp3 = icmp eq i32 %6, 0
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.then
  %7 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %workingStream.addr, align 4, !tbaa !7
  %internal_donotuse5 = bitcast %union.LZ4_stream_u* %7 to %struct.LZ4_stream_t_internal*
  %currentOffset6 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %internal_donotuse5, i32 0, i32 1
  store i32 65536, i32* %currentOffset6, align 8, !tbaa !9
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.then
  %8 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dictCtx, align 4, !tbaa !7
  %dictSize = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %8, i32 0, i32 6
  %9 = load i32, i32* %dictSize, align 4, !tbaa !17
  %cmp7 = icmp eq i32 %9, 0
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end
  store %struct.LZ4_stream_t_internal* null, %struct.LZ4_stream_t_internal** %dictCtx, align 4, !tbaa !7
  br label %if.end9

if.end9:                                          ; preds = %if.then8, %if.end
  br label %if.end10

if.end10:                                         ; preds = %if.end9, %cond.end
  %10 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dictCtx, align 4, !tbaa !7
  %11 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %workingStream.addr, align 4, !tbaa !7
  %internal_donotuse11 = bitcast %union.LZ4_stream_u* %11 to %struct.LZ4_stream_t_internal*
  %dictCtx12 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %internal_donotuse11, i32 0, i32 5
  store %struct.LZ4_stream_t_internal* %10, %struct.LZ4_stream_t_internal** %dictCtx12, align 4, !tbaa !9
  %12 = bitcast %struct.LZ4_stream_t_internal** %dictCtx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  ret void
}

; Function Attrs: nounwind
define i32 @LZ4_compress_fast_continue(%union.LZ4_stream_u* %LZ4_stream, i8* %source, i8* %dest, i32 %inputSize, i32 %maxOutputSize, i32 %acceleration) #0 {
entry:
  %retval = alloca i32, align 4
  %LZ4_stream.addr = alloca %union.LZ4_stream_u*, align 4
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %inputSize.addr = alloca i32, align 4
  %maxOutputSize.addr = alloca i32, align 4
  %acceleration.addr = alloca i32, align 4
  %tableType = alloca i32, align 4
  %streamPtr = alloca %struct.LZ4_stream_t_internal*, align 4
  %dictEnd = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %sourceEnd = alloca i8*, align 4
  %result = alloca i32, align 4
  store %union.LZ4_stream_u* %LZ4_stream, %union.LZ4_stream_u** %LZ4_stream.addr, align 4, !tbaa !7
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %inputSize, i32* %inputSize.addr, align 4, !tbaa !3
  store i32 %maxOutputSize, i32* %maxOutputSize.addr, align 4, !tbaa !3
  store i32 %acceleration, i32* %acceleration.addr, align 4, !tbaa !3
  %0 = bitcast i32* %tableType to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 2, i32* %tableType, align 4, !tbaa !9
  %1 = bitcast %struct.LZ4_stream_t_internal** %streamPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %LZ4_stream.addr, align 4, !tbaa !7
  %internal_donotuse = bitcast %union.LZ4_stream_u* %2 to %struct.LZ4_stream_t_internal*
  store %struct.LZ4_stream_t_internal* %internal_donotuse, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %3 = bitcast i8** %dictEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictionary = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %4, i32 0, i32 4
  %5 = load i8*, i8** %dictionary, align 4, !tbaa !16
  %6 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictSize = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %6, i32 0, i32 6
  %7 = load i32, i32* %dictSize, align 4, !tbaa !17
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %7
  store i8* %add.ptr, i8** %dictEnd, align 4, !tbaa !7
  %8 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dirty = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %8, i32 0, i32 2
  %9 = load i16, i16* %dirty, align 4, !tbaa !19
  %tobool = icmp ne i16 %9, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %10 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %11 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  call void @LZ4_renormDictT(%struct.LZ4_stream_t_internal* %10, i32 %11)
  %12 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %cmp = icmp slt i32 %12, 1
  br i1 %cmp, label %if.then1, label %if.end2

if.then1:                                         ; preds = %if.end
  store i32 1, i32* %acceleration.addr, align 4, !tbaa !3
  br label %if.end2

if.end2:                                          ; preds = %if.then1, %if.end
  %13 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictSize3 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %13, i32 0, i32 6
  %14 = load i32, i32* %dictSize3, align 4, !tbaa !17
  %sub = sub i32 %14, 1
  %cmp4 = icmp ult i32 %sub, 3
  br i1 %cmp4, label %land.lhs.true, label %if.end9

land.lhs.true:                                    ; preds = %if.end2
  %15 = load i8*, i8** %dictEnd, align 4, !tbaa !7
  %16 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %cmp5 = icmp ne i8* %15, %16
  br i1 %cmp5, label %if.then6, label %if.end9

if.then6:                                         ; preds = %land.lhs.true
  %17 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictSize7 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %17, i32 0, i32 6
  store i32 0, i32* %dictSize7, align 4, !tbaa !17
  %18 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %19 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictionary8 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %19, i32 0, i32 4
  store i8* %18, i8** %dictionary8, align 4, !tbaa !16
  %20 = load i8*, i8** %source.addr, align 4, !tbaa !7
  store i8* %20, i8** %dictEnd, align 4, !tbaa !7
  br label %if.end9

if.end9:                                          ; preds = %if.then6, %land.lhs.true, %if.end2
  %21 = bitcast i8** %sourceEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #7
  %22 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %23 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %add.ptr10 = getelementptr inbounds i8, i8* %22, i32 %23
  store i8* %add.ptr10, i8** %sourceEnd, align 4, !tbaa !7
  %24 = load i8*, i8** %sourceEnd, align 4, !tbaa !7
  %25 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictionary11 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %25, i32 0, i32 4
  %26 = load i8*, i8** %dictionary11, align 4, !tbaa !16
  %cmp12 = icmp ugt i8* %24, %26
  br i1 %cmp12, label %land.lhs.true13, label %if.end30

land.lhs.true13:                                  ; preds = %if.end9
  %27 = load i8*, i8** %sourceEnd, align 4, !tbaa !7
  %28 = load i8*, i8** %dictEnd, align 4, !tbaa !7
  %cmp14 = icmp ult i8* %27, %28
  br i1 %cmp14, label %if.then15, label %if.end30

if.then15:                                        ; preds = %land.lhs.true13
  %29 = load i8*, i8** %dictEnd, align 4, !tbaa !7
  %30 = load i8*, i8** %sourceEnd, align 4, !tbaa !7
  %sub.ptr.lhs.cast = ptrtoint i8* %29 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %30 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %31 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictSize16 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %31, i32 0, i32 6
  store i32 %sub.ptr.sub, i32* %dictSize16, align 4, !tbaa !17
  %32 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictSize17 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %32, i32 0, i32 6
  %33 = load i32, i32* %dictSize17, align 4, !tbaa !17
  %cmp18 = icmp ugt i32 %33, 65536
  br i1 %cmp18, label %if.then19, label %if.end21

if.then19:                                        ; preds = %if.then15
  %34 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictSize20 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %34, i32 0, i32 6
  store i32 65536, i32* %dictSize20, align 4, !tbaa !17
  br label %if.end21

if.end21:                                         ; preds = %if.then19, %if.then15
  %35 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictSize22 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %35, i32 0, i32 6
  %36 = load i32, i32* %dictSize22, align 4, !tbaa !17
  %cmp23 = icmp ult i32 %36, 4
  br i1 %cmp23, label %if.then24, label %if.end26

if.then24:                                        ; preds = %if.end21
  %37 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictSize25 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %37, i32 0, i32 6
  store i32 0, i32* %dictSize25, align 4, !tbaa !17
  br label %if.end26

if.end26:                                         ; preds = %if.then24, %if.end21
  %38 = load i8*, i8** %dictEnd, align 4, !tbaa !7
  %39 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictSize27 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %39, i32 0, i32 6
  %40 = load i32, i32* %dictSize27, align 4, !tbaa !17
  %idx.neg = sub i32 0, %40
  %add.ptr28 = getelementptr inbounds i8, i8* %38, i32 %idx.neg
  %41 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictionary29 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %41, i32 0, i32 4
  store i8* %add.ptr28, i8** %dictionary29, align 4, !tbaa !16
  br label %if.end30

if.end30:                                         ; preds = %if.end26, %land.lhs.true13, %if.end9
  %42 = bitcast i8** %sourceEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #7
  %43 = load i8*, i8** %dictEnd, align 4, !tbaa !7
  %44 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %cmp31 = icmp eq i8* %43, %44
  br i1 %cmp31, label %if.then32, label %if.end40

if.then32:                                        ; preds = %if.end30
  %45 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictSize33 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %45, i32 0, i32 6
  %46 = load i32, i32* %dictSize33, align 4, !tbaa !17
  %cmp34 = icmp ult i32 %46, 65536
  br i1 %cmp34, label %land.lhs.true35, label %if.else

land.lhs.true35:                                  ; preds = %if.then32
  %47 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictSize36 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %47, i32 0, i32 6
  %48 = load i32, i32* %dictSize36, align 4, !tbaa !17
  %49 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %currentOffset = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %49, i32 0, i32 1
  %50 = load i32, i32* %currentOffset, align 4, !tbaa !12
  %cmp37 = icmp ult i32 %48, %50
  br i1 %cmp37, label %if.then38, label %if.else

if.then38:                                        ; preds = %land.lhs.true35
  %51 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %52 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %53 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %54 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %55 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %56 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %51, i8* %52, i8* %53, i32 %54, i32* null, i32 %55, i32 1, i32 2, i32 1, i32 1, i32 %56)
  store i32 %call, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %land.lhs.true35, %if.then32
  %57 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %58 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %59 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %60 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %61 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %62 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %call39 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %57, i8* %58, i8* %59, i32 %60, i32* null, i32 %61, i32 1, i32 2, i32 1, i32 0, i32 %62)
  store i32 %call39, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end40:                                         ; preds = %if.end30
  %63 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #7
  %64 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictCtx = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %64, i32 0, i32 5
  %65 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dictCtx, align 4, !tbaa !15
  %tobool41 = icmp ne %struct.LZ4_stream_t_internal* %65, null
  br i1 %tobool41, label %if.then42, label %if.else50

if.then42:                                        ; preds = %if.end40
  %66 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %cmp43 = icmp sgt i32 %66, 4096
  br i1 %cmp43, label %if.then44, label %if.else47

if.then44:                                        ; preds = %if.then42
  %67 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %68 = bitcast %struct.LZ4_stream_t_internal* %67 to i8*
  %69 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictCtx45 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %69, i32 0, i32 5
  %70 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dictCtx45, align 4, !tbaa !15
  %71 = bitcast %struct.LZ4_stream_t_internal* %70 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %68, i8* align 4 %71, i32 16416, i1 false)
  %72 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %73 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %74 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %75 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %76 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %77 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %call46 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %72, i8* %73, i8* %74, i32 %75, i32* null, i32 %76, i32 1, i32 2, i32 2, i32 0, i32 %77)
  store i32 %call46, i32* %result, align 4, !tbaa !3
  br label %if.end49

if.else47:                                        ; preds = %if.then42
  %78 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %79 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %80 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %81 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %82 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %83 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %call48 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %78, i8* %79, i8* %80, i32 %81, i32* null, i32 %82, i32 1, i32 2, i32 3, i32 0, i32 %83)
  store i32 %call48, i32* %result, align 4, !tbaa !3
  br label %if.end49

if.end49:                                         ; preds = %if.else47, %if.then44
  br label %if.end62

if.else50:                                        ; preds = %if.end40
  %84 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictSize51 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %84, i32 0, i32 6
  %85 = load i32, i32* %dictSize51, align 4, !tbaa !17
  %cmp52 = icmp ult i32 %85, 65536
  br i1 %cmp52, label %land.lhs.true53, label %if.else59

land.lhs.true53:                                  ; preds = %if.else50
  %86 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictSize54 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %86, i32 0, i32 6
  %87 = load i32, i32* %dictSize54, align 4, !tbaa !17
  %88 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %currentOffset55 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %88, i32 0, i32 1
  %89 = load i32, i32* %currentOffset55, align 4, !tbaa !12
  %cmp56 = icmp ult i32 %87, %89
  br i1 %cmp56, label %if.then57, label %if.else59

if.then57:                                        ; preds = %land.lhs.true53
  %90 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %91 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %92 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %93 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %94 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %95 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %call58 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %90, i8* %91, i8* %92, i32 %93, i32* null, i32 %94, i32 1, i32 2, i32 2, i32 1, i32 %95)
  store i32 %call58, i32* %result, align 4, !tbaa !3
  br label %if.end61

if.else59:                                        ; preds = %land.lhs.true53, %if.else50
  %96 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %97 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %98 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %99 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %100 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %101 = load i32, i32* %acceleration.addr, align 4, !tbaa !3
  %call60 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %96, i8* %97, i8* %98, i32 %99, i32* null, i32 %100, i32 1, i32 2, i32 2, i32 0, i32 %101)
  store i32 %call60, i32* %result, align 4, !tbaa !3
  br label %if.end61

if.end61:                                         ; preds = %if.else59, %if.then57
  br label %if.end62

if.end62:                                         ; preds = %if.end61, %if.end49
  %102 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %103 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictionary63 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %103, i32 0, i32 4
  store i8* %102, i8** %dictionary63, align 4, !tbaa !16
  %104 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %105 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictSize64 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %105, i32 0, i32 6
  store i32 %104, i32* %dictSize64, align 4, !tbaa !17
  %106 = load i32, i32* %result, align 4, !tbaa !3
  store i32 %106, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %107 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #7
  br label %cleanup

cleanup:                                          ; preds = %if.end62, %if.else, %if.then38, %if.then
  %108 = bitcast i8** %dictEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #7
  %109 = bitcast %struct.LZ4_stream_t_internal** %streamPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #7
  %110 = bitcast i32* %tableType to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #7
  %111 = load i32, i32* %retval, align 4
  ret i32 %111
}

; Function Attrs: nounwind
define internal void @LZ4_renormDictT(%struct.LZ4_stream_t_internal* %LZ4_dict, i32 %nextSize) #0 {
entry:
  %LZ4_dict.addr = alloca %struct.LZ4_stream_t_internal*, align 4
  %nextSize.addr = alloca i32, align 4
  %delta = alloca i32, align 4
  %dictEnd = alloca i8*, align 4
  %i = alloca i32, align 4
  store %struct.LZ4_stream_t_internal* %LZ4_dict, %struct.LZ4_stream_t_internal** %LZ4_dict.addr, align 4, !tbaa !7
  store i32 %nextSize, i32* %nextSize.addr, align 4, !tbaa !3
  %0 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %LZ4_dict.addr, align 4, !tbaa !7
  %currentOffset = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %0, i32 0, i32 1
  %1 = load i32, i32* %currentOffset, align 4, !tbaa !12
  %2 = load i32, i32* %nextSize.addr, align 4, !tbaa !3
  %add = add i32 %1, %2
  %cmp = icmp ugt i32 %add, -2147483648
  br i1 %cmp, label %if.then, label %if.end19

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %LZ4_dict.addr, align 4, !tbaa !7
  %currentOffset1 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %4, i32 0, i32 1
  %5 = load i32, i32* %currentOffset1, align 4, !tbaa !12
  %sub = sub i32 %5, 65536
  store i32 %sub, i32* %delta, align 4, !tbaa !3
  %6 = bitcast i8** %dictEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %LZ4_dict.addr, align 4, !tbaa !7
  %dictionary = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %7, i32 0, i32 4
  %8 = load i8*, i8** %dictionary, align 4, !tbaa !16
  %9 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %LZ4_dict.addr, align 4, !tbaa !7
  %dictSize = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %9, i32 0, i32 6
  %10 = load i32, i32* %dictSize, align 4, !tbaa !17
  %add.ptr = getelementptr inbounds i8, i8* %8, i32 %10
  store i8* %add.ptr, i8** %dictEnd, align 4, !tbaa !7
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  store i32 0, i32* %i, align 4, !tbaa !3
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %12 = load i32, i32* %i, align 4, !tbaa !3
  %cmp2 = icmp slt i32 %12, 4096
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %LZ4_dict.addr, align 4, !tbaa !7
  %hashTable = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %13, i32 0, i32 0
  %14 = load i32, i32* %i, align 4, !tbaa !3
  %arrayidx = getelementptr inbounds [4096 x i32], [4096 x i32]* %hashTable, i32 0, i32 %14
  %15 = load i32, i32* %arrayidx, align 4, !tbaa !3
  %16 = load i32, i32* %delta, align 4, !tbaa !3
  %cmp3 = icmp ult i32 %15, %16
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %for.body
  %17 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %LZ4_dict.addr, align 4, !tbaa !7
  %hashTable5 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %17, i32 0, i32 0
  %18 = load i32, i32* %i, align 4, !tbaa !3
  %arrayidx6 = getelementptr inbounds [4096 x i32], [4096 x i32]* %hashTable5, i32 0, i32 %18
  store i32 0, i32* %arrayidx6, align 4, !tbaa !3
  br label %if.end

if.else:                                          ; preds = %for.body
  %19 = load i32, i32* %delta, align 4, !tbaa !3
  %20 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %LZ4_dict.addr, align 4, !tbaa !7
  %hashTable7 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %20, i32 0, i32 0
  %21 = load i32, i32* %i, align 4, !tbaa !3
  %arrayidx8 = getelementptr inbounds [4096 x i32], [4096 x i32]* %hashTable7, i32 0, i32 %21
  %22 = load i32, i32* %arrayidx8, align 4, !tbaa !3
  %sub9 = sub i32 %22, %19
  store i32 %sub9, i32* %arrayidx8, align 4, !tbaa !3
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then4
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %23 = load i32, i32* %i, align 4, !tbaa !3
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %i, align 4, !tbaa !3
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %24 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %LZ4_dict.addr, align 4, !tbaa !7
  %currentOffset10 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %24, i32 0, i32 1
  store i32 65536, i32* %currentOffset10, align 4, !tbaa !12
  %25 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %LZ4_dict.addr, align 4, !tbaa !7
  %dictSize11 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %25, i32 0, i32 6
  %26 = load i32, i32* %dictSize11, align 4, !tbaa !17
  %cmp12 = icmp ugt i32 %26, 65536
  br i1 %cmp12, label %if.then13, label %if.end15

if.then13:                                        ; preds = %for.end
  %27 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %LZ4_dict.addr, align 4, !tbaa !7
  %dictSize14 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %27, i32 0, i32 6
  store i32 65536, i32* %dictSize14, align 4, !tbaa !17
  br label %if.end15

if.end15:                                         ; preds = %if.then13, %for.end
  %28 = load i8*, i8** %dictEnd, align 4, !tbaa !7
  %29 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %LZ4_dict.addr, align 4, !tbaa !7
  %dictSize16 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %29, i32 0, i32 6
  %30 = load i32, i32* %dictSize16, align 4, !tbaa !17
  %idx.neg = sub i32 0, %30
  %add.ptr17 = getelementptr inbounds i8, i8* %28, i32 %idx.neg
  %31 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %LZ4_dict.addr, align 4, !tbaa !7
  %dictionary18 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %31, i32 0, i32 4
  store i8* %add.ptr17, i8** %dictionary18, align 4, !tbaa !16
  %32 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #7
  %33 = bitcast i8** %dictEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #7
  %34 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #7
  br label %if.end19

if.end19:                                         ; preds = %if.end15, %entry
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define hidden i32 @LZ4_compress_forceExtDict(%union.LZ4_stream_u* %LZ4_dict, i8* %source, i8* %dest, i32 %srcSize) #0 {
entry:
  %LZ4_dict.addr = alloca %union.LZ4_stream_u*, align 4
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %streamPtr = alloca %struct.LZ4_stream_t_internal*, align 4
  %result = alloca i32, align 4
  store %union.LZ4_stream_u* %LZ4_dict, %union.LZ4_stream_u** %LZ4_dict.addr, align 4, !tbaa !7
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !3
  %0 = bitcast %struct.LZ4_stream_t_internal** %streamPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %LZ4_dict.addr, align 4, !tbaa !7
  %internal_donotuse = bitcast %union.LZ4_stream_u* %1 to %struct.LZ4_stream_t_internal*
  store %struct.LZ4_stream_t_internal* %internal_donotuse, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %2 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %4 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  call void @LZ4_renormDictT(%struct.LZ4_stream_t_internal* %3, i32 %4)
  %5 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictSize = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %5, i32 0, i32 6
  %6 = load i32, i32* %dictSize, align 4, !tbaa !17
  %cmp = icmp ult i32 %6, 65536
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %7 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictSize1 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %7, i32 0, i32 6
  %8 = load i32, i32* %dictSize1, align 4, !tbaa !17
  %9 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %currentOffset = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %9, i32 0, i32 1
  %10 = load i32, i32* %currentOffset, align 4, !tbaa !12
  %cmp2 = icmp ult i32 %8, %10
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %11 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %12 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %13 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %14 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %11, i8* %12, i8* %13, i32 %14, i32* null, i32 0, i32 0, i32 2, i32 2, i32 1, i32 1)
  store i32 %call, i32* %result, align 4, !tbaa !3
  br label %if.end

if.else:                                          ; preds = %land.lhs.true, %entry
  %15 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %16 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %17 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %18 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %call3 = call i32 @LZ4_compress_generic(%struct.LZ4_stream_t_internal* %15, i8* %16, i8* %17, i32 %18, i32* null, i32 0, i32 0, i32 2, i32 2, i32 0, i32 1)
  store i32 %call3, i32* %result, align 4, !tbaa !3
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %19 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %20 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictionary = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %20, i32 0, i32 4
  store i8* %19, i8** %dictionary, align 4, !tbaa !16
  %21 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %22 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %streamPtr, align 4, !tbaa !7
  %dictSize4 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %22, i32 0, i32 6
  store i32 %21, i32* %dictSize4, align 4, !tbaa !17
  %23 = load i32, i32* %result, align 4, !tbaa !3
  %24 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #7
  %25 = bitcast %struct.LZ4_stream_t_internal** %streamPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #7
  ret i32 %23
}

; Function Attrs: nounwind
define i32 @LZ4_saveDict(%union.LZ4_stream_u* %LZ4_dict, i8* %safeBuffer, i32 %dictSize) #0 {
entry:
  %LZ4_dict.addr = alloca %union.LZ4_stream_u*, align 4
  %safeBuffer.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  %dict = alloca %struct.LZ4_stream_t_internal*, align 4
  %previousDictEnd = alloca i8*, align 4
  store %union.LZ4_stream_u* %LZ4_dict, %union.LZ4_stream_u** %LZ4_dict.addr, align 4, !tbaa !7
  store i8* %safeBuffer, i8** %safeBuffer.addr, align 4, !tbaa !7
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !3
  %0 = bitcast %struct.LZ4_stream_t_internal** %dict to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %LZ4_dict.addr, align 4, !tbaa !7
  %internal_donotuse = bitcast %union.LZ4_stream_u* %1 to %struct.LZ4_stream_t_internal*
  store %struct.LZ4_stream_t_internal* %internal_donotuse, %struct.LZ4_stream_t_internal** %dict, align 4, !tbaa !7
  %2 = bitcast i8** %previousDictEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dict, align 4, !tbaa !7
  %dictionary = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %3, i32 0, i32 4
  %4 = load i8*, i8** %dictionary, align 4, !tbaa !16
  %5 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dict, align 4, !tbaa !7
  %dictSize1 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %5, i32 0, i32 6
  %6 = load i32, i32* %dictSize1, align 4, !tbaa !17
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %6
  store i8* %add.ptr, i8** %previousDictEnd, align 4, !tbaa !7
  %7 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %cmp = icmp ugt i32 %7, 65536
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 65536, i32* %dictSize.addr, align 4, !tbaa !3
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %9 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dict, align 4, !tbaa !7
  %dictSize2 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %9, i32 0, i32 6
  %10 = load i32, i32* %dictSize2, align 4, !tbaa !17
  %cmp3 = icmp ugt i32 %8, %10
  br i1 %cmp3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.end
  %11 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dict, align 4, !tbaa !7
  %dictSize5 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %11, i32 0, i32 6
  %12 = load i32, i32* %dictSize5, align 4, !tbaa !17
  store i32 %12, i32* %dictSize.addr, align 4, !tbaa !3
  br label %if.end6

if.end6:                                          ; preds = %if.then4, %if.end
  %13 = load i8*, i8** %safeBuffer.addr, align 4, !tbaa !7
  %14 = load i8*, i8** %previousDictEnd, align 4, !tbaa !7
  %15 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %idx.neg = sub i32 0, %15
  %add.ptr7 = getelementptr inbounds i8, i8* %14, i32 %idx.neg
  %16 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %13, i8* align 1 %add.ptr7, i32 %16, i1 false)
  %17 = load i8*, i8** %safeBuffer.addr, align 4, !tbaa !7
  %18 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dict, align 4, !tbaa !7
  %dictionary8 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %18, i32 0, i32 4
  store i8* %17, i8** %dictionary8, align 4, !tbaa !16
  %19 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %20 = load %struct.LZ4_stream_t_internal*, %struct.LZ4_stream_t_internal** %dict, align 4, !tbaa !7
  %dictSize9 = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %20, i32 0, i32 6
  store i32 %19, i32* %dictSize9, align 4, !tbaa !17
  %21 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %22 = bitcast i8** %previousDictEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #7
  %23 = bitcast %struct.LZ4_stream_t_internal** %dict to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #7
  ret i32 %21
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define i32 @LZ4_decompress_safe(i8* %source, i8* %dest, i32 %compressedSize, i32 %maxDecompressedSize) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %compressedSize.addr = alloca i32, align 4
  %maxDecompressedSize.addr = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %compressedSize, i32* %compressedSize.addr, align 4, !tbaa !3
  store i32 %maxDecompressedSize, i32* %maxDecompressedSize.addr, align 4, !tbaa !3
  %0 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %2 = load i32, i32* %compressedSize.addr, align 4, !tbaa !3
  %3 = load i32, i32* %maxDecompressedSize.addr, align 4, !tbaa !3
  %4 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %call = call i32 @LZ4_decompress_generic(i8* %0, i8* %1, i32 %2, i32 %3, i32 1, i32 0, i32 0, i8* %4, i8* null, i32 0)
  ret i32 %call
}

; Function Attrs: alwaysinline nounwind
define internal i32 @LZ4_decompress_generic(i8* %src, i8* %dst, i32 %srcSize, i32 %outputSize, i32 %endOnInput, i32 %partialDecoding, i32 %dict, i8* %lowPrefix, i8* %dictStart, i32 %dictSize) #2 {
entry:
  %retval = alloca i32, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %outputSize.addr = alloca i32, align 4
  %endOnInput.addr = alloca i32, align 4
  %partialDecoding.addr = alloca i32, align 4
  %dict.addr = alloca i32, align 4
  %lowPrefix.addr = alloca i8*, align 4
  %dictStart.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  %ip = alloca i8*, align 4
  %iend = alloca i8*, align 4
  %op = alloca i8*, align 4
  %oend = alloca i8*, align 4
  %cpy = alloca i8*, align 4
  %dictEnd = alloca i8*, align 4
  %safeDecode = alloca i32, align 4
  %checkOffset = alloca i32, align 4
  %shortiend = alloca i8*, align 4
  %shortoend = alloca i8*, align 4
  %match = alloca i8*, align 4
  %offset = alloca i32, align 4
  %token = alloca i32, align 4
  %length = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %error = alloca i32, align 4
  %error219 = alloca i32, align 4
  %copySize = alloca i32, align 4
  %restSize = alloca i32, align 4
  %endOfMatch = alloca i8*, align 4
  %copyFrom = alloca i8*, align 4
  %mlen = alloca i32, align 4
  %matchEnd = alloca i8*, align 4
  %copyEnd = alloca i8*, align 4
  %oCopyLimit = alloca i8*, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !7
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !3
  store i32 %outputSize, i32* %outputSize.addr, align 4, !tbaa !3
  store i32 %endOnInput, i32* %endOnInput.addr, align 4, !tbaa !9
  store i32 %partialDecoding, i32* %partialDecoding.addr, align 4, !tbaa !9
  store i32 %dict, i32* %dict.addr, align 4, !tbaa !9
  store i8* %lowPrefix, i8** %lowPrefix.addr, align 4, !tbaa !7
  store i8* %dictStart, i8** %dictStart.addr, align 4, !tbaa !7
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !10
  %0 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %cmp = icmp eq i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast i8** %ip to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i8*, i8** %src.addr, align 4, !tbaa !7
  store i8* %2, i8** %ip, align 4, !tbaa !7
  %3 = bitcast i8** %iend to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i8*, i8** %ip, align 4, !tbaa !7
  %5 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %5
  store i8* %add.ptr, i8** %iend, align 4, !tbaa !7
  %6 = bitcast i8** %op to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #7
  %7 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  store i8* %7, i8** %op, align 4, !tbaa !7
  %8 = bitcast i8** %oend to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = load i8*, i8** %op, align 4, !tbaa !7
  %10 = load i32, i32* %outputSize.addr, align 4, !tbaa !3
  %add.ptr1 = getelementptr inbounds i8, i8* %9, i32 %10
  store i8* %add.ptr1, i8** %oend, align 4, !tbaa !7
  %11 = bitcast i8** %cpy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #7
  %12 = bitcast i8** %dictEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #7
  %13 = load i8*, i8** %dictStart.addr, align 4, !tbaa !7
  %cmp2 = icmp eq i8* %13, null
  br i1 %cmp2, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %14 = load i8*, i8** %dictStart.addr, align 4, !tbaa !7
  %15 = load i32, i32* %dictSize.addr, align 4, !tbaa !10
  %add.ptr3 = getelementptr inbounds i8, i8* %14, i32 %15
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ null, %cond.true ], [ %add.ptr3, %cond.false ]
  store i8* %cond, i8** %dictEnd, align 4, !tbaa !7
  %16 = bitcast i32* %safeDecode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %17 = load i32, i32* %endOnInput.addr, align 4, !tbaa !9
  %cmp4 = icmp eq i32 %17, 1
  %conv = zext i1 %cmp4 to i32
  store i32 %conv, i32* %safeDecode, align 4, !tbaa !3
  %18 = bitcast i32* %checkOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #7
  %19 = load i32, i32* %safeDecode, align 4, !tbaa !3
  %tobool = icmp ne i32 %19, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %cond.end
  %20 = load i32, i32* %dictSize.addr, align 4, !tbaa !10
  %cmp5 = icmp ult i32 %20, 65536
  br label %land.end

land.end:                                         ; preds = %land.rhs, %cond.end
  %21 = phi i1 [ false, %cond.end ], [ %cmp5, %land.rhs ]
  %land.ext = zext i1 %21 to i32
  store i32 %land.ext, i32* %checkOffset, align 4, !tbaa !3
  %22 = bitcast i8** %shortiend to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #7
  %23 = load i8*, i8** %iend, align 4, !tbaa !7
  %24 = load i32, i32* %endOnInput.addr, align 4, !tbaa !9
  %tobool7 = icmp ne i32 %24, 0
  %25 = zext i1 %tobool7 to i64
  %cond8 = select i1 %tobool7, i32 14, i32 8
  %idx.neg = sub i32 0, %cond8
  %add.ptr9 = getelementptr inbounds i8, i8* %23, i32 %idx.neg
  %add.ptr10 = getelementptr inbounds i8, i8* %add.ptr9, i32 -2
  store i8* %add.ptr10, i8** %shortiend, align 4, !tbaa !7
  %26 = bitcast i8** %shortoend to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #7
  %27 = load i8*, i8** %oend, align 4, !tbaa !7
  %28 = load i32, i32* %endOnInput.addr, align 4, !tbaa !9
  %tobool11 = icmp ne i32 %28, 0
  %29 = zext i1 %tobool11 to i64
  %cond12 = select i1 %tobool11, i32 14, i32 8
  %idx.neg13 = sub i32 0, %cond12
  %add.ptr14 = getelementptr inbounds i8, i8* %27, i32 %idx.neg13
  %add.ptr15 = getelementptr inbounds i8, i8* %add.ptr14, i32 -18
  store i8* %add.ptr15, i8** %shortoend, align 4, !tbaa !7
  %30 = bitcast i8** %match to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #7
  %31 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #7
  %32 = bitcast i32* %token to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #7
  %33 = bitcast i32* %length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #7
  %34 = load i32, i32* %endOnInput.addr, align 4, !tbaa !9
  %tobool16 = icmp ne i32 %34, 0
  br i1 %tobool16, label %land.lhs.true, label %if.end35

land.lhs.true:                                    ; preds = %land.end
  %35 = load i32, i32* %outputSize.addr, align 4, !tbaa !3
  %cmp17 = icmp eq i32 %35, 0
  %conv18 = zext i1 %cmp17 to i32
  %cmp19 = icmp ne i32 %conv18, 0
  %conv20 = zext i1 %cmp19 to i32
  %expval = call i32 @llvm.expect.i32(i32 %conv20, i32 0)
  %tobool21 = icmp ne i32 %expval, 0
  br i1 %tobool21, label %if.then22, label %if.end35

if.then22:                                        ; preds = %land.lhs.true
  %36 = load i32, i32* %partialDecoding.addr, align 4, !tbaa !9
  %tobool23 = icmp ne i32 %36, 0
  br i1 %tobool23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %if.then22
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup445

if.end25:                                         ; preds = %if.then22
  %37 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %cmp26 = icmp eq i32 %37, 1
  br i1 %cmp26, label %land.rhs28, label %land.end32

land.rhs28:                                       ; preds = %if.end25
  %38 = load i8*, i8** %ip, align 4, !tbaa !7
  %39 = load i8, i8* %38, align 1, !tbaa !9
  %conv29 = zext i8 %39 to i32
  %cmp30 = icmp eq i32 %conv29, 0
  br label %land.end32

land.end32:                                       ; preds = %land.rhs28, %if.end25
  %40 = phi i1 [ false, %if.end25 ], [ %cmp30, %land.rhs28 ]
  %41 = zext i1 %40 to i64
  %cond34 = select i1 %40, i32 0, i32 -1
  store i32 %cond34, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup445

if.end35:                                         ; preds = %land.lhs.true, %land.end
  %42 = load i32, i32* %endOnInput.addr, align 4, !tbaa !9
  %tobool36 = icmp ne i32 %42, 0
  br i1 %tobool36, label %if.end49, label %land.lhs.true37

land.lhs.true37:                                  ; preds = %if.end35
  %43 = load i32, i32* %outputSize.addr, align 4, !tbaa !3
  %cmp38 = icmp eq i32 %43, 0
  %conv39 = zext i1 %cmp38 to i32
  %cmp40 = icmp ne i32 %conv39, 0
  %conv41 = zext i1 %cmp40 to i32
  %expval42 = call i32 @llvm.expect.i32(i32 %conv41, i32 0)
  %tobool43 = icmp ne i32 %expval42, 0
  br i1 %tobool43, label %if.then44, label %if.end49

if.then44:                                        ; preds = %land.lhs.true37
  %44 = load i8*, i8** %ip, align 4, !tbaa !7
  %45 = load i8, i8* %44, align 1, !tbaa !9
  %conv45 = zext i8 %45 to i32
  %cmp46 = icmp eq i32 %conv45, 0
  %46 = zext i1 %cmp46 to i64
  %cond48 = select i1 %cmp46, i32 1, i32 -1
  store i32 %cond48, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup445

if.end49:                                         ; preds = %land.lhs.true37, %if.end35
  %47 = load i32, i32* %endOnInput.addr, align 4, !tbaa !9
  %tobool50 = icmp ne i32 %47, 0
  br i1 %tobool50, label %land.lhs.true51, label %if.end59

land.lhs.true51:                                  ; preds = %if.end49
  %48 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %cmp52 = icmp eq i32 %48, 0
  %conv53 = zext i1 %cmp52 to i32
  %cmp54 = icmp ne i32 %conv53, 0
  %conv55 = zext i1 %cmp54 to i32
  %expval56 = call i32 @llvm.expect.i32(i32 %conv55, i32 0)
  %tobool57 = icmp ne i32 %expval56, 0
  br i1 %tobool57, label %if.then58, label %if.end59

if.then58:                                        ; preds = %land.lhs.true51
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup445

if.end59:                                         ; preds = %land.lhs.true51, %if.end49
  br label %while.cond

while.cond:                                       ; preds = %if.end429, %cleanup361, %if.end322, %if.then102, %if.end59
  br label %while.body

while.body:                                       ; preds = %while.cond
  %49 = load i8*, i8** %ip, align 4, !tbaa !7
  %incdec.ptr = getelementptr inbounds i8, i8* %49, i32 1
  store i8* %incdec.ptr, i8** %ip, align 4, !tbaa !7
  %50 = load i8, i8* %49, align 1, !tbaa !9
  %conv60 = zext i8 %50 to i32
  store i32 %conv60, i32* %token, align 4, !tbaa !3
  %51 = load i32, i32* %token, align 4, !tbaa !3
  %shr = lshr i32 %51, 4
  store i32 %shr, i32* %length, align 4, !tbaa !10
  %52 = load i32, i32* %endOnInput.addr, align 4, !tbaa !9
  %tobool61 = icmp ne i32 %52, 0
  br i1 %tobool61, label %cond.true62, label %cond.false65

cond.true62:                                      ; preds = %while.body
  %53 = load i32, i32* %length, align 4, !tbaa !10
  %cmp63 = icmp ne i32 %53, 15
  br i1 %cmp63, label %land.lhs.true68, label %if.end111

cond.false65:                                     ; preds = %while.body
  %54 = load i32, i32* %length, align 4, !tbaa !10
  %cmp66 = icmp ule i32 %54, 8
  br i1 %cmp66, label %land.lhs.true68, label %if.end111

land.lhs.true68:                                  ; preds = %cond.false65, %cond.true62
  %55 = load i32, i32* %endOnInput.addr, align 4, !tbaa !9
  %tobool69 = icmp ne i32 %55, 0
  br i1 %tobool69, label %cond.true70, label %cond.false73

cond.true70:                                      ; preds = %land.lhs.true68
  %56 = load i8*, i8** %ip, align 4, !tbaa !7
  %57 = load i8*, i8** %shortiend, align 4, !tbaa !7
  %cmp71 = icmp ult i8* %56, %57
  %conv72 = zext i1 %cmp71 to i32
  br label %cond.end74

cond.false73:                                     ; preds = %land.lhs.true68
  br label %cond.end74

cond.end74:                                       ; preds = %cond.false73, %cond.true70
  %cond75 = phi i32 [ %conv72, %cond.true70 ], [ 1, %cond.false73 ]
  %58 = load i8*, i8** %op, align 4, !tbaa !7
  %59 = load i8*, i8** %shortoend, align 4, !tbaa !7
  %cmp76 = icmp ule i8* %58, %59
  %conv77 = zext i1 %cmp76 to i32
  %and = and i32 %cond75, %conv77
  %cmp78 = icmp ne i32 %and, 0
  %conv79 = zext i1 %cmp78 to i32
  %expval80 = call i32 @llvm.expect.i32(i32 %conv79, i32 1)
  %tobool81 = icmp ne i32 %expval80, 0
  br i1 %tobool81, label %if.then82, label %if.end111

if.then82:                                        ; preds = %cond.end74
  %60 = load i8*, i8** %op, align 4, !tbaa !7
  %61 = load i8*, i8** %ip, align 4, !tbaa !7
  %62 = load i32, i32* %endOnInput.addr, align 4, !tbaa !9
  %tobool83 = icmp ne i32 %62, 0
  %63 = zext i1 %tobool83 to i64
  %cond84 = select i1 %tobool83, i32 16, i32 8
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %60, i8* align 1 %61, i32 %cond84, i1 false)
  %64 = load i32, i32* %length, align 4, !tbaa !10
  %65 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr85 = getelementptr inbounds i8, i8* %65, i32 %64
  store i8* %add.ptr85, i8** %op, align 4, !tbaa !7
  %66 = load i32, i32* %length, align 4, !tbaa !10
  %67 = load i8*, i8** %ip, align 4, !tbaa !7
  %add.ptr86 = getelementptr inbounds i8, i8* %67, i32 %66
  store i8* %add.ptr86, i8** %ip, align 4, !tbaa !7
  %68 = load i32, i32* %token, align 4, !tbaa !3
  %and87 = and i32 %68, 15
  store i32 %and87, i32* %length, align 4, !tbaa !10
  %69 = load i8*, i8** %ip, align 4, !tbaa !7
  %call = call zeroext i16 @LZ4_readLE16(i8* %69)
  %conv88 = zext i16 %call to i32
  store i32 %conv88, i32* %offset, align 4, !tbaa !10
  %70 = load i8*, i8** %ip, align 4, !tbaa !7
  %add.ptr89 = getelementptr inbounds i8, i8* %70, i32 2
  store i8* %add.ptr89, i8** %ip, align 4, !tbaa !7
  %71 = load i8*, i8** %op, align 4, !tbaa !7
  %72 = load i32, i32* %offset, align 4, !tbaa !10
  %idx.neg90 = sub i32 0, %72
  %add.ptr91 = getelementptr inbounds i8, i8* %71, i32 %idx.neg90
  store i8* %add.ptr91, i8** %match, align 4, !tbaa !7
  %73 = load i32, i32* %length, align 4, !tbaa !10
  %cmp92 = icmp ne i32 %73, 15
  br i1 %cmp92, label %land.lhs.true94, label %if.end110

land.lhs.true94:                                  ; preds = %if.then82
  %74 = load i32, i32* %offset, align 4, !tbaa !10
  %cmp95 = icmp uge i32 %74, 8
  br i1 %cmp95, label %land.lhs.true97, label %if.end110

land.lhs.true97:                                  ; preds = %land.lhs.true94
  %75 = load i32, i32* %dict.addr, align 4, !tbaa !9
  %cmp98 = icmp eq i32 %75, 1
  br i1 %cmp98, label %if.then102, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true97
  %76 = load i8*, i8** %match, align 4, !tbaa !7
  %77 = load i8*, i8** %lowPrefix.addr, align 4, !tbaa !7
  %cmp100 = icmp uge i8* %76, %77
  br i1 %cmp100, label %if.then102, label %if.end110

if.then102:                                       ; preds = %lor.lhs.false, %land.lhs.true97
  %78 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr103 = getelementptr inbounds i8, i8* %78, i32 0
  %79 = load i8*, i8** %match, align 4, !tbaa !7
  %add.ptr104 = getelementptr inbounds i8, i8* %79, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr103, i8* align 1 %add.ptr104, i32 8, i1 false)
  %80 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr105 = getelementptr inbounds i8, i8* %80, i32 8
  %81 = load i8*, i8** %match, align 4, !tbaa !7
  %add.ptr106 = getelementptr inbounds i8, i8* %81, i32 8
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr105, i8* align 1 %add.ptr106, i32 8, i1 false)
  %82 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr107 = getelementptr inbounds i8, i8* %82, i32 16
  %83 = load i8*, i8** %match, align 4, !tbaa !7
  %add.ptr108 = getelementptr inbounds i8, i8* %83, i32 16
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr107, i8* align 1 %add.ptr108, i32 2, i1 false)
  %84 = load i32, i32* %length, align 4, !tbaa !10
  %add = add i32 %84, 4
  %85 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr109 = getelementptr inbounds i8, i8* %85, i32 %add
  store i8* %add.ptr109, i8** %op, align 4, !tbaa !7
  br label %while.cond

if.end110:                                        ; preds = %lor.lhs.false, %land.lhs.true94, %if.then82
  br label %_copy_match

if.end111:                                        ; preds = %cond.end74, %cond.false65, %cond.true62
  %86 = load i32, i32* %length, align 4, !tbaa !10
  %cmp112 = icmp eq i32 %86, 15
  br i1 %cmp112, label %if.then114, label %if.end144

if.then114:                                       ; preds = %if.end111
  %87 = bitcast i32* %error to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #7
  store i32 0, i32* %error, align 4, !tbaa !9
  %88 = load i8*, i8** %iend, align 4, !tbaa !7
  %add.ptr115 = getelementptr inbounds i8, i8* %88, i32 -15
  %89 = load i32, i32* %endOnInput.addr, align 4, !tbaa !9
  %90 = load i32, i32* %endOnInput.addr, align 4, !tbaa !9
  %call116 = call i32 @read_variable_length(i8** %ip, i8* %add.ptr115, i32 %89, i32 %90, i32* %error)
  %91 = load i32, i32* %length, align 4, !tbaa !10
  %add117 = add i32 %91, %call116
  store i32 %add117, i32* %length, align 4, !tbaa !10
  %92 = load i32, i32* %error, align 4, !tbaa !9
  %cmp118 = icmp eq i32 %92, -1
  br i1 %cmp118, label %if.then120, label %if.end121

if.then120:                                       ; preds = %if.then114
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end121:                                        ; preds = %if.then114
  %93 = load i32, i32* %safeDecode, align 4, !tbaa !3
  %tobool122 = icmp ne i32 %93, 0
  br i1 %tobool122, label %land.lhs.true123, label %if.end132

land.lhs.true123:                                 ; preds = %if.end121
  %94 = load i8*, i8** %op, align 4, !tbaa !7
  %95 = ptrtoint i8* %94 to i32
  %96 = load i32, i32* %length, align 4, !tbaa !10
  %add124 = add i32 %95, %96
  %97 = load i8*, i8** %op, align 4, !tbaa !7
  %98 = ptrtoint i8* %97 to i32
  %cmp125 = icmp ult i32 %add124, %98
  %conv126 = zext i1 %cmp125 to i32
  %cmp127 = icmp ne i32 %conv126, 0
  %conv128 = zext i1 %cmp127 to i32
  %expval129 = call i32 @llvm.expect.i32(i32 %conv128, i32 0)
  %tobool130 = icmp ne i32 %expval129, 0
  br i1 %tobool130, label %if.then131, label %if.end132

if.then131:                                       ; preds = %land.lhs.true123
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end132:                                        ; preds = %land.lhs.true123, %if.end121
  %99 = load i32, i32* %safeDecode, align 4, !tbaa !3
  %tobool133 = icmp ne i32 %99, 0
  br i1 %tobool133, label %land.lhs.true134, label %if.end143

land.lhs.true134:                                 ; preds = %if.end132
  %100 = load i8*, i8** %ip, align 4, !tbaa !7
  %101 = ptrtoint i8* %100 to i32
  %102 = load i32, i32* %length, align 4, !tbaa !10
  %add135 = add i32 %101, %102
  %103 = load i8*, i8** %ip, align 4, !tbaa !7
  %104 = ptrtoint i8* %103 to i32
  %cmp136 = icmp ult i32 %add135, %104
  %conv137 = zext i1 %cmp136 to i32
  %cmp138 = icmp ne i32 %conv137, 0
  %conv139 = zext i1 %cmp138 to i32
  %expval140 = call i32 @llvm.expect.i32(i32 %conv139, i32 0)
  %tobool141 = icmp ne i32 %expval140, 0
  br i1 %tobool141, label %if.then142, label %if.end143

if.then142:                                       ; preds = %land.lhs.true134
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end143:                                        ; preds = %land.lhs.true134, %if.end132
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.then142, %if.then131, %if.then120, %if.end143
  %105 = bitcast i32* %error to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup445 [
    i32 0, label %cleanup.cont
    i32 5, label %_output_error
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end144

if.end144:                                        ; preds = %cleanup.cont, %if.end111
  %106 = load i8*, i8** %op, align 4, !tbaa !7
  %107 = load i32, i32* %length, align 4, !tbaa !10
  %add.ptr145 = getelementptr inbounds i8, i8* %106, i32 %107
  store i8* %add.ptr145, i8** %cpy, align 4, !tbaa !7
  %108 = load i32, i32* %endOnInput.addr, align 4, !tbaa !9
  %tobool146 = icmp ne i32 %108, 0
  br i1 %tobool146, label %land.lhs.true147, label %lor.lhs.false156

land.lhs.true147:                                 ; preds = %if.end144
  %109 = load i8*, i8** %cpy, align 4, !tbaa !7
  %110 = load i8*, i8** %oend, align 4, !tbaa !7
  %add.ptr148 = getelementptr inbounds i8, i8* %110, i32 -12
  %cmp149 = icmp ugt i8* %109, %add.ptr148
  br i1 %cmp149, label %if.then162, label %lor.lhs.false151

lor.lhs.false151:                                 ; preds = %land.lhs.true147
  %111 = load i8*, i8** %ip, align 4, !tbaa !7
  %112 = load i32, i32* %length, align 4, !tbaa !10
  %add.ptr152 = getelementptr inbounds i8, i8* %111, i32 %112
  %113 = load i8*, i8** %iend, align 4, !tbaa !7
  %add.ptr153 = getelementptr inbounds i8, i8* %113, i32 -8
  %cmp154 = icmp ugt i8* %add.ptr152, %add.ptr153
  br i1 %cmp154, label %if.then162, label %lor.lhs.false156

lor.lhs.false156:                                 ; preds = %lor.lhs.false151, %if.end144
  %114 = load i32, i32* %endOnInput.addr, align 4, !tbaa !9
  %tobool157 = icmp ne i32 %114, 0
  br i1 %tobool157, label %if.else207, label %land.lhs.true158

land.lhs.true158:                                 ; preds = %lor.lhs.false156
  %115 = load i8*, i8** %cpy, align 4, !tbaa !7
  %116 = load i8*, i8** %oend, align 4, !tbaa !7
  %add.ptr159 = getelementptr inbounds i8, i8* %116, i32 -8
  %cmp160 = icmp ugt i8* %115, %add.ptr159
  br i1 %cmp160, label %if.then162, label %if.else207

if.then162:                                       ; preds = %land.lhs.true158, %lor.lhs.false151, %land.lhs.true147
  %117 = load i32, i32* %partialDecoding.addr, align 4, !tbaa !9
  %tobool163 = icmp ne i32 %117, 0
  br i1 %tobool163, label %if.then164, label %if.else

if.then164:                                       ; preds = %if.then162
  %118 = load i8*, i8** %ip, align 4, !tbaa !7
  %119 = load i32, i32* %length, align 4, !tbaa !10
  %add.ptr165 = getelementptr inbounds i8, i8* %118, i32 %119
  %120 = load i8*, i8** %iend, align 4, !tbaa !7
  %add.ptr166 = getelementptr inbounds i8, i8* %120, i32 -8
  %cmp167 = icmp ugt i8* %add.ptr165, %add.ptr166
  br i1 %cmp167, label %land.lhs.true169, label %if.end174

land.lhs.true169:                                 ; preds = %if.then164
  %121 = load i8*, i8** %ip, align 4, !tbaa !7
  %122 = load i32, i32* %length, align 4, !tbaa !10
  %add.ptr170 = getelementptr inbounds i8, i8* %121, i32 %122
  %123 = load i8*, i8** %iend, align 4, !tbaa !7
  %cmp171 = icmp ne i8* %add.ptr170, %123
  br i1 %cmp171, label %if.then173, label %if.end174

if.then173:                                       ; preds = %land.lhs.true169
  br label %_output_error

if.end174:                                        ; preds = %land.lhs.true169, %if.then164
  %124 = load i8*, i8** %cpy, align 4, !tbaa !7
  %125 = load i8*, i8** %oend, align 4, !tbaa !7
  %cmp175 = icmp ugt i8* %124, %125
  br i1 %cmp175, label %if.then177, label %if.end178

if.then177:                                       ; preds = %if.end174
  %126 = load i8*, i8** %oend, align 4, !tbaa !7
  store i8* %126, i8** %cpy, align 4, !tbaa !7
  %127 = load i8*, i8** %oend, align 4, !tbaa !7
  %128 = load i8*, i8** %op, align 4, !tbaa !7
  %sub.ptr.lhs.cast = ptrtoint i8* %127 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %128 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %length, align 4, !tbaa !10
  br label %if.end178

if.end178:                                        ; preds = %if.then177, %if.end174
  br label %if.end195

if.else:                                          ; preds = %if.then162
  %129 = load i32, i32* %endOnInput.addr, align 4, !tbaa !9
  %tobool179 = icmp ne i32 %129, 0
  br i1 %tobool179, label %if.end184, label %land.lhs.true180

land.lhs.true180:                                 ; preds = %if.else
  %130 = load i8*, i8** %cpy, align 4, !tbaa !7
  %131 = load i8*, i8** %oend, align 4, !tbaa !7
  %cmp181 = icmp ne i8* %130, %131
  br i1 %cmp181, label %if.then183, label %if.end184

if.then183:                                       ; preds = %land.lhs.true180
  br label %_output_error

if.end184:                                        ; preds = %land.lhs.true180, %if.else
  %132 = load i32, i32* %endOnInput.addr, align 4, !tbaa !9
  %tobool185 = icmp ne i32 %132, 0
  br i1 %tobool185, label %land.lhs.true186, label %if.end194

land.lhs.true186:                                 ; preds = %if.end184
  %133 = load i8*, i8** %ip, align 4, !tbaa !7
  %134 = load i32, i32* %length, align 4, !tbaa !10
  %add.ptr187 = getelementptr inbounds i8, i8* %133, i32 %134
  %135 = load i8*, i8** %iend, align 4, !tbaa !7
  %cmp188 = icmp ne i8* %add.ptr187, %135
  br i1 %cmp188, label %if.then193, label %lor.lhs.false190

lor.lhs.false190:                                 ; preds = %land.lhs.true186
  %136 = load i8*, i8** %cpy, align 4, !tbaa !7
  %137 = load i8*, i8** %oend, align 4, !tbaa !7
  %cmp191 = icmp ugt i8* %136, %137
  br i1 %cmp191, label %if.then193, label %if.end194

if.then193:                                       ; preds = %lor.lhs.false190, %land.lhs.true186
  br label %_output_error

if.end194:                                        ; preds = %lor.lhs.false190, %if.end184
  br label %if.end195

if.end195:                                        ; preds = %if.end194, %if.end178
  %138 = load i8*, i8** %op, align 4, !tbaa !7
  %139 = load i8*, i8** %ip, align 4, !tbaa !7
  %140 = load i32, i32* %length, align 4, !tbaa !10
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %138, i8* align 1 %139, i32 %140, i1 false)
  %141 = load i32, i32* %length, align 4, !tbaa !10
  %142 = load i8*, i8** %ip, align 4, !tbaa !7
  %add.ptr196 = getelementptr inbounds i8, i8* %142, i32 %141
  store i8* %add.ptr196, i8** %ip, align 4, !tbaa !7
  %143 = load i32, i32* %length, align 4, !tbaa !10
  %144 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr197 = getelementptr inbounds i8, i8* %144, i32 %143
  store i8* %add.ptr197, i8** %op, align 4, !tbaa !7
  %145 = load i32, i32* %partialDecoding.addr, align 4, !tbaa !9
  %tobool198 = icmp ne i32 %145, 0
  br i1 %tobool198, label %lor.lhs.false199, label %if.then205

lor.lhs.false199:                                 ; preds = %if.end195
  %146 = load i8*, i8** %cpy, align 4, !tbaa !7
  %147 = load i8*, i8** %oend, align 4, !tbaa !7
  %cmp200 = icmp eq i8* %146, %147
  br i1 %cmp200, label %if.then205, label %lor.lhs.false202

lor.lhs.false202:                                 ; preds = %lor.lhs.false199
  %148 = load i8*, i8** %ip, align 4, !tbaa !7
  %149 = load i8*, i8** %iend, align 4, !tbaa !7
  %cmp203 = icmp eq i8* %148, %149
  br i1 %cmp203, label %if.then205, label %if.end206

if.then205:                                       ; preds = %lor.lhs.false202, %lor.lhs.false199, %if.end195
  br label %while.end430

if.end206:                                        ; preds = %lor.lhs.false202
  br label %if.end209

if.else207:                                       ; preds = %land.lhs.true158, %lor.lhs.false156
  %150 = load i8*, i8** %op, align 4, !tbaa !7
  %151 = load i8*, i8** %ip, align 4, !tbaa !7
  %152 = load i8*, i8** %cpy, align 4, !tbaa !7
  call void @LZ4_wildCopy8(i8* %150, i8* %151, i8* %152)
  %153 = load i32, i32* %length, align 4, !tbaa !10
  %154 = load i8*, i8** %ip, align 4, !tbaa !7
  %add.ptr208 = getelementptr inbounds i8, i8* %154, i32 %153
  store i8* %add.ptr208, i8** %ip, align 4, !tbaa !7
  %155 = load i8*, i8** %cpy, align 4, !tbaa !7
  store i8* %155, i8** %op, align 4, !tbaa !7
  br label %if.end209

if.end209:                                        ; preds = %if.else207, %if.end206
  %156 = load i8*, i8** %ip, align 4, !tbaa !7
  %call210 = call zeroext i16 @LZ4_readLE16(i8* %156)
  %conv211 = zext i16 %call210 to i32
  store i32 %conv211, i32* %offset, align 4, !tbaa !10
  %157 = load i8*, i8** %ip, align 4, !tbaa !7
  %add.ptr212 = getelementptr inbounds i8, i8* %157, i32 2
  store i8* %add.ptr212, i8** %ip, align 4, !tbaa !7
  %158 = load i8*, i8** %op, align 4, !tbaa !7
  %159 = load i32, i32* %offset, align 4, !tbaa !10
  %idx.neg213 = sub i32 0, %159
  %add.ptr214 = getelementptr inbounds i8, i8* %158, i32 %idx.neg213
  store i8* %add.ptr214, i8** %match, align 4, !tbaa !7
  %160 = load i32, i32* %token, align 4, !tbaa !3
  %and215 = and i32 %160, 15
  store i32 %and215, i32* %length, align 4, !tbaa !10
  br label %_copy_match

_copy_match:                                      ; preds = %if.end209, %if.end110
  %161 = load i32, i32* %length, align 4, !tbaa !10
  %cmp216 = icmp eq i32 %161, 15
  br i1 %cmp216, label %if.then218, label %if.end242

if.then218:                                       ; preds = %_copy_match
  %162 = bitcast i32* %error219 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %162) #7
  store i32 0, i32* %error219, align 4, !tbaa !9
  %163 = load i8*, i8** %iend, align 4, !tbaa !7
  %add.ptr220 = getelementptr inbounds i8, i8* %163, i32 -5
  %add.ptr221 = getelementptr inbounds i8, i8* %add.ptr220, i32 1
  %164 = load i32, i32* %endOnInput.addr, align 4, !tbaa !9
  %call222 = call i32 @read_variable_length(i8** %ip, i8* %add.ptr221, i32 %164, i32 0, i32* %error219)
  %165 = load i32, i32* %length, align 4, !tbaa !10
  %add223 = add i32 %165, %call222
  store i32 %add223, i32* %length, align 4, !tbaa !10
  %166 = load i32, i32* %error219, align 4, !tbaa !9
  %cmp224 = icmp ne i32 %166, 0
  br i1 %cmp224, label %if.then226, label %if.end227

if.then226:                                       ; preds = %if.then218
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup239

if.end227:                                        ; preds = %if.then218
  %167 = load i32, i32* %safeDecode, align 4, !tbaa !3
  %tobool228 = icmp ne i32 %167, 0
  br i1 %tobool228, label %land.lhs.true229, label %if.end238

land.lhs.true229:                                 ; preds = %if.end227
  %168 = load i8*, i8** %op, align 4, !tbaa !7
  %169 = ptrtoint i8* %168 to i32
  %170 = load i32, i32* %length, align 4, !tbaa !10
  %add230 = add i32 %169, %170
  %171 = load i8*, i8** %op, align 4, !tbaa !7
  %172 = ptrtoint i8* %171 to i32
  %cmp231 = icmp ult i32 %add230, %172
  %conv232 = zext i1 %cmp231 to i32
  %cmp233 = icmp ne i32 %conv232, 0
  %conv234 = zext i1 %cmp233 to i32
  %expval235 = call i32 @llvm.expect.i32(i32 %conv234, i32 0)
  %tobool236 = icmp ne i32 %expval235, 0
  br i1 %tobool236, label %if.then237, label %if.end238

if.then237:                                       ; preds = %land.lhs.true229
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup239

if.end238:                                        ; preds = %land.lhs.true229, %if.end227
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup239

cleanup239:                                       ; preds = %if.then237, %if.then226, %if.end238
  %173 = bitcast i32* %error219 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #7
  %cleanup.dest240 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest240, label %cleanup445 [
    i32 0, label %cleanup.cont241
    i32 5, label %_output_error
  ]

cleanup.cont241:                                  ; preds = %cleanup239
  br label %if.end242

if.end242:                                        ; preds = %cleanup.cont241, %_copy_match
  %174 = load i32, i32* %length, align 4, !tbaa !10
  %add243 = add i32 %174, 4
  store i32 %add243, i32* %length, align 4, !tbaa !10
  %175 = load i32, i32* %checkOffset, align 4, !tbaa !3
  %tobool244 = icmp ne i32 %175, 0
  br i1 %tobool244, label %land.lhs.true245, label %if.end254

land.lhs.true245:                                 ; preds = %if.end242
  %176 = load i8*, i8** %match, align 4, !tbaa !7
  %177 = load i32, i32* %dictSize.addr, align 4, !tbaa !10
  %add.ptr246 = getelementptr inbounds i8, i8* %176, i32 %177
  %178 = load i8*, i8** %lowPrefix.addr, align 4, !tbaa !7
  %cmp247 = icmp ult i8* %add.ptr246, %178
  %conv248 = zext i1 %cmp247 to i32
  %cmp249 = icmp ne i32 %conv248, 0
  %conv250 = zext i1 %cmp249 to i32
  %expval251 = call i32 @llvm.expect.i32(i32 %conv250, i32 0)
  %tobool252 = icmp ne i32 %expval251, 0
  br i1 %tobool252, label %if.then253, label %if.end254

if.then253:                                       ; preds = %land.lhs.true245
  br label %_output_error

if.end254:                                        ; preds = %land.lhs.true245, %if.end242
  %179 = load i32, i32* %dict.addr, align 4, !tbaa !9
  %cmp255 = icmp eq i32 %179, 2
  br i1 %cmp255, label %land.lhs.true257, label %if.end323

land.lhs.true257:                                 ; preds = %if.end254
  %180 = load i8*, i8** %match, align 4, !tbaa !7
  %181 = load i8*, i8** %lowPrefix.addr, align 4, !tbaa !7
  %cmp258 = icmp ult i8* %180, %181
  br i1 %cmp258, label %if.then260, label %if.end323

if.then260:                                       ; preds = %land.lhs.true257
  %182 = load i8*, i8** %op, align 4, !tbaa !7
  %183 = load i32, i32* %length, align 4, !tbaa !10
  %add.ptr261 = getelementptr inbounds i8, i8* %182, i32 %183
  %184 = load i8*, i8** %oend, align 4, !tbaa !7
  %add.ptr262 = getelementptr inbounds i8, i8* %184, i32 -5
  %cmp263 = icmp ugt i8* %add.ptr261, %add.ptr262
  %conv264 = zext i1 %cmp263 to i32
  %cmp265 = icmp ne i32 %conv264, 0
  %conv266 = zext i1 %cmp265 to i32
  %expval267 = call i32 @llvm.expect.i32(i32 %conv266, i32 0)
  %tobool268 = icmp ne i32 %expval267, 0
  br i1 %tobool268, label %if.then269, label %if.end286

if.then269:                                       ; preds = %if.then260
  %185 = load i32, i32* %partialDecoding.addr, align 4, !tbaa !9
  %tobool270 = icmp ne i32 %185, 0
  br i1 %tobool270, label %if.then271, label %if.else284

if.then271:                                       ; preds = %if.then269
  %186 = load i32, i32* %length, align 4, !tbaa !10
  %187 = load i8*, i8** %oend, align 4, !tbaa !7
  %188 = load i8*, i8** %op, align 4, !tbaa !7
  %sub.ptr.lhs.cast272 = ptrtoint i8* %187 to i32
  %sub.ptr.rhs.cast273 = ptrtoint i8* %188 to i32
  %sub.ptr.sub274 = sub i32 %sub.ptr.lhs.cast272, %sub.ptr.rhs.cast273
  %cmp275 = icmp ult i32 %186, %sub.ptr.sub274
  br i1 %cmp275, label %cond.true277, label %cond.false278

cond.true277:                                     ; preds = %if.then271
  %189 = load i32, i32* %length, align 4, !tbaa !10
  br label %cond.end282

cond.false278:                                    ; preds = %if.then271
  %190 = load i8*, i8** %oend, align 4, !tbaa !7
  %191 = load i8*, i8** %op, align 4, !tbaa !7
  %sub.ptr.lhs.cast279 = ptrtoint i8* %190 to i32
  %sub.ptr.rhs.cast280 = ptrtoint i8* %191 to i32
  %sub.ptr.sub281 = sub i32 %sub.ptr.lhs.cast279, %sub.ptr.rhs.cast280
  br label %cond.end282

cond.end282:                                      ; preds = %cond.false278, %cond.true277
  %cond283 = phi i32 [ %189, %cond.true277 ], [ %sub.ptr.sub281, %cond.false278 ]
  store i32 %cond283, i32* %length, align 4, !tbaa !10
  br label %if.end285

if.else284:                                       ; preds = %if.then269
  br label %_output_error

if.end285:                                        ; preds = %cond.end282
  br label %if.end286

if.end286:                                        ; preds = %if.end285, %if.then260
  %192 = load i32, i32* %length, align 4, !tbaa !10
  %193 = load i8*, i8** %lowPrefix.addr, align 4, !tbaa !7
  %194 = load i8*, i8** %match, align 4, !tbaa !7
  %sub.ptr.lhs.cast287 = ptrtoint i8* %193 to i32
  %sub.ptr.rhs.cast288 = ptrtoint i8* %194 to i32
  %sub.ptr.sub289 = sub i32 %sub.ptr.lhs.cast287, %sub.ptr.rhs.cast288
  %cmp290 = icmp ule i32 %192, %sub.ptr.sub289
  br i1 %cmp290, label %if.then292, label %if.else299

if.then292:                                       ; preds = %if.end286
  %195 = load i8*, i8** %op, align 4, !tbaa !7
  %196 = load i8*, i8** %dictEnd, align 4, !tbaa !7
  %197 = load i8*, i8** %lowPrefix.addr, align 4, !tbaa !7
  %198 = load i8*, i8** %match, align 4, !tbaa !7
  %sub.ptr.lhs.cast293 = ptrtoint i8* %197 to i32
  %sub.ptr.rhs.cast294 = ptrtoint i8* %198 to i32
  %sub.ptr.sub295 = sub i32 %sub.ptr.lhs.cast293, %sub.ptr.rhs.cast294
  %idx.neg296 = sub i32 0, %sub.ptr.sub295
  %add.ptr297 = getelementptr inbounds i8, i8* %196, i32 %idx.neg296
  %199 = load i32, i32* %length, align 4, !tbaa !10
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %195, i8* align 1 %add.ptr297, i32 %199, i1 false)
  %200 = load i32, i32* %length, align 4, !tbaa !10
  %201 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr298 = getelementptr inbounds i8, i8* %201, i32 %200
  store i8* %add.ptr298, i8** %op, align 4, !tbaa !7
  br label %if.end322

if.else299:                                       ; preds = %if.end286
  %202 = bitcast i32* %copySize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %202) #7
  %203 = load i8*, i8** %lowPrefix.addr, align 4, !tbaa !7
  %204 = load i8*, i8** %match, align 4, !tbaa !7
  %sub.ptr.lhs.cast300 = ptrtoint i8* %203 to i32
  %sub.ptr.rhs.cast301 = ptrtoint i8* %204 to i32
  %sub.ptr.sub302 = sub i32 %sub.ptr.lhs.cast300, %sub.ptr.rhs.cast301
  store i32 %sub.ptr.sub302, i32* %copySize, align 4, !tbaa !10
  %205 = bitcast i32* %restSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %205) #7
  %206 = load i32, i32* %length, align 4, !tbaa !10
  %207 = load i32, i32* %copySize, align 4, !tbaa !10
  %sub = sub i32 %206, %207
  store i32 %sub, i32* %restSize, align 4, !tbaa !10
  %208 = load i8*, i8** %op, align 4, !tbaa !7
  %209 = load i8*, i8** %dictEnd, align 4, !tbaa !7
  %210 = load i32, i32* %copySize, align 4, !tbaa !10
  %idx.neg303 = sub i32 0, %210
  %add.ptr304 = getelementptr inbounds i8, i8* %209, i32 %idx.neg303
  %211 = load i32, i32* %copySize, align 4, !tbaa !10
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %208, i8* align 1 %add.ptr304, i32 %211, i1 false)
  %212 = load i32, i32* %copySize, align 4, !tbaa !10
  %213 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr305 = getelementptr inbounds i8, i8* %213, i32 %212
  store i8* %add.ptr305, i8** %op, align 4, !tbaa !7
  %214 = load i32, i32* %restSize, align 4, !tbaa !10
  %215 = load i8*, i8** %op, align 4, !tbaa !7
  %216 = load i8*, i8** %lowPrefix.addr, align 4, !tbaa !7
  %sub.ptr.lhs.cast306 = ptrtoint i8* %215 to i32
  %sub.ptr.rhs.cast307 = ptrtoint i8* %216 to i32
  %sub.ptr.sub308 = sub i32 %sub.ptr.lhs.cast306, %sub.ptr.rhs.cast307
  %cmp309 = icmp ugt i32 %214, %sub.ptr.sub308
  br i1 %cmp309, label %if.then311, label %if.else319

if.then311:                                       ; preds = %if.else299
  %217 = bitcast i8** %endOfMatch to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %217) #7
  %218 = load i8*, i8** %op, align 4, !tbaa !7
  %219 = load i32, i32* %restSize, align 4, !tbaa !10
  %add.ptr312 = getelementptr inbounds i8, i8* %218, i32 %219
  store i8* %add.ptr312, i8** %endOfMatch, align 4, !tbaa !7
  %220 = bitcast i8** %copyFrom to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %220) #7
  %221 = load i8*, i8** %lowPrefix.addr, align 4, !tbaa !7
  store i8* %221, i8** %copyFrom, align 4, !tbaa !7
  br label %while.cond313

while.cond313:                                    ; preds = %while.body316, %if.then311
  %222 = load i8*, i8** %op, align 4, !tbaa !7
  %223 = load i8*, i8** %endOfMatch, align 4, !tbaa !7
  %cmp314 = icmp ult i8* %222, %223
  br i1 %cmp314, label %while.body316, label %while.end

while.body316:                                    ; preds = %while.cond313
  %224 = load i8*, i8** %copyFrom, align 4, !tbaa !7
  %incdec.ptr317 = getelementptr inbounds i8, i8* %224, i32 1
  store i8* %incdec.ptr317, i8** %copyFrom, align 4, !tbaa !7
  %225 = load i8, i8* %224, align 1, !tbaa !9
  %226 = load i8*, i8** %op, align 4, !tbaa !7
  %incdec.ptr318 = getelementptr inbounds i8, i8* %226, i32 1
  store i8* %incdec.ptr318, i8** %op, align 4, !tbaa !7
  store i8 %225, i8* %226, align 1, !tbaa !9
  br label %while.cond313

while.end:                                        ; preds = %while.cond313
  %227 = bitcast i8** %copyFrom to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %227) #7
  %228 = bitcast i8** %endOfMatch to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %228) #7
  br label %if.end321

if.else319:                                       ; preds = %if.else299
  %229 = load i8*, i8** %op, align 4, !tbaa !7
  %230 = load i8*, i8** %lowPrefix.addr, align 4, !tbaa !7
  %231 = load i32, i32* %restSize, align 4, !tbaa !10
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %229, i8* align 1 %230, i32 %231, i1 false)
  %232 = load i32, i32* %restSize, align 4, !tbaa !10
  %233 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr320 = getelementptr inbounds i8, i8* %233, i32 %232
  store i8* %add.ptr320, i8** %op, align 4, !tbaa !7
  br label %if.end321

if.end321:                                        ; preds = %if.else319, %while.end
  %234 = bitcast i32* %restSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %234) #7
  %235 = bitcast i32* %copySize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %235) #7
  br label %if.end322

if.end322:                                        ; preds = %if.end321, %if.then292
  br label %while.cond

if.end323:                                        ; preds = %land.lhs.true257, %if.end254
  %236 = load i8*, i8** %op, align 4, !tbaa !7
  %237 = load i32, i32* %length, align 4, !tbaa !10
  %add.ptr324 = getelementptr inbounds i8, i8* %236, i32 %237
  store i8* %add.ptr324, i8** %cpy, align 4, !tbaa !7
  %238 = load i32, i32* %partialDecoding.addr, align 4, !tbaa !9
  %tobool325 = icmp ne i32 %238, 0
  br i1 %tobool325, label %land.lhs.true326, label %if.end365

land.lhs.true326:                                 ; preds = %if.end323
  %239 = load i8*, i8** %cpy, align 4, !tbaa !7
  %240 = load i8*, i8** %oend, align 4, !tbaa !7
  %add.ptr327 = getelementptr inbounds i8, i8* %240, i32 -12
  %cmp328 = icmp ugt i8* %239, %add.ptr327
  br i1 %cmp328, label %if.then330, label %if.end365

if.then330:                                       ; preds = %land.lhs.true326
  %241 = bitcast i32* %mlen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %241) #7
  %242 = load i32, i32* %length, align 4, !tbaa !10
  %243 = load i8*, i8** %oend, align 4, !tbaa !7
  %244 = load i8*, i8** %op, align 4, !tbaa !7
  %sub.ptr.lhs.cast331 = ptrtoint i8* %243 to i32
  %sub.ptr.rhs.cast332 = ptrtoint i8* %244 to i32
  %sub.ptr.sub333 = sub i32 %sub.ptr.lhs.cast331, %sub.ptr.rhs.cast332
  %cmp334 = icmp ult i32 %242, %sub.ptr.sub333
  br i1 %cmp334, label %cond.true336, label %cond.false337

cond.true336:                                     ; preds = %if.then330
  %245 = load i32, i32* %length, align 4, !tbaa !10
  br label %cond.end341

cond.false337:                                    ; preds = %if.then330
  %246 = load i8*, i8** %oend, align 4, !tbaa !7
  %247 = load i8*, i8** %op, align 4, !tbaa !7
  %sub.ptr.lhs.cast338 = ptrtoint i8* %246 to i32
  %sub.ptr.rhs.cast339 = ptrtoint i8* %247 to i32
  %sub.ptr.sub340 = sub i32 %sub.ptr.lhs.cast338, %sub.ptr.rhs.cast339
  br label %cond.end341

cond.end341:                                      ; preds = %cond.false337, %cond.true336
  %cond342 = phi i32 [ %245, %cond.true336 ], [ %sub.ptr.sub340, %cond.false337 ]
  store i32 %cond342, i32* %mlen, align 4, !tbaa !10
  %248 = bitcast i8** %matchEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %248) #7
  %249 = load i8*, i8** %match, align 4, !tbaa !7
  %250 = load i32, i32* %mlen, align 4, !tbaa !10
  %add.ptr343 = getelementptr inbounds i8, i8* %249, i32 %250
  store i8* %add.ptr343, i8** %matchEnd, align 4, !tbaa !7
  %251 = bitcast i8** %copyEnd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %251) #7
  %252 = load i8*, i8** %op, align 4, !tbaa !7
  %253 = load i32, i32* %mlen, align 4, !tbaa !10
  %add.ptr344 = getelementptr inbounds i8, i8* %252, i32 %253
  store i8* %add.ptr344, i8** %copyEnd, align 4, !tbaa !7
  %254 = load i8*, i8** %matchEnd, align 4, !tbaa !7
  %255 = load i8*, i8** %op, align 4, !tbaa !7
  %cmp345 = icmp ugt i8* %254, %255
  br i1 %cmp345, label %if.then347, label %if.else355

if.then347:                                       ; preds = %cond.end341
  br label %while.cond348

while.cond348:                                    ; preds = %while.body351, %if.then347
  %256 = load i8*, i8** %op, align 4, !tbaa !7
  %257 = load i8*, i8** %copyEnd, align 4, !tbaa !7
  %cmp349 = icmp ult i8* %256, %257
  br i1 %cmp349, label %while.body351, label %while.end354

while.body351:                                    ; preds = %while.cond348
  %258 = load i8*, i8** %match, align 4, !tbaa !7
  %incdec.ptr352 = getelementptr inbounds i8, i8* %258, i32 1
  store i8* %incdec.ptr352, i8** %match, align 4, !tbaa !7
  %259 = load i8, i8* %258, align 1, !tbaa !9
  %260 = load i8*, i8** %op, align 4, !tbaa !7
  %incdec.ptr353 = getelementptr inbounds i8, i8* %260, i32 1
  store i8* %incdec.ptr353, i8** %op, align 4, !tbaa !7
  store i8 %259, i8* %260, align 1, !tbaa !9
  br label %while.cond348

while.end354:                                     ; preds = %while.cond348
  br label %if.end356

if.else355:                                       ; preds = %cond.end341
  %261 = load i8*, i8** %op, align 4, !tbaa !7
  %262 = load i8*, i8** %match, align 4, !tbaa !7
  %263 = load i32, i32* %mlen, align 4, !tbaa !10
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %261, i8* align 1 %262, i32 %263, i1 false)
  br label %if.end356

if.end356:                                        ; preds = %if.else355, %while.end354
  %264 = load i8*, i8** %copyEnd, align 4, !tbaa !7
  store i8* %264, i8** %op, align 4, !tbaa !7
  %265 = load i8*, i8** %op, align 4, !tbaa !7
  %266 = load i8*, i8** %oend, align 4, !tbaa !7
  %cmp357 = icmp eq i8* %265, %266
  br i1 %cmp357, label %if.then359, label %if.end360

if.then359:                                       ; preds = %if.end356
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup361

if.end360:                                        ; preds = %if.end356
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup361

cleanup361:                                       ; preds = %if.end360, %if.then359
  %267 = bitcast i8** %copyEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %267) #7
  %268 = bitcast i8** %matchEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %268) #7
  %269 = bitcast i32* %mlen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %269) #7
  %cleanup.dest364 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest364, label %unreachable [
    i32 3, label %while.end430
    i32 2, label %while.cond
  ]

if.end365:                                        ; preds = %land.lhs.true326, %if.end323
  %270 = load i32, i32* %offset, align 4, !tbaa !10
  %cmp366 = icmp ult i32 %270, 8
  %conv367 = zext i1 %cmp366 to i32
  %cmp368 = icmp ne i32 %conv367, 0
  %conv369 = zext i1 %cmp368 to i32
  %expval370 = call i32 @llvm.expect.i32(i32 %conv369, i32 0)
  %tobool371 = icmp ne i32 %expval370, 0
  br i1 %tobool371, label %if.then372, label %if.else386

if.then372:                                       ; preds = %if.end365
  %271 = load i8*, i8** %op, align 4, !tbaa !7
  call void @LZ4_write32(i8* %271, i32 0)
  %272 = load i8*, i8** %match, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i8, i8* %272, i32 0
  %273 = load i8, i8* %arrayidx, align 1, !tbaa !9
  %274 = load i8*, i8** %op, align 4, !tbaa !7
  %arrayidx373 = getelementptr inbounds i8, i8* %274, i32 0
  store i8 %273, i8* %arrayidx373, align 1, !tbaa !9
  %275 = load i8*, i8** %match, align 4, !tbaa !7
  %arrayidx374 = getelementptr inbounds i8, i8* %275, i32 1
  %276 = load i8, i8* %arrayidx374, align 1, !tbaa !9
  %277 = load i8*, i8** %op, align 4, !tbaa !7
  %arrayidx375 = getelementptr inbounds i8, i8* %277, i32 1
  store i8 %276, i8* %arrayidx375, align 1, !tbaa !9
  %278 = load i8*, i8** %match, align 4, !tbaa !7
  %arrayidx376 = getelementptr inbounds i8, i8* %278, i32 2
  %279 = load i8, i8* %arrayidx376, align 1, !tbaa !9
  %280 = load i8*, i8** %op, align 4, !tbaa !7
  %arrayidx377 = getelementptr inbounds i8, i8* %280, i32 2
  store i8 %279, i8* %arrayidx377, align 1, !tbaa !9
  %281 = load i8*, i8** %match, align 4, !tbaa !7
  %arrayidx378 = getelementptr inbounds i8, i8* %281, i32 3
  %282 = load i8, i8* %arrayidx378, align 1, !tbaa !9
  %283 = load i8*, i8** %op, align 4, !tbaa !7
  %arrayidx379 = getelementptr inbounds i8, i8* %283, i32 3
  store i8 %282, i8* %arrayidx379, align 1, !tbaa !9
  %284 = load i32, i32* %offset, align 4, !tbaa !10
  %arrayidx380 = getelementptr inbounds [8 x i32], [8 x i32]* @inc32table, i32 0, i32 %284
  %285 = load i32, i32* %arrayidx380, align 4, !tbaa !3
  %286 = load i8*, i8** %match, align 4, !tbaa !7
  %add.ptr381 = getelementptr inbounds i8, i8* %286, i32 %285
  store i8* %add.ptr381, i8** %match, align 4, !tbaa !7
  %287 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr382 = getelementptr inbounds i8, i8* %287, i32 4
  %288 = load i8*, i8** %match, align 4, !tbaa !7
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr382, i8* align 1 %288, i32 4, i1 false)
  %289 = load i32, i32* %offset, align 4, !tbaa !10
  %arrayidx383 = getelementptr inbounds [8 x i32], [8 x i32]* @dec64table, i32 0, i32 %289
  %290 = load i32, i32* %arrayidx383, align 4, !tbaa !3
  %291 = load i8*, i8** %match, align 4, !tbaa !7
  %idx.neg384 = sub i32 0, %290
  %add.ptr385 = getelementptr inbounds i8, i8* %291, i32 %idx.neg384
  store i8* %add.ptr385, i8** %match, align 4, !tbaa !7
  br label %if.end388

if.else386:                                       ; preds = %if.end365
  %292 = load i8*, i8** %op, align 4, !tbaa !7
  %293 = load i8*, i8** %match, align 4, !tbaa !7
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %292, i8* align 1 %293, i32 8, i1 false)
  %294 = load i8*, i8** %match, align 4, !tbaa !7
  %add.ptr387 = getelementptr inbounds i8, i8* %294, i32 8
  store i8* %add.ptr387, i8** %match, align 4, !tbaa !7
  br label %if.end388

if.end388:                                        ; preds = %if.else386, %if.then372
  %295 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr389 = getelementptr inbounds i8, i8* %295, i32 8
  store i8* %add.ptr389, i8** %op, align 4, !tbaa !7
  %296 = load i8*, i8** %cpy, align 4, !tbaa !7
  %297 = load i8*, i8** %oend, align 4, !tbaa !7
  %add.ptr390 = getelementptr inbounds i8, i8* %297, i32 -12
  %cmp391 = icmp ugt i8* %296, %add.ptr390
  %conv392 = zext i1 %cmp391 to i32
  %cmp393 = icmp ne i32 %conv392, 0
  %conv394 = zext i1 %cmp393 to i32
  %expval395 = call i32 @llvm.expect.i32(i32 %conv394, i32 0)
  %tobool396 = icmp ne i32 %expval395, 0
  br i1 %tobool396, label %if.then397, label %if.else422

if.then397:                                       ; preds = %if.end388
  %298 = bitcast i8** %oCopyLimit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %298) #7
  %299 = load i8*, i8** %oend, align 4, !tbaa !7
  %add.ptr398 = getelementptr inbounds i8, i8* %299, i32 -7
  store i8* %add.ptr398, i8** %oCopyLimit, align 4, !tbaa !7
  %300 = load i8*, i8** %cpy, align 4, !tbaa !7
  %301 = load i8*, i8** %oend, align 4, !tbaa !7
  %add.ptr399 = getelementptr inbounds i8, i8* %301, i32 -5
  %cmp400 = icmp ugt i8* %300, %add.ptr399
  br i1 %cmp400, label %if.then402, label %if.end403

if.then402:                                       ; preds = %if.then397
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup419

if.end403:                                        ; preds = %if.then397
  %302 = load i8*, i8** %op, align 4, !tbaa !7
  %303 = load i8*, i8** %oCopyLimit, align 4, !tbaa !7
  %cmp404 = icmp ult i8* %302, %303
  br i1 %cmp404, label %if.then406, label %if.end411

if.then406:                                       ; preds = %if.end403
  %304 = load i8*, i8** %op, align 4, !tbaa !7
  %305 = load i8*, i8** %match, align 4, !tbaa !7
  %306 = load i8*, i8** %oCopyLimit, align 4, !tbaa !7
  call void @LZ4_wildCopy8(i8* %304, i8* %305, i8* %306)
  %307 = load i8*, i8** %oCopyLimit, align 4, !tbaa !7
  %308 = load i8*, i8** %op, align 4, !tbaa !7
  %sub.ptr.lhs.cast407 = ptrtoint i8* %307 to i32
  %sub.ptr.rhs.cast408 = ptrtoint i8* %308 to i32
  %sub.ptr.sub409 = sub i32 %sub.ptr.lhs.cast407, %sub.ptr.rhs.cast408
  %309 = load i8*, i8** %match, align 4, !tbaa !7
  %add.ptr410 = getelementptr inbounds i8, i8* %309, i32 %sub.ptr.sub409
  store i8* %add.ptr410, i8** %match, align 4, !tbaa !7
  %310 = load i8*, i8** %oCopyLimit, align 4, !tbaa !7
  store i8* %310, i8** %op, align 4, !tbaa !7
  br label %if.end411

if.end411:                                        ; preds = %if.then406, %if.end403
  br label %while.cond412

while.cond412:                                    ; preds = %while.body415, %if.end411
  %311 = load i8*, i8** %op, align 4, !tbaa !7
  %312 = load i8*, i8** %cpy, align 4, !tbaa !7
  %cmp413 = icmp ult i8* %311, %312
  br i1 %cmp413, label %while.body415, label %while.end418

while.body415:                                    ; preds = %while.cond412
  %313 = load i8*, i8** %match, align 4, !tbaa !7
  %incdec.ptr416 = getelementptr inbounds i8, i8* %313, i32 1
  store i8* %incdec.ptr416, i8** %match, align 4, !tbaa !7
  %314 = load i8, i8* %313, align 1, !tbaa !9
  %315 = load i8*, i8** %op, align 4, !tbaa !7
  %incdec.ptr417 = getelementptr inbounds i8, i8* %315, i32 1
  store i8* %incdec.ptr417, i8** %op, align 4, !tbaa !7
  store i8 %314, i8* %315, align 1, !tbaa !9
  br label %while.cond412

while.end418:                                     ; preds = %while.cond412
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup419

cleanup419:                                       ; preds = %if.then402, %while.end418
  %316 = bitcast i8** %oCopyLimit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %316) #7
  %cleanup.dest420 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest420, label %cleanup445 [
    i32 0, label %cleanup.cont421
    i32 5, label %_output_error
  ]

cleanup.cont421:                                  ; preds = %cleanup419
  br label %if.end429

if.else422:                                       ; preds = %if.end388
  %317 = load i8*, i8** %op, align 4, !tbaa !7
  %318 = load i8*, i8** %match, align 4, !tbaa !7
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %317, i8* align 1 %318, i32 8, i1 false)
  %319 = load i32, i32* %length, align 4, !tbaa !10
  %cmp423 = icmp ugt i32 %319, 16
  br i1 %cmp423, label %if.then425, label %if.end428

if.then425:                                       ; preds = %if.else422
  %320 = load i8*, i8** %op, align 4, !tbaa !7
  %add.ptr426 = getelementptr inbounds i8, i8* %320, i32 8
  %321 = load i8*, i8** %match, align 4, !tbaa !7
  %add.ptr427 = getelementptr inbounds i8, i8* %321, i32 8
  %322 = load i8*, i8** %cpy, align 4, !tbaa !7
  call void @LZ4_wildCopy8(i8* %add.ptr426, i8* %add.ptr427, i8* %322)
  br label %if.end428

if.end428:                                        ; preds = %if.then425, %if.else422
  br label %if.end429

if.end429:                                        ; preds = %if.end428, %cleanup.cont421
  %323 = load i8*, i8** %cpy, align 4, !tbaa !7
  store i8* %323, i8** %op, align 4, !tbaa !7
  br label %while.cond

while.end430:                                     ; preds = %cleanup361, %if.then205
  %324 = load i32, i32* %endOnInput.addr, align 4, !tbaa !9
  %tobool431 = icmp ne i32 %324, 0
  br i1 %tobool431, label %if.then432, label %if.else436

if.then432:                                       ; preds = %while.end430
  %325 = load i8*, i8** %op, align 4, !tbaa !7
  %326 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %sub.ptr.lhs.cast433 = ptrtoint i8* %325 to i32
  %sub.ptr.rhs.cast434 = ptrtoint i8* %326 to i32
  %sub.ptr.sub435 = sub i32 %sub.ptr.lhs.cast433, %sub.ptr.rhs.cast434
  store i32 %sub.ptr.sub435, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup445

if.else436:                                       ; preds = %while.end430
  %327 = load i8*, i8** %ip, align 4, !tbaa !7
  %328 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %sub.ptr.lhs.cast437 = ptrtoint i8* %327 to i32
  %sub.ptr.rhs.cast438 = ptrtoint i8* %328 to i32
  %sub.ptr.sub439 = sub i32 %sub.ptr.lhs.cast437, %sub.ptr.rhs.cast438
  store i32 %sub.ptr.sub439, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup445

_output_error:                                    ; preds = %cleanup419, %cleanup239, %cleanup, %if.else284, %if.then253, %if.then193, %if.then183, %if.then173
  %329 = load i8*, i8** %ip, align 4, !tbaa !7
  %330 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %sub.ptr.lhs.cast440 = ptrtoint i8* %329 to i32
  %sub.ptr.rhs.cast441 = ptrtoint i8* %330 to i32
  %sub.ptr.sub442 = sub i32 %sub.ptr.lhs.cast440, %sub.ptr.rhs.cast441
  %sub443 = sub nsw i32 0, %sub.ptr.sub442
  %sub444 = sub nsw i32 %sub443, 1
  store i32 %sub444, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup445

cleanup445:                                       ; preds = %_output_error, %if.else436, %if.then432, %cleanup419, %cleanup239, %cleanup, %if.then58, %if.then44, %land.end32, %if.then24
  %331 = bitcast i32* %length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %331) #7
  %332 = bitcast i32* %token to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %332) #7
  %333 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %333) #7
  %334 = bitcast i8** %match to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %334) #7
  %335 = bitcast i8** %shortoend to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %335) #7
  %336 = bitcast i8** %shortiend to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %336) #7
  %337 = bitcast i32* %checkOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %337) #7
  %338 = bitcast i32* %safeDecode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %338) #7
  %339 = bitcast i8** %dictEnd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %339) #7
  %340 = bitcast i8** %cpy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %340) #7
  %341 = bitcast i8** %oend to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %341) #7
  %342 = bitcast i8** %op to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %342) #7
  %343 = bitcast i8** %iend to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %343) #7
  %344 = bitcast i8** %ip to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %344) #7
  br label %return

return:                                           ; preds = %cleanup445, %if.then
  %345 = load i32, i32* %retval, align 4
  ret i32 %345

unreachable:                                      ; preds = %cleanup361
  unreachable
}

; Function Attrs: nounwind
define i32 @LZ4_decompress_safe_partial(i8* %src, i8* %dst, i32 %compressedSize, i32 %targetOutputSize, i32 %dstCapacity) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %compressedSize.addr = alloca i32, align 4
  %targetOutputSize.addr = alloca i32, align 4
  %dstCapacity.addr = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !7
  store i32 %compressedSize, i32* %compressedSize.addr, align 4, !tbaa !3
  store i32 %targetOutputSize, i32* %targetOutputSize.addr, align 4, !tbaa !3
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !3
  %0 = load i32, i32* %targetOutputSize.addr, align 4, !tbaa !3
  %1 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %targetOutputSize.addr, align 4, !tbaa !3
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %2, %cond.true ], [ %3, %cond.false ]
  store i32 %cond, i32* %dstCapacity.addr, align 4, !tbaa !3
  %4 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %5 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %6 = load i32, i32* %compressedSize.addr, align 4, !tbaa !3
  %7 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %8 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %call = call i32 @LZ4_decompress_generic(i8* %4, i8* %5, i32 %6, i32 %7, i32 1, i32 1, i32 0, i8* %8, i8* null, i32 0)
  ret i32 %call
}

; Function Attrs: nounwind
define i32 @LZ4_decompress_fast(i8* %source, i8* %dest, i32 %originalSize) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %originalSize.addr = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %originalSize, i32* %originalSize.addr, align 4, !tbaa !3
  %0 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %2 = load i32, i32* %originalSize.addr, align 4, !tbaa !3
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 -65536
  %call = call i32 @LZ4_decompress_generic(i8* %0, i8* %1, i32 0, i32 %2, i32 0, i32 0, i32 1, i8* %add.ptr, i8* null, i32 0)
  ret i32 %call
}

; Function Attrs: nounwind
define i32 @LZ4_decompress_safe_withPrefix64k(i8* %source, i8* %dest, i32 %compressedSize, i32 %maxOutputSize) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %compressedSize.addr = alloca i32, align 4
  %maxOutputSize.addr = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %compressedSize, i32* %compressedSize.addr, align 4, !tbaa !3
  store i32 %maxOutputSize, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %0 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %2 = load i32, i32* %compressedSize.addr, align 4, !tbaa !3
  %3 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %4 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 -65536
  %call = call i32 @LZ4_decompress_generic(i8* %0, i8* %1, i32 %2, i32 %3, i32 1, i32 0, i32 1, i8* %add.ptr, i8* null, i32 0)
  ret i32 %call
}

; Function Attrs: nounwind
define i32 @LZ4_decompress_fast_withPrefix64k(i8* %source, i8* %dest, i32 %originalSize) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %originalSize.addr = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %originalSize, i32* %originalSize.addr, align 4, !tbaa !3
  %0 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %2 = load i32, i32* %originalSize.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_decompress_fast(i8* %0, i8* %1, i32 %2)
  ret i32 %call
}

; Function Attrs: nounwind
define hidden i32 @LZ4_decompress_safe_forceExtDict(i8* %source, i8* %dest, i32 %compressedSize, i32 %maxOutputSize, i8* %dictStart, i32 %dictSize) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %compressedSize.addr = alloca i32, align 4
  %maxOutputSize.addr = alloca i32, align 4
  %dictStart.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %compressedSize, i32* %compressedSize.addr, align 4, !tbaa !3
  store i32 %maxOutputSize, i32* %maxOutputSize.addr, align 4, !tbaa !3
  store i8* %dictStart, i8** %dictStart.addr, align 4, !tbaa !7
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !10
  %0 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %2 = load i32, i32* %compressedSize.addr, align 4, !tbaa !3
  %3 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %4 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %5 = load i8*, i8** %dictStart.addr, align 4, !tbaa !7
  %6 = load i32, i32* %dictSize.addr, align 4, !tbaa !10
  %call = call i32 @LZ4_decompress_generic(i8* %0, i8* %1, i32 %2, i32 %3, i32 1, i32 0, i32 2, i8* %4, i8* %5, i32 %6)
  ret i32 %call
}

; Function Attrs: nounwind
define %union.LZ4_streamDecode_u* @LZ4_createStreamDecode() #0 {
entry:
  %lz4s = alloca %union.LZ4_streamDecode_u*, align 4
  %0 = bitcast %union.LZ4_streamDecode_u** %lz4s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i8* @calloc(i32 1, i32 32)
  %1 = bitcast i8* %call to %union.LZ4_streamDecode_u*
  store %union.LZ4_streamDecode_u* %1, %union.LZ4_streamDecode_u** %lz4s, align 4, !tbaa !7
  %2 = load %union.LZ4_streamDecode_u*, %union.LZ4_streamDecode_u** %lz4s, align 4, !tbaa !7
  %3 = bitcast %union.LZ4_streamDecode_u** %lz4s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #7
  ret %union.LZ4_streamDecode_u* %2
}

declare i8* @calloc(i32, i32) #3

; Function Attrs: nounwind
define i32 @LZ4_freeStreamDecode(%union.LZ4_streamDecode_u* %LZ4_stream) #0 {
entry:
  %retval = alloca i32, align 4
  %LZ4_stream.addr = alloca %union.LZ4_streamDecode_u*, align 4
  store %union.LZ4_streamDecode_u* %LZ4_stream, %union.LZ4_streamDecode_u** %LZ4_stream.addr, align 4, !tbaa !7
  %0 = load %union.LZ4_streamDecode_u*, %union.LZ4_streamDecode_u** %LZ4_stream.addr, align 4, !tbaa !7
  %cmp = icmp eq %union.LZ4_streamDecode_u* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %union.LZ4_streamDecode_u*, %union.LZ4_streamDecode_u** %LZ4_stream.addr, align 4, !tbaa !7
  %2 = bitcast %union.LZ4_streamDecode_u* %1 to i8*
  call void @free(i8* %2)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: nounwind
define i32 @LZ4_setStreamDecode(%union.LZ4_streamDecode_u* %LZ4_streamDecode, i8* %dictionary, i32 %dictSize) #0 {
entry:
  %LZ4_streamDecode.addr = alloca %union.LZ4_streamDecode_u*, align 4
  %dictionary.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  %lz4sd = alloca %struct.LZ4_streamDecode_t_internal*, align 4
  store %union.LZ4_streamDecode_u* %LZ4_streamDecode, %union.LZ4_streamDecode_u** %LZ4_streamDecode.addr, align 4, !tbaa !7
  store i8* %dictionary, i8** %dictionary.addr, align 4, !tbaa !7
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !3
  %0 = bitcast %struct.LZ4_streamDecode_t_internal** %lz4sd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %union.LZ4_streamDecode_u*, %union.LZ4_streamDecode_u** %LZ4_streamDecode.addr, align 4, !tbaa !7
  %internal_donotuse = bitcast %union.LZ4_streamDecode_u* %1 to %struct.LZ4_streamDecode_t_internal*
  store %struct.LZ4_streamDecode_t_internal* %internal_donotuse, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %2 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %3 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixSize = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %3, i32 0, i32 3
  store i32 %2, i32* %prefixSize, align 4, !tbaa !20
  %4 = load i8*, i8** %dictionary.addr, align 4, !tbaa !7
  %5 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %5
  %6 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixEnd = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %6, i32 0, i32 2
  store i8* %add.ptr, i8** %prefixEnd, align 4, !tbaa !22
  %7 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %externalDict = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %7, i32 0, i32 0
  store i8* null, i8** %externalDict, align 4, !tbaa !23
  %8 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %extDictSize = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %8, i32 0, i32 1
  store i32 0, i32* %extDictSize, align 4, !tbaa !24
  %9 = bitcast %struct.LZ4_streamDecode_t_internal** %lz4sd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #7
  ret i32 1
}

; Function Attrs: nounwind
define i32 @LZ4_decoderRingBufferSize(i32 %maxBlockSize) #0 {
entry:
  %retval = alloca i32, align 4
  %maxBlockSize.addr = alloca i32, align 4
  store i32 %maxBlockSize, i32* %maxBlockSize.addr, align 4, !tbaa !3
  %0 = load i32, i32* %maxBlockSize.addr, align 4, !tbaa !3
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %maxBlockSize.addr, align 4, !tbaa !3
  %cmp1 = icmp sgt i32 %1, 2113929216
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %2 = load i32, i32* %maxBlockSize.addr, align 4, !tbaa !3
  %cmp4 = icmp slt i32 %2, 16
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i32 16, i32* %maxBlockSize.addr, align 4, !tbaa !3
  br label %if.end6

if.end6:                                          ; preds = %if.then5, %if.end3
  %3 = load i32, i32* %maxBlockSize.addr, align 4, !tbaa !3
  %add = add nsw i32 65550, %3
  store i32 %add, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end6, %if.then2, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: nounwind
define i32 @LZ4_decompress_safe_continue(%union.LZ4_streamDecode_u* %LZ4_streamDecode, i8* %source, i8* %dest, i32 %compressedSize, i32 %maxOutputSize) #0 {
entry:
  %retval = alloca i32, align 4
  %LZ4_streamDecode.addr = alloca %union.LZ4_streamDecode_u*, align 4
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %compressedSize.addr = alloca i32, align 4
  %maxOutputSize.addr = alloca i32, align 4
  %lz4sd = alloca %struct.LZ4_streamDecode_t_internal*, align 4
  %result = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %union.LZ4_streamDecode_u* %LZ4_streamDecode, %union.LZ4_streamDecode_u** %LZ4_streamDecode.addr, align 4, !tbaa !7
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %compressedSize, i32* %compressedSize.addr, align 4, !tbaa !3
  store i32 %maxOutputSize, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %0 = bitcast %struct.LZ4_streamDecode_t_internal** %lz4sd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %union.LZ4_streamDecode_u*, %union.LZ4_streamDecode_u** %LZ4_streamDecode.addr, align 4, !tbaa !7
  %internal_donotuse = bitcast %union.LZ4_streamDecode_u* %1 to %struct.LZ4_streamDecode_t_internal*
  store %struct.LZ4_streamDecode_t_internal* %internal_donotuse, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %2 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixSize = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %3, i32 0, i32 3
  %4 = load i32, i32* %prefixSize, align 4, !tbaa !20
  %cmp = icmp eq i32 %4, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %6 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %7 = load i32, i32* %compressedSize.addr, align 4, !tbaa !3
  %8 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_decompress_safe(i8* %5, i8* %6, i32 %7, i32 %8)
  store i32 %call, i32* %result, align 4, !tbaa !3
  %9 = load i32, i32* %result, align 4, !tbaa !3
  %cmp1 = icmp sle i32 %9, 0
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %10 = load i32, i32* %result, align 4, !tbaa !3
  store i32 %10, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %11 = load i32, i32* %result, align 4, !tbaa !3
  %12 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixSize3 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %12, i32 0, i32 3
  store i32 %11, i32* %prefixSize3, align 4, !tbaa !20
  %13 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %14 = load i32, i32* %result, align 4, !tbaa !3
  %add.ptr = getelementptr inbounds i8, i8* %13, i32 %14
  %15 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixEnd = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %15, i32 0, i32 2
  store i8* %add.ptr, i8** %prefixEnd, align 4, !tbaa !22
  br label %if.end45

if.else:                                          ; preds = %entry
  %16 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixEnd4 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %16, i32 0, i32 2
  %17 = load i8*, i8** %prefixEnd4, align 4, !tbaa !22
  %18 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %cmp5 = icmp eq i8* %17, %18
  br i1 %cmp5, label %if.then6, label %if.else28

if.then6:                                         ; preds = %if.else
  %19 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixSize7 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %19, i32 0, i32 3
  %20 = load i32, i32* %prefixSize7, align 4, !tbaa !20
  %cmp8 = icmp uge i32 %20, 65535
  br i1 %cmp8, label %if.then9, label %if.else11

if.then9:                                         ; preds = %if.then6
  %21 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %22 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %23 = load i32, i32* %compressedSize.addr, align 4, !tbaa !3
  %24 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %call10 = call i32 @LZ4_decompress_safe_withPrefix64k(i8* %21, i8* %22, i32 %23, i32 %24)
  store i32 %call10, i32* %result, align 4, !tbaa !3
  br label %if.end21

if.else11:                                        ; preds = %if.then6
  %25 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %extDictSize = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %25, i32 0, i32 1
  %26 = load i32, i32* %extDictSize, align 4, !tbaa !24
  %cmp12 = icmp eq i32 %26, 0
  br i1 %cmp12, label %if.then13, label %if.else16

if.then13:                                        ; preds = %if.else11
  %27 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %28 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %29 = load i32, i32* %compressedSize.addr, align 4, !tbaa !3
  %30 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %31 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixSize14 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %31, i32 0, i32 3
  %32 = load i32, i32* %prefixSize14, align 4, !tbaa !20
  %call15 = call i32 @LZ4_decompress_safe_withSmallPrefix(i8* %27, i8* %28, i32 %29, i32 %30, i32 %32)
  store i32 %call15, i32* %result, align 4, !tbaa !3
  br label %if.end20

if.else16:                                        ; preds = %if.else11
  %33 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %34 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %35 = load i32, i32* %compressedSize.addr, align 4, !tbaa !3
  %36 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %37 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixSize17 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %37, i32 0, i32 3
  %38 = load i32, i32* %prefixSize17, align 4, !tbaa !20
  %39 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %externalDict = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %39, i32 0, i32 0
  %40 = load i8*, i8** %externalDict, align 4, !tbaa !23
  %41 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %extDictSize18 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %41, i32 0, i32 1
  %42 = load i32, i32* %extDictSize18, align 4, !tbaa !24
  %call19 = call i32 @LZ4_decompress_safe_doubleDict(i8* %33, i8* %34, i32 %35, i32 %36, i32 %38, i8* %40, i32 %42)
  store i32 %call19, i32* %result, align 4, !tbaa !3
  br label %if.end20

if.end20:                                         ; preds = %if.else16, %if.then13
  br label %if.end21

if.end21:                                         ; preds = %if.end20, %if.then9
  %43 = load i32, i32* %result, align 4, !tbaa !3
  %cmp22 = icmp sle i32 %43, 0
  br i1 %cmp22, label %if.then23, label %if.end24

if.then23:                                        ; preds = %if.end21
  %44 = load i32, i32* %result, align 4, !tbaa !3
  store i32 %44, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end24:                                         ; preds = %if.end21
  %45 = load i32, i32* %result, align 4, !tbaa !3
  %46 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixSize25 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %46, i32 0, i32 3
  %47 = load i32, i32* %prefixSize25, align 4, !tbaa !20
  %add = add i32 %47, %45
  store i32 %add, i32* %prefixSize25, align 4, !tbaa !20
  %48 = load i32, i32* %result, align 4, !tbaa !3
  %49 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixEnd26 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %49, i32 0, i32 2
  %50 = load i8*, i8** %prefixEnd26, align 4, !tbaa !22
  %add.ptr27 = getelementptr inbounds i8, i8* %50, i32 %48
  store i8* %add.ptr27, i8** %prefixEnd26, align 4, !tbaa !22
  br label %if.end44

if.else28:                                        ; preds = %if.else
  %51 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixSize29 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %51, i32 0, i32 3
  %52 = load i32, i32* %prefixSize29, align 4, !tbaa !20
  %53 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %extDictSize30 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %53, i32 0, i32 1
  store i32 %52, i32* %extDictSize30, align 4, !tbaa !24
  %54 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixEnd31 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %54, i32 0, i32 2
  %55 = load i8*, i8** %prefixEnd31, align 4, !tbaa !22
  %56 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %extDictSize32 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %56, i32 0, i32 1
  %57 = load i32, i32* %extDictSize32, align 4, !tbaa !24
  %idx.neg = sub i32 0, %57
  %add.ptr33 = getelementptr inbounds i8, i8* %55, i32 %idx.neg
  %58 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %externalDict34 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %58, i32 0, i32 0
  store i8* %add.ptr33, i8** %externalDict34, align 4, !tbaa !23
  %59 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %60 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %61 = load i32, i32* %compressedSize.addr, align 4, !tbaa !3
  %62 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %63 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %externalDict35 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %63, i32 0, i32 0
  %64 = load i8*, i8** %externalDict35, align 4, !tbaa !23
  %65 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %extDictSize36 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %65, i32 0, i32 1
  %66 = load i32, i32* %extDictSize36, align 4, !tbaa !24
  %call37 = call i32 @LZ4_decompress_safe_forceExtDict(i8* %59, i8* %60, i32 %61, i32 %62, i8* %64, i32 %66)
  store i32 %call37, i32* %result, align 4, !tbaa !3
  %67 = load i32, i32* %result, align 4, !tbaa !3
  %cmp38 = icmp sle i32 %67, 0
  br i1 %cmp38, label %if.then39, label %if.end40

if.then39:                                        ; preds = %if.else28
  %68 = load i32, i32* %result, align 4, !tbaa !3
  store i32 %68, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end40:                                         ; preds = %if.else28
  %69 = load i32, i32* %result, align 4, !tbaa !3
  %70 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixSize41 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %70, i32 0, i32 3
  store i32 %69, i32* %prefixSize41, align 4, !tbaa !20
  %71 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %72 = load i32, i32* %result, align 4, !tbaa !3
  %add.ptr42 = getelementptr inbounds i8, i8* %71, i32 %72
  %73 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixEnd43 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %73, i32 0, i32 2
  store i8* %add.ptr42, i8** %prefixEnd43, align 4, !tbaa !22
  br label %if.end44

if.end44:                                         ; preds = %if.end40, %if.end24
  br label %if.end45

if.end45:                                         ; preds = %if.end44, %if.end
  %74 = load i32, i32* %result, align 4, !tbaa !3
  store i32 %74, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end45, %if.then39, %if.then23, %if.then2
  %75 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #7
  %76 = bitcast %struct.LZ4_streamDecode_t_internal** %lz4sd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #7
  %77 = load i32, i32* %retval, align 4
  ret i32 %77
}

; Function Attrs: nounwind
define internal i32 @LZ4_decompress_safe_withSmallPrefix(i8* %source, i8* %dest, i32 %compressedSize, i32 %maxOutputSize, i32 %prefixSize) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %compressedSize.addr = alloca i32, align 4
  %maxOutputSize.addr = alloca i32, align 4
  %prefixSize.addr = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %compressedSize, i32* %compressedSize.addr, align 4, !tbaa !3
  store i32 %maxOutputSize, i32* %maxOutputSize.addr, align 4, !tbaa !3
  store i32 %prefixSize, i32* %prefixSize.addr, align 4, !tbaa !10
  %0 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %2 = load i32, i32* %compressedSize.addr, align 4, !tbaa !3
  %3 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %4 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %5 = load i32, i32* %prefixSize.addr, align 4, !tbaa !10
  %idx.neg = sub i32 0, %5
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %idx.neg
  %call = call i32 @LZ4_decompress_generic(i8* %0, i8* %1, i32 %2, i32 %3, i32 1, i32 0, i32 0, i8* %add.ptr, i8* null, i32 0)
  ret i32 %call
}

; Function Attrs: alwaysinline nounwind
define internal i32 @LZ4_decompress_safe_doubleDict(i8* %source, i8* %dest, i32 %compressedSize, i32 %maxOutputSize, i32 %prefixSize, i8* %dictStart, i32 %dictSize) #2 {
entry:
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %compressedSize.addr = alloca i32, align 4
  %maxOutputSize.addr = alloca i32, align 4
  %prefixSize.addr = alloca i32, align 4
  %dictStart.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %compressedSize, i32* %compressedSize.addr, align 4, !tbaa !3
  store i32 %maxOutputSize, i32* %maxOutputSize.addr, align 4, !tbaa !3
  store i32 %prefixSize, i32* %prefixSize.addr, align 4, !tbaa !10
  store i8* %dictStart, i8** %dictStart.addr, align 4, !tbaa !7
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !10
  %0 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %2 = load i32, i32* %compressedSize.addr, align 4, !tbaa !3
  %3 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %4 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %5 = load i32, i32* %prefixSize.addr, align 4, !tbaa !10
  %idx.neg = sub i32 0, %5
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %idx.neg
  %6 = load i8*, i8** %dictStart.addr, align 4, !tbaa !7
  %7 = load i32, i32* %dictSize.addr, align 4, !tbaa !10
  %call = call i32 @LZ4_decompress_generic(i8* %0, i8* %1, i32 %2, i32 %3, i32 1, i32 0, i32 2, i8* %add.ptr, i8* %6, i32 %7)
  ret i32 %call
}

; Function Attrs: nounwind
define i32 @LZ4_decompress_fast_continue(%union.LZ4_streamDecode_u* %LZ4_streamDecode, i8* %source, i8* %dest, i32 %originalSize) #0 {
entry:
  %retval = alloca i32, align 4
  %LZ4_streamDecode.addr = alloca %union.LZ4_streamDecode_u*, align 4
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %originalSize.addr = alloca i32, align 4
  %lz4sd = alloca %struct.LZ4_streamDecode_t_internal*, align 4
  %result = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %union.LZ4_streamDecode_u* %LZ4_streamDecode, %union.LZ4_streamDecode_u** %LZ4_streamDecode.addr, align 4, !tbaa !7
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %originalSize, i32* %originalSize.addr, align 4, !tbaa !3
  %0 = bitcast %struct.LZ4_streamDecode_t_internal** %lz4sd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load %union.LZ4_streamDecode_u*, %union.LZ4_streamDecode_u** %LZ4_streamDecode.addr, align 4, !tbaa !7
  %internal_donotuse = bitcast %union.LZ4_streamDecode_u* %1 to %struct.LZ4_streamDecode_t_internal*
  store %struct.LZ4_streamDecode_t_internal* %internal_donotuse, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %2 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixSize = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %3, i32 0, i32 3
  %4 = load i32, i32* %prefixSize, align 4, !tbaa !20
  %cmp = icmp eq i32 %4, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %6 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %7 = load i32, i32* %originalSize.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_decompress_fast(i8* %5, i8* %6, i32 %7)
  store i32 %call, i32* %result, align 4, !tbaa !3
  %8 = load i32, i32* %result, align 4, !tbaa !3
  %cmp1 = icmp sle i32 %8, 0
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %9 = load i32, i32* %result, align 4, !tbaa !3
  store i32 %9, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %10 = load i32, i32* %originalSize.addr, align 4, !tbaa !3
  %11 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixSize3 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %11, i32 0, i32 3
  store i32 %10, i32* %prefixSize3, align 4, !tbaa !20
  %12 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %13 = load i32, i32* %originalSize.addr, align 4, !tbaa !3
  %add.ptr = getelementptr inbounds i8, i8* %12, i32 %13
  %14 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixEnd = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %14, i32 0, i32 2
  store i8* %add.ptr, i8** %prefixEnd, align 4, !tbaa !22
  br label %if.end40

if.else:                                          ; preds = %entry
  %15 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixEnd4 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %15, i32 0, i32 2
  %16 = load i8*, i8** %prefixEnd4, align 4, !tbaa !22
  %17 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %cmp5 = icmp eq i8* %16, %17
  br i1 %cmp5, label %if.then6, label %if.else23

if.then6:                                         ; preds = %if.else
  %18 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixSize7 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %18, i32 0, i32 3
  %19 = load i32, i32* %prefixSize7, align 4, !tbaa !20
  %cmp8 = icmp uge i32 %19, 65535
  br i1 %cmp8, label %if.then10, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then6
  %20 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %extDictSize = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %20, i32 0, i32 1
  %21 = load i32, i32* %extDictSize, align 4, !tbaa !24
  %cmp9 = icmp eq i32 %21, 0
  br i1 %cmp9, label %if.then10, label %if.else12

if.then10:                                        ; preds = %lor.lhs.false, %if.then6
  %22 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %23 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %24 = load i32, i32* %originalSize.addr, align 4, !tbaa !3
  %call11 = call i32 @LZ4_decompress_fast(i8* %22, i8* %23, i32 %24)
  store i32 %call11, i32* %result, align 4, !tbaa !3
  br label %if.end16

if.else12:                                        ; preds = %lor.lhs.false
  %25 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %26 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %27 = load i32, i32* %originalSize.addr, align 4, !tbaa !3
  %28 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixSize13 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %28, i32 0, i32 3
  %29 = load i32, i32* %prefixSize13, align 4, !tbaa !20
  %30 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %externalDict = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %30, i32 0, i32 0
  %31 = load i8*, i8** %externalDict, align 4, !tbaa !23
  %32 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %extDictSize14 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %32, i32 0, i32 1
  %33 = load i32, i32* %extDictSize14, align 4, !tbaa !24
  %call15 = call i32 @LZ4_decompress_fast_doubleDict(i8* %25, i8* %26, i32 %27, i32 %29, i8* %31, i32 %33)
  store i32 %call15, i32* %result, align 4, !tbaa !3
  br label %if.end16

if.end16:                                         ; preds = %if.else12, %if.then10
  %34 = load i32, i32* %result, align 4, !tbaa !3
  %cmp17 = icmp sle i32 %34, 0
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end16
  %35 = load i32, i32* %result, align 4, !tbaa !3
  store i32 %35, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end19:                                         ; preds = %if.end16
  %36 = load i32, i32* %originalSize.addr, align 4, !tbaa !3
  %37 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixSize20 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %37, i32 0, i32 3
  %38 = load i32, i32* %prefixSize20, align 4, !tbaa !20
  %add = add i32 %38, %36
  store i32 %add, i32* %prefixSize20, align 4, !tbaa !20
  %39 = load i32, i32* %originalSize.addr, align 4, !tbaa !3
  %40 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixEnd21 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %40, i32 0, i32 2
  %41 = load i8*, i8** %prefixEnd21, align 4, !tbaa !22
  %add.ptr22 = getelementptr inbounds i8, i8* %41, i32 %39
  store i8* %add.ptr22, i8** %prefixEnd21, align 4, !tbaa !22
  br label %if.end39

if.else23:                                        ; preds = %if.else
  %42 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixSize24 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %42, i32 0, i32 3
  %43 = load i32, i32* %prefixSize24, align 4, !tbaa !20
  %44 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %extDictSize25 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %44, i32 0, i32 1
  store i32 %43, i32* %extDictSize25, align 4, !tbaa !24
  %45 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixEnd26 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %45, i32 0, i32 2
  %46 = load i8*, i8** %prefixEnd26, align 4, !tbaa !22
  %47 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %extDictSize27 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %47, i32 0, i32 1
  %48 = load i32, i32* %extDictSize27, align 4, !tbaa !24
  %idx.neg = sub i32 0, %48
  %add.ptr28 = getelementptr inbounds i8, i8* %46, i32 %idx.neg
  %49 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %externalDict29 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %49, i32 0, i32 0
  store i8* %add.ptr28, i8** %externalDict29, align 4, !tbaa !23
  %50 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %51 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %52 = load i32, i32* %originalSize.addr, align 4, !tbaa !3
  %53 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %externalDict30 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %53, i32 0, i32 0
  %54 = load i8*, i8** %externalDict30, align 4, !tbaa !23
  %55 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %extDictSize31 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %55, i32 0, i32 1
  %56 = load i32, i32* %extDictSize31, align 4, !tbaa !24
  %call32 = call i32 @LZ4_decompress_fast_extDict(i8* %50, i8* %51, i32 %52, i8* %54, i32 %56)
  store i32 %call32, i32* %result, align 4, !tbaa !3
  %57 = load i32, i32* %result, align 4, !tbaa !3
  %cmp33 = icmp sle i32 %57, 0
  br i1 %cmp33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %if.else23
  %58 = load i32, i32* %result, align 4, !tbaa !3
  store i32 %58, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end35:                                         ; preds = %if.else23
  %59 = load i32, i32* %originalSize.addr, align 4, !tbaa !3
  %60 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixSize36 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %60, i32 0, i32 3
  store i32 %59, i32* %prefixSize36, align 4, !tbaa !20
  %61 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %62 = load i32, i32* %originalSize.addr, align 4, !tbaa !3
  %add.ptr37 = getelementptr inbounds i8, i8* %61, i32 %62
  %63 = load %struct.LZ4_streamDecode_t_internal*, %struct.LZ4_streamDecode_t_internal** %lz4sd, align 4, !tbaa !7
  %prefixEnd38 = getelementptr inbounds %struct.LZ4_streamDecode_t_internal, %struct.LZ4_streamDecode_t_internal* %63, i32 0, i32 2
  store i8* %add.ptr37, i8** %prefixEnd38, align 4, !tbaa !22
  br label %if.end39

if.end39:                                         ; preds = %if.end35, %if.end19
  br label %if.end40

if.end40:                                         ; preds = %if.end39, %if.end
  %64 = load i32, i32* %result, align 4, !tbaa !3
  store i32 %64, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end40, %if.then34, %if.then18, %if.then2
  %65 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #7
  %66 = bitcast %struct.LZ4_streamDecode_t_internal** %lz4sd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #7
  %67 = load i32, i32* %retval, align 4
  ret i32 %67
}

; Function Attrs: alwaysinline nounwind
define internal i32 @LZ4_decompress_fast_doubleDict(i8* %source, i8* %dest, i32 %originalSize, i32 %prefixSize, i8* %dictStart, i32 %dictSize) #2 {
entry:
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %originalSize.addr = alloca i32, align 4
  %prefixSize.addr = alloca i32, align 4
  %dictStart.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %originalSize, i32* %originalSize.addr, align 4, !tbaa !3
  store i32 %prefixSize, i32* %prefixSize.addr, align 4, !tbaa !10
  store i8* %dictStart, i8** %dictStart.addr, align 4, !tbaa !7
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !10
  %0 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %2 = load i32, i32* %originalSize.addr, align 4, !tbaa !3
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %4 = load i32, i32* %prefixSize.addr, align 4, !tbaa !10
  %idx.neg = sub i32 0, %4
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %idx.neg
  %5 = load i8*, i8** %dictStart.addr, align 4, !tbaa !7
  %6 = load i32, i32* %dictSize.addr, align 4, !tbaa !10
  %call = call i32 @LZ4_decompress_generic(i8* %0, i8* %1, i32 0, i32 %2, i32 0, i32 0, i32 2, i8* %add.ptr, i8* %5, i32 %6)
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @LZ4_decompress_fast_extDict(i8* %source, i8* %dest, i32 %originalSize, i8* %dictStart, i32 %dictSize) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %originalSize.addr = alloca i32, align 4
  %dictStart.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %originalSize, i32* %originalSize.addr, align 4, !tbaa !3
  store i8* %dictStart, i8** %dictStart.addr, align 4, !tbaa !7
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !10
  %0 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %2 = load i32, i32* %originalSize.addr, align 4, !tbaa !3
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %4 = load i8*, i8** %dictStart.addr, align 4, !tbaa !7
  %5 = load i32, i32* %dictSize.addr, align 4, !tbaa !10
  %call = call i32 @LZ4_decompress_generic(i8* %0, i8* %1, i32 0, i32 %2, i32 0, i32 0, i32 2, i8* %3, i8* %4, i32 %5)
  ret i32 %call
}

; Function Attrs: nounwind
define i32 @LZ4_decompress_safe_usingDict(i8* %source, i8* %dest, i32 %compressedSize, i32 %maxOutputSize, i8* %dictStart, i32 %dictSize) #0 {
entry:
  %retval = alloca i32, align 4
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %compressedSize.addr = alloca i32, align 4
  %maxOutputSize.addr = alloca i32, align 4
  %dictStart.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %compressedSize, i32* %compressedSize.addr, align 4, !tbaa !3
  store i32 %maxOutputSize, i32* %maxOutputSize.addr, align 4, !tbaa !3
  store i8* %dictStart, i8** %dictStart.addr, align 4, !tbaa !7
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !3
  %0 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %2 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %3 = load i32, i32* %compressedSize.addr, align 4, !tbaa !3
  %4 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_decompress_safe(i8* %1, i8* %2, i32 %3, i32 %4)
  store i32 %call, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %5 = load i8*, i8** %dictStart.addr, align 4, !tbaa !7
  %6 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %6
  %7 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %cmp1 = icmp eq i8* %add.ptr, %7
  br i1 %cmp1, label %if.then2, label %if.end8

if.then2:                                         ; preds = %if.end
  %8 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %cmp3 = icmp sge i32 %8, 65535
  br i1 %cmp3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.then2
  %9 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %10 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %11 = load i32, i32* %compressedSize.addr, align 4, !tbaa !3
  %12 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %call5 = call i32 @LZ4_decompress_safe_withPrefix64k(i8* %9, i8* %10, i32 %11, i32 %12)
  store i32 %call5, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.then2
  %13 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %14 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %15 = load i32, i32* %compressedSize.addr, align 4, !tbaa !3
  %16 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %17 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %call7 = call i32 @LZ4_decompress_safe_withSmallPrefix(i8* %13, i8* %14, i32 %15, i32 %16, i32 %17)
  store i32 %call7, i32* %retval, align 4
  br label %return

if.end8:                                          ; preds = %if.end
  %18 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %19 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %20 = load i32, i32* %compressedSize.addr, align 4, !tbaa !3
  %21 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %22 = load i8*, i8** %dictStart.addr, align 4, !tbaa !7
  %23 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %call9 = call i32 @LZ4_decompress_safe_forceExtDict(i8* %18, i8* %19, i32 %20, i32 %21, i8* %22, i32 %23)
  store i32 %call9, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end8, %if.end6, %if.then4, %if.then
  %24 = load i32, i32* %retval, align 4
  ret i32 %24
}

; Function Attrs: nounwind
define i32 @LZ4_decompress_fast_usingDict(i8* %source, i8* %dest, i32 %originalSize, i8* %dictStart, i32 %dictSize) #0 {
entry:
  %retval = alloca i32, align 4
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %originalSize.addr = alloca i32, align 4
  %dictStart.addr = alloca i8*, align 4
  %dictSize.addr = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %originalSize, i32* %originalSize.addr, align 4, !tbaa !3
  store i8* %dictStart, i8** %dictStart.addr, align 4, !tbaa !7
  store i32 %dictSize, i32* %dictSize.addr, align 4, !tbaa !3
  %0 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i8*, i8** %dictStart.addr, align 4, !tbaa !7
  %2 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %2
  %3 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %cmp1 = icmp eq i8* %add.ptr, %3
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %4 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %5 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %6 = load i32, i32* %originalSize.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_decompress_fast(i8* %4, i8* %5, i32 %6)
  store i32 %call, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %7 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %8 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %9 = load i32, i32* %originalSize.addr, align 4, !tbaa !3
  %10 = load i8*, i8** %dictStart.addr, align 4, !tbaa !7
  %11 = load i32, i32* %dictSize.addr, align 4, !tbaa !3
  %call2 = call i32 @LZ4_decompress_fast_extDict(i8* %7, i8* %8, i32 %9, i8* %10, i32 %11)
  store i32 %call2, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

; Function Attrs: nounwind
define i32 @LZ4_compress_limitedOutput(i8* %source, i8* %dest, i32 %inputSize, i32 %maxOutputSize) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %inputSize.addr = alloca i32, align 4
  %maxOutputSize.addr = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %inputSize, i32* %inputSize.addr, align 4, !tbaa !3
  store i32 %maxOutputSize, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %0 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %2 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %3 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_compress_default(i8* %0, i8* %1, i32 %2, i32 %3)
  ret i32 %call
}

; Function Attrs: nounwind
define i32 @LZ4_compress(i8* %src, i8* %dest, i32 %srcSize) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !3
  %0 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %2 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %3 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_compressBound(i32 %3)
  %call1 = call i32 @LZ4_compress_default(i8* %0, i8* %1, i32 %2, i32 %call)
  ret i32 %call1
}

; Function Attrs: nounwind
define i32 @LZ4_compress_limitedOutput_withState(i8* %state, i8* %src, i8* %dst, i32 %srcSize, i32 %dstSize) #0 {
entry:
  %state.addr = alloca i8*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %dstSize.addr = alloca i32, align 4
  store i8* %state, i8** %state.addr, align 4, !tbaa !7
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !7
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !3
  store i32 %dstSize, i32* %dstSize.addr, align 4, !tbaa !3
  %0 = load i8*, i8** %state.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %2 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %3 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %4 = load i32, i32* %dstSize.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_compress_fast_extState(i8* %0, i8* %1, i8* %2, i32 %3, i32 %4, i32 1)
  ret i32 %call
}

; Function Attrs: nounwind
define i32 @LZ4_compress_withState(i8* %state, i8* %src, i8* %dst, i32 %srcSize) #0 {
entry:
  %state.addr = alloca i8*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  store i8* %state, i8** %state.addr, align 4, !tbaa !7
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !7
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !3
  %0 = load i8*, i8** %state.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %2 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %3 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %4 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_compressBound(i32 %4)
  %call1 = call i32 @LZ4_compress_fast_extState(i8* %0, i8* %1, i8* %2, i32 %3, i32 %call, i32 1)
  ret i32 %call1
}

; Function Attrs: nounwind
define i32 @LZ4_compress_limitedOutput_continue(%union.LZ4_stream_u* %LZ4_stream, i8* %src, i8* %dst, i32 %srcSize, i32 %dstCapacity) #0 {
entry:
  %LZ4_stream.addr = alloca %union.LZ4_stream_u*, align 4
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %srcSize.addr = alloca i32, align 4
  %dstCapacity.addr = alloca i32, align 4
  store %union.LZ4_stream_u* %LZ4_stream, %union.LZ4_stream_u** %LZ4_stream.addr, align 4, !tbaa !7
  store i8* %src, i8** %src.addr, align 4, !tbaa !7
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !7
  store i32 %srcSize, i32* %srcSize.addr, align 4, !tbaa !3
  store i32 %dstCapacity, i32* %dstCapacity.addr, align 4, !tbaa !3
  %0 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %LZ4_stream.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %src.addr, align 4, !tbaa !7
  %2 = load i8*, i8** %dst.addr, align 4, !tbaa !7
  %3 = load i32, i32* %srcSize.addr, align 4, !tbaa !3
  %4 = load i32, i32* %dstCapacity.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_compress_fast_continue(%union.LZ4_stream_u* %0, i8* %1, i8* %2, i32 %3, i32 %4, i32 1)
  ret i32 %call
}

; Function Attrs: nounwind
define i32 @LZ4_compress_continue(%union.LZ4_stream_u* %LZ4_stream, i8* %source, i8* %dest, i32 %inputSize) #0 {
entry:
  %LZ4_stream.addr = alloca %union.LZ4_stream_u*, align 4
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %inputSize.addr = alloca i32, align 4
  store %union.LZ4_stream_u* %LZ4_stream, %union.LZ4_stream_u** %LZ4_stream.addr, align 4, !tbaa !7
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %inputSize, i32* %inputSize.addr, align 4, !tbaa !3
  %0 = load %union.LZ4_stream_u*, %union.LZ4_stream_u** %LZ4_stream.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %2 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %3 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %4 = load i32, i32* %inputSize.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_compressBound(i32 %4)
  %call1 = call i32 @LZ4_compress_fast_continue(%union.LZ4_stream_u* %0, i8* %1, i8* %2, i32 %3, i32 %call, i32 1)
  ret i32 %call1
}

; Function Attrs: nounwind
define i32 @LZ4_uncompress(i8* %source, i8* %dest, i32 %outputSize) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %outputSize.addr = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %outputSize, i32* %outputSize.addr, align 4, !tbaa !3
  %0 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %2 = load i32, i32* %outputSize.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_decompress_fast(i8* %0, i8* %1, i32 %2)
  ret i32 %call
}

; Function Attrs: nounwind
define i32 @LZ4_uncompress_unknownOutputSize(i8* %source, i8* %dest, i32 %isize, i32 %maxOutputSize) #0 {
entry:
  %source.addr = alloca i8*, align 4
  %dest.addr = alloca i8*, align 4
  %isize.addr = alloca i32, align 4
  %maxOutputSize.addr = alloca i32, align 4
  store i8* %source, i8** %source.addr, align 4, !tbaa !7
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !7
  store i32 %isize, i32* %isize.addr, align 4, !tbaa !3
  store i32 %maxOutputSize, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %0 = load i8*, i8** %source.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %dest.addr, align 4, !tbaa !7
  %2 = load i32, i32* %isize.addr, align 4, !tbaa !3
  %3 = load i32, i32* %maxOutputSize.addr, align 4, !tbaa !3
  %call = call i32 @LZ4_decompress_safe(i8* %0, i8* %1, i32 %2, i32 %3)
  ret i32 %call
}

; Function Attrs: nounwind
define i32 @LZ4_sizeofStreamState() #0 {
entry:
  ret i32 16416
}

; Function Attrs: nounwind
define i32 @LZ4_resetStreamState(i8* %state, i8* %inputBuffer) #0 {
entry:
  %state.addr = alloca i8*, align 4
  %inputBuffer.addr = alloca i8*, align 4
  store i8* %state, i8** %state.addr, align 4, !tbaa !7
  store i8* %inputBuffer, i8** %inputBuffer.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %inputBuffer.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %state.addr, align 4, !tbaa !7
  %2 = bitcast i8* %1 to %union.LZ4_stream_u*
  call void @LZ4_resetStream(%union.LZ4_stream_u* %2)
  ret i32 0
}

; Function Attrs: nounwind
define i8* @LZ4_create(i8* %inputBuffer) #0 {
entry:
  %inputBuffer.addr = alloca i8*, align 4
  store i8* %inputBuffer, i8** %inputBuffer.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %inputBuffer.addr, align 4, !tbaa !7
  %call = call %union.LZ4_stream_u* @LZ4_createStream()
  %1 = bitcast %union.LZ4_stream_u* %call to i8*
  ret i8* %1
}

; Function Attrs: nounwind
define i8* @LZ4_slideInputBuffer(i8* %state) #0 {
entry:
  %state.addr = alloca i8*, align 4
  store i8* %state, i8** %state.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %state.addr, align 4, !tbaa !7
  %1 = bitcast i8* %0 to %union.LZ4_stream_u*
  %internal_donotuse = bitcast %union.LZ4_stream_u* %1 to %struct.LZ4_stream_t_internal*
  %dictionary = getelementptr inbounds %struct.LZ4_stream_t_internal, %struct.LZ4_stream_t_internal* %internal_donotuse, i32 0, i32 4
  %2 = load i8*, i8** %dictionary, align 8, !tbaa !9
  %3 = ptrtoint i8* %2 to i32
  %4 = inttoptr i32 %3 to i8*
  ret i8* %4
}

; Function Attrs: alwaysinline nounwind
define internal i32 @LZ4_hashPosition(i8* %p, i32 %tableType) #2 {
entry:
  %p.addr = alloca i8*, align 4
  %tableType.addr = alloca i32, align 4
  store i8* %p, i8** %p.addr, align 4, !tbaa !7
  store i32 %tableType, i32* %tableType.addr, align 4, !tbaa !9
  %0 = load i8*, i8** %p.addr, align 4, !tbaa !7
  %call = call i32 @LZ4_read32(i8* %0)
  %1 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %call1 = call i32 @LZ4_hash4(i32 %call, i32 %1)
  ret i32 %call1
}

; Function Attrs: nounwind readnone willreturn
declare i32 @llvm.expect.i32(i32, i32) #5

; Function Attrs: nounwind
define internal i8* @LZ4_getPositionOnHash(i32 %h, i8* %tableBase, i32 %tableType, i8* %srcBase) #0 {
entry:
  %retval = alloca i8*, align 4
  %h.addr = alloca i32, align 4
  %tableBase.addr = alloca i8*, align 4
  %tableType.addr = alloca i32, align 4
  %srcBase.addr = alloca i8*, align 4
  %hashTable = alloca i8**, align 4
  %hashTable3 = alloca i32*, align 4
  %hashTable6 = alloca i16*, align 4
  store i32 %h, i32* %h.addr, align 4, !tbaa !3
  store i8* %tableBase, i8** %tableBase.addr, align 4, !tbaa !7
  store i32 %tableType, i32* %tableType.addr, align 4, !tbaa !9
  store i8* %srcBase, i8** %srcBase.addr, align 4, !tbaa !7
  %0 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %cmp = icmp eq i32 %0, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i8*** %hashTable to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i8*, i8** %tableBase.addr, align 4, !tbaa !7
  %3 = bitcast i8* %2 to i8**
  store i8** %3, i8*** %hashTable, align 4, !tbaa !7
  %4 = load i8**, i8*** %hashTable, align 4, !tbaa !7
  %5 = load i32, i32* %h.addr, align 4, !tbaa !3
  %arrayidx = getelementptr inbounds i8*, i8** %4, i32 %5
  %6 = load i8*, i8** %arrayidx, align 4, !tbaa !7
  store i8* %6, i8** %retval, align 4
  %7 = bitcast i8*** %hashTable to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %return

if.end:                                           ; preds = %entry
  %8 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %cmp1 = icmp eq i32 %8, 2
  br i1 %cmp1, label %if.then2, label %if.end5

if.then2:                                         ; preds = %if.end
  %9 = bitcast i32** %hashTable3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load i8*, i8** %tableBase.addr, align 4, !tbaa !7
  %11 = bitcast i8* %10 to i32*
  store i32* %11, i32** %hashTable3, align 4, !tbaa !7
  %12 = load i32*, i32** %hashTable3, align 4, !tbaa !7
  %13 = load i32, i32* %h.addr, align 4, !tbaa !3
  %arrayidx4 = getelementptr inbounds i32, i32* %12, i32 %13
  %14 = load i32, i32* %arrayidx4, align 4, !tbaa !3
  %15 = load i8*, i8** %srcBase.addr, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds i8, i8* %15, i32 %14
  store i8* %add.ptr, i8** %retval, align 4
  %16 = bitcast i32** %hashTable3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #7
  br label %return

if.end5:                                          ; preds = %if.end
  %17 = bitcast i16** %hashTable6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #7
  %18 = load i8*, i8** %tableBase.addr, align 4, !tbaa !7
  %19 = bitcast i8* %18 to i16*
  store i16* %19, i16** %hashTable6, align 4, !tbaa !7
  %20 = load i16*, i16** %hashTable6, align 4, !tbaa !7
  %21 = load i32, i32* %h.addr, align 4, !tbaa !3
  %arrayidx7 = getelementptr inbounds i16, i16* %20, i32 %21
  %22 = load i16, i16* %arrayidx7, align 2, !tbaa !25
  %conv = zext i16 %22 to i32
  %23 = load i8*, i8** %srcBase.addr, align 4, !tbaa !7
  %add.ptr8 = getelementptr inbounds i8, i8* %23, i32 %conv
  store i8* %add.ptr8, i8** %retval, align 4
  %24 = bitcast i16** %hashTable6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #7
  br label %return

return:                                           ; preds = %if.end5, %if.then2, %if.then
  %25 = load i8*, i8** %retval, align 4
  ret i8* %25
}

; Function Attrs: alwaysinline nounwind
define internal void @LZ4_putPositionOnHash(i8* %p, i32 %h, i8* %tableBase, i32 %tableType, i8* %srcBase) #2 {
entry:
  %p.addr = alloca i8*, align 4
  %h.addr = alloca i32, align 4
  %tableBase.addr = alloca i8*, align 4
  %tableType.addr = alloca i32, align 4
  %srcBase.addr = alloca i8*, align 4
  %hashTable = alloca i8**, align 4
  %hashTable3 = alloca i32*, align 4
  %hashTable6 = alloca i16*, align 4
  store i8* %p, i8** %p.addr, align 4, !tbaa !7
  store i32 %h, i32* %h.addr, align 4, !tbaa !3
  store i8* %tableBase, i8** %tableBase.addr, align 4, !tbaa !7
  store i32 %tableType, i32* %tableType.addr, align 4, !tbaa !9
  store i8* %srcBase, i8** %srcBase.addr, align 4, !tbaa !7
  %0 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  switch i32 %0, label %sw.epilog [
    i32 0, label %sw.bb
    i32 1, label %sw.bb1
    i32 2, label %sw.bb2
    i32 3, label %sw.bb5
  ]

sw.bb:                                            ; preds = %entry
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry
  %1 = bitcast i8*** %hashTable to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i8*, i8** %tableBase.addr, align 4, !tbaa !7
  %3 = bitcast i8* %2 to i8**
  store i8** %3, i8*** %hashTable, align 4, !tbaa !7
  %4 = load i8*, i8** %p.addr, align 4, !tbaa !7
  %5 = load i8**, i8*** %hashTable, align 4, !tbaa !7
  %6 = load i32, i32* %h.addr, align 4, !tbaa !3
  %arrayidx = getelementptr inbounds i8*, i8** %5, i32 %6
  store i8* %4, i8** %arrayidx, align 4, !tbaa !7
  %7 = bitcast i8*** %hashTable to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %sw.epilog

sw.bb2:                                           ; preds = %entry
  %8 = bitcast i32** %hashTable3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = load i8*, i8** %tableBase.addr, align 4, !tbaa !7
  %10 = bitcast i8* %9 to i32*
  store i32* %10, i32** %hashTable3, align 4, !tbaa !7
  %11 = load i8*, i8** %p.addr, align 4, !tbaa !7
  %12 = load i8*, i8** %srcBase.addr, align 4, !tbaa !7
  %sub.ptr.lhs.cast = ptrtoint i8* %11 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %12 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %13 = load i32*, i32** %hashTable3, align 4, !tbaa !7
  %14 = load i32, i32* %h.addr, align 4, !tbaa !3
  %arrayidx4 = getelementptr inbounds i32, i32* %13, i32 %14
  store i32 %sub.ptr.sub, i32* %arrayidx4, align 4, !tbaa !3
  %15 = bitcast i32** %hashTable3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  br label %sw.epilog

sw.bb5:                                           ; preds = %entry
  %16 = bitcast i16** %hashTable6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #7
  %17 = load i8*, i8** %tableBase.addr, align 4, !tbaa !7
  %18 = bitcast i8* %17 to i16*
  store i16* %18, i16** %hashTable6, align 4, !tbaa !7
  %19 = load i8*, i8** %p.addr, align 4, !tbaa !7
  %20 = load i8*, i8** %srcBase.addr, align 4, !tbaa !7
  %sub.ptr.lhs.cast7 = ptrtoint i8* %19 to i32
  %sub.ptr.rhs.cast8 = ptrtoint i8* %20 to i32
  %sub.ptr.sub9 = sub i32 %sub.ptr.lhs.cast7, %sub.ptr.rhs.cast8
  %conv = trunc i32 %sub.ptr.sub9 to i16
  %21 = load i16*, i16** %hashTable6, align 4, !tbaa !7
  %22 = load i32, i32* %h.addr, align 4, !tbaa !3
  %arrayidx10 = getelementptr inbounds i16, i16* %21, i32 %22
  store i16 %conv, i16* %arrayidx10, align 2, !tbaa !25
  %23 = bitcast i16** %hashTable6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #7
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.bb, %sw.bb1, %sw.bb2, %sw.bb5, %entry
  ret void
}

; Function Attrs: nounwind
define internal i32 @LZ4_read32(i8* %ptr) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !7
  %1 = bitcast i8* %0 to %union.unalign*
  %u32 = bitcast %union.unalign* %1 to i32*
  %2 = load i32, i32* %u32, align 1, !tbaa !9
  ret i32 %2
}

; Function Attrs: alwaysinline nounwind
define internal i32 @LZ4_getIndexOnHash(i32 %h, i8* %tableBase, i32 %tableType) #2 {
entry:
  %retval = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %tableBase.addr = alloca i8*, align 4
  %tableType.addr = alloca i32, align 4
  %hashTable = alloca i32*, align 4
  %hashTable3 = alloca i16*, align 4
  store i32 %h, i32* %h.addr, align 4, !tbaa !3
  store i8* %tableBase, i8** %tableBase.addr, align 4, !tbaa !7
  store i32 %tableType, i32* %tableType.addr, align 4, !tbaa !9
  %0 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %cmp = icmp eq i32 %0, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i32** %hashTable to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i8*, i8** %tableBase.addr, align 4, !tbaa !7
  %3 = bitcast i8* %2 to i32*
  store i32* %3, i32** %hashTable, align 4, !tbaa !7
  %4 = load i32*, i32** %hashTable, align 4, !tbaa !7
  %5 = load i32, i32* %h.addr, align 4, !tbaa !3
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = load i32, i32* %arrayidx, align 4, !tbaa !3
  store i32 %6, i32* %retval, align 4
  %7 = bitcast i32** %hashTable to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %return

if.end:                                           ; preds = %entry
  %8 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %cmp1 = icmp eq i32 %8, 3
  br i1 %cmp1, label %if.then2, label %if.end5

if.then2:                                         ; preds = %if.end
  %9 = bitcast i16** %hashTable3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #7
  %10 = load i8*, i8** %tableBase.addr, align 4, !tbaa !7
  %11 = bitcast i8* %10 to i16*
  store i16* %11, i16** %hashTable3, align 4, !tbaa !7
  %12 = load i16*, i16** %hashTable3, align 4, !tbaa !7
  %13 = load i32, i32* %h.addr, align 4, !tbaa !3
  %arrayidx4 = getelementptr inbounds i16, i16* %12, i32 %13
  %14 = load i16, i16* %arrayidx4, align 2, !tbaa !25
  %conv = zext i16 %14 to i32
  store i32 %conv, i32* %retval, align 4
  %15 = bitcast i16** %hashTable3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #7
  br label %return

if.end5:                                          ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then2, %if.then
  %16 = load i32, i32* %retval, align 4
  ret i32 %16
}

; Function Attrs: alwaysinline nounwind
define internal void @LZ4_putIndexOnHash(i32 %idx, i32 %h, i8* %tableBase, i32 %tableType) #2 {
entry:
  %idx.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %tableBase.addr = alloca i8*, align 4
  %tableType.addr = alloca i32, align 4
  %hashTable = alloca i32*, align 4
  %hashTable3 = alloca i16*, align 4
  store i32 %idx, i32* %idx.addr, align 4, !tbaa !3
  store i32 %h, i32* %h.addr, align 4, !tbaa !3
  store i8* %tableBase, i8** %tableBase.addr, align 4, !tbaa !7
  store i32 %tableType, i32* %tableType.addr, align 4, !tbaa !9
  %0 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  switch i32 %0, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb
    i32 2, label %sw.bb1
    i32 3, label %sw.bb2
  ]

sw.default:                                       ; preds = %entry
  br label %sw.bb

sw.bb:                                            ; preds = %entry, %entry, %sw.default
  br label %return

sw.bb1:                                           ; preds = %entry
  %1 = bitcast i32** %hashTable to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i8*, i8** %tableBase.addr, align 4, !tbaa !7
  %3 = bitcast i8* %2 to i32*
  store i32* %3, i32** %hashTable, align 4, !tbaa !7
  %4 = load i32, i32* %idx.addr, align 4, !tbaa !3
  %5 = load i32*, i32** %hashTable, align 4, !tbaa !7
  %6 = load i32, i32* %h.addr, align 4, !tbaa !3
  %arrayidx = getelementptr inbounds i32, i32* %5, i32 %6
  store i32 %4, i32* %arrayidx, align 4, !tbaa !3
  %7 = bitcast i32** %hashTable to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %return

sw.bb2:                                           ; preds = %entry
  %8 = bitcast i16** %hashTable3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #7
  %9 = load i8*, i8** %tableBase.addr, align 4, !tbaa !7
  %10 = bitcast i8* %9 to i16*
  store i16* %10, i16** %hashTable3, align 4, !tbaa !7
  %11 = load i32, i32* %idx.addr, align 4, !tbaa !3
  %conv = trunc i32 %11 to i16
  %12 = load i16*, i16** %hashTable3, align 4, !tbaa !7
  %13 = load i32, i32* %h.addr, align 4, !tbaa !3
  %arrayidx4 = getelementptr inbounds i16, i16* %12, i32 %13
  store i16 %conv, i16* %arrayidx4, align 2, !tbaa !25
  %14 = bitcast i16** %hashTable3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #7
  br label %return

return:                                           ; preds = %sw.bb2, %sw.bb1, %sw.bb
  ret void
}

; Function Attrs: nounwind
define internal void @LZ4_wildCopy8(i8* %dstPtr, i8* %srcPtr, i8* %dstEnd) #0 {
entry:
  %dstPtr.addr = alloca i8*, align 4
  %srcPtr.addr = alloca i8*, align 4
  %dstEnd.addr = alloca i8*, align 4
  %d = alloca i8*, align 4
  %s = alloca i8*, align 4
  %e = alloca i8*, align 4
  store i8* %dstPtr, i8** %dstPtr.addr, align 4, !tbaa !7
  store i8* %srcPtr, i8** %srcPtr.addr, align 4, !tbaa !7
  store i8* %dstEnd, i8** %dstEnd.addr, align 4, !tbaa !7
  %0 = bitcast i8** %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8*, i8** %dstPtr.addr, align 4, !tbaa !7
  store i8* %1, i8** %d, align 4, !tbaa !7
  %2 = bitcast i8** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load i8*, i8** %srcPtr.addr, align 4, !tbaa !7
  store i8* %3, i8** %s, align 4, !tbaa !7
  %4 = bitcast i8** %e to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load i8*, i8** %dstEnd.addr, align 4, !tbaa !7
  store i8* %5, i8** %e, align 4, !tbaa !7
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  %6 = load i8*, i8** %d, align 4, !tbaa !7
  %7 = load i8*, i8** %s, align 4, !tbaa !7
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %6, i8* align 1 %7, i32 8, i1 false)
  %8 = load i8*, i8** %d, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds i8, i8* %8, i32 8
  store i8* %add.ptr, i8** %d, align 4, !tbaa !7
  %9 = load i8*, i8** %s, align 4, !tbaa !7
  %add.ptr1 = getelementptr inbounds i8, i8* %9, i32 8
  store i8* %add.ptr1, i8** %s, align 4, !tbaa !7
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %10 = load i8*, i8** %d, align 4, !tbaa !7
  %11 = load i8*, i8** %e, align 4, !tbaa !7
  %cmp = icmp ult i8* %10, %11
  br i1 %cmp, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %12 = bitcast i8** %e to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  %13 = bitcast i8** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  %14 = bitcast i8** %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #7
  ret void
}

; Function Attrs: nounwind
define internal void @LZ4_writeLE16(i8* %memPtr, i16 zeroext %value) #0 {
entry:
  %memPtr.addr = alloca i8*, align 4
  %value.addr = alloca i16, align 2
  %p = alloca i8*, align 4
  store i8* %memPtr, i8** %memPtr.addr, align 4, !tbaa !7
  store i16 %value, i16* %value.addr, align 2, !tbaa !25
  %call = call i32 @LZ4_isLittleEndian()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %0 = load i8*, i8** %memPtr.addr, align 4, !tbaa !7
  %1 = load i16, i16* %value.addr, align 2, !tbaa !25
  call void @LZ4_write16(i8* %0, i16 zeroext %1)
  br label %if.end

if.else:                                          ; preds = %entry
  %2 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load i8*, i8** %memPtr.addr, align 4, !tbaa !7
  store i8* %3, i8** %p, align 4, !tbaa !7
  %4 = load i16, i16* %value.addr, align 2, !tbaa !25
  %conv = trunc i16 %4 to i8
  %5 = load i8*, i8** %p, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i8, i8* %5, i32 0
  store i8 %conv, i8* %arrayidx, align 1, !tbaa !9
  %6 = load i16, i16* %value.addr, align 2, !tbaa !25
  %conv1 = zext i16 %6 to i32
  %shr = ashr i32 %conv1, 8
  %conv2 = trunc i32 %shr to i8
  %7 = load i8*, i8** %p, align 4, !tbaa !7
  %arrayidx3 = getelementptr inbounds i8, i8* %7, i32 1
  store i8 %conv2, i8* %arrayidx3, align 1, !tbaa !9
  %8 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #7
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal i32 @LZ4_count(i8* %pIn, i8* %pMatch, i8* %pInLimit) #2 {
entry:
  %retval = alloca i32, align 4
  %pIn.addr = alloca i8*, align 4
  %pMatch.addr = alloca i8*, align 4
  %pInLimit.addr = alloca i8*, align 4
  %pStart = alloca i8*, align 4
  %diff = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %diff17 = alloca i32, align 4
  store i8* %pIn, i8** %pIn.addr, align 4, !tbaa !7
  store i8* %pMatch, i8** %pMatch.addr, align 4, !tbaa !7
  store i8* %pInLimit, i8** %pInLimit.addr, align 4, !tbaa !7
  %0 = bitcast i8** %pStart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8*, i8** %pIn.addr, align 4, !tbaa !7
  store i8* %1, i8** %pStart, align 4, !tbaa !7
  %2 = load i8*, i8** %pIn.addr, align 4, !tbaa !7
  %3 = load i8*, i8** %pInLimit.addr, align 4, !tbaa !7
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 -3
  %cmp = icmp ult i8* %2, %add.ptr
  %conv = zext i1 %cmp to i32
  %cmp1 = icmp ne i32 %conv, 0
  %conv2 = zext i1 %cmp1 to i32
  %expval = call i32 @llvm.expect.i32(i32 %conv2, i32 1)
  %tobool = icmp ne i32 %expval, 0
  br i1 %tobool, label %if.then, label %if.end9

if.then:                                          ; preds = %entry
  %4 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #7
  %5 = load i8*, i8** %pMatch.addr, align 4, !tbaa !7
  %call = call i32 @LZ4_read_ARCH(i8* %5)
  %6 = load i8*, i8** %pIn.addr, align 4, !tbaa !7
  %call3 = call i32 @LZ4_read_ARCH(i8* %6)
  %xor = xor i32 %call, %call3
  store i32 %xor, i32* %diff, align 4, !tbaa !10
  %7 = load i32, i32* %diff, align 4, !tbaa !10
  %tobool4 = icmp ne i32 %7, 0
  br i1 %tobool4, label %if.else, label %if.then5

if.then5:                                         ; preds = %if.then
  %8 = load i8*, i8** %pIn.addr, align 4, !tbaa !7
  %add.ptr6 = getelementptr inbounds i8, i8* %8, i32 4
  store i8* %add.ptr6, i8** %pIn.addr, align 4, !tbaa !7
  %9 = load i8*, i8** %pMatch.addr, align 4, !tbaa !7
  %add.ptr7 = getelementptr inbounds i8, i8* %9, i32 4
  store i8* %add.ptr7, i8** %pMatch.addr, align 4, !tbaa !7
  br label %if.end

if.else:                                          ; preds = %if.then
  %10 = load i32, i32* %diff, align 4, !tbaa !10
  %call8 = call i32 @LZ4_NbCommonBytes(i32 %10)
  store i32 %call8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then5
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.else
  %11 = bitcast i32* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup55 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end9

if.end9:                                          ; preds = %cleanup.cont, %entry
  br label %while.cond

while.cond:                                       ; preds = %cleanup28, %if.end9
  %12 = load i8*, i8** %pIn.addr, align 4, !tbaa !7
  %13 = load i8*, i8** %pInLimit.addr, align 4, !tbaa !7
  %add.ptr10 = getelementptr inbounds i8, i8* %13, i32 -3
  %cmp11 = icmp ult i8* %12, %add.ptr10
  %conv12 = zext i1 %cmp11 to i32
  %cmp13 = icmp ne i32 %conv12, 0
  %conv14 = zext i1 %cmp13 to i32
  %expval15 = call i32 @llvm.expect.i32(i32 %conv14, i32 1)
  %tobool16 = icmp ne i32 %expval15, 0
  br i1 %tobool16, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %14 = bitcast i32* %diff17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #7
  %15 = load i8*, i8** %pMatch.addr, align 4, !tbaa !7
  %call18 = call i32 @LZ4_read_ARCH(i8* %15)
  %16 = load i8*, i8** %pIn.addr, align 4, !tbaa !7
  %call19 = call i32 @LZ4_read_ARCH(i8* %16)
  %xor20 = xor i32 %call18, %call19
  store i32 %xor20, i32* %diff17, align 4, !tbaa !10
  %17 = load i32, i32* %diff17, align 4, !tbaa !10
  %tobool21 = icmp ne i32 %17, 0
  br i1 %tobool21, label %if.end25, label %if.then22

if.then22:                                        ; preds = %while.body
  %18 = load i8*, i8** %pIn.addr, align 4, !tbaa !7
  %add.ptr23 = getelementptr inbounds i8, i8* %18, i32 4
  store i8* %add.ptr23, i8** %pIn.addr, align 4, !tbaa !7
  %19 = load i8*, i8** %pMatch.addr, align 4, !tbaa !7
  %add.ptr24 = getelementptr inbounds i8, i8* %19, i32 4
  store i8* %add.ptr24, i8** %pMatch.addr, align 4, !tbaa !7
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup28

if.end25:                                         ; preds = %while.body
  %20 = load i32, i32* %diff17, align 4, !tbaa !10
  %call26 = call i32 @LZ4_NbCommonBytes(i32 %20)
  %21 = load i8*, i8** %pIn.addr, align 4, !tbaa !7
  %add.ptr27 = getelementptr inbounds i8, i8* %21, i32 %call26
  store i8* %add.ptr27, i8** %pIn.addr, align 4, !tbaa !7
  %22 = load i8*, i8** %pIn.addr, align 4, !tbaa !7
  %23 = load i8*, i8** %pStart, align 4, !tbaa !7
  %sub.ptr.lhs.cast = ptrtoint i8* %22 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %23 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup28

cleanup28:                                        ; preds = %if.end25, %if.then22
  %24 = bitcast i32* %diff17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #7
  %cleanup.dest29 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest29, label %cleanup55 [
    i32 2, label %while.cond
  ]

while.end:                                        ; preds = %while.cond
  %25 = load i8*, i8** %pIn.addr, align 4, !tbaa !7
  %26 = load i8*, i8** %pInLimit.addr, align 4, !tbaa !7
  %add.ptr30 = getelementptr inbounds i8, i8* %26, i32 -1
  %cmp31 = icmp ult i8* %25, %add.ptr30
  br i1 %cmp31, label %land.lhs.true, label %if.end42

land.lhs.true:                                    ; preds = %while.end
  %27 = load i8*, i8** %pMatch.addr, align 4, !tbaa !7
  %call33 = call zeroext i16 @LZ4_read16(i8* %27)
  %conv34 = zext i16 %call33 to i32
  %28 = load i8*, i8** %pIn.addr, align 4, !tbaa !7
  %call35 = call zeroext i16 @LZ4_read16(i8* %28)
  %conv36 = zext i16 %call35 to i32
  %cmp37 = icmp eq i32 %conv34, %conv36
  br i1 %cmp37, label %if.then39, label %if.end42

if.then39:                                        ; preds = %land.lhs.true
  %29 = load i8*, i8** %pIn.addr, align 4, !tbaa !7
  %add.ptr40 = getelementptr inbounds i8, i8* %29, i32 2
  store i8* %add.ptr40, i8** %pIn.addr, align 4, !tbaa !7
  %30 = load i8*, i8** %pMatch.addr, align 4, !tbaa !7
  %add.ptr41 = getelementptr inbounds i8, i8* %30, i32 2
  store i8* %add.ptr41, i8** %pMatch.addr, align 4, !tbaa !7
  br label %if.end42

if.end42:                                         ; preds = %if.then39, %land.lhs.true, %while.end
  %31 = load i8*, i8** %pIn.addr, align 4, !tbaa !7
  %32 = load i8*, i8** %pInLimit.addr, align 4, !tbaa !7
  %cmp43 = icmp ult i8* %31, %32
  br i1 %cmp43, label %land.lhs.true45, label %if.end51

land.lhs.true45:                                  ; preds = %if.end42
  %33 = load i8*, i8** %pMatch.addr, align 4, !tbaa !7
  %34 = load i8, i8* %33, align 1, !tbaa !9
  %conv46 = zext i8 %34 to i32
  %35 = load i8*, i8** %pIn.addr, align 4, !tbaa !7
  %36 = load i8, i8* %35, align 1, !tbaa !9
  %conv47 = zext i8 %36 to i32
  %cmp48 = icmp eq i32 %conv46, %conv47
  br i1 %cmp48, label %if.then50, label %if.end51

if.then50:                                        ; preds = %land.lhs.true45
  %37 = load i8*, i8** %pIn.addr, align 4, !tbaa !7
  %incdec.ptr = getelementptr inbounds i8, i8* %37, i32 1
  store i8* %incdec.ptr, i8** %pIn.addr, align 4, !tbaa !7
  br label %if.end51

if.end51:                                         ; preds = %if.then50, %land.lhs.true45, %if.end42
  %38 = load i8*, i8** %pIn.addr, align 4, !tbaa !7
  %39 = load i8*, i8** %pStart, align 4, !tbaa !7
  %sub.ptr.lhs.cast52 = ptrtoint i8* %38 to i32
  %sub.ptr.rhs.cast53 = ptrtoint i8* %39 to i32
  %sub.ptr.sub54 = sub i32 %sub.ptr.lhs.cast52, %sub.ptr.rhs.cast53
  store i32 %sub.ptr.sub54, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup55

cleanup55:                                        ; preds = %if.end51, %cleanup28, %cleanup
  %40 = bitcast i8** %pStart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #7
  %41 = load i32, i32* %retval, align 4
  ret i32 %41
}

; Function Attrs: alwaysinline nounwind
define internal void @LZ4_clearHash(i32 %h, i8* %tableBase, i32 %tableType) #2 {
entry:
  %h.addr = alloca i32, align 4
  %tableBase.addr = alloca i8*, align 4
  %tableType.addr = alloca i32, align 4
  %hashTable = alloca i8**, align 4
  %hashTable3 = alloca i32*, align 4
  %hashTable6 = alloca i16*, align 4
  store i32 %h, i32* %h.addr, align 4, !tbaa !3
  store i8* %tableBase, i8** %tableBase.addr, align 4, !tbaa !7
  store i32 %tableType, i32* %tableType.addr, align 4, !tbaa !9
  %0 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  switch i32 %0, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb1
    i32 2, label %sw.bb2
    i32 3, label %sw.bb5
  ]

sw.default:                                       ; preds = %entry
  br label %sw.bb

sw.bb:                                            ; preds = %entry, %sw.default
  br label %return

sw.bb1:                                           ; preds = %entry
  %1 = bitcast i8*** %hashTable to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i8*, i8** %tableBase.addr, align 4, !tbaa !7
  %3 = bitcast i8* %2 to i8**
  store i8** %3, i8*** %hashTable, align 4, !tbaa !7
  %4 = load i8**, i8*** %hashTable, align 4, !tbaa !7
  %5 = load i32, i32* %h.addr, align 4, !tbaa !3
  %arrayidx = getelementptr inbounds i8*, i8** %4, i32 %5
  store i8* null, i8** %arrayidx, align 4, !tbaa !7
  %6 = bitcast i8*** %hashTable to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #7
  br label %return

sw.bb2:                                           ; preds = %entry
  %7 = bitcast i32** %hashTable3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #7
  %8 = load i8*, i8** %tableBase.addr, align 4, !tbaa !7
  %9 = bitcast i8* %8 to i32*
  store i32* %9, i32** %hashTable3, align 4, !tbaa !7
  %10 = load i32*, i32** %hashTable3, align 4, !tbaa !7
  %11 = load i32, i32* %h.addr, align 4, !tbaa !3
  %arrayidx4 = getelementptr inbounds i32, i32* %10, i32 %11
  store i32 0, i32* %arrayidx4, align 4, !tbaa !3
  %12 = bitcast i32** %hashTable3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #7
  br label %return

sw.bb5:                                           ; preds = %entry
  %13 = bitcast i16** %hashTable6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i8*, i8** %tableBase.addr, align 4, !tbaa !7
  %15 = bitcast i8* %14 to i16*
  store i16* %15, i16** %hashTable6, align 4, !tbaa !7
  %16 = load i16*, i16** %hashTable6, align 4, !tbaa !7
  %17 = load i32, i32* %h.addr, align 4, !tbaa !3
  %arrayidx7 = getelementptr inbounds i16, i16* %16, i32 %17
  store i16 0, i16* %arrayidx7, align 2, !tbaa !25
  %18 = bitcast i16** %hashTable6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #7
  br label %return

return:                                           ; preds = %sw.bb5, %sw.bb2, %sw.bb1, %sw.bb
  ret void
}

; Function Attrs: nounwind
define internal void @LZ4_write32(i8* %memPtr, i32 %value) #0 {
entry:
  %memPtr.addr = alloca i8*, align 4
  %value.addr = alloca i32, align 4
  store i8* %memPtr, i8** %memPtr.addr, align 4, !tbaa !7
  store i32 %value, i32* %value.addr, align 4, !tbaa !3
  %0 = load i32, i32* %value.addr, align 4, !tbaa !3
  %1 = load i8*, i8** %memPtr.addr, align 4, !tbaa !7
  %2 = bitcast i8* %1 to %union.unalign*
  %u32 = bitcast %union.unalign* %2 to i32*
  store i32 %0, i32* %u32, align 1, !tbaa !9
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal i8* @LZ4_getPosition(i8* %p, i8* %tableBase, i32 %tableType, i8* %srcBase) #2 {
entry:
  %p.addr = alloca i8*, align 4
  %tableBase.addr = alloca i8*, align 4
  %tableType.addr = alloca i32, align 4
  %srcBase.addr = alloca i8*, align 4
  %h = alloca i32, align 4
  store i8* %p, i8** %p.addr, align 4, !tbaa !7
  store i8* %tableBase, i8** %tableBase.addr, align 4, !tbaa !7
  store i32 %tableType, i32* %tableType.addr, align 4, !tbaa !9
  store i8* %srcBase, i8** %srcBase.addr, align 4, !tbaa !7
  %0 = bitcast i32* %h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i8*, i8** %p.addr, align 4, !tbaa !7
  %2 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %call = call i32 @LZ4_hashPosition(i8* %1, i32 %2)
  store i32 %call, i32* %h, align 4, !tbaa !3
  %3 = load i32, i32* %h, align 4, !tbaa !3
  %4 = load i8*, i8** %tableBase.addr, align 4, !tbaa !7
  %5 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %6 = load i8*, i8** %srcBase.addr, align 4, !tbaa !7
  %call1 = call i8* @LZ4_getPositionOnHash(i32 %3, i8* %4, i32 %5, i8* %6)
  %7 = bitcast i32* %h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret i8* %call1
}

; Function Attrs: alwaysinline nounwind
define internal i32 @LZ4_hash4(i32 %sequence, i32 %tableType) #2 {
entry:
  %retval = alloca i32, align 4
  %sequence.addr = alloca i32, align 4
  %tableType.addr = alloca i32, align 4
  store i32 %sequence, i32* %sequence.addr, align 4, !tbaa !3
  store i32 %tableType, i32* %tableType.addr, align 4, !tbaa !9
  %0 = load i32, i32* %tableType.addr, align 4, !tbaa !9
  %cmp = icmp eq i32 %0, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %sequence.addr, align 4, !tbaa !3
  %mul = mul i32 %1, -1640531535
  %shr = lshr i32 %mul, 19
  store i32 %shr, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %2 = load i32, i32* %sequence.addr, align 4, !tbaa !3
  %mul1 = mul i32 %2, -1640531535
  %shr2 = lshr i32 %mul1, 20
  store i32 %shr2, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.else, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: nounwind
define internal i32 @LZ4_isLittleEndian() #0 {
entry:
  %one = alloca %union.anon, align 4
  %0 = bitcast %union.anon* %one to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = bitcast %union.anon* %one to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 bitcast (%union.anon* @__const.LZ4_isLittleEndian.one to i8*), i32 4, i1 false)
  %c = bitcast %union.anon* %one to [4 x i8]*
  %arrayidx = getelementptr inbounds [4 x i8], [4 x i8]* %c, i32 0, i32 0
  %2 = load i8, i8* %arrayidx, align 4, !tbaa !9
  %conv = zext i8 %2 to i32
  %3 = bitcast %union.anon* %one to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #7
  ret i32 %conv
}

; Function Attrs: nounwind
define internal void @LZ4_write16(i8* %memPtr, i16 zeroext %value) #0 {
entry:
  %memPtr.addr = alloca i8*, align 4
  %value.addr = alloca i16, align 2
  store i8* %memPtr, i8** %memPtr.addr, align 4, !tbaa !7
  store i16 %value, i16* %value.addr, align 2, !tbaa !25
  %0 = load i16, i16* %value.addr, align 2, !tbaa !25
  %1 = load i8*, i8** %memPtr.addr, align 4, !tbaa !7
  %2 = bitcast i8* %1 to %union.unalign*
  %u16 = bitcast %union.unalign* %2 to i16*
  store i16 %0, i16* %u16, align 1, !tbaa !9
  ret void
}

; Function Attrs: nounwind
define internal i32 @LZ4_read_ARCH(i8* %ptr) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !7
  %1 = bitcast i8* %0 to %union.unalign*
  %uArch = bitcast %union.unalign* %1 to i32*
  %2 = load i32, i32* %uArch, align 1, !tbaa !9
  ret i32 %2
}

; Function Attrs: nounwind
define internal i32 @LZ4_NbCommonBytes(i32 %val) #0 {
entry:
  %retval = alloca i32, align 4
  %val.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !10
  %call = call i32 @LZ4_isLittleEndian()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %0 = load i32, i32* %val.addr, align 4, !tbaa !10
  %1 = call i32 @llvm.cttz.i32(i32 %0, i1 false)
  %shr = lshr i32 %1, 3
  store i32 %shr, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %2 = load i32, i32* %val.addr, align 4, !tbaa !10
  %3 = call i32 @llvm.ctlz.i32(i32 %2, i1 false)
  %shr1 = lshr i32 %3, 3
  store i32 %shr1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.else, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: nounwind
define internal zeroext i16 @LZ4_read16(i8* %ptr) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !7
  %1 = bitcast i8* %0 to %union.unalign*
  %u16 = bitcast %union.unalign* %1 to i16*
  %2 = load i16, i16* %u16, align 1, !tbaa !9
  ret i16 %2
}

; Function Attrs: nounwind readnone speculatable willreturn
declare i32 @llvm.cttz.i32(i32, i1 immarg) #6

; Function Attrs: nounwind readnone speculatable willreturn
declare i32 @llvm.ctlz.i32(i32, i1 immarg) #6

; Function Attrs: nounwind
define internal zeroext i16 @LZ4_readLE16(i8* %memPtr) #0 {
entry:
  %retval = alloca i16, align 2
  %memPtr.addr = alloca i8*, align 4
  %p = alloca i8*, align 4
  store i8* %memPtr, i8** %memPtr.addr, align 4, !tbaa !7
  %call = call i32 @LZ4_isLittleEndian()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %0 = load i8*, i8** %memPtr.addr, align 4, !tbaa !7
  %call1 = call zeroext i16 @LZ4_read16(i8* %0)
  store i16 %call1, i16* %retval, align 2
  br label %return

if.else:                                          ; preds = %entry
  %1 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i8*, i8** %memPtr.addr, align 4, !tbaa !7
  store i8* %2, i8** %p, align 4, !tbaa !7
  %3 = load i8*, i8** %p, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 0
  %4 = load i8, i8* %arrayidx, align 1, !tbaa !9
  %conv = zext i8 %4 to i16
  %conv2 = zext i16 %conv to i32
  %5 = load i8*, i8** %p, align 4, !tbaa !7
  %arrayidx3 = getelementptr inbounds i8, i8* %5, i32 1
  %6 = load i8, i8* %arrayidx3, align 1, !tbaa !9
  %conv4 = zext i8 %6 to i32
  %shl = shl i32 %conv4, 8
  %add = add nsw i32 %conv2, %shl
  %conv5 = trunc i32 %add to i16
  store i16 %conv5, i16* %retval, align 2
  %7 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %return

return:                                           ; preds = %if.else, %if.then
  %8 = load i16, i16* %retval, align 2
  ret i16 %8
}

; Function Attrs: alwaysinline nounwind
define internal i32 @read_variable_length(i8** %ip, i8* %lencheck, i32 %loop_check, i32 %initial_check, i32* %error) #2 {
entry:
  %retval = alloca i32, align 4
  %ip.addr = alloca i8**, align 4
  %lencheck.addr = alloca i8*, align 4
  %loop_check.addr = alloca i32, align 4
  %initial_check.addr = alloca i32, align 4
  %error.addr = alloca i32*, align 4
  %length = alloca i32, align 4
  %s = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8** %ip, i8*** %ip.addr, align 4, !tbaa !7
  store i8* %lencheck, i8** %lencheck.addr, align 4, !tbaa !7
  store i32 %loop_check, i32* %loop_check.addr, align 4, !tbaa !3
  store i32 %initial_check, i32* %initial_check.addr, align 4, !tbaa !3
  store i32* %error, i32** %error.addr, align 4, !tbaa !7
  %0 = bitcast i32* %length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  store i32 0, i32* %length, align 4, !tbaa !3
  %1 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %initial_check.addr, align 4, !tbaa !3
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %3 = load i8**, i8*** %ip.addr, align 4, !tbaa !7
  %4 = load i8*, i8** %3, align 4, !tbaa !7
  %5 = load i8*, i8** %lencheck.addr, align 4, !tbaa !7
  %cmp = icmp uge i8* %4, %5
  %conv = zext i1 %cmp to i32
  %cmp1 = icmp ne i32 %conv, 0
  %conv2 = zext i1 %cmp1 to i32
  %expval = call i32 @llvm.expect.i32(i32 %conv2, i32 0)
  %tobool3 = icmp ne i32 %expval, 0
  br i1 %tobool3, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %6 = load i32*, i32** %error.addr, align 4, !tbaa !7
  store i32 -1, i32* %6, align 4, !tbaa !9
  %7 = load i32, i32* %length, align 4, !tbaa !3
  store i32 %7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %land.lhs.true, %entry
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.end
  %8 = load i8**, i8*** %ip.addr, align 4, !tbaa !7
  %9 = load i8*, i8** %8, align 4, !tbaa !7
  %10 = load i8, i8* %9, align 1, !tbaa !9
  %conv4 = zext i8 %10 to i32
  store i32 %conv4, i32* %s, align 4, !tbaa !3
  %11 = load i8**, i8*** %ip.addr, align 4, !tbaa !7
  %12 = load i8*, i8** %11, align 4, !tbaa !7
  %incdec.ptr = getelementptr inbounds i8, i8* %12, i32 1
  store i8* %incdec.ptr, i8** %11, align 4, !tbaa !7
  %13 = load i32, i32* %s, align 4, !tbaa !3
  %14 = load i32, i32* %length, align 4, !tbaa !3
  %add = add i32 %14, %13
  store i32 %add, i32* %length, align 4, !tbaa !3
  %15 = load i32, i32* %loop_check.addr, align 4, !tbaa !3
  %tobool5 = icmp ne i32 %15, 0
  br i1 %tobool5, label %land.lhs.true6, label %if.end14

land.lhs.true6:                                   ; preds = %do.body
  %16 = load i8**, i8*** %ip.addr, align 4, !tbaa !7
  %17 = load i8*, i8** %16, align 4, !tbaa !7
  %18 = load i8*, i8** %lencheck.addr, align 4, !tbaa !7
  %cmp7 = icmp uge i8* %17, %18
  %conv8 = zext i1 %cmp7 to i32
  %cmp9 = icmp ne i32 %conv8, 0
  %conv10 = zext i1 %cmp9 to i32
  %expval11 = call i32 @llvm.expect.i32(i32 %conv10, i32 0)
  %tobool12 = icmp ne i32 %expval11, 0
  br i1 %tobool12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %land.lhs.true6
  %19 = load i32*, i32** %error.addr, align 4, !tbaa !7
  store i32 -2, i32* %19, align 4, !tbaa !9
  %20 = load i32, i32* %length, align 4, !tbaa !3
  store i32 %20, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %land.lhs.true6, %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end14
  %21 = load i32, i32* %s, align 4, !tbaa !3
  %cmp15 = icmp eq i32 %21, 255
  br i1 %cmp15, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %22 = load i32, i32* %length, align 4, !tbaa !3
  store i32 %22, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end, %if.then13, %if.then
  %23 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #7
  %24 = bitcast i32* %length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #7
  %25 = load i32, i32* %retval, align 4
  ret i32 %25
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { alwaysinline nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { nounwind readnone willreturn }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!3 = !{!4, !4, i64 0}
!4 = !{!"int", !5, i64 0}
!5 = !{!"omnipotent char", !6, i64 0}
!6 = !{!"Simple C/C++ TBAA"}
!7 = !{!8, !8, i64 0}
!8 = !{!"any pointer", !5, i64 0}
!9 = !{!5, !5, i64 0}
!10 = !{!11, !11, i64 0}
!11 = !{!"long", !5, i64 0}
!12 = !{!13, !4, i64 16384}
!13 = !{!"LZ4_stream_t_internal", !5, i64 0, !4, i64 16384, !14, i64 16388, !14, i64 16390, !8, i64 16392, !8, i64 16396, !4, i64 16400}
!14 = !{!"short", !5, i64 0}
!15 = !{!13, !8, i64 16396}
!16 = !{!13, !8, i64 16392}
!17 = !{!13, !4, i64 16400}
!18 = !{!13, !14, i64 16390}
!19 = !{!13, !14, i64 16388}
!20 = !{!21, !11, i64 12}
!21 = !{!"", !8, i64 0, !11, i64 4, !8, i64 8, !11, i64 12}
!22 = !{!21, !8, i64 8}
!23 = !{!21, !8, i64 0}
!24 = !{!21, !11, i64 4}
!25 = !{!14, !14, i64 0}
