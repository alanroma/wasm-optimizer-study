; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-enc-mt/src/colr.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-enc-mt/src/colr.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.avifColorPrimariesTable = type { i32, i8*, [8 x float] }
%struct.avifMatrixCoefficientsTable = type { i32, i8*, float, float }
%struct.avifImage = type { i32, i32, i32, i32, i32, i32, [3 x i8*], [3 x i32], i32, i32, i8*, i32, i32, %struct.avifRWData, i32, i32, i32, i32, %struct.avifPixelAspectRatioBox, %struct.avifCleanApertureBox, %struct.avifImageRotation, %struct.avifImageMirror, %struct.avifRWData, %struct.avifRWData }
%struct.avifPixelAspectRatioBox = type { i32, i32 }
%struct.avifCleanApertureBox = type { i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.avifImageRotation = type { i8 }
%struct.avifImageMirror = type { i8 }
%struct.avifRWData = type { i8*, i32 }

@avifColorPrimariesTables = internal constant [11 x %struct.avifColorPrimariesTable] [%struct.avifColorPrimariesTable { i32 1, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str, i32 0, i32 0), [8 x float] [float 0x3FE47AE140000000, float 0x3FD51EB860000000, float 0x3FD3333340000000, float 0x3FE3333340000000, float 0x3FC3333340000000, float 0x3FAEB851E0000000, float 0x3FD40346E0000000, float 0x3FD50E5600000000] }, %struct.avifColorPrimariesTable { i32 4, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.1, i32 0, i32 0), [8 x float] [float 0x3FE570A3E0000000, float 0x3FD51EB860000000, float 0x3FCAE147A0000000, float 0x3FE6B851E0000000, float 0x3FC1EB8520000000, float 0x3FB47AE140000000, float 0x3FD3D70A40000000, float 0x3FD4395820000000] }, %struct.avifColorPrimariesTable { i32 5, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.2, i32 0, i32 0), [8 x float] [float 0x3FE47AE140000000, float 0x3FD51EB860000000, float 0x3FD28F5C20000000, float 0x3FE3333340000000, float 0x3FC3333340000000, float 0x3FAEB851E0000000, float 0x3FD40346E0000000, float 0x3FD50E5600000000] }, %struct.avifColorPrimariesTable { i32 6, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i32 0, i32 0), [8 x float] [float 0x3FE428F5C0000000, float 0x3FD5C28F60000000, float 0x3FD3D70A40000000, float 0x3FE30A3D80000000, float 0x3FC3D70A40000000, float 0x3FB1EB8520000000, float 0x3FD40346E0000000, float 0x3FD50E5600000000] }, %struct.avifColorPrimariesTable { i32 7, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.4, i32 0, i32 0), [8 x float] [float 0x3FE428F5C0000000, float 0x3FD5C28F60000000, float 0x3FD3D70A40000000, float 0x3FE30A3D80000000, float 0x3FC3D70A40000000, float 0x3FB1EB8520000000, float 0x3FD40346E0000000, float 0x3FD50E5600000000] }, %struct.avifColorPrimariesTable { i32 8, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.5, i32 0, i32 0), [8 x float] [float 0x3FE5CAC080000000, float 0x3FD46A7F00000000, float 0x3FCF1A9FC0000000, float 0x3FE624DD20000000, float 0x3FC28F5C20000000, float 0x3FA9168720000000, float 0x3FD3D70A40000000, float 0x3FD4395820000000] }, %struct.avifColorPrimariesTable { i32 9, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.6, i32 0, i32 0), [8 x float] [float 0x3FE6A7EFA0000000, float 0x3FD2B020C0000000, float 0x3FC5C28F60000000, float 0x3FE9810620000000, float 0x3FC0C49BA0000000, float 0x3FA78D4FE0000000, float 0x3FD40346E0000000, float 0x3FD50E5600000000] }, %struct.avifColorPrimariesTable { i32 10, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.7, i32 0, i32 0), [8 x float] [float 1.000000e+00, float 0.000000e+00, float 0.000000e+00, float 1.000000e+00, float 0.000000e+00, float 0.000000e+00, float 0x3FD554C980000000, float 0x3FD554C980000000] }, %struct.avifColorPrimariesTable { i32 11, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.8, i32 0, i32 0), [8 x float] [float 0x3FE5C28F60000000, float 0x3FD47AE140000000, float 0x3FD0F5C280000000, float 0x3FE6147AE0000000, float 0x3FC3333340000000, float 0x3FAEB851E0000000, float 0x3FD4189380000000, float 0x3FD676C8C0000000] }, %struct.avifColorPrimariesTable { i32 12, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.9, i32 0, i32 0), [8 x float] [float 0x3FE5C28F60000000, float 0x3FD47AE140000000, float 0x3FD0F5C280000000, float 0x3FE6147AE0000000, float 0x3FC3333340000000, float 0x3FAEB851E0000000, float 0x3FD40346E0000000, float 0x3FD50E5600000000] }, %struct.avifColorPrimariesTable { i32 22, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.10, i32 0, i32 0), [8 x float] [float 0x3FE428F5C0000000, float 0x3FD5C28F60000000, float 0x3FD2E147A0000000, float 0x3FE35C2900000000, float 0x3FC3D70A40000000, float 0x3FB3B645A0000000, float 0x3FD40346E0000000, float 0x3FD50E5600000000] }], align 16
@.str = private unnamed_addr constant [7 x i8] c"BT.709\00", align 1
@.str.1 = private unnamed_addr constant [18 x i8] c"BT.470-6 System M\00", align 1
@.str.2 = private unnamed_addr constant [19 x i8] c"BT.470-6 System BG\00", align 1
@.str.3 = private unnamed_addr constant [7 x i8] c"BT.601\00", align 1
@.str.4 = private unnamed_addr constant [11 x i8] c"SMPTE 240M\00", align 1
@.str.5 = private unnamed_addr constant [13 x i8] c"Generic film\00", align 1
@.str.6 = private unnamed_addr constant [8 x i8] c"BT.2020\00", align 1
@.str.7 = private unnamed_addr constant [4 x i8] c"XYZ\00", align 1
@.str.8 = private unnamed_addr constant [15 x i8] c"SMPTE RP 431-2\00", align 1
@.str.9 = private unnamed_addr constant [24 x i8] c"SMPTE EG 432-1 (DCI P3)\00", align 1
@.str.10 = private unnamed_addr constant [17 x i8] c"EBU Tech. 3213-E\00", align 1
@matrixCoefficientsTables = internal constant [6 x %struct.avifMatrixCoefficientsTable] [%struct.avifMatrixCoefficientsTable { i32 1, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str, i32 0, i32 0), float 0x3FCB367A00000000, float 0x3FB27BB300000000 }, %struct.avifMatrixCoefficientsTable { i32 4, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.11, i32 0, i32 0), float 0x3FD3333340000000, float 0x3FBC28F5C0000000 }, %struct.avifMatrixCoefficientsTable { i32 5, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.2, i32 0, i32 0), float 0x3FD322D0E0000000, float 0x3FBD2F1AA0000000 }, %struct.avifMatrixCoefficientsTable { i32 6, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i32 0, i32 0), float 0x3FD322D0E0000000, float 0x3FC26E9780000000 }, %struct.avifMatrixCoefficientsTable { i32 7, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.12, i32 0, i32 0), float 0x3FCB22D0E0000000, float 0x3FB645A1C0000000 }, %struct.avifMatrixCoefficientsTable { i32 9, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.13, i32 0, i32 0), float 0x3FD0D013A0000000, float 0x3FAE5C91E0000000 }], align 16
@.str.11 = private unnamed_addr constant [16 x i8] c"FCC USFC 73.682\00", align 1
@.str.12 = private unnamed_addr constant [13 x i8] c"SMPTE ST 240\00", align 1
@.str.13 = private unnamed_addr constant [33 x i8] c"BT.2020 (non-constant luminance)\00", align 1

; Function Attrs: nounwind
define hidden void @avifColorPrimariesGetValues(i32 %acp, float* %outPrimaries) #0 {
entry:
  %acp.addr = alloca i32, align 4
  %outPrimaries.addr = alloca float*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i32 %acp, i32* %acp.addr, align 4, !tbaa !2
  store float* %outPrimaries, float** %outPrimaries.addr, align 4, !tbaa !5
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !7
  %cmp = icmp slt i32 %1, 11
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds [11 x %struct.avifColorPrimariesTable], [11 x %struct.avifColorPrimariesTable]* @avifColorPrimariesTables, i32 0, i32 %2
  %colorPrimariesEnum = getelementptr inbounds %struct.avifColorPrimariesTable, %struct.avifColorPrimariesTable* %arrayidx, i32 0, i32 0
  %3 = load i32, i32* %colorPrimariesEnum, align 8, !tbaa !9
  %4 = load i32, i32* %acp.addr, align 4, !tbaa !2
  %cmp1 = icmp eq i32 %3, %4
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %5 = load float*, float** %outPrimaries.addr, align 4, !tbaa !5
  %6 = bitcast float* %5 to i8*
  %7 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx2 = getelementptr inbounds [11 x %struct.avifColorPrimariesTable], [11 x %struct.avifColorPrimariesTable]* @avifColorPrimariesTables, i32 0, i32 %7
  %primaries = getelementptr inbounds %struct.avifColorPrimariesTable, %struct.avifColorPrimariesTable* %arrayidx2, i32 0, i32 2
  %arraydecay = getelementptr inbounds [8 x float], [8 x float]* %primaries, i32 0, i32 0
  %8 = bitcast float* %arraydecay to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 8 %8, i32 32, i1 false)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %9 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond

cleanup:                                          ; preds = %if.then, %for.cond.cleanup
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 2, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup
  %11 = load float*, float** %outPrimaries.addr, align 4, !tbaa !5
  %12 = bitcast float* %11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 8 bitcast (float* getelementptr inbounds ([11 x %struct.avifColorPrimariesTable], [11 x %struct.avifColorPrimariesTable]* @avifColorPrimariesTables, i32 0, i32 0, i32 2, i32 0) to i8*), i32 32, i1 false)
  br label %return

return:                                           ; preds = %for.end, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden i32 @avifColorPrimariesFind(float* %inPrimaries, i8** %outName) #0 {
entry:
  %retval = alloca i32, align 4
  %inPrimaries.addr = alloca float*, align 4
  %outName.addr = alloca i8**, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store float* %inPrimaries, float** %inPrimaries.addr, align 4, !tbaa !5
  store i8** %outName, i8*** %outName.addr, align 4, !tbaa !5
  %0 = load i8**, i8*** %outName.addr, align 4, !tbaa !5
  %tobool = icmp ne i8** %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i8**, i8*** %outName.addr, align 4, !tbaa !5
  store i8* null, i8** %1, align 4, !tbaa !5
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %3 = load i32, i32* %i, align 4, !tbaa !7
  %cmp = icmp slt i32 %3, 11
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %4 = load float*, float** %inPrimaries.addr, align 4, !tbaa !5
  %5 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds [11 x %struct.avifColorPrimariesTable], [11 x %struct.avifColorPrimariesTable]* @avifColorPrimariesTables, i32 0, i32 %5
  %primaries = getelementptr inbounds %struct.avifColorPrimariesTable, %struct.avifColorPrimariesTable* %arrayidx, i32 0, i32 2
  %arraydecay = getelementptr inbounds [8 x float], [8 x float]* %primaries, i32 0, i32 0
  %call = call i32 @primariesMatch(float* %4, float* %arraydecay)
  %tobool1 = icmp ne i32 %call, 0
  br i1 %tobool1, label %if.then2, label %if.end8

if.then2:                                         ; preds = %for.body
  %6 = load i8**, i8*** %outName.addr, align 4, !tbaa !5
  %tobool3 = icmp ne i8** %6, null
  br i1 %tobool3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.then2
  %7 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx5 = getelementptr inbounds [11 x %struct.avifColorPrimariesTable], [11 x %struct.avifColorPrimariesTable]* @avifColorPrimariesTables, i32 0, i32 %7
  %name = getelementptr inbounds %struct.avifColorPrimariesTable, %struct.avifColorPrimariesTable* %arrayidx5, i32 0, i32 1
  %8 = load i8*, i8** %name, align 4, !tbaa !11
  %9 = load i8**, i8*** %outName.addr, align 4, !tbaa !5
  store i8* %8, i8** %9, align 4, !tbaa !5
  br label %if.end6

if.end6:                                          ; preds = %if.then4, %if.then2
  %10 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx7 = getelementptr inbounds [11 x %struct.avifColorPrimariesTable], [11 x %struct.avifColorPrimariesTable]* @avifColorPrimariesTables, i32 0, i32 %10
  %colorPrimariesEnum = getelementptr inbounds %struct.avifColorPrimariesTable, %struct.avifColorPrimariesTable* %arrayidx7, i32 0, i32 0
  %11 = load i32, i32* %colorPrimariesEnum, align 8, !tbaa !9
  store i32 %11, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end8
  %12 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond

cleanup:                                          ; preds = %if.end6, %for.cond.cleanup
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 2, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %cleanup
  %14 = load i32, i32* %retval, align 4
  ret i32 %14

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal i32 @primariesMatch(float* %p1, float* %p2) #0 {
entry:
  %p1.addr = alloca float*, align 4
  %p2.addr = alloca float*, align 4
  store float* %p1, float** %p1.addr, align 4, !tbaa !5
  store float* %p2, float** %p2.addr, align 4, !tbaa !5
  %0 = load float*, float** %p1.addr, align 4, !tbaa !5
  %arrayidx = getelementptr inbounds float, float* %0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !12
  %2 = load float*, float** %p2.addr, align 4, !tbaa !5
  %arrayidx1 = getelementptr inbounds float, float* %2, i32 0
  %3 = load float, float* %arrayidx1, align 4, !tbaa !12
  %call = call i32 @matchesTo3RoundedPlaces(float %1, float %3)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %4 = load float*, float** %p1.addr, align 4, !tbaa !5
  %arrayidx2 = getelementptr inbounds float, float* %4, i32 1
  %5 = load float, float* %arrayidx2, align 4, !tbaa !12
  %6 = load float*, float** %p2.addr, align 4, !tbaa !5
  %arrayidx3 = getelementptr inbounds float, float* %6, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !12
  %call4 = call i32 @matchesTo3RoundedPlaces(float %5, float %7)
  %tobool5 = icmp ne i32 %call4, 0
  br i1 %tobool5, label %land.lhs.true6, label %land.end

land.lhs.true6:                                   ; preds = %land.lhs.true
  %8 = load float*, float** %p1.addr, align 4, !tbaa !5
  %arrayidx7 = getelementptr inbounds float, float* %8, i32 2
  %9 = load float, float* %arrayidx7, align 4, !tbaa !12
  %10 = load float*, float** %p2.addr, align 4, !tbaa !5
  %arrayidx8 = getelementptr inbounds float, float* %10, i32 2
  %11 = load float, float* %arrayidx8, align 4, !tbaa !12
  %call9 = call i32 @matchesTo3RoundedPlaces(float %9, float %11)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %land.lhs.true11, label %land.end

land.lhs.true11:                                  ; preds = %land.lhs.true6
  %12 = load float*, float** %p1.addr, align 4, !tbaa !5
  %arrayidx12 = getelementptr inbounds float, float* %12, i32 3
  %13 = load float, float* %arrayidx12, align 4, !tbaa !12
  %14 = load float*, float** %p2.addr, align 4, !tbaa !5
  %arrayidx13 = getelementptr inbounds float, float* %14, i32 3
  %15 = load float, float* %arrayidx13, align 4, !tbaa !12
  %call14 = call i32 @matchesTo3RoundedPlaces(float %13, float %15)
  %tobool15 = icmp ne i32 %call14, 0
  br i1 %tobool15, label %land.lhs.true16, label %land.end

land.lhs.true16:                                  ; preds = %land.lhs.true11
  %16 = load float*, float** %p1.addr, align 4, !tbaa !5
  %arrayidx17 = getelementptr inbounds float, float* %16, i32 4
  %17 = load float, float* %arrayidx17, align 4, !tbaa !12
  %18 = load float*, float** %p2.addr, align 4, !tbaa !5
  %arrayidx18 = getelementptr inbounds float, float* %18, i32 4
  %19 = load float, float* %arrayidx18, align 4, !tbaa !12
  %call19 = call i32 @matchesTo3RoundedPlaces(float %17, float %19)
  %tobool20 = icmp ne i32 %call19, 0
  br i1 %tobool20, label %land.lhs.true21, label %land.end

land.lhs.true21:                                  ; preds = %land.lhs.true16
  %20 = load float*, float** %p1.addr, align 4, !tbaa !5
  %arrayidx22 = getelementptr inbounds float, float* %20, i32 5
  %21 = load float, float* %arrayidx22, align 4, !tbaa !12
  %22 = load float*, float** %p2.addr, align 4, !tbaa !5
  %arrayidx23 = getelementptr inbounds float, float* %22, i32 5
  %23 = load float, float* %arrayidx23, align 4, !tbaa !12
  %call24 = call i32 @matchesTo3RoundedPlaces(float %21, float %23)
  %tobool25 = icmp ne i32 %call24, 0
  br i1 %tobool25, label %land.lhs.true26, label %land.end

land.lhs.true26:                                  ; preds = %land.lhs.true21
  %24 = load float*, float** %p1.addr, align 4, !tbaa !5
  %arrayidx27 = getelementptr inbounds float, float* %24, i32 6
  %25 = load float, float* %arrayidx27, align 4, !tbaa !12
  %26 = load float*, float** %p2.addr, align 4, !tbaa !5
  %arrayidx28 = getelementptr inbounds float, float* %26, i32 6
  %27 = load float, float* %arrayidx28, align 4, !tbaa !12
  %call29 = call i32 @matchesTo3RoundedPlaces(float %25, float %27)
  %tobool30 = icmp ne i32 %call29, 0
  br i1 %tobool30, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true26
  %28 = load float*, float** %p1.addr, align 4, !tbaa !5
  %arrayidx31 = getelementptr inbounds float, float* %28, i32 7
  %29 = load float, float* %arrayidx31, align 4, !tbaa !12
  %30 = load float*, float** %p2.addr, align 4, !tbaa !5
  %arrayidx32 = getelementptr inbounds float, float* %30, i32 7
  %31 = load float, float* %arrayidx32, align 4, !tbaa !12
  %call33 = call i32 @matchesTo3RoundedPlaces(float %29, float %31)
  %tobool34 = icmp ne i32 %call33, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true26, %land.lhs.true21, %land.lhs.true16, %land.lhs.true11, %land.lhs.true6, %land.lhs.true, %entry
  %32 = phi i1 [ false, %land.lhs.true26 ], [ false, %land.lhs.true21 ], [ false, %land.lhs.true16 ], [ false, %land.lhs.true11 ], [ false, %land.lhs.true6 ], [ false, %land.lhs.true ], [ false, %entry ], [ %tobool34, %land.rhs ]
  %land.ext = zext i1 %32 to i32
  ret i32 %land.ext
}

; Function Attrs: nounwind
define hidden void @avifCalcYUVCoefficients(%struct.avifImage* %image, float* %outR, float* %outG, float* %outB) #0 {
entry:
  %image.addr = alloca %struct.avifImage*, align 4
  %outR.addr = alloca float*, align 4
  %outG.addr = alloca float*, align 4
  %outB.addr = alloca float*, align 4
  %kr = alloca float, align 4
  %kb = alloca float, align 4
  %kg = alloca float, align 4
  %coeffs = alloca [3 x float], align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !5
  store float* %outR, float** %outR.addr, align 4, !tbaa !5
  store float* %outG, float** %outG.addr, align 4, !tbaa !5
  store float* %outB, float** %outB.addr, align 4, !tbaa !5
  %0 = bitcast float* %kr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store float 0x3FCB367A00000000, float* %kr, align 4, !tbaa !12
  %1 = bitcast float* %kb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  store float 0x3FB27BB300000000, float* %kb, align 4, !tbaa !12
  %2 = bitcast float* %kg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load float, float* %kr, align 4, !tbaa !12
  %sub = fsub float 1.000000e+00, %3
  %4 = load float, float* %kb, align 4, !tbaa !12
  %sub1 = fsub float %sub, %4
  store float %sub1, float* %kg, align 4, !tbaa !12
  %5 = bitcast [3 x float]* %coeffs to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %5) #3
  %6 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %arraydecay = getelementptr inbounds [3 x float], [3 x float]* %coeffs, i32 0, i32 0
  %call = call i32 @calcYUVInfoFromCICP(%struct.avifImage* %6, float* %arraydecay)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %arrayidx = getelementptr inbounds [3 x float], [3 x float]* %coeffs, i32 0, i32 0
  %7 = load float, float* %arrayidx, align 4, !tbaa !12
  store float %7, float* %kr, align 4, !tbaa !12
  %arrayidx2 = getelementptr inbounds [3 x float], [3 x float]* %coeffs, i32 0, i32 1
  %8 = load float, float* %arrayidx2, align 4, !tbaa !12
  store float %8, float* %kg, align 4, !tbaa !12
  %arrayidx3 = getelementptr inbounds [3 x float], [3 x float]* %coeffs, i32 0, i32 2
  %9 = load float, float* %arrayidx3, align 4, !tbaa !12
  store float %9, float* %kb, align 4, !tbaa !12
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = load float, float* %kr, align 4, !tbaa !12
  %11 = load float*, float** %outR.addr, align 4, !tbaa !5
  store float %10, float* %11, align 4, !tbaa !12
  %12 = load float, float* %kg, align 4, !tbaa !12
  %13 = load float*, float** %outG.addr, align 4, !tbaa !5
  store float %12, float* %13, align 4, !tbaa !12
  %14 = load float, float* %kb, align 4, !tbaa !12
  %15 = load float*, float** %outB.addr, align 4, !tbaa !5
  store float %14, float* %15, align 4, !tbaa !12
  %16 = bitcast [3 x float]* %coeffs to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %16) #3
  %17 = bitcast float* %kg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #3
  %18 = bitcast float* %kb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #3
  %19 = bitcast float* %kr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #3
  ret void
}

; Function Attrs: nounwind
define internal i32 @calcYUVInfoFromCICP(%struct.avifImage* %image, float* %coeffs) #0 {
entry:
  %retval = alloca i32, align 4
  %image.addr = alloca %struct.avifImage*, align 4
  %coeffs.addr = alloca float*, align 4
  %primaries = alloca [8 x float], align 16
  %rX = alloca float, align 4
  %rY = alloca float, align 4
  %gX = alloca float, align 4
  %gY = alloca float, align 4
  %bX = alloca float, align 4
  %bY = alloca float, align 4
  %wX = alloca float, align 4
  %wY = alloca float, align 4
  %rZ = alloca float, align 4
  %gZ = alloca float, align 4
  %bZ = alloca float, align 4
  %wZ = alloca float, align 4
  %kr = alloca float, align 4
  %kb = alloca float, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %table = alloca %struct.avifMatrixCoefficientsTable*, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !5
  store float* %coeffs, float** %coeffs.addr, align 4, !tbaa !5
  %0 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %matrixCoefficients = getelementptr inbounds %struct.avifImage, %struct.avifImage* %0, i32 0, i32 16
  %1 = load i32, i32* %matrixCoefficients, align 4, !tbaa !14
  %cmp = icmp eq i32 %1, 12
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = bitcast [8 x float]* %primaries to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %2) #3
  %3 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %colorPrimaries = getelementptr inbounds %struct.avifImage, %struct.avifImage* %3, i32 0, i32 14
  %4 = load i32, i32* %colorPrimaries, align 4, !tbaa !22
  %arraydecay = getelementptr inbounds [8 x float], [8 x float]* %primaries, i32 0, i32 0
  call void @avifColorPrimariesGetValues(i32 %4, float* %arraydecay)
  %5 = bitcast float* %rX to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %arrayidx = getelementptr inbounds [8 x float], [8 x float]* %primaries, i32 0, i32 0
  %6 = load float, float* %arrayidx, align 16, !tbaa !12
  store float %6, float* %rX, align 4, !tbaa !12
  %7 = bitcast float* %rY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %arrayidx1 = getelementptr inbounds [8 x float], [8 x float]* %primaries, i32 0, i32 1
  %8 = load float, float* %arrayidx1, align 4, !tbaa !12
  store float %8, float* %rY, align 4, !tbaa !12
  %9 = bitcast float* %gX to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %arrayidx2 = getelementptr inbounds [8 x float], [8 x float]* %primaries, i32 0, i32 2
  %10 = load float, float* %arrayidx2, align 8, !tbaa !12
  store float %10, float* %gX, align 4, !tbaa !12
  %11 = bitcast float* %gY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %arrayidx3 = getelementptr inbounds [8 x float], [8 x float]* %primaries, i32 0, i32 3
  %12 = load float, float* %arrayidx3, align 4, !tbaa !12
  store float %12, float* %gY, align 4, !tbaa !12
  %13 = bitcast float* %bX to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %arrayidx4 = getelementptr inbounds [8 x float], [8 x float]* %primaries, i32 0, i32 4
  %14 = load float, float* %arrayidx4, align 16, !tbaa !12
  store float %14, float* %bX, align 4, !tbaa !12
  %15 = bitcast float* %bY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %arrayidx5 = getelementptr inbounds [8 x float], [8 x float]* %primaries, i32 0, i32 5
  %16 = load float, float* %arrayidx5, align 4, !tbaa !12
  store float %16, float* %bY, align 4, !tbaa !12
  %17 = bitcast float* %wX to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %arrayidx6 = getelementptr inbounds [8 x float], [8 x float]* %primaries, i32 0, i32 6
  %18 = load float, float* %arrayidx6, align 8, !tbaa !12
  store float %18, float* %wX, align 4, !tbaa !12
  %19 = bitcast float* %wY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %arrayidx7 = getelementptr inbounds [8 x float], [8 x float]* %primaries, i32 0, i32 7
  %20 = load float, float* %arrayidx7, align 4, !tbaa !12
  store float %20, float* %wY, align 4, !tbaa !12
  %21 = bitcast float* %rZ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #3
  %22 = load float, float* %rX, align 4, !tbaa !12
  %23 = load float, float* %rY, align 4, !tbaa !12
  %add = fadd float %22, %23
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %rZ, align 4, !tbaa !12
  %24 = bitcast float* %gZ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #3
  %25 = load float, float* %gX, align 4, !tbaa !12
  %26 = load float, float* %gY, align 4, !tbaa !12
  %add8 = fadd float %25, %26
  %sub9 = fsub float 1.000000e+00, %add8
  store float %sub9, float* %gZ, align 4, !tbaa !12
  %27 = bitcast float* %bZ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #3
  %28 = load float, float* %bX, align 4, !tbaa !12
  %29 = load float, float* %bY, align 4, !tbaa !12
  %add10 = fadd float %28, %29
  %sub11 = fsub float 1.000000e+00, %add10
  store float %sub11, float* %bZ, align 4, !tbaa !12
  %30 = bitcast float* %wZ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #3
  %31 = load float, float* %wX, align 4, !tbaa !12
  %32 = load float, float* %wY, align 4, !tbaa !12
  %add12 = fadd float %31, %32
  %sub13 = fsub float 1.000000e+00, %add12
  store float %sub13, float* %wZ, align 4, !tbaa !12
  %33 = bitcast float* %kr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #3
  %34 = load float, float* %rY, align 4, !tbaa !12
  %35 = load float, float* %wX, align 4, !tbaa !12
  %36 = load float, float* %gY, align 4, !tbaa !12
  %37 = load float, float* %bZ, align 4, !tbaa !12
  %mul = fmul float %36, %37
  %38 = load float, float* %bY, align 4, !tbaa !12
  %39 = load float, float* %gZ, align 4, !tbaa !12
  %mul14 = fmul float %38, %39
  %sub15 = fsub float %mul, %mul14
  %mul16 = fmul float %35, %sub15
  %40 = load float, float* %wY, align 4, !tbaa !12
  %41 = load float, float* %bX, align 4, !tbaa !12
  %42 = load float, float* %gZ, align 4, !tbaa !12
  %mul17 = fmul float %41, %42
  %43 = load float, float* %gX, align 4, !tbaa !12
  %44 = load float, float* %bZ, align 4, !tbaa !12
  %mul18 = fmul float %43, %44
  %sub19 = fsub float %mul17, %mul18
  %mul20 = fmul float %40, %sub19
  %add21 = fadd float %mul16, %mul20
  %45 = load float, float* %wZ, align 4, !tbaa !12
  %46 = load float, float* %gX, align 4, !tbaa !12
  %47 = load float, float* %bY, align 4, !tbaa !12
  %mul22 = fmul float %46, %47
  %48 = load float, float* %bX, align 4, !tbaa !12
  %49 = load float, float* %gY, align 4, !tbaa !12
  %mul23 = fmul float %48, %49
  %sub24 = fsub float %mul22, %mul23
  %mul25 = fmul float %45, %sub24
  %add26 = fadd float %add21, %mul25
  %mul27 = fmul float %34, %add26
  %50 = load float, float* %wY, align 4, !tbaa !12
  %51 = load float, float* %rX, align 4, !tbaa !12
  %52 = load float, float* %gY, align 4, !tbaa !12
  %53 = load float, float* %bZ, align 4, !tbaa !12
  %mul28 = fmul float %52, %53
  %54 = load float, float* %bY, align 4, !tbaa !12
  %55 = load float, float* %gZ, align 4, !tbaa !12
  %mul29 = fmul float %54, %55
  %sub30 = fsub float %mul28, %mul29
  %mul31 = fmul float %51, %sub30
  %56 = load float, float* %gX, align 4, !tbaa !12
  %57 = load float, float* %bY, align 4, !tbaa !12
  %58 = load float, float* %rZ, align 4, !tbaa !12
  %mul32 = fmul float %57, %58
  %59 = load float, float* %rY, align 4, !tbaa !12
  %60 = load float, float* %bZ, align 4, !tbaa !12
  %mul33 = fmul float %59, %60
  %sub34 = fsub float %mul32, %mul33
  %mul35 = fmul float %56, %sub34
  %add36 = fadd float %mul31, %mul35
  %61 = load float, float* %bX, align 4, !tbaa !12
  %62 = load float, float* %rY, align 4, !tbaa !12
  %63 = load float, float* %gZ, align 4, !tbaa !12
  %mul37 = fmul float %62, %63
  %64 = load float, float* %gY, align 4, !tbaa !12
  %65 = load float, float* %rZ, align 4, !tbaa !12
  %mul38 = fmul float %64, %65
  %sub39 = fsub float %mul37, %mul38
  %mul40 = fmul float %61, %sub39
  %add41 = fadd float %add36, %mul40
  %mul42 = fmul float %50, %add41
  %div = fdiv float %mul27, %mul42
  store float %div, float* %kr, align 4, !tbaa !12
  %66 = bitcast float* %kb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #3
  %67 = load float, float* %bY, align 4, !tbaa !12
  %68 = load float, float* %wX, align 4, !tbaa !12
  %69 = load float, float* %rY, align 4, !tbaa !12
  %70 = load float, float* %gZ, align 4, !tbaa !12
  %mul43 = fmul float %69, %70
  %71 = load float, float* %gY, align 4, !tbaa !12
  %72 = load float, float* %rZ, align 4, !tbaa !12
  %mul44 = fmul float %71, %72
  %sub45 = fsub float %mul43, %mul44
  %mul46 = fmul float %68, %sub45
  %73 = load float, float* %wY, align 4, !tbaa !12
  %74 = load float, float* %gX, align 4, !tbaa !12
  %75 = load float, float* %rZ, align 4, !tbaa !12
  %mul47 = fmul float %74, %75
  %76 = load float, float* %rX, align 4, !tbaa !12
  %77 = load float, float* %gZ, align 4, !tbaa !12
  %mul48 = fmul float %76, %77
  %sub49 = fsub float %mul47, %mul48
  %mul50 = fmul float %73, %sub49
  %add51 = fadd float %mul46, %mul50
  %78 = load float, float* %wZ, align 4, !tbaa !12
  %79 = load float, float* %rX, align 4, !tbaa !12
  %80 = load float, float* %gY, align 4, !tbaa !12
  %mul52 = fmul float %79, %80
  %81 = load float, float* %gX, align 4, !tbaa !12
  %82 = load float, float* %rY, align 4, !tbaa !12
  %mul53 = fmul float %81, %82
  %sub54 = fsub float %mul52, %mul53
  %mul55 = fmul float %78, %sub54
  %add56 = fadd float %add51, %mul55
  %mul57 = fmul float %67, %add56
  %83 = load float, float* %wY, align 4, !tbaa !12
  %84 = load float, float* %rX, align 4, !tbaa !12
  %85 = load float, float* %gY, align 4, !tbaa !12
  %86 = load float, float* %bZ, align 4, !tbaa !12
  %mul58 = fmul float %85, %86
  %87 = load float, float* %bY, align 4, !tbaa !12
  %88 = load float, float* %gZ, align 4, !tbaa !12
  %mul59 = fmul float %87, %88
  %sub60 = fsub float %mul58, %mul59
  %mul61 = fmul float %84, %sub60
  %89 = load float, float* %gX, align 4, !tbaa !12
  %90 = load float, float* %bY, align 4, !tbaa !12
  %91 = load float, float* %rZ, align 4, !tbaa !12
  %mul62 = fmul float %90, %91
  %92 = load float, float* %rY, align 4, !tbaa !12
  %93 = load float, float* %bZ, align 4, !tbaa !12
  %mul63 = fmul float %92, %93
  %sub64 = fsub float %mul62, %mul63
  %mul65 = fmul float %89, %sub64
  %add66 = fadd float %mul61, %mul65
  %94 = load float, float* %bX, align 4, !tbaa !12
  %95 = load float, float* %rY, align 4, !tbaa !12
  %96 = load float, float* %gZ, align 4, !tbaa !12
  %mul67 = fmul float %95, %96
  %97 = load float, float* %gY, align 4, !tbaa !12
  %98 = load float, float* %rZ, align 4, !tbaa !12
  %mul68 = fmul float %97, %98
  %sub69 = fsub float %mul67, %mul68
  %mul70 = fmul float %94, %sub69
  %add71 = fadd float %add66, %mul70
  %mul72 = fmul float %83, %add71
  %div73 = fdiv float %mul57, %mul72
  store float %div73, float* %kb, align 4, !tbaa !12
  %99 = load float, float* %kr, align 4, !tbaa !12
  %100 = load float*, float** %coeffs.addr, align 4, !tbaa !5
  %arrayidx74 = getelementptr inbounds float, float* %100, i32 0
  store float %99, float* %arrayidx74, align 4, !tbaa !12
  %101 = load float, float* %kb, align 4, !tbaa !12
  %102 = load float*, float** %coeffs.addr, align 4, !tbaa !5
  %arrayidx75 = getelementptr inbounds float, float* %102, i32 2
  store float %101, float* %arrayidx75, align 4, !tbaa !12
  %103 = load float*, float** %coeffs.addr, align 4, !tbaa !5
  %arrayidx76 = getelementptr inbounds float, float* %103, i32 0
  %104 = load float, float* %arrayidx76, align 4, !tbaa !12
  %sub77 = fsub float 1.000000e+00, %104
  %105 = load float*, float** %coeffs.addr, align 4, !tbaa !5
  %arrayidx78 = getelementptr inbounds float, float* %105, i32 2
  %106 = load float, float* %arrayidx78, align 4, !tbaa !12
  %sub79 = fsub float %sub77, %106
  %107 = load float*, float** %coeffs.addr, align 4, !tbaa !5
  %arrayidx80 = getelementptr inbounds float, float* %107, i32 1
  store float %sub79, float* %arrayidx80, align 4, !tbaa !12
  store i32 1, i32* %retval, align 4
  %108 = bitcast float* %kb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #3
  %109 = bitcast float* %kr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #3
  %110 = bitcast float* %wZ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #3
  %111 = bitcast float* %bZ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #3
  %112 = bitcast float* %gZ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #3
  %113 = bitcast float* %rZ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #3
  %114 = bitcast float* %wY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #3
  %115 = bitcast float* %wX to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #3
  %116 = bitcast float* %bY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #3
  %117 = bitcast float* %bX to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #3
  %118 = bitcast float* %gY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #3
  %119 = bitcast float* %gX to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #3
  %120 = bitcast float* %rY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #3
  %121 = bitcast float* %rX to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #3
  %122 = bitcast [8 x float]* %primaries to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %122) #3
  br label %return

if.else:                                          ; preds = %entry
  %123 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %123) #3
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %124 = load i32, i32* %i, align 4, !tbaa !7
  %cmp81 = icmp slt i32 %124, 6
  br i1 %cmp81, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup95

for.body:                                         ; preds = %for.cond
  %125 = bitcast %struct.avifMatrixCoefficientsTable** %table to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %125) #3
  %126 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx82 = getelementptr inbounds [6 x %struct.avifMatrixCoefficientsTable], [6 x %struct.avifMatrixCoefficientsTable]* @matrixCoefficientsTables, i32 0, i32 %126
  store %struct.avifMatrixCoefficientsTable* %arrayidx82, %struct.avifMatrixCoefficientsTable** %table, align 4, !tbaa !5
  %127 = load %struct.avifMatrixCoefficientsTable*, %struct.avifMatrixCoefficientsTable** %table, align 4, !tbaa !5
  %matrixCoefficientsEnum = getelementptr inbounds %struct.avifMatrixCoefficientsTable, %struct.avifMatrixCoefficientsTable* %127, i32 0, i32 0
  %128 = load i32, i32* %matrixCoefficientsEnum, align 4, !tbaa !23
  %129 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %matrixCoefficients83 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %129, i32 0, i32 16
  %130 = load i32, i32* %matrixCoefficients83, align 4, !tbaa !14
  %cmp84 = icmp eq i32 %128, %130
  br i1 %cmp84, label %if.then85, label %if.end

if.then85:                                        ; preds = %for.body
  %131 = load %struct.avifMatrixCoefficientsTable*, %struct.avifMatrixCoefficientsTable** %table, align 4, !tbaa !5
  %kr86 = getelementptr inbounds %struct.avifMatrixCoefficientsTable, %struct.avifMatrixCoefficientsTable* %131, i32 0, i32 2
  %132 = load float, float* %kr86, align 4, !tbaa !25
  %133 = load float*, float** %coeffs.addr, align 4, !tbaa !5
  %arrayidx87 = getelementptr inbounds float, float* %133, i32 0
  store float %132, float* %arrayidx87, align 4, !tbaa !12
  %134 = load %struct.avifMatrixCoefficientsTable*, %struct.avifMatrixCoefficientsTable** %table, align 4, !tbaa !5
  %kb88 = getelementptr inbounds %struct.avifMatrixCoefficientsTable, %struct.avifMatrixCoefficientsTable* %134, i32 0, i32 3
  %135 = load float, float* %kb88, align 4, !tbaa !26
  %136 = load float*, float** %coeffs.addr, align 4, !tbaa !5
  %arrayidx89 = getelementptr inbounds float, float* %136, i32 2
  store float %135, float* %arrayidx89, align 4, !tbaa !12
  %137 = load float*, float** %coeffs.addr, align 4, !tbaa !5
  %arrayidx90 = getelementptr inbounds float, float* %137, i32 0
  %138 = load float, float* %arrayidx90, align 4, !tbaa !12
  %sub91 = fsub float 1.000000e+00, %138
  %139 = load float*, float** %coeffs.addr, align 4, !tbaa !5
  %arrayidx92 = getelementptr inbounds float, float* %139, i32 2
  %140 = load float, float* %arrayidx92, align 4, !tbaa !12
  %sub93 = fsub float %sub91, %140
  %141 = load float*, float** %coeffs.addr, align 4, !tbaa !5
  %arrayidx94 = getelementptr inbounds float, float* %141, i32 1
  store float %sub93, float* %arrayidx94, align 4, !tbaa !12
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then85
  %142 = bitcast %struct.avifMatrixCoefficientsTable** %table to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup95 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %143 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add nsw i32 %143, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond

cleanup95:                                        ; preds = %cleanup, %for.cond.cleanup
  %144 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #3
  %cleanup.dest96 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest96, label %unreachable [
    i32 2, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup95
  br label %if.end97

if.end97:                                         ; preds = %for.end
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end97, %cleanup95, %if.then
  %145 = load i32, i32* %retval, align 4
  ret i32 %145

unreachable:                                      ; preds = %cleanup95
  unreachable
}

; Function Attrs: nounwind
define internal i32 @matchesTo3RoundedPlaces(float %a, float %b) #0 {
entry:
  %a.addr = alloca float, align 4
  %b.addr = alloca float, align 4
  store float %a, float* %a.addr, align 4, !tbaa !12
  store float %b, float* %b.addr, align 4, !tbaa !12
  %0 = load float, float* %a.addr, align 4, !tbaa !12
  %1 = load float, float* %b.addr, align 4, !tbaa !12
  %sub = fsub float %0, %1
  %2 = call float @llvm.fabs.f32(float %sub)
  %cmp = fcmp olt float %2, 0x3F50624DE0000000
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind readnone speculatable willreturn }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = !{!6, !6, i64 0}
!6 = !{!"any pointer", !3, i64 0}
!7 = !{!8, !8, i64 0}
!8 = !{!"int", !3, i64 0}
!9 = !{!10, !3, i64 0}
!10 = !{!"avifColorPrimariesTable", !3, i64 0, !6, i64 4, !3, i64 8}
!11 = !{!10, !6, i64 4}
!12 = !{!13, !13, i64 0}
!13 = !{!"float", !3, i64 0}
!14 = !{!15, !3, i64 84}
!15 = !{!"avifImage", !8, i64 0, !8, i64 4, !8, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 36, !8, i64 48, !3, i64 52, !6, i64 56, !8, i64 60, !8, i64 64, !16, i64 68, !3, i64 76, !3, i64 80, !3, i64 84, !8, i64 88, !18, i64 92, !19, i64 100, !20, i64 132, !21, i64 133, !16, i64 136, !16, i64 144}
!16 = !{!"avifRWData", !6, i64 0, !17, i64 4}
!17 = !{!"long", !3, i64 0}
!18 = !{!"avifPixelAspectRatioBox", !8, i64 0, !8, i64 4}
!19 = !{!"avifCleanApertureBox", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28}
!20 = !{!"avifImageRotation", !3, i64 0}
!21 = !{!"avifImageMirror", !3, i64 0}
!22 = !{!15, !3, i64 76}
!23 = !{!24, !3, i64 0}
!24 = !{!"avifMatrixCoefficientsTable", !3, i64 0, !6, i64 4, !13, i64 8, !13, i64 12}
!25 = !{!24, !13, i64 8}
!26 = !{!24, !13, i64 12}
