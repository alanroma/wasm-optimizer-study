; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/third_party/vector/vector.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/third_party/vector/vector.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.Vector = type { i32, i32, i32, i8* }
%struct.Iterator = type { i8*, i32 }

; Function Attrs: nounwind
define hidden i32 @aom_vector_setup(%struct.Vector* %vector, i32 %capacity, i32 %element_size) #0 {
entry:
  %retval = alloca i32, align 4
  %vector.addr = alloca %struct.Vector*, align 4
  %capacity.addr = alloca i32, align 4
  %element_size.addr = alloca i32, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  store i32 %capacity, i32* %capacity.addr, align 4, !tbaa !6
  store i32 %element_size, i32* %element_size.addr, align 4, !tbaa !6
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.Vector* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %1, i32 0, i32 0
  store i32 0, i32* %size, align 4, !tbaa !8
  %2 = load i32, i32* %capacity.addr, align 4, !tbaa !6
  %cmp1 = icmp ugt i32 2, %2
  br i1 %cmp1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %3 = load i32, i32* %capacity.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 2, %cond.true ], [ %3, %cond.false ]
  %4 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %capacity2 = getelementptr inbounds %struct.Vector, %struct.Vector* %4, i32 0, i32 1
  store i32 %cond, i32* %capacity2, align 4, !tbaa !10
  %5 = load i32, i32* %element_size.addr, align 4, !tbaa !6
  %6 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size3 = getelementptr inbounds %struct.Vector, %struct.Vector* %6, i32 0, i32 2
  store i32 %5, i32* %element_size3, align 4, !tbaa !11
  %7 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %capacity4 = getelementptr inbounds %struct.Vector, %struct.Vector* %7, i32 0, i32 1
  %8 = load i32, i32* %capacity4, align 4, !tbaa !10
  %9 = load i32, i32* %element_size.addr, align 4, !tbaa !6
  %mul = mul i32 %8, %9
  %call = call i8* @malloc(i32 %mul)
  %10 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.Vector, %struct.Vector* %10, i32 0, i32 3
  store i8* %call, i8** %data, align 4, !tbaa !12
  %11 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %data5 = getelementptr inbounds %struct.Vector, %struct.Vector* %11, i32 0, i32 3
  %12 = load i8*, i8** %data5, align 4, !tbaa !12
  %cmp6 = icmp eq i8* %12, null
  %13 = zext i1 %cmp6 to i64
  %cond7 = select i1 %cmp6, i32 -1, i32 0
  store i32 %cond7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %cond.end, %if.then
  %14 = load i32, i32* %retval, align 4
  ret i32 %14
}

declare i8* @malloc(i32) #1

; Function Attrs: nounwind
define hidden i32 @aom_vector_copy(%struct.Vector* %destination, %struct.Vector* %source) #0 {
entry:
  %retval = alloca i32, align 4
  %destination.addr = alloca %struct.Vector*, align 4
  %source.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %destination, %struct.Vector** %destination.addr, align 4, !tbaa !2
  store %struct.Vector* %source, %struct.Vector** %source.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.Vector* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %cmp1 = icmp eq %struct.Vector* %1, null
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %2 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %call = call zeroext i1 @aom_vector_is_initialized(%struct.Vector* %2)
  br i1 %call, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end3
  store i32 -1, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end3
  %3 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %call6 = call zeroext i1 @aom_vector_is_initialized(%struct.Vector* %3)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i32 -1, i32* %retval, align 4
  br label %return

if.end8:                                          ; preds = %if.end5
  %4 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %4, i32 0, i32 0
  %5 = load i32, i32* %size, align 4, !tbaa !8
  %6 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %size9 = getelementptr inbounds %struct.Vector, %struct.Vector* %6, i32 0, i32 0
  store i32 %5, i32* %size9, align 4, !tbaa !8
  %7 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %size10 = getelementptr inbounds %struct.Vector, %struct.Vector* %7, i32 0, i32 0
  %8 = load i32, i32* %size10, align 4, !tbaa !8
  %mul = mul i32 %8, 2
  %9 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %capacity = getelementptr inbounds %struct.Vector, %struct.Vector* %9, i32 0, i32 1
  store i32 %mul, i32* %capacity, align 4, !tbaa !10
  %10 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %element_size = getelementptr inbounds %struct.Vector, %struct.Vector* %10, i32 0, i32 2
  %11 = load i32, i32* %element_size, align 4, !tbaa !11
  %12 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %element_size11 = getelementptr inbounds %struct.Vector, %struct.Vector* %12, i32 0, i32 2
  store i32 %11, i32* %element_size11, align 4, !tbaa !11
  %13 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %capacity12 = getelementptr inbounds %struct.Vector, %struct.Vector* %13, i32 0, i32 1
  %14 = load i32, i32* %capacity12, align 4, !tbaa !10
  %15 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %element_size13 = getelementptr inbounds %struct.Vector, %struct.Vector* %15, i32 0, i32 2
  %16 = load i32, i32* %element_size13, align 4, !tbaa !11
  %mul14 = mul i32 %14, %16
  %call15 = call i8* @malloc(i32 %mul14)
  %17 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.Vector, %struct.Vector* %17, i32 0, i32 3
  store i8* %call15, i8** %data, align 4, !tbaa !12
  %18 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %data16 = getelementptr inbounds %struct.Vector, %struct.Vector* %18, i32 0, i32 3
  %19 = load i8*, i8** %data16, align 4, !tbaa !12
  %cmp17 = icmp eq i8* %19, null
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end8
  store i32 -1, i32* %retval, align 4
  br label %return

if.end19:                                         ; preds = %if.end8
  %20 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %data20 = getelementptr inbounds %struct.Vector, %struct.Vector* %20, i32 0, i32 3
  %21 = load i8*, i8** %data20, align 4, !tbaa !12
  %22 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %data21 = getelementptr inbounds %struct.Vector, %struct.Vector* %22, i32 0, i32 3
  %23 = load i8*, i8** %data21, align 4, !tbaa !12
  %24 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %call22 = call i32 @aom_vector_byte_size(%struct.Vector* %24)
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %21, i8* align 1 %23, i32 %call22, i1 false)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end19, %if.then18, %if.then7, %if.then4, %if.then2, %if.then
  %25 = load i32, i32* %retval, align 4
  ret i32 %25
}

; Function Attrs: nounwind
define hidden zeroext i1 @aom_vector_is_initialized(%struct.Vector* %vector) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.Vector, %struct.Vector* %0, i32 0, i32 3
  %1 = load i8*, i8** %data, align 4, !tbaa !12
  %cmp = icmp ne i8* %1, null
  ret i1 %cmp
}

; Function Attrs: nounwind
define hidden i32 @aom_vector_byte_size(%struct.Vector* %vector) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %0, i32 0, i32 0
  %1 = load i32, i32* %size, align 4, !tbaa !8
  %2 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size = getelementptr inbounds %struct.Vector, %struct.Vector* %2, i32 0, i32 2
  %3 = load i32, i32* %element_size, align 4, !tbaa !11
  %mul = mul i32 %1, %3
  ret i32 %mul
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: nounwind
define hidden i32 @aom_vector_copy_assign(%struct.Vector* %destination, %struct.Vector* %source) #0 {
entry:
  %retval = alloca i32, align 4
  %destination.addr = alloca %struct.Vector*, align 4
  %source.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %destination, %struct.Vector** %destination.addr, align 4, !tbaa !2
  store %struct.Vector* %source, %struct.Vector** %source.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.Vector* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %cmp1 = icmp eq %struct.Vector* %1, null
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %2 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %call = call zeroext i1 @aom_vector_is_initialized(%struct.Vector* %2)
  br i1 %call, label %if.end5, label %if.then4

if.then4:                                         ; preds = %if.end3
  store i32 -1, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end3
  %3 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %call6 = call zeroext i1 @aom_vector_is_initialized(%struct.Vector* %3)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i32 -1, i32* %retval, align 4
  br label %return

if.end8:                                          ; preds = %if.end5
  %4 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %call9 = call i32 @aom_vector_destroy(%struct.Vector* %4)
  %5 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %6 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %call10 = call i32 @aom_vector_copy(%struct.Vector* %5, %struct.Vector* %6)
  store i32 %call10, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end8, %if.then7, %if.then4, %if.then2, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: nounwind
define hidden i32 @aom_vector_destroy(%struct.Vector* %vector) #0 {
entry:
  %retval = alloca i32, align 4
  %vector.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.Vector* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.Vector, %struct.Vector* %1, i32 0, i32 3
  %2 = load i8*, i8** %data, align 4, !tbaa !12
  call void @free(i8* %2)
  %3 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.Vector, %struct.Vector* %3, i32 0, i32 3
  store i8* null, i8** %data1, align 4, !tbaa !12
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: nounwind
define hidden i32 @aom_vector_move(%struct.Vector* %destination, %struct.Vector* %source) #0 {
entry:
  %retval = alloca i32, align 4
  %destination.addr = alloca %struct.Vector*, align 4
  %source.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %destination, %struct.Vector** %destination.addr, align 4, !tbaa !2
  store %struct.Vector* %source, %struct.Vector** %source.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.Vector* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %cmp1 = icmp eq %struct.Vector* %1, null
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %2 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %3 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %4 = bitcast %struct.Vector* %2 to i8*
  %5 = bitcast %struct.Vector* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !13
  %6 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.Vector, %struct.Vector* %6, i32 0, i32 3
  store i8* null, i8** %data, align 4, !tbaa !12
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end3, %if.then2, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: nounwind
define hidden i32 @aom_vector_move_assign(%struct.Vector* %destination, %struct.Vector* %source) #0 {
entry:
  %destination.addr = alloca %struct.Vector*, align 4
  %source.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %destination, %struct.Vector** %destination.addr, align 4, !tbaa !2
  store %struct.Vector* %source, %struct.Vector** %source.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %1 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %call = call i32 @aom_vector_swap(%struct.Vector* %0, %struct.Vector* %1)
  %2 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %call1 = call i32 @aom_vector_destroy(%struct.Vector* %2)
  ret i32 %call1
}

; Function Attrs: nounwind
define hidden i32 @aom_vector_swap(%struct.Vector* %destination, %struct.Vector* %source) #0 {
entry:
  %retval = alloca i32, align 4
  %destination.addr = alloca %struct.Vector*, align 4
  %source.addr = alloca %struct.Vector*, align 4
  %temp = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.Vector* %destination, %struct.Vector** %destination.addr, align 4, !tbaa !2
  store %struct.Vector* %source, %struct.Vector** %source.addr, align 4, !tbaa !2
  %0 = bitcast i8** %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.Vector* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %2 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %cmp1 = icmp eq %struct.Vector* %2, null
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %if.end
  %3 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %call = call zeroext i1 @aom_vector_is_initialized(%struct.Vector* %3)
  br i1 %call, label %if.end5, label %if.then4

if.then4:                                         ; preds = %if.end3
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %if.end3
  %4 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %call6 = call zeroext i1 @aom_vector_is_initialized(%struct.Vector* %4)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end5
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end5
  %5 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %5, i32 0, i32 0
  %6 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %size9 = getelementptr inbounds %struct.Vector, %struct.Vector* %6, i32 0, i32 0
  call void @_vector_swap(i32* %size, i32* %size9)
  %7 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %capacity = getelementptr inbounds %struct.Vector, %struct.Vector* %7, i32 0, i32 1
  %8 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %capacity10 = getelementptr inbounds %struct.Vector, %struct.Vector* %8, i32 0, i32 1
  call void @_vector_swap(i32* %capacity, i32* %capacity10)
  %9 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %element_size = getelementptr inbounds %struct.Vector, %struct.Vector* %9, i32 0, i32 2
  %10 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %element_size11 = getelementptr inbounds %struct.Vector, %struct.Vector* %10, i32 0, i32 2
  call void @_vector_swap(i32* %element_size, i32* %element_size11)
  %11 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.Vector, %struct.Vector* %11, i32 0, i32 3
  %12 = load i8*, i8** %data, align 4, !tbaa !12
  store i8* %12, i8** %temp, align 4, !tbaa !2
  %13 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %data12 = getelementptr inbounds %struct.Vector, %struct.Vector* %13, i32 0, i32 3
  %14 = load i8*, i8** %data12, align 4, !tbaa !12
  %15 = load %struct.Vector*, %struct.Vector** %destination.addr, align 4, !tbaa !2
  %data13 = getelementptr inbounds %struct.Vector, %struct.Vector* %15, i32 0, i32 3
  store i8* %14, i8** %data13, align 4, !tbaa !12
  %16 = load i8*, i8** %temp, align 4, !tbaa !2
  %17 = load %struct.Vector*, %struct.Vector** %source.addr, align 4, !tbaa !2
  %data14 = getelementptr inbounds %struct.Vector, %struct.Vector* %17, i32 0, i32 3
  store i8* %16, i8** %data14, align 4, !tbaa !12
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end8, %if.then7, %if.then4, %if.then2, %if.then
  %18 = bitcast i8** %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #4
  %19 = load i32, i32* %retval, align 4
  ret i32 %19
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define internal void @_vector_swap(i32* %first, i32* %second) #0 {
entry:
  %first.addr = alloca i32*, align 4
  %second.addr = alloca i32*, align 4
  %temp = alloca i32, align 4
  store i32* %first, i32** %first.addr, align 4, !tbaa !2
  store i32* %second, i32** %second.addr, align 4, !tbaa !2
  %0 = bitcast i32* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32*, i32** %first.addr, align 4, !tbaa !2
  %2 = load i32, i32* %1, align 4, !tbaa !6
  store i32 %2, i32* %temp, align 4, !tbaa !6
  %3 = load i32*, i32** %second.addr, align 4, !tbaa !2
  %4 = load i32, i32* %3, align 4, !tbaa !6
  %5 = load i32*, i32** %first.addr, align 4, !tbaa !2
  store i32 %4, i32* %5, align 4, !tbaa !6
  %6 = load i32, i32* %temp, align 4, !tbaa !6
  %7 = load i32*, i32** %second.addr, align 4, !tbaa !2
  store i32 %6, i32* %7, align 4, !tbaa !6
  %8 = bitcast i32* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

declare void @free(i8*) #1

; Function Attrs: nounwind
define hidden i32 @aom_vector_push_back(%struct.Vector* %vector, i8* %element) #0 {
entry:
  %retval = alloca i32, align 4
  %vector.addr = alloca %struct.Vector*, align 4
  %element.addr = alloca i8*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  store i8* %element, i8** %element.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %call = call zeroext i1 @_vector_should_grow(%struct.Vector* %0)
  br i1 %call, label %if.then, label %if.end3

if.then:                                          ; preds = %entry
  %1 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %call1 = call i32 @_vector_adjust_capacity(%struct.Vector* %1)
  %cmp = icmp eq i32 %call1, -1
  br i1 %cmp, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end3

if.end3:                                          ; preds = %if.end, %entry
  %2 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %3 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %3, i32 0, i32 0
  %4 = load i32, i32* %size, align 4, !tbaa !8
  %5 = load i8*, i8** %element.addr, align 4, !tbaa !2
  call void @_vector_assign(%struct.Vector* %2, i32 %4, i8* %5)
  %6 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size4 = getelementptr inbounds %struct.Vector, %struct.Vector* %6, i32 0, i32 0
  %7 = load i32, i32* %size4, align 4, !tbaa !8
  %inc = add i32 %7, 1
  store i32 %inc, i32* %size4, align 4, !tbaa !8
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end3, %if.then2
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: nounwind
define internal zeroext i1 @_vector_should_grow(%struct.Vector* %vector) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %0, i32 0, i32 0
  %1 = load i32, i32* %size, align 4, !tbaa !8
  %2 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %capacity = getelementptr inbounds %struct.Vector, %struct.Vector* %2, i32 0, i32 1
  %3 = load i32, i32* %capacity, align 4, !tbaa !10
  %cmp = icmp eq i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: nounwind
define internal i32 @_vector_adjust_capacity(%struct.Vector* %vector) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %1 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %1, i32 0, i32 0
  %2 = load i32, i32* %size, align 4, !tbaa !8
  %mul = mul i32 %2, 2
  %cmp = icmp ugt i32 1, %mul
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size1 = getelementptr inbounds %struct.Vector, %struct.Vector* %3, i32 0, i32 0
  %4 = load i32, i32* %size1, align 4, !tbaa !8
  %mul2 = mul i32 %4, 2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 1, %cond.true ], [ %mul2, %cond.false ]
  %call = call i32 @_vector_reallocate(%struct.Vector* %0, i32 %cond)
  ret i32 %call
}

; Function Attrs: nounwind
define internal void @_vector_assign(%struct.Vector* %vector, i32 %index, i8* %element) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  %index.addr = alloca i32, align 4
  %element.addr = alloca i8*, align 4
  %offset = alloca i8*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  store i8* %element, i8** %element.addr, align 4, !tbaa !2
  %0 = bitcast i8** %offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %2 = load i32, i32* %index.addr, align 4, !tbaa !6
  %call = call i8* @_vector_offset(%struct.Vector* %1, i32 %2)
  store i8* %call, i8** %offset, align 4, !tbaa !2
  %3 = load i8*, i8** %offset, align 4, !tbaa !2
  %4 = load i8*, i8** %element.addr, align 4, !tbaa !2
  %5 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size = getelementptr inbounds %struct.Vector, %struct.Vector* %5, i32 0, i32 2
  %6 = load i32, i32* %element_size, align 4, !tbaa !11
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %3, i8* align 1 %4, i32 %6, i1 false)
  %7 = bitcast i8** %offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  ret void
}

; Function Attrs: nounwind
define hidden i32 @aom_vector_push_front(%struct.Vector* %vector, i8* %element) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  %element.addr = alloca i8*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  store i8* %element, i8** %element.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %element.addr, align 4, !tbaa !2
  %call = call i32 @aom_vector_insert(%struct.Vector* %0, i32 0, i8* %1)
  ret i32 %call
}

; Function Attrs: nounwind
define hidden i32 @aom_vector_insert(%struct.Vector* %vector, i32 %index, i8* %element) #0 {
entry:
  %retval = alloca i32, align 4
  %vector.addr = alloca %struct.Vector*, align 4
  %index.addr = alloca i32, align 4
  %element.addr = alloca i8*, align 4
  %offset = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  store i8* %element, i8** %element.addr, align 4, !tbaa !2
  %0 = bitcast i8** %offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.Vector* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %2 = load i8*, i8** %element.addr, align 4, !tbaa !2
  %cmp1 = icmp eq i8* %2, null
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %if.end
  %3 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size = getelementptr inbounds %struct.Vector, %struct.Vector* %3, i32 0, i32 2
  %4 = load i32, i32* %element_size, align 4, !tbaa !11
  %cmp4 = icmp eq i32 %4, 0
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end6:                                          ; preds = %if.end3
  %5 = load i32, i32* %index.addr, align 4, !tbaa !6
  %6 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %6, i32 0, i32 0
  %7 = load i32, i32* %size, align 4, !tbaa !8
  %cmp7 = icmp ugt i32 %5, %7
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end6
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end9:                                          ; preds = %if.end6
  %8 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %call = call zeroext i1 @_vector_should_grow(%struct.Vector* %8)
  br i1 %call, label %if.then10, label %if.end15

if.then10:                                        ; preds = %if.end9
  %9 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %call11 = call i32 @_vector_adjust_capacity(%struct.Vector* %9)
  %cmp12 = icmp eq i32 %call11, -1
  br i1 %cmp12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %if.then10
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %if.then10
  br label %if.end15

if.end15:                                         ; preds = %if.end14, %if.end9
  %10 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %11 = load i32, i32* %index.addr, align 4, !tbaa !6
  %call16 = call i32 @_vector_move_right(%struct.Vector* %10, i32 %11)
  %cmp17 = icmp eq i32 %call16, -1
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end15
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end19:                                         ; preds = %if.end15
  %12 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %13 = load i32, i32* %index.addr, align 4, !tbaa !6
  %call20 = call i8* @_vector_offset(%struct.Vector* %12, i32 %13)
  store i8* %call20, i8** %offset, align 4, !tbaa !2
  %14 = load i8*, i8** %offset, align 4, !tbaa !2
  %15 = load i8*, i8** %element.addr, align 4, !tbaa !2
  %16 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size21 = getelementptr inbounds %struct.Vector, %struct.Vector* %16, i32 0, i32 2
  %17 = load i32, i32* %element_size21, align 4, !tbaa !11
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %14, i8* align 1 %15, i32 %17, i1 false)
  %18 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size22 = getelementptr inbounds %struct.Vector, %struct.Vector* %18, i32 0, i32 0
  %19 = load i32, i32* %size22, align 4, !tbaa !8
  %inc = add i32 %19, 1
  store i32 %inc, i32* %size22, align 4, !tbaa !8
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end19, %if.then18, %if.then13, %if.then8, %if.then5, %if.then2, %if.then
  %20 = bitcast i8** %offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #4
  %21 = load i32, i32* %retval, align 4
  ret i32 %21
}

; Function Attrs: nounwind
define internal i32 @_vector_move_right(%struct.Vector* %vector, i32 %index) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  %index.addr = alloca i32, align 4
  %offset = alloca i8*, align 4
  %elements_in_bytes = alloca i32, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  %0 = bitcast i8** %offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %2 = load i32, i32* %index.addr, align 4, !tbaa !6
  %call = call i8* @_vector_offset(%struct.Vector* %1, i32 %2)
  store i8* %call, i8** %offset, align 4, !tbaa !2
  %3 = bitcast i32* %elements_in_bytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %4, i32 0, i32 0
  %5 = load i32, i32* %size, align 4, !tbaa !8
  %6 = load i32, i32* %index.addr, align 4, !tbaa !6
  %sub = sub i32 %5, %6
  %7 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size = getelementptr inbounds %struct.Vector, %struct.Vector* %7, i32 0, i32 2
  %8 = load i32, i32* %element_size, align 4, !tbaa !11
  %mul = mul i32 %sub, %8
  store i32 %mul, i32* %elements_in_bytes, align 4, !tbaa !6
  %9 = load i8*, i8** %offset, align 4, !tbaa !2
  %10 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size1 = getelementptr inbounds %struct.Vector, %struct.Vector* %10, i32 0, i32 2
  %11 = load i32, i32* %element_size1, align 4, !tbaa !11
  %add.ptr = getelementptr inbounds i8, i8* %9, i32 %11
  %12 = load i8*, i8** %offset, align 4, !tbaa !2
  %13 = load i32, i32* %elements_in_bytes, align 4, !tbaa !6
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %12, i32 %13, i1 false)
  %14 = bitcast i32* %elements_in_bytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  ret i32 0
}

; Function Attrs: nounwind
define internal i8* @_vector_offset(%struct.Vector* %vector, i32 %index) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  %index.addr = alloca i32, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.Vector, %struct.Vector* %0, i32 0, i32 3
  %1 = load i8*, i8** %data, align 4, !tbaa !12
  %2 = load i32, i32* %index.addr, align 4, !tbaa !6
  %3 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size = getelementptr inbounds %struct.Vector, %struct.Vector* %3, i32 0, i32 2
  %4 = load i32, i32* %element_size, align 4, !tbaa !11
  %mul = mul i32 %2, %4
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %mul
  ret i8* %add.ptr
}

; Function Attrs: nounwind
define hidden i32 @aom_vector_assign(%struct.Vector* %vector, i32 %index, i8* %element) #0 {
entry:
  %retval = alloca i32, align 4
  %vector.addr = alloca %struct.Vector*, align 4
  %index.addr = alloca i32, align 4
  %element.addr = alloca i8*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  store i8* %element, i8** %element.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.Vector* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %element.addr, align 4, !tbaa !2
  %cmp1 = icmp eq i8* %1, null
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %2 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size = getelementptr inbounds %struct.Vector, %struct.Vector* %2, i32 0, i32 2
  %3 = load i32, i32* %element_size, align 4, !tbaa !11
  %cmp4 = icmp eq i32 %3, 0
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i32 -1, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  %4 = load i32, i32* %index.addr, align 4, !tbaa !6
  %5 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %5, i32 0, i32 0
  %6 = load i32, i32* %size, align 4, !tbaa !8
  %cmp7 = icmp uge i32 %4, %6
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end6
  store i32 -1, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end6
  %7 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %8 = load i32, i32* %index.addr, align 4, !tbaa !6
  %9 = load i8*, i8** %element.addr, align 4, !tbaa !2
  call void @_vector_assign(%struct.Vector* %7, i32 %8, i8* %9)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end9, %if.then8, %if.then5, %if.then2, %if.then
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: nounwind
define hidden i32 @aom_vector_pop_back(%struct.Vector* %vector) #0 {
entry:
  %retval = alloca i32, align 4
  %vector.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.Vector* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size = getelementptr inbounds %struct.Vector, %struct.Vector* %1, i32 0, i32 2
  %2 = load i32, i32* %element_size, align 4, !tbaa !11
  %cmp1 = icmp eq i32 %2, 0
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %3 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %3, i32 0, i32 0
  %4 = load i32, i32* %size, align 4, !tbaa !8
  %dec = add i32 %4, -1
  store i32 %dec, i32* %size, align 4, !tbaa !8
  %5 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %call = call zeroext i1 @_vector_should_shrink(%struct.Vector* %5)
  br i1 %call, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.end3
  %6 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %call5 = call i32 @_vector_adjust_capacity(%struct.Vector* %6)
  br label %if.end6

if.end6:                                          ; preds = %if.then4, %if.end3
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end6, %if.then2, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: nounwind
define internal zeroext i1 @_vector_should_shrink(%struct.Vector* %vector) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %0, i32 0, i32 0
  %1 = load i32, i32* %size, align 4, !tbaa !8
  %2 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %capacity = getelementptr inbounds %struct.Vector, %struct.Vector* %2, i32 0, i32 1
  %3 = load i32, i32* %capacity, align 4, !tbaa !10
  %mul = mul i32 %3, 0
  %cmp = icmp eq i32 %1, %mul
  ret i1 %cmp
}

; Function Attrs: nounwind
define hidden i32 @aom_vector_pop_front(%struct.Vector* %vector) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %call = call i32 @aom_vector_erase(%struct.Vector* %0, i32 0)
  ret i32 %call
}

; Function Attrs: nounwind
define hidden i32 @aom_vector_erase(%struct.Vector* %vector, i32 %index) #0 {
entry:
  %retval = alloca i32, align 4
  %vector.addr = alloca %struct.Vector*, align 4
  %index.addr = alloca i32, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.Vector* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size = getelementptr inbounds %struct.Vector, %struct.Vector* %1, i32 0, i32 2
  %2 = load i32, i32* %element_size, align 4, !tbaa !11
  %cmp1 = icmp eq i32 %2, 0
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %3 = load i32, i32* %index.addr, align 4, !tbaa !6
  %4 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %4, i32 0, i32 0
  %5 = load i32, i32* %size, align 4, !tbaa !8
  %cmp4 = icmp uge i32 %3, %5
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i32 -1, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  %6 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %7 = load i32, i32* %index.addr, align 4, !tbaa !6
  call void @_vector_move_left(%struct.Vector* %6, i32 %7)
  %8 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size7 = getelementptr inbounds %struct.Vector, %struct.Vector* %8, i32 0, i32 0
  %9 = load i32, i32* %size7, align 4, !tbaa !8
  %dec = add i32 %9, -1
  store i32 %dec, i32* %size7, align 4, !tbaa !8
  %10 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %capacity = getelementptr inbounds %struct.Vector, %struct.Vector* %10, i32 0, i32 1
  %11 = load i32, i32* %capacity, align 4, !tbaa !10
  %div = udiv i32 %11, 4
  %cmp8 = icmp eq i32 %dec, %div
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %if.end6
  %12 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %call = call i32 @_vector_adjust_capacity(%struct.Vector* %12)
  br label %if.end10

if.end10:                                         ; preds = %if.then9, %if.end6
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end10, %if.then5, %if.then2, %if.then
  %13 = load i32, i32* %retval, align 4
  ret i32 %13
}

; Function Attrs: nounwind
define internal void @_vector_move_left(%struct.Vector* %vector, i32 %index) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  %index.addr = alloca i32, align 4
  %right_elements_in_bytes = alloca i32, align 4
  %offset = alloca i8*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  %0 = bitcast i32* %right_elements_in_bytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %3 = load i32, i32* %index.addr, align 4, !tbaa !6
  %call = call i8* @_vector_offset(%struct.Vector* %2, i32 %3)
  store i8* %call, i8** %offset, align 4, !tbaa !2
  %4 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %4, i32 0, i32 0
  %5 = load i32, i32* %size, align 4, !tbaa !8
  %6 = load i32, i32* %index.addr, align 4, !tbaa !6
  %sub = sub i32 %5, %6
  %sub1 = sub i32 %sub, 1
  %7 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size = getelementptr inbounds %struct.Vector, %struct.Vector* %7, i32 0, i32 2
  %8 = load i32, i32* %element_size, align 4, !tbaa !11
  %mul = mul i32 %sub1, %8
  store i32 %mul, i32* %right_elements_in_bytes, align 4, !tbaa !6
  %9 = load i8*, i8** %offset, align 4, !tbaa !2
  %10 = load i8*, i8** %offset, align 4, !tbaa !2
  %11 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size2 = getelementptr inbounds %struct.Vector, %struct.Vector* %11, i32 0, i32 2
  %12 = load i32, i32* %element_size2, align 4, !tbaa !11
  %add.ptr = getelementptr inbounds i8, i8* %10, i32 %12
  %13 = load i32, i32* %right_elements_in_bytes, align 4, !tbaa !6
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %9, i8* align 1 %add.ptr, i32 %13, i1 false)
  %14 = bitcast i8** %offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i32* %right_elements_in_bytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  ret void
}

; Function Attrs: nounwind
define hidden i32 @aom_vector_clear(%struct.Vector* %vector) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %call = call i32 @aom_vector_resize(%struct.Vector* %0, i32 0)
  ret i32 %call
}

; Function Attrs: nounwind
define hidden i32 @aom_vector_resize(%struct.Vector* %vector, i32 %new_size) #0 {
entry:
  %retval = alloca i32, align 4
  %vector.addr = alloca %struct.Vector*, align 4
  %new_size.addr = alloca i32, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  store i32 %new_size, i32* %new_size.addr, align 4, !tbaa !6
  %0 = load i32, i32* %new_size.addr, align 4, !tbaa !6
  %1 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %capacity = getelementptr inbounds %struct.Vector, %struct.Vector* %1, i32 0, i32 1
  %2 = load i32, i32* %capacity, align 4, !tbaa !10
  %mul = mul i32 %2, 0
  %cmp = icmp ule i32 %0, %mul
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = load i32, i32* %new_size.addr, align 4, !tbaa !6
  %4 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %4, i32 0, i32 0
  store i32 %3, i32* %size, align 4, !tbaa !8
  %5 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %6 = load i32, i32* %new_size.addr, align 4, !tbaa !6
  %mul1 = mul i32 %6, 2
  %call = call i32 @_vector_reallocate(%struct.Vector* %5, i32 %mul1)
  %cmp2 = icmp eq i32 %call, -1
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end13

if.else:                                          ; preds = %entry
  %7 = load i32, i32* %new_size.addr, align 4, !tbaa !6
  %8 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %capacity4 = getelementptr inbounds %struct.Vector, %struct.Vector* %8, i32 0, i32 1
  %9 = load i32, i32* %capacity4, align 4, !tbaa !10
  %cmp5 = icmp ugt i32 %7, %9
  br i1 %cmp5, label %if.then6, label %if.end12

if.then6:                                         ; preds = %if.else
  %10 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %11 = load i32, i32* %new_size.addr, align 4, !tbaa !6
  %mul7 = mul i32 %11, 2
  %call8 = call i32 @_vector_reallocate(%struct.Vector* %10, i32 %mul7)
  %cmp9 = icmp eq i32 %call8, -1
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.then6
  store i32 -1, i32* %retval, align 4
  br label %return

if.end11:                                         ; preds = %if.then6
  br label %if.end12

if.end12:                                         ; preds = %if.end11, %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.end
  %12 = load i32, i32* %new_size.addr, align 4, !tbaa !6
  %13 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size14 = getelementptr inbounds %struct.Vector, %struct.Vector* %13, i32 0, i32 0
  store i32 %12, i32* %size14, align 4, !tbaa !8
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end13, %if.then10, %if.then3
  %14 = load i32, i32* %retval, align 4
  ret i32 %14
}

; Function Attrs: nounwind
define hidden i8* @aom_vector_get(%struct.Vector* %vector, i32 %index) #0 {
entry:
  %retval = alloca i8*, align 4
  %vector.addr = alloca %struct.Vector*, align 4
  %index.addr = alloca i32, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.Vector* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size = getelementptr inbounds %struct.Vector, %struct.Vector* %1, i32 0, i32 2
  %2 = load i32, i32* %element_size, align 4, !tbaa !11
  %cmp1 = icmp eq i32 %2, 0
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i8* null, i8** %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %3 = load i32, i32* %index.addr, align 4, !tbaa !6
  %4 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %4, i32 0, i32 0
  %5 = load i32, i32* %size, align 4, !tbaa !8
  %cmp4 = icmp uge i32 %3, %5
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i8* null, i8** %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  %6 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %7 = load i32, i32* %index.addr, align 4, !tbaa !6
  %call = call i8* @_vector_offset(%struct.Vector* %6, i32 %7)
  store i8* %call, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end6, %if.then5, %if.then2, %if.then
  %8 = load i8*, i8** %retval, align 4
  ret i8* %8
}

; Function Attrs: nounwind
define hidden i8* @aom_vector_const_get(%struct.Vector* %vector, i32 %index) #0 {
entry:
  %retval = alloca i8*, align 4
  %vector.addr = alloca %struct.Vector*, align 4
  %index.addr = alloca i32, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.Vector* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size = getelementptr inbounds %struct.Vector, %struct.Vector* %1, i32 0, i32 2
  %2 = load i32, i32* %element_size, align 4, !tbaa !11
  %cmp1 = icmp eq i32 %2, 0
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i8* null, i8** %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %3 = load i32, i32* %index.addr, align 4, !tbaa !6
  %4 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %4, i32 0, i32 0
  %5 = load i32, i32* %size, align 4, !tbaa !8
  %cmp4 = icmp uge i32 %3, %5
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i8* null, i8** %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  %6 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %7 = load i32, i32* %index.addr, align 4, !tbaa !6
  %call = call i8* @_vector_const_offset(%struct.Vector* %6, i32 %7)
  store i8* %call, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end6, %if.then5, %if.then2, %if.then
  %8 = load i8*, i8** %retval, align 4
  ret i8* %8
}

; Function Attrs: nounwind
define internal i8* @_vector_const_offset(%struct.Vector* %vector, i32 %index) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  %index.addr = alloca i32, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.Vector, %struct.Vector* %0, i32 0, i32 3
  %1 = load i8*, i8** %data, align 4, !tbaa !12
  %2 = load i32, i32* %index.addr, align 4, !tbaa !6
  %3 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size = getelementptr inbounds %struct.Vector, %struct.Vector* %3, i32 0, i32 2
  %4 = load i32, i32* %element_size, align 4, !tbaa !11
  %mul = mul i32 %2, %4
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %mul
  ret i8* %add.ptr
}

; Function Attrs: nounwind
define hidden i8* @aom_vector_front(%struct.Vector* %vector) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %call = call i8* @aom_vector_get(%struct.Vector* %0, i32 0)
  ret i8* %call
}

; Function Attrs: nounwind
define hidden i8* @aom_vector_back(%struct.Vector* %vector) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %1 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %1, i32 0, i32 0
  %2 = load i32, i32* %size, align 4, !tbaa !8
  %sub = sub i32 %2, 1
  %call = call i8* @aom_vector_get(%struct.Vector* %0, i32 %sub)
  ret i8* %call
}

; Function Attrs: nounwind
define hidden i32 @aom_vector_free_space(%struct.Vector* %vector) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %capacity = getelementptr inbounds %struct.Vector, %struct.Vector* %0, i32 0, i32 1
  %1 = load i32, i32* %capacity, align 4, !tbaa !10
  %2 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %2, i32 0, i32 0
  %3 = load i32, i32* %size, align 4, !tbaa !8
  %sub = sub i32 %1, %3
  ret i32 %sub
}

; Function Attrs: nounwind
define hidden zeroext i1 @aom_vector_is_empty(%struct.Vector* %vector) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %0, i32 0, i32 0
  %1 = load i32, i32* %size, align 4, !tbaa !8
  %cmp = icmp eq i32 %1, 0
  ret i1 %cmp
}

; Function Attrs: nounwind
define internal i32 @_vector_reallocate(%struct.Vector* %vector, i32 %new_capacity) #0 {
entry:
  %retval = alloca i32, align 4
  %vector.addr = alloca %struct.Vector*, align 4
  %new_capacity.addr = alloca i32, align 4
  %new_capacity_in_bytes = alloca i32, align 4
  %old = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  store i32 %new_capacity, i32* %new_capacity.addr, align 4, !tbaa !6
  %0 = bitcast i32* %new_capacity_in_bytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %old to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i32, i32* %new_capacity.addr, align 4, !tbaa !6
  %cmp = icmp ult i32 %2, 2
  br i1 %cmp, label %if.then, label %if.end3

if.then:                                          ; preds = %entry
  %3 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %capacity = getelementptr inbounds %struct.Vector, %struct.Vector* %3, i32 0, i32 1
  %4 = load i32, i32* %capacity, align 4, !tbaa !10
  %cmp1 = icmp ugt i32 %4, 2
  br i1 %cmp1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  store i32 2, i32* %new_capacity.addr, align 4, !tbaa !6
  br label %if.end

if.else:                                          ; preds = %if.then
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then2
  br label %if.end3

if.end3:                                          ; preds = %if.end, %entry
  %5 = load i32, i32* %new_capacity.addr, align 4, !tbaa !6
  %6 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size = getelementptr inbounds %struct.Vector, %struct.Vector* %6, i32 0, i32 2
  %7 = load i32, i32* %element_size, align 4, !tbaa !11
  %mul = mul i32 %5, %7
  store i32 %mul, i32* %new_capacity_in_bytes, align 4, !tbaa !6
  %8 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.Vector, %struct.Vector* %8, i32 0, i32 3
  %9 = load i8*, i8** %data, align 4, !tbaa !12
  store i8* %9, i8** %old, align 4, !tbaa !2
  %10 = load i32, i32* %new_capacity_in_bytes, align 4, !tbaa !6
  %call = call i8* @malloc(i32 %10)
  %11 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %data4 = getelementptr inbounds %struct.Vector, %struct.Vector* %11, i32 0, i32 3
  store i8* %call, i8** %data4, align 4, !tbaa !12
  %cmp5 = icmp eq i8* %call, null
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end3
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %if.end3
  %12 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %data8 = getelementptr inbounds %struct.Vector, %struct.Vector* %12, i32 0, i32 3
  %13 = load i8*, i8** %data8, align 4, !tbaa !12
  %14 = load i8*, i8** %old, align 4, !tbaa !2
  %15 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %call9 = call i32 @aom_vector_byte_size(%struct.Vector* %15)
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %13, i8* align 1 %14, i32 %call9, i1 false)
  %16 = load i32, i32* %new_capacity.addr, align 4, !tbaa !6
  %17 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %capacity10 = getelementptr inbounds %struct.Vector, %struct.Vector* %17, i32 0, i32 1
  store i32 %16, i32* %capacity10, align 4, !tbaa !10
  %18 = load i8*, i8** %old, align 4, !tbaa !2
  call void @free(i8* %18)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end7, %if.then6, %if.else
  %19 = bitcast i8** %old to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  %20 = bitcast i32* %new_capacity_in_bytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #4
  %21 = load i32, i32* %retval, align 4
  ret i32 %21
}

; Function Attrs: nounwind
define hidden i32 @aom_vector_reserve(%struct.Vector* %vector, i32 %minimum_capacity) #0 {
entry:
  %retval = alloca i32, align 4
  %vector.addr = alloca %struct.Vector*, align 4
  %minimum_capacity.addr = alloca i32, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  store i32 %minimum_capacity, i32* %minimum_capacity.addr, align 4, !tbaa !6
  %0 = load i32, i32* %minimum_capacity.addr, align 4, !tbaa !6
  %1 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %capacity = getelementptr inbounds %struct.Vector, %struct.Vector* %1, i32 0, i32 1
  %2 = load i32, i32* %capacity, align 4, !tbaa !10
  %cmp = icmp ugt i32 %0, %2
  br i1 %cmp, label %if.then, label %if.end3

if.then:                                          ; preds = %entry
  %3 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %4 = load i32, i32* %minimum_capacity.addr, align 4, !tbaa !6
  %call = call i32 @_vector_reallocate(%struct.Vector* %3, i32 %4)
  %cmp1 = icmp eq i32 %call, -1
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end3

if.end3:                                          ; preds = %if.end, %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end3, %if.then2
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: nounwind
define hidden i32 @aom_vector_shrink_to_fit(%struct.Vector* %vector) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %1 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %1, i32 0, i32 0
  %2 = load i32, i32* %size, align 4, !tbaa !8
  %call = call i32 @_vector_reallocate(%struct.Vector* %0, i32 %2)
  ret i32 %call
}

; Function Attrs: nounwind
define hidden void @aom_vector_begin(%struct.Iterator* noalias sret align 4 %agg.result, %struct.Vector* %vector) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  call void @aom_vector_iterator(%struct.Iterator* sret align 4 %agg.result, %struct.Vector* %0, i32 0)
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_vector_iterator(%struct.Iterator* noalias sret align 4 %agg.result, %struct.Vector* %vector, i32 %index) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  %index.addr = alloca i32, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  %0 = bitcast %struct.Iterator* %agg.result to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %0, i8 0, i32 8, i1 false)
  %1 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.Vector* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %index.addr, align 4, !tbaa !6
  %3 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %3, i32 0, i32 0
  %4 = load i32, i32* %size, align 4, !tbaa !8
  %cmp1 = icmp ugt i32 %2, %4
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  br label %return

if.end3:                                          ; preds = %if.end
  %5 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size = getelementptr inbounds %struct.Vector, %struct.Vector* %5, i32 0, i32 2
  %6 = load i32, i32* %element_size, align 4, !tbaa !11
  %cmp4 = icmp eq i32 %6, 0
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  br label %return

if.end6:                                          ; preds = %if.end3
  %7 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %8 = load i32, i32* %index.addr, align 4, !tbaa !6
  %call = call i8* @_vector_offset(%struct.Vector* %7, i32 %8)
  %pointer = getelementptr inbounds %struct.Iterator, %struct.Iterator* %agg.result, i32 0, i32 0
  store i8* %call, i8** %pointer, align 4, !tbaa !14
  %9 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size7 = getelementptr inbounds %struct.Vector, %struct.Vector* %9, i32 0, i32 2
  %10 = load i32, i32* %element_size7, align 4, !tbaa !11
  %element_size8 = getelementptr inbounds %struct.Iterator, %struct.Iterator* %agg.result, i32 0, i32 1
  store i32 %10, i32* %element_size8, align 4, !tbaa !16
  br label %return

return:                                           ; preds = %if.end6, %if.then5, %if.then2, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_vector_end(%struct.Iterator* noalias sret align 4 %agg.result, %struct.Vector* %vector) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %0 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %1 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.Vector, %struct.Vector* %1, i32 0, i32 0
  %2 = load i32, i32* %size, align 4, !tbaa !8
  call void @aom_vector_iterator(%struct.Iterator* sret align 4 %agg.result, %struct.Vector* %0, i32 %2)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: nounwind
define hidden i8* @aom_iterator_get(%struct.Iterator* %iterator) #0 {
entry:
  %iterator.addr = alloca %struct.Iterator*, align 4
  store %struct.Iterator* %iterator, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  %0 = load %struct.Iterator*, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  %pointer = getelementptr inbounds %struct.Iterator, %struct.Iterator* %0, i32 0, i32 0
  %1 = load i8*, i8** %pointer, align 4, !tbaa !14
  ret i8* %1
}

; Function Attrs: nounwind
define hidden i32 @aom_iterator_erase(%struct.Vector* %vector, %struct.Iterator* %iterator) #0 {
entry:
  %retval = alloca i32, align 4
  %vector.addr = alloca %struct.Vector*, align 4
  %iterator.addr = alloca %struct.Iterator*, align 4
  %index = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %tmp = alloca %struct.Iterator, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  store %struct.Iterator* %iterator, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  %0 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %2 = load %struct.Iterator*, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  %call = call i32 @aom_iterator_index(%struct.Vector* %1, %struct.Iterator* %2)
  store i32 %call, i32* %index, align 4, !tbaa !6
  %3 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %4 = load i32, i32* %index, align 4, !tbaa !6
  %call1 = call i32 @aom_vector_erase(%struct.Vector* %3, i32 %4)
  %cmp = icmp eq i32 %call1, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %5 = load %struct.Iterator*, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  %6 = bitcast %struct.Iterator* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #4
  %7 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %8 = load i32, i32* %index, align 4, !tbaa !6
  call void @aom_vector_iterator(%struct.Iterator* sret align 4 %tmp, %struct.Vector* %7, i32 %8)
  %9 = bitcast %struct.Iterator* %5 to i8*
  %10 = bitcast %struct.Iterator* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 8, i1 false), !tbaa.struct !17
  %11 = bitcast %struct.Iterator* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %11) #4
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %12 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #4
  %13 = load i32, i32* %retval, align 4
  ret i32 %13
}

; Function Attrs: nounwind
define hidden i32 @aom_iterator_index(%struct.Vector* %vector, %struct.Iterator* %iterator) #0 {
entry:
  %vector.addr = alloca %struct.Vector*, align 4
  %iterator.addr = alloca %struct.Iterator*, align 4
  store %struct.Vector* %vector, %struct.Vector** %vector.addr, align 4, !tbaa !2
  store %struct.Iterator* %iterator, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  %0 = load %struct.Iterator*, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  %pointer = getelementptr inbounds %struct.Iterator, %struct.Iterator* %0, i32 0, i32 0
  %1 = load i8*, i8** %pointer, align 4, !tbaa !14
  %2 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.Vector, %struct.Vector* %2, i32 0, i32 3
  %3 = load i8*, i8** %data, align 4, !tbaa !12
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %4 = load %struct.Vector*, %struct.Vector** %vector.addr, align 4, !tbaa !2
  %element_size = getelementptr inbounds %struct.Vector, %struct.Vector* %4, i32 0, i32 2
  %5 = load i32, i32* %element_size, align 4, !tbaa !11
  %div = udiv i32 %sub.ptr.sub, %5
  ret i32 %div
}

; Function Attrs: nounwind
define hidden void @aom_iterator_increment(%struct.Iterator* %iterator) #0 {
entry:
  %iterator.addr = alloca %struct.Iterator*, align 4
  store %struct.Iterator* %iterator, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  %0 = load %struct.Iterator*, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  %pointer = getelementptr inbounds %struct.Iterator, %struct.Iterator* %0, i32 0, i32 0
  %1 = load i8*, i8** %pointer, align 4, !tbaa !14
  %2 = load %struct.Iterator*, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  %element_size = getelementptr inbounds %struct.Iterator, %struct.Iterator* %2, i32 0, i32 1
  %3 = load i32, i32* %element_size, align 4, !tbaa !16
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %3
  %4 = load %struct.Iterator*, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  %pointer1 = getelementptr inbounds %struct.Iterator, %struct.Iterator* %4, i32 0, i32 0
  store i8* %add.ptr, i8** %pointer1, align 4, !tbaa !14
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_iterator_decrement(%struct.Iterator* %iterator) #0 {
entry:
  %iterator.addr = alloca %struct.Iterator*, align 4
  store %struct.Iterator* %iterator, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  %0 = load %struct.Iterator*, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  %pointer = getelementptr inbounds %struct.Iterator, %struct.Iterator* %0, i32 0, i32 0
  %1 = load i8*, i8** %pointer, align 4, !tbaa !14
  %2 = load %struct.Iterator*, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  %element_size = getelementptr inbounds %struct.Iterator, %struct.Iterator* %2, i32 0, i32 1
  %3 = load i32, i32* %element_size, align 4, !tbaa !16
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %idx.neg
  %4 = load %struct.Iterator*, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  %pointer1 = getelementptr inbounds %struct.Iterator, %struct.Iterator* %4, i32 0, i32 0
  store i8* %add.ptr, i8** %pointer1, align 4, !tbaa !14
  ret void
}

; Function Attrs: nounwind
define hidden i8* @aom_iterator_next(%struct.Iterator* %iterator) #0 {
entry:
  %iterator.addr = alloca %struct.Iterator*, align 4
  %current = alloca i8*, align 4
  store %struct.Iterator* %iterator, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  %0 = bitcast i8** %current to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.Iterator*, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  %pointer = getelementptr inbounds %struct.Iterator, %struct.Iterator* %1, i32 0, i32 0
  %2 = load i8*, i8** %pointer, align 4, !tbaa !14
  store i8* %2, i8** %current, align 4, !tbaa !2
  %3 = load %struct.Iterator*, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  call void @aom_iterator_increment(%struct.Iterator* %3)
  %4 = load i8*, i8** %current, align 4, !tbaa !2
  %5 = bitcast i8** %current to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #4
  ret i8* %4
}

; Function Attrs: nounwind
define hidden i8* @aom_iterator_previous(%struct.Iterator* %iterator) #0 {
entry:
  %iterator.addr = alloca %struct.Iterator*, align 4
  %current = alloca i8*, align 4
  store %struct.Iterator* %iterator, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  %0 = bitcast i8** %current to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.Iterator*, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  %pointer = getelementptr inbounds %struct.Iterator, %struct.Iterator* %1, i32 0, i32 0
  %2 = load i8*, i8** %pointer, align 4, !tbaa !14
  store i8* %2, i8** %current, align 4, !tbaa !2
  %3 = load %struct.Iterator*, %struct.Iterator** %iterator.addr, align 4, !tbaa !2
  call void @aom_iterator_decrement(%struct.Iterator* %3)
  %4 = load i8*, i8** %current, align 4, !tbaa !2
  %5 = bitcast i8** %current to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #4
  ret i8* %4
}

; Function Attrs: nounwind
define hidden zeroext i1 @aom_iterator_equals(%struct.Iterator* %first, %struct.Iterator* %second) #0 {
entry:
  %first.addr = alloca %struct.Iterator*, align 4
  %second.addr = alloca %struct.Iterator*, align 4
  store %struct.Iterator* %first, %struct.Iterator** %first.addr, align 4, !tbaa !2
  store %struct.Iterator* %second, %struct.Iterator** %second.addr, align 4, !tbaa !2
  %0 = load %struct.Iterator*, %struct.Iterator** %first.addr, align 4, !tbaa !2
  %pointer = getelementptr inbounds %struct.Iterator, %struct.Iterator* %0, i32 0, i32 0
  %1 = load i8*, i8** %pointer, align 4, !tbaa !14
  %2 = load %struct.Iterator*, %struct.Iterator** %second.addr, align 4, !tbaa !2
  %pointer1 = getelementptr inbounds %struct.Iterator, %struct.Iterator* %2, i32 0, i32 0
  %3 = load i8*, i8** %pointer1, align 4, !tbaa !14
  %cmp = icmp eq i8* %1, %3
  ret i1 %cmp
}

; Function Attrs: nounwind
define hidden zeroext i1 @aom_iterator_is_before(%struct.Iterator* %first, %struct.Iterator* %second) #0 {
entry:
  %first.addr = alloca %struct.Iterator*, align 4
  %second.addr = alloca %struct.Iterator*, align 4
  store %struct.Iterator* %first, %struct.Iterator** %first.addr, align 4, !tbaa !2
  store %struct.Iterator* %second, %struct.Iterator** %second.addr, align 4, !tbaa !2
  %0 = load %struct.Iterator*, %struct.Iterator** %first.addr, align 4, !tbaa !2
  %pointer = getelementptr inbounds %struct.Iterator, %struct.Iterator* %0, i32 0, i32 0
  %1 = load i8*, i8** %pointer, align 4, !tbaa !14
  %2 = load %struct.Iterator*, %struct.Iterator** %second.addr, align 4, !tbaa !2
  %pointer1 = getelementptr inbounds %struct.Iterator, %struct.Iterator* %2, i32 0, i32 0
  %3 = load i8*, i8** %pointer1, align 4, !tbaa !14
  %cmp = icmp ult i8* %1, %3
  ret i1 %cmp
}

; Function Attrs: nounwind
define hidden zeroext i1 @aom_iterator_is_after(%struct.Iterator* %first, %struct.Iterator* %second) #0 {
entry:
  %first.addr = alloca %struct.Iterator*, align 4
  %second.addr = alloca %struct.Iterator*, align 4
  store %struct.Iterator* %first, %struct.Iterator** %first.addr, align 4, !tbaa !2
  store %struct.Iterator* %second, %struct.Iterator** %second.addr, align 4, !tbaa !2
  %0 = load %struct.Iterator*, %struct.Iterator** %first.addr, align 4, !tbaa !2
  %pointer = getelementptr inbounds %struct.Iterator, %struct.Iterator* %0, i32 0, i32 0
  %1 = load i8*, i8** %pointer, align 4, !tbaa !14
  %2 = load %struct.Iterator*, %struct.Iterator** %second.addr, align 4, !tbaa !2
  %pointer1 = getelementptr inbounds %struct.Iterator, %struct.Iterator* %2, i32 0, i32 0
  %3 = load i8*, i8** %pointer1, align 4, !tbaa !14
  %cmp = icmp ugt i8* %1, %3
  ret i1 %cmp
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"long", !4, i64 0}
!8 = !{!9, !7, i64 0}
!9 = !{!"Vector", !7, i64 0, !7, i64 4, !7, i64 8, !3, i64 12}
!10 = !{!9, !7, i64 4}
!11 = !{!9, !7, i64 8}
!12 = !{!9, !3, i64 12}
!13 = !{i64 0, i64 4, !6, i64 4, i64 4, !6, i64 8, i64 4, !6, i64 12, i64 4, !2}
!14 = !{!15, !3, i64 0}
!15 = !{!"Iterator", !3, i64 0, !7, i64 4}
!16 = !{!15, !7, i64 4}
!17 = !{i64 0, i64 4, !2, i64 4, i64 4, !6}
