; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/grain_table.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/grain_table.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.aom_film_grain_table_t = type { %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t* }
%struct.aom_film_grain_table_entry_t = type { %struct.aom_film_grain_t, i64, i64, %struct.aom_film_grain_table_entry_t* }
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct._IO_FILE = type opaque

@.str = private unnamed_addr constant [3 x i8] c"rb\00", align 1
@.str.1 = private unnamed_addr constant [18 x i8] c"Unable to open %s\00", align 1
@kFileMagic = internal constant [8 x i8] c"filmgrn1", align 1
@.str.2 = private unnamed_addr constant [39 x i8] c"Unable to read (or invalid) file magic\00", align 1
@.str.3 = private unnamed_addr constant [3 x i8] c"wb\00", align 1
@.str.4 = private unnamed_addr constant [23 x i8] c"Unable to open file %s\00", align 1
@.str.5 = private unnamed_addr constant [27 x i8] c"Unable to write file magic\00", align 1
@.str.6 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.7 = private unnamed_addr constant [23 x i8] c"E %lld %lld %d %hd %d\0A\00", align 1
@.str.8 = private unnamed_addr constant [42 x i8] c"Unable to read entry header. Read %d != 5\00", align 1
@.str.9 = private unnamed_addr constant [39 x i8] c"p %d %d %d %d %d %d %d %d %d %d %d %d\0A\00", align 1
@.str.10 = private unnamed_addr constant [43 x i8] c"Unable to read entry params. Read %d != 12\00", align 1
@.str.11 = private unnamed_addr constant [8 x i8] c"\09sY %d \00", align 1
@.str.12 = private unnamed_addr constant [28 x i8] c"Unable to read num y points\00", align 1
@.str.13 = private unnamed_addr constant [6 x i8] c"%d %d\00", align 1
@.str.14 = private unnamed_addr constant [32 x i8] c"Unable to read y scaling points\00", align 1
@.str.15 = private unnamed_addr constant [9 x i8] c"\0A\09sCb %d\00", align 1
@.str.16 = private unnamed_addr constant [29 x i8] c"Unable to read num cb points\00", align 1
@.str.17 = private unnamed_addr constant [33 x i8] c"Unable to read cb scaling points\00", align 1
@.str.18 = private unnamed_addr constant [9 x i8] c"\0A\09sCr %d\00", align 1
@.str.19 = private unnamed_addr constant [29 x i8] c"Unable to read num cr points\00", align 1
@.str.20 = private unnamed_addr constant [33 x i8] c"Unable to read cr scaling points\00", align 1
@.str.21 = private unnamed_addr constant [5 x i8] c"\0A\09cY\00", align 1
@.str.22 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.23 = private unnamed_addr constant [24 x i8] c"Unable to read Y coeffs\00", align 1
@.str.24 = private unnamed_addr constant [6 x i8] c"\0A\09cCb\00", align 1
@.str.25 = private unnamed_addr constant [25 x i8] c"Unable to read Cb coeffs\00", align 1
@.str.26 = private unnamed_addr constant [6 x i8] c"\0A\09cCr\00", align 1
@.str.27 = private unnamed_addr constant [25 x i8] c"Unable to read Cr coeffs\00", align 1
@.str.28 = private unnamed_addr constant [22 x i8] c"E %lld %lld %d %d %d\0A\00", align 1
@.str.29 = private unnamed_addr constant [40 x i8] c"\09p %d %d %d %d %d %d %d %d %d %d %d %d\0A\00", align 1
@.str.30 = private unnamed_addr constant [7 x i8] c" %d %d\00", align 1
@.str.31 = private unnamed_addr constant [4 x i8] c" %d\00", align 1

; Function Attrs: nounwind
define hidden void @aom_film_grain_table_append(%struct.aom_film_grain_table_t* %t, i64 %time_stamp, i64 %end_time, %struct.aom_film_grain_t* %grain) #0 {
entry:
  %t.addr = alloca %struct.aom_film_grain_table_t*, align 4
  %time_stamp.addr = alloca i64, align 8
  %end_time.addr = alloca i64, align 8
  %grain.addr = alloca %struct.aom_film_grain_t*, align 4
  %new_tail = alloca %struct.aom_film_grain_table_entry_t*, align 4
  store %struct.aom_film_grain_table_t* %t, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  store i64 %time_stamp, i64* %time_stamp.addr, align 8, !tbaa !6
  store i64 %end_time, i64* %end_time.addr, align 8, !tbaa !6
  store %struct.aom_film_grain_t* %grain, %struct.aom_film_grain_t** %grain.addr, align 4, !tbaa !2
  %0 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %tail = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %0, i32 0, i32 1
  %1 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %tail, align 4, !tbaa !8
  %tobool = icmp ne %struct.aom_film_grain_table_entry_t* %1, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %2 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %grain.addr, align 4, !tbaa !2
  %3 = bitcast %struct.aom_film_grain_t* %2 to i8*
  %4 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %tail1 = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %4, i32 0, i32 1
  %5 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %tail1, align 4, !tbaa !8
  %params = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %5, i32 0, i32 0
  %6 = bitcast %struct.aom_film_grain_t* %params to i8*
  %call = call i32 @memcmp(i8* %3, i8* %6, i32 648)
  %tobool2 = icmp ne i32 %call, 0
  br i1 %tobool2, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %entry
  %7 = bitcast %struct.aom_film_grain_table_entry_t** %new_tail to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %call3 = call i8* @aom_malloc(i32 672)
  %8 = bitcast i8* %call3 to %struct.aom_film_grain_table_entry_t*
  store %struct.aom_film_grain_table_entry_t* %8, %struct.aom_film_grain_table_entry_t** %new_tail, align 4, !tbaa !2
  %9 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %new_tail, align 4, !tbaa !2
  %10 = bitcast %struct.aom_film_grain_table_entry_t* %9 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %10, i8 0, i32 672, i1 false)
  %11 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %tail4 = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %11, i32 0, i32 1
  %12 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %tail4, align 4, !tbaa !8
  %tobool5 = icmp ne %struct.aom_film_grain_table_entry_t* %12, null
  br i1 %tobool5, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.then
  %13 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %new_tail, align 4, !tbaa !2
  %14 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %tail7 = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %14, i32 0, i32 1
  %15 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %tail7, align 4, !tbaa !8
  %next = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %15, i32 0, i32 3
  store %struct.aom_film_grain_table_entry_t* %13, %struct.aom_film_grain_table_entry_t** %next, align 8, !tbaa !10
  br label %if.end

if.end:                                           ; preds = %if.then6, %if.then
  %16 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %head = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %16, i32 0, i32 0
  %17 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %head, align 4, !tbaa !15
  %tobool8 = icmp ne %struct.aom_film_grain_table_entry_t* %17, null
  br i1 %tobool8, label %if.end11, label %if.then9

if.then9:                                         ; preds = %if.end
  %18 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %new_tail, align 4, !tbaa !2
  %19 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %head10 = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %19, i32 0, i32 0
  store %struct.aom_film_grain_table_entry_t* %18, %struct.aom_film_grain_table_entry_t** %head10, align 4, !tbaa !15
  br label %if.end11

if.end11:                                         ; preds = %if.then9, %if.end
  %20 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %new_tail, align 4, !tbaa !2
  %21 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %tail12 = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %21, i32 0, i32 1
  store %struct.aom_film_grain_table_entry_t* %20, %struct.aom_film_grain_table_entry_t** %tail12, align 4, !tbaa !8
  %22 = load i64, i64* %time_stamp.addr, align 8, !tbaa !6
  %23 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %new_tail, align 4, !tbaa !2
  %start_time = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %23, i32 0, i32 1
  store i64 %22, i64* %start_time, align 8, !tbaa !16
  %24 = load i64, i64* %end_time.addr, align 8, !tbaa !6
  %25 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %new_tail, align 4, !tbaa !2
  %end_time13 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %25, i32 0, i32 2
  store i64 %24, i64* %end_time13, align 8, !tbaa !17
  %26 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %new_tail, align 4, !tbaa !2
  %params14 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %26, i32 0, i32 0
  %27 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %grain.addr, align 4, !tbaa !2
  %28 = bitcast %struct.aom_film_grain_t* %params14 to i8*
  %29 = bitcast %struct.aom_film_grain_t* %27 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %28, i8* align 4 %29, i32 648, i1 false), !tbaa.struct !18
  %30 = bitcast %struct.aom_film_grain_table_entry_t** %new_tail to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #4
  br label %if.end32

if.else:                                          ; preds = %lor.lhs.false
  %31 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %tail15 = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %31, i32 0, i32 1
  %32 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %tail15, align 4, !tbaa !8
  %end_time16 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %32, i32 0, i32 2
  %33 = load i64, i64* %end_time16, align 8, !tbaa !17
  %34 = load i64, i64* %end_time.addr, align 8, !tbaa !6
  %cmp = icmp sgt i64 %33, %34
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %35 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %tail17 = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %35, i32 0, i32 1
  %36 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %tail17, align 4, !tbaa !8
  %end_time18 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %36, i32 0, i32 2
  %37 = load i64, i64* %end_time18, align 8, !tbaa !17
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %38 = load i64, i64* %end_time.addr, align 8, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i64 [ %37, %cond.true ], [ %38, %cond.false ]
  %39 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %tail19 = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %39, i32 0, i32 1
  %40 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %tail19, align 4, !tbaa !8
  %end_time20 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %40, i32 0, i32 2
  store i64 %cond, i64* %end_time20, align 8, !tbaa !17
  %41 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %tail21 = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %41, i32 0, i32 1
  %42 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %tail21, align 4, !tbaa !8
  %start_time22 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %42, i32 0, i32 1
  %43 = load i64, i64* %start_time22, align 8, !tbaa !16
  %44 = load i64, i64* %time_stamp.addr, align 8, !tbaa !6
  %cmp23 = icmp slt i64 %43, %44
  br i1 %cmp23, label %cond.true24, label %cond.false27

cond.true24:                                      ; preds = %cond.end
  %45 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %tail25 = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %45, i32 0, i32 1
  %46 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %tail25, align 4, !tbaa !8
  %start_time26 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %46, i32 0, i32 1
  %47 = load i64, i64* %start_time26, align 8, !tbaa !16
  br label %cond.end28

cond.false27:                                     ; preds = %cond.end
  %48 = load i64, i64* %time_stamp.addr, align 8, !tbaa !6
  br label %cond.end28

cond.end28:                                       ; preds = %cond.false27, %cond.true24
  %cond29 = phi i64 [ %47, %cond.true24 ], [ %48, %cond.false27 ]
  %49 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %tail30 = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %49, i32 0, i32 1
  %50 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %tail30, align 4, !tbaa !8
  %start_time31 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %50, i32 0, i32 1
  store i64 %cond29, i64* %start_time31, align 8, !tbaa !16
  br label %if.end32

if.end32:                                         ; preds = %cond.end28, %if.end11
  ret void
}

declare i32 @memcmp(i8*, i8*, i32) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

declare i8* @aom_malloc(i32) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define hidden i32 @aom_film_grain_table_lookup(%struct.aom_film_grain_table_t* %t, i64 %time_stamp, i64 %end_time, i32 %erase, %struct.aom_film_grain_t* %grain) #0 {
entry:
  %retval = alloca i32, align 4
  %t.addr = alloca %struct.aom_film_grain_table_t*, align 4
  %time_stamp.addr = alloca i64, align 8
  %end_time.addr = alloca i64, align 8
  %erase.addr = alloca i32, align 4
  %grain.addr = alloca %struct.aom_film_grain_t*, align 4
  %entry1 = alloca %struct.aom_film_grain_table_entry_t*, align 4
  %prev_entry = alloca %struct.aom_film_grain_table_entry_t*, align 4
  %random_seed = alloca i16, align 2
  %next = alloca %struct.aom_film_grain_table_entry_t*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %entry_end_time = alloca i64, align 8
  %new_entry = alloca %struct.aom_film_grain_table_entry_t*, align 4
  store %struct.aom_film_grain_table_t* %t, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  store i64 %time_stamp, i64* %time_stamp.addr, align 8, !tbaa !6
  store i64 %end_time, i64* %end_time.addr, align 8, !tbaa !6
  store i32 %erase, i32* %erase.addr, align 4, !tbaa !19
  store %struct.aom_film_grain_t* %grain, %struct.aom_film_grain_t** %grain.addr, align 4, !tbaa !2
  %0 = bitcast %struct.aom_film_grain_table_entry_t** %entry1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %head = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %1, i32 0, i32 0
  %2 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %head, align 4, !tbaa !15
  store %struct.aom_film_grain_table_entry_t* %2, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %3 = bitcast %struct.aom_film_grain_table_entry_t** %prev_entry to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  store %struct.aom_film_grain_table_entry_t* null, %struct.aom_film_grain_table_entry_t** %prev_entry, align 4, !tbaa !2
  %4 = bitcast i16* %random_seed to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %4) #4
  %5 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %grain.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_film_grain_t* %5, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %6 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %grain.addr, align 4, !tbaa !2
  %random_seed2 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %6, i32 0, i32 25
  %7 = load i16, i16* %random_seed2, align 4, !tbaa !22
  %conv = zext i16 %7 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ 0, %cond.false ]
  %conv3 = trunc i32 %cond to i16
  store i16 %conv3, i16* %random_seed, align 2, !tbaa !21
  %8 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %grain.addr, align 4, !tbaa !2
  %tobool4 = icmp ne %struct.aom_film_grain_t* %8, null
  br i1 %tobool4, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %9 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %grain.addr, align 4, !tbaa !2
  %10 = bitcast %struct.aom_film_grain_t* %9 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %10, i8 0, i32 648, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %if.end
  %11 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %tobool5 = icmp ne %struct.aom_film_grain_table_entry_t* %11, null
  br i1 %tobool5, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %12 = bitcast %struct.aom_film_grain_table_entry_t** %next to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %next6 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %13, i32 0, i32 3
  %14 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %next6, align 8, !tbaa !10
  store %struct.aom_film_grain_table_entry_t* %14, %struct.aom_film_grain_table_entry_t** %next, align 4, !tbaa !2
  %15 = load i64, i64* %time_stamp.addr, align 8, !tbaa !6
  %16 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %start_time = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %16, i32 0, i32 1
  %17 = load i64, i64* %start_time, align 8, !tbaa !16
  %cmp = icmp sge i64 %15, %17
  br i1 %cmp, label %land.lhs.true, label %if.end89

land.lhs.true:                                    ; preds = %while.body
  %18 = load i64, i64* %time_stamp.addr, align 8, !tbaa !6
  %19 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %end_time8 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %19, i32 0, i32 2
  %20 = load i64, i64* %end_time8, align 8, !tbaa !17
  %cmp9 = icmp slt i64 %18, %20
  br i1 %cmp9, label %if.then11, label %if.end89

if.then11:                                        ; preds = %land.lhs.true
  %21 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %grain.addr, align 4, !tbaa !2
  %tobool12 = icmp ne %struct.aom_film_grain_t* %21, null
  br i1 %tobool12, label %if.then13, label %if.end19

if.then13:                                        ; preds = %if.then11
  %22 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %grain.addr, align 4, !tbaa !2
  %23 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %params = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %23, i32 0, i32 0
  %24 = bitcast %struct.aom_film_grain_t* %22 to i8*
  %25 = bitcast %struct.aom_film_grain_t* %params to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %24, i8* align 8 %25, i32 648, i1 false), !tbaa.struct !18
  %26 = load i64, i64* %time_stamp.addr, align 8, !tbaa !6
  %cmp14 = icmp ne i64 %26, 0
  br i1 %cmp14, label %if.then16, label %if.end18

if.then16:                                        ; preds = %if.then13
  %27 = load i16, i16* %random_seed, align 2, !tbaa !21
  %28 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %grain.addr, align 4, !tbaa !2
  %random_seed17 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %28, i32 0, i32 25
  store i16 %27, i16* %random_seed17, align 4, !tbaa !22
  br label %if.end18

if.end18:                                         ; preds = %if.then16, %if.then13
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %if.then11
  %29 = load i32, i32* %erase.addr, align 4, !tbaa !19
  %tobool20 = icmp ne i32 %29, 0
  br i1 %tobool20, label %if.end22, label %if.then21

if.then21:                                        ; preds = %if.end19
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end22:                                         ; preds = %if.end19
  %30 = bitcast i64* %entry_end_time to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %30) #4
  %31 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %end_time23 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %31, i32 0, i32 2
  %32 = load i64, i64* %end_time23, align 8, !tbaa !17
  store i64 %32, i64* %entry_end_time, align 8, !tbaa !6
  %33 = load i64, i64* %time_stamp.addr, align 8, !tbaa !6
  %34 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %start_time24 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %34, i32 0, i32 1
  %35 = load i64, i64* %start_time24, align 8, !tbaa !16
  %cmp25 = icmp sle i64 %33, %35
  br i1 %cmp25, label %land.lhs.true27, label %if.else44

land.lhs.true27:                                  ; preds = %if.end22
  %36 = load i64, i64* %end_time.addr, align 8, !tbaa !6
  %37 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %end_time28 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %37, i32 0, i32 2
  %38 = load i64, i64* %end_time28, align 8, !tbaa !17
  %cmp29 = icmp sge i64 %36, %38
  br i1 %cmp29, label %if.then31, label %if.else44

if.then31:                                        ; preds = %land.lhs.true27
  %39 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %tail = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %39, i32 0, i32 1
  %40 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %tail, align 4, !tbaa !8
  %41 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %cmp32 = icmp eq %struct.aom_film_grain_table_entry_t* %40, %41
  br i1 %cmp32, label %if.then34, label %if.end36

if.then34:                                        ; preds = %if.then31
  %42 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %prev_entry, align 4, !tbaa !2
  %43 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %tail35 = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %43, i32 0, i32 1
  store %struct.aom_film_grain_table_entry_t* %42, %struct.aom_film_grain_table_entry_t** %tail35, align 4, !tbaa !8
  br label %if.end36

if.end36:                                         ; preds = %if.then34, %if.then31
  %44 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %prev_entry, align 4, !tbaa !2
  %tobool37 = icmp ne %struct.aom_film_grain_table_entry_t* %44, null
  br i1 %tobool37, label %if.then38, label %if.else

if.then38:                                        ; preds = %if.end36
  %45 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %next39 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %45, i32 0, i32 3
  %46 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %next39, align 8, !tbaa !10
  %47 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %prev_entry, align 4, !tbaa !2
  %next40 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %47, i32 0, i32 3
  store %struct.aom_film_grain_table_entry_t* %46, %struct.aom_film_grain_table_entry_t** %next40, align 8, !tbaa !10
  br label %if.end43

if.else:                                          ; preds = %if.end36
  %48 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %next41 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %48, i32 0, i32 3
  %49 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %next41, align 8, !tbaa !10
  %50 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %head42 = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %50, i32 0, i32 0
  store %struct.aom_film_grain_table_entry_t* %49, %struct.aom_film_grain_table_entry_t** %head42, align 4, !tbaa !15
  br label %if.end43

if.end43:                                         ; preds = %if.else, %if.then38
  %51 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %52 = bitcast %struct.aom_film_grain_table_entry_t* %51 to i8*
  call void @aom_free(i8* %52)
  br label %if.end82

if.else44:                                        ; preds = %land.lhs.true27, %if.end22
  %53 = load i64, i64* %time_stamp.addr, align 8, !tbaa !6
  %54 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %start_time45 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %54, i32 0, i32 1
  %55 = load i64, i64* %start_time45, align 8, !tbaa !16
  %cmp46 = icmp sle i64 %53, %55
  br i1 %cmp46, label %land.lhs.true48, label %if.else54

land.lhs.true48:                                  ; preds = %if.else44
  %56 = load i64, i64* %end_time.addr, align 8, !tbaa !6
  %57 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %end_time49 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %57, i32 0, i32 2
  %58 = load i64, i64* %end_time49, align 8, !tbaa !17
  %cmp50 = icmp slt i64 %56, %58
  br i1 %cmp50, label %if.then52, label %if.else54

if.then52:                                        ; preds = %land.lhs.true48
  %59 = load i64, i64* %end_time.addr, align 8, !tbaa !6
  %60 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %start_time53 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %60, i32 0, i32 1
  store i64 %59, i64* %start_time53, align 8, !tbaa !16
  br label %if.end81

if.else54:                                        ; preds = %land.lhs.true48, %if.else44
  %61 = load i64, i64* %time_stamp.addr, align 8, !tbaa !6
  %62 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %start_time55 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %62, i32 0, i32 1
  %63 = load i64, i64* %start_time55, align 8, !tbaa !16
  %cmp56 = icmp sgt i64 %61, %63
  br i1 %cmp56, label %land.lhs.true58, label %if.else64

land.lhs.true58:                                  ; preds = %if.else54
  %64 = load i64, i64* %end_time.addr, align 8, !tbaa !6
  %65 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %end_time59 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %65, i32 0, i32 2
  %66 = load i64, i64* %end_time59, align 8, !tbaa !17
  %cmp60 = icmp sge i64 %64, %66
  br i1 %cmp60, label %if.then62, label %if.else64

if.then62:                                        ; preds = %land.lhs.true58
  %67 = load i64, i64* %time_stamp.addr, align 8, !tbaa !6
  %68 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %end_time63 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %68, i32 0, i32 2
  store i64 %67, i64* %end_time63, align 8, !tbaa !17
  br label %if.end80

if.else64:                                        ; preds = %land.lhs.true58, %if.else54
  %69 = bitcast %struct.aom_film_grain_table_entry_t** %new_entry to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #4
  %call = call i8* @aom_malloc(i32 672)
  %70 = bitcast i8* %call to %struct.aom_film_grain_table_entry_t*
  store %struct.aom_film_grain_table_entry_t* %70, %struct.aom_film_grain_table_entry_t** %new_entry, align 4, !tbaa !2
  %71 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %next65 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %71, i32 0, i32 3
  %72 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %next65, align 8, !tbaa !10
  %73 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %new_entry, align 4, !tbaa !2
  %next66 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %73, i32 0, i32 3
  store %struct.aom_film_grain_table_entry_t* %72, %struct.aom_film_grain_table_entry_t** %next66, align 8, !tbaa !10
  %74 = load i64, i64* %end_time.addr, align 8, !tbaa !6
  %75 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %new_entry, align 4, !tbaa !2
  %start_time67 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %75, i32 0, i32 1
  store i64 %74, i64* %start_time67, align 8, !tbaa !16
  %76 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %end_time68 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %76, i32 0, i32 2
  %77 = load i64, i64* %end_time68, align 8, !tbaa !17
  %78 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %new_entry, align 4, !tbaa !2
  %end_time69 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %78, i32 0, i32 2
  store i64 %77, i64* %end_time69, align 8, !tbaa !17
  %79 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %new_entry, align 4, !tbaa !2
  %params70 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %79, i32 0, i32 0
  %80 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %params71 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %80, i32 0, i32 0
  %81 = bitcast %struct.aom_film_grain_t* %params70 to i8*
  %82 = bitcast %struct.aom_film_grain_t* %params71 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %81, i8* align 8 %82, i32 648, i1 false), !tbaa.struct !18
  %83 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %new_entry, align 4, !tbaa !2
  %84 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %next72 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %84, i32 0, i32 3
  store %struct.aom_film_grain_table_entry_t* %83, %struct.aom_film_grain_table_entry_t** %next72, align 8, !tbaa !10
  %85 = load i64, i64* %time_stamp.addr, align 8, !tbaa !6
  %86 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %end_time73 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %86, i32 0, i32 2
  store i64 %85, i64* %end_time73, align 8, !tbaa !17
  %87 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %tail74 = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %87, i32 0, i32 1
  %88 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %tail74, align 4, !tbaa !8
  %89 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %cmp75 = icmp eq %struct.aom_film_grain_table_entry_t* %88, %89
  br i1 %cmp75, label %if.then77, label %if.end79

if.then77:                                        ; preds = %if.else64
  %90 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %new_entry, align 4, !tbaa !2
  %91 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %tail78 = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %91, i32 0, i32 1
  store %struct.aom_film_grain_table_entry_t* %90, %struct.aom_film_grain_table_entry_t** %tail78, align 4, !tbaa !8
  br label %if.end79

if.end79:                                         ; preds = %if.then77, %if.else64
  %92 = bitcast %struct.aom_film_grain_table_entry_t** %new_entry to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  br label %if.end80

if.end80:                                         ; preds = %if.end79, %if.then62
  br label %if.end81

if.end81:                                         ; preds = %if.end80, %if.then52
  br label %if.end82

if.end82:                                         ; preds = %if.end81, %if.end43
  %93 = load i64, i64* %end_time.addr, align 8, !tbaa !6
  %94 = load i64, i64* %entry_end_time, align 8, !tbaa !6
  %cmp83 = icmp sgt i64 %93, %94
  br i1 %cmp83, label %if.then85, label %if.end88

if.then85:                                        ; preds = %if.end82
  %95 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %96 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %end_time86 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %96, i32 0, i32 2
  %97 = load i64, i64* %end_time86, align 8, !tbaa !17
  %98 = load i64, i64* %end_time.addr, align 8, !tbaa !6
  %call87 = call i32 @aom_film_grain_table_lookup(%struct.aom_film_grain_table_t* %95, i64 %97, i64 %98, i32 1, %struct.aom_film_grain_t* null)
  br label %if.end88

if.end88:                                         ; preds = %if.then85, %if.end82
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %99 = bitcast i64* %entry_end_time to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %99) #4
  br label %cleanup

if.end89:                                         ; preds = %land.lhs.true, %while.body
  %100 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  store %struct.aom_film_grain_table_entry_t* %100, %struct.aom_film_grain_table_entry_t** %prev_entry, align 4, !tbaa !2
  %101 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %next, align 4, !tbaa !2
  store %struct.aom_film_grain_table_entry_t* %101, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end89, %if.end88, %if.then21
  %102 = bitcast %struct.aom_film_grain_table_entry_t** %next to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup90 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup90

cleanup90:                                        ; preds = %while.end, %cleanup
  %103 = bitcast i16* %random_seed to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %103) #4
  %104 = bitcast %struct.aom_film_grain_table_entry_t** %prev_entry to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #4
  %105 = bitcast %struct.aom_film_grain_table_entry_t** %entry1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #4
  %106 = load i32, i32* %retval, align 4
  ret i32 %106
}

declare void @aom_free(i8*) #1

; Function Attrs: nounwind
define hidden i32 @aom_film_grain_table_read(%struct.aom_film_grain_table_t* %t, i8* %filename, %struct.aom_internal_error_info* %error_info) #0 {
entry:
  %retval = alloca i32, align 4
  %t.addr = alloca %struct.aom_film_grain_table_t*, align 4
  %filename.addr = alloca i8*, align 4
  %error_info.addr = alloca %struct.aom_internal_error_info*, align 4
  %file = alloca %struct._IO_FILE*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %magic = alloca [9 x i8], align 1
  %prev_entry = alloca %struct.aom_film_grain_table_entry_t*, align 4
  %entry13 = alloca %struct.aom_film_grain_table_entry_t*, align 4
  store %struct.aom_film_grain_table_t* %t, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  store i8* %filename, i8** %filename.addr, align 4, !tbaa !2
  store %struct.aom_internal_error_info* %error_info, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  %0 = bitcast %struct._IO_FILE** %file to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  %call = call %struct._IO_FILE* @fopen(i8* %1, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str, i32 0, i32 0))
  store %struct._IO_FILE* %call, %struct._IO_FILE** %file, align 4, !tbaa !2
  %2 = load %struct._IO_FILE*, %struct._IO_FILE** %file, align 4, !tbaa !2
  %tobool = icmp ne %struct._IO_FILE* %2, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %3 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %3, i32 1, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.1, i32 0, i32 0), i8* %4)
  %5 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  %error_code = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %5, i32 0, i32 0
  %6 = load i32, i32* %error_code, align 4, !tbaa !23
  store i32 %6, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup30

if.end:                                           ; preds = %entry
  %7 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  %error_code1 = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %7, i32 0, i32 0
  store i32 0, i32* %error_code1, align 4, !tbaa !23
  %8 = bitcast [9 x i8]* %magic to i8*
  call void @llvm.lifetime.start.p0i8(i64 9, i8* %8) #4
  %arraydecay = getelementptr inbounds [9 x i8], [9 x i8]* %magic, i32 0, i32 0
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** %file, align 4, !tbaa !2
  %call2 = call i32 @fread(i8* %arraydecay, i32 9, i32 1, %struct._IO_FILE* %9)
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %lor.lhs.false, label %if.then7

lor.lhs.false:                                    ; preds = %if.end
  %arraydecay4 = getelementptr inbounds [9 x i8], [9 x i8]* %magic, i32 0, i32 0
  %call5 = call i32 @memcmp(i8* %arraydecay4, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @kFileMagic, i32 0, i32 0), i32 8)
  %tobool6 = icmp ne i32 %call5, 0
  br i1 %tobool6, label %if.then7, label %if.end10

if.then7:                                         ; preds = %lor.lhs.false, %if.end
  %10 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %10, i32 1, i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.2, i32 0, i32 0))
  %11 = load %struct._IO_FILE*, %struct._IO_FILE** %file, align 4, !tbaa !2
  %call8 = call i32 @fclose(%struct._IO_FILE* %11)
  %12 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  %error_code9 = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %12, i32 0, i32 0
  %13 = load i32, i32* %error_code9, align 4, !tbaa !23
  store i32 %13, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup29

if.end10:                                         ; preds = %lor.lhs.false
  %14 = bitcast %struct.aom_film_grain_table_entry_t** %prev_entry to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  store %struct.aom_film_grain_table_entry_t* null, %struct.aom_film_grain_table_entry_t** %prev_entry, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %if.end10
  %15 = load %struct._IO_FILE*, %struct._IO_FILE** %file, align 4, !tbaa !2
  %call11 = call i32 @feof(%struct._IO_FILE* %15)
  %tobool12 = icmp ne i32 %call11, 0
  %lnot = xor i1 %tobool12, true
  br i1 %lnot, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %16 = bitcast %struct.aom_film_grain_table_entry_t** %entry13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %call14 = call i8* @aom_malloc(i32 672)
  %17 = bitcast i8* %call14 to %struct.aom_film_grain_table_entry_t*
  store %struct.aom_film_grain_table_entry_t* %17, %struct.aom_film_grain_table_entry_t** %entry13, align 4, !tbaa !2
  %18 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry13, align 4, !tbaa !2
  %19 = bitcast %struct.aom_film_grain_table_entry_t* %18 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %19, i8 0, i32 672, i1 false)
  %20 = load %struct._IO_FILE*, %struct._IO_FILE** %file, align 4, !tbaa !2
  %21 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  %22 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry13, align 4, !tbaa !2
  call void @grain_table_entry_read(%struct._IO_FILE* %20, %struct.aom_internal_error_info* %21, %struct.aom_film_grain_table_entry_t* %22)
  %23 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry13, align 4, !tbaa !2
  %next = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %23, i32 0, i32 3
  store %struct.aom_film_grain_table_entry_t* null, %struct.aom_film_grain_table_entry_t** %next, align 8, !tbaa !10
  %24 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %prev_entry, align 4, !tbaa !2
  %tobool15 = icmp ne %struct.aom_film_grain_table_entry_t* %24, null
  br i1 %tobool15, label %if.then16, label %if.end18

if.then16:                                        ; preds = %while.body
  %25 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry13, align 4, !tbaa !2
  %26 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %prev_entry, align 4, !tbaa !2
  %next17 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %26, i32 0, i32 3
  store %struct.aom_film_grain_table_entry_t* %25, %struct.aom_film_grain_table_entry_t** %next17, align 8, !tbaa !10
  br label %if.end18

if.end18:                                         ; preds = %if.then16, %while.body
  %27 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %head = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %27, i32 0, i32 0
  %28 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %head, align 4, !tbaa !15
  %tobool19 = icmp ne %struct.aom_film_grain_table_entry_t* %28, null
  br i1 %tobool19, label %if.end22, label %if.then20

if.then20:                                        ; preds = %if.end18
  %29 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry13, align 4, !tbaa !2
  %30 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %head21 = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %30, i32 0, i32 0
  store %struct.aom_film_grain_table_entry_t* %29, %struct.aom_film_grain_table_entry_t** %head21, align 4, !tbaa !15
  br label %if.end22

if.end22:                                         ; preds = %if.then20, %if.end18
  %31 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry13, align 4, !tbaa !2
  %32 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %tail = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %32, i32 0, i32 1
  store %struct.aom_film_grain_table_entry_t* %31, %struct.aom_film_grain_table_entry_t** %tail, align 4, !tbaa !8
  %33 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry13, align 4, !tbaa !2
  store %struct.aom_film_grain_table_entry_t* %33, %struct.aom_film_grain_table_entry_t** %prev_entry, align 4, !tbaa !2
  %34 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  %error_code23 = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %34, i32 0, i32 0
  %35 = load i32, i32* %error_code23, align 4, !tbaa !23
  %cmp = icmp ne i32 %35, 0
  br i1 %cmp, label %if.then24, label %if.end25

if.then24:                                        ; preds = %if.end22
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end25:                                         ; preds = %if.end22
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end25, %if.then24
  %36 = bitcast %struct.aom_film_grain_table_entry_t** %entry13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 3, label %while.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %cleanup, %while.cond
  %37 = load %struct._IO_FILE*, %struct._IO_FILE** %file, align 4, !tbaa !2
  %call26 = call i32 @fclose(%struct._IO_FILE* %37)
  %38 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  %error_code27 = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %38, i32 0, i32 0
  %39 = load i32, i32* %error_code27, align 4, !tbaa !23
  store i32 %39, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %40 = bitcast %struct.aom_film_grain_table_entry_t** %prev_entry to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  br label %cleanup29

cleanup29:                                        ; preds = %while.end, %if.then7
  %41 = bitcast [9 x i8]* %magic to i8*
  call void @llvm.lifetime.end.p0i8(i64 9, i8* %41) #4
  br label %cleanup30

cleanup30:                                        ; preds = %cleanup29, %if.then
  %42 = bitcast %struct._IO_FILE** %file to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  %43 = load i32, i32* %retval, align 4
  ret i32 %43

unreachable:                                      ; preds = %cleanup
  unreachable
}

declare %struct._IO_FILE* @fopen(i8*, i8*) #1

declare void @aom_internal_error(%struct.aom_internal_error_info*, i32, i8*, ...) #1

declare i32 @fread(i8*, i32, i32, %struct._IO_FILE*) #1

declare i32 @fclose(%struct._IO_FILE*) #1

declare i32 @feof(%struct._IO_FILE*) #1

; Function Attrs: nounwind
define internal void @grain_table_entry_read(%struct._IO_FILE* %file, %struct.aom_internal_error_info* %error_info, %struct.aom_film_grain_table_entry_t* %entry1) #0 {
entry:
  %file.addr = alloca %struct._IO_FILE*, align 4
  %error_info.addr = alloca %struct.aom_internal_error_info*, align 4
  %entry.addr = alloca %struct.aom_film_grain_table_entry_t*, align 4
  %pars = alloca %struct.aom_film_grain_t*, align 4
  %num_read = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %i31 = alloca i32, align 4
  %i55 = alloca i32, align 4
  %n = alloca i32, align 4
  %i79 = alloca i32, align 4
  %i95 = alloca i32, align 4
  %i111 = alloca i32, align 4
  store %struct._IO_FILE* %file, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  store %struct.aom_internal_error_info* %error_info, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  store %struct.aom_film_grain_table_entry_t* %entry1, %struct.aom_film_grain_table_entry_t** %entry.addr, align 4, !tbaa !2
  %0 = bitcast %struct.aom_film_grain_t** %pars to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry.addr, align 4, !tbaa !2
  %params = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %1, i32 0, i32 0
  store %struct.aom_film_grain_t* %params, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %2 = bitcast i32* %num_read to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %4 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry.addr, align 4, !tbaa !2
  %start_time = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %4, i32 0, i32 1
  %5 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry.addr, align 4, !tbaa !2
  %end_time = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %5, i32 0, i32 2
  %6 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %apply_grain = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %6, i32 0, i32 0
  %7 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %random_seed = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %7, i32 0, i32 25
  %8 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %update_parameters = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %8, i32 0, i32 1
  %call = call i32 (%struct._IO_FILE*, i8*, ...) @fscanf(%struct._IO_FILE* %3, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.7, i32 0, i32 0), i64* %start_time, i64* %end_time, i32* %apply_grain, i16* %random_seed, i32* %update_parameters)
  store i32 %call, i32* %num_read, align 4, !tbaa !19
  %9 = load i32, i32* %num_read, align 4, !tbaa !19
  %cmp = icmp eq i32 %9, 0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %10 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call2 = call i32 @feof(%struct._IO_FILE* %10)
  %tobool = icmp ne i32 %call2, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup130

if.end:                                           ; preds = %land.lhs.true, %entry
  %11 = load i32, i32* %num_read, align 4, !tbaa !19
  %cmp3 = icmp ne i32 %11, 5
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %12 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  %13 = load i32, i32* %num_read, align 4, !tbaa !19
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %12, i32 1, i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.8, i32 0, i32 0), i32 %13)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup130

if.end5:                                          ; preds = %if.end
  %14 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %update_parameters6 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %14, i32 0, i32 1
  %15 = load i32, i32* %update_parameters6, align 4, !tbaa !25
  %tobool7 = icmp ne i32 %15, 0
  br i1 %tobool7, label %if.then8, label %if.end129

if.then8:                                         ; preds = %if.end5
  %16 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %17 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %ar_coeff_lag = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %17, i32 0, i32 9
  %18 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %ar_coeff_shift = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %18, i32 0, i32 13
  %19 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %grain_scale_shift = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %19, i32 0, i32 24
  %20 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %scaling_shift = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %20, i32 0, i32 8
  %21 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %chroma_scaling_from_luma = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %21, i32 0, i32 23
  %22 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %overlap_flag = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %22, i32 0, i32 20
  %23 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %cb_mult = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %23, i32 0, i32 14
  %24 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %cb_luma_mult = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %24, i32 0, i32 15
  %25 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %cb_offset = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %25, i32 0, i32 16
  %26 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %cr_mult = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %26, i32 0, i32 17
  %27 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %cr_luma_mult = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %27, i32 0, i32 18
  %28 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %cr_offset = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %28, i32 0, i32 19
  %call9 = call i32 (%struct._IO_FILE*, i8*, ...) @fscanf(%struct._IO_FILE* %16, i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.9, i32 0, i32 0), i32* %ar_coeff_lag, i32* %ar_coeff_shift, i32* %grain_scale_shift, i32* %scaling_shift, i32* %chroma_scaling_from_luma, i32* %overlap_flag, i32* %cb_mult, i32* %cb_luma_mult, i32* %cb_offset, i32* %cr_mult, i32* %cr_luma_mult, i32* %cr_offset)
  store i32 %call9, i32* %num_read, align 4, !tbaa !19
  %29 = load i32, i32* %num_read, align 4, !tbaa !19
  %cmp10 = icmp ne i32 %29, 12
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.then8
  %30 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  %31 = load i32, i32* %num_read, align 4, !tbaa !19
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %30, i32 1, i8* getelementptr inbounds ([43 x i8], [43 x i8]* @.str.10, i32 0, i32 0), i32 %31)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup130

if.end12:                                         ; preds = %if.then8
  %32 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %33 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %num_y_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %33, i32 0, i32 3
  %call13 = call i32 (%struct._IO_FILE*, i8*, ...) @fscanf(%struct._IO_FILE* %32, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.11, i32 0, i32 0), i32* %num_y_points)
  %tobool14 = icmp ne i32 %call13, 0
  br i1 %tobool14, label %if.end16, label %if.then15

if.then15:                                        ; preds = %if.end12
  %34 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %34, i32 1, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.12, i32 0, i32 0))
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup130

if.end16:                                         ; preds = %if.end12
  %35 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #4
  store i32 0, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end16
  %36 = load i32, i32* %i, align 4, !tbaa !19
  %37 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %num_y_points17 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %37, i32 0, i32 3
  %38 = load i32, i32* %num_y_points17, align 4, !tbaa !26
  %cmp18 = icmp slt i32 %36, %38
  br i1 %cmp18, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %39 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %40 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %scaling_points_y = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %40, i32 0, i32 2
  %41 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds [14 x [2 x i32]], [14 x [2 x i32]]* %scaling_points_y, i32 0, i32 %41
  %arrayidx19 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx, i32 0, i32 0
  %42 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %scaling_points_y20 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %42, i32 0, i32 2
  %43 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx21 = getelementptr inbounds [14 x [2 x i32]], [14 x [2 x i32]]* %scaling_points_y20, i32 0, i32 %43
  %arrayidx22 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx21, i32 0, i32 1
  %call23 = call i32 (%struct._IO_FILE*, i8*, ...) @fscanf(%struct._IO_FILE* %39, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.13, i32 0, i32 0), i32* %arrayidx19, i32* %arrayidx22)
  %cmp24 = icmp ne i32 2, %call23
  br i1 %cmp24, label %if.then25, label %if.end26

if.then25:                                        ; preds = %for.body
  %44 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %44, i32 1, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.14, i32 0, i32 0))
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end26:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end26
  %45 = load i32, i32* %i, align 4, !tbaa !19
  %inc = add nsw i32 %45, 1
  store i32 %inc, i32* %i, align 4, !tbaa !19
  br label %for.cond

cleanup:                                          ; preds = %if.then25, %for.cond.cleanup
  %46 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup130 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup
  %47 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %48 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %num_cb_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %48, i32 0, i32 5
  %call27 = call i32 (%struct._IO_FILE*, i8*, ...) @fscanf(%struct._IO_FILE* %47, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.15, i32 0, i32 0), i32* %num_cb_points)
  %tobool28 = icmp ne i32 %call27, 0
  br i1 %tobool28, label %if.end30, label %if.then29

if.then29:                                        ; preds = %for.end
  %49 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %49, i32 1, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.16, i32 0, i32 0))
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup130

if.end30:                                         ; preds = %for.end
  %50 = bitcast i32* %i31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #4
  store i32 0, i32* %i31, align 4, !tbaa !19
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc46, %if.end30
  %51 = load i32, i32* %i31, align 4, !tbaa !19
  %52 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %num_cb_points33 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %52, i32 0, i32 5
  %53 = load i32, i32* %num_cb_points33, align 4, !tbaa !27
  %cmp34 = icmp slt i32 %51, %53
  br i1 %cmp34, label %for.body36, label %for.cond.cleanup35

for.cond.cleanup35:                               ; preds = %for.cond32
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup48

for.body36:                                       ; preds = %for.cond32
  %54 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %55 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %scaling_points_cb = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %55, i32 0, i32 4
  %56 = load i32, i32* %i31, align 4, !tbaa !19
  %arrayidx37 = getelementptr inbounds [10 x [2 x i32]], [10 x [2 x i32]]* %scaling_points_cb, i32 0, i32 %56
  %arrayidx38 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx37, i32 0, i32 0
  %57 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %scaling_points_cb39 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %57, i32 0, i32 4
  %58 = load i32, i32* %i31, align 4, !tbaa !19
  %arrayidx40 = getelementptr inbounds [10 x [2 x i32]], [10 x [2 x i32]]* %scaling_points_cb39, i32 0, i32 %58
  %arrayidx41 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx40, i32 0, i32 1
  %call42 = call i32 (%struct._IO_FILE*, i8*, ...) @fscanf(%struct._IO_FILE* %54, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.13, i32 0, i32 0), i32* %arrayidx38, i32* %arrayidx41)
  %cmp43 = icmp ne i32 2, %call42
  br i1 %cmp43, label %if.then44, label %if.end45

if.then44:                                        ; preds = %for.body36
  %59 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %59, i32 1, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.17, i32 0, i32 0))
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup48

if.end45:                                         ; preds = %for.body36
  br label %for.inc46

for.inc46:                                        ; preds = %if.end45
  %60 = load i32, i32* %i31, align 4, !tbaa !19
  %inc47 = add nsw i32 %60, 1
  store i32 %inc47, i32* %i31, align 4, !tbaa !19
  br label %for.cond32

cleanup48:                                        ; preds = %if.then44, %for.cond.cleanup35
  %61 = bitcast i32* %i31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #4
  %cleanup.dest49 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest49, label %cleanup130 [
    i32 5, label %for.end50
  ]

for.end50:                                        ; preds = %cleanup48
  %62 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %63 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %num_cr_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %63, i32 0, i32 7
  %call51 = call i32 (%struct._IO_FILE*, i8*, ...) @fscanf(%struct._IO_FILE* %62, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.18, i32 0, i32 0), i32* %num_cr_points)
  %tobool52 = icmp ne i32 %call51, 0
  br i1 %tobool52, label %if.end54, label %if.then53

if.then53:                                        ; preds = %for.end50
  %64 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %64, i32 1, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.19, i32 0, i32 0))
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup130

if.end54:                                         ; preds = %for.end50
  %65 = bitcast i32* %i55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #4
  store i32 0, i32* %i55, align 4, !tbaa !19
  br label %for.cond56

for.cond56:                                       ; preds = %for.inc70, %if.end54
  %66 = load i32, i32* %i55, align 4, !tbaa !19
  %67 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %num_cr_points57 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %67, i32 0, i32 7
  %68 = load i32, i32* %num_cr_points57, align 4, !tbaa !28
  %cmp58 = icmp slt i32 %66, %68
  br i1 %cmp58, label %for.body60, label %for.cond.cleanup59

for.cond.cleanup59:                               ; preds = %for.cond56
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup72

for.body60:                                       ; preds = %for.cond56
  %69 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %70 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %scaling_points_cr = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %70, i32 0, i32 6
  %71 = load i32, i32* %i55, align 4, !tbaa !19
  %arrayidx61 = getelementptr inbounds [10 x [2 x i32]], [10 x [2 x i32]]* %scaling_points_cr, i32 0, i32 %71
  %arrayidx62 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx61, i32 0, i32 0
  %72 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %scaling_points_cr63 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %72, i32 0, i32 6
  %73 = load i32, i32* %i55, align 4, !tbaa !19
  %arrayidx64 = getelementptr inbounds [10 x [2 x i32]], [10 x [2 x i32]]* %scaling_points_cr63, i32 0, i32 %73
  %arrayidx65 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx64, i32 0, i32 1
  %call66 = call i32 (%struct._IO_FILE*, i8*, ...) @fscanf(%struct._IO_FILE* %69, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.13, i32 0, i32 0), i32* %arrayidx62, i32* %arrayidx65)
  %cmp67 = icmp ne i32 2, %call66
  br i1 %cmp67, label %if.then68, label %if.end69

if.then68:                                        ; preds = %for.body60
  %74 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %74, i32 1, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.20, i32 0, i32 0))
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup72

if.end69:                                         ; preds = %for.body60
  br label %for.inc70

for.inc70:                                        ; preds = %if.end69
  %75 = load i32, i32* %i55, align 4, !tbaa !19
  %inc71 = add nsw i32 %75, 1
  store i32 %inc71, i32* %i55, align 4, !tbaa !19
  br label %for.cond56

cleanup72:                                        ; preds = %if.then68, %for.cond.cleanup59
  %76 = bitcast i32* %i55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #4
  %cleanup.dest73 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest73, label %cleanup130 [
    i32 8, label %for.end74
  ]

for.end74:                                        ; preds = %cleanup72
  %77 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call75 = call i32 (%struct._IO_FILE*, i8*, ...) @fscanf(%struct._IO_FILE* %77, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.21, i32 0, i32 0))
  %78 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #4
  %79 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %ar_coeff_lag76 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %79, i32 0, i32 9
  %80 = load i32, i32* %ar_coeff_lag76, align 4, !tbaa !29
  %mul = mul nsw i32 2, %80
  %81 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %ar_coeff_lag77 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %81, i32 0, i32 9
  %82 = load i32, i32* %ar_coeff_lag77, align 4, !tbaa !29
  %add = add nsw i32 %82, 1
  %mul78 = mul nsw i32 %mul, %add
  store i32 %mul78, i32* %n, align 4, !tbaa !19
  %83 = bitcast i32* %i79 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #4
  store i32 0, i32* %i79, align 4, !tbaa !19
  br label %for.cond80

for.cond80:                                       ; preds = %for.inc89, %for.end74
  %84 = load i32, i32* %i79, align 4, !tbaa !19
  %85 = load i32, i32* %n, align 4, !tbaa !19
  %cmp81 = icmp slt i32 %84, %85
  br i1 %cmp81, label %for.body83, label %for.cond.cleanup82

for.cond.cleanup82:                               ; preds = %for.cond80
  store i32 11, i32* %cleanup.dest.slot, align 4
  br label %cleanup91

for.body83:                                       ; preds = %for.cond80
  %86 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %87 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %ar_coeffs_y = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %87, i32 0, i32 10
  %88 = load i32, i32* %i79, align 4, !tbaa !19
  %arrayidx84 = getelementptr inbounds [24 x i32], [24 x i32]* %ar_coeffs_y, i32 0, i32 %88
  %call85 = call i32 (%struct._IO_FILE*, i8*, ...) @fscanf(%struct._IO_FILE* %86, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.22, i32 0, i32 0), i32* %arrayidx84)
  %cmp86 = icmp ne i32 1, %call85
  br i1 %cmp86, label %if.then87, label %if.end88

if.then87:                                        ; preds = %for.body83
  %89 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %89, i32 1, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.23, i32 0, i32 0))
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup91

if.end88:                                         ; preds = %for.body83
  br label %for.inc89

for.inc89:                                        ; preds = %if.end88
  %90 = load i32, i32* %i79, align 4, !tbaa !19
  %inc90 = add nsw i32 %90, 1
  store i32 %inc90, i32* %i79, align 4, !tbaa !19
  br label %for.cond80

cleanup91:                                        ; preds = %if.then87, %for.cond.cleanup82
  %91 = bitcast i32* %i79 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #4
  %cleanup.dest92 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest92, label %cleanup127 [
    i32 11, label %for.end93
  ]

for.end93:                                        ; preds = %cleanup91
  %92 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call94 = call i32 (%struct._IO_FILE*, i8*, ...) @fscanf(%struct._IO_FILE* %92, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.24, i32 0, i32 0))
  %93 = bitcast i32* %i95 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %93) #4
  store i32 0, i32* %i95, align 4, !tbaa !19
  br label %for.cond96

for.cond96:                                       ; preds = %for.inc105, %for.end93
  %94 = load i32, i32* %i95, align 4, !tbaa !19
  %95 = load i32, i32* %n, align 4, !tbaa !19
  %cmp97 = icmp sle i32 %94, %95
  br i1 %cmp97, label %for.body99, label %for.cond.cleanup98

for.cond.cleanup98:                               ; preds = %for.cond96
  store i32 14, i32* %cleanup.dest.slot, align 4
  br label %cleanup107

for.body99:                                       ; preds = %for.cond96
  %96 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %97 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %ar_coeffs_cb = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %97, i32 0, i32 11
  %98 = load i32, i32* %i95, align 4, !tbaa !19
  %arrayidx100 = getelementptr inbounds [25 x i32], [25 x i32]* %ar_coeffs_cb, i32 0, i32 %98
  %call101 = call i32 (%struct._IO_FILE*, i8*, ...) @fscanf(%struct._IO_FILE* %96, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.22, i32 0, i32 0), i32* %arrayidx100)
  %cmp102 = icmp ne i32 1, %call101
  br i1 %cmp102, label %if.then103, label %if.end104

if.then103:                                       ; preds = %for.body99
  %99 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %99, i32 1, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.25, i32 0, i32 0))
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup107

if.end104:                                        ; preds = %for.body99
  br label %for.inc105

for.inc105:                                       ; preds = %if.end104
  %100 = load i32, i32* %i95, align 4, !tbaa !19
  %inc106 = add nsw i32 %100, 1
  store i32 %inc106, i32* %i95, align 4, !tbaa !19
  br label %for.cond96

cleanup107:                                       ; preds = %if.then103, %for.cond.cleanup98
  %101 = bitcast i32* %i95 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #4
  %cleanup.dest108 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest108, label %cleanup127 [
    i32 14, label %for.end109
  ]

for.end109:                                       ; preds = %cleanup107
  %102 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call110 = call i32 (%struct._IO_FILE*, i8*, ...) @fscanf(%struct._IO_FILE* %102, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.26, i32 0, i32 0))
  %103 = bitcast i32* %i111 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %103) #4
  store i32 0, i32* %i111, align 4, !tbaa !19
  br label %for.cond112

for.cond112:                                      ; preds = %for.inc121, %for.end109
  %104 = load i32, i32* %i111, align 4, !tbaa !19
  %105 = load i32, i32* %n, align 4, !tbaa !19
  %cmp113 = icmp sle i32 %104, %105
  br i1 %cmp113, label %for.body115, label %for.cond.cleanup114

for.cond.cleanup114:                              ; preds = %for.cond112
  store i32 17, i32* %cleanup.dest.slot, align 4
  br label %cleanup123

for.body115:                                      ; preds = %for.cond112
  %106 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %107 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %ar_coeffs_cr = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %107, i32 0, i32 12
  %108 = load i32, i32* %i111, align 4, !tbaa !19
  %arrayidx116 = getelementptr inbounds [25 x i32], [25 x i32]* %ar_coeffs_cr, i32 0, i32 %108
  %call117 = call i32 (%struct._IO_FILE*, i8*, ...) @fscanf(%struct._IO_FILE* %106, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.22, i32 0, i32 0), i32* %arrayidx116)
  %cmp118 = icmp ne i32 1, %call117
  br i1 %cmp118, label %if.then119, label %if.end120

if.then119:                                       ; preds = %for.body115
  %109 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %109, i32 1, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.27, i32 0, i32 0))
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup123

if.end120:                                        ; preds = %for.body115
  br label %for.inc121

for.inc121:                                       ; preds = %if.end120
  %110 = load i32, i32* %i111, align 4, !tbaa !19
  %inc122 = add nsw i32 %110, 1
  store i32 %inc122, i32* %i111, align 4, !tbaa !19
  br label %for.cond112

cleanup123:                                       ; preds = %if.then119, %for.cond.cleanup114
  %111 = bitcast i32* %i111 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #4
  %cleanup.dest124 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest124, label %cleanup127 [
    i32 17, label %for.end125
  ]

for.end125:                                       ; preds = %cleanup123
  %112 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call126 = call i32 (%struct._IO_FILE*, i8*, ...) @fscanf(%struct._IO_FILE* %112, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.6, i32 0, i32 0))
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup127

cleanup127:                                       ; preds = %for.end125, %cleanup123, %cleanup107, %cleanup91
  %113 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #4
  %cleanup.dest128 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest128, label %cleanup130 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup127
  br label %if.end129

if.end129:                                        ; preds = %cleanup.cont, %if.end5
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup130

cleanup130:                                       ; preds = %if.end129, %cleanup127, %cleanup72, %if.then53, %cleanup48, %if.then29, %cleanup, %if.then15, %if.then11, %if.then4, %if.then
  %114 = bitcast i32* %num_read to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #4
  %115 = bitcast %struct.aom_film_grain_t** %pars to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #4
  %cleanup.dest132 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest132, label %unreachable [
    i32 0, label %cleanup.cont133
    i32 1, label %cleanup.cont133
  ]

cleanup.cont133:                                  ; preds = %cleanup130, %cleanup130
  ret void

unreachable:                                      ; preds = %cleanup130
  unreachable
}

; Function Attrs: nounwind
define hidden i32 @aom_film_grain_table_write(%struct.aom_film_grain_table_t* %t, i8* %filename, %struct.aom_internal_error_info* %error_info) #0 {
entry:
  %retval = alloca i32, align 4
  %t.addr = alloca %struct.aom_film_grain_table_t*, align 4
  %filename.addr = alloca i8*, align 4
  %error_info.addr = alloca %struct.aom_internal_error_info*, align 4
  %file = alloca %struct._IO_FILE*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %entry9 = alloca %struct.aom_film_grain_table_entry_t*, align 4
  store %struct.aom_film_grain_table_t* %t, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  store i8* %filename, i8** %filename.addr, align 4, !tbaa !2
  store %struct.aom_internal_error_info* %error_info, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  %0 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  %error_code = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %0, i32 0, i32 0
  store i32 0, i32* %error_code, align 4, !tbaa !23
  %1 = bitcast %struct._IO_FILE** %file to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  %call = call %struct._IO_FILE* @fopen(i8* %2, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.3, i32 0, i32 0))
  store %struct._IO_FILE* %call, %struct._IO_FILE** %file, align 4, !tbaa !2
  %3 = load %struct._IO_FILE*, %struct._IO_FILE** %file, align 4, !tbaa !2
  %tobool = icmp ne %struct._IO_FILE* %3, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %4 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  %5 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %4, i32 1, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.4, i32 0, i32 0), i8* %5)
  %6 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  %error_code1 = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %6, i32 0, i32 0
  %7 = load i32, i32* %error_code1, align 4, !tbaa !23
  store i32 %7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %8 = load %struct._IO_FILE*, %struct._IO_FILE** %file, align 4, !tbaa !2
  %call2 = call i32 @fwrite(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @kFileMagic, i32 0, i32 0), i32 8, i32 1, %struct._IO_FILE* %8)
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %if.end7, label %if.then4

if.then4:                                         ; preds = %if.end
  %9 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %9, i32 1, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.5, i32 0, i32 0))
  %10 = load %struct._IO_FILE*, %struct._IO_FILE** %file, align 4, !tbaa !2
  %call5 = call i32 @fclose(%struct._IO_FILE* %10)
  %11 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  %error_code6 = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %11, i32 0, i32 0
  %12 = load i32, i32* %error_code6, align 4, !tbaa !23
  store i32 %12, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %if.end
  %13 = load %struct._IO_FILE*, %struct._IO_FILE** %file, align 4, !tbaa !2
  %call8 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %13, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.6, i32 0, i32 0))
  %14 = bitcast %struct.aom_film_grain_table_entry_t** %entry9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %head = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %15, i32 0, i32 0
  %16 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %head, align 4, !tbaa !15
  store %struct.aom_film_grain_table_entry_t* %16, %struct.aom_film_grain_table_entry_t** %entry9, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end7
  %17 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry9, align 4, !tbaa !2
  %tobool10 = icmp ne %struct.aom_film_grain_table_entry_t* %17, null
  br i1 %tobool10, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %18 = load %struct._IO_FILE*, %struct._IO_FILE** %file, align 4, !tbaa !2
  %19 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry9, align 4, !tbaa !2
  call void @grain_table_entry_write(%struct._IO_FILE* %18, %struct.aom_film_grain_table_entry_t* %19)
  %20 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry9, align 4, !tbaa !2
  %next = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %20, i32 0, i32 3
  %21 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %next, align 8, !tbaa !10
  store %struct.aom_film_grain_table_entry_t* %21, %struct.aom_film_grain_table_entry_t** %entry9, align 4, !tbaa !2
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %22 = load %struct._IO_FILE*, %struct._IO_FILE** %file, align 4, !tbaa !2
  %call11 = call i32 @fclose(%struct._IO_FILE* %22)
  %23 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error_info.addr, align 4, !tbaa !2
  %error_code12 = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %23, i32 0, i32 0
  %24 = load i32, i32* %error_code12, align 4, !tbaa !23
  store i32 %24, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %25 = bitcast %struct.aom_film_grain_table_entry_t** %entry9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  br label %cleanup

cleanup:                                          ; preds = %while.end, %if.then4, %if.then
  %26 = bitcast %struct._IO_FILE** %file to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  %27 = load i32, i32* %retval, align 4
  ret i32 %27
}

declare i32 @fwrite(i8*, i32, i32, %struct._IO_FILE*) #1

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #1

; Function Attrs: nounwind
define internal void @grain_table_entry_write(%struct._IO_FILE* %file, %struct.aom_film_grain_table_entry_t* %entry1) #0 {
entry:
  %file.addr = alloca %struct._IO_FILE*, align 4
  %entry.addr = alloca %struct.aom_film_grain_table_entry_t*, align 4
  %pars = alloca %struct.aom_film_grain_t*, align 4
  %i = alloca i32, align 4
  %i13 = alloca i32, align 4
  %i30 = alloca i32, align 4
  %n = alloca i32, align 4
  %i50 = alloca i32, align 4
  %i62 = alloca i32, align 4
  %i74 = alloca i32, align 4
  store %struct._IO_FILE* %file, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  store %struct.aom_film_grain_table_entry_t* %entry1, %struct.aom_film_grain_table_entry_t** %entry.addr, align 4, !tbaa !2
  %0 = bitcast %struct.aom_film_grain_t** %pars to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry.addr, align 4, !tbaa !2
  %params = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %1, i32 0, i32 0
  store %struct.aom_film_grain_t* %params, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %2 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %3 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry.addr, align 4, !tbaa !2
  %start_time = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %3, i32 0, i32 1
  %4 = load i64, i64* %start_time, align 8, !tbaa !16
  %5 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry.addr, align 4, !tbaa !2
  %end_time = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %5, i32 0, i32 2
  %6 = load i64, i64* %end_time, align 8, !tbaa !17
  %7 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %apply_grain = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %7, i32 0, i32 0
  %8 = load i32, i32* %apply_grain, align 4, !tbaa !30
  %9 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %random_seed = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %9, i32 0, i32 25
  %10 = load i16, i16* %random_seed, align 4, !tbaa !22
  %conv = zext i16 %10 to i32
  %11 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %update_parameters = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %11, i32 0, i32 1
  %12 = load i32, i32* %update_parameters, align 4, !tbaa !25
  %call = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %2, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.28, i32 0, i32 0), i64 %4, i64 %6, i32 %8, i32 %conv, i32 %12)
  %13 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %update_parameters2 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %13, i32 0, i32 1
  %14 = load i32, i32* %update_parameters2, align 4, !tbaa !25
  %tobool = icmp ne i32 %14, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %15 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %16 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %ar_coeff_lag = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %16, i32 0, i32 9
  %17 = load i32, i32* %ar_coeff_lag, align 4, !tbaa !29
  %18 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %ar_coeff_shift = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %18, i32 0, i32 13
  %19 = load i32, i32* %ar_coeff_shift, align 4, !tbaa !31
  %20 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %grain_scale_shift = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %20, i32 0, i32 24
  %21 = load i32, i32* %grain_scale_shift, align 4, !tbaa !32
  %22 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %scaling_shift = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %22, i32 0, i32 8
  %23 = load i32, i32* %scaling_shift, align 4, !tbaa !33
  %24 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %chroma_scaling_from_luma = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %24, i32 0, i32 23
  %25 = load i32, i32* %chroma_scaling_from_luma, align 4, !tbaa !34
  %26 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %overlap_flag = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %26, i32 0, i32 20
  %27 = load i32, i32* %overlap_flag, align 4, !tbaa !35
  %28 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %cb_mult = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %28, i32 0, i32 14
  %29 = load i32, i32* %cb_mult, align 4, !tbaa !36
  %30 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %cb_luma_mult = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %30, i32 0, i32 15
  %31 = load i32, i32* %cb_luma_mult, align 4, !tbaa !37
  %32 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %cb_offset = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %32, i32 0, i32 16
  %33 = load i32, i32* %cb_offset, align 4, !tbaa !38
  %34 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %cr_mult = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %34, i32 0, i32 17
  %35 = load i32, i32* %cr_mult, align 4, !tbaa !39
  %36 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %cr_luma_mult = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %36, i32 0, i32 18
  %37 = load i32, i32* %cr_luma_mult, align 4, !tbaa !40
  %38 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %cr_offset = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %38, i32 0, i32 19
  %39 = load i32, i32* %cr_offset, align 4, !tbaa !41
  %call3 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %15, i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.29, i32 0, i32 0), i32 %17, i32 %19, i32 %21, i32 %23, i32 %25, i32 %27, i32 %29, i32 %31, i32 %33, i32 %35, i32 %37, i32 %39)
  %40 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %41 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %num_y_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %41, i32 0, i32 3
  %42 = load i32, i32* %num_y_points, align 4, !tbaa !26
  %call4 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %40, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.11, i32 0, i32 0), i32 %42)
  %43 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #4
  store i32 0, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %44 = load i32, i32* %i, align 4, !tbaa !19
  %45 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %num_y_points5 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %45, i32 0, i32 3
  %46 = load i32, i32* %num_y_points5, align 4, !tbaa !26
  %cmp = icmp slt i32 %44, %46
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %47 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %48 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %49 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %scaling_points_y = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %49, i32 0, i32 2
  %50 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds [14 x [2 x i32]], [14 x [2 x i32]]* %scaling_points_y, i32 0, i32 %50
  %arrayidx7 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx, i32 0, i32 0
  %51 = load i32, i32* %arrayidx7, align 4, !tbaa !19
  %52 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %scaling_points_y8 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %52, i32 0, i32 2
  %53 = load i32, i32* %i, align 4, !tbaa !19
  %arrayidx9 = getelementptr inbounds [14 x [2 x i32]], [14 x [2 x i32]]* %scaling_points_y8, i32 0, i32 %53
  %arrayidx10 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx9, i32 0, i32 1
  %54 = load i32, i32* %arrayidx10, align 4, !tbaa !19
  %call11 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %48, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.30, i32 0, i32 0), i32 %51, i32 %54)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %55 = load i32, i32* %i, align 4, !tbaa !19
  %inc = add nsw i32 %55, 1
  store i32 %inc, i32* %i, align 4, !tbaa !19
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %56 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %57 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %num_cb_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %57, i32 0, i32 5
  %58 = load i32, i32* %num_cb_points, align 4, !tbaa !27
  %call12 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %56, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.15, i32 0, i32 0), i32 %58)
  %59 = bitcast i32* %i13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #4
  store i32 0, i32* %i13, align 4, !tbaa !19
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc26, %for.end
  %60 = load i32, i32* %i13, align 4, !tbaa !19
  %61 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %num_cb_points15 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %61, i32 0, i32 5
  %62 = load i32, i32* %num_cb_points15, align 4, !tbaa !27
  %cmp16 = icmp slt i32 %60, %62
  br i1 %cmp16, label %for.body19, label %for.cond.cleanup18

for.cond.cleanup18:                               ; preds = %for.cond14
  %63 = bitcast i32* %i13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #4
  br label %for.end28

for.body19:                                       ; preds = %for.cond14
  %64 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %65 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %scaling_points_cb = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %65, i32 0, i32 4
  %66 = load i32, i32* %i13, align 4, !tbaa !19
  %arrayidx20 = getelementptr inbounds [10 x [2 x i32]], [10 x [2 x i32]]* %scaling_points_cb, i32 0, i32 %66
  %arrayidx21 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx20, i32 0, i32 0
  %67 = load i32, i32* %arrayidx21, align 4, !tbaa !19
  %68 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %scaling_points_cb22 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %68, i32 0, i32 4
  %69 = load i32, i32* %i13, align 4, !tbaa !19
  %arrayidx23 = getelementptr inbounds [10 x [2 x i32]], [10 x [2 x i32]]* %scaling_points_cb22, i32 0, i32 %69
  %arrayidx24 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx23, i32 0, i32 1
  %70 = load i32, i32* %arrayidx24, align 4, !tbaa !19
  %call25 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %64, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.30, i32 0, i32 0), i32 %67, i32 %70)
  br label %for.inc26

for.inc26:                                        ; preds = %for.body19
  %71 = load i32, i32* %i13, align 4, !tbaa !19
  %inc27 = add nsw i32 %71, 1
  store i32 %inc27, i32* %i13, align 4, !tbaa !19
  br label %for.cond14

for.end28:                                        ; preds = %for.cond.cleanup18
  %72 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %73 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %num_cr_points = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %73, i32 0, i32 7
  %74 = load i32, i32* %num_cr_points, align 4, !tbaa !28
  %call29 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %72, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.18, i32 0, i32 0), i32 %74)
  %75 = bitcast i32* %i30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #4
  store i32 0, i32* %i30, align 4, !tbaa !19
  br label %for.cond31

for.cond31:                                       ; preds = %for.inc43, %for.end28
  %76 = load i32, i32* %i30, align 4, !tbaa !19
  %77 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %num_cr_points32 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %77, i32 0, i32 7
  %78 = load i32, i32* %num_cr_points32, align 4, !tbaa !28
  %cmp33 = icmp slt i32 %76, %78
  br i1 %cmp33, label %for.body36, label %for.cond.cleanup35

for.cond.cleanup35:                               ; preds = %for.cond31
  %79 = bitcast i32* %i30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #4
  br label %for.end45

for.body36:                                       ; preds = %for.cond31
  %80 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %81 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %scaling_points_cr = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %81, i32 0, i32 6
  %82 = load i32, i32* %i30, align 4, !tbaa !19
  %arrayidx37 = getelementptr inbounds [10 x [2 x i32]], [10 x [2 x i32]]* %scaling_points_cr, i32 0, i32 %82
  %arrayidx38 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx37, i32 0, i32 0
  %83 = load i32, i32* %arrayidx38, align 4, !tbaa !19
  %84 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %scaling_points_cr39 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %84, i32 0, i32 6
  %85 = load i32, i32* %i30, align 4, !tbaa !19
  %arrayidx40 = getelementptr inbounds [10 x [2 x i32]], [10 x [2 x i32]]* %scaling_points_cr39, i32 0, i32 %85
  %arrayidx41 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx40, i32 0, i32 1
  %86 = load i32, i32* %arrayidx41, align 4, !tbaa !19
  %call42 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %80, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.30, i32 0, i32 0), i32 %83, i32 %86)
  br label %for.inc43

for.inc43:                                        ; preds = %for.body36
  %87 = load i32, i32* %i30, align 4, !tbaa !19
  %inc44 = add nsw i32 %87, 1
  store i32 %inc44, i32* %i30, align 4, !tbaa !19
  br label %for.cond31

for.end45:                                        ; preds = %for.cond.cleanup35
  %88 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call46 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %88, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.21, i32 0, i32 0))
  %89 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #4
  %90 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %ar_coeff_lag47 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %90, i32 0, i32 9
  %91 = load i32, i32* %ar_coeff_lag47, align 4, !tbaa !29
  %mul = mul nsw i32 2, %91
  %92 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %ar_coeff_lag48 = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %92, i32 0, i32 9
  %93 = load i32, i32* %ar_coeff_lag48, align 4, !tbaa !29
  %add = add nsw i32 %93, 1
  %mul49 = mul nsw i32 %mul, %add
  store i32 %mul49, i32* %n, align 4, !tbaa !19
  %94 = bitcast i32* %i50 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %94) #4
  store i32 0, i32* %i50, align 4, !tbaa !19
  br label %for.cond51

for.cond51:                                       ; preds = %for.inc58, %for.end45
  %95 = load i32, i32* %i50, align 4, !tbaa !19
  %96 = load i32, i32* %n, align 4, !tbaa !19
  %cmp52 = icmp slt i32 %95, %96
  br i1 %cmp52, label %for.body55, label %for.cond.cleanup54

for.cond.cleanup54:                               ; preds = %for.cond51
  %97 = bitcast i32* %i50 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #4
  br label %for.end60

for.body55:                                       ; preds = %for.cond51
  %98 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %99 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %ar_coeffs_y = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %99, i32 0, i32 10
  %100 = load i32, i32* %i50, align 4, !tbaa !19
  %arrayidx56 = getelementptr inbounds [24 x i32], [24 x i32]* %ar_coeffs_y, i32 0, i32 %100
  %101 = load i32, i32* %arrayidx56, align 4, !tbaa !19
  %call57 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %98, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.31, i32 0, i32 0), i32 %101)
  br label %for.inc58

for.inc58:                                        ; preds = %for.body55
  %102 = load i32, i32* %i50, align 4, !tbaa !19
  %inc59 = add nsw i32 %102, 1
  store i32 %inc59, i32* %i50, align 4, !tbaa !19
  br label %for.cond51

for.end60:                                        ; preds = %for.cond.cleanup54
  %103 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call61 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %103, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.24, i32 0, i32 0))
  %104 = bitcast i32* %i62 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %104) #4
  store i32 0, i32* %i62, align 4, !tbaa !19
  br label %for.cond63

for.cond63:                                       ; preds = %for.inc70, %for.end60
  %105 = load i32, i32* %i62, align 4, !tbaa !19
  %106 = load i32, i32* %n, align 4, !tbaa !19
  %cmp64 = icmp sle i32 %105, %106
  br i1 %cmp64, label %for.body67, label %for.cond.cleanup66

for.cond.cleanup66:                               ; preds = %for.cond63
  %107 = bitcast i32* %i62 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #4
  br label %for.end72

for.body67:                                       ; preds = %for.cond63
  %108 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %109 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %ar_coeffs_cb = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %109, i32 0, i32 11
  %110 = load i32, i32* %i62, align 4, !tbaa !19
  %arrayidx68 = getelementptr inbounds [25 x i32], [25 x i32]* %ar_coeffs_cb, i32 0, i32 %110
  %111 = load i32, i32* %arrayidx68, align 4, !tbaa !19
  %call69 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %108, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.31, i32 0, i32 0), i32 %111)
  br label %for.inc70

for.inc70:                                        ; preds = %for.body67
  %112 = load i32, i32* %i62, align 4, !tbaa !19
  %inc71 = add nsw i32 %112, 1
  store i32 %inc71, i32* %i62, align 4, !tbaa !19
  br label %for.cond63

for.end72:                                        ; preds = %for.cond.cleanup66
  %113 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call73 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %113, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.26, i32 0, i32 0))
  %114 = bitcast i32* %i74 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %114) #4
  store i32 0, i32* %i74, align 4, !tbaa !19
  br label %for.cond75

for.cond75:                                       ; preds = %for.inc82, %for.end72
  %115 = load i32, i32* %i74, align 4, !tbaa !19
  %116 = load i32, i32* %n, align 4, !tbaa !19
  %cmp76 = icmp sle i32 %115, %116
  br i1 %cmp76, label %for.body79, label %for.cond.cleanup78

for.cond.cleanup78:                               ; preds = %for.cond75
  %117 = bitcast i32* %i74 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #4
  br label %for.end84

for.body79:                                       ; preds = %for.cond75
  %118 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %119 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %pars, align 4, !tbaa !2
  %ar_coeffs_cr = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %119, i32 0, i32 12
  %120 = load i32, i32* %i74, align 4, !tbaa !19
  %arrayidx80 = getelementptr inbounds [25 x i32], [25 x i32]* %ar_coeffs_cr, i32 0, i32 %120
  %121 = load i32, i32* %arrayidx80, align 4, !tbaa !19
  %call81 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %118, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.31, i32 0, i32 0), i32 %121)
  br label %for.inc82

for.inc82:                                        ; preds = %for.body79
  %122 = load i32, i32* %i74, align 4, !tbaa !19
  %inc83 = add nsw i32 %122, 1
  store i32 %inc83, i32* %i74, align 4, !tbaa !19
  br label %for.cond75

for.end84:                                        ; preds = %for.cond.cleanup78
  %123 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call85 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %123, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.6, i32 0, i32 0))
  %124 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #4
  br label %if.end

if.end:                                           ; preds = %for.end84, %entry
  %125 = bitcast %struct.aom_film_grain_t** %pars to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_film_grain_table_free(%struct.aom_film_grain_table_t* %t) #0 {
entry:
  %t.addr = alloca %struct.aom_film_grain_table_t*, align 4
  %entry1 = alloca %struct.aom_film_grain_table_entry_t*, align 4
  %next = alloca %struct.aom_film_grain_table_entry_t*, align 4
  store %struct.aom_film_grain_table_t* %t, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %0 = bitcast %struct.aom_film_grain_table_entry_t** %entry1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %head = getelementptr inbounds %struct.aom_film_grain_table_t, %struct.aom_film_grain_table_t* %1, i32 0, i32 0
  %2 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %head, align 4, !tbaa !15
  store %struct.aom_film_grain_table_entry_t* %2, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %3 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_film_grain_table_entry_t* %3, null
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = bitcast %struct.aom_film_grain_table_entry_t** %next to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %next2 = getelementptr inbounds %struct.aom_film_grain_table_entry_t, %struct.aom_film_grain_table_entry_t* %5, i32 0, i32 3
  %6 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %next2, align 8, !tbaa !10
  store %struct.aom_film_grain_table_entry_t* %6, %struct.aom_film_grain_table_entry_t** %next, align 4, !tbaa !2
  %7 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %8 = bitcast %struct.aom_film_grain_table_entry_t* %7 to i8*
  call void @aom_free(i8* %8)
  %9 = load %struct.aom_film_grain_table_entry_t*, %struct.aom_film_grain_table_entry_t** %next, align 4, !tbaa !2
  store %struct.aom_film_grain_table_entry_t* %9, %struct.aom_film_grain_table_entry_t** %entry1, align 4, !tbaa !2
  %10 = bitcast %struct.aom_film_grain_table_entry_t** %next to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %11 = load %struct.aom_film_grain_table_t*, %struct.aom_film_grain_table_t** %t.addr, align 4, !tbaa !2
  %12 = bitcast %struct.aom_film_grain_table_t* %11 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %12, i8 0, i32 8, i1 false)
  %13 = bitcast %struct.aom_film_grain_table_entry_t** %entry1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  ret void
}

declare i32 @fscanf(%struct._IO_FILE*, i8*, ...) #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"long long", !4, i64 0}
!8 = !{!9, !3, i64 4}
!9 = !{!"", !3, i64 0, !3, i64 4}
!10 = !{!11, !3, i64 664}
!11 = !{!"aom_film_grain_table_entry_t", !12, i64 0, !7, i64 648, !7, i64 656, !3, i64 664}
!12 = !{!"", !13, i64 0, !13, i64 4, !4, i64 8, !13, i64 120, !4, i64 124, !13, i64 204, !4, i64 208, !13, i64 288, !13, i64 292, !13, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !13, i64 596, !13, i64 600, !13, i64 604, !13, i64 608, !13, i64 612, !13, i64 616, !13, i64 620, !13, i64 624, !13, i64 628, !13, i64 632, !13, i64 636, !13, i64 640, !14, i64 644}
!13 = !{!"int", !4, i64 0}
!14 = !{!"short", !4, i64 0}
!15 = !{!9, !3, i64 0}
!16 = !{!11, !7, i64 648}
!17 = !{!11, !7, i64 656}
!18 = !{i64 0, i64 4, !19, i64 4, i64 4, !19, i64 8, i64 112, !20, i64 120, i64 4, !19, i64 124, i64 80, !20, i64 204, i64 4, !19, i64 208, i64 80, !20, i64 288, i64 4, !19, i64 292, i64 4, !19, i64 296, i64 4, !19, i64 300, i64 96, !20, i64 396, i64 100, !20, i64 496, i64 100, !20, i64 596, i64 4, !19, i64 600, i64 4, !19, i64 604, i64 4, !19, i64 608, i64 4, !19, i64 612, i64 4, !19, i64 616, i64 4, !19, i64 620, i64 4, !19, i64 624, i64 4, !19, i64 628, i64 4, !19, i64 632, i64 4, !19, i64 636, i64 4, !19, i64 640, i64 4, !19, i64 644, i64 2, !21}
!19 = !{!13, !13, i64 0}
!20 = !{!4, !4, i64 0}
!21 = !{!14, !14, i64 0}
!22 = !{!12, !14, i64 644}
!23 = !{!24, !4, i64 0}
!24 = !{!"aom_internal_error_info", !4, i64 0, !13, i64 4, !4, i64 8, !13, i64 88, !4, i64 92}
!25 = !{!12, !13, i64 4}
!26 = !{!12, !13, i64 120}
!27 = !{!12, !13, i64 204}
!28 = !{!12, !13, i64 288}
!29 = !{!12, !13, i64 296}
!30 = !{!12, !13, i64 0}
!31 = !{!12, !13, i64 596}
!32 = !{!12, !13, i64 640}
!33 = !{!12, !13, i64 292}
!34 = !{!12, !13, i64 636}
!35 = !{!12, !13, i64 624}
!36 = !{!12, !13, i64 600}
!37 = !{!12, !13, i64 604}
!38 = !{!12, !13, i64 608}
!39 = !{!12, !13, i64 612}
!40 = !{!12, !13, i64 616}
!41 = !{!12, !13, i64 620}
