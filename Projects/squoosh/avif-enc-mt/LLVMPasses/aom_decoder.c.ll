; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom/src/aom_decoder.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom/src/aom_decoder.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.aom_codec_ctx = type { i8*, %struct.aom_codec_iface*, i32, i8*, i32, %union.anon.0, %struct.aom_codec_priv* }
%union.anon.0 = type { %struct.aom_codec_dec_cfg* }
%struct.aom_codec_priv = type { i8*, i32, %struct.anon.1 }
%struct.anon.1 = type { %struct.aom_fixed_buf, i32, i32, %struct.aom_codec_cx_pkt }
%struct.aom_fixed_buf = type { i8*, i32 }
%struct.aom_codec_cx_pkt = type { i32, %union.anon }
%union.anon = type { %struct.aom_psnr_pkt, [48 x i8] }
%struct.aom_psnr_pkt = type { [4 x i32], [4 x i64], [4 x double] }
%struct.aom_codec_iface = type { i8*, i32, i32, i32 (%struct.aom_codec_ctx*)*, i32 (%struct.aom_codec_alg_priv*)*, %struct.aom_codec_ctrl_fn_map*, %struct.aom_codec_dec_iface, %struct.aom_codec_enc_iface }
%struct.aom_codec_alg_priv = type opaque
%struct.aom_codec_ctrl_fn_map = type { i32, i32 (%struct.aom_codec_alg_priv*, i8*)* }
%struct.aom_codec_dec_iface = type { i32 (i8*, i32, %struct.aom_codec_stream_info*)*, i32 (%struct.aom_codec_alg_priv*, %struct.aom_codec_stream_info*)*, i32 (%struct.aom_codec_alg_priv*, i8*, i32, i8*)*, %struct.aom_image* (%struct.aom_codec_alg_priv*, i8**)*, i32 (%struct.aom_codec_alg_priv*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, i8*)* }
%struct.aom_codec_stream_info = type { i32, i32, i32, i32, i32, i32 }
%struct.aom_image = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [3 x i8*], [3 x i32], i32, i32, i32, i32, i8*, i8*, i32, i32, %struct.aom_metadata_array*, i8* }
%struct.aom_metadata_array = type opaque
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.aom_codec_enc_iface = type { i32, %struct.aom_codec_enc_cfg*, i32 (%struct.aom_codec_alg_priv*, %struct.aom_image*, i64, i32, i32)*, %struct.aom_codec_cx_pkt* (%struct.aom_codec_alg_priv*, i8**)*, i32 (%struct.aom_codec_alg_priv*, %struct.aom_codec_enc_cfg*)*, %struct.aom_fixed_buf* (%struct.aom_codec_alg_priv*)*, %struct.aom_image* (%struct.aom_codec_alg_priv*)* }
%struct.aom_codec_enc_cfg = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.aom_rational, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.aom_fixed_buf, %struct.aom_fixed_buf, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [64 x i32], [64 x i32], i32, [5 x i32], %struct.cfg_options }
%struct.aom_rational = type { i32, i32 }
%struct.cfg_options = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.aom_codec_dec_cfg = type { i32, i32, i32, i32 }

; Function Attrs: nounwind
define hidden i32 @aom_codec_dec_init_ver(%struct.aom_codec_ctx* %ctx, %struct.aom_codec_iface* %iface, %struct.aom_codec_dec_cfg* %cfg, i32 %flags, i32 %ver) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_ctx*, align 4
  %iface.addr = alloca %struct.aom_codec_iface*, align 4
  %cfg.addr = alloca %struct.aom_codec_dec_cfg*, align 4
  %flags.addr = alloca i32, align 4
  %ver.addr = alloca i32, align 4
  %res = alloca i32, align 4
  store %struct.aom_codec_ctx* %ctx, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  store %struct.aom_codec_iface* %iface, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  store %struct.aom_codec_dec_cfg* %cfg, %struct.aom_codec_dec_cfg** %cfg.addr, align 4, !tbaa !2
  store i32 %flags, i32* %flags.addr, align 4, !tbaa !6
  store i32 %ver, i32* %ver.addr, align 4, !tbaa !8
  %0 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %ver.addr, align 4, !tbaa !8
  %cmp = icmp ne i32 %1, 20
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 3, i32* %res, align 4, !tbaa !10
  br label %if.end23

if.else:                                          ; preds = %entry
  %2 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_ctx* %2, null
  br i1 %tobool, label %lor.lhs.false, label %if.then2

lor.lhs.false:                                    ; preds = %if.else
  %3 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.aom_codec_iface* %3, null
  br i1 %tobool1, label %if.else3, label %if.then2

if.then2:                                         ; preds = %lor.lhs.false, %if.else
  store i32 8, i32* %res, align 4, !tbaa !10
  br label %if.end22

if.else3:                                         ; preds = %lor.lhs.false
  %4 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %abi_version = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %4, i32 0, i32 1
  %5 = load i32, i32* %abi_version, align 4, !tbaa !11
  %cmp4 = icmp ne i32 %5, 7
  br i1 %cmp4, label %if.then5, label %if.else6

if.then5:                                         ; preds = %if.else3
  store i32 3, i32* %res, align 4, !tbaa !10
  br label %if.end21

if.else6:                                         ; preds = %if.else3
  %6 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %caps = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %6, i32 0, i32 2
  %7 = load i32, i32* %caps, align 4, !tbaa !15
  %and = and i32 %7, 1
  %tobool7 = icmp ne i32 %and, 0
  br i1 %tobool7, label %if.else9, label %if.then8

if.then8:                                         ; preds = %if.else6
  store i32 4, i32* %res, align 4, !tbaa !10
  br label %if.end20

if.else9:                                         ; preds = %if.else6
  %8 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %9 = bitcast %struct.aom_codec_ctx* %8 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %9, i8 0, i32 28, i1 false)
  %10 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %11 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface10 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %11, i32 0, i32 1
  store %struct.aom_codec_iface* %10, %struct.aom_codec_iface** %iface10, align 4, !tbaa !16
  %12 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %name = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %12, i32 0, i32 0
  %13 = load i8*, i8** %name, align 4, !tbaa !18
  %14 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %name11 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %14, i32 0, i32 0
  store i8* %13, i8** %name11, align 4, !tbaa !19
  %15 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %15, i32 0, i32 6
  store %struct.aom_codec_priv* null, %struct.aom_codec_priv** %priv, align 4, !tbaa !20
  %16 = load i32, i32* %flags.addr, align 4, !tbaa !6
  %17 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %init_flags = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %17, i32 0, i32 4
  store i32 %16, i32* %init_flags, align 4, !tbaa !21
  %18 = load %struct.aom_codec_dec_cfg*, %struct.aom_codec_dec_cfg** %cfg.addr, align 4, !tbaa !2
  %19 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %config = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %19, i32 0, i32 5
  %dec = bitcast %union.anon.0* %config to %struct.aom_codec_dec_cfg**
  store %struct.aom_codec_dec_cfg* %18, %struct.aom_codec_dec_cfg** %dec, align 4, !tbaa !10
  %20 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface12 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %20, i32 0, i32 1
  %21 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface12, align 4, !tbaa !16
  %init = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %21, i32 0, i32 3
  %22 = load i32 (%struct.aom_codec_ctx*)*, i32 (%struct.aom_codec_ctx*)** %init, align 4, !tbaa !22
  %23 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %call = call i32 %22(%struct.aom_codec_ctx* %23)
  store i32 %call, i32* %res, align 4, !tbaa !10
  %24 = load i32, i32* %res, align 4, !tbaa !10
  %tobool13 = icmp ne i32 %24, 0
  br i1 %tobool13, label %if.then14, label %if.end

if.then14:                                        ; preds = %if.else9
  %25 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv15 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %25, i32 0, i32 6
  %26 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv15, align 4, !tbaa !20
  %tobool16 = icmp ne %struct.aom_codec_priv* %26, null
  br i1 %tobool16, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then14
  %27 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv17 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %27, i32 0, i32 6
  %28 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv17, align 4, !tbaa !20
  %err_detail = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %28, i32 0, i32 0
  %29 = load i8*, i8** %err_detail, align 8, !tbaa !23
  br label %cond.end

cond.false:                                       ; preds = %if.then14
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %29, %cond.true ], [ null, %cond.false ]
  %30 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err_detail18 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %30, i32 0, i32 3
  store i8* %cond, i8** %err_detail18, align 4, !tbaa !28
  %31 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %call19 = call i32 @aom_codec_destroy(%struct.aom_codec_ctx* %31)
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.else9
  br label %if.end20

if.end20:                                         ; preds = %if.end, %if.then8
  br label %if.end21

if.end21:                                         ; preds = %if.end20, %if.then5
  br label %if.end22

if.end22:                                         ; preds = %if.end21, %if.then2
  br label %if.end23

if.end23:                                         ; preds = %if.end22, %if.then
  %32 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool24 = icmp ne %struct.aom_codec_ctx* %32, null
  br i1 %tobool24, label %cond.true25, label %cond.false26

cond.true25:                                      ; preds = %if.end23
  %33 = load i32, i32* %res, align 4, !tbaa !10
  %34 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %34, i32 0, i32 2
  store i32 %33, i32* %err, align 4, !tbaa !29
  br label %cond.end27

cond.false26:                                     ; preds = %if.end23
  %35 = load i32, i32* %res, align 4, !tbaa !10
  br label %cond.end27

cond.end27:                                       ; preds = %cond.false26, %cond.true25
  %cond28 = phi i32 [ %33, %cond.true25 ], [ %35, %cond.false26 ]
  %36 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  ret i32 %cond28
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

declare i32 @aom_codec_destroy(%struct.aom_codec_ctx*) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden i32 @aom_codec_peek_stream_info(%struct.aom_codec_iface* %iface, i8* %data, i32 %data_sz, %struct.aom_codec_stream_info* %si) #0 {
entry:
  %iface.addr = alloca %struct.aom_codec_iface*, align 4
  %data.addr = alloca i8*, align 4
  %data_sz.addr = alloca i32, align 4
  %si.addr = alloca %struct.aom_codec_stream_info*, align 4
  %res = alloca i32, align 4
  store %struct.aom_codec_iface* %iface, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %data_sz, i32* %data_sz.addr, align 4, !tbaa !6
  store %struct.aom_codec_stream_info* %si, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %0 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_iface* %1, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %2 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %tobool1 = icmp ne i8* %2, null
  br i1 %tobool1, label %lor.lhs.false2, label %if.then

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %3 = load i32, i32* %data_sz.addr, align 4, !tbaa !6
  %tobool3 = icmp ne i32 %3, 0
  br i1 %tobool3, label %lor.lhs.false4, label %if.then

lor.lhs.false4:                                   ; preds = %lor.lhs.false2
  %4 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %tobool5 = icmp ne %struct.aom_codec_stream_info* %4, null
  br i1 %tobool5, label %if.else, label %if.then

if.then:                                          ; preds = %lor.lhs.false4, %lor.lhs.false2, %lor.lhs.false, %entry
  store i32 8, i32* %res, align 4, !tbaa !10
  br label %if.end

if.else:                                          ; preds = %lor.lhs.false4
  %5 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %w = getelementptr inbounds %struct.aom_codec_stream_info, %struct.aom_codec_stream_info* %5, i32 0, i32 0
  store i32 0, i32* %w, align 4, !tbaa !30
  %6 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %h = getelementptr inbounds %struct.aom_codec_stream_info, %struct.aom_codec_stream_info* %6, i32 0, i32 1
  store i32 0, i32* %h, align 4, !tbaa !32
  %7 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface.addr, align 4, !tbaa !2
  %dec = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %7, i32 0, i32 6
  %peek_si = getelementptr inbounds %struct.aom_codec_dec_iface, %struct.aom_codec_dec_iface* %dec, i32 0, i32 0
  %8 = load i32 (i8*, i32, %struct.aom_codec_stream_info*)*, i32 (i8*, i32, %struct.aom_codec_stream_info*)** %peek_si, align 4, !tbaa !33
  %9 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %10 = load i32, i32* %data_sz.addr, align 4, !tbaa !6
  %11 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %call = call i32 %8(i8* %9, i32 %10, %struct.aom_codec_stream_info* %11)
  store i32 %call, i32* %res, align 4, !tbaa !10
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %12 = load i32, i32* %res, align 4, !tbaa !10
  %13 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  ret i32 %12
}

; Function Attrs: nounwind
define hidden i32 @aom_codec_get_stream_info(%struct.aom_codec_ctx* %ctx, %struct.aom_codec_stream_info* %si) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_ctx*, align 4
  %si.addr = alloca %struct.aom_codec_stream_info*, align 4
  %res = alloca i32, align 4
  store %struct.aom_codec_ctx* %ctx, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  store %struct.aom_codec_stream_info* %si, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %0 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_ctx* %1, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %2 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.aom_codec_stream_info* %2, null
  br i1 %tobool1, label %if.else, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 8, i32* %res, align 4, !tbaa !10
  br label %if.end9

if.else:                                          ; preds = %lor.lhs.false
  %3 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %3, i32 0, i32 1
  %4 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface, align 4, !tbaa !16
  %tobool2 = icmp ne %struct.aom_codec_iface* %4, null
  br i1 %tobool2, label %lor.lhs.false3, label %if.then5

lor.lhs.false3:                                   ; preds = %if.else
  %5 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %5, i32 0, i32 6
  %6 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv, align 4, !tbaa !20
  %tobool4 = icmp ne %struct.aom_codec_priv* %6, null
  br i1 %tobool4, label %if.else6, label %if.then5

if.then5:                                         ; preds = %lor.lhs.false3, %if.else
  store i32 1, i32* %res, align 4, !tbaa !10
  br label %if.end

if.else6:                                         ; preds = %lor.lhs.false3
  %7 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %w = getelementptr inbounds %struct.aom_codec_stream_info, %struct.aom_codec_stream_info* %7, i32 0, i32 0
  store i32 0, i32* %w, align 4, !tbaa !30
  %8 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %h = getelementptr inbounds %struct.aom_codec_stream_info, %struct.aom_codec_stream_info* %8, i32 0, i32 1
  store i32 0, i32* %h, align 4, !tbaa !32
  %9 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface7 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %9, i32 0, i32 1
  %10 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface7, align 4, !tbaa !16
  %dec = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %10, i32 0, i32 6
  %get_si = getelementptr inbounds %struct.aom_codec_dec_iface, %struct.aom_codec_dec_iface* %dec, i32 0, i32 1
  %11 = load i32 (%struct.aom_codec_alg_priv*, %struct.aom_codec_stream_info*)*, i32 (%struct.aom_codec_alg_priv*, %struct.aom_codec_stream_info*)** %get_si, align 4, !tbaa !34
  %12 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %call = call %struct.aom_codec_alg_priv* @get_alg_priv(%struct.aom_codec_ctx* %12)
  %13 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %call8 = call i32 %11(%struct.aom_codec_alg_priv* %call, %struct.aom_codec_stream_info* %13)
  store i32 %call8, i32* %res, align 4, !tbaa !10
  br label %if.end

if.end:                                           ; preds = %if.else6, %if.then5
  br label %if.end9

if.end9:                                          ; preds = %if.end, %if.then
  %14 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool10 = icmp ne %struct.aom_codec_ctx* %14, null
  br i1 %tobool10, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end9
  %15 = load i32, i32* %res, align 4, !tbaa !10
  %16 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %16, i32 0, i32 2
  store i32 %15, i32* %err, align 4, !tbaa !29
  br label %cond.end

cond.false:                                       ; preds = %if.end9
  %17 = load i32, i32* %res, align 4, !tbaa !10
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %15, %cond.true ], [ %17, %cond.false ]
  %18 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #4
  ret i32 %cond
}

; Function Attrs: nounwind
define internal %struct.aom_codec_alg_priv* @get_alg_priv(%struct.aom_codec_ctx* %ctx) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_ctx*, align 4
  store %struct.aom_codec_ctx* %ctx, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %0 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %0, i32 0, i32 6
  %1 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv, align 4, !tbaa !20
  %2 = bitcast %struct.aom_codec_priv* %1 to %struct.aom_codec_alg_priv*
  ret %struct.aom_codec_alg_priv* %2
}

; Function Attrs: nounwind
define hidden i32 @aom_codec_decode(%struct.aom_codec_ctx* %ctx, i8* %data, i32 %data_sz, i8* %user_priv) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_ctx*, align 4
  %data.addr = alloca i8*, align 4
  %data_sz.addr = alloca i32, align 4
  %user_priv.addr = alloca i8*, align 4
  %res = alloca i32, align 4
  store %struct.aom_codec_ctx* %ctx, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %data_sz, i32* %data_sz.addr, align 4, !tbaa !6
  store i8* %user_priv, i8** %user_priv.addr, align 4, !tbaa !2
  %0 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_ctx* %1, null
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  store i32 8, i32* %res, align 4, !tbaa !10
  br label %if.end7

if.else:                                          ; preds = %entry
  %2 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %2, i32 0, i32 1
  %3 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface, align 4, !tbaa !16
  %tobool1 = icmp ne %struct.aom_codec_iface* %3, null
  br i1 %tobool1, label %lor.lhs.false, label %if.then3

lor.lhs.false:                                    ; preds = %if.else
  %4 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %4, i32 0, i32 6
  %5 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv, align 4, !tbaa !20
  %tobool2 = icmp ne %struct.aom_codec_priv* %5, null
  br i1 %tobool2, label %if.else4, label %if.then3

if.then3:                                         ; preds = %lor.lhs.false, %if.else
  store i32 1, i32* %res, align 4, !tbaa !10
  br label %if.end

if.else4:                                         ; preds = %lor.lhs.false
  %6 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface5 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %6, i32 0, i32 1
  %7 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface5, align 4, !tbaa !16
  %dec = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %7, i32 0, i32 6
  %decode = getelementptr inbounds %struct.aom_codec_dec_iface, %struct.aom_codec_dec_iface* %dec, i32 0, i32 2
  %8 = load i32 (%struct.aom_codec_alg_priv*, i8*, i32, i8*)*, i32 (%struct.aom_codec_alg_priv*, i8*, i32, i8*)** %decode, align 4, !tbaa !35
  %9 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %call = call %struct.aom_codec_alg_priv* @get_alg_priv(%struct.aom_codec_ctx* %9)
  %10 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %11 = load i32, i32* %data_sz.addr, align 4, !tbaa !6
  %12 = load i8*, i8** %user_priv.addr, align 4, !tbaa !2
  %call6 = call i32 %8(%struct.aom_codec_alg_priv* %call, i8* %10, i32 %11, i8* %12)
  store i32 %call6, i32* %res, align 4, !tbaa !10
  br label %if.end

if.end:                                           ; preds = %if.else4, %if.then3
  br label %if.end7

if.end7:                                          ; preds = %if.end, %if.then
  %13 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool8 = icmp ne %struct.aom_codec_ctx* %13, null
  br i1 %tobool8, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end7
  %14 = load i32, i32* %res, align 4, !tbaa !10
  %15 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %15, i32 0, i32 2
  store i32 %14, i32* %err, align 4, !tbaa !29
  br label %cond.end

cond.false:                                       ; preds = %if.end7
  %16 = load i32, i32* %res, align 4, !tbaa !10
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %14, %cond.true ], [ %16, %cond.false ]
  %17 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  ret i32 %cond
}

; Function Attrs: nounwind
define hidden %struct.aom_image* @aom_codec_get_frame(%struct.aom_codec_ctx* %ctx, i8** %iter) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_ctx*, align 4
  %iter.addr = alloca i8**, align 4
  %img = alloca %struct.aom_image*, align 4
  store %struct.aom_codec_ctx* %ctx, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  store i8** %iter, i8*** %iter.addr, align 4, !tbaa !2
  %0 = bitcast %struct.aom_image** %img to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_ctx* %1, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %2 = load i8**, i8*** %iter.addr, align 4, !tbaa !2
  %tobool1 = icmp ne i8** %2, null
  br i1 %tobool1, label %lor.lhs.false2, label %if.then

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %3 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %3, i32 0, i32 1
  %4 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface, align 4, !tbaa !16
  %tobool3 = icmp ne %struct.aom_codec_iface* %4, null
  br i1 %tobool3, label %lor.lhs.false4, label %if.then

lor.lhs.false4:                                   ; preds = %lor.lhs.false2
  %5 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %5, i32 0, i32 6
  %6 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv, align 4, !tbaa !20
  %tobool5 = icmp ne %struct.aom_codec_priv* %6, null
  br i1 %tobool5, label %if.else, label %if.then

if.then:                                          ; preds = %lor.lhs.false4, %lor.lhs.false2, %lor.lhs.false, %entry
  store %struct.aom_image* null, %struct.aom_image** %img, align 4, !tbaa !2
  br label %if.end

if.else:                                          ; preds = %lor.lhs.false4
  %7 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface6 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %7, i32 0, i32 1
  %8 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface6, align 4, !tbaa !16
  %dec = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %8, i32 0, i32 6
  %get_frame = getelementptr inbounds %struct.aom_codec_dec_iface, %struct.aom_codec_dec_iface* %dec, i32 0, i32 3
  %9 = load %struct.aom_image* (%struct.aom_codec_alg_priv*, i8**)*, %struct.aom_image* (%struct.aom_codec_alg_priv*, i8**)** %get_frame, align 4, !tbaa !36
  %10 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %call = call %struct.aom_codec_alg_priv* @get_alg_priv(%struct.aom_codec_ctx* %10)
  %11 = load i8**, i8*** %iter.addr, align 4, !tbaa !2
  %call7 = call %struct.aom_image* %9(%struct.aom_codec_alg_priv* %call, i8** %11)
  store %struct.aom_image* %call7, %struct.aom_image** %img, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %12 = load %struct.aom_image*, %struct.aom_image** %img, align 4, !tbaa !2
  %13 = bitcast %struct.aom_image** %img to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  ret %struct.aom_image* %12
}

; Function Attrs: nounwind
define hidden i32 @aom_codec_set_frame_buffer_functions(%struct.aom_codec_ctx* %ctx, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* %cb_get, i32 (i8*, %struct.aom_codec_frame_buffer*)* %cb_release, i8* %cb_priv) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_ctx*, align 4
  %cb_get.addr = alloca i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, align 4
  %cb_release.addr = alloca i32 (i8*, %struct.aom_codec_frame_buffer*)*, align 4
  %cb_priv.addr = alloca i8*, align 4
  %res = alloca i32, align 4
  store %struct.aom_codec_ctx* %ctx, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  store i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* %cb_get, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %cb_get.addr, align 4, !tbaa !2
  store i32 (i8*, %struct.aom_codec_frame_buffer*)* %cb_release, i32 (i8*, %struct.aom_codec_frame_buffer*)** %cb_release.addr, align 4, !tbaa !2
  store i8* %cb_priv, i8** %cb_priv.addr, align 4, !tbaa !2
  %0 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_codec_ctx* %1, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %2 = load i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %cb_get.addr, align 4, !tbaa !2
  %tobool1 = icmp ne i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* %2, null
  br i1 %tobool1, label %lor.lhs.false2, label %if.then

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %3 = load i32 (i8*, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)** %cb_release.addr, align 4, !tbaa !2
  %tobool3 = icmp ne i32 (i8*, %struct.aom_codec_frame_buffer*)* %3, null
  br i1 %tobool3, label %if.else, label %if.then

if.then:                                          ; preds = %lor.lhs.false2, %lor.lhs.false, %entry
  store i32 8, i32* %res, align 4, !tbaa !10
  br label %if.end16

if.else:                                          ; preds = %lor.lhs.false2
  %4 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %4, i32 0, i32 1
  %5 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface, align 4, !tbaa !16
  %tobool4 = icmp ne %struct.aom_codec_iface* %5, null
  br i1 %tobool4, label %lor.lhs.false5, label %if.then7

lor.lhs.false5:                                   ; preds = %if.else
  %6 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %6, i32 0, i32 6
  %7 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv, align 4, !tbaa !20
  %tobool6 = icmp ne %struct.aom_codec_priv* %7, null
  br i1 %tobool6, label %if.else8, label %if.then7

if.then7:                                         ; preds = %lor.lhs.false5, %if.else
  store i32 1, i32* %res, align 4, !tbaa !10
  br label %if.end15

if.else8:                                         ; preds = %lor.lhs.false5
  %8 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface9 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %8, i32 0, i32 1
  %9 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface9, align 4, !tbaa !16
  %caps = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %9, i32 0, i32 2
  %10 = load i32, i32* %caps, align 4, !tbaa !15
  %and = and i32 %10, 2097152
  %tobool10 = icmp ne i32 %and, 0
  br i1 %tobool10, label %if.else12, label %if.then11

if.then11:                                        ; preds = %if.else8
  store i32 4, i32* %res, align 4, !tbaa !10
  br label %if.end

if.else12:                                        ; preds = %if.else8
  %11 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %iface13 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %11, i32 0, i32 1
  %12 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %iface13, align 4, !tbaa !16
  %dec = getelementptr inbounds %struct.aom_codec_iface, %struct.aom_codec_iface* %12, i32 0, i32 6
  %set_fb_fn = getelementptr inbounds %struct.aom_codec_dec_iface, %struct.aom_codec_dec_iface* %dec, i32 0, i32 4
  %13 = load i32 (%struct.aom_codec_alg_priv*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, i8*)*, i32 (%struct.aom_codec_alg_priv*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, i8*)** %set_fb_fn, align 4, !tbaa !37
  %14 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %call = call %struct.aom_codec_alg_priv* @get_alg_priv(%struct.aom_codec_ctx* %14)
  %15 = load i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %cb_get.addr, align 4, !tbaa !2
  %16 = load i32 (i8*, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)** %cb_release.addr, align 4, !tbaa !2
  %17 = load i8*, i8** %cb_priv.addr, align 4, !tbaa !2
  %call14 = call i32 %13(%struct.aom_codec_alg_priv* %call, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* %15, i32 (i8*, %struct.aom_codec_frame_buffer*)* %16, i8* %17)
  store i32 %call14, i32* %res, align 4, !tbaa !10
  br label %if.end

if.end:                                           ; preds = %if.else12, %if.then11
  br label %if.end15

if.end15:                                         ; preds = %if.end, %if.then7
  br label %if.end16

if.end16:                                         ; preds = %if.end15, %if.then
  %18 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %tobool17 = icmp ne %struct.aom_codec_ctx* %18, null
  br i1 %tobool17, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end16
  %19 = load i32, i32* %res, align 4, !tbaa !10
  %20 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %20, i32 0, i32 2
  store i32 %19, i32* %err, align 4, !tbaa !29
  br label %cond.end

cond.false:                                       ; preds = %if.end16
  %21 = load i32, i32* %res, align 4, !tbaa !10
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %19, %cond.true ], [ %21, %cond.false ]
  %22 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #4
  ret i32 %cond
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"long", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"int", !4, i64 0}
!10 = !{!4, !4, i64 0}
!11 = !{!12, !9, i64 4}
!12 = !{!"aom_codec_iface", !3, i64 0, !9, i64 4, !7, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !13, i64 24, !14, i64 44}
!13 = !{!"aom_codec_dec_iface", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!14 = !{!"aom_codec_enc_iface", !9, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24}
!15 = !{!12, !7, i64 8}
!16 = !{!17, !3, i64 4}
!17 = !{!"aom_codec_ctx", !3, i64 0, !3, i64 4, !4, i64 8, !3, i64 12, !7, i64 16, !4, i64 20, !3, i64 24}
!18 = !{!12, !3, i64 0}
!19 = !{!17, !3, i64 0}
!20 = !{!17, !3, i64 24}
!21 = !{!17, !7, i64 16}
!22 = !{!12, !3, i64 12}
!23 = !{!24, !3, i64 0}
!24 = !{!"aom_codec_priv", !3, i64 0, !7, i64 4, !25, i64 8}
!25 = !{!"", !26, i64 0, !9, i64 8, !9, i64 12, !27, i64 16}
!26 = !{!"aom_fixed_buf", !3, i64 0, !7, i64 4}
!27 = !{!"aom_codec_cx_pkt", !4, i64 0, !4, i64 8}
!28 = !{!17, !3, i64 12}
!29 = !{!17, !4, i64 8}
!30 = !{!31, !9, i64 0}
!31 = !{!"aom_codec_stream_info", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !9, i64 20}
!32 = !{!31, !9, i64 4}
!33 = !{!12, !3, i64 24}
!34 = !{!12, !3, i64 28}
!35 = !{!12, !3, i64 32}
!36 = !{!12, !3, i64 36}
!37 = !{!12, !3, i64 40}
