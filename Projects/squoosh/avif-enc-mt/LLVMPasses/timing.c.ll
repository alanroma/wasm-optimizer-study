; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/timing.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/timing.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.aom_dec_model_info = type { i32, i32, i32, i32 }
%struct.aom_dec_model_op_parameters = type { i32, i64, i64, i32, i32, i32, i32, i32 }

@high_kbps = internal global [32 x i32] [i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 30000, i32 50000, i32 2097152, i32 2097152, i32 100000, i32 160000, i32 240000, i32 240000, i32 240000, i32 480000, i32 800000, i32 800000, i32 2097152, i32 2097152, i32 2097152, i32 2097152, i32 2097152, i32 2097152, i32 2097152, i32 2097152, i32 2097152, i32 2097152, i32 2097152, i32 2097152], align 16
@bitrate_profile_factor = internal global [8 x i32] [i32 1, i32 2, i32 3, i32 0, i32 0, i32 0, i32 0, i32 0], align 16
@main_kbps = internal global [32 x i32] [i32 1500, i32 3000, i32 2097152, i32 2097152, i32 6000, i32 10000, i32 2097152, i32 2097152, i32 12000, i32 20000, i32 2097152, i32 2097152, i32 30000, i32 40000, i32 60000, i32 60000, i32 60000, i32 100000, i32 160000, i32 160000, i32 2097152, i32 2097152, i32 2097152, i32 2097152, i32 2097152, i32 2097152, i32 2097152, i32 2097152, i32 2097152, i32 2097152, i32 2097152, i32 2097152], align 16

; Function Attrs: nounwind
define hidden i64 @av1_max_level_bitrate(i8 signext %seq_profile, i32 %seq_level_idx, i32 %seq_tier) #0 {
entry:
  %seq_profile.addr = alloca i8, align 1
  %seq_level_idx.addr = alloca i32, align 4
  %seq_tier.addr = alloca i32, align 4
  %bitrate = alloca i64, align 8
  store i8 %seq_profile, i8* %seq_profile.addr, align 1, !tbaa !2
  store i32 %seq_level_idx, i32* %seq_level_idx.addr, align 4, !tbaa !5
  store i32 %seq_tier, i32* %seq_tier.addr, align 4, !tbaa !5
  %0 = bitcast i64* %bitrate to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #2
  %1 = load i32, i32* %seq_tier.addr, align 4, !tbaa !5
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %seq_level_idx.addr, align 4, !tbaa !5
  %arrayidx = getelementptr inbounds [32 x i32], [32 x i32]* @high_kbps, i32 0, i32 %2
  %3 = load i32, i32* %arrayidx, align 4, !tbaa !5
  %4 = load i8, i8* %seq_profile.addr, align 1, !tbaa !2
  %idxprom = sext i8 %4 to i32
  %arrayidx1 = getelementptr inbounds [8 x i32], [8 x i32]* @bitrate_profile_factor, i32 0, i32 %idxprom
  %5 = load i32, i32* %arrayidx1, align 4, !tbaa !5
  %mul = mul nsw i32 %3, %5
  %conv = sext i32 %mul to i64
  store i64 %conv, i64* %bitrate, align 8, !tbaa !7
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = load i32, i32* %seq_level_idx.addr, align 4, !tbaa !5
  %arrayidx2 = getelementptr inbounds [32 x i32], [32 x i32]* @main_kbps, i32 0, i32 %6
  %7 = load i32, i32* %arrayidx2, align 4, !tbaa !5
  %8 = load i8, i8* %seq_profile.addr, align 1, !tbaa !2
  %idxprom3 = sext i8 %8 to i32
  %arrayidx4 = getelementptr inbounds [8 x i32], [8 x i32]* @bitrate_profile_factor, i32 0, i32 %idxprom3
  %9 = load i32, i32* %arrayidx4, align 4, !tbaa !5
  %mul5 = mul nsw i32 %7, %9
  %conv6 = sext i32 %mul5 to i64
  store i64 %conv6, i64* %bitrate, align 8, !tbaa !7
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %10 = load i64, i64* %bitrate, align 8, !tbaa !7
  %mul7 = mul nsw i64 %10, 1000
  %11 = bitcast i64* %bitrate to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %11) #2
  ret i64 %mul7
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @av1_set_aom_dec_model_info(%struct.aom_dec_model_info* %decoder_model) #0 {
entry:
  %decoder_model.addr = alloca %struct.aom_dec_model_info*, align 4
  store %struct.aom_dec_model_info* %decoder_model, %struct.aom_dec_model_info** %decoder_model.addr, align 4, !tbaa !9
  %0 = load %struct.aom_dec_model_info*, %struct.aom_dec_model_info** %decoder_model.addr, align 4, !tbaa !9
  %encoder_decoder_buffer_delay_length = getelementptr inbounds %struct.aom_dec_model_info, %struct.aom_dec_model_info* %0, i32 0, i32 1
  store i32 16, i32* %encoder_decoder_buffer_delay_length, align 4, !tbaa !11
  %1 = load %struct.aom_dec_model_info*, %struct.aom_dec_model_info** %decoder_model.addr, align 4, !tbaa !9
  %buffer_removal_time_length = getelementptr inbounds %struct.aom_dec_model_info, %struct.aom_dec_model_info* %1, i32 0, i32 2
  store i32 10, i32* %buffer_removal_time_length, align 4, !tbaa !13
  %2 = load %struct.aom_dec_model_info*, %struct.aom_dec_model_info** %decoder_model.addr, align 4, !tbaa !9
  %frame_presentation_time_length = getelementptr inbounds %struct.aom_dec_model_info, %struct.aom_dec_model_info* %2, i32 0, i32 3
  store i32 10, i32* %frame_presentation_time_length, align 4, !tbaa !14
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_set_dec_model_op_parameters(%struct.aom_dec_model_op_parameters* %op_params) #0 {
entry:
  %op_params.addr = alloca %struct.aom_dec_model_op_parameters*, align 4
  store %struct.aom_dec_model_op_parameters* %op_params, %struct.aom_dec_model_op_parameters** %op_params.addr, align 4, !tbaa !9
  %0 = load %struct.aom_dec_model_op_parameters*, %struct.aom_dec_model_op_parameters** %op_params.addr, align 4, !tbaa !9
  %decoder_model_param_present_flag = getelementptr inbounds %struct.aom_dec_model_op_parameters, %struct.aom_dec_model_op_parameters* %0, i32 0, i32 0
  store i32 1, i32* %decoder_model_param_present_flag, align 8, !tbaa !15
  %1 = load %struct.aom_dec_model_op_parameters*, %struct.aom_dec_model_op_parameters** %op_params.addr, align 4, !tbaa !9
  %decoder_buffer_delay = getelementptr inbounds %struct.aom_dec_model_op_parameters, %struct.aom_dec_model_op_parameters* %1, i32 0, i32 3
  store i32 45000, i32* %decoder_buffer_delay, align 8, !tbaa !17
  %2 = load %struct.aom_dec_model_op_parameters*, %struct.aom_dec_model_op_parameters** %op_params.addr, align 4, !tbaa !9
  %encoder_buffer_delay = getelementptr inbounds %struct.aom_dec_model_op_parameters, %struct.aom_dec_model_op_parameters* %2, i32 0, i32 4
  store i32 45000, i32* %encoder_buffer_delay, align 4, !tbaa !18
  %3 = load %struct.aom_dec_model_op_parameters*, %struct.aom_dec_model_op_parameters** %op_params.addr, align 4, !tbaa !9
  %low_delay_mode_flag = getelementptr inbounds %struct.aom_dec_model_op_parameters, %struct.aom_dec_model_op_parameters* %3, i32 0, i32 5
  store i32 0, i32* %low_delay_mode_flag, align 8, !tbaa !19
  %4 = load %struct.aom_dec_model_op_parameters*, %struct.aom_dec_model_op_parameters** %op_params.addr, align 4, !tbaa !9
  %display_model_param_present_flag = getelementptr inbounds %struct.aom_dec_model_op_parameters, %struct.aom_dec_model_op_parameters* %4, i32 0, i32 6
  store i32 1, i32* %display_model_param_present_flag, align 4, !tbaa !20
  %5 = load %struct.aom_dec_model_op_parameters*, %struct.aom_dec_model_op_parameters** %op_params.addr, align 4, !tbaa !9
  %initial_display_delay = getelementptr inbounds %struct.aom_dec_model_op_parameters, %struct.aom_dec_model_op_parameters* %5, i32 0, i32 7
  store i32 8, i32* %initial_display_delay, align 8, !tbaa !21
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_set_resource_availability_parameters(%struct.aom_dec_model_op_parameters* %op_params) #0 {
entry:
  %op_params.addr = alloca %struct.aom_dec_model_op_parameters*, align 4
  store %struct.aom_dec_model_op_parameters* %op_params, %struct.aom_dec_model_op_parameters** %op_params.addr, align 4, !tbaa !9
  %0 = load %struct.aom_dec_model_op_parameters*, %struct.aom_dec_model_op_parameters** %op_params.addr, align 4, !tbaa !9
  %decoder_model_param_present_flag = getelementptr inbounds %struct.aom_dec_model_op_parameters, %struct.aom_dec_model_op_parameters* %0, i32 0, i32 0
  store i32 0, i32* %decoder_model_param_present_flag, align 8, !tbaa !15
  %1 = load %struct.aom_dec_model_op_parameters*, %struct.aom_dec_model_op_parameters** %op_params.addr, align 4, !tbaa !9
  %decoder_buffer_delay = getelementptr inbounds %struct.aom_dec_model_op_parameters, %struct.aom_dec_model_op_parameters* %1, i32 0, i32 3
  store i32 70000, i32* %decoder_buffer_delay, align 8, !tbaa !17
  %2 = load %struct.aom_dec_model_op_parameters*, %struct.aom_dec_model_op_parameters** %op_params.addr, align 4, !tbaa !9
  %encoder_buffer_delay = getelementptr inbounds %struct.aom_dec_model_op_parameters, %struct.aom_dec_model_op_parameters* %2, i32 0, i32 4
  store i32 20000, i32* %encoder_buffer_delay, align 4, !tbaa !18
  %3 = load %struct.aom_dec_model_op_parameters*, %struct.aom_dec_model_op_parameters** %op_params.addr, align 4, !tbaa !9
  %low_delay_mode_flag = getelementptr inbounds %struct.aom_dec_model_op_parameters, %struct.aom_dec_model_op_parameters* %3, i32 0, i32 5
  store i32 0, i32* %low_delay_mode_flag, align 8, !tbaa !19
  %4 = load %struct.aom_dec_model_op_parameters*, %struct.aom_dec_model_op_parameters** %op_params.addr, align 4, !tbaa !9
  %display_model_param_present_flag = getelementptr inbounds %struct.aom_dec_model_op_parameters, %struct.aom_dec_model_op_parameters* %4, i32 0, i32 6
  store i32 1, i32* %display_model_param_present_flag, align 4, !tbaa !20
  %5 = load %struct.aom_dec_model_op_parameters*, %struct.aom_dec_model_op_parameters** %op_params.addr, align 4, !tbaa !9
  %initial_display_delay = getelementptr inbounds %struct.aom_dec_model_op_parameters, %struct.aom_dec_model_op_parameters* %5, i32 0, i32 7
  store i32 8, i32* %initial_display_delay, align 8, !tbaa !21
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = !{!6, !6, i64 0}
!6 = !{!"int", !3, i64 0}
!7 = !{!8, !8, i64 0}
!8 = !{!"long long", !3, i64 0}
!9 = !{!10, !10, i64 0}
!10 = !{!"any pointer", !3, i64 0}
!11 = !{!12, !6, i64 4}
!12 = !{!"aom_dec_model_info", !6, i64 0, !6, i64 4, !6, i64 8, !6, i64 12}
!13 = !{!12, !6, i64 8}
!14 = !{!12, !6, i64 12}
!15 = !{!16, !6, i64 0}
!16 = !{!"aom_dec_model_op_parameters", !6, i64 0, !8, i64 8, !8, i64 16, !6, i64 24, !6, i64 28, !6, i64 32, !6, i64 36, !6, i64 40}
!17 = !{!16, !6, i64 24}
!18 = !{!16, !6, i64 28}
!19 = !{!16, !6, i64 32}
!20 = !{!16, !6, i64 36}
!21 = !{!16, !6, i64 40}
