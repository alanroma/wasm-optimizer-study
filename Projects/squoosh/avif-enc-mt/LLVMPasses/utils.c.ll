; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-enc-mt/src/utils.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-enc-mt/src/utils.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.avifArrayInternal = type { i8*, i32, i32, i32 }

; Function Attrs: nounwind
define hidden float @avifRoundf(float %v) #0 {
entry:
  %v.addr = alloca float, align 4
  store float %v, float* %v.addr, align 4, !tbaa !2
  %0 = load float, float* %v.addr, align 4, !tbaa !2
  %add = fadd float %0, 5.000000e-01
  %1 = call float @llvm.floor.f32(float %add)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.floor.f32(float) #1

; Function Attrs: nounwind
define hidden zeroext i16 @avifHTONS(i16 zeroext %s) #0 {
entry:
  %s.addr = alloca i16, align 2
  %data = alloca [2 x i8], align 1
  %result = alloca i16, align 2
  store i16 %s, i16* %s.addr, align 2, !tbaa !6
  %0 = bitcast [2 x i8]* %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %0) #5
  %1 = load i16, i16* %s.addr, align 2, !tbaa !6
  %conv = zext i16 %1 to i32
  %shr = ashr i32 %conv, 8
  %and = and i32 %shr, 255
  %conv1 = trunc i32 %and to i8
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %data, i32 0, i32 0
  store i8 %conv1, i8* %arrayidx, align 1, !tbaa !8
  %2 = load i16, i16* %s.addr, align 2, !tbaa !6
  %conv2 = zext i16 %2 to i32
  %shr3 = ashr i32 %conv2, 0
  %and4 = and i32 %shr3, 255
  %conv5 = trunc i32 %and4 to i8
  %arrayidx6 = getelementptr inbounds [2 x i8], [2 x i8]* %data, i32 0, i32 1
  store i8 %conv5, i8* %arrayidx6, align 1, !tbaa !8
  %3 = bitcast i16* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %3) #5
  %4 = bitcast i16* %result to i8*
  %arraydecay = getelementptr inbounds [2 x i8], [2 x i8]* %data, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %4, i8* align 1 %arraydecay, i32 2, i1 false)
  %5 = load i16, i16* %result, align 2, !tbaa !6
  %6 = bitcast i16* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %6) #5
  %7 = bitcast [2 x i8]* %data to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %7) #5
  ret i16 %5
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define hidden zeroext i16 @avifNTOHS(i16 zeroext %s) #0 {
entry:
  %s.addr = alloca i16, align 2
  %data = alloca [2 x i8], align 1
  store i16 %s, i16* %s.addr, align 2, !tbaa !6
  %0 = bitcast [2 x i8]* %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %0) #5
  %1 = bitcast [2 x i8]* %data to i8*
  %2 = bitcast i16* %s.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %1, i8* align 2 %2, i32 2, i1 false)
  %arrayidx = getelementptr inbounds [2 x i8], [2 x i8]* %data, i32 0, i32 1
  %3 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %3 to i32
  %shl = shl i32 %conv, 0
  %arrayidx1 = getelementptr inbounds [2 x i8], [2 x i8]* %data, i32 0, i32 0
  %4 = load i8, i8* %arrayidx1, align 1, !tbaa !8
  %conv2 = zext i8 %4 to i32
  %shl3 = shl i32 %conv2, 8
  %or = or i32 %shl, %shl3
  %conv4 = trunc i32 %or to i16
  %5 = bitcast [2 x i8]* %data to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %5) #5
  ret i16 %conv4
}

; Function Attrs: nounwind
define hidden i32 @avifHTONL(i32 %l) #0 {
entry:
  %l.addr = alloca i32, align 4
  %data = alloca [4 x i8], align 1
  %result = alloca i32, align 4
  store i32 %l, i32* %l.addr, align 4, !tbaa !9
  %0 = bitcast [4 x i8]* %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %l.addr, align 4, !tbaa !9
  %shr = lshr i32 %1, 24
  %and = and i32 %shr, 255
  %conv = trunc i32 %and to i8
  %arrayidx = getelementptr inbounds [4 x i8], [4 x i8]* %data, i32 0, i32 0
  store i8 %conv, i8* %arrayidx, align 1, !tbaa !8
  %2 = load i32, i32* %l.addr, align 4, !tbaa !9
  %shr1 = lshr i32 %2, 16
  %and2 = and i32 %shr1, 255
  %conv3 = trunc i32 %and2 to i8
  %arrayidx4 = getelementptr inbounds [4 x i8], [4 x i8]* %data, i32 0, i32 1
  store i8 %conv3, i8* %arrayidx4, align 1, !tbaa !8
  %3 = load i32, i32* %l.addr, align 4, !tbaa !9
  %shr5 = lshr i32 %3, 8
  %and6 = and i32 %shr5, 255
  %conv7 = trunc i32 %and6 to i8
  %arrayidx8 = getelementptr inbounds [4 x i8], [4 x i8]* %data, i32 0, i32 2
  store i8 %conv7, i8* %arrayidx8, align 1, !tbaa !8
  %4 = load i32, i32* %l.addr, align 4, !tbaa !9
  %shr9 = lshr i32 %4, 0
  %and10 = and i32 %shr9, 255
  %conv11 = trunc i32 %and10 to i8
  %arrayidx12 = getelementptr inbounds [4 x i8], [4 x i8]* %data, i32 0, i32 3
  store i8 %conv11, i8* %arrayidx12, align 1, !tbaa !8
  %5 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = bitcast i32* %result to i8*
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %data, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 1 %arraydecay, i32 4, i1 false)
  %7 = load i32, i32* %result, align 4, !tbaa !9
  %8 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #5
  %9 = bitcast [4 x i8]* %data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #5
  ret i32 %7
}

; Function Attrs: nounwind
define hidden i32 @avifNTOHL(i32 %l) #0 {
entry:
  %l.addr = alloca i32, align 4
  %data = alloca [4 x i8], align 1
  store i32 %l, i32* %l.addr, align 4, !tbaa !9
  %0 = bitcast [4 x i8]* %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast [4 x i8]* %data to i8*
  %2 = bitcast i32* %l.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %1, i8* align 4 %2, i32 4, i1 false)
  %arrayidx = getelementptr inbounds [4 x i8], [4 x i8]* %data, i32 0, i32 3
  %3 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %3 to i32
  %shl = shl i32 %conv, 0
  %arrayidx1 = getelementptr inbounds [4 x i8], [4 x i8]* %data, i32 0, i32 2
  %4 = load i8, i8* %arrayidx1, align 1, !tbaa !8
  %conv2 = zext i8 %4 to i32
  %shl3 = shl i32 %conv2, 8
  %or = or i32 %shl, %shl3
  %arrayidx4 = getelementptr inbounds [4 x i8], [4 x i8]* %data, i32 0, i32 1
  %5 = load i8, i8* %arrayidx4, align 1, !tbaa !8
  %conv5 = zext i8 %5 to i32
  %shl6 = shl i32 %conv5, 16
  %or7 = or i32 %or, %shl6
  %arrayidx8 = getelementptr inbounds [4 x i8], [4 x i8]* %data, i32 0, i32 0
  %6 = load i8, i8* %arrayidx8, align 1, !tbaa !8
  %conv9 = zext i8 %6 to i32
  %shl10 = shl i32 %conv9, 24
  %or11 = or i32 %or7, %shl10
  %7 = bitcast [4 x i8]* %data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #5
  ret i32 %or11
}

; Function Attrs: nounwind
define hidden i64 @avifHTON64(i64 %l) #0 {
entry:
  %l.addr = alloca i64, align 8
  %data = alloca [8 x i8], align 1
  %result = alloca i64, align 8
  store i64 %l, i64* %l.addr, align 8, !tbaa !11
  %0 = bitcast [8 x i8]* %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #5
  %1 = load i64, i64* %l.addr, align 8, !tbaa !11
  %shr = lshr i64 %1, 56
  %and = and i64 %shr, 255
  %conv = trunc i64 %and to i8
  %arrayidx = getelementptr inbounds [8 x i8], [8 x i8]* %data, i32 0, i32 0
  store i8 %conv, i8* %arrayidx, align 1, !tbaa !8
  %2 = load i64, i64* %l.addr, align 8, !tbaa !11
  %shr1 = lshr i64 %2, 48
  %and2 = and i64 %shr1, 255
  %conv3 = trunc i64 %and2 to i8
  %arrayidx4 = getelementptr inbounds [8 x i8], [8 x i8]* %data, i32 0, i32 1
  store i8 %conv3, i8* %arrayidx4, align 1, !tbaa !8
  %3 = load i64, i64* %l.addr, align 8, !tbaa !11
  %shr5 = lshr i64 %3, 40
  %and6 = and i64 %shr5, 255
  %conv7 = trunc i64 %and6 to i8
  %arrayidx8 = getelementptr inbounds [8 x i8], [8 x i8]* %data, i32 0, i32 2
  store i8 %conv7, i8* %arrayidx8, align 1, !tbaa !8
  %4 = load i64, i64* %l.addr, align 8, !tbaa !11
  %shr9 = lshr i64 %4, 32
  %and10 = and i64 %shr9, 255
  %conv11 = trunc i64 %and10 to i8
  %arrayidx12 = getelementptr inbounds [8 x i8], [8 x i8]* %data, i32 0, i32 3
  store i8 %conv11, i8* %arrayidx12, align 1, !tbaa !8
  %5 = load i64, i64* %l.addr, align 8, !tbaa !11
  %shr13 = lshr i64 %5, 24
  %and14 = and i64 %shr13, 255
  %conv15 = trunc i64 %and14 to i8
  %arrayidx16 = getelementptr inbounds [8 x i8], [8 x i8]* %data, i32 0, i32 4
  store i8 %conv15, i8* %arrayidx16, align 1, !tbaa !8
  %6 = load i64, i64* %l.addr, align 8, !tbaa !11
  %shr17 = lshr i64 %6, 16
  %and18 = and i64 %shr17, 255
  %conv19 = trunc i64 %and18 to i8
  %arrayidx20 = getelementptr inbounds [8 x i8], [8 x i8]* %data, i32 0, i32 5
  store i8 %conv19, i8* %arrayidx20, align 1, !tbaa !8
  %7 = load i64, i64* %l.addr, align 8, !tbaa !11
  %shr21 = lshr i64 %7, 8
  %and22 = and i64 %shr21, 255
  %conv23 = trunc i64 %and22 to i8
  %arrayidx24 = getelementptr inbounds [8 x i8], [8 x i8]* %data, i32 0, i32 6
  store i8 %conv23, i8* %arrayidx24, align 1, !tbaa !8
  %8 = load i64, i64* %l.addr, align 8, !tbaa !11
  %shr25 = lshr i64 %8, 0
  %and26 = and i64 %shr25, 255
  %conv27 = trunc i64 %and26 to i8
  %arrayidx28 = getelementptr inbounds [8 x i8], [8 x i8]* %data, i32 0, i32 7
  store i8 %conv27, i8* %arrayidx28, align 1, !tbaa !8
  %9 = bitcast i64* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %9) #5
  %10 = bitcast i64* %result to i8*
  %arraydecay = getelementptr inbounds [8 x i8], [8 x i8]* %data, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %10, i8* align 1 %arraydecay, i32 8, i1 false)
  %11 = load i64, i64* %result, align 8, !tbaa !11
  %12 = bitcast i64* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %12) #5
  %13 = bitcast [8 x i8]* %data to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %13) #5
  ret i64 %11
}

; Function Attrs: nounwind
define hidden i64 @avifNTOH64(i64 %l) #0 {
entry:
  %l.addr = alloca i64, align 8
  %data = alloca [8 x i8], align 1
  store i64 %l, i64* %l.addr, align 8, !tbaa !11
  %0 = bitcast [8 x i8]* %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #5
  %1 = bitcast [8 x i8]* %data to i8*
  %2 = bitcast i64* %l.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %1, i8* align 8 %2, i32 8, i1 false)
  %arrayidx = getelementptr inbounds [8 x i8], [8 x i8]* %data, i32 0, i32 7
  %3 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %3 to i64
  %shl = shl i64 %conv, 0
  %arrayidx1 = getelementptr inbounds [8 x i8], [8 x i8]* %data, i32 0, i32 6
  %4 = load i8, i8* %arrayidx1, align 1, !tbaa !8
  %conv2 = zext i8 %4 to i64
  %shl3 = shl i64 %conv2, 8
  %or = or i64 %shl, %shl3
  %arrayidx4 = getelementptr inbounds [8 x i8], [8 x i8]* %data, i32 0, i32 5
  %5 = load i8, i8* %arrayidx4, align 1, !tbaa !8
  %conv5 = zext i8 %5 to i64
  %shl6 = shl i64 %conv5, 16
  %or7 = or i64 %or, %shl6
  %arrayidx8 = getelementptr inbounds [8 x i8], [8 x i8]* %data, i32 0, i32 4
  %6 = load i8, i8* %arrayidx8, align 1, !tbaa !8
  %conv9 = zext i8 %6 to i64
  %shl10 = shl i64 %conv9, 24
  %or11 = or i64 %or7, %shl10
  %arrayidx12 = getelementptr inbounds [8 x i8], [8 x i8]* %data, i32 0, i32 3
  %7 = load i8, i8* %arrayidx12, align 1, !tbaa !8
  %conv13 = zext i8 %7 to i64
  %shl14 = shl i64 %conv13, 32
  %or15 = or i64 %or11, %shl14
  %arrayidx16 = getelementptr inbounds [8 x i8], [8 x i8]* %data, i32 0, i32 2
  %8 = load i8, i8* %arrayidx16, align 1, !tbaa !8
  %conv17 = zext i8 %8 to i64
  %shl18 = shl i64 %conv17, 40
  %or19 = or i64 %or15, %shl18
  %arrayidx20 = getelementptr inbounds [8 x i8], [8 x i8]* %data, i32 0, i32 1
  %9 = load i8, i8* %arrayidx20, align 1, !tbaa !8
  %conv21 = zext i8 %9 to i64
  %shl22 = shl i64 %conv21, 48
  %or23 = or i64 %or19, %shl22
  %arrayidx24 = getelementptr inbounds [8 x i8], [8 x i8]* %data, i32 0, i32 0
  %10 = load i8, i8* %arrayidx24, align 1, !tbaa !8
  %conv25 = zext i8 %10 to i64
  %shl26 = shl i64 %conv25, 56
  %or27 = or i64 %or23, %shl26
  %11 = bitcast [8 x i8]* %data to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %11) #5
  ret i64 %or27
}

; Function Attrs: nounwind
define hidden void @avifArrayCreate(i8* %arrayStruct, i32 %elementSize, i32 %initialCapacity) #0 {
entry:
  %arrayStruct.addr = alloca i8*, align 4
  %elementSize.addr = alloca i32, align 4
  %initialCapacity.addr = alloca i32, align 4
  %arr = alloca %struct.avifArrayInternal*, align 4
  store i8* %arrayStruct, i8** %arrayStruct.addr, align 4, !tbaa !13
  store i32 %elementSize, i32* %elementSize.addr, align 4, !tbaa !9
  store i32 %initialCapacity, i32* %initialCapacity.addr, align 4, !tbaa !9
  %0 = bitcast %struct.avifArrayInternal** %arr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %arrayStruct.addr, align 4, !tbaa !13
  %2 = bitcast i8* %1 to %struct.avifArrayInternal*
  store %struct.avifArrayInternal* %2, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %3 = load i32, i32* %elementSize.addr, align 4, !tbaa !9
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i32, i32* %elementSize.addr, align 4, !tbaa !9
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %4, %cond.true ], [ 1, %cond.false ]
  %5 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %elementSize1 = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %5, i32 0, i32 1
  store i32 %cond, i32* %elementSize1, align 4, !tbaa !15
  %6 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %count = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %6, i32 0, i32 2
  store i32 0, i32* %count, align 4, !tbaa !17
  %7 = load i32, i32* %initialCapacity.addr, align 4, !tbaa !9
  %8 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %capacity = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %8, i32 0, i32 3
  store i32 %7, i32* %capacity, align 4, !tbaa !18
  %9 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %elementSize2 = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %9, i32 0, i32 1
  %10 = load i32, i32* %elementSize2, align 4, !tbaa !15
  %11 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %capacity3 = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %11, i32 0, i32 3
  %12 = load i32, i32* %capacity3, align 4, !tbaa !18
  %mul = mul i32 %10, %12
  %call = call i8* @avifAlloc(i32 %mul)
  %13 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %ptr = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %13, i32 0, i32 0
  store i8* %call, i8** %ptr, align 4, !tbaa !19
  %14 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %ptr4 = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %14, i32 0, i32 0
  %15 = load i8*, i8** %ptr4, align 4, !tbaa !19
  %16 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %elementSize5 = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %16, i32 0, i32 1
  %17 = load i32, i32* %elementSize5, align 4, !tbaa !15
  %18 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %capacity6 = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %18, i32 0, i32 3
  %19 = load i32, i32* %capacity6, align 4, !tbaa !18
  %mul7 = mul i32 %17, %19
  call void @llvm.memset.p0i8.i32(i8* align 1 %15, i8 0, i32 %mul7, i1 false)
  %20 = bitcast %struct.avifArrayInternal** %arr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  ret void
}

declare i8* @avifAlloc(i32) #3

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: nounwind
define hidden i32 @avifArrayPushIndex(i8* %arrayStruct) #0 {
entry:
  %arrayStruct.addr = alloca i8*, align 4
  %arr = alloca %struct.avifArrayInternal*, align 4
  %oldPtr = alloca i8*, align 4
  %oldByteCount = alloca i32, align 4
  store i8* %arrayStruct, i8** %arrayStruct.addr, align 4, !tbaa !13
  %0 = bitcast %struct.avifArrayInternal** %arr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %arrayStruct.addr, align 4, !tbaa !13
  %2 = bitcast i8* %1 to %struct.avifArrayInternal*
  store %struct.avifArrayInternal* %2, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %3 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %count = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %3, i32 0, i32 2
  %4 = load i32, i32* %count, align 4, !tbaa !17
  %5 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %capacity = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %5, i32 0, i32 3
  %6 = load i32, i32* %capacity, align 4, !tbaa !18
  %cmp = icmp eq i32 %4, %6
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = bitcast i8** %oldPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %ptr = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %8, i32 0, i32 0
  %9 = load i8*, i8** %ptr, align 4, !tbaa !19
  store i8* %9, i8** %oldPtr, align 4, !tbaa !13
  %10 = bitcast i32* %oldByteCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %elementSize = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %11, i32 0, i32 1
  %12 = load i32, i32* %elementSize, align 4, !tbaa !15
  %13 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %capacity1 = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %13, i32 0, i32 3
  %14 = load i32, i32* %capacity1, align 4, !tbaa !18
  %mul = mul i32 %12, %14
  store i32 %mul, i32* %oldByteCount, align 4, !tbaa !9
  %15 = load i32, i32* %oldByteCount, align 4, !tbaa !9
  %mul2 = mul i32 %15, 2
  %call = call i8* @avifAlloc(i32 %mul2)
  %16 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %ptr3 = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %16, i32 0, i32 0
  store i8* %call, i8** %ptr3, align 4, !tbaa !19
  %17 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %ptr4 = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %17, i32 0, i32 0
  %18 = load i8*, i8** %ptr4, align 4, !tbaa !19
  %19 = load i32, i32* %oldByteCount, align 4, !tbaa !9
  %add.ptr = getelementptr inbounds i8, i8* %18, i32 %19
  %20 = load i32, i32* %oldByteCount, align 4, !tbaa !9
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr, i8 0, i32 %20, i1 false)
  %21 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %ptr5 = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %21, i32 0, i32 0
  %22 = load i8*, i8** %ptr5, align 4, !tbaa !19
  %23 = load i8*, i8** %oldPtr, align 4, !tbaa !13
  %24 = load i32, i32* %oldByteCount, align 4, !tbaa !9
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %22, i8* align 1 %23, i32 %24, i1 false)
  %25 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %capacity6 = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %25, i32 0, i32 3
  %26 = load i32, i32* %capacity6, align 4, !tbaa !18
  %mul7 = mul i32 %26, 2
  store i32 %mul7, i32* %capacity6, align 4, !tbaa !18
  %27 = load i8*, i8** %oldPtr, align 4, !tbaa !13
  call void @avifFree(i8* %27)
  %28 = bitcast i32* %oldByteCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  %29 = bitcast i8** %oldPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #5
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %30 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %count8 = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %30, i32 0, i32 2
  %31 = load i32, i32* %count8, align 4, !tbaa !17
  %inc = add i32 %31, 1
  store i32 %inc, i32* %count8, align 4, !tbaa !17
  %32 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %count9 = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %32, i32 0, i32 2
  %33 = load i32, i32* %count9, align 4, !tbaa !17
  %sub = sub i32 %33, 1
  %34 = bitcast %struct.avifArrayInternal** %arr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #5
  ret i32 %sub
}

declare void @avifFree(i8*) #3

; Function Attrs: nounwind
define hidden i8* @avifArrayPushPtr(i8* %arrayStruct) #0 {
entry:
  %arrayStruct.addr = alloca i8*, align 4
  %index = alloca i32, align 4
  %arr = alloca %struct.avifArrayInternal*, align 4
  store i8* %arrayStruct, i8** %arrayStruct.addr, align 4, !tbaa !13
  %0 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %arrayStruct.addr, align 4, !tbaa !13
  %call = call i32 @avifArrayPushIndex(i8* %1)
  store i32 %call, i32* %index, align 4, !tbaa !9
  %2 = bitcast %struct.avifArrayInternal** %arr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i8*, i8** %arrayStruct.addr, align 4, !tbaa !13
  %4 = bitcast i8* %3 to %struct.avifArrayInternal*
  store %struct.avifArrayInternal* %4, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %5 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %ptr = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %5, i32 0, i32 0
  %6 = load i8*, i8** %ptr, align 4, !tbaa !19
  %7 = load i32, i32* %index, align 4, !tbaa !9
  %8 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %elementSize = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %8, i32 0, i32 1
  %9 = load i32, i32* %elementSize, align 4, !tbaa !15
  %mul = mul i32 %7, %9
  %arrayidx = getelementptr inbounds i8, i8* %6, i32 %mul
  %10 = bitcast %struct.avifArrayInternal** %arr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #5
  %11 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #5
  ret i8* %arrayidx
}

; Function Attrs: nounwind
define hidden void @avifArrayPush(i8* %arrayStruct, i8* %element) #0 {
entry:
  %arrayStruct.addr = alloca i8*, align 4
  %element.addr = alloca i8*, align 4
  %arr = alloca %struct.avifArrayInternal*, align 4
  %newElement = alloca i8*, align 4
  store i8* %arrayStruct, i8** %arrayStruct.addr, align 4, !tbaa !13
  store i8* %element, i8** %element.addr, align 4, !tbaa !13
  %0 = bitcast %struct.avifArrayInternal** %arr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %arrayStruct.addr, align 4, !tbaa !13
  %2 = bitcast i8* %1 to %struct.avifArrayInternal*
  store %struct.avifArrayInternal* %2, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %3 = bitcast i8** %newElement to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %5 = bitcast %struct.avifArrayInternal* %4 to i8*
  %call = call i8* @avifArrayPushPtr(i8* %5)
  store i8* %call, i8** %newElement, align 4, !tbaa !13
  %6 = load i8*, i8** %newElement, align 4, !tbaa !13
  %7 = load i8*, i8** %element.addr, align 4, !tbaa !13
  %8 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %elementSize = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %8, i32 0, i32 1
  %9 = load i32, i32* %elementSize, align 4, !tbaa !15
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %6, i8* align 1 %7, i32 %9, i1 false)
  %10 = bitcast i8** %newElement to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #5
  %11 = bitcast %struct.avifArrayInternal** %arr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @avifArrayDestroy(i8* %arrayStruct) #0 {
entry:
  %arrayStruct.addr = alloca i8*, align 4
  %arr = alloca %struct.avifArrayInternal*, align 4
  store i8* %arrayStruct, i8** %arrayStruct.addr, align 4, !tbaa !13
  %0 = bitcast %struct.avifArrayInternal** %arr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %arrayStruct.addr, align 4, !tbaa !13
  %2 = bitcast i8* %1 to %struct.avifArrayInternal*
  store %struct.avifArrayInternal* %2, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %3 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %ptr = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %3, i32 0, i32 0
  %4 = load i8*, i8** %ptr, align 4, !tbaa !19
  %tobool = icmp ne i8* %4, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %ptr1 = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %5, i32 0, i32 0
  %6 = load i8*, i8** %ptr1, align 4, !tbaa !19
  call void @avifFree(i8* %6)
  %7 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %ptr2 = getelementptr inbounds %struct.avifArrayInternal, %struct.avifArrayInternal* %7, i32 0, i32 0
  store i8* null, i8** %ptr2, align 4, !tbaa !19
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = load %struct.avifArrayInternal*, %struct.avifArrayInternal** %arr, align 4, !tbaa !13
  %9 = bitcast %struct.avifArrayInternal* %8 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %9, i8 0, i32 16, i1 false)
  %10 = bitcast %struct.avifArrayInternal** %arr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #5
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone speculatable willreturn }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"float", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"short", !4, i64 0}
!8 = !{!4, !4, i64 0}
!9 = !{!10, !10, i64 0}
!10 = !{!"int", !4, i64 0}
!11 = !{!12, !12, i64 0}
!12 = !{!"long long", !4, i64 0}
!13 = !{!14, !14, i64 0}
!14 = !{!"any pointer", !4, i64 0}
!15 = !{!16, !10, i64 4}
!16 = !{!"avifArrayInternal", !14, i64 0, !10, i64 4, !10, i64 8, !10, i64 12}
!17 = !{!16, !10, i64 8}
!18 = !{!16, !10, i64 12}
!19 = !{!16, !14, i64 0}
