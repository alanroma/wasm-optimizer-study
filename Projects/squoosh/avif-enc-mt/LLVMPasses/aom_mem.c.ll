; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_mem/aom_mem.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_mem/aom_mem.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: nounwind
define hidden i8* @aom_memalign(i32 %align, i32 %size) #0 {
entry:
  %retval = alloca i8*, align 4
  %align.addr = alloca i32, align 4
  %size.addr = alloca i32, align 4
  %x = alloca i8*, align 4
  %aligned_size = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %addr = alloca i8*, align 4
  store i32 %align, i32* %align.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !2
  %0 = bitcast i8** %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i8* null, i8** %x, align 4, !tbaa !6
  %1 = bitcast i32* %aligned_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i32, i32* %size.addr, align 4, !tbaa !2
  %3 = load i32, i32* %align.addr, align 4, !tbaa !2
  %call = call i32 @GetAlignedMallocSize(i32 %2, i32 %3)
  store i32 %call, i32* %aligned_size, align 4, !tbaa !2
  %4 = load i32, i32* %aligned_size, align 4, !tbaa !2
  %conv = zext i32 %4 to i64
  %call1 = call i32 @check_size_argument_overflow(i64 1, i64 %conv)
  %tobool = icmp ne i32 %call1, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %5 = bitcast i8** %addr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i32, i32* %aligned_size, align 4, !tbaa !2
  %call2 = call i8* @malloc(i32 %6)
  store i8* %call2, i8** %addr, align 4, !tbaa !6
  %7 = load i8*, i8** %addr, align 4, !tbaa !6
  %tobool3 = icmp ne i8* %7, null
  br i1 %tobool3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.end
  %8 = load i8*, i8** %addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %8, i32 4
  %9 = ptrtoint i8* %add.ptr to i32
  %10 = load i32, i32* %align.addr, align 4, !tbaa !2
  %sub = sub i32 %10, 1
  %add = add i32 %9, %sub
  %11 = load i32, i32* %align.addr, align 4, !tbaa !2
  %sub5 = sub i32 %11, 1
  %neg = xor i32 %sub5, -1
  %and = and i32 %add, %neg
  %12 = inttoptr i32 %and to i8*
  store i8* %12, i8** %x, align 4, !tbaa !6
  %13 = load i8*, i8** %x, align 4, !tbaa !6
  %14 = load i8*, i8** %addr, align 4, !tbaa !6
  call void @SetActualMallocAddress(i8* %13, i8* %14)
  br label %if.end6

if.end6:                                          ; preds = %if.then4, %if.end
  %15 = load i8*, i8** %x, align 4, !tbaa !6
  store i8* %15, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %16 = bitcast i8** %addr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #4
  br label %cleanup

cleanup:                                          ; preds = %if.end6, %if.then
  %17 = bitcast i32* %aligned_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  %18 = bitcast i8** %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #4
  %19 = load i8*, i8** %retval, align 4
  ret i8* %19
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal i32 @GetAlignedMallocSize(i32 %size, i32 %align) #0 {
entry:
  %size.addr = alloca i32, align 4
  %align.addr = alloca i32, align 4
  store i32 %size, i32* %size.addr, align 4, !tbaa !2
  store i32 %align, i32* %align.addr, align 4, !tbaa !2
  %0 = load i32, i32* %size.addr, align 4, !tbaa !2
  %1 = load i32, i32* %align.addr, align 4, !tbaa !2
  %add = add i32 %0, %1
  %sub = sub i32 %add, 1
  %add1 = add i32 %sub, 4
  ret i32 %add1
}

; Function Attrs: nounwind
define internal i32 @check_size_argument_overflow(i64 %nmemb, i64 %size) #0 {
entry:
  %retval = alloca i32, align 4
  %nmemb.addr = alloca i64, align 8
  %size.addr = alloca i64, align 8
  %total_size = alloca i64, align 8
  %cleanup.dest.slot = alloca i32, align 4
  store i64 %nmemb, i64* %nmemb.addr, align 8, !tbaa !8
  store i64 %size, i64* %size.addr, align 8, !tbaa !8
  %0 = bitcast i64* %total_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = load i64, i64* %nmemb.addr, align 8, !tbaa !8
  %2 = load i64, i64* %size.addr, align 8, !tbaa !8
  %mul = mul i64 %1, %2
  store i64 %mul, i64* %total_size, align 8, !tbaa !8
  %3 = load i64, i64* %nmemb.addr, align 8, !tbaa !8
  %cmp = icmp eq i64 %3, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %4 = load i64, i64* %size.addr, align 8, !tbaa !8
  %5 = load i64, i64* %nmemb.addr, align 8, !tbaa !8
  %div = udiv i64 2147418112, %5
  %cmp1 = icmp ugt i64 %4, %div
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %if.end
  %6 = load i64, i64* %total_size, align 8, !tbaa !8
  %7 = load i64, i64* %total_size, align 8, !tbaa !8
  %conv = trunc i64 %7 to i32
  %conv4 = zext i32 %conv to i64
  %cmp5 = icmp ne i64 %6, %conv4
  br i1 %cmp5, label %if.then7, label %if.end8

if.then7:                                         ; preds = %if.end3
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end3
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end8, %if.then7, %if.then2, %if.then
  %8 = bitcast i64* %total_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %8) #4
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

declare i8* @malloc(i32) #2

; Function Attrs: nounwind
define internal void @SetActualMallocAddress(i8* %mem, i8* %malloc_addr) #0 {
entry:
  %mem.addr = alloca i8*, align 4
  %malloc_addr.addr = alloca i8*, align 4
  %malloc_addr_location = alloca i32*, align 4
  store i8* %mem, i8** %mem.addr, align 4, !tbaa !6
  store i8* %malloc_addr, i8** %malloc_addr.addr, align 4, !tbaa !6
  %0 = bitcast i32** %malloc_addr_location to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %mem.addr, align 4, !tbaa !6
  %call = call i32* @GetMallocAddressLocation(i8* %1)
  store i32* %call, i32** %malloc_addr_location, align 4, !tbaa !6
  %2 = load i8*, i8** %malloc_addr.addr, align 4, !tbaa !6
  %3 = ptrtoint i8* %2 to i32
  %4 = load i32*, i32** %malloc_addr_location, align 4, !tbaa !6
  store i32 %3, i32* %4, align 4, !tbaa !2
  %5 = bitcast i32** %malloc_addr_location to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden i8* @aom_malloc(i32 %size) #0 {
entry:
  %size.addr = alloca i32, align 4
  store i32 %size, i32* %size.addr, align 4, !tbaa !2
  %0 = load i32, i32* %size.addr, align 4, !tbaa !2
  %call = call i8* @aom_memalign(i32 8, i32 %0)
  ret i8* %call
}

; Function Attrs: nounwind
define hidden i8* @aom_calloc(i32 %num, i32 %size) #0 {
entry:
  %num.addr = alloca i32, align 4
  %size.addr = alloca i32, align 4
  %total_size = alloca i32, align 4
  %x = alloca i8*, align 4
  store i32 %num, i32* %num.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !2
  %0 = bitcast i32* %total_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %num.addr, align 4, !tbaa !2
  %2 = load i32, i32* %size.addr, align 4, !tbaa !2
  %mul = mul i32 %1, %2
  store i32 %mul, i32* %total_size, align 4, !tbaa !2
  %3 = bitcast i8** %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load i32, i32* %total_size, align 4, !tbaa !2
  %call = call i8* @aom_malloc(i32 %4)
  store i8* %call, i8** %x, align 4, !tbaa !6
  %5 = load i8*, i8** %x, align 4, !tbaa !6
  %tobool = icmp ne i8* %5, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load i8*, i8** %x, align 4, !tbaa !6
  %7 = load i32, i32* %total_size, align 4, !tbaa !2
  call void @llvm.memset.p0i8.i32(i8* align 1 %6, i8 0, i32 %7, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = load i8*, i8** %x, align 4, !tbaa !6
  %9 = bitcast i8** %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i32* %total_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #4
  ret i8* %8
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: nounwind
define hidden void @aom_free(i8* %memblk) #0 {
entry:
  %memblk.addr = alloca i8*, align 4
  %addr = alloca i8*, align 4
  store i8* %memblk, i8** %memblk.addr, align 4, !tbaa !6
  %0 = load i8*, i8** %memblk.addr, align 4, !tbaa !6
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i8** %addr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i8*, i8** %memblk.addr, align 4, !tbaa !6
  %call = call i8* @GetActualMallocAddress(i8* %2)
  store i8* %call, i8** %addr, align 4, !tbaa !6
  %3 = load i8*, i8** %addr, align 4, !tbaa !6
  call void @free(i8* %3)
  %4 = bitcast i8** %addr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: nounwind
define internal i8* @GetActualMallocAddress(i8* %mem) #0 {
entry:
  %mem.addr = alloca i8*, align 4
  %malloc_addr_location = alloca i32*, align 4
  store i8* %mem, i8** %mem.addr, align 4, !tbaa !6
  %0 = bitcast i32** %malloc_addr_location to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %mem.addr, align 4, !tbaa !6
  %call = call i32* @GetMallocAddressLocation(i8* %1)
  store i32* %call, i32** %malloc_addr_location, align 4, !tbaa !6
  %2 = load i32*, i32** %malloc_addr_location, align 4, !tbaa !6
  %3 = load i32, i32* %2, align 4, !tbaa !2
  %4 = inttoptr i32 %3 to i8*
  %5 = bitcast i32** %malloc_addr_location to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #4
  ret i8* %4
}

declare void @free(i8*) #2

; Function Attrs: nounwind
define hidden i8* @aom_memset16(i8* %dest, i32 %val, i32 %length) #0 {
entry:
  %dest.addr = alloca i8*, align 4
  %val.addr = alloca i32, align 4
  %length.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %dest16 = alloca i16*, align 4
  store i8* %dest, i8** %dest.addr, align 4, !tbaa !6
  store i32 %val, i32* %val.addr, align 4, !tbaa !10
  store i32 %length, i32* %length.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i16** %dest16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i8*, i8** %dest.addr, align 4, !tbaa !6
  %3 = bitcast i8* %2 to i16*
  store i16* %3, i16** %dest16, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %i, align 4, !tbaa !2
  %5 = load i32, i32* %length.addr, align 4, !tbaa !2
  %cmp = icmp ult i32 %4, %5
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load i32, i32* %val.addr, align 4, !tbaa !10
  %conv = trunc i32 %6 to i16
  %7 = load i16*, i16** %dest16, align 4, !tbaa !6
  %incdec.ptr = getelementptr inbounds i16, i16* %7, i32 1
  store i16* %incdec.ptr, i16** %dest16, align 4, !tbaa !6
  store i16 %conv, i16* %7, align 2, !tbaa !12
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4, !tbaa !2
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = load i8*, i8** %dest.addr, align 4, !tbaa !6
  %10 = bitcast i16** %dest16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret i8* %9
}

; Function Attrs: nounwind
define internal i32* @GetMallocAddressLocation(i8* %mem) #0 {
entry:
  %mem.addr = alloca i8*, align 4
  store i8* %mem, i8** %mem.addr, align 4, !tbaa !6
  %0 = load i8*, i8** %mem.addr, align 4, !tbaa !6
  %1 = bitcast i8* %0 to i32*
  %add.ptr = getelementptr inbounds i32, i32* %1, i32 -1
  ret i32* %add.ptr
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"long", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"any pointer", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"long long", !4, i64 0}
!10 = !{!11, !11, i64 0}
!11 = !{!"int", !4, i64 0}
!12 = !{!13, !13, i64 0}
!13 = !{!"short", !4, i64 0}
