; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/third_party/fastfeat/nonmax.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/third_party/fastfeat/nonmax.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.xy = type { i32, i32 }

; Function Attrs: nounwind
define hidden %struct.xy* @aom_nonmax_suppression(%struct.xy* %corners, i32* %scores, i32 %num_corners, i32* %ret_num_nonmax) #0 {
entry:
  %retval = alloca %struct.xy*, align 4
  %corners.addr = alloca %struct.xy*, align 4
  %scores.addr = alloca i32*, align 4
  %num_corners.addr = alloca i32, align 4
  %ret_num_nonmax.addr = alloca i32*, align 4
  %num_nonmax = alloca i32, align 4
  %last_row = alloca i32, align 4
  %row_start = alloca i32*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %ret_nonmax = alloca %struct.xy*, align 4
  %sz = alloca i32, align 4
  %point_above = alloca i32, align 4
  %point_below = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %prev_row = alloca i32, align 4
  %score = alloca i32, align 4
  %pos = alloca %struct.xy, align 4
  %x113 = alloca i32, align 4
  %x193 = alloca i32, align 4
  store %struct.xy* %corners, %struct.xy** %corners.addr, align 4, !tbaa !2
  store i32* %scores, i32** %scores.addr, align 4, !tbaa !2
  store i32 %num_corners, i32* %num_corners.addr, align 4, !tbaa !6
  store i32* %ret_num_nonmax, i32** %ret_num_nonmax.addr, align 4, !tbaa !2
  %0 = bitcast i32* %num_nonmax to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %num_nonmax, align 4, !tbaa !6
  %1 = bitcast i32* %last_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32** %row_start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast %struct.xy** %ret_nonmax to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load i32, i32* %num_corners.addr, align 4, !tbaa !6
  store i32 %7, i32* %sz, align 4, !tbaa !6
  %8 = bitcast i32* %point_above to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  store i32 0, i32* %point_above, align 4, !tbaa !6
  %9 = bitcast i32* %point_below to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  store i32 0, i32* %point_below, align 4, !tbaa !6
  %10 = load i32, i32* %num_corners.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %10, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %11 = load i32*, i32** %ret_num_nonmax.addr, align 4, !tbaa !2
  store i32 0, i32* %11, align 4, !tbaa !6
  store %struct.xy* null, %struct.xy** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup229

if.end:                                           ; preds = %entry
  %12 = load i32, i32* %num_corners.addr, align 4, !tbaa !6
  %mul = mul i32 %12, 8
  %call = call i8* @malloc(i32 %mul)
  %13 = bitcast i8* %call to %struct.xy*
  store %struct.xy* %13, %struct.xy** %ret_nonmax, align 4, !tbaa !2
  %14 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %15 = load i32, i32* %num_corners.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %15, 1
  %arrayidx = getelementptr inbounds %struct.xy, %struct.xy* %14, i32 %sub
  %y = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx, i32 0, i32 1
  %16 = load i32, i32* %y, align 4, !tbaa !8
  store i32 %16, i32* %last_row, align 4, !tbaa !6
  %17 = load i32, i32* %last_row, align 4, !tbaa !6
  %add = add nsw i32 %17, 1
  %mul1 = mul i32 %add, 4
  %call2 = call i8* @malloc(i32 %mul1)
  %18 = bitcast i8* %call2 to i32*
  store i32* %18, i32** %row_start, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %20 = load i32, i32* %last_row, align 4, !tbaa !6
  %add3 = add nsw i32 %20, 1
  %cmp4 = icmp slt i32 %19, %add3
  br i1 %cmp4, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %21 = load i32*, i32** %row_start, align 4, !tbaa !2
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds i32, i32* %21, i32 %22
  store i32 -1, i32* %arrayidx5, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %24 = bitcast i32* %prev_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #3
  store i32 -1, i32* %prev_row, align 4, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc19, %for.end
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %26 = load i32, i32* %num_corners.addr, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %25, %26
  br i1 %cmp7, label %for.body8, label %for.end21

for.body8:                                        ; preds = %for.cond6
  %27 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %28 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds %struct.xy, %struct.xy* %27, i32 %28
  %y10 = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx9, i32 0, i32 1
  %29 = load i32, i32* %y10, align 4, !tbaa !8
  %30 = load i32, i32* %prev_row, align 4, !tbaa !6
  %cmp11 = icmp ne i32 %29, %30
  br i1 %cmp11, label %if.then12, label %if.end18

if.then12:                                        ; preds = %for.body8
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %32 = load i32*, i32** %row_start, align 4, !tbaa !2
  %33 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %34 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds %struct.xy, %struct.xy* %33, i32 %34
  %y14 = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx13, i32 0, i32 1
  %35 = load i32, i32* %y14, align 4, !tbaa !8
  %arrayidx15 = getelementptr inbounds i32, i32* %32, i32 %35
  store i32 %31, i32* %arrayidx15, align 4, !tbaa !6
  %36 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds %struct.xy, %struct.xy* %36, i32 %37
  %y17 = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx16, i32 0, i32 1
  %38 = load i32, i32* %y17, align 4, !tbaa !8
  store i32 %38, i32* %prev_row, align 4, !tbaa !6
  br label %if.end18

if.end18:                                         ; preds = %if.then12, %for.body8
  br label %for.inc19

for.inc19:                                        ; preds = %if.end18
  %39 = load i32, i32* %i, align 4, !tbaa !6
  %inc20 = add nsw i32 %39, 1
  store i32 %inc20, i32* %i, align 4, !tbaa !6
  br label %for.cond6

for.end21:                                        ; preds = %for.cond6
  %40 = bitcast i32* %prev_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #3
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc226, %for.end21
  %41 = load i32, i32* %i, align 4, !tbaa !6
  %42 = load i32, i32* %sz, align 4, !tbaa !6
  %cmp23 = icmp slt i32 %41, %42
  br i1 %cmp23, label %for.body24, label %for.end228

for.body24:                                       ; preds = %for.cond22
  %43 = bitcast i32* %score to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #3
  %44 = load i32*, i32** %scores.addr, align 4, !tbaa !2
  %45 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx25 = getelementptr inbounds i32, i32* %44, i32 %45
  %46 = load i32, i32* %arrayidx25, align 4, !tbaa !6
  store i32 %46, i32* %score, align 4, !tbaa !6
  %47 = bitcast %struct.xy* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %47) #3
  %48 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %49 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx26 = getelementptr inbounds %struct.xy, %struct.xy* %48, i32 %49
  %50 = bitcast %struct.xy* %pos to i8*
  %51 = bitcast %struct.xy* %arrayidx26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %50, i8* align 4 %51, i32 8, i1 false), !tbaa.struct !10
  %52 = load i32, i32* %i, align 4, !tbaa !6
  %cmp27 = icmp sgt i32 %52, 0
  br i1 %cmp27, label %if.then28, label %if.end45

if.then28:                                        ; preds = %for.body24
  %53 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %54 = load i32, i32* %i, align 4, !tbaa !6
  %sub29 = sub nsw i32 %54, 1
  %arrayidx30 = getelementptr inbounds %struct.xy, %struct.xy* %53, i32 %sub29
  %x = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx30, i32 0, i32 0
  %55 = load i32, i32* %x, align 4, !tbaa !11
  %x31 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 0
  %56 = load i32, i32* %x31, align 4, !tbaa !11
  %sub32 = sub nsw i32 %56, 1
  %cmp33 = icmp eq i32 %55, %sub32
  br i1 %cmp33, label %land.lhs.true, label %if.end44

land.lhs.true:                                    ; preds = %if.then28
  %57 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %58 = load i32, i32* %i, align 4, !tbaa !6
  %sub34 = sub nsw i32 %58, 1
  %arrayidx35 = getelementptr inbounds %struct.xy, %struct.xy* %57, i32 %sub34
  %y36 = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx35, i32 0, i32 1
  %59 = load i32, i32* %y36, align 4, !tbaa !8
  %y37 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 1
  %60 = load i32, i32* %y37, align 4, !tbaa !8
  %cmp38 = icmp eq i32 %59, %60
  br i1 %cmp38, label %land.lhs.true39, label %if.end44

land.lhs.true39:                                  ; preds = %land.lhs.true
  %61 = load i32*, i32** %scores.addr, align 4, !tbaa !2
  %62 = load i32, i32* %i, align 4, !tbaa !6
  %sub40 = sub nsw i32 %62, 1
  %arrayidx41 = getelementptr inbounds i32, i32* %61, i32 %sub40
  %63 = load i32, i32* %arrayidx41, align 4, !tbaa !6
  %64 = load i32, i32* %score, align 4, !tbaa !6
  %cmp42 = icmp sge i32 %63, %64
  br i1 %cmp42, label %if.then43, label %if.end44

if.then43:                                        ; preds = %land.lhs.true39
  store i32 10, i32* %cleanup.dest.slot, align 4
  br label %cleanup222

if.end44:                                         ; preds = %land.lhs.true39, %land.lhs.true, %if.then28
  br label %if.end45

if.end45:                                         ; preds = %if.end44, %for.body24
  %65 = load i32, i32* %i, align 4, !tbaa !6
  %66 = load i32, i32* %sz, align 4, !tbaa !6
  %sub46 = sub nsw i32 %66, 1
  %cmp47 = icmp slt i32 %65, %sub46
  br i1 %cmp47, label %if.then48, label %if.end67

if.then48:                                        ; preds = %if.end45
  %67 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %68 = load i32, i32* %i, align 4, !tbaa !6
  %add49 = add nsw i32 %68, 1
  %arrayidx50 = getelementptr inbounds %struct.xy, %struct.xy* %67, i32 %add49
  %x51 = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx50, i32 0, i32 0
  %69 = load i32, i32* %x51, align 4, !tbaa !11
  %x52 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 0
  %70 = load i32, i32* %x52, align 4, !tbaa !11
  %add53 = add nsw i32 %70, 1
  %cmp54 = icmp eq i32 %69, %add53
  br i1 %cmp54, label %land.lhs.true55, label %if.end66

land.lhs.true55:                                  ; preds = %if.then48
  %71 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %72 = load i32, i32* %i, align 4, !tbaa !6
  %add56 = add nsw i32 %72, 1
  %arrayidx57 = getelementptr inbounds %struct.xy, %struct.xy* %71, i32 %add56
  %y58 = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx57, i32 0, i32 1
  %73 = load i32, i32* %y58, align 4, !tbaa !8
  %y59 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 1
  %74 = load i32, i32* %y59, align 4, !tbaa !8
  %cmp60 = icmp eq i32 %73, %74
  br i1 %cmp60, label %land.lhs.true61, label %if.end66

land.lhs.true61:                                  ; preds = %land.lhs.true55
  %75 = load i32*, i32** %scores.addr, align 4, !tbaa !2
  %76 = load i32, i32* %i, align 4, !tbaa !6
  %add62 = add nsw i32 %76, 1
  %arrayidx63 = getelementptr inbounds i32, i32* %75, i32 %add62
  %77 = load i32, i32* %arrayidx63, align 4, !tbaa !6
  %78 = load i32, i32* %score, align 4, !tbaa !6
  %cmp64 = icmp sge i32 %77, %78
  br i1 %cmp64, label %if.then65, label %if.end66

if.then65:                                        ; preds = %land.lhs.true61
  store i32 10, i32* %cleanup.dest.slot, align 4
  br label %cleanup222

if.end66:                                         ; preds = %land.lhs.true61, %land.lhs.true55, %if.then48
  br label %if.end67

if.end67:                                         ; preds = %if.end66, %if.end45
  %y68 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 1
  %79 = load i32, i32* %y68, align 4, !tbaa !8
  %cmp69 = icmp sgt i32 %79, 0
  br i1 %cmp69, label %if.then70, label %if.end134

if.then70:                                        ; preds = %if.end67
  %80 = load i32*, i32** %row_start, align 4, !tbaa !2
  %y71 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 1
  %81 = load i32, i32* %y71, align 4, !tbaa !8
  %sub72 = sub nsw i32 %81, 1
  %arrayidx73 = getelementptr inbounds i32, i32* %80, i32 %sub72
  %82 = load i32, i32* %arrayidx73, align 4, !tbaa !6
  %cmp74 = icmp ne i32 %82, -1
  br i1 %cmp74, label %if.then75, label %if.end133

if.then75:                                        ; preds = %if.then70
  %83 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %84 = load i32, i32* %point_above, align 4, !tbaa !6
  %arrayidx76 = getelementptr inbounds %struct.xy, %struct.xy* %83, i32 %84
  %y77 = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx76, i32 0, i32 1
  %85 = load i32, i32* %y77, align 4, !tbaa !8
  %y78 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 1
  %86 = load i32, i32* %y78, align 4, !tbaa !8
  %sub79 = sub nsw i32 %86, 1
  %cmp80 = icmp slt i32 %85, %sub79
  br i1 %cmp80, label %if.then81, label %if.end85

if.then81:                                        ; preds = %if.then75
  %87 = load i32*, i32** %row_start, align 4, !tbaa !2
  %y82 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 1
  %88 = load i32, i32* %y82, align 4, !tbaa !8
  %sub83 = sub nsw i32 %88, 1
  %arrayidx84 = getelementptr inbounds i32, i32* %87, i32 %sub83
  %89 = load i32, i32* %arrayidx84, align 4, !tbaa !6
  store i32 %89, i32* %point_above, align 4, !tbaa !6
  br label %if.end85

if.end85:                                         ; preds = %if.then81, %if.then75
  br label %for.cond86

for.cond86:                                       ; preds = %for.inc97, %if.end85
  %90 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %91 = load i32, i32* %point_above, align 4, !tbaa !6
  %arrayidx87 = getelementptr inbounds %struct.xy, %struct.xy* %90, i32 %91
  %y88 = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx87, i32 0, i32 1
  %92 = load i32, i32* %y88, align 4, !tbaa !8
  %y89 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 1
  %93 = load i32, i32* %y89, align 4, !tbaa !8
  %cmp90 = icmp slt i32 %92, %93
  br i1 %cmp90, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond86
  %94 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %95 = load i32, i32* %point_above, align 4, !tbaa !6
  %arrayidx91 = getelementptr inbounds %struct.xy, %struct.xy* %94, i32 %95
  %x92 = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx91, i32 0, i32 0
  %96 = load i32, i32* %x92, align 4, !tbaa !11
  %x93 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 0
  %97 = load i32, i32* %x93, align 4, !tbaa !11
  %sub94 = sub nsw i32 %97, 1
  %cmp95 = icmp slt i32 %96, %sub94
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond86
  %98 = phi i1 [ false, %for.cond86 ], [ %cmp95, %land.rhs ]
  br i1 %98, label %for.body96, label %for.end99

for.body96:                                       ; preds = %land.end
  br label %for.inc97

for.inc97:                                        ; preds = %for.body96
  %99 = load i32, i32* %point_above, align 4, !tbaa !6
  %inc98 = add nsw i32 %99, 1
  store i32 %inc98, i32* %point_above, align 4, !tbaa !6
  br label %for.cond86

for.end99:                                        ; preds = %land.end
  %100 = load i32, i32* %point_above, align 4, !tbaa !6
  store i32 %100, i32* %j, align 4, !tbaa !6
  br label %for.cond100

for.cond100:                                      ; preds = %for.inc130, %for.end99
  %101 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %102 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx101 = getelementptr inbounds %struct.xy, %struct.xy* %101, i32 %102
  %y102 = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx101, i32 0, i32 1
  %103 = load i32, i32* %y102, align 4, !tbaa !8
  %y103 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 1
  %104 = load i32, i32* %y103, align 4, !tbaa !8
  %cmp104 = icmp slt i32 %103, %104
  br i1 %cmp104, label %land.rhs105, label %land.end111

land.rhs105:                                      ; preds = %for.cond100
  %105 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %106 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx106 = getelementptr inbounds %struct.xy, %struct.xy* %105, i32 %106
  %x107 = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx106, i32 0, i32 0
  %107 = load i32, i32* %x107, align 4, !tbaa !11
  %x108 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 0
  %108 = load i32, i32* %x108, align 4, !tbaa !11
  %add109 = add nsw i32 %108, 1
  %cmp110 = icmp sle i32 %107, %add109
  br label %land.end111

land.end111:                                      ; preds = %land.rhs105, %for.cond100
  %109 = phi i1 [ false, %for.cond100 ], [ %cmp110, %land.rhs105 ]
  br i1 %109, label %for.body112, label %for.end132

for.body112:                                      ; preds = %land.end111
  %110 = bitcast i32* %x113 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %110) #3
  %111 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %112 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx114 = getelementptr inbounds %struct.xy, %struct.xy* %111, i32 %112
  %x115 = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx114, i32 0, i32 0
  %113 = load i32, i32* %x115, align 4, !tbaa !11
  store i32 %113, i32* %x113, align 4, !tbaa !6
  %114 = load i32, i32* %x113, align 4, !tbaa !6
  %x116 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 0
  %115 = load i32, i32* %x116, align 4, !tbaa !11
  %sub117 = sub nsw i32 %115, 1
  %cmp118 = icmp eq i32 %114, %sub117
  br i1 %cmp118, label %land.lhs.true125, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body112
  %116 = load i32, i32* %x113, align 4, !tbaa !6
  %x119 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 0
  %117 = load i32, i32* %x119, align 4, !tbaa !11
  %cmp120 = icmp eq i32 %116, %117
  br i1 %cmp120, label %land.lhs.true125, label %lor.lhs.false121

lor.lhs.false121:                                 ; preds = %lor.lhs.false
  %118 = load i32, i32* %x113, align 4, !tbaa !6
  %x122 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 0
  %119 = load i32, i32* %x122, align 4, !tbaa !11
  %add123 = add nsw i32 %119, 1
  %cmp124 = icmp eq i32 %118, %add123
  br i1 %cmp124, label %land.lhs.true125, label %if.end129

land.lhs.true125:                                 ; preds = %lor.lhs.false121, %lor.lhs.false, %for.body112
  %120 = load i32*, i32** %scores.addr, align 4, !tbaa !2
  %121 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx126 = getelementptr inbounds i32, i32* %120, i32 %121
  %122 = load i32, i32* %arrayidx126, align 4, !tbaa !6
  %123 = load i32, i32* %score, align 4, !tbaa !6
  %cmp127 = icmp sge i32 %122, %123
  br i1 %cmp127, label %if.then128, label %if.end129

if.then128:                                       ; preds = %land.lhs.true125
  store i32 17, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end129:                                        ; preds = %land.lhs.true125, %lor.lhs.false121
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.then128, %if.end129
  %124 = bitcast i32* %x113 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup222 [
    i32 0, label %cleanup.cont
    i32 17, label %cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc130

for.inc130:                                       ; preds = %cleanup.cont
  %125 = load i32, i32* %j, align 4, !tbaa !6
  %inc131 = add nsw i32 %125, 1
  store i32 %inc131, i32* %j, align 4, !tbaa !6
  br label %for.cond100

for.end132:                                       ; preds = %land.end111
  br label %if.end133

if.end133:                                        ; preds = %for.end132, %if.then70
  br label %if.end134

if.end134:                                        ; preds = %if.end133, %if.end67
  %y135 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 1
  %126 = load i32, i32* %y135, align 4, !tbaa !8
  %cmp136 = icmp sge i32 %126, 0
  br i1 %cmp136, label %if.then137, label %if.end218

if.then137:                                       ; preds = %if.end134
  %y138 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 1
  %127 = load i32, i32* %y138, align 4, !tbaa !8
  %128 = load i32, i32* %last_row, align 4, !tbaa !6
  %cmp139 = icmp ne i32 %127, %128
  br i1 %cmp139, label %land.lhs.true140, label %if.end217

land.lhs.true140:                                 ; preds = %if.then137
  %129 = load i32*, i32** %row_start, align 4, !tbaa !2
  %y141 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 1
  %130 = load i32, i32* %y141, align 4, !tbaa !8
  %add142 = add nsw i32 %130, 1
  %arrayidx143 = getelementptr inbounds i32, i32* %129, i32 %add142
  %131 = load i32, i32* %arrayidx143, align 4, !tbaa !6
  %cmp144 = icmp ne i32 %131, -1
  br i1 %cmp144, label %land.lhs.true145, label %if.end217

land.lhs.true145:                                 ; preds = %land.lhs.true140
  %132 = load i32, i32* %point_below, align 4, !tbaa !6
  %133 = load i32, i32* %sz, align 4, !tbaa !6
  %cmp146 = icmp slt i32 %132, %133
  br i1 %cmp146, label %if.then147, label %if.end217

if.then147:                                       ; preds = %land.lhs.true145
  %134 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %135 = load i32, i32* %point_below, align 4, !tbaa !6
  %arrayidx148 = getelementptr inbounds %struct.xy, %struct.xy* %134, i32 %135
  %y149 = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx148, i32 0, i32 1
  %136 = load i32, i32* %y149, align 4, !tbaa !8
  %y150 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 1
  %137 = load i32, i32* %y150, align 4, !tbaa !8
  %add151 = add nsw i32 %137, 1
  %cmp152 = icmp slt i32 %136, %add151
  br i1 %cmp152, label %if.then153, label %if.end157

if.then153:                                       ; preds = %if.then147
  %138 = load i32*, i32** %row_start, align 4, !tbaa !2
  %y154 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 1
  %139 = load i32, i32* %y154, align 4, !tbaa !8
  %add155 = add nsw i32 %139, 1
  %arrayidx156 = getelementptr inbounds i32, i32* %138, i32 %add155
  %140 = load i32, i32* %arrayidx156, align 4, !tbaa !6
  store i32 %140, i32* %point_below, align 4, !tbaa !6
  br label %if.end157

if.end157:                                        ; preds = %if.then153, %if.then147
  br label %for.cond158

for.cond158:                                      ; preds = %for.inc174, %if.end157
  %141 = load i32, i32* %point_below, align 4, !tbaa !6
  %142 = load i32, i32* %sz, align 4, !tbaa !6
  %cmp159 = icmp slt i32 %141, %142
  br i1 %cmp159, label %land.lhs.true160, label %land.end172

land.lhs.true160:                                 ; preds = %for.cond158
  %143 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %144 = load i32, i32* %point_below, align 4, !tbaa !6
  %arrayidx161 = getelementptr inbounds %struct.xy, %struct.xy* %143, i32 %144
  %y162 = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx161, i32 0, i32 1
  %145 = load i32, i32* %y162, align 4, !tbaa !8
  %y163 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 1
  %146 = load i32, i32* %y163, align 4, !tbaa !8
  %add164 = add nsw i32 %146, 1
  %cmp165 = icmp eq i32 %145, %add164
  br i1 %cmp165, label %land.rhs166, label %land.end172

land.rhs166:                                      ; preds = %land.lhs.true160
  %147 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %148 = load i32, i32* %point_below, align 4, !tbaa !6
  %arrayidx167 = getelementptr inbounds %struct.xy, %struct.xy* %147, i32 %148
  %x168 = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx167, i32 0, i32 0
  %149 = load i32, i32* %x168, align 4, !tbaa !11
  %x169 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 0
  %150 = load i32, i32* %x169, align 4, !tbaa !11
  %sub170 = sub nsw i32 %150, 1
  %cmp171 = icmp slt i32 %149, %sub170
  br label %land.end172

land.end172:                                      ; preds = %land.rhs166, %land.lhs.true160, %for.cond158
  %151 = phi i1 [ false, %land.lhs.true160 ], [ false, %for.cond158 ], [ %cmp171, %land.rhs166 ]
  br i1 %151, label %for.body173, label %for.end176

for.body173:                                      ; preds = %land.end172
  br label %for.inc174

for.inc174:                                       ; preds = %for.body173
  %152 = load i32, i32* %point_below, align 4, !tbaa !6
  %inc175 = add nsw i32 %152, 1
  store i32 %inc175, i32* %point_below, align 4, !tbaa !6
  br label %for.cond158

for.end176:                                       ; preds = %land.end172
  %153 = load i32, i32* %point_below, align 4, !tbaa !6
  store i32 %153, i32* %j, align 4, !tbaa !6
  br label %for.cond177

for.cond177:                                      ; preds = %for.inc214, %for.end176
  %154 = load i32, i32* %j, align 4, !tbaa !6
  %155 = load i32, i32* %sz, align 4, !tbaa !6
  %cmp178 = icmp slt i32 %154, %155
  br i1 %cmp178, label %land.lhs.true179, label %land.end191

land.lhs.true179:                                 ; preds = %for.cond177
  %156 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %157 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx180 = getelementptr inbounds %struct.xy, %struct.xy* %156, i32 %157
  %y181 = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx180, i32 0, i32 1
  %158 = load i32, i32* %y181, align 4, !tbaa !8
  %y182 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 1
  %159 = load i32, i32* %y182, align 4, !tbaa !8
  %add183 = add nsw i32 %159, 1
  %cmp184 = icmp eq i32 %158, %add183
  br i1 %cmp184, label %land.rhs185, label %land.end191

land.rhs185:                                      ; preds = %land.lhs.true179
  %160 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %161 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx186 = getelementptr inbounds %struct.xy, %struct.xy* %160, i32 %161
  %x187 = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx186, i32 0, i32 0
  %162 = load i32, i32* %x187, align 4, !tbaa !11
  %x188 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 0
  %163 = load i32, i32* %x188, align 4, !tbaa !11
  %add189 = add nsw i32 %163, 1
  %cmp190 = icmp sle i32 %162, %add189
  br label %land.end191

land.end191:                                      ; preds = %land.rhs185, %land.lhs.true179, %for.cond177
  %164 = phi i1 [ false, %land.lhs.true179 ], [ false, %for.cond177 ], [ %cmp190, %land.rhs185 ]
  br i1 %164, label %for.body192, label %for.end216

for.body192:                                      ; preds = %land.end191
  %165 = bitcast i32* %x193 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %165) #3
  %166 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %167 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx194 = getelementptr inbounds %struct.xy, %struct.xy* %166, i32 %167
  %x195 = getelementptr inbounds %struct.xy, %struct.xy* %arrayidx194, i32 0, i32 0
  %168 = load i32, i32* %x195, align 4, !tbaa !11
  store i32 %168, i32* %x193, align 4, !tbaa !6
  %169 = load i32, i32* %x193, align 4, !tbaa !6
  %x196 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 0
  %170 = load i32, i32* %x196, align 4, !tbaa !11
  %sub197 = sub nsw i32 %170, 1
  %cmp198 = icmp eq i32 %169, %sub197
  br i1 %cmp198, label %land.lhs.true206, label %lor.lhs.false199

lor.lhs.false199:                                 ; preds = %for.body192
  %171 = load i32, i32* %x193, align 4, !tbaa !6
  %x200 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 0
  %172 = load i32, i32* %x200, align 4, !tbaa !11
  %cmp201 = icmp eq i32 %171, %172
  br i1 %cmp201, label %land.lhs.true206, label %lor.lhs.false202

lor.lhs.false202:                                 ; preds = %lor.lhs.false199
  %173 = load i32, i32* %x193, align 4, !tbaa !6
  %x203 = getelementptr inbounds %struct.xy, %struct.xy* %pos, i32 0, i32 0
  %174 = load i32, i32* %x203, align 4, !tbaa !11
  %add204 = add nsw i32 %174, 1
  %cmp205 = icmp eq i32 %173, %add204
  br i1 %cmp205, label %land.lhs.true206, label %if.end210

land.lhs.true206:                                 ; preds = %lor.lhs.false202, %lor.lhs.false199, %for.body192
  %175 = load i32*, i32** %scores.addr, align 4, !tbaa !2
  %176 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx207 = getelementptr inbounds i32, i32* %175, i32 %176
  %177 = load i32, i32* %arrayidx207, align 4, !tbaa !6
  %178 = load i32, i32* %score, align 4, !tbaa !6
  %cmp208 = icmp sge i32 %177, %178
  br i1 %cmp208, label %if.then209, label %if.end210

if.then209:                                       ; preds = %land.lhs.true206
  store i32 17, i32* %cleanup.dest.slot, align 4
  br label %cleanup211

if.end210:                                        ; preds = %land.lhs.true206, %lor.lhs.false202
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup211

cleanup211:                                       ; preds = %if.then209, %if.end210
  %179 = bitcast i32* %x193 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #3
  %cleanup.dest212 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest212, label %cleanup222 [
    i32 0, label %cleanup.cont213
    i32 17, label %cont
  ]

cleanup.cont213:                                  ; preds = %cleanup211
  br label %for.inc214

for.inc214:                                       ; preds = %cleanup.cont213
  %180 = load i32, i32* %j, align 4, !tbaa !6
  %inc215 = add nsw i32 %180, 1
  store i32 %inc215, i32* %j, align 4, !tbaa !6
  br label %for.cond177

for.end216:                                       ; preds = %land.end191
  br label %if.end217

if.end217:                                        ; preds = %for.end216, %land.lhs.true145, %land.lhs.true140, %if.then137
  br label %if.end218

if.end218:                                        ; preds = %if.end217, %if.end134
  %181 = load %struct.xy*, %struct.xy** %ret_nonmax, align 4, !tbaa !2
  %182 = load i32, i32* %num_nonmax, align 4, !tbaa !6
  %inc219 = add nsw i32 %182, 1
  store i32 %inc219, i32* %num_nonmax, align 4, !tbaa !6
  %arrayidx220 = getelementptr inbounds %struct.xy, %struct.xy* %181, i32 %182
  %183 = load %struct.xy*, %struct.xy** %corners.addr, align 4, !tbaa !2
  %184 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx221 = getelementptr inbounds %struct.xy, %struct.xy* %183, i32 %184
  %185 = bitcast %struct.xy* %arrayidx220 to i8*
  %186 = bitcast %struct.xy* %arrayidx221 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %185, i8* align 4 %186, i32 8, i1 false), !tbaa.struct !10
  br label %cont

cont:                                             ; preds = %if.end218, %cleanup211, %cleanup
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup222

cleanup222:                                       ; preds = %cont, %cleanup211, %cleanup, %if.then65, %if.then43
  %187 = bitcast %struct.xy* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %187) #3
  %188 = bitcast i32* %score to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %188) #3
  %cleanup.dest224 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest224, label %unreachable [
    i32 0, label %cleanup.cont225
    i32 10, label %for.inc226
  ]

cleanup.cont225:                                  ; preds = %cleanup222
  br label %for.inc226

for.inc226:                                       ; preds = %cleanup.cont225, %cleanup222
  %189 = load i32, i32* %i, align 4, !tbaa !6
  %inc227 = add nsw i32 %189, 1
  store i32 %inc227, i32* %i, align 4, !tbaa !6
  br label %for.cond22

for.end228:                                       ; preds = %for.cond22
  %190 = load i32*, i32** %row_start, align 4, !tbaa !2
  %191 = bitcast i32* %190 to i8*
  call void @free(i8* %191)
  %192 = load i32, i32* %num_nonmax, align 4, !tbaa !6
  %193 = load i32*, i32** %ret_num_nonmax.addr, align 4, !tbaa !2
  store i32 %192, i32* %193, align 4, !tbaa !6
  %194 = load %struct.xy*, %struct.xy** %ret_nonmax, align 4, !tbaa !2
  store %struct.xy* %194, %struct.xy** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup229

cleanup229:                                       ; preds = %for.end228, %if.then
  %195 = bitcast i32* %point_below to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #3
  %196 = bitcast i32* %point_above to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #3
  %197 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #3
  %198 = bitcast %struct.xy** %ret_nonmax to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #3
  %199 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #3
  %200 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #3
  %201 = bitcast i32** %row_start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #3
  %202 = bitcast i32* %last_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #3
  %203 = bitcast i32* %num_nonmax to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #3
  %204 = load %struct.xy*, %struct.xy** %retval, align 4
  ret %struct.xy* %204

unreachable:                                      ; preds = %cleanup222
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare i8* @malloc(i32) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

declare void @free(i8*) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !7, i64 4}
!9 = !{!"", !7, i64 0, !7, i64 4}
!10 = !{i64 0, i64 4, !6, i64 4, i64 4, !6}
!11 = !{!9, !7, i64 0}
