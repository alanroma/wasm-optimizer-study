; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-enc-mt/src/avif.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-enc-mt/src/avif.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.AvailableCodec = type { i32, i8*, i8* ()*, %struct.avifCodec* ()*, i32 }
%struct.avifCodec = type { %struct.avifCodecDecodeInput*, %struct.avifCodecConfigurationBox, %struct.avifCodecInternal*, i32 (%struct.avifCodec*, i32)*, i32 (%struct.avifCodec*, %struct.avifImage*)*, i32 (%struct.avifCodec*, %struct.avifEncoder*, %struct.avifImage*, i32, i32, %struct.avifCodecEncodeOutput*)*, i32 (%struct.avifCodec*, %struct.avifCodecEncodeOutput*)*, {}* }
%struct.avifCodecDecodeInput = type { %struct.avifDecodeSampleArray, i32 }
%struct.avifDecodeSampleArray = type { %struct.avifDecodeSample*, i32, i32, i32 }
%struct.avifDecodeSample = type { %struct.avifROData, i32 }
%struct.avifROData = type { i8*, i32 }
%struct.avifCodecConfigurationBox = type { i8, i8, i8, i8, i8, i8, i8, i8, i8 }
%struct.avifCodecInternal = type opaque
%struct.avifImage = type { i32, i32, i32, i32, i32, i32, [3 x i8*], [3 x i32], i32, i32, i8*, i32, i32, %struct.avifRWData, i32, i32, i32, i32, %struct.avifPixelAspectRatioBox, %struct.avifCleanApertureBox, %struct.avifImageRotation, %struct.avifImageMirror, %struct.avifRWData, %struct.avifRWData }
%struct.avifPixelAspectRatioBox = type { i32, i32 }
%struct.avifCleanApertureBox = type { i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.avifImageRotation = type { i8 }
%struct.avifImageMirror = type { i8 }
%struct.avifRWData = type { i8*, i32 }
%struct.avifEncoder = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i64, %struct.avifIOStats, %struct.avifEncoderData* }
%struct.avifIOStats = type { i32, i32 }
%struct.avifEncoderData = type opaque
%struct.avifCodecEncodeOutput = type { %struct.avifEncodeSampleArray }
%struct.avifEncodeSampleArray = type { %struct.avifEncodeSample*, i32, i32, i32 }
%struct.avifEncodeSample = type { %struct.avifRWData, i32 }
%struct.avifPixelFormatInfo = type { i32, i32, i32 }
%struct.avifRGBImage = type { i32, i32, i32, i32, i32, i32, i8*, i32 }

@.str = private unnamed_addr constant [6 x i8] c"0.8.1\00", align 1
@.str.1 = private unnamed_addr constant [7 x i8] c"YUV444\00", align 1
@.str.2 = private unnamed_addr constant [7 x i8] c"YUV420\00", align 1
@.str.3 = private unnamed_addr constant [7 x i8] c"YUV422\00", align 1
@.str.4 = private unnamed_addr constant [7 x i8] c"YUV400\00", align 1
@.str.5 = private unnamed_addr constant [8 x i8] c"Unknown\00", align 1
@.str.6 = private unnamed_addr constant [3 x i8] c"OK\00", align 1
@.str.7 = private unnamed_addr constant [13 x i8] c"Invalid ftyp\00", align 1
@.str.8 = private unnamed_addr constant [11 x i8] c"No content\00", align 1
@.str.9 = private unnamed_addr constant [23 x i8] c"No YUV format selected\00", align 1
@.str.10 = private unnamed_addr constant [16 x i8] c"Reformat failed\00", align 1
@.str.11 = private unnamed_addr constant [18 x i8] c"Unsupported depth\00", align 1
@.str.12 = private unnamed_addr constant [32 x i8] c"Encoding of color planes failed\00", align 1
@.str.13 = private unnamed_addr constant [31 x i8] c"Encoding of alpha plane failed\00", align 1
@.str.14 = private unnamed_addr constant [20 x i8] c"BMFF parsing failed\00", align 1
@.str.15 = private unnamed_addr constant [19 x i8] c"No AV1 items found\00", align 1
@.str.16 = private unnamed_addr constant [32 x i8] c"Decoding of color planes failed\00", align 1
@.str.17 = private unnamed_addr constant [31 x i8] c"Decoding of alpha plane failed\00", align 1
@.str.18 = private unnamed_addr constant [37 x i8] c"Color and alpha planes size mismatch\00", align 1
@.str.19 = private unnamed_addr constant [36 x i8] c"Plane sizes don't match ispe values\00", align 1
@.str.20 = private unnamed_addr constant [19 x i8] c"No codec available\00", align 1
@.str.21 = private unnamed_addr constant [20 x i8] c"No images remaining\00", align 1
@.str.22 = private unnamed_addr constant [21 x i8] c"Invalid Exif payload\00", align 1
@.str.23 = private unnamed_addr constant [19 x i8] c"Invalid image grid\00", align 1
@.str.24 = private unnamed_addr constant [14 x i8] c"Unknown Error\00", align 1
@availableCodecs = internal global [2 x %struct.AvailableCodec] [%struct.AvailableCodec { i32 1, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.27, i32 0, i32 0), i8* ()* @avifCodecVersionAOM, %struct.avifCodec* ()* @avifCodecCreateAOM, i32 3 }, %struct.AvailableCodec zeroinitializer], align 16
@.str.25 = private unnamed_addr constant [3 x i8] c", \00", align 1
@.str.26 = private unnamed_addr constant [2 x i8] c":\00", align 1
@.str.27 = private unnamed_addr constant [4 x i8] c"aom\00", align 1

; Function Attrs: nounwind
define hidden i8* @avifVersion() #0 {
entry:
  ret i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: nounwind
define hidden i8* @avifPixelFormatToString(i32 %format) #0 {
entry:
  %retval = alloca i8*, align 4
  %format.addr = alloca i32, align 4
  store i32 %format, i32* %format.addr, align 4, !tbaa !2
  %0 = load i32, i32* %format.addr, align 4, !tbaa !2
  switch i32 %0, label %sw.default [
    i32 1, label %sw.bb
    i32 3, label %sw.bb1
    i32 2, label %sw.bb2
    i32 4, label %sw.bb3
    i32 0, label %sw.bb4
  ]

sw.bb:                                            ; preds = %entry
  store i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.1, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %entry
  store i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.2, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb2:                                           ; preds = %entry
  store i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb3:                                           ; preds = %entry
  store i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.4, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb4:                                           ; preds = %entry
  br label %sw.default

sw.default:                                       ; preds = %entry, %sw.bb4
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default
  store i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.5, i32 0, i32 0), i8** %retval, align 4
  br label %return

return:                                           ; preds = %sw.epilog, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  %1 = load i8*, i8** %retval, align 4
  ret i8* %1
}

; Function Attrs: nounwind
define hidden void @avifGetPixelFormatInfo(i32 %format, %struct.avifPixelFormatInfo* %info) #0 {
entry:
  %format.addr = alloca i32, align 4
  %info.addr = alloca %struct.avifPixelFormatInfo*, align 4
  store i32 %format, i32* %format.addr, align 4, !tbaa !2
  store %struct.avifPixelFormatInfo* %info, %struct.avifPixelFormatInfo** %info.addr, align 4, !tbaa !5
  %0 = load %struct.avifPixelFormatInfo*, %struct.avifPixelFormatInfo** %info.addr, align 4, !tbaa !5
  %1 = bitcast %struct.avifPixelFormatInfo* %0 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %1, i8 0, i32 12, i1 false)
  %2 = load i32, i32* %format.addr, align 4, !tbaa !2
  switch i32 %2, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb1
    i32 3, label %sw.bb4
    i32 4, label %sw.bb7
    i32 0, label %sw.bb10
  ]

sw.bb:                                            ; preds = %entry
  %3 = load %struct.avifPixelFormatInfo*, %struct.avifPixelFormatInfo** %info.addr, align 4, !tbaa !5
  %chromaShiftX = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %3, i32 0, i32 1
  store i32 0, i32* %chromaShiftX, align 4, !tbaa !7
  %4 = load %struct.avifPixelFormatInfo*, %struct.avifPixelFormatInfo** %info.addr, align 4, !tbaa !5
  %chromaShiftY = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %4, i32 0, i32 2
  store i32 0, i32* %chromaShiftY, align 4, !tbaa !10
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry
  %5 = load %struct.avifPixelFormatInfo*, %struct.avifPixelFormatInfo** %info.addr, align 4, !tbaa !5
  %chromaShiftX2 = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %5, i32 0, i32 1
  store i32 1, i32* %chromaShiftX2, align 4, !tbaa !7
  %6 = load %struct.avifPixelFormatInfo*, %struct.avifPixelFormatInfo** %info.addr, align 4, !tbaa !5
  %chromaShiftY3 = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %6, i32 0, i32 2
  store i32 0, i32* %chromaShiftY3, align 4, !tbaa !10
  br label %sw.epilog

sw.bb4:                                           ; preds = %entry
  %7 = load %struct.avifPixelFormatInfo*, %struct.avifPixelFormatInfo** %info.addr, align 4, !tbaa !5
  %chromaShiftX5 = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %7, i32 0, i32 1
  store i32 1, i32* %chromaShiftX5, align 4, !tbaa !7
  %8 = load %struct.avifPixelFormatInfo*, %struct.avifPixelFormatInfo** %info.addr, align 4, !tbaa !5
  %chromaShiftY6 = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %8, i32 0, i32 2
  store i32 1, i32* %chromaShiftY6, align 4, !tbaa !10
  br label %sw.epilog

sw.bb7:                                           ; preds = %entry
  %9 = load %struct.avifPixelFormatInfo*, %struct.avifPixelFormatInfo** %info.addr, align 4, !tbaa !5
  %chromaShiftX8 = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %9, i32 0, i32 1
  store i32 1, i32* %chromaShiftX8, align 4, !tbaa !7
  %10 = load %struct.avifPixelFormatInfo*, %struct.avifPixelFormatInfo** %info.addr, align 4, !tbaa !5
  %chromaShiftY9 = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %10, i32 0, i32 2
  store i32 1, i32* %chromaShiftY9, align 4, !tbaa !10
  %11 = load %struct.avifPixelFormatInfo*, %struct.avifPixelFormatInfo** %info.addr, align 4, !tbaa !5
  %monochrome = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %11, i32 0, i32 0
  store i32 1, i32* %monochrome, align 4, !tbaa !11
  br label %sw.epilog

sw.bb10:                                          ; preds = %entry
  br label %sw.default

sw.default:                                       ; preds = %entry, %sw.bb10
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb7, %sw.bb4, %sw.bb1, %sw.bb
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #1

; Function Attrs: nounwind
define hidden i8* @avifResultToString(i32 %result) #0 {
entry:
  %retval = alloca i8*, align 4
  %result.addr = alloca i32, align 4
  store i32 %result, i32* %result.addr, align 4, !tbaa !2
  %0 = load i32, i32* %result.addr, align 4, !tbaa !2
  switch i32 %0, label %sw.default [
    i32 0, label %sw.bb
    i32 2, label %sw.bb1
    i32 3, label %sw.bb2
    i32 4, label %sw.bb3
    i32 5, label %sw.bb4
    i32 6, label %sw.bb5
    i32 7, label %sw.bb6
    i32 8, label %sw.bb7
    i32 9, label %sw.bb8
    i32 10, label %sw.bb9
    i32 11, label %sw.bb10
    i32 12, label %sw.bb11
    i32 13, label %sw.bb12
    i32 14, label %sw.bb13
    i32 15, label %sw.bb14
    i32 16, label %sw.bb15
    i32 17, label %sw.bb16
    i32 18, label %sw.bb17
    i32 1, label %sw.bb18
  ]

sw.bb:                                            ; preds = %entry
  store i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.6, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %entry
  store i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.7, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb2:                                           ; preds = %entry
  store i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.8, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb3:                                           ; preds = %entry
  store i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.9, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb4:                                           ; preds = %entry
  store i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.10, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb5:                                           ; preds = %entry
  store i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.11, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb6:                                           ; preds = %entry
  store i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.12, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb7:                                           ; preds = %entry
  store i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.13, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb8:                                           ; preds = %entry
  store i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.14, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb9:                                           ; preds = %entry
  store i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.15, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb10:                                          ; preds = %entry
  store i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.16, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb11:                                          ; preds = %entry
  store i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.17, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb12:                                          ; preds = %entry
  store i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.18, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb13:                                          ; preds = %entry
  store i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.19, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb14:                                          ; preds = %entry
  store i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.20, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb15:                                          ; preds = %entry
  store i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.21, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb16:                                          ; preds = %entry
  store i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.22, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb17:                                          ; preds = %entry
  store i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.23, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb18:                                          ; preds = %entry
  br label %sw.default

sw.default:                                       ; preds = %entry, %sw.bb18
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default
  store i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.24, i32 0, i32 0), i8** %retval, align 4
  br label %return

return:                                           ; preds = %sw.epilog, %sw.bb17, %sw.bb16, %sw.bb15, %sw.bb14, %sw.bb13, %sw.bb12, %sw.bb11, %sw.bb10, %sw.bb9, %sw.bb8, %sw.bb7, %sw.bb6, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  %1 = load i8*, i8** %retval, align 4
  ret i8* %1
}

; Function Attrs: nounwind
define hidden %struct.avifImage* @avifImageCreate(i32 %width, i32 %height, i32 %depth, i32 %yuvFormat) #0 {
entry:
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %depth.addr = alloca i32, align 4
  %yuvFormat.addr = alloca i32, align 4
  %image = alloca %struct.avifImage*, align 4
  store i32 %width, i32* %width.addr, align 4, !tbaa !12
  store i32 %height, i32* %height.addr, align 4, !tbaa !12
  store i32 %depth, i32* %depth.addr, align 4, !tbaa !12
  store i32 %yuvFormat, i32* %yuvFormat.addr, align 4, !tbaa !2
  %0 = bitcast %struct.avifImage** %image to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %call = call i8* @avifAlloc(i32 152)
  %1 = bitcast i8* %call to %struct.avifImage*
  store %struct.avifImage* %1, %struct.avifImage** %image, align 4, !tbaa !5
  %2 = load %struct.avifImage*, %struct.avifImage** %image, align 4, !tbaa !5
  call void @avifImageSetDefaults(%struct.avifImage* %2)
  %3 = load i32, i32* %width.addr, align 4, !tbaa !12
  %4 = load %struct.avifImage*, %struct.avifImage** %image, align 4, !tbaa !5
  %width1 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %4, i32 0, i32 0
  store i32 %3, i32* %width1, align 4, !tbaa !13
  %5 = load i32, i32* %height.addr, align 4, !tbaa !12
  %6 = load %struct.avifImage*, %struct.avifImage** %image, align 4, !tbaa !5
  %height2 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %6, i32 0, i32 1
  store i32 %5, i32* %height2, align 4, !tbaa !21
  %7 = load i32, i32* %depth.addr, align 4, !tbaa !12
  %8 = load %struct.avifImage*, %struct.avifImage** %image, align 4, !tbaa !5
  %depth3 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %8, i32 0, i32 2
  store i32 %7, i32* %depth3, align 4, !tbaa !22
  %9 = load i32, i32* %yuvFormat.addr, align 4, !tbaa !2
  %10 = load %struct.avifImage*, %struct.avifImage** %image, align 4, !tbaa !5
  %yuvFormat4 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %10, i32 0, i32 3
  store i32 %9, i32* %yuvFormat4, align 4, !tbaa !23
  %11 = load %struct.avifImage*, %struct.avifImage** %image, align 4, !tbaa !5
  %12 = bitcast %struct.avifImage** %image to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #4
  ret %struct.avifImage* %11
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

declare i8* @avifAlloc(i32) #3

; Function Attrs: nounwind
define internal void @avifImageSetDefaults(%struct.avifImage* %image) #0 {
entry:
  %image.addr = alloca %struct.avifImage*, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %0 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %1 = bitcast %struct.avifImage* %0 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %1, i8 0, i32 152, i1 false)
  %2 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvRange = getelementptr inbounds %struct.avifImage, %struct.avifImage* %2, i32 0, i32 4
  store i32 1, i32* %yuvRange, align 4, !tbaa !24
  %3 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %alphaRange = getelementptr inbounds %struct.avifImage, %struct.avifImage* %3, i32 0, i32 9
  store i32 1, i32* %alphaRange, align 4, !tbaa !25
  %4 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %colorPrimaries = getelementptr inbounds %struct.avifImage, %struct.avifImage* %4, i32 0, i32 14
  store i32 2, i32* %colorPrimaries, align 4, !tbaa !26
  %5 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %transferCharacteristics = getelementptr inbounds %struct.avifImage, %struct.avifImage* %5, i32 0, i32 15
  store i32 2, i32* %transferCharacteristics, align 4, !tbaa !27
  %6 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %matrixCoefficients = getelementptr inbounds %struct.avifImage, %struct.avifImage* %6, i32 0, i32 16
  store i32 2, i32* %matrixCoefficients, align 4, !tbaa !28
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define hidden %struct.avifImage* @avifImageCreateEmpty() #0 {
entry:
  %call = call %struct.avifImage* @avifImageCreate(i32 0, i32 0, i32 0, i32 0)
  ret %struct.avifImage* %call
}

; Function Attrs: nounwind
define hidden void @avifImageCopy(%struct.avifImage* %dstImage, %struct.avifImage* %srcImage, i32 %planes) #0 {
entry:
  %dstImage.addr = alloca %struct.avifImage*, align 4
  %srcImage.addr = alloca %struct.avifImage*, align 4
  %planes.addr = alloca i32, align 4
  %formatInfo = alloca %struct.avifPixelFormatInfo, align 4
  %uvHeight = alloca i32, align 4
  %yuvPlane = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %planeHeight = alloca i32, align 4
  %j = alloca i32, align 4
  %srcRow = alloca i8*, align 4
  %dstRow = alloca i8*, align 4
  %j65 = alloca i32, align 4
  %srcAlphaRow = alloca i8*, align 4
  %dstAlphaRow = alloca i8*, align 4
  store %struct.avifImage* %dstImage, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  store %struct.avifImage* %srcImage, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  store i32 %planes, i32* %planes.addr, align 4, !tbaa !12
  %0 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  call void @avifImageFreePlanes(%struct.avifImage* %0, i32 255)
  %1 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %1, i32 0, i32 0
  %2 = load i32, i32* %width, align 4, !tbaa !13
  %3 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %width1 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %3, i32 0, i32 0
  store i32 %2, i32* %width1, align 4, !tbaa !13
  %4 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %4, i32 0, i32 1
  %5 = load i32, i32* %height, align 4, !tbaa !21
  %6 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %height2 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %6, i32 0, i32 1
  store i32 %5, i32* %height2, align 4, !tbaa !21
  %7 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %7, i32 0, i32 2
  %8 = load i32, i32* %depth, align 4, !tbaa !22
  %9 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %depth3 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %9, i32 0, i32 2
  store i32 %8, i32* %depth3, align 4, !tbaa !22
  %10 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvFormat = getelementptr inbounds %struct.avifImage, %struct.avifImage* %10, i32 0, i32 3
  %11 = load i32, i32* %yuvFormat, align 4, !tbaa !23
  %12 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %yuvFormat4 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %12, i32 0, i32 3
  store i32 %11, i32* %yuvFormat4, align 4, !tbaa !23
  %13 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvRange = getelementptr inbounds %struct.avifImage, %struct.avifImage* %13, i32 0, i32 4
  %14 = load i32, i32* %yuvRange, align 4, !tbaa !24
  %15 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %yuvRange5 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %15, i32 0, i32 4
  store i32 %14, i32* %yuvRange5, align 4, !tbaa !24
  %16 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvChromaSamplePosition = getelementptr inbounds %struct.avifImage, %struct.avifImage* %16, i32 0, i32 5
  %17 = load i32, i32* %yuvChromaSamplePosition, align 4, !tbaa !29
  %18 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %yuvChromaSamplePosition6 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %18, i32 0, i32 5
  store i32 %17, i32* %yuvChromaSamplePosition6, align 4, !tbaa !29
  %19 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %alphaRange = getelementptr inbounds %struct.avifImage, %struct.avifImage* %19, i32 0, i32 9
  %20 = load i32, i32* %alphaRange, align 4, !tbaa !25
  %21 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %alphaRange7 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %21, i32 0, i32 9
  store i32 %20, i32* %alphaRange7, align 4, !tbaa !25
  %22 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %colorPrimaries = getelementptr inbounds %struct.avifImage, %struct.avifImage* %22, i32 0, i32 14
  %23 = load i32, i32* %colorPrimaries, align 4, !tbaa !26
  %24 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %colorPrimaries8 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %24, i32 0, i32 14
  store i32 %23, i32* %colorPrimaries8, align 4, !tbaa !26
  %25 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %transferCharacteristics = getelementptr inbounds %struct.avifImage, %struct.avifImage* %25, i32 0, i32 15
  %26 = load i32, i32* %transferCharacteristics, align 4, !tbaa !27
  %27 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %transferCharacteristics9 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %27, i32 0, i32 15
  store i32 %26, i32* %transferCharacteristics9, align 4, !tbaa !27
  %28 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %matrixCoefficients = getelementptr inbounds %struct.avifImage, %struct.avifImage* %28, i32 0, i32 16
  %29 = load i32, i32* %matrixCoefficients, align 4, !tbaa !28
  %30 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %matrixCoefficients10 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %30, i32 0, i32 16
  store i32 %29, i32* %matrixCoefficients10, align 4, !tbaa !28
  %31 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %transformFlags = getelementptr inbounds %struct.avifImage, %struct.avifImage* %31, i32 0, i32 17
  %32 = load i32, i32* %transformFlags, align 4, !tbaa !30
  %33 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %transformFlags11 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %33, i32 0, i32 17
  store i32 %32, i32* %transformFlags11, align 4, !tbaa !30
  %34 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %pasp = getelementptr inbounds %struct.avifImage, %struct.avifImage* %34, i32 0, i32 18
  %35 = bitcast %struct.avifPixelAspectRatioBox* %pasp to i8*
  %36 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %pasp12 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %36, i32 0, i32 18
  %37 = bitcast %struct.avifPixelAspectRatioBox* %pasp12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %35, i8* align 4 %37, i32 8, i1 false)
  %38 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %clap = getelementptr inbounds %struct.avifImage, %struct.avifImage* %38, i32 0, i32 19
  %39 = bitcast %struct.avifCleanApertureBox* %clap to i8*
  %40 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %clap13 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %40, i32 0, i32 19
  %41 = bitcast %struct.avifCleanApertureBox* %clap13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %39, i8* align 4 %41, i32 32, i1 false)
  %42 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %irot = getelementptr inbounds %struct.avifImage, %struct.avifImage* %42, i32 0, i32 20
  %43 = bitcast %struct.avifImageRotation* %irot to i8*
  %44 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %irot14 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %44, i32 0, i32 20
  %45 = bitcast %struct.avifImageRotation* %irot14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %43, i8* align 4 %45, i32 1, i1 false)
  %46 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %imir = getelementptr inbounds %struct.avifImage, %struct.avifImage* %46, i32 0, i32 21
  %47 = bitcast %struct.avifImageMirror* %imir to i8*
  %48 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %imir15 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %48, i32 0, i32 21
  %49 = bitcast %struct.avifImageMirror* %imir15 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %47, i8* align 1 %49, i32 8, i1 false)
  %50 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %51 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %icc = getelementptr inbounds %struct.avifImage, %struct.avifImage* %51, i32 0, i32 13
  %data = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %icc, i32 0, i32 0
  %52 = load i8*, i8** %data, align 4, !tbaa !31
  %53 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %icc16 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %53, i32 0, i32 13
  %size = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %icc16, i32 0, i32 1
  %54 = load i32, i32* %size, align 4, !tbaa !32
  call void @avifImageSetProfileICC(%struct.avifImage* %50, i8* %52, i32 %54)
  %55 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %56 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %exif = getelementptr inbounds %struct.avifImage, %struct.avifImage* %56, i32 0, i32 22
  %data17 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %exif, i32 0, i32 0
  %57 = load i8*, i8** %data17, align 4, !tbaa !33
  %58 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %exif18 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %58, i32 0, i32 22
  %size19 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %exif18, i32 0, i32 1
  %59 = load i32, i32* %size19, align 4, !tbaa !34
  call void @avifImageSetMetadataExif(%struct.avifImage* %55, i8* %57, i32 %59)
  %60 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %61 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %xmp = getelementptr inbounds %struct.avifImage, %struct.avifImage* %61, i32 0, i32 23
  %data20 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %xmp, i32 0, i32 0
  %62 = load i8*, i8** %data20, align 4, !tbaa !35
  %63 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %xmp21 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %63, i32 0, i32 23
  %size22 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %xmp21, i32 0, i32 1
  %64 = load i32, i32* %size22, align 4, !tbaa !36
  call void @avifImageSetMetadataXMP(%struct.avifImage* %60, i8* %62, i32 %64)
  %65 = load i32, i32* %planes.addr, align 4, !tbaa !12
  %and = and i32 %65, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %land.lhs.true, label %if.end59

land.lhs.true:                                    ; preds = %entry
  %66 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %66, i32 0, i32 6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes, i32 0, i32 0
  %67 = load i8*, i8** %arrayidx, align 4, !tbaa !5
  %tobool23 = icmp ne i8* %67, null
  br i1 %tobool23, label %if.then, label %if.end59

if.then:                                          ; preds = %land.lhs.true
  %68 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  call void @avifImageAllocatePlanes(%struct.avifImage* %68, i32 1)
  %69 = bitcast %struct.avifPixelFormatInfo* %formatInfo to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %69) #4
  %70 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvFormat24 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %70, i32 0, i32 3
  %71 = load i32, i32* %yuvFormat24, align 4, !tbaa !23
  call void @avifGetPixelFormatInfo(i32 %71, %struct.avifPixelFormatInfo* %formatInfo)
  %72 = bitcast i32* %uvHeight to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #4
  %73 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %height25 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %73, i32 0, i32 1
  %74 = load i32, i32* %height25, align 4, !tbaa !21
  %chromaShiftY = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %formatInfo, i32 0, i32 2
  %75 = load i32, i32* %chromaShiftY, align 4, !tbaa !10
  %add = add i32 %74, %75
  %chromaShiftY26 = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %formatInfo, i32 0, i32 2
  %76 = load i32, i32* %chromaShiftY26, align 4, !tbaa !10
  %shr = lshr i32 %add, %76
  store i32 %shr, i32* %uvHeight, align 4, !tbaa !12
  %77 = bitcast i32* %yuvPlane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #4
  store i32 0, i32* %yuvPlane, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc55, %if.then
  %78 = load i32, i32* %yuvPlane, align 4, !tbaa !12
  %cmp = icmp slt i32 %78, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %79 = bitcast i32* %yuvPlane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #4
  br label %for.end58

for.body:                                         ; preds = %for.cond
  %80 = bitcast i32* %planeHeight to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #4
  %81 = load i32, i32* %yuvPlane, align 4, !tbaa !12
  %cmp27 = icmp eq i32 %81, 0
  br i1 %cmp27, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %82 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %height28 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %82, i32 0, i32 1
  %83 = load i32, i32* %height28, align 4, !tbaa !21
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %84 = load i32, i32* %uvHeight, align 4, !tbaa !12
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %83, %cond.true ], [ %84, %cond.false ]
  store i32 %cond, i32* %planeHeight, align 4, !tbaa !12
  %85 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %85, i32 0, i32 7
  %86 = load i32, i32* %yuvPlane, align 4, !tbaa !12
  %arrayidx29 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes, i32 0, i32 %86
  %87 = load i32, i32* %arrayidx29, align 4, !tbaa !12
  %tobool30 = icmp ne i32 %87, 0
  br i1 %tobool30, label %if.end, label %if.then31

if.then31:                                        ; preds = %cond.end
  %88 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %yuvPlanes32 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %88, i32 0, i32 6
  %89 = load i32, i32* %yuvPlane, align 4, !tbaa !12
  %arrayidx33 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes32, i32 0, i32 %89
  %90 = load i8*, i8** %arrayidx33, align 4, !tbaa !5
  call void @avifFree(i8* %90)
  %91 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %yuvPlanes34 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %91, i32 0, i32 6
  %92 = load i32, i32* %yuvPlane, align 4, !tbaa !12
  %arrayidx35 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes34, i32 0, i32 %92
  store i8* null, i8** %arrayidx35, align 4, !tbaa !5
  %93 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %yuvRowBytes36 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %93, i32 0, i32 7
  %94 = load i32, i32* %yuvPlane, align 4, !tbaa !12
  %arrayidx37 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes36, i32 0, i32 %94
  store i32 0, i32* %arrayidx37, align 4, !tbaa !12
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %cond.end
  %95 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %95) #4
  store i32 0, i32* %j, align 4, !tbaa !12
  br label %for.cond38

for.cond38:                                       ; preds = %for.inc, %if.end
  %96 = load i32, i32* %j, align 4, !tbaa !12
  %97 = load i32, i32* %planeHeight, align 4, !tbaa !12
  %cmp39 = icmp ult i32 %96, %97
  br i1 %cmp39, label %for.body41, label %for.cond.cleanup40

for.cond.cleanup40:                               ; preds = %for.cond38
  store i32 5, i32* %cleanup.dest.slot, align 4
  %98 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #4
  br label %for.end

for.body41:                                       ; preds = %for.cond38
  %99 = bitcast i8** %srcRow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #4
  %100 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvPlanes42 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %100, i32 0, i32 6
  %101 = load i32, i32* %yuvPlane, align 4, !tbaa !12
  %arrayidx43 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes42, i32 0, i32 %101
  %102 = load i8*, i8** %arrayidx43, align 4, !tbaa !5
  %103 = load i32, i32* %j, align 4, !tbaa !12
  %104 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvRowBytes44 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %104, i32 0, i32 7
  %105 = load i32, i32* %yuvPlane, align 4, !tbaa !12
  %arrayidx45 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes44, i32 0, i32 %105
  %106 = load i32, i32* %arrayidx45, align 4, !tbaa !12
  %mul = mul i32 %103, %106
  %arrayidx46 = getelementptr inbounds i8, i8* %102, i32 %mul
  store i8* %arrayidx46, i8** %srcRow, align 4, !tbaa !5
  %107 = bitcast i8** %dstRow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %107) #4
  %108 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %yuvPlanes47 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %108, i32 0, i32 6
  %109 = load i32, i32* %yuvPlane, align 4, !tbaa !12
  %arrayidx48 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes47, i32 0, i32 %109
  %110 = load i8*, i8** %arrayidx48, align 4, !tbaa !5
  %111 = load i32, i32* %j, align 4, !tbaa !12
  %112 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %yuvRowBytes49 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %112, i32 0, i32 7
  %113 = load i32, i32* %yuvPlane, align 4, !tbaa !12
  %arrayidx50 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes49, i32 0, i32 %113
  %114 = load i32, i32* %arrayidx50, align 4, !tbaa !12
  %mul51 = mul i32 %111, %114
  %arrayidx52 = getelementptr inbounds i8, i8* %110, i32 %mul51
  store i8* %arrayidx52, i8** %dstRow, align 4, !tbaa !5
  %115 = load i8*, i8** %dstRow, align 4, !tbaa !5
  %116 = load i8*, i8** %srcRow, align 4, !tbaa !5
  %117 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %yuvRowBytes53 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %117, i32 0, i32 7
  %118 = load i32, i32* %yuvPlane, align 4, !tbaa !12
  %arrayidx54 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes53, i32 0, i32 %118
  %119 = load i32, i32* %arrayidx54, align 4, !tbaa !12
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %115, i8* align 1 %116, i32 %119, i1 false)
  %120 = bitcast i8** %dstRow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #4
  %121 = bitcast i8** %srcRow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body41
  %122 = load i32, i32* %j, align 4, !tbaa !12
  %inc = add i32 %122, 1
  store i32 %inc, i32* %j, align 4, !tbaa !12
  br label %for.cond38

for.end:                                          ; preds = %for.cond.cleanup40
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then31
  %123 = bitcast i32* %planeHeight to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 4, label %for.inc55
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc55

for.inc55:                                        ; preds = %cleanup.cont, %cleanup
  %124 = load i32, i32* %yuvPlane, align 4, !tbaa !12
  %inc56 = add nsw i32 %124, 1
  store i32 %inc56, i32* %yuvPlane, align 4, !tbaa !12
  br label %for.cond

for.end58:                                        ; preds = %for.cond.cleanup
  %125 = bitcast i32* %uvHeight to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #4
  %126 = bitcast %struct.avifPixelFormatInfo* %formatInfo to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %126) #4
  br label %if.end59

if.end59:                                         ; preds = %for.end58, %land.lhs.true, %entry
  %127 = load i32, i32* %planes.addr, align 4, !tbaa !12
  %and60 = and i32 %127, 2
  %tobool61 = icmp ne i32 %and60, 0
  br i1 %tobool61, label %land.lhs.true62, label %if.end83

land.lhs.true62:                                  ; preds = %if.end59
  %128 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %alphaPlane = getelementptr inbounds %struct.avifImage, %struct.avifImage* %128, i32 0, i32 10
  %129 = load i8*, i8** %alphaPlane, align 4, !tbaa !37
  %tobool63 = icmp ne i8* %129, null
  br i1 %tobool63, label %if.then64, label %if.end83

if.then64:                                        ; preds = %land.lhs.true62
  %130 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  call void @avifImageAllocatePlanes(%struct.avifImage* %130, i32 2)
  %131 = bitcast i32* %j65 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %131) #4
  store i32 0, i32* %j65, align 4, !tbaa !12
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc79, %if.then64
  %132 = load i32, i32* %j65, align 4, !tbaa !12
  %133 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %height67 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %133, i32 0, i32 1
  %134 = load i32, i32* %height67, align 4, !tbaa !21
  %cmp68 = icmp ult i32 %132, %134
  br i1 %cmp68, label %for.body70, label %for.cond.cleanup69

for.cond.cleanup69:                               ; preds = %for.cond66
  store i32 8, i32* %cleanup.dest.slot, align 4
  %135 = bitcast i32* %j65 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #4
  br label %for.end82

for.body70:                                       ; preds = %for.cond66
  %136 = bitcast i8** %srcAlphaRow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %136) #4
  %137 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %alphaPlane71 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %137, i32 0, i32 10
  %138 = load i8*, i8** %alphaPlane71, align 4, !tbaa !37
  %139 = load i32, i32* %j65, align 4, !tbaa !12
  %140 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %alphaRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %140, i32 0, i32 11
  %141 = load i32, i32* %alphaRowBytes, align 4, !tbaa !38
  %mul72 = mul i32 %139, %141
  %arrayidx73 = getelementptr inbounds i8, i8* %138, i32 %mul72
  store i8* %arrayidx73, i8** %srcAlphaRow, align 4, !tbaa !5
  %142 = bitcast i8** %dstAlphaRow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %142) #4
  %143 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %alphaPlane74 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %143, i32 0, i32 10
  %144 = load i8*, i8** %alphaPlane74, align 4, !tbaa !37
  %145 = load i32, i32* %j65, align 4, !tbaa !12
  %146 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %alphaRowBytes75 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %146, i32 0, i32 11
  %147 = load i32, i32* %alphaRowBytes75, align 4, !tbaa !38
  %mul76 = mul i32 %145, %147
  %arrayidx77 = getelementptr inbounds i8, i8* %144, i32 %mul76
  store i8* %arrayidx77, i8** %dstAlphaRow, align 4, !tbaa !5
  %148 = load i8*, i8** %dstAlphaRow, align 4, !tbaa !5
  %149 = load i8*, i8** %srcAlphaRow, align 4, !tbaa !5
  %150 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %alphaRowBytes78 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %150, i32 0, i32 11
  %151 = load i32, i32* %alphaRowBytes78, align 4, !tbaa !38
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %148, i8* align 1 %149, i32 %151, i1 false)
  %152 = bitcast i8** %dstAlphaRow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #4
  %153 = bitcast i8** %srcAlphaRow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #4
  br label %for.inc79

for.inc79:                                        ; preds = %for.body70
  %154 = load i32, i32* %j65, align 4, !tbaa !12
  %inc80 = add i32 %154, 1
  store i32 %inc80, i32* %j65, align 4, !tbaa !12
  br label %for.cond66

for.end82:                                        ; preds = %for.cond.cleanup69
  br label %if.end83

if.end83:                                         ; preds = %for.end82, %land.lhs.true62, %if.end59
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden void @avifImageFreePlanes(%struct.avifImage* %image, i32 %planes) #0 {
entry:
  %image.addr = alloca %struct.avifImage*, align 4
  %planes.addr = alloca i32, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !5
  store i32 %planes, i32* %planes.addr, align 4, !tbaa !12
  %0 = load i32, i32* %planes.addr, align 4, !tbaa !12
  %and = and i32 %0, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %land.lhs.true, label %if.end19

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvFormat = getelementptr inbounds %struct.avifImage, %struct.avifImage* %1, i32 0, i32 3
  %2 = load i32, i32* %yuvFormat, align 4, !tbaa !23
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %if.then, label %if.end19

if.then:                                          ; preds = %land.lhs.true
  %3 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %imageOwnsYUVPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %3, i32 0, i32 8
  %4 = load i32, i32* %imageOwnsYUVPlanes, align 4, !tbaa !39
  %tobool1 = icmp ne i32 %4, 0
  br i1 %tobool1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %5 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %5, i32 0, i32 6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes, i32 0, i32 0
  %6 = load i8*, i8** %arrayidx, align 4, !tbaa !5
  call void @avifFree(i8* %6)
  %7 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvPlanes3 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %7, i32 0, i32 6
  %arrayidx4 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes3, i32 0, i32 1
  %8 = load i8*, i8** %arrayidx4, align 4, !tbaa !5
  call void @avifFree(i8* %8)
  %9 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvPlanes5 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %9, i32 0, i32 6
  %arrayidx6 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes5, i32 0, i32 2
  %10 = load i8*, i8** %arrayidx6, align 4, !tbaa !5
  call void @avifFree(i8* %10)
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  %11 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvPlanes7 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %11, i32 0, i32 6
  %arrayidx8 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes7, i32 0, i32 0
  store i8* null, i8** %arrayidx8, align 4, !tbaa !5
  %12 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %12, i32 0, i32 7
  %arrayidx9 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes, i32 0, i32 0
  store i32 0, i32* %arrayidx9, align 4, !tbaa !12
  %13 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvPlanes10 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %13, i32 0, i32 6
  %arrayidx11 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes10, i32 0, i32 1
  store i8* null, i8** %arrayidx11, align 4, !tbaa !5
  %14 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvRowBytes12 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %14, i32 0, i32 7
  %arrayidx13 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes12, i32 0, i32 1
  store i32 0, i32* %arrayidx13, align 4, !tbaa !12
  %15 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvPlanes14 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %15, i32 0, i32 6
  %arrayidx15 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes14, i32 0, i32 2
  store i8* null, i8** %arrayidx15, align 4, !tbaa !5
  %16 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvRowBytes16 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %16, i32 0, i32 7
  %arrayidx17 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes16, i32 0, i32 2
  store i32 0, i32* %arrayidx17, align 4, !tbaa !12
  %17 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %imageOwnsYUVPlanes18 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %17, i32 0, i32 8
  store i32 0, i32* %imageOwnsYUVPlanes18, align 4, !tbaa !39
  br label %if.end19

if.end19:                                         ; preds = %if.end, %land.lhs.true, %entry
  %18 = load i32, i32* %planes.addr, align 4, !tbaa !12
  %and20 = and i32 %18, 2
  %tobool21 = icmp ne i32 %and20, 0
  br i1 %tobool21, label %if.then22, label %if.end28

if.then22:                                        ; preds = %if.end19
  %19 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %imageOwnsAlphaPlane = getelementptr inbounds %struct.avifImage, %struct.avifImage* %19, i32 0, i32 12
  %20 = load i32, i32* %imageOwnsAlphaPlane, align 4, !tbaa !40
  %tobool23 = icmp ne i32 %20, 0
  br i1 %tobool23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %if.then22
  %21 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %alphaPlane = getelementptr inbounds %struct.avifImage, %struct.avifImage* %21, i32 0, i32 10
  %22 = load i8*, i8** %alphaPlane, align 4, !tbaa !37
  call void @avifFree(i8* %22)
  br label %if.end25

if.end25:                                         ; preds = %if.then24, %if.then22
  %23 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %alphaPlane26 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %23, i32 0, i32 10
  store i8* null, i8** %alphaPlane26, align 4, !tbaa !37
  %24 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %alphaRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %24, i32 0, i32 11
  store i32 0, i32* %alphaRowBytes, align 4, !tbaa !38
  %25 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %imageOwnsAlphaPlane27 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %25, i32 0, i32 12
  store i32 0, i32* %imageOwnsAlphaPlane27, align 4, !tbaa !40
  br label %if.end28

if.end28:                                         ; preds = %if.end25, %if.end19
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: nounwind
define hidden void @avifImageSetProfileICC(%struct.avifImage* %image, i8* %icc, i32 %iccSize) #0 {
entry:
  %image.addr = alloca %struct.avifImage*, align 4
  %icc.addr = alloca i8*, align 4
  %iccSize.addr = alloca i32, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !5
  store i8* %icc, i8** %icc.addr, align 4, !tbaa !5
  store i32 %iccSize, i32* %iccSize.addr, align 4, !tbaa !41
  %0 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %icc1 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %0, i32 0, i32 13
  %1 = load i8*, i8** %icc.addr, align 4, !tbaa !5
  %2 = load i32, i32* %iccSize.addr, align 4, !tbaa !41
  call void @avifRWDataSet(%struct.avifRWData* %icc1, i8* %1, i32 %2)
  ret void
}

; Function Attrs: nounwind
define hidden void @avifImageSetMetadataExif(%struct.avifImage* %image, i8* %exif, i32 %exifSize) #0 {
entry:
  %image.addr = alloca %struct.avifImage*, align 4
  %exif.addr = alloca i8*, align 4
  %exifSize.addr = alloca i32, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !5
  store i8* %exif, i8** %exif.addr, align 4, !tbaa !5
  store i32 %exifSize, i32* %exifSize.addr, align 4, !tbaa !41
  %0 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %exif1 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %0, i32 0, i32 22
  %1 = load i8*, i8** %exif.addr, align 4, !tbaa !5
  %2 = load i32, i32* %exifSize.addr, align 4, !tbaa !41
  call void @avifRWDataSet(%struct.avifRWData* %exif1, i8* %1, i32 %2)
  ret void
}

; Function Attrs: nounwind
define hidden void @avifImageSetMetadataXMP(%struct.avifImage* %image, i8* %xmp, i32 %xmpSize) #0 {
entry:
  %image.addr = alloca %struct.avifImage*, align 4
  %xmp.addr = alloca i8*, align 4
  %xmpSize.addr = alloca i32, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !5
  store i8* %xmp, i8** %xmp.addr, align 4, !tbaa !5
  store i32 %xmpSize, i32* %xmpSize.addr, align 4, !tbaa !41
  %0 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %xmp1 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %0, i32 0, i32 23
  %1 = load i8*, i8** %xmp.addr, align 4, !tbaa !5
  %2 = load i32, i32* %xmpSize.addr, align 4, !tbaa !41
  call void @avifRWDataSet(%struct.avifRWData* %xmp1, i8* %1, i32 %2)
  ret void
}

; Function Attrs: nounwind
define hidden void @avifImageAllocatePlanes(%struct.avifImage* %image, i32 %planes) #0 {
entry:
  %image.addr = alloca %struct.avifImage*, align 4
  %planes.addr = alloca i32, align 4
  %channelSize = alloca i32, align 4
  %fullRowBytes = alloca i32, align 4
  %fullSize = alloca i32, align 4
  %info = alloca %struct.avifPixelFormatInfo, align 4
  %shiftedW = alloca i32, align 4
  %shiftedH = alloca i32, align 4
  %uvRowBytes = alloca i32, align 4
  %uvSize = alloca i32, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !5
  store i32 %planes, i32* %planes.addr, align 4, !tbaa !12
  %0 = bitcast i32* %channelSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %call = call i32 @avifImageUsesU16(%struct.avifImage* %1)
  %tobool = icmp ne i32 %call, 0
  %2 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 2, i32 1
  store i32 %cond, i32* %channelSize, align 4, !tbaa !12
  %3 = bitcast i32* %fullRowBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load i32, i32* %channelSize, align 4, !tbaa !12
  %5 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %5, i32 0, i32 0
  %6 = load i32, i32* %width, align 4, !tbaa !13
  %mul = mul i32 %4, %6
  store i32 %mul, i32* %fullRowBytes, align 4, !tbaa !12
  %7 = bitcast i32* %fullSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load i32, i32* %fullRowBytes, align 4, !tbaa !12
  %9 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %9, i32 0, i32 1
  %10 = load i32, i32* %height, align 4, !tbaa !21
  %mul1 = mul i32 %8, %10
  store i32 %mul1, i32* %fullSize, align 4, !tbaa !12
  %11 = load i32, i32* %planes.addr, align 4, !tbaa !12
  %and = and i32 %11, 1
  %tobool2 = icmp ne i32 %and, 0
  br i1 %tobool2, label %land.lhs.true, label %if.end42

land.lhs.true:                                    ; preds = %entry
  %12 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvFormat = getelementptr inbounds %struct.avifImage, %struct.avifImage* %12, i32 0, i32 3
  %13 = load i32, i32* %yuvFormat, align 4, !tbaa !23
  %cmp = icmp ne i32 %13, 0
  br i1 %cmp, label %if.then, label %if.end42

if.then:                                          ; preds = %land.lhs.true
  %14 = bitcast %struct.avifPixelFormatInfo* %info to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %14) #4
  %15 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvFormat3 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %15, i32 0, i32 3
  %16 = load i32, i32* %yuvFormat3, align 4, !tbaa !23
  call void @avifGetPixelFormatInfo(i32 %16, %struct.avifPixelFormatInfo* %info)
  %17 = bitcast i32* %shiftedW to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %width4 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %18, i32 0, i32 0
  %19 = load i32, i32* %width4, align 4, !tbaa !13
  %chromaShiftX = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %info, i32 0, i32 1
  %20 = load i32, i32* %chromaShiftX, align 4, !tbaa !7
  %add = add i32 %19, %20
  %chromaShiftX5 = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %info, i32 0, i32 1
  %21 = load i32, i32* %chromaShiftX5, align 4, !tbaa !7
  %shr = lshr i32 %add, %21
  store i32 %shr, i32* %shiftedW, align 4, !tbaa !12
  %22 = bitcast i32* %shiftedH to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #4
  %23 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %height6 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %23, i32 0, i32 1
  %24 = load i32, i32* %height6, align 4, !tbaa !21
  %chromaShiftY = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %info, i32 0, i32 2
  %25 = load i32, i32* %chromaShiftY, align 4, !tbaa !10
  %add7 = add i32 %24, %25
  %chromaShiftY8 = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %info, i32 0, i32 2
  %26 = load i32, i32* %chromaShiftY8, align 4, !tbaa !10
  %shr9 = lshr i32 %add7, %26
  store i32 %shr9, i32* %shiftedH, align 4, !tbaa !12
  %27 = bitcast i32* %uvRowBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load i32, i32* %channelSize, align 4, !tbaa !12
  %29 = load i32, i32* %shiftedW, align 4, !tbaa !12
  %mul10 = mul nsw i32 %28, %29
  store i32 %mul10, i32* %uvRowBytes, align 4, !tbaa !12
  %30 = bitcast i32* %uvSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  %31 = load i32, i32* %uvRowBytes, align 4, !tbaa !12
  %32 = load i32, i32* %shiftedH, align 4, !tbaa !12
  %mul11 = mul nsw i32 %31, %32
  store i32 %mul11, i32* %uvSize, align 4, !tbaa !12
  %33 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %33, i32 0, i32 6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes, i32 0, i32 0
  %34 = load i8*, i8** %arrayidx, align 4, !tbaa !5
  %tobool12 = icmp ne i8* %34, null
  br i1 %tobool12, label %if.end, label %if.then13

if.then13:                                        ; preds = %if.then
  %35 = load i32, i32* %fullRowBytes, align 4, !tbaa !12
  %36 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %36, i32 0, i32 7
  %arrayidx14 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes, i32 0, i32 0
  store i32 %35, i32* %arrayidx14, align 4, !tbaa !12
  %37 = load i32, i32* %fullSize, align 4, !tbaa !12
  %call15 = call i8* @avifAlloc(i32 %37)
  %38 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvPlanes16 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %38, i32 0, i32 6
  %arrayidx17 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes16, i32 0, i32 0
  store i8* %call15, i8** %arrayidx17, align 4, !tbaa !5
  br label %if.end

if.end:                                           ; preds = %if.then13, %if.then
  %39 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvFormat18 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %39, i32 0, i32 3
  %40 = load i32, i32* %yuvFormat18, align 4, !tbaa !23
  %cmp19 = icmp ne i32 %40, 4
  br i1 %cmp19, label %if.then20, label %if.end41

if.then20:                                        ; preds = %if.end
  %41 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvPlanes21 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %41, i32 0, i32 6
  %arrayidx22 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes21, i32 0, i32 1
  %42 = load i8*, i8** %arrayidx22, align 4, !tbaa !5
  %tobool23 = icmp ne i8* %42, null
  br i1 %tobool23, label %if.end30, label %if.then24

if.then24:                                        ; preds = %if.then20
  %43 = load i32, i32* %uvRowBytes, align 4, !tbaa !12
  %44 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvRowBytes25 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %44, i32 0, i32 7
  %arrayidx26 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes25, i32 0, i32 1
  store i32 %43, i32* %arrayidx26, align 4, !tbaa !12
  %45 = load i32, i32* %uvSize, align 4, !tbaa !12
  %call27 = call i8* @avifAlloc(i32 %45)
  %46 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvPlanes28 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %46, i32 0, i32 6
  %arrayidx29 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes28, i32 0, i32 1
  store i8* %call27, i8** %arrayidx29, align 4, !tbaa !5
  br label %if.end30

if.end30:                                         ; preds = %if.then24, %if.then20
  %47 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvPlanes31 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %47, i32 0, i32 6
  %arrayidx32 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes31, i32 0, i32 2
  %48 = load i8*, i8** %arrayidx32, align 4, !tbaa !5
  %tobool33 = icmp ne i8* %48, null
  br i1 %tobool33, label %if.end40, label %if.then34

if.then34:                                        ; preds = %if.end30
  %49 = load i32, i32* %uvRowBytes, align 4, !tbaa !12
  %50 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvRowBytes35 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %50, i32 0, i32 7
  %arrayidx36 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes35, i32 0, i32 2
  store i32 %49, i32* %arrayidx36, align 4, !tbaa !12
  %51 = load i32, i32* %uvSize, align 4, !tbaa !12
  %call37 = call i8* @avifAlloc(i32 %51)
  %52 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %yuvPlanes38 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %52, i32 0, i32 6
  %arrayidx39 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes38, i32 0, i32 2
  store i8* %call37, i8** %arrayidx39, align 4, !tbaa !5
  br label %if.end40

if.end40:                                         ; preds = %if.then34, %if.end30
  br label %if.end41

if.end41:                                         ; preds = %if.end40, %if.end
  %53 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %imageOwnsYUVPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %53, i32 0, i32 8
  store i32 1, i32* %imageOwnsYUVPlanes, align 4, !tbaa !39
  %54 = bitcast i32* %uvSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #4
  %55 = bitcast i32* %uvRowBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #4
  %56 = bitcast i32* %shiftedH to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #4
  %57 = bitcast i32* %shiftedW to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #4
  %58 = bitcast %struct.avifPixelFormatInfo* %info to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %58) #4
  br label %if.end42

if.end42:                                         ; preds = %if.end41, %land.lhs.true, %entry
  %59 = load i32, i32* %planes.addr, align 4, !tbaa !12
  %and43 = and i32 %59, 2
  %tobool44 = icmp ne i32 %and43, 0
  br i1 %tobool44, label %if.then45, label %if.end53

if.then45:                                        ; preds = %if.end42
  %60 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %alphaPlane = getelementptr inbounds %struct.avifImage, %struct.avifImage* %60, i32 0, i32 10
  %61 = load i8*, i8** %alphaPlane, align 4, !tbaa !37
  %tobool46 = icmp ne i8* %61, null
  br i1 %tobool46, label %if.end52, label %if.then47

if.then47:                                        ; preds = %if.then45
  %62 = load i32, i32* %fullRowBytes, align 4, !tbaa !12
  %63 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %alphaRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %63, i32 0, i32 11
  store i32 %62, i32* %alphaRowBytes, align 4, !tbaa !38
  %64 = load i32, i32* %fullRowBytes, align 4, !tbaa !12
  %65 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %height48 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %65, i32 0, i32 1
  %66 = load i32, i32* %height48, align 4, !tbaa !21
  %mul49 = mul i32 %64, %66
  %call50 = call i8* @avifAlloc(i32 %mul49)
  %67 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %alphaPlane51 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %67, i32 0, i32 10
  store i8* %call50, i8** %alphaPlane51, align 4, !tbaa !37
  br label %if.end52

if.end52:                                         ; preds = %if.then47, %if.then45
  %68 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %imageOwnsAlphaPlane = getelementptr inbounds %struct.avifImage, %struct.avifImage* %68, i32 0, i32 12
  store i32 1, i32* %imageOwnsAlphaPlane, align 4, !tbaa !40
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %if.end42
  %69 = bitcast i32* %fullSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #4
  %70 = bitcast i32* %fullRowBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #4
  %71 = bitcast i32* %channelSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #4
  ret void
}

declare void @avifFree(i8*) #3

; Function Attrs: nounwind
define hidden void @avifImageDestroy(%struct.avifImage* %image) #0 {
entry:
  %image.addr = alloca %struct.avifImage*, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %0 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  call void @avifImageFreePlanes(%struct.avifImage* %0, i32 255)
  %1 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %icc = getelementptr inbounds %struct.avifImage, %struct.avifImage* %1, i32 0, i32 13
  call void @avifRWDataFree(%struct.avifRWData* %icc)
  %2 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %exif = getelementptr inbounds %struct.avifImage, %struct.avifImage* %2, i32 0, i32 22
  call void @avifRWDataFree(%struct.avifRWData* %exif)
  %3 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %xmp = getelementptr inbounds %struct.avifImage, %struct.avifImage* %3, i32 0, i32 23
  call void @avifRWDataFree(%struct.avifRWData* %xmp)
  %4 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %5 = bitcast %struct.avifImage* %4 to i8*
  call void @avifFree(i8* %5)
  ret void
}

declare void @avifRWDataFree(%struct.avifRWData*) #3

declare void @avifRWDataSet(%struct.avifRWData*, i8*, i32) #3

; Function Attrs: nounwind
define hidden i32 @avifImageUsesU16(%struct.avifImage* %image) #0 {
entry:
  %image.addr = alloca %struct.avifImage*, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %0 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %0, i32 0, i32 2
  %1 = load i32, i32* %depth, align 4, !tbaa !22
  %cmp = icmp ugt i32 %1, 8
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: nounwind
define hidden void @avifImageStealPlanes(%struct.avifImage* %dstImage, %struct.avifImage* %srcImage, i32 %planes) #0 {
entry:
  %dstImage.addr = alloca %struct.avifImage*, align 4
  %srcImage.addr = alloca %struct.avifImage*, align 4
  %planes.addr = alloca i32, align 4
  store %struct.avifImage* %dstImage, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  store %struct.avifImage* %srcImage, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  store i32 %planes, i32* %planes.addr, align 4, !tbaa !12
  %0 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %1 = load i32, i32* %planes.addr, align 4, !tbaa !12
  call void @avifImageFreePlanes(%struct.avifImage* %0, i32 %1)
  %2 = load i32, i32* %planes.addr, align 4, !tbaa !12
  %and = and i32 %2, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %3, i32 0, i32 6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes, i32 0, i32 0
  %4 = load i8*, i8** %arrayidx, align 4, !tbaa !5
  %5 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %yuvPlanes1 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %5, i32 0, i32 6
  %arrayidx2 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes1, i32 0, i32 0
  store i8* %4, i8** %arrayidx2, align 4, !tbaa !5
  %6 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %6, i32 0, i32 7
  %arrayidx3 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes, i32 0, i32 0
  %7 = load i32, i32* %arrayidx3, align 4, !tbaa !12
  %8 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %yuvRowBytes4 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %8, i32 0, i32 7
  %arrayidx5 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes4, i32 0, i32 0
  store i32 %7, i32* %arrayidx5, align 4, !tbaa !12
  %9 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvPlanes6 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %9, i32 0, i32 6
  %arrayidx7 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes6, i32 0, i32 1
  %10 = load i8*, i8** %arrayidx7, align 4, !tbaa !5
  %11 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %yuvPlanes8 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %11, i32 0, i32 6
  %arrayidx9 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes8, i32 0, i32 1
  store i8* %10, i8** %arrayidx9, align 4, !tbaa !5
  %12 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvRowBytes10 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %12, i32 0, i32 7
  %arrayidx11 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes10, i32 0, i32 1
  %13 = load i32, i32* %arrayidx11, align 4, !tbaa !12
  %14 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %yuvRowBytes12 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %14, i32 0, i32 7
  %arrayidx13 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes12, i32 0, i32 1
  store i32 %13, i32* %arrayidx13, align 4, !tbaa !12
  %15 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvPlanes14 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %15, i32 0, i32 6
  %arrayidx15 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes14, i32 0, i32 2
  %16 = load i8*, i8** %arrayidx15, align 4, !tbaa !5
  %17 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %yuvPlanes16 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %17, i32 0, i32 6
  %arrayidx17 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes16, i32 0, i32 2
  store i8* %16, i8** %arrayidx17, align 4, !tbaa !5
  %18 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvRowBytes18 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %18, i32 0, i32 7
  %arrayidx19 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes18, i32 0, i32 2
  %19 = load i32, i32* %arrayidx19, align 4, !tbaa !12
  %20 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %yuvRowBytes20 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %20, i32 0, i32 7
  %arrayidx21 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes20, i32 0, i32 2
  store i32 %19, i32* %arrayidx21, align 4, !tbaa !12
  %21 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvPlanes22 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %21, i32 0, i32 6
  %arrayidx23 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes22, i32 0, i32 0
  store i8* null, i8** %arrayidx23, align 4, !tbaa !5
  %22 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvRowBytes24 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %22, i32 0, i32 7
  %arrayidx25 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes24, i32 0, i32 0
  store i32 0, i32* %arrayidx25, align 4, !tbaa !12
  %23 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvPlanes26 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %23, i32 0, i32 6
  %arrayidx27 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes26, i32 0, i32 1
  store i8* null, i8** %arrayidx27, align 4, !tbaa !5
  %24 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvRowBytes28 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %24, i32 0, i32 7
  %arrayidx29 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes28, i32 0, i32 1
  store i32 0, i32* %arrayidx29, align 4, !tbaa !12
  %25 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvPlanes30 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %25, i32 0, i32 6
  %arrayidx31 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes30, i32 0, i32 2
  store i8* null, i8** %arrayidx31, align 4, !tbaa !5
  %26 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvRowBytes32 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %26, i32 0, i32 7
  %arrayidx33 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes32, i32 0, i32 2
  store i32 0, i32* %arrayidx33, align 4, !tbaa !12
  %27 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvFormat = getelementptr inbounds %struct.avifImage, %struct.avifImage* %27, i32 0, i32 3
  %28 = load i32, i32* %yuvFormat, align 4, !tbaa !23
  %29 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %yuvFormat34 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %29, i32 0, i32 3
  store i32 %28, i32* %yuvFormat34, align 4, !tbaa !23
  %30 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvRange = getelementptr inbounds %struct.avifImage, %struct.avifImage* %30, i32 0, i32 4
  %31 = load i32, i32* %yuvRange, align 4, !tbaa !24
  %32 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %yuvRange35 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %32, i32 0, i32 4
  store i32 %31, i32* %yuvRange35, align 4, !tbaa !24
  %33 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %yuvChromaSamplePosition = getelementptr inbounds %struct.avifImage, %struct.avifImage* %33, i32 0, i32 5
  %34 = load i32, i32* %yuvChromaSamplePosition, align 4, !tbaa !29
  %35 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %yuvChromaSamplePosition36 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %35, i32 0, i32 5
  store i32 %34, i32* %yuvChromaSamplePosition36, align 4, !tbaa !29
  %36 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %imageOwnsYUVPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %36, i32 0, i32 8
  %37 = load i32, i32* %imageOwnsYUVPlanes, align 4, !tbaa !39
  %38 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %imageOwnsYUVPlanes37 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %38, i32 0, i32 8
  store i32 %37, i32* %imageOwnsYUVPlanes37, align 4, !tbaa !39
  %39 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %imageOwnsYUVPlanes38 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %39, i32 0, i32 8
  store i32 0, i32* %imageOwnsYUVPlanes38, align 4, !tbaa !39
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %40 = load i32, i32* %planes.addr, align 4, !tbaa !12
  %and39 = and i32 %40, 2
  %tobool40 = icmp ne i32 %and39, 0
  br i1 %tobool40, label %if.then41, label %if.end49

if.then41:                                        ; preds = %if.end
  %41 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %alphaPlane = getelementptr inbounds %struct.avifImage, %struct.avifImage* %41, i32 0, i32 10
  %42 = load i8*, i8** %alphaPlane, align 4, !tbaa !37
  %43 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %alphaPlane42 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %43, i32 0, i32 10
  store i8* %42, i8** %alphaPlane42, align 4, !tbaa !37
  %44 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %alphaRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %44, i32 0, i32 11
  %45 = load i32, i32* %alphaRowBytes, align 4, !tbaa !38
  %46 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %alphaRowBytes43 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %46, i32 0, i32 11
  store i32 %45, i32* %alphaRowBytes43, align 4, !tbaa !38
  %47 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %alphaRange = getelementptr inbounds %struct.avifImage, %struct.avifImage* %47, i32 0, i32 9
  %48 = load i32, i32* %alphaRange, align 4, !tbaa !25
  %49 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %alphaRange44 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %49, i32 0, i32 9
  store i32 %48, i32* %alphaRange44, align 4, !tbaa !25
  %50 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %alphaPlane45 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %50, i32 0, i32 10
  store i8* null, i8** %alphaPlane45, align 4, !tbaa !37
  %51 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %alphaRowBytes46 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %51, i32 0, i32 11
  store i32 0, i32* %alphaRowBytes46, align 4, !tbaa !38
  %52 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %imageOwnsAlphaPlane = getelementptr inbounds %struct.avifImage, %struct.avifImage* %52, i32 0, i32 12
  %53 = load i32, i32* %imageOwnsAlphaPlane, align 4, !tbaa !40
  %54 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !5
  %imageOwnsAlphaPlane47 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %54, i32 0, i32 12
  store i32 %53, i32* %imageOwnsAlphaPlane47, align 4, !tbaa !40
  %55 = load %struct.avifImage*, %struct.avifImage** %srcImage.addr, align 4, !tbaa !5
  %imageOwnsAlphaPlane48 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %55, i32 0, i32 12
  store i32 0, i32* %imageOwnsAlphaPlane48, align 4, !tbaa !40
  br label %if.end49

if.end49:                                         ; preds = %if.then41, %if.end
  ret void
}

; Function Attrs: nounwind
define hidden void @avifCodecDestroy(%struct.avifCodec* %codec) #0 {
entry:
  %codec.addr = alloca %struct.avifCodec*, align 4
  store %struct.avifCodec* %codec, %struct.avifCodec** %codec.addr, align 4, !tbaa !5
  %0 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !5
  %tobool = icmp ne %struct.avifCodec* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !5
  %destroyInternal = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %1, i32 0, i32 7
  %destroyInternal1 = bitcast {}** %destroyInternal to void (%struct.avifCodec*)**
  %2 = load void (%struct.avifCodec*)*, void (%struct.avifCodec*)** %destroyInternal1, align 4, !tbaa !42
  %tobool2 = icmp ne void (%struct.avifCodec*)* %2, null
  br i1 %tobool2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %3 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !5
  %destroyInternal3 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %3, i32 0, i32 7
  %destroyInternal4 = bitcast {}** %destroyInternal3 to void (%struct.avifCodec*)**
  %4 = load void (%struct.avifCodec*)*, void (%struct.avifCodec*)** %destroyInternal4, align 4, !tbaa !42
  %5 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !5
  call void %4(%struct.avifCodec* %5)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %6 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !5
  %7 = bitcast %struct.avifCodec* %6 to i8*
  call void @avifFree(i8* %7)
  ret void
}

; Function Attrs: nounwind
define hidden i32 @avifRGBFormatHasAlpha(i32 %format) #0 {
entry:
  %format.addr = alloca i32, align 4
  store i32 %format, i32* %format.addr, align 4, !tbaa !2
  %0 = load i32, i32* %format.addr, align 4, !tbaa !2
  %cmp = icmp ne i32 %0, 0
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load i32, i32* %format.addr, align 4, !tbaa !2
  %cmp1 = icmp ne i32 %1, 3
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %cmp1, %land.rhs ]
  %land.ext = zext i1 %2 to i32
  ret i32 %land.ext
}

; Function Attrs: nounwind
define hidden i32 @avifRGBFormatChannelCount(i32 %format) #0 {
entry:
  %format.addr = alloca i32, align 4
  store i32 %format, i32* %format.addr, align 4, !tbaa !2
  %0 = load i32, i32* %format.addr, align 4, !tbaa !2
  %call = call i32 @avifRGBFormatHasAlpha(i32 %0)
  %tobool = icmp ne i32 %call, 0
  %1 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 4, i32 3
  ret i32 %cond
}

; Function Attrs: nounwind
define hidden i32 @avifRGBImagePixelSize(%struct.avifRGBImage* %rgb) #0 {
entry:
  %rgb.addr = alloca %struct.avifRGBImage*, align 4
  store %struct.avifRGBImage* %rgb, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %0 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %format = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %0, i32 0, i32 3
  %1 = load i32, i32* %format, align 4, !tbaa !45
  %call = call i32 @avifRGBFormatChannelCount(i32 %1)
  %2 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %depth = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %2, i32 0, i32 2
  %3 = load i32, i32* %depth, align 4, !tbaa !47
  %cmp = icmp ugt i32 %3, 8
  %4 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 2, i32 1
  %mul = mul i32 %call, %cond
  ret i32 %mul
}

; Function Attrs: nounwind
define hidden void @avifRGBImageSetDefaults(%struct.avifRGBImage* %rgb, %struct.avifImage* %image) #0 {
entry:
  %rgb.addr = alloca %struct.avifRGBImage*, align 4
  %image.addr = alloca %struct.avifImage*, align 4
  store %struct.avifRGBImage* %rgb, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %0 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %0, i32 0, i32 0
  %1 = load i32, i32* %width, align 4, !tbaa !13
  %2 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %width1 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %2, i32 0, i32 0
  store i32 %1, i32* %width1, align 4, !tbaa !48
  %3 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %3, i32 0, i32 1
  %4 = load i32, i32* %height, align 4, !tbaa !21
  %5 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %height2 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %5, i32 0, i32 1
  store i32 %4, i32* %height2, align 4, !tbaa !49
  %6 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !5
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %6, i32 0, i32 2
  %7 = load i32, i32* %depth, align 4, !tbaa !22
  %8 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %depth3 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %8, i32 0, i32 2
  store i32 %7, i32* %depth3, align 4, !tbaa !47
  %9 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %format = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %9, i32 0, i32 3
  store i32 1, i32* %format, align 4, !tbaa !45
  %10 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %chromaUpsampling = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %10, i32 0, i32 4
  store i32 0, i32* %chromaUpsampling, align 4, !tbaa !50
  %11 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %ignoreAlpha = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %11, i32 0, i32 5
  store i32 0, i32* %ignoreAlpha, align 4, !tbaa !51
  %12 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %pixels = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %12, i32 0, i32 6
  store i8* null, i8** %pixels, align 4, !tbaa !52
  %13 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %rowBytes = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %13, i32 0, i32 7
  store i32 0, i32* %rowBytes, align 4, !tbaa !53
  ret void
}

; Function Attrs: nounwind
define hidden void @avifRGBImageAllocatePixels(%struct.avifRGBImage* %rgb) #0 {
entry:
  %rgb.addr = alloca %struct.avifRGBImage*, align 4
  store %struct.avifRGBImage* %rgb, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %0 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %pixels = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %0, i32 0, i32 6
  %1 = load i8*, i8** %pixels, align 4, !tbaa !52
  %tobool = icmp ne i8* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %pixels1 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %2, i32 0, i32 6
  %3 = load i8*, i8** %pixels1, align 4, !tbaa !52
  call void @avifFree(i8* %3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %width = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %4, i32 0, i32 0
  %5 = load i32, i32* %width, align 4, !tbaa !48
  %6 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %call = call i32 @avifRGBImagePixelSize(%struct.avifRGBImage* %6)
  %mul = mul i32 %5, %call
  %7 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %rowBytes = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %7, i32 0, i32 7
  store i32 %mul, i32* %rowBytes, align 4, !tbaa !53
  %8 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %rowBytes2 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %8, i32 0, i32 7
  %9 = load i32, i32* %rowBytes2, align 4, !tbaa !53
  %10 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %height = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %10, i32 0, i32 1
  %11 = load i32, i32* %height, align 4, !tbaa !49
  %mul3 = mul i32 %9, %11
  %call4 = call i8* @avifAlloc(i32 %mul3)
  %12 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %pixels5 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %12, i32 0, i32 6
  store i8* %call4, i8** %pixels5, align 4, !tbaa !52
  ret void
}

; Function Attrs: nounwind
define hidden void @avifRGBImageFreePixels(%struct.avifRGBImage* %rgb) #0 {
entry:
  %rgb.addr = alloca %struct.avifRGBImage*, align 4
  store %struct.avifRGBImage* %rgb, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %0 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %pixels = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %0, i32 0, i32 6
  %1 = load i8*, i8** %pixels, align 4, !tbaa !52
  %tobool = icmp ne i8* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %pixels1 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %2, i32 0, i32 6
  %3 = load i8*, i8** %pixels1, align 4, !tbaa !52
  call void @avifFree(i8* %3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %pixels2 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %4, i32 0, i32 6
  store i8* null, i8** %pixels2, align 4, !tbaa !52
  %5 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !5
  %rowBytes = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %5, i32 0, i32 7
  store i32 0, i32* %rowBytes, align 4, !tbaa !53
  ret void
}

; Function Attrs: nounwind
define hidden i8* @avifCodecName(i32 %choice, i32 %requiredFlags) #0 {
entry:
  %retval = alloca i8*, align 4
  %choice.addr = alloca i32, align 4
  %requiredFlags.addr = alloca i32, align 4
  %availableCodec = alloca %struct.AvailableCodec*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i32 %choice, i32* %choice.addr, align 4, !tbaa !2
  store i32 %requiredFlags, i32* %requiredFlags.addr, align 4, !tbaa !12
  %0 = bitcast %struct.AvailableCodec** %availableCodec to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %choice.addr, align 4, !tbaa !2
  %2 = load i32, i32* %requiredFlags.addr, align 4, !tbaa !12
  %call = call %struct.AvailableCodec* @findAvailableCodec(i32 %1, i32 %2)
  store %struct.AvailableCodec* %call, %struct.AvailableCodec** %availableCodec, align 4, !tbaa !5
  %3 = load %struct.AvailableCodec*, %struct.AvailableCodec** %availableCodec, align 4, !tbaa !5
  %tobool = icmp ne %struct.AvailableCodec* %3, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %struct.AvailableCodec*, %struct.AvailableCodec** %availableCodec, align 4, !tbaa !5
  %name = getelementptr inbounds %struct.AvailableCodec, %struct.AvailableCodec* %4, i32 0, i32 1
  %5 = load i8*, i8** %name, align 4, !tbaa !54
  store i8* %5, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %6 = bitcast %struct.AvailableCodec** %availableCodec to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #4
  %7 = load i8*, i8** %retval, align 4
  ret i8* %7
}

; Function Attrs: nounwind
define internal %struct.AvailableCodec* @findAvailableCodec(i32 %choice, i32 %requiredFlags) #0 {
entry:
  %retval = alloca %struct.AvailableCodec*, align 4
  %choice.addr = alloca i32, align 4
  %requiredFlags.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i32 %choice, i32* %choice.addr, align 4, !tbaa !2
  store i32 %requiredFlags, i32* %requiredFlags.addr, align 4, !tbaa !12
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !12
  %cmp = icmp slt i32 %1, 1
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %choice.addr, align 4, !tbaa !2
  %cmp1 = icmp ne i32 %2, 0
  br i1 %cmp1, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %3 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds [2 x %struct.AvailableCodec], [2 x %struct.AvailableCodec]* @availableCodecs, i32 0, i32 %3
  %choice2 = getelementptr inbounds %struct.AvailableCodec, %struct.AvailableCodec* %arrayidx, i32 0, i32 0
  %4 = load i32, i32* %choice2, align 4, !tbaa !56
  %5 = load i32, i32* %choice.addr, align 4, !tbaa !2
  %cmp3 = icmp ne i32 %4, %5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  br label %for.inc

if.end:                                           ; preds = %land.lhs.true, %for.body
  %6 = load i32, i32* %requiredFlags.addr, align 4, !tbaa !12
  %tobool = icmp ne i32 %6, 0
  br i1 %tobool, label %land.lhs.true4, label %if.end8

land.lhs.true4:                                   ; preds = %if.end
  %7 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx5 = getelementptr inbounds [2 x %struct.AvailableCodec], [2 x %struct.AvailableCodec]* @availableCodecs, i32 0, i32 %7
  %flags = getelementptr inbounds %struct.AvailableCodec, %struct.AvailableCodec* %arrayidx5, i32 0, i32 4
  %8 = load i32, i32* %flags, align 4, !tbaa !57
  %9 = load i32, i32* %requiredFlags.addr, align 4, !tbaa !12
  %and = and i32 %8, %9
  %10 = load i32, i32* %requiredFlags.addr, align 4, !tbaa !12
  %cmp6 = icmp ne i32 %and, %10
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %land.lhs.true4
  br label %for.inc

if.end8:                                          ; preds = %land.lhs.true4, %if.end
  %11 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx9 = getelementptr inbounds [2 x %struct.AvailableCodec], [2 x %struct.AvailableCodec]* @availableCodecs, i32 0, i32 %11
  store %struct.AvailableCodec* %arrayidx9, %struct.AvailableCodec** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.inc:                                          ; preds = %if.then7, %if.then
  %12 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

cleanup:                                          ; preds = %if.end8, %for.cond.cleanup
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 2, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup
  store %struct.AvailableCodec* null, %struct.AvailableCodec** %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %cleanup
  %14 = load %struct.AvailableCodec*, %struct.AvailableCodec** %retval, align 4
  ret %struct.AvailableCodec* %14

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden i32 @avifCodecChoiceFromName(i8* %name) #0 {
entry:
  %retval = alloca i32, align 4
  %name.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %name, i8** %name.addr, align 4, !tbaa !5
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !12
  %cmp = icmp slt i32 %1, 1
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds [2 x %struct.AvailableCodec], [2 x %struct.AvailableCodec]* @availableCodecs, i32 0, i32 %2
  %name1 = getelementptr inbounds %struct.AvailableCodec, %struct.AvailableCodec* %arrayidx, i32 0, i32 1
  %3 = load i8*, i8** %name1, align 4, !tbaa !54
  %4 = load i8*, i8** %name.addr, align 4, !tbaa !5
  %call = call i32 @strcmp(i8* %3, i8* %4)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx2 = getelementptr inbounds [2 x %struct.AvailableCodec], [2 x %struct.AvailableCodec]* @availableCodecs, i32 0, i32 %5
  %choice = getelementptr inbounds %struct.AvailableCodec, %struct.AvailableCodec* %arrayidx2, i32 0, i32 0
  %6 = load i32, i32* %choice, align 4, !tbaa !56
  store i32 %6, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

cleanup:                                          ; preds = %if.then, %for.cond.cleanup
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 2, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %cleanup
  %9 = load i32, i32* %retval, align 4
  ret i32 %9

unreachable:                                      ; preds = %cleanup
  unreachable
}

declare i32 @strcmp(i8*, i8*) #3

; Function Attrs: nounwind
define hidden %struct.avifCodec* @avifCodecCreate(i32 %choice, i32 %requiredFlags) #0 {
entry:
  %retval = alloca %struct.avifCodec*, align 4
  %choice.addr = alloca i32, align 4
  %requiredFlags.addr = alloca i32, align 4
  %availableCodec = alloca %struct.AvailableCodec*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i32 %choice, i32* %choice.addr, align 4, !tbaa !2
  store i32 %requiredFlags, i32* %requiredFlags.addr, align 4, !tbaa !12
  %0 = bitcast %struct.AvailableCodec** %availableCodec to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %choice.addr, align 4, !tbaa !2
  %2 = load i32, i32* %requiredFlags.addr, align 4, !tbaa !12
  %call = call %struct.AvailableCodec* @findAvailableCodec(i32 %1, i32 %2)
  store %struct.AvailableCodec* %call, %struct.AvailableCodec** %availableCodec, align 4, !tbaa !5
  %3 = load %struct.AvailableCodec*, %struct.AvailableCodec** %availableCodec, align 4, !tbaa !5
  %tobool = icmp ne %struct.AvailableCodec* %3, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %struct.AvailableCodec*, %struct.AvailableCodec** %availableCodec, align 4, !tbaa !5
  %create = getelementptr inbounds %struct.AvailableCodec, %struct.AvailableCodec* %4, i32 0, i32 3
  %5 = load %struct.avifCodec* ()*, %struct.avifCodec* ()** %create, align 4, !tbaa !58
  %call1 = call %struct.avifCodec* %5()
  store %struct.avifCodec* %call1, %struct.avifCodec** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store %struct.avifCodec* null, %struct.avifCodec** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %6 = bitcast %struct.AvailableCodec** %availableCodec to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.avifCodec*, %struct.avifCodec** %retval, align 4
  ret %struct.avifCodec* %7
}

; Function Attrs: nounwind
define hidden void @avifCodecVersions(i8* %outBuffer) #0 {
entry:
  %outBuffer.addr = alloca i8*, align 4
  %remainingLen = alloca i32, align 4
  %writePos = alloca i8*, align 4
  %i = alloca i32, align 4
  store i8* %outBuffer, i8** %outBuffer.addr, align 4, !tbaa !5
  %0 = bitcast i32* %remainingLen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 255, i32* %remainingLen, align 4, !tbaa !41
  %1 = bitcast i8** %writePos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i8*, i8** %outBuffer.addr, align 4, !tbaa !5
  store i8* %2, i8** %writePos, align 4, !tbaa !5
  %3 = load i8*, i8** %writePos, align 4, !tbaa !5
  store i8 0, i8* %3, align 1, !tbaa !2
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4, !tbaa !12
  %cmp = icmp slt i32 %5, 1
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i32, i32* %i, align 4, !tbaa !12
  %cmp1 = icmp sgt i32 %7, 0
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  call void @append(i8** %writePos, i32* %remainingLen, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.25, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %8 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds [2 x %struct.AvailableCodec], [2 x %struct.AvailableCodec]* @availableCodecs, i32 0, i32 %8
  %name = getelementptr inbounds %struct.AvailableCodec, %struct.AvailableCodec* %arrayidx, i32 0, i32 1
  %9 = load i8*, i8** %name, align 4, !tbaa !54
  call void @append(i8** %writePos, i32* %remainingLen, i8* %9)
  call void @append(i8** %writePos, i32* %remainingLen, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.26, i32 0, i32 0))
  %10 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx2 = getelementptr inbounds [2 x %struct.AvailableCodec], [2 x %struct.AvailableCodec]* @availableCodecs, i32 0, i32 %10
  %version = getelementptr inbounds %struct.AvailableCodec, %struct.AvailableCodec* %arrayidx2, i32 0, i32 2
  %11 = load i8* ()*, i8* ()** %version, align 4, !tbaa !59
  %call = call i8* %11()
  call void @append(i8** %writePos, i32* %remainingLen, i8* %call)
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %12 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %13 = bitcast i8** %writePos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %remainingLen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  ret void
}

; Function Attrs: nounwind
define internal void @append(i8** %writePos, i32* %remainingLen, i8* %appendStr) #0 {
entry:
  %writePos.addr = alloca i8**, align 4
  %remainingLen.addr = alloca i32*, align 4
  %appendStr.addr = alloca i8*, align 4
  %appendLen = alloca i32, align 4
  store i8** %writePos, i8*** %writePos.addr, align 4, !tbaa !5
  store i32* %remainingLen, i32** %remainingLen.addr, align 4, !tbaa !5
  store i8* %appendStr, i8** %appendStr.addr, align 4, !tbaa !5
  %0 = bitcast i32* %appendLen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %appendStr.addr, align 4, !tbaa !5
  %call = call i32 @strlen(i8* %1)
  store i32 %call, i32* %appendLen, align 4, !tbaa !41
  %2 = load i32, i32* %appendLen, align 4, !tbaa !41
  %3 = load i32*, i32** %remainingLen.addr, align 4, !tbaa !5
  %4 = load i32, i32* %3, align 4, !tbaa !41
  %cmp = icmp ugt i32 %2, %4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load i32*, i32** %remainingLen.addr, align 4, !tbaa !5
  %6 = load i32, i32* %5, align 4, !tbaa !41
  store i32 %6, i32* %appendLen, align 4, !tbaa !41
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load i8**, i8*** %writePos.addr, align 4, !tbaa !5
  %8 = load i8*, i8** %7, align 4, !tbaa !5
  %9 = load i8*, i8** %appendStr.addr, align 4, !tbaa !5
  %10 = load i32, i32* %appendLen, align 4, !tbaa !41
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %8, i8* align 1 %9, i32 %10, i1 false)
  %11 = load i32, i32* %appendLen, align 4, !tbaa !41
  %12 = load i32*, i32** %remainingLen.addr, align 4, !tbaa !5
  %13 = load i32, i32* %12, align 4, !tbaa !41
  %sub = sub i32 %13, %11
  store i32 %sub, i32* %12, align 4, !tbaa !41
  %14 = load i32, i32* %appendLen, align 4, !tbaa !41
  %15 = load i8**, i8*** %writePos.addr, align 4, !tbaa !5
  %16 = load i8*, i8** %15, align 4, !tbaa !5
  %add.ptr = getelementptr inbounds i8, i8* %16, i32 %14
  store i8* %add.ptr, i8** %15, align 4, !tbaa !5
  %17 = load i8**, i8*** %writePos.addr, align 4, !tbaa !5
  %18 = load i8*, i8** %17, align 4, !tbaa !5
  store i8 0, i8* %18, align 1, !tbaa !2
  %19 = bitcast i32* %appendLen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  ret void
}

declare i8* @avifCodecVersionAOM() #3

declare %struct.avifCodec* @avifCodecCreateAOM() #3

declare i32 @strlen(i8*) #3

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn writeonly }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = !{!6, !6, i64 0}
!6 = !{!"any pointer", !3, i64 0}
!7 = !{!8, !9, i64 4}
!8 = !{!"avifPixelFormatInfo", !9, i64 0, !9, i64 4, !9, i64 8}
!9 = !{!"int", !3, i64 0}
!10 = !{!8, !9, i64 8}
!11 = !{!8, !9, i64 0}
!12 = !{!9, !9, i64 0}
!13 = !{!14, !9, i64 0}
!14 = !{!"avifImage", !9, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 36, !9, i64 48, !3, i64 52, !6, i64 56, !9, i64 60, !9, i64 64, !15, i64 68, !3, i64 76, !3, i64 80, !3, i64 84, !9, i64 88, !17, i64 92, !18, i64 100, !19, i64 132, !20, i64 133, !15, i64 136, !15, i64 144}
!15 = !{!"avifRWData", !6, i64 0, !16, i64 4}
!16 = !{!"long", !3, i64 0}
!17 = !{!"avifPixelAspectRatioBox", !9, i64 0, !9, i64 4}
!18 = !{!"avifCleanApertureBox", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !9, i64 20, !9, i64 24, !9, i64 28}
!19 = !{!"avifImageRotation", !3, i64 0}
!20 = !{!"avifImageMirror", !3, i64 0}
!21 = !{!14, !9, i64 4}
!22 = !{!14, !9, i64 8}
!23 = !{!14, !3, i64 12}
!24 = !{!14, !3, i64 16}
!25 = !{!14, !3, i64 52}
!26 = !{!14, !3, i64 76}
!27 = !{!14, !3, i64 80}
!28 = !{!14, !3, i64 84}
!29 = !{!14, !3, i64 20}
!30 = !{!14, !9, i64 88}
!31 = !{!14, !6, i64 68}
!32 = !{!14, !16, i64 72}
!33 = !{!14, !6, i64 136}
!34 = !{!14, !16, i64 140}
!35 = !{!14, !6, i64 144}
!36 = !{!14, !16, i64 148}
!37 = !{!14, !6, i64 56}
!38 = !{!14, !9, i64 60}
!39 = !{!14, !9, i64 48}
!40 = !{!14, !9, i64 64}
!41 = !{!16, !16, i64 0}
!42 = !{!43, !6, i64 36}
!43 = !{!"avifCodec", !6, i64 0, !44, i64 4, !6, i64 16, !6, i64 20, !6, i64 24, !6, i64 28, !6, i64 32, !6, i64 36}
!44 = !{!"avifCodecConfigurationBox", !3, i64 0, !3, i64 1, !3, i64 2, !3, i64 3, !3, i64 4, !3, i64 5, !3, i64 6, !3, i64 7, !3, i64 8}
!45 = !{!46, !3, i64 12}
!46 = !{!"avifRGBImage", !9, i64 0, !9, i64 4, !9, i64 8, !3, i64 12, !3, i64 16, !9, i64 20, !6, i64 24, !9, i64 28}
!47 = !{!46, !9, i64 8}
!48 = !{!46, !9, i64 0}
!49 = !{!46, !9, i64 4}
!50 = !{!46, !3, i64 16}
!51 = !{!46, !9, i64 20}
!52 = !{!46, !6, i64 24}
!53 = !{!46, !9, i64 28}
!54 = !{!55, !6, i64 4}
!55 = !{!"AvailableCodec", !3, i64 0, !6, i64 4, !6, i64 8, !6, i64 12, !9, i64 16}
!56 = !{!55, !3, i64 0}
!57 = !{!55, !9, i64 16}
!58 = !{!55, !6, i64 12}
!59 = !{!55, !6, i64 8}
